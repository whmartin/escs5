﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucContratoDetail.ascx.cs"
    Inherits="Page_Contratos_UserControl_ucContratoDetail" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
<link type="text/css" href="../../../Styles/direccion.css" rel="Stylesheet" />
<style type="text/css">
    .auto-style1 {
        width: 203px;
    }
    .auto-style2 {
        height: 26px;
    }
</style>
<asp:HiddenField ID="hfIdContrato" runat="server" />
<asp:HiddenField ID="hfContratoMigrado" runat="server" />
<div style="margin-right: 30px">
    <table cellpadding="0" cellspacing="0" id="fT">
        <tr>
            <td>
                <h2>
                    <asp:Image ID="imgPrograma" ImageUrl="~/Image/main/prog-edit.png" runat="server"
                        alt="programa" CssClass="CI" />
                    <asp:Label ID="lblTitulo" runat="server"></asp:Label></h2>
            </td>
            <td id="eT">
            </td>
            <td id="fTb">
            </td>
        </tr>
    </table>
</div>
<table width="90%" align="center">
    <tr>
        <td align="center">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <div id="AccContratos" runat="server" width="100%" height="100%">
                <div id="PanelesAcordeon">
                    <div id="ApDatosGeneralContrato" runat="server">
                        <div id="ContentApDatosGeneralContrato">
                            <table width="90%" align="center">
                                <tr class="rowB">
                                    <td colspan="3">
                                        <div id="dvUsuarioCreacion" runat="server" visible="false">
                                            <table width="100%">
                                                <tr class="rowB">
                                                    <td colspan="2">
                                                        <asp:MultiView ID="MultiViewAsignarContrato" ActiveViewIndex="-1" runat="server">
                                                            <asp:View ID="ViewAsignarContrato" runat="server">
                                                                    <table width="90%" align="center">
                                                                        <tr>
                                                                            <td class="auto-style1">
                                                                                <asp:Label ID="lblNombreUsu0" runat="server" Text="Asignar Contrato"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="btnAsignarContrato" OnClientClick="AsignarContratista(); return false;" runat="server" ImageUrl="~/Image/btn/info.jpg"  ToolTip="Asignar Contrato" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                    </td>
                                                </tr>
                                                <tr class="rowB">
                                                    <td>
                                                        <asp:Label runat="server" ID="lblNombreUsu" Text="Nombre del Usuario "></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblRegUsu" Text="Regional del Usuario "></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td class="Cell">
                                                        <asp:TextBox ID="txtNombreUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                                    </td>
                                                    <td class="Cell">
                                                        <asp:HiddenField ID="hfRegional" runat="server" />
                                                        <asp:TextBox ID="txtRegionalUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                                        <asp:DropDownList ID="ddlRegionalUsuarioContrato" runat="server" Visible="false"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlRegionalUsuarioContratoSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblfechaRegSist" Text="Fecha de Registro al Sistema "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblIdContrato" Text="Id Contrato/convenio "></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtFechaRegistro" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell">
                                        <asp:TextBox ID="txtIdContrato" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblNumeroContrato" Text="N&uacute;mero de Contrato/convenio"></asp:Label>
                                    </td>
                                    <td>
                                        Número de Contrato/convenio Migrado</td>
                                    <td>
                                        <asp:Label runat="server" ID="Label5" Text="Contrato Asociado"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td>
                                        <asp:TextBox runat="server" ID="txtNumeroContrato" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtNumContMigrado" Enabled="False"></asp:TextBox> &nbsp;</td>
                                    <td>
                                        <asp:HiddenField ID="hfFechaInicioEjecConvMarco" runat="server" />
                                        <asp:HiddenField ID="hfFechaFinalTerminContConvMarco" runat="server" />
                                        <asp:CheckBox Enabled="False" ID="chkConvenioMarco" runat="server" AutoPostBack="true"
                                            OnCheckedChanged="chkContratoAsociadoCheckedChanged" />
                                        <asp:Label runat="server" ID="lblConvenioMarco" Text="Convenio Marco"></asp:Label>
                                    </td>
                                </tr>                                
                                <tr class="rowA">
                                    <td colspan="2">
                                      &nbsp;</td> 
                                    <td>
                                        <asp:HiddenField ID="hfFechaInicioEjecContConvAd" runat="server" />
                                        <asp:HiddenField ID="hfFechaFinalTerminContConvAd" runat="server" />
                                        <asp:CheckBox Enabled="False" ID="chkContratoConvenioAd" runat="server" AutoPostBack="true"
                                            OnCheckedChanged="chkContratoAsociadoCheckedChanged" />
                                        <asp:Label runat="server" ID="lblConvAdh" Text="Contrato/Convenio Adhesión"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td rowspan="2" style="padding: 0px" colspan="2">
                                        <div id="PnValorConvenio" runat="server" style="visibility: collapse">
                                            <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbNumConvenioContrato" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td class="Cell">
                                                        <asp:HiddenField ID="hfNumConvenioContratoAsociado" runat="server" />
                                                        <asp:HiddenField ID="hfIdConvenioContratoAsociado" runat="server" />
                                                        <asp:TextBox ID="txtNumConvenioContrato" runat="server" Enabled="false" Width="80%"
                                                            ViewStateMode="Enabled" OnTextChanged="txtNumConvenioContratoTextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <asp:ImageButton ID="imgNumConvenioContrato" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                            Enabled="false" OnClientClick="GetNumConvenioContrato(); return false;" Style="cursor: hand"
                                                            ToolTip="Buscar" Visible="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td style="padding: 0px">
                                        <asp:Label runat="server" ID="lblCatContr" Text="Categoría de Contrato/convenio "></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" style="padding-bottom: 4px">
                                        <asp:DropDownList ID="ddlCategoriaContrato" runat="server" OnSelectedIndexChanged="ddlCategoriaContratoSelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblTipoContrato" Text="Tipo de Contrato/convenio "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblModalidadAcadem" Text="Modalidad Académica"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <asp:DropDownList ID="ddlTipoContratoConvenio" runat="server" Enabled="false" OnSelectedIndexChanged="ddlTipoContratoConvenioSelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Cell">
                                        <asp:DropDownList ID="ddlModalidadAcademica" runat="server" Enabled="false" OnSelectedIndexChanged="ddlModalidadAcademicaSelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label ID="lbNombreProfesion" runat="server" Text="Nombre de la Profesión"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblModSelecc" Text="Modalidad de Selección "></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <asp:DropDownList ID="ddlNombreProfesion" runat="server" Enabled="false" OnSelectedIndexChanged="ddlNombreProfesionSeleccionSelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Cell">
                                        <asp:DropDownList ID="ddlModalidadSeleccion" runat="server" Enabled="false" OnSelectedIndexChanged="ddlModalidadSeleccionSelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td rowspan="6" colspan="2">
                                        <div id="PnNumeroProceso" runat="server" style="visibility: collapse">
                                            <table style="width:100%">
                                                <tr class="rowB">
                                                    <td>
                                                        <asp:Label runat="server" ID="lblNumproc" Text="Número de Proceso"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td class="Cell">
                                                        <asp:HiddenField ID="hfNumeroProceso" runat="server" />
                                                        <asp:HiddenField ID="hfIdNumeroProceso" runat="server" />
                                                        <asp:TextBox ID="txtNumeroProceso" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                                            OnTextChanged="txtNumeroProcesoTextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <asp:ImageButton ID="imgNumeroProceso" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                            OnClientClick="GetNumeroProceso(); return false;" Style="cursor: hand" ToolTip="Buscar"
                                                            Visible="False" />
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                        <table style="width:100%">                                            
                                        <tr class="rowB">
                                            <td>
                                                Codigo SECOP&nbsp; / <asp:HyperLink ID="hlkVinculoSECOP" runat="server" Target="_blank" Text="Url SECOP" />
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                        <asp:ImageButton ID="imgNumeroProceso0" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                            OnClientClick="GetCodigosSECOP(); return false;" Style="cursor: hand" ToolTip="Buscar"
                                                            />
                                                    </td>
                                        </tr>
                                        </table>

                                       <table style="width:100%">                                            
                                        <tr class="rowB">
                                            <td>
                                                Requiere Garantia</td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                <asp:RadioButtonList runat="server" ID="rblRequiereGarantia" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList> 
                                            &nbsp;</td>
                                        </tr>
                                        </table>

                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblFechAdjProc" Text="Fecha Adjudicación del proceso" ></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <uc1:fecha ID="caFechaAdjudicaProceso" runat="server" Enabled="false" Requerid="false"  />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">                                                                                           
                                                 &nbsp;</td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">              
                                        
                                    </td>
                                </tr>  
                                <tr class="rowB">
                                    <td class="Cell">
                                                     Aportes en Especie ICBF                                       
                                                 &nbsp;</td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">                                        
                                        <asp:RadioButtonList runat="server" ID="rblAportesEspecie" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
                                        &nbsp;</td>
                                </tr>                                
                                <tr class="rowB">
                                    <td colspan="2">
                                        <div id="PnRequiereActaInicio" runat="server" style="visibility: collapse">
                                            <table width="100%">
                                                <tr class="rowB">
                                                    <td colspan="2" class="auto-style2">
                                                        <asp:Label runat="server" ID="lblReqActaInicio" Text="Requiere Acta de Inicio "></asp:Label>
                                                        </td>
                                                    <td colspan="2" class="auto-style2">
                                                        <asp:Label runat="server" ID="lblFeActaInicio" Text="Fecha Firma Acta de Inicio" Visible="false"></asp:Label>  
                                                        <asp:RequiredFieldValidator runat="server" ID="rfvActualizarFechaActa" ControlToValidate="txtFechaActa"
                                                              SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="ActualizarFechaInicio"
                                                              ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>                                                      
                                                        </td>
                                                    <td class="auto-style2">
                                                        </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td>
                                                        <asp:CheckBox ID="chkSiReqActa" runat="server" onclick="juegoReqActa(this)" />
                                                        Si
                                                    </td>
                                                    <td colspan="2">
                                                       
                                                           <asp:TextBox runat="server" ID="txtFechaActa" visible ="false" Enabled="true" AutoPostBack="True"></asp:TextBox>
                                                                <asp:Image ID="ImageFechaActa" runat="server" Visible="false" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                                                                 <Ajax:CalendarExtender ID="CalendarExtenderFechaActa" runat="server"  Format="dd/MM/yyyy" 
                                                                        PopupButtonID="ImageFechaActa" TargetControlID="txtFechaActa"></Ajax:CalendarExtender>
                                                                 <Ajax:MaskedEditExtender ID="meetxtFechaActa" runat="server" CultureAMPMPlaceholder="AM;PM"
                                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaActa">
                                                                 </Ajax:MaskedEditExtender>
                                                        &nbsp;</td>                                                   
                                                    <td colspan="2">
                                                       <asp:ImageButton ID="btnActualizar" runat="server" Visible="false"  ValidationGroup="ActualizarFechaInicio" ImageUrl="~/Image/btn/save.gif" ToolTip="Aprobar" OnClick="btnActualizar_Click" />                                                         
                                                           &nbsp;</td>                                                   
                                                </tr>
                                                <tr class="rowA">
                                                    <td colspan="5">
                                                        <asp:CheckBox ID="chkNoReqActa" runat="server" onclick="juegoReqActa(this)" />
                                                        No
                                                    </td>
                                                    
                                                    
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="PnManejaAportes" runat="server" style="visibility: collapse">
                                            <table width="100%">
                                                <tr class="rowB">
                                                    <td>
                                                        <asp:Label runat="server" ID="lblManejaAportes" Text="Maneja Cofinanciaci&oacute;n "></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td>
                                                        <asp:CheckBox ID="chkSiManejaAportes" runat="server" onclick="juegoManejaAportes(this)" />
                                                        <asp:Label runat="server" ID="lblManAportesSI" Text="Si"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td>
                                                        <asp:CheckBox ID="chkNoManejaAportes" runat="server" onclick="juegoManejaAportes(this)" />
                                                        <asp:Label runat="server" ID="lblManAportesNo" Text="No"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td rowspan="2" style="padding: 0px" colspan="2">
                                        <div id="PnManejaRecursos" runat="server" style="visibility: collapse">
                                            <table width="100%">
                                                <tr class="rowB">
                                                    <td>
                                                        <asp:Label runat="server" ID="lblManejaRecursos" Text="Maneja Recursos ICBF"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td>
                                                        <asp:CheckBox ID="chkSiManejaRecursos" runat="server" onclick="juegoManejaRecursos(this)" />
                                                        <asp:Label runat="server" ID="lblSiManejaRecursos" Text="Si"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td>
                                                        <asp:CheckBox ID="chkNoManejaRecursos" runat="server" onclick="juegoManejaRecursos(this)" />
                                                        <asp:Label runat="server" ID="lblNoManejaRecursos" Text="No"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td style="padding: 0px">
                                        <asp:Label runat="server" ID="lblRegContrata" Text="Régimen de Contratación"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" style="padding-bottom: 15px">
                                        <asp:DropDownList ID="ddlRegimenContratacion" runat="server" OnSelectedIndexChanged="ddlRegimenContratacionSelectedIndexChanged"
                                            AutoPostBack="true" Enabled="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblNombreSol" Text="Nombre del Solicitante"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblRegionalContrato" Text="Regional del Contrato/convenio"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <asp:HiddenField ID="hfNombreSolicitante" runat="server" />
                                        <asp:TextBox ID="txtNombreSolicitante" runat="server" Enabled="false" Width="80%"
                                            ViewStateMode="Enabled" OnTextChanged="txtNombreSolicitanteTextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgNombreSolicitante" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            OnClientClick="GetNombreSolicitante(); return false;" Style="cursor: hand" ToolTip="Buscar"
                                            Enabled="false" Visible="False" />
                                    </td>
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfRegionalContConv" runat="server" />
                                        <asp:HiddenField ID="hfCodRegionalContConv" runat="server" />
                                        <asp:TextBox ID="txtRegionalContratoConvenio" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblDepSol" Text="Dependencia Solicitante"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblCargSol" Text="Cargo del Solicitante"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <asp:HiddenField ID="hfDependenciaSolicitante" runat="server" />
                                        <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"
                                            ViewStateMode="Enabled"></asp:TextBox>
                                    </td>
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfCargoSolicitante" runat="server" />
                                        <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblNombrordGas" Text="Nombre del Ordenador del Gasto "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblTipIdOrdGas" Text="Tipo Identificación Ordenador del Gasto"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <asp:HiddenField ID="hfNombreOrdenadorGasto" runat="server" />
                                        <asp:TextBox ID="txtNombreOrdenadorGasto" runat="server" Enabled="false" Width="80%"
                                            ViewStateMode="Enabled" OnTextChanged="txtNombreOrdenadorGastoTextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgNombreOrdenadorGasto" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            OnClientClick="GetNombreOrdenadorGasto(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Enabled="false" Visible="False" />
                                    </td>
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfTipoIdentOrdenadorGasto" runat="server" />
                                        <asp:TextBox ID="txtTipoIdentOrdenadorGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblnumIdordGas" Text="Número Identificación Ordenador del Gasto"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblCargOrdGas" Text="Cargo Ordenador del Gasto"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <asp:HiddenField ID="hfNumeroIdentOrdenadoGasto" runat="server" />
                                        <asp:TextBox ID="txtNumeroIdentOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                    </td>
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfCargoOrdenadoGasto" runat="server" />
                                        <asp:TextBox ID="txtCargoOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApVigenciaValorContrato" runat="server" visible="True">
                        <div id="ContentApVigenciaValorContrato">
                            <table width="90%" align="center">
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblobjContr" Text="Objeto del Contrato/convenio "></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblobj" Text="Alcance del  Objeto del contrato/Convenio "></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfObjeto" runat="server" />
                                        <asp:TextBox ID="txtObjeto" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                            AutoPostBack="true" OnTextChanged="txtObjetoTextChanged" MaxLength="500" onKeyDown="limitText(this,500);"
                                            onKeyUp="limitText(this,500);"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:HiddenField ID="hfAlcance" runat="server" />
                                        <asp:TextBox ID="txtAlcance" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                            AutoPostBack="true" OnTextChanged="txtAlcanceTextChanged" MaxLength="4000" onKeyDown="limitText(this,4000);"
                                            onKeyUp="limitText(this,4000);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValIniCon" Text="Valor Total Inicial del contrato/Convenio "></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblFechIniCont" Text="Fecha de Inicio de Ejecución del contrato/convenio "></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValorInicialContConv" runat="server" Enabled="false" MaxLength="18"
                                            AutoPostBack="true" OnTextChanged="txtValorInicialContConvTextChanged"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <uc1:fecha ID="caFechaInicioEjecucion" runat="server" Enabled="false" Requerid="false"
                                            Visible="True" />
                                        <asp:CheckBox runat="server" AutoPostBack="true" ID="chkCalculaFecha" Text="Calcula la Fecha de Terminación"   Enabled="false" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblFFininiContr" Text="Fecha de Finalización Inicial del Contrato/Convenio "></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblPlazoIniEjec" Text="Plazo Inicial de Ejecución"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" rowspan="4">
                                        <uc1:fecha ID="caFechaFinalizacionInicial" runat="server" Enabled="false" Requerid="false"
                                            Visible="True" />
                                        
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblPlazoDiasEjec" Text="Días"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDiasPiEj" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblPlazoMesesEjec" Text="Meses"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMesesPiEj" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblPlazoAnnosEjec" Text="Años"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAcnosPiEj" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="Label4" Text="Valor Total CDP"></asp:Label>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:Label runat="server" ID="lblConsPlanCom" Text="Consecutivo Plan de Compras Contrato"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfCDP" runat="server" />
                                        <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" OnTextChanged="txtCDPTextChanged"
                                            AutoPostBack="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoCDP(); return false;" Style="cursor: hand"
                                            ToolTip="Ver CDP" />
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:HiddenField ID="hfConsecutivosPlanCompras" runat="server" />
                                        <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="false" Width="30%" ViewStateMode="Enabled"
                                            OnTextChanged="txtPlanComprasTextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblProductos" Text="Productos"></asp:Label>
                                        <asp:ImageButton ID="ibtnProductos" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoProductos(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Productos" />
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblPlanComAsoc" Text="Plan de Compras Asociado"></asp:Label>
                                        <asp:ImageButton ID="ibtnPlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoPlanCompras(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Plan de Compras" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValTotAdic" Text="Valor Total Adiciones"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblValTotReduc" Text="Valor Total Reducciones"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValTotAdiciones" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnAdiciones" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetAdiciones(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Adiciones" />
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtValTotReducciones" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnReducciones" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetReducciones(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Reducciones" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValContAdh" Text="Valor Contrato Adheridos"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblValAportCont" Text="Valor Aportes Contratista"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValContAdheridos" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtValorAportesContratista" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnAportesContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoAportesContratista(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Aportes Contratista" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValFinCont" Text="Valor Total Final del Contrato/Convenio"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblValorAporICBF" Text="Valor Total vigencias futuras"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValorFinalContrato" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtValorVigenciasFuturas" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnAportes" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoAportesICBF(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Aportes ICBF" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValorInicialRecursosICBF" Text="Valor Inicial Recursos Dinero ICBF"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblFechaSus" Text="Fecha de Suscripción"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValorInicialRecursosICBF" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtFechaSuscripcionContr" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblPlazoTotEjec" Text="Plazo Total de Ejecuci&oacute;n"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblProrr" Text="Prorrogas"></asp:Label>
                                        <asp:ImageButton ID="ibtnProrrogas" runat="server" CssClass="bN" Enabled="true"
                                            ImageUrl="~/Image/btn/icoPagBuscar.gif" OnClientClick="GetProrrogas(); return false;"
                                            Style="cursor: hand" ToolTip="Ver Prorrogas" />
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" rowspan="4">
                                        <table width="100%">
                                            <tr class="rowB">
                                                <td>
                                                    <asp:Label runat="server" ID="lblDiasPlazo" Text="Días"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPlazoTotEjecDias" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>
                                                    <asp:Label runat="server" ID="lblMesesPlazo" Text="Meses"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPlazoTotEjecMeses" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>
                                                    <asp:Label runat="server" ID="lblAnnosPlazo" Text="A&ntilde;os"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPlazoTotEjecAnios" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblDiasProrr" Text="Dias"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProrrogaDias" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblMesesProrr" Text="Meses"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProrrogaMeses" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblAnnosProrr" Text="Años"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtProrrogaAnios" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblFechProyLiquid" Text="Fecha Proyectada de Liquidación"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblFecLiquid" Text="Fecha de Liquidaci&oacute;n"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtFechaProyLiq" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtFechaLiquidacion" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table width="90%" align="center" id="tblFechaLiq" runat="server">
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblFechFinTer" Text="Fecha Final de Terminación contrato/convenio "></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblmanejaVigenciasFuturas" Text="Maneja Vigencias Futuras "></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" rowspan="3">
                                        <asp:TextBox ID="txtFechaFinalTerminacion" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkSiManejaVigFuturas" runat="server" Enabled="false" onclick="juegoManejaVigFuturas(this)" />
                                        <asp:Label runat="server" ID="lblSiManVigFut" Text="Si"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkNoManejaVigFuturas" runat="server" Enabled="false" onclick="juegoManejaVigFuturas(this)" />
                                        <asp:Label runat="server" ID="lblNoManVigFut" Text="No"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblVigenciaFiscalInicial" Visible="True" Text="Vigencia Fiscal Inicial del Contrato/Convenio"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="TextBox1" Visible="True" Text="Vigencia Fiscal Final del Contrato/Convenio"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:DropDownList ID="ddlVigenciaFiscalIni" runat="server" Enabled="false" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVigenciaFiscalIniSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:DropDownList ID="ddlVigenciaFiscalFin" runat="server" Enabled="false" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVigenciaFiscalFinSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <table width="90%" align="center">
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblFormaPago" Visible="True" Text="Anticipos  y/o Pago Anticipado"></asp:Label>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:Label runat="server" ID="lblTipoFormaPago" Visible="True" Text="Tipos de Pago"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:DropDownList ID="ddlFormaPago" runat="server" Enabled="false" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlFormaPagoSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:DropDownList ID="ddlTipoFormaPago" runat="server" Enabled="false" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlTipoFormaPagoSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblPorcentaje" Visible="True" Text="% Anticipo/Anticipado"></asp:Label>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:Label runat="server" ID="lblValorAnticipo" Visible="True" Text="Valor del Anticipo/Anticipado"></asp:Label>                                        
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtPorcentaje" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtValorAnticipo" runat="server" Enabled="false"></asp:TextBox>                                        
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblFechAnulac" Text="Fecha de Anulaci&oacute;n"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblEstadoContrato" Visible="True" Text="Estado del Contrato/Convenio"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td>
                                        <asp:TextBox ID="txtFechaAnulacion" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:TextBox ID="txtEstadoContrato" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblCont" Text="Contratista"></asp:Label>
                                        <asp:ImageButton ID="ibtnContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoContratista(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Contratistas" />
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblSupInter" Text="Supervisor y/o Interventor"></asp:Label><asp:ImageButton
                                            ID="ibtnSupervisorInterventor" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoSupervisorInterventor(); return false;"
                                            Style="cursor: hand" ToolTip="Ver Supervisor y/o Interventor" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblVigFut" Text="Vigencias Futuras"></asp:Label>
                                        <asp:ImageButton ID="ibtnVigenciasFuturas" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoVigenciasFuturas(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Vigencias Futuras" />
                                    </td>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblGarant" Text="Garantias"></asp:Label>
                                        <asp:ImageButton ID="ibtnGarantias" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoGarantias(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Garantías" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblRP" Text="RP"></asp:Label>
                                        <asp:ImageButton ID="ibtnRP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoRP(); return false;" Style="cursor: hand"
                                            ToolTip="Ver RP" />
                                    </td>
                                    <td colspan="2">
                                         <asp:Label runat="server" ID="lblConsolidado" Text="Consolidado Modificaciones"></asp:Label>
                                        <asp:ImageButton ID="ImageButton1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoConsolidado(); return false;" Style="cursor: hand"
                                            ToolTip="Ver RP" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblLugEjec" Text="Lugar de Ejecuci&oacute;n"></asp:Label>
                                    </td>
                                    <td>
                                        Historico Modificaciones
                                        <asp:ImageButton runat="server" ID="ImgHisModificaciones" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoHistoricoModificaciones(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Historico de las Modificaciones" />
                                        &nbsp;</td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:CheckBox ID="chkNivelNacional" runat="server" Enabled="false" AutoPostBack="true"
                                            OnCheckedChanged="chkNivelNacionalCheckedChanged" />
                                        <asp:Label runat="server" ID="lblNivNac" Text="Nivel Nacional"></asp:Label>
                                    </td>
                                    <td class="Cell" colspan="2">
                                        <asp:Label runat="server" ID="Label6" Text="Datos adicionales"></asp:Label>
                                        <asp:ImageButton runat="server" ID="ibtnLugarEjec" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="True" OnClientClick="GetInfoLugarEjecucion(); return false;" Style="cursor: hand"
                                            ToolTip="Ver Lugares de Ejecución" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValContAsocConMar" Text="Valor Total Contratos Asociados al Convenio Marco"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        </td>
                                    
                                </tr>
                                <tr class="rowA">
                                    <td>
                                        <asp:TextBox ID="txtVlaConAsocConvMac" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        Contratos Relacionados al Convenio Marco
                                    </td>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="3">
                                        <asp:GridView ID="gvContRelConMar" runat="server" AutoGenerateColumns="false" GridLines="None"
                                            Width="100%" CellPadding="8" Height="16px" AllowPaging="true" Visible="true"
                                            DataKeyNames="valorfinalcontrato" OnPageIndexChanging="gvContRelConMar_PageIndexChanging"
                                            OnSelectedIndexChanged="gvContRelConMar_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Id Contrato/Convenio" DataField="idContrato" />
                                                <asp:BoundField HeaderText="Número de Contrato/Convenio" DataField="NumeroContrato" />
                                                <asp:BoundField HeaderText="Valor Final Contrato/Convenio" DataField="valorfinalcontrato"
                                                    DataFormatString="{0:c}" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        Contratos Adheridos al Contrato/Convenio
                                    </td>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="3">
                                        <asp:GridView ID="gvContAdhContConv" runat="server" AutoGenerateColumns="false" GridLines="None"
                                            Width="100%" CellPadding="8" Height="16px" AllowPaging="true" Visible="true"
                                            OnPageIndexChanging="gvContAdhContConv_PageIndexChanging" OnSelectedIndexChanged="gvContAdhContConv_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Id Contrato/Convenio" DataField="idContrato" />
                                                <asp:BoundField HeaderText="Número de Contrato/Convenio" DataField="NumeroContrato" />
                                                <asp:BoundField HeaderText="Valor Final Contrato/Convenio" DataField="valorfinalcontrato"
                                                    DataFormatString="{0:c}" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        Archivos Adjuntos al Contrato/Convenio
                                    </td>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="3">
                                        <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="false"
                                            GridLines="None" Width="100%" DataKeyNames="IdArchivoContrato, NombreArchivo" CellPadding="0"
                                            Height="16px" OnSelectedIndexChanged="gvDocumentos_SelectedIndexChanged">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Nombre Archivo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LbArchivo" runat="server" Text='<%#Eval("NombreArchivoOri") %>'></asp:Label>
                                                        <asp:Label ID="LbArchivoFTP" runat="server" Visible="false" Text='<%#Eval("NombreArchivo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" ImageUrl="~/Image/btn/list.png"
                                                            Height="16px" Width="16px" ToolTip="Ver Documento" Enabled="True" OnClick="btnMostrar_Click"
                                                            Visible='<%# HttpUtility.HtmlDecode((string)Eval("NombreArchivo")) != "" %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="LightBlue" />
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApPlanComprasProductos" runat="server" visible="False">
                        <div id="ContentApPlanComprasProductos">
                            <table id="tb001" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td>
                                        <asp:Label ID="lblEPlanCompras" runat="server" Text="Plan de compras *" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvConsecutivos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                            Width="100%" DataKeyNames="IDPlanDeComprasContratos" CellPadding="8" Height="16px"
                                            Visible="false">
                                            <Columns>
                                                <asp:BoundField HeaderText="Número consecutivo plan de compras" DataField="IDPlanDeCompras" />
                                                <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblEProductos" Text="Productos *" Visible="false"></asp:Label>
                                        <asp:HiddenField ID="hfProductos" runat="server" />
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                            Width="100%" CellPadding="8" Height="16px" Visible="false">
                                            <Columns>
                                                <asp:BoundField HeaderText="Consecutivo plan de compras" DataField="consecutivo" />
                                                <asp:BoundField HeaderText="Código del producto" DataField="codigo_producto" />
                                                <asp:BoundField HeaderText="Nombre del producto" DataField="nombre_producto" />
                                                <asp:BoundField HeaderText="Tipo de producto" DataField="tipoProducto" />
                                                <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                <asp:BoundField HeaderText="Valor unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                <asp:BoundField HeaderText="Unidad de tiempo" DataField="tipotiempo" />
                                                <asp:BoundField HeaderText="Unidad de medida" DataField="unidad_medida" />
                                                <asp:BoundField HeaderText="Valor total" DataField="valor_total" DataFormatString="{0:c}" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblRubPlanCompras" Text="Rubros plan de compras" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="false"
                                            GridLines="None" Width="100%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Número consecutivo plan de compras" DataField="consecutivo" />
                                                <asp:BoundField HeaderText="Código rubro" DataField="codigo_rubro" />
                                                <asp:BoundField HeaderText="Descripción del rubro" DataField="NombreRubro" />
                                                <asp:BoundField HeaderText="Valor del rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label ID="lblEConsPlanCompras" runat="server" Visible="False" Text="Consecutivo plan de compras"> </asp:Label>
                                        Consecutivo plan de compras
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:DropDownList ID="DdlConsecutivoPlanCompras" runat="server" Width="30%" Enabled="false"
                                            AutoPostBack="true" OnSelectedIndexChanged="DdlConsecutivoPlanComprasSelectedIndexChanged"
                                            Visible="False">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApAportes" runat="server">
                        <div id="ContentApAportes">
                            <table id="tb002" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblValoraportesICBF" Visible="False" Text="Valor aportes ICBF"></asp:Label>
                                        <asp:HiddenField ID="hfAportes" runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblAportesContratista" Visible="False" Text="Valor aportes contratista"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValorApoICBF" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                            OnTextChanged="txtValorApoICBFTextChanged" AutoPostBack="true" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="imgValorApoICBF" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="false" OnClientClick="GetValorApoICBF(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Visible="False" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="Cell">
                                        <asp:TextBox ID="txtValorApoContrat" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                            OnTextChanged="txtValorApoContratTextChanged" AutoPostBack="true" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="imgValorApoContrat" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="false" OnClientClick="GetValorApoContrat(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Visible="False" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="3">
                                        <asp:Label runat="server" ID="lblEAportesICBF" Text="Aportes ICBF" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td colspan="3">
                                        <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false" DataKeyNames="IdAporteContrato"
                                            AllowSorting="true" OnSorting="gvAportesICBF_Sorting" GridLines="None" Width="100%"
                                            CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Tipo de aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                <asp:BoundField HeaderText="Valor aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                    DataFormatString="{0:c}" />
                                                <asp:BoundField HeaderText="Descripción aporte especie" DataField="DescripcionAporte"
                                                    SortExpression="DescripcionAporte" />
                                                <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" runat="server" Enabled="False" OnClick="btnEliminarAporteICBFClick"
                                                            OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="3">
                                        <asp:Label runat="server" ID="lblEAportesContratista" Text="Aportes contratista"
                                            Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td colspan="3">
                                        <asp:GridView ID="gvAportesContratista" runat="server" AutoGenerateColumns="false"
                                            DataKeyNames="IdAporteContrato" AllowSorting="true" OnSorting="gvAportesContratista_Sorting"
                                            GridLines="None" Width="100%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Identificación contratista" DataField="NumeroIdentificacionContratista"
                                                    SortExpression="NumeroIdentificacionContratista" />
                                                <asp:BoundField HeaderText="Información contratista" DataField="InformacionAportante"
                                                    SortExpression="InformacionAportante" />
                                                <asp:BoundField HeaderText="Tipo aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                <asp:BoundField HeaderText="Valor aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                    DataFormatString="{0:c}" />
                                                <asp:BoundField HeaderText="Descripción aporte especie" DataField="DescripcionAporte"
                                                    SortExpression="DescripcionAporte" />
                                                <asp:BoundField HeaderText="Fecha RP" DataField="FechaRP" SortExpression="FechaRP"
                                                    DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField HeaderText="Número RP" DataField="NumeroRP" SortExpression="NumeroRP" />
                                                <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarAporteContratistaClick"
                                                            OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApLugarEjecucion" runat="server">
                        <div id="ContentApLugarEjecucion">
                            <table id="tb003" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblLugarEjec" Text="Lugar de ejecución *" Visible="False"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblDatoadicLugarEjec" Text="Datos adicionales lugar de ejecución *"
                                            Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfLugarEjecucion" runat="server" />
                                        <asp:TextBox ID="txtLugarEjecucion" runat="server" Enabled="false" ViewStateMode="Enabled"
                                            OnTextChanged="txtLugarEjecucionTextChanged" AutoPostBack="true" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="imgLugarEjecucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="false" OnClientClick="GetLugarEjecucion(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Visible="False" />
                                    </td>
                                    <td class="Cell">
                                        <asp:TextBox ID="txtDatosAdicionalesLugarEjecucion" runat="server" Width="80%" TextMode="MultiLine"
                                            Enabled="false" AutoPostBack="true" OnTextChanged="txtDatosAdicionalesLugarEjecucionTextChanged"
                                            Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell" colspan="2">
                                        <div id="dvLugaresEjecucion" runat="server">
                                            <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos"
                                                AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px"
                                                Visible="False">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                    <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarLugEjecucionClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApContratista" runat="server">
                        <div id="ContentApContratista">
                            <table id="tb004" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="lblEContratista" Visible="False" Text="Contratista *"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfContratistas" runat="server" />
                                        <asp:TextBox ID="txtContratista" runat="server" Enabled="false" ViewStateMode="Enabled"
                                            OnTextChanged="txtContratistaTextChanged" AutoPostBack="true" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="imgContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="false" OnClientClick="GetInfoContratista(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Visible="False" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td>
                                        <asp:Label runat="server" ID="lblContratistasRelacionados" Visible="False" Text="Contratistas relacionados"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                            GridLines="None" Width="100%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Tipo persona" DataField="TipoPersonaNombre" />
                                                <asp:BoundField HeaderText="Tipo identificación" DataField="TipoIdentificacion" />
                                                <asp:BoundField HeaderText="Número identificación" DataField="NumeroIdentificacion" />
                                                <asp:BoundField HeaderText="Información contratista" DataField="Proveedor" />
                                                <asp:BoundField HeaderText="Identificación representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                            OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                        </asp:LinkButton>
                                                        <asp:LinkButton Enabled="False" ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                            CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Eliminar" />
                                                        </asp:LinkButton>
                                                             <asp:LinkButton Enabled="False" ID="LinkButton1" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                            CommandName="VerSupervisor" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/list.png" title="Historíco Representantes" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="lblIntegRelConsor" Visible="False" Text="Integrantes relacionados al consorcio o unión temporal"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvIntegrantesConsorcio_UnionTemp" runat="server" AutoGenerateColumns="false"
                                            GridLines="None" Width="100%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Identificación contratista" DataField="NumeroIdentificacion" />
                                                <asp:BoundField HeaderText="Tipo persona" DataField="TipoPersonaNombre" />
                                                <asp:BoundField HeaderText="Tipo identificación" DataField="TipoIdentificacion" />
                                                <asp:BoundField HeaderText="Número identificación" DataField="NumeroIdentificacionIntegrante" />
                                                <asp:BoundField HeaderText="Información integrante" DataField="Proveedor" />
                                                <asp:BoundField HeaderText="Porcentaje participación" DataField="PorcentajeParticipacion" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApSupervisorInterventor" runat="server">
                        <div id="ContentApSupervisorInterventor">
                            <table id="tb005" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="lblSupervInrvent" Visible="False" Text="Supervisor y/o interventor relacionados *"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfSupervInterv" runat="server" />
                                        <asp:TextBox ID="txtSuperInterv" runat="server" Enabled="false" ViewStateMode="Enabled"
                                            OnTextChanged="txtSuperIntervTextChanged" AutoPostBack="true" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="imgSuperInterv" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="false" OnClientClick="GetSuperInterv(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Visible="False" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="Label1" Visible="False" Text="Supervisor y/o interventor relacionados"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <div id="dvSupervInterv" runat="server">
                                            <asp:GridView ID="gvSupervInterv" runat="server" AutoGenerateColumns="false" DataKeyNames="IDSupervisorIntervContrato"
                                                GridLines="None" Width="80%" CellPadding="8" Height="16px" Visible="False">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Supervisor y/o interventor" DataField="EtQSupervisorInterventor" />
                                                    <asp:BoundField HeaderText="Tipo Supervisor y/o interventor" DataField="EtQInternoExterno" />
                                                    <asp:BoundField HeaderText="Fecha de inicio" DataField="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Tipo identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número identificación" DataField="Identificacion" />
                                                    <asp:BoundField HeaderText="Tipo vinculación contractual" DataField="SupervisorInterventor.TipoVinculacionContractual" />
                                                    <asp:BoundField HeaderText="Información supervisor y/o interventor" DataField="SupervisorInterventor.NombreCompleto" />
                                                    <asp:BoundField HeaderText="Regional" DataField="SupervisorInterventor.Regional" />
                                                    <asp:BoundField HeaderText="Dependencia" DataField="SupervisorInterventor.Dependencia" />
                                                    <asp:BoundField HeaderText="Cargo" DataField="SupervisorInterventor.Cargo" />
                                                    <asp:BoundField HeaderText="Dirección" DataField="SupervisorInterventor.Direccion" />
                                                    <asp:BoundField HeaderText="Teléfono" DataField="SupervisorInterventor.Telefono" />
                                                    <asp:BoundField HeaderText="Número contrato interventoria" DataField="IdNumeroContratoInterventoria" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarGvSupervIntervClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="Label2" Visible="False" Text="Director interventoria"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvDirectoresInterv" runat="server" AutoGenerateColumns="false"
                                            GridLines="None" Width="80%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Tipo identificación" DataField="TipoIdentificacion" />
                                                <asp:BoundField HeaderText="Número identificación" DataField="Identificacion" />
                                                <asp:BoundField HeaderText="Información director" DataField="SupervisorInterventor.NombreCompleto" />
                                                <asp:BoundField HeaderText="Teléfono" DataField="SupervisorInterventor.Telefono" />
                                                <asp:BoundField HeaderText="Celular" DataField="SupervisorInterventor.Celular" />
                                                <asp:BoundField HeaderText="Correo electrónico" DataField="SupervisorInterventor.CorreoElectronico" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApCDP" runat="server">
                        <div id="ContentApCDP">
                            <table id="tb006" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="lblEtCDP" Text=" CDP " Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdContratosCDP"
                                            GridLines="None" Width="100%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarCDPClick"
                                                            OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Regional" DataField="Regional" />
                                                <asp:BoundField HeaderText="Área" DataField="Area" />
                                                <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP" DataFormatString="{0:c}" />
                                                <asp:BoundField HeaderText="Rubro presupuestal" DataField="RubroPresupuestal" />
                                                <asp:BoundField HeaderText="Tipo fuente financiamiento" DataField="TipofuenteFinanciamiento" />
                                                <asp:BoundField HeaderText="Recurso presupuestal" DataField="RecursoPresupuestal" />
                                                <asp:BoundField HeaderText="Dependencia afectación gastos" DataField="DependenciaAfectacionGastos" />
                                                <asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumentoSoporte" />
                                                <asp:BoundField HeaderText="Tipo situación fondos" DataField="TipoSituacionFondos" />
                                                <asp:BoundField HeaderText="Consecutivo plan de compras" DataField="ConsecutivoPlanCompras" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="ApVigenciasFuturas" runat="server">
                        <div id="ContentApVigenciasFuturas">
                            <table id="tb007" runat="server" width="90%" align="center" visible="False">
                                <tr class="rowB">
                                    <td class="Cell">
                                        <asp:Label runat="server" ID="Label3" Text="Vigencias Futuras *" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:HiddenField ID="hfVigenciasFuturas" runat="server" />
                                        <asp:TextBox ID="txtVigFuturas" runat="server" Enabled="false" ViewStateMode="Enabled"
                                            OnTextChanged="txtVigFuturasTextChanged" AutoPostBack="true" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="imgVigFuturas" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                            Enabled="false" OnClientClick="GetVigFuturas(); return false;" Style="cursor: hand"
                                            ToolTip="Buscar" Visible="False" />
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                        <asp:GridView ID="gvVigFuturas" runat="server" AutoGenerateColumns="false" DataKeyNames="IDVigenciaFuturas"
                                            GridLines="None" Width="100%" CellPadding="8" Height="16px" Visible="False">
                                            <Columns>
                                                <asp:BoundField HeaderText="Número del radicado" DataField="NumeroRadicado" />
                                                <asp:BoundField HeaderText="Fecha de expedición vigencia futura" DataField="FechaExpedicion"
                                                    DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField HeaderText="Valor vigencia futura" DataField="ValorVigenciaFutura"
                                                    DataFormatString="{0:c}" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarGvVigFuturasClick"
                                                            OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hfIdEstadoContrato" runat="server" />
<asp:HiddenField ID="hfCodContratoAsociadoSel" runat="server" />
<asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
<asp:HiddenField ID="hfIdCategoriaConvenio" runat="server" />
<asp:HiddenField ID="hfIdTipoContConvPrestServApoyoGestion" runat="server" />
<asp:HiddenField ID="hfIdTipoContConvPrestServProfesionales" runat="server" />
<asp:HiddenField ID="hfIdTipoContAporte" runat="server" />
<asp:HiddenField ID="hfIdMarcoInteradministrativo" runat="server" />
<asp:HiddenField ID="hfIdContratacionDirecta" runat="server" />
<asp:HiddenField ID="hfTotalProductos" runat="server" />
<asp:HiddenField ID="hfAcordeonActivo" runat="server" />
<asp:HiddenField ID="hfIdContratoS" runat="server" />
<script type="text/javascript" language="javascript">
    function GetInfoGarantias() {
        var ContratoMigrado = document.getElementById('<%= hfContratoMigrado.ClientID %>');   
       
        if (ContratoMigrado.value == "" || ContratoMigrado.value == null)
        {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoGarantias.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        else
        {
            window_showModalDialog('../../../Page/Contratos/LupasMigracion/LupaInfoGarantiasMigracion.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        
    }

    function GetInfoRP() {
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoRP.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoCDP() {
        var idContrato = document.getElementById('<%= hfIdContratoS.ClientID %>');   
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoCDPLinea.aspx?IdContrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoAportesICBF() {
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoAportesICBF.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoAportesContratista() {
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoAportesContratista.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoContratista() {
        var ContratoMigrado = document.getElementById('<%= hfContratoMigrado.ClientID %>');   
       
        if (ContratoMigrado.value == "" || ContratoMigrado.value == null)
        {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoContratista.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        else
        {
            window_showModalDialog('../../../Page/Contratos/LupasMigracion/LupaInfoContratistaMigrados.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');          
        }
        
    }

    function GetInfoLugarEjecucion() {
                var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoLugarEjecucion.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoHistoricoModificaciones() {
                var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/LupasModificaciones/LupaHistoricoModificaciones.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetAdiciones() {
        //alert(idContrato);
        var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/LupasModificaciones/LupasAdiciones.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }


    function GetCodigosSECOP() {
        var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaCodigosSECOP.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetReducciones() {
        
        var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/LupasModificaciones/LupasReducciones.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetProrrogas() {
        var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/LupasModificaciones/LupasProrrogas.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoConsolidado() {
        var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/LupasModificaciones/LupasConsolidado.aspx?idcontrato=' + idContrato.value, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetHistoricoSupervisores(idSupervisor)
    {
        alert(idSupervisor);
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaRepresentanteLegal.aspx?idproveedor=' + idSupervisor, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoSupervisorInterventor() {
        var ContratoMigrado = document.getElementById('<%= hfContratoMigrado.ClientID %>');   
       
        if (ContratoMigrado.value == "" || ContratoMigrado.value == null)
        {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaSupervisorInterventor.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        else
        {
            window_showModalDialog('../../../Page/Contratos/LupasMigracion/LupaSupervisorInterventorMigracion.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        
    }

    function GetInfoVigenciasFuturas() {
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoVigenciasFuturas.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoPlanCompras() {
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoPlanCompras.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

    function GetInfoProductos() {
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaInfoProductos.aspx', '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }

</script>
