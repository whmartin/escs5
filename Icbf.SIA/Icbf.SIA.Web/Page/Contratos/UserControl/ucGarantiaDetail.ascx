﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucGarantiaDetail.ascx.cs" Inherits="Page_Contratos_UserControl_ucGarantiaDetail" %>

<%@ Register TagPrefix="uc2" TagName="IcbfDireccion" Src="~/General/General/Control/IcbfDireccion.ascx" %>
<%@ Register TagPrefix="uc1" TagName="fecha_1" Src="~/General/General/Control/fecha.ascx" %>

<link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
    <link type="text/css" href="../../../Styles/direccion.css" rel="Stylesheet" />

<asp:HiddenField ID="hfIDGarantia" runat="server" />
 <table width="90%" align="center">
     <td align="center" colspan="2">
                            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
                        </td>
      <tr class="rowB">
            <td colspan= "2">
               Número del Contrato/Convenio
            </td>
   </tr>
   <tr class="rowA">
            <td colspan = "2">
               <asp:TextBox runat="server" ID="txtNumeroContratoConvenio" MaxLength="80" Width="34%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContratoConvenio" runat="server" TargetControlID="txtNumeroContratoConvenio"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
   </tr>
   <tr class="rowB">
		<td>
		   Número Garant&iacute;a 
		</td>
		<td>
		   Tipo Garant&iacute;a 
		</td>
    </tr>
    <tr class="rowA">
		<td>
		    <asp:TextBox runat="server" ID="txtNumeroGarantia" MaxLength="50"  Width="80%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroGarantia" runat="server" TargetControlID="txtNumeroGarantia"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-" />
		</td>
		<td style="width: 50%">
			<asp:DropDownList runat="server" ID="ddlIDTipoGarantia" Enabled="False"></asp:DropDownList>
		</td>
    </tr>
    </table>
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
	<fieldset>
        <legend>Información Aseguradora</legend>
        <table width="90%" align="center">
			<tr class="rowB">
            <td>
                NIT de la Aseguradora 
            </td>
            <td>
               Nombre de la Aseguradora
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtNitAseguradora" MaxLength="80" Width="80%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNitAseguradora" runat="server" TargetControlID="txtNitAseguradora"
                    FilterType="Custom,Numbers,LowercaseLetters,UppercaseLetters" ValidChars="- "/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreAseguradora" MaxLength="80" Width="80%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteNombreSucursal" runat="server" TargetControlID="txtNombreAseguradora"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars="áéíóúÁÉÍÓÚñÑ "/>
                    <asp:HiddenField runat="server" ID="HdNombreAseguradora" ClientIDMode="Static" />
           </td>
        </tr>
		  <tr class="rowB">
            <td>
               Departamento Sucursal 
            </td>
            <td>
               Municipio Sucursal 
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 50%">
               <asp:DropDownList runat="server" ID="ddlDepartamentoSucursal" Enabled="False"></asp:DropDownList>
            </td>
            <td>
              <asp:DropDownList runat="server" ID="ddlMunicipioSucursal" Enabled="False"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
               Nombre de la Sucursal
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                 <asp:TextBox runat="server" ID="txtNombreSucursal" MaxLength="256" Width="40%" Height="80px" TextMode="MultiLine"  onKeyDown="limitText(this,256);" 
                onKeyUp="limitText(this,256);" CssClass="TextBoxGrande" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreAseguradora" runat="server" TargetControlID="txtNombreSucursal"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars=" "/>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
               Dirección Notificación
            </td>
         </tr>
        <tr class="rowA">
            <td colspan="2">
                <uc2:IcbfDireccion ID="txtDireccionNotificacion" runat="server" Requerido="False" Enabled="False"/>
            </td>
         </tr>
          <tr class="rowB">
            <td>
               Correo Electrónico 
            </td>
            <td>
               Indicativo 
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:TextBox runat="server" ID="txtCorreoElectronico" MaxLength="128" Width="80%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftecorreoelectronico" runat="server" TargetControlID="txtCorreoElectronico"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars="@.-_"/>
            </td>
            <td>
               <asp:TextBox runat="server" ID="txtIndicativo" MaxLength="3" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteIndicativo" runat="server" TargetControlID="txtIndicativo"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
        </tr>
          <tr class="rowB">
            <td>
               Teléfono 
            </td>
            <td>
               Extensión 
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:TextBox runat="server" ID="txtTelefono" MaxLength="7" Width="40%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteTelefono" runat="server" TargetControlID="txtTelefono"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtExtension" MaxLength="5" Enabled="False" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtExtension"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
        </tr>
         <tr class="rowB">
            <td>
               Celular
            </td>
            <td>
               Beneficio y/o Asegurado y/o Afianzado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCelular" MaxLength="10" Width="40%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteCelular" runat="server" TargetControlID="txtCelular"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
            <td>
              <asp:CheckBoxList runat="server" ID="chkBeneAsegAfianz" RepeatDirection="Horizontal" Enabled="False"></asp:CheckBoxList>
            </td>
        </tr>
		 </table>
         <table width="90%" align="center">
      <tr class="rowB">
            <td colspan= "2">
               Descripción de los Beneficiarios y/o Asegurados y/o Afianzados
            </td>
       </tr>
       <tr class="rowA">
                <td colspan = "2" style="text-align:center; vertical-align: middle;">
                   <asp:TextBox runat="server" ID="txtDescBenAsegAfianz" MaxLength="200" Width="90%" Height="80px" TextMode="MultiLine" 
                onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);" CssClass="TextBoxGrande" Enabled="False"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteDescBenAsegAfianz" runat="server" TargetControlID="txtDescBenAsegAfianz"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars=" -.,;:"/>
                </td>
       </tr>
   </table>
    </fieldset>
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
 <fieldset>
        <legend>Tomador Garant&iacute;a</legend>
        <table width="90%" align="center">
             <tr class="rowAG">
                <td>
                     <asp:GridView runat="server" ID="gvProveedores" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" DataKeyNames="IDContratista_Garantias" CellPadding="0" Height="16px"
                    EmptyDataText="No se encontraron datos, verifique por favor.">
                    <Columns>
                <%--<asp:TemplateField HeaderText="Seleccionar">
                    <ItemTemplate>
                       <asp:CheckBox runat="server" CommandName="Select" ToolTip="Seleccionar" ID="check_SeleccionarContratista"/>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" SortExpression="TipoPersonaNombre"  />
                <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                <asp:BoundField HeaderText="Contratista" DataField="Proveedor" SortExpression="Proveedor"  />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                </td>
            </tr>
         </table>
    </fieldset>
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
    
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
                Amparos 
            </td>
        </tr>
    </table>
   <asp:Panel runat="server" ID="pnlAmparos">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAmparosGarantias" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDAmparosGarantias" CellPadding="0" Height="16px"
                         EmptyDataText="No se encontraron datos, verifique por favor.">
                        <Columns>
                           <asp:BoundField HeaderText="Tipo Amparo" DataField="NombreTipoAmparo"  SortExpression="NombreTipoAmparo"/>
                            <%--<asp:BoundField HeaderText="Fecha Vigencia Desde" DataField="FechaVigenciaDesde"  SortExpression="FechaVigenciaDesde"/>--%>
                             <asp:TemplateField HeaderText="Fecha Vigencia Desde" ItemStyle-HorizontalAlign="Center" SortExpression="FechaVigenciaDesde">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 80px;">
                                            <%#Convert.ToDateTime(Eval("FechaVigenciaDesde")).ToString("dd/MM/yyyy")%>
                                     </div>
                                 </ItemTemplate>
                              </asp:TemplateField>

                            <%--<asp:BoundField HeaderText="Fecha Vigencia Hasta" DataField="FechaVigenciaHasta"  SortExpression="FechaVigenciaHasta"/>--%>
                           
                            <asp:TemplateField HeaderText="Fecha Vigencia hasta" ItemStyle-HorizontalAlign="Center" SortExpression="FechaVigenciaHasta">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 80px;">
                                            <%#Convert.ToDateTime(Eval("FechaVigenciaHasta")).ToString("dd/MM/yyyy")%>
                                     </div>
                                 </ItemTemplate>
                              </asp:TemplateField>

                            <asp:BoundField HeaderText="Valor para Cálculo Asegurado" DataField="ValorCalculoAsegurado"  SortExpression="ValorCalculoAsegurado"/>
                             <asp:BoundField HeaderText="Unidad de Cálculo Porcentual" DataField="NombreUnidadCalculo"  SortExpression="NombreUnidadCalculo"/>
                            <%--<asp:BoundField HeaderText="Valor Asegurado" DataField="ValorAsegurado"  SortExpression="ValorAsegurado"/>--%>
                            <asp:TemplateField HeaderText="Valor Asegurado" ItemStyle-HorizontalAlign="Center" SortExpression="ValorAsegurado">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 150px;">
                                         <%# Convert.ToDecimal(Eval("ValorAsegurado")).ToString("$ #,###0.00##;($ #,###0.00##)")%>
                                     </div>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField>
                                <ItemTemplate>
                                   <%-- <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />--%>
                                    <%--<asp:ImageButton ID="btneliminar" runat="server" CommandName="Delete" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Eliminar" />--%>
                                    <%--<asp:ImageButton ID="btneliminarAmparo" runat="server" CommandName="Borrar" ImageUrl="~/Image/btn/delete.gif" 
                                    Height="16px" Width="16px" ToolTip="Eliminar" 
                                    Enabled="False" 
                                    OnClientClick="javascript:if (!confirm('¿Est&#225; seguro que desea eliminar el documento?')) return false;" 
                                    onclick="btnEliminarAmparo_Click" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
      <table width="90%" align="center">
			<tr class="rowB">
            <td>
               Fecha de Inicio de Garant&iacute;a 
			     
            </td>
            <td>
               Fecha de Expedición de Garant&iacute;a 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicioGarantia" Enabled="False"></asp:TextBox>
                <%--<uc1:fecha_1 ID="" runat="server"  Enabled="False" Requerid="False" />--%>
            </td>
            <td>
                <%--<uc1:fecha ID="txtFechaExpedicionGarantia" runat="server"  Enabled="False" Requerid="False" />--%>
                <asp:TextBox runat="server" ID="txtFechaExpedicionGarantia" Enabled="False"></asp:TextBox>
                <%--<asp:Image ID="imgFechaExpedicionGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />--%>
                <%--<Ajax:CalendarExtender ID="cetxtFechaDesde" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="imgFechaExpedicionGarantia" TargetControlID="txtFechaExpedicionGarantia"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="False" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaExpedicionGarantia">
                </Ajax:MaskedEditExtender>--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               Fecha de Vencimiento Inicial 
                de Garantía 
            </td>
            <td>
               Fecha de Vencimiento Final&nbsp; 
                de Garantía</td>
        </tr>
        <tr class="rowA">
            <td>
                <%--<uc1:fecha ID="txtFechaVencimientoInicial" runat="server"  Enabled="False" Requerid="False"/>--%>
                <asp:TextBox runat="server" ID="txtFechaVencimientoInicial" Enabled="False"></asp:TextBox>
                <%--<asp:Image ID="ImgFechaVencimientoInicial" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />--%>
                <%--<Ajax:CalendarExtender ID="ceFechaVencimientoInicial" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="ImgFechaVencimientoInicial" TargetControlID="txtFechaVencimientoInicial"></Ajax:CalendarExtender>--%>
                <%--<Ajax:MaskedEditExtender ID="meeFechaVencimientoInicial" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="False" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVencimientoInicial">
                </Ajax:MaskedEditExtender>--%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaVencimientoFinal" Enabled="False"></asp:TextBox>
                <%--<uc1:fecha_1 ID="" runat="server"  Enabled="False" Requerid="False" />--%>
            </td>
        </tr>
        <tr class="rowB">
        <td>
               Fecha de Recibo de Garant&iacute;a 
            </td>
            <td>
                Fecha Devolución Garant&iacute;a
            </td>
            
        </tr>
        <tr class="rowA">
            <td>
                <%--<uc1:fecha ID="txtFechaReciboGarantia" runat="server"  Enabled="False" Requerid="False" />--%>
                <asp:TextBox runat="server" ID="txtFechaReciboGarantia" Enabled="False"></asp:TextBox>
                <%--<asp:Image ID="ImgFechaReciboGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />--%>
                <%--<Ajax:CalendarExtender ID="ceFechaReciboGarantia" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="ImgFechaReciboGarantia" TargetControlID="txtFechaReciboGarantia"></Ajax:CalendarExtender>--%>
                <%--<Ajax:MaskedEditExtender ID="meeFechaReciboGarantia" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="False" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaReciboGarantia">
                </Ajax:MaskedEditExtender>--%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaDevolucionGarantia" Enabled="False"></asp:TextBox>
            </td>
            
        </tr>
        <tr class="rowB">
            <td>
               Fecha de Aprobaci&oacute;n 
            </td>
            <td>
               Fecha de Certificaci&oacute;n del Pago a la Garant&iacute;a
            </td>
            
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaAprobacion" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaCertificacion" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
               Motivo Devoluci&oacute;n Garant&iacute;a
            </td>
            <td>
               Estado
            </td>
            
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtMotivoDevolucion" Enabled="False" 
                    TextMode="MultiLine" Height="80px" Width="80%" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEstado" Enabled="False" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               Valor Garant&iacute;a 
            </td>
            <td>
              Anexos 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtValorGarantia" MaxLength="50" Width="40%" Enabled="False"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="fteValorGarantia" runat="server" TargetControlID="txtValorGarantia"
                    FilterType="LowercaseLetters,UppercaseLetters,Numbers" />--%>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblAnexos" RepeatDirection="Horizontal" Enabled="False"></asp:RadioButtonList>
            </td>
            
        </tr>
        <tr  class="rowB">
            <td>
              Observaciones 
            </td>
            <td>
              <%--Zona--%>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionAnexos" MaxLength="250" 
                    Width="80%" Height="80px" TextMode="MultiLine" 
            onKeyDown="limitText(this,250);" onKeyUp="limitText(this,250);" 
                    CssClass="TextBoxGrande" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtDescripcionAnexos"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
            <td>
                <div style="visibility:hidden" >
                    <asp:TextBox runat="server" ID="txtZona" Enabled="False" ></asp:TextBox>
                </div>
            </td>
        </tr>
		</table>
         <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
     <asp:Panel runat="server" ID="PnlArchivosAnexos" Visible="True">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="NombreArchivo,IdArchivo,IDArchivosGarantias"
                                CellPadding="0" Height="16px" 
                                onselectedindexchanged="gvDocumentos_SelectedIndexChanged" 
                                onrowcommand="gvDocumentos_RowCommand">
                     <Columns>
                        <%--<asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnSeleccionar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                    Height="16px" Width="16px" ToolTip="Seleccionar" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:BoundField HeaderText="Documento Anexo" DataField="NombreArchivoOri" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" ImageUrl="~/Image/btn/list.png"
                                    Height="16px" Width="16px" ToolTip="Mostrar" Enabled="true" 
                                    onclick="btnMostrar_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="LightBlue" />
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
     </table>
     <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                <asp:Label runat="server" ID="lblValorTotalGarantias" 
                    Text="Valor Total Garantía"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtValorTotalGarantias" Visible="True" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        </table>
