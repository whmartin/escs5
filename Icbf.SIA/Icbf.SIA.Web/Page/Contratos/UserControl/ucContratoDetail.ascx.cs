﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Business;

public partial class Page_Contratos_UserControl_ucContratoDetail : GeneralUserControl
{
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();
    Contrato vContrato = new Contrato();
    GeneralWeb pageWeb;

    string PageName = "Contratos/Contratos";

    private bool scriptEnCola;
    private int NoPrecargarInformacion;
    private const string CodEstadoEnRegistro = "REE";
    private const string CodEstadoRegistrado = "REC";
    private const string Op = "E";
    #region ConstantesConfiguracionControles
    //Contrato Asociado
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    //Categoria contrato
    private const string CodCategoriaContrato = "CTR"; //"Contrato";
    private const string CodCategoriaConvenio = "CVN"; //"Convenio";

    //Tipos contrato
    private const string CotTipoContConvPrestServApoyoGestion = "PREST_SERV_APO_GEST"; //"Prestación de Servicios de Apoyo a la gestión";
    private const string CodTipoContConvPrestServProfesionales = "PREST_SERV_PROF"; //"Prestación de Servicios Profesionales";
    private const string CodTipoContAporte = "APORTE"; //"Aporte";
    private const string CodMarcoInteradministrativo = "MC_INT_ADMIN"; //"Marco Interadministrativo";

    //Modalidad selección
    private const string CodContratacionDirecta = "CONT_DIR"; //"Contratación Directa";  
    private const string CodContratacionDirectaAporte = "CDA";
    #endregion

    /// <summary>
    /// Los identificadores fijos corresponden a los id de tabla que corresponden a los códigos
    /// establecidos como constantes y usados para temas de configuración de controles dentro de esta pantalla
    /// </summary>
    #region IdentificadoresFijos
    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }
    private string IdEstadoContrato
    {
        get
        {
            return hfIdEstadoContrato.Value;
        }
        set
        {
            hfIdEstadoContrato.Value = value;
        }
    }

    private string IdCategoriaContrato
    {
        get
        {
            return hfIdCategoriaContrato.Value;
        }
        set
        {
            hfIdCategoriaContrato.Value = value;
        }
    }
    private string IdCategoriaConvenio
    {
        get
        {
            return hfIdCategoriaConvenio.Value;
        }
        set
        {
            hfIdCategoriaConvenio.Value = value;
        }
    }

    private string IdTipoContConvPrestServApoyoGestion
    {
        get
        {
            return hfIdTipoContConvPrestServApoyoGestion.Value;
        }
        set
        {
            hfIdTipoContConvPrestServApoyoGestion.Value = value;
        }
    }
    private string IdTipoContConvPrestServProfesionales
    {
        get
        {
            return hfIdTipoContConvPrestServProfesionales.Value;
        }
        set
        {
            hfIdTipoContConvPrestServProfesionales.Value = value;
        }
    }

    private string IdMarcoInteradministrativo
    {
        get
        {
            return hfIdMarcoInteradministrativo.Value;
        }
        set
        {
            hfIdMarcoInteradministrativo.Value = value;
        }
    }

    private string IdContratacionDirecta
    {
        get
        {
            return hfIdContratacionDirecta.Value;
        }
        set
        {
            hfIdContratacionDirecta.Value = value;
        }
    }
    #endregion

    private string CodContratoAsociadoSeleccionado
    {
        get
        {
            return hfCodContratoAsociadoSel.Value;
        }
        set
        {
            hfCodContratoAsociadoSel.Value = value;
        }
    }
    private string TotalProductos
    {
        get
        {
            return hfTotalProductos.Value;
        }
        set
        {
            hfTotalProductos.Value = value;
        }
    }
    private string IdContratoAsociado
    {
        get
        {
            return hfIdConvenioContratoAsociado.Value;
        }
        set
        {
            hfIdConvenioContratoAsociado.Value = value;
        }
    }
    private string AcordeonActivo
    {
        get
        {
            return hfAcordeonActivo.Value;
        }
        set
        {
            hfAcordeonActivo.Value = value;
        }
    }

    private string nConsecutivosPlanCompras
    { set { hfConsecutivosPlanCompras.Value = value; } }
    private string nProductos
    { set { hfProductos.Value = value; } }
    private string nAportes
    { set { hfAportes.Value = value; } }
    private string nLugarEjecucion
    { set { hfLugarEjecucion.Value = value; } }
    private string nContratistas
    { set { hfContratistas.Value = value; } }
    private string nSupervInterv
    { set { hfSupervInterv.Value = value; } }
    private string nCDP
    { set { hfCDP.Value = value; } }
    private string nVigenciasFuturas
    { set { hfVigenciasFuturas.Value = value; } }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad Contrato
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;

            vContrato.IdContrato = Convert.ToInt32(IdContrato);
            vContrato.IdEstadoContrato = Convert.ToInt32(IdEstadoContrato);

            #region DatosGeneralesContrato
            #region ObtieneIdTipoContratoAsociado
            TipoContratoAsociado vContratoAsociado = new TipoContratoAsociado();
            vContratoAsociado.CodContratoAsociado = CodContratoAsociadoSeleccionado;
            vContratoAsociado = vContratoService.IdentificadorCodigoTipoContratoAsociado(vContratoAsociado);
            #endregion
            if (vContratoAsociado.IdContratoAsociado != 0)
                vContrato.IdContratoAsociado = vContratoAsociado.IdContratoAsociado; // TipoContratoAsociado

            vContrato.ConvenioMarco = chkConvenioMarco.Checked;
            vContrato.ConvenioAdhesion = chkContratoConvenioAd.Checked;

            if (!IdContratoAsociado.Equals(string.Empty))
                vContrato.FK_IdContrato = Convert.ToInt32(IdContratoAsociado); // no lo hace la lupa

            if (!ddlCategoriaContrato.SelectedValue.Equals("-1"))
                vContrato.IdCategoriaContrato = Convert.ToInt32(ddlCategoriaContrato.SelectedValue);

            if (!ddlTipoContratoConvenio.SelectedValue.Equals("-1"))
                vContrato.IdTipoContrato = Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue);

            if (!ddlModalidadAcademica.SelectedValue.Equals("-1"))
                vContrato.IdModalidadAcademica = ddlModalidadAcademica.SelectedValue;

            if (!ddlNombreProfesion.SelectedValue.Equals("-1"))
                vContrato.IdProfesion = ddlNombreProfesion.SelectedValue;

            if (!ddlModalidadSeleccion.SelectedValue.Equals("-1"))
                vContrato.IdModalidadSeleccion = Convert.ToInt32(ddlModalidadSeleccion.SelectedValue);

            if (!hfIdNumeroProceso.Value.Equals(string.Empty))
                vContrato.IdNumeroProceso = Convert.ToInt32(hfIdNumeroProceso.Value); // no lo hace la lupa

            if (!caFechaAdjudicaProceso.Date.ToShortDateString().Equals(string.Empty) && !caFechaAdjudicaProceso.Date.ToShortDateString().Equals("01/01/1900"))
                vContrato.FechaAdjudicacionProceso = caFechaAdjudicaProceso.Date;

            vContrato.ActaDeInicio = null;
            if (chkSiReqActa.Checked)
                vContrato.ActaDeInicio = true;
            if (chkNoReqActa.Checked)
                vContrato.ActaDeInicio = false;

            vContrato.ManejaAporte = null;
            if (chkSiManejaAportes.Checked)
                vContrato.ManejaAporte = true;
            if (chkNoManejaAportes.Checked)
                vContrato.ManejaAporte = false;

            vContrato.ManejaRecurso = null;
            if (chkSiManejaRecursos.Checked)
                vContrato.ManejaRecurso = true;
            if (chkNoManejaRecursos.Checked)
                vContrato.ManejaRecurso = false;

            if (!ddlRegimenContratacion.SelectedValue.Equals("-1"))
                vContrato.IdRegimenContratacion = Convert.ToInt32(ddlRegimenContratacion.SelectedValue);

            if (!txtDatosAdicionalesLugarEjecucion.Text.Equals(string.Empty))
                vContrato.DatosAdicionaleslugarEjecucion = txtDatosAdicionalesLugarEjecucion.Text;

            if (!rblRequiereGarantia.SelectedValue.Equals(string.Empty))
                vContrato.RequiereGarantia = Convert.ToBoolean(rblRequiereGarantia.SelectedValue);

            if (!rblAportesEspecie.SelectedValue.Equals(string.Empty))
                vContrato.AportesEspecie = Convert.ToBoolean(rblAportesEspecie.SelectedValue);


            //vContrato.IdEmpleadoSolicitante // lo hace la lupa
            //vContrato.IdEmpleadoOrdenadorGasto // lo hace la lupa
            #endregion

            #region PlanCompras

            #region ConsecutivoPlanCompras
            if (!DdlConsecutivoPlanCompras.SelectedValue.Equals("-1"))
                vContrato.ConsecutivoPlanComprasAsociado = Convert.ToInt32(DdlConsecutivoPlanCompras.SelectedValue);
            #endregion

            #endregion

            #region VigenciaValorContrato
            #region Objeto
            if (!txtObjeto.Text.Equals(string.Empty))
                vContrato.ObjetoContrato = txtObjeto.Text;
            #endregion

            #region Alcance
            if (!txtAlcance.Text.Equals(string.Empty))
                vContrato.AlcanceObjetoContrato = txtAlcance.Text;
            #endregion

            #region ValorInicial
            if (!txtValorInicialContConv.Text.Equals(string.Empty))
            {
                //string valorInicial = txtValorInicialContConv.Text.Replace(",", "");
                string valorInicial = txtValorInicialContConv.Text; //.Replace("$", "");
                vContrato.ValorInicialContrato = Convert.ToDecimal(valorInicial);
            }
            #endregion

            #region FechaInicioEjec
            if (!caFechaInicioEjecucion.Date.ToShortDateString().Equals(string.Empty) && !caFechaInicioEjecucion.Date.ToShortDateString().Equals("01/01/1900"))
                vContrato.FechaInicioEjecucion = caFechaInicioEjecucion.Date;
            #endregion

            #region FechaFinalizacionInicial
            if (!caFechaFinalizacionInicial.Date.ToShortDateString().Equals(string.Empty) && !caFechaFinalizacionInicial.Date.ToShortDateString().Equals("01/01/1900"))
                vContrato.FechaFinalizacionIniciaContrato = caFechaFinalizacionInicial.Date;
            #endregion

            #region FechaFinalTerminacion
            if (!txtFechaFinalTerminacion.Text.Equals(string.Empty))
                vContrato.FechaFinalTerminacionContrato = Convert.ToDateTime(txtFechaFinalTerminacion.Text);
            #endregion

            #region ManejaVigenciasFuturas
            vContrato.ManejaVigenciaFuturas = null;
            if (chkSiManejaVigFuturas.Checked)
                vContrato.ManejaVigenciaFuturas = true;
            if (chkNoManejaVigFuturas.Checked)
                vContrato.ManejaVigenciaFuturas = false;
            #endregion

            #region VigenciaFiscalInicial
            if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1"))
                vContrato.IdVigenciaInicial = Convert.ToInt32(ddlVigenciaFiscalIni.SelectedValue);
            #endregion

            #region VigenciaFiscalFinal
            if (!ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
                vContrato.IdVigenciaFinal = Convert.ToInt32(ddlVigenciaFiscalFin.SelectedValue);
            #endregion

            #region FormaPago
            if (!ddlFormaPago.SelectedValue.Equals("-1"))
                vContrato.IdFormaPago = Convert.ToInt32(ddlFormaPago.SelectedValue);
            #endregion
            #region Tipo Forma de Pago
            if (!ddlTipoFormaPago.SelectedValue.Equals("-1"))
                vContrato.IdTipoPago = Convert.ToInt32(ddlTipoFormaPago.SelectedValue);
            #endregion


            //if (!ddlCodigoSECOP.SelectedValue.Equals("-1"))
            //    vContrato.IdCodigoSECOP = Convert.ToInt32(ddlCodigoSECOP.SelectedValue);

            #endregion

            #region LugarEjecucion

            #region DatosAdicionales

            if (!txtDatosAdicionalesLugarEjecucion.Text.Equals(string.Empty))
                vContrato.DatosAdicionaleslugarEjecucion = txtDatosAdicionalesLugarEjecucion.Text;

            #endregion

            #endregion

            if (Op == "E")
            {
                vContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
            }
            else
            {
                vContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
            }

            //InformacionAudioria(vContrato, this.PageName, vSolutionPage);
            vResultado = vContratoService.ContratoActualizacion(vContrato);

            if (vResultado == 0)
            {
                lblError.Text = @"La operación no se completo satisfactoriamente, verifique por favor.";
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Contrato.IdContrato", vContrato.IdContrato);
                SetSessionParameter("Contrato.Guardado", "1");
                RemoveSessionParameter("Contrato.ContratosAPP");

            }
            else
            {
                lblError.Text = @"La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.";
            }
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = @ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = @ex.Message;
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            pageWeb = (GeneralWeb)this.Page;
            lblTitulo.Text = @"Contrato";
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = @ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = @ex.Message;
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdContrato = 0;

            if (! string.IsNullOrEmpty(GetSessionParameter("Contrato.IdContrato").ToString()))
                vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            else if (!string.IsNullOrEmpty(GetSessionParameter("Contrato.IdContratoReasignar").ToString()))
            {
                vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContratoReasignar"));
                RemoveSessionParameter("Contrato.IdContratoReasignar");
                MultiViewAsignarContrato.ActiveViewIndex = 0;
            }
            hfIdContrato.Value = vIdContrato.ToString();

            SetSessionParameter("Contrato.ContratosAPP", vIdContrato);

            #region CargaEncabezado

            dvUsuarioCreacion.Visible = true;

                 txtFechaRegistro.Text = DateTime.Today.ToString().Substring(0, 10);

            #endregion

            #region Radio Button
            rblRequiereGarantia.Items.Insert(0, new ListItem("No", "False"));
            rblRequiereGarantia.Items.Insert(0, new ListItem("Si", "True"));

            rblAportesEspecie.Items.Insert(0, new ListItem("No", "False"));
            rblAportesEspecie.Items.Insert(0, new ListItem("Si", "True"));
            #endregion

            #region CargarIdentificadores
            List<EstadoContrato> estadoContrato = vContratoService.ConsultarEstadoContrato(null, CodEstadoEnRegistro, null, true);
            if (estadoContrato.Count > 0)
                IdEstadoContrato = estadoContrato[0].IdEstadoContrato.ToString();

            CategoriaContrato vCategoriaContrato = new CategoriaContrato();
            vCategoriaContrato.CodigoCategoriaContrato = CodCategoriaContrato;
            IdCategoriaContrato = vContratoService.IdentificadorCodigoCategoriaContrato(vCategoriaContrato).IdCategoriaContrato.ToString();
            vCategoriaContrato = new CategoriaContrato();
            vCategoriaContrato.CodigoCategoriaContrato = CodCategoriaConvenio;
            IdCategoriaConvenio = vContratoService.IdentificadorCodigoCategoriaContrato(vCategoriaContrato).IdCategoriaContrato.ToString();

            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato.CodigoTipoContrato = CotTipoContConvPrestServApoyoGestion;
            IdTipoContConvPrestServApoyoGestion = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
            vTipoContrato = new TipoContrato();
            vTipoContrato.CodigoTipoContrato = CodTipoContConvPrestServProfesionales;
            IdTipoContConvPrestServProfesionales = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
            vTipoContrato = new TipoContrato();
            vTipoContrato.CodigoTipoContrato = CodMarcoInteradministrativo;
            IdMarcoInteradministrativo = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
            //vTipoContrato = new TipoContrato();
            //vTipoContrato.CodigoTipoContrato = CodTipoContAporte;
            //IdTipoContAporte = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();

            ModalidadSeleccion vModalidad = new ModalidadSeleccion();
            vModalidad.CodigoModalidad = CodContratacionDirecta;
            IdContratacionDirecta = vContratoService.IdentificadorCodigoModalidadSeleccion(vModalidad).IdModalidad.ToString();
            #endregion

            #region LlenarComboCategoriaContrato
            ddlCategoriaContrato.Items.Clear();
            List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
            foreach (CategoriaContrato tD in vLContratos)
            {
                ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
            }
            ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #region LlenarComboModalidadAcademicaKACTUS
            ddlModalidadAcademica.Items.Clear();
            List<ModalidadAcademicaKactus> vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, null, null);
            foreach (ModalidadAcademicaKactus tD in vLModalidadesAcademicas)
            {
                ddlModalidadAcademica.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
            }
            ddlModalidadAcademica.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #region LlenarComboModalidadSeleccion
            ddlModalidadSeleccion.Items.Clear();
            List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
            foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
            {
                ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
            }
            ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            #region LlenarCodigoSECOP
            //ddlCodigoSECOP.Items.Clear();
            //List<CodigosSECOP> vLCodigoSECOP = vContratoService.ConsultarVariosCodigosSECOP(null, null);
            //foreach (CodigosSECOP tD in vLCodigoSECOP)
            //{
            //    ddlCodigoSECOP.Items.Add(new ListItem(tD.CodigoSECOP, tD.IdCodigoSECOP.ToString()));
            //}
            //ddlCodigoSECOP.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            #region LlenarComboRegimenContratacion
            ddlRegimenContratacion.Items.Clear();
            List<RegimenContratacion> vLRegimensContratacion = vContratoService.ConsultarRegimenContratacions(null, null, null);
            foreach (RegimenContratacion tD in vLRegimensContratacion)
            {
                ddlRegimenContratacion.Items.Add(new ListItem(tD.NombreRegimenContratacion, tD.IdRegimenContratacion.ToString()));
            }
            ddlRegimenContratacion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #region LlenarCombosVigencias
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalIni, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalFin, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            #endregion

            #region LlenarComboFormaPago
            ddlFormaPago.Items.Clear();
            List<TipoFormaPago> vLFormasPago = vContratoService.ConsultarTipoMedioPagos(null, null, null, null);
            foreach (TipoFormaPago tD in vLFormasPago)
            {
                ddlFormaPago.Items.Add(new ListItem(tD.NombreTipoFormaPago, Convert.ToString(tD.IdTipoFormaPago)));
            }
            ddlFormaPago.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            #region LlenarComboTipoFormaPago
            ddlTipoFormaPago.Items.Clear();
            List<TipoPago> vLTipoFormasPago = vContratoService.ConsultarVariosTipoPago(null,null);
            foreach (TipoPago tD in vLTipoFormasPago)
            {
                ddlTipoFormaPago.Items.Add(new ListItem(tD.NombreTipoPago, Convert.ToString(tD.IdTipoPago)));
            }
            ddlTipoFormaPago.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion


            AcordeonActivo = "1";
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    #region Encabezado

    private string ObtenerRegionalUsuario(int? IdRegional)
    {
        Regional usuarioRegional = vSIAService.ConsultarRegional(IdRegional);
        return usuarioRegional.CodigoRegional + " - " + usuarioRegional.NombreRegional; // Pagina 6 Descripcion adicional Regional del Usuario
    }

    private void SeleccionaRegional(string idRegionalSeleccionada)
    {
        if (idRegionalSeleccionada.Equals("-1"))
        {
            chkContratoConvenioAd.Enabled = false;
            chkConvenioMarco.Enabled = false;
            HabilitaCategoriaContrato(false);
            SeleccionadoContratoAsociado(string.Empty);
        }
        else
        {
            hfRegional.Value = idRegionalSeleccionada;
            chkContratoConvenioAd.Enabled = false;
            chkConvenioMarco.Enabled = false;
            HabilitaCategoriaContrato(true);
        }
    }

    #endregion
    private void SeleccionarPanel(string commandNamePanel)
    {
        switch (commandNamePanel)
        {
            case "DatosGeneralContrato":
                break;
            case "PlanComprasProductos":
                break;
            case "VigenciaValorContrato":
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            NoPrecargarInformacion = 1;

            int vIdContrato;
            vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
            hfIdContratoS.Value = vIdContrato.ToString();
            vContrato = vContratoService.ContratoObtener(vIdContrato);

            if (vContrato.UsuarioCrea == "MigracionFUC" && !string.IsNullOrEmpty(vContrato.NumeroContratoMigrado))
            {
                hfContratoMigrado.Value = vContrato.IdContrato.ToString();
            }
            else
            {
                hfContratoMigrado.Value = null;
            }

            if (!string.IsNullOrEmpty(vContrato.VinculoSECOP))
                hlkVinculoSECOP.NavigateUrl = vContrato.VinculoSECOP;

            txtNombreUsuario.Text = !string.IsNullOrEmpty(vContrato.UsuarioModifica) && vContrato.UsuarioModifica != "job_EstadosContratos" ? vContrato.UsuarioModifica : vContrato.UsuarioCrea;
            IdContrato = vContrato.IdContrato.ToString();
            txtIdContrato.Text = IdContrato;
            txtNumeroContrato.Text = vContrato.NumeroContrato;
            rblRequiereGarantia.SelectedValue = vContrato.RequiereGarantia.ToString();
            rblAportesEspecie.SelectedValue = vContrato.AportesEspecie.ToString();
            txtNumContMigrado.Text = vContrato.NumeroContratoMigrado;
            string CodContratoAsociado = string.Empty;

            #region Regional
            Usuario vUsuario = vSIAService.ConsultarUsuario(txtNombreUsuario.Text);

            if (vUsuario != null)
            {
                int? idRegional = vUsuario.IdRegional;

                if (idRegional != null)
	            {
                    txtRegionalUsuario.Text = ObtenerRegionalUsuario(idRegional);
                    hfRegional.Value = Convert.ToString(idRegional);		 
	            }
                else
                {
                    int tipoUsuario = vUsuario.IdTipoUsuario;
                    if (tipoUsuario == 1) // Sede Nacional
                    {
                        ManejoControlesContratos.LlenarComboLista(ddlRegionalUsuarioContrato, vSIAService.ConsultarRegionals(null, null), "IdRegional", "NombreRegional");

                        ddlRegionalUsuarioContrato.Visible = true;
                        txtRegionalUsuario.Visible = false;
                        chkContratoConvenioAd.Enabled = false;
                        chkConvenioMarco.Enabled = false;
                        ddlCategoriaContrato.Enabled = false;
                        ddlRegionalUsuarioContrato.SelectedValue = "34";
                    }
                    else
                    {
                        // Diferente a Sede Nacional sin regional
                        lblError.Text = @"El usuario no tiene registrada una regional. Debe asignarle una regional para poder continuar";

                        chkContratoConvenioAd.Enabled = false;
                        chkConvenioMarco.Enabled = false;
                    }
                }
            }
            ddlRegionalUsuarioContrato.Enabled = false;
            #endregion

            if (vContrato.IdContratoAsociado != null)
            {
                TipoContratoAsociado vTipoContratoAsociado = new TipoContratoAsociado();
                vTipoContratoAsociado.IdContratoAsociado = (int)vContrato.IdContratoAsociado;
                vTipoContratoAsociado = vContratoService.IdentificadorCodigoTipoContratoAsociado(vTipoContratoAsociado);
                CodContratoAsociado = vTipoContratoAsociado.CodContratoAsociado;
                chkConvenioMarco.Checked = vContrato.ConvenioMarco;
                chkContratoConvenioAd.Checked = vContrato.ConvenioAdhesion;
                SeleccionadoContratoAsociado(CodContratoAsociado);
                NumeroContrato();
                CategoriaContrato();
            }
            else
            {
                SeleccionadoContratoAsociado(CodContratoAsociado);
                CategoriaContrato();
            }

            chkConvenioMarco.Enabled = false;
            chkContratoConvenioAd.Enabled = false;


            CargarInforProrroga();

            #region Fechas
            txtFechaRegistro.Text = vContrato.FechaCrea.ToString("dd/MM/yyyy");//.Substring(0, 10);
            if (vContrato.FechaSuscripcionContrato != null)
            {
                if (Convert.ToDateTime(vContrato.FechaSuscripcionContrato).Year != 1)
                {
                    txtFechaSuscripcionContr.Text = Convert.ToDateTime(vContrato.FechaSuscripcionContrato).ToShortDateString();
                    //if(vContrato.ActaDeInicio == false)
                    //txtFechaFirmaActIni.Text = Convert.ToDateTime(vContrato.FechaSuscripcionContrato).ToShortDateString();
                }
            }

            if (vContrato.FechaFinalTerminacionContrato != null)
            {
                if (vContrato.FechaLiquidacion == null)
                {
                    DateTime vFechaProyectadaLiq = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato).AddMonths(4);
                    txtFechaProyLiq.Text = vFechaProyectadaLiq.ToString("dd/MM/yyyy");
                }
                else
                {
                    txtFechaLiquidacion.Text = Convert.ToDateTime(vContrato.FechaLiquidacion).ToString("dd/MM/yyyy");
                }
            }

           // #region FechaFinalTerminacionContrato

            //if (vContrato.FechaFinalTerminacionContrato != null)
            //{
            //    txtFechaFinalTerminacion.Text = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato).ToString("dd/MM/yyyy");
            //}

            //#endregion
            //#region FechaInicioEjecucion

            //if (vContrato.FechaInicioEjecucion != null)
            //{
            //    txtFechaInicioEjecucion.Text = Convert.ToDateTime(vContrato.FechaInicioEjecucion).ToString("dd/MM/yyyy");
            //}
            ////#endregion
            //#region FechaFinalizacionInicial
            //if (vContrato.FechaFinalizacionIniciaContrato != null)
            //{
            //    txtFechaFinalizacionInicial.Text = Convert.ToDateTime(vContrato.FechaFinalizacionIniciaContrato).ToString("dd/MM/yyyy");
            //}
            //#endregion

            #endregion

            #region SumatoriasValores

            txtCDP.Text = vContrato.ValorTotalCDP != null ? Convert.ToDecimal(vContrato.ValorTotalCDP).ToString("$ #,###0.00##;($ #,###0.00##)") : "0";

            txtValorVigenciasFuturas.Text =  vContrato.ValorVigenciasFuturas.ToString("$ #,###0.00##;($ #,###0.00##)");

            txtValorInicialRecursosICBF.Text = vContrato.ValorAportesInicialDineroICBF.ToString("$ #,###0.00##;($ #,###0.00##)");

            txtValorAportesContratista.Text = vContrato.ValorTotalAportesContratista != null ? Convert.ToDecimal(vContrato.ValorTotalAportesContratista).ToString("$ #,###0.00##;($ #,###0.00##)") : "0";

            txtValContAdheridos.Text = vContrato.ValorTotalContratosAdheridos != null ? Convert.ToDecimal(vContrato.ValorTotalContratosAdheridos).ToString("$ #,###0.00##;($ #,###0.00##)") : "0";

            txtValorFinalContrato.Text = vContrato.ValorFinalContrato == null ? Convert.ToDecimal(vContrato.ValorInicialContrato).ToString("$ #,###0.00##;($ #,###0.00##)") : 
                                                                                Convert.ToDecimal(vContrato.ValorFinalContrato).ToString("$ #,###0.00##;($ #,###0.00##)");

            #endregion

            #region ValorInicialContrato

            txtValorInicialContConv.Text =  Convert.ToDecimal(vContrato.ValorInicialContrato).ToString("$ #,###0.00##;($ #,###0.00##)");           

            
            #endregion

            #region Acta de Inicio

            if (vContrato.ActaDeInicio == true)
            {
                if (vContrato.FechaActaInicio.HasValue)
                {
                    txtFechaActa.Text = Convert.ToDateTime(vContrato.FechaActaInicio).ToShortDateString();
                    txtFechaActa.Visible = true;
                    lblFeActaInicio.Visible = true;
                    txtFechaActa.Enabled = false;
                }
                else
                {
                    bool isValid = vContratoService.ConsultarContratoRPGarantiasAprobadas(vContrato.IdContrato);

                    if (isValid)
                    {
                        txtFechaActa.Visible = true;
                        lblFeActaInicio.Visible = true;
                        txtFechaActa.Text = string.Empty;
                        ImageFechaActa.Visible = true;
                        rfvActualizarFechaActa.Visible = true;
                        btnActualizar.Visible = true;
                        CalendarExtenderFechaActa.StartDate = vContrato.FechaInicioEjecucion;
                        CalendarExtenderFechaActa.EndDate = vContrato.FechaFinalTerminacionContrato;
                    }
                }
            }

            #endregion

            #region GrillasGontratosAdheridos

            LlenarGrillaContratosAdheridos(vContrato.IdContrato);

            LlenarGrillaContratosRelacionadosConvMarco(vContrato.IdContrato);

            #endregion

            #region GrillaDocumentosContrato

            LlenarGrillaDocumentosContrato(vContrato.IdContrato);

            #endregion

            #region CargarRegionalContrato

            //Regional vRegional = vSIAService.ConsultarRegional(vContrato.IdRegionalContrato);


            //txtRegionalContratoConvenio.Text = vRegional.CodigoRegional.Trim() + @"-" + vRegional.NombreRegional.Trim();
            #endregion

            #region ConsecutivoPlanCompras

            List<PlanDeComprasContratos> consecutivosPlanCompras = vContratoService.ConsultarPlanDeComprasContratoss(vIdContrato, null);
            if (consecutivosPlanCompras.Count > 0)
            {
                txtPlanCompras.Text = consecutivosPlanCompras[0].IDPlanDeCompras.ToString().Trim();
            }


            #endregion

            #region ValorTotalContratosAsociadosConvenioMarco

            if (gvContRelConMar.Rows.Count > 0)
            {
                decimal vValorTotalCDP = 0;
                foreach (GridViewRow fila in gvContRelConMar.Rows)
                {

                    if (gvContRelConMar.DataKeys[fila.RowIndex].Values["valorfinalcontrato"] != null)
                    {

                        vValorTotalCDP += Convert.ToDecimal(gvContRelConMar.DataKeys[fila.RowIndex].Values["valorfinalcontrato"].ToString());
                    }
                }

                txtVlaConAsocConvMac.Text = vValorTotalCDP.ToString("$ #,###0.00##;($ #,###0.00##)");

            }
            else
            {
                txtVlaConAsocConvMac.Text = "0";
            }

            #endregion

            #region TipoContrato

            ddlTipoContratoConvenio.Enabled = false;

            #endregion

            #region TipoAdicionesProrrogas

            txtValTotAdiciones.Text = vContratoService.ConsultarValorTotalAdicionado(int.Parse(hfIdContrato.Value)).ToString("$ #,###0.00##;($ #,###0.00##)");
            txtValTotReducciones.Text = vContratoService.ConsultarValorTotalReducido(int.Parse(hfIdContrato.Value)).ToString("$ #,###0.00##;($ #,###0.00##)");

            #endregion

            #region chkConvenioMarcochkContratoConvenioAd

            chkConvenioMarco.Enabled = false;
            chkContratoConvenioAd.Enabled = false;

            txtEstadoContrato.Text = vContrato.NombreEstadoContrato;
            txtPorcentaje.Text = vContrato.PorcentajeAnticipo.ToString();
            decimal valor = vContratoService.ConsultarValorsinAportes(vIdContrato);
            var resultado = (valor / 100 * vContrato.PorcentajeAnticipo);
            txtValorAnticipo.Text = resultado.ToString("$ #,###0.00##;($ #,###0.00##)");
            
            #endregion



        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            throw new GenericException(ex);
        }
    }

    #region FuncionesCarga

    private void CargarInforProrroga()
    {
        try
        {
            var item = vContratoService.ConsultarProrrogasAprobadasContratoDetalle(int.Parse(hfIdContrato.Value));

            int anios = 0;
            int dias = 0;
            int meses = 0;

            if (item.Anios != 0)
                anios = item.Anios;

            if (item.Meses != 0)
            {
                var modulo = item.Meses % 12;
                var division = item.Meses / 12;

                if (modulo == item.Meses)
                    meses = item.Meses;
                else
                {
                    anios += division;
                    meses = modulo;
                }
            }

            if (item.Dias != 0)
            {
                var modulo = item.Dias % 30;
                var division = item.Dias / 30;

                if (modulo == item.Dias)
                    dias = item.Dias;
                else
                {
                    meses += division;
                    dias = modulo;
                }
            }

            txtProrrogaDias.Text = dias.ToString();
            txtProrrogaAnios.Text = anios.ToString();
            txtProrrogaMeses.Text = dias.ToString();
        }
        catch (Exception ex)
        {

        }
    }

    public void NumeroContrato()
    {
        #region NumeroContrato
        if (vContrato.FK_IdContrato != null)
        {
            IdContratoAsociado = Convert.ToString(vContrato.FK_IdContrato);
        }
        SeleccionaNumeroConvenioContrato(IdContratoAsociado); // Aqui se debe cargar toda la informacion del contrato asociado en hiddens
        #endregion
    }

    public void CategoriaContrato()
    {
        #region Categoria
        string IdCategoriaContrato = "-1";
        if (vContrato.IdCategoriaContrato != null)
        {
            IdCategoriaContrato = vContrato.IdCategoriaContrato.ToString();
            SeleccionadoCategoriaContrato(IdCategoriaContrato);
            ddlCategoriaContrato.SelectedValue = IdCategoriaContrato;
        }

        TipoContrato();
        #endregion
    }

    public void TipoContrato()
    {
        #region TipoContrato
        string IdTipoContrato = "-1";
        if (vContrato.IdTipoContrato != null)
        {
            IdTipoContrato = vContrato.IdTipoContrato.ToString();
            SeleccionadoTipoContratoConvenio(IdCategoriaContrato, IdTipoContrato);
            ddlTipoContratoConvenio.SelectedValue = IdTipoContrato;
        }

        ModalidadAcademica();
        ModalidadSeleccion();

        #endregion
    }

    public void ModalidadAcademica()
    {
        #region ModalidadAcademica
        string IdModalidadAcademica = "-1";
        if (vContrato.IdModalidadAcademica != null)
        {
            IdModalidadAcademica = vContrato.IdModalidadAcademica;
            SeleccionarModalidadAcademica(IdModalidadAcademica);
            ddlModalidadAcademica.SelectedValue = IdModalidadAcademica;
        }

        Profesion();
        #endregion
    }

    public void Profesion()
    {
        #region Profesion
        string IdProfesion = "-1";
        if (vContrato.IdProfesion != null)
            IdProfesion = vContrato.IdProfesion;
        SeleccionaProfesion(IdProfesion);
        ddlNombreProfesion.SelectedValue = IdProfesion.Trim();
        #endregion
    }

    public void ModalidadSeleccion()
    {
        #region ModalidadSeleccion
        string IdModalidadSeleccion = "-1";
        if (vContrato.IdModalidadSeleccion != null)
        {
            IdModalidadSeleccion = vContrato.IdModalidadSeleccion.ToString();
            SeleccionarModalidadSeleccion(IdModalidadSeleccion);
            ddlModalidadSeleccion.SelectedValue = IdModalidadSeleccion;
        }
        NumeroProceso();
        ActaInicio();
        CodigoSECOP();
        #endregion
    }

    public void CodigoSECOP()
    {
        string IdCodigoSECOP = "-1";
        if (vContrato.IdCodigoSECOP != null)
        {
            //ddlCodigoSECOP.SelectedValue = vContrato.IdCodigoSECOP.ToString();
        }
    }

    public void NumeroProceso()
    {
        #region NumeroProceso
        if (vContrato.IdNumeroProceso != null)
        {
            hfIdNumeroProceso.Value = Convert.ToString(vContrato.IdNumeroProceso);
            hfNumeroProceso.Value = Convert.ToString(vContrato.NumeroProceso);
            txtNumeroProceso.Text = Convert.ToString(vContrato.NumeroProceso);
            HabilitarNumeroProceso(true);
        }

        FechaAdjudicacionProceso();
        SeleccionarNumeroProceso(hfNumeroProceso.Value);
        #endregion
    }

    public void FechaAdjudicacionProceso()
    {
        #region FechaAdjudicacionProceso
        string FechaAdjudicacionProceso = string.Empty;
        if (vContrato.FechaAdjudicacionProceso != null)
        {
            FechaAdjudicacionProceso = vContrato.FechaAdjudicacionProceso.ToString();
            caFechaAdjudicaProceso.Date = (DateTime)vContrato.FechaAdjudicacionProceso;
        }
        SeleccionaFechaAdjudicacion(FechaAdjudicacionProceso);
        #endregion
    }

    public void ActaInicio()
    {
        #region ActaInicio
        // (SE REPITE)
        //RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Se comenta para que no se repita el llamado
        if (vContrato.ActaDeInicio != null)
        {
            HabilitarManejaAportes(true);
            RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
            if (vContrato.ActaDeInicio == true)
            {
                chkSiReqActa.Checked = true;
                chkNoReqActa.Checked = false;
            }

            else
            {
                chkNoReqActa.Checked = true;
                chkSiReqActa.Checked = false;
            }
        }
        else
        {
            HabilitarManejaAportes(false);
        }

        ManejaAportes();
        #endregion
    }

    public void ManejaAportes()
    {
        #region ManejaAportes
        //RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue);
        if (vContrato.ManejaAporte != null)
        {
            HabilitarManejaRecursos(true);
            RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
            if (vContrato.ManejaAporte == true)
            {
                chkSiManejaAportes.Checked = true;
                chkNoManejaAportes.Checked = false;
            }

            else
            {
                chkNoManejaAportes.Checked = true;
                chkSiManejaAportes.Checked = false;
            }
        }
        else
        {
            HabilitarManejaRecursos(false);
        }

        ManejaRecursos();
        #endregion
    }

    public void ManejaRecursos()
    {
        #region ManejaRecursos
        //RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue);
        if (vContrato.ManejaRecurso != null)
        {
            HabilitarRegimenContratacion(true);
            if (vContrato.ManejaRecurso == true)
            {
                chkSiManejaRecursos.Checked = true;
                chkNoManejaRecursos.Checked = false;
            }

            else
            {
                chkSiManejaRecursos.Checked = false;
                chkNoManejaRecursos.Checked = true;
            }

        }
        else
        {
            HabilitarRegimenContratacion(false);
        }

        RegimenContratacion();
        #endregion
    }

    public void RegimenContratacion()
    {
        #region RegimenContratacion
        string IdRegimenContratacion = "-1";
        if (vContrato.IdRegimenContratacion != null)
        {
            IdRegimenContratacion = Convert.ToString(vContrato.IdRegimenContratacion);
            SeleccionaRegimenContratacion(IdRegimenContratacion);
            ddlRegimenContratacion.SelectedValue = IdRegimenContratacion;
        }
        Solicitante();
        #endregion
    }

    public void Solicitante()
    {
        #region Solicitante
        if (vContrato.IdEmpleadoSolicitante != null)
        {
            Empleado vEmpleadoSol = new Empleado();
            vEmpleadoSol = vContratoService.ConsultarEmpleado(vContrato.IdEmpleadoSolicitante.ToString(), vContrato.IdRegionalEmpSol.ToString(),
                vContrato.IdDependenciaEmpSol.ToString(), vContrato.IdCargoEmpSol.ToString());
            hfNombreSolicitante.Value = vEmpleadoSol.PrimerNombre + " " + vEmpleadoSol.PrimerApellido;
            hfRegionalContConv.Value = vEmpleadoSol.Regional;
            hfDependenciaSolicitante.Value = vEmpleadoSol.Dependencia;
            hfCargoSolicitante.Value = vEmpleadoSol.Cargo;
            hfCodRegionalContConv.Value = vEmpleadoSol.IdRegional;

            txtNombreSolicitante.Text = hfNombreSolicitante.Value;
            txtRegionalContratoConvenio.Text = hfRegionalContConv.Value;
            txtDependenciaSolicitante.Text = hfDependenciaSolicitante.Value;
            txtCargoSolicitante.Text = hfCargoSolicitante.Value;
            SeleccionarSolicitante(txtNombreSolicitante.Text);
        }

        OrdenadorGasto();
        #endregion
    }

    public void OrdenadorGasto()
    {
        #region OrdenadoGasto
        
        if (vContrato.IdEmpleadoOrdenadorGasto != null)
        {
            Empleado vEmpleadoOrdG = new Empleado();
            vEmpleadoOrdG = vContratoService.ConsultarEmpleado(vContrato.IdEmpleadoOrdenadorGasto.ToString(), vContrato.IdRegionalEmpOrdG.ToString(),
            vContrato.IdDependenciaEmpOrdG.ToString(), vContrato.IdCargoEmpOrdG.ToString());

            hfNombreOrdenadorGasto.Value = vEmpleadoOrdG.PrimerNombre + " " + vEmpleadoOrdG.PrimerApellido;
            hfTipoIdentOrdenadorGasto.Value = vEmpleadoOrdG.TipoIdentificacion;
            hfNumeroIdentOrdenadoGasto.Value = vEmpleadoOrdG.NumeroIdentificacion;
            hfCargoOrdenadoGasto.Value = vEmpleadoOrdG.Cargo;

            txtNombreOrdenadorGasto.Text = hfNombreOrdenadorGasto.Value;
            txtTipoIdentOrdenadorGasto.Text = hfTipoIdentOrdenadorGasto.Value;
            txtNumeroIdentOrdenadoGasto.Text = hfNumeroIdentOrdenadoGasto.Value;
            txtCargoOrdenadoGasto.Text = hfCargoOrdenadoGasto.Value;

            SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value);

            if (chkSiManejaRecursos.Checked)
            {
                SeleccionarPlanCompras("1");
                ConsecutivoPlanCompras();
                AcordeonActivo = "2";
            }
        }
        else if (vContrato.UsuarioCrea == "MigracionFUC")
        {
            var NumeroIdentificacion = vContratoService.ConsultarIdentificacionOrdenadorG(vContrato.IdContrato);
            Empleado vEmpleadoOrdG = new Empleado();
            vEmpleadoOrdG = vContratoService.ConsultarEmpleado(NumeroIdentificacion.ToString(), null,null,null);

            hfNombreOrdenadorGasto.Value = vEmpleadoOrdG.PrimerNombre + " " + vEmpleadoOrdG.PrimerApellido;
            hfTipoIdentOrdenadorGasto.Value = vEmpleadoOrdG.TipoIdentificacion;
            hfNumeroIdentOrdenadoGasto.Value = vEmpleadoOrdG.NumeroIdentificacion;
            hfCargoOrdenadoGasto.Value = vEmpleadoOrdG.Cargo;

            txtNombreOrdenadorGasto.Text = hfNombreOrdenadorGasto.Value;
            txtTipoIdentOrdenadorGasto.Text = hfTipoIdentOrdenadorGasto.Value;
            txtNumeroIdentOrdenadoGasto.Text = hfNumeroIdentOrdenadoGasto.Value;
            txtCargoOrdenadoGasto.Text = hfCargoOrdenadoGasto.Value;

            SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value);

            if (chkSiManejaRecursos.Checked)
            {
                SeleccionarPlanCompras("1");
                ConsecutivoPlanCompras();
                AcordeonActivo = "2";
            }
        }

        ObjetoContrato();

        #endregion
    }

    public void ConsecutivoPlanCompras()
    {
        #region ConsecutivoPlanCompras

        string IdConsecutivoPlanCompras = "-1";
        if (vContrato.ConsecutivoPlanComprasAsociado != null)
        {
            IdConsecutivoPlanCompras = vContrato.ConsecutivoPlanComprasAsociado.ToString();

        }
        DdlConsecutivoPlanCompras.SelectedValue = IdConsecutivoPlanCompras;
        SeleccionaConsecutivoPlanDeCompras(IdConsecutivoPlanCompras);

        #endregion
    }

    public void ObjetoContrato()
    {
        if (vContrato.ObjetoContrato != null)
        {
            AcordeonActivo = "3";
            txtObjeto.Text = vContrato.ObjetoContrato;
            if (chkContratoConvenioAd.Checked) // Pagina 15 Paso 62
            {
                txtObjeto.Enabled = false;
            }
            else
            {
                if (chkSiManejaRecursos.Checked)
                {
                    if (gvConsecutivos.Rows.Count > 0)
                    {
                        if (chkConvenioMarco.Checked)
                            txtObjeto.Enabled = false;
                        else
                            txtObjeto.Enabled = false;
                    }
                    else
                        txtObjeto.Enabled = false;
                }
                else
                    txtObjeto.Enabled = false;
            }

            IngresaObjetoContratoConvenio(txtObjeto.Text);
        }

        AlcanceObjetoContrato();
    }

    public void AlcanceObjetoContrato()
    {
        if (vContrato.AlcanceObjetoContrato != null)
        {
            txtAlcance.Text = vContrato.AlcanceObjetoContrato;
            IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
        }

        ValorInicialContrato();
    }

    public void ValorInicialContrato()
    {
        if (vContrato.ValorInicialContrato != null)
        {
            txtValorInicialContConv.Text = Convert.ToDecimal(vContrato.ValorInicialContrato).ToString();//.ToString("#,###0.00##;(#,###0.00##)"); //.ToString("$ #,###0.00##;($ #,###0.00##)"); ;
            IngresaValorInicialContConv(txtValorInicialContConv.Text);
        }
        FechaInicioEjecucion();

    }

    public void FechaInicioEjecucion()
    {
        string FechaInicioEjecucion = string.Empty;

        if (vContrato.FechaInicioEjecucion != null)
        {
            FechaInicioEjecucion = vContrato.FechaInicioEjecucion.ToString();
            caFechaInicioEjecucion.Date = (DateTime)vContrato.FechaInicioEjecucion;

        }
        SeleccionaFechaInicioEjecucion(FechaInicioEjecucion);
        EsFechaFinalizacionCalculada();

    }

    public void EsFechaFinalizacionCalculada()
    {
        if (vContrato.EsFechaFinalCalculada != null)
        {
            chkCalculaFecha.Checked = vContrato.EsFechaFinalCalculada.Value ? true : false;
            chkCalculaFecha.Enabled = false;

            if (chkCalculaFecha.Checked)
            {
                txtAcnosPiEj.Text = "0";
                txtDiasPiEj.Text = vContrato.DiasFechaFinalCalculada.HasValue ? vContrato.DiasFechaFinalCalculada.Value.ToString() : "0";
                txtMesesPiEj.Text = vContrato.MesesFechaFinalCalculada.HasValue ? vContrato.MesesFechaFinalCalculada.Value.ToString() : "0";
            }
        }
        else
        {
            chkCalculaFecha.Checked = false;
            chkCalculaFecha.Enabled = false;
        }

        FechaFinalizacionInicial();
    }

    public void FechaFinalizacionInicial()
    {
        string FechaFinalizacionInicial = string.Empty;
        string FechaFinalizacionFinal = string.Empty;
        if (vContrato.FechaFinalizacionIniciaContrato != null)
        {
            FechaFinalizacionInicial = vContrato.FechaFinalizacionIniciaContrato.ToString();
            FechaFinalizacionFinal = vContrato.FechaFinalizacionIniciaContrato.ToString();
            caFechaFinalizacionInicial.Date = (DateTime)vContrato.FechaFinalizacionIniciaContrato;
        }

        FechaFinalTerminacion();
        SeleccionaFechaFinalizacionInicialContConv(FechaFinalizacionInicial);
    }

    public void FechaFinalTerminacion()
    {
        if (vContrato.FechaFinalTerminacionContrato != null)
        {
            txtFechaFinalTerminacion.Text = vContrato.FechaFinalTerminacionContrato.ToString().Substring(0, 10);
            
            var diferenciaFechas = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion.Date, vContrato.FechaFinalTerminacionContrato.Value, out diferenciaFechas);

            var items1 = diferenciaFechas.Split('|');

            txtPlazoTotEjecDias.Text = items1[2];
            txtPlazoTotEjecMeses.Text = items1[1];
            txtPlazoTotEjecAnios.Text = items1[0];
        }
        else if (vContrato.FechaFinalizacionIniciaContrato != null)
        {
            txtFechaFinalTerminacion.Text = vContrato.FechaFinalizacionIniciaContrato.ToString().Substring(0, 10);

            var diferenciaFechas = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion.Date, caFechaFinalizacionInicial.Date, out diferenciaFechas);

            var items1 = diferenciaFechas.Split('|');

            txtPlazoTotEjecDias.Text = items1[2];
            txtPlazoTotEjecMeses.Text = items1[1];
            txtPlazoTotEjecAnios.Text = items1[0];
        }

        ManejaVigenciasFuturas();
    }

    public void ManejaVigenciasFuturas()
    {
        if (vContrato.ManejaVigenciaFuturas != null)
        {
            HabilitarVigenciaFiscal(true);
            if (vContrato.ManejaVigenciaFuturas == true)
            {
                chkSiManejaVigFuturas.Checked = true;
                chkNoManejaVigFuturas.Checked = false;
            }

            else
            {
                chkSiManejaVigFuturas.Checked = false;
                chkNoManejaVigFuturas.Checked = true;
            }

        }
        else
        {
            HabilitarVigenciaFiscal(false);
        }

        VigenciaFiscal();
    }

    public void VigenciaFiscal()
    {
        string IdVigenciaFiscalInicial = "-1";
        if (vContrato.IdVigenciaInicial != null)
            IdVigenciaFiscalInicial = Convert.ToString(vContrato.IdVigenciaInicial);
        ddlVigenciaFiscalIni.SelectedValue = IdVigenciaFiscalInicial;
        SeleccionaVigenciaFiscalInicial(IdVigenciaFiscalInicial);

        string IdVigenciaFiscalFinal = "-1";
        if (vContrato.IdVigenciaFinal != null)
            IdVigenciaFiscalFinal = Convert.ToString(vContrato.IdVigenciaFinal);
        ddlVigenciaFiscalFin.SelectedValue = IdVigenciaFiscalFinal;
        SeleccionaVigenciaFiscalFinal(IdVigenciaFiscalFinal);


        if (vContrato.IdVigenciaInicial != null && vContrato.IdVigenciaFinal != null)
        {

        }
        else
        {
            HabilitarFormaPago(false);
        }

        FormaPago();
    }

    public void FormaPago()
    {
        string IdFormaPago = "-1";
        if (vContrato.IdFormaPago != null)
        {
            IdFormaPago = Convert.ToString(vContrato.IdFormaPago);
            SeleccionaLugarEjecucion("1");
        }

        LugarEjecucion();
        SeleccionaFormaPago(IdFormaPago);
        TipoFormaPago();
        ddlFormaPago.SelectedValue = IdFormaPago;
    }

    public void TipoFormaPago()
    {
        string IdTipoPago = "-1";
        if (vContrato.IdTipoPago != null)
        {
            ddlTipoFormaPago.SelectedValue = vContrato.IdTipoPago.ToString();
        }
    }

    public void LugarEjecucion()
    {
        #region LugarEjecucion

        if (vContrato.DatosAdicionaleslugarEjecucion != null)
        {
            txtDatosAdicionalesLugarEjecucion.Text = vContrato.DatosAdicionaleslugarEjecucion;
        }
        IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);

        #endregion
    }

    #endregion

    #region DatosGeneralesContrato

    #region ContratoAsociado
    private void SeleccionadoContratoAsociado(string codContratoAsociado)
    {
        switch (codContratoAsociado)
        {
            case CodConvenioMarco:
                chkContratoConvenioAd.Checked = false;
                lbNumConvenioContrato.Text = "Número Convenio Marco Asociado *"; // Pagina 7/11 paso 4.1
                CodContratoAsociadoSeleccionado = CodConvenioMarco;
                HabilitaCategoriaContrato(false); //Se deshabilita la eleccion de la categoria Pagina 11 paso 4.3
                SeleccionadoContratoAsociado(true);
                break;
            case CodContratoConvenioAdhesion:
                chkConvenioMarco.Checked = false;
                lbNumConvenioContrato.Text = "Número Contrato/Convenio Adhesión *"; // Pagina 7/11 paso 4.2
                CodContratoAsociadoSeleccionado = CodContratoConvenioAdhesion;
                HabilitaCategoriaContrato(false); //Se deshabilita la eleccion de la categoria Pagina 11 paso 4.3
                SeleccionadoContratoAsociado(true);
                break;
            default:
                CodContratoAsociadoSeleccionado = string.Empty;
                HabilitaCategoriaContrato(true); // Pagina 11 Paso 6 Se habilita la eleccion de la categoria 
                SeleccionadoContratoAsociado(false); // 
                break;
        }
    }
    /// <summary>
    /// Pagina 11 Paso 6 Habilita Categoria por seleccion de contrato asociado
    /// </summary>
    /// <param name="seleccionado"></param>
    private void SeleccionadoContratoAsociado(bool seleccionado)
    {
        imgNumConvenioContrato.Enabled = seleccionado;
        if (!seleccionado)
        {
            SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue);
            PnValorConvenio.Style.Add("visibility", "collapse");

            hfFechaInicioEjecConvMarco.Value = string.Empty;
            hfFechaFinalTerminContConvMarco.Value = string.Empty;
            hfFechaInicioEjecContConvAd.Value = string.Empty;
            hfFechaFinalTerminContConvAd.Value = string.Empty;

            IdContratoAsociado = string.Empty;
            hfNumConvenioContratoAsociado.Value = string.Empty;
            txtNumConvenioContrato.Text = string.Empty;
        }
        else
        {
            PnValorConvenio.Style.Add("visibility", "");
        }
    }
    #endregion

    /// <summary>
    /// AQUI SE CARGA INFO DEL CONTRATO ASOCIADO
    /// </summary>
    /// <param name="numeroContratoSeleccionado"></param>
    public void SeleccionaNumeroConvenioContrato(string numeroContratoSeleccionado)
    {
        if (!IdContratoAsociado.Equals(string.Empty))
        {
            #region RecuperacionFechasContraAsociadoParaValidaciones
            Contrato vContratoAsociado = new Contrato();
            vContratoAsociado = vContratoService.ContratoObtener(Convert.ToInt32(IdContratoAsociado));

            //if (txtNumConvenioContrato.Text.Equals(string.Empty))
            //{
            hfNumConvenioContratoAsociado.Value = vContratoAsociado.NumeroContrato;
            txtNumConvenioContrato.Text = vContratoAsociado.NumeroContrato;
            //}

            if (chkConvenioMarco.Checked)
            {
                hfObjeto.Value = vContratoAsociado.ObjetoContrato;
                hfAlcance.Value = vContratoAsociado.AlcanceObjetoContrato;

                hfFechaInicioEjecConvMarco.Value = vContratoAsociado.FechaInicioEjecucion.ToString();
                hfFechaFinalTerminContConvMarco.Value = vContratoAsociado.FechaFinalTerminacionContrato.ToString();

                hfFechaInicioEjecContConvAd.Value = string.Empty;
                hfFechaFinalTerminContConvAd.Value = string.Empty;

                //HabilitaCategoriaContrato(true); // ?????? no lo dice en el caso de uso
            }
            else if (chkContratoConvenioAd.Checked)
            {
                hfObjeto.Value = vContratoAsociado.ObjetoContrato;
                hfAlcance.Value = vContratoAsociado.AlcanceObjetoContrato;

                hfFechaInicioEjecContConvAd.Value = vContratoAsociado.FechaInicioEjecucion.ToString();
                hfFechaFinalTerminContConvAd.Value = vContratoAsociado.FechaFinalTerminacionContrato.ToString();

                hfFechaInicioEjecConvMarco.Value = string.Empty;
                hfFechaFinalTerminContConvMarco.Value = string.Empty;
            }
            #endregion
            HabilitaCategoriaContrato(true); // ?????? no lo dice en el caso de uso
        }
        else
        {
            hfNumConvenioContratoAsociado.Value = string.Empty;
            txtNumConvenioContrato.Text = string.Empty;
            hfFechaInicioEjecContConvAd.Value = string.Empty;
            hfFechaFinalTerminContConvAd.Value = string.Empty;
            hfFechaInicioEjecConvMarco.Value = string.Empty;
            hfFechaFinalTerminContConvMarco.Value = string.Empty;
            HabilitaCategoriaContrato(false);
        }
    }

    #region CategoriaContrato

    private void HabilitaCategoriaContrato(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlCategoriaContrato.SelectedIndex = 0;
            SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue);
            //cvCategoriaContrato.Enabled = false;
        }
        else if (habilitar == true)
        {
            ddlCategoriaContrato.Enabled = false;
            //cvCategoriaContrato.Enabled = false;
        }
        else
        {
            if (IdContratoAsociado.Equals(string.Empty))
                ddlCategoriaContrato.Enabled = false;
            //cvCategoriaContrato.Enabled = false;
            //SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue);
        }
    }

    private void ObtenerIdContratoConvenio()
    {
        try
        {
            if (IdContrato.Equals(string.Empty))
            {
                int vResultado;
                Contrato vContratoRegistroInicial = new Contrato();
                vContratoRegistroInicial.UsuarioCrea = GetSessionUser().NombreUsuario;
                vContratoRegistroInicial.IdRegionalContrato = Convert.ToInt32(hfRegional.Value);
                vResultado = vContratoService.ContratoRegistroInicial(vContratoRegistroInicial);

                if (vResultado == 1)
                {
                    IdContrato = Convert.ToString(vContratoRegistroInicial.IdContrato);
                    SetSessionParameter("Contrato.ContratosAPP", IdContrato);
                    //toolBar.LipiarMensajeError();
                }
                else
                {
                    lblError.Text = @"La operación no se completo satisfactoriamente, verifique por favor.";
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = @ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = @ex.Message;
        }
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            ObtenerIdContratoConvenio();

            // Página 1 Descripción adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio
            ddlTipoContratoConvenio.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            switch (CodContratoAsociadoSeleccionado) // Se asigna previamente en SeleccionadoContratoAsociado 
            {
                case CodContratoConvenioAdhesion:
                    #region IdTipoContAporte_CategoriaSeleccionada
                    TipoContrato vTipoContrato = new TipoContrato();
                    vTipoContrato.CodigoTipoContrato = CodTipoContAporte;
                    vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaSeleccionada);
                    String IdTipoContAporte = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
                    #endregion
                    HabilitarTipoContratoConvenio(false);
                    if (ddlTipoContratoConvenio.Items.FindByValue(IdTipoContAporte) != null)
                        ddlTipoContratoConvenio.SelectedValue = IdTipoContAporte; // Página 1 Descripción adicional Tipo Contrato Convenio
                    break;
                default:
                    HabilitarTipoContratoConvenio(true); //Página 11 Paso 9 
                    break;
            }
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            HabilitarTipoContratoConvenio(false);
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
            #endregion
        }

        ConfigurarModalidad(idCategoriaSeleccionada, ddlTipoContratoConvenio.SelectedValue);
    }

    private void HabilitarTipoContratoConvenio(bool habilitar)
    {
        ddlTipoContratoConvenio.Enabled = false;
        if (habilitar)
        {
            //cvTipoContratoConvenio.Enabled = false;
        }
        else
        {
            //cvTipoContratoConvenio.Enabled = false;
            ddlTipoContratoConvenio.SelectedIndex = 0;
            SeleccionadoTipoContratoConvenio(ddlCategoriaContrato.SelectedValue, ddlTipoContratoConvenio.SelectedValue);
        }
    }

    private void SeleccionadoTipoContratoConvenio(string idCategoriaSeleccionada, string idTipoContratoConvenioSeleccionado)
    {
        ConfigurarModalidad(idCategoriaSeleccionada, idTipoContratoConvenioSeleccionado);
    }
    #endregion

    #region Modalidad
    private void ConfigurarModalidad(string idCategoriaSeleccionada, string idTipoContratoConvenioSeleccionado)
    {
        if (idCategoriaSeleccionada.Equals("-1"))
        {
            ddlTipoContratoConvenio.Enabled = false;
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(false);
        }
        else if (idTipoContratoConvenioSeleccionado.Equals("-1"))
        {
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(false);
        }
        else if (idCategoriaSeleccionada.Equals(IdCategoriaContrato) &&
            idTipoContratoConvenioSeleccionado.Equals(IdTipoContConvPrestServApoyoGestion)) // Pagina 2 Descripción adicional modalidad academica
        {
            HabilitarModalidadAcademica(true); // Pagina 11 Paso 12
            HabilitarModalidadSeleccion(false);
        }
        else if (idCategoriaSeleccionada.Equals(IdCategoriaContrato) &&
            idTipoContratoConvenioSeleccionado.Equals(IdTipoContConvPrestServProfesionales)) // Pagina 2 Descripción adicional modalidad academica
        {

            HabilitarModalidadAcademica(true); // Pagina 11 Paso 12
            HabilitarModalidadSeleccion(false);
        }
        // PAgina 12 Paso 12 Si corresponde a otra categoria(Diferente a Contrato) y tipo contrato(Diferente a prestación de serv)
        else if (idCategoriaSeleccionada.Equals(IdCategoriaConvenio) &&
            idTipoContratoConvenioSeleccionado.Equals(IdMarcoInteradministrativo))
        {
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(false);
            ddlModalidadSeleccion.Enabled = false;
            ddlModalidadSeleccion.SelectedValue = IdContratacionDirecta; //Pagina 12 Paso 18.2 Preseleccionar Contratacion Directa
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else
        {
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(true); // Pagina 12 Paso 18.1 
        }
    }

    private void HabilitarModalidadAcademica(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlModalidadAcademica.SelectedIndex = 0;
            ddlModalidadAcademica.Enabled = false;
            HabilitarNombreProfesion(habilitar);
            SeleccionarModalidadAcademica(ddlModalidadAcademica.SelectedValue);
        }
        else if (habilitar == true)
        {
            ddlModalidadAcademica.Enabled = false;
        }
        else
        {
            ddlModalidadAcademica.Enabled = false;
            HabilitarNombreProfesion(habilitar);
            SeleccionarModalidadAcademica(ddlModalidadAcademica.SelectedValue);
        }
    }

    private void HabilitarModalidadSeleccion(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlModalidadSeleccion.Enabled = false;
            ddlModalidadSeleccion.SelectedIndex = 0;
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
            //cvModalidadSeleccion.Enabled = false;
        }
        else if (habilitar == true)
        {
            ddlModalidadSeleccion.Enabled = false;
            //cvModalidadSeleccion.Enabled = false;
        }
        else
        {
            ddlModalidadSeleccion.Enabled = false;
            //cvModalidadSeleccion.Enabled = false;
        }
    }

    private void HabilitarNombreProfesion(bool? habilitar)
    {
        if (habilitar == null)
        {
            lbNombreProfesion.Text = "Nombre de la Profesión";
            ddlNombreProfesion.SelectedIndex = 0;
            SeleccionaProfesion(ddlNombreProfesion.SelectedValue);
            ddlNombreProfesion.Enabled = false;
            //cvNombreProfesion.Enabled = false;
        }
        else if (habilitar == true)
        {
            lbNombreProfesion.Text = "Nombre de la Profesión";
            ddlNombreProfesion.Enabled = false;
            //cvNombreProfesion.Enabled = false;
        }
        else
        {
            lbNombreProfesion.Text = "Nombre de la Profesión";
            ddlNombreProfesion.Enabled = false;
            //cvNombreProfesion.Enabled = false;
        }
    }

    private void SeleccionarModalidadAcademica(string idModalidadAcademicaSeleccionada)
    {
        #region LlenarComboNombreProfesion
        ddlNombreProfesion.Items.Clear();
        List<ProfesionKactus> vLTiposContratos = vContratoService.ConsultarProfesionesKactus(idModalidadAcademicaSeleccionada, null, null, null);
        foreach (ProfesionKactus tD in vLTiposContratos)
        {
            ddlNombreProfesion.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString().Trim()));
        }
        ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        #endregion

        if (idModalidadAcademicaSeleccionada.Equals("-1"))
        {
            HabilitarNombreProfesion(false);
        }
        else
        {
            HabilitarNombreProfesion(true); // Pagina 2 Descripción adicional Nombre de la profesión. Pagina 12 Paso 15 
        }

        //HabilitarModalidadSeleccion(false);
    }

    private void SeleccionaProfesion(string idProfesionSeleccionada)
    {
        if (idProfesionSeleccionada.Equals("-1"))
        {
            if (ddlNombreProfesion.Enabled) //Permite que se de el Paso 18.2 cuando se carga el contrato
                HabilitarModalidadSeleccion(false);
        }
        else
        {
            HabilitarModalidadSeleccion(true);
        }
    }

    /// <summary>
    /// Pagina 3 Descripción adicional Número de Proceso
    /// </summary>
    /// <param name="idModalidadSeleccionSeleccionada"></param>
    private void SeleccionarModalidadSeleccion(string idModalidadSeleccionSeleccionada)
    {
        if (idModalidadSeleccionSeleccionada.Equals("-1"))
        {
            HabilitarNumeroProceso(false);
            HabilitarActaInicio(null);
        }
        else
        {
            ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
            vModalidadSeleccion.IdModalidad = Convert.ToInt32(idModalidadSeleccionSeleccionada);
            string CodModalidadSeleccionSeleccionada = vContratoService.IdentificadorCodigoModalidadSeleccion(vModalidadSeleccion).CodigoModalidad;
            switch (CodModalidadSeleccionSeleccionada)
            {
                case CodContratacionDirecta:
                    HabilitarNumeroProceso(false); // Pagina 12 Paso 20.1
                    HabilitarActaInicio(true); // Pagina 12 Paso 20.2
                    RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
                    break;
                default:
                    HabilitarNumeroProceso(true); // Página 12 Paso 20.1 
                    HabilitarActaInicio(false); // Pagina 12 Paso 20.2 // Se anula porque elimina informacion en la edicion.
                    break;
            }
        }
    }
    #endregion

    private void HabilitarNumeroProceso(bool habilitar)
    {
        if (habilitar)
        {
            PnNumeroProceso.Style.Add("visibility", "");
            caFechaAdjudicaProceso.Visible = true;
            //rfvNumeroProceso.Enabled = false;
            //Se deberia cargar el numero de proceso??
        }
        else
        {
            PnNumeroProceso.Style.Add("visibility", "collapse");
            hfNumeroProceso.Value = string.Empty;
            hfIdNumeroProceso.Value = string.Empty;
            txtNumeroProceso.Text = string.Empty;
            caFechaAdjudicaProceso.Visible = false;
            //rfvNumeroProceso.Enabled = false;
            SeleccionarNumeroProceso(txtNumeroProceso.Text);
        }
    }

    private void SeleccionarNumeroProceso(string numeroProcesoSeleccionado)
    {
        if (numeroProcesoSeleccionado.Equals(string.Empty))
        {
            HabilitarFechaAdjudicacion(false);
        }
        else
        {
            HabilitarFechaAdjudicacion(true); // Pagina 12 Paso 22
        }
    }

    private void HabilitarFechaAdjudicacion(bool habilitar)
    {
        if (habilitar)
        {
            caFechaAdjudicaProceso.Enabled = false;
            caFechaAdjudicaProceso.Requerid = true;
        }
        else
        {
            caFechaAdjudicaProceso.InitNull = true;
            caFechaAdjudicaProceso.Enabled = false;
            caFechaAdjudicaProceso.Requerid = false;
            SeleccionaFechaAdjudicacion(caFechaAdjudicaProceso.Date.ToShortDateString());
        }
    }

    private void SeleccionaFechaAdjudicacion(string fechaAdjudicacion)
    {
        if (PnNumeroProceso.Style["visibility"].Equals(string.Empty))
        {
            if (fechaAdjudicacion.Equals(string.Empty) || fechaAdjudicacion.Substring(0, 10).Equals("01/01/1900"))
            {
                HabilitarActaInicio(false);
            }
            else
            {
                string textValFechaAdjuProc = ValidacionFechaAdjudicacionProceso(caFechaAdjudicaProceso.Date); // Pagina 12 Paso 25 
                if (textValFechaAdjuProc.Equals(string.Empty))
                {
                    HabilitarActaInicio(true); // Pagina 13 Paso 26.1
                    RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
                }
                else
                {
                    HabilitarActaInicio(false);
                    caFechaAdjudicaProceso.InitNull = true;
                    //if (!scriptEnCola)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "valFechaAdjuProc", "alert('" + textValFechaAdjuProc + "')", true);
                    //    scriptEnCola = true;
                    //}
                }
            }
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        var contrato = vContratoService.ContratoObtener(int.Parse(hfIdContrato.Value));

        pageWeb = (GeneralWeb)this.Page;

        if (ValidacionFecha(txtFechaActa.Text) == 1)
        {
            
            pageWeb.MostrarMensajeErrorMasterPrincipal("La fecha de Acta de Inicio no es válida");            
        }
        else
        {

            if ((contrato.FechaInicioEjecucion <= DateTime.Parse(txtFechaActa.Text)) && DateTime.Parse(txtFechaActa.Text) < contrato.FechaFinalizacionIniciaContrato)
            {
                pageWeb.LimpiarMensajeMasterPrincipal();
                contrato.FechaActaInicio = DateTime.Parse(txtFechaActa.Text);
                contrato.FechaInicioEjecucion = DateTime.Parse(txtFechaActa.Text);
                var vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "EJE", null, true);
                contrato.IdEstadoContrato = vEstadoContrato[0].IdEstadoContrato;

                DateTime? fechaFinalTerminacion = null;

                if (contrato.EsFechaFinalCalculada.HasValue && contrato.EsFechaFinalCalculada.Value)
                {
                    fechaFinalTerminacion = ContratoService.CalcularFechaFinalContrato
                        (
                         contrato.FechaInicioEjecucion.Value,
                         contrato.DiasFechaFinalCalculada.Value,
                         contrato.MesesFechaFinalCalculada.Value
                        );

                    contrato.FechaFinalizacionIniciaContrato = fechaFinalTerminacion;
                    contrato.FechaFinalTerminacionContrato = fechaFinalTerminacion;
                }

                vContratoService.ContratoActualizacion(contrato);
                btnActualizar.Visible = false;
                ImageFechaActa.Visible = false;
                txtFechaActa.Enabled = false;
                caFechaInicioEjecucion.Date = (DateTime)contrato.FechaInicioEjecucion;
                pageWeb.MostrarMensajeGuardadoMasterPrincipal("La información ha sido registrada en forma exitosa");
            }
            else
            {
                pageWeb.MostrarMensajeErrorMasterPrincipal("La fecha debe ser mayor o igual a la fecha de inicio de Ejecución y menor a la fecha de terminacion Inicial");
            }
        }
    }

    private string ValidacionFechaAdjudicacionProceso(DateTime? fechaAdjudicacionSeleccionada)
    {
        string textValFechaAdjuProc = string.Empty;

        if (((DateTime)fechaAdjudicacionSeleccionada).Year < (DateTime.Today.Year - 1) || ((DateTime)fechaAdjudicacionSeleccionada).Year > DateTime.Today.Year)
        {
            textValFechaAdjuProc = "El año de la fecha de adjudicación del proceso debe ser menor un año o igual al año de la Fecha actual";
        }

        return textValFechaAdjuProc;
    }

    private void HabilitarActaInicio(bool? habilitar)
    {
        if (habilitar == null)
        {
            PnRequiereActaInicio.Style.Add("visibility", "collapse");
            chkSiReqActa.Checked = false; //.Items[0].Selected = false;
            chkNoReqActa.Checked = false; //chkSiReqActa.Items[1].Selected = false; //
            //cuvReqActa.Enabled = false;
            HabilitarManejaAportes(null);
        }
        else if (habilitar == true)
        {
            PnRequiereActaInicio.Style.Add("visibility", "");
            //cuvReqActa.Enabled = false;
            // (SE REPITE) 
            //RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
        }
        else
        {
            chkSiReqActa.Enabled = false;
            chkNoReqActa.Enabled = false;
            //cuvReqActa.Enabled = false;
            HabilitarManejaAportes(false);
        }
    }

    private void RequiereActaDeInicio(string idTipoContrato, string idCategoriaContrato)
    {
        TipoContrato vTipoContrato = new TipoContrato();
        vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContrato);
        vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
        bool requiereActaInicio = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).ActaInicio; // Pagina 13 Paso 26.2

        if (requiereActaInicio)
        {
            chkSiReqActa.Checked = true; //.SelectedIndex = 0; // Pagina 13 Paso 26.3
            chkSiReqActa.Enabled = false;
            chkNoReqActa.Enabled = false;

            chkNoReqActa.Checked = false;

            PnManejaAportes.Style.Add("visibility", "");
            RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
        }
        else
        {
            chkSiReqActa.Enabled = false; // Pagina 13 Paso 26.4
            chkNoReqActa.Enabled = false;
            if (chkSiReqActa.Checked || chkNoReqActa.Checked)
            {
                HabilitarManejaAportes(true);
                RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
            }
        }
    }

    private void HabilitarManejaAportes(bool? habilitar)
    {
        if (habilitar == null)
        {
            PnManejaAportes.Style.Add("visibility", "collapse");
            chkSiManejaAportes.Checked = false;
            chkNoManejaAportes.Checked = false;
            //cuvManejaAportes.Enabled = false;
            HabilitarManejaRecursos(null);
        }
        else if (habilitar == true)
        {
            PnManejaAportes.Style.Add("visibility", "");
            //cuvManejaAportes.Enabled = false;
            // (SE REPITE) 
            //RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
        }
        else
        {
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;
            //cuvManejaAportes.Enabled = false;
            HabilitarManejaRecursos(false);
        }
    }

    private void RequiereManejoAportes(string idTipoContrato, string idCategoriaContrato)
    {
        TipoContrato vTipoContrato = new TipoContrato();
        vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContrato);
        vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
        bool requiereManejoAportes = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).AporteCofinaciacion; // Pagina 13 Paso 29.2

        if (requiereManejoAportes)
        {
            chkSiManejaAportes.Checked = true; // Pagina 13 Paso 29.3
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkNoManejaAportes.Checked = false;

            PnManejaRecursos.Style.Add("visibility", "");
            RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
        }
        else
        {
            chkSiManejaAportes.Enabled = false; // Pagina 13 Paso 29.4
            chkNoManejaAportes.Enabled = false;
            if (chkSiManejaAportes.Checked || chkNoManejaAportes.Checked)
            {
                HabilitarManejaRecursos(true);
                RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
            }
        }
    }

    private void HabilitarManejaRecursos(bool? habilitar)
    {
        if (habilitar == null)
        {
            PnManejaRecursos.Style.Add("visibility", "collapse");
            chkSiManejaRecursos.Checked = false;
            chkNoManejaRecursos.Checked = false;
            //cuvManejaRecursos.Enabled = false;
            HabilitarRegimenContratacion(null);
        }
        else if (habilitar == true)
        {
            PnManejaRecursos.Style.Add("visibility", "");
            //cuvManejaRecursos.Enabled = false;
            // (SE REPITE) 
            //RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
        }
        else
        {
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;
            //cuvManejaRecursos.Enabled = false;
            HabilitarRegimenContratacion(false);
        }
    }

    private void RequiereManejaRecursos(string idTipoContrato, string idCategoriaContrato)
    {
        TipoContrato vTipoContrato = new TipoContrato();
        vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContrato);
        vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
        bool requiereManejoRecursos = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).RecursoFinanciero; // Pagina 13 Paso 32.2

        if (requiereManejoRecursos)
        {
            chkSiManejaRecursos.Checked = true; // Pagina 13 Paso 32.3
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            chkNoManejaRecursos.Checked = false;

            HabilitarRegimenContratacion(true);
        }
        else
        {
            chkSiManejaRecursos.Enabled = false; // Pagina 13 Paso 32.4
            chkNoManejaRecursos.Enabled = false;
            if (chkSiManejaRecursos.Checked || chkNoManejaRecursos.Checked)
                HabilitarRegimenContratacion(true);
        }
    }

    private void HabilitarRegimenContratacion(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlRegimenContratacion.Enabled = false;
            ddlRegimenContratacion.SelectedIndex = 0;
            //cvRegimenContratacion.Enabled = false;
            HabilitarBusquedaSolicitante(null);
        }
        else if (habilitar == true)
        {
            ddlRegimenContratacion.Enabled = false;
            //cvRegimenContratacion.Enabled = false;
            // (SE REPITE) 
            //SeleccionaRegimenContratacion(ddlRegimenContratacion.SelectedValue);
        }
        else
        {
            ddlRegimenContratacion.Enabled = false;
            ddlRegimenContratacion.Enabled = false;
            HabilitarBusquedaSolicitante(false);
        }
    }

    private void SeleccionaRegimenContratacion(string idRegimenContratacion)
    {
        if (idRegimenContratacion.Equals("-1"))
        {
            HabilitarBusquedaSolicitante(false);
        }
        else
        {
            HabilitarBusquedaSolicitante(true);
        }
    }

    private void HabilitarBusquedaSolicitante(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgNombreSolicitante.Enabled = false;

            hfNombreSolicitante.Value = string.Empty;
            hfRegionalContConv.Value = string.Empty;
            hfDependenciaSolicitante.Value = string.Empty;
            hfCargoSolicitante.Value = string.Empty;
            hfCodRegionalContConv.Value = string.Empty;

            txtNombreSolicitante.Text = string.Empty;
            //txtRegionalContratoConvenio.Text = string.Empty;
            txtDependenciaSolicitante.Text = string.Empty;
            txtCargoSolicitante.Text = string.Empty;

            //rfvNombreSolicitante.Enabled = false;
            //rfvRegionalContConv.Enabled = false;

            HabilitarOrdenadorGasto(null);
            SeleccionarSolicitante(string.Empty);
        }
        else if (habilitar == true)
        {
            imgNombreSolicitante.Enabled = false;
            //rfvNombreSolicitante.Enabled = false;
            //rfvRegionalContConv.Enabled = false;
            if (!txtNombreSolicitante.Text.Equals(string.Empty))
                SeleccionarSolicitante(txtNombreSolicitante.Text); // Puede que no sea necesaria esta linea
        }
        else
        {
            imgNombreSolicitante.Enabled = false;
            //rfvNombreSolicitante.Enabled = false;
            //rfvRegionalContConv.Enabled = false;
            HabilitarOrdenadorGasto(false);

            //SeleccionarSolicitante(string.Empty);
        }
    }

    private void SeleccionarSolicitante(string solicitanteSeleccionado)
    {
        if (solicitanteSeleccionado.Equals(string.Empty))
        {
            HabilitarOrdenadorGasto(false);
        }
        else
        {
            HabilitarOrdenadorGasto(true); // Pagina 14 Paso 42
        }
    }

    private void HabilitarOrdenadorGasto(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgNombreOrdenadorGasto.Enabled = false;

            hfNombreOrdenadorGasto.Value = string.Empty;
            hfTipoIdentOrdenadorGasto.Value = string.Empty;
            hfNumeroIdentOrdenadoGasto.Value = string.Empty;
            hfCargoOrdenadoGasto.Value = string.Empty;

            txtNombreOrdenadorGasto.Text = string.Empty;
            txtTipoIdentOrdenadorGasto.Text = string.Empty;
            txtNumeroIdentOrdenadoGasto.Text = string.Empty;
            txtCargoOrdenadoGasto.Text = string.Empty;

            //rfvNombreOrdenadorGasto.Enabled = false;

            SeleccionarOrdenadorGasto(string.Empty);
        }
        else if (habilitar == true)
        {
            imgNombreOrdenadorGasto.Enabled = false;
            //rfvNombreOrdenadorGasto.Enabled = false;
            if (!txtNombreOrdenadorGasto.Text.Equals(string.Empty))
                SeleccionarOrdenadorGasto(txtNombreOrdenadorGasto.Text); // Puede que no sea necesaria esta linea
        }
        else
        {
            imgNombreOrdenadorGasto.Enabled = false;
            //rfvNombreOrdenadorGasto.Enabled = false;
            SeleccionarOrdenadorGasto(string.Empty);
        }
    }

    private void SeleccionarOrdenadorGasto(string ordenadorGastoSeleccionado)
    {
        if (ordenadorGastoSeleccionado.Equals(string.Empty))
        {
            HabilitarPlanDeCompras(false);
            HabilitarObjetoContratoConvenio(false);
        }
        else
        {
            if (chkSiManejaRecursos.Checked)
            {
                HabilitarPlanDeCompras(true); // Pagina 14 Paso 46
                //SeleccionarPlanCompras("1"); // Carga la información de la sección Plan de Compras
                AcordeonActivo = "2";
                // Muestra Plan de compras acordeon
                //AccContratos.SelectedIndex = 1;
            }
            else
            {
                AcordeonActivo = "3";
                HabilitarPlanDeCompras(null);
                HabilitarObjetoContratoConvenio(true); // Se continua con el paso 55, despues 66 y despues 62
                if (!ddlFormaPago.SelectedValue.Equals("-1"))
                {
                    SeleccionaFormaPago(ddlFormaPago.SelectedValue);
                    SeleccionaLugarEjecucion(string.Empty);
                }
            }
        }
    }

    #endregion

    #region PlanDeCompras

    private void HabilitarPlanDeCompras(bool? habilitar)
    {
        if (habilitar == null)
        {
            //imgPlanCompras.Enabled = false;
            //gvConsecutivos.DataSource = null;
            //gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = "0";
            //gvProductos.DataSource = null;
            //gvProductos.DataBind();
            nProductos = "0";
            gvRubrosPlanCompras.DataSource = null;
            gvRubrosPlanCompras.DataBind();
            HabilitarConsecutivoPlanDeCompras(null);
            //cvPlanCompras.Enabled = false;
            //cvProductos.Enabled = false;
        }
        else if (habilitar == true)
        {
            //imgPlanCompras.Enabled = false;
            //cvPlanCompras.Enabled = false;
            //cvProductos.Enabled = false;
        }
        else
        {
            //imgPlanCompras.Enabled = false;
            //cvPlanCompras.Enabled = false;
            //cvProductos.Enabled = false;
            HabilitarConsecutivoPlanDeCompras(false);
        }
    }

    private void SeleccionarPlanCompras(string idPlanDeComprasSeleccionado)
    {
        if (!idPlanDeComprasSeleccionado.Equals(string.Empty) && !idPlanDeComprasSeleccionado.Equals("-1"))
        {
            //Carga grila Consecutivo Plan de Compras
            List<PlanDeComprasContratos> consecutivosPlanCompras = vContratoService.ConsultarPlanDeComprasContratoss(Convert.ToInt32(IdContrato), null);
            gvConsecutivos.DataSource = consecutivosPlanCompras;
            gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = gvConsecutivos.Rows.Count.ToString();

            if (consecutivosPlanCompras.Count() > 0)
            {
                // Carga grilla Plan de Compras y Rubros con webservice, filtrar por productos registrados en ProductoPlanComprasContrato
                #region CargaGrillaProductosRubros

                decimal? sumatoriaProductos = 0;

                #region ProductosDelPlanComprasPaso1
                List<PlanComprasProductos> productosContratos = new List<PlanComprasProductos>();
                List<PlanComprasRubrosCDP> rubrosContratos = new List<PlanComprasRubrosCDP>();
                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras)
                {
                    foreach (PlanComprasProductos p in vContratoService.ObtenerProductosPlanCompras(pc.IDPlanDeComprasContratos))
                    {
                        productosContratos.Add(p);
                    }

                    foreach (PlanComprasRubrosCDP r in vContratoService.ObtenerRubrosPlanCompras(pc.IDPlanDeComprasContratos))
                    {
                        rubrosContratos.Add(r);
                    }
                }
                #endregion

                var client = new WsContratosPacco.WSContratosPACCOSoapClient();
                List<GetDetalleProductosServicioWSPacco> productos = new List<GetDetalleProductosServicioWSPacco>();
                List<WsContratosPacco.GetDetalleRubrosServicio_Result> rubros = new List<WsContratosPacco.GetDetalleRubrosServicio_Result>();

                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras) //Recorro las vigencias y consecutivos asociados al plan de compras del contrato
                {
                    #region FiltradosProductosWebService

                    var misItemsComprasProductos = client.GetListaDetalleProductoPACCO(pc.Vigencia, pc.IDPlanDeCompras);

                    foreach (WsContratosPacco.GetDetalleProductosServicio_Result pWs in misItemsComprasProductos) //Recorro y obtengo los productos obtenidos del web service
                    {
                        foreach (PlanComprasProductos p in productosContratos) // Recorro los productos obtenidos en ProductosDelPlanComprasPaso1
                        {
                            GetDetalleProductosServicioWSPacco itemToAdd = new GetDetalleProductosServicioWSPacco();

                            if ((p.CodigoProducto == pWs.codigo_producto) && (p.NumeroConsecutivoPlanCompras == pWs.consecutivo)
                            && (p.IdDetalleObjeto == pWs.iddetalleobjetocontractual.ToString()))  // Verifico que los productosContratos coincidan con los productos del webservice
                            {
                                itemToAdd.valor_total = pWs.valor_total;
                                sumatoriaProductos += itemToAdd.valor_total;
                                productos.Add(itemToAdd);
                            }
                        }
                    }
                    #endregion

                    #region FiltradosRubrosWebService
                    foreach (WsContratosPacco.GetDetalleRubrosServicio_Result rWs in client.GetListaDetalleRubroPACCO(pc.Vigencia, pc.IDPlanDeCompras))
                    {
                        foreach (PlanComprasRubrosCDP r in rubrosContratos)
                        {
                            if ((r.CodigoRubro == rWs.codigo_rubro) && (r.NumeroConsecutivoPlanCompras == rWs.consecutivo))
                            {
                                rubros.Add(rWs);
                            }
                        }
                    }
                    #endregion
                }

                TotalProductos = sumatoriaProductos.ToString();

                gvProductos.DataSource = productos;
                gvProductos.DataBind();
                nProductos = gvProductos.Rows.Count.ToString();

                gvRubrosPlanCompras.DataSource = rubros;
                gvRubrosPlanCompras.DataBind();
                #endregion

                #region ListaConsecutivoPlanCompras
                HabilitarConsecutivoPlanDeCompras(true);

                string ConsecutivoPlanComprasSeleccionado = DdlConsecutivoPlanCompras.SelectedValue;
                DdlConsecutivoPlanCompras.Items.Clear();
                foreach (PlanDeComprasContratos tD in consecutivosPlanCompras)
                {
                    DdlConsecutivoPlanCompras.Items.Add(new ListItem(tD.IDPlanDeCompras.ToString(), tD.IDPlanDeComprasContratos.ToString()));
                }
                DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));

                if (DdlConsecutivoPlanCompras.Items.FindByValue(ConsecutivoPlanComprasSeleccionado) != null)
                    DdlConsecutivoPlanCompras.SelectedValue = ConsecutivoPlanComprasSeleccionado;
                #endregion
            }
            else
            {
                TotalProductos = string.Empty;
                HabilitarConsecutivoPlanDeCompras(false);
            }
        }
        else
        {
            TotalProductos = string.Empty;
            HabilitarConsecutivoPlanDeCompras(false);
        }
    }

    private void HabilitarConsecutivoPlanDeCompras(bool? habilitar)
    {
        if (habilitar == null)
        {
            DdlConsecutivoPlanCompras.SelectedIndex = 0;
            DdlConsecutivoPlanCompras.Enabled = false;
            SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);
        }
        else if (habilitar == true)
        {
            DdlConsecutivoPlanCompras.Enabled = false;
        }
        else
        {
            DdlConsecutivoPlanCompras.Enabled = false;
        }
    }

    private void SeleccionaConsecutivoPlanDeCompras(string idConsecutivoPlanDeCompras)
    {
        if (idConsecutivoPlanDeCompras.Equals("-1"))
        {
            //HabilitarCDP(false);
            if (chkSiManejaRecursos.Checked && (Convert.ToInt32(AcordeonActivo) < 3))
                HabilitarObjetoContratoConvenio(false); //Se cambia el flujo por la posición de secciones(Acordeones) definida en el Acta 62
        }
        else
        {
            string textoAlertavalSeleccionaConsecPlanComp = string.Empty;

            ////////// (Se quita por indicación de Clara Perez, comunicado a Jorge Viscaino 02/07/2014) Paso 53 Valida si los tipos de contrato del consecutivo plan de compras vs del contrato coinciden
            ////////if (false)
            ////////{
            ////////    textoAlertavalSeleccionaConsecPlanComp += "Alerta: El Tipo de Contrato/Convenio seleccionado no corresponde con el Plan de Compras seleccionado, Verifique. <br />";
            ////////}

            // Paso 54 Valida si las regionales del consecutivo plan de compras vs del contrato coinciden
            // hfCodRegionalContConv
            if (!vContratoService.ValidacionCoincideRegionalesContratoPlanComprasContrato(Convert.ToInt32(IdContrato), hfCodRegionalContConv.Value))
            {
                textoAlertavalSeleccionaConsecPlanComp += "Alerta: La Regional del Contrato/Convenio no corresponde con el Plan de Compras seleccionado, Verifique";
            }

            if (!textoAlertavalSeleccionaConsecPlanComp.Equals(string.Empty))
            {
                DdlConsecutivoPlanCompras.SelectedIndex = 0;
                textoAlertavalSeleccionaConsecPlanComp = "alert('" + textoAlertavalSeleccionaConsecPlanComp + "')";
                //if (!scriptEnCola)
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "valSeleccionaConsecPlanComp", textoAlertavalSeleccionaConsecPlanComp, true);
                //    scriptEnCola = true;
                //}
            }

            //HabilitarCDP(true); // Pagina 15 Paso 55 , Se cambia el flujo por la posición de secciones(Acordeones) definida en el Acta 62
            AcordeonActivo = "3";
            HabilitarObjetoContratoConvenio(true);
        }
    }

    #endregion

    #region VigenciaValorContrato

    private void HabilitarObjetoContratoConvenio(bool habilitar)
    {
        if (habilitar)
        {
            if (chkContratoConvenioAd.Checked) // Pagina 15 Paso 62
            {
                txtObjeto.Text = hfObjeto.Value;// Precarga info del num contrato convenio adhesion????
                txtObjeto.Enabled = false;
                IngresaObjetoContratoConvenio(txtObjeto.Text);
            }
            else
            {
                if (chkSiManejaRecursos.Checked)
                {
                    if (gvConsecutivos.Rows.Count > 0)
                    {
                        if (chkConvenioMarco.Checked)
                        {
                            // Pagina 15 Paso 63.1
                            txtObjeto.Text = hfObjeto.Value;// Pregarga info del objeto del contrato/convenio del consecutivo plan de compras contratado seleccionado
                            txtObjeto.Enabled = false;
                            IngresaObjetoContratoConvenio(txtObjeto.Text);
                        }
                        else
                        {
                            txtObjeto.Enabled = false;
                        }
                    }
                    else
                    {
                        txtObjeto.Text = string.Empty; //Limpieza
                        txtObjeto.Enabled = false;
                    }
                }
                else
                {
                    // Pagina 16 Paso 63.2
                    txtObjeto.Enabled = false;
                }
            }
        }
        else
        {
            txtObjeto.Text = string.Empty;
            txtObjeto.Enabled = false;
            IngresaObjetoContratoConvenio(string.Empty);
        }
    }

    private void IngresaObjetoContratoConvenio(string objetoContratoConvenio)
    {
        if (objetoContratoConvenio.Equals(string.Empty))
        {
            HabilitaAlcanceObjetoContratoConvenio(false);
        }
        else
        {
            HabilitaAlcanceObjetoContratoConvenio(true);
        }
    }

    private void HabilitaAlcanceObjetoContratoConvenio(bool habilitar)
    {
        if (habilitar)
        {
            if (chkContratoConvenioAd.Checked) // Pagina 16 Paso 66
            {
                if (txtAlcance.Text.Equals(string.Empty))
                    txtAlcance.Text = hfAlcance.Value;// Precarga info del num contrato convenio adhesion????
                txtAlcance.Enabled = false;
                IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
            }

            if (chkSiManejaRecursos.Checked)
            {
                if (chkConvenioMarco.Checked)
                {
                    // Pagina 16 Paso 67.1
                    txtAlcance.Text = hfAlcance.Value; // Pregarga info del alcance objeto del contrato/convenio del consecutivo plan de compras contratado seleccionado
                    //txtAlcance.Enabled = false;
                    IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
                }
            }

            // Pagina 16 Paso 67.2
            txtAlcance.Enabled = false;
        }
        else
        {
            txtAlcance.Text = string.Empty;
            txtAlcance.Enabled = false;
            IngresaAlcanceObjetoContratoConvenio(string.Empty);
        }
    }

    private void IngresaAlcanceObjetoContratoConvenio(string alcanceObjetoContratoConvenio)
    {
        if (alcanceObjetoContratoConvenio.Equals(string.Empty))
        {
            HabilitaValorInicialContConv(false);
        }
        else
        {
            if (chkSiManejaRecursos.Checked)
            {

                if (txtValorInicialContConv.Text.Equals(string.Empty))
                {
                    //try
                    //{
                    NumberFormatInfo nfi = new CultureInfo("en-US", true).NumberFormat;
                    nfi.NumberDecimalDigits = 2;
                    nfi.NumberDecimalSeparator = ",";
                    nfi.NumberGroupSeparator = "";
                    decimal totalProductos = decimal.Parse(TotalProductos);
                    txtValorInicialContConv.Text = totalProductos.ToString("n", nfi);//.ToString("#,###0.00##;(#,###0.00##)"); ; // Pagina 16 paso 70.1
                    //} catch (Exception e)
                    //{

                    //}
                }


                decimal sumatoriaProductos = 0;
                if (!TotalProductos.Equals(string.Empty))
                    sumatoriaProductos = Convert.ToDecimal(TotalProductos);

                if (ValidacionValorInicialContConv(sumatoriaProductos)) // Pagina 16 paso 73 valor inicial no puede superar el 0.0025 del total de los productos
                {
                    HabilitaValorInicialContConv(true);
                    HabilitaFechaInicioEjecucionContConv(true);
                }
                else
                {
                    string textoAlertavalInicialContConv = string.Empty;
                    textoAlertavalInicialContConv = "El Valor Inicial del Contrato/Convenio no puede superar el 0.0025% del Valor Total de los Productos";
                    textoAlertavalInicialContConv = "alert('" + textoAlertavalInicialContConv + "')";
                    //if (!scriptEnCola)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "valInicialContConv", textoAlertavalInicialContConv, true);
                    //    scriptEnCola = true;
                    //}
                }
            }
            else
            {
                //txtValorInicialContConv.Text = string.Empty;
                HabilitaValorInicialContConv(true); // Pagina 16 70.2
            }
        }
    }

    private void HabilitaValorInicialContConv(bool habilitar)
    {
        if (habilitar)
        {
            txtValorInicialContConv.Enabled = false;
        }
        else
        {
            txtValorInicialContConv.Text = string.Empty;
            txtValorInicialContConv.Enabled = false;
            IngresaValorInicialContConv(string.Empty);
        }
    }

    private bool ValidacionValorInicialContConv(decimal sumatoriaProductos)
    {
        // desarrollo validacion Paso 73 el valor inicial no puede superar el 0.0025 del total de los productos
        decimal tope = sumatoriaProductos + (sumatoriaProductos * Convert.ToDecimal(0.000025));
        decimal valorInicial = 0;
        if (!txtValorInicialContConv.Text.Equals(string.Empty))
        {
            //NumberFormatInfo nfi = new CultureInfo("en-US", true).NumberFormat;
            //nfi.NumberDecimalDigits = 2;
            //nfi.NumberDecimalSeparator = ",";
            //nfi.NumberGroupSeparator = "";
            valorInicial = decimal.Parse(txtValorInicialContConv.Text);
            //valorInicial = Convert.ToDecimal(txtValorInicialContConv.Text, new CultureInfo("en-US"));
        }

        if (valorInicial > tope)
            return false;
        else
            return true;
    }

    private void IngresaValorInicialContConv(string valorInicialContConv)
    {
        if (valorInicialContConv.Equals(string.Empty))
        {
            HabilitaFechaInicioEjecucionContConv(false);
        }
        else
        {
            string textoAlertavalInicialContConv = string.Empty;

            if (chkSiManejaRecursos.Checked)
            {
                if (!ValidacionValorInicialContConv(decimal.Parse(valorInicialContConv))) // Pagina 16 paso 73
                {
                    textoAlertavalInicialContConv = "El Valor Inicial del Contrato/Convenio no puede superar el 0.0025% del Valor Total de los Productos\\n";
                }
            }

            if (chkConvenioMarco.Checked) // Pagina 17 Paso 74
            {
                if (!ValidacionValorInicialSuperaValorFinal(decimal.Parse(valorInicialContConv)))
                {
                    textoAlertavalInicialContConv += "El Valor Inicial Contrato/Convenio supera el Valor Final del Contrato/Convenio del Convenio Marco Asociado\\n";
                }
            }

            if (textoAlertavalInicialContConv.Equals(string.Empty))
            {
                HabilitaFechaInicioEjecucionContConv(true);
            }
            else
            {
                //txtValorInicialContConv.Text = string.Empty;
                textoAlertavalInicialContConv = "alert('" + textoAlertavalInicialContConv + "')";
                //if (!scriptEnCola)
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "valInicialContConv", textoAlertavalInicialContConv, true);
                //    scriptEnCola = true;
                //}
                HabilitaFechaInicioEjecucionContConv(false);
            }

        }
    }

    private bool ValidacionValorInicialSuperaValorFinal(decimal valorInicialContConv)
    {
        if (!IdContratoAsociado.Equals(string.Empty))
        {
            int idContratoConvMarco = Convert.ToInt32(IdContratoAsociado);
            return vContratoService.ValidacionValorInicialSuperaValorFinal(idContratoConvMarco, valorInicialContConv, Convert.ToInt32(IdContrato)); // validación paso 74 
        }
        else
        {
            return false;
        }
    }

    private void HabilitaFechaInicioEjecucionContConv(bool habilitar)
    {
        if (habilitar)
        {
            caFechaInicioEjecucion.Enabled = false;
        }
        else
        {
            caFechaInicioEjecucion.Enabled = false;
            //caFechaInicioEjecucion.InitNull = true;
            SeleccionaFechaInicioEjecucion(caFechaInicioEjecucion.Date.ToShortDateString());
        }
    }

    private void SeleccionaFechaInicioEjecucion(string fechaInicioEjecucionSeleccionada)
    {

        //if (fechaInicioEjecucionSeleccionada.Equals(string.Empty) || fechaInicioEjecucionSeleccionada.Substring(0, 10).Equals("01/01/1900"))
        if (fechaInicioEjecucionSeleccionada.Equals(string.Empty))
        {
            HabilitaFechaFinalizacionInicial(false);
        }
        else
        {
            if (Convert.ToDateTime(fechaInicioEjecucionSeleccionada) == Convert.ToDateTime("01/01/1900"))
            {
                HabilitaFechaFinalizacionInicial(false);
            }
            else
            {

                string textValFechaInicioEjec = ValidacionesFechaInicioEjecucion(caFechaInicioEjecucion.Date).ToString();
                if (textValFechaInicioEjec.Equals(string.Empty))
                {
                    HabilitaFechaFinalizacionInicial(true); // Pagina 18 Paso 84
                }
                else
                {
                    HabilitaFechaFinalizacionInicial(false);
                    caFechaInicioEjecucion.InitNull = true;
                    //if (!scriptEnCola)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "valFechaIniEjec", "alert('" + textValFechaInicioEjec + "')", true);
                    //    scriptEnCola = true;
                    //}
                }
            }
        }
    }

    private string ValidacionesFechaInicioEjecucion(DateTime? fechaInicioEjecucionSeleccionada)
    {
        string textValFechaInicioEjec = string.Empty;
        DateTime unAcnooMayorHoy = new DateTime(DateTime.Today.Year + 10, DateTime.Today.Month, DateTime.Today.Day);
        DateTime unAcnoMenorHoy = new DateTime(DateTime.Today.Year - 10, DateTime.Today.Month, DateTime.Today.Day);

        if (fechaInicioEjecucionSeleccionada < unAcnoMenorHoy || fechaInicioEjecucionSeleccionada > unAcnooMayorHoy) // Paso 78
        {
            textValFechaInicioEjec = "La fecha ingresada debe estar dentro del rango de un año menos y un año más del año actual\\n";
        }

        if (!txtNumeroProceso.Text.Equals(string.Empty)) // Paso 79
        {
            DateTime fechaAdjudicacionProceso = caFechaAdjudicaProceso.Date;
            if (fechaInicioEjecucionSeleccionada < fechaAdjudicacionProceso)
            {
                textValFechaInicioEjec += "La fecha ingresada debe ser mayor o igual a la Fecha de Adjudicación del Proceso\\n";
            }
        }

        if (chkConvenioMarco.Checked) // Paso 80 y 81
        {
            DateTime fechaInicioEjecConvMarco = Convert.ToDateTime(hfFechaInicioEjecConvMarco.Value);
            if (fechaInicioEjecucionSeleccionada < fechaInicioEjecConvMarco)
            {
                textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser mayor o igual a la Fecha de Inicio de Ejecución del Convenio Marco\\n";
            }

            DateTime fechaFinalTermContConvMarco = Convert.ToDateTime(hfFechaFinalTerminContConvMarco.Value);
            if (fechaInicioEjecucionSeleccionada > fechaFinalTermContConvMarco)
            {
                textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Convenio Marco\\n";
            }
        }
        else if (chkContratoConvenioAd.Checked) // Paso 82 y 83
        {
            DateTime fechaInicioEjecContConvAd = Convert.ToDateTime(hfFechaInicioEjecContConvAd.Value);
            if (fechaInicioEjecucionSeleccionada < fechaInicioEjecContConvAd)
            {
                textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser mayor o igual a la Fecha de Inicio de Ejecución del Contrato/Convenio Adhesión\\n";
            }

            DateTime fechaFinalTerminContConvAd = Convert.ToDateTime(hfFechaFinalTerminContConvAd.Value);
            if (fechaInicioEjecucionSeleccionada > fechaFinalTerminContConvAd)
            {
                textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Contrato/Convenio Adhesión\\n";
            }
        }

        return textValFechaInicioEjec;
    }

    private void HabilitaFechaFinalizacionInicial(bool habilitar)
    {
        if (habilitar)
        {
            caFechaFinalizacionInicial.Enabled = false;
        }
        else
        {
            txtFechaFinalTerminacion.Text = string.Empty;
            caFechaFinalizacionInicial.Enabled = false;
            caFechaFinalizacionInicial.InitNull = true;
            SeleccionaFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date.ToShortDateString());
        }
    }

    private void SeleccionaFechaFinalizacionInicialContConv(string fechaFinalizacionInicialContConvSeleccionada)
    {
        if (fechaFinalizacionInicialContConvSeleccionada.Equals(string.Empty))
        {
            //LimpiarPlazoInicialEjecucion();
            HabilitarVigenciasFuturas(false);
        }
        else
        {
            if (Convert.ToDateTime(fechaFinalizacionInicialContConvSeleccionada) == Convert.ToDateTime("01/01/1900"))
            {
                HabilitarVigenciasFuturas(false);
            }
            else
            {
                string textValFechaFinalIniciContConv = ValidacionesFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date);

                if (textValFechaFinalIniciContConv.Equals(string.Empty))
                {
                    // Paso 91
                    if (!chkCalculaFecha.Checked)
                    {
                        string diferenciaFechas = string.Empty;
                        ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion.Date, caFechaFinalizacionInicial.Date, out diferenciaFechas);

                        var items = diferenciaFechas.Split('|');

                        txtDiasPiEj.Text = items[2];
                        txtMesesPiEj.Text = items[1];
                        txtAcnosPiEj.Text = items[0];
                    }

                    HabilitarVigenciasFuturas(true);
                }
                else
                {
                    LimpiarPlazoInicialEjecucion();
                    HabilitarVigenciasFuturas(false);
                    caFechaFinalizacionInicial.InitNull = true;
                    if (!scriptEnCola)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "valFechaFinIniContConv",
                            "alert('" + textValFechaFinalIniciContConv + "')", true);
                        scriptEnCola = true;
                    }
                }
            }
        }
    }

    private string ValidacionesFechaFinalizacionInicialContConv(DateTime fechaFinalizacionInicialContConvSeleccionada)
    {
        string textValFechaFinalIniciContConv = string.Empty;

        if (fechaFinalizacionInicialContConvSeleccionada < caFechaInicioEjecucion.Date)
        {
            textValFechaFinalIniciContConv += "La fecha de Finalización Inicial deb ser mayor o igual a la Fecha de Inicio de Ejecución\\n";
        }

        if (chkConvenioMarco.Checked) // Paso 88
        {
            DateTime fechaFinalTermContConvMarco = Convert.ToDateTime(hfFechaFinalTerminContConvMarco.Value);
            if (fechaFinalizacionInicialContConvSeleccionada > fechaFinalTermContConvMarco)
            {
                textValFechaFinalIniciContConv += "La Fecha de Finalización Inicial del contrato/convenio debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Convenio Marco\\n";
            }
        }
        else if (chkContratoConvenioAd.Checked) // Paso 89
        {
            DateTime fechaFinalTerminContConvAd = Convert.ToDateTime(hfFechaFinalTerminContConvAd.Value);
            if (fechaFinalizacionInicialContConvSeleccionada > fechaFinalTerminContConvAd)
            {
                textValFechaFinalIniciContConv += "La fecha de Finalización Inicial del contrato/convenio debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Contrato/Convenio Adhesión\\n";
            }
        }

        return textValFechaFinalIniciContConv;
    }

    private void LimpiarPlazoInicialEjecucion()
    {
        txtFechaFinalTerminacion.Text = string.Empty;
        txtDiasPiEj.Text = string.Empty;
        txtMesesPiEj.Text = string.Empty;
        txtAcnosPiEj.Text = string.Empty;
    }

    private void HabilitarVigenciasFuturas(bool habilitar)
    {
        if (habilitar)
        {
            chkSiManejaVigFuturas.Enabled = false;
            chkNoManejaVigFuturas.Enabled = false;
            //cvManejaVigFut.Enabled = false;
        }
        else
        {
            chkSiManejaVigFuturas.Checked = false;
            chkSiManejaVigFuturas.Enabled = false;
            chkNoManejaVigFuturas.Checked = false;
            chkNoManejaVigFuturas.Enabled = false;
            //cvManejaVigFut.Enabled = false;
            HabilitarVigenciaFiscal(false);
            HabilitarSeccionVigenciaFuturas(false);
        }
    }

    private void HabilitarVigenciaFiscal(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlVigenciaFiscalIni.Enabled = false;
            ddlVigenciaFiscalFin.Enabled = false;
            ddlVigenciaFiscalIni.SelectedIndex = 0;
            ddlVigenciaFiscalFin.SelectedIndex = 0;
            //cvVigenciaFiscalIni.Enabled = false;
            //cvVigenciaFiscalFin.Enabled = false;
            SeleccionaVigenciaFiscalInicial(ddlVigenciaFiscalIni.SelectedValue);
            SeleccionaVigenciaFiscalFinal(ddlVigenciaFiscalFin.SelectedValue);
        }
        else if (habilitar == true)
        {
            ddlVigenciaFiscalIni.Enabled = false;
            ddlVigenciaFiscalFin.Enabled = false;
            //cvVigenciaFiscalIni.Enabled = false;
            //cvVigenciaFiscalFin.Enabled = false;
        }
        else
        {
            ddlVigenciaFiscalIni.Enabled = false;
            ddlVigenciaFiscalFin.Enabled = false;
            //cvVigenciaFiscalIni.Enabled = false;
            //cvVigenciaFiscalFin.Enabled = false;
        }
    }

    private void SeleccionaVigenciaFiscalInicial(string vigenciaFiscalInicialSeleccionada)
    {
        if (!vigenciaFiscalInicialSeleccionada.Equals("-1"))
        {
            if (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) < caFechaInicioEjecucion.Date.Year ||
                (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) > (caFechaInicioEjecucion.Date.Year + 1))) // Paso 98
            {
                ddlVigenciaFiscalIni.SelectedIndex = 0;
                string textValVigeFiscIni = "La Vigencia Fiscal Inicial del Contrato/Convenio debe ser Mayor o Igual al año de la Fecha de Inicio de Ejecución del contrato/Convenio";
                textValVigeFiscIni = "mensajeAlerta('" + ddlVigenciaFiscalIni.ClientID + "', '" + textValVigeFiscIni + "')";
                //if (!scriptEnCola)
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "valVigeFiscIni", textValVigeFiscIni, true);
                //    scriptEnCola = true;
                //}
            }
        }
        else
        {
            HabilitarFormaPago(false);
        }
    }

    private void SeleccionaVigenciaFiscalFinal(string vigenciaFiscalFinalSeleccionada)
    {
        if (!vigenciaFiscalFinalSeleccionada.Equals("-1") && !ddlVigenciaFiscalIni.SelectedValue.Equals("-1"))
        {
            if (chkSiManejaVigFuturas.Checked)
            {
                if (int.Parse(ddlVigenciaFiscalFin.SelectedItem.Text) <= int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text)) // Paso 101
                {
                    HabilitarFormaPago(false);
                    ddlVigenciaFiscalFin.SelectedIndex = 0;
                    string textValVigeFiscFin = "La Vigencia Fiscal Final del Contrato/Convenio debe ser Mayor a la Vigencia Fiscal Final del Contrato/Convenio";
                    textValVigeFiscFin = "mensajeAlerta('" + ddlVigenciaFiscalIni.ClientID + "', '" + textValVigeFiscFin + "')";
                    //if (!scriptEnCola)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "valVigeFiscIni", textValVigeFiscFin, true);
                    //    scriptEnCola = true;
                    //}
                }
                else
                {
                    HabilitarFormaPago(true); // Paso 103
                }
            }
            else
            {
                HabilitarFormaPago(true); // ??????
            }
        }
        else
        {
            HabilitarFormaPago(false);
        }
    }

    private void HabilitarFormaPago(bool habilitar)
    {
        if (habilitar)
        {
            ddlFormaPago.Enabled = false;
            //cvFormaPago.Enabled = false;
        }
        else
        {
            ddlFormaPago.Enabled = false;
            //cvFormaPago.Enabled = false;
            ddlFormaPago.SelectedIndex = 0;
            SeleccionaFormaPago(ddlFormaPago.SelectedValue);

            ddlTipoFormaPago.Enabled = false;
            //cvFormaPago.Enabled = false;
            ddlTipoFormaPago.SelectedIndex = 0;
            SeleccionaFormaPago(ddlTipoFormaPago.SelectedValue);
        }
    }

    private void SeleccionaFormaPago(string formaPagoSeleccionada)
    {
        if (formaPagoSeleccionada.Equals("-1"))
        {
            HabilitarAportes(false);
            HabilitarLugarEjecucion(false);
        }
        else
        {
            if (chkSiManejaAportes.Checked) // Página 21 Paso 122
            {
                AcordeonActivo = "4";
                HabilitarAportes(true);
                SeleccionarAportes(string.Empty);
            }
            else //if (chkNoManejaAportes.Checked)
            {
                AcordeonActivo = "5";
                HabilitarAportes(false);
                HabilitarLugarEjecucion(true);// Pagina 20 Paso 106
            }
        }
    }

    #endregion

    #region Aportes

    private void HabilitarAportes(bool habilitar)
    {
        if (habilitar)
        {
            imgValorApoICBF.Enabled = false;
            imgValorApoContrat.Enabled = false;
            //cvAportes.Enabled = false;
        }
        else
        {
            imgValorApoICBF.Enabled = false;
            imgValorApoContrat.Enabled = false;
            txtValorApoICBF.Text = string.Empty;
            txtValorApoContrat.Text = string.Empty;
            //gvAportesICBF.DataSource = null;
            //gvAportesICBF.DataBind();
            //gvAportesContratista.DataSource = null;
            //gvAportesContratista.DataBind();
            nAportes = "0";
            //cvAportes.Enabled = false;
        }
    }

    private void SeleccionarAportes(string idAporteSeleccionado)
    {
        CargarAportes(IdContrato);
    }

    private void CargarAportes(string IdContrato)
    {
        //int numAportes = 0;

        //List<AporteContrato> aportes = vContratoService.ObtenerAportesContrato(true, Convert.ToInt32(IdContrato), null, null);
        //gvAportesICBF.DataSource = aportes;
        //gvAportesICBF.DataBind();

        //txtValorApoICBF.Text = Convert.ToString(aportes.Sum(a => a.ValorAporte));

        //numAportes += aportes.Count();

        //aportes = vContratoService.ObtenerAportesContrato(false, Convert.ToInt32(IdContrato), null, null);
        //gvAportesContratista.DataSource = aportes;
        //gvAportesContratista.DataBind();

        //txtValorApoContrat.Text = Convert.ToString(aportes.Sum(a => a.ValorAporte));

        //numAportes += aportes.Count();
        //nAportes = numAportes.ToString();

        //if (numAportes > 0)
        //{
        //    SeleccionaLugarEjecucion(string.Empty);
        //}
        //else
        //{
        //    HabilitarLugarEjecucion(false);
        //}
    }

    private void EliminarAporteICBF(int rowIndex)
    {
        //AporteContrato vAporteContrato = new AporteContrato();
        //vAporteContrato.IdAporteContrato = Convert.ToInt32(gvAportesICBF.DataKeys[rowIndex]["IdAporteContrato"]);
        //vContratoService.EliminarAporteContrato(vAporteContrato);
        //SeleccionarAportes(string.Empty);
    }

    private void EliminarAporteContratista(int rowIndex)
    {
        //AporteContrato vAporteContrato = new AporteContrato();
        //vAporteContrato.IdAporteContrato = Convert.ToInt32(gvAportesContratista.DataKeys[rowIndex]["IdAporteContrato"]);
        //vContratoService.EliminarAporteContrato(vAporteContrato);
        //SeleccionarAportes(string.Empty);
    }

    #endregion

    #region LugarEjecucion

    private void HabilitarLugarEjecucion(bool habilitar)
    {
        if (habilitar)
        {
            imgLugarEjecucion.Enabled = false;
            //cvLugarEjecucion.Enabled = false;
        }
        else
        {
            imgLugarEjecucion.Enabled = false;
            //gvLugaresEjecucion.DataSource = null;
            //gvLugaresEjecucion.DataBind();
            nLugarEjecucion = "0";
            dvLugaresEjecucion.Style.Remove("height");
            dvLugaresEjecucion.Style.Remove("overflow-x");
            dvLugaresEjecucion.Style.Remove("overflow-y");
            HabilitarNivelNacional(false);
            HabilitarDatosAdicionalesLugarEjecucion(false);
            //cvLugarEjecucion.Enabled = false;
        }
    }

    private void EliminarLugarEjecucion(int rowIndex)
    {
        //LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
        //vLugarEjecucionContrato.IdLugarEjecucionContratos = Convert.ToInt32(gvLugaresEjecucion.DataKeys[rowIndex]["IdLugarEjecucionContratos"]);
        //vContratoService.EliminarLugarEjecucionContrato(vLugarEjecucionContrato);
        //SeleccionaLugarEjecucion(string.Empty);
    }

    private void SeleccionaLugarEjecucion(string LugarSeleccionado)
    {
        List<LugarEjecucionContrato> lugaresContrato = vContratoService.ConsultarLugaresEjecucionContrato(Convert.ToInt32(IdContrato)).Where(X => X.Historico != true).ToList();
        gvLugaresEjecucion.DataSource = lugaresContrato;
        gvLugaresEjecucion.DataBind();
        nLugarEjecucion = gvLugaresEjecucion.Rows.Count.ToString();

        dvLugaresEjecucion.Style.Remove("height");
        dvLugaresEjecucion.Style.Remove("overflow-x");
        dvLugaresEjecucion.Style.Remove("overflow-y");

        if (lugaresContrato.Any(x=>x.NivelNacional == true) || lugaresContrato.Any(x => x.EsNivelNacional == true))
        {
            SeleccionarNivelNacional(true);
            // HabilitarNivelNacional(true);
           // ibtnLugarEjec.Enabled = false;
        }
        else
        {
            if (lugaresContrato.Count() > 2)
            {
                dvLugaresEjecucion.Style.Add("height", "100px");
                dvLugaresEjecucion.Style.Add("overflow-x", "hidden");
                dvLugaresEjecucion.Style.Add("overflow-y", "scroll");
            }

            HabilitarNivelNacional(false);
            //SeleccionarNivelNacional(false); // Se comenta por que sobra
            HabilitarDatosAdicionalesLugarEjecucion(true);
        }
    }

    private void HabilitarNivelNacional(bool habilitar)
    {
        if (habilitar)
        {
            chkNivelNacional.Enabled = false;
        }
        else
        {
            chkNivelNacional.Enabled = false;
        }
    }

    private void SeleccionarNivelNacional(bool seleccionaNivelNacional)
    {
        if ((bool)seleccionaNivelNacional)
        {
            chkNivelNacional.Checked = true;
            HabilitarLugarEjecucion(false);
            HabilitarDatosAdicionalesLugarEjecucion(true);
        }
        else
        {
            HabilitarLugarEjecucion(true);
            HabilitarDatosAdicionalesLugarEjecucion(false);
        }
    }

    private void HabilitarDatosAdicionalesLugarEjecucion(bool? habilitar)
    {
        if (habilitar == null)
        {
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            txtDatosAdicionalesLugarEjecucion.Text = string.Empty;
            //rfvDatosAdicionales.Enabled = false;
            IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
        }
        else if (habilitar == true)
        {
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            //rfvDatosAdicionales.Enabled = false;
        }
        else
        {
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            //rfvDatosAdicionales.Enabled = false;
            //IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
        }
    }

    private void IngresaDatosAdicionalesLugarEjecucion(string datosAdicionalesLugarEjecucionIngresados)
    {
        if (datosAdicionalesLugarEjecucionIngresados.Equals(string.Empty))
        {
            HabilitarContratista(false);
        }
        else
        {
            AcordeonActivo = "6";
            HabilitarContratista(true);
            SeleccionarContratistas(string.Empty);
        }

    }

    #endregion

    #region Contratista

    private void HabilitarContratista(bool habilitar)
    {
        if (habilitar)
        {
            imgContratista.Enabled = false;
            //cvContratistas.Enabled = false;
        }
        else
        {
            imgContratista.Enabled = false;
            //gvContratistas.DataSource = null;
            //gvContratistas.DataBind();
            nContratistas = "0";
            HabilitarSupervisorInterventor(false);
            gvIntegrantesConsorcio_UnionTemp.DataSource = null;
            gvIntegrantesConsorcio_UnionTemp.DataBind();
            //cvContratistas.Enabled = false;
        }
    }

    private void SeleccionarContratistas(string contratistaSeleccionado)
    {
        //List<Proveedores_Contratos> contratistas = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(IdContrato), null);
        //gvContratistas.DataSource = contratistas;
        //gvContratistas.DataBind();
        //nContratistas = gvContratistas.Rows.Count.ToString();

        //List<Proveedores_Contratos> integrantesConsorcioUnionTemp = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(IdContrato), true);
        //gvIntegrantesConsorcio_UnionTemp.DataSource = integrantesConsorcioUnionTemp;
        //gvIntegrantesConsorcio_UnionTemp.DataBind();

        //if (contratistas.Count() == 0)
        //{
        //    HabilitarSupervisorInterventor(false);
        //}
        //else
        //{
        //    AcordeonActivo = "7";
        //    HabilitarSupervisorInterventor(true);
        //    SeleccionarSupervisorInterventor(string.Empty);
        //}
    }

    private void EliminarContratista(int rowIndex)
    {
        Proveedores_Contratos vProveedor_Contrato = new Proveedores_Contratos();
        vProveedor_Contrato.IdProveedoresContratos = Convert.ToInt32(gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"]);
        vContratoService.EliminarProveedores_Contratos(vProveedor_Contrato);
        SeleccionarContratistas(string.Empty);
    }

    #endregion

    #region SupervisorInterventor

    private void HabilitarSupervisorInterventor(bool habilitar)
    {
        if (habilitar)
        {
            imgSuperInterv.Enabled = false;
            //cvSupervInterv.Enabled = false;
        }
        else
        {
            imgSuperInterv.Enabled = false;
            //gvSupervInterv.DataSource = null;
            //gvSupervInterv.DataBind();
            nSupervInterv = "0";
            HabilitarCDP(false);
            gvDirectoresInterv.DataSource = null;
            gvDirectoresInterv.DataBind();
            //cvSupervInterv.Enabled = false;
        }
    }

    private void SeleccionarSupervisorInterventor(string supervisorInterventorSeleccionado)
    {
        //List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContrato), null);
        //gvSupervInterv.DataSource = supervisoresInterventores;
        //gvSupervInterv.DataBind();

        //if (chkContratoConvenioAd.Checked)
        //{
        //    if ((NoPrecargarInformacion == 0) && (supervisoresInterventores.Count() == 0))
        //    {
        //        PrecargaSupervInterContraConvAdhesion();// Pagina 21 Paso 128
        //    }
        //}

        nSupervInterv = gvSupervInterv.Rows.Count.ToString();

        List<SupervisorInterContrato> directoresInterventoria = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContrato), true);
        gvDirectoresInterv.DataSource = directoresInterventoria;
        gvDirectoresInterv.DataBind();

        FlujoPosteriorCargaSupervInterv(gvSupervInterv.Rows.Count);
    }

    private void PrecargaSupervInterContraConvAdhesion()
    {
        List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContratoAsociado), null);
        foreach (SupervisorInterContrato sic in supervisoresInterventores)
        {
            sic.IDSupervisorIntervContrato = 0;
            sic.IdContrato = Convert.ToInt32(IdContrato);
            vContratoService.InsertarSupervisorInterContrato(sic);
        }

        supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContrato), null);
        gvSupervInterv.DataSource = supervisoresInterventores;
        gvSupervInterv.DataBind();
    }

    private void FlujoPosteriorCargaSupervInterv(int numeroSupervisoresInterventores)
    {
        //dvSupervInterv.Style.Remove("overflow-x");
        //dvSupervInterv.Style.Remove("width");
        //dvSupervInterv.Style.Remove("height");
        //dvSupervInterv.Style.Remove("overflow-y");

        if (numeroSupervisoresInterventores == 0)
        {
            HabilitarCDP(false);
        }
        else
        {
            if (numeroSupervisoresInterventores > 2)
            {
                //dvSupervInterv.Style.Add("overflow-x", "scroll");
                //dvSupervInterv.Style.Add("width", "80%");
                //dvSupervInterv.Style.Add("height", "200px");
                //dvSupervInterv.Style.Add("overflow-y", "scroll");
            }

            if (chkSiManejaRecursos.Checked)
            {
                AcordeonActivo = "8";
                HabilitarCDP(true);
                //SeleccionarCDP(string.Empty);
            }
            else
            {
                HabilitarCDP(false);

                if (chkSiManejaVigFuturas.Checked)
                {
                    AcordeonActivo = "9";
                    HabilitarSeccionVigenciaFuturas(true);
                    SeleccionarVigenciaFutura(string.Empty);
                }
                else
                {
                    HabilitarSeccionVigenciaFuturas(false);
                    //toolBar.MostrarBotonAprobar(true);
                    //// FIN HABILITAR BOTON GUARDAR FINAL
                }
            }
        }
    }

    private void EliminarSupervInterv(int rowIndex)
    {
        //SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
        //vSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(gvSupervInterv.DataKeys[rowIndex]["IDSupervisorIntervContrato"]);
        //vContratoService.EliminarSupervisorInterContrato(vSupervisorInterContrato);
        //NoPrecargarInformacion = 1;
        //SeleccionarSupervisorInterventor(string.Empty);
    }

    #endregion

    #region CDP

    private void HabilitarCDP(bool habilitar)
    {
        //if (habilitar)
        //{
        //    imgCDP.Enabled = false;
        //    //cvCDP.Enabled = false;
        //}
        //else
        //{
        //    imgCDP.Enabled = false;
        //    //gvCDP.DataSource = null;
        //    //gvCDP.DataBind();
        //    nCDP = "0";
        //    HabilitarSeccionVigenciaFuturas(false);
        //    //cvCDP.Enabled = false;
        //}
    }

    private void SeleccionarCDP(string CDPseleccionado)
    {
        List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDP(Convert.ToInt32(IdContrato));
        gvCDP.DataSource = cdps;
        gvCDP.DataBind();
        nCDP = gvCDP.Rows.Count.ToString();

        if (gvCDP.Rows.Count == 0)
        {
            HabilitarSeccionVigenciaFuturas(false);
        }
        else
        {
            // Paso 56 Valida rubros del CDP vs Plan de compras
            string textoAlertavalValidacionesCDP = ValidacionesCDP();
            if (textoAlertavalValidacionesCDP.Equals(string.Empty))
            {
                if (chkSiManejaVigFuturas.Checked)
                {
                    AcordeonActivo = "9";
                    HabilitarSeccionVigenciaFuturas(true);
                    SeleccionarVigenciaFutura(string.Empty);
                }
                else
                {
                    HabilitarSeccionVigenciaFuturas(false);
                    //// FIN HABILITAR BOTON GUARDAR FINAL
                }
            }
            else
            {
                textoAlertavalValidacionesCDP = "alert('" + textoAlertavalValidacionesCDP + "')";
                //if (!scriptEnCola)
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "valValidacionesCDP", textoAlertavalValidacionesCDP, true);
                //    scriptEnCola = true;
                //}
                HabilitarSeccionVigenciaFuturas(false);
            }

        }
    }

    private string ValidacionesCDP()
    {
        string textoAlertavalValidacionesCDP = string.Empty;
        // (FALTA) Validaciones rubros Paso 61 y 62
        return textoAlertavalValidacionesCDP;
    }

    private void EliminarCDP(int rowIndex)
    {
        ContratosCDP vContratoCPD = new ContratosCDP();
        vContratoCPD.IdCDP = Convert.ToInt32(gvCDP.DataKeys[rowIndex]["IdContratosCDP"]);
        vContratoService.EliminarContratosCDP(vContratoCPD);
        //SeleccionarCDP(string.Empty);
    }

    #endregion

    #region VigenciasFuturas

    public void HabilitarSeccionVigenciaFuturas(bool habilitar)
    {
        if (habilitar)
        {
            //imgVigFuturas.Enabled = false;
            ////cvVigenciasFuturas.Enabled = false;
        }
        else
        {
            //imgVigFuturas.Enabled = false;
            //gvVigFuturas.DataSource = null;
            //gvVigFuturas.DataBind();
            //nVigenciasFuturas = "0";
            //cvVigenciasFuturas.Enabled = false;
        }
    }

    public void SeleccionarVigenciaFutura(string VigenciaFuturaSeleccionada)
    {
        //List<VigenciaFuturas> vigencias = vContratoService.ConsultarVigenciaFuturass(null, null, Convert.ToInt32(IdContrato), null);
        //gvVigFuturas.DataSource = vigencias;
        //gvVigFuturas.DataBind();
        //nVigenciasFuturas = gvVigFuturas.Rows.Count.ToString();

        //if (gvVigFuturas.Rows.Count == 0)
        //{
        //    //toolBar.MostrarBotonAprobar(false);
        //}
        //else
        //{

        //}
    }

    private void EliminarVigenciaFutura(int rowIndex)
    {
        VigenciaFuturas vVigenciaFutura = new VigenciaFuturas();
        vVigenciaFutura.IDVigenciaFuturas = Convert.ToInt32(gvVigFuturas.DataKeys[rowIndex]["IDVigenciaFuturas"]);
        vContratoService.EliminarVigenciaFuturas(vVigenciaFutura);
        SeleccionarVigenciaFutura(string.Empty);
    }

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //Response.HeaderEncoding = System.Text.Encoding.UTF32;
        //scriptEnCola = false;
        //vSolutionPage = SolutionPage.Add;
        //vSolutionPage = SolutionPage.Edit;


        try
        {

            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();                
            }
            else
            {
                #region CargaValoresContratoAsociado
                if (hfNumConvenioContratoAsociado.Value != "")
                    txtNumConvenioContrato.Text = hfNumConvenioContratoAsociado.Value;
                #endregion

                #region CargaValoresSolicitante
                if (hfNombreSolicitante.Value != "")
                    txtNombreSolicitante.Text = hfNombreSolicitante.Value;

                //if (hfRegionalContConv.Value != "")
                //    txtRegionalContratoConvenio.Text = hfRegionalContConv.Value;

                if (hfDependenciaSolicitante.Value != "")
                    txtDependenciaSolicitante.Text = hfDependenciaSolicitante.Value;

                if (hfCargoSolicitante.Value != "")
                    txtCargoSolicitante.Text = hfCargoSolicitante.Value;
                #endregion

                #region CagaValoresOrdenadorGasto
                if (hfNombreOrdenadorGasto.Value != "")
                    txtNombreOrdenadorGasto.Text = hfNombreOrdenadorGasto.Value;

                if (hfTipoIdentOrdenadorGasto.Value != "")
                    txtTipoIdentOrdenadorGasto.Text = hfTipoIdentOrdenadorGasto.Value;

                if (hfNumeroIdentOrdenadoGasto.Value != "")
                    txtNumeroIdentOrdenadoGasto.Text = hfNumeroIdentOrdenadoGasto.Value;

                if (hfCargoOrdenadoGasto.Value != "")
                    txtCargoOrdenadoGasto.Text = hfCargoOrdenadoGasto.Value;
                #endregion

                #region NumeroProceso
                if (hfNumeroProceso.Value != "")
                    txtNumeroProceso.Text = hfNumeroProceso.Value;
                #endregion

                #region AdministraPostBack
                //string sControlName = Request.Params.Get("__EVENTTARGET");
                //switch (sControlName)
                //{
                //    case "cphCont_txtNumConvenioContrato":
                //        txtNumConvenioContratoTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtNumeroProceso":
                //        txtNumeroProcesoTextChanged(sender, e);
                //        break;
                //    case "cphCont_caFechaAdjudicaProceso":
                //        SeleccionaFechaAdjudicacion(caFechaAdjudicaProceso.Date.ToString());
                //        break;
                //    case "cphCont_chkSiReqActa":
                //        chkReqActaCheckedChanged(chkSiReqActa, e);
                //        break;
                //    case "cphCont_chkNoReqActa":
                //        chkReqActaCheckedChanged(chkNoReqActa, e);
                //        break;
                //    case "cphCont_chkSiManejaAportes":
                //        chkManejaAportesCheckedChanged(chkSiManejaAportes, e);
                //        break;
                //    case "cphCont_chkNoManejaAportes":
                //        chkManejaAportesCheckedChanged(chkNoManejaAportes, e);
                //        break;
                //    case "cphCont_chkSiManejaRecursos":
                //        chkManejaRecursosCheckedChanged(chkSiManejaRecursos, e);
                //        break;
                //    case "cphCont_chkNoManejaRecursos":
                //        chkManejaRecursosCheckedChanged(chkNoManejaRecursos, e);
                //        break;
                //    case "cphCont_txtNombreSolicitante":
                //        txtNombreSolicitanteTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtNombreOrdenadorGasto":
                //        txtNombreOrdenadorGastoTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtPlanCompras":
                //        txtPlanComprasTextChanged(sender, e);
                //        break;
                //    case "cphCont_caFechaInicioEjecucion":
                //        SeleccionaFechaInicioEjecucion(caFechaInicioEjecucion.Date.ToString());
                //        break;
                //    case "cphCont_caFechaFinalizacionInicial":
                //        SeleccionaFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date.ToString());
                //        break;
                //    case "cphCont_chkSiManejaVigFuturas":
                //        chkManejaVigFuturasCheckedChanged(chkSiManejaVigFuturas, e);
                //        break;
                //    case "cphCont_chkNoManejaVigFuturas":
                //        chkManejaVigFuturasCheckedChanged(chkNoManejaVigFuturas, e);
                //        break;
                //    case "cphCont_txtValorApoICBF":
                //        txtValorApoICBFTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtValorApoContrat":
                //        txtValorApoContratTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtLugarEjecucion":
                //        txtLugarEjecucionTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtContratista":
                //        txtContratistaTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtSuperInterv":
                //        txtSuperIntervTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtCDP":
                //        txtCDPTextChanged(sender, e);
                //        break;
                //    case "cphCont_txtVigFuturas":
                //        txtVigFuturasTextChanged(sender, e);
                //        break;
                //    default:
                //        break;
                //}
                #endregion
            }
            ddlTipoContratoConvenio.Enabled = false;

        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = @ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = @ex.Message;
        }
    }

    protected void AccContratos_ItemCommand(object sender, CommandEventArgs e)
    {
        SeleccionarPanel(e.CommandName);
    }

    protected void ddlRegionalUsuarioContratoSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaRegional(ddlRegionalUsuarioContrato.SelectedValue);
    }

    protected void chkContratoAsociadoCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            switch (((CheckBox)sender).ID)
            {
                case "chkConvenioMarco":
                    SeleccionadoContratoAsociado(CodConvenioMarco);
                    break;
                case "chkContratoConvenioAd":
                    SeleccionadoContratoAsociado(CodContratoConvenioAdhesion);
                    break;
                default:
                    break;
            }

            ObtenerIdContratoConvenio();
        }
        else
        {
            SeleccionadoContratoAsociado(string.Empty);
        }
    }

    protected void txtNumConvenioContratoTextChanged(object sender, EventArgs e)
    {
        SeleccionaNumeroConvenioContrato(IdContratoAsociado);
        //SeleccionaNumeroConvenioContrato(txtNumConvenioContrato.Text);
    }

    protected void ddlCategoriaContratoSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue);
    }

    protected void ddlTipoContratoConvenioSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoTipoContratoConvenio(ddlCategoriaContrato.SelectedValue, ddlTipoContratoConvenio.SelectedValue);
    }

    protected void ddlModalidadAcademicaSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarModalidadAcademica(ddlModalidadAcademica.SelectedValue);
    }

    protected void ddlNombreProfesionSeleccionSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaProfesion(ddlNombreProfesion.SelectedValue);
    }

    protected void ddlModalidadSeleccionSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
    }

    protected void txtNumeroProcesoTextChanged(object sender, EventArgs e)
    {
        SeleccionarNumeroProceso(txtNumeroProceso.Text);
    }

    protected void chkReqActaCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            //#region UnicaSeleccionCheck
            //switch (((CheckBox)sender).ID)
            //{
            //    case "chkSiReqActa":
            //        chkNoReqActa.Checked = false;
            //        break;
            //    case "chkNoReqActa":
            //        chkSiReqActa.Checked = false;
            //        break;
            //    default:
            //        break;
            //}
            //#endregion

            HabilitarManejaAportes(true); // Pagina 13 Paso 29.1
            RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
        }
        else
        {
            HabilitarManejaAportes(null);
        }
    }

    protected void chkManejaAportesCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            //#region UnicaSeleccionCheck
            //switch (((CheckBox)sender).ID)
            //{
            //    case "chkSiManejaAportes":
            //        chkNoManejaAportes.Checked = false;
            //        break;
            //    case "chkNoManejaAportes":
            //        chkSiManejaAportes.Checked = false;
            //        break;
            //    default:
            //        break;
            //}
            //#endregion

            HabilitarManejaRecursos(true); // Pagina 13 Paso 32.1
            RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
            SeleccionaFormaPago(ddlFormaPago.SelectedValue);
        }
        else
        {
            HabilitarManejaRecursos(null);
        }
    }

    protected void chkManejaRecursosCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            //#region UnicaSeleccionCheck
            //switch (((CheckBox)sender).ID)
            //{
            //    case "chkSiManejaRecursos":
            //        chkNoManejaRecursos.Checked = false;
            //        break;
            //    case "chkNoManejaRecursos":
            //        chkSiManejaRecursos.Checked = false;
            //        break;
            //    default:
            //        break;
            //}
            //#endregion

            HabilitarRegimenContratacion(true); // Pagina 13 Paso 35
            //SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value);

            if (chkSiManejaRecursos.Checked)
            {
                HabilitarPlanDeCompras(true); // Pagina 14 Paso 46
                SeleccionarPlanCompras("1"); // Carga la información de la sección Plan de Compras
            }
            else
            {
                AcordeonActivo = "3";
                HabilitarPlanDeCompras(null);
                if (!hfNombreOrdenadorGasto.Value.Equals(string.Empty))
                    HabilitarObjetoContratoConvenio(true); // Se continua con el paso 55, despues 66 y despues 62
            }
        }
        else
        {
            HabilitarRegimenContratacion(null);
        }
    }

    protected void ddlRegimenContratacionSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaRegimenContratacion(ddlRegimenContratacion.SelectedValue); // Pagina 14 Paso 38
    }

    protected void txtNombreSolicitanteTextChanged(object sender, EventArgs e)
    {
        SeleccionarSolicitante(txtNombreSolicitante.Text); // Pagina 14 Paso 42
    }

    protected void txtNombreOrdenadorGastoTextChanged(object sender, EventArgs e)
    {
        SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value); // Pagina 14 Paso 46
    }

    protected void txtPlanComprasTextChanged(object sender, EventArgs e)
    {
        SeleccionarPlanCompras("1"); //txtPlanCompras.Text);
    }

    protected void DdlConsecutivoPlanComprasSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);
    }

    protected void txtObjetoTextChanged(object sender, EventArgs e)
    {
        IngresaObjetoContratoConvenio(txtObjeto.Text);
    }

    protected void txtAlcanceTextChanged(object sender, EventArgs e)
    {
        IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
    }

    protected void txtValorInicialContConvTextChanged(object sender, EventArgs e)
    {
        bool error = false;
        string s = txtValorInicialContConv.Text;
        string sPattern = @"^(\$|)([1-9]\d{0,2}(\.\d{3})*|([1-9]\d*))(\,\d{2})?$";
        //
        if (System.Text.RegularExpressions.Regex.IsMatch(s, sPattern))
        {
            decimal number;
            bool result = decimal.TryParse(s, out number);
            if (!result)
            {
                IngresaValorInicialContConv(string.Empty);
                error = true;
            }
            else
            {
                if (decimal.Parse(s) < 1000000000000)
                {
                    IngresaValorInicialContConv(txtValorInicialContConv.Text);
                }
                else
                {
                    //txtValorInicialContConv.Text = string.Empty;
                    IngresaValorInicialContConv(string.Empty);
                    error = true;
                }
            }
        }
        else
        {
            //txtValorInicialContConv.Text = string.Empty;
            IngresaValorInicialContConv(string.Empty);
            error = true;
        }

        if (error)
        {
            string mensajeValorInicial = "El valor del aporte es demasiado largo";
            ScriptManager.RegisterStartupScript(this, GetType(), "valValorInicial", "mensajeAlerta('" + txtValorInicialContConv.ClientID + "', '" + mensajeValorInicial + "')", true);
        }
    }

    protected void chkManejaVigFuturasCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            #region UnicaSeleccionCheck
            switch (((CheckBox)sender).ID)
            {
                case "chkSiManejaVigFuturas":
                    HabilitarSeccionVigenciaFuturas(true);
                    break;
                case "chkNoManejaVigFuturas":
                    HabilitarSeccionVigenciaFuturas(false);
                    //toolBar.MostrarBotonAprobar(true);
                    break;
                default:
                    break;
            }
            #endregion

            HabilitarVigenciaFiscal(true); // Pagina 19 Paso 95
        }
        else
        {
            HabilitarVigenciaFiscal(null);
        }
    }

    protected void ddlVigenciaFiscalIniSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaVigenciaFiscalInicial(ddlVigenciaFiscalIni.SelectedValue);
    }

    protected void ddlVigenciaFiscalFinSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaVigenciaFiscalFinal(ddlVigenciaFiscalFin.SelectedValue);
    }

    protected void ddlFormaPagoSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaFormaPago(ddlFormaPago.SelectedValue);
    }

    protected void ddlTipoFormaPagoSelectedIndexChanged(object sender, EventArgs e)
    {
        //SeleccionaTipoFormaPago(ddlTipoFormaPago.SelectedValue);
    }
    protected void txtValorApoICBFTextChanged(object sender, EventArgs e)
    {
        SeleccionarAportes(string.Empty);
    }

    protected void txtValorApoContratTextChanged(object sender, EventArgs e)
    {
        SeleccionarAportes(string.Empty);
    }
    protected void gvDocumentos_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    #region gvAportesICBF

    protected void gvAportesICBF_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void btnEliminarAporteICBFClick(object sender, EventArgs e)
    {
        EliminarAporteICBF(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    #endregion

    #region gvAportesContratista

    protected void gvAportesContratista_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    protected void btnEliminarAporteContratistaClick(object sender, EventArgs e)
    {
        EliminarAporteContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    #endregion

    protected void txtLugarEjecucionTextChanged(object sender, EventArgs e)
    {
        SeleccionaLugarEjecucion(string.Empty);
    }

    protected void chkNivelNacionalCheckedChanged(object sender, EventArgs e)
    {
        SeleccionarNivelNacional(((CheckBox)sender).Checked);
    }

    protected void btnEliminarLugEjecucionClick(object sender, EventArgs e)
    {
        EliminarLugarEjecucion(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtDatosAdicionalesLugarEjecucionTextChanged(object sender, EventArgs e)
    {
        IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
    }

    protected void txtContratistaTextChanged(object sender, EventArgs e)
    {
        SeleccionarContratistas(string.Empty);
    }

    protected void btnOpcionGvContratistasClick(object sender, EventArgs e)
    {
        switch (((LinkButton)sender).CommandName)
        {
            case "Eliminar":
                EliminarContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
                break;
            case "Detalle":
                int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);

                string idProveedor = gvContratistas.DataKeys[rowIndex]["IdTercero"].ToString();
                SetSessionParameter("Contrato.IdProveedor", idProveedor);

                string idProveedoresContratos = gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"].ToString();
                SetSessionParameter("FormaPago.IDProveedoresContratos", idProveedoresContratos);

                SetSessionParameter("Contrato.ContratosAPP", IdContrato);

                ScriptManager.RegisterStartupScript(this, GetType(), "formaPago", "GetFormaPago()", true);
                break;
            case "VerSupervisor":
                int rowIndex1 = Convert.ToInt32(((LinkButton)sender).CommandArgument);
                string idProveedoresContratos1 = gvContratistas.DataKeys[rowIndex1]["IdProveedoresContratos"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "HistoricoSupervisores", "GetHistoricoSupervisores('" + idProveedoresContratos1 + "')", true);
                break;
            default:
                break;
        }
    }

    protected void txtSuperIntervTextChanged(object sender, EventArgs e)
    {
        SeleccionarSupervisorInterventor(string.Empty);
    }

    protected void btnEliminarGvSupervIntervClick(object sender, EventArgs e)
    {
        EliminarSupervInterv(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtCDPTextChanged(object sender, EventArgs e)
    {
        //SeleccionarCDP(string.Empty); //activar la Validacion si se carga la información
    }

    protected void btnEliminarCDPClick(object sender, EventArgs e)
    {
        EliminarCDP(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtVigFuturasTextChanged(object sender, EventArgs e)
    {
        SeleccionarVigenciaFutura(string.Empty);
    }

    protected void btnEliminarGvVigFuturasClick(object sender, EventArgs e)
    {
        EliminarVigenciaFutura(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void btnMostrar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {

            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;

            if (dataItem != null)
            {
                Show(dataItem.RowIndex);
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    #endregion

    #region MétodosPágina
    protected int PageSize()
    {
        Int32 vPageSize;
        if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
        {
            throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
        }
        return vPageSize;
    }
    protected String EmptyDataText()
    {
        String vEmptyDataText;
        if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
        {
            throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
        }
        vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
        return vEmptyDataText;
    }

    private void Show(int pRow)
    {
        try
        {
            //gvDocumentos
            int rowIndex = pRow; //pRow.SelectedRow.RowIndex;
            string nombreArchivo = "";

            Label LbArchivo = (Label)gvDocumentos.Rows[rowIndex].FindControl("LbArchivoFTP");
            if (LbArchivo != null)
            {
                nombreArchivo = LbArchivo.Text;
            }

            Response.Redirect("../DescargarArchivo/DescargarArchivo.aspx?fname=" + nombreArchivo);
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }
    #endregion

    #region GrillasContratosAsociadosoAdheridos
    private void LlenarGrillaContratosAdheridos(int pIdContrato)
    {

        gvContAdhContConv.PageSize = PageSize();
        gvContAdhContConv.EmptyDataText = EmptyDataText();
        //Carga grilla Contratos Adheridos
        List<Contrato> vContratosAdheridos = vContratoService.ConsultarContratosAdheridos(pIdContrato);
        gvContAdhContConv.DataSource = vContratosAdheridos;
        gvContAdhContConv.DataBind();

    }

    private void LlenarGrillaContratosRelacionadosConvMarco(int pIdContrato)
    {

        //Carga grilla Contratos Relacionados Convenio Marco
        gvContRelConMar.PageSize = PageSize();
        gvContRelConMar.EmptyDataText = EmptyDataText();



        List<Contrato> vContratosAdheridos = vContratoService.ConsultarContratosRelacionadosConvMarco(pIdContrato);
        gvContRelConMar.DataSource = vContratosAdheridos;
        gvContRelConMar.DataBind();

    }

    private void LlenarGrillaDocumentosContrato(int pIdContrato)
    {

        List<ArchivoContrato> lArchivoContrato = new List<ArchivoContrato>();

        lArchivoContrato = vContratoService.ConsultarArchivo(pIdContrato);
        lArchivoContrato.Add(new ArchivoContrato
        {
            IdArchivoContrato = 0,
            IdArchivo = 0,
            IdContrato = 0,
            NombreArchivo = ""

        });

        this.gvDocumentos.DataSource = lArchivoContrato;
        this.gvDocumentos.DataBind();



    }

    #endregion

    protected void gvContRelConMar_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistroContRelConMar(gvContRelConMar.SelectedRow);
    }
    protected void gvContAdhContConv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistroContAdhContConv(gvContAdhContConv.SelectedRow);
    }
    protected void gvContAdhContConv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void gvContRelConMar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistroContRelConMar(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValueIdContrato = string.Empty;
            string strValueEstadoContrato = string.Empty;
            string strValueUsuarioCrea = string.Empty;

            if (gvContRelConMar.Rows[rowIndex].Cells[1].Text != null)
            {
                strValueIdContrato = gvContRelConMar.Rows[rowIndex].Cells[1].Text;
            }

            SetSessionParameter("Contrato.IdContrato", strValueIdContrato);

            Response.Redirect("../Contratos/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }
    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistroContAdhContConv(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValueIdContrato = string.Empty;
            string strValueEstadoContrato = string.Empty;
            string strValueUsuarioCrea = string.Empty;

            if (gvContAdhContConv.Rows[rowIndex].Cells[1].Text != null)
            {
                strValueIdContrato = gvContAdhContConv.Rows[rowIndex].Cells[1].Text;
            }

            SetSessionParameter("Contrato.IdContrato", strValueIdContrato);

            Response.Redirect("../Contratos/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }

    private int ValidacionFecha(String Fecha)
    {
        int temp = 0;
        DateTime fechavalidacion = new DateTime(1900, 1, 1);

        if (string.IsNullOrEmpty(Fecha))
        {
            temp = 1;
            return temp;
        }

        if (Fecha.StartsWith("_"))
        {
            temp = 1;
            return temp;
        }

        if (!DateTime.TryParse(Fecha, out fechavalidacion))
        {
            temp = 1;
            return temp;
        }

        fechavalidacion = new DateTime(1900, 1, 1);
        if (Convert.ToDateTime(Fecha).Date.Equals(fechavalidacion.Date))
        {
            temp = 1;
            return temp;
        }

        return temp;
    }
}
