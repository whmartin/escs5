﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using AjaxControlToolkit;

public partial class Page_Contratos_LugarEjecucionContrato_ConsultarLugarContrato : GeneralWeb
{
    #region Variables
    ContratoService vContratoService = new ContratoService();
    SIAService vRUBOService = new SIAService();
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/LugarEjecucionContrato";

    public string vIdsDepartamento { set { ViewState["vIdsDepartamento"] = value; } get { return (string)ViewState["vIdsDepartamento"]; } }
    public string vIdsMunicipio { set { ViewState["vIdsMunicipio"] = value; } get { return (string)ViewState["vIdsMunicipio"]; } }

    #endregion

    #region eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //SolutionPage vSolutionPage = SolutionPage.List;
        //if (ValidateAccess(toolBar, PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {
            vIdsDepartamento = string.Empty;
            vIdsMunicipio = string.Empty;
            VerficarQueryStrings();
            CargarDatosIniciales();
        }
        //}
    }

    /// <summary>
    /// Manejador del evento cambio de elemento en un listbox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbxDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        vIdsDepartamento = ddlDepartamento.SelectedValue;
        //foreach (ListItem li in lbxDepartamento.Items)
        //{
        //    if (li.Selected == true && li.Value != "-1")
        //    {
        //        // get the value of the item in your loop
        //        vIdsDepartamento += li.Value + ",";
        //    }
        //}
        if (vIdsDepartamento.Length > 0)
        {
            vIdsDepartamento = vIdsDepartamento;//.Substring(0, vIdsDepartamento.LastIndexOf(","));
            LlenarMunicipios(vIdsDepartamento);
        }
        else
        {
            limpiarComboMunicipio();
        }


    }

    protected void chkNivelNacional_CheckedChanged(object sender, EventArgs e)
    {
        if (chkNivelNacional.Checked)
        {
            ddlDepartamento.Items.Clear();
            ddlDepartamento.Enabled = false;
            lbxMunicipio.Items.Clear();
            lbxMunicipio.Enabled = false;
            vIdsDepartamento = string.Empty;
            vIdsMunicipio = string.Empty;
            rfvddlDepartamento.Enabled = false;
            rfvMunicipio.Enabled = false;
        }
        else
        {
            ddlDepartamento.Enabled = true;
            lbxMunicipio.Enabled = true;
            ManejoControlesContratos vControles = new ManejoControlesContratos();
            vControles.LlenarDepartamentos(ddlDepartamento, "-1",true);

            rfvddlDepartamento.Enabled = true;
            rfvMunicipio.Enabled = true;
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (esTodosMunicipios())//Todos
            SiTodosMunicipios();
        else
            ObtenerIdsMunicipio();
        Guardar();
    }

    #endregion

    #region Métodos
    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesContratos vControles = new ManejoControlesContratos();
                vControles.LlenarDepartamentos(ddlDepartamento, "-1",true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click); ;

            toolBar.EstablecerTitulos("Lugar Ejecuci&#243;n Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

   

    private void SiTodosMunicipios()
    {
       
            vIdsMunicipio = string.Empty;
            foreach (ListItem li in lbxMunicipio.Items)
            {
                if (li.Value != "-1" && li.Value != "0")
                {
                    // get the value of the item in your loop
                    vIdsMunicipio += li.Value + ",";
                }
            }
            if (vIdsMunicipio.Length > 0)
            {
                vIdsMunicipio = vIdsMunicipio.Substring(0, vIdsMunicipio.LastIndexOf(","));
            }
        
    }


    private void Guardar()
    {
        try
        {
            if (GuardarEnTabla())
            {
                string returnValues =
                "<script language='javascript'> " +
                "var pObj = Array();";

                returnValues += "pObj[" + (0) + "] = '" + chkNivelNacional.Checked + "';";
                returnValues += "pObj[" + (1) + "] = '" + vIdsDepartamento + "';";
                returnValues += "pObj[" + (2) + "] = '" + vIdsMunicipio + "';";

                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                               " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool GuardarEnTabla()
    {
        bool vSeGuardo = false;
        try {
            rfvMunicipio.Validate();
            rfvddlDepartamento.Validate();
            if (rfvMunicipio.IsValid == true && rfvddlDepartamento.IsValid == true)
            {
                //Session["ContratosAPP"] = 17;
                //Almacena en tabla
                  if (!string.IsNullOrEmpty(hfIDDetalleConsModContractual.Value))
                {

                    int vResultado;
                    CambioLugarEjecucion vLugarEjecucionContrato = new CambioLugarEjecucion();

                    vLugarEjecucionContrato.IdDetalleConsModContractual = Convert.ToInt32(hfIDDetalleConsModContractual.Value);

                    vLugarEjecucionContrato.IdsRegionales = vIdsMunicipio;
                    vLugarEjecucionContrato.NivelNacional = chkNivelNacional.Checked;
                    vLugarEjecucionContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vLugarEjecucionContrato, this.PageName, vSolutionPage);

                    vResultado = vContratoService.InsertarCambioLugarEjecucion(vLugarEjecucionContrato);

                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado >= 1)
                    {
                        vSeGuardo = true;
                    }
                }
                else if (GetSessionParameter("Contrato.ContratosAPP") != null && GetSessionParameter("Contrato.ContratosAPP") != "")
                {
                    int vResultado;
                    int IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
                    //RemoveSessionParameter("Contrato.ContratosAPP");
                    LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();

                    vLugarEjecucionContrato.IdContrato = IdContrato;

                    vLugarEjecucionContrato.IdsRegionales = vIdsMunicipio;
                    vLugarEjecucionContrato.NivelNacional = chkNivelNacional.Checked;
                    vLugarEjecucionContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vLugarEjecucionContrato, this.PageName, vSolutionPage);

                    vResultado = vContratoService.InsertarLugarEjecucionContrato(vLugarEjecucionContrato);

                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado >= 1)
                    {
                        vSeGuardo = true;
                    }
                }               
                else
                {
                    throw new Exception("El valor del contrato es nulo");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return vSeGuardo;
    }

    /// <summary>
    /// Determina si es para todos los municipios
    /// </summary>
    /// <returns></returns>
    private bool esTodosMunicipios()
    {
        foreach (ListItem li in lbxMunicipio.Items)
        {
            if (li.Selected == true && li.Value == "0")
            {
                return true;
            }
        }
        return false;
    }

   

    private void ObtenerIdsMunicipio()
    {
        vIdsMunicipio = string.Empty;
        foreach (ListItem li in lbxMunicipio.Items)
        {
            if (li.Selected == true && li.Value != "-1" && li.Value != "0")
            {
                // get the value of the item in your loop
                vIdsMunicipio += li.Value + ",";
            }
        }
        if (vIdsMunicipio.Length > 0)
        {
            vIdsMunicipio = vIdsMunicipio.Substring(0, vIdsMunicipio.LastIndexOf(","));
        }
    }

    private void limpiarComboMunicipio()
    {
        lbxMunicipio.Items.Clear();
        vIdsMunicipio = string.Empty;
    }

    private void LlenarMunicipios(string pIdsDepartamento)
    {
        SIAService vRUBOService = new SIAService();

        List<Icbf.SIA.Entity.Municipio> LstMunicipios = vRUBOService.ConsultarMunicipiosPorIdsDep(pIdsDepartamento);


        LstMunicipios.Insert(LstMunicipios.Count() , new Icbf.SIA.Entity.Municipio() { NombreMunicipio = "Todos", IdMunicipio = 0 });
        //LstMunicipios.Insert(0, new Icbf.SIA.Entity.Municipio() { NombreMunicipio = "Seleccione", IdMunicipio = -1 });

        lbxMunicipio.DataSource = LstMunicipios;
        lbxMunicipio.DataTextField = "NombreMunicipio";
        lbxMunicipio.DataValueField = "IdMunicipio";
        lbxMunicipio.DataBind();
    }

    private void VerficarQueryStrings()
    {
        
        if (!string.IsNullOrEmpty(Request.QueryString["IDDetalleConsModContractual"]))
            hfIDDetalleConsModContractual.Value = Request.QueryString["IDDetalleConsModContractual"];

        if (!string.IsNullOrEmpty(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString()))
            hfIDDetalleConsModContractual.Value = GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString();
    }
    #endregion
}