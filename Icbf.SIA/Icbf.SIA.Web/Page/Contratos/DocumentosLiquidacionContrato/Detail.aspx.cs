﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;


/// <summary>
/// Página de visualización detallada para la entidad NumeroProcesos
/// </summary>
public partial class Page_Contratos_DocumentosLiquidacionContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/DocumentosLiquidacionContrato";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DocumentoLiquidacion.IdDocumentoLiquidacion", hfIdDocumentoLiquidacion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdDocumentoLiquidacion = Convert.ToInt32(GetSessionParameter("DocumentoLiquidacion.IdDocumentoLiquidacion"));
            RemoveSessionParameter("DocumentoLiquidacion.IdDocumentoLiquidacion");

            if (GetSessionParameter("DocumentoLiquidacion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("DocumentoLiquidacion.Guardado");


            DocumentosLiquidacion vDocumentoLiquidacion = new DocumentosLiquidacion();
            vDocumentoLiquidacion = vContratoService.ConsultarDocumentosLiquidacion(vIdDocumentoLiquidacion);
            hfIdDocumentoLiquidacion.Value = vDocumentoLiquidacion.IdDocumentoLiquidacion.ToString();
            txtNombre.Text = vDocumentoLiquidacion.NombreDocumento;
            txtDescripcion.Text = vDocumentoLiquidacion.DescripcionDocumento;
            rblInactivo.SelectedValue = vDocumentoLiquidacion.Estado.ToString().Trim().ToLower();
            
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDocumentoLiquidacion.UsuarioCrea, vDocumentoLiquidacion.FechaCrea, vDocumentoLiquidacion.UsuarioModifica, vDocumentoLiquidacion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdDocumentoLiquidacion = Convert.ToInt32(hfIdDocumentoLiquidacion.Value);

            DocumentosLiquidacion vDocumentoLiquidacion = new DocumentosLiquidacion();
            vDocumentoLiquidacion = vContratoService.ConsultarDocumentosLiquidacion(vIdDocumentoLiquidacion);

            //if (vContratoService.ConsultarNumeroProcesosPorContrato(vNumeroProcesos.IdNumeroProceso))
            //{
            //    toolBar.MostrarMensajeError("El registro tiene elementos  que dependen de él, verifique por favor.");
            //    return;
            //}

            InformacionAudioria(vDocumentoLiquidacion, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarDocumentosLiquidacion(vDocumentoLiquidacion);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("DocumentoLiquidacion.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Documentos Liquidacion Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

   

}
