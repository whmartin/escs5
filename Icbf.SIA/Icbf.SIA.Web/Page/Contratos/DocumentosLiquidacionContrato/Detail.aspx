﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Contratos_DocumentosLiquidacionContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdDocumentoLiquidacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="auto-style1">
                Nombre del Documento
            </td>
            <td>
                Descripción del Documento
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtNombre" Width="200px" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false" TextMode="MultiLine" Width="400px" Height="42px"></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
             <td class="auto-style1">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            
            <td class="auto-style1">
                <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>

        
    </table>
</asp:Content>


<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 500px;
        }
    </style>
</asp:Content>



