﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Certificaciones.aspx.cs" Inherits="Page_Contratos_CertificacionesContratos_Certificaciones" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr>
                <td></td>
                <td>
                    <asp:GridView runat="server" ID="gvCertificacionesContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDContrato,IDCertificacion,NombreArchivo" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvCertificacionesContratos_PageIndexChanging">
                        <Columns>

                            <asp:BoundField HeaderText="N&uacute;mero Certificacion" DataField="IDCertificacion" SortExpression="IDCertificacion" />
                            <asp:BoundField HeaderText="Fecha Certificacion" DataField="FechaCertificacion" DataFormatString="{0:d}" ItemStyle-CssClass="Otra Fecha" />
                            <asp:BoundField HeaderText="UsuarioCrea" DataField="UsuarioCrea" SortExpression="UsuarioCrea" />
                            <asp:BoundField HeaderText="Estado" DataField="StrEstado" SortExpression="StrEstado" />
                            <asp:TemplateField HeaderText="Documento Anexo">
                                <ItemTemplate>
                                    <asp:Label ID="LbArchivo" runat="server" Text='<%#Eval("NombreArchivoOri") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnAdjuntar" runat="server" CommandName="Attach" ImageUrl="~/Image/btn/attach.jpg"
                                        Height="16px" Width="16px" ToolTip="Adjuntar" Enabled="true"
                                        Visible='<%# HttpUtility.HtmlDecode((string)Eval("NombreArchivo")) == "" %>'
                                        OnClick="btnAdjuntar_Click" />
                                    <asp:FileUpload ID="fuArchivo" runat="server" Visible="false" />
                                    <asp:ImageButton ID="btnSave" runat="server" CommandName="Save" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Guardar" Visible="false"
                                        OnClick="btnSave_Click" />
                                    <%--<asp:HiddenField ID="hfIdArchivo" runat="server" />--%>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" ImageUrl="~/Image/btn/list.png"
                                        Height="16px" Width="16px" ToolTip="Visualizar" Enabled="True"
                                        OnClick="btnMostrar_Click" Visible='<%# HttpUtility.HtmlDecode((string)Eval("NombreArchivo")) != "" %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

