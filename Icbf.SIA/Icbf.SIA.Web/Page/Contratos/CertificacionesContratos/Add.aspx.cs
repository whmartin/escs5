using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición para la entidad CertificacionesContratos
/// </summary>
public partial class Page_CertificacionesContratos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/CertificacionesContratos";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad CertificacionesContratos
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();

            vCertificacionesContratos.IDContrato = Convert.ToInt32(txtIDContrato.Text);
            vCertificacionesContratos.FechaCertificacion = Convert.ToDateTime(txtFechaCertificacion.Text);
            vCertificacionesContratos.Estado = Convert.ToBoolean(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vCertificacionesContratos.IDCertificacion = Convert.ToInt32(hfIDCertificacion.Value);
                vCertificacionesContratos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCertificacionesContratos, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarCertificacionesContratos(vCertificacionesContratos);
            }
            else
            {
                vCertificacionesContratos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCertificacionesContratos, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarCertificacionesContratos(vCertificacionesContratos);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("CertificacionesContratos.IDCertificacion", vCertificacionesContratos.IDCertificacion);
                SetSessionParameter("CertificacionesContratos.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("CertificacionesContratos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIDCertificacion = Convert.ToInt32(GetSessionParameter("CertificacionesContratos.IDCertificacion"));
            RemoveSessionParameter("CertificacionesContratos.Id");

            CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();
            vCertificacionesContratos = vContratoService.ConsultarCertificacionesContratos(vIDCertificacion);
            hfIDCertificacion.Value = vCertificacionesContratos.IDCertificacion.ToString();
            txtIDContrato.Text = vCertificacionesContratos.IDContrato.ToString();
            txtFechaCertificacion.Text = vCertificacionesContratos.FechaCertificacion.ToString();
            rblEstado.SelectedValue = vCertificacionesContratos.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCertificacionesContratos.UsuarioCrea, vCertificacionesContratos.FechaCrea, vCertificacionesContratos.UsuarioModifica, vCertificacionesContratos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "true"));
            rblEstado.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblEstado.SelectedValue = "true";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
