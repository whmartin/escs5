<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_CertificacionesContratos_Detail" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDCertificacion" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />    
    <table width="90%" align="center">
        <tr class="rowB">
            <td style="width: 50%">ID Contrato
            </td>
            <td style="width: 50%">Número Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 50%">
                <asp:TextBox runat="server" ID="txtIDContrato" Enabled="false" Width="250px"></asp:TextBox>
            </td>
            <td style="width: 50%">
                <asp:TextBox runat="server" ID="txtNumeroContrato" Enabled="false" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Fecha Inicio Ejecuci&oacute;n Contrato
            </td>
            <td>Fecha Final Terminaci&oacute;n del Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="TxtFechaInicioContrato" Text="" Enabled="False"></asp:TextBox>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="TxtFechaFinContrato" Text="" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Persona que certifica *
            </td>
            <td>Requiere Obligaciones
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlPersonaCertifica" runat="server" Visible="False"></asp:DropDownList>
                <asp:TextBox runat="server" ID="TxtPersonaCertifica" Text="" Width="80%" Visible="False"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rbRequiereObligaciones" RepeatDirection="Horizontal" AutoPostBack="True"
                    OnSelectedIndexChanged="rbRequiereObligaciones_SelectedIndexChanged">
                </asp:RadioButtonList>
            </td>
        </tr>
       <tr class="rowB">
            <td>Exportar Word
            </td>
            <td>
            </td>
        </tr>
                <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rbExportaExcel" RepeatDirection="Horizontal" >
                    <asp:ListItem Text="Si" Value="Si" />
                    <asp:ListItem Text="No" Value="No" Selected="True" />
                </asp:RadioButtonList>
            </td>
            <td colspan="2">

            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:Panel runat="server" ID="PnlObligaciones" Visible="False" Width="100%">
                    <table width="100%">
                        <tr class="rowB">
                            <td colspan="2">Obligaciones
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2">
                                <div style="float: left; height: 100px; width: 65%; margin: auto;">
                                    <asp:TextBox runat="server" ID="TextObligaciones" Width="100%"  TextMode="MultiLine" Height="100px"></asp:TextBox>
                                </div>
                                <div style="float: right; height: 100px; width: 34%;">
                                    <div style="margin-top: 40px; margin-left: 10px">
                                        <asp:LinkButton ID="btnAgregar" runat="server" OnClick="btnAgregar_Click1">
                            <img alt="Nuevo" src="../../../Image/btn/add.gif" title="Agregar" width="20px" height="20px" />
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView runat="server" ID="gvObligaciones" AutoGenerateColumns="False" AllowPaging="True"
                                   OnPageIndexChanging="gvObligaciones_PageIndexChanging" GridLines="None" Width="100%" 
                                    DataKeyNames="IDObligacionContrato" CellPadding="0" Height="16px"
                                    AllowSorting="True" >
                                    <Columns>
                                       
                                        <asp:TemplateField HeaderText="Obligación">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtDescripcionObligacion" TextMode="MultiLine" Width="600px" Height="50px" Text='<%#Eval("DescripcionObligacion") %>'></asp:TextBox>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Eliminar">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnCertificaciones" Visible="true" runat="server" ImageUrl="~/Image/btn/delete.gif"
                                                    Height="16px" Width="16px" ToolTip="Certificaciones" OnClick="btnCertificaciones_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Modificar">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnModCertificaciones" Visible="true" runat="server" ImageUrl="~/Image/btn/save.gif"
                                                    Height="16px" Width="16px" ToolTip="Certificaciones" OnClick="btnModCertificaciones_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

            </td>
        </tr>

    </table>

</asp:Content>
