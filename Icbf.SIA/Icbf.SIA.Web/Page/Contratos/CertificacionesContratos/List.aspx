<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_CertificacionesContratos_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Vigencia Fiscal Inicial *
                    <asp:RequiredFieldValidator runat="server" ID="rfvIDContrato" ControlToValidate="TxtAnioVigencia"
                     SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                     ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>Regional Contrato/Convenio
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox ID="TxtAnioVigencia" runat="server" Text=""></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIDRegional"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">N&uacute;mero Contrato/Convenio
                </td>
                <td class="Cell">Fecha de Inicio de Ejecución Desde
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="40"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftIDContrato" runat="server" TargetControlID="txtNumeroContrato"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td class="Cell">
                    <uc1:fecha id="txtFechaInicial" runat="server" width="80%" enabled="True"
                        requerid="False" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Fecha Inicio de Ejecución Hasta
                </td>
                <td class="Cell">
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <uc1:fecha id="TxtFechaFinal" runat="server" width="80%" enabled="True"
                        requerid="False" />
                </td>
                <td class="Cell">
                    
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvCertificacionesContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDContrato" CellPadding="0" Height="16px"
                        OnSorting="gvCertificacionesContratos_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvCertificacionesContratos_PageIndexChanging" OnSelectedIndexChanged="gvCertificacionesContratos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Número de Contrato/Convenio" DataField="NumeroContrato" SortExpression="NumeroContrato" />
                            <asp:BoundField HeaderText="Valor Inicial del Contrato" DataField="ValorInicialContrato" SortExpression="ValorInicialContrato"  DataFormatString="{0:c}"/>
                            <asp:BoundField HeaderText="Fecha de inicio de ejecuci&oacute;n" DataField="FechaInicioEjecucion" SortExpression="FechaInicioEjecucion" />
                            <asp:BoundField HeaderText="Regional" DataField="DescRegional" SortExpression="DescRegional" />
                            <asp:BoundField HeaderText="Estado" DataField="DescEstado" SortExpression="DescEstado" />
                           
                            <asp:TemplateField HeaderText="Certificaciones">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnCertificaciones" runat="server" ImageUrl="~/Image/btn/info.jpg" 
                                        Height="16px" Width="16px" ToolTip="Certificaciones" OnClick="btnCertificaciones_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
