﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;


using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;


public partial class Page_Contratos_CertificacionesContratos_Certificaciones : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/CertificacionesContratos";
    ContratoService _vContratoService = new ContratoService();
    SIAService _vSiaServices = new SIAService();

    private enum grilla
    {
        Archivo = 0,
        Adjuntar = 1,
        Filearchivo = 1,
        Guardar = 1,
        Mostrar = 2
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CargarRegistro();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }


    protected void gvCertificacionesContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCertificacionesContratos.PageIndex = e.NewPageIndex;
        CargarRegistro();
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvCertificacionesContratos.PageSize = PageSize();
            gvCertificacionesContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Certificaciones Contratos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {

            int vIDCertificacion = Convert.ToInt32(GetSessionParameter("CertificacionesContratos.IDCertificacionContrato"));
            RemoveSessionParameter("CertificacionesContratos.IDCertificacionContrato");

            gvCertificacionesContratos.DataSource = _vContratoService.ConsultarCertificacionesPorContratos(vIDCertificacion);
            gvCertificacionesContratos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Save(int pRow)
    {
        int rowIndex = pRow;

        var fuArchivo = (FileUpload)gvCertificacionesContratos.Rows[rowIndex].FindControl("fuArchivo");


        if (fuArchivo.HasFile)
        {
            try
            {
                //Extraer la extensión del archivo
                var finfo = new FileInfo(fuArchivo.FileName);

                string fileExt = finfo.Extension.ToLower();

                if (ConfigurationManager.AppSettings["MaxSizeDoc"] == null)
                {
                    throw new GenericException("No se han configurado las variables del maximo tamaño permitido.");
                }

                string sMaxFileSizeDoc = ConfigurationManager.AppSettings["MaxSizeDoc"].ToString();


                int maxFileSizeDoc = 0;
                if (!int.TryParse(sMaxFileSizeDoc, out maxFileSizeDoc))
                {
                    throw new GenericException("Error en la aplicación, consulte con soporte técnico.");
                }


                if (fileExt == ".pdf" || fileExt == ".jpg" || fileExt == ".jpeg"
                    || fileExt == ".png" || fileExt == ".doc" || fileExt == ".docx")
                {
                    //Si el achivo es un documento o una imagen que no exceda los 4 MegaBytes
                    if (fuArchivo.PostedFile.ContentLength / 1024 > maxFileSizeDoc * 1024)
                    {
                        throw new GenericException("Documento no v&aacute;lido por tamaño.");
                    }
                }
                else
                {
                    throw new GenericException("Formato de archivo no valido.");
                }

                string filename = Path.GetFileName(fuArchivo.FileName);

                if (filename == null)
                    throw new GenericException("Archivo no valido.");

                //Creación del Nombre del archivo
                string nombreArchivo = filename.Substring(0, filename.IndexOf(".", System.StringComparison.Ordinal));
                nombreArchivo = _vContratoService.QuitarAcentos(nombreArchivo);
                nombreArchivo = DateTime.Now.Year + "_" + DateTime.Now.Month + "_" +
                                DateTime.Now.Day + "_" + DateTime.Now.Hour + "_" +
                                DateTime.Now.Minute + "_" + DateTime.Now.Second +
                                "_DocCertContrato_" + nombreArchivo + filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal));
                //NombreArchivo = Guid.NewGuid().ToString() + NombreArchivo;
                string nombreArchivoFtp = ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                          ConfigurationManager.AppSettings["FtpPort"] + @"/CertificacionesContrato";

                if (!Directory.Exists(nombreArchivoFtp))
                {
                    System.IO.Directory.CreateDirectory(nombreArchivoFtp);
                }

                //Guardado del archivo en el servidor
                HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;
                Boolean archivoSubido = _vSiaServices.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream,
                                                                     nombreArchivoFtp, nombreArchivo);


                if (archivoSubido)
                {
                    //toolBar.MostrarMensajeError("El archivo se cargo correctamente");
                    toolBar.MostrarMensajeGuardado();

                    //Creación del registro de control del archivo subido

                    List<FormatoArchivo> vFormatoArchivo =
                      _vSiaServices.ConsultarFormatoArchivoTemporarExt("General.archivoPDF", "PDF");

                    var registroControl = new ArchivoContrato
                    {
                        IdUsuario = Convert.ToInt32(GetSessionUser().IdUsuario.ToString()),
                        FechaRegistro = DateTime.Now,
                        ServidorFTP = nombreArchivoFtp,
                        NombreArchivo = nombreArchivo,
                        Estado = "C",
                        ResumenCarga = string.Empty,
                        UsuarioCrea = GetSessionUser().NombreUsuario,
                        FechaCrea = DateTime.Now,
                        IdFormatoArchivo = vFormatoArchivo[0].IdFormatoArchivo,
                        NombreArchivoOri = filename
                    };

                  

                    InformacionAudioria(registroControl, PageName, vSolutionPage);
                    int vResultado = _vContratoService.InsertarArchivo(registroControl);
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        decimal vIdArchivo = registroControl.IdArchivo;

                        var dataKey = gvCertificacionesContratos.DataKeys[rowIndex];
                        if (dataKey != null)
                        {
                            int idCertificacion = Convert.ToInt32(dataKey["IDCertificacion"].ToString());

                            var vArchivoCertificacion = new CertificacionesContratos
                            {
                                UsuarioModifica = GetSessionUser().NombreUsuario,
                                IdArchivo = vIdArchivo,
                                IDCertificacion = idCertificacion

                            };
                            _vContratoService.ModificarCertificacionesContratos(vArchivoCertificacion);
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
                return;
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Seleccione un archivo");
            return;
        }



        fuArchivo.Visible = false;
        var btnAdjuntar = (ImageButton)gvCertificacionesContratos.Rows[rowIndex].FindControl("btnAdjuntar");
        btnAdjuntar.Visible = false;
        btnAdjuntar.Enabled = false;
        var btnSave = (ImageButton)gvCertificacionesContratos.Rows[rowIndex].FindControl("btnSave");
        btnSave.Visible = false;

        var btnMostrar = (ImageButton)gvCertificacionesContratos.Rows[rowIndex].FindControl("btnMostrar");
        btnMostrar.Enabled = true;
        var btnEliminar = (ImageButton)gvCertificacionesContratos.Rows[rowIndex].FindControl("btnEliminar");
        btnEliminar.Enabled = true;
     
        toolBar.LipiarMensajeError();
    }

    private void Show(int pRow)
    {
        try
        {
            //gvDocumentos
            int rowIndex = pRow; //pRow.SelectedRow.RowIndex;
            string nombreArchivo = "";

            var dataKey = gvCertificacionesContratos.DataKeys[rowIndex];

            if (dataKey != null)
                nombreArchivo = dataKey["NombreArchivo"].ToString();
            
            Response.Redirect("../DescargarArchivo/DescargarArchivo.aspx?fname=CertificacionesContrato/" + nombreArchivo);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnSave_Click(object sender, ImageClickEventArgs e)
    {

        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
                Save(dataIteme.RowIndex);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnAdjuntar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            var dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
            {

                int rowIndex = dataIteme.RowIndex;

                var fuArchivo =
                    (FileUpload)gvCertificacionesContratos.Rows[rowIndex].Cells[(int)grilla.Filearchivo].FindControl("fuArchivo");
                fuArchivo.Visible = true;
                var btnAdjuntar =
                    (ImageButton)gvCertificacionesContratos.Rows[rowIndex].Cells[(int)grilla.Adjuntar].FindControl("btnAdjuntar");
                btnAdjuntar.Visible = false;
                var btnSave =
                    (ImageButton)gvCertificacionesContratos.Rows[rowIndex].Cells[(int)grilla.Guardar].FindControl("btnSave");
                btnSave.Visible = true;
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnMostrar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {

            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;

            if (dataItem != null)
            {
                Show(dataItem.RowIndex);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}