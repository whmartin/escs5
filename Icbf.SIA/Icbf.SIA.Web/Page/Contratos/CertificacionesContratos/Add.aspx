<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_CertificacionesContratos_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIDCertificacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                ID del Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDContrato" ControlToValidate="txtIDContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Fecha Cetificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaCertificacion" ControlToValidate="txtFechaCertificacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIDContrato" MaxLength="10" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIDContrato" runat="server" TargetControlID="txtIDContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaCertificacion"  Rows="3" CssClass="TextBoxGrande" TextMode="MultiLine" onKeyDown="limitText(this,60);" onKeyUp="limitText(this,60);"   MaxLength="60" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
