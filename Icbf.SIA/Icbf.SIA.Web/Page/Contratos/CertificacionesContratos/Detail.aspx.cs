using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad CertificacionesContratos
/// </summary>
public partial class Page_CertificacionesContratos_Detail : GeneralWeb
{
    masterPrincipal _toolBar;
    private const string PageName = "Contratos/CertificacionesContratos";
    readonly ContratoService _vContratoService = new ContratoService();
    

    #region ProcedimientoyFunciones

    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("CertificacionesContratos.IDContratoDeCertificacion"));
            RemoveSessionParameter("CertificacionesContratos.IDContratoDeCertificacion");

            Contrato vDatosContrato = _vContratoService.ConsultarContrato(vIdContrato);
            
            hfIdContrato.Value = vIdContrato.ToString();
            //hfNumeroIdentificacion.Value = vObligacionesCDP.NumeroIdentificacion;


            if (vDatosContrato != null)
            {
                txtIDContrato.Text = vDatosContrato.IdContrato.ToString();
                txtNumeroContrato.Text = vDatosContrato.NumeroContrato;
                if (vDatosContrato.FechaInicioEjecucion != null)
                    TxtFechaInicioContrato.Text = Convert.ToDateTime(vDatosContrato.FechaInicioEjecucion).ToShortDateString();

                if (vDatosContrato.FechaFinalTerminacionContrato != null)
                    TxtFechaFinContrato.Text = Convert.ToDateTime(vDatosContrato.FechaFinalTerminacionContrato).ToShortDateString();


                //ConsultarObligacionesSIGEPSY();
                CArgarObligaciones();
                
                

                var  vListEtado = _vContratoService.ConsultarEstadoContrato(vDatosContrato.IdEstadoContrato, null, null, true);

                if (vListEtado.Count > 0)
                {
                    string codEstadoContrat = vListEtado[0].CodEstado;

                    if (codEstadoContrat == "LIQ")
                    {
                        ddlPersonaCertifica.Visible = false;
                        TxtPersonaCertifica.Visible = true;
                    }
                    else
                    {
                        ddlPersonaCertifica.Visible = true;
                        TxtPersonaCertifica.Visible = false;

                        ddlPersonaCertifica.DataSource =
                            _vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null).ToList();
                        ddlPersonaCertifica.DataValueField = "IDSupervisorIntervContrato";
                        ddlPersonaCertifica.DataTextField = "NombreCompletoSuperInterventor";
                        ddlPersonaCertifica.DataBind();
                    }
                }
                else
                {
                    _toolBar.MostrarMensajeError("Estado del contrato no configurado.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            //int vIDCertificacion = Convert.ToInt32(hfIDCertificacion.Value);

            //CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();
            //vCertificacionesContratos = _vContratoService.ConsultarCertificacionesContratos(vIDCertificacion);
            //InformacionAudioria(vCertificacionesContratos, PageName, vSolutionPage);
            //int vResultado = _vContratoService.EliminarCertificacionesContratos(vCertificacionesContratos);
            //if (vResultado == 0)
            //{
            //    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    _toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            //    SetSessionParameter("CertificacionesContratos.Eliminado", "1");
            //    NavigateTo(SolutionPage.List);
            //}
            //else
            //{
            //    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (masterPrincipal)this.Master;
            if (_toolBar != null)
            {
                _toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
                _toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                _toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
                //_toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

                _toolBar.EstablecerTitulos("Certificaciones Contratos", SolutionPage.Detail.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rbRequiereObligaciones.Items.Insert(0, new ListItem("Si", "True"));
            rbRequiereObligaciones.Items.Insert(0, new ListItem("No", "False"));
            rbRequiereObligaciones.SelectedValue = "0";
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void LlenarGrillaObligaciones()
    {
        try
        {
            var insertObligacionesReportes = new ObligacionesContratoReporte();
            if (txtIDContrato.Text == string.Empty)
                return;

            int idContrato = Convert.ToInt32(txtIDContrato.Text);
            insertObligacionesReportes.IDContrato = idContrato;
            insertObligacionesReportes.DescripcionObligacion = TextObligaciones.Text;
            insertObligacionesReportes.UsuarioCrea = GetSessionUser().NombreUsuario;

            _vContratoService.InsertarObligacionesContratoReporte(insertObligacionesReportes);

            //List<ObligacionesContrato> listObligaciones = (from item in gvObligaciones.Rows.Cast<GridViewRow>()
            //                                               select new ObligacionesContrato
            //                                              {
            //                                                  IdObligacion = Convert.ToInt32(gvObligaciones.DataKeys[item.RowIndex].Value),
            //                                                  Obligacion = item.Cells[0].Text,

            //                                              }).ToList();

            //ObligacionesContrato obligacion = new ObligacionesContrato();
            //obligacion.Obligacion = TextObligaciones.Text;
            //obligacion.IdObligacion = 0;

            //listObligaciones.Add(obligacion);
            CArgarObligaciones();
            TextObligaciones.Text = "";
            
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(_toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }
    protected void btnReporte_Click(object sender, EventArgs e)
    {
        GenerarReporte(true);
    }

    protected void InsertarCertificacion()
    {
        try
        {
            int vResultado;
            CertificacionesContratos vCertificacionesContratos = new CertificacionesContratos();

            vCertificacionesContratos.IDContrato = Convert.ToInt32(txtIDContrato.Text);
            vCertificacionesContratos.FechaCertificacion = DateTime.Now;
            vCertificacionesContratos.Estado = true;

            #region Insertar Certificacion

            vCertificacionesContratos.UsuarioCrea = GetSessionUser().NombreUsuario;
            InformacionAudioria(vCertificacionesContratos, PageName, vSolutionPage);
            vResultado = _vContratoService.InsertarCertificacionesContratos(vCertificacionesContratos);
            #endregion

        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void GenerarReporte(bool pPrevisualizar = false)
    {

        try
        {
            #region Creacion reporte

            //Crea un objeto de tipo reporte
            Report objReport;
            ObligacionesCDP vObligacionesCDP = _vContratoService.ConsultarDatosContratoSigepcyp(Convert.ToInt32( txtIDContrato.Text));


            if (pPrevisualizar)
            {
                objReport = new Report("CertificacionContrato", false, PageName, "Generar Certificacion");
            }
            else
            {
                if(rbExportaExcel.SelectedValue == "No")
                    objReport = new Report("CertificacionContrato", true, PageName, "Generar Certificacion", true, "CertificacionContrato" + txtIDContrato.Text,false);
                else
                    objReport = new Report("CertificacionContrato", true, PageName, "Generar Certificacion", false, "CertificacionContrato" + txtIDContrato.Text,true);
            }

            var miContrato = _vContratoService.ConsultarContrato(int.Parse(txtIDContrato.Text));

            string result = string.Empty;

            if (miContrato.FechaInicioEjecucion.HasValue && miContrato.FechaFinalTerminacionContrato.HasValue)
            {
                ContratoService.ObtenerDiferenciaFechas(miContrato.FechaInicioEjecucion.Value, miContrato.FechaFinalTerminacionContrato.Value, out result);

                char[] split = new[] { '|' };

                string[] itemsFecha = result.Split(split);

                int anios = 0, meses = 0;

                if (! string.IsNullOrEmpty(itemsFecha[1]))
                {
                    anios = int.Parse(itemsFecha[1]) / 12;
                    meses = int.Parse(itemsFecha[1]) % 12;
                }

                string duracion = string.Format("{0} Año(s) {1} Mes(es) {2} Día(s)",anios.ToString(),meses.ToString(),itemsFecha[2]);

                objReport.AddParameter("DuracionTotalContrato", duracion);
            }
            else
                objReport.AddParameter("DuracionTotalContrato", string.Empty);

            if (miContrato.FechaInicioEjecucion.HasValue && miContrato.FechaFinalizacionIniciaContrato.HasValue)
            {
                ContratoService.ObtenerDiferenciaFechas(miContrato.FechaInicioEjecucion.Value, miContrato.FechaFinalizacionIniciaContrato.Value, out result);

                char[] split = new[] { '|' };

                string[] itemsFecha = result.Split(split);

                int anios = 0, meses = 0;

                if (!string.IsNullOrEmpty(itemsFecha[1]))
                {
                    anios = int.Parse(itemsFecha[1]) / 12;
                    meses = int.Parse(itemsFecha[1]) % 12;
                }

                string duracion = string.Format("{0} Año(s) {1} Mes(es) {2} Día(s)", anios.ToString(), meses.ToString(), itemsFecha[2]);

                objReport.AddParameter("DuracionContrato", duracion);
            }
            else
                objReport.AddParameter("DuracionContrato", string.Empty);

           
            //Adiciona los parametros al objeto reporte
            objReport.AddParameter("IDContarto", txtIDContrato.Text);

            if (TxtPersonaCertifica.Visible == false)
            {
                objReport.AddParameter("PersonaCertifica", ddlPersonaCertifica.SelectedItem.Text);
                objReport.AddParameter("TituloPersonaCertifica", "SUPERVISOR  DEL CONTRATO");
            }
            else
            {
                if (TxtPersonaCertifica.Text == string.Empty)
                {
                    _toolBar.MostrarMensajeError("El nombre de la persona que certifica es obligatorio.");
                    return;
                }

                objReport.AddParameter("PersonaCertifica", TxtPersonaCertifica.Text);
                objReport.AddParameter("TituloPersonaCertifica", "DIRECTOR(A) DE CONTRATACIÓN");
            }
            objReport.AddParameter("Obligaciones", rbRequiereObligaciones.SelectedValue);
            objReport.AddParameter("CDPresupuestal", vObligacionesCDP.NumeroCDP.ToString());
            objReport.AddParameter("NumIdentificacion", vObligacionesCDP.NumeroIdentificacion.ToString());
            objReport.AddParameter("AnioVigencia", vObligacionesCDP.AnioVigencia.ToString());
            

            //Crea un session con el objeto Reporte
            SetSessionParameter("Report", objReport);

        
            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
            #endregion
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }


    }

    protected void btnModCertificaciones_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvObligaciones.DataKeys[rowIndex];
            if (dataKey != null)
            {
                int strValue = Convert.ToInt32(dataKey["IDObligacionContrato"].ToString());
                
                var obligacionesDel = new ObligacionesContratoReporte
                {
                    IDObligacionContrato = strValue,
                    IDContrato = Convert.ToInt32(hfIdContrato.Value),
                    DescripcionObligacion = ((TextBox)gvObligaciones.Rows[rowIndex].FindControl("txtDescripcionObligacion")).Text,
                    UsuarioModifica = GetSessionUser().NombreUsuario
            };

                _vContratoService.ModificarObligacionesContratoReporte(obligacionesDel);
                CArgarObligaciones();
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("CertificacionesContratos.IDCertificacion", hfIDCertificacion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        InsertarCertificacion();
        GenerarReporte();

    }
    #endregion

    protected void btnAgregar_Click1(object sender, EventArgs e)
    {
        LlenarGrillaObligaciones();
    }
    protected void rbRequiereObligaciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbRequiereObligaciones.SelectedValue == "True")
        {
            PnlObligaciones.Visible = true;
        }
        else
        {
            PnlObligaciones.Visible = false;
        }
    }
    protected void btnCertificaciones_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvObligaciones.DataKeys[rowIndex];
            if (dataKey != null)
            {
                int strValue = Convert.ToInt32(dataKey["IDObligacionContrato"].ToString());

                var obligacionesDel = new ObligacionesContratoReporte
                {
                    IDObligacionContrato = strValue
                };

                _vContratoService.EliminarObligacionesContratoReporte(obligacionesDel);
                CArgarObligaciones();
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }    

    private void CArgarObligaciones()
    {
        int idContrato = Convert.ToInt32(txtIDContrato.Text);
        ObligacionesCDP vObligacionesCDP = _vContratoService.ConsultarDatosContratoSigepcyp(idContrato);
        List<ObligacionesContratoReporte> ObligacionesSigepcyp = _vContratoService.ConsultarObligacionesSigepcyp(vObligacionesCDP.NumeroCDP, vObligacionesCDP.NumeroIdentificacion, vObligacionesCDP.AnioVigencia);

        if (ObligacionesSigepcyp.Count > 0)
        {
            TextObligaciones.Visible = false;
            btnAgregar.Visible = false;
            
            PnlObligaciones.Visible = true;
            gvObligaciones.DataSource = ObligacionesSigepcyp;
            gvObligaciones.DataBind();
            
            rbRequiereObligaciones.SelectedValue = "True";

            this.gvObligaciones.Columns[1].Visible = false;
            this.gvObligaciones.Columns[2].Visible = false;

        }
        else
        {
            List<ObligacionesContratoReporte> obligaciones = _vContratoService.ConsultarObligacionesContratoReportes(idContrato);

            if (obligaciones.Count > 0)
            {
                PnlObligaciones.Visible = true;
                gvObligaciones.DataSource = obligaciones;
                gvObligaciones.DataBind();
                rbRequiereObligaciones.SelectedValue = "True";
            }
            else
            {
                gvObligaciones.DataSource = obligaciones;
                gvObligaciones.DataBind();
                PnlObligaciones.Visible = false;
                rbRequiereObligaciones.SelectedValue = "False";
            }            
        }
    }

    private void ConsultarObligacionesSIGEPSY()
    {
        try
        {
                ObligacionesCDP vObligacionesCDP = _vContratoService.ConsultarDatosContratoSigepcyp(Convert.ToInt32(hfIdContrato.Value));
                List<ObligacionesContratoReporte> ObligacionesContrato = _vContratoService.ConsultarObligacionesSigepcyp(vObligacionesCDP.NumeroCDP, vObligacionesCDP.NumeroIdentificacion,vObligacionesCDP.AnioVigencia);

                if (ObligacionesContrato.Count > 0)
                {
                    TextObligaciones.Visible = false;
                    btnAgregar.Visible = false;
                }
                
                foreach (ObligacionesContratoReporte Obligacion in ObligacionesContrato)
                {
                    var insertObligacionesReportes = new ObligacionesContratoReporte();           
                        
                    insertObligacionesReportes.IDContrato = Convert.ToInt32(hfIdContrato.Value);
                    insertObligacionesReportes.DescripcionObligacion = Obligacion.DescripcionObligacion;
                    insertObligacionesReportes.IDObligacionSigepcyp = Obligacion.IDObligacionSigepcyp;
                    insertObligacionesReportes.UsuarioCrea = GetSessionUser().NombreUsuario;

                    _vContratoService.InsertarObligacionesContratoReporte(insertObligacionesReportes);
                }
        
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvObligaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvObligaciones.PageIndex = e.NewPageIndex;
        CArgarObligaciones();
    }

}
