using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tipos de cl�usula
/// </summary>
public partial class Page_TipoClausula_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoClausula";
    ContratoService vContratoService = new ContratoService();


    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreTipoClausula = null;
            String vDescripcion = null;
            Boolean? vEstado = null;
            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }
            if (txtNombreTipoClausula.Text!= "")
            {
                vNombreTipoClausula = Convert.ToString(txtNombreTipoClausula.Text);
            }
            if (txtDescripcion.Text != "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }
            gvTipoClausula.DataSource = vContratoService.ConsultarTipoClausulas(vNombreTipoClausula,vDescripcion, vEstado);
            gvTipoClausula.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.LipiarMensajeError();
            gvTipoClausula.PageSize = PageSize();
            gvTipoClausula.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo Cl&#225;usula", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoClausula.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoClausula.IdTipoClausula", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoClausula_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoClausula.SelectedRow);
    }
    protected void gvTipoClausula_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoClausula.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoClausula.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoClausula.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            rblEstado.SelectedIndex = -1;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvTipoClausula_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNombreTipoClausula = null;
        String vDescripcion = null;
        Boolean? vEstado = null;
        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }
        if (txtNombreTipoClausula.Text != "")
        {
            vNombreTipoClausula = Convert.ToString(txtNombreTipoClausula.Text);
        }
        if (txtDescripcion.Text != "")
        {
            vDescripcion = Convert.ToString(txtDescripcion.Text);
        }
        var myGridResults = vContratoService.ConsultarTipoClausulas(vNombreTipoClausula,vDescripcion, vEstado);
        if (myGridResults != null)
        {
            var param = Expression.Parameter(typeof(TipoClausula), e.SortExpression);

            var prop = Expression.Property(param, e.SortExpression);

            var sortExpression = Expression.Lambda<Func<TipoClausula, object>>(Expression.Convert(prop, typeof(object)), param);

            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvTipoClausula.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                gvTipoClausula.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvTipoClausula.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}
