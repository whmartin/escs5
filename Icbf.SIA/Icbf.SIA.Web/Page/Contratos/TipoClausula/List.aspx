<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoClausula_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" style="width: 50%">
                    Nombre Tipo Cl&#225;usula
                </td>
                <td style="width: 50%">
                    Descripci&#243;n Tipo de Cl&#225;usula
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtNombreTipoClausula" Height="25px" MaxLength="50"
                        Width="320px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreTipoClausula" runat="server" TargetControlID="txtNombreTipoClausula"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; "
                        InvalidChars="&#63;" />
                </td>
                <td valign="top">
                    <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="128" TextMode="MultiLine"
                        Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoClausula" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoClausula" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoClausula_PageIndexChanging" OnSelectedIndexChanged="gvTipoClausula_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvTipoClausula_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Nombre Tipo Cl&#225;usula" DataField="NombreTipoClausula"
                                SortExpression="NombreTipoClausula" />--%>
                            <asp:TemplateField HeaderText="Nombre Tipo Cl&#225;usula" ItemStyle-HorizontalAlign="Center" SortExpression="NombreTipoClausula">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreTipoClausula")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="Descripcion" HeaderText="Descripci&#243;n Tipo de Cl&#225;usula"
                                SortExpression="Descripcion" />--%>
                            <asp:TemplateField HeaderText="Descripci&#243;n Tipo de Cl&#225;usula" ItemStyle-HorizontalAlign="Center" SortExpression="Descripcion">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Descripcion")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
