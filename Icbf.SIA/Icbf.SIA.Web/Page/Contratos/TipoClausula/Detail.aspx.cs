using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de tipo cláusula
/// </summary>
public partial class Page_TipoClausula_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoClausula";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("TipoClausula.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoClausula.IdTipoClausula", hfIdTipoClausula.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoClausula = Convert.ToInt32(GetSessionParameter("TipoClausula.IdTipoClausula"));
            RemoveSessionParameter("TipoClausula.IdTipoClausula");

            if (GetSessionParameter("TipoClausula.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoClausula");

            if (GetSessionParameter("TipoClausula.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
            RemoveSessionParameter("TipoClausula.Modificado");

            TipoClausula vTipoClausula = new TipoClausula();
            vTipoClausula = vContratoService.ConsultarTipoClausula(vIdTipoClausula);
            hfIdTipoClausula.Value = vTipoClausula.IdTipoClausula.ToString();
            txtNombreTipoClausula.Text = vTipoClausula.NombreTipoClausula;
            txtDescripcion.Text = vTipoClausula.Descripcion;
            rblEstado.SelectedValue = vTipoClausula.Estado == true ? "1" : "0";
            ObtenerAuditoria(PageName, hfIdTipoClausula.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoClausula.UsuarioCrea, vTipoClausula.FechaCrea, vTipoClausula.UsuarioModifica, vTipoClausula.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoClausula = Convert.ToInt32(hfIdTipoClausula.Value);

            TipoClausula vTipoClausula = new TipoClausula();
            vTipoClausula = vContratoService.ConsultarTipoClausula(vIdTipoClausula);
            InformacionAudioria(vTipoClausula, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarTipoClausula(vTipoClausula);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoClausula.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Cl&#225;usula", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
