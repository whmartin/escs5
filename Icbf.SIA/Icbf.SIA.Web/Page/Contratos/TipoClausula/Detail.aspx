<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoClausula_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoClausula" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Tipo Cl&#225;usula *
            </td>
            <td style="width: 50%">
                Descripci&#243;n Tipo de Cl&#225;usula
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreTipoClausula"  Enabled="false" 
                    Height="25px" MaxLength="50" Width="320px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 372px;
        }
    </style>
</asp:Content>

