<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Pregunta_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdPregunta" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Descripcion de la Pregunta *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionPregunta" ControlToValidate="txtDescripcionPregunta"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoContrato" ControlToValidate="ddlIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoContrato" ControlToValidate="ddlIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionPregunta"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionPregunta" runat="server" TargetControlID="txtDescripcionPregunta"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoContrato"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Componente *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdComponente" ControlToValidate="ddlIdComponente"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdComponente" ControlToValidate="ddlIdComponente"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                SubComponente *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdSubComponente" ControlToValidate="ddlIdSubComponente"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdSubComponente" ControlToValidate="ddlIdSubComponente"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComponente"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdSubComponente"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categoria del Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaContrato" ControlToValidate="ddlIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdCategoriaContrato" ControlToValidate="ddlIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Requiere Documento *
                <asp:RequiredFieldValidator runat="server" ID="rfvRequiereDocumento" ControlToValidate="rblRequiereDocumento"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblRequiereDocumento" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado del SubComponente *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
