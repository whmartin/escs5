<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Pregunta_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdPregunta" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Descripcion de la Pregunta *
            </td>
            <td>
                Tipo contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionPregunta"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoContrato"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Componente *
            </td>
            <td>
                SubComponente *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComponente"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdSubComponente"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categoria del Contrato *
            </td>
            <td>
                Requiere Documento *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblRequiereDocumento" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado del SubComponente *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
