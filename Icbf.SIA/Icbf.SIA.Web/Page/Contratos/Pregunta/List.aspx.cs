using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Pregunta_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Pregunta";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            String vDescripcionPregunta = null;
            int? vIdTipoContrato = null;
            int? vIdComponente = null;
            int? vIdSubComponente = null;
            int? vIdCategoriaContrato = null;
            int? vRequiereDocumento = null;
            int? vEstado = null;
            if (txtDescripcionPregunta.Text!= "")
            {
                vDescripcionPregunta = Convert.ToString(txtDescripcionPregunta.Text);
            }
            if (ddlIdTipoContrato.SelectedValue!= "-1")
            {
                vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            }
            if (ddlIdComponente.SelectedValue!= "-1")
            {
                vIdComponente = Convert.ToInt32(ddlIdComponente.SelectedValue);
            }
            if (ddlIdSubComponente.SelectedValue!= "-1")
            {
                vIdSubComponente = Convert.ToInt32(ddlIdSubComponente.SelectedValue);
            }
            if (ddlIdCategoriaContrato.SelectedValue!= "-1")
            {
                vIdCategoriaContrato = Convert.ToInt32(ddlIdCategoriaContrato.SelectedValue);
            }
            if (rblRequiereDocumento.SelectedValue!= "-1")
            {
                vRequiereDocumento = Convert.ToInt32(rblRequiereDocumento.SelectedValue);
            }
            if (rblEstado.SelectedValue!= "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            gvPregunta.DataSource = vContratoService.ConsultarPreguntas( vDescripcionPregunta, vIdTipoContrato, vIdComponente, vIdSubComponente, vIdCategoriaContrato, vRequiereDocumento, vEstado);
            gvPregunta.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvPregunta.PageSize = PageSize();
            gvPregunta.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Pregunta", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvPregunta.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Pregunta.IdPregunta", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvPregunta_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvPregunta.SelectedRow);
    }
    protected void gvPregunta_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPregunta.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Pregunta.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Pregunta.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdTipoContrato.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdComponente.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdSubComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdSubComponente.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdCategoriaContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdCategoriaContrato.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblRequiereDocumento.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblRequiereDocumento.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
