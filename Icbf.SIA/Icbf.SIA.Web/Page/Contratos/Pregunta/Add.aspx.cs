using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Pregunta_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/Pregunta";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            Pregunta vPregunta = new Pregunta();

            vPregunta.DescripcionPregunta = Convert.ToString(txtDescripcionPregunta.Text);
            vPregunta.IdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            vPregunta.IdComponente = Convert.ToInt32(ddlIdComponente.SelectedValue);
            vPregunta.IdSubComponente = Convert.ToInt32(ddlIdSubComponente.SelectedValue);
            vPregunta.IdCategoriaContrato = Convert.ToInt32(ddlIdCategoriaContrato.SelectedValue);
            vPregunta.RequiereDocumento = Convert.ToInt32(rblRequiereDocumento.SelectedValue);
            vPregunta.Estado = Convert.ToInt32(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vPregunta.IdPregunta = Convert.ToInt32(hfIdPregunta.Value);
                vPregunta.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vPregunta, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarPregunta(vPregunta);
            }
            else
            {
                vPregunta.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vPregunta, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarPregunta(vPregunta);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Pregunta.IdPregunta", vPregunta.IdPregunta);
                SetSessionParameter("Pregunta.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Pregunta", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdPregunta = Convert.ToInt32(GetSessionParameter("Pregunta.IdPregunta"));
            RemoveSessionParameter("Pregunta.Id");

            Pregunta vPregunta = new Pregunta();
            vPregunta = vContratoService.ConsultarPregunta(vIdPregunta);
            hfIdPregunta.Value = vPregunta.IdPregunta.ToString();
            txtDescripcionPregunta.Text = vPregunta.DescripcionPregunta;
            ddlIdTipoContrato.SelectedValue = vPregunta.IdTipoContrato.ToString();
            ddlIdComponente.SelectedValue = vPregunta.IdComponente.ToString();
            ddlIdSubComponente.SelectedValue = vPregunta.IdSubComponente.ToString();
            ddlIdCategoriaContrato.SelectedValue = vPregunta.IdCategoriaContrato.ToString();
            rblRequiereDocumento.SelectedValue = vPregunta.RequiereDocumento.ToString();
            rblEstado.SelectedValue = vPregunta.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vPregunta.UsuarioCrea, vPregunta.FechaCrea, vPregunta.UsuarioModifica, vPregunta.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdTipoContrato.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdComponente.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdSubComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdSubComponente.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdCategoriaContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdCategoriaContrato.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblRequiereDocumento.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblRequiereDocumento.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
