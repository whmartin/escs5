<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Pregunta_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Descripcion de la Pregunta *
            </td>
            <td>
                Tipo contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionPregunta"  ></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Componente *
            </td>
            <td>
                SubComponente *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComponente"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdSubComponente"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categoria del Contrato *
            </td>
            <td>
                Requiere Documento *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato"  ></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblRequiereDocumento" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado del SubComponente *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvPregunta" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdPregunta" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvPregunta_PageIndexChanging" OnSelectedIndexChanged="gvPregunta_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Descripcion de la Pregunta" DataField="DescripcionPregunta" />
                            <asp:BoundField HeaderText="Tipo contrato" DataField="IdTipoContrato" />
                            <asp:BoundField HeaderText="Componente" DataField="IdComponente" />
                            <asp:BoundField HeaderText="SubComponente" DataField="IdSubComponente" />
                            <asp:BoundField HeaderText="Categoria del Contrato" DataField="IdCategoriaContrato" />
                            <asp:BoundField HeaderText="Requiere Documento" DataField="RequiereDocumento" />
                            <asp:BoundField HeaderText="Estado del SubComponente" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
