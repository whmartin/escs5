using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Pregunta_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Pregunta";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Pregunta.IdPregunta", hfIdPregunta.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdPregunta = Convert.ToInt32(GetSessionParameter("Pregunta.IdPregunta"));
            RemoveSessionParameter("Pregunta.IdPregunta");

            if (GetSessionParameter("Pregunta.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Pregunta");


            Pregunta vPregunta = new Pregunta();
            vPregunta = vContratoService.ConsultarPregunta(vIdPregunta);
            hfIdPregunta.Value = vPregunta.IdPregunta.ToString();
            txtDescripcionPregunta.Text = vPregunta.DescripcionPregunta;
            ddlIdTipoContrato.SelectedValue = vPregunta.IdTipoContrato.ToString();
            ddlIdComponente.SelectedValue = vPregunta.IdComponente.ToString();
            ddlIdSubComponente.SelectedValue = vPregunta.IdSubComponente.ToString();
            ddlIdCategoriaContrato.SelectedValue = vPregunta.IdCategoriaContrato.ToString();
            rblRequiereDocumento.SelectedValue = vPregunta.RequiereDocumento.ToString();
            rblEstado.SelectedValue = vPregunta.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdPregunta.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vPregunta.UsuarioCrea, vPregunta.FechaCrea, vPregunta.UsuarioModifica, vPregunta.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdPregunta = Convert.ToInt32(hfIdPregunta.Value);

            Pregunta vPregunta = new Pregunta();
            vPregunta = vContratoService.ConsultarPregunta(vIdPregunta);
            InformacionAudioria(vPregunta, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarPregunta(vPregunta);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Pregunta.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Pregunta", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIdTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdTipoContrato.SelectedValue = "-1";
            ddlIdComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdComponente.SelectedValue = "-1";
            ddlIdSubComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdSubComponente.SelectedValue = "-1";
            ddlIdCategoriaContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdCategoriaContrato.SelectedValue = "-1";
            rblRequiereDocumento.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblRequiereDocumento.SelectedValue = "-1";
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
