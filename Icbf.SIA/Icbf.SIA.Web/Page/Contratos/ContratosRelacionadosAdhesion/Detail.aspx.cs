using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;

/// <summary>
/// P�gina que despliega el detalle del registro de contratos relacionaos
/// </summary>
public partial class Page_LugarEjecucionContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ContratosRelacionadosAdhesion";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("RelacionContrato.IdContratoMaestro", hdIDContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdContratomaestro = Convert.ToInt32(GetSessionParameter("RelacionContrato.IdContratoMaestro"));
            RemoveSessionParameter("RelacionContrato.IdContratoMaestro");
            RemoveSessionParameter("RelacionContrato");

            var vContrato = vContratoService.ConsultarContrato(vIdContratomaestro);
            hdIDContrato.Value = vContrato.IdContrato.ToString();
            txtNumeroContrato.Text = vContrato.NumeroContrato;
            gvContratosAsociados.DataSource = vContratoService.ConsultarRelacionContrato(vContrato.IdContrato);
            gvContratosAsociados.DataBind();

            ObtenerAuditoria(PageName, hdIDContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Contratos Relacionados", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvContratosAsociados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratosAsociados.DataSource = vContratoService.ConsultarRelacionContrato(Convert.ToInt32(this.hdIDContrato.Value));
        gvContratosAsociados.DataBind();
    }

    protected void gvContratosAsociados_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label LblIdContratomaestro = ((Label)e.Row.FindControl("LblIdContratomaestro"));
            Label LblIdContratoAsociado = ((Label)e.Row.FindControl("LblIdContratoAsociado"));
            Label LblNumeroContratoMaestro = ((Label)e.Row.FindControl("LblNumeroContratoMaestro"));
            Label LblNumeroContratoAsociado = ((Label)e.Row.FindControl("LblNumeroContratoAsociado"));

            if (LblIdContratomaestro.Text != string.Empty)
            {
                var vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(LblIdContratomaestro.Text));
                LblNumeroContratoMaestro.Text = vContrato.NumeroContrato;
            }
            if (LblIdContratoAsociado.Text != string.Empty)
            {
                var vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(LblIdContratoAsociado.Text));
                LblNumeroContratoAsociado.Text = vContrato.NumeroContrato;
            }
        }
    }
}
