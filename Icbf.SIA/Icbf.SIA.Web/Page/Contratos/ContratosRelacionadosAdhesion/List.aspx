<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contrato_List" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&#250;mero Contrato
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtNumeroContrato" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContratoMaestro" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContrato_PageIndexChanging" OnSelectedIndexChanged="gvContrato_SelectedIndexChanged"
                        OnRowDataBound="gvContrato_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contrato Principal">
                                <ItemTemplate>
                                    <asp:Label ID="LblIdContratomaestro" runat="server" Text='<%# Bind("IdContratoMaestro")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="LblNumeroContratoMaestro" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contrato Asociado">
                                <ItemTemplate>
                                    <asp:Label ID="LblIdContratoAsociado" runat="server" Text='<%# Bind("IdContratoAsociado")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="LblNumeroContratoAsociado" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Asociacion">
                                <ItemTemplate>
                                    <asp:Label ID="LblFechaAsociacion" runat="server" Text='<%# Bind("FechaAsociacion","{0:dd/MM/yyyy hh:mm:ss tt}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
