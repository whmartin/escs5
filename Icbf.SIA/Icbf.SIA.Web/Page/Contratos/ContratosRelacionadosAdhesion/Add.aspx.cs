using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using AjaxControlToolkit;

/// <summary>
/// Página de registro y edición de lugares de contratos relacionados
/// </summary>
public partial class Page_LugarEjecucionContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/ContratosRelacionadosAdhesion";

    public List<RelacionContrato> LstRelacionContrato { get { return (List<RelacionContrato>)ViewState["LstRelacionContrato"]; } set { ViewState["LstRelacionContrato"] = value; } }

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botón Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botón Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Relaci&#243;n Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos provenientes de la entidad a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdContratoMaestro = Convert.ToInt32(GetSessionParameter("RelacionContrato.IdContratoMaestro"));
            RemoveSessionParameter("RelacionContrato.IdContratoMaestro");
            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato(vIdContratoMaestro);
            this.hdIDContrato.Value = vIdContratoMaestro.ToString();
            gvContratosAsociados.DataSource = vContratoService.ConsultarRelacionContrato(vIdContratoMaestro);
            gvContratosAsociados.DataBind();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvContratosAsociados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratosAsociados.DataSource = vContratoService.ConsultarRelacionContrato(Convert.ToInt32(this.hdIDContrato.Value));
        gvContratosAsociados.DataBind();
    }
    protected void btnAgregarContrato_Click(object sender, EventArgs e)
    {
        try
        {
            int vResultado;
            RelacionContrato vRelacionContrato = new RelacionContrato();
            vRelacionContrato.IdContratoMaestro = Convert.ToInt32(this.hdIDContrato.Value);
            vRelacionContrato.IdContratoAsociado = Convert.ToInt32(this.HdIDContratoAdhesion.Value);
            vRelacionContrato.FechaAsociacion = DateTime.Now;
            vRelacionContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
            vResultado = vContratoService.InsertarRelacionContrato(vRelacionContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeGuardado();
                gvContratosAsociados.DataSource = vContratoService.ConsultarRelacionContrato(Convert.ToInt32(this.hdIDContrato.Value));
                gvContratosAsociados.DataBind();
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContratosAsociados_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label LblIdContratomaestro = ((Label)e.Row.FindControl("LblIdContratomaestro"));
            Label LblIdContratoAsociado = ((Label)e.Row.FindControl("LblIdContratoAsociado"));
            Label LblNumeroContratoMaestro = ((Label)e.Row.FindControl("LblNumeroContratoMaestro"));
            Label LblNumeroContratoAsociado = ((Label)e.Row.FindControl("LblNumeroContratoAsociado"));

            if (LblIdContratomaestro.Text != string.Empty)
            {
                var vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(LblIdContratomaestro.Text));
                LblNumeroContratoMaestro.Text = vContrato.NumeroContrato;
            }
            if (LblIdContratoAsociado.Text != string.Empty)
            {
               var vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(LblIdContratoAsociado.Text));
               LblNumeroContratoAsociado.Text = vContrato.NumeroContrato;
            }
        }
    }
    protected void gvContratosAsociados_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int vIdContratomaestro, vIdContratoAsociado;
        Label LblIdContratomaestro = (Label)gvContratosAsociados.Rows[e.RowIndex].FindControl("LblIdContratomaestro");
        Label LblIdContratoAsociado = (Label)gvContratosAsociados.Rows[e.RowIndex].FindControl("LblIdContratoAsociado");
        vIdContratomaestro = Convert.ToInt32(LblIdContratomaestro.Text);
        vIdContratoAsociado = Convert.ToInt32(LblIdContratoAsociado.Text);
        RelacionContrato vRelacionContrato = new RelacionContrato();
        vRelacionContrato.IdContratoMaestro = vIdContratomaestro;
        vRelacionContrato.IdContratoAsociado = vIdContratoAsociado;
        vContratoService.EliminarRelacionContrato(vRelacionContrato);
        toolBar.MostrarMensajeEliminado();
        gvContratosAsociados.DataSource = vContratoService.ConsultarRelacionContrato(vIdContratomaestro);
        gvContratosAsociados.DataBind();
    }
    
}
