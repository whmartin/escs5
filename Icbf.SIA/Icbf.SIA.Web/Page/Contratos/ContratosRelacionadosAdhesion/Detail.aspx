<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_LugarEjecucionContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de contrato Marco</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField ID="hdIDContrato" runat="server" />
                <asp:TextBox runat="server" ID="txtNumeroContrato"  Enabled="false"></asp:TextBox>
            
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
     <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratosAsociados" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContratoMaestro" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContratosAsociados_PageIndexChanging" 
                        onrowdatabound="gvContratosAsociados_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Contrato Marco">
                                <ItemTemplate>
                                    <asp:Label ID="LblIdContratomaestro" runat="server" Text='<%# Bind("IdContratoMaestro")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="LblNumeroContratoMaestro" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contrato Asociado">
                                <ItemTemplate>
                                    <asp:Label ID="LblIdContratoAsociado" runat="server" Text='<%# Bind("IdContratoAsociado")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="LblNumeroContratoAsociado" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Asociacion">
                                <ItemTemplate>
                                    <asp:Label ID="LblFechaAsociacion" runat="server" Text='<%# Bind("FechaAsociacion","{0:dd/MM/yyyy hh:mm:ss tt}")%>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
