<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_LugarEjecucionContrato_Add" enableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContratoLugarEjecucion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtIDContrato" ControlToValidate="txtIDContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="AgregarContrato"
                 ForeColor="Red"></asp:RequiredFieldValidator></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContrato" />
                <asp:UpdatePanel ID="upnlContrato" runat="server">
                    <ContentTemplate>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtIDContrato" Enabled="false" Width="200px"></asp:TextBox>
                        <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetContrato('hdIDContrato,txtIDContrato','0,3')" Style="cursor: hand" ToolTip="Buscar" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Contrato Adhesión<asp:RequiredFieldValidator runat="server" 
                    ID="rfvtxtIDContratoAdhesion" ControlToValidate="txtIDContratoAdhesion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="AgregarContrato"
                 ForeColor="Red"></asp:RequiredFieldValidator></td>
                <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HdIDContratoAdhesion" />
                <asp:UpdatePanel ID="upnlConatratoAdhesion" runat="server">
                    <ContentTemplate>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtIDContratoAdhesion" Enabled="false" Width="200px"></asp:TextBox>
                        <asp:Image ID="imgBcodigoUsuario0" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                            OnClick="GetContratoClase('HdIDContratoAdhesion,txtIDContratoAdhesion','0,3','A')" 
                            Style="cursor: hand" ToolTip="Buscar" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </td>
        </tr>
    </table>
    
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowA">
                <td>
                     
                        <asp:Button ID="btnAgregarContrato" runat="server" Text="Agregar Contrato" 
                                onclick="btnAgregarContrato_Click" Width="150px" ValidationGroup="AgregarContrato" />
                   
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratosAsociados" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContratoMaestro" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContratosAsociados_PageIndexChanging" 
                        onrowdatabound="gvContratosAsociados_RowDataBound" 
                        onrowdeleting="gvContratosAsociados_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Contrato Principal">
                                <ItemTemplate>
                                    <asp:Label ID="LblIdContratomaestro" runat="server" Text='<%# Bind("IdContratoMaestro")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="LblNumeroContratoMaestro" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contrato Asociado">
                                <ItemTemplate>
                                    <asp:Label ID="LblIdContratoAsociado" runat="server" Text='<%# Bind("IdContratoAsociado")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="LblNumeroContratoAsociado" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Asociacion">
                                <ItemTemplate>
                                    <asp:Label ID="LblFechaAsociacion" runat="server" Text='<%# Bind("FechaAsociacion","{0:dd/MM/yyyy hh:mm:ss tt}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Delete" OnClientClick="return confirm('Esta seguro de Eliminar el registro');" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Eliminar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
</asp:Content>
