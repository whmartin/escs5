using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de contratos de adhesi�n
/// </summary>
public partial class Page_Contrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ContratosRelacionadosAdhesion";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
        SetSessionParameter("Contrato.Clase", "N");
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            string vNumeroContrato = null;
            if(this.txtNumeroContrato.Text.Trim() != string.Empty)
                vNumeroContrato = this.txtNumeroContrato.Text;
            gvContrato.DataSource = vContratoService.ConsultarRelacionContratos(vNumeroContrato);
            gvContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvContrato.PageSize = PageSize();
            gvContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("RelacionContrato.IdContratoMaestro", strValue);
            
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContrato.SelectedRow);
    }
    protected void gvContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvContrato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label LblIdContratomaestro = ((Label)e.Row.FindControl("LblIdContratomaestro"));
            Label LblIdContratoAsociado = ((Label)e.Row.FindControl("LblIdContratoAsociado"));
            Label LblNumeroContratoMaestro = ((Label)e.Row.FindControl("LblNumeroContratoMaestro"));
            Label LblNumeroContratoAsociado = ((Label)e.Row.FindControl("LblNumeroContratoAsociado"));

            if (LblIdContratomaestro.Text != string.Empty)
            {
                var vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(LblIdContratomaestro.Text));
                LblNumeroContratoMaestro.Text = vContrato.NumeroContrato;
            }
            if (LblIdContratoAsociado.Text != string.Empty)
            {
                var vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(LblIdContratoAsociado.Text));
                LblNumeroContratoAsociado.Text = vContrato.NumeroContrato;
            }
        }
    }
}
