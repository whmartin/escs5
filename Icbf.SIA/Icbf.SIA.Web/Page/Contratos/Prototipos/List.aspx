<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contrato_Add" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

<asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlContrato">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
            Nombre de Usuario*
            </td>
            <td>
            Cargo del Usuario*
            </td>
        </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtNombreUsuario0" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtCargo0" runat="server"></asp:TextBox>
                </td>
            </tr>
        
    </table>
        <table align="center" width="90%">
            <tr class="rowB">
                <td>
                    Regional de usuario*
                </td>
                <td>
                    Id Contrato / Convenio*
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtNombreUsuario" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtCargo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">
                    Fecha Registro Al Sistema *
                </td>
                <td style="width: 50%">
                    Contrato Asociado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha ID="fechaRegistro" runat="server" />
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox1" Text="Convenio Marco" runat="server" />
                    <br />
                    <asp:CheckBox ID="CheckBox2" Text="Contrato / Convenio Adhesión" runat="server" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Número del Convenio Marco Asociado-
                    <p />
                    Numero Contrato / Convenio Adhesión *
                </td>
                <td>
                    Categoria Contrato / Convenio *
                    <asp:RequiredFieldValidator ID="rfvddlModalidad" runat="server" 
                        ControlToValidate="ddlModalidad" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                        ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvddlModalidad" runat="server" 
                        ControlToValidate="ddlModalidad" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                        SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtNumeroContrato" runat="server" Enabled="false" 
                        MaxLength="25"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList ID="ddlModalidad" runat="server">
                        <asp:ListItem>Selecciones</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Tipo de Contrato/ Convenio *</td>
                <td>
                    Modalidad Académica *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlModalidadAcademica0" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlModalidadAcademica" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre de la Profeción *
                    <asp:RequiredFieldValidator ID="rfvdllIdCategoriaContrato" runat="server" 
                        ControlToValidate="dllIdCategoriaContrato" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                        ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvdllIdCategoriaContrato" runat="server" 
                        ControlToValidate="dllIdCategoriaContrato" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                        SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
                </td>
                <td>
                    Modalidad de la Selección *
                    <asp:RequiredFieldValidator ID="rfvdllIdTipoContrato" runat="server" 
                        ControlToValidate="dllIdTipoContrato" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                        ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvdllIdTipoContrato" runat="server" 
                        ControlToValidate="dllIdTipoContrato" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                        SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="dllIdCategoriaContrato" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="dllIdTipoContrato" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Numero de Procesos *
                </td>
                <td>
                    Fecha Adjudicación del proceso *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="TextBox1" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                        ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToValidate="TextBox1" Display="Dynamic" 
                        ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                        SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
                </td>
                <td>
                    <uc1:fecha ID="fechaRegistro0" runat="server" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Requiere Acta de Inicio *</td>
                <td>
                    Maneja aportes *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:CheckBox ID="CheckBox3" runat="server" Text="Si" />
                    <br />
                    <asp:CheckBox ID="CheckBox4" runat="server" Text="No" />                    
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox5" runat="server" Text="Si" />
                    <br />
                    <asp:CheckBox ID="CheckBox6" runat="server" Text="No" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Maneja Recursos *</td>
                <td>
                    Régimen de Contratación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:CheckBox ID="CheckBox7" runat="server" Text="Si" />
                   
                    <br />
                    <asp:CheckBox ID="CheckBox8" runat="server" Text="No" />
                    
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                   
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre del Solicitanten*</td>
                <td>
                    Regional Contrato / Convenio *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    <asp:Image ID="imgBuscarPersona" runat="server" Height="20px" 
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" onClick="GetPersona()" 
                        Style="cursor: hand" ToolTip="Buscar" Width="20px" />
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Dependencia Solicitante</td>
                <td>
                    Cargo Solicitante</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre del Ordenador del Gasto*</td>
                <td>
                    Tipo Identificación ordenador del Gasto</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    <asp:Image ID="imgBuscarPersona0" runat="server" Height="20px" 
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" onClick="GetPersona()" 
                        Style="cursor: hand" ToolTip="Buscar" Width="20px" />
                </td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Numero Identificacion Ordenador del Gasto</td>
                <td>
                    Cargo Ordenador del Gato</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    CDP</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                    <asp:Image ID="imgBuscarPersona1" runat="server" Height="20px" 
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" onClick="GetPersona()" 
                        Style="cursor: hand" ToolTip="Buscar" Width="20px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Numero del CDP" HeaderText="Numero del CDP" />
                            <asp:BoundField FooterText="Fecha Expedición CDP" 
                                HeaderText="Fecha Expedición CDP" />
                            <asp:BoundField FooterText="Valor CDP" HeaderText="Valor CDP" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    Valor total CDP
                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Rubros CDP </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas0" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Numero consecutivo plan de compras" 
                                HeaderText="Numero consecutivo plan de compras" />
                            <asp:BoundField FooterText="Codigo rubro" HeaderText="Codigo rubro" />
                            <asp:BoundField FooterText="Descripcion del rubro" 
                                HeaderText="Descripcion del rubro" />
                            <asp:BoundField FooterText="Valor del rubro" HeaderText="Valor del rubro" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    Plan de Compras Asociado</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                    <asp:Image ID="imgBuscarPersona3" runat="server" Height="20px" 
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" onClick="GetPersona()" 
                        Style="cursor: hand" ToolTip="Buscar" Width="20px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas1" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Numero consecutivo plan de compras" 
                                HeaderText="Numero consecutivo plan de compras" />
                            <asp:BoundField FooterText="Vigencia" HeaderText="Vigencia" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Productos Contrato / Convenio</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td class="style1" colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas2" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Numero Consecutivo Plan de Compra" 
                                HeaderText="Numero Consecutivo Plan de Compra" />
                            <asp:BoundField FooterText="Codigo del Producto" 
                                HeaderText="Codigo del Producto" />
                            <asp:BoundField FooterText="Nombre del Producto" 
                                HeaderText="Nombre del Producto" />
                            <asp:BoundField FooterText="Tipo Producto" HeaderText="Tipo Producto" />
                            <asp:BoundField FooterText="Cantidad /Cupos" HeaderText="Cantidad /Cupos" />
                            <asp:BoundField FooterText="Valor Unitario" HeaderText="Valor Unitario" />
                            <asp:BoundField FooterText="Valor Total" HeaderText="Valor Total" />
                            <asp:BoundField FooterText="Tiempo" HeaderText="Tiempo" />
                            <asp:BoundField FooterText="Unidad de Tiempo" HeaderText="Unidad de Tiempo" />
                            <asp:BoundField FooterText="Unidad de Medida" HeaderText="Unidad de Medida" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Consecutivo Plan de Compra Contratos</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td>
                    Objeto del Contrato / Convenio</td>
                <td>
                    Alcancel del Objeto del Contrato / Convenio</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TextBox14" runat="server" TextMode="multiline" Rows="5" Width="350px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBox15" runat="server" TextMode="multiline" Rows="5" Width="350px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Valor Inicial Contrato / Convenio</td>
                <td>
                    Fecha de Inicio de Ejecucion del Contrato / Convenio</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox16" runat="server" Width="205px"></asp:TextBox>
                </td>
                <td style="text-align: left">
                    <uc1:fecha ID="fechaRegistro1" runat="server" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha de Finalización Inicial Contrato / Convenio</td>
                <td>
                    Plazo Inicial de Ejecucion</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <uc1:fecha ID="fechaRegistro2" runat="server" />
                </td>
                <td style="text-align: left">
                    Dias&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                    <br />
                    Meses&nbsp;&nbsp;
                    <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
                    <br />
                    Años&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha Final de Terminacion Contrato / Convenio*</td>
                <td>
                    Maneja Vigencia Futuras*</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <uc1:fecha ID="fechaRegistro3" runat="server" />
                </td>
                <td style="text-align: left">
                    <asp:CheckBox ID="CheckBox9" runat="server" Text="Si" />
                    <br />
                    <asp:CheckBox ID="CheckBox10" runat="server" Text="No" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Vigencia Fiscal Inicial Contrato / Convenio</td>
                <td>
                    Vigencia Fiscal Final Contrato / Convenio</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox20" runat="server" Width="205px"></asp:TextBox>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox21" runat="server" Width="205px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Forma de Pago</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:DropDownList ID="DropDownList3" runat="server">
                        <asp:ListItem>Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    Lugar de Ejecucion</td>
                <td>
                    Datos Adicionales Lugar Ejecucion</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox22" runat="server" Width="205px"></asp:TextBox>
                    <asp:Image ID="imgBuscarPersona4" runat="server" Height="20px" 
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" onClick="GetPersona()" 
                        Style="cursor: hand" ToolTip="Buscar" Width="20px" />
                </td>
                <td>
                    <asp:TextBox ID="TextBox23" runat="server" Rows="4" TextMode="multiline" 
                        Width="350px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    <asp:CheckBox ID="CheckBox11" runat="server" Text="Nivel Nacional" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left" colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas3" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Departamento" HeaderText="Departamento" />
                            <asp:BoundField FooterText="Municipio" HeaderText="Municipio" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    Contratista *</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox24" runat="server" Width="205px"></asp:TextBox>
                    <asp:Image ID="imgBuscarPersona5" runat="server" Height="20px" 
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" onClick="GetPersona()" 
                        Style="cursor: hand" ToolTip="Buscar" Width="20px" />
                </td>
                <td style="text-align: left">
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    Contratistas Relacionados</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left" colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas4" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Tipo Persona" HeaderText="Tipo Persona" />
                            <asp:BoundField FooterText="Tipo Identificación" 
                                HeaderText="Tipo Identificación" />
                            <asp:BoundField FooterText="Numero Identificación" 
                                HeaderText="Numero Identificación" />
                            <asp:BoundField FooterText="Información Contratista" 
                                HeaderText="Información Contratista" />
                            <asp:BoundField FooterText="Tipo Identificación Representante Legal" 
                                HeaderText="Tipo Identificación Representante Legal" />
                            <asp:BoundField FooterText="Numero Identificación Representante Legal" 
                                HeaderText="Numero Identificación Representante Legal" />
                            <asp:BoundField FooterText="Información Representante Legal" 
                                HeaderText="Información Representante Legal" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    Tipo Person y/o Organizacion</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:CheckBox ID="CheckBox12" runat="server" Text="Consorcio" />
                    <br />
                    <asp:CheckBox ID="CheckBox13" runat="server" Text="Union Temporal" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left" colspan="2">
                    Integrantes Relacionados al Consorcio o union Temporal</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left" colspan="2">
                    <asp:GridView ID="gvRelacionarContratistas5" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="0" 
                        DataKeyNames="IdContratistaContrato" GridLines="None" Height="16px" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField FooterText="Identificación Contratista" 
                                HeaderText="Identificación Contratista" />
                            <asp:BoundField FooterText="Tipo Persona" HeaderText="Tipo Persona" />
                            <asp:BoundField FooterText="Tipo Identificación" 
                                HeaderText="Tipo Identificación" />
                            <asp:BoundField FooterText="Numero Identificación" 
                                HeaderText="Numero Identificación" />
                            <asp:BoundField FooterText="Información Integrante" 
                                HeaderText="Información Integrante" />
                            <asp:BoundField FooterText="Porcentaje Participación" 
                                HeaderText="Porcentaje Participación" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    Valor Aporte ICBF</td>
                <td>
                    Valor Aporte Contratista</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td style="text-align: left">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="rowB">
                <td style="text-align: left">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            height: 21px;
        }
    </style>
</asp:Content>

