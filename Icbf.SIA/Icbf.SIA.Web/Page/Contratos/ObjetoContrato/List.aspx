<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ObjetoContrato_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td style="width: 50%">
                Objeto Contractual
            </td>
            <td style="width: 50%">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContractual" Height="50px" 
                    TextMode="MultiLine" Width="420px"   onKeyDown="limitText(this,500);" 
                    onKeyUp="limitText(this,500);"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="ftObjetoContractual" runat="server" TargetControlID="txtObjetoContractual"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " InvalidChars="&#63;"/>--%>
                <Ajax:FilteredTextBoxExtender ID="ftObjetoContractual" runat="server" TargetControlID="txtObjetoContractual"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="������������,.;:!=?��#$%&/()=�?+*-�'\ " />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObjetoContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdObjetoContratoContractual" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvObjetoContrato_PageIndexChanging" 
                        OnSelectedIndexChanged="gvObjetoContrato_SelectedIndexChanged" 
                        AllowSorting="True" onsorting="gvObjetoContrato_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Objeto Contractual" ItemStyle-HorizontalAlign="Center" SortExpression="ObjetoContractual" >
                                    <ItemTemplate>
                                         <div style="word-wrap: break-word; width: 300px;">
                                                <%#Eval("ObjetoContractual")%>
                                         </div>
                                     </ItemTemplate>
                                     </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />	
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
