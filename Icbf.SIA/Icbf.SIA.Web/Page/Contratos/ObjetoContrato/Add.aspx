<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ObjetoContrato_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdObjetoContratoContractual" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto Contractual *
                <asp:RequiredFieldValidator runat="server" ID="rfvObjetoContractual" ControlToValidate="txtObjetoContractual"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 50%">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtObjetoContractual" Height="50px" 
                    TextMode="MultiLine" Width="420px" onKeyDown="limitText(this,500);" 
                    onKeyUp="limitText(this,500);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjetoContractual" runat="server" TargetControlID="txtObjetoContractual"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 441px;
        }
    </style>
</asp:Content>

