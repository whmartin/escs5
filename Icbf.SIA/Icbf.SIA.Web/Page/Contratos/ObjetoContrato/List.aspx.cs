using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de objetos contrato
/// </summary>
public partial class Page_ObjetoContrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ObjetoContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdObjetoContratoContractual = null;
            String vObjetoContractual = null;
            Boolean? vEstado = null;
            
            if (txtObjetoContractual.Text!= "")
            {
                vObjetoContractual = Convert.ToString(txtObjetoContractual.Text);
            }
            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }
            gvObjetoContrato.DataSource = vContratoService.ConsultarObjetoContratos( vIdObjetoContratoContractual, vObjetoContractual, vEstado);
            gvObjetoContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvObjetoContrato.PageSize = PageSize();
            gvObjetoContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Objeto Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvObjetoContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ObjetoContrato.IdObjetoContratoContractual", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvObjetoContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvObjetoContrato.SelectedRow);
    }
    protected void gvObjetoContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvObjetoContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ObjetoContrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ObjetoContrato.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedIndex = -1;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvObjetoContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        int? vIdObjetoContratoContractual = null;
        String vObjetoContractual = null;
        Boolean? vEstado = null;
        //if (hfIdObjetoContratoContractual.Value!= "")
        //{
        //    vIdObjetoContratoContractual = Convert.ToInt32(hfIdObjetoContratoContractual.Value);
        //}
        if (txtObjetoContractual.Text != "")
        {
            vObjetoContractual = Convert.ToString(txtObjetoContractual.Text);
        }
        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }

        var myGridResults =  vContratoService.ConsultarObjetoContratos(vIdObjetoContratoContractual, vObjetoContractual, vEstado);
        if (myGridResults != null)
        {
            var param = Expression.Parameter(typeof(ObjetoContrato), e.SortExpression);

            var prop = Expression.Property(param, e.SortExpression);

            var sortExpression = Expression.Lambda<Func<ObjetoContrato, object>>(Expression.Convert(prop, typeof(object)), param);

            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvObjetoContrato.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                gvObjetoContrato.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvObjetoContrato.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}
