﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddDetalle.aspx.cs" Inherits="Page_Contratos_SupervisionConfiguracionCriterio_AddDetalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="dvListaCriterios" runat="server" visible="false">
        <fieldset>
            <legend>Criterios:</legend>
            <asp:CheckBoxList ID="chklCriterios" runat="server">
            </asp:CheckBoxList>
        </fieldset>
    </div>
    <div id="dvRango" runat="server" visible="false">
        <fieldset>
            <legend>Rango:</legend>
            <table style="width: 100%">
                <tr>
                    <td>
                        <label>
                            Valor mínimo criterios cumplidos*</label>
                    </td>
                    <td>
                        <label>
                            Valor m&aacute;ximo criterios cumplidos*</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="txtMinimo" runat="server" type="text" style="width: 250px" />
                    </td>
                    <td>
                        <input id="txtMaximo" runat="server" type="text" style="width: 250px" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</body>
</html>
