﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_Contratos_SupervisionConfiguracionCriterio_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionCriterio";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }


    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionConfiguracionCriterios.IdCriterioConf", hfIdConfCriterio.Value);
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        //try
        //{
        //    int vIdCriterio = Convert.ToInt32(GetSessionParameter("SupervisionCriterios.IdCriterio"));
        //    RemoveSessionParameter("SupervisionCriterios.IdCriterio");

        //    if (GetSessionParameter("SupervisionCriterios.Guardado").ToString() == "1")
        //        toolBar.MostrarMensajeGuardado();
        //    RemoveSessionParameter("SupervisionCriterios");


        //    SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
        //    vSupervisionCriterios = vSupervisionService.ConsultarSupervisionCriterios(vIdCriterio);


        //    rblEstado.SelectedValue = vSupervisionCriterios.Estado.ToString();

        //    ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionCriterios.UsuarioCrea, vSupervisionCriterios.FechaCrea, vSupervisionCriterios.UsuarioModifica, vSupervisionCriterios.FechaModifica);
        //}
        //catch (UserInterfaceException ex)
        //{
        //    toolBar.MostrarMensajeError(ex.Message);
        //}
        //catch (Exception ex)
        //{
        //    toolBar.MostrarMensajeError(ex.Message);
        //}
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Criterios", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            rblConfig.Items.Add(new ListItem("Específica", "1"));
            rblConfig.Items.Add(new ListItem("Lógica", "2"));
            rdblAplicableA.Enabled = false;

            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlDireccion, "-1", true);

            String IdconfCriterio = "";
            IdconfCriterio = Session["SupervisionConfiguracionCriterios.IdCriterioConf"].ToString();
            hfIdConfCriterio.Value = IdconfCriterio;

            List<SupervisionReglasCriterios> objSupReglas = new List<SupervisionReglasCriterios>();

            objSupReglas = vSupervisionService.ConsultarSupervisionReglasCriterios(Convert.ToInt32(IdconfCriterio), null, null, null, 2);

            SupervisionPreguntas objSupPreguntas = new SupervisionPreguntas();
            objSupPreguntas = vSupervisionService.ConsultarDatosFiltrosSupervisionCriterios(objSupReglas[0].IdRespuesta);

            ManejoControlesSupervision vControles = new ManejoControlesSupervision();
            vControles.LlenarPregunta(ddlPregunta, objSupPreguntas.IdPregunta.ToString(), true, Convert.ToInt32(objSupPreguntas.IdDireccion));
            vControles.LlenarRespuestas(ddlRespuesta, objSupReglas[0].IdRespuesta.ToString(), true, objSupPreguntas.IdPregunta);
            rblEstado.SelectedValue = objSupReglas[0].Estado.ToString();
            rblConfig.SelectedValue = objSupReglas[0].TipoConfiguracion.ToString();
            rdblAplicableA.SelectedValue = objSupPreguntas.Aplicable.ToString();
            ddlDireccion.SelectedValue = objSupPreguntas.IdDireccion.ToString();


            if (objSupReglas[0].TipoConfiguracion == 1)
            {

                dvListaCriterios.Visible = true;

                chklCriterios.DataSource = vSupervisionService.ConsultarSupervisionCriterioss(objSupPreguntas.IdPregunta, null, 1);
                chklCriterios.DataValueField = "IdCriterio";
                chklCriterios.DataTextField = "Valor";
                chklCriterios.DataBind();


                foreach (ListItem objItem in chklCriterios.Items)
                {
                    objItem.Enabled = false;
                    //Se busca si la pregunta ya fue respondida
                    var vRegistrosRespuestas = from resp in objSupReglas
                                               where resp.IdCriterio == Convert.ToInt32(objItem.Value)
                                               select resp;

                    if (vRegistrosRespuestas.Count() > 0)
                    { objItem.Selected=true; }
                }

            }
            else
            {

                dvRango.Visible = true;
                txtMinimo.Enabled=false;
                txtMaximo.Enabled = false;
                txtMinimo.Text = objSupReglas[0].ValorMinimo.ToString();
                txtMaximo.Text = objSupReglas[0].ValorMaximo.ToString();

            }


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}