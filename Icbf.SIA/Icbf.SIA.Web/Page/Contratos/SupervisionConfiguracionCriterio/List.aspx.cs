using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;


public partial class Page_Contratos_SupervisionConfiguracionCriterio_List : GeneralWeb
{

    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionConfiguracionCriterio";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                BuscarLoad();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void BuscarLoad()
    {
        try
        {
            int? vIdPregunta = null;

            if (hfIdPregunta.Value != "")
            {
                vIdPregunta = Convert.ToInt32(hfIdPregunta.Value);
            }

            gvSupervisionCriterios.DataSource = vSupervisionService.ConsultarSupervisionReglasCriterios(null, null, null, null, 1);
            gvSupervisionCriterios.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Buscar()
    {
        try
        {
            int? vIdRespuesta = null;
            int? vEstado = null;

            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }

            if (ddlRespuesta.SelectedValue != "-1")
            { vIdRespuesta = Convert.ToInt32(ddlRespuesta.SelectedValue); }

            gvSupervisionCriterios.DataSource = vSupervisionService.ConsultarSupervisionReglasCriterios(null, vIdRespuesta, Convert.ToInt32(rblConfig.SelectedValue), vEstado, 1);
            gvSupervisionCriterios.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionCriterios.PageSize = PageSize();
            gvSupervisionCriterios.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Reglas de Criterios", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionCriterios.DataKeys[rowIndex].Value.ToString();
            Session.Add("SupervisionConfiguracionCriterios.IdCriterioConf", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionCriterios_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionCriterios.SelectedRow);
    }

    protected void gvSupervisionCriterios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionCriterios.PageIndex = e.NewPageIndex;
        Buscar();
    }

    private void CargarDatosIniciales()
    {
        try
        {
            //if (GetSessionParameter("SupervisionCriterios.Eliminado").ToString() == "1")
            //toolBar.MostrarMensajeEliminado();
            //RemoveSessionParameter("SupervisionCriterios.Eliminado");

            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlDireccion, "-1", true);

            //ddlPregunta ConsultarSupervisionPreguntass
            ddlPregunta.Items.Add(new ListItem("Seleccione", "-1"));
            ddlRespuesta.Items.Add(new ListItem("Seleccione", "-1"));

            //ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblConfig, "Espec�fica", "L�gica", null);

            rblConfig.Items.Add(new ListItem("Espec&iacute;fica", "1"));
            rblConfig.Items.Add(new ListItem("L&oacute;gica", "2"));
            rblConfig.SelectedValue = "1";

            //List<string> Preguntasupervision = new List<string>();
            //Preguntasupervision = (List<string>)Session["Preguntasupervision"];

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void ddlDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {

        ManejoControlesSupervision vControles = new ManejoControlesSupervision();
        vControles.LlenarPregunta(ddlPregunta, "-1", true, Convert.ToInt32(ddlDireccion.SelectedValue));
        vControles.LlenarRespuestas(ddlRespuesta, "-1", true, Convert.ToInt32(ddlPregunta.SelectedValue));
    }

    protected void ddlPregunta_SelectedIndexChanged(object sender, EventArgs e)
    {
        ManejoControlesSupervision vControles = new ManejoControlesSupervision();
        vControles.LlenarRespuestas(ddlRespuesta, "-1", true, Convert.ToInt32(ddlPregunta.SelectedValue));
    }
    protected void gvSupervisionCriterios_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (e.Row.Cells[2].Text == "1")
            { e.Row.Cells[2].Text = "Espec&iacute;fica"; }
            else { e.Row.Cells[2].Text = "L&oacute;gica"; }

            if (e.Row.Cells[3].Text == "1")
            { e.Row.Cells[3].Text = "Activo"; }
            else { e.Row.Cells[3].Text = "Inactivo"; }


        }
    }
}