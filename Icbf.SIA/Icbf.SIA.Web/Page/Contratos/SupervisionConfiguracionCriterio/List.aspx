<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true"
    CodeFile="List.aspx.cs" Inherits="Page_Contratos_SupervisionConfiguracionCriterio_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdPregunta" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Direcci&oacute;n *
                </td>
                <td>
                    Aplicable a *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlDireccion" runat="server" AutoPostBack="True" Width="250px"
                        OnSelectedIndexChanged="ddlDireccion_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RadioButtonList ID="rdblAplicableA" runat="server" Width="123px" RepeatLayout="Flow"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Text="EC" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="UDS" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Pregunta *
                </td>
                <td>
                    Respuesta *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlPregunta" runat="server" Width="250px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlPregunta_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlRespuesta" runat="server" Width="250px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Configuraci&oacuten *
                </td>
                <td colspan="2">
                    Estado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblConfig" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
            </tr>
            <tr class="rowA">
            </tr>
            <tr>
                <td colspan="2">
                    <div id="CargueCriterios">
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSupervisionCriterios" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdCriterioRespuesta"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvSupervisionCriterios_PageIndexChanging"
                        OnSelectedIndexChanged="gvSupervisionCriterios_SelectedIndexChanged" OnRowDataBound="gvSupervisionCriterios_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Pregunta" DataField="IdPregunta" />--%>
                            <asp:BoundField HeaderText="Respuesta" DataField="IdRespuesta" />
                            <asp:BoundField DataField="TipoConfiguracion" HeaderText="Tipo Configuraci&oacute;n" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
