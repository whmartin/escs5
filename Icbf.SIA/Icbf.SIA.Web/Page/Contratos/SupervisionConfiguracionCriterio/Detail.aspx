<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true"
    CodeFile="Detail.aspx.cs" Inherits="Page_Contratos_SupervisionConfiguracionCriterio_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdConfCriterio" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Direcci&oacute;n *
            </td>
            <td>
                Aplicable a *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlDireccion" runat="server" Width="250px" ClientIDMode="Static"
                    Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList ID="rdblAplicableA" runat="server" Width="123px" RepeatLayout="Flow"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Text="EC" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="UDS" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Pregunta *
            </td>
            <td>
                Respuesta *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlPregunta" runat="server" Width="250px" ClientIDMode="Static"
                    Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlRespuesta" runat="server" Width="250px" ClientIDMode="Static"
                    Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Configuraci&oacuten *
            </td>
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblConfig" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="Div1">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="CargueCriterios">
                    <div id="dvListaCriterios" runat="server" visible="false">
                        <fieldset>
                            <legend>Criterios:</legend>
                            <asp:CheckBoxList ID="chklCriterios" runat="server">
                            </asp:CheckBoxList>
                        </fieldset>
                    </div>
                    <div id="dvRango" runat="server" visible="false">
                        <fieldset>
                            <legend>Rango:</legend>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <label>
                                            Valor m�nimo criterios cumplidos*</label>
                                    </td>
                                    <td>
                                        <label>
                                            Valor m&aacute;ximo criterios cumplidos*</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMinimo" runat="server" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMaximo" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
