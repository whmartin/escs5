using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionConfiguracionCriterios_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionCriterio";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {

        //ClientScript.RegisterClientScriptBlock(this.GetType(), "cargueDetalle", "alert($.fn.jquery);$(document).ready(function() {alert('prueba');carguePaginaDetalle(" + rblConfig.SelectedValue + ");});", true);

        //ClientScript.RegisterClientScriptBlock(this.GetType(), "cargueDetalle", "alert('prueba');carguePaginaDetalle(" + rblConfig.SelectedValue + ");", true);
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
   
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            // toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Reglas de Criterios", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {

            //ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            //Controles.LlenarDireccion(ddlDireccion, "-1", true);

            String IdconfCriterio = "";
            IdconfCriterio = Session["SupervisionConfiguracionCriterios.IdCriterioConf"].ToString();
            hfIdConfCriterio.Value = IdconfCriterio;

            List<SupervisionReglasCriterios> objSupReglas = new List<SupervisionReglasCriterios>();

            objSupReglas = vSupervisionService.ConsultarSupervisionReglasCriterios(Convert.ToInt32(IdconfCriterio), null, null, null, 2);

            SupervisionPreguntas objSupPreguntas = new SupervisionPreguntas();
            objSupPreguntas = vSupervisionService.ConsultarDatosFiltrosSupervisionCriterios(objSupReglas[0].IdRespuesta);

            ManejoControlesSupervision vControles = new ManejoControlesSupervision();
            vControles.LlenarPregunta(ddlPregunta, objSupPreguntas.IdPregunta.ToString(), true, Convert.ToInt32(objSupPreguntas.IdDireccion));
            vControles.LlenarRespuestas(ddlRespuesta, objSupReglas[0].IdRespuesta.ToString(), true, objSupPreguntas.IdPregunta);
            rblEstado.SelectedValue = objSupReglas[0].Estado.ToString();
            rblConfig.SelectedValue = objSupReglas[0].TipoConfiguracion.ToString();
            rdblAplicableA.SelectedValue = objSupPreguntas.Aplicable.ToString();
            ddlDireccion.SelectedValue = objSupPreguntas.IdDireccion.ToString();


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            List<string> Preguntasupervision = new List<string>();

            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlDireccion, "-1", true);


            ddlPregunta.Items.Add(new ListItem("Seleccione", "-1"));
            ddlRespuesta.Items.Add(new ListItem("Seleccione", "-1"));

            rblConfig.Items.Add(new ListItem("Específica", "1"));
            rblConfig.Items.Add(new ListItem("Lógica", "2"));

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    [System.Web.Services.WebMethod]
    public static String ConsultaSubcomponentes(String pDireccion)
    {
        ManejoControlesSupervision oManejoControles = new ManejoControlesSupervision();
        String vOpciones = "";
        vOpciones = oManejoControles.LlenarComponenteAjax(true, Convert.ToInt32(pDireccion));
        return vOpciones;

    }

    [System.Web.Services.WebMethod]
    public static String ConsultaPreguntas(String pDireccion, String pAplicable)
    {
        ManejoControlesSupervision oManejoControles = new ManejoControlesSupervision();
        String vOpciones = "";
        vOpciones = oManejoControles.LlenarPreguntaAjax(Convert.ToInt32(pDireccion), Convert.ToInt32(pAplicable));
        return vOpciones;

    }

    [System.Web.Services.WebMethod]
    public static String ConsultaRespuestas(String pIdPregunta)
    {
        ManejoControlesSupervision oManejoControles = new ManejoControlesSupervision();
        String vOpciones = "";
        vOpciones = oManejoControles.LlenarRespuestasAjax(Convert.ToInt32(pIdPregunta));
        return vOpciones;

    }

    [System.Web.Services.WebMethod]
    public static String GuardarConfiguracion(String tipoConf, String IdRespuesta, String Criterios, String valMaximo, String valMinimo)
    {
        System.Data.DataTable dtbCriterios = new System.Data.DataTable();
        dtbCriterios.Columns.Add("IdCriterio");
        String vRespuesta = "@OK";

        GeneralWeb vObjGeneralWeb = new GeneralWeb();

        if (Criterios.IndexOf("-") != -1)
        {
            // Criterios = Criterios.Substring(0, Criterios.Length - 1);
            char[] delimiterChars = { '-' };
            foreach (String vPipe in Criterios.Split(delimiterChars))
            {
                System.Data.DataRow drValores = dtbCriterios.NewRow();
                drValores["IdCriterio"] = vPipe;
                dtbCriterios.Rows.Add(drValores);
            }
        }
        else
        {
            if (Criterios != "")
            {
                System.Data.DataRow drValores = dtbCriterios.NewRow();
                drValores["IdCriterio"] = Criterios;
                dtbCriterios.Rows.Add(drValores);
            }
        }


        SupervisionService vSupervisionService = new SupervisionService();
        SupervisionReglasCriterios pObjSupReglasCriterios = new SupervisionReglasCriterios();
        pObjSupReglasCriterios.IdRespuesta = Convert.ToInt32(IdRespuesta);
        pObjSupReglasCriterios.TipoConfiguracion = Convert.ToInt32(tipoConf);
        pObjSupReglasCriterios.UsuarioCrea = vObjGeneralWeb.GetSessionUser().NombreUsuario;
        pObjSupReglasCriterios.ValorMinimo = Convert.ToInt32(valMinimo);
        pObjSupReglasCriterios.ValorMaximo = Convert.ToInt32(valMaximo);
        pObjSupReglasCriterios.Estado = 1;

        SupervisionReglasCriterios vObjSuperCri = vSupervisionService.InsertarSupervisionReglasCriterios(pObjSupReglasCriterios, dtbCriterios);

        if (vObjSuperCri.MensajeError != "")
        {
            vRespuesta = vObjSuperCri.MensajeError;
        }

        return "@OK";
    }

}
