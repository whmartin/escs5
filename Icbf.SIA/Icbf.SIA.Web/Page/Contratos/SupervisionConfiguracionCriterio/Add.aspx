﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SupervisionConfiguracionCriterios_Add"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../../../Scripts/SupervisionReglasCriterios.js" type="text/javascript"></script>
    <asp:HiddenField ID="hfIdConfCriterio" runat="server" ClientIDMode="Static" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Dirección *
            </td>
            <td>
                Aplicable a *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlDireccion" runat="server" Width="250px" ClientIDMode="Static">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList ID="rdblAplicableA" runat="server" Width="123px" RepeatLayout="Flow"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Text="EC" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="UDS" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Pregunta *
            </td>
            <td>
                Respuesta *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlPregunta" runat="server" Width="250px" ClientIDMode="Static">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlRespuesta" runat="server" Width="250px" ClientIDMode="Static">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Configuraci&oacuten *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblConfig" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
        </tr>
        <tr class="rowA">
        </tr>
        <tr>
            <td colspan="2">
                <div id="Div1">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="CargueCriterios">
                </div>
            </td>
        </tr>
    </table>
    <div id="dvBloqueo" style="position: absolute; z-index: 1; width: 100%; height: 100%;
        background-color: #000000; top: 0px; left: 0px; display: none; opacity: 0.25;
        filter: alpha(opacity=25);">
    </div>
    <div id="dvConfirmacion" class="modalDialog">
        <table style="text-align: center; padding: 9px">
            <tr>
                <td colspan="2">
                    ¿Está seguro de guardar la configuración de criterios? Recuerde que después de guardada
                    no puede ser modificada toda la información
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnAceptar" type="button" value="Aceptar" onclick="" />
                </td>
                <td>
                    <input id="btnCancelar" type="button" value="Cancelar" onclick="ocultarDiv();" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
