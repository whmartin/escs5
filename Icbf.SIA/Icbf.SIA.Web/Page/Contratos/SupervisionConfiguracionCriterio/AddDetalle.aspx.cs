﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Supervision.Entity;

public partial class Page_Contratos_SupervisionConfiguracionCriterio_AddDetalle : System.Web.UI.Page
{

    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_Load(object sender, EventArgs e)
    {


        if (Request.Params["tipoConf"].ToString() == "1")
        {
            dvListaCriterios.Visible = true;
            dvRango.Visible = false;

            //Consulta el listado de criterios de una pregunta

            chklCriterios.DataSource = vSupervisionService.ConsultarSupervisionCriterioss(Convert.ToInt32(Request.Params["IdPregunta"].ToString()), null, 1);
            chklCriterios.DataValueField = "IdCriterio";
            chklCriterios.DataTextField = "Valor";
            chklCriterios.DataBind();


            //Si vienen un idCriterio es una edición y se proceden a cargar los datos que estan en la BD para ese ID
            if (Request.Params["IdCriterio"].ToString() != "")
            {
                foreach (ListItem objItem in chklCriterios.Items)
                {

                    String IdconfCriterio = "";
                    IdconfCriterio = Session["SupervisionConfiguracionCriterios.IdCriterioConf"].ToString();
                    // hfIdConfCriterio.Value = IdconfCriterio;

                    List<SupervisionReglasCriterios> objSupReglas = new List<SupervisionReglasCriterios>();
                    objSupReglas = vSupervisionService.ConsultarSupervisionReglasCriterios(Convert.ToInt32(IdconfCriterio), null, null, null, 2);

                    //Se busca si la pregunta ya fue respondida
                    var vRegistrosRespuestas = from resp in objSupReglas
                                               where resp.IdCriterio == Convert.ToInt32(objItem.Value)
                                               select resp;

                    if (vRegistrosRespuestas.Count() > 0)
                    { objItem.Selected = true; }
                }
            }




        }
        else if (Request.Params["tipoConf"].ToString() == "2")
        {
            dvRango.Visible = true;
            dvListaCriterios.Visible = false;

            //Si vienen un idCriterio es una edición y se proceden a cargar los datos que estan en la BD para ese ID
            if (Request.Params["IdCriterio"].ToString() != "")
            {
                String IdconfCriterio = "";
                IdconfCriterio = Session["SupervisionConfiguracionCriterios.IdCriterioConf"].ToString();
                // hfIdConfCriterio.Value = IdconfCriterio;

                List<SupervisionReglasCriterios> objSupReglas = new List<SupervisionReglasCriterios>();
                objSupReglas = vSupervisionService.ConsultarSupervisionReglasCriterios(Convert.ToInt32(IdconfCriterio), null, null, null, 2);
                txtMinimo.Value = objSupReglas[0].ValorMinimo.ToString();
                txtMaximo.Value = objSupReglas[0].ValorMaximo.ToString();
            }

        }

    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // No obliga a la página a tener un form incluido
    }

}