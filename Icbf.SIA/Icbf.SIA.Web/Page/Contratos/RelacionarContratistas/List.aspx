<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RelacionarContratistas_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Número Contrato
                </td>
                <td>
                    Número de identificación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" MaxLength="3"
                        onfocus="blur();" class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtNumeroContrato"
                        FilterType="Numbers" ValidChars="0123456789" />
                    <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
                    <asp:HiddenField ID="hfIdContrato" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hfNumeroContrato" runat="server" ClientIDMode="Static" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" ClientIDMode="Static" onfocus="blur();"
                        class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                        FilterType="Numbers" />
                    <asp:Image Width="20px" Height="20px" ID="imgBuscarPersona" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        Style="cursor: hand" ToolTip="Buscar" onClick="GetPersona()" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Número de Identificación Representante Legal&nbsp;
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionRL" ClientIDMode="Static"
                        onfocus="blur();" class="enable" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FtNumeroIdentificacionRL" runat="server" TargetControlID="txtNumeroIdentificacionRL"
                        FilterType="Numbers" ValidChars="0123456789" />
                    <asp:Image Width="20px" Height="20px" ID="Image1" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        Style="cursor: hand" ToolTip="Buscar" onClick="GetRepresentanteLegal()" />
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRelacionarContratistas" AutoGenerateColumns="False"
                        AllowPaging="True" AllowSorting="true" GridLines="None" Width="100%" DataKeyNames="IdContratistaContrato"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvRelacionarContratistas_PageIndexChanging"
                        OnSelectedIndexChanged="gvRelacionarContratistas_SelectedIndexChanged" OnSorting="gvRelacionarContratistas_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código Contratista Contrato" DataField="IdContratistaContrato"
                                SortExpression="IdContratistaContrato" />
                            <asp:BoundField HeaderText="Código Contrato" DataField="IdContrato" SortExpression="IdContrato" />
                            <asp:BoundField HeaderText="Número de identificación" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Clase de Entidad" DataField="ClaseEntidad" SortExpression="ClaseEntidad" />
                            <asp:BoundField HeaderText="Porcentaje de Participación" DataField="PorcentajeParticipacion"
                                SortExpression="PorcentajeParticipacion" />
                            <asp:BoundField HeaderText="Numero de Identificacion Representante Legal" DataField="NumeroIdentificacionRepresentanteLegal"
                                SortExpression="NumeroIdentificacionRepresentanteLegal" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 3) {
                    $('#txtNumeroContrato').val(retLupa[3]);
                    $('#hfIdContrato').val(retLupa[0]);
                }
            }
        }

        function GetPersona() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaTercero.aspx', null, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetPersona(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 3) {
                    $('#txtNumeroIdentificacion').val(retLupa[3]);
                }
            }
        }

        function GetRepresentanteLegal() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaTercero.aspx', setReturnGetRepresentanteLegal, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetRepresentanteLegal(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 3) {
                    $('#txtNumeroIdentificacionRL').val(retLupa[3]);
                }
            }
        }
    </script>
</asp:Content>
