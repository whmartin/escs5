<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_RelacionarContratistas_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContratistaContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número Contrato</td>
            <td>
                Número de identificación</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" 
                    Enabled="False"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />--%>
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" Visible="false" />
                <asp:HiddenField ID="hfIdContrato" runat="server" ClientIDMode="Static"/>
                <asp:HiddenField ID="hfNumeroContrato" runat="server" ClientIDMode="Static"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" ClientIDMode="Static" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>

        <tr class="rowB">
            <td>
                Tipo Persona *
            </td>
            <%--<td>
                Clase Entidad *
            </td>--%>
            <td>
                Razón Social</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoPersona" ClientIDMode="Static" 
                    Enabled="False"></asp:TextBox>
            </td>
            <%--<td>
                <asp:RadioButtonList runat="server" ID="rblClaseEntidad" 
                    RepeatDirection="Horizontal" AutoPostBack="True" 
                    onselectedindexchanged="rblTipoPersona_SelectedIndexChanged">
                    <asp:ListItem Value="ConsorcioUnionTemporal">Consorcio  y/o Unión Temporal</asp:ListItem>
                </asp:RadioButtonList>
            </td>--%>
            <td>
            <asp:HiddenField ID="hfClaseEntidad" runat="server" ClientIDMode="Static"/>
                <asp:TextBox runat="server" ID="txtRazonSocial" ClientIDMode="Static" 
                    Enabled="False"></asp:TextBox>
            </td>
            
        </tr>

        <tr class="rowB">
           <td>
                Primer Nombre
           </td>
           <td>
                Segundo Nombre
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" ClientIDMode="Static" 
                     MaxLength="50" Enabled="False" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" ClientIDMode="Static" 
                    MaxLength="50" Enabled="False" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Primer Apellido
           </td>
           <td>
                Segundo Apellido
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" ClientIDMode="Static" 
                     MaxLength="50" Enabled="False" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" ClientIDMode="Static" 
                    MaxLength="50" Enabled="False" ></asp:TextBox>
            </td>
        </tr>



        <%--<tr class="rowB">
            <td>
                Código Contrato *
            </td>
            <td>
                Número de identificación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>

        <%--<tr class="rowB">
            <td>
                Clase de Entidad *
            </td>
            <td>
                Porcentaje de Participación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtClaseEntidad"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPorcentajeParticipacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>

        <%--<tr class="rowB">
            <td colspan="2">
                Numero de Identificacion Representante Legal *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtNumeroIdentificacionRepresentanteLegal"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>



        <%--<tr class="rowB">
            <td>
                Código Contrato *
            </td>
            <td>
                Número de identificación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Clase de Entidad *
            </td>
            <td>
                Porcentaje de Participación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtClaseEntidad"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPorcentajeParticipacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Numero de Identificacion Representante Legal *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtNumeroIdentificacionRepresentanteLegal"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>
    </table>


    <asp:Panel ID="panelRepresentanteLegalContratista" ClientIDMode="Static" Visible="false" runat="server">
        <asp:Label ID="Label1" runat="server" Text="INFORMACION REPRESENTANTE LEGAL" Font-Bold="true" Font-Size="Small"  ></asp:Label>
        <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Numero de Identificacion Representante Legal
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacionRL" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td></td>
        </tr>

        <tr class="rowB">
           <td>
                Primer Nombre
           </td>
           <td>
                Segundo Nombre
           </td>
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerNombreRepresentanteLegal" ClientIDMode="Static" 
                     Enabled="False" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombreRepresentanteLegal" ClientIDMode="Static" 
                    Enabled="False" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Primer Apellido
           </td>
           <td>
                Segundo Apellido
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerApellidoRepresentanteLegal" ClientIDMode="Static" 
                     Enabled="False" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellidoRepresentanteLegal" ClientIDMode="Static" 
                    Enabled="False" ></asp:TextBox>
            </td>
        </tr>

        
        </table>
    </asp:Panel>
</asp:Content>
