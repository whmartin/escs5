<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_TipoModificacionBasica_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoModificacionBasica" runat="server" />
<asp:HiddenField ID="hfIdsAsociados" runat="server" />
    <table width="90%" align="center">
        <%--<tr class="rowB">
            <td>
                Descripcion de la Modificacion *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Requiere Modificacion *
                <asp:RequiredFieldValidator runat="server" ID="rfvRequiereModificacion" ControlToValidate="txtRequiereModificacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRequiereModificacion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftRequiereModificacion" runat="server" TargetControlID="txtRequiereModificacion"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>--%>
        <tr class="rowA">
            <td>
                 <asp:Label ID="lblCodigo" Visible="false" runat="server" Text=""></asp:Label>
            </td>
        </tr>
      <tr class="rowB">
            <td>
                Tipos de Modificación *
            </td>
        </tr>
      <tr class="rowA">
            <td>
                <asp:CheckBoxList AutoPostBack="true" ID="chkListTiposModificacion" runat="server" OnSelectedIndexChanged="chkListTiposModificacion_SelectedIndexChanged">

                </asp:CheckBoxList>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                Descripción
            </td>
        </tr>
      <tr class="rowA">
            <td>
                <asp:Label ID="lblDescripcion" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado de la Modificacion *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado"  RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
       <tr class="rowB">
            <asp:MultiView ID="ViewModificaciones" runat="server" ActiveViewIndex="-1">
                <asp:View runat="server">
                    <table width="90%" align="center">
                                    <tr class="rowB">
            <td colspan="2">
                ¿ Requiere Modificar Garantía ?
                </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblRequiereModificacion" RepeatDirection="Horizontal" Enabled="False"  >                                      
                </asp:RadioButtonList>
            </td>
        </tr>
                     <tr class="rowB">
            <td colspan="2">
                ¿ Requiere Crear nueva Garantía ?
                </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblGenerarModificacion" RepeatDirection="Horizontal" Enabled="False"  >                                      
                </asp:RadioButtonList>
            </td>
        </tr>
                        </table>
                </asp:View>
            </asp:MultiView>


        </tr>
    </table>
</asp:Content>
