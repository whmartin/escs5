using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_TipoModificacionBasica_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vTipoModificacionService = new ContratoService();
    string PageName = "Contratos/TipoModificacionBasica";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
        {
            vSolutionPage = SolutionPage.Edit;
            toolBar.eventoRetornar += toolBar_eventoRetornar;
        }
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado = -1;
            TipoModificacion vTipoModificacionBasica = new TipoModificacion();

            string descripcion = string.Empty;
            string idIngresados = string.Empty;

            int index = 0;

            foreach (ListItem item in chkListTiposModificacion.Items)
            {
                if (item.Selected)
                {
                    index++;
                    descripcion += descripcion == string.Empty ? item.Text : " - " + item.Text;
                    idIngresados += idIngresados == string.Empty ? item.Value : "," + item.Value;
                }
            }

            var isEdit = Request.QueryString["oP"] == "E" ? true : false;

            bool isValid =true;

            if(index <= 1 && !isEdit)
            {
                toolBar.MostrarMensajeError("El tipo de modificación que desea crear ya existe en el sistema.");
                isValid = false;
                return;
            }
            
            if ( isValid && descripcion != string.Empty   && rblEstado.SelectedValue != "-1" )
            {
                vTipoModificacionBasica.Descripcion = descripcion;
                vTipoModificacionBasica.Estado = rblEstado.SelectedValue == "Activo" ? true:false;
                vTipoModificacionBasica.RequiereModificacion = false;
                vTipoModificacionBasica.IdsAsociados = idIngresados;

                if (Request.QueryString["oP"] == "E")
                {
                    vTipoModificacionBasica.IdTipoModificacionBasica = Convert.ToInt32(hfIdTipoModificacionBasica.Value);

                    if (
                        hfIdsAsociados.Value.ToString() == idIngresados || 
                        vTipoModificacionService.ConsultarPuedeEliminarTipodeModificacion(vTipoModificacionBasica.IdTipoModificacionBasica)
                       )
                    {
                        if (
                            hfIdsAsociados.Value.ToString() == idIngresados || 
                            !vTipoModificacionService.ConsultarExistenciaCombinacion(idIngresados, vTipoModificacionBasica.IdTipoModificacionBasica)
                           )
                        {
                            vTipoModificacionBasica.UsuarioModifica = GetSessionUser().NombreUsuario;
                            InformacionAudioria(vTipoModificacionBasica, this.PageName, vSolutionPage);
                            vResultado = vTipoModificacionService.ModificarTipoModificacion(vTipoModificacionBasica);
                        }
                        else
                            toolBar.MostrarMensajeError("La combinación realizada ya existe, verifique por favor.");
                    }
                    else
                        toolBar.MostrarMensajeError("No puede eliminar este tipo de modificación porque ya esta siendo utilizada.");
                }
                else
                {
                    bool existe = vTipoModificacionService.ConsultarExistenciaCombinacion(idIngresados,0);

                    if (! existe)
                    {
                        vTipoModificacionBasica.UsuarioCrea = GetSessionUser().NombreUsuario;
                        InformacionAudioria(vTipoModificacionBasica, this.PageName, vSolutionPage);
                        vResultado = vTipoModificacionService.InsertarTipoModificacion(vTipoModificacionBasica, idIngresados);
                    }
                    else
                        toolBar.MostrarMensajeError("La combinación realizada ya existe, verifique por favor.");
                }

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado >= 1)
                {
                    SetSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica", vTipoModificacionBasica.IdTipoModificacionBasica);
                    SetSessionParameter("TipoModificacionBasica.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
            }
            else
            {
                if(isValid == false)
                toolBar.MostrarMensajeError("Debe seleccionar el estado y/o si requiere modificación de la garantía.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tipo de Modificaci&#243;n Contractual", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        SetSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica", hfIdTipoModificacionBasica.Value);
       NavigateTo(SolutionPage.Detail);
    }

    private void CargarRegistro()
    {
        try
        {//"TipoModificacionBasica.IdTipoModificacionBasica
            ViewModificaciones.ActiveViewIndex = 0;
            int vIdTipoModificacionBasica = Convert.ToInt32(GetSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica"));
            RemoveSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica");

            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
            vTipoModificacionBasica = vTipoModificacionService.consultartipomodificacion(vIdTipoModificacionBasica);
            hfIdTipoModificacionBasica.Value = vTipoModificacionBasica.IdTipoModificacionBasica.ToString();
            lblDescripcion.Text = vTipoModificacionBasica.Descripcion;
            lblDescripcion.Visible = true;
            lblCodigo.Text = "Codígo: " + vTipoModificacionBasica.Codigo;
            lblCodigo.Visible = true;
            chkListTiposModificacion.Enabled = false;
            rblEstado.SelectedValue = vTipoModificacionBasica.Estado == true ? "Activo": "Inactivo";
            rblRequiereModificacion.SelectedValue = vTipoModificacionBasica.RequiereModificacion ? "SI" : "NO";
            rblGenerarModificacion.SelectedValue = vTipoModificacionBasica.GeneraNuevaGarantia == true ? "SI" : "NO";

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoModificacionBasica.UsuarioCrea, vTipoModificacionBasica.FechaCrea, vTipoModificacionBasica.UsuarioModifica, vTipoModificacionBasica.FechaModifica);

            if (!vTipoModificacionBasica.EsPorDefecto)
            {
                chkListTiposModificacion.DataSource = vTipoModificacionService.ConsultarTipoModificacionPorDefecto(false);
                chkListTiposModificacion.DataValueField = "IdTipoModificacionBasica";
                chkListTiposModificacion.DataTextField = "Descripcion";
                chkListTiposModificacion.DataBind();

                List<TipoModificacion> items = vTipoModificacionService.ConsultarTipoModificacionHijos(vIdTipoModificacionBasica);

                int i = 1;
                string idIngresados = string.Empty;

                foreach (ListItem item in chkListTiposModificacion.Items)
                {
                    int idOut = int.Parse(item.Value);
                    bool isValid = items.Any(e => e.IdTipoModificacionBasica == idOut);

                    if (isValid)
                    {
                        item.Selected = true;
                        idIngresados += idIngresados == string.Empty ? item.Value : "," + item.Value;
                    }

                    i++;
                }

                hfIdsAsociados.Value = idIngresados;

                if (i > 1)
                    chkListTiposModificacion.Enabled = true;
            }
            else
            {
                chkListTiposModificacion.SelectedValue = vIdTipoModificacionBasica.ToString();
                hfIdsAsociados.Value = vIdTipoModificacionBasica.ToString();
            }            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {

            if(Request.QueryString["oP"] == "E")
                 chkListTiposModificacion.DataSource = vTipoModificacionService.ConsultarTipoModificacionPorDefecto(true);
            else
                chkListTiposModificacion.DataSource = vTipoModificacionService.ConsultarTipoModificacionPorDefecto(false);

            chkListTiposModificacion.DataValueField = "IdTipoModificacionBasica";
            chkListTiposModificacion.DataTextField = "Descripcion";
            chkListTiposModificacion.DataBind();

            //rblModificarGarantia.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblRequiereModificacion.Items.Insert(0, new ListItem("SI", "SI"));
            rblRequiereModificacion.Items.Insert(1, new ListItem("NO", "NO"));

            //rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.Items.Insert(0, new ListItem("Activo", "Activo"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "Inactivo"));

            rblGenerarModificacion.Items.Insert(0, new ListItem("SI", "SI"));
            rblGenerarModificacion.Items.Insert(1, new ListItem("NO", "NO")); 

            //rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void chkListTiposModificacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        string descripcion = string.Empty;

        foreach (ListItem item in chkListTiposModificacion.Items)
        {
            if (item.Selected)
            {
                descripcion += descripcion == string.Empty ? item.Text : " - " + item.Text;
            }
        }



        lblDescripcion.Text = descripcion;
    }
}
