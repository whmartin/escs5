using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_TipoModificacionBasica_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoModificacionBasica";
    ContratoService vTipoModificacionService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica", hfIdTipoModificacionBasica.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdTipoModificacionBasica = Convert.ToInt32(GetSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica"));
            RemoveSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica");

            if (GetSessionParameter("TipoModificacionBasica.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoModificacionBasica.Guardado");

            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
            vTipoModificacionBasica = vTipoModificacionService.consultartipomodificacion(vIdTipoModificacionBasica);
            hfIdTipoModificacionBasica.Value = vTipoModificacionBasica.IdTipoModificacionBasica.ToString();
            lblCodigo.Text = vTipoModificacionBasica.Codigo;
            lblDescripcion.Text = vTipoModificacionBasica.Descripcion;
            rblRequiereModificacion.SelectedValue = vTipoModificacionBasica.RequiereModificacion == true ? "SI":"NO";
            rblEstado.SelectedValue = vTipoModificacionBasica.Estado == true ? "Activo":"Inactivo";
            rblGenerarModificacion.SelectedValue = vTipoModificacionBasica.GeneraNuevaGarantia == true ? "SI" : "NO";

            if (! vTipoModificacionBasica.EsPorDefecto)
            {
                var itemsHijos = vTipoModificacionService.ConsultarTipoModificacionHijos(vIdTipoModificacionBasica);

                foreach (ListItem item in chkListTiposModificacion.Items)
                {
                    int idOut = int.Parse(item.Value);
                    bool isValid = itemsHijos.Any(e => e.IdTipoModificacionBasica == idOut);
                    item.Selected = isValid;
                }
            }
            else
            {
                chkListTiposModificacion.SelectedValue = vTipoModificacionBasica.IdTipoModificacionBasica.ToString();
            }


            ObtenerAuditoria(PageName, hfIdTipoModificacionBasica.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoModificacionBasica.UsuarioCrea, vTipoModificacionBasica.FechaCrea, vTipoModificacionBasica.UsuarioModifica, vTipoModificacionBasica.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoModificacionBasica = Convert.ToInt32(hfIdTipoModificacionBasica.Value);

            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
            vTipoModificacionBasica = vTipoModificacionService.consultartipomodificacion(vIdTipoModificacionBasica);
            InformacionAudioria(vTipoModificacionBasica, this.PageName, vSolutionPage);

            if (vTipoModificacionBasica.EsPorDefecto)
            {
                toolBar.MostrarMensajeError("No Se pueden eliminar la modificaciones por defecto.");                
            }
            else if (vTipoModificacionService.ConsultarPuedeEliminarTipodeModificacion(vTipoModificacionBasica.IdTipoModificacionBasica))
            {
                int vResultado = vTipoModificacionService.EliminarTipoModificacionContractual(vIdTipoModificacionBasica);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado >= 1)
                {
                    toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                    SetSessionParameter("TipoModificacionBasica.Eliminado", "1");
                    NavigateTo(SolutionPage.List);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
                toolBar.MostrarMensajeError("No puede eliminar este tipo de modificación porque ya esta siendo utilizada.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Tipo de Modificaci&#243;n Contractual", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            chkListTiposModificacion.DataSource = vTipoModificacionService.ConsultarTipoModificacionPorDefecto(true);
            chkListTiposModificacion.DataValueField = "IdTipoModificacionBasica";
            chkListTiposModificacion.DataTextField = "Descripcion";
            chkListTiposModificacion.DataBind();

            /*Coloque aqui el codigo para llenar los radio button*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "Activo"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "Inactivo"));

            rblRequiereModificacion.Items.Insert(0, new ListItem("SI", "SI"));
            rblRequiereModificacion.Items.Insert(1, new ListItem("NO", "NO"));

            rblGenerarModificacion.Items.Insert(0, new ListItem("SI", "SI"));
            rblGenerarModificacion.Items.Insert(1, new ListItem("NO", "NO")); 
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
