<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoModificacionBasica_Detail" %>


<asp:Content ID="Content11" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoModificacionBasica" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                C&oacute;digo
            </td>            
        </tr>
        <tr class="rowA">
            <td style="width: 618px">
                <asp:label runat="server" ID="lblCodigo"  Enabled="false"></asp:label>
            </td>            
        </tr>
        <tr class="rowB">
            <td >
                Tipos de Modificaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBoxList ID="chkListTiposModificacion" runat="server" Enabled="False" >
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Descripci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2"> 
                <asp:label runat="server" ID="lblDescripcion"  Enabled="false" Width="549px"></asp:label>
            </td>
        </tr>        
        <tr class="rowB">
            <td >
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>                
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="False"  >                                      
                </asp:RadioButtonList>
            </td>
        </tr>        
        <tr class="rowB">
            <td colspan="2">
                &iquest; Requiere Modificar Garant&iacute;a?
            </td>
        </tr>
        <tr class="rowA">
            <td>                
                <asp:RadioButtonList runat="server" ID="rblRequiereModificacion" RepeatDirection="Horizontal" Enabled="False"  >                                      
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                &iquest; Requiere Crear Garant&iacute;a?
            </td>
        </tr>
        <tr class="rowA">
            <td>                
                <asp:RadioButtonList runat="server" ID="rblGenerarModificacion" RepeatDirection="Horizontal" Enabled="False"  >                                      
                </asp:RadioButtonList>
            </td>
        </tr>
              
    </table>
</asp:Content>
