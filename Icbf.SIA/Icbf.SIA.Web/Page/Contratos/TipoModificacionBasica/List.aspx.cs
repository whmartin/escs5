using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_TipoModificacionBasica_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoModificacionBasica";
    ContratoService vTipoModificacionService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdTipoModificacion = null;
            String vDescripcion = null;
            bool? vRequiereModificacion = null;
            bool? vEstado = null;
            string vcodigo = null;
            if (hfIdTipoModificacion.Text!= "")
            {
                vIdTipoModificacion = Convert.ToInt32(hfIdTipoModificacion.Text);
            }
            if (txtDescripcion.Text!= "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }
            if (ddlEstado.SelectedValue.Equals("false"))
            {
                vEstado = false;
            }
            else if (ddlEstado.SelectedValue.Equals("true"))
            {
                vEstado = true;
            }
            else
            {
                vEstado = null;
            }
            //if (rblEstado.SelectedValue!= "-1")
            //{
            //    //vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            //}
            gvTipoModificacionBasica.DataSource = vTipoModificacionService.ConsultarTipoModificaciones(vIdTipoModificacion, vDescripcion, vRequiereModificacion, vEstado, vcodigo);
            gvTipoModificacionBasica.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            gvTipoModificacionBasica.PageSize = PageSize();
            gvTipoModificacionBasica.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo de Modificaci&#243;n Contractual", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoModificacionBasica.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoModificacionBasica.IdTipoModificacionBasica", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoModificacionBasica_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoModificacionBasica.SelectedRow);
    }
    protected void gvTipoModificacionBasica_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoModificacionBasica.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoModificacionBasica.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoModificacionBasica.Eliminado");

            ddlEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlEstado.Items.Insert(1, new ListItem("ACTIVO", "true"));
            ddlEstado.Items.Insert(2, new ListItem("INACTIVO", "false"));
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
