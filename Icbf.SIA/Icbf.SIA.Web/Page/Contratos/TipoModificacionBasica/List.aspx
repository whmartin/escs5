<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoModificacionBasica_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            < <td>
                C&oacute;digo 
            </td>
            <td>
                Descripci&oacute;n 
            </td>
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="hfIdTipoModificacion"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 618px">
                Estado  
            </td>
            <%--<td>
                Requiere Modificaci&oacute;n *
            </td> --%> 
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlEstado" Width="100px">
                   <%--<asp:ListItem Text ="Seleccione" Value="-1"></asp:ListItem>
                   <asp:ListItem Text ="Activo" Value="true"></asp:ListItem>
                   <asp:ListItem Text ="Inactivo" Value="false"></asp:ListItem>--%>
                </asp:DropDownList>
                
            </td>
            <%--<td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  >
                    <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Todos" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
                 
            </td>--%>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                 <td>
                    <asp:GridView runat="server" ID="gvTipoModificacionBasica" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoModificacionBasica" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoModificacionBasica_PageIndexChanging" OnSelectedIndexChanged="gvTipoModificacionBasica_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="C&oacute;digo" DataField="Codigo" />
                            <asp:BoundField HeaderText="Descripci&oacute;n" DataField="Descripcion" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoToString" />
                            <asp:BoundField HeaderText="Requiere Modificar Garant&iacute;a" DataField="RequiereModificacionToString" />          
                            <asp:BoundField HeaderText="Genera Nueva Garant&iacute;a" DataField="GeneraNuevaGarantiaToString" />          
                                              
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
