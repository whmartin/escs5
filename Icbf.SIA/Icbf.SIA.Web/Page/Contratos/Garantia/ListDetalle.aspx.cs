﻿using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;
using System.Text;

public partial class Page_Contratos_Garantia_ListDetalle : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Garantia";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.MostrarBotonEditar(false);
            toolBar.OcultarBotonGuardar(true);
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.eventoNuevo += toolBar_eventoGuardar;
            
            toolBar.EstablecerTitulos("Gesti&oacute;n de las garantías del contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoGuardar(object sender, EventArgs e)
    {
        var idContrato = int.Parse(hfIdContrato.Value);
        SetSessionParameter("Garantia.IDContratoConsultado", idContrato);
        SetSessionParameter("Garantia.IDGarantia", "0");
        NavigateTo(SolutionPage.Add); 
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles Controles = new ManejoControles();
            int idContrato = int.Parse(GetSessionParameter("Garantia.IDContratoConsultado").ToString());
            RemoveSessionParameter("Garantia.IDContratoConsultado");
            hfIdContrato.Value = idContrato.ToString();
            GridGarantias.EmptyDataText = "No existen garantìas pendientes de revisiòn y/o devueltas";
            GridGarantias.PageSize = PageSize();
            var valor =  ObtenerGarantias();
            txtValorTotalGarantias.Text = valor.ToString("#,###0.00##;($ #,###0.00##)");

            var contrato = vContratoService.ContratoObtener(idContrato);
            txtNumeroContrato.Text = contrato.NumeroContrato;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private decimal ObtenerGarantias()
    {
        int idContrato = int.Parse(hfIdContrato.Value);
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(idContrato);

        decimal valor = 0;

        IEnumerable<Garantia> itemsResult = null;

        if (items != null && items.Count() > 0)
        {
            foreach (var item in items)
                if (!string.IsNullOrEmpty(item.ValorGarantia))
                    valor += decimal.Parse(item.ValorGarantia);

            var itemsValidos = items.Where(e => e.CodigoEstadoGarantia == "002" || 
                                                e.CodigoEstadoGarantia == "003");

            if (itemsValidos != null)
                itemsResult = itemsValidos.ToList();
        }

        GridGarantias.DataSource = itemsResult;
        GridGarantias.DataBind();

        return valor;
    }

    protected void GridGarantias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridGarantias.PageIndex = e.NewPageIndex;
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(int.Parse(hfIdContrato.Value));
        GridGarantias.DataSource = items;
        GridGarantias.DataBind();
    }

    protected void GridGarantias_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var idContrato = int.Parse(hfIdContrato.Value);
        int index = int.Parse(e.CommandArgument.ToString());
        var idGarantia = int.Parse(GridGarantias.DataKeys[index].Value.ToString());
        var estado = GridGarantias.DataKeys[index].Values[2];

        if (e.CommandName == "Select")
        {
            var idSucursal = GridGarantias.DataKeys[index].Values[1];
            SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
            SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", idSucursal);
            SetSessionParameter("Garantia.Estado", estado);
            SetSessionParameter("Garantia.IdGarantia", idGarantia); 
            NavigateTo(SolutionPage.Detail);
        }
        else if (e.CommandName == "Edit")
        {
            SetSessionParameter("Garantia.IDContratoConsultado", idContrato);
            SetSessionParameter("Garantia.IDGarantia", idGarantia);
            NavigateTo(SolutionPage.Edit);
        }
        else if (e.CommandName == "Aprobar")
        {
            var itemsMsjs = vContratoService.ValidarAprobacionGarantia(idGarantia);

            if (itemsMsjs != null && itemsMsjs.Count == 0 )
            {
                vContratoService.EnviarAprobacionGarantia(idGarantia, estado.ToString());
                ObtenerGarantias();
            }
            else
            {
                StringBuilder texto = new StringBuilder();

                foreach (var item in itemsMsjs)
                    texto.AppendLine(item);

                toolBar.MostrarMensajeError(texto.ToString().Replace("\n", "<br/>"));
            }
        }
    }
}