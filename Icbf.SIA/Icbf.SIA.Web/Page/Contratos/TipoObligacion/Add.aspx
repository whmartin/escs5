<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_TipoObligacion_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdTipoObligacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Tipo Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreTipoObligacion" ControlToValidate="txtNombreTipoObligacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 50%">
                Descripci&#243;n Tipo de Obligaci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreTipoObligacion" Height="22px" 
                    MaxLength="50" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreTipoObligacion" runat="server" TargetControlID="txtNombreTipoObligacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " InvalidChars="&#63;"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="128" 
                    TextMode="MultiLine" Width="300px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 413px;
        }
    </style>
</asp:Content>

