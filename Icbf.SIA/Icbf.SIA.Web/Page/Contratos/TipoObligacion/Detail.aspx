<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoObligacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoObligacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Tipo Obligaci&#243;n *
            </td>
            <td style="width: 50%">
                Descripci&#243;n Tipo de Obligaci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreTipoObligacion"  Enabled="false" 
                    Height="50px" MaxLength="50" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 378px;
        }
    </style>
</asp:Content>

