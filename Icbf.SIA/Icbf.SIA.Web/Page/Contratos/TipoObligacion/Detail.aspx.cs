using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de tipo obligación
/// </summary>
public partial class Page_TipoObligacion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoObligacion";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            RemoveSessionParameter("TipoObligacion.Modificado");
            RemoveSessionParameter("TipoObligacion.Guardado");
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoObligacion.IdTipoObligacion", hfIdTipoObligacion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoObligacion = Convert.ToInt32(GetSessionParameter("TipoObligacion.IdTipoObligacion"));
            RemoveSessionParameter("TipoObligacion.IdTipoObligacion");

            if (GetSessionParameter("TipoObligacion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            if (GetSessionParameter("TipoObligacion.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&oacute;n ha sido modificada exitosamente");
            RemoveSessionParameter("TipoObligacion");


            TipoObligacion vTipoObligacion = new TipoObligacion();
            vTipoObligacion = vContratoService.ConsultarTipoObligacion(vIdTipoObligacion);
            hfIdTipoObligacion.Value = vTipoObligacion.IdTipoObligacion.ToString();
            txtNombreTipoObligacion.Text = vTipoObligacion.NombreTipoObligacion;
            txtDescripcion.Text = vTipoObligacion.Descripcion;
            rblEstado.SelectedValue = vTipoObligacion.Estado == true ? "1" : "0";
            ObtenerAuditoria(PageName, hfIdTipoObligacion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoObligacion.UsuarioCrea, vTipoObligacion.FechaCrea, vTipoObligacion.UsuarioModifica, vTipoObligacion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoObligacion = Convert.ToInt32(hfIdTipoObligacion.Value);

            TipoObligacion vTipoObligacion = new TipoObligacion();
            vTipoObligacion = vContratoService.ConsultarTipoObligacion(vIdTipoObligacion);
            InformacionAudioria(vTipoObligacion, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarTipoObligacion(vTipoObligacion);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoObligacion.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Obligaci&#243;n", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
