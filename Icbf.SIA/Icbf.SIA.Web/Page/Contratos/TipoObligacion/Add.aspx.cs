using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de tipos de obligación
/// </summary>
public partial class Page_TipoObligacion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/TipoObligacion";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoObligacion vTipoObligacion = new TipoObligacion();

            vTipoObligacion.NombreTipoObligacion = Convert.ToString(txtNombreTipoObligacion.Text);
            vTipoObligacion.Descripcion = Convert.ToString(txtDescripcion.Text);
            vTipoObligacion.Estado = rblEstado.SelectedValue == "True" ? true : false; 

            if (Request.QueryString["oP"] == "E")
            {
            vTipoObligacion.IdTipoObligacion = Convert.ToInt32(hfIdTipoObligacion.Value);
                vTipoObligacion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarTipoObligacion(vTipoObligacion);
            }
            else
            {
                vTipoObligacion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarTipoObligacion(vTipoObligacion);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoObligacion.IdTipoObligacion", vTipoObligacion.IdTipoObligacion);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("TipoObligacion.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("TipoObligacion.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Obligaci&#243;n", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdTipoObligacion = Convert.ToInt32(GetSessionParameter("TipoObligacion.IdTipoObligacion"));
            int vIdTipoObligacion = 0;

            if (int.TryParse(GetSessionParameter("TipoObligacion.IdTipoObligacion").ToString(), out vIdTipoObligacion))
            {
                RemoveSessionParameter("TipoObligacion.Id");

                TipoObligacion vTipoObligacion = new TipoObligacion();
                vTipoObligacion = vContratoService.ConsultarTipoObligacion(vIdTipoObligacion);
                hfIdTipoObligacion.Value = vTipoObligacion.IdTipoObligacion.ToString();
                txtNombreTipoObligacion.Text = vTipoObligacion.NombreTipoObligacion;
                txtDescripcion.Text = vTipoObligacion.Descripcion;
                rblEstado.SelectedValue = vTipoObligacion.Estado == true ? "True" : "False";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoObligacion.UsuarioCrea, vTipoObligacion.FechaCrea, vTipoObligacion.UsuarioModifica, vTipoObligacion.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado,"Activo","Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
