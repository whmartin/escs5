<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoObligacion_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2" style="width: 50%">
                Nombre Tipo Obligaci&#243;n
            </td>
            <td style="width: 50%">
                Estado
                
                
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtNombreTipoObligacion" Height="25px" 
                    MaxLength="50" Width="320px"  ></asp:TextBox>
                     <Ajax:FilteredTextBoxExtender ID="ftNombreTipoObligacion" runat="server" TargetControlID="txtNombreTipoObligacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " InvalidChars="&#63;" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
                
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:Label runat="server" ID="lblDescTipoObligacion" Text="Descripci&#243;n Tipo de Obligaci&#243;n" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcion" Visible="false" Height="50px" MaxLength="128" 
                    TextMode="MultiLine" Width="300px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">

        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoObligacion" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoObligacion" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoObligacion_PageIndexChanging" 
                        OnSelectedIndexChanged="gvTipoObligacion_SelectedIndexChanged" 
                        AllowSorting="True" onsorting="gvTipoObligacion_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre Tipo Obligaci&#243;n" DataField="NombreTipoObligacion" SortExpression="NombreTipoObligacion" />
                            <asp:BoundField HeaderText="Descripci&#243;n Tipo de Obligaci&#243;n" DataField="Descripcion" SortExpression="Descripcion" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
