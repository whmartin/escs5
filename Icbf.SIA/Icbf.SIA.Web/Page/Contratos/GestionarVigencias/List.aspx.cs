﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de consulta a través de filtros para la entidad Garantia
/// </summary>
public partial class Page_Contratos_GestionarVigencias_List : GeneralWeb
{

    #region Variables
    masterPrincipal toolBar;
    string PageName = "Contratos/GestionarVigencias";
    ContratoService vContratoService = new ContratoService();
    #endregion

    #region Eventos
    protected void gvContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContrato.SelectedRow);
    }

    protected void gvContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContrato.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void ddlCategoriaContrato_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int idTipoContrato = Convert.ToInt32(ddlCategoriaContrato.SelectedValue);
        CargarTipoContrato(idTipoContrato);
    }
    #endregion

    #region Métodos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                SetSessionParameter("Garantia.IDContratoConsultado", "");
                //if (GetState(Page.Master, PageName)) { Buscar(); }

            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        //SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvContrato, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvContrato.PageSize = PageSize();
            gvContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Vigencia.IDContratoConsultado", strValue);
            //string strValue2 = gvContrato.Rows[rowIndex].Cells[9].Text;
            //SetSessionParameter("Vigencia.IdRegional", strValue2);
            //SetSessionParameter("Garantia.IDGarantia", "0");
            NavigateTo("Add.aspx");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            toolBar.LipiarMensajeError();
            DateTime? vFechaRegistroDesde = null;
            DateTime vValidarfecha;
            DateTime? vFechaRegistroHasta = null;
            String vNumeroContratoConvenio = null;
            String vVigenciaFiscalInicial = null;
            int? vRegionalContrato = null;
            int? vCategoriaContrato = null;
            int? vTipoContrato = null;

            if (txtFechaRegistroDesde.Text.StartsWith("_"))
                txtFechaRegistroDesde.Text = string.Empty;
            if (txtFechaRegistroHasta.Text.StartsWith("_"))
                txtFechaRegistroHasta.Text = string.Empty;

            if ((txtFechaRegistroDesde.Text.Equals("") && txtFechaRegistroHasta.Text != "") || (txtFechaRegistroDesde.Text != "" && txtFechaRegistroHasta.Text.Equals("")))
            {
                toolBar.MostrarMensajeError("Se debe registrar el rango de fechas a consultar");
                return;
            }

            if (txtFechaRegistroDesde.Text != "" && txtFechaRegistroHasta.Text != "")
            {
                if (!DateTime.TryParse(txtFechaRegistroDesde.Text, out vValidarfecha) || !DateTime.TryParse(txtFechaRegistroHasta.Text, out vValidarfecha))
                {
                    toolBar.MostrarMensajeError("Fecha(s) Inválidas, por favor validar");
                    return;
                }

                if (Convert.ToDateTime(txtFechaRegistroDesde.Text) > Convert.ToDateTime(txtFechaRegistroHasta.Text))
                {
                    toolBar.MostrarMensajeError("La Fecha de Registro Desde no puede ser mayor a la Fecha de Registro Hasta");
                    return;
                }

                if (Convert.ToDateTime(txtFechaRegistroHasta.Text) < Convert.ToDateTime(txtFechaRegistroDesde.Text))
                {
                    toolBar.MostrarMensajeError("La Fecha de Registro Hasta no puede ser menor a la Fecha de Registro Desde");
                    return;
                }
            }

            if (txtFechaRegistroDesde.Text != "")
            {
                vFechaRegistroDesde = Convert.ToDateTime(txtFechaRegistroDesde.Text);
            }

            if (txtFechaRegistroHasta.Text != "")
            {
                vFechaRegistroHasta = Convert.ToDateTime(txtFechaRegistroHasta.Text);
            }

            if (txtNumeroContratoConvenio.Text != "")
            {
                vNumeroContratoConvenio = Convert.ToString(txtNumeroContratoConvenio.Text);
            }

            if (txtVigenciaFiscalInicial.Text != "")
            {
                vVigenciaFiscalInicial = Convert.ToString(txtVigenciaFiscalInicial.Text);
            }

            if (ddlRegionalContrato.SelectedValue != "-1")
            {
                vRegionalContrato = Convert.ToInt32(ddlRegionalContrato.SelectedValue);
            }

            if (ddlCategoriaContrato.SelectedValue != "-1")
            {
                vCategoriaContrato = Convert.ToInt32(ddlCategoriaContrato.SelectedValue);
            }

            if (ddlTipoContrato.SelectedValue != "-1")
            {
                vTipoContrato = Convert.ToInt32(ddlTipoContrato.SelectedValue);
            }

            var myGridResults = vContratoService.ConsultarContratosVigenciasFuturas(vFechaRegistroDesde, vFechaRegistroHasta, vNumeroContratoConvenio, vVigenciaFiscalInicial, vRegionalContrato, vCategoriaContrato, vTipoContrato, new GeneralWeb().GetSessionUser().IdUsuario);
            
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Contrato), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Contrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarRegionales();
            CargarCategoriaContrato();
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContrato.SelectedValue = "-1";
            txtVigenciaFiscalInicial.Text = Convert.ToString(System.DateTime.Now.Year);
            txtVigenciaFiscalInicial.Text = string.Empty;
            txtNumeroContratoConvenio.Text = string.Empty;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos de las Regionales
    /// </summary>
    private void CargarRegionales()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarRegionalesUsuario(ddlRegionalContrato, new GeneralWeb().GetSessionUser().IdUsuario, "-1", true);
    }

    /// <summary>
    /// Método para cargar los datos de IDTipoGarantia
    /// </summary>
    private void CargarTipoContrato(int pIdTipoContrato)
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarTipoContrato(ddlTipoContrato, "-1", true, pIdTipoContrato);
    }

    /// <summary>
    /// Método para cargar los datos de IDTipoGarantia
    /// </summary>
    private void CargarCategoriaContrato()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarCategoriaContrato(ddlCategoriaContrato, "-1", true);
    }
    #endregion

}
