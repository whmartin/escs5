﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_GestionarVigencias_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        
    <link href="../../../Styles/TabContainer.css" rel="stylesheet" type="text/css" />

    <asp:HiddenField ID="hfIDContrato" runat="server" />
    <asp:HiddenField ID="hfIdRegional" runat="server" />
    <asp:HiddenField ID="hfVigencias" runat="server" />
    <asp:HiddenField ID="hfIdVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfAnioVigencia" runat="server" />
    <asp:HiddenField ID="hfValorVigencia" runat="server" />
    <asp:HiddenField ID="hfValorPlanCompras" runat="server" />
      <script type="text/javascript" language="javascript">
       

          function ValidaEliminacion() {
              return confirm('Esta seguro de que desea eliminar el registro?');
          }

          function muestraImagenLoading() {
              var imgLoading = document.getElementById("imgLoading");
              imgLoading.style.visibility = "visible";
          }

          function ocultaImagenLoading() {
              var imgLoading = document.getElementById("imgLoading");
              imgLoading.style.visibility = "";
          }

           function GetPlanCompras() {
            //muestraImagenLoading();
               var vVigencias = 1;
               var vIdContrato = document.getElementById('<%= hfIDContrato.ClientID %>');  
               var vIdRegContrato = document.getElementById('<%= hfIdRegional.ClientID %>');
               var vAnioVigencia = document.getElementById('<%= hfAnioVigencia.ClientID %>');
               var vIdVigenciaFutura = document.getElementById('<%= hfIdVigenciaFutura.ClientID %>');   
               var vValorVigenciaFutura = document.getElementById('<%= hfValorVigencia.ClientID %>');  
               var url = '../../../Page/Contratos/LupasV11/LupasPlanCompras.aspx?vesVigenciaFutura=' + vVigencias + '&idContrato=' + vIdContrato.value
               + '&vidVigenciaFutura=' + vIdVigenciaFutura.value + '&vAnioVigencia=' + vAnioVigencia.value + '&vValorVigenciaFutura=' + vValorVigenciaFutura.value
               + '&vIdRegContrato=' + vIdRegContrato.value;
               window_showModalDialog(url, setReturnGetPlanCompras, 'dialogWidth:800px;dialogHeight:600px;resizable:yes;');        
              
           }
          function setReturnGetPlanCompras(dialog) {
           // prePostbck(false);
            __doPostBack('<%= txtPlanCompras.ClientID %>', '');
        }

          function GetCDP() {
            muestraImagenLoading();
            //CU-031-CONT-REG_INF_CD-RP
              var idRegional = document.getElementById('<%= hfIdRegional.ClientID %>').value;
              var vVigencias = 1;
              var vIdContrato = document.getElementById('<%= hfIDContrato.ClientID %>');    
              var vAnioVigencia = document.getElementById('<%= hfAnioVigencia.ClientID %>');
               var vIdVigenciaFutura = document.getElementById('<%= hfIdVigenciaFutura.ClientID %>');
              var url = '../../../Page/Contratos/Lupas/LupaRegInfoPresupuestalEnLinea.aspx?IdRegContrato=' + idRegional + '&vesVigenciaFutura=' + vVigencias + '&idContrato='
                  + vIdContrato.value + '&vidVigenciaFutura=' + vIdVigenciaFutura.value + '&vAnioVigencia=' + vAnioVigencia.value;
            window_showModalDialog(url, setReturnGetCDP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
          }
          function setReturnGetCDP() {
              __doPostBack('<%= txtCDP.ClientID %>', '');
          }

          function GetRP() {
            muestraImagenLoading();
            //CU-031-CONT-REG_INF_CD-RP
              var idRegional = document.getElementById('<%= hfIdRegional.ClientID %>').value;
              var vVigencias = 1;
              var vIdContrato = document.getElementById('<%= hfIDContrato.ClientID %>');   
              var vValorVigenciaFutura = document.getElementById('<%= hfValorVigencia.ClientID %>');
              var vAnioVigencia = document.getElementById('<%= hfAnioVigencia.ClientID %>');
              var vIdVigenciaFutura = document.getElementById('<%= hfIdVigenciaFutura.ClientID %>');
              var url = '../../../Page/Contratos/Lupas/LupaListRP.aspx?IdRegContrato=' + idRegional + '&vesVigenciaFutura=' + vVigencias + '&idContrato='
                  + vIdContrato.value + '&vidVigenciaFutura=' + vIdVigenciaFutura.value + '&vValorVigenciaFutura=' + vValorVigenciaFutura.value + '&vAnioVigencia=' + vAnioVigencia.value;
            window_showModalDialog(url, setReturnGetRP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
          }
          function setReturnGetRP(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                //prePostbck(false);
                __doPostBack('<%= txtRP.ClientID %>', '');
            } else {
                ocultaImagenLoading();
            }
        }

         function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    
    <div>
         <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Numero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminaci&oacute;n de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>        
    </table>


        <ajax:tabcontainer ID="tcInfoContraro" runat="server" Visible="true" 
            ActiveTabIndex="0" CssClass="ajax__tab_green-theme"
            Width="97%" OnActiveTabChanged="tcInfoContraro_ActiveTabChanged" 
            AutoPostBack="false" >
            <Ajax:TabPanel  ID="ApPlanComprasProductos" runat="server">
                            <HeaderTemplate>
                                Plan de compras asociado / Productos
                            </HeaderTemplate> 
                            <ContentTemplate>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="lbPlanCompras" runat="server" Text="Plan de compras"></asp:Label>
                                            <asp:CustomValidator ID="cvPlanCompras" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="False" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaPlanCompras"></asp:CustomValidator>
                                        
                                        </td>
                                       
                                        <td>
                                           <asp:Label ID="lblValorPlanComprasContratos" runat="server" Text="Valor Plan de compras contrato"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            
                                            <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="False" Width="30%" ViewStateMode="Enabled"
                                                AutoPostBack="True"></asp:TextBox>
                                            <asp:ImageButton ID="imgPlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif" OnClientClick="GetPlanCompras(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        

                                        <td>
                                            <asp:TextBox ID="txtValorPlanComprasContratos"  
                                                runat="server" 
                                                
                                                onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')" Enabled="False" Width="250px"></asp:TextBox>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="gvConsecutivos" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                Width="100%" DataKeyNames="IDPlanDeComprasContratos,IDPlanDeCompras,Vigencia"
                                                CellPadding="8" Height="16px" CssClass="grillaCentral">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="IDPlanDeCompras" />
                                                    <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="headerForm" />
                                                <AlternatingRowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                                <RowStyle CssClass="rowBG" HorizontalAlign="Center" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="2">
                                            <asp:Label ID="LbProductos" runat="server" Text="Productos"></asp:Label>
                                            <asp:HiddenField ID="hfProductos" runat="server" />
                                            <asp:CustomValidator ID="cvProductos" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="False" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaProductos"></asp:CustomValidator>
                                        </td>
                                        
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />
                                                    <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="2">
                                            Rubros Plan de Compras
                                        </td>
                                      
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="False"
                                                GridLines="None" Width="100%" DataKeyNames="total_rubro" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código Rubro" DataField="codigo_rubro" />
                                                    <asp:BoundField HeaderText="Descripción del Rubro" DataField="NombreRubro" />
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                    
                                </table>
                            </ContentTemplate>     
                </Ajax:TabPanel>         

            <Ajax:TabPanel ID="ApCDP" runat="server">
                            <HeaderTemplate>
                                CDP
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbCDP" runat="server" Text="CDP"></asp:Label>
                                            <asp:CustomValidator ID="cvCDP" runat="server" ErrorMessage="Campo Requerido" Enabled="false"
                                                ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCDP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCDP" runat="server" />
                                            <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" 
                                                AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="true" OnClientClick="GetCDP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
<%--                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false"  DataKeyNames="IdContratosCDP,CodigoRubro,ValorCDP"
                                                AllowPaging="True" OnPageIndexChanging="gvCDP_PageIndexChanging" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" Visible='<%# Eval("Estado").ToString() == "3" %>'  OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion();"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP" Visible="false" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>--%>
                                     <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdContratosCDP,CodigoRubro,ValorCDP,ValorRubro"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion('CDP');"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP"  DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                     <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="CodigoRubro"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                        <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                        <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DependenciaAfectacionGastos"/>
                                                        <asp:BoundField HeaderText="Tipo Fuente Financiamiento" DataField="TipoFuenteFinanciamiento"/>
                                                        <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                                                        <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
                                                        <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="TipoSituacionFondos"/>
                                                        <asp:BoundField HeaderText="Valor Rubro" DataField="ValorRubro"  DataFormatString="{0:C}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
            </Ajax:TabPanel>

            <Ajax:TabPanel ID="ApRP" runat="server">
                            <HeaderTemplate>
                                RP
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbRP" runat="server" Text="RP"></asp:Label>
                                            <asp:CustomValidator ID="cvRP" runat="server" ErrorMessage="Campo Requerido" Enabled="false"
                                                ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaRP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfRP" runat="server" />
                                            <asp:TextBox ID="txtRP" runat="server" Enabled="false" ViewStateMode="Enabled" 
                                                AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgRP" runat="server"  CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="False" OnClientClick="GetRP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdRPContrato,IdRegional,ValorInicialRP,NumeroRP"
                                                AllowPaging="True" OnPageIndexChanging="gvCDP_PageIndexChanging" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarRPClick" OnClientClick="return ValidaEliminacion();"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Regional" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                                                    <asp:BoundField HeaderText="Id RP" DataField="IdRP" Visible="false"  SortExpression="IdRP"/>
                                                    <asp:BoundField HeaderText="Valor Inicial RP" DataField="ValorInicialRP"  SortExpression="ValorInicialRP" DataFormatString="{0:C}" />
                                                   <asp:BoundField HeaderText="Valor Actual  RP" DataField="ValorActualRP"  SortExpression="ValorActualRP" DataFormatString="{0:C}" />
                                                   <asp:BoundField HeaderText="Número RP" DataField="NumeroRP" SortExpression="NumeroRP"/>                                            
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                      </asp:GridView>
                                      </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
</Ajax:TabPanel>

</ajax:tabcontainer>


</div>
    
    
   
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
    </style>
</asp:Content>






