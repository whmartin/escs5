﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using WsContratosPacco;

/// <summary>
/// Página de visualización detallada para la entidad AsociarRPContrato
/// </summary>
public partial class Page_Contratos_GestionarVigencias_Add : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GestionarVigencias";
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();
    
    public string nConsecutivosPlanCompras = "0";
    public string nProductos = "0";
    public string TotalProductos = "0";
    public string TotalProductosCDP = "";  

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                string sControlName = Request.Params.Get("__EVENTTARGET");
                switch (sControlName)
                {

                    case "cphCont_tcInfoContraro_ApCDP_txtCDP":
                        txtCDPTextChanged(sender, e);
                        break;
                    case "cphCont_tcInfoContraro_ApRP_txtRP":
                        txtRPTextChanged(sender, e);
                        break;
                    case "cphCont_tcInfoContraro_ApPlanComprasProductos_txtPlanCompras":
                        PlanComprasChanged(sender,e);
                        break;
                }
            }
        }
    }

    private void PlanComprasChanged(object sender, EventArgs e)
    {
        HabilitarLupas();
        SeleccionarPlanCompras("1");
        toolBar.LipiarMensajeError();
    }

    private void txtCDPTextChanged(object sender, EventArgs e)
    {
        HabilitarLupas();
        SeleccionarCDP("1");
        ValidacionesCDP();
            
    }
    private void txtRPTextChanged(object sender, EventArgs e)
    {
        HabilitarLupas();
        SeleccionarRP("1");
        SeleccionarCDP("1");

    }   

    //protected void btnNuevo_Click(object sender, EventArgs e)
    //{
    //    //RemoveSessionParameter("Contrato.IdContrato");
    //    RemoveSessionParameter("Contrato.ContratosAPP");
    //    NavigateTo("ListRP.aspx");
    //}
    //protected void btnEditar_Click(object sender, EventArgs e)
    //{
    //    RemoveSessionParameter("Contrato.IdContrato");
    //    RemoveSessionParameter("Contrato.ContratosAPP");
    //    NavigateTo(SolutionPage.Edit);
    //}
    //protected void btnEliminar_Click(object sender, EventArgs e)
    //{
    //    RemoveSessionParameter("Contrato.IdContrato");
    //    RemoveSessionParameter("Contrato.ContratosAPP");
    //    EliminarRegistro();
    //}
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("Contrato.GuardadoRP").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La información ha sido registrada en forma exitosa");
            RemoveSessionParameter("Contrato.GuardadoRP");

            int vIdContrato = Convert.ToInt32(GetSessionParameter("Vigencia.IDContratoConsultado"));
            hfIDContrato.Value = vIdContrato.ToString();
            RemoveSessionParameter("Vigencia.IDContratoConsultado");
            

            var VigenciaCont = vContratoService.ConsultarVigenciaFuturasporContrato(vIdContrato);
            hfIdVigenciaFutura.Value = VigenciaCont.IDVigenciaFuturas.ToString();
            hfAnioVigencia.Value = VigenciaCont.AnioVigencia.ToString();
            hfValorVigencia.Value = VigenciaCont.ValorVigenciaFutura.ToString();

            List<Contrato> lContrato = new List<Contrato>();    

            lContrato = vContratoService.ConsultarContratoss(null, null, vIdContrato, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            var vContrato = vContratoService.ConsultarContrato(vIdContrato);

            txtContrato.Text = vContrato.NumeroContrato;
            txtRegional.Text = vContrato.NombreRegional;
            hfIdRegional.Value = vContrato.IdRegionalContrato.ToString();

            txtobjeto.Text = vContrato.ObjetoContrato;
            txtalcance.Text = vContrato.AlcanceObjetoContrato;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();
            HabilitarLupas();
            SeleccionarPlanCompras("1");
            ValidacionesCDP();


            


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        int vIdContrato = Convert.ToInt32(hfIDContrato.Value);
        var myGridResults = vContratoService.ConsultarRPContratosAsociados(vIdContrato, null);


        //////////////////////////////////////////////////////////////////////////////////
        //////Fin del código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        gridViewsender.DataSource = myGridResults;
        if (expresionOrdenamiento != null)
        {
            //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
            if (string.IsNullOrEmpty(GridViewSortExpression))
            {
                GridViewSortDirection = SortDirection.Ascending;
            }
            else if (GridViewSortExpression != expresionOrdenamiento)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            if (myGridResults != null)
            {
                var param = Expression.Parameter(typeof(RPContrato), expresionOrdenamiento);

                //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                var prop = Expression.Property(param, expresionOrdenamiento);

                //Creo en tiempo de ejecución la expresión lambda
                var sortExpression = Expression.Lambda<Func<RPContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                //Dependiendo del modo de ordenamiento . . .
                if (GridViewSortDirection == SortDirection.Ascending)
                {

                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                    if (cambioPaginacion == false)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                    }
                    else
                    {
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                    }
                }
                else
                {

                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                    if (cambioPaginacion == false)
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                    }
                    else
                    {
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                    }
                }

                GridViewSortExpression = expresionOrdenamiento;
            }
        }
        else
        {
            gridViewsender.DataSource = myGridResults;
        }

        gridViewsender.DataBind();

    }

    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

            //AsociarRPContrato vAsociarRPContrato = new AsociarRPContrato();
            //vAsociarRPContrato = vContratoService.ConsultarAsociarRPContrato();
            //InformacionAudioria(vAsociarRPContrato, this.PageName, vSolutionPage);
            //int vResultado = vContratoService.EliminarAsociarRPContrato(vAsociarRPContrato);
            //if (vResultado == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            //    SetSessionParameter("AsociarRPContrato.Eliminado", "1");
            //    NavigateTo(SolutionPage.List);
            //}
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {

            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);


            toolBar.EstablecerTitulos("Gestion de Vigencias Futuras", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void HabilitarPlanDeCompras(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgPlanCompras.Enabled = false;
            gvConsecutivos.DataSource = null;
            gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = "0";
            gvProductos.DataSource = null;
            gvProductos.DataBind();
            nProductos = "0";
            gvRubrosPlanCompras.DataSource = null;
            gvRubrosPlanCompras.DataBind();
            lbPlanCompras.Text = "Plan de compras";
            LbProductos.Text = "Productos";
        }
        else if (habilitar == true)
        {
            imgPlanCompras.Enabled = true;           
            lbPlanCompras.Text = "Plan de compras *";
            LbProductos.Text = "Productos *";
        }
        else
        {
            imgPlanCompras.Enabled = false;           
            lbPlanCompras.Text = "Plan de compras";
            LbProductos.Text = "Productos";
        }
    }

    protected void gvRegistroPresupuestalContrato_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvRegistroPresupuestalContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    //protected void gvRegistroPresupuestalContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    gvRegistroPresupuestalContrato.PageIndex = e.NewPageIndex;
    //    CargarGrilla((GridView)sender, GridViewSortExpression, true);
    //}
    protected void tcInfoContraro_ActiveTabChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }   

    private void SeleccionarPlanCompras(string idPlanDeComprasSeleccionado)
    {
        if (!string.IsNullOrEmpty(idPlanDeComprasSeleccionado) && idPlanDeComprasSeleccionado != "-1")
        {
            int idContrato = Convert.ToInt32(hfIDContrato.Value);

            List<PlanDeComprasContratos> consecutivosPlanCompras = vContratoService.ConsultarPlanDeComprasContratosVigencias(idContrato, null,true,Convert.ToInt32(hfAnioVigencia.Value));
            gvConsecutivos.DataSource = consecutivosPlanCompras;
            gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = gvConsecutivos.Rows.Count.ToString();

            if (consecutivosPlanCompras.Count() > 0)
            {
                decimal? valorTotal = null;
                decimal valorTotalItem = 0;

                foreach (var item in consecutivosPlanCompras)
                {
                    if (item.ValorTotal.HasValue)
                        valorTotalItem += item.ValorTotal.Value;
                }

                if (valorTotalItem > 0)
                    valorTotal = valorTotalItem;

                decimal? sumatoriaProductos = 0;
                decimal? sumatoriaRubros = 0;

                var client = new WSContratosPACCOSoapClient();

                List<GetDetalleProductosServicio_Result> productos = new List<GetDetalleProductosServicio_Result>();

                List<GetDetalleRubrosServicio_Result> rubros = new List<GetDetalleRubrosServicio_Result>();

                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras)
                {
                    var itemsDetallePACCO = client.GetListaDetalleProductoPACCO(pc.Vigencia, pc.IDPlanDeCompras);

                    foreach (GetDetalleProductosServicio_Result pWs in itemsDetallePACCO)
                    {
                        sumatoriaProductos += pWs.valor_total;
                        productos.Add(pWs);
                    }

                    var itemsRubrosPACCO = client.GetListaDetalleRubroPACCO(pc.Vigencia, pc.IDPlanDeCompras);

                    foreach (GetDetalleRubrosServicio_Result pWs in itemsRubrosPACCO)
                    {
                        sumatoriaRubros += pWs.total_rubro;
                        rubros.Add(pWs);
                    }                    
                }

                decimal valorTotalD = valorTotal.HasValue ? valorTotal.Value : sumatoriaProductos.Value;
                TotalProductos = valorTotalD.ToString();
                // PrecargaValorInicialValorProductosPlanCompras();
                hfValorPlanCompras.Value = valorTotalD.ToString();
                txtValorPlanComprasContratos.Text = valorTotalD.ToString("#,###0.00##;($ #,###0.00##)");

                gvProductos.DataSource = productos;
                gvProductos.DataBind();
                nProductos = gvProductos.Rows.Count.ToString();

                gvRubrosPlanCompras.DataSource = rubros;
                gvRubrosPlanCompras.DataBind();

                #region ListaConsecutivoPlanCompras
                

                #endregion

                //vContratoService.ModificarAporteContratoValor(idContrato, Convert.ToDecimal(TotalProductos), GetSessionUser().NombreUsuario);
                SeleccionarCDP("1");               
                
                //txtValorApoICBFTextChanged(null, EventArgs.Empty);
            }
            else
            {
                gvProductos.DataSource = null;
                gvProductos.DataBind();

                gvRubrosPlanCompras.DataSource = null;
                gvRubrosPlanCompras.DataBind();

                TotalProductos = string.Empty;
                
                //vContratoService.EliminarAporteContratoValor(int.Parse(hfIDContrato.Value));
                txtValorPlanComprasContratos.Text = "";
               // txtValorApoICBFTextChanged(null, EventArgs.Empty);
            }
        }
        else
        {
            TotalProductos = string.Empty;
            
        }
    }
    
    protected void btnEliminarCDPClick(object sender, EventArgs e)
    {
        EliminarCDP(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    
    protected void gvCDP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCDP.PageIndex = e.NewPageIndex;
        CargarGrillaCDP();
    }

    protected void gvRP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRP.PageIndex = e.NewPageIndex;
        CargarGrillaRP();
    }




    #region CDP

    private void HabilitarCDP(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgCDP.Enabled = false;
            gvCDP.DataSource = null;
            gvCDP.DataBind();
           
          
            lbCDP.Text = "CDP";
        }
        else if (habilitar == true)
        {
            imgCDP.Enabled = true;
            gvCDP.Enabled = true;
            //cvCDP.Enabled = true;
            lbCDP.Text = "CDP *";
        }
        else
        {
            imgCDP.Enabled = false;
            gvCDP.Enabled = false;
         
           
                lbCDP.Text = "CDP";
                //cvCDP.Enabled = false;
            
        }
    }

    private void SeleccionarCDP(string CDPseleccionado)
    {
        CargarGrillaCDP();       
    }

    private void CargarGrillaCDP()
    {
        //List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDPVigencias(Convert.ToInt32(hfIDContrato.Value),true,Convert.ToInt32(hfAnioVigencia.Value));
       
        //gvCDP.DataSource = cdps;
        //gvCDP.DataBind();

        gvCDP.EmptyDataText = EmptyDataText();
        gvCDP.PageSize = PageSize();
        gvRubrosCDP.EmptyDataText = EmptyDataText();
        gvRubrosCDP.PageSize = PageSize();

        List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDPLineaVF(Convert.ToInt32(hfIDContrato.Value), true, Convert.ToInt32(hfAnioVigencia.Value));
        gvCDP.DataSource = cdps;
        gvCDP.DataBind();

        List<ContratosCDP> rubrosCdp = vContratoService.ConsultarContratosCDPRubroLineaVF(Convert.ToInt32(hfIDContrato.Value), true, Convert.ToInt32(hfAnioVigencia.Value));
        gvRubrosCDP.DataSource = rubrosCdp;
        gvRubrosCDP.DataBind();

        SeleccionarRP("1");
    }

    private void ValidacionesCDP()
    {
        string result = string.Empty;

        bool isValid = true;

        foreach (GridViewRow gvrCDP in gvRubrosCDP.Rows)
        {
            isValid = false;

            string codigoRubroCDP = HttpUtility.HtmlDecode(gvRubrosCDP.DataKeys[gvrCDP.RowIndex]["CodigoRubro"].ToString()).Trim();

            foreach (GridViewRow gvrPlan in gvRubrosPlanCompras.Rows)
            {
                string codigoRubroPlan = HttpUtility.HtmlDecode(gvrPlan.Cells[1].Text).Trim();

                if (codigoRubroCDP == codigoRubroPlan)
                {
                    isValid = true;
                    break;
                }
            }

            if (!isValid)
            {
               toolBar.MostrarMensajeError("Verifique los rubros del plan de compras vs el CDP");
                break;
            }
        }

        if (result == string.Empty && !string.IsNullOrEmpty(txtValorPlanComprasContratos.Text))
        {
            var valorPlanCompras = decimal.Parse(txtValorPlanComprasContratos.Text);

            decimal valorCDP = 0;

            foreach (GridViewRow gvrCDP in gvCDP.Rows)
                valorCDP += decimal.Parse(HttpUtility.HtmlDecode(gvCDP.DataKeys[gvrCDP.RowIndex]["ValorCDP"].ToString()).Trim());

            if (valorCDP < valorPlanCompras)
                toolBar.MostrarMensajeError("Verifique el valor del CDP, debe ser mayor o igual al valor del Plan de Compras.");
        }

        var VigenciaCont = vContratoService.ConsultarVigenciaFuturasporContrato(Convert.ToInt32(hfIDContrato.Value));
        if (VigenciaCont.Estado == 3 || VigenciaCont.Estado == 4)
            HabilitarRP(true);
    }

    private void EliminarCDP(int rowIndex)
    {
        ContratosCDP vContratoCPD = new ContratosCDP();
        vContratoCPD.IdContratosCDP = Convert.ToInt32(gvCDP.DataKeys[rowIndex]["IdContratosCDP"]);
        vContratoCPD.UsuarioModifica = GetSessionUser().NombreUsuario;
        vContratoService.EliminarContratosCDPVigencias(vContratoCPD,Convert.ToInt32(hfIdVigenciaFutura.Value));
        HabilitarLupas();       
        SeleccionarCDP(string.Empty);
    }

    #endregion

    #region RP

    private void HabilitarRP(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgRP.Enabled = false;
            gvRP.DataSource = null;
            gvRP.DataBind();


            lbRP.Text = "RP";
        }
        else if (habilitar == true)
        {
            imgRP.Enabled = true;
            gvRP.Enabled = true;
            //cvCDP.Enabled = true;
            lbRP.Text = "RP *";
        }
        else
        {
            imgRP.Enabled = false;
            gvRP.Enabled = false;


            lbRP.Text = "RP";
            //cvCDP.Enabled = false;

        }
    }

    private void SeleccionarRP(string RPseleccionado)
    {
        CargarGrillaRP();
    }

    private void CargarGrillaRP()
    {
        List<RPContrato> RPs = vContratoService.ConsultarRPContratosAsociadosVigencias(Convert.ToInt32(hfIDContrato.Value),true, Convert.ToInt32(hfAnioVigencia.Value));
        gvRP.DataSource = RPs;
        gvRP.DataBind();
    }

    //private string ValidacionesRP()
    //{
    //    // Paso 60
    //    bool seEncontroRubro;
    //    foreach (GridViewRow gvrCDP in gvCDP.Rows)
    //    {
    //        seEncontroRubro = false;
    //        string codigoRubroCDP = HttpUtility.HtmlDecode(gvCDP.DataKeys[gvrCDP.RowIndex]["CodigoRubro"].ToString()).Trim();

    //        foreach (GridViewRow gvrPlan in gvRubrosPlanCompras.Rows)
    //        {
    //            string codigoRubroPlan = HttpUtility.HtmlDecode(gvrPlan.Cells[1].Text).Trim();
    //            if (codigoRubroCDP.Equals(codigoRubroPlan))
    //            {
    //                seEncontroRubro = true;
    //                break;
    //            }
    //        }

    //        if (!seEncontroRubro)
    //        {
    //            return "Verifique los rubros del plan de compras vs el CDP\\n";
    //        }
    //    }

    //    decimal valorPlanCompras = decimal.Parse(TotalProductos);

    //    // Paso 61
    //    decimal valorRubroCDP;
    //    foreach (GridViewRow gvrPlan in gvRubrosPlanCompras.Rows)
    //    {
    //        TotalProductosCDP = "0";
    //        valorRubroCDP = 0;
    //        string codigoRubroPlan = HttpUtility.HtmlDecode(gvrPlan.Cells[1].Text);

    //        foreach (GridViewRow gvrCDP in gvCDP.Rows)
    //        {
    //            string codigoRubroCDP = HttpUtility.HtmlDecode(gvCDP.DataKeys[gvrCDP.RowIndex]["CodigoRubro"].ToString());
    //            if (codigoRubroPlan.Equals(codigoRubroCDP))
    //            {
    //                valorRubroCDP += decimal.Parse(gvCDP.DataKeys[gvrCDP.RowIndex]["ValorRubro"].ToString());
    //            }
    //        }

    //        //if (valorRubroCDP >= valorPlanCompras)
    //        //{
    //        //    return "Verifique el valor del CDP, debe ser mayor  o igual al valor del Plan de Compras.";
    //        //}
    //    }

    //    return string.Empty;
    //}

    private void EliminarRP(int rowIndex)
    {
        RPContrato vContratoRP = new RPContrato();
        vContratoRP.IdRPContrato = Convert.ToInt32(gvRP.DataKeys[rowIndex]["IdRPContrato"]);
        vContratoRP.UsuarioModifica = GetSessionUser().NombreUsuario;
        vContratoService.EliminarRPContratos(vContratoRP,Convert.ToInt32( hfIdVigenciaFutura.Value));
        HabilitarLupas();
        SeleccionarRP(string.Empty);
        SeleccionarCDP("1");
    }
    protected void btnEliminarRPClick(object sender, EventArgs e)
    {
        EliminarRP(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    private void HabilitarLupas()
    {
        var VigenciaCont = vContratoService.ConsultarVigenciaFuturasporContrato(Convert.ToInt32(hfIDContrato.Value));
        if (VigenciaCont.Estado == 1 || VigenciaCont.Estado == 2)
            HabilitarPlanDeCompras(true);
        else HabilitarPlanDeCompras(false);
        if (VigenciaCont.Estado == 2 || VigenciaCont.Estado == 3)
            HabilitarCDP(true);
        else HabilitarCDP(false);
        //if (VigenciaCont.Estado == 3 || VigenciaCont.Estado == 4)
        //   HabilitarRP(true);
        
    }
    #endregion
}
