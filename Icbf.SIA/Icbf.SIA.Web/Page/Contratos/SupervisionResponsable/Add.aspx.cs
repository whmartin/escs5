using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionResponsables_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionResponsable";
    SupervisionResponsables vSupervisionResponsablesOld = new SupervisionResponsables();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionResponsables vSupervisionResponsables = new SupervisionResponsables();

            vSupervisionResponsables.IdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            vSupervisionResponsables.Nombre = Convert.ToString(txtNombre.Text);
            vSupervisionResponsables.Estado = Convert.ToInt32(rblEstado.SelectedValue);

            if (hfIdResponsable.Value != "")
            {  vSupervisionResponsables.IdResponsable = Convert.ToInt32(hfIdResponsable.Value); }

            
            int vResultExis = vSupervisionService.ConsultarSupervisionResponsables(vSupervisionResponsables.Nombre);

            if (Request.QueryString["oP"] == "E")
            {

                if (vResultExis == 0)
                {
                    vSupervisionResponsables.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionResponsables, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.ModificarSupervisionResponsables(vSupervisionResponsables);
                }
                else
                {
                    toolBar.MostrarMensajeError("El responsable ingresado ya existe");
                    vResultado = 3;
                }

            }
            else
            {
                if (vResultExis == 0)
                {
                    vSupervisionResponsables.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionResponsables, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.InsertarSupervisionResponsables(vSupervisionResponsables);
                }
                else
                {
                    toolBar.MostrarMensajeError("El responsable ingresado ya existe");
                    vResultado = 3;
                }

            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SupervisionResponsables.IdResponsable", vSupervisionResponsables.IdResponsable);
                SetSessionParameter("SupervisionResponsables.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado != 3)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Responsables", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdResponsable = Convert.ToInt32(GetSessionParameter("SupervisionResponsables.IdResponsable"));
            RemoveSessionParameter("SupervisionResponsables.Id");

            SupervisionResponsables vSupervisionResponsables = new SupervisionResponsables();
            vSupervisionResponsables = vSupervisionService.ConsultarSupervisionResponsables(vIdResponsable);
            hfIdResponsable.Value = vSupervisionResponsables.IdResponsable.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionResponsables.IdDireccion.ToString();
            txtNombre.Text = vSupervisionResponsables.Nombre;

            vSupervisionResponsablesOld.IdResponsable = vIdResponsable;
            vSupervisionResponsablesOld.IdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            vSupervisionResponsablesOld.Nombre = txtNombre.Text;
            vSupervisionResponsablesOld.Estado = Convert.ToInt16(rblEstado.SelectedValue);

            rblEstado.SelectedValue = vSupervisionResponsables.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionResponsables.UsuarioCrea, vSupervisionResponsables.FechaCrea, vSupervisionResponsables.UsuarioModifica, vSupervisionResponsables.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
