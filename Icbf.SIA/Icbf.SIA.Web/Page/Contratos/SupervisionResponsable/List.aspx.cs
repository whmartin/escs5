using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionResponsables_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionResponsable";
    SupervisionService vSupervisionService = new SupervisionService();
    List<SupervisionDireccion> lstDireccion;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdDireccion = null;
            String vNombre = null;
            int? vEstado = null;
            if (ddlIdDireccion.SelectedValue != "-1")
            {
                vIdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            lstDireccion = vSupervisionService.ConsultarSupervisionDireccions(null, "", "", 1);
            gvSupervisionResponsables.DataSource = vSupervisionService.ConsultarSupervisionResponsabless(vIdDireccion, vNombre, vEstado);
            gvSupervisionResponsables.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionResponsables.PageSize = PageSize();
            gvSupervisionResponsables.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Responsables", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionResponsables.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionResponsables.IdResponsable", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionResponsables_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionResponsables.SelectedRow);
    }
    protected void gvSupervisionResponsables_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionResponsables.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionResponsables.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionResponsables.Eliminado");

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionResponsables_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Se busca el nombre de las direcciones
            var vRegistrosDireccion = from resp in lstDireccion
                                      where resp.IdDireccionesICBF == Convert.ToInt16(e.Row.Cells[1].Text)
                                      select resp;
            e.Row.Cells[1].Text = vRegistrosDireccion.ToList()[0].NombreDireccion;

            //Se busca el estado
            switch (e.Row.Cells[3].Text)
            {
                case "1":
                    e.Row.Cells[3].Text = "Activo";
                    break;
                default:
                    e.Row.Cells[3].Text = "Desactivado";
                    break;
            }
        }
    }
}
