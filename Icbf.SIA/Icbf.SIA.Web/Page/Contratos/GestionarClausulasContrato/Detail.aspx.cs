using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de las cláuslas de contrato
/// </summary>
public partial class Page_GestionarClausulasContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GestionarClausulasContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("GestionarClausulasContrato.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("GestionarClausulasContrato.IdGestionClausula", hfIdGestionClausula.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdGestionClausula = Convert.ToInt32(GetSessionParameter("GestionarClausulasContrato.IdGestionClausula"));
            RemoveSessionParameter("GestionarClausulasContrato.IdGestionClausula");

            if (GetSessionParameter("GestionarClausulasContrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("GestionarClausulasContrato");


            GestionarClausulasContrato vGestionarClausulasContrato = new GestionarClausulasContrato();
            vGestionarClausulasContrato = vContratoService.ConsultarGestionarClausulasContrato(vIdGestionClausula);
            hfIdGestionClausula.Value = vGestionarClausulasContrato.IdGestionClausula.ToString();
            txtIdContrato.Text = vGestionarClausulasContrato.IdContrato;
            txtTipoContrato.Text = GetSessionParameter("GestionarClausulaContrato.TipoContrato").ToString();
            txtIdClausula.Text = GetSessionParameter("GestionarClausulaContrato.IdClausula").ToString();
            txtNombreClausula.Text = vGestionarClausulasContrato.NombreClausula;
            txtTipoClausula.Text = vGestionarClausulasContrato.TipoClausula.ToString();
            txtOrden.Text = vGestionarClausulasContrato.OrdenNumero.ToString();
            txtAlias.Text = vGestionarClausulasContrato.Orden;
            txtDescripcionClausula.Text = vGestionarClausulasContrato.DescripcionClausula;
            ObtenerAuditoria(PageName, hfIdGestionClausula.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGestionarClausulasContrato.UsuarioCrea, vGestionarClausulasContrato.FechaCrea, vGestionarClausulasContrato.UsuarioModifica, vGestionarClausulasContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdGestionClausula = Convert.ToInt32(hfIdGestionClausula.Value);

            GestionarClausulasContrato vGestionarClausulasContrato = new GestionarClausulasContrato();
            vGestionarClausulasContrato = vContratoService.ConsultarGestionarClausulasContrato(vIdGestionClausula);
            InformacionAudioria(vGestionarClausulasContrato, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarGestionarClausulasContrato(vGestionarClausulasContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("GestionarClausulasContrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Gestionar Cl&#225;usulas Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
