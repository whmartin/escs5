<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_GestionarClausulasContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdGestionClausula" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                C&#243;digo Contrato *
            </td>
            <td>
                Contrato/Tipo Contrato *
            </td>
            <td>
                C&#243digo Cl&#225;usula *
            </td>
            <td>
                Nombre Cl&#225;usula *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"  Enabled="false"  onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoContrato" ClientIDMode="Static"  onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdClausula" ClientIDMode="Static"  onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreClausula"  Enabled="false"  onfocus=blur(); class="enable"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Cl&#225;usula *
            </td>
            <td>
                Orden *
            </td>
            <td>
                Alias *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoClausula" onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtOrden"   onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtAlias" Width="300px"  onfocus=blur(); class="enable"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Descripci&#243;n Cl&#225;usula *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionClausula"  
                    Height="50px" MaxLength="128" TextMode="MultiLine" Width="320px"  onfocus=blur(); class="enable"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
