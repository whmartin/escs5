using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de cl�usulas de contrato
/// </summary>
public partial class Page_GestionarClausulasContrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GestionarClausulasContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdGestionClausula = null;
            String vIdContrato = null;
            String vNombreClausula = null;
            int? vTipoClausula = null;
            String vOrden = null;
            int? vOrdenNumero = null;
            String vDescripcionClausula = null;
           
            if (txtIdContrato.Text!= "")
            {
                vIdContrato = txtIdContrato.Text;
            }
            if (txtNombreClausula.Text!= "")
            {
                vNombreClausula = txtNombreClausula.Text;
            }
            if (txtTipoClausula.Text!= "")
            {
                vTipoClausula = Convert.ToInt32(txtTipoClausula.Text);
            }
            if (txtAlias.Text != "")
            {
                vOrden = txtAlias.Text;
            }
            if (txtOrden.Text != "")
            {
                vOrdenNumero = Convert.ToInt32(txtOrden.Text); ;
            }
            if (txtDescripcionClausula.Text!= "")
            {
                vDescripcionClausula = Convert.ToString(txtDescripcionClausula.Text);
            }
            gvGestionarClausulasContrato.DataSource = vContratoService.ConsultarGestionarClausulasContratos(vIdGestionClausula, vIdContrato, vNombreClausula, vTipoClausula, vOrden, vOrdenNumero, vDescripcionClausula);
            gvGestionarClausulasContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvGestionarClausulasContrato.PageSize = PageSize();
            gvGestionarClausulasContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gestionar Cl&#225;usulas Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvGestionarClausulasContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("GestionarClausulasContrato.IdGestionClausula", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvGestionarClausulasContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvGestionarClausulasContrato.SelectedRow);
    }
    protected void gvGestionarClausulasContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGestionarClausulasContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("GestionarClausulasContrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("GestionarClausulasContrato.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvGestionarClausulasContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        try
        {
            int? vIdGestionClausula = null;
            String vIdContrato = null;
            String vNombreClausula = null;
            int? vTipoClausula = null;
            String vOrden = null;
            int? vOrdenNumero = null;
            String vDescripcionClausula = null;
            
            if (txtIdContrato.Text != "")
            {
                vIdContrato = Convert.ToString(txtIdContrato.Text);
            }
            if (txtNombreClausula.Text != "")
            {
                vNombreClausula = Convert.ToString(txtNombreClausula.Text);
            }
            if (txtTipoClausula.Text != "")
            {
                vTipoClausula = Convert.ToInt32(txtTipoClausula.Text);
            }
            if (txtOrden.Text != "")
            {
                vOrden = Convert.ToString(txtOrden.Text);
            }
            if (txtDescripcionClausula.Text != "")
            {
                vDescripcionClausula = Convert.ToString(txtDescripcionClausula.Text);
            }
            var myGridResults = vContratoService.ConsultarGestionarClausulasContratos(vIdGestionClausula, vIdContrato, vNombreClausula, vTipoClausula, vOrden, vOrdenNumero, vDescripcionClausula);
            if (myGridResults != null)
            {
                var param = Expression.Parameter(typeof(GestionarClausulasContrato), e.SortExpression);

                var prop = Expression.Property(param, e.SortExpression);

                var sortExpression = Expression.Lambda<Func<GestionarClausulasContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                //Dependiendo del modo de ordenamiento . . .
                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    gvGestionarClausulasContrato.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                    GridViewSortDirection = SortDirection.Descending;
                }
                else
                {
                    gvGestionarClausulasContrato.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                    GridViewSortDirection = SortDirection.Ascending;
                }

                gvGestionarClausulasContrato.DataBind();
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void txtOrden_TextChanged(object sender, EventArgs e)
    {
        convierteNumeroLetraCardinal(Convert.ToInt32(txtOrden.Text));
    }

    protected void convierteNumeroLetraCardinal(int numero)
    {
        string[] unidades = new string[] {"Primera", "Segunda", "Tercera" ,"Cuarta" ,"Quinta" ,"Sexta" ,  
         "Septima" ,"Octava" ,"Novena" ,"Decima"};

        string[] decenas = new string[] {"Decima","Vigesima", "Trigesima","Cuadragesima",  
         "Quincuagesima","Sexagesima", "Septuagesima",   
         "Octogesima", "Nonagesima"};

        string[] centenas = new string[] {"Centesima","Ducentesima", "Tricentesima","Cuadringentesima",  
         "Quingentesima", "Sexcentesima", "Septingentesima",   
         "Octingentesima", "Noningentesima"};

        int num = numero;


        if (num > 0 && num < 11)
        {
            txtAlias.Text = unidades[num - 1];
        }
        else if (num < 1000)
        {
            int c = num / 100;
            int d = (num - c * 100) / 10;
            int u = num % 10;
            if (u == 0 && d == 0)
                txtAlias.Text = centenas[c - 1];
            else if (u == 0 && c == 0)
                txtAlias.Text = decenas[d - 1];
            else if (d == 0 && c == 0)
                txtAlias.Text = unidades[u - 1];
            else if (c == 0)
                txtAlias.Text = decenas[d - 1] + " " + unidades[u - 1];
            else
            {
                string centena = string.Empty;
                string decena = string.Empty;
                string unidad = string.Empty;
                if (c == 0)
                {
                    centena = "";
                }
                else
                {
                    centena = centenas[c - 1];
                }
                if (d == 0)
                {
                    decena = "";
                }
                else
                {
                    decena = decenas[d - 1];
                }
                if (u == 0)
                {
                    unidad = "";
                }
                else
                {
                    unidad = unidades[u - 1];
                }
                txtAlias.Text = centena + " " + decena + " " + unidad;
            }

        }
    }  
}
