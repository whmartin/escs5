using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de clausulas contrato
/// </summary>
public partial class Page_GestionarClausulasContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/GestionarClausulasContrato";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
        
    }

    /// <summary>
    /// Manejador de eventos click para el botón guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            String fecha = GetSessionParameter("Clausula.FechaSuscripcion").ToString();
            if (fecha != null && !fecha.Equals(String.Empty))
                throw new Exception("El contrato ya se encuentra suscrito");

            int vResultado;
            GestionarClausulasContrato vGestionarClausulasContrato = new GestionarClausulasContrato();

            vGestionarClausulasContrato.IdContrato = Convert.ToString(txtIdContrato.Text);
            vGestionarClausulasContrato.NombreClausula = Convert.ToString(txtNombreClausula.Text);
            vGestionarClausulasContrato.TipoClausula = Convert.ToInt32(txtIdClausula.Text);// Convert.ToInt32(txtTipoClausula.Text);
            vGestionarClausulasContrato.Orden = Convert.ToString(txtAlias.Text);
            vGestionarClausulasContrato.OrdenNumero = Convert.ToInt32(txtOrden.Text);
            vGestionarClausulasContrato.DescripcionClausula = Convert.ToString(txtDescripcionClausula.Text);

            SetSessionParameter("GestionarClausulaContrato.TipoContrato", txtTipoContrato.Text);
            SetSessionParameter("GestionarClausulaContrato.IdClausula", txtIdClausula.Text);
            SetSessionParameter("GestionarClausulaContrato.NombreClausula", txtNombreClausula.Text);

            if (Request.QueryString["oP"] == "E")
            {
            vGestionarClausulasContrato.IdGestionClausula = Convert.ToInt32(hfIdGestionClausula.Value);
                vGestionarClausulasContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vGestionarClausulasContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarGestionarClausulasContrato(vGestionarClausulasContrato);
            }
            else
            {
                vGestionarClausulasContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vGestionarClausulasContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarGestionarClausulasContrato(vGestionarClausulasContrato);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("GestionarClausulasContrato.IdGestionClausula", vGestionarClausulasContrato.IdGestionClausula);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("GestionarClausulasContrato.Modificado", "1");
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                }
                else
                {
                    SetSessionParameter("GestionarClausulasContrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }                
                
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Gestionar Cl&#225;usulas Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos provenientes de la entidad a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdGestionClausula = Convert.ToInt32(GetSessionParameter("GestionarClausulasContrato.IdGestionClausula"));
            RemoveSessionParameter("GestionarClausulasContrato.Id");
                 
            GestionarClausulasContrato vGestionarClausulasContrato = new GestionarClausulasContrato();
            vGestionarClausulasContrato = vContratoService.ConsultarGestionarClausulasContrato(vIdGestionClausula);
            hfIdGestionClausula.Value = vGestionarClausulasContrato.IdGestionClausula.ToString();
            txtIdContrato.Text = vGestionarClausulasContrato.IdContrato;
            txtTipoContrato.Text = GetSessionParameter("GestionarClausulaContrato.TipoContrato").ToString();
            txtIdClausula.Text = GetSessionParameter("GestionarClausulaContrato.IdClausula").ToString();
            txtNombreClausula.Text = GetSessionParameter("GestionarClausulaContrato.NombreClausula").ToString();
            txtTipoClausula.Text = vGestionarClausulasContrato.TipoClausula.ToString();
            txtAlias.Text = vGestionarClausulasContrato.Orden;
            txtOrden.Text = vGestionarClausulasContrato.OrdenNumero.ToString();
            txtDescripcionClausula.Text = vGestionarClausulasContrato.DescripcionClausula;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGestionarClausulasContrato.UsuarioCrea, vGestionarClausulasContrato.FechaCrea, vGestionarClausulasContrato.UsuarioModifica, vGestionarClausulasContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void txtOrden_TextChanged(object sender, EventArgs e)
    {
        convierteNumeroLetraCardinal(Convert.ToInt32(txtOrden.Text));
    }

    protected void convierteNumeroLetraCardinal(int numero)  
{  
        string[] unidades = new string[] {"Primera", "Segunda", "Tercera" ,"Cuarta" ,"Quinta" ,"Sexta" ,  
         "Septima" ,"Octava" ,"Novena" ,"Decima"};

        string[] decenas = new string[] {"Decima","Vigesima", "Trigesima","Cuadragesima",  
         "Quincuagesima","Sexagesima", "Septuagesima",   
         "Octogesima", "Nonagesima"};

        string[] centenas = new string[] {"Centesima","Ducentesima", "Tricentesima","Cuadringentesima",  
         "Quingentesima", "Sexcentesima", "Septingentesima",   
         "Octingentesima", "Noningentesima"};

        int num = numero;  
  
    
    if (num > 0 && num < 11)
    {
        txtAlias.Text = unidades[num - 1];
    }
    else if (num < 1000)
    {
        int c = num / 100;
        int d = (num - c * 100) / 10;
        int u = num % 10;
        if (u == 0 && d == 0)
            txtAlias.Text = centenas[c - 1];
        else if (u == 0 && c == 0)
            txtAlias.Text = decenas[d - 1];
        else if (d == 0 && c == 0)
            txtAlias.Text = unidades[u - 1];
        else if (c == 0)
            txtAlias.Text = decenas[d - 1] + " " +   unidades[u - 1];
        else
        {
            string centena = string.Empty;
            string decena = string.Empty;
            string unidad = string.Empty;
            if (c == 0)
            {
                centena = "";
            }else{
                centena = centenas[c - 1];
            }
            if (d == 0)
            {
                decena = "";
            }else{
                decena =  decenas[d - 1];
            }
            if (u == 0)
            {
                unidad = "";
            }else{
                unidad = unidades[u - 1];
            }
            txtAlias.Text = centena + " " + decena + " " + unidad;
        }
            
    }
}  
}
