<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_GestionarClausulasContrato_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdGestionClausula" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtIdContrato"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Contrato/Tipo Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoContrato" ControlToValidate="txtTipoContrato"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Id Gestión Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdClausula" ControlToValidate="txtIdClausula"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Nombre Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreClausula" ControlToValidate="txtNombreClausula"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td valign="middle">
                <asp:TextBox runat="server" ID="txtIdContrato" ClientIDMode="Static" MaxLength="3"
                    onfocus="blur();" class="enable"></asp:TextBox>
            </td>
            <td valign="middle">
                <asp:TextBox runat="server" ID="txtTipoContrato" ClientIDMode="Static" MaxLength="50"
                    onfocus="blur();" class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTipoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIdTipoContrato" />
            </td>
            <td valign="middle">
                <asp:TextBox runat="server" ID="txtIdClausula" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
            </td>
            <td valign="middle">
                <asp:TextBox runat="server" ID="txtNombreClausula" ClientIDMode="Static" MaxLength="10"
                    onfocus="blur();" class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreClausula" runat="server" TargetControlID="txtNombreClausula"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
                <asp:Image ID="imgBClausula" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetClausula()" Style="cursor: hand" ToolTip="Buscar" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Orden *
                <asp:RequiredFieldValidator runat="server" ID="rfvOrden" ControlToValidate="txtOrden"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Alias *
                <asp:RequiredFieldValidator runat="server" ID="rfvAlias" ControlToValidate="txtAlias"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td valign="top">
                <asp:TextBox runat="server" ID="txtOrden" OnTextChanged="txtOrden_TextChanged" AutoPostBack="True"
                    MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftOrden" runat="server" TargetControlID="txtOrden"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtAlias" Width="300px" MaxLength="128" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAlias"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
                <asp:TextBox runat="server" ID="txtTipoClausula" Visible="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoClausula" runat="server" TargetControlID="txtTipoClausula"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Descripci&#243;n Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionClausula" ControlToValidate="txtDescripcionClausula"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" valign="top">
                <asp:TextBox runat="server" ID="txtDescripcionClausula" ClientIDMode="Static" Height="50px"
                    MaxLength="128" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);"
                    onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionClausula" runat="server" TargetControlID="txtDescripcionClausula"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @"
                    InvalidChars="&#63;" />
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 11) {
                    $('#txtIdContrato').val(retLupa[0]);
                    $('#txtTipoContrato').val(retLupa[11]);
                    $('#hdIdTipoContrato').val(retLupa[7]);
                }
            }
        }

        function GetClausula() {
            if ($("#hdIdTipoContrato").val() != "") {
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaClausula.aspx', setReturnGetClausula, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
            }
            else {
                alert("Debe Seleccionar un Tipo de Contrato");
            }
        }
        function setReturnGetClausula(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 4) {
                    $('#txtIdClausula').val(retLupa[0]);
                    $('#txtNombreClausula').val(retLupa[2]);
                    $('#txtDescripcionClausula').val(retLupa[4]);
                }
            }
        }
    </script>
</asp:Content>
