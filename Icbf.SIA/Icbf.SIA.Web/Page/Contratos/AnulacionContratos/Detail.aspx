<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_AnulacionContratos_Detail" %>
<%@ Register src="../UserControl/ucContratoDetail.ascx" tagname="ucContratoDetail" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript">
    
        function Confirmacion() {

            return confirm("Confirma que desea anular el contrato ?");
        }

    </script>
<asp:HiddenField ID="hfIdContrato" runat="server" />
<table width="100%">
       <tr class="rowA">
           <td style=" width:50%">

               <strong>Fecha de de Anulaci&oacute;n</strong> 

           </td>
           <td style=" width:50%">

           </td>
       </tr>
       <tr class="rowB">
           <td class="auto-style1">

                <asp:TextBox runat="server" ID="TxtFechaRadicacion"  Enabled="true" AutoPostBack="True"></asp:TextBox>
                    <Ajax:CalendarExtender ID="CalendarExtenderFechaAnulacion" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="ImageFechaTerminacion" TargetControlID="TxtFechaRadicacion"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TxtFechaRadicacion">
                    </Ajax:MaskedEditExtender>
                <asp:Image ID="ImageFechaRadicacion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                <asp:RequiredFieldValidator runat="server" ID="rfFechaRadicacion" ControlToValidate="TxtFechaRadicacion"
                 SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="AnularContrato"
                 ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <div style=" text-align:center">
                            <asp:ImageButton ID="btnAnular" 
                                            runat="server" 
                                            OnClientClick="if ( ! Confirmacion()) return false;"
                                            ImageUrl="~/Image/btn/Cancel.png" 
                                            ValidationGroup="AnularContrato" 
                                            ToolTip="Anular" 
                                            OnClick="btnRechazar_Click" />
                        </div>
                    </td>
              </tr>
</table>
<div id="pnDetailContratos">
    <uc1:ucContratoDetail ID="ucContratoDetail1" runat="server" />
</div>
    <%--<table width="90%" align="center">
    </table>--%>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 586px;
        }
    </style>
</asp:Content>

