using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad Contrato
/// </summary>
public partial class Page_AnulacionContratos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    
    string PageName = "Contratos/AnulacionContratos";

    Contrato vContrato = new Contrato();
    
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatos();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            //RemoveSessionParameter("Contrato.ContratosAPP");

            if (GetSessionParameter("Contrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Contrato.Guardado");

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ContratoObtener(vIdContrato);
            hfIdContrato.Value =  vContrato.IdContrato.ToString();
            if (vContrato.FechaSuscripcionContrato.HasValue)
                CalendarExtenderFechaAnulacion.StartDate = vContrato.FechaSuscripcionContrato.Value;
            else if(vContrato.FechaInicioEjecucion.HasValue)
                CalendarExtenderFechaAnulacion.StartDate = vContrato.FechaInicioEjecucion.Value;

            CalendarExtenderFechaAnulacion.EndDate = DateTime.Now;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.MostrarBotonEditar(false);
            toolBar.MostrarBotonNuevo(false);
            toolBar.OcultarBotonEliminar(true);
            toolBar.OcultarBotonBuscar(true);
            toolBar.EstablecerTitulos("Anulación de Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int idContrato = int.Parse(hfIdContrato.Value);
            DateTime fechaAnulación = DateTime.Parse(TxtFechaRadicacion.Text);
            int vResultado = vContratoService.AnularContrato(idContrato, fechaAnulación, GetSessionUser().NombreUsuario);
           
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
