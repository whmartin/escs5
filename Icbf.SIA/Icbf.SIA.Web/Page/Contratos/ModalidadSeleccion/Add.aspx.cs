using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de modalidades de sección
/// </summary>
public partial class Page_ModalidadSeleccion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/ModalidadSeleccion";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    
    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();

            vModalidadSeleccion.Nombre = Convert.ToString(txtNombre.Text);
            vModalidadSeleccion.Sigla = Convert.ToString(txtSigla.Text);
            vModalidadSeleccion.Estado = rblEstado.SelectedValue == "True" ? true : false; 

            if (Request.QueryString["oP"] == "E")
            {
                vModalidadSeleccion.IdModalidad = Convert.ToInt32(hfIdModalidad.Value);
                vModalidadSeleccion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vModalidadSeleccion, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarModalidadSeleccion(vModalidadSeleccion);
            }
            else
            {
                vModalidadSeleccion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vModalidadSeleccion, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarModalidadSeleccion(vModalidadSeleccion);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ModalidadSeleccion.IdModalidad", vModalidadSeleccion.IdModalidad);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("ModalidadSeleccion.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("ModalidadSeleccion.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }


                
            }
            else if (vResultado == 4)
            {
                toolBar.MostrarMensajeError(
                    "Registro duplicado, verifique por favor.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Modalidad Selecci&#243;n", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdModalidad = Convert.ToInt32(GetSessionParameter("ModalidadSeleccion.IdModalidad"));
            int vIdModalidad = 0;

            if (int.TryParse(GetSessionParameter("ModalidadSeleccion.IdModalidad").ToString(), out vIdModalidad))
            {
                RemoveSessionParameter("ModalidadSeleccion.Id");

                ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
                vModalidadSeleccion = vContratoService.ConsultarModalidadSeleccion(vIdModalidad);
                hfIdModalidad.Value = vModalidadSeleccion.IdModalidad.ToString();
                txtNombre.Text = vModalidadSeleccion.Nombre;
                txtSigla.Text = vModalidadSeleccion.Sigla;
                rblEstado.SelectedValue = vModalidadSeleccion.Estado == true ? "True" : "False";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModalidadSeleccion.UsuarioCrea, vModalidadSeleccion.FechaCrea, vModalidadSeleccion.UsuarioModifica, vModalidadSeleccion.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado,"Activo","Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
