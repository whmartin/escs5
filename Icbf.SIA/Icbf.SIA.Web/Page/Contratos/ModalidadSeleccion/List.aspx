<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ModalidadSeleccion_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="style1" style="width: 50%">
                    Nombre modalidad selecci&#243;n
                </td>
                <td style="width: 50%">
                    Sigla modalidad
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:TextBox runat="server" ID="txtNombre" MaxLength="128" Height="50px" TextMode="MultiLine"
                        Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"> </asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; "
                        InvalidChars="&#63;" />
                </td>
                <td valign="top">
                    <asp:TextBox runat="server" ID="txtSigla" MaxLength="5"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSigla"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvModalidadSeleccion" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdModalidad" CellPadding="0"
                        Height="16px" OnPageIndexChanging="gvModalidadSeleccion_PageIndexChanging" OnSelectedIndexChanged="gvModalidadSeleccion_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvModalidadSeleccion_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Modalidad" DataField="Nombre" SortExpression="Nombre" />--%>
                            <asp:TemplateField HeaderText="Nombre modalidad selecci&#243;n" ItemStyle-HorizontalAlign="Center"
                                SortExpression="Nombre">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Sigla Modalidad" DataField="Sigla" SortExpression="Sigla" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 400px;
        }
    </style>
</asp:Content>
