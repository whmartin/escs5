<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ModalidadSeleccion_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdModalidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre modalidad selecci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombre" ControlToValidate="txtNombre"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 50%">
                Sigla
                modalidad * <asp:RequiredFieldValidator runat="server" ID="rfvSiglaModalidad" ControlToValidate="txtSigla"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator></td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombre" MaxLength="128" Height="50px" 
                    TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtSigla" MaxLength="5"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftSigla" runat="server" TargetControlID="txtSigla"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 414px;
        }
    </style>
</asp:Content>

