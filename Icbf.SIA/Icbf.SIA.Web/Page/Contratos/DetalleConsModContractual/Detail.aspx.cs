using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_DetalleConsModContractual_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/DetalleConsModContractual";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIDDetalleConsModContractual.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIDDetalleConsModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
            RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");

            if (GetSessionParameter("DetalleConsModContractual.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("DetalleConsModContractual");


            DetalleConsModContractual vDetalleConsModContractual = new DetalleConsModContractual();
            vDetalleConsModContractual = vContratoService.ConsultarDetalleConsModContractual(vIDDetalleConsModContractual);
            hfIDDetalleConsModContractual.Value = vDetalleConsModContractual.IDDetalleConsModContractual.ToString();
            ddlIDTipoModificacionContractual.SelectedValue = vDetalleConsModContractual.IDTipoModificacionContractual.ToString();
            ddlIDCosModContractual.SelectedValue = vDetalleConsModContractual.IDCosModContractual.ToString();
            ObtenerAuditoria(PageName, hfIDDetalleConsModContractual.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDetalleConsModContractual.UsuarioCrea, vDetalleConsModContractual.FechaCrea, vDetalleConsModContractual.UsuarioModifica, vDetalleConsModContractual.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIDDetalleConsModContractual = Convert.ToInt32(hfIDDetalleConsModContractual.Value);

            DetalleConsModContractual vDetalleConsModContractual = new DetalleConsModContractual();
            vDetalleConsModContractual = vContratoService.ConsultarDetalleConsModContractual(vIDDetalleConsModContractual);
            InformacionAudioria(vDetalleConsModContractual, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarDetalleConsModContractual(vDetalleConsModContractual);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("DetalleConsModContractual.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("DetalleConsModContractual", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIDTipoModificacionContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoModificacionContractual.SelectedValue = "-1";
            ddlIDCosModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDCosModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
