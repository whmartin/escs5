using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_DetalleConsModContractual_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/DetalleConsModContractual";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            DetalleConsModContractual vDetalleConsModContractual = new DetalleConsModContractual();

            vDetalleConsModContractual.IDTipoModificacionContractual = Convert.ToInt32(ddlIDTipoModificacionContractual.SelectedValue);
            vDetalleConsModContractual.IDCosModContractual = Convert.ToInt32(ddlIDCosModContractual.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vDetalleConsModContractual.IDDetalleConsModContractual = Convert.ToInt32(hfIDDetalleConsModContractual.Value);
                vDetalleConsModContractual.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vDetalleConsModContractual, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarDetalleConsModContractual(vDetalleConsModContractual);
            }
            else
            {
                vDetalleConsModContractual.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vDetalleConsModContractual, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarDetalleConsModContractual(vDetalleConsModContractual);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", vDetalleConsModContractual.IDDetalleConsModContractual);
                SetSessionParameter("DetalleConsModContractual.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("DetalleConsModContractual", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIDDetalleConsModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
            RemoveSessionParameter("DetalleConsModContractual.Id");

            DetalleConsModContractual vDetalleConsModContractual = new DetalleConsModContractual();
            vDetalleConsModContractual = vContratoService.ConsultarDetalleConsModContractual(vIDDetalleConsModContractual);
            hfIDDetalleConsModContractual.Value = vDetalleConsModContractual.IDDetalleConsModContractual.ToString();
            ddlIDTipoModificacionContractual.SelectedValue = vDetalleConsModContractual.IDTipoModificacionContractual.ToString();
            ddlIDCosModContractual.SelectedValue = vDetalleConsModContractual.IDCosModContractual.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDetalleConsModContractual.UsuarioCrea, vDetalleConsModContractual.FechaCrea, vDetalleConsModContractual.UsuarioModifica, vDetalleConsModContractual.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIDTipoModificacionContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoModificacionContractual.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIDCosModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDCosModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
