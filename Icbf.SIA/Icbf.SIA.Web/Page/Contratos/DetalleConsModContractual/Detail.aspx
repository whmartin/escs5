<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_DetalleConsModContractual_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Identificador del tipo de modificación *
            </td>
            <td>
                Identificador de la modificación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDTipoModificacionContractual"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIDCosModContractual"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
