<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_DetalleConsModContractual_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Identificador del tipo de modificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDTipoModificacionContractual" ControlToValidate="ddlIDTipoModificacionContractual"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIDTipoModificacionContractual" ControlToValidate="ddlIDTipoModificacionContractual"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Identificador de la modificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDCosModContractual" ControlToValidate="ddlIDCosModContractual"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIDCosModContractual" ControlToValidate="ddlIDCosModContractual"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDTipoModificacionContractual"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIDCosModContractual"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
