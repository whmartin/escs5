using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_DetalleConsModContractual_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/DetalleConsModContractual";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIDTipoModificacionContractual = null;
            int? vIDCosModContractual = null;
            if (ddlIDTipoModificacionContractual.SelectedValue!= "-1")
            {
                vIDTipoModificacionContractual = Convert.ToInt32(ddlIDTipoModificacionContractual.SelectedValue);
            }
            if (ddlIDCosModContractual.SelectedValue!= "-1")
            {
                vIDCosModContractual = Convert.ToInt32(ddlIDCosModContractual.SelectedValue);
            }
            gvDetalleConsModContractual.DataSource = vContratoService.ConsultarDetalleConsModContractuals( vIDTipoModificacionContractual, vIDCosModContractual);
            gvDetalleConsModContractual.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvDetalleConsModContractual.PageSize = PageSize();
            gvDetalleConsModContractual.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("DetalleConsModContractual", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDetalleConsModContractual.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDetalleConsModContractual_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDetalleConsModContractual.SelectedRow);
    }
    protected void gvDetalleConsModContractual_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDetalleConsModContractual.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("DetalleConsModContractual.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("DetalleConsModContractual.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIDTipoModificacionContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoModificacionContractual.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIDCosModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDCosModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
