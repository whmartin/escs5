<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RelacionarSupervisorInterventor_Add" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDSupervisorInterv" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDContratoSupervisa" ControlToValidate="txtNumeroContrato"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoSupervisa" />
                <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" MaxLength="50"
                    onfocus="blur();" class="enable"></asp:TextBox>
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato('hdIDContratoSupervisa,txtNumeroContrato','0,3')" Style="cursor: hand"
                    ToolTip="Buscar" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Supervisor y/o Interventor *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDTipoSupvInterventor" ControlToValidate="ddlIDTipoSupvInterventor"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIDTipoSupvInterventor" ControlToValidate="ddlIDTipoSupvInterventor"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Tipo Supervisor y/o Interventor *
                <asp:RequiredFieldValidator runat="server" ID="rfvOrigenTipoSupervisor" ControlToValidate="rblOrigenTipoSupervisor"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDTipoSupvInterventor">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ClientIDMode="Static" ID="rblOrigenTipoSupervisor"
                    RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblOrigenTipoSupervisor_SelectedIndexChanged">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Inicio Supervisión y/o Interventoría *
            </td>
            <td>
                Fecha de Finalización Supervisión y/o Interventoría *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="FechaInicia" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="FechaFinaliza" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Modificación Supervisor y/o interventor *
            </td>
            <td>
                Tipo de Vinculación Contractual *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="FechaModificaInterventor" runat="server" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblTipoVinculacion" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlInterno" runat="server">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Número de Identificación *
                    <asp:RequiredFieldValidator ID="rfvNumeroIdentificacion" runat="server" ControlToValidate="txtNumeroIdentificacion"
                        Display="Dynamic" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true"
                        ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                </td>
                <td>
                    Nombre y/o razón social *<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                        runat="server" ControlToValidate="txtNombreRazonSocial" Display="Dynamic" ErrorMessage="RequiredFieldValidator"
                        ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerdio</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdIDPersona" ClientIDMode="Static" runat="server" />
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                        FilterType="Numbers" ValidChars="" />
                    <asp:Image Width="20px" Height="20px" ID="imgBuscarPersona" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        Style="cursor: hand" ToolTip="Buscar" onClick="GetPersona()" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreRazonSocial" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Tipo Persona
                </td>
                <td>
                    Tipo Identificación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtTipoPersona" runat="server" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTipoIdentificacion" runat="server" ClientIDMode="Static" onfocus="blur();"
                        class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Apellido
                </td>
                <td>
                    Segundo Apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Nombre
                </td>
                <td>
                    Segundo Nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Dirección
                </td>
                <td>
                    Teléfono
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDireccion" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtTelefono" ClientIDMode="Static" MaxLength="10"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Correo Electrónico
                </td>
                <td>
                    Dependencia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ClientIDMode="Static" ID="txtCorreoElectronico" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ClientIDMode="Static" ID="txtDependencia" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Cargo
                </td>
                <td>
                    Regional
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtCargo" ClientIDMode="Static" MaxLength="50" texto
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRegional" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado *
                    <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlExterno" runat="server" Visible="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Número Contrato Interventoría *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:HiddenField ID="hdIDContratoInterventoria" ClientIDMode="Static" runat="server" />
                    <asp:TextBox runat="server" ID="txtNumeroContratoIntermediario" ClientIDMode="Static"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteNumeroContratoIntermediario" runat="server"
                        TargetControlID="txtNumeroContratoIntermediario" FilterType="Numbers" ValidChars="" />
                    <asp:Image ID="ImgBContratoIntervent" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        Style="cursor: hand" ToolTip="Buscar" onClick="GetContrato('hdIDContratoInterventoria,txtNumeroContratoIntermediario','0,3')" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Número Identificación Director de la Interventoria *
                </td>
                <td>
                    Nombre y/o razón social Director de la Interventoria *<asp:RequiredFieldValidator
                        ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtNombreRazonSocialInterventoria"
                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:HiddenField ID="hdIDDirectorInterventoria" runat="server" ClientIDMode="Static" />
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionDirector" ClientIDMode="Static"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteNumeroIdentificacionDirector" runat="server"
                        TargetControlID="txtNumeroIdentificacionDirector" FilterType="Numbers" ValidChars="" />
                    <asp:Image ID="ImgBTerceroInteventoria" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        onClick="GetTercero('hdIDDirectorInterventoria,txtNumeroIdentificacionDirector,txtNombreRazonSocialInterventoria','0,3,4')"
                        Style="cursor: hand" ToolTip="Buscar" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreRazonSocialInterventoria" ClientIDMode="Static"
                        MaxLength="50" onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Apellido Director de la Interventoria
                </td>
                <td>
                    Segundo Apellido Director de la Interventoria
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellidoDirectorInter" ClientIDMode="Static"
                        MaxLength="50" onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellidoDirectorInter" ClientIDMode="Static"
                        MaxLength="50" onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Nombre Director de la Interventoria
                </td>
                <td>
                    Segundo Nombre Director de la Interventoria
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombreDirectorInter" ClientIDMode="Static"
                        MaxLength="50" onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombreDirectorInter" ClientIDMode="Static"
                        MaxLength="50" onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Telefono Director de la Interventoria
                </td>
                <td>
                    Celular Director de la Interventoria *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTelefonoDirectorInter" ClientIDMode="Static" MaxLength="10"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCelularDirectorInter" ClientIDMode="Static" MaxLength="10"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function GetPersona() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaTercero.aspx', setReturnGetPersona, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetPersona(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 14) {
                    $("#hdIDPersona").val(retLupa[0]);
                    $('#txtTipoPersona').val(retLupa[1]);
                    $('#txtTipoIdentificacion').val(retLupa[2]);
                    $('#txtNumeroIdentificacion').val(retLupa[3]);
                    $('#txtNombreRazonSocial').val(retLupa[4]);
                    $('#txtPrimerApellido').val(retLupa[9]);
                    $('#txtSegundoApellido').val(retLupa[10]);
                    $('#txtPrimerNombre').val(retLupa[7]);
                    $('#txtSegundoNombre').val(retLupa[8]);
                    $('#txtCorreoElectronico').val(retLupa[14]);
                }
            }
        }
    </script>
</asp:Content>
