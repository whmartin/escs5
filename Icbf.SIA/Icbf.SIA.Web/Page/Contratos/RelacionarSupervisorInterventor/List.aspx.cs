using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de supervisores e interventores
/// </summary>
public partial class Page_ConsultarSupervisorInterventor_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ConsultarSupervisorInterventor";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vTipoSupervisorInterventor = null;
            String vNumeroContrato = null;
            String vNumeroIdentificacion = null;
            String vNombreRazonSocialSupervisorInterventor = null;
            String vNumeroIdentificacionDirectorInterventoria = null;
            String vNombreRazonSocialDirectorInterventoria = null;
            if (hfTipoSupervisorInterventor.SelectedValue != "-1")
            {
                vTipoSupervisorInterventor = Convert.ToInt32(hfTipoSupervisorInterventor.SelectedValue);
            }
            if (txtNumeroContrato.Text!= "")
            {
                vNumeroContrato = txtNumeroContrato.Text;
            }
            if (txtNumeroIdentificacion.Text!= "")
            {
                vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            }
            if (txtNombreRazonSocialSupervisorInterventor.Text!= "")
            {
                vNombreRazonSocialSupervisorInterventor = Convert.ToString(txtNombreRazonSocialSupervisorInterventor.Text);
            }
            if (txtNumeroIdentificacionDirectorInterventoria.Text!= "")
            {
                vNumeroIdentificacionDirectorInterventoria = txtNumeroIdentificacionDirectorInterventoria.Text;
            }
            if (txtNombreRazonSocialDirectorInterventoria.Text!= "")
            {
                vNombreRazonSocialDirectorInterventoria = Convert.ToString(txtNombreRazonSocialDirectorInterventoria.Text);
            }
            gvConsultarSupervisorInterventor.DataSource = vContratoService.ConsultarConsultarSupervisorInterventors( vTipoSupervisorInterventor, vNumeroContrato, vNumeroIdentificacion, vNombreRazonSocialSupervisorInterventor, vNumeroIdentificacionDirectorInterventoria, vNombreRazonSocialDirectorInterventoria);
            gvConsultarSupervisorInterventor.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvConsultarSupervisorInterventor.PageSize = PageSize();
            gvConsultarSupervisorInterventor.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Nombre y/o Raz&#243;n Social", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvConsultarSupervisorInterventor.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("RelacionarSupervisorInterventor.IDSupervisorInterv", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvConsultarSupervisorInterventor_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConsultarSupervisorInterventor.SelectedRow);
    }
    protected void gvConsultarSupervisorInterventor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConsultarSupervisorInterventor.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales() 
    {
        try
        {
            if (GetSessionParameter("ConsultarSupervisorInterventor.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ConsultarSupervisorInterventor.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService clsContratoService = new ContratoService();
            ManejoControlesContratos.LlenarComboLista(hfTipoSupervisorInterventor, clsContratoService.ConsultarTipoSupvInterventors(null, true).Where(x => x.Estado == true), "IdTipoSupvInterventor", "Nombre");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
