using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;

/// <summary>
/// Página que despliega el detalle del registro del supervisor o interventor
/// </summary>
public partial class Page_RelacionarSupervisorInterventor_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RelacionarSupervisorInterventor";
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("RelacionarSupervisorInterventor.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("RelacionarSupervisorInterventor.IDSupervisorInterv", hfIDSupervisorInterv.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIDSupervisorInterv = Convert.ToInt32(GetSessionParameter("RelacionarSupervisorInterventor.IDSupervisorInterv"));
            RemoveSessionParameter("RelacionarSupervisorInterventor.IDSupervisorInterv");

            if (GetSessionParameter("RelacionarSupervisorInterventor.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("RelacionarSupervisorInterventor");
            RemoveSessionParameter("RelacionarSupervisorInterventor.Guardado");
            


            RelacionarSupervisorInterventor vRelacionarSupervisorInterventor = new RelacionarSupervisorInterventor();
            vRelacionarSupervisorInterventor = vContratoService.ConsultarRelacionarSupervisorInterventor(vIDSupervisorInterv);
            hfIDSupervisorInterv.Value = vRelacionarSupervisorInterventor.IDSupervisorInterv.ToString();
            hdIDContratoSupervisa.Value = vRelacionarSupervisorInterventor.IDContratoSupervisa.ToString();
            // consultamos los datos del contrato supervisado
            var contrato = vContratoService.ConsultarContrato(vRelacionarSupervisorInterventor.IDContratoSupervisa);
            this.txtNumeroContrato.Text = contrato.NumeroContrato.ToString();
            rblOrigenTipoSupervisor.SelectedValue = vRelacionarSupervisorInterventor.OrigenTipoSupervisor.ToString();
            ddlIDTipoSupvInterventor.SelectedValue = vRelacionarSupervisorInterventor.IDTipoSupvInterventor.ToString();
            if (vRelacionarSupervisorInterventor.IDTerceroExterno != null)
            {
                hdIDPersona.Value = vRelacionarSupervisorInterventor.IDTerceroExterno.ToString();
                // consultamos los datos del tercero
                var supervisor = vProveedorService.ConsultarTercero(vRelacionarSupervisorInterventor.IDTerceroExterno.Value);
                this.txtNumeroIdentificacion.Text = supervisor.NumeroIdentificacion;
                this.txtNombreRazonSocial.Text = supervisor.Nombre_Razonsocial;
                this.txtTipoPersona.Text = supervisor.NombreTipoPersona;
                this.txtTipoIdentificacion.Text = supervisor.NombreListaTipoDocumento;
            }
            else if (vRelacionarSupervisorInterventor.IDFuncionarioInterno != null)
            {
                hdIDPersona.Value = vRelacionarSupervisorInterventor.IDFuncionarioInterno.ToString();
                // TODO: Cuando es funcionario
            }
            if (vRelacionarSupervisorInterventor.IDContratoInterventoria != null)
            {
                hdIDContratoInterventoria.Value = vRelacionarSupervisorInterventor.IDContratoInterventoria.ToString();
                var contratoInterventoria = vContratoService.ConsultarContrato(vRelacionarSupervisorInterventor.IDContratoInterventoria.Value);
                this.txtNumeroContratoIntermediario.Text = contratoInterventoria.NumeroContrato.ToString();
            }
            if (vRelacionarSupervisorInterventor.IDDirectorInterventoria != null)
            {
                hdIDDirectorInterventoria.Value = vRelacionarSupervisorInterventor.IDDirectorInterventoria.ToString();
                // consultamos los datos del director de interventoria
                var directorInterventor = vProveedorService.ConsultarTercero(vRelacionarSupervisorInterventor.IDDirectorInterventoria.Value);
                txtNumeroIdentificacionDirector.Text = directorInterventor.NumeroIdentificacion;
                txtNombreRazonSocialInterventoria.Text = directorInterventor.Nombre_Razonsocial;
            }
            if (vRelacionarSupervisorInterventor.OrigenTipoSupervisor)
            {
                this.pnlExterno.Visible = false;
            }
            else
            {
                this.pnlExterno.Visible = true;
            }
            rblTipoVinculacion.SelectedValue = vRelacionarSupervisorInterventor.TipoVinculacion.ToString();
            FechaInicia.Date = vRelacionarSupervisorInterventor.FechaInicia;
            FechaFinaliza.Date = vRelacionarSupervisorInterventor.FechaFinaliza;
            rblEstado.SelectedValue = vRelacionarSupervisorInterventor.Estado.ToString();
            FechaModificaInterventor.Date = vRelacionarSupervisorInterventor.FechaModificaInterventor;
            ObtenerAuditoria(PageName, hfIDSupervisorInterv.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRelacionarSupervisorInterventor.UsuarioCrea, vRelacionarSupervisorInterventor.FechaCrea, vRelacionarSupervisorInterventor.UsuarioModifica, vRelacionarSupervisorInterventor.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIDSupervisorInterv = Convert.ToInt32(hfIDSupervisorInterv.Value);

            RelacionarSupervisorInterventor vRelacionarSupervisorInterventor = new RelacionarSupervisorInterventor();
            vRelacionarSupervisorInterventor = vContratoService.ConsultarRelacionarSupervisorInterventor(vIDSupervisorInterv);
            InformacionAudioria(vRelacionarSupervisorInterventor, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarRelacionarSupervisorInterventor(vRelacionarSupervisorInterventor);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("RelacionarSupervisorInterventor.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Relacionar Supervisor Interventor", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblOrigenTipoSupervisor, "Interno", "Externo");
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService clsContratoService = new ContratoService();
            ManejoControlesContratos.LlenarComboLista(ddlIDTipoSupvInterventor, clsContratoService.ConsultarTipoSupvInterventors(null, true).Where(x => x.Estado == true), "IdTipoSupvInterventor", "Nombre");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblTipoVinculacion, "Funcionario", "Contratista");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
