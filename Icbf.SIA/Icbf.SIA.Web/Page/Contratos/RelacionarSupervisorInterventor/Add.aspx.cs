using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;

/// <summary>
/// Página de registro y edición de asociación de supervisores
/// </summary>
public partial class Page_RelacionarSupervisorInterventor_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    ManejoControles ManejoControles = new ManejoControles();
    OferenteService vOferenteService = new OferenteService();
    string PageName = "Contratos/RelacionarSupervisorInterventor";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                {
                    this.imgBcodigoUsuario.Visible = false;
                    CargarRegistro();
                }
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            RelacionarSupervisorInterventor vRelacionarSupervisorInterventor = new RelacionarSupervisorInterventor();

            vRelacionarSupervisorInterventor.IDContratoSupervisa = Convert.ToInt32(hdIDContratoSupervisa.Value);
            vRelacionarSupervisorInterventor.OrigenTipoSupervisor = Convert.ToBoolean(rblOrigenTipoSupervisor.SelectedValue);
            vRelacionarSupervisorInterventor.IDTipoSupvInterventor = Convert.ToInt32(ddlIDTipoSupvInterventor.SelectedValue);
            if (rblOrigenTipoSupervisor.SelectedValue == "True")
                vRelacionarSupervisorInterventor.IDFuncionarioInterno = Convert.ToInt32(hdIDPersona.Value);
            else
                vRelacionarSupervisorInterventor.IDTerceroExterno = Convert.ToInt32(hdIDPersona.Value);
            if(hdIDContratoInterventoria.Value != string.Empty)
                vRelacionarSupervisorInterventor.IDContratoInterventoria = Convert.ToInt32(hdIDContratoInterventoria.Value);
            if (hdIDDirectorInterventoria.Value != string.Empty)
                vRelacionarSupervisorInterventor.IDDirectorInterventoria = Convert.ToInt32(hdIDDirectorInterventoria.Value);
            vRelacionarSupervisorInterventor.TipoVinculacion = Convert.ToBoolean(rblTipoVinculacion.SelectedValue);
            vRelacionarSupervisorInterventor.FechaInicia = FechaInicia.Date;
            vRelacionarSupervisorInterventor.FechaFinaliza = FechaFinaliza.Date;
            vRelacionarSupervisorInterventor.Estado = Convert.ToBoolean(rblEstado.SelectedValue);
            vRelacionarSupervisorInterventor.FechaModificaInterventor = FechaModificaInterventor.Date;
            
            if (Request.QueryString["oP"] == "E")
            {
            vRelacionarSupervisorInterventor.IDSupervisorInterv = Convert.ToInt32(hfIDSupervisorInterv.Value);
                vRelacionarSupervisorInterventor.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRelacionarSupervisorInterventor, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarRelacionarSupervisorInterventor(vRelacionarSupervisorInterventor);
            }
            else
            {
                vRelacionarSupervisorInterventor.UsuarioCrea = GetSessionUser().NombreUsuario;
                vRelacionarSupervisorInterventor.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRelacionarSupervisorInterventor, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarRelacionarSupervisorInterventor(vRelacionarSupervisorInterventor);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("RelacionarSupervisorInterventor.IDSupervisorInterv", vRelacionarSupervisorInterventor.IDSupervisorInterv);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("RelacionarSupervisorInterventor.Modificado", "1");
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                }
                else
                {
                    SetSessionParameter("RelacionarSupervisorInterventor.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }                
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Relacionar Supervisor/Interventor", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIDSupervisorInterv = Convert.ToInt32(GetSessionParameter("RelacionarSupervisorInterventor.IDSupervisorInterv"));
            RemoveSessionParameter("RelacionarSupervisorInterventor.Id");
            this.imgBcodigoUsuario.Visible = false;

            RelacionarSupervisorInterventor vRelacionarSupervisorInterventor = new RelacionarSupervisorInterventor();
            vRelacionarSupervisorInterventor = vContratoService.ConsultarRelacionarSupervisorInterventor(vIDSupervisorInterv);
            hfIDSupervisorInterv.Value = vRelacionarSupervisorInterventor.IDSupervisorInterv.ToString();
            hdIDContratoSupervisa.Value = vRelacionarSupervisorInterventor.IDContratoSupervisa.ToString();
            // consultamos los datos del contrato supervisado
            var contrato = vContratoService.ConsultarContrato(vRelacionarSupervisorInterventor.IDContratoSupervisa);
            this.txtNumeroContrato.Text = contrato.NumeroContrato.ToString();
            rblOrigenTipoSupervisor.SelectedValue = vRelacionarSupervisorInterventor.OrigenTipoSupervisor.ToString();
            ddlIDTipoSupvInterventor.SelectedValue = vRelacionarSupervisorInterventor.IDTipoSupvInterventor.ToString();
            if (vRelacionarSupervisorInterventor.IDTerceroExterno != null)
            {
                hdIDPersona.Value = vRelacionarSupervisorInterventor.IDTerceroExterno.ToString();
                // consultamos los datos del tercero
                var supervisor = vOferenteService.ConsultarTercero(vRelacionarSupervisorInterventor.IDTerceroExterno.Value);
                this.txtNumeroIdentificacion.Text = supervisor.NumeroIdentificacion;
                this.txtNombreRazonSocial.Text = supervisor.Nombre_Razonsocial;
                this.txtTipoPersona.Text = supervisor.NombreTipoPersona;
                this.txtTipoIdentificacion.Text = supervisor.NombreListaTipoDocumento;
            }
            else if (vRelacionarSupervisorInterventor.IDFuncionarioInterno != null)
            {
                hdIDPersona.Value = vRelacionarSupervisorInterventor.IDFuncionarioInterno.ToString();
                // TODO: Cuando es funcionario
            }
            if (vRelacionarSupervisorInterventor.IDContratoInterventoria != null)
            {
                hdIDContratoInterventoria.Value = vRelacionarSupervisorInterventor.IDContratoInterventoria.ToString();
                var contratoInterventoria = vContratoService.ConsultarContrato(vRelacionarSupervisorInterventor.IDContratoInterventoria.Value);
                this.txtNumeroContratoIntermediario.Text = contratoInterventoria.NumeroContrato.ToString();
            }
            if (vRelacionarSupervisorInterventor.IDDirectorInterventoria != null)
            {
                hdIDDirectorInterventoria.Value = vRelacionarSupervisorInterventor.IDDirectorInterventoria.ToString();
                // consultamos los datos del director de interventoria
                var directorInterventor = vOferenteService.ConsultarTercero(vRelacionarSupervisorInterventor.IDDirectorInterventoria.Value);
                txtNumeroIdentificacionDirector.Text = directorInterventor.NumeroIdentificacion;
                txtNombreRazonSocialInterventoria.Text = directorInterventor.Nombre_Razonsocial;
            }
            if (vRelacionarSupervisorInterventor.OrigenTipoSupervisor)
            {
                pnlExterno.Visible = false;
            }
            else
            {
                pnlExterno.Visible = true;
            }
            rblTipoVinculacion.SelectedValue = vRelacionarSupervisorInterventor.TipoVinculacion.ToString();
            FechaInicia.Date = vRelacionarSupervisorInterventor.FechaInicia;
            FechaFinaliza.Date = vRelacionarSupervisorInterventor.FechaFinaliza;
            rblEstado.SelectedValue = vRelacionarSupervisorInterventor.Estado.ToString();
            FechaModificaInterventor.Date = vRelacionarSupervisorInterventor.FechaModificaInterventor;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRelacionarSupervisorInterventor.UsuarioCrea, vRelacionarSupervisorInterventor.FechaCrea, vRelacionarSupervisorInterventor.UsuarioModifica, vRelacionarSupervisorInterventor.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblOrigenTipoSupervisor,"Interno", "Externo");
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService clsContratoService = new ContratoService();
            ManejoControlesContratos.LlenarComboLista(ddlIDTipoSupvInterventor, clsContratoService.ConsultarTipoSupvInterventors(null, true).Where(x => x.Estado == true), "IdTipoSupvInterventor", "Nombre");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblTipoVinculacion, "Funcionario", "Contratista");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            FechaInicia.HabilitarObligatoriedad(true);
            FechaFinaliza.HabilitarObligatoriedad(true);
            FechaModificaInterventor.HabilitarObligatoriedad(true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void rblOrigenTipoSupervisor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.rblOrigenTipoSupervisor.SelectedValue == "True")
        {
            rblTipoVinculacion.Enabled = false;
            this.pnlExterno.Enabled = false;
            this.pnlExterno.Visible = false;
            this.txtNumeroIdentificacion.Enabled = true;
        }
        else
        {
            this.pnlExterno.Enabled = true;
            this.pnlExterno.Visible = true;
            this.txtNumeroIdentificacion.Enabled = false;
        }
    }
}
