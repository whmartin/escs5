<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_RelacionarSupervisorInterventor_Detail" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIDSupervisorInterv" runat="server" />
    <%--<table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Contrato Supervisa *
            </td>
            <td>
                Supervisor y/o interventor *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIDContratoSupervisa"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblOrigenTipoSupervisor" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Supervisor y/o interventor *
            </td>
            <td>
                Id Tercero Externo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDTipoSupvInterventor"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIDTerceroExterno"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Id Funcionario Interno
            </td>
            <td>
                Id Contrato Interventoria
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIDFuncionarioInterno"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIDContratoInterventoria"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo de Vinculación Contractual *
            </td>
            <td>
                Fecha de Inicio Supervisión  y/o Interventoría  *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblTipoVinculacion" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicia"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Finalización Supervisión y/o Interventoría *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlFechaFinaliza"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Modificación Supervisor y/o interventor *
            </td>
            <td>
                Fecha Creación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaModificaInterventor"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaCrea"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Creación *
            </td>
            <td>
                Fecha Modificación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUsuarioCrea"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaModifica"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Usuario Modificación *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtUsuarioModifica"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>--%>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de Contrato
            </td>
            
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoSupervisa" />
                <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            
        </tr>
        <tr class="rowB">
            <td>
                Tipo Supervisor y/o interventor
            </td>
            <td>
                Clase Supervisor y/o interventor
            </td>
        </tr>
        <tr class="rowA">
             <td>
                <asp:DropDownList runat="server" ID="ddlIDTipoSupvInterventor" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ClientIDMode="Static" ID="rblOrigenTipoSupervisor" 
                    RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
         <tr class="rowB">
            <td>
                Fecha de Inicio Supervisión  y/o Interventoría
               
            </td>
            <td>
                Fecha de Finalización Supervisión y/o Interventoría
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="FechaInicia" runat="server" Enabled="false" />
            </td>
            <td>
                <uc1:fecha ID="FechaFinaliza" runat="server" Enabled="false" />
            </td>
        </tr>
        <tr class="rowB">
             <td>
                Fecha de Modificación Supervisor y/o interventor
                
            </td>
             <td>
                Tipo de Vinculación Contractual
                
            </td>
            
        </tr>
        <tr class="rowA">
           <td>
                <uc1:fecha ID="FechaModificaInterventor" runat="server" Enabled="false" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblTipoVinculacion" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
           
        </tr>
        
    </table>
    <asp:Panel ID="pnlInterno" runat="server">
      <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de Identificación
            </td>
            <td>
                Nombre y/o razón social</td>
        </tr>
        <tr >
            <td>
                <asp:HiddenField ID="hdIDPersona" ClientIDMode="Static" runat="server" />
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" ClientIDMode="Static" Enabled="false"></asp:TextBox>               
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocial" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Persona
            </td>
            <td>
                Tipo Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtTipoPersona" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtTipoIdentificacion" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Primer Apellido
           </td>
           <td>
                Segundo Apellido
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Primer Nombre
           </td>
           <td>
                Segundo Nombre
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Dirección
           </td>
           <td>
                Telefono
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtDireccion" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTelefono" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Correo Electrónico
           </td>
           <td>
                Dependencia
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtCorreoElectronico" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtDependencia" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Cargo
           </td>
           <td>
                Regional
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtCargo" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlExterno" runat="server" Visible="false">
        <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número Contrato Interventoría</td>
            
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField ID="hdIDContratoInterventoria" ClientIDMode="Static" runat="server" />
                <asp:TextBox runat="server" ID="txtNumeroContratoIntermediario" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            
        </tr>
        <tr class="rowB">
            <td>
                Número Identificación Director de la Interventoria
            </td>
            <td>
                Nombre y/o razón social Director de la Interventoria
            </td>
        </tr>
        <tr class="rowA">
            <td> 
                <asp:HiddenField ID="hdIDDirectorInterventoria" runat="server" />
                <asp:TextBox runat="server" ID="txtNumeroIdentificacionDirector" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocialInterventoria" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Primer Apellido Director de la Interventoria
           </td>
           <td>
                Segundo Apellido Director de la Interventoria
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerApellidoDirectorInter" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellidoDirectorInter" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Primer Nombre Director de la Interventoria
           </td>
           <td>
                Segundo Nombre Director de la Interventoria
           </td>
            
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtPrimerNombreDirectorInter" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombreDirectorInter" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
           <td>
                Telefono Director de la Interventoria
           </td>
           <td>
                Celular Director de la Interventoria
           </td>
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtTelefonoDirectorInter" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCelularDirectorInter" ClientIDMode="Static" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        </table>
    </asp:Panel>
</asp:Content>
