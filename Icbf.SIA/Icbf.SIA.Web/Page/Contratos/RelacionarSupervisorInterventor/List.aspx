<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConsultarSupervisorInterventor_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta" >
    <table width="100%" align="center">
        <tr class="rowB">
            <td width="20%">
                Tipo Supervisor y/o Interventor
            </td>
            <td width="20%">
                Número de Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td >
                <asp:DropDownList runat="server" ID="hfTipoSupervisorInterventor"  ></asp:DropDownList>
            </td>
            <td >
                 <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoSupervisa" />

                <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" 
                    MaxLength="50" onfocus=blur(); class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />

                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato('hdIDContratoSupervisa,txtNumeroContrato','0,3')" Style="cursor: hand" ToolTip="Buscar" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Identificación Supervisor/Interventor
            </td>
            <td>
                Nombre y/o Razón Social</td>
        </tr>
        <tr class="rowA">
            <td>
            <asp:HiddenField ID="hdIdTercero" ClientIDMode="Static" runat="server" />
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="50"  ClientIDMode="Static" onfocus=blur(); class="enable"></asp:TextBox>
                <asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetTercero('hdIdTercero,txtNumeroIdentificacion','0,3')" 
                     Style="cursor: hand" ToolTip="Buscar" />
            

                 <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocialSupervisorInterventor" 
                    MaxLength="50" ></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftNombreRazonSocialSupervisorInterventor" runat="server" 
                 TargetControlID="txtNombreRazonSocialSupervisorInterventor" FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
</td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Identificación Director de la Interventoría</td>
            <td>
                Nombre Director de la Interventoría
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField ID="hdIdInterventor" ClientIDMode="Static" runat="server" />
                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtNumeroIdentificacionDirectorInterventoria" 
                    MaxLength="50" onfocus=blur(); class="enable"></asp:TextBox><asp:Image ID="Image2" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetTercero('hdIdInterventor,txtNumeroIdentificacionDirectorInterventoria','0,3')" 
                     Style="cursor: hand" ToolTip="Buscar" />

                <Ajax:FilteredTextBoxExtender ID="fteNumeroIdentificacionDirectorInterventoria" runat="server" TargetControlID="txtNumeroIdentificacionDirectorInterventoria" FilterType="Numbers" ValidChars=""></Ajax:FilteredTextBoxExtender>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocialDirectorInterventoria" 
                    MaxLength="50"  ></asp:TextBox>
<Ajax:FilteredTextBoxExtender ID="fteNombreRazonSocialDirectorInterventoria" runat="server" 
                 TargetControlID="txtNombreRazonSocialDirectorInterventoria" FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
    </table>
    </asp:Panel>

<div id="Layer1" style="width:1050px; height:245px; overflow:scroll;">
    
    <asp:Panel runat="server" ID="pnlLista" ScrollBars="Horizontal">
        <asp:GridView runat="server" ID="gvConsultarSupervisorInterventor" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None"  DataKeyNames="IDSupervisorInterv" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvConsultarSupervisorInterventor_PageIndexChanging" 
                        OnSelectedIndexChanged="gvConsultarSupervisorInterventor_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Supervisor y/o Interventor" DataField="TipoSupervisorInterventor" />
                            <asp:BoundField HeaderText="Núm. contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="Fecha Suscripción" DataField="FECHASUSCRIPCION" />
                            <asp:BoundField HeaderText="Fecha Terminación" DataField="FECHAFINALTERMINACION" />
                            <asp:BoundField HeaderText="Núm. Contrato interventoría" DataField="NUMEROCONTRATOINTER" />
                            <asp:BoundField HeaderText="Fecha Inicio" DataField="FECHAINICIA" />
                            <asp:BoundField HeaderText="Fecha Finalización" DataField="FECHAFINALIZA" />
                            <asp:BoundField HeaderText="Fecha Modificación" DataField="FECHAMODIFICAINTERVENTOR" />
                            <asp:BoundField HeaderText="Identificación" DataField="TIPOIDENTIFICACIONSUPINV" />
                            <asp:BoundField HeaderText="Núm. Identificación" DataField="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Nombre y/o Razón Social" DataField="NombreRazonSocialSupervisorInterventor" />
                            <asp:BoundField HeaderText="Primer Apellido" DataField="PRIMERAPELLIDO" />
                            <asp:BoundField HeaderText="Segundo Apellido" DataField="SEGUNDOAPELLIDO" />
                            <asp:BoundField HeaderText="Primer Nombre" DataField="PRIMERNOMBRE" />
                            <asp:BoundField HeaderText="Segundo Nombre" DataField="SEGUNDONOMBRE" />
                            <asp:BoundField HeaderText="Identificación Director Interventoría" DataField="NumeroIdentificacionDirectorInterventoria" />
                            <asp:BoundField HeaderText="Nombre o Razón social" DataField="NombreRazonSocialDirectorInterventoria" />
                            <asp:BoundField HeaderText="Primer Apellido" DataField="PRIMERAPELLIDODIRECT" />
                            <asp:BoundField HeaderText="Segundo Apellido" DataField="SEGUNDOAPELLIDODIRECT" />
                            <asp:BoundField HeaderText="Primer Nombre" DataField="PRIMERNOMBREDIRECT" />
                            <asp:BoundField HeaderText="Segundo Nombre" DataField="SEGUNDONOMBREDIRECT" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
    </asp:Panel>
</div>
</asp:Content>
