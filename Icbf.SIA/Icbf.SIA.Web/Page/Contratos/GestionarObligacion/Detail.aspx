<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_GestionarObligacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdGestionObligacion" runat="server" />
    <table width="90%" align="center">



        
    <tr class="rowB">
            <td>
                C&#243;digo Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtIdContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoContrato" ControlToValidate="txtTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Id Gestión Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdClausula" ControlToValidate="txtIdClausula"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Nombre Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreClausula" ControlToValidate="txtNombreClausula"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td valign="top">
                <asp:TextBox runat="server" ID="txtIdContrato" ClientIDMode="Static"></asp:TextBox>
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtTipoContrato" ClientIDMode="Static"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTipoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <%--<asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIdTipoContrato" />
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtIdClausula" ClientIDMode="Static"></asp:TextBox>
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtNombreClausula"  ClientIDMode="Static"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreClausula" runat="server" TargetControlID="txtNombreClausula"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <%--<asp:Image ID="imgBClausula" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetClausula()" Style="cursor: hand" ToolTip="Buscar" />--%>
            </td>
        </tr>




        <tr class="rowB">
            <td>
                Id Gestión Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdGestionObligacion" ControlToValidate="txtIdGestionObligacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoObligacion" ControlToValidate="txtIdTipoObligacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Nombre Obligación *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtNombreObligacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdGestionObligacion" ClientIDMode="Static"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdGestionObligacion" runat="server" TargetControlID="txtIdGestionObligacion"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtIdTipoObligacion" ClientIDMode="Static"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="ftIdTipoObligacion" runat="server" TargetControlID="txtIdTipoObligacion"
                    FilterType="Numbers" ValidChars="" />--%>
            </td>
            <td valign="top">
                <asp:TextBox runat="server" ID="txtNombreObligacion" ClientIDMode="Static"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreObligacion" runat="server" TargetControlID="txtNombreObligacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    
                <%--<asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetObligacion()" Style="cursor: hand" ToolTip="Buscar" />--%>
            </td>
        </tr>





        <tr class="rowB">
            <td>
                Orden *
                <asp:RequiredFieldValidator runat="server" ID="rfvOrden" ControlToValidate="txtOrden"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">
                Descripci&#243;n Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionObligacion" ControlToValidate="txtDescripcionObligacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td valign="top">
                <asp:TextBox runat="server" ID="txtOrden" MaxLength="128"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionObligacion" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionObligacion" runat="server" TargetControlID="txtDescripcionObligacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>

    </table>
</asp:Content>
