using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de gesti�n de obligaciones
/// </summary>
public partial class Page_GestionarObligacion_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GestionarObligacion";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdGestionObligacion = null;
            int? vIdGestionClausula = null;
            String vNombreObligacion = null;
            int? vIdTipoObligacion = null;
            String vOrden = null;
            String vDescripcionObligacion = null;
            
            if (txtIdGestionClausula.Text!= "")
            {
                vIdGestionClausula = Convert.ToInt32(txtIdGestionClausula.Text);
            }
            if (txtNombreObligacion.Text!= "")
            {
                vNombreObligacion = Convert.ToString(txtNombreObligacion.Text);
            }
            if (txtIdTipoObligacion.Text!= "")
            {
                vIdTipoObligacion = Convert.ToInt32(hfIdGestionObligacion.Text);
            }
            if (txtOrden.Text!= "")
            {
                vOrden = Convert.ToString(txtOrden.Text);
            }
            if (txtDescripcionObligacion.Text!= "")
            {
                vDescripcionObligacion = Convert.ToString(txtDescripcionObligacion.Text);
            }
            gvGestionarObligacion.DataSource = vContratoService.ConsultarGestionarObligacions( vIdGestionObligacion, vIdGestionClausula, vNombreObligacion, vIdTipoObligacion, vOrden, vDescripcionObligacion);
            gvGestionarObligacion.DataBind();
            CargarDatosIniciales();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvGestionarObligacion.PageSize = PageSize();
            gvGestionarObligacion.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gestionar Obligacion", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvGestionarObligacion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("GestionarObligacion.IdGestionObligacion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvGestionarObligacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvGestionarObligacion.SelectedRow);
    }
    protected void gvGestionarObligacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGestionarObligacion.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("GestionarObligacion.Eliminado").ToString() == "1")
            toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("GestionarObligacion.Eliminado");
            txtIdContrato.Text = "";
            txtTipoContrato.Text = "";
            txtIdGestionClausula.Text = "";
            txtNombreClausula.Text = "";
            hfIdGestionObligacion.Text = "";
            txtNombreObligacion.Text = "";
            txtIdTipoObligacion.Text = "";
            txtOrden.Text = "";
            txtDescripcionObligacion.Text = "";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
