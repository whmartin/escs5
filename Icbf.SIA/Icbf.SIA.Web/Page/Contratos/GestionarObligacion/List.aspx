<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_GestionarObligacion_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Código Contrato
                </td>
                <td>
                    Tipo Contrato
                </td>
                <td>
                    Id Gestión Cl&#225;usula
                </td>
                <td>
                    Nombre Cl&#225;usula
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtIdContrato" ClientIDMode="Static" onfocus="blur();"
                        class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtTipoContrato" ClientIDMode="Static" onfocus="blur();"
                        class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTipoContrato"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                    <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIdTipoContrato" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIdGestionClausula" ClientIDMode="Static" onfocus="blur();"
                        class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreClausula" ClientIDMode="Static" onfocus="blur();"
                        class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreClausula" runat="server" TargetControlID="txtNombreClausula"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                    <asp:Image ID="imgBClausula" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetClausula()" Style="cursor: hand" ToolTip="Buscar" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Id Gestión Obligación
                </td>
                <td>
                    Tipo Obligaci&#243;n
                </td>
                <td>
                    Nombre Obligaci&#243;n
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="hfIdGestionObligacion" MaxLength="20" ClientIDMode="Static"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIdTipoObligacion" ClientIDMode="Static" MaxLength="50"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreObligacion" ClientIDMode="Static" Width="300px"
                        onfocus="blur();" class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreObligacion" runat="server" TargetControlID="txtNombreObligacion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                    <asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetObligacion()" Style="cursor: hand" ToolTip="Buscar" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Orden
                </td>
                <td>
                    Descripción Obligación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtOrden" runat="server" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftOrden" runat="server" FilterType="Numbers" TargetControlID="txtOrden"
                        ValidChars="" />
                </td>
                <td>
                    <asp:TextBox ID="txtDescripcionObligacion" runat="server" Height="50px" MaxLength="128"
                        onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);" TextMode="MultiLine"
                        Width="320px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDescripcionObligacion" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                        TargetControlID="txtDescripcionObligacion" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvGestionarObligacion" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdGestionObligacion"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvGestionarObligacion_PageIndexChanging"
                        OnSelectedIndexChanged="gvGestionarObligacion_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código Gestión Obligación" DataField="IdGestionObligacion" />
                            <asp:BoundField HeaderText="Código Gestión Cláusula" DataField="IdGestionClausula" />
                            <asp:BoundField HeaderText="Nombre Obligacion" DataField="NombreObligacion" />
                            <asp:BoundField HeaderText="Tipo Obligación" DataField="IdTipoObligacion" />
                            <asp:BoundField HeaderText="Orden" DataField="Orden" />
                            <asp:BoundField HeaderText="Descripción Obligación" DataField="DescripcionObligacion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 7) {
                    $('#txtIdContrato').val(retLupa[0]);
                    $('#txtTipoContrato').val(retLupa[6]);
                    $('#hdIdTipoContrato').val(retLupa[7]);
                }
            }
        }
        function GetClausula() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratoClausulaObligacion.aspx', setReturnGetClausula, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetClausula(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 2) {
                    $('#txtIdGestionClausula').val(retLupa[0]);
                    $('#txtNombreClausula').val(retLupa[2]);
                }
            }
        }
        function GetObligacion() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaObligacion.aspx', setReturnGetObligacion, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetObligacion(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 4) {
                    $('#hfIdGestionObligacion').val(retLupa[0]);
                    $('#txtNombreObligacion').val(retLupa[2]);
                    $('#txtIdTipoObligacion').val(retLupa[4]);
                }
            }
        }
    </script>
</asp:Content>
