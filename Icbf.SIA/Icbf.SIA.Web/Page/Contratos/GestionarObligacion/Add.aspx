<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_GestionarObligacion_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdGestionObligacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="25%">
                Código Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtIdContrato"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="25%">
                Tipo Contrato *&nbsp;
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoContrato" ControlToValidate="txtTipoContrato"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="25%">
                Id Gestión Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdClausula" ControlToValidate="txtIdClausula"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="25%">
                Nombre Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreClausula" ControlToValidate="txtNombreClausula"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoContrato" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTipoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIdTipoContrato" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdClausula" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreClausula" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreClausula" runat="server" TargetControlID="txtNombreClausula"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ @" />
                <asp:Image ID="imgBClausula" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetClausula()" Style="cursor: hand" ToolTip="Buscar" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Id Gestión Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdGestionObligacion" ControlToValidate="txtIdGestionObligacion"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoObligacion" ControlToValidate="txtIdTipoObligacion"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Nombre Obligación *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtNombreObligacion"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdGestionObligacion" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdGestionObligacion" runat="server" TargetControlID="txtIdGestionObligacion"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoObligacion" ClientIDMode="Static" MaxLength="50"
                    onfocus="blur();" class="enable"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreObligacion" ClientIDMode="Static" Width="300px"
                    onfocus="blur();" class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreObligacion" runat="server" TargetControlID="txtNombreObligacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
                <asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetObligacion()" Style="cursor: hand" ToolTip="Buscar" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Orden *
                <asp:RequiredFieldValidator runat="server" ID="rfvOrden" ControlToValidate="txtOrden"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">
                Descripci&#243;n Obligaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionObligacion" ControlToValidate="txtDescripcionObligacion"
                    SetFocusOnError="true" ErrorMessage="Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtOrden" Height="22px" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftOrden" runat="server" TargetControlID="txtOrden"
                    FilterType="Numbers" ValidChars="0123456789" />
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionObligacion" Height="50px" MaxLength="128"
                    TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionObligacion" runat="server" TargetControlID="txtDescripcionObligacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 7) {
                    $('#txtIdContrato').val(retLupa[0]);
                    $('#txtTipoContrato').val(retLupa[6]);
                    $('#hdIdTipoContrato').val(retLupa[7]);
                }
            }
        }
        function GetClausula() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratoClausulaObligacion.aspx', setReturnGetClausula, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetClausula(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 2) {
                    $('#txtIdClausula').val(retLupa[0]);
                    $('#txtNombreClausula').val(retLupa[2]);
                }
            }
        }
        function GetObligacion() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaObligacion.aspx', setReturnGetObligacion, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetObligacion(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 4) {
                    $('#txtIdGestionObligacion').val(retLupa[0]);
                    $('#txtNombreObligacion').val(retLupa[2]);
                    $('#txtIdTipoObligacion').val(retLupa[4]);
                }
            }
        }
    </script>
</asp:Content>
