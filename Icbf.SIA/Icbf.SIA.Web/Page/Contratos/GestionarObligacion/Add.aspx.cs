using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de gestión de obligaciones
/// </summary>
public partial class Page_GestionarObligacion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/GestionarObligacion";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            GestionarObligacion vGestionarObligacion = new GestionarObligacion();

            vGestionarObligacion.IdGestionClausula = Convert.ToInt32(txtIdClausula.Text);
            vGestionarObligacion.NombreObligacion = Convert.ToString(txtNombreObligacion.Text);
            vGestionarObligacion.IdTipoObligacion = Convert.ToInt32(GetSessionParameter("GestionarObligacion.IdTipoObligacion"));
            vGestionarObligacion.Orden = Convert.ToString(txtOrden.Text);
            vGestionarObligacion.DescripcionObligacion = Convert.ToString(txtDescripcionObligacion.Text);

            SetSessionParameter("GestionarObligacion.IdContrato", txtIdContrato.Text);
            SetSessionParameter("GestionarObligacion.TipoContrato", txtTipoContrato.Text);
            SetSessionParameter("GestionarObligacion.NombreClausula", txtNombreClausula.Text);
            SetSessionParameter("GestionarObligacion.IdObligacion", txtIdGestionObligacion.Text);

            if (Request.QueryString["oP"] == "E")
            {
            vGestionarObligacion.IdGestionObligacion = Convert.ToInt32(hfIdGestionObligacion.Value);
                vGestionarObligacion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vGestionarObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarGestionarObligacion(vGestionarObligacion);
            }
            else
            {
                vGestionarObligacion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vGestionarObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarGestionarObligacion(vGestionarObligacion);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("GestionarObligacion.IdGestionObligacion", vGestionarObligacion.IdGestionObligacion);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("GestionarObligacion.Modificado", "1");
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                }
                else
                {
                    SetSessionParameter("GestionarObligacion.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }


            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Gestionar Obligaciones", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos provenientes de la entidad a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdGestionObligacion = Convert.ToInt32(GetSessionParameter("GestionarObligacion.IdGestionObligacion"));
            RemoveSessionParameter("GestionarObligacion.Id");

            GestionarObligacion vGestionarObligacion = new GestionarObligacion();
            vGestionarObligacion = vContratoService.ConsultarGestionarObligacion(vIdGestionObligacion);
            hfIdGestionObligacion.Value = vGestionarObligacion.IdGestionObligacion.ToString();
            txtIdClausula.Text = vGestionarObligacion.IdGestionClausula.ToString();
            txtNombreObligacion.Text = vGestionarObligacion.NombreObligacion;
            txtIdTipoObligacion.Text = vGestionarObligacion.IdTipoObligacion.ToString();
            txtOrden.Text = vGestionarObligacion.Orden;
            txtDescripcionObligacion.Text = vGestionarObligacion.DescripcionObligacion;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGestionarObligacion.UsuarioCrea, vGestionarObligacion.FechaCrea, vGestionarObligacion.UsuarioModifica, vGestionarObligacion.FechaModifica);

            txtIdContrato.Text = GetSessionParameter("GestionarObligacion.IdContrato").ToString();
            txtTipoContrato.Text = GetSessionParameter("GestionarObligacion.TipoContrato").ToString();
            txtNombreClausula.Text = GetSessionParameter("GestionarObligacion.NombreClausula").ToString();
            txtIdGestionObligacion.Text = GetSessionParameter("GestionarObligacion.IdObligacion").ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}