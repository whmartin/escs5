﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_ReasignarAbogado_Add" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Radicado denuncia
                        </td>
                        <td>Departamento ubicación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamentoUbicacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Tipo identificación
                        </td>
                        <td>Número identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td style="width: 45%">Nombre / razón social
                        </td>
                        <td style="width: 55%">Fecha radicado denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Abogado asignado
                        </td>
                        <td style="width: 55%">Fecha asignación abogado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtAbogadoAsignado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfechaAsignacionAbogado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Estado
                        </td>
                        <td style="width: 55%">Fecha estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionAbogado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Información del abogado
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nombre de abogado *  
                <asp:Label ID="lblRequeridoNombreAbogado" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
            </td>
            <td>
                Fecha de reasignacion de abogado * 
                 <asp:Label ID="lblRequeridoFechaAsignacion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbogado" Width="50%" MaxLength="256"></asp:TextBox>
                <asp:ImageButton ToolTip="Buscar" ID="btnBuscarAbogados" runat="server" OnClick="btnBuscarAbogados_Click" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                     Height="22px" Width="22px" />
                <Ajax:FilteredTextBoxExtender ID="ftNombreAbogado" runat="server" TargetControlID="txtNombreAbogado" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <uc1:fecha runat="server" ID="FechaAsignacion" Enabled="false" />
            </td>
        </tr>   
                    <tr class="rowB">
            <td colspan="2">
                Profesíon 
            </td>          
        </tr>    
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtProfesion" Width="21%" MaxLength="256"></asp:TextBox>                
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProfesion" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>            
        </tr>            
                         
                </table>
            </asp:Panel>
             <asp:Panel runat="server" ID="pnlBuscarAbogado">
                <asp:Panel runat="server" ID="pnlLista">
                    <table width="90%" align="center">                        
                        <tr class="rowB">
                            <td colspan="2"></td>
                        </tr>
                        <tr class="rowAG">
                            <td>
                              <asp:GridView ID="gvwConsultaAbogados" runat="server"
                                  DataKeyNames="IdAbogado,TipoFuncionario,IdUsuario,IdRegionalAbogado,NombreRegionalAbogado,Correo"
                                AllowSorting="True" AllowPaging="true" AutoGenerateColumns="False" CellPadding="0" GridLines="None"
                                Width="100%" EnableViewState="true" OnSelectedIndexChanged="gvwConsultaAbogados_SelectedIndexChanged"
                                OnPageIndexChanging="gvwConsultaAbogados_PageIndexChanging"
                                  OnSorting="gvwConsultaAbogados_OnSorting">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                   <asp:TemplateField>
                                        <ItemTemplate>
                                        <asp:ImageButton ID="btnApply" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                            Height="16px" Width="16px" ToolTip="Aplicar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Nombre de abogado" DataField="NombreAbogado" SortExpression="NombreAbogado" />
                                    <asp:BoundField HeaderText="Núnero de identificación" DataField="Identificacion" SortExpression="Identificacion" />
                                    <asp:BoundField HeaderText="Profesión" DataField="Profesion" SortExpression="Profesion" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
            <asp:HiddenField runat="server" ID="hfIdAbogado" />
            <asp:HiddenField runat="server" ID="hfNombreAbogado" />
            <asp:HiddenField runat="server" ID="hfIdRegional" />
            <asp:HiddenField runat="server" ID="hfNombreregional" />
            <asp:HiddenField runat="server" ID="hfTipoFuncionario" />
            <asp:HiddenField runat="server" ID="hfIdUsuario" />
            <asp:HiddenField runat="server" ID="hfIdentificacion" />
            <asp:HiddenField runat="server" ID="hfCorreo" />
        </ContentTemplate>
     
</asp:Content>


