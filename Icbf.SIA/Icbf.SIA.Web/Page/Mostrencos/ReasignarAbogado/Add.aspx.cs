﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.Configuration;
using System.Web.Security;
using Icbf.SIA.Entity;

/// <summary>
/// Clase del formulario Page_Mostrencos_ReasignarAbogado_Add
/// </summary>
public partial class Page_Mostrencos_ReasignarAbogado_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ReasignarAbogado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }

            this.toolBar.LipiarMensajeError();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/List.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// evento del boton buscar abogados
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscarAbogados_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!this.ExistenCamposRequeridos(false))
            {
                this.pnlBuscarAbogado.Visible = true;
                this.Buscar();
            }
            else
            {
                this.pnlBuscarAbogado.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }

    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwConsultaAbogados_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvwConsultaAbogados.SelectedRow);
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvwConsultaAbogados_OnSorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            List<AsignacionAbogado> vList = new List<AsignacionAbogado>();
            List<AsignacionAbogado> vResult = new List<AsignacionAbogado>();

            if (this.ViewState["SortedgvwConsultaAbogados"] != null)
            {
                vList = (List<AsignacionAbogado>)this.ViewState["SortedgvwConsultaAbogados"];
            }

            switch (e.SortExpression)
            {
                case "NombreAbogado":
                    if (this.Direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreAbogado).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreAbogado).ToList();
                    }

                    break;

                case "Identificacion":
                    if (this.Direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.Identificacion).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.Identificacion).ToList();
                    }

                    break;

                case "Profesion":
                    if (this.Direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.Profesion).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.Profesion).ToList();
                    }

                    break;
            }

            if (this.Direction == SortDirection.Ascending)
            {
                this.Direction = SortDirection.Descending;
            }
            else
            {
                this.Direction = SortDirection.Ascending;
            }

            this.ViewState["SortedgvwConsultaAbogados"] = vResult;
            this.gvwConsultaAbogados.DataSource = vResult;
            this.gvwConsultaAbogados.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwConsultaAbogados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwConsultaAbogados.PageIndex = e.NewPageIndex;        
        this.gvwConsultaAbogados.DataSource = (List<AsignacionAbogado>)this.ViewState["SortedgvwConsultaAbogados"];
        this.gvwConsultaAbogados.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// método invocado desde el evento Selectedindex de la grilla Maestra, guarda los datos llave y direcciona la página de detalles
    /// </summary>
    /// <param name="pRow">Recibe el GridViewRow seleccionado de la grilla maestra</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            bool vEsAbogado = false;
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            Abogados vAbogado = this.vMostrencosService.ConsultarAbogadoXId(this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien).IdAbogado);
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvwConsultaAbogados.DataKeys[rowIndex].Value.ToString();

            this.hfIdAbogado.Value = strValue;
            this.hfTipoFuncionario.Value = this.gvwConsultaAbogados.DataKeys[rowIndex].Values[1].ToString().Replace(",", "");
            this.hfIdUsuario.Value = Convert.ToInt32(this.gvwConsultaAbogados.DataKeys[rowIndex].Values[2]).ToString().Replace(",", "");
            this.hfIdRegional.Value = string.IsNullOrEmpty(this.gvwConsultaAbogados.DataKeys[rowIndex].Values[3].ToString())
                                    ? "0"
                                    : Convert.ToInt32(this.gvwConsultaAbogados.DataKeys[rowIndex].Values[3]).ToString().Replace(",", "");
            this.hfNombreregional.Value = this.gvwConsultaAbogados.DataKeys[rowIndex].Values[4] == null ? "" : this.gvwConsultaAbogados.DataKeys[rowIndex].Values[4].ToString().Replace(",", "");
            this.hfCorreo.Value = this.gvwConsultaAbogados.DataKeys[rowIndex].Values[5] == null ? "" : this.gvwConsultaAbogados.DataKeys[rowIndex].Values[5].ToString().Replace(",", "");
            this.hfNombreAbogado.Value = Convert.ToString(this.gvwConsultaAbogados.Rows[rowIndex].Cells[1].Text).Replace(",", "");
            this.hfIdentificacion.Value = Convert.ToString(this.gvwConsultaAbogados.Rows[rowIndex].Cells[2].Text).Replace(",", "");
            Usuario vUsuario = this.vMostrencosService.ConsultarUsuarioPorIdUsuario(Convert.ToInt32(this.hfIdUsuario.Value));

            if (vUsuario != null)
            {
                List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", "ABOGADO");
                string[] vRoles = vUsuario.Rol.Split(';');
                foreach (string item in vRoles)
                {
                    if (vListaRoles.Where(p => p.NombreRol.ToUpper() == item.ToUpper()).Count() > 0)
                    {
                        vEsAbogado = true;
                        break;
                    }
                }
            }

            if (vUsuario == null)
            {
                this.toolBar.MostrarMensajeError("El usuario seleccionado no se encuentra registrado en el sistema. Por favor revisar");
            }
            else if (!vEsAbogado)
            {
                this.toolBar.MostrarMensajeError("El usuario seleccionado no tiene el rol \"Abogado\". Por favor revisar");
            }
            else if (!vUsuario.Estado)
            {
                this.toolBar.MostrarMensajeError("El usuario seleccionado no se encuentra activo. Por favor revisar");
            }
            else if (vAbogado.IdUsuario == Convert.ToInt32(this.hfIdUsuario.Value))
            {
                this.toolBar.MostrarMensajeError("El usuario seleccionado debe ser diferente al usuario asignado actualmente. Por favor revisar");
            }
            else
            {
                this.pnlBuscarAbogado.Visible = false;
                txtNombreAbogado.Text = this.gvwConsultaAbogados.Rows[rowIndex].Cells[1].Text.Trim();
                this.FechaAsignacion.Date = DateTime.Now.Date;
            }
        }

        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Reasignar Abogado", "Add");
            this.FechaAsignacion.NoChecarFormato();
            this.gvwConsultaAbogados.PageSize = this.PageSize();
            this.gvwConsultaAbogados.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            if (this.vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                this.txtDepartamentoUbicacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Regional.NombreRegional) ? vDenunciaBien.Regional.NombreRegional.ToUpper() : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                this.txtNombreRazonSocial.Text = vDenunciaBien.NombreMostrar;
                this.txtFechaRadicado.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy") : string.Empty;
                this.txtAbogadoAsignado.Text = vDenunciaBien.Abogados.NombreAbogado;
                this.txtfechaAsignacionAbogado.Text = vDenunciaBien.FechaAsignacionAbogado.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaAsignacionAbogado).ToString("dd/MM/yyyy") : string.Empty;
                this.txtEstado.Text = !string.IsNullOrEmpty(vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString()) ? vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString() : string.Empty;
                this.txtFechaEstado.Text = vDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count > 0 ? vDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.IdDenunciaBien).LastOrDefault().FechaCrea.ToString("dd/MM/yyyy") : this.txtfechaAsignacionAbogado.Text;
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Busca la informacion que coisida con los parámetros de busqueda
    /// </summary>
    private void Buscar()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            int vIdRegional = this.GetSessionUser().IdRegional == null ? this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien).IdRegionalUbicacion : Convert.ToInt32(this.GetSessionUser().IdRegional);
            AsignacionAbogado pConsultaAbogados = new AsignacionAbogado();
            pConsultaAbogados.IdRegionalAbogado = vIdRegional;
            pConsultaAbogados.NombreAbogado = txtNombreAbogado.Text == string.Empty ? null : txtNombreAbogado.Text.ToUpper();
            pConsultaAbogados.Profesion = txtProfesion.Text == string.Empty ? null : txtProfesion.Text.ToUpper();
            List<AsignacionAbogado> vListaAsignacion = this.vMostrencosService.ConsultarAbogadosSoloEnSIA(pConsultaAbogados);
            this.ViewState["SortedgvwConsultaAbogados"] = vListaAsignacion;
            this.gvwConsultaAbogados.DataSource = vListaAsignacion;
            this.gvwConsultaAbogados.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }

    }

    /// <summary>
    /// guarda la informacion en la base de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            if (!this.ExistenCamposRequeridos(true))
            {
                if (this.PuedeReasignarAbogado())
                {
                    int vResultado;
                    int vIdAbogadoAnterior = 0;
                    Abogados vAbogados = CapturarValoresAbogados();
                    this.InformacionAudioria(vAbogados, this.PageName, SolutionPage.Add);
                    vResultado = this.vMostrencosService.InsetrarAbogados(vAbogados);
                    if (vResultado == 0)
                    {
                        this.toolBar.MostrarMensajeError("No fue posible reasignar el abogado. Por favor inténtelo nuevamente.");
                    }
                    else
                    {
                        HistoricoAsignacionDenuncias vHistoricoAsignacionDenuncias = CapturarValores();
                        vHistoricoAsignacionDenuncias.IdAbogado = vResultado;
                        this.InformacionAudioria(vHistoricoAsignacionDenuncias, this.PageName, SolutionPage.Add);
                        vResultado = this.vMostrencosService.InsertarHistoricoAsignacionDenuncias(vHistoricoAsignacionDenuncias);
                        if (vResultado == 0)
                        {
                            this.toolBar.MostrarMensajeError("No fue posible reasignar el abogado. Por favor inténtelo nuevamente.");
                        }
                        else
                        {
                            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vHistoricoAsignacionDenuncias.IdDenunciaBien);
                            vIdAbogadoAnterior = vDenunciaBien.IdAbogado;
                            vDenunciaBien.IdEstadoDenuncia = this.vMostrencosService.ConsultarEstadosDenuncia().Where(p => p.NombreEstadoDenuncia.Equals("REASIGNADA")).FirstOrDefault().IdEstadoDenuncia;
                            vDenunciaBien.IdAbogado = vHistoricoAsignacionDenuncias.IdAbogado;
                            vDenunciaBien.FechaAsignacionAbogado = vHistoricoAsignacionDenuncias.FechaAsignacion;
                            vDenunciaBien.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                            this.InformacionAudioria(vDenunciaBien, this.PageName, SolutionPage.Add);
                            vResultado = this.vMostrencosService.EditarDenunciaBien(vDenunciaBien);
                            if (vResultado > 0)
                            {
                                HistoricoEstadosDenunciaBien vHistorico = new HistoricoEstadosDenunciaBien();
                                vHistorico.IdDenunciaBien = vHistoricoAsignacionDenuncias.IdDenunciaBien;
                                vHistorico.IdEstadoDenuncia = vDenunciaBien.IdEstadoDenuncia;
                                vHistorico.IdUsuarioCrea = this.GetSessionUser().IdUsuario;
                                vHistorico.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                                this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(vHistorico);
                            }

                            this.EnviarNotificacion(vHistoricoAsignacionDenuncias, vIdAbogadoAnterior);
                            this.SetSessionParameter("Mostrencos.ReasignarAbogado.Mensaje", "La información ha sido guardada exitosamente");
                            this.NavigateTo("ListReasignacion.aspx");
                        }
                    }
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No fue posible resignar el abogado. Por favor verifique el estado de traslado de la denuncia.");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// captura los datos diligenciados por el usuario
    /// </summary>
    /// <returns>una variable de tipo Abogados con los datos capturados</returns>
    private Abogados CapturarValoresAbogados()
    {
        try
        {
            Abogados vAbogados = new Abogados();
            vAbogados.Identificacion = Convert.ToInt64(this.hfIdentificacion.Value);
            vAbogados.IdRegionalAbogado = Convert.ToInt32(this.hfIdRegional.Value);
            vAbogados.IdUsuario = Convert.ToInt32(this.hfIdUsuario.Value);
            vAbogados.NombreAbogado = this.hfNombreAbogado.Value.ToString();
            vAbogados.NombreRegionalAbogado = this.hfNombreregional.Value.ToString();
            vAbogados.UsuarioCrea = this.GetSessionUser().NombreUsuario;
            vAbogados.FechaAsignacionAbogado = this.FechaAsignacion.Date;
            vAbogados.TipoFuncionarioAbogado = this.hfTipoFuncionario.Value.ToString();
            return vAbogados;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida si se puede reasignar el abogado
    /// </summary>
    /// <returns>true o false</returns>
    private bool PuedeReasignarAbogado()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            int vIdEstadoDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien).IdEstadoDenuncia;
            string vNombreEstadoTrasladoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vIdEstadoDenuncia).NombreEstadoDenuncia;
            return vNombreEstadoTrasladoDenuncia != "REGISTRADA" && vNombreEstadoTrasladoDenuncia != "TRASLADO SOLICITADO" && vNombreEstadoTrasladoDenuncia != "TRASLADO SOLICITADO APROBADO" && vNombreEstadoTrasladoDenuncia != "TRASLADO NEGADO";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// captura los datos diligenciados por el usuario
    /// </summary>
    /// <returns>una variable de tipo HistoricoAsignacionDenuncias con los datos capturados</returns>
    private HistoricoAsignacionDenuncias CapturarValores()
    {
        try
        {
            HistoricoAsignacionDenuncias vHistoricoAsignacionDenuncias = new HistoricoAsignacionDenuncias();
            vHistoricoAsignacionDenuncias.IdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            vHistoricoAsignacionDenuncias.FechaAsignacion = DateTime.Now;
            vHistoricoAsignacionDenuncias.UsuarioCrea = this.GetSessionUser().NombreUsuario;
            return vHistoricoAsignacionDenuncias;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// valida si existen campos requeridos sin ser diligenciados
    /// </summary>
    /// <param name="vValidarFecha">identifica si se valida la fecha o no </param>
    /// <returns>true o false</returns>
    private bool ExistenCamposRequeridos(bool vValidarFecha)
    {
        bool vEsrequerido = false;
        try
        {
            if (string.IsNullOrEmpty(this.txtNombreAbogado.Text))
            {
                this.lblRequeridoNombreAbogado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoNombreAbogado.Visible = false;
                vEsrequerido = false;
            }

            if (this.FechaAsignacion.Date == Convert.ToDateTime("1/01/1900") && vValidarFecha)
            {
                this.lblRequeridoFechaAsignacion.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoFechaAsignacion.Visible = false;
                vEsrequerido = false;
            }

            return false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// Envia notificación al guardar el registro de la denuncia
    /// </summary>
    /// <param name="pRegistroDenuncia">Entidad pRegistroDenuncia</param>
    private void EnviarNotificacion(HistoricoAsignacionDenuncias pHistoricoAsignacionDenuncias, int pIdAbogadoAnterior)
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            Abogados vAbogadoAnterior = this.vMostrencosService.ConsultarAbogadoXId(pIdAbogadoAnterior);
            Abogados vAbogadoAsignado = this.vMostrencosService.ConsultarAbogadoXId(pHistoricoAsignacionDenuncias.IdAbogado);
            string vEmailPara = this.vMostrencosService.ConsultarUsuarioPorIdUsuario(vAbogadoAnterior.IdUsuario).CorreoElectronico;
            string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
            string vAsunto = HttpContext.Current.Server.HtmlDecode("Nueva asignaci&oacute;n de denuncia");
            int vIdUsuario = 0;
            string vArchivo = string.Empty;
            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);
            string vMensaje = "<div style='color: #000000;'> <font color='black'> <CENTER> <B> Se ha Reasignado la denuncia </B> </CENTER> </font> " +
                       "<br/> <br/> <font color='black'> <B> Apreciado (a) </B> </font> " + vAbogadoAnterior.NombreAbogado.Trim() +
                       HttpContext.Current.Server.HtmlDecode(" <br/> <br/> La asignaci&oacute;n a esta denuncia, ha sido reasignada: ") +
                       " <br/> <br/> <br/> Radicado de la denuncia " + vDenunciaBien.RadicadoDenuncia +
                       "<br/> <br/> Fecha de radicado " + vDenunciaBien.FechaRadicadoDenuncia.ToString() +
                       "<br/> <br/> <br/> Por favor tener en cuenta</div>";

            bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);

            vEmailPara = this.vMostrencosService.ConsultarUsuarioPorIdUsuario(vAbogadoAsignado.IdUsuario).CorreoElectronico;
            vMensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B> Asignaci&oacute;n de la denuncia  </B> </CENTER> </font> ") +
                       "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + vAbogadoAsignado.NombreAbogado.Trim() +
                       " <br/> <br/> Ha sido asignado a la siguiente denuncia: " +
                       " <br/> <br/> <br/> Radicado de la denuncia " + vDenunciaBien.RadicadoDenuncia +
                       "<br/> <br/> Fecha de radicado " + vDenunciaBien.FechaRadicadoDenuncia.ToString() +
                      HttpContext.Current.Server.HtmlDecode("<br/> <br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria. </div>");
            result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
}