﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="ListReasignacion.aspx.cs" Inherits="Page_Mostrencos_ReasignarAbogado_ListAsignacion" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Radicado denuncia
                        </td>
                        <td>Departamento ubicación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamentoUbicacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Tipo identificación
                        </td>
                        <td>Número identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td style="width: 45%">Nombre / razón social
                        </td>
                        <td style="width: 55%">Fecha radicado denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Abogado asignado
                        </td>
                        <td style="width: 55%">Fecha asignación abogado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtAbogadoAsignado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfechaAsignacionAbogado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Estado
                        </td>
                        <td style="width: 55%">Fecha estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionTraslado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del abogado
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>                
                                       
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
     <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvHistorialAsignaciones" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdHistoricoAsignacionDenuncias" CellPadding="0" Height="16px" OnSorting="gvHistorialAsignaciones_OnSorting"
                        OnPageIndexChanging="gvHistorialAsignaciones_PageIndexChanging">
                        <Columns>                            
                            <asp:BoundField HeaderText="Nombre de abogado" DataField="Abogados.NombreAbogado" SortExpression="NombreAbogado" />
                            <asp:BoundField HeaderText="Número de identificación" DataField="Abogados.Identificacion" SortExpression="NumreoIdentificacion"/>
                            <asp:BoundField HeaderText="Fecha de Asignación" DataField="FechaAsignacion" SortExpression="FechaAsignacion" DataFormatString="{0:d}"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>


