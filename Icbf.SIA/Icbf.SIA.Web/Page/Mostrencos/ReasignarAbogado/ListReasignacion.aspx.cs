﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_ReasignarAbogado_ListAsignacion
/// </summary>
public partial class Page_Mostrencos_ReasignarAbogado_ListAsignacion : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ReasignarAbogado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento del boton nuevo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        if (this.PuedeReasignarAbogado())
        {
            this.NavigateTo(SolutionPage.Add);
        }
        else
        {
            this.toolBar.MostrarMensajeError("Es estado actual de la denuncia no permite reasignar el abogado, por favor revisar");
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/List.aspx");
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvHistorialAsignaciones_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoAsignacionDenuncias> vList = new List<HistoricoAsignacionDenuncias>();
        List<HistoricoAsignacionDenuncias> vResult = new List<HistoricoAsignacionDenuncias>();

        if (this.ViewState["SortedgvHistorialAsignaciones"] != null)
        {
            vList = (List<HistoricoAsignacionDenuncias>)this.ViewState["SortedgvHistorialAsignaciones"];
        }

        switch (e.SortExpression)
        {
            case "NombreAbogado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Abogados.NombreAbogado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Abogados.NombreAbogado).ToList();
                }

                break;

            case "NumreoIdentificacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Abogados.Identificacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Abogados.Identificacion).ToList();
                }

                break;

            case "FechaAsignacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaAsignacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaAsignacion).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.ViewState["SortedgvHistorialAsignaciones"] = vResult;
        this.gvHistorialAsignaciones.DataSource = vResult;
        this.gvHistorialAsignaciones.DataBind();
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvHistorialAsignaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvHistorialAsignaciones.PageIndex = e.NewPageIndex;
        this.gvHistorialAsignaciones.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvHistorialAsignaciones"] != null)
        {
            List<RegistroDenuncia> vList = (List<RegistroDenuncia>)this.ViewState["SortedgvHistorialAsignaciones"];
            this.gvHistorialAsignaciones.DataSource = vList;
            this.gvHistorialAsignaciones.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            //this.toolBar.EstablecerTitulos("Reasignar Abogado", "List");
            this.toolBar.EstablecerTitulos("Reasignar Abogado");
            this.gvHistorialAsignaciones.PageSize = this.PageSize();
            this.gvHistorialAsignaciones.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.Buscar();
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            if (this.vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);
                if (vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 11 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 13 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                this.txtDepartamentoUbicacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Regional.NombreRegional) ? vDenunciaBien.Regional.NombreRegional.ToUpper() : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                this.txtNombreRazonSocial.Text = vDenunciaBien.NombreMostrar;
                this.txtFechaRadicado.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy") : string.Empty;
                this.txtAbogadoAsignado.Text = vDenunciaBien.Abogados.NombreAbogado;
                this.txtfechaAsignacionAbogado.Text = vDenunciaBien.FechaAsignacionAbogado.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaAsignacionAbogado).ToString("dd/MM/yyyy") : string.Empty;
                this.txtEstado.Text = !string.IsNullOrEmpty(vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString()) ? vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString() : string.Empty;
                this.txtFechaEstado.Text = vDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count > 0 ? vDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.IdDenunciaBien).LastOrDefault().FechaCrea.ToString("dd/MM/yyyy") : this.txtfechaAsignacionAbogado.Text;
            }

            string vMensaje = (string)this.GetSessionParameter("Mostrencos.ReasignarAbogado.Mensaje");
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.ReasignarAbogado.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Busca la informacion que coisida con los parámetros de busqueda
    /// </summary>
    private void Buscar()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            List<HistoricoAsignacionDenuncias> vListaHistoricoAsignacionDenuncias = this.vMostrencosService.ConsultarHistoricoAsignacionDenunciasPorIdDenunciaBien(this.vIdDenunciaBien);
            foreach (HistoricoAsignacionDenuncias item in vListaHistoricoAsignacionDenuncias)
            {
                item.DenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(item.IdDenunciaBien);
                item.Abogados = this.vMostrencosService.ConsultarAbogadoXId(item.IdAbogado);
            }

            if (vListaHistoricoAsignacionDenuncias.Count == 0)
            {
                HistoricoAsignacionDenuncias vHistoricoAsignacionDenuncias = new HistoricoAsignacionDenuncias();
                vHistoricoAsignacionDenuncias.IdDenunciaBien = this.vIdDenunciaBien;
                vHistoricoAsignacionDenuncias.DenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien);
                vHistoricoAsignacionDenuncias.IdAbogado = vHistoricoAsignacionDenuncias.DenunciaBien.IdAbogado;
                vHistoricoAsignacionDenuncias.Abogados = this.vMostrencosService.ConsultarAbogadoXId(vHistoricoAsignacionDenuncias.IdAbogado);
                vHistoricoAsignacionDenuncias.FechaAsignacion = Convert.ToDateTime(vHistoricoAsignacionDenuncias.DenunciaBien.FechaAsignacionAbogado);
                if (vHistoricoAsignacionDenuncias.IdAbogado != 0)
                {
                    vListaHistoricoAsignacionDenuncias.Add(vHistoricoAsignacionDenuncias);
                }
            }

            this.ViewState["SortedgvHistorialAsignaciones"] = vListaHistoricoAsignacionDenuncias;
            this.gvHistorialAsignaciones.DataSource = vListaHistoricoAsignacionDenuncias;
            this.gvHistorialAsignaciones.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Valida si se puede reasignar el abogado
    /// </summary>
    /// <returns>true o false</returns>
    private bool PuedeReasignarAbogado()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien"));
            int vIdEstadoDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien).IdEstadoDenuncia;
            string vNombreEstadoTrasladoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vIdEstadoDenuncia).NombreEstadoDenuncia;
            return vNombreEstadoTrasladoDenuncia != "REGISTRADA" && vNombreEstadoTrasladoDenuncia != "TRASLADO SOLICITADO" && vNombreEstadoTrasladoDenuncia != "TRASLADO SOLICITADO APROBADO" && vNombreEstadoTrasladoDenuncia != "TRASLADO NEGADO";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }
    #endregion
}