<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ReportesEstadisticos_List" %>

<%@ Register Src="~/General/General/Control/fechaEdit.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript">
        $(function () {
            $('[id*=ddlEstado]').multiselect({
                includeSelectAllOption: false,
                nonSelectedText: 'SELECCIONE',
                Widht: 185,
            });

            $('[id*=lbRegionales]').multiselect({
                includeSelectAllOption: false,
                nonSelectedText: 'SELECCIONE',
                numberDisplayed: 6,
                Widht: 185,
            });
        });
    </script>
    <asp:Panel runat="server" ID="pConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 60%;">Tipo de Reporte *
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoReporte" ControlToValidate="ddlTipoReporte"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
                <td>Regional *
                    <asp:RequiredFieldValidator runat="server" ID="rfvRegional" ControlToValidate="ddlRegional"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
                    <asp:Label runat="server" ID="lblCountRegionales" Visible="false" ForeColor="Red"
                            Text="Debe seleccionar como máximo seis (6) regionales"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvRegionallb" ControlToValidate="lbRegionales"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoReporte" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlTipoReporte_SelectedIndexChanged">
                        <asp:ListItem Text="SELECCIONAR" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="REPORTE CONSOLIDADO DE BIENES ADJUDICADOS POR REGIONAL" Value="1"></asp:ListItem>
                        <asp:ListItem Text="REPORTE CONSOLIDADO POR RERGIONAL Y ESTADOS DEL PROCESO" Value="2"></asp:ListItem>
                        <asp:ListItem Text="REPORTE CONSOLIDADO POR RERGIONAL Y SENTIDO DE LA SENTENCIA" Value="3"></asp:ListItem>
                        <asp:ListItem Text="REPORTE CONSOLIDADO POR RERGIONAL Y TIPO DE TRÁMITE" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlRegional"></asp:DropDownList>

                    <asp:ListBox ID="lbRegionales" runat="server" SelectionMode="Multiple"
                            OnSelectedIndexChanged="lbRegionales_SelectedIndexChanged" Visible="false"></asp:ListBox>
                </td>
            </tr>
            <asp:Panel runat="server" ID="pnlFechas" Visible="false">
                <tr class="rowB">
                    <td style="width: 60%;">Fecha Inicial *
                    <asp:RequiredFieldValidator runat="server" ID="rfvFechaInicial" ControlToValidate="txtFechaInicial$txtFecha"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                        <asp:Label runat="server" ID="lblFechaInicialMayor" Visible="false" ForeColor="Red"
                            Text="La fecha inicial no puede ser mayor a la fecha actual"></asp:Label>
                    </td>
                    <td>Fecha Final *
                    <asp:RequiredFieldValidator runat="server" ID="rfvFechaFinal" ControlToValidate="txtFechaFinal$txtFecha"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                        <asp:Label runat="server" ID="lblFechaFinalMayor" Visible="false" ForeColor="Red"
                            Text="La fecha final debe ser mayor o igual a la fecha inicial"></asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <uc1:fecha runat="server" ID="txtFechaInicial" onkeypress="return false;" />
                    </td>
                    <td>
                        <uc1:fecha runat="server" ID="txtFechaFinal" onkeypress="return false;" />
                    </td>
                </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlEstado" Visible="false">
                <tr class="rowB">
                    <td colspan="2">Estado *
                        <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="ddlEstado"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:ListBox ID="ddlEstado" runat="server" SelectionMode="Multiple"></asp:ListBox>
                    </td>
                </tr>
            </asp:Panel>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pLista" Width="1000px">
        <table width="90%" align="center" style="margin-top:20px;">
            <tr class="rowB">
                <td rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <div style="overflow: auto !important; width: 100%; height: 100% !important; align-content: center; background-color: white; text-align: center;">
                        <rsweb:ReportViewer ID="rviReportes" runat="server" ProcessingMode="Remote" AsyncRendering="false" SizeToReportContent="true" KeepSessionAlive="false"
                            Width="100%" Height="100%" Visible="false">
                        </rsweb:ReportViewer>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
