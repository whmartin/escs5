﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs</summary>
// <author>Ingenian Software</author>
// <date>12/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Clase Parcial para Gestionar el List de ReportesEstadisticos
/// </summary>
public partial class Page_ReportesEstadisticos_List : GeneralWeb
{
    #region Variables

    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Variable global del path de la página
    /// </summary>
    private string vPageName = "Mostrencos/ReportesEstadisticos";

    /// <summary>
    /// Referencia a vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Referencia a ManejoControles
    /// </summary>
    private ManejoControles vManejoControles = new ManejoControles();

    /// <summary>
    /// Object for reports
    /// </summary>
    private Report ObjReport;

    /// <summary>
    /// Variable estado de tipo lista
    /// </summary>
    public List<int> vlEstado = new List<int>();

    /// <summary>
    /// Variable para Incrementar las Regionales
    /// </summary>
    public int vIncReg = 0;

    #endregion

    #region Eventos Pagina

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">Pre Init</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;

        if (this.ValidateAccess(this.toolBar, this.vPageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.toolBar.LipiarMensajeError();
                this.CargarDatosIniciales();
            }
        }
    }

    #endregion

    #region Eventos Controles

    /// <summary>
    /// Evento para generar el reporte
    /// </summary>
    /// <param name="sender">El objeto sender</param>
    /// <param name="e">El objeto e</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.toolBar.LipiarMensajeError();

        if (this.txtFechaInicial.Date > DateTime.Now.Date)
        {
            this.lblFechaInicialMayor.Visible = true;

            if (this.txtFechaFinal.Date < this.txtFechaInicial.Date)
            {
                this.lblFechaFinalMayor.Visible = true;
            }
            else
            {
                this.lblFechaFinalMayor.Visible = false;
            }

            if ((txtFechaFinal.Date - txtFechaInicial.Date).Days > 365)
            {
                this.toolBar.MostrarMensajeError("El rango de las fechas debe ser menor o igual a un año");
            }

            if (this.ValidaRegionales())
            {
                this.lblCountRegionales.Visible = true;
            }
            else
            {
                this.lblCountRegionales.Visible = false;
            }

            return;
        }
        else
        {
            this.lblFechaInicialMayor.Visible = false;

            if (this.txtFechaFinal.Date < this.txtFechaInicial.Date)
            {
                this.lblFechaFinalMayor.Visible = true;
            }
            else
            {
                this.lblFechaFinalMayor.Visible = false;
            }
        }

        if ((txtFechaFinal.Date - txtFechaInicial.Date).Days > 365)
        {
            this.toolBar.MostrarMensajeError("El rango de las fechas debe ser menor o igual a un año");

            if (this.ValidaRegionales())
            {
                this.lblCountRegionales.Visible = true;
            }
            else
            {
                this.lblCountRegionales.Visible = false;
            }

            return;
        }
        else
        {
            if (this.ValidaRegionales())
            {
                this.lblCountRegionales.Visible = true;
                return;
            }
            else
            {
                this.lblCountRegionales.Visible = false;
            }
        }

        this.GenerarReporte();
    }

    /// <summary>
    /// Evento encargado de realizar el Conteo de Regionales
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbRegionales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            vIncReg++;

            foreach (ListItem item in ddlEstado.Items)
            {
                if (item.Selected)
                {
                    var existe = vlEstado.Find(x => x.Equals(item));
                    if (existe == 0 && vIncReg == 2)
                    {
                        vlEstado.Add(Convert.ToInt32(item.Value));
                    }
                }
            }

            this.toolBar.LipiarMensajeError();
            this.lbRegionales.Focus();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Evento para el manejo de cambio del Control
    /// </summary>
    /// <param name="sender">The ddlTipoReporte</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void ddlTipoReporte_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(this.ddlTipoReporte.SelectedValue) == 2)
        {
            //// Visualización del control Estado
            this.pnlEstado.Visible = true;
            this.rfvEstado.Enabled = true;

            //// Visualización del control Regional ListBox
            this.rfvRegionallb.Enabled = false;
            this.lblCountRegionales.Visible = false;
            this.lbRegionales.Visible = false;
            this.lbRegionales.ClearSelection();

            //// Visualización del control Regional DropDownList
            this.rfvRegional.Enabled = true;
            this.ddlRegional.Visible = true;
        }
        else
        {
            //// Visualización del control Estado
            this.pnlEstado.Visible = false;
            this.rfvEstado.Enabled = false;

            //// Visualización del control Regional ListBox
            this.rfvRegionallb.Enabled = true;
            this.lbRegionales.Visible = true;

            //// Visualización del control Regional DropDownList
            this.rfvRegional.Enabled = false;
            this.ddlRegional.Visible = false;
            this.ddlRegional.SelectedValue = "-1";
        }

        this.pnlFechas.Visible = true;
        this.rfvFechaInicial.Enabled = true;
        this.rfvFechaFinal.Enabled = true;
        this.ddlTipoReporte.Focus();
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Cargar Nombre del Reporte
    /// </summary>
    /// <param name="reportName">Report Name</param>
    /// <returns>Nombre Reporth Path</returns>
    private static string LoadReportName(string reportName)
    {
        string ReportPath = string.Empty;

        XmlDocument xDoc = new XmlDocument();

        xDoc.Load(HttpContext.Current.Server.MapPath("~/General/General/Report/Reports.xml"));

        XmlNodeList Reportes = xDoc.GetElementsByTagName("Reportes");

        XmlNodeList lista = ((XmlElement)Reportes[0]).GetElementsByTagName("reporte");

        foreach (XmlElement reporte in lista)
        {
            if (reporte.GetAttribute("nombre") == reportName)
            {
                ReportPath = reporte.GetAttribute("ruta");
                break;
            }
        }

        return ReportPath;
    }

    /// <summary>
    /// Método para carga los datos iniciales de la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);

            this.toolBar.EstablecerTitulos("Reportes Estad&iacute;sticos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para inicializar controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.ddlTipoReporte.Focus();

            List<EstadoDenuncia> vListaEstado = this.vMostrencosService.ConsultarEstadosDenunciasReportes();
            this.ddlEstado.DataSource = vListaEstado;
            this.ddlEstado.DataTextField = "NombreEstadoDenuncia";
            this.ddlEstado.DataValueField = "IdEstadoDenuncia";
            this.ddlEstado.DataBind();

            List<Regionales> vRegionales = this.vMostrencosService.ConsultarRegionales();
            this.lbRegionales.DataSource = vRegionales;
            this.lbRegionales.DataTextField = "NombreRegional";
            this.lbRegionales.DataValueField = "IdRegional";
            this.lbRegionales.DataBind();

            vRegionales.Insert(0, new Regionales() { NombreRegional = "SELECCIONE", IdRegional = -1 });
            this.ddlRegional.DataSource = vRegionales;
            this.ddlRegional.DataTextField = "NombreRegional";
            this.ddlRegional.DataValueField = "IdRegional";
            this.ddlRegional.DataBind();
            this.ddlRegional.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que valida la cantidada de regionales Seleccionadas
    /// </summary>
    /// <returns>Resultado Booleano</returns>
    private bool ValidaRegionales()
    {
        if (Convert.ToInt32(this.ddlTipoReporte.SelectedValue) != 2)
        {
            if (this.lbRegionales.GetSelectedIndices().Count() > 6)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Método que genera el reporte en el Visor
    /// </summary>
    private void GenerarReporte()
    {
        string nombreReporte = string.Empty;
        string vRegional = string.Empty; //// eliminar esto, ya no se usa, se debe eliminar del reporte
        string vIdRegional = string.Empty;
        string vIdEstado = string.Empty;
        string vFechaInicial = txtFechaInicial.Date.ToString("yyyy-MM-dd");
        string vFechaFinal = txtFechaFinal.Date.ToString("yyyy-MM-dd");

        if (Convert.ToInt32(this.ddlTipoReporte.SelectedValue) != 2)
        {
            foreach (ListItem vListItem in this.lbRegionales.Items)
            {
                if (vListItem.Selected)
                {
                    if (vListItem.Value == "-1")
                    {
                        break;
                    }
                    else
                    {
                        vIdRegional = vIdRegional == string.Empty ? vListItem.Value : vIdRegional + "," + vListItem.Value;
                    }
                }
            }
        }
        else
        {
            foreach (ListItem vListItem in this.ddlEstado.Items)
            {
                if (vListItem.Selected)
                {
                    if (vListItem.Value == "-1")
                    {
                        break;
                    }
                    else
                    {
                        vIdEstado = vIdEstado == string.Empty ? vListItem.Value : vIdEstado + "," + vListItem.Value;
                    }
                }
            }
        }

        try
        {
            switch (Convert.ToInt32(this.ddlTipoReporte.SelectedValue))
            {
                case 1:
                    nombreReporte = "ConsolidadoBienesAdjudicadosRegional";
                    break;
                case 2:
                    nombreReporte = "ConsolidadoRegionalEstadosProceso";
                    vRegional = ddlRegional.SelectedItem.Text; //// eliminar esto, ya no se usa, se debe eliminar del reporte
                    vIdRegional = this.ddlRegional.SelectedValue;
                    break;
                case 3:
                    nombreReporte = "ConsolidadoRegionalSentidoSentencia";
                    break;
                case 4:
                    nombreReporte = "ConsolidadoRegionalTipoTramite";
                    break;
                default:
                    break;
            }

            ////Crea un objeto de tipo reporte
            this.ObjReport = new Report(nombreReporte, false, vPageName, nombreReporte);

            if (Convert.ToInt32(this.ddlTipoReporte.SelectedValue) != 2)
            {
                ////Adiciona los parametros x Opción al objeto reporte
                this.ObjReport.AddParameter("IdRegional", vIdRegional);
            }
            else
            {
                ////Adiciona los parametros x Opción al objeto reporte
                this.ObjReport.AddParameter("Regional", vRegional); //// eliminar esto, ya no se usa, se debe eliminar del reporte
                this.ObjReport.AddParameter("IdRegional", vIdRegional);
                this.ObjReport.AddParameter("IdEstado", vIdEstado);
            }

            ////Adiciona los parametros genericos al objeto reporte
            this.ObjReport.AddParameter("FechaInicial", vFechaInicial);
            this.ObjReport.AddParameter("FechaFinal", vFechaFinal);

            this.ShowReport();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Mostrar Reporte
    /// </summary>
    private void ShowReport()
    {
        try
        {
            string vReportServerUrl = ConfigurationManager.AppSettings["ReportServerUrl"];
            string vReportPath = ConfigurationManager.AppSettings["ReportPath"];
            string vUserName = ConfigurationManager.AppSettings["UserName"];
            string vPassword = ConfigurationManager.AppSettings["Password"];
            string vDomainName = ConfigurationManager.AppSettings["DomainName"];

            string Reportpath = LoadReportName(ObjReport.ReportName);

            ////set the url for the reporting service web service
            rviReportes.ServerReport.ReportServerUrl = new System.Uri(vReportServerUrl);

            ////gets, checks and assign the report path for the reports
            if (vReportPath != string.Empty)
            {
                rviReportes.ServerReport.ReportPath = "/" + vReportPath + "/" + Reportpath;
            }
            else
            {
                rviReportes.ServerReport.ReportPath = "/" + Reportpath;
            }

            ////set the report credentias
            rviReportes.ServerReport.ReportServerCredentials = new CustomReportCredentials(vUserName, vPassword, vDomainName);

            ////set the Processing Mode
            rviReportes.ProcessingMode = ProcessingMode.Remote;

            ////set the Processing Mode
            rviReportes.ServerReport.Refresh();

            ////set the ShowExportControls Option
            rviReportes.ShowExportControls = ObjReport.ShowExportControls;

            ////set Porcentaje de Zoom predefinido
            rviReportes.ZoomPercent = 100;

            ////Set Navegación entre páginas
            rviReportes.ShowPageNavigationControls = false;

            ////Set Reporte Habilitado
            rviReportes.Enabled = true;

            ////Set Reporte Visible 
            rviReportes.Visible = true;

            ////Set Show Control Zoom
            rviReportes.ShowZoomControl = false;
            rviReportes.ShowFindControls = false;

            ////Set Show Control Print
            rviReportes.ShowPrintButton = false;

            ////Set Show Control Print
            rviReportes.ShowRefreshButton = false;

            rviReportes.ShowBackButton = false;
            rviReportes.ShowToolBar = false;

            ////If there are parameter we procced to pass them to the report viewer
            if (ObjReport.GetParemeters().Count > 0)
            {
                rviReportes.ServerReport.SetParameters(ObjReport.GetParemeters());
                ////Clear all the parameter from the report object
                ObjReport.ClearParameters();
            }

            this.toolBar.LipiarMensajeError();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}