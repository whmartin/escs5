﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_DocumentosSoporteDenuncia_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Nombre del documento
                        </td>
                        <td>Descripción del documento
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td style="width:50%">
                            <asp:TextBox runat="server" ID="txtNombre" MaxLength="128" Width="300" Enabled="false"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />

                        </td>
                        <td>
                           <asp:TextBox runat="server" ID="txtDescripcion"  MaxLength="512" TextMode="MultiLine" Enabled="false" Rows="8" Width="80%" Height="70px" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Estado del documento *
                <asp:Label ID="lblRequeridoEstado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td style="width:50%">
                            <asp:RadioButtonList ID="rblEstado" Enabled="false" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="ACTIVO">Activo</asp:ListItem>
                                <asp:ListItem Value="INACTIVO">Inactivo</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


