﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_DocumentosSoporteDenuncia_List
/// </summary>
public partial class Page_Mostrencos_DocumentosSoporteDenuncia_List : GeneralWeb
{
    /// <summary>
    /// Declaracion Toolbar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/DocumentosSoporteDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// preinicia la pagina
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Carga la pagina
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        this.txtNombre.Focus();
        this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage);
        this.toolBar.LipiarMensajeError();
    }

    /// <summary>
    /// evento del boton Buscar
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// evento del boton nuevo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Busca la informacion que coisida con los parámetros de busqueda
    /// </summary>
    private void Buscar()
    {
        bool vEsrequerido = false;
        try
        {
            if (string.IsNullOrEmpty(this.rblEstado.SelectedValue))
            {
                this.lblRequeridoEstado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoEstado.Visible = false;
                vEsrequerido = false;
            }

            List<TipoDocumentoBienDenunciado> vListaTipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciado(this.txtNombre.Text.Trim(), this.rblEstado.SelectedValue);
            this.ViewState["SortedgvDocumentodSoporteDenuncia"] = vListaTipoDocumentoBienDenunciado;
            this.gvDocumentodSoporteDenuncia.DataSource = vListaTipoDocumentoBienDenunciado;
            this.gvDocumentodSoporteDenuncia.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);

            this.toolBar.EstablecerTitulos("Documentos soporte de la denuncia","List");

            this.gvDocumentodSoporteDenuncia.PageSize = this.PageSize();
            this.gvDocumentodSoporteDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la información de la fila seleccionada en la grilla
    /// </summary>
    /// <param name="pRow">fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvDocumentodSoporteDenuncia.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.IdTipoDocumentoBienDenunciado", strValue);
            this.NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvDocumentodSoporteDenuncia_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvDocumentodSoporteDenuncia.SelectedRow);
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvDocumentodSoporteDenuncia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvDocumentodSoporteDenuncia.PageIndex = e.NewPageIndex;
        this.gvDocumentodSoporteDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvDocumentodSoporteDenuncia"] != null)
        {
            List<TipoDocumentoBienDenunciado> vList = (List<TipoDocumentoBienDenunciado>)this.ViewState["SortedgvDocumentodSoporteDenuncia"];
            this.gvDocumentodSoporteDenuncia.DataSource = vList;
            this.gvDocumentodSoporteDenuncia.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }    

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvDocumentodSoporteDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<TipoDocumentoBienDenunciado> vList = new List<TipoDocumentoBienDenunciado>();
        List<TipoDocumentoBienDenunciado> vResult = new List<TipoDocumentoBienDenunciado>();

        if (this.ViewState["SortedgvDocumentodSoporteDenuncia"] != null)
        {
            vList = (List<TipoDocumentoBienDenunciado>)this.ViewState["SortedgvDocumentodSoporteDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "Nombre":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
                }

                break;

            case "Descripcion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.DescripcionDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.DescripcionDocumento).ToList();
                }

                break;

            case "Estado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Estado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Estado).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.ViewState["SortedgvDocumentodSoporteDenuncia"] = vResult;
        this.gvDocumentodSoporteDenuncia.DataSource = vResult;
        this.gvDocumentodSoporteDenuncia.DataBind();
    }
}