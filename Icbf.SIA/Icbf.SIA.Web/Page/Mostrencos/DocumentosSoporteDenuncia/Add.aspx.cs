﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_DocumentosSoporteDenuncia_Add
/// </summary>
public partial class Page_Mostrencos_DocumentosSoporteDenuncia_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/DocumentosSoporteDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDocumentosSoporteDenuncia;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Detail);
    }
    
    /// <summary>
    /// evento del boton guardar
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }
    
    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Documentos soporte de la denuncia","Add");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// guarda la informacion en la base de datos
    /// </summary>
    private void Guardar()
    {
        bool vEsrequerido = false;
        try
        {
            if (string.IsNullOrEmpty(this.rblEstado.SelectedValue))
            {
                this.lblRequeridoEstado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoEstado.Visible = false;
                vEsrequerido = false;
            }

            if (string.IsNullOrEmpty(this.txtNombre.Text.Trim()))
            {
                this.lblRequeridoNombre.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoNombre.Visible = false;
                vEsrequerido = false;
            }

            if (this.txtDescripcion.Text.Length > 512)
            {
                this.lblCantidadMaximaCaracteres.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblCantidadMaximaCaracteres.Visible = false;
                vEsrequerido = false;
            }

            int vResultado;
            TipoDocumentoBienDenunciado vDocumentosSoporteDenuncia = new TipoDocumentoBienDenunciado();
            vDocumentosSoporteDenuncia.NombreTipoDocumento = this.txtNombre.Text.Trim().ToUpper();
            vDocumentosSoporteDenuncia.DescripcionDocumento = this.txtDescripcion.Text.Trim().ToUpper();
            vDocumentosSoporteDenuncia.Estado = this.rblEstado.SelectedValue.ToUpper();
            vDocumentosSoporteDenuncia.UsuarioCrea = this.GetSessionUser().NombreUsuario;
            this.InformacionAudioria(vDocumentosSoporteDenuncia, this.PageName, SolutionPage.Add);
            vResultado = this.vMostrencosService.InsertarTipoDocumentoBienDenunciado(vDocumentosSoporteDenuncia);
            if (vResultado == 0)
            {
                this.toolBar.MostrarMensajeError("No fue posible agregar el documento. Por favor inténtelo nuevamente.");
            }
            else
            {
                this.SetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.IdTipoDocumentoBienDenunciado", vResultado);
                this.SetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.Mensaje", "La información ha sido guardada exitosamente");
                this.NavigateTo(SolutionPage.Detail);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }
    
    #endregion
}