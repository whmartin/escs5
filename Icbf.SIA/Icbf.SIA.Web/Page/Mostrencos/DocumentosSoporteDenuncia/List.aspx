﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_DocumentosSoporteDenuncia_List" %>

<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<%--<%@ Register Src="~/General/General/Control/fechaFestivos.ascx" TagPrefix="uc1" TagName="fecha" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Nombre del documento
                        </td>
                        <td>Estado del documento *
                <asp:Label ID="lblRequeridoEstado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombre" MaxLength="128" Width="300"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />

                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblEstado" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="ACTIVO" Selected="True">Activo</asp:ListItem>
                                <asp:ListItem Value="INACTIVO">Inactivo</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDocumentodSoporteDenuncia" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoDocumentoBienDenunciado" CellPadding="0" Height="16px" OnSorting="gvDocumentodSoporteDenuncia_OnSorting"
                        OnPageIndexChanging="gvDocumentodSoporteDenuncia_PageIndexChanging" OnSelectedIndexChanged="gvDocumentodSoporteDenuncia_SelectedIndexChanged" EmptyDataText="No se encontraron resultados, verifique por favor">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre documento" DataField="NombreTipoDocumento" SortExpression="Nombre" />
                            <asp:BoundField HeaderText="Descripción documento" DataField="DescripcionDocumento" SortExpression="Descripcion" />
                            <asp:BoundField HeaderText="Estado documento" DataField="Estado" SortExpression="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
