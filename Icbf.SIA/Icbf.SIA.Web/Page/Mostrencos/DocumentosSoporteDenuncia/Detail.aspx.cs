﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_DocumentosSoporteDenuncia_Detail
/// </summary>
public partial class Page_Mostrencos_DocumentosSoporteDenuncia_Detail : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/DocumentosSoporteDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDocumentosSoporteDenuncia;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// evento del boton nuevo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento del boton eliminar
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        try
        {
            int vResultado;
            this.vIdDocumentosSoporteDenuncia = Convert.ToInt32(this.GetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.IdTipoDocumentoBienDenunciado"));
            TipoDocumentoBienDenunciado vDocumentosSoporteDenuncia = vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(this.vIdDocumentosSoporteDenuncia);
            vResultado = this.vMostrencosService.EliminarTipoDocumentoBienDenunciado(vDocumentosSoporteDenuncia);
            if (vResultado > 0)
            {
                this.toolBar.MostrarMensajeEliminado();
                this.toolBar.MostrarBotonEditar(false);
                this.toolBar.MostrarBotonEliminar(false);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoEliminar += new ToolBarDelegate(this.btnEliminar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.EstablecerTitulos("Documentos soporte de la denuncia","Detail");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.vIdDocumentosSoporteDenuncia = Convert.ToInt32(this.GetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.IdTipoDocumentoBienDenunciado"));
            if (this.vIdDocumentosSoporteDenuncia != 0)
            {
                TipoDocumentoBienDenunciado vDocumentosSoporteDenuncia = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(this.vIdDocumentosSoporteDenuncia);
                this.txtNombre.Text = !string.IsNullOrEmpty(vDocumentosSoporteDenuncia.NombreTipoDocumento) ? vDocumentosSoporteDenuncia.NombreTipoDocumento : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vDocumentosSoporteDenuncia.DescripcionDocumento) ? vDocumentosSoporteDenuncia.DescripcionDocumento.ToUpper() : string.Empty;
                this.rblEstado.SelectedValue = !string.IsNullOrEmpty(vDocumentosSoporteDenuncia.Estado) ? vDocumentosSoporteDenuncia.Estado : string.Empty;
            }
            
            string vMensaje = Convert.ToString(this.GetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.Mensaje"));
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.DocumentosSoporteDenuncia.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
}