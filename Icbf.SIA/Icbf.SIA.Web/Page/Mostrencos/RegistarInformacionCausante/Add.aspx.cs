﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_InformacionCausante_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionCausante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlPopUpHistorico.CssClass = "popuphIstorico hidden";
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            this.toolBar.LipiarMensajeError();
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Metodo para abrir el popup historico
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnHistorico_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAgregar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.CapturarValoresDocumentacion();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!this.ExistenCamposCausanteRequeridos(false))
            {
                Tercero vTercero = this.vMostrencosService.ConsultarTercero(Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue), this.txtNumeroIdentificacionCausante.Text, this.txtTipoIdentificacioncausante.Text);
                if (vTercero.IdTercero != 0)
                {
                    this.hfIdTercero.Value = vTercero.IdTercero.ToString();
                    this.txtPrimerNombreCausante.Text = vTercero.PrimerNombre;
                    this.txtSegundoNombreCausante.Text = vTercero.SegundoNombre;
                    this.txtPrimerApellidoCausante.Text = vTercero.PrimerApellido;
                    this.txtSegundoApellidoCausante.Text = vTercero.SegundoApellido;
                    this.toolBar.LipiarMensajeError();
                }
                else
                {
                    this.hfIdTercero.Value = string.Empty;
                    this.txtPrimerNombreCausante.Text = string.Empty;
                    this.txtSegundoNombreCausante.Text = string.Empty;
                    this.txtPrimerApellidoCausante.Text = string.Empty;
                    this.txtSegundoApellidoCausante.Text = string.Empty;
                    this.toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Guardar();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
            this.gvwDocumentacionRecibida.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla RowCommand
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vLista.Remove(vDocumento);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vLista;
                this.CargarGrilla();
            }
            if (e.CommandName.Equals("Editar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                this.LimpiarCampos();
                this.CargarDatosDocumentoSolocitado(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }

                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante).IdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();
        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }
        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "IdFase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdFase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdFase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdActuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdActuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdAccion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdAccion).ToList();
                }

                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }
                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Información del Causante", "Add");
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
            this.gvwHistoricoDenuncia.PageSize = this.PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            }
            this.CargarListaDesplegable();
            this.CargarGrilla();
            this.CargarGrillaHistorico(vIdDenunciaBien);
            this.ddlTipoIdentificacion.Focus();
            this.hfIdDocumentoSolocitado.Value = "0";
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// Guarda la informacion del causante
    /// </summary>
    private void Guardar()
    {
        int vResultado = 0;
        try
        {
            this.toolBar.LipiarMensajeError();
            Causantes vCausante = this.CapturarValoresCausante(); ;
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vCausante != null)
            {
                if (vListaDocumentos.Count == 0)
                {
                    this.toolBar.MostrarMensajeError("No se ha agregado ningún documento solicitado");
                }
                else
                {
                    this.InformacionAudioria(vCausante, this.PageName, SolutionPage.Add);

                    if (this.txtNumeroIdentificacionCausante.Text != string.Empty)
                    {
                        if (this.txtNumeroIdentificacion.Text == this.txtNumeroIdentificacionCausante.Text)
                        {
                            this.toolBar.MostrarMensajeError("El causante no puede ser el mismo denunciante");
                        }
                        else
                        {
                            vResultado = this.vMostrencosService.InsertarCausante(vCausante);
                            if (vResultado > 0)
                            {
                                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                                {
                                    item.IdCausante = vResultado;
                                    item.IdDenunciaBien = vCausante.IdDenunciaBien;
                                    item.IdApoderado = null;
                                    this.InformacionAudioria(item, this.PageName, SolutionPage.Add);
                                    this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                                }

                                this.SetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante", vResultado);
                                this.SetSessionParameter("Mostrencos.RegistrarInformacionCausante.Mensaje", "La información ha sido guardada exitosamente");
                                this.NavigateTo(SolutionPage.Detail);
                            }
                            else
                            {
                                this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la infortación del causante
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (!ExistenCamposDocumentacionRequeridos())
            {
                if (vListaDocumentacion.Count > 0)
                {
                    if (this.hfIdDocumentoSolocitado.Value != "0")
                    {
                        vListaDocumentacion.Remove(vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == Convert.ToInt32(this.hfIdDocumentoSolocitado.Value)).First());
                    }
                    foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                    {
                        if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue))
                        {
                            this.toolBar.MostrarMensajeError("El Registro ya existe");
                            vExiste = true;
                            break;
                        }
                    }
                }
                if (!vExiste)
                {
                    vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                    vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                    vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue);
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                    vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObserbacionesDocumentoSolicitado.Text.Trim().ToUpper();
                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                    vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    vListaDocumentacion.Add(vDocumentacionSolicitada);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                    this.CargarGrilla();
                    this.ddlDocumentoSoporte.SelectedValue = "-1";
                    this.FechaSolicitud.InitNull = true;
                    this.txtObserbacionesDocumentoSolicitado.Text = string.Empty;
                    this.LimpiarCampos();
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la información del causante
    /// </summary>
    /// <returns>variable de tipo Causantes con la información capturada</returns>
    private Causantes CapturarValoresCausante()
    {
        try
        {
            Causantes vCausante = new Causantes();
            if (!this.ExistenCamposCausanteRequeridos(true))
            {
                int vIdTercero = Convert.ToInt32(this.hfIdTercero.Value);
                int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
                if (this.vMostrencosService.ValidarDuplicidadCausante(vIdTercero, vIdDenunciaBien))
                {
                    this.toolBar.MostrarMensajeError("El registro ya existe");
                    return null;
                }
                else
                {
                    vCausante.IdDenunciaBien = vIdDenunciaBien;
                    vCausante.IdTercero = vIdTercero;
                    vCausante.FechaDefuncion = this.FechaDefuncion.Date;
                    vCausante.NumeroRegistroDefuncion = this.txtNumeroRegistroDefuncion.Text.Trim();
                    vCausante.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    vCausante.IdUsuarioCrea = this.GetSessionUser().IdUsuario;
                    return vCausante;
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposDocumentacionRequeridos()
    {
        bool vEsrequerido = false;
        int error = 0;
        try
        {
            if (this.ddlDocumentoSoporte.SelectedValue.Equals("-1"))
            {
                this.lblrequeridoDocumentoSoporte.Visible = true;
                error += 1;
            }
            else
            {
                this.lblrequeridoDocumentoSoporte.Visible = false;
            }
            try
            {
                DateTime vFecha = Convert.ToDateTime(this.FechaSolicitud.Date);
                this.lblFechaInvalida.Visible = false;
            }
            catch (Exception)
            {
                this.lblFechaInvalida.Visible = true;
                error += 1;
            }
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrequeridoFechaSolicitud.Visible = true;
                error += 1;
            }
            else
            {
                this.lblrequeridoFechaSolicitud.Visible = false;
            }
            if (this.txtObserbacionesDocumentoSolicitado.Text.Length > 512)
            {
                this.lblCantidadCaracteresSolicitado.Visible = true;
                error += 1;
            }
            else
            {
                this.lblCantidadCaracteresSolicitado.Visible = false;
            }
            if (error == 0)
            {
                vEsrequerido = false;
            }
            else
            {
                vEsrequerido = true;
            }
            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// limpia los campos
    /// </summary>
    private void LimpiarCampos()
    {
        try
        {
            this.lblCantidadCaracteresSolicitado.Visible = false;
            this.lblrequeridoDocumentoSoporte.Visible = false;
            this.lblRequeridoFechaDefuncion.Visible = false;
            this.lblRequeridoFechaRecibido.Visible = false;
            this.lblRequeridoNombreArchivo.Visible = false;
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            this.txtObserbacionesDocumentoSolicitado.Text = string.Empty;
            this.rbtEstadoDocumento.ClearSelection();
            this.txtObservacionesRecibido.Text = string.Empty;
            this.FechaSolicitud.InitNull = true;
            this.hfIdDocumentoSolocitado.Value = "0";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposCausanteRequeridos(bool pEsGuardar)
    {
        bool vEsrequerido = false;
        int error = 0;
        try
        {
            if (this.ddlTipoIdentificacion.SelectedValue.Equals("-1"))
            {
                this.lblRequeridoTipoIdent.Visible = true;
                error += 1;
            }
            else
            {
                this.lblRequeridoTipoIdent.Visible = false;
            }

            if (string.IsNullOrEmpty(this.txtNumeroIdentificacionCausante.Text))
            {
                this.lblRequeridoNumIdent.Visible = true;
                error += 1;
            }
            else
            {
                this.lblRequeridoNumIdent.Visible = false;
            }

            if (pEsGuardar)
            {
                this.lblRequeridoNumIdent.Visible = false;
                this.lblRequeridoTipoIdent.Visible = false;
                this.lblCantidadCaracteresSolicitado.Visible = false;
                this.lblrequeridoFechaSolicitud.Visible = false;
                this.lblrequeridoDocumentoSoporte.Visible = false;
                this.lblFechaInvalida.Visible = false;

                if (string.IsNullOrEmpty(this.txtNumeroRegistroDefuncion.Text))
                {
                    this.lblRequeridoNumRegistroDefuncion.Text = "Campo Requerido";
                    this.lblRequeridoNumRegistroDefuncion.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridoNumRegistroDefuncion.Visible = false;
                }

                this.txtNumeroRegistroDefuncion.Text = (this.txtNumeroRegistroDefuncion.Text == string.Empty) ? "0" : this.txtNumeroRegistroDefuncion.Text;

                if (Convert.ToInt32(this.txtNumeroRegistroDefuncion.Text) == 0)
                {
                    this.lblRequeridoNumRegistroDefuncion.Text = "Número de Registro de Defunción debe ser diferente de cero";
                    this.lblRequeridoNumRegistroDefuncion.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridoNumRegistroDefuncion.Visible = false;
                }

                try
                {
                    DateTime vFecha = Convert.ToDateTime(this.FechaDefuncion.Date);
                    this.lblFechaDefuncionInvalida.Visible = false;
                }
                catch (Exception)
                {
                    this.lblFechaDefuncionInvalida.Visible = true;
                    error += 1;
                }

                if (this.FechaDefuncion.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblRequeridoFechaDefuncion.Text = "Campo Requerido";
                    this.lblRequeridoFechaDefuncion.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridoFechaDefuncion.Visible = false;

                    if (this.FechaDefuncion.Date > DateTime.Now)
                    {
                        this.lblRequeridoFechaDefuncion.Text = "La fecha de defunción no puede ser mayor a la fecha actual";
                        this.lblRequeridoFechaDefuncion.Visible = true;
                        error += 1;
                    }
                    else
                    {
                        this.lblRequeridoFechaDefuncion.Visible = false;
                    }
                }



                if (string.IsNullOrEmpty(this.hfIdTercero.Value.ToString()))
                {
                    this.btnBuscar_Click(new Object(), new ImageClickEventArgs(1, 1));
                    error += 1;
                }
            }

            if (error == 0)
            {
                vEsrequerido = false;
            }
            else
            {
                vEsrequerido = true;
            }

            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// carga los datos de los documentos solicitados 
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void CargarDatosDocumentoSolocitado(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            this.hfIdDocumentoSolocitado.Value = pDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            this.ddlDocumentoSoporte.SelectedValue = pDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
            this.FechaSolicitud.Date = Convert.ToDateTime(pDocumentoSolicitado.FechaSolicitud);
            this.txtObserbacionesDocumentoSolicitado.Text = pDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion
}