﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_InformacionCausante_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>            
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *              
                </td>
                <td>Fecha radicado en correspondencia *             
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de identificación *
                </td>
                <td>Número identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td >Primer nombre *
                </td>
                <td >Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td >Primer apellido *
                </td>
                <td >Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">Descripción de la denuncia</td>
                <td style="width: 50%">Histórico de la denuncia
                 <a class="btnPopUpHistorico" style="width: 16px; height: 16px;">
                        <img alt="h" src="../../../Image/btn/info.jpg" style="width: 20px; height: 20px;"> </img>
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="700px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel4">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del causante
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Tipo de identificación
                    <asp:Label ID="lblRequeridoTipoIdent" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>


                </td>
                <td>Número identificación
                    <asp:Label ID="lblRequeridoNumIdent" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>

                </td>

            </tr>
            <tr class="rowA">
                <td>

                    <asp:DropDownList ID="ddlTipoIdentificacion" Enabled="false" runat="server" Width="250px"></asp:DropDownList></td>
                </td>
                <td>

                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionCausante" Enabled="false" MaxLength="20" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacionCausante" runat="server" TargetControlID="txtNumeroIdentificacionCausante"
                        FilterType="Numbers" />
            </tr>
            <tr class="rowB">
                <td>Tipo de persona </td>
                <td>Primer nombre              
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoPersonacausante" MaxLength="20" Text="NATURAL" Width="250px" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombreCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Segundo nombre
                </td>
                <td>Primer apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombreCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellidoCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Segundo apellido
                </td>
                <td>Número de registro de defunción
                    <asp:Label ID="lblRequeridoNumRegistroDefuncion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellidoCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroRegistroDefuncion" Enabled="false" MaxLength="16" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroRegistroDefuncion"
                        FilterType="Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha de defunción
                    <asp:Label ID="lblRequeridoFechaDefuncion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td></td>
            </tr>

            <tr class="rowA">
                <td>
                    <uc1:fecha runat="server" ID="FechaDefuncion" Enabled="false" Requerid="false" Width="250px" />
                </td>
                <td></td>
            </tr>
        </table>
        <asp:HiddenField ID="hfIdTercero" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="tdTitulos" colspan="3">Documentación Solicitada
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td>Documento soporte de la denuncia *
                    <asp:Label ID="lblrequeridoDocumentoSoporte" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td >Fecha de solicitud *
                    <asp:Label ID="lblrequeridoFechaSolicitud" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td style="width: 46%">
                    <asp:DropDownList ID="ddlDocumentoSoporte" Enabled="false" runat="server" Height="16px" Width="250px"></asp:DropDownList></td>
                <td>
                    <uc1:fecha runat="server" ID="FechaSolicitud" Requerid="false" Enabled="false" Width="250px" />
                </td>               
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento solicitado
                    <asp:Label ID="lblCantidadCaracteresSolicitado" runat="server" Text="Debe ingresar hasta un máximo de 512 caracteres" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObserbacionesDocumentoSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" AllowSorting="True"
                        OnSorting="gvwDocumentacionRecibida_OnSorting"
                        GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px"        >                
                        <Columns>
                            <asp:BoundField HeaderText="Tipo de documento" DataField="TipoDocumentoBienDenunciado.NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" />
                            <asp:BoundField HeaderText="Estado del documento" DataField="EstadoDocumento.NombreEstadoDocumento" SortExpression="NombreEstadoDocumento" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" DataFormatString="{0:d}" SortExpression="FechaRecibido" />
                            <asp:BoundField HeaderText="Nombre de archivo" DataField="NombreArchivo" SortExpression="NombreArchivo" />
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" />                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>    
</asp:Content>