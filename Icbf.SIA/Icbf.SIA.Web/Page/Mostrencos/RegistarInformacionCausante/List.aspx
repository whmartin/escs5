﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/General/General/Master/main2.master"  CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_InformacionCausante_List" %>

<%@ Register Src="~/General/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">   

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *              
                </td>
                <td>Fecha radicado en correspondencia *             
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de identificación *
                </td>
                <td>Número identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td >Primer nombre *
                </td>
                <td >Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td >Primer apellido *
                </td>
                <td >Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">Descripción de la denuncia</td>
                <td style="width: 50%">Histórico de la denuncia
                 <a class="btnPopUpHistorico" style="width: 16px; height: 16px;">
                        <img alt="h" src="../../../Image/btn/info.jpg" style="width: 20px; height: 20px;"> </img>
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="700px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>


    <asp:Panel runat="server" ID="pnlGridCausantes">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvCausantes" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="0" DataKeyNames="IdCausante" GridLines="None" Height="16px" OnPageIndexChanging="gvCausantes_OnPageIndexChanging"
                        OnSelectedIndexChanged="gvCausantes_SelectedIndexChanged" OnSorting="gvCausantes_OnSorting" PageSize="10" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" Height="16px" ImageUrl="~/Image/btn/info.jpg" ToolTip="Detalle" Width="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Tercero.TiposDocumentosGlobal.NomTipoDocumento" HeaderText="Tipo de identificación" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField DataField="Tercero.NumeroIdentificacion" HeaderText="Número de identificación" SortExpression="NumeroIdentificacion" />
                            <asp:BoundField DataField="NombreMostrar" HeaderText="Nombre causante" SortExpression="NombreCausante" />
                            <asp:BoundField DataField="NumeroRegistroDefuncion" HeaderText="Número registro defunción" SortExpression="NumeroRegistroDefuncion" />
                            <asp:BoundField DataField="FechaDefuncion" HeaderText="Fecha defunción" SortExpression="FechaDefuncion" DataFormatString="{0:d}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
        </table>
    </asp:Panel>



</asp:Content>
