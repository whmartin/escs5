﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_InformacionCausante_List : GeneralWeb
{
    #region Declaración Variables
    /// <summary>
    /// The tool bar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// The page name
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionCausante";

    /// <summary>
    /// The v mostrencos service
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// The v identifier denuncia bien/
    /// </summary>
    private int vIdDenunciaBien;
    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #endregion

    #region "Eventos"

    /// <summary>
    /// Page PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvCausantes_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvCausantes.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvCausantes"] != null)
        {
            List<Causantes> vList = (List<Causantes>)this.ViewState["SortedgvCausantes"];
            gvCausantes.DataSource = vList;
            gvCausantes.DataBind();
        }
        else
        {
            this.CargarGrilla();
        }
    }

    /// <summary>
    /// Metodo quese encarga de organizar el orden delos registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvCausantes_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<Causantes> vList = new List<Causantes>();
        List<Causantes> vResult = new List<Causantes>();

        if (this.ViewState["SortedgvCausantes"] != null)
        {
            vList = (List<Causantes>)this.ViewState["SortedgvCausantes"];
        }

        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Tercero.TiposDocumentosGlobal.NomTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Tercero.TiposDocumentosGlobal.NomTipoDocumento).ToList();
                }

                break;

            case "NumeroIdentificacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Tercero.NumeroIdentificacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Tercero.NumeroIdentificacion).ToList();
                }

                break;

            case "NombreCausante":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreMostrar).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreMostrar).ToList();
                }

                break;

            case "NumeroRegistroDefuncion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NumeroRegistroDefuncion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NumeroRegistroDefuncion).ToList();
                }

                break;

            case "FechaDefuncion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaDefuncion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaDefuncion).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }

        ViewState["SortedgvCausantes"] = vResult;
        this.gvCausantes.DataSource = vResult;
        this.gvCausantes.DataBind();
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvCausantes_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvCausantes.SelectedRow);
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Iniciars this instance.
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            toolBar.EstablecerTitulos("Información del Causante", "List");
            this.gvCausantes.PageSize = this.PageSize();
            this.gvCausantes.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the datos iniciales.
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;

            if (vDenuncia.IdEstadoDenuncia == 11 || vDenuncia.IdEstadoDenuncia == 13 || vDenuncia.IdEstadoDenuncia == 19)
            {
                this.toolBar.OcultarBotonNuevo(true);
                this.toolBar.MostrarBotonEditar(false);
            }

            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ?
                (vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001 00:00:00") ?
                vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }

            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            this.CargarGrilla();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    public void CargarGrilla()
    {
        try
        {
            this.pnlGridCausantes.Visible = true;
            List<Causantes> vListaCausantes = this.vMostrencosService.ConsultarCausantesPorIdDenunciaBien(this.vIdDenunciaBien);
            if (vListaCausantes.Count > 0)
            {
                for (int i = 0; i < vListaCausantes.Count(); i++)
                {
                    vListaCausantes[i] = this.vMostrencosService.ConsultarInfirmacionCausantes(vListaCausantes[i]);
                }
                vListaCausantes = vListaCausantes.OrderBy(p => p.NombreMostrar).ToList();
                ViewState["SortedgvCausantes"] = vListaCausantes;
                this.gvCausantes.DataSource = vListaCausantes;
                this.gvCausantes.DataBind();
            }
            else
            {
                this.pnlGridCausantes.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.pnlGridCausantes.Visible = false;
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// guarda datos y los envia al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvCausantes.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

}