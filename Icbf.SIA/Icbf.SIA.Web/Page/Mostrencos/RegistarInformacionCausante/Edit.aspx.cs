﻿//-----------------------------------------------------------------------
// <copyright file="Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Edit.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;


public partial class Page_Mostrencos_InformacionCausante_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionCausante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlPopUpHistorico.CssClass = "popuphIstorico hidden";
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Edit))
        {
            this.toolBar.LipiarMensajeError();
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.CapturarValoresDocumentacion();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Guardar();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
            this.gvwDocumentacionRecibida.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                if (vDocumentoSolicitado.EsNuevo)
                {
                    vListaDocumento.Remove(vDocumentoSolicitado);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                }
                else
                {
                    List<DocumentosSolicitadosDenunciaBien> vDocumentosEliminar = new List<DocumentosSolicitadosDenunciaBien>();
                    vListaDocumento.Remove(vDocumentoSolicitado);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                    vDocumentosEliminar.Add(vDocumentoSolicitado);
                    this.ViewState["DocumentosEliminar"] = vDocumentosEliminar;
                }
                this.CargarGrillaDocumentos();
                this.LimpiarCampos();
                this.FecharRecibido.Date = DateTime.Now.Date;
            }
            if (e.CommandName.Equals("Editar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                this.LimpiarCampos();
                this.CargarDatosDocumentoSolocitado(vDocumentoSolicitado);
                this.CargarDatosDocumentoRecibido(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante).IdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// rbtEstadoDocumento_SelectedIndexChanged
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.lblAsterFecha.Visible = true;
        this.lblAsterNombre.Visible = true;
        this.lblAsterObservacion.Visible = true;
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();
        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }
        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.IdFase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.IdFase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.IdActuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.IdActuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.IdAccion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.IdAccion).ToList();
                }

                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }

                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Información del Causante", "Add");
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
            this.toolBar.ConfirmarAccion("btnElininar", "¿Está seguro de eliminar la información?");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
            Causantes vCausante = this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante);
            if (vCausante.IdDenunciaBien != 0)
            {
                this.hfIdDocumentoSolocitado.Value = "0";
                this.CargarListaDesplegable();
                this.CargarDatosDenuncia(vCausante);
                this.CargarDatosCausante(vCausante);
                this.CargarGrillaDocumentos();
                this.CargarGrillaHistorico(vCausante.IdDenunciaBien);
                this.FecharRecibido.Date = DateTime.Now;
            }
            this.txtNumeroRegistroDefuncion.Focus();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                Causantes vCausante = this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante);
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(vIdCausante);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }
                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Carga los datos del causante
    /// </summary>
    /// <param name="pCausante">Información del causante</param>
    private void CargarDatosCausante(Causantes pCausante)
    {
        try
        {
            pCausante = this.vMostrencosService.ConsultarInfirmacionCausantes(pCausante);
            this.txtNumeroIdentificacionCausante.Text = pCausante.Tercero.NumeroIdentificacion;
            this.txtTipoPersonacausante.Text = pCausante.Tercero.NombreTipoPersona;
            this.txtPrimerNombreCausante.Text = pCausante.Tercero.PrimerNombre;
            this.txtSegundoNombreCausante.Text = pCausante.Tercero.SegundoNombre;
            this.txtPrimerApellidoCausante.Text = pCausante.Tercero.PrimerApellido;
            this.txtSegundoApellidoCausante.Text = pCausante.Tercero.SegundoApellido;
            this.txtNumeroRegistroDefuncion.Text = pCausante.NumeroRegistroDefuncion;
            this.ddlTipoIdentificacion.SelectedValue = pCausante.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString();
            this.FechaDefuncion.Date = pCausante.FechaDefuncion;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los datos de la denuncia
    /// </summary>
    /// <param name="pCausante">Información del causante</param>
    private void CargarDatosDenuncia(Causantes pCausante)
    {
        try
        {
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(pCausante.IdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(pCausante.IdDenunciaBien).IdEstadoDenuncia).NombreEstadoDenuncia;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.Equals("Archivado") || vRegistroDenuncia.NombreEstadoDenuncia.Equals("Anulado"))
            {
                this.toolBar.MostrarBotonNuevo(false);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información del la lista desplegable
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la información del caudante
    /// </summary>
    private void Guardar()
    {
        try
        {
            string vRuta = string.Empty;
            int vResultado;
            this.FecharRecibido.Date = DateTime.Now.Date;
            if (this.rbtEstadoDocumento.SelectedIndex >= 0)
            {
                this.CapturarValoresDocumentacion();
            }
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            Causantes vCausante = this.CapturarValoresCausante();
            if (vCausante != null)
            {
                this.InformacionAudioria(vCausante, this.PageName, SolutionPage.Edit);
                vResultado = this.vMostrencosService.EditarCausante(vCausante);
                if (vResultado > 0)
                {
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];
                    if (vListaDocumentosEliminar != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosEliminar)
                        {
                            item.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("ELIMINADO").IdEstadoDocumento;
                            item.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }
                        this.ViewState["DocumentosEliminar"] = null;
                    }
                    if (vListaDocumentos != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                        {
                            vRuta = Server.MapPath("~/Page/Mostrencos/" + item.RutaArchivo);
                            if (item.IdDocumentosSolicitadosDenunciaBien < 0)
                            {
                                item.IdCausante = vCausante.IdCausante;
                                item.IdDenunciaBien = vCausante.IdDenunciaBien;
                                item.IdApoderado = null;
                                vResultado = this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                                item.IdDocumentosSolicitadosDenunciaBien = vResultado;
                            }
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            if (!string.IsNullOrEmpty(item.RutaArchivo))
                            {
                                if (item.bytes != null)
                                {
                                    if (!System.IO.Directory.Exists(vRuta))
                                    {
                                        System.IO.Directory.CreateDirectory(vRuta);
                                    }
                                    System.IO.File.WriteAllBytes(vRuta + "\\" + item.NombreArchivo, item.bytes);
                                }
                            }
                            else
                            {
                                item.RutaArchivo = string.Empty;
                                item.NombreArchivo = string.Empty;
                            }
                            vResultado = this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }
                    }
                    this.SetSessionParameter("Mostrencos.RegistrarInformacionCausante.Mensaje", "La información ha sido guardada exitosamente");
                    this.NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// captura los datos de la documentación 
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            bool EsExiste = false;
            if (this.fulArchivoRecibido.HasFile)
            {
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }
                DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                vDocumentoCargado.IdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                this.CargarArchivo(vDocumentoCargado);
                this.SetSessionParameter("Mostrencos.RegistarInformacionCausante.FileCausante", vDocumentoCargado);
                this.lblNombreArchivo.Visible = true;
                this.lblNombreArchivo.Text = this.fulArchivoRecibido.FileName;
            }
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (!ExistenCamposDocumentacionRequeridos())
            {
                if (vListaDocumentacion == null)
                {
                    vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
                }
                if (vListaDocumentacion.Count > 0)
                {
                    if (Convert.ToInt32(this.hfIdDocumentoSolocitado.Value) == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue)).Count() > 0)
                    {
                        this.toolBar.MostrarMensajeError("El Registro ya existe");
                        EsExiste = true;
                    }
                    else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue) && p.EsNuevo)).Count() > 0)
                    {
                        this.toolBar.MostrarMensajeError("El Registro ya existe");
                        EsExiste = true;
                    }
                }
                if (!EsExiste || !this.hfIdDocumentoSolocitado.Value.Equals("0"))
                {
                    bool EsArchivoEsValido = false;
                    if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                    {
                        int vIdDocumentosSolicitados = Convert.ToInt32(this.hfIdDocumentoSolocitado.Value);
                        vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentosSolicitados).FirstOrDefault();
                        if (vDocumentacionSolicitada == null)
                        {
                            vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
                        }
                        vDocumentacionSolicitada.IdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                        EsArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);
                        if (EsArchivoEsValido)
                        {
                            if (this.hfIdDocumentoSolocitado.Value.Equals("0"))
                            {
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                            }
                            else
                            {
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(this.hfIdDocumentoSolocitado.Value);
                            }
                            vDocumentacionSolicitada.FechaRecibido = this.FecharRecibido.Date;
                            vDocumentacionSolicitada.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre(this.rbtEstadoDocumento.SelectedValue).IdEstadoDocumento;
                            vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                            vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                            vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                            vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                            vDocumentacionSolicitada.EsNuevo = this.hfIdDocumentoSolocitado.Value.Equals("0");
                            this.lblNombreArchivo.Visible = false;
                            this.SetSessionParameter("Mostrencos.RegistarInformacionCausante.FileCausante", null);
                        }
                    }
                    else
                    {
                        vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                        vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                        vDocumentacionSolicitada.EsNuevo = this.hfIdDocumentoSolocitado.Value.Equals("0");
                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                        vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    }
                    if (EsArchivoEsValido || !EsExiste)
                    {
                        vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue);
                        vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                        vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                        vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObserbacionesDocumentoSolicitado.Text.ToUpper();
                        if (!vDocumentacionSolicitada.EsNuevo)
                        {
                            DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == Convert.ToInt32(this.hfIdDocumentoSolocitado.Value)).FirstOrDefault();
                            vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                            vListaDocumentacion.Remove(vDocumentacion);
                            vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                        }
                        vListaDocumentacion.Add(vDocumentacionSolicitada);
                        this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                        this.CargarGrillaDocumentos();
                        this.LimpiarCampos();
                        this.FecharRecibido.Date = DateTime.Now.Date;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Metodo encargado de limpiar los campos
    /// </summary>
    private void LimpiarCampos()
    {
        try
        {
            this.lblFechaRecibido.Visible = false;
            this.lblCantidadCaracteresSolicitado.Visible = false;
            this.lblrequeridoDocumentoSoporte.Visible = false;
            this.lblRequeridoFechaDefuncion.Visible = false;
            this.lblRequeridoFechaRecibido.Visible = false;
            this.lblNombreArchivo.Visible = false;
            this.lblRequeridoNombreArchivo.Visible = false;
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            this.txtObserbacionesDocumentoSolicitado.Text = string.Empty;
            this.rbtEstadoDocumento.ClearSelection();
            this.txtObservacionesRecibido.Text = string.Empty;
            this.FechaSolicitud.InitNull = true;
            this.hfIdDocumentoSolocitado.Value = "0";
            this.lblAsterFecha.Visible = false;
            this.lblAsterNombre.Visible = false;
            this.lblAsterObservacion.Visible = false;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Captura la información del causante
    /// </summary>
    /// <returns>variable de tipo Causantes con la información capturada</returns>
    private Causantes CapturarValoresCausante()
    {
        try
        {
            if (!this.ExistenCamposCausanteRequeridos(true))
            {
                int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                Causantes vCausante = this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante);
                vCausante = this.vMostrencosService.ConsultarInfirmacionCausantes(vCausante);
                vCausante.FechaDefuncion = this.FechaDefuncion.Date;
                vCausante.NumeroRegistroDefuncion = this.txtNumeroRegistroDefuncion.Text;
                vCausante.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                return vCausante;
            }
            return null;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }
                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Mb");
                }
                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = "RegistarInformacionCausante/Archivos/" + pDocumentoSolicitado.IdCausante;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.FileCausante") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }
            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposDocumentacionRequeridos()
    {
        bool EsRequerido = false;
        int error = 0;
        try
        {
            if (this.ddlDocumentoSoporte.SelectedValue.Equals("-1"))
            {
                this.lblrequeridoDocumentoSoporte.Visible = true;
                error += 1;
            }
            else
            {
                this.lblrequeridoDocumentoSoporte.Visible = false;
            }
            try
            {
                DateTime vFecha = Convert.ToDateTime(this.FechaDefuncion.Date);
                this.lblFechaInvalida.Visible = false;
            }
            catch (Exception)
            {
                this.lblFechaInvalida.Visible = true;
                error += 1;
            }
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrequeridoFechaSolicitud.Visible = true;
                error += 1;
            }
            else
            {
                this.lblrequeridoFechaSolicitud.Visible = false;
            }
            if (this.txtObserbacionesDocumentoSolicitado.Text.Length > 512)
            {
                this.lblCantidadCaracteresSolicitado.Visible = true;
                error += 1;
            }
            else
            {
                this.lblCantidadCaracteresSolicitado.Visible = false;
            }
            if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
            {
                if (string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                {
                    this.lblRequeridorbtEstadoDocumento.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridorbtEstadoDocumento.Visible = false;
                }
                if (this.FecharRecibido.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblRequeridoFechaRecibido.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridoFechaRecibido.Visible = false;
                }
                try
                {
                    DateTime vFecha = Convert.ToDateTime(this.FecharRecibido.Date);
                    this.lblFechaRecibido.Visible = false;
                }
                catch (Exception)
                {
                    this.lblFechaInvalida.Visible = true;
                    error += 1;
                }
                if (this.FecharRecibido.Date > DateTime.Now && this.FecharRecibido.Date != this.FechaSolicitud.Date)
                {
                    this.lblFechaRecibido.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblFechaRecibido.Visible = false;
                }
                if (!fulArchivoRecibido.HasFile && !this.lblNombreArchivo.Visible)
                {
                    this.lblRequeridoNombreArchivo.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridoNombreArchivo.Visible = false;
                }
                if (string.IsNullOrEmpty(this.txtObservacionesRecibido.Text))
                {
                    this.lblRequeridoObservacionesRecibido.Visible = true;
                    error += 1;
                }
                else
                {
                    this.lblRequeridoObservacionesRecibido.Visible = false;
                }
            }
            if (error == 0)
            {
                EsRequerido = false;
            }
            else
            {
                EsRequerido = true;
            }
            return EsRequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return EsRequerido;
        }
        catch (Exception ex)
        {
            if (EsRequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
            return EsRequerido;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposCausanteRequeridos(bool pEsGuardar)
    {
        bool Esrequerido = false;
        try
        {
            if (this.ddlTipoIdentificacion.SelectedValue.Equals("-1"))
            {
                this.lblRequeridoTipoIdent.Visible = true;
                Esrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoTipoIdent.Visible = false;
                Esrequerido = false;
            }
            if (string.IsNullOrEmpty(this.txtNumeroIdentificacionCausante.Text))
            {
                this.lblRequeridoNumIdent.Visible = true;
                Esrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoNumIdent.Visible = false;
                Esrequerido = false;
            }
            if (pEsGuardar)
            {
                if (string.IsNullOrEmpty(this.txtNumeroRegistroDefuncion.Text))
                {
                    this.lblRequeridoNumRegistroDefuncion.Text = "Campo Requerido";
                    this.lblRequeridoNumRegistroDefuncion.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoNumRegistroDefuncion.Visible = false;
                    Esrequerido = false;
                }
                if (Convert.ToInt32(this.txtNumeroRegistroDefuncion.Text) == 0)
                {
                    this.lblRequeridoNumRegistroDefuncion.Text = "Número de Registro de Defunción debe ser diferente de cero";
                    this.lblRequeridoNumRegistroDefuncion.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoNumRegistroDefuncion.Visible = false;
                    Esrequerido = false;
                }
                try
                {
                    DateTime vFecha = Convert.ToDateTime(this.FechaDefuncion.Date);
                    this.lblFechaDefuncionInvalida.Visible = false;
                    Esrequerido = false;
                }
                catch (Exception)
                {
                    this.lblFechaDefuncionInvalida.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                if (this.FechaDefuncion.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblRequeridoFechaDefuncion.Text = "Campo Requerido";
                    this.lblRequeridoFechaDefuncion.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoFechaDefuncion.Visible = false;
                    Esrequerido = false;
                }
                if (this.FechaDefuncion.Date > DateTime.Now)
                {
                    this.lblRequeridoFechaDefuncion.Text = "La fecha de defunción no puede ser mayor a la fecha actual";
                    this.lblRequeridoFechaDefuncion.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoFechaDefuncion.Visible = false;
                    Esrequerido = false;
                }
                if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue) || !this.ddlDocumentoSoporte.SelectedValue.Equals("-1"))
                {
                    Esrequerido = this.ExistenCamposDocumentacionRequeridos();
                }
            }
            return Esrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return Esrequerido;
        }
        catch (Exception ex)
        {
            if (Esrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
            return Esrequerido;
        }
    }

    /// <summary>
    /// carga los datos de los documentos recibidos
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void CargarDatosDocumentoRecibido(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            this.hfIdDocumentoSolocitado.Value = pDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            this.ddlDocumentoSoporte.SelectedValue = pDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
            this.FechaSolicitud.Date = Convert.ToDateTime(pDocumentoSolicitado.FechaSolicitud);
            this.txtObserbacionesDocumentoSolicitado.Text = pDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }

    }

    /// <summary>
    /// carga los datos de los documentos solicitados 
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void CargarDatosDocumentoSolocitado(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            this.hfIdDocumentoSolocitado.Value = pDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            this.ddlDocumentoSoporte.SelectedValue = pDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
            this.FechaSolicitud.Date = Convert.ToDateTime(pDocumentoSolicitado.FechaSolicitud);
            this.txtObserbacionesDocumentoSolicitado.Text = pDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion
}