﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_InformacionCausante_Detail : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionCausante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ValidateAccess(toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnNuevo_Click control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentos;
        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vListaDocumentos;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            vListaDocumentos = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentos;
            this.gvwDocumentacionRecibida.DataSource = vListaDocumentos;
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }

                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.EstablecerTitulos("Información del Causante", "Detail");
            this.gvwDocumentacionRecibida.PageSize = this.PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = this.EmptyDataText();

            this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            if (vDenuncia.IdEstadoDenuncia != 11 && vDenuncia.IdEstadoDenuncia != 13 && vDenuncia.IdEstadoDenuncia != 19)
            {
                this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
                this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);


            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo que carga los datos de la denuncia
    /// </summary>
    /// <param name="pCausante"></param>
    private void CargarDatosDenuncia(Causantes pCausante)
    {
        try
        {
            bool EsProvieneDelCU0139 = !string.IsNullOrEmpty(Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarInformacionCausante.ProvieneDelCU0139")));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(pCausante.IdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(pCausante.IdDenunciaBien).IdEstadoDenuncia).NombreEstadoDenuncia;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA"))
            {
                this.toolBar.MostrarBotonNuevo(false);
                this.toolBar.MostrarBotonEditar(false);
            }
            else
            {
                this.toolBar.MostrarBotonNuevo(!EsProvieneDelCU0139);
                this.toolBar.MostrarBotonEditar(!EsProvieneDelCU0139);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {


            int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
            Causantes vCausante = this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante);
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(vIdCausante);
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento != "ELIMINADO").ToList();
            if (vCausante.IdDenunciaBien != 0)
            {
                this.CargarListaDesplegable();
                this.CargarDatosDenuncia(vCausante);
                this.CargarDatosCausante(vCausante);
                this.CargarGrillaDocumentos();
            }
            string vMensaje = Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarInformacionCausante.Mensaje"));
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.RegistrarInformacionCausante.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion del causante
    /// </summary>
    /// <param name="pCausante">informacion del causante</param>
    private void CargarDatosCausante(Causantes pCausante)
    {
        try
        {
            pCausante = this.vMostrencosService.ConsultarInfirmacionCausantes(pCausante);
            this.txtNumeroIdentificacionCausante.Text = pCausante.Tercero.NumeroIdentificacion;
            this.txtTipoPersonacausante.Text = pCausante.Tercero.NombreTipoPersona;
            this.txtPrimerNombreCausante.Text = pCausante.Tercero.PrimerNombre;
            this.txtSegundoNombreCausante.Text = pCausante.Tercero.SegundoNombre;
            this.txtPrimerApellidoCausante.Text = pCausante.Tercero.PrimerApellido;
            this.txtSegundoApellidoCausante.Text = pCausante.Tercero.SegundoApellido;
            this.txtNumeroRegistroDefuncion.Text = pCausante.NumeroRegistroDefuncion;
            this.ddlTipoIdentificacion.SelectedValue = pCausante.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString();
            this.FechaDefuncion.Date = pCausante.FechaDefuncion;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// Metodo para cargar listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}