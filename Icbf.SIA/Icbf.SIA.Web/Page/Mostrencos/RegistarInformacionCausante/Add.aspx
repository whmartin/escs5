﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_InformacionCausante_Add" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *              
                </td>
                <td>Fecha radicado en correspondencia *              
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de identificación *
                </td>
                <td>Número identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Primer nombre *
                </td>
                <td>Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Primer apellido *
                </td>
                <td>Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">Descripción de la denuncia</td>
                <td style="width: 50%">Histórico de la denuncia
                 
                    <a class="btnPopUpHistorico" style="width: 16px; height: 16px;">
                        <img alt="h" src="../../../Image/btn/info.jpg" style="width: 20px; height: 20px;"> </img>
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="700px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel4">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del causante
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Tipo de persona             
                </td>
                <td>Tipo de identificación *
                    <asp:Label ID="lblRequeridoTipoIdent" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>

            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacioncausante" MaxLength="20" Text="NATURAL" Width="250px" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoIdentificacion" runat="server" Width="250px"></asp:DropDownList>

                </td>
            </tr>
            <tr class="rowB">
                <td>Número identificación *
                    <asp:Label ID="lblRequeridoNumIdent" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Primer nombre         
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionCausante" MaxLength="20" Width="250px"></asp:TextBox>

                    <asp:ImageButton ID="btnBuscar" OnClick="btnBuscar_Click" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif" />
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacionCausante" runat="server" TargetControlID="txtNumeroIdentificacionCausante"
                        FilterType="Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombreCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Segundo nombre
                </td>
                <td>Primer apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombreCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellidoCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Segundo apellido
                </td>
                <td>Número de registro de defunción *
                    <asp:Label ID="lblRequeridoNumRegistroDefuncion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellidoCausante" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroRegistroDefuncion" MaxLength="16" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroRegistroDefuncion"
                        FilterType="Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha de defunción *
                    <asp:Label ID="lblRequeridoFechaDefuncion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                    &nbsp;
                    <asp:Label ID="lblFechaDefuncionInvalida" runat="server" Text="Fecha Inválida" ForeColor="Red" Visible="false"></asp:Label>
                </td>
                <td></td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha runat="server" ID="FechaDefuncion" Requerid="false" Width="250px" />
                </td>
                <td></td>
            </tr>
        </table>
        <asp:HiddenField ID="hfIdTercero" runat="server" />
        <asp:HiddenField ID="hfLimpiarFechaRecibido" ClientIDMode="Static" runat="server" />
         <asp:HiddenField ID="hfIdDocumentoSolocitado" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="tdTitulos" colspan="3">Documentación Solicitada
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td>Documento soporte de la denuncia *
                    <asp:Label ID="lblrequeridoDocumentoSoporte" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td colspan="2">Fecha de solicitud *
                    <asp:Label ID="lblrequeridoFechaSolicitud" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                    <asp:Label ID="lblFechaInvalida" runat="server" Text="Fecha Inválida" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
            </tr>
            <tr class="rowA">
                
                <td>
                    <asp:DropDownList ID="ddlDocumentoSoporte" runat="server" Width="250px"></asp:DropDownList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaSolicitud" ClientIDMode="Static" Width="250px" Requerid="false" />
                </td>
                <td style="width: 150px; background-color: white; text-align: center">
                    <asp:ImageButton ID="btnAgregar" ToolTip="Adicionar" OnClick="btnAgregar_Click" runat="server" ImageUrl="~/Image/btn/add.gif" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento solicitado
                    <asp:Label ID="lblCantidadCaracteresSolicitado" runat="server" Text="Debe ingresar hasta un máximo de 512 caracteres" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtObserbacionesDocumentoSolicitado" CssClass="Validar2" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObserbacionesDocumentoSolicitado" runat="server" TargetControlID="txtObserbacionesDocumentoSolicitado"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">Estado del documento
                    <asp:Label ID="lblRequeridoEstadoDocumento" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Fecha de recibido
                    <asp:Label ID="lblRequeridoFechaRecibido" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Value="3">Aceptado</asp:ListItem>
                        <asp:ListItem Value="5">Devuelto</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FecharRecibido" Requerid="false" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Nombre de archivo&nbsp;
                    <asp:Label ID="lblRequeridoNombreArchivo" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td></td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:FileUpload ID="fulArchivoRecibido" runat="server" Enabled="false" Width="342px" />
                </td>
                <td></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento recibido
                 <asp:Label ID="lblCantidadCaracteresRecibido" runat="server" Text="Debe ingresar hasta un máximo de 512 caracteres" ForeColor="Red" Visible="false"></asp:Label>

                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesRecibido" Enabled="false" CssClass="Validar2" TextMode="MultiLine" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObservacionesRecibido" runat="server" TargetControlID="txtObservacionesRecibido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" AllowSorting="True"
                        OnSorting="gvwDocumentacionRecibida_OnSorting"
                        GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px"
                        OnRowCommand="gvwDocumentacionRecibida_RowCommand">
                        <Columns>
                             <asp:BoundField HeaderText="Tipo de documento" DataField="TipoDocumentoBienDenunciado.NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" />
                            <asp:BoundField HeaderText="Estado del documento" DataField="EstadoDocumento.NombreEstadoDocumento" SortExpression="NombreEstadoDocumento" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" DataFormatString="{0:d}" SortExpression="FechaRecibido" />
                            <asp:BoundField HeaderText="Nombre de archivo" DataField="NombreArchivo" SortExpression="NombreArchivo" />
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument="<%# Container.DataItemIndex%>" Height="16px" ImageUrl="~/Image/btn/edit.gif" ToolTip="Editar" Width="18px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnElininar" ClientIDMode="Static" runat="server" Enabled="true" OnClientClick="return confirm('¿Está seguro de eliminar la información?')" CommandName="Eliminar" CommandArgument="<%# Container.DataItemIndex%>" Height="16px" ImageUrl="~/Image/btn/Cancel.png" ToolTip="Detalle" Width="18px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" Width="500px" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 23%; width: 500px; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                    </div>
                    <div>
                        <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                            <img alt="h" src="../../../Image/btn/close.png" style="width: 20px; height: 20px;"> </img>
                        </a>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                        <asp:GridView ID="gvwHistoricoDenuncia" Width="90%" runat="server" Visible="true" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" GridLines="None"
                            OnSorting="gvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="gvwHistoricoDenuncia_OnPageIndexChanging">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('.Validar2').attr('MaxLength', 512);                       
        });
    </script>
</asp:Content>
