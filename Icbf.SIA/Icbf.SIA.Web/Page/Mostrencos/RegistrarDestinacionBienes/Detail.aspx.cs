﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistrarDestinacionBienes_Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>28/09/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

public partial class Page_RegistrarDestinacionBienes_Detail : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Declaración de Toolbar
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Path de la Página
    /// </summary>
    private string PageName = "Mostrencos/RegistrarDestinacionBienes";

    /// <summary>
    /// Servicio vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Variable del Id de la Denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Variable del Id del Mueble/Inmueble
    /// </summary>
    private int vIdMuebleInmueble;

    /// <summary>
    /// Variable del Id del Titulo/Valor
    /// </summary>
    private int vIdTituloValor;

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento para Retornar a la Página de Ir a
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Evento para validar el acceso a la página editar y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble")) != "")
        {
            vIdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble", vIdMuebleInmueble);
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor", "");
        }

        if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor")) != "")
        {
            vIdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor", vIdTituloValor);
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble", "");
        }

        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento para validar el acceso a la página List y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método que página la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwHistoricoDenuncia</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;

            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Método que Ordena las columnas de la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwHistoricoDenuncia</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }

                break;

            case "Actuacion":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }

                break;

            case "Accion":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }

                break;
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.EstablecerTitulos("Destinación de Bienes", "Detail");
            this.gvwHistoricoDenuncia.PageSize = PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.Guardado").ToString() == "1")
            {
                this.toolBar.MostrarMensajeGuardado();
            }

            this.RemoveSessionParameter("Mostrencos.RegistrarDestinacionBienes.Guardado");

            int? vIdInformacionVenta = 0;
            int? vUsoBien = 0;

            #region InformaciónDenuncia

            vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            if (vRegistroDenuncia.IdEstadoDenuncia == 11 || vRegistroDenuncia.IdEstadoDenuncia == 13 || vRegistroDenuncia.IdEstadoDenuncia == 19)
            {
                this.toolBar.MostrarBotonEditar(false);
            }

            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                   vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                   ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia.ToString();

            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.PRIMERNOMBRE.ToString() + " " + vRegistroDenuncia.SEGUNDONOMBRE.ToString() + " " + vRegistroDenuncia.PRIMERAPELLIDO.ToString() + " " + vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.RazonSocial.ToString();
            }
            #endregion

            #region MuebleInmueble

            if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble")) != "")
            {
                vIdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
                MuebleInmueble vMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorId(this.vIdMuebleInmueble);

                this.txtTipoBien.Text = vMuebleInmueble.IdTipoBien.Equals("2") ? "MUEBLE" : "INMUEBLE";
                this.txtSubTipoBien.Text = vMuebleInmueble.NombreSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(vMuebleInmueble.IdTipoBien).Where(p => p.IdSubTipoBien == vMuebleInmueble.IdSubTipoBien).FirstOrDefault().NombreSubTipoBien;
                this.txtClaseBien.Text = vMuebleInmueble.NombreClaseBien = this.vMostrencosService.ConsultarClaseBien(vMuebleInmueble.IdSubTipoBien).Where(p => p.IdClaseBien == vMuebleInmueble.IdClaseBien).FirstOrDefault().NombreClaseBien;
                this.txtEstadoBien.Text = vMuebleInmueble.EstadoBien.ToString();
                this.txtDescripcionBien.Text = vMuebleInmueble.DescripcionBien.ToString();
                this.rblDestinacionBienes.Items.Insert(0, new ListItem("Venta", "1"));
                this.rblDestinacionBienes.Items.Insert(1, new ListItem("Uso propio", "2"));
                this.rblDestinacionBienes.SelectedValue = vMuebleInmueble.UsoBien.ToString();
                vIdInformacionVenta = vMuebleInmueble.IdInformacionVenta;
                vUsoBien = vMuebleInmueble.UsoBien;
            }

            #endregion

            #region TituloValor

            if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor")) != "")
            {
                vIdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
                TituloValor vTituloValor = this.vMostrencosService.ConsultarTituloValorPorIdTituloValor(this.vIdTituloValor);
                TipoTitulo vTipoTitulo = this.vMostrencosService.ConsultarTipoTituloPorIdTipoTitulo(vTituloValor.IdTipoTitulo);

                this.txtTipoTitulo.Text = vTipoTitulo.NombreTipoTitulo;
                this.txtEstadoBienTitulo.Text = vTituloValor.EstadoBien.ToString();
                this.txtDescripcionBienTitulo.Text = vTituloValor.DescripcionTitulo.ToString();
                this.rblDestinacionBienes.Items.Insert(0, new ListItem("Venta", "1"));
                this.rblDestinacionBienes.Items.Insert(1, new ListItem("Uso propio", "2"));
                this.rblDestinacionBienes.SelectedValue = vTituloValor.UsoBien.ToString();
                vIdInformacionVenta = vTituloValor.IdInformacionVenta;
                vUsoBien = vTituloValor.UsoBien;
            }

            #endregion

            #region InformacionVenta e InformacionAvaluo

            if (vIdInformacionVenta != 0 && vIdInformacionVenta != null)
            {
                int vIdInfoVenta = Convert.ToInt32(vIdInformacionVenta);
                InformacionVenta vInformacionVenta = this.vMostrencosService.ConsultarInformacionVentaId(vIdInfoVenta);

                this.txtNumeroVenta.Text = vInformacionVenta.NumeroDocumentoVenta != null ? vInformacionVenta.NumeroDocumentoVenta.ToString() : null;
                this.txtDocumentoVenta.Text = vInformacionVenta.NombreTipoDocumento;
                CultureInfo cultureInfo = new CultureInfo("el-GR");
                this.txtValorVenta.Text = vInformacionVenta.ValorVenta != null ? string.Format(cultureInfo, "$ {0:C}", vInformacionVenta.ValorVenta) : null;
                this.txtValorAvaluo.Text = vInformacionVenta.ValorAvaluo != null ? string.Format(cultureInfo, "$ {0:C}", vInformacionVenta.ValorAvaluo) : null;
                this.txtProfesionalAvaluo.Text = vInformacionVenta.ProfesionalrealizaAvaluo;

                if (vInformacionVenta.FechaVenta != Convert.ToDateTime("1/01/0001 00:00:00"))
                {
                    this.txtFechaVenta.Text = vInformacionVenta.FechaVenta.ToString("dd/MM/yyyy");
                }

                if (vInformacionVenta.FechaSolicitudAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                {
                    this.txtFechaSolicitudAvaluo.Text = vInformacionVenta.FechaSolicitudAvaluo.ToString("dd/MM/yyyy");
                }

                if (vInformacionVenta.FechaAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                {
                    this.txtFechaAvaluo.Text = vInformacionVenta.FechaAvaluo.ToString("dd/MM/yyyy");
                } 
            }

            #endregion

            this.CargarGrillaHistorico(vIdDenunciaBien);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien">Id de la Denuncia</param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);

            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;

                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;

                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion
}