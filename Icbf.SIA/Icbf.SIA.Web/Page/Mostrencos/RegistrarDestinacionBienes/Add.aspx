﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs" Inherits="Page_RegistrarDestinacionBienes_Add" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/General/General/Control/fechaEdit.ascx" TagPrefix="uc1" TagName="fechaSN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdInformacionVenta" runat="server" />
    <script type="text/jscript" src="../../../Scripts/autoNumeric.js"></script>
    <style type="text/css">
        .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 350px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%-- INFORMACIÓN DE LA DENUNCIA --%>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado de la denuncia</td>
                        <td>Fecha de radicado de la denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en correspondencia</td>
                        <td>Fecha radicado en correspondencia</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de identificación</td>
                        <td>Número de identificación</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Primer nombre</td>
                        <td style="width: 55%">Segundo nombre</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Primer apellido</td>
                        <td style="width: 55%">Segundo apellido</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Descripción de la Denuncia
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" class="Background" />
                    </a>
                        </td>
                        <caption>
                        </caption>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- DESTINACIÓN DE BIENES --%>
            <asp:Panel runat="server" ID="PanelDestinacionBienes">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos">Destinación de Bienes
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>Uso del bien
                            <asp:RequiredFieldValidator ID="rfvUsoBen" ControlToValidate="rblDestinacionBienes" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblDestinacionBienes" RepeatDirection="Vertical" 
                                OnSelectedIndexChanged="rblDestinacionBienes_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="1">Venta</asp:ListItem>
                                <asp:ListItem Value="2">Uso propio</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DEL MUEBLE O INMUEBLE --%>
            <asp:Panel runat="server" ID="pnlInformacionInmueble">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del Mueble o Inmueble
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de bien
                        </td>
                        <td>Subtipo de bien
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoBien" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSubTipoBien" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Clase de bien
                        </td>
                        <td>Estado del bien   
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtClaseBien" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEstadoBien" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Descripción del bien</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionBien" TextMode="MultiLine" MaxLength="512" Width="900px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DEL TITULO VALOR --%>
            <asp:Panel runat="server" ID="PnlTituloValor">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información Títulos de Valor
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de título
                        </td>
                        <td>Estado del bien
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoTitulo" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEstadoBienTitulo" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Descripción del bien</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionBienTitulo" TextMode="MultiLine" MaxLength="512" Width="900px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DE LA VENTA --%>
            <asp:Panel runat="server" ID="PnlInfoVenta">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Venta
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Número de documento de venta
                            <asp:Label runat="server" ID="lblNumeroDocumentoVenta" Text=" *"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvNumeroVenta" ControlToValidate="txtNumeroVenta" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroDocumentoVenta" runat="server" TargetControlID="txtNumeroVenta"
                                FilterType="Numbers" />
                        </td>
                        <td>Tipo de documento de la venta 
                            <asp:Label runat="server" ID="lblTipoDocumentoVenta" Text=" *"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvTipoDocumentoVenta" ControlToValidate="ddlTipoDocumentoVenta" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido"
                                SetFocusOnError="true" Display="Dynamic" InitialValue="-1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroVenta" Enabled="false" MaxLength="20"
                                OnTextChanged="txtNumeroVenta_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </td>
                        <td style="width:50%;">
                            <asp:DropDownList runat="server" ID="ddlTipoDocumentoVenta" Enabled="false"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Valor de venta 
                            <asp:Label runat="server" ID="lblValorVenta" Text=" *"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvValorVenta" ControlToValidate="txtValorVenta" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td>Fecha de venta 
                            <asp:Label runat="server" ID="lblFechaVenta" Text=" *"></asp:Label>
                            <asp:Label runat="server" ID="lblFechaVentaMayor" Visible="false" ForeColor="Red"
                                Text="La Fecha de venta debe ser menor a la fecha actual" ></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvFechaVenta" ControlToValidate="txtFechaVenta$txtFecha" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtValorVenta" Enabled="false"
                                MaxLength="19" onkeypress="SetDecimal(this)"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftValorVenta" runat="server" TargetControlID="txtValorVenta"
                                FilterType="Custom,Numbers" ValidChars=",.$" />
                        </td>
                        <td>
                            <uc1:fechaSN runat="server" ID="txtFechaVenta" Enabled="false" AutoPostBackFecha="true"
                                OnFechaChange="txtFechaVenta_FechaChange"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DEL AVALÚO --%>
            <asp:Panel runat="server" ID="PnlInfoAvaluo">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del Avalúo
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha de solicitud del avalúo
                            <asp:Label runat="server" ID="lblFechaSolicitdAvaluo" Visible="false" ForeColor="Red"
                                Text="La Fecha de solicitud del avalúo debe ser menor a la fecha actual" ></asp:Label>
                        </td>
                        <td>Valor del Avalúo
                            <asp:Label runat="server" ID="lblValorAvaluo" Text=" *" Visible="false"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvValorAvaluo" ControlToValidate="txtValorAvaluo" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Enabled="false"></asp:RequiredFieldValidator>
                            <asp:CompareValidator runat="server" ID="cvValorAvaluo" ControlToValidate="txtValorAvaluo"
                                SetFocusOnError="true" ErrorMessage="Debe ser mayor de cero (0)" ValidationGroup="btnGuardar"
                                ForeColor="Red" Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fechaSN runat="server" ID="txtFechaSolicitudAvaluo" AutoPostBackFecha="true"
                                OnFechaChange="txtFechaSolicitudAvaluo_FechaChange"/>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtValorAvaluo" MaxLength="19" onkeypress="SetDecimal(this)"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftValorAvaluo" runat="server" TargetControlID="txtValorAvaluo"
                                FilterType="Custom,Numbers" ValidChars=",.$" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha del Avalúo
                            <asp:Label runat="server" ID="lblFechaAvaluo" Text=" *" Visible="false"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvtFechaAvaluo" ControlToValidate="txtFechaAvaluo$txtFecha" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Enabled="false"></asp:RequiredFieldValidator>
                            <asp:Label runat="server" ID="lblFechaAvaluoMayor" Visible="false" ForeColor="Red"
                                Text="La Fecha del avalúo debe ser menor a la fecha actual" ></asp:Label>
                        </td>
                        <td>Profesional que realizó el avalúo
                            <asp:Label runat="server" ID="lblProfesionalAvaluo" Text=" *" Visible="false"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvProfesionalAvaluo" ControlToValidate="txtProfesionalAvaluo" runat="server" 
                                ForeColor="Red" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Enabled="false"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fechaSN runat="server" ID="txtFechaAvaluo" Requerid="true" AutoPostBackFecha="true"
                                OnFechaChange="txtFechaAvaluo_FechaChange"/>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtProfesionalAvaluo" MaxLength="80"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- HISTORICO DE LA DENUNCIA --%>
            <asp:Panel ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" runat="server"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 25%; left: 15%; width: 800px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 800px;">
                    <tr>
                        <td style="background-color: #FFFFFF; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView ID="gvwHistoricoDenuncia" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False"
                                    AllowPaging="true" AllowSorting="true" GridLines="None" HorizontalAlign="Center"
                                    OnSorting="gvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="gvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" ItemStyle-Width="100" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                        <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" HorizontalAlign="Center" />
                                    <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <img src="../../../Image/main/loading.gif" name="imgLoading" id="imgLoading" class="loading" alt="" />
    <script type="text/javascript">
        function SetDecimal(control) {
            $('#' + control.id).autoNumeric('init', {
                vMin: '0', vMax: '9999999999999999.99', aSep: '.', aDec: ',', aSign: '$'
            });
        }

        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });

            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $(document).on('click', '#btnGuardar', function () {
                if (Page_ClientValidate('btnGuardar')) {
                    $('#btnGuardar').addClass('hidden');
                    var imgLoading = document.getElementById("imgLoading");
                    imgLoading.style.visibility = "visible";
                }
                if (Page_ClientValidate('btnRetornar')) {
                    $('#btnRetornar').addClass('hidden');
                }
            });
        });
    </script>
</asp:Content>
