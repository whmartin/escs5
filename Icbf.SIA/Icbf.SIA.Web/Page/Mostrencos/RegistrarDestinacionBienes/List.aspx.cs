﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistrarDestinacionBienes_List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>02/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

public partial class Page_RegistrarDestinacionBienes_List : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Declaración de Toolbar
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Path de la Página
    /// </summary>
    private string PageName = "Mostrencos/RegistrarDestinacionBienes";

    /// <summary>
    /// Servicio vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Variable del Id de la Denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento para Retornar a la Página de Ir a
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        //int vIdDenunciaBien = 2260;
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #region Historico Denuncia

    /// <summary>
    /// Método que página la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwHistoricoDenuncia</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Método que Ordena las columnas de la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwHistoricoDenuncia</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }

                break;

            case "Actuacion":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }

                break;

            case "Accion":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }

                break;
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    #endregion

    #region MuebleInmueble

    /// <summary>
    /// Método para el manejo de cambio del Control
    /// </summary>
    /// <param name="sender">The Grilla gvwInformacionInmueble</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwInformacionInmueble_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistroMueble(gvwInformacionInmueble.SelectedRow);
    }

    /// <summary>
    /// Método que página la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwInformacionInmueble</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvwInformacionInmueble_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwInformacionInmueble.PageIndex = e.NewPageIndex;

        if (this.Session["SortedViewMuebleInmueble"] != null)
        {
            List<MuebleInmueble> vList = (List<MuebleInmueble>)this.Session["SortedViewMuebleInmueble"];
            this.gvwInformacionInmueble.DataSource = vList;
            this.gvwInformacionInmueble.DataBind();
        }
        else
        {
            this.CargarGrillaInmueble();
        }
    }

    /// <summary>
    /// Método que Ordena las columnas de la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwInformacionInmueble</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwInformacionInmueble_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<MuebleInmueble> vList = new List<MuebleInmueble>();

        if (this.Session["SortedViewMuebleInmueble"] != null)
        {
            vList = (List<MuebleInmueble>)this.Session["SortedViewMuebleInmueble"];
        }
        else
        {
            vList = this.CargarGrillaInmueble();
        }

        switch (e.SortExpression)
        {
            case "NombreTipoBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.NombreTipoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.NombreTipoBien).ToList();
                }

                break;
            case "NombreSubTipoBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.NombreSubTipoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.NombreSubTipoBien).ToList();
                }

                break;
            case "NombreClaseBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.NombreClaseBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.NombreClaseBien).ToList();
                }
                break;
            case "EstadoBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.EstadoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.EstadoBien).ToList();
                }
                break;
            case "DescripcionBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.DescripcionBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.DescripcionBien).ToList();
                }
                break;
            case "UsoBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.UsoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.UsoBien).ToList();
                }
                break;
            default:
                break;
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        this.Session["SortedViewMuebleInmueble"] = vList;
        this.gvwInformacionInmueble.DataSource = vList;
        this.gvwInformacionInmueble.DataBind();
    }

    #endregion

    #region TituloValor

    /// <summary>
    /// Método para el manejo de cambio del Control
    /// </summary>
    /// <param name="sender">The Grilla gvwInformacionTitulo</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwInformacionTitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistroTitulo(gvwInformacionTitulo.SelectedRow);
    }

    /// <summary>
    /// Método que página la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwInformacionTitulo</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvwInformacionTitulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwInformacionTitulo.PageIndex = e.NewPageIndex;

        if (this.Session["SortedViewTituloValor"] != null)
        {
            List<TituloValor> vList = (List<TituloValor>)this.Session["SortedViewTituloValor"];
            this.gvwInformacionTitulo.DataSource = vList;
            this.gvwInformacionTitulo.DataBind();
        }
        else
        {
            this.CargarGrillaTitulo();
        }
    }

    /// <summary>
    /// Método que Ordena las columnas de la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwInformacionTitulo</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwInformacionTitulo_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<TituloValor> vList = new List<TituloValor>();

        if (this.Session["SortedViewTituloValor"] != null)
        {
            vList = (List<TituloValor>)this.Session["SortedViewTituloValor"];
        }
        else
        {
            vList = this.CargarGrillaTitulo();
        }

        switch (e.SortExpression)
        {
            case "NombreTipoTitulo":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.TipoTitulo).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.TipoTitulo).ToList();
                }

                break;
            case "EstadoBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.EstadoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.EstadoBien).ToList();
                }

                break;
            case "DescripcionTitulo":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.DescripcionTitulo).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.DescripcionTitulo).ToList();
                }
                break;
            case "UsoBien":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.UsoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.UsoBien).ToList();
                }
                break;
            default:
                break;
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        this.Session["SortedView"] = vList;
        this.gvwInformacionTitulo.DataSource = vList;
        this.gvwInformacionTitulo.DataBind();
    }

    #endregion

    #endregion

    #region METODOS

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.EstablecerTitulos("Destinación de Bienes", "List");
            this.gvwHistoricoDenuncia.PageSize = PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
            //vIdDenunciaBien = 2260;
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            this.hfCorreo.Value = !string.IsNullOrEmpty(vRegistroDenuncia.CORREOELECTRONICO) ? vRegistroDenuncia.CORREOELECTRONICO : string.Empty;
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                   vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                   ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia.ToString();

            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.PRIMERNOMBRE.ToString() + " " + vRegistroDenuncia.SEGUNDONOMBRE.ToString() + " " + vRegistroDenuncia.PRIMERAPELLIDO.ToString() + " " + vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.RazonSocial.ToString();
            }

            this.CargarGrillaHistorico(vIdDenunciaBien);
            SetSessionParameter("DocumentarBienDenunciado.Denuncia", vRegistroDenuncia);

            this.CargarGrillaInmueble();
            this.CargarGrillaTitulo();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien">Id de la Denuncia</param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;

                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #region MuebleInmueble

    /// <summary>
    /// Carga la informacion de la grilla Imueble
    /// </summary>
    private List<MuebleInmueble> CargarGrillaInmueble()
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        //int vIdDenunciaBien = 2260;
        List<MuebleInmueble> vListaMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueble(vIdDenunciaBien);
        vListaMuebleInmueble = vListaMuebleInmueble.OrderBy(p => p.NombreTipoBien).ToList();
        this.ViewState["SortedgvwInformacionInmueble"] = vListaMuebleInmueble;
        this.gvwInformacionInmueble.DataSource = vListaMuebleInmueble;
        this.gvwInformacionInmueble.DataBind();

        return vListaMuebleInmueble;
    }

    /// <summary>
    /// Método que envia al detalle para consultar la información
    /// </summary>
    /// <param name="pRow">The pRow</param>
    private void SeleccionarRegistroMueble(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvwInformacionInmueble.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble", strValue);
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor", "");

            vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
            //int vIdDenunciaBien = 2260;
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien", vIdDenunciaBien);
            this.NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region TituloValor

    /// <summary>
    /// Carga la informacion de la grilla TituloValor
    /// </summary>
    private List<TituloValor> CargarGrillaTitulo()
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        //int vIdDenunciaBien = 2260;
        List<TituloValor> vListaAnulaciones = this.vMostrencosService.ConsultarTituloValor(vIdDenunciaBien);
        vListaAnulaciones = vListaAnulaciones.OrderBy(p => p.TipoTitulo.NombreTipoTitulo).ToList();

        this.ViewState["SortedgvwInformacionTitulo"] = vListaAnulaciones;
        this.gvwInformacionTitulo.DataSource = vListaAnulaciones;
        this.gvwInformacionTitulo.DataBind();

        return vListaAnulaciones;
    }

    /// <summary>
    /// Método que envia al detalle para consultar la información
    /// </summary>
    /// <param name="pRow">The pRow</param>
    private void SeleccionarRegistroTitulo(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvwInformacionTitulo.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor", strValue);
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble", "");

            vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
            //vIdDenunciaBien = 2260;
            this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien", vIdDenunciaBien);
            this.NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #endregion
}