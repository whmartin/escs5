﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistrarDestinacionBienes_Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>28/09/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_RegistrarDestinacionBienes_Add : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Declaración de Toolbar
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Path de la Página
    /// </summary>
    private string PageName = "Mostrencos/RegistrarDestinacionBienes";

    /// <summary>
    /// Servicio vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Variable del Id de la Denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Variable del Id del Mueble/Inmueble
    /// </summary>
    private int vIdMuebleInmueble;

    /// <summary>
    /// Variable del Id del Titulo/Valor
    /// </summary>
    private int vIdTituloValor;

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!this.Page.IsPostBack)
            {
                this.rblDestinacionBienes.Focus();
                this.ValidacionesPrevias();
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento para Retornar a la Página de Ir a
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Evento para Almacenar la información del formulario
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        bool vResult = this.Validaciones();

        if (!vResult)
        {
            this.GuardarTodo();
        }
    }

    /// <summary>
    /// Evento que habilita la Sección "Información de Venta"
    /// </summary>
    /// <param name="sender">The RadioButton</param>
    /// <param name="e">The Click</param>
    protected void rblDestinacionBienes_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblDestinacionBienes.SelectedValue == "1" && this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble") != null)
        {
            this.txtNumeroVenta.Enabled = true;
            this.ddlTipoDocumentoVenta.Enabled = true;
            this.txtValorVenta.Enabled = true;
            this.txtFechaVenta.Enabled = true;

            this.rfvNumeroVenta.Enabled = true;
            this.rfvTipoDocumentoVenta.Enabled = true;
            this.rfvValorVenta.Enabled = true;
            this.rfvValorVenta.Enabled = true;
            this.rfvFechaVenta.Enabled = true;

            this.lblNumeroDocumentoVenta.Visible = true;
            this.lblTipoDocumentoVenta.Visible = true;
            this.lblValorVenta.Visible = true;
            this.lblFechaVenta.Visible = true;
        }
        else
        {
            this.txtNumeroVenta.Enabled = false;
            this.ddlTipoDocumentoVenta.Enabled = false;
            this.txtValorVenta.Enabled = false;
            this.txtFechaVenta.Enabled = false;

            this.txtNumeroVenta.Text = string.Empty;
            this.ddlTipoDocumentoVenta.SelectedValue = "-1";
            this.txtValorVenta.Text = string.Empty;
            this.txtFechaVenta.InitNull = true;

            this.rfvNumeroVenta.Enabled = false;
            this.rfvTipoDocumentoVenta.Enabled = false;
            this.rfvValorVenta.Enabled = false;
            this.rfvValorVenta.Enabled = false;
            this.rfvFechaVenta.Enabled = false;

            this.lblNumeroDocumentoVenta.Visible = false;
            this.lblTipoDocumentoVenta.Visible = false;
            this.lblValorVenta.Visible = false;
            this.lblFechaVenta.Visible = false;
        }

        this.rblDestinacionBienes.Focus();
    }

    /// <summary>
    /// Método que página la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwHistoricoDenuncia</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Método que Ordena las columnas de la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvwHistoricoDenuncia</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }

                break;

            case "Actuacion":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }

                break;

            case "Accion":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }

                break;
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    /// <summary>
    /// Evento que valida los controles obligatorios de Información del avaluo
    /// </summary>
    /// <param name="sender">The txtFechaSolicitudAvaluo</param>
    /// <param name="e">The FechaChange</param>
    protected void txtFechaSolicitudAvaluo_FechaChange(object sender, EventArgs e)
    {
        if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("01/01/1900") && this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("01/01/0001"))
        {
            this.lblValorAvaluo.Visible = true;
            this.rfvValorAvaluo.Enabled = true;
            this.lblFechaAvaluo.Visible = true;
            this.rfvtFechaAvaluo.Enabled = true;
            this.lblProfesionalAvaluo.Visible = true;
            this.rfvProfesionalAvaluo.Enabled = true;
        }
        else
        {
            this.lblValorAvaluo.Visible = false;
            this.rfvValorAvaluo.Enabled = false;
            this.lblFechaAvaluo.Visible = false;
            this.rfvtFechaAvaluo.Enabled = false;
            this.lblProfesionalAvaluo.Visible = false;
            this.rfvProfesionalAvaluo.Enabled = false;
        }

        if (this.txtFechaSolicitudAvaluo.Date > DateTime.Now.Date)
        {
            this.lblFechaSolicitdAvaluo.Visible = true;
        }
        else
        {
            this.lblFechaSolicitdAvaluo.Visible = false;
        }

        this.txtFechaSolicitudAvaluo.Focus();
    }

    /// <summary>
    /// Evento que valida La fecha del control 
    /// </summary>
    /// <param name="sender">The txtFechaVenta</param>
    /// <param name="e">The FechaChange</param>
    protected void txtFechaVenta_FechaChange(object sender, EventArgs e)
    {
        if (this.txtFechaVenta.Date > DateTime.Now.Date)
        {
            this.lblFechaVentaMayor.Visible = true;
        }
        else
        {
            this.lblFechaVentaMayor.Visible = false;
        }

        this.txtFechaVenta.Focus();
    }

    /// <summary>
    /// Evento que valida la obligatoriedad de Información de la venta
    /// </summary>
    /// <param name="sender">The txtNumeroVenta</param>
    /// <param name="e">The TextChanged</param>
    protected void txtNumeroVenta_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor")) != "")
        {
            if (!string.IsNullOrEmpty(this.txtNumeroVenta.Text))
            {
                this.rfvNumeroVenta.Enabled = true;
                this.rfvTipoDocumentoVenta.Enabled = true;
                this.rfvValorVenta.Enabled = true;
                this.rfvFechaVenta.Enabled = true;

                this.lblNumeroDocumentoVenta.Visible = true;
                this.lblTipoDocumentoVenta.Visible = true;
                this.lblValorVenta.Visible = true;
                this.lblFechaVenta.Visible = true;
            }
            else
            {
                this.rfvNumeroVenta.Enabled = false;
                this.rfvTipoDocumentoVenta.Enabled = false;
                this.rfvValorVenta.Enabled = false;
                this.rfvFechaVenta.Enabled = false;

                this.lblNumeroDocumentoVenta.Visible = false;
                this.lblTipoDocumentoVenta.Visible = false;
                this.lblValorVenta.Visible = false;
                this.lblFechaVenta.Visible = false;
            }
        }

        this.txtNumeroVenta.Focus();
    }

    /// <summary>
    /// Evento que valida La fecha del control 
    /// </summary>
    /// <param name="sender">The txtFechaAvalu</param>
    /// <param name="e">The FechaChange</param>
    protected void txtFechaAvaluo_FechaChange(object sender, EventArgs e)
    {
        if (this.txtFechaAvaluo.Date > DateTime.Now.Date)
        {
            this.lblFechaAvaluoMayor.Visible = true;
            this.rfvtFechaAvaluo.Visible = false;
        }
        else
        {
            this.lblFechaAvaluoMayor.Visible = false;
            this.rfvtFechaAvaluo.Visible = true;
        }

        this.txtFechaAvaluo.Focus();
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Destinación de Bienes", "Add");
            this.gvwHistoricoDenuncia.PageSize = PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int? vUsoBien = 0;
            int? vIdInformacionVenta = 0;

            #region InformaciónDenuncia

            vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            if (vRegistroDenuncia.IdEstadoDenuncia == 11 || vRegistroDenuncia.IdEstadoDenuncia == 13 || vRegistroDenuncia.IdEstadoDenuncia == 19)
            {
                this.toolBar.MostrarBotonEditar(false);
            }

            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                   vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                   ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia.ToString();

            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.PRIMERNOMBRE.ToString() + " " + vRegistroDenuncia.SEGUNDONOMBRE.ToString() + " " + vRegistroDenuncia.PRIMERAPELLIDO.ToString() + " " + vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.RazonSocial.ToString();
            }
            #endregion

            #region MuebleInmueble

            if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble")) != "")
            {
                vIdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
                MuebleInmueble vMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorId(this.vIdMuebleInmueble);

                this.txtTipoBien.Text = vMuebleInmueble.IdTipoBien.Equals("2") ? "MUEBLE" : "INMUEBLE";
                this.txtSubTipoBien.Text = vMuebleInmueble.NombreSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(vMuebleInmueble.IdTipoBien).Where(p => p.IdSubTipoBien == vMuebleInmueble.IdSubTipoBien).FirstOrDefault().NombreSubTipoBien;
                this.txtClaseBien.Text = vMuebleInmueble.NombreClaseBien = this.vMostrencosService.ConsultarClaseBien(vMuebleInmueble.IdSubTipoBien).Where(p => p.IdClaseBien == vMuebleInmueble.IdClaseBien).FirstOrDefault().NombreClaseBien;
                this.txtEstadoBien.Text = vMuebleInmueble.EstadoBien.ToString();
                this.txtDescripcionBien.Text = vMuebleInmueble.DescripcionBien.ToString();
                vIdInformacionVenta = vMuebleInmueble.IdInformacionVenta;
                vUsoBien = vMuebleInmueble.UsoBien;
                this.hfIdInformacionVenta.Value = vIdInformacionVenta.ToString();

                if (vUsoBien == 2)
                {
                    this.rfvNumeroVenta.Enabled = false;
                    this.rfvTipoDocumentoVenta.Enabled = false;
                    this.rfvValorVenta.Enabled = false;
                    this.rfvFechaVenta.Enabled = false;

                    this.lblNumeroDocumentoVenta.Visible = false;
                    this.lblTipoDocumentoVenta.Visible = false;
                    this.lblValorVenta.Visible = false;
                    this.lblFechaVenta.Visible = false;
                }
            }

            #endregion

            #region TituloValor

            if (Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor")) != "")
            {
                vIdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
                TituloValor vTituloValor = this.vMostrencosService.ConsultarTituloValorPorIdTituloValor(this.vIdTituloValor);
                TipoTitulo vTipoTitulo = this.vMostrencosService.ConsultarTipoTituloPorIdTipoTitulo(vTituloValor.IdTipoTitulo);

                this.txtTipoTitulo.Text = vTipoTitulo.NombreTipoTitulo;
                this.txtEstadoBienTitulo.Text = vTituloValor.EstadoBien.ToString();
                this.txtDescripcionBienTitulo.Text = vTituloValor.DescripcionTitulo.ToString();
                vIdInformacionVenta = vTituloValor.IdInformacionVenta;
                vUsoBien = vTituloValor.UsoBien;
                this.hfIdInformacionVenta.Value = vIdInformacionVenta.ToString();

                this.rfvNumeroVenta.Enabled = false;
                this.rfvTipoDocumentoVenta.Enabled = false;
                this.rfvValorVenta.Enabled = false;
                this.rfvFechaVenta.Enabled = false;

                this.lblNumeroDocumentoVenta.Visible = false;
                this.lblTipoDocumentoVenta.Visible = false;
                this.lblValorVenta.Visible = false;
                this.lblFechaVenta.Visible = false;
            }

            #endregion

            #region InformacionVenta e InformacionAvaluo

            List<TipoDocumentoBienDenunciado> vTipoDocumentoSoporte = this.vMostrencosService.ConsultarTipoDocumentoSoporte();

            vTipoDocumentoSoporte.Insert(0, new TipoDocumentoBienDenunciado() { NombreTipoDocumento = "SELECCIONE", IdTipoDocumentoBienDenunciado = -1 });
            this.ddlTipoDocumentoVenta.DataSource = vTipoDocumentoSoporte;
            this.ddlTipoDocumentoVenta.DataTextField = "NombreTipoDocumento";
            this.ddlTipoDocumentoVenta.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlTipoDocumentoVenta.DataBind();
            this.ddlTipoDocumentoVenta.SelectedValue = "-1";

            if (vIdInformacionVenta != 0 && vIdInformacionVenta != null)
            {
                int vIdInfoVenta = Convert.ToInt32(vIdInformacionVenta);
                InformacionVenta vInformacionVenta = this.vMostrencosService.ConsultarInformacionVentaId(vIdInfoVenta);

                this.txtNumeroVenta.Text = vInformacionVenta.NumeroDocumentoVenta != null ? vInformacionVenta.NumeroDocumentoVenta.ToString() : null;
                this.ddlTipoDocumentoVenta.SelectedValue = vInformacionVenta.IdTipoDocumentoVenta != null ? vInformacionVenta.IdTipoDocumentoVenta.ToString() : "-1";
                CultureInfo cultureInfo = new CultureInfo("el-GR");
                this.txtValorVenta.Text = vInformacionVenta.ValorVenta != null ? string.Format(cultureInfo, "$ {0:C}", vInformacionVenta.ValorVenta) : null;
                this.txtValorAvaluo.Text = vInformacionVenta.ValorAvaluo != null ? string.Format(cultureInfo, "$ {0:C}", vInformacionVenta.ValorAvaluo) : null;
                this.txtProfesionalAvaluo.Text = vInformacionVenta.ProfesionalrealizaAvaluo;

                if (vInformacionVenta.FechaVenta != Convert.ToDateTime("1/01/0001 00:00:00"))
                {
                    this.txtFechaVenta.Date = vInformacionVenta.FechaVenta;
                }

                if (vInformacionVenta.FechaSolicitudAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                {
                    this.txtFechaSolicitudAvaluo.Date = vInformacionVenta.FechaSolicitudAvaluo;
                }

                if (vInformacionVenta.FechaAvaluo != Convert.ToDateTime("1/01/0001 00:00:00"))
                {
                    this.txtFechaAvaluo.Date = vInformacionVenta.FechaAvaluo;
                }
            }

            if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00") && this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("1/01/0001 00:00:00"))
            {
                this.lblValorAvaluo.Visible = true;
                this.rfvValorAvaluo.Enabled = true;
                this.lblFechaAvaluo.Visible = true;
                this.rfvtFechaAvaluo.Enabled = true;
                this.lblProfesionalAvaluo.Visible = true;
                this.rfvProfesionalAvaluo.Enabled = true;
            }

            #endregion

            this.UsoBien(vUsoBien);
            this.CargarGrillaHistorico(vIdDenunciaBien);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que almacena la información del formulario
    /// </summary>
    public void GuardarTodo()
    {
        try
        {
            int vResultadoVenta;
            int vResultadoMueble;
            int vResultadoTitulo;

            #region Venta 1 - MuebleInmueble

            if (this.rblDestinacionBienes.SelectedValue == "1" && Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble")) != "")
            {
                int vIdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
                InformacionVenta vInformacionVenta = new InformacionVenta();

                vInformacionVenta.NumeroDocumentoVenta = Convert.ToInt32(this.txtNumeroVenta.Text);
                vInformacionVenta.IdTipoDocumentoVenta = Convert.ToInt32(this.ddlTipoDocumentoVenta.SelectedValue);
                vInformacionVenta.ValorVenta = Convert.ToDecimal(this.txtValorVenta.Text.Remove(0,1));
                vInformacionVenta.FechaVenta = this.txtFechaVenta.Date;

                if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaSolicitudAvaluo = this.txtFechaSolicitudAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtValorAvaluo.Text))
                {
                    vInformacionVenta.ValorAvaluo = Convert.ToDecimal(this.txtValorAvaluo.Text.Remove(0, 1));
                }

                if (this.txtFechaAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaAvaluo = this.txtFechaAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtProfesionalAvaluo.Text))
                {
                    vInformacionVenta.ProfesionalrealizaAvaluo = this.txtProfesionalAvaluo.Text;
                }

                this.InsertarHistorico();
                this.InformacionAudioria(vInformacionVenta, this.PageName, this.vSolutionPage);

                if (hfIdInformacionVenta.Value != "")
                {
                    vInformacionVenta.IdInformacionVenta = Convert.ToInt32(hfIdInformacionVenta.Value);
                    vInformacionVenta.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.ModificarInformacionVenta(vInformacionVenta);
                }
                else
                {
                    vInformacionVenta.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.InsertarInformacionVenta(vInformacionVenta);
                }

                if (vResultadoVenta > 0)
                {
                    MuebleInmueble vMuebleInmueble = new MuebleInmueble();

                    vMuebleInmueble.IdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
                    vMuebleInmueble.UsoBien = Convert.ToInt32(this.rblDestinacionBienes.SelectedValue);
                    vMuebleInmueble.IdInformacionVenta = vResultadoVenta;
                    vMuebleInmueble.FechaVenta = this.txtFechaVenta.Date;
                    vMuebleInmueble.EstadoBien = this.txtEstadoBien.Text;
                    vMuebleInmueble.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoMueble = this.vMostrencosService.ModificarMuebleInmueble(vMuebleInmueble);
                }

                this.toolBar.MostrarMensajeGuardado();
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdInformacionVenta", vResultadoVenta);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble", vIdMuebleInmueble);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.Guardado", "1");
                this.NavigateTo(SolutionPage.Detail);
            }

            #endregion

            #region Venta 1 - TituloValor

            if (this.rblDestinacionBienes.SelectedValue == "1" && Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor")) != "")
            {
                int vIdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
               
                InformacionVenta vInformacionVenta = new InformacionVenta();

                if (!string.IsNullOrEmpty(this.txtNumeroVenta.Text))
                {
                    vInformacionVenta.NumeroDocumentoVenta = Convert.ToInt32(this.txtNumeroVenta.Text);
                }

                if (this.ddlTipoDocumentoVenta.SelectedValue != "-1")
                {
                    vInformacionVenta.IdTipoDocumentoVenta = Convert.ToInt32(this.ddlTipoDocumentoVenta.SelectedValue);
                }

                if (!string.IsNullOrEmpty(this.txtValorVenta.Text))
                {
                    vInformacionVenta.ValorVenta = Convert.ToDecimal(this.txtValorVenta.Text.Remove(0, 1));
                }

                if (this.txtFechaVenta.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaVenta = this.txtFechaVenta.Date;
                }

                if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaSolicitudAvaluo = this.txtFechaSolicitudAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtValorAvaluo.Text))
                {
                    vInformacionVenta.ValorAvaluo = Convert.ToDecimal(this.txtValorAvaluo.Text.Remove(0, 1));
                }

                if (this.txtFechaAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaAvaluo = this.txtFechaAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtProfesionalAvaluo.Text))
                {
                    vInformacionVenta.ProfesionalrealizaAvaluo = this.txtProfesionalAvaluo.Text;
                }

                this.InsertarHistorico();
                this.InformacionAudioria(vInformacionVenta, this.PageName, this.vSolutionPage);

                if (hfIdInformacionVenta.Value != "")
                {
                    vInformacionVenta.IdInformacionVenta = Convert.ToInt32(hfIdInformacionVenta.Value);
                    vInformacionVenta.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.ModificarInformacionVenta(vInformacionVenta);
                }
                else
                {
                    vInformacionVenta.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.InsertarInformacionVenta(vInformacionVenta);
                }

                if (vResultadoVenta > 0)
                {
                    TituloValor vTituloValor = new TituloValor();

                    vTituloValor.IdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
                    vTituloValor.UsoBien = Convert.ToInt32(this.rblDestinacionBienes.SelectedValue);
                    vTituloValor.IdInformacionVenta = vResultadoVenta;
                    vTituloValor.FechaVenta = this.txtFechaVenta.Date;
                    vTituloValor.EstadoBien = this.txtEstadoBienTitulo.Text;
                    vTituloValor.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoTitulo = this.vMostrencosService.ModificarTituloValor(vTituloValor);
                }

                this.toolBar.MostrarMensajeGuardado();
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdInformacionVenta", vResultadoVenta);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor", vIdTituloValor);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.Guardado", "1");
                this.NavigateTo(SolutionPage.Detail);
            }

            #endregion

            #region Uso Propio 2 - MuebleInmueble

            if (this.rblDestinacionBienes.SelectedValue == "2" && Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble")) != "")
            {
                int vIdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
                InformacionVenta vInformacionVenta = new InformacionVenta();

                if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaSolicitudAvaluo = this.txtFechaSolicitudAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtValorAvaluo.Text))
                {
                    vInformacionVenta.ValorAvaluo = Convert.ToDecimal(this.txtValorAvaluo.Text.Remove(0, 1));
                }

                if (this.txtFechaAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaAvaluo = this.txtFechaAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtProfesionalAvaluo.Text))
                {
                    vInformacionVenta.ProfesionalrealizaAvaluo = this.txtProfesionalAvaluo.Text;
                }

                this.InsertarHistorico();
                this.InformacionAudioria(vInformacionVenta, this.PageName, this.vSolutionPage);

                if (hfIdInformacionVenta.Value != "")
                {
                    vInformacionVenta.IdInformacionVenta = Convert.ToInt32(hfIdInformacionVenta.Value);
                    vInformacionVenta.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.ModificarInformacionVenta(vInformacionVenta);
                }
                else
                {
                    vInformacionVenta.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.InsertarInformacionVenta(vInformacionVenta);
                }

                if (vResultadoVenta > 0)
                {
                    MuebleInmueble vMuebleInmueble = new MuebleInmueble();

                    vMuebleInmueble.IdMuebleInmueble = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble"));
                    vMuebleInmueble.UsoBien = Convert.ToInt32(this.rblDestinacionBienes.SelectedValue);
                    vMuebleInmueble.IdInformacionVenta = vResultadoVenta;
                    vMuebleInmueble.FechaVenta = this.txtFechaVenta.Date;
                    vMuebleInmueble.EstadoBien = this.txtEstadoBien.Text;
                    vMuebleInmueble.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoMueble = this.vMostrencosService.ModificarMuebleInmueble(vMuebleInmueble);
                }

                this.toolBar.MostrarMensajeGuardado();
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdInformacionVenta", vResultadoVenta);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdMuebleInmueble", vIdMuebleInmueble);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.Guardado", "1");
                this.NavigateTo(SolutionPage.Detail);
            }

            #endregion

            #region Uso Propio 2 - TituloValor

            if (this.rblDestinacionBienes.SelectedValue == "2" && Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor")) != "")
            {
                int vIdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
             
                InformacionVenta vInformacionVenta = new InformacionVenta();

                if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaSolicitudAvaluo = this.txtFechaSolicitudAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtValorAvaluo.Text))
                {
                    vInformacionVenta.ValorAvaluo = Convert.ToDecimal(this.txtValorAvaluo.Text.Remove(0, 1));
                }

                if (this.txtFechaAvaluo.Date != Convert.ToDateTime("1/01/1900 00:00:00"))
                {
                    vInformacionVenta.FechaAvaluo = this.txtFechaAvaluo.Date;
                }

                if (!string.IsNullOrEmpty(this.txtProfesionalAvaluo.Text))
                {
                    vInformacionVenta.ProfesionalrealizaAvaluo = this.txtProfesionalAvaluo.Text;
                }

                this.InsertarHistorico();
                this.InformacionAudioria(vInformacionVenta, this.PageName, this.vSolutionPage);

                if (hfIdInformacionVenta.Value != "")
                {
                    vInformacionVenta.IdInformacionVenta = Convert.ToInt32(hfIdInformacionVenta.Value);
                    vInformacionVenta.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.ModificarInformacionVenta(vInformacionVenta);
                }
                else
                {
                    vInformacionVenta.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultadoVenta = this.vMostrencosService.InsertarInformacionVenta(vInformacionVenta);
                }

                if (vResultadoVenta > 0)
                {
                    TituloValor vTituloValor = new TituloValor();

                    vTituloValor.IdTituloValor = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor"));
                    vTituloValor.IdInformacionVenta = null;
                    vTituloValor.UsoBien = Convert.ToInt32(this.rblDestinacionBienes.SelectedValue);
                    vTituloValor.IdInformacionVenta = vResultadoVenta;
                    vTituloValor.FechaVenta = this.txtFechaVenta.Date;
                    vTituloValor.EstadoBien = this.txtEstadoBienTitulo.Text;
                    vTituloValor.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultadoTitulo = this.vMostrencosService.ModificarTituloValor(vTituloValor);
                }

                this.toolBar.MostrarMensajeGuardado();
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdInformacionVenta", vResultadoVenta);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor", vIdTituloValor);
                SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.Guardado", "1");
                this.NavigateTo(SolutionPage.Detail);
            }

            #endregion
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien">Id de la Denuncia</param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;

                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Método que inserta el historico de los Estados de la Denuncia Bien
    /// </summary>
    /// <returns>Id del Historico</returns>
    public int InsertarHistorico()
    {
        vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();

        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = vRegistroDenuncia.IdEstado;
        pHistoricoEstadosDenunciaBien.Responsable = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.IdFase = 20;
        pHistoricoEstadosDenunciaBien.IdActuacion = 28;
        pHistoricoEstadosDenunciaBien.IdAccion = 41;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();

        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenuncia(pHistoricoEstadosDenunciaBien);
        return IdHistoricoDocumentosSolicitados;
    }

    /// <summary>
    /// Método que carga las validaciones del Uso Bien - Venta
    /// </summary>
    /// <param name="pUsoBien">Id del Uso del Bien</param>
    private void UsoBien(int? pUsoBien)
    {
        if (pUsoBien == 1 || pUsoBien == 2)
        {
            this.rblDestinacionBienes.Enabled = false;
        }

        if (pUsoBien == 1 && this.GetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdTituloValor") != null)
        {
            this.rblDestinacionBienes.SelectedValue = "1";
            this.txtNumeroVenta.Enabled = true;
            this.ddlTipoDocumentoVenta.Enabled = true;
            this.txtValorVenta.Enabled = true;
            this.txtFechaVenta.Enabled = true;
        }
        else if (pUsoBien == 2)
        {
            this.rblDestinacionBienes.SelectedValue = "2";
            this.txtNumeroVenta.Enabled = false;
            this.ddlTipoDocumentoVenta.Enabled = false;
            this.txtValorVenta.Enabled = false;
            this.txtFechaVenta.Enabled = false;
        }
    }

    /// <summary>
    /// Método que realiza la validación de Fechas
    /// </summary>
    /// <returns>Resultado Booleano</returns>
    private bool Validaciones()
    {
        bool vEsRequerido = false;

        if (this.txtFechaVenta.Date > DateTime.Now.Date)
        {
            this.lblFechaVentaMayor.Visible = true;

            if (this.txtFechaSolicitudAvaluo.Date > DateTime.Now.Date)
            {
                this.lblFechaSolicitdAvaluo.Visible = true;

                if (this.txtFechaAvaluo.Date > DateTime.Now.Date)
                {
                    this.lblFechaAvaluoMayor.Visible = true;
                    return vEsRequerido = true;
                }

                this.lblFechaAvaluoMayor.Visible = false;
                return vEsRequerido = true;
            }
            else
            {
                this.lblFechaSolicitdAvaluo.Visible = false;

                if (this.txtFechaAvaluo.Date > DateTime.Now.Date)
                {
                    this.lblFechaAvaluoMayor.Visible = true;
                    return vEsRequerido = true;
                }
                else
                {
                    this.lblFechaAvaluoMayor.Visible = false;
                    return vEsRequerido = true;
                }
            }
        }
        else
        {
            this.lblFechaVentaMayor.Visible = false;

            if (this.txtFechaSolicitudAvaluo.Date > DateTime.Now.Date)
            {
                this.lblFechaSolicitdAvaluo.Visible = true;

                if (this.txtFechaAvaluo.Date > DateTime.Now.Date)
                {
                    this.lblFechaAvaluoMayor.Visible = true;
                    return vEsRequerido = true;
                }

                this.lblFechaAvaluoMayor.Visible = false;
                return vEsRequerido = true;
            }
            else
            {
                this.lblFechaSolicitdAvaluo.Visible = false;

                if (this.txtFechaAvaluo.Date > DateTime.Now.Date)
                {
                    this.lblFechaAvaluoMayor.Visible = true;
                    return vEsRequerido = true;
                }
                else
                {
                    this.lblFechaAvaluoMayor.Visible = false;
                    return vEsRequerido = false;
                }
            }
        }
    }

    /// <summary>
    /// Método que realiza las validaciones previas al ingresar
    /// </summary>
    private void ValidacionesPrevias()
    {
        if (this.rblDestinacionBienes.SelectedValue == "2")
        {
            this.txtNumeroVenta.Enabled = false;
            this.ddlTipoDocumentoVenta.Enabled = false;
            this.txtValorVenta.Enabled = false;
            this.txtFechaVenta.Enabled = false;
        }
        else
        {
            this.txtNumeroVenta.Enabled = true;
            this.ddlTipoDocumentoVenta.Enabled = true;
            this.txtValorVenta.Enabled = true;
            this.txtFechaVenta.Enabled = true;
        }

        if (this.txtFechaSolicitudAvaluo.Date != Convert.ToDateTime("01/01/1900"))
        {
            this.lblValorAvaluo.Visible = true;
            this.rfvValorAvaluo.Enabled = true;
            this.lblFechaAvaluo.Visible = true;
            this.rfvtFechaAvaluo.Enabled = true;
            this.lblProfesionalAvaluo.Visible = true;
            this.rfvProfesionalAvaluo.Enabled = true;
        }
        else
        {
            this.lblValorAvaluo.Visible = false;
            this.rfvValorAvaluo.Enabled = false;
            this.lblFechaAvaluo.Visible = false;
            this.rfvtFechaAvaluo.Enabled = false;
            this.lblProfesionalAvaluo.Visible = false;
            this.rfvProfesionalAvaluo.Enabled = false;
        }
    }

    /// <summary>
    /// Buscar control en el aspx
    /// </summary>
    /// <param name="pRootControl">Control pRootControl</param>
    /// <param name="pControlID">String pControlID</param>
    /// <returns>Control encontrado</returns>
    private Control FindControlRecursive(Control pRootControl, string pControlID)
    {
        if (pRootControl.ID == pControlID)
        {
            return pRootControl;
        }

        foreach (Control controlToSearch in pRootControl.Controls)
        {
            Control controlToReturn = this.FindControlRecursive(controlToSearch, pControlID);
            if (controlToReturn != null)
            {
                return controlToReturn;
            }
        }

        return null;
    }

    #endregion
}