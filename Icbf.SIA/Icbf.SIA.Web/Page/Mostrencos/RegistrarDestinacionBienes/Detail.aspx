﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_RegistrarDestinacionBienes_Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 350px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%-- INFORMACIÓN DE LA DENUNCIA --%>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado de la denuncia</td>
                        <td>Fecha de radicado de la denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en correspondencia</td>
                        <td>Fecha radicado en correspondencia</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de identificación</td>
                        <td>Número de identificación</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Primer nombre</td>
                        <td style="width: 55%">Segundo nombre</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Primer apellido</td>
                        <td style="width: 55%">Segundo apellido</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Descripción de la Denuncia
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" class="Background" />
                    </a>
                        </td>
                        <caption>
                        </caption>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- DESTINACIÓN DE BIENES --%>
            <asp:Panel runat="server" ID="PanelDestinacionBienes">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos">Destinación de Bienes
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>Uso del bien
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblDestinacionBienes" RepeatDirection="Vertical" Enabled="false">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DEL MUEBLE O INMUEBLE --%>
            <asp:Panel runat="server" ID="pnlInformacionInmueble">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del Mueble o Inmueble
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de bien
                        </td>
                        <td>Subtipo de bien
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoBien" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSubTipoBien" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Clase de bien
                        </td>
                        <td>Estado del bien   
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtClaseBien" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEstadoBien" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Descripción del bien</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionBien" TextMode="MultiLine" MaxLength="512" Width="900px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DEL TITULO VALOR --%>
            <asp:Panel runat="server" ID="PnlTituloValor">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información Títulos de Valor
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de Título
                        </td>
                        <td>Estado del bien
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoTitulo" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtEstadoBienTitulo" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Descripción del bien</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionBienTitulo" TextMode="MultiLine" MaxLength="512" Width="900px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DE LA VENTA --%>
            <asp:Panel runat="server" ID="PnlInfoVenta">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Venta
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Número de documento de venta
                        </td>
                        <td>Tipo documento de la venta
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroVenta" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDocumentoVenta" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Valor de venta
                        </td>
                        <td>Fecha de venta   
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtValorVenta" Enabled="false"
                                MaxLength="19" onkeypress="SetDecimal(this)"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftValorVenta" runat="server" TargetControlID="txtValorVenta"
                                FilterType="Custom,Numbers" ValidChars=",.$" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaVenta" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- INFORMACION DEL AVALÚO --%>
            <asp:Panel runat="server" ID="PnlInfoAvaluo">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del Avalúo
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha de solicitud del avalúo
                        </td>
                        <td>Valor del Avalúo
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaSolicitudAvaluo" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtValorAvaluo" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftValorAvaluo" runat="server" TargetControlID="txtValorAvaluo"
                                FilterType="Custom,Numbers" ValidChars=",.$" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha del Avalúo
                        </td>
                        <td>Profesional que realizó el avalúo 
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaAvaluo" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtProfesionalAvaluo" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <%-- HISTORICO DE LA DENUNCIA --%>
            <asp:Panel ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" runat="server" 
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; 
                    top: 25%; left: 15%; width: 800px; background-color: white; border: 1px solid #dfdfdf;" 
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; 
                    text-align: center; width: 800px;">
                    <tr>
                        <td style="background-color: #FFFFFF; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; 
                                    margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView ID="gvwHistoricoDenuncia" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False"
                                    AllowPaging="true" AllowSorting="true" GridLines="None" HorizontalAlign="Center"
                                    OnSorting="gvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="gvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" ItemStyle-Width="100" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                        <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" HorizontalAlign="Center" />
                                    <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });
        });
    </script>
</asp:Content>
