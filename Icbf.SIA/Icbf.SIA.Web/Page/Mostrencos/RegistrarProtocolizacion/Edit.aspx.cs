﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.IO;
using System.Globalization;

public partial class Page_Mostrencos_RegistrarProtocolizacion_Edit : GeneralWeb
{
    #region VARIABLES

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdCalidadDenuncianteDetalle;

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdResolucion;

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    private string PageName = "Mostrencos/RegistrarProtocolizacion";

    /// <summary>
    /// Servicio
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region PAGE LOAD

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            CargarDatos();
        }
        else
        {
            this.toolBar.LipiarMensajeError();
            this.toolBar.MostrarBotonNuevo(false);
            this.toolBar.MostrarBotonEditar(false);
            this.toolBar.OcultarBotonGuardar(true);
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            this.toolBar.EstablecerTitulos("Protocolización");
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo cargar Datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            // Codigo para cargar datos 
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.btnDescargar.Visible = false;
            this.pnlArchivo.Visible = false;
            List<DocumentosSolicitadosDenunciaBien> vlstTipoDocumentosBien = null;
            this.ViewState["DocumentosEliminar"] = new List<DocumentosSolicitadosDenunciaBien>();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            ////Se oculta el botón aplicar
            this.btnAplicar.Visible = false;
            this.toolBar.MostrarBotonEditar(false);
            if (vIdDenunciaBien != 0)
            {

                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                //Consulto la información de la denuncia
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

                if (vRegistroDenuncia.IdEstado == 11 || vRegistroDenuncia.IdEstado == 13 || vRegistroDenuncia.IdEstado == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    //this.toolBar.MostrarBotonEditar(false);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString() : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;

                var Protocolizacion = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 20 && x.IdActuacion == 26 && x.IdAccion == 39);
                var Escritura = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 20 && x.IdActuacion == 27 && x.IdAccion == 40);

                if (Protocolizacion == null && Escritura == null)
                {
                    this.toolBar.OcultarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(false);
                }
                else
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                //// Se llena la grilla con los documentos cuyo estado sea diferente al inicial y el eliminado (0)
                ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                this.gvwDocumentacionRecibida.DataBind();

                this.cargarTipoDocumentos();

                CargarGrillas();
                // Obtiene el tipo de trámite de la denuncia

                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        this.tipoDenun.Value = "2";
                        TramiteNotarial tramiteNotarial = new TramiteNotarial();
                        tramiteNotarial = this.vMostrencosService.ConsultarTramiteNotarial(vIdDenunciaBien);
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        this.InfoResolReconoce.Visible = false;
                        this.gvwInformacion.Visible = true;
                        this.Panel4.Visible = true;
                        this.pnlgvwInfoBien.Visible = true;

                        // Se habilitan campos correspondientes.
                        this.fechaSentencia.Enabled = true;
                        this.fechaConstanciaEjecutoriada.Enabled = true;

                        this.radioAdjudicada.Enabled = true;
                        this.radioNoAdjudicada.Enabled = true;
                        this.radioAdjudicadaParcial.Enabled = true;
                        if ((tramiteNotarial.FechaComunicacion != null) && (tramiteNotarial.FechaComunicacion != new DateTime()))
                        {
                            this.fechaComunicacionFinanciera.Date = Convert.ToDateTime(tramiteNotarial.FechaComunicacion);
                        }
                        // Si la decisión de trámite es "Rechazado" y "Judicial".
                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        {
                            TramiteJudicial tramiteJudicial = new TramiteJudicial();
                            tramiteJudicial = this.vMostrencosService.ConsultaTramiteJudicial(vIdDenunciaBien);
                            if ((tramiteNotarial.FechaComunicacion == null) || (tramiteNotarial.FechaComunicacion == new DateTime()))
                            {
                                if ((tramiteJudicial.FechaComunicacion != null) && (tramiteJudicial.FechaComunicacion != new DateTime()))
                                {
                                    this.fechaComunicacionFinanciera.Date = Convert.ToDateTime(tramiteJudicial.FechaComunicacion);
                                }
                            }

                            this.InfoResolReconoce.Visible = true;
                            // Se llena la información de info adjudicación
                            InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);
                            this.txtJuzgadoSentencia.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            this.txtNombreJuzgado.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            this.txtMunicipioJudicial.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                            this.txtDepartamentoDespacho.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;
                            if (vInfoAdjudicacion.FechaConstanciaEjecutoria != new DateTime())
                            {
                                this.fechaConstanciaEjecutoriada.Date = vInfoAdjudicacion.FechaConstanciaEjecutoria;
                            }
                            else
                            {
                                this.fechaConstanciaEjecutoriada.InitNull = true;
                            }
                            if (vInfoAdjudicacion.FechaSentencia != new DateTime())
                            {
                                this.fechaSentencia.Date = vInfoAdjudicacion.FechaSentencia;
                            }
                            else
                            {
                                this.fechaSentencia.InitNull = true;
                            }

                            if (vInfoAdjudicacion.SentidoSentencia != null)
                            {
                                if (vInfoAdjudicacion.SentidoSentencia.Equals("Adjudicada"))
                                {
                                    this.radioAdjudicada.Checked = true;

                                }
                                else if (vInfoAdjudicacion.SentidoSentencia.Equals("No Adjudicada"))
                                {
                                    this.radioNoAdjudicada.Checked = true;
                                }
                                else if (vInfoAdjudicacion.SentidoSentencia.Equals("Adjudicada parcialmente"))
                                {
                                    radioAdjudicadaParcial.Checked = true;
                                }
                            }


                            // Se habilitan campos correspondientes.
                            this.fechaSentencia.Enabled = true;
                            this.radioAdjudicada.Enabled = true;
                            this.radioAdjudicadaParcial.Enabled = true;
                            this.radioNoAdjudicada.Enabled = true;
                        }
                        else
                        {
                            this.Label1.Visible = true;
                            this.Label2.Visible = true;
                            this.fechaSentencia.Enabled = false;
                            this.radioAdjudicada.Enabled = true;
                            this.radioAdjudicadaParcial.Enabled = true;
                            this.radioNoAdjudicada.Enabled = true;
                        }
                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        this.tipoDenun.Value = "1";
                        this.Label1.Visible = true;
                        this.Label2.Visible = true;
                        TramiteJudicial tramiteJudicial = new TramiteJudicial();
                        tramiteJudicial = this.vMostrencosService.ConsultaTramiteJudicial(vIdDenunciaBien);
                        // Se llena la información de info adjudicación
                        InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);
                        this.InfoResolReconoce.Visible = true;
                        this.gvwInformacion.Visible = true;
                        this.Panel4.Visible = false;
                        this.pnlgvwInfoBien.Visible = true;


                        //Llena campos de decisión de adjudicación
                        if ((vInfoAdjudicacion.FechaSentencia != null) && (vInfoAdjudicacion.FechaSentencia != new DateTime()))
                            this.fechaSentencia.Date = Convert.ToDateTime(vInfoAdjudicacion.FechaSentencia);

                        if ((vInfoAdjudicacion.FechaConstanciaEjecutoria != null) && (vInfoAdjudicacion.FechaConstanciaEjecutoria != new DateTime()))
                            this.fechaConstanciaEjecutoriada.Date = Convert.ToDateTime(vInfoAdjudicacion.FechaConstanciaEjecutoria);

                        this.txtJuzgadoSentencia.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        this.txtNombreJuzgado.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        this.txtMunicipioJudicial.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                        this.txtDepartamentoDespacho.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;
                        this.txtFechaEscritura.Date = Convert.ToDateTime(tramiteJudicial.FechaComunicacion);
                        if ((tramiteJudicial.FechaComunicacion != null) && (tramiteJudicial.FechaComunicacion != new DateTime()))
                        {
                            this.fechaComunicacionFinanciera.Date = Convert.ToDateTime(tramiteJudicial.FechaComunicacion);
                        }

                        // Se habilitan campos correspondientes.
                        this.fechaSentencia.Enabled = true;
                        this.fechaConstanciaEjecutoriada.Enabled = true;

                        this.radioAdjudicada.Enabled = true;
                        this.radioNoAdjudicada.Enabled = true;
                        this.radioAdjudicadaParcial.Enabled = true;
                    }
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitadosCache()
    {
        try
        {
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(Convert.ToInt32(this.ddlTipoDocumento.SelectedValue));
                    vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                    vDocumentacionSolicitada.NombreEstado = "Solicitado";
                    if (item.NombreTipoDocumento == vDocumentacionSolicitada.NombreTipoDocumento)
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentacionSolicitada.NombreArchivo = string.Empty;
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = 0;
                vDocumentacionSolicitada.EsNuevo = true;
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrillaDocumentos();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }

            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Captura los valores para cargar la documentacion
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            toolBar.LipiarMensajeError();
            var IdDocumento = GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDocumento").GetType().Name;
            if (IdDocumento != "String")
            {
                if (ValidarDocumentacionRecibida() == false)
                {
                    int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDocumento");
                    int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                    bool vExiste = false;
                    if (this.fulArchivoRecibido.HasFile)
                    {
                        bool respPdf = this.fulArchivoRecibido.FileName.Contains(".pdf");
                        bool respImg = this.fulArchivoRecibido.FileName.Contains(".jpg");
                        string NombreArchivo = string.Empty;
                        if (respPdf)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }
                        else if (respImg)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }

                        if (NombreArchivo.Length > 50)
                        {
                            throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                        }
                        DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                        this.CargarArchivo(vDocumentoCargado);
                        this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", vDocumentoCargado);
                    }
                    DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                    if (vListaDocumentacion == null)
                    {
                        vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
                    }
                    if (vListaDocumentacion.Count() > 0)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                        else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.EsNuevo)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                        else if (IdDocumentosSolicitadosDenunciaBien != 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.IdDocumentosSolicitadosDenunciaBien != IdDocumentosSolicitadosDenunciaBien).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                    }

                    if (!vExiste)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien > 0)
                        {
                            bool vArchivoEsValido = false;
                            if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                            {
                                vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                if (vDocumentacionSolicitada == null)
                                    vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();

                                vArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);

                                if (vArchivoEsValido)
                                {
                                    if (IdDocumentosSolicitadosDenunciaBien == 0)
                                    {
                                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                    }
                                    else
                                    {
                                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                                    }

                                    vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                                    vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                                    vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                                    vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                                    vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                    this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", null);
                                }
                            }
                            else
                            {
                                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                                vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                            }

                            if (vArchivoEsValido)
                            {
                                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
                                if (!vDocumentacionSolicitada.EsNuevo)
                                {
                                    DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                                    vListaDocumentacion.Remove(vDocumentacion);
                                    vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                                }
                                vListaDocumentacion.Add(vDocumentacionSolicitada);
                                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                                this.CargarGrillaDocumentos();
                                this.txtObservacionesSolicitado.Enabled = true;
                                this.FechaSolicitud.Enabled = true;
                                this.ddlTipoDocumento.Enabled = true;
                                this.ddlTipoDocumento.SelectedValue = "-1";
                                this.txtObservacionesSolicitado.Text = string.Empty;
                                this.FechaSolicitud.InitNull = true;
                                this.lblValidacionFechaSolicitud.Visible = false;
                                this.rbtEstadoDocumento.ClearSelection();
                                this.FechaRecibido.InitNull = true;
                                this.txtObservacionesRecibido.Text = string.Empty;
                                this.lblNombreArchivo.Visible = false;
                            }
                        }
                    }
                }
            }
            else
            {
                if (this.ValidarDocumentacionSolicitada() == false)
                {
                    toolBar.LipiarMensajeError();
                    if (this.AdicionarDocumentosSolicitadosCache())
                    {
                        this.CargarGrillaDocumentosSolicitadosYRecibidos();
                        this.ddlTipoDocumento.SelectedValue = "-1";
                    }
                }
                else
                {
                    //toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 MB");
                }

                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = IdDenunciaBien + "/" + this.fulArchivoRecibido.FileName;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien.FileApoderado") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }

            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Cargar las grillas.
    /// </summary>
    private void CargarGrillas()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            int vIdDenunciaBienD = Convert.ToInt32(this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));

            List<InfoMuebleProtocolizacion> vListInfoMueble = new List<InfoMuebleProtocolizacion>();// vMostrencosService.ConsultarInfoMuebleXIdBienDenuncia(vIdDenunciaBien);
            List<ResultFiltroTitulo> vListInfoTitulo = new List<ResultFiltroTitulo>(); //vMostrencosService.ConsultarTituloXIdDenuncia(vIdDenunciaBien);

            List<InfoMuebleProtocolizacion> vListInfoMuebles = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
            List<ResultFiltroTitulo> vListInfoTitulos = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];

            if (vListInfoMuebles != null)
            {
                vListInfoMueble = vListInfoMuebles;
            }
            else
            {
                vListInfoMueble = vMostrencosService.ConsultarInfoMuebleXIdBienDenuncia(vIdDenunciaBien);
            }
            if (vListInfoTitulos != null)
            {
                vListInfoTitulo = vListInfoTitulos;
            }
            else
            {
                vListInfoTitulo = vMostrencosService.ConsultarTituloXIdDenuncia(vIdDenunciaBien);
            }

            if (vListInfoMueble.Count > 0)
            {
                for (int i = 0; i < vListInfoMueble.Count; i++)
                {
                    List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(vListInfoMueble[i].TipoBien);
                    var subtipo = vListaSubTipoBien.Find(x => x.IdSubTipoBien == vListInfoMueble[i].SubTipoBien);
                    vListInfoMueble[i].SubTipoBien = subtipo.IdSubTipoBien;
                    vListInfoMueble[i].NombreSubTipoBien = subtipo.NombreSubTipoBien;
                    List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(vListInfoMueble[i].SubTipoBien.ToString());
                    vListInfoMueble[i].ClaseBien = vListaClaseBien[0].IdClaseBien;
                    vListInfoMueble[i].NombreClaseBien = vListaClaseBien[0].NombreClaseBien;
                    vListInfoMueble[i].NombreTipoBien = (vListInfoMueble[i].TipoBien.Equals("3")) ? vListInfoMueble[i].NombreTipoBien = "INMUEBLE" : vListInfoMueble[i].NombreTipoBien = "MUEBLE";
                    if (vListInfoMueble[i].FechaEscritura != null)
                    {
                        vListInfoMueble[i].FechaEscritura = (!vListInfoMueble[i].FechaEscritura.Equals(string.Empty)) ? Convert.ToDateTime(vListInfoMueble[i].FechaEscritura).ToString("dd/MM/yyyy") : string.Empty;
                    }
                    else
                    {
                        vListInfoMueble[i].FechaEscritura = string.Empty;
                    }
                }
            }


            if (vListInfoMueble.Count > 0)
            {
                ViewState["SortedgvwInfoBien"] = vListInfoMueble;
                this.gvwInformacionMueble.DataSource = vListInfoMueble;
                this.gvwInformacionMueble.DataBind();
            }
            else
            {
                this.gvwInformacionMueble.EmptyDataText = EmptyDataText();
                this.gvwInformacionMueble.DataBind();
            }

            if (vListInfoTitulo.Count > 0)
            {
                ViewState["SortedgwvInfoTipoTitulo"] = vListInfoTitulo;
                this.gvwInformacionTitulo.DataSource = vListInfoTitulo;
                this.gvwInformacionTitulo.DataBind();
            }
            else
            {
                this.gvwInformacionTitulo.EmptyDataText = EmptyDataText();
                this.gvwInformacionTitulo.DataBind();
            }

            this.CargarGrillaHistorico(vIdDenunciaBien);

            // Se llena la grilla con los documentos cuyo estado sea diferente al inicial y el eliminado (0)
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Carga la grilla de la documentacion
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlDocumentacionSolicitada.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlDocumentacionSolicitada.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// GridView Documentos Solicitados y Recibidos
    /// </summary>
    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
        var Data = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        ViewState["SortedgvwDocumentacionRecibida"] = Data;
        gvwDocumentacionRecibida.DataSource = Data;
        gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="vIdDenunciaBien"></param>
    protected void CargarGrillaHistorico(int vIdDenunciaBien)
    {
        // Se llena la grilla de histórico de la denuncia.
        List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
        foreach (HistoricoEstadosDenunciaBien item in vHistorico)
        {
            Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
            item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
            item.Responsable = vUsuario.PrimerNombre;
            if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
            {
                item.Responsable += " " + vUsuario.SegundoNombre;
            }

            item.Responsable += " " + vUsuario.PrimerApellido;
            if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
            {
                item.Responsable += " " + vUsuario.SegundoApellido;
            }
        }
        this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
        this.tbHistoria.DataSource = vHistorico;
        this.tbHistoria.DataBind();
    }

    /// <summary>
    /// Cargar la lista del tipo sub Bien
    /// </summary>
    /// <param name="pIdSubTipoBien"></param>
    private void CargarListaClasebien(string pIdSubTipoBien)
    {
        try
        {
            List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(pIdSubTipoBien);
            vListaClaseBien = vListaClaseBien.OrderBy(p => p.NombreClaseBien).ToList();
            this.ddlClasebien.DataSource = vListaClaseBien;
            this.ddlClasebien.DataTextField = "NombreClaseBien";
            this.ddlClasebien.DataValueField = "IdClaseBien";
            this.ddlClasebien.DataBind();
            this.ddlClasebien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClasebien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar la lista del tipo bien
    /// </summary>
    /// <param name="pIdTipoBien"></param>
    private void CargarListaSubTipoBien(string pIdTipoBien)
    {
        try
        {
            List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(pIdTipoBien);
            vListaSubTipoBien = vListaSubTipoBien.OrderBy(p => p.NombreSubTipoBien).ToList();
            this.ddlSubTipoBien.DataSource = vListaSubTipoBien;
            this.ddlSubTipoBien.DataTextField = "NombreSubTipoBien";
            this.ddlSubTipoBien.DataValueField = "IdSubTipoBien";
            this.ddlSubTipoBien.DataBind();
            this.ddlSubTipoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlSubTipoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Control de los botones en el toolbar
    /// </summary>
    public void ControlBotones()
    {
        this.toolBar.MostrarBotonNuevo(true);
        this.toolBar.MostrarBotonEditar(true);
        this.toolBar.OcultarBotonGuardar(false);
    }

    /// <summary>
    /// Control de campos luego de guardar
    /// </summary>
    /// <param name="activar"></param>
    public void ControlDeCampos(bool activar)
    {
        this.fechaSentencia.Enabled = activar;
        this.fechaConstanciaEjecutoriada.Enabled = activar;
        this.radioAdjudicada.Enabled = activar;
        this.radioNoAdjudicada.Enabled = activar;
        this.radioAdjudicadaParcial.Enabled = activar;
        this.fechaComunicacionFinanciera.Enabled = activar;
        this.ddlTipoDocumento.Enabled = activar;
        this.FechaSolicitud.Enabled = activar;
        this.rbtEstadoDocumento.Enabled = activar;
        this.FechaRecibido.Enabled = activar;
        this.txtObservacionesRecibido.Enabled = activar;
        this.btnAdd.Enabled = activar;
        this.btnAplicar.Enabled = activar;
        this.FechaSolicitud.Enabled = activar;
        this.txtObservacionesSolicitado.Enabled = activar;
        this.fulArchivoRecibido.Enabled = activar;
        this.gvwDocumentacionRecibida.Enabled = activar;
        this.gvwInformacionMueble.Enabled = activar;
        this.gvwInformacionTitulo.Enabled = activar;
        this.btnInfoDenuncia.Enabled = activar;
    }

    /// <summary>
    /// Control del panel documentación recibida, botones add y aplicar
    /// </summary>
    private void ControlPanelDocRecibida(bool activar)
    {
        this.btnAplicar.Visible = activar;
        this.rbtEstadoDocumento.Enabled = activar;
        this.FechaRecibido.Enabled = activar;
        this.fulArchivoRecibido.Enabled = activar;
        this.txtObservacionesRecibido.Enabled = activar;
    }

    /// <summary>
    /// Control de panel documentación solicitada
    /// </summary>
    /// <param name="activa">Estado del campo</param>
    private void ControlPanelDocSolicitada(bool activa)
    {
        this.btnAdd.Visible = false;
        this.txtObservacionesSolicitado.Enabled = activa;
        this.FechaSolicitud.Enabled = activa;
        this.ddlTipoDocumento.Enabled = activa;
    }

    /// <summary>
    /// Cargar los tipos de documentación
    /// </summary>
    private void cargarTipoDocumentos()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
            vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
            vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
            this.ddlTipoDocumento.DataTextField = "NombreTipoDocumento";
            this.ddlTipoDocumento.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlTipoDocumento.DataBind();
            this.ddlTipoDocumento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoDocumento.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Desactiva paneles y campos
    /// </summary>
    private void DeshabilitarCamposyPaneles()
    {
        this.fechaSentencia.Enabled = false;
        this.fechaComunicacionFinanciera.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = false;
        this.radioAdjudicada.Enabled = false;
        this.radioNoAdjudicada.Enabled = false;
        this.radioAdjudicadaParcial.Enabled = false;
        this.gvwInformacionTitulo.Enabled = false;
        this.gvwInformacionMueble.Enabled = false;
        this.btnAplicarMuebleInmueble.Enabled = false;
        this.btnInfoDenuncia.Enabled = false;
    }

    /// <summary>
    /// Método para guardar un RegistroDenuncia Nuevo o Editado.
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));

            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            if (vRegistroDenuncia.TipoTramite != null)
            {

                bool bandera = true;
                if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                {
                    if (!this.radioAdjudicada.Checked && !this.radioNoAdjudicada.Checked && !this.radioAdjudicadaParcial.Checked)
                    {
                        this.reqSentido.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.reqSentido.Visible = false;
                    }
                    DateTime fechaComunicacion = this.fechaConstanciaEjecutoriada.Date;
                    if (fechaComunicacion.ToString().Equals("1/01/1900 12:00:00 a. m."))
                    {
                        this.reqFecha.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.reqFecha.Visible = false;
                    }
                }
                else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                {
                    InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                    if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                    {
                        if (!this.radioAdjudicada.Checked && !this.radioNoAdjudicada.Checked && !this.radioAdjudicadaParcial.Checked)
                        {
                            this.reqSentido.Visible = true;
                            bandera = false;
                        }
                        else
                        {
                            this.reqSentido.Visible = false;
                        }
                        DateTime fechaComunicacion = this.fechaConstanciaEjecutoriada.Date;
                        if (fechaComunicacion.ToString().Equals("1/01/1900 12:00:00 a. m."))
                        {
                            this.reqFecha.Visible = true;
                            bandera = false;
                        }
                        else
                        {
                            this.reqFecha.Visible = false;
                        }
                    }
                }
                else
                {
                    this.reqSentido.Visible = false;
                    this.reqFecha.Visible = false;
                    bandera = true;
                }
                if (bandera)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);

                        string vJuzgadoSentencia = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        string vNombreJuzgado = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        string vMunicipioJudicial = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                        string vDepartamentoDespacho = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;

                        DateTime vfechaSentencia = this.fechaSentencia.Date;
                        DateTime vfechaConstancia = this.fechaConstanciaEjecutoriada.Date;
                        string vSentidoSentencia = string.Empty;
                        TramiteJudicial vTramiteJudicial = new TramiteJudicial();
                        if (this.radioAdjudicada.Checked)
                        {
                            vTramiteJudicial.SentidoSentencia = "Adjudicada";
                            vTramiteJudicial.IdSentidoSentencia = 1;
                        }
                        else if (radioNoAdjudicada.Checked)
                        {
                            vTramiteJudicial.SentidoSentencia = "No Adjudicada";
                            vTramiteJudicial.IdSentidoSentencia = 2;
                        }
                        else if (radioAdjudicadaParcial.Checked)
                        {
                            vTramiteJudicial.SentidoSentencia = "Adjudicada parcialmente";
                            vTramiteJudicial.IdSentidoSentencia = 3;
                        }

                        string vUsuario = this.GetSessionUser().NombreUsuario;


                        vTramiteJudicial.IdTipoTramite = 1;
                        vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                        vTramiteJudicial.TipoTramite = "JUDICIAL";
                        vTramiteJudicial.FechaSentencia = vfechaSentencia;
                        vTramiteJudicial.FechaConstanciaEjecutoria = vfechaConstancia;
                        vTramiteJudicial.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                        vTramiteJudicial.UsuarioModifica = vUsuario;
                        vTramiteJudicial.FechaModifica = DateTime.Now;
                        vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                        this.InformacionAudioria(vTramiteJudicial, this.PageName, SolutionPage.Edit);
                        int vResultadoTrx = this.vMostrencosService.ActualizarTramiteJudicialxIdDenunciaBien(vTramiteJudicial);

                        if (vResultadoTrx > 0)
                        {
                            /// Grilla Titulo Valor

                            List<ResultFiltroTitulo> vListInfoTitulo = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];
                            if (vListInfoTitulo != null)
                            {
                                vListInfoTitulo.ForEach(item =>
                                {
                                    if (item.Modificado)
                                    {
                                        this.InformacionAudioria(vTramiteJudicial, this.PageName, SolutionPage.Edit);
                                        this.vMostrencosService.ActualizarTituloBienxIdDenuncia(item.estado, vIdDenunciaBien, item.FechaAdjudicados, item.IdTituloValor);
                                    }
                                });
                            }

                            /// Grilla Mueble

                            List<InfoMuebleProtocolizacion> vListInfoMueble = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                            if (vListInfoMueble != null)
                            {
                                vListInfoMueble.ForEach(item =>
                                {
                                    if (item.Modificado)
                                    {
                                        this.InformacionAudioria(vTramiteJudicial, this.PageName, SolutionPage.Edit);
                                        this.vMostrencosService.ActualizarEstadoBienxIdDenuncia(item.EstadoBien, vIdDenunciaBien, item.FechaAdjudicado, item.IdMuebleInmueble);
                                    }
                                });
                            }

                            if (vListInfoTitulo == null && vListInfoMueble == null)
                            {
                                DateTime fechaComunicacion = this.fechaComunicacionFinanciera.Date;
                                if (!fechaComunicacion.ToString().Equals("1/01/1900 12:00:00 a. m."))
                                {
                                    this.InformacionAudioria(vTramiteJudicial, this.PageName, SolutionPage.Edit);
                                    this.vMostrencosService.ActualizarFechaComunicacionTitulo(vIdDenunciaBien, fechaComunicacion);
                                }
                            }


                            this.FechaRecibido.InitNull = true;
                            this.FechaRecibido.Enabled = false;
                            this.ddlTipoDocumento.SelectedValue = "-1";
                            this.ddlTipoDocumento.Enabled = false;
                            this.txtObservacionesSolicitado.Text = string.Empty;
                            this.txtObservacionesSolicitado.Enabled = false;
                            this.FechaSolicitud.InitNull = true;
                            this.FechaSolicitud.Enabled = false;
                            this.lblValidacionFechaSolicitud.Visible = false;
                            this.rbtEstadoDocumento.ClearSelection();
                            this.txtObservacionesRecibido.Text = string.Empty;
                            this.radioAdjudicada.Enabled = false;
                            this.radioNoAdjudicada.Enabled = false;
                            this.radioAdjudicadaParcial.Enabled = false;
                            this.txtObservacionesRecibido.Enabled = false;
                            this.lblNombreArchivo.Visible = false;
                            this.lblNombreArchivo.Text = string.Empty;
                            this.btnAplicar.Visible = false;
                            this.btnAdd.Visible = false;
                            this.toolBar.OcultarBotonBuscar(false);
                            this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                        }

                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {

                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        {
                            InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);

                            string vJuzgadoSentencia = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            string vNombreJuzgado = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            string vMunicipioJudicial = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                            string vDepartamentoDespacho = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;

                            DateTime vfechaSentencia = this.fechaSentencia.Date;
                            TramiteJudicial vTramiteJudicial = new TramiteJudicial();


                            if (this.radioAdjudicada.Checked)
                            {
                                vTramiteJudicial.SentidoSentencia = "Adjudicada";
                                vTramiteJudicial.IdSentidoSentencia = 1;

                            }
                            else if (this.radioNoAdjudicada.Checked)
                            {
                                vTramiteJudicial.SentidoSentencia = "No Adjudicada";
                                vTramiteJudicial.IdSentidoSentencia = 2;
                            }
                            else if (this.radioAdjudicadaParcial.Checked)
                            {
                                vTramiteJudicial.SentidoSentencia = "Adjudicada parcialmente";
                                vTramiteJudicial.IdSentidoSentencia = 3;
                            }

                            vTramiteJudicial.IdTipoTramite = 1;
                            vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                            vTramiteJudicial.TipoTramite = "JUDICIAL";
                            vTramiteJudicial.FechaSentencia = vfechaSentencia;
                            vTramiteJudicial.UsuarioModifica = this.GetSessionUser().NombreUsuario; ;
                            vTramiteJudicial.FechaModifica = DateTime.Now;
                            vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                            if (this.fechaComunicacionFinanciera.Date != new DateTime()  || this.fechaComunicacionFinanciera.Date.ToString() != "1/01/1900 12:00:00 a. m.")
                            {
                                vTramiteJudicial.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                            }
                            else
                            {
                                vTramiteJudicial.FechaComunicacion = null;
                            }
                            

                            this.InformacionAudioria(vTramiteJudicial, this.PageName, SolutionPage.Add);
                            int vResultadoTrx = this.vMostrencosService.ActualizarTramiteJudicialxIdDenunciaBien(vTramiteJudicial);
                        }

                        /// Grilla Titulo Valor

                        List<ResultFiltroTitulo> vListInfoTitulo = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];
                        if (vListInfoTitulo != null)
                        {
                            vListInfoTitulo.ForEach(item =>
                            {
                                if (item.Modificado)
                                {
                                    this.InformacionAudioria(vListInfoTitulo[0], this.PageName, SolutionPage.Edit);
                                    this.vMostrencosService.ActualizarTituloBienxIdDenuncia(item.estado, vIdDenunciaBien, item.FechaAdjudicados, item.IdTituloValor);
                                }
                            });
                        }


                        /// Grilla Mueble
                        List<InfoMuebleProtocolizacion> vListInfoMueble = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                        if (vListInfoMueble != null)
                        {
                            vListInfoMueble.ForEach(item =>
                            {
                                if (item.Modificado)
                                {
                                    this.InformacionAudioria(vListInfoMueble[0], this.PageName, SolutionPage.Edit);
                                    this.vMostrencosService.ActualizarEstadoBienxIdDenuncia(item.EstadoBien, vIdDenunciaBien, item.FechaAdjudicado, item.IdMuebleInmueble);

                                    DateTime FechaEscritura = (!item.FechaEscritura.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaEscritura) : new DateTime();
                                    ulong NumeroEscritura = (!item.NumeroEscritura.Equals(string.Empty)) ? Convert.ToUInt64(item.NumeroEscritura) : 0;
                                    decimal NumeroRegistro = (!item.NumeroRegistro.Equals(string.Empty)) ? Convert.ToDecimal(item.NumeroRegistro) : 0;
                                    DateTime FechaIngresoICBF = (!item.FechaIngresoICBF.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaIngresoICBF) : new DateTime();
                                    DateTime FechaInstrumentosPublicos = (!item.FechaInstrumentosPublicos.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaInstrumentosPublicos.ToString()) : new DateTime();
                                    DateTime FechaComunicacion = (!item.FechaComunicacion.Equals(null) && !item.FechaComunicacion.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaComunicacion.ToString()) : new DateTime();
                                    this.InformacionAudioria(vListInfoMueble[0], this.PageName, SolutionPage.Edit);
                                    this.vMostrencosService.ActualizarTramiteNotarialxIdDenuncia(vIdDenunciaBien, FechaEscritura, NumeroEscritura, NumeroRegistro, FechaIngresoICBF,
                                            FechaInstrumentosPublicos, FechaComunicacion);
                                }
                            });
                        }

                        //if (vListInfoTitulo == null && vListInfoMueble == null)
                        //{
                            DateTime fechaComunicacion = this.fechaComunicacionFinanciera.Date;
                            if (!fechaComunicacion.ToString().Equals("1/01/1900 12:00:00 a. m."))
                            {
                                this.InformacionAudioria(new TramiteNotarial(), this.PageName, SolutionPage.Edit);
                                this.vMostrencosService.ActualizarFechaComunicacionTitulo(vIdDenunciaBien, fechaComunicacion);
                            }
                        //}

                        this.DeshabilitarCamposyPaneles();
                        this.toolBar.MostrarBotonConsultar();
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.txtObservacionesSolicitado.Enabled = true;
                        this.FechaSolicitud.InitNull = true;
                        this.FechaSolicitud.Enabled = false;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.ddlTipoDocumento.Enabled = true;
                        this.txtObservacionesSolicitado.Text = string.Empty;
                        this.lblValidacionFechaSolicitud.Visible = false;
                        this.rbtEstadoDocumento.ClearSelection();
                        this.FechaRecibido.InitNull = true;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.lblNombreArchivo.Visible = false;
                        this.lblNombreArchivo.Text = string.Empty;
                        this.btnAplicar.Visible = false;
                        this.btnAdd.Visible = false;
                        this.toolBar.MostrarBotonNuevo(true);
                        this.toolBar.MostrarBotonEditar(true);
                        this.toolBar.OcultarBotonGuardar(false);
                        this.toolBar.OcultarBotonBuscar(false);
                        this.radioAdjudicado.Enabled = false;
                        this.radioNoAdjudicado.Enabled = false;
                        this.btnAplicarActualizacionTitulo.Visible = false;
                        this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                    }
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];
                    string vRuta = string.Empty;

                    if (vListaDocumentos != null)
                    {
                        if (vListaDocumentosEliminar != null)
                        {
                            foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosEliminar)
                            {
                                if (item.IdDocumentosSolicitadosDenunciaBien > 0)
                                {
                                    DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
                                    vdo.IdDocumentosSolicitadosDenunciaBien = item.IdDocumentosSolicitadosDenunciaBien;
                                    vdo.IdEstadoDocumento = 6;
                                    vdo.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                                    vdo.RutaArchivo = "";
                                    this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                                    this.vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(vdo);
                                }
                            }
                            this.ViewState["DocumentosEliminar"] = null;
                        }

                        vListaDocumentos.ForEach(item =>
                        {
                            item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            if (item.EsNuevo == true)
                                this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                            else
                                this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
                        });
                    }

                    this.toolBar.MostrarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(true);
                    this.toolBar.OcultarBotonBuscar(false);
                    this.toolBar.OcultarBotonGuardar(false);
                    this.CargarGrillas();
                    this.ControlDeCampos(false);
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda archivos fisico en el servidor
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    /// <param name="pPosition"></param>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        bool Esresultado = false;
        try
        {
            HttpFileCollection files = Request.Files;
            if (files.Count > 0)
            {
                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                string extJgp = ".JPG".ToLower();
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos.");
                }
                int tamano = this.fulArchivoRecibido.FileName.Length;
                if (extFile != extPdf && extFile != extJgp)
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Mb");
                }

                HttpPostedFile file = files[pPosition];
                if (file.ContentLength > 0)
                {
                    string rutaParcial = string.Empty;
                    string carpetaBase = string.Empty;
                    string filePath = string.Empty;
                    if (extFile.Equals(".pdf"))
                    {
                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");

                    }
                    else if (extFile.ToLower().Equals(".jpg"))
                    {

                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                    }
                    bool EsexisteCarpeta = System.IO.Directory.Exists(carpetaBase);
                    if (!EsexisteCarpeta)
                    {
                        System.IO.Directory.CreateDirectory(carpetaBase);
                        Esresultado = true;
                    }
                    this.vFileName = rutaParcial;
                    file.SaveAs(filePath);
                    Esresultado = true;
                }
            }
        }
        catch (HttpRequestValidationException httpex)
        {
            throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return Esresultado;
    }

    /// <summary>
    /// Guardar el documento.
    /// </summary>
    /// <param name="pDocumento">The p documento.</param>
    public void GuardarDocumento(DocumentosSolicitadosDenunciaBien pDocumento)
    {
        try
        {
            if (pDocumento.IdDocumentosSolicitadosDenunciaBien == 0)
            {
                this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(pDocumento);
                this.CargarGrillas();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Limpia la informacion del panel del mueble inmueble
    /// </summary>
    private void LimpiarInformacionMuebleInmueble()
    {
        this.ddlTipoBien.SelectedValue = "-1";
        this.ddlSubTipoBien.SelectedValue = "-1";
        this.ddlClasebien.SelectedValue = "-1";
        this.radioAdjudicado.Checked = false;
        this.radioNoAdjudicado.Checked = false;
        this.dtFechaAdjudicacionBien.InitNull = true;
        this.dtFechaAdjudicacionBien.Enabled = false;
        this.txtDescripcionBien.Text = string.Empty;
        this.radioAdjudicado.Checked = false;
        this.radioAdjudicado.Enabled = false;
        this.radioNoAdjudicado.Checked = false;
        this.radioNoAdjudicado.Enabled = false;
        this.btnAplicarMuebleInmueble.Visible = false;

        this.txtFechaEscritura.InitNull = true;
        this.txtNumeroEscritura.Text = null;
        this.txtNotariaDondeRegistra.Text = null;
        this.txtNumeroRegistro.Text = null;
        this.txtFechaIngresoICBF.InitNull = true;
        this.txtFechaInstrumentosPublicos.InitNull = true;

        this.txtFechaEscritura.Enabled = false;
        this.txtNumeroEscritura.Enabled = false;
        this.txtNotariaDondeRegistra.Enabled = false;
        this.txtNumeroRegistro.Enabled = false;
        this.txtFechaIngresoICBF.Enabled = false;
        this.txtFechaInstrumentosPublicos.Enabled = false;
        this.fechaComunicacionFinanciera.InitNull = true;
    }

    /// <summary>
    /// Limpia la informacion del panel del titulo de valor
    /// </summary>
    private void LimpiarInformacionTituloValor()
    {
        this.txtTipoTitulo.Text = string.Empty;
        this.txtFechaAdjudicacionTitulo.InitNull = true;
        this.txtFechaAdjudicacionTitulo.Enabled = false;
        this.txtDescripcionBienTitulo.Text = string.Empty;
        this.rbnEstadoBnTituloAdjudicado.Checked = false;
        this.rbnEstadoBnTituloAdjudicado.Enabled = false;
        this.rbnEstadoBnTituloNoAdjudicado.Checked = false;
        this.rbnEstadoBnTituloNoAdjudicado.Enabled = false;
        this.btnAplicarActualizacionTitulo.Visible = false;
    }

    /// <summary>
    /// Habilitar panel de recibidos
    /// </summary>
    /// <param name="EsHabilitado"></param>
    private void HabilitarPanelRecibido(Boolean EsHabilitado)
    {
        try
        {
            this.rbtEstadoDocumento.Enabled = EsHabilitado;
            this.fulArchivoRecibido.Enabled = EsHabilitado;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Validar Documentación Recibida
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblrqfEstadoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblrqfEstadoRecibido.Visible = false;

                if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblrqfFechaRecibido.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblrqfFechaRecibido.Visible = false;
                    vEsrequerido = false;

                    //Validar Campo Fecha Recibido
                    if (this.FechaRecibido.Date > DateTime.Now)
                    {
                        this.lblValidacionFechaMaxima.Visible = true;
                        vEsrequerido = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblValidacionFechaMaxima.Visible = false;
                        vEsrequerido = false;
                    }

                    //Validar Campo Fecha Recibido
                    if (this.FechaRecibido.Date < this.FechaSolicitud.Date)
                    {
                        this.lblValidacionFechaSolicitud.Visible = true;
                        vEsrequerido = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblValidacionFechaSolicitud.Visible = false;
                        vEsrequerido = false;
                    }
                }

                //Validar Campo Nombre Archivo
                if (this.fulArchivoRecibido.FileName == "")
                {
                    this.lblRqfNombreArchivo.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblRqfNombreArchivo.Visible = false;
                    vEsrequerido = false;
                }

                //Validar Observaciones de documento recibido
                if (this.txtObservacionesRecibido.Text == string.Empty)
                {
                    this.lblObservacionesRecibido.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblObservacionesRecibido.Visible = false;
                    vEsrequerido = false;
                }
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Solicitada
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionSolicitada()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblReqTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblReqTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Fecha Recibido
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblReqFechaSolicitud.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblReqFechaSolicitud.Visible = false;
                vEsrequerido = false;
            }

            //Validar Observaciones Documento Solciitado
            if (this.txtObservacionesSolicitado.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento que selecciona el tipo de bien
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoBien_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (this.ddlTipoBien.SelectedValue)
        {
            case "-1":
                this.hfTipoParametro.Value = string.Empty;
                break;

            case "3":
                this.CargarListaSubTipoBien("3");
                break;

            case "2":
                this.CargarListaSubTipoBien("2");
                break;
        }

        this.ddlTipoBien.Focus();
    }

    /// <summary>
    /// Evento que carga la informacion del subTipo del Bien
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSubTipoBien_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!this.ddlSubTipoBien.SelectedValue.Equals("-1"))
        {
            this.CargarListaClasebien(this.ddlSubTipoBien.SelectedValue);
            string vTipoParametro = this.ddlSubTipoBien.SelectedValue.Substring(0, 3);
            if (!vTipoParametro.Equals("308"))
            {
                this.hfTipoParametro.Value = vTipoParametro;
                this.hfCodigoParametro.Value = this.ddlSubTipoBien.SelectedValue;
            }
        }

        this.ddlSubTipoBien.Focus();
    }

    /// <summary>
    /// evento page change documentación recibida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// Ordenamiento de la grilla información de titulo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionTitulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// Validación para el valor por defecto de la fecha adjudicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionTitulo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int LoopCounter;

        // Variable for starting index. Use this to make sure the tabindexes start at a higher
        // value than any other controls above the gridview. 
        // Header row indexes will be 110, 111, 112...
        // First data row will be 210, 211, 212... 
        // Second data row 310, 311, 312 .... and so on
        int tabIndexStart = 10;

        for (LoopCounter = 0; LoopCounter < e.Row.Cells.Count; LoopCounter++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                // Check to see if the cell contains any controls
                if (e.Row.Cells[LoopCounter].Controls.Count > 0)
                {
                    // Set the TabIndex. Increment RowIndex by 2 because it starts at -1
                    ((LinkButton)e.Row.Cells[LoopCounter].Controls[0]).TabIndex = -1;//short.Parse((e.Row.RowIndex + 2).ToString() + tabIndexStart++.ToString());
                }
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.TabIndex = -1;
            if (e.Row.Cells[3].Text.Equals("01/01/0001") || e.Row.Cells[3].Text.Equals("1/01/1900 12:00:00 a. m.") || e.Row.Cells[3].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbHistoria.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.tbHistoria.DataSource = vList;
                this.tbHistoria.DataBind();
            }
            else
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                this.CargarGrillaHistorico(vIdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();
        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }
        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdFase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdFase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdActuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdActuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdAccion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdAccion).ToList();
                }

                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.tbHistoria.DataSource = vResult;
        this.tbHistoria.DataBind();
    }

    /// <summary>
    /// Evento que se ejecuta cuando se selecciona un comando de la grilla de Info muebles
    /// </summary>
    /// <param name="sender">sender attribute</param>
    /// <param name="e">e attribute</param>
    protected void gvwInfoBien_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Editar")
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                this.btnAplicarActualizacionTitulo.Enabled = true;
                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);

                        // Si la decisión de trámite es "Rechazado" y "Judicial".
                        // if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        if (gvr.Cells[6].Text.ToUpper().Equals("ADJUDICADO") && (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                    && !vInfoEscrituracion.FechaSolemnizacion.ToString().Equals("1900-01-01 00:00:00.000"))
                        {
                            this.radioAdjudicado.Enabled = true;
                            this.radioNoAdjudicado.Enabled = true;
                            string vTipoBien = gvr.Cells[0].Text;
                            string vSubTipoBien = gvr.Cells[1].Text;
                            string vClaseBien = gvr.Cells[2].Text;
                            string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                            string vFechaAdjudicadoBien = gvr.Cells[5].Text;
                            this.idBien.Text = gvr.Cells[15].Text;
                            this.CargarListaSubTipoBien(vTipoBien);
                            this.CargarListaClasebien(vSubTipoBien);
                            this.ddlTipoBien.SelectedValue = vTipoBien;
                            this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                            this.ddlClasebien.SelectedValue = vClaseBien;
                            this.txtDescripcionBien.Text = vDescripcionBien;
                            this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                            this.btnAplicarMuebleInmueble.Visible = true;

                            // Informacion de escrituracion
                            if (!gvr.Cells[6].Text.Equals(string.Empty))
                            {
                                DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[6].Text);
                                this.txtFechaEscritura.Date = fechaEscrituracion;
                            }

                            string numeroEscritura = gvr.Cells[7].Text;
                            string notariaDondeRegistra = gvr.Cells[8].Text;
                            this.txtFechaEscritura.Enabled = true;
                            this.txtNumeroEscritura.Enabled = true;
                            this.txtNumeroEscritura.Text = numeroEscritura;
                            this.txtNotariaDondeRegistra.Enabled = false;
                            this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                            this.txtNumeroRegistro.Enabled = true;
                            this.txtFechaIngresoICBF.Enabled = true;
                            this.txtFechaInstrumentosPublicos.Enabled = true;
                        }
                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        this.radioAdjudicado.Enabled = true;
                        this.radioNoAdjudicado.Enabled = true;
                        string vTipoBien = gvr.Cells[0].Text;
                        string vSubTipoBien = gvr.Cells[1].Text;
                        string vClaseBien = gvr.Cells[2].Text;
                        string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                        string vFechaAdjudicadoBien = gvr.Cells[5].Text;
                        this.idBien.Text = gvr.Cells[15].Text;
                        this.CargarListaSubTipoBien(vTipoBien);
                        this.CargarListaClasebien(vSubTipoBien);
                        this.ddlTipoBien.SelectedValue = vTipoBien;
                        this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                        this.ddlClasebien.SelectedValue = vClaseBien;
                        this.txtDescripcionBien.Text = vDescripcionBien;
                        this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                        this.btnAplicarMuebleInmueble.Visible = true;
                    }
                }



            }

            this.toolBar.LipiarMensajeError();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Ordena grilla de información Mueble
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionMueble_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// Informacion mueble row command event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionMueble_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            string vSentidoSentencia = string.Empty;
            if (this.radioAdjudicada.Text != "")
            {
                vSentidoSentencia = this.radioAdjudicada.Text;
            }
            else if (this.radioNoAdjudicada.Text != "")
            {
                vSentidoSentencia = this.radioNoAdjudicada.Text;
            }
            else if (this.radioAdjudicadaParcial.Text != "")
            {
                vSentidoSentencia = this.radioAdjudicadaParcial.Text;
            }
            if (e.CommandName == "Editar")
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));

                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        string vEstadoBien = gvr.Cells[6].Text;
                        // Si la decisión de trámite es "Rechazado" y "Judicial".
                        if (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper().Equals("RECHAZADO") && vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper().Equals("JUDICIAL"))
                        {
                            if (vSentidoSentencia.ToUpper().Equals("ADJUDICADA") || vSentidoSentencia.ToUpper().Equals("ADJUDICADA PARCIALMENTE"))
                            {
                                this.radioAdjudicado.Enabled = true;
                                this.radioNoAdjudicado.Enabled = true;
                                string vTipoBien = gvr.Cells[0].Text;
                                string vSubTipoBien = gvr.Cells[2].Text;
                                string vClaseBien = gvr.Cells[4].Text;
                                string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                                string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                                this.idBien.Text = gvr.Cells[15].Text;
                                this.CargarListaSubTipoBien(vTipoBien);
                                this.CargarListaClasebien(vSubTipoBien);
                                this.ddlTipoBien.SelectedValue = vTipoBien;
                                this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                                this.ddlClasebien.SelectedValue = vClaseBien;
                                this.txtDescripcionBien.Text = vDescripcionBien;
                                if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                                    this.dtFechaAdjudicacionBien.InitNull = true;
                                else
                                    this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);

                                this.btnAplicarMuebleInmueble.Visible = true;

                                //// Informacion de escrituracion

                                if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                                {
                                    DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                                    this.txtFechaEscritura.Date = fechaEscrituracion;
                                }
                                string numeroEscritura = gvr.Cells[7].Text;
                                string notariaDondeRegistra = gvr.Cells[8].Text;
                                this.txtNumeroEscritura.Text = numeroEscritura;
                                this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                            }
                        }
                        else
                        {
                            this.txtNumeroRegistro.Text = (!gvr.Cells[12].Text.Equals("&nbsp;")) ? gvr.Cells[12].Text : string.Empty;
                            this.radioAdjudicado.Enabled = true;
                            this.radioNoAdjudicado.Enabled = true;
                            string vTipoBien = gvr.Cells[0].Text;
                            string vSubTipoBien = gvr.Cells[2].Text;
                            string vClaseBien = gvr.Cells[4].Text;
                            string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                            string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                            this.idBien.Text = gvr.Cells[15].Text;
                            this.CargarListaSubTipoBien(vTipoBien);
                            this.CargarListaClasebien(vSubTipoBien);
                            this.ddlTipoBien.SelectedValue = vTipoBien;
                            this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                            this.ddlClasebien.SelectedValue = vClaseBien;
                            this.txtDescripcionBien.Text = vDescripcionBien;
                            if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                            {
                                this.dtFechaAdjudicacionBien.InitNull = true;
                            }
                            else
                            {
                                this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                            }
                            if (gvr.Cells[13].Text == "&nbsp;")
                            {
                                this.txtFechaIngresoICBF.InitNull = true;
                            }
                            else
                            {
                                this.txtFechaIngresoICBF.Date = Convert.ToDateTime(gvr.Cells[13].Text);
                            }
                            if (gvr.Cells[14].Text == "&nbsp;")
                            {
                                this.txtFechaInstrumentosPublicos.InitNull = true;
                            }
                            else
                            {
                                this.txtFechaInstrumentosPublicos.Date = Convert.ToDateTime(gvr.Cells[14].Text);
                            }

                            this.btnAplicarMuebleInmueble.Visible = true;

                            if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                            {
                                DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                                this.txtFechaEscritura.Date = fechaEscrituracion;
                            }
                            string numeroEscritura = gvr.Cells[10].Text;
                            string notariaDondeRegistra = gvr.Cells[11].Text;
                            this.txtNumeroEscritura.Text = numeroEscritura;
                            this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;

                            if (vEstadoBien.ToUpper().Equals("ADJUDICADO") && (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                            && !vInfoEscrituracion.FechaSolemnizacion.ToString().Equals("1900-01-01 00:00:00.000"))
                            {
                                this.CamposEscriturizacion(true);
                            }
                            else
                            {
                                this.CamposEscriturizacion(false);
                                this.toolBar.MostrarMensajeError("El estado del bien debe ser Adjudicado, la Decisión del Trámite Notarial debe ser Aceptado y debe tener registrado la Fecha de Solemnización");
                            }

                        }

                        if (vEstadoBien.ToUpper().Equals("ADJUDICADO"))
                        {
                            this.radioAdjudicado.Checked = true;
                            this.radioNoAdjudicado.Checked = false;
                            this.radioAdjudicado.Enabled = true;
                            this.radioNoAdjudicado.Enabled = true;
                            this.dtFechaAdjudicacionBien.Enabled = true;
                        }
                        else if (vEstadoBien.ToUpper().Equals("NO ADJUDICADO"))
                        {
                            this.radioAdjudicado.Checked = false;
                            this.radioNoAdjudicado.Checked = true;
                            this.radioAdjudicado.Enabled = true;
                            this.radioNoAdjudicado.Enabled = true;
                        }
                    }
                    else if ((vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL") && vSentidoSentencia.ToUpper().Equals("ADJUDICADA")) || (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL") && vSentidoSentencia.ToUpper().Equals("ADJUDICADA PARCIALMENTE")))
                    {
                        this.radioAdjudicado.Enabled = true;
                        this.radioNoAdjudicado.Enabled = true;

                        string vTipoBien = gvr.Cells[0].Text;
                        string vSubTipoBien = gvr.Cells[2].Text;
                        string vClaseBien = gvr.Cells[4].Text;
                        string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                        string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                        this.CargarListaSubTipoBien(vTipoBien);
                        this.CargarListaClasebien(vSubTipoBien);
                        this.ddlTipoBien.SelectedValue = vTipoBien;
                        this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                        this.ddlClasebien.SelectedValue = vClaseBien;
                        this.txtDescripcionBien.Text = vDescripcionBien;
                        if (vFechaAdjudicadoBien == "" || vFechaAdjudicadoBien == "")
                        {
                            this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                        }
                        else
                        {
                            this.dtFechaAdjudicacionBien.InitNull = true;
                        }
                        this.btnAplicarMuebleInmueble.Visible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CamposEscriturizacion(bool estado)
    {

        this.txtFechaEscritura.Enabled = estado;
        this.txtNumeroEscritura.Enabled = estado;
        this.txtNotariaDondeRegistra.Enabled = false;
        this.txtNumeroRegistro.Enabled = estado;
        this.txtFechaIngresoICBF.Enabled = estado;
        this.txtFechaInstrumentosPublicos.Enabled = estado;

    }

    /// <summary>
    /// Validación para el valor por defecto de la fecha adjudicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionMueble_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int LoopCounter;

        // Variable for starting index. Use this to make sure the tabindexes start at a higher
        // value than any other controls above the gridview. 
        // Header row indexes will be 110, 111, 112...
        // First data row will be 210, 211, 212... 
        // Second data row 310, 311, 312 .... and so on
        int tabIndexStart = 10;

        for (LoopCounter = 0; LoopCounter < e.Row.Cells.Count; LoopCounter++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                // Check to see if the cell contains any controls
                if (e.Row.Cells[LoopCounter].Controls.Count > 0)
                {
                    // Set the TabIndex. Increment RowIndex by 2 because it starts at -1
                    ((LinkButton)e.Row.Cells[LoopCounter].Controls[0]).TabIndex = -1;//short.Parse((e.Row.RowIndex + 2).ToString() + tabIndexStart++.ToString());
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.TabIndex = -1;
            if (e.Row.Cells[8].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[8].Text.Equals("01/01/0001") || e.Row.Cells[8].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[8].Text = "";
            }
        }
    }

    /// <summary>
    /// Evento que se ejecuta cuando se selecciona un comando de la grilla de Tipo titulo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gwvInfoTipoTitulo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Editar")
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                this.btnAplicarActualizacionTitulo.Visible = true;

                // Validar el tipo de tramite de la denuncia
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        this.idTitulo.Text = gvr.Cells[4].Text;
                        // Si la decisión de trámite es "Rechazado" y "Judicial".
                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        {
                            this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                            this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;

                            this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                            this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                            this.txtTipoTitulo.Text = gvr.Cells[0].Text; ;
                            this.txtDescripcionBienTitulo.Text = ((Label)gvr.FindControl("lbDescripcionTitulo")).Text;

                            if (gvr.Cells[3].Text != "")
                            {
                                this.txtFechaAdjudicacionTitulo.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                            }
                            else
                            {
                                this.txtFechaAdjudicacionTitulo.InitNull = true;
                            }
                        }
                        else
                        {
                            this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                            this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;

                            string tipoTitulo = gvr.Cells[0].Text;
                            string descripcionBien = ((Label)gvr.FindControl("lbDescripcionTitulo")).Text;
                            if (!gvr.Cells[3].Text.Equals(string.Empty))
                            {
                                DateTime fechaAdjudicacion = DateTime.Parse(gvr.Cells[3].Text);
                                this.txtFechaAdjudicacionTitulo.Date = fechaAdjudicacion;

                            }
                            this.txtTipoTitulo.Text = tipoTitulo;
                            this.txtDescripcionBienTitulo.Text = descripcionBien;
                        }
                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                        this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                        this.idTitulo.Text = gvr.Cells[4].Text;
                        this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                        this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                        this.txtTipoTitulo.Text = gvr.Cells[0].Text; ;
                        this.txtDescripcionBienTitulo.Text = ((Label)gvr.FindControl("lbDescripcionTitulo")).Text;

                        if (gvr.Cells[3].Text != "")
                        {
                            this.txtFechaAdjudicacionTitulo.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                        }
                        else
                        {
                            this.txtFechaAdjudicacionTitulo.InitNull = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Radio button Estado documento select change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbtEstadoDocumento_TextChanged(object sender, EventArgs e)
    {
        if (this.rbtEstadoDocumento.SelectedValue != "-1")
        {
            this.FechaRecibido.Enabled = true;
            this.txtObservacionesRecibido.Enabled = true;
        }
        this.ObliFeRe.Visible = true;
        this.ObliNoAr.Visible = true;
        this.ObliObDoRe.Visible = true;
    }

    /// <summary>
    /// Evento selección del check Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void radioAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
        {
            InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
            if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                           && vInfoEscrituracion.FechaSolemnizacion.Date != new DateTime())
            {
                this.dtFechaAdjudicacionBien.Enabled = true;
                this.CamposEscriturizacion(true);
            }
            else
            {
                this.dtFechaAdjudicacionBien.Enabled = true;
                this.toolBar.MostrarMensajeError("El estado del bien debe ser Adjudicado, la Decisión del Trámite Notarial debe ser Aceptado y debe tener registrado la Fecha de Solemnización");
            }
        }
        else
        {
            this.dtFechaAdjudicacionBien.Enabled = true;
            this.toolBar.MostrarMensajeGuardado("El estado del bien debe ser Adjudicado, la Decisión del Trámite Notarial debe ser Aceptado y debe tener registrado la Fecha de Solemnización");
        }
    }

    /// <summary>
    /// Evento selección del check No Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void radioNoAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        this.dtFechaAdjudicacionBien.Enabled = false;
        this.dtFechaAdjudicacionBien.InitNull = true;
        this.CamposEscriturizacion(false);
    }

    /// <summary>
    /// Evento selección Titulo Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbnEstadoBnTituloAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        this.txtFechaAdjudicacionTitulo.Enabled = true;
    }

    /// <summary>
    /// Evento selección Titulo No Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbnEstadoBnTituloNoAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        this.txtFechaAdjudicacionTitulo.Enabled = false;
        this.txtFechaAdjudicacionTitulo.InitNull = true;
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Handles the Click event of the btnAdd control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        if (ValidarDocumentacionSolicitada() == false)
        {
            toolBar.LipiarMensajeError();
            if (AdicionarDocumentosSolicitadosCache())
            {
                //CargarGrillaDocumentosSolicitadosYRecibidos();
            }
        }
        else
        {
            //toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
        }
    }

    /// <summary>
    /// Evento que actualiza la informacion del titulo de valor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicarActualizacionTitulo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            string vEstadoBien = string.Empty;
            DateTime? vFechaAdjudica = new DateTime();
            int pIdTitulo = Convert.ToInt32(this.idTitulo.Text);
            if (this.rbnEstadoBnTituloAdjudicado.Checked)
            {
                vEstadoBien = this.rbnEstadoBnTituloAdjudicado.Text;
                vFechaAdjudica = this.txtFechaAdjudicacionTitulo.Date;
            }
            else if (this.rbnEstadoBnTituloNoAdjudicado.Checked)
            {
                vEstadoBien = this.rbnEstadoBnTituloNoAdjudicado.Text;
                vFechaAdjudica = null;
            }

            List<ResultFiltroTitulo> vListInfoTitulo = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];
            vListInfoTitulo.ForEach(item =>
            {
                if (item.IdTituloValor == pIdTitulo)
                {
                    item.estado = vEstadoBien;
                    item.FechaAdjudicados = vFechaAdjudica;
                    item.Modificado = true;
                }
            });
            ViewState["SortedgwvInfoTipoTitulo"] = vListInfoTitulo;
            this.CargarGrillas();
            this.LimpiarInformacionTituloValor();
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento que actualiza la informacion del mueble e inmueble
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicarMuebleInmueble_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            string vEstadoBien = string.Empty;
            DateTime? vFechaAdjudica = new DateTime();
            int vIdMueble = Convert.ToInt32(this.idBien.Text);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
            bool bandera = true;
            if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
            {
                string NumeroEscritura = this.txtNumeroEscritura.Text;
                string NumeroRegistro = this.txtNumeroRegistro.Text;

                DateTime FechaEscritura = this.txtFechaEscritura.Date;
                DateTime FechaIngresoICBF = this.txtFechaIngresoICBF.Date;
                DateTime FechaInstrumentosPublicos = this.txtFechaInstrumentosPublicos.Date;

                if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                               && vInfoEscrituracion.FechaSolemnizacion.Date != new DateTime())
                {
                    if (FechaEscritura.ToString().Equals("1/01/1900 12:00:00 a. m."))
                    {
                        this.Label3.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label3.Visible = false;
                    }

                    if (FechaIngresoICBF.ToString().Equals("1/01/1900 12:00:00 a. m."))
                    {
                        this.Label6.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label6.Visible = false;
                    }

                    if (FechaInstrumentosPublicos.ToString().Equals("1/01/1900 12:00:00 a. m."))
                    {
                        this.Label7.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label7.Visible = false;
                    }

                    if (NumeroEscritura.Equals(string.Empty))
                    {
                        this.Label4.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label4.Visible = false;
                    }

                    if (NumeroRegistro.Equals(string.Empty))
                    {
                        this.Label5.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label5.Visible = false;
                    }
                }



            }
            if (bandera)
            {
                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        //if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        if (radioAdjudicado.Checked && (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                                   && !vInfoEscrituracion.FechaSolemnizacion.ToString().Equals("1900-01-01 00:00:00.000"))
                        {
                            List<InfoMuebleProtocolizacion> vListInfoMuebles = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                            vListInfoMuebles.ForEach(item =>
                            {
                                if (item.IdMuebleInmueble == vIdMueble)
                                {
                                    item.FechaEscritura = this.txtFechaEscritura.Date.ToString("dd/MM/yyyy");
                                    item.NumeroEscritura = (this.txtNumeroEscritura.Text).Equals(string.Empty) ? "0" : this.txtNumeroEscritura.Text;
                                    item.NumeroRegistro = (this.txtNumeroRegistro.Text).Equals(string.Empty) ? "0" : this.txtNumeroRegistro.Text;
                                    item.FechaIngresoICBF = this.txtFechaIngresoICBF.Date;
                                    item.FechaInstrumentosPublicos = this.txtFechaInstrumentosPublicos.Date;
                                    item.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                                    item.Modificado = true;
                                }
                            });
                            ViewState["SortedgvwInfoBien"] = vListInfoMuebles;
                        }
                    }
                }
                if (this.radioAdjudicado.Checked)
                {
                    vEstadoBien = this.radioAdjudicado.Text;
                    vFechaAdjudica = this.dtFechaAdjudicacionBien.Date;
                }
                else if (radioNoAdjudicado.Checked)
                {
                    vEstadoBien = this.radioNoAdjudicado.Text;
                    vFechaAdjudica = null;
                }
                List<InfoMuebleProtocolizacion> vListInfoMueble = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                vListInfoMueble.ForEach(item =>
                {
                    if (item.IdMuebleInmueble == vIdMueble)
                    {
                        item.EstadoBien = vEstadoBien;
                        item.FechaAdjudicado = vFechaAdjudica;
                        item.Modificado = true;
                    }
                });
                ViewState["SortedgvwInfoBien"] = vListInfoMueble;
                this.CargarGrillas();
                this.dtFechaAdjudicacionBien.Enabled = false;
                this.LimpiarInformacionMuebleInmueble();
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAplicar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);

            if (vIdDocumentoDenuncia != 0)
            {
                if (this.ValidarDocumentacionRecibida() == false)
                {
                    if (GuardarArchivos(vIdDocumentoDenuncia, 0))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                        vListaDocumentosSolicitados.ForEach(item =>
                        {
                            if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoDenuncia)
                            {
                                item.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                item.NombreEstado = (Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue) == 3) ? "Aceptado" : "Devuelto";
                                item.FechaSolicitud = new DateTime(FechaSolicitud.Date.Year, FechaSolicitud.Date.Month, FechaSolicitud.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text;
                                item.IdTipoDocumentoSoporteDenuncia = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                item.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.FechaRecibidoGrilla = DateTime.Today.ToString("dd/MM/yyyy");
                                item.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                                item.NombreArchivo = fulArchivoRecibido.FileName;
                                item.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName;
                            }
                        });
                        this.txtObservacionesSolicitado.Enabled = true;
                        this.FechaSolicitud.Enabled = true;
                        this.ddlTipoDocumento.Enabled = true;
                        this.txtObservacionesSolicitado.Text = string.Empty;
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.FechaSolicitud.InitNull = true;
                        this.FechaRecibido.InitNull = true;
                        this.rbtEstadoDocumento.SelectedIndex = -1;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.btnAplicar.Visible = false;
                        this.btnAdd.Visible = true;
                        this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                        this.CargarGrillaDocumentos();
                    }

                    this.lblNombreArchivo.Text = string.Empty;
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// cierra el modal de mueble
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarInmuebleModal_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlPopUpHistorico.Visible = false;
    }

    /// <summary>
    /// Cerrar el panel de Documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarPop_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    /// <summary>
    /// Evento del boton consultar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Descargar Documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarProtocolizacion/Edit.aspx");
    }

    /// <summary>
    /// Editar documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        ////Control de paneles
        this.ControlPanelDocRecibida(true);
        this.ControlPanelDocSolicitada(true);
        this.btnAplicar.Visible = true;
        List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
        ImageButton vEditDocumento = (ImageButton)sender;
        string[] ArrayEditDocumento = vEditDocumento.CommandArgument.Split('|');
        //vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));

        int vIdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(ArrayEditDocumento[0]);
        DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
        vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentosSolicitadosDenunciaBien).FirstOrDefault().EsNuevo = false;
        //var vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        //SetSessionParameter("Mostrencos.Protocolizacion.IdDocumento", vIdDocumentosSolicitadosDenunciaBien);

        var Tipo = vListaDocumento.Find(x => x.NombreTipoDocumento == ArrayEditDocumento[1]);
        if (Tipo != null)
        {
            this.hfIdDocumentoSolocitado.Value = vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            ddlTipoDocumento.SelectedValue = Tipo.IdTipoDocumentoBienDenunciado.ToString();

            if (ArrayEditDocumento[2].ToString() == "1/01/0001 12:00:00 a. m.")
            {
                this.FechaSolicitud.InitNull = true;
            }
            else
            {
                this.FechaSolicitud.Date = Convert.ToDateTime(ArrayEditDocumento[2]);
            }

            txtObservacionesSolicitado.Text = ArrayEditDocumento[3];

            if (ArrayEditDocumento[5] != "")
            {
                FechaRecibido.Date = Convert.ToDateTime(ArrayEditDocumento[5]);
            }
            else
            {
                FechaRecibido.Date = DateTime.Now;
            }

            if (ArrayEditDocumento[4] == "3" || ArrayEditDocumento[4] == "5")
            {
                if (ArrayEditDocumento[4] == "3")
                {
                    this.rbtEstadoDocumento.SelectedValue = "3";
                    this.ObliFeRe.Visible = true;
                    this.ObliNoAr.Visible = true;
                    this.ObliObDoRe.Visible = true;
                }
                if (ArrayEditDocumento[4] == "5")
                {
                    this.rbtEstadoDocumento.SelectedValue = "5";
                    this.ObliFeRe.Visible = true;
                    this.ObliNoAr.Visible = true;
                    this.ObliObDoRe.Visible = true;
                }

            }
            else
            {
                this.ObliFeRe.Visible = false;
                this.ObliNoAr.Visible = false;
                this.ObliObDoRe.Visible = false;
            }

            lblNombreArchivo.Text = ArrayEditDocumento[6];
            txtObservacionesRecibido.Text = ArrayEditDocumento[7];
            lblNombreArchivo.Visible = true;
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
        }
    }

    /// <summary>
    /// Eliminar documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton vIdDeleteDocumento = (ImageButton)sender;
            DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
            vdo.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vIdDeleteDocumento.CommandArgument);
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];

            foreach (var item in vListaDocumentosSolicitados.ToList())
            {
                if (item.IdDocumentosSolicitadosDenunciaBien == vdo.IdDocumentosSolicitadosDenunciaBien)
                {
                    vListaDocumentosEliminar.Add(item);
                    vListaDocumentosSolicitados.Remove(item);
                }
            }

            this.ViewState["DocumentosEliminar"] = vListaDocumentosEliminar;
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
            gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
            gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// Abre el pop up información de la denuncia
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnInfoDenuncia_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlPopUpHistorico.Visible = true;
    }

    /// <summary>
    /// Evento del boton Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento del boton retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle");
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteAdd");
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdResolucion");
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// ver archivos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkVerArchivo_Click(object sender, EventArgs e)
    {
        try
        {
            this.pnlArchivo.Visible = true;
            bool vVerArchivo = false;
            string vRuta = string.Empty;
            LinkButton imageButton = (LinkButton)sender;
            TableCell tableCell = (TableCell)imageButton.Parent;
            GridViewRow row = (GridViewRow)tableCell.Parent;
            gvwDocumentacionRecibida.SelectedIndex = row.RowIndex;
            int fila = row.RowIndex;

            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[fila].Value);
            List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
            vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo;

            if (!vVerArchivo)
            {
                vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo;
            }

            if (vVerArchivo)
            {
                this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vDocumento.RutaArchivo.ToString();
                if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion



    protected void gvwDocumentacionRecibida_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    // Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    // Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        // Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        // Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Control de los datos recibidos por el gridview documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        int LoopCounter;

        // Variable for starting index. Use this to make sure the tabindexes start at a higher
        // value than any other controls above the gridview. 
        // Header row indexes will be 110, 111, 112...
        // First data row will be 210, 211, 212... 
        // Second data row 310, 311, 312 .... and so on
        int tabIndexStart = 10;

        for (LoopCounter = 0; LoopCounter < e.Row.Cells.Count; LoopCounter++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                // Check to see if the cell contains any controls
                if (e.Row.Cells[LoopCounter].Controls.Count > 0)
                {
                    // Set the TabIndex. Increment RowIndex by 2 because it starts at -1
                    ((LinkButton)e.Row.Cells[LoopCounter].Controls[0]).TabIndex = -1;//short.Parse((e.Row.RowIndex + 2).ToString() + tabIndexStart++.ToString());
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[1].Text.Equals("1/01/0001") || e.Row.Cells[1].Text.Equals("01/01/0001"))
            {
                e.Row.Cells[1].Text = "";
            }
        }
    }
}