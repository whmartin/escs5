﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_RegistrarProtocolizacion_Add" %>

<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .rbl {
            margin-right: 20px;
        }

        .auto-style1 {
            width: 45%;
        }

        .auto-style2 {
            height: 27px;
        }

        .auto-style3 {
            height: 22px;
        }

        .fechaaviso {
            margin-left: 20px;
        }


        .auto-style4 {
            border: thin solid white;
            font-family: Arial;
            color: white;
            padding-left: 5px;
            background-color: #81BA3D;
            width: 762px;
        }

        .auto-style5 {
            width: 55%;
            height: 27px;
        }

        .auto-style6 {
            border: thin solid white;
            font-family: Arial;
            color: white;
            padding-left: 5px;
            background-color: #81BA3D;
            width: 468px;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>

    <script type="text/javascript">
        // Validacion solo numeros
        function isNumberKey(evt) {
            //var e = evt || window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode !== 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        // Validacion solo letras
        function ValidateAlpha(evt) {
            var keyCode = (evt.which) ? evt.which : evt.keyCode
            if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode !== 32)
                return false;
            return true;
        }
    </script>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia</td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *</td>
                <td>Fecha de radicado de la denuncia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *         </td>
                <td>Fecha radicado en correspondencia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *</td>
                <td>Número de identificación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Primer nombre *</td>
                <td style="width: 55%">Segundo nombre *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45%">Primer apellido *</td>
                <td style="width: 55%">Segundo apellido *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false" Width="250px"></asp:TextBox>
                    
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td>Histórico de la Denuncia                            
                            <asp:ImageButton ID="btnInfoDenuncia" CssClass="btnVerMueble" runat="server" ImageUrl="~/Image/btn/info.jpg" Visible="true"
                                Height="16px" Width="16px" OnClick="btnInfoDenuncia_Click" AutoPostBack="true" ToolTip="Ver Detalle" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfIdDocumentoSolocitado" runat="server" />
        <asp:Panel runat="server" ID="pnlPopUpHistorico" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 25%; left: 15%; width: 800px; background-color: white; border: 1px solid #dfdfdf;"
            BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
            <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
                <tr>
                    <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                        <div>
                            <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                        </div>
                        <div>
                            <div>
                                <asp:ImageButton ID="btnCerrarInmuebleModal" runat="server" Style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;" alt="h" OnClick="btnCerrarInmuebleModal_Click" ImageUrl="../../../Image/btn/close.png" />
                            </div>
                        </div>
                        <tr style="padding: 5px;">
                            <td style="text-align: center;">
                                <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                    <asp:GridView ID="tbHistoria" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False"
                                        AllowPaging="true" AllowSorting="true" GridLines="None"
                                        OnSorting="gvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="gvwHistoricoDenuncia_OnPageIndexChanging">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                            <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                            <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                            <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                            <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                            <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
            </table>
        </asp:Panel>

    </asp:Panel>

    <asp:Panel runat="server" ID="pnlInfoDecisionAdjudicacion">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Decisión de Adjudicación</td>
            </tr>
            <tr class="rowA">
                <td colspan="2" class="auto-style3"></td>
            </tr>
            <tr class="rowB">
                <td>
                      <asp:CustomValidator runat="server"
                                ID="CustomValidator19" ForeColor="Red"
                                ControlToValidate="fechaSentencia$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha de sentencia debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                    Fecha de la sentencia
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator9" ForeColor="Red"
                        ControlToValidate="fechaSentencia$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
                <td>Nombre del Juzgado</td>
            </tr>
            <tr class="rowA">
                <td>
                    <div>
                        <uc1:fecha runat="server" ID="fechaSentencia" Requerid="false" Enabled="false" Width="250px" />
                    </div>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreJuzgado" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Sentido de la sentencia
                    <asp:Label runat="server" ID="Label1" Visible="false">*</asp:Label>
                    <asp:Label runat="server" ID="reqSentido" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                </td>
                <td>
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator1" ForeColor="Red"
                        ControlToValidate="fechaConstanciaEjecutoriada$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                        ErrorMessage="La fecha de constancia ejecutoriada debe ser menor o igual a la fecha actual.">
                    </asp:CustomValidator>
                    <br />
                    Fecha de constancia ejecutoriada
                    <asp:Label runat="server" ID="Label2" Visible="false">*</asp:Label>
                    <asp:Label runat="server" ID="reqFecha" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator10" ForeColor="Red"
                        ControlToValidate="fechaConstanciaEjecutoriada$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblSentidoSentencia" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                        <asp:ListItem Value="radioAdjudicada" Text="Adjudicada" />
                        <asp:ListItem Value="radioNoAdjudicada" Text="No Adjudicada" />
                        <asp:ListItem Value="radioAdjudicadaParcial" Text="Adjudicada parcialmente" />
                    </asp:RadioButtonList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="fechaConstanciaEjecutoriada" Requerid="false" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="auto-style1">Juzgado que profiere la sentencia</td>
                <td style="width: 55%">Departamento de despacho Judicial</td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtJuzgadoSentencia" onKeyPress="return ValidateAlpha(event);" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDepartamentoDespacho" onKeyPress="return ValidateAlpha(event);" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="auto-style2">Municipio del Despacho Judicial</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtMunicipioJudicial" onKeyPress="return ValidateAlpha(event);" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="relacionBienes">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="auto-style4">Relación de Bienes</td>
            </tr>
        </table>
        <asp:Panel runat="server" ID="pnlInformacionMuebleInmueble">
            <asp:HiddenField ID="hfTipoParametro" runat="server" />
            <asp:HiddenField ID="hfCodigoParametro" runat="server" />
            <table width="90%" align="center">
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información del Mueble o Inmueble</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2" class="auto-style3"></td>
                </tr>
                <tr class="rowB">
                    <td class="auto-style2">Tipo de bien</td>
                    <td class="auto-style5">Subtipo de bien</td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" Enabled="false" ID="ddlTipoBien" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="ddlTipoBien_SelectedIndexChanged">
                            <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            <asp:ListItem Value="2">MUEBLE</asp:ListItem>
                            <asp:ListItem Value="3">INMUEBLE</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" Enabled="false" ID="ddlSubTipoBien" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="ddlSubTipoBien_SelectedIndexChanged">
                            <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:ImageButton ID="btnAplicarMuebleInmueble" Visible="false" runat="server" ImageUrl="~/Image/btn/apply.png"
                            OnClick="btnAplicarMuebleInmueble_Click" ValidationGroup="btnAplicarMuebleInmueble" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Clase del bien</td>
                    <td style="width: 55%">Estado del bien</td>
                </tr>
                <tr class="rowA">
                    <td class="auto-style1" valign="top">
                        <asp:DropDownList runat="server" Enabled="false" ID="ddlClasebien" AutoPostBack="true" Width="250px">
                            <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RadioButton ID="radioAdjudicado" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudacacion" Text="Adjudicado" OnCheckedChanged="radioAdjudicado_CheckedChanged" AutoPostBack="true" />
                        <br />
                        <asp:RadioButton ID="radioNoAdjudicado" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudacacion" Text="No Adjudicado" OnCheckedChanged="radioNoAdjudicado_CheckedChanged" AutoPostBack="true" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="auto-style1">Fecha de Adjudicación
                        <asp:CustomValidator runat="server"
                            ID="CustomValidator2" ForeColor="Red"
                            ControlToValidate="dtFechaAdjudicacionBien$txtFecha"
                            ClientValidationFunction="validateDate" ValidationGroup="btnAplicarMuebleInmueble"
                            ErrorMessage="La fecha de Adjudicación debe ser menor o igual a la fecha actual.">
                        </asp:CustomValidator>
                         <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator11" ForeColor="Red"
                        ControlToValidate="dtFechaAdjudicacionBien$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="idBien" runat="server" Visible="false" ></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <div>
                            <uc1:fecha runat="server" ID="dtFechaAdjudicacionBien" onKeyPress="return isNumberKey(event);" Requerid="false" Enabled="false" />
                        </div>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="auto-style2">Descripción del bien</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="txtDescripcionBien" onKeyPress="return ValidateAlpha(event);" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlInfoEscrituracion">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información de Escrituración</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2" class="auto-style3"></td>
                </tr>
                <tr class="rowB">
                    <td>
                        <asp:CustomValidator runat="server"
                            ID="CustomValidator3" ForeColor="Red"
                            ControlToValidate="txtFechaEscritura$txtFecha"
                            ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                            ErrorMessage="La fecha de escritura debe ser menor o igual a la fecha actual.">
                        </asp:CustomValidator>
                         <br />
                        Fecha de escritura *
                        <asp:Label runat="server" ID="Label3" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                        <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator12" ForeColor="Red"
                        ControlToValidate="txtFechaEscritura$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>

                    </td>
                    <td style="width: 55%">Número de escritura *
                        <asp:Label runat="server" ID="Label4" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <div>
                            <uc1:fecha runat="server" ID="txtFechaEscritura" onKeyPress="return isNumberKey(event);" Requerid="false" Enabled="false" />
                        </div>
                    </td>
                    <td valign="top">
                        <asp:TextBox runat="server" ID="txtNumeroEscritura" onKeyPress="return isNumberKey(event);" Enabled="false" MaxLength="15"></asp:TextBox>
                         <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNumeroEscritura" FilterType="numbers" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Notaria donde se registra</td>
                    <td style="width: 55%">Número de registro *
                        <asp:Label runat="server" ID="Label5" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td class="auto-style1">
                        <asp:TextBox runat="server" ID="txtNotariaDondeRegistra" onKeyPress="return ValidateAlpha(event);" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroRegistro" onKeyPress="return isNumberKey(event);" Enabled="false" MaxLength="20"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtNumeroRegistro" FilterType="numbers" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td class="auto-style1">
                        <asp:CustomValidator runat="server"
                            ID="CustomValidator5" ForeColor="Red"
                            ControlToValidate="txtFechaIngresoICBF$txtFecha"
                            ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                            ErrorMessage="La fecha de ingreso al ICBF debe ser menor o igual a la fecha actual.">
                        </asp:CustomValidator>
                        <br />
                        Fecha de ingreso al ICBF *
                        <asp:Label runat="server" ID="Label6" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                        <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator13" ForeColor="Red"
                        ControlToValidate="txtFechaIngresoICBF$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                    </td>
                    <td style="width: 55%">
                        <asp:CustomValidator runat="server"
                            ID="CustomValidator6" ForeColor="Red"
                            ControlToValidate="txtFechaInstrumentosPublicos$txtFecha"
                            ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                            ErrorMessage="La fecha de Instrumentos Públicos debe ser menor o igual a la fecha actual.">
                        </asp:CustomValidator>
                        <br />
                        Fecha de Instrumentos Públicos *
                        <asp:Label runat="server" ID="Label7" Visible="false" ForeColor="Red"> Campo Requerido</asp:Label>
                        <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator14" ForeColor="Red"
                        ControlToValidate="txtFechaInstrumentosPublicos$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>

                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <div>
                            <uc1:fecha runat="server" ID="txtFechaIngresoICBF" Requerid="false" Enabled="false" />
                        </div>
                    </td>
                    <td colspan="2">
                        <uc1:fecha runat="server" ID="txtFechaInstrumentosPublicos" Requerid="false" Enabled="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlgvwInfoBien" TabIndex="-1" TabStop="true">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvwInfoBien" AutoGenerateColumns="False" AllowPaging="True"
                            OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging"
                            OnRowDataBound="gvwInfoBien_RowDataBound"
                            GridLines="None" Width="100%" DataKeyNames="IdMuebleInmueble" CellPadding="0" Height="16px"
                            OnRowCommand="gvwInfoBien_RowCommand" ondblclick="this.blur();" TabStop="true" TabIndex="-1" sortOnLoad="false">
                            <Columns>
                                <asp:BoundField DataField="TipoBien" ItemStyle-CssClass="Ocultar_" />
                                <asp:BoundField HeaderText="Tipo de bien" DataField="NombreTipoBien" SortExpression="NombreTipoBien" HtmlEncode="false" />
                                <asp:BoundField DataField="SubTipoBien" ItemStyle-CssClass="Ocultar_" />
                                <asp:BoundField HeaderText="Subtipo de bien" DataField="NombreSubTipoBien" SortExpression="NombreSubTipoBien" HtmlEncode="false" />
                                <asp:BoundField DataField="ClaseBien" ItemStyle-CssClass="Ocultar_" />
                                <asp:BoundField HeaderText="Clase de bien" DataField="NombreClaseBien" SortExpression="NombreClaseBien" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Estado del bien" DataField="EstadoBien" SortExpression="EstadoBien" HtmlEncode="false" />
                                <%--<asp:BoundField HeaderText="Descripción del bien" DataField="DescripcionBien" SortExpression="DescripcionBien" ItemStyle-CssClass="descripcionGrilla" HtmlEncode="false" />--%>
                                <asp:TemplateField HeaderText="Descripción de bien" SortExpression="DescripcionBien">
                                    <ItemTemplate>
                                        <asp:Label ID="lbDescripcionMueble" runat="server" Text='<%# Bind("DescripcionBien") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="descripcionGrilla" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Fecha de adjudicación" DataField="FechaAdjudicado" SortExpression="FechaAdjudicado" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Fecha de escritura" DataField="FechaEscritura" SortExpression="FechaEscritura" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Número de escritura" DataField="NumeroEscritura" SortExpression="NumeroEscritura" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Notaria donde se registra" DataField="Notaria" SortExpression="Notaria" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Número de Registro" DataField="NumeroRegistro" SortExpression="NumeroRegistro" HtmlEncode="false" />
                                <asp:BoundField HeaderText="Fecha de Ingreso al ICBF" DataField="FechaIngresoICBF" SortExpression="FechaIngresoICBF" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false" />
                                <asp:BoundField DataField="FechaInstrumentosPublicos" ItemStyle-CssClass="Ocultar_" />
                                <asp:BoundField DataField="IdMuebleInmueble" ItemStyle-CssClass="Ocultar_" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" ImageUrl="~/Image/btn/edit.gif"
                                            Height="16px" Width="16px" ToolTip="Editar" TabIndex="-1" TabStop="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="Panel1" TabIndex="-1" TabStop="true">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información del Título Valor</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2" class="auto-style3"></td>
                </tr>
                <tr class="rowB">
                    <td>Tipo de Título</td>
                    <td style="width: 55%">Estado del bien</td>
                </tr>
                <tr class="rowA">
                    <td class="auto-style1">
                        <asp:TextBox runat="server" ID="txtTipoTitulo" onKeyPress="return ValidateAlpha(event);" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RadioButton ID="rbnEstadoBnTituloAdjudicado" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudacacion2" Text="Adjudicado" OnCheckedChanged="rbnEstadoBnTituloAdjudicado_CheckedChanged" AutoPostBack="true" />
                        <br />
                        <asp:RadioButton ID="rbnEstadoBnTituloNoAdjudicado" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudacacion2" Text="No Adjudicado" OnCheckedChanged="rbnEstadoBnTituloNoAdjudicado_CheckedChanged" AutoPostBack="true" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnAplicarActualizacionTitulo" Visible="false" runat="server" ImageUrl="~/Image/btn/apply.png"
                            OnClick="btnAplicarActualizacionTitulo_Click" ValidationGroup="btnAplicarActualizacionTitulo" TabIndex="-1" TabStop="true" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="auto-style1">Fecha de Adjudicación
                        <asp:CustomValidator runat="server"
                            ID="CustomValidator4" ForeColor="Red"
                            ControlToValidate="txtFechaAdjudicacionTitulo$txtFecha"
                            ClientValidationFunction="validateDate" ValidationGroup="btnAplicarActualizacionTitulo"
                            ErrorMessage="La fecha de Adjudicación debe ser menor o igual a la fecha actual.">
                        </asp:CustomValidator>
                        <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator15" ForeColor="Red"
                        ControlToValidate="txtFechaAdjudicacionTitulo$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAplicarActualizacionTitulo"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="idTitulo" runat="server" Visible="false" ></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <div>
                            <uc1:fecha runat="server" ID="txtFechaAdjudicacionTitulo" Requerid="false" Enabled="false" />
                        </div>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="auto-style2">Descripción del bien</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="txtDescripcionBienTitulo" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView EnablePersistedSelection="true" runat="server" ID="gwvInfoTipoTitulo" AutoGenerateColumns="False" AllowPaging="True"
                            OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging"
                            OnRowDataBound="gwvInfoTipoTitulo_RowDataBound"
                            GridLines="None" Width="100%" DataKeyNames="IdTituloValor" CellPadding="0" Height="16px"
                            OnRowCommand="gwvInfoTipoTitulo_RowCommand" ondblclick="this.blur();" TabStop="true" TabIndex="-1" sortOnLoad="false">
                            <Columns>
                                <asp:BoundField HeaderText="Tipo de título" DataField="nombreTitulo" SortExpression="nombreTitulo" />
                                <asp:BoundField HeaderText="Estado del bien" DataField="estado" SortExpression="estado" />
                                <%--<asp:BoundField HeaderText="Descripción de bien" DataField="DescripcionTitulo" SortExpression="DescripcionTitulo" ItemStyle-CssClass="descripcionGrilla" />--%>
                                  <asp:TemplateField HeaderText="Descripción de bien" SortExpression="DescripcionTitulo">
                                    <ItemTemplate>
                                        <asp:Label ID="lbDescripcionTitulo" runat="server" Text='<%# Bind("DescripcionTitulo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="descripcionGrilla" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Fecha de adjudicación" DataField="FechaAdjudicados" DataFormatString="{0:dd/MM/yyyy}" SortExpression="FechaAdjudicados" />
                                <asp:BoundField DataField="IdTituloValor" ItemStyle-CssClass="Ocultar_" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" ImageUrl="~/Image/btn/edit.gif" TabIndex="-1"
                                            Height="16px" Width="16px" ToolTip="Editar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel2">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Comunicación Administrativa y Financiera</td>
            </tr>
            <tr class="rowB">
                <td>Fecha de la Comunicación
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator7" ForeColor="Red"
                        ControlToValidate="fechaComunicacionFinanciera$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                        ErrorMessage="La fecha de la Comunicación debe ser menor o igual a la fecha actual.">
                    </asp:CustomValidator>
                     <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator16" ForeColor="Red"
                        ControlToValidate="fechaComunicacionFinanciera$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <div>
                        <asp:HiddenField ID="tipoDenun" runat="server"></asp:HiddenField>
                        <uc1:fecha runat="server" ID="fechaComunicacionFinanciera" Requerid="false" Enabled="true" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación solicitada</td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Documento soporte de la denuncia *
                    <asp:Label runat="server" ID="lblReqTipoDocumentoSolicitado" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                </td>
                <td>Fecha de solicitud *
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator8" ForeColor="Red"
                        ControlToValidate="FechaSolicitud$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnAdd_Click"
                        ErrorMessage="La fecha de solicitud debe ser menor o igual a la fecha actual.">
                    </asp:CustomValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator17" ForeColor="Red"
                        ControlToValidate="FechaSolicitud$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAdd"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                    <asp:Label runat="server" ID="lblReqFechaSolicitud" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                    <asp:Label runat="server" ID="lblFechaSolicitud" Visible="false" Text="La fecha seleccionada no es correcta. Por favor revisar" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="true"
                        DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                        <asp:ListItem Value="0">Seleccione</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaSolicitud" Enabled="true" />
                </td>
                <td>
                    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Image/btn/add.gif" OnClick="btnAdd_Click" TabIndex="-1" TabStop="false" ValidationGroup="btnAdd"/>
                    <asp:ImageButton ID="btnAplicar" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicar_Click" TabIndex="-1" TabStop="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Observaciones al documento solicitado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesSolicitado" Enabled="true" TextMode="MultiLine" MaxLength="512" Rows="8"
                        Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength('cphCont_txtObservacionesSolicitado');"
                        onkeyup="return CheckLength('cphCont_txtObservacionesSolicitado');"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtObservacionesSolicitado"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
        </table>

        <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
            <table width="90%" align="center">
                <tr class="rowA">
                    <td></td>
                </tr>
                <tr class="rowB">
                    <td class="tdTitulos" colspan="2">Documentación recibida
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2"></td>
                </tr>
                <tr class="rowB">
                    <td style="width: 50%">Estado del documento *
                    <asp:Label runat="server" ID="lblrqfEstadoRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                    </td>
                    <td>Fecha de recibido <asp:Label runat="server" ID="ObliFeRe" Visible="false">*</asp:Label>
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator18" ForeColor="Red"
                        ControlToValidate="FechaRecibido$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAdd"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                    <asp:Label runat="server" ID="lblFechaInvalida" Visible="false" Text="Fecha Inválida" ForeColor="Red"></asp:Label>
                    <asp:Label runat="server" ID="lblrqfFechaRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                    <asp:Label runat="server" ID="lblValidacionFechaMaxima" Visible="false" Text="La fecha seleccionada no debe ser posterior a la fecha actual. Por favor revisar" ForeColor="Red"></asp:Label>
                    <asp:Label runat="server" ID="lblValidacionFechaSolicitud" Visible="false" Text="La fecha seleccionada no debe ser anterior a la fecha de solicitud. Por favor revisar" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" AutoPostBack="true" RepeatDirection="Horizontal" Enabled="false" OnTextChanged="rbtEstadoDocumento_TextChanged">
                            <asp:ListItem Value="3">Aceptado</asp:ListItem>
                            <asp:ListItem Value="5">Devuelto</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <uc1:fecha runat="server" ID="FechaRecibido" Enabled="false" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Nombre de archivo  <asp:Label runat="server" ID="ObliNoAr" Visible="false">*</asp:Label>
                    <asp:Label runat="server" ID="lblRqfNombreArchivo" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:FileUpload ID="fulArchivoRecibido" Enabled="false" runat="server" />
                        <asp:Label ID="lblNombreArchivo" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">Observaciones al documento recibido <asp:Label runat="server" ID="ObliObDoRe" Visible="false">*</asp:Label>
                        <asp:Label runat="server" ID="lblObservacionesRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                    </td>
                </tr>

                <tr class="rowA">
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100"
                            Style="resize: none" Enabled="false" onkeypress="return CheckLength('cphCont_txtObservacionesRecibido');"
                            onkeyup="return CheckLength('cphCont_txtObservacionesRecibido');"></asp:TextBox>

                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />

                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida" TabIndex="-1" TabStop="true">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" ShowHeader="true" Visible="true" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px" PageSize="10"
                            OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" TabIndex="-1" TabStop="false" sortOnLoad="false"
                            OnRowDataBound="gvwDocumentacionRecibida_RowDataBound">
                            <Columns>
                                <asp:BoundField HeaderText="Tipo de documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                                <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla" SortExpression="ObservacionesDocumentacionSolicitada" />
                                <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                                <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" DataFormatString="{0:dd/MM/yyyy}" SortExpression="FechaRecibido" />
                                <asp:TemplateField HeaderText="Nombre del Archivo">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkVerArchivo" runat="server" CommandArgument='<%# Eval("RutaArchivo") %>' Text='<%# Eval("NombreArchivo")%>'
                                            OnClick="lnkVerArchivo_Click" AutoPostBack="true" TabIndex="-1" TabStop="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla" SortExpression="ObservacionesDocumentacionRecibida" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEditarDocumento" runat="server" CommandName="Select" ImageUrl="~/Image/btn/edit.gif" TabIndex="-1" TabStop="true"
                                            Height="16px" Width="16px" ToolTip="Editar" Enable="true" CommandArgument='<%# Eval("IdDocumentosSolicitadosDenunciaBien") +"|"
                                                + Eval("NombreTipoDocumento") +"|"+ Eval("FechaSolicitud") +"|"+ Eval("ObservacionesDocumentacionSolicitada") +"|"
                                                + Eval("IdEstadoDocumento") +"|"+ Eval("FechaRecibido") +"|"+ Eval("NombreArchivo") +"|"
                                                + Eval("ObservacionesDocumentacionRecibida") %>'
                                            OnClick="btnEditarDocumento_Click" AutoPostBack="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminarDocumento" runat="server" CommandName="Delete" ImageUrl="~/Image/btn/delete.gif" TabIndex="-1" TabStop="true"
                                            Height="16px" Width="16px" ToolTip="Eliminar" Enable="true" CommandArgument='<%# Eval("IdDocumentosSolicitadosDenunciaBien") %>'
                                            OnClick="btnEliminarDocumento_Click" AutoPostBack="true" OnClientClick="return confirm('¿Está seguro de eliminar la información?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>

            <asp:Panel runat="server" ID="pnlArchivo" Width="90%" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackgroundCssClass="Background"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">

                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">
                                    <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="always">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnDescargar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-1 align-center">
                                    <asp:ImageButton ID="btnCerrarPop" runat="server" OnClick="btnCerrarPop_Click" ImageUrl="../../../Image/btn/close.png" Style="cursor: pointer; width: 20px; height: 20px; margin-right: -30px;"></asp:ImageButton>
                                </div>
                            </div>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </asp:Panel>
    </asp:Panel>

    <script type="text/javascript">
        this.SetFocus();
        function SetFocus() {
            var tipoDenuncia = document.getElementById('cphCont_tipoDenun').value;
            if (tipoDenuncia == '1') {
                var txtControl = document.getElementById('cphCont_fechaSentencia_txtFecha');
                txtControl.focus();
            }
            else if (tipoDenuncia == '2')
            {
                var txtControl2 = document.getElementById('cphCont_fechaComunicacionFinanciera_txtFecha');
                txtControl.focus();
            }
           
        }

        function SetFocusNota() {
            var txtControlnota = document.getElementById('cphCont_fechaComunicacionFinanciera_txtFecha');
            txtControlnota.focus();
        }

        function CheckLength(campo) {
            var textbox = document.getElementById(campo).value;
            if (textbox.trim().length >= 512) {
                document.getElementById(campo).value = textbox.substr(0, 512);
                return false;
            }
            else {
                return true;
            }
        }

        function validateTodaymenos1(source, arguments) {
            debugger;
            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

            if (DateResolucionA != '') {

                var today = new Date();
                var yesterday = new Date();
                yesterday.setDate(today.getDate() - 1);

                if (DateResolucionA > yesterday) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function validateFechaInvalida(source, arguments) {
            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            if (parseInt(fechaResolucionA[2]) < 1900 || fechaResolucionA[2] == '0000') {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function validateDate(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

            if (DateResolucionA != '') {

                var today = new Date();
                if (DateResolucionA > today) {
                    arguments.IsValid = false;
                }
                else if (fechaResolucionA[2] == "0000") {
                    arguments.IsValid = true;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

    </script>

</asp:Content>
