﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_RegistrarProtocolizacion_List" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .rbl {
            margin-right: 20px;
        }

        .fechaaviso {
            margin-left: 20px;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *</td>
                <td>Fecha de radicado de la denuncia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *</td>
                <td>Fecha radicado en correspondencia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *</td>
                <td>Número de identificación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
   
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Primer nombre *</td>
                <td style="width: 55%">Segundo nombre *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45%">Primer apellido *</td>
                <td style="width: 55%">Segundo apellido *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

    <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Descripción de la denuncia *</td>
                        <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

    <asp:Panel runat="server" ID="pnlInfoDecisionAdjudicacion">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Decisión de Adjudicación</td>
            </tr>
            <tr class="rowA">
                <td colspan="2" class="auto-style3"></td>
            </tr>
            <tr class="rowB">
                <td>Fecha de la sentencia</td>
                <td>Nombre del Juzgado</td>
            </tr>
            <tr class="rowA">
                <td valign="top">
                    <div>
                        <uc1:fecha runat="server" ID="fechaSentencia" Requerid="true" Enabled="false" Width="250px"/>
                    </div>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreJuzgado" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Sentido de la sentencia</td>
                <td>Fecha de constancia ejecutoriada</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButton ID="radioAdjudicada" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudicacion" Text="Adjudicada" />
                    <br />
                    <asp:RadioButton ID="radioNoAdjudicada" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudicacion" Text="No Adjudicada" />
                    <br />
                    <asp:RadioButton ID="radioAdjudicadaParcial" runat="server" CssClass="rbl" Enabled="false" GroupName="Adjudicacion" Text="Adjudicada parcialmente" />
                </td>
                <td valign="top">
                    <uc1:fecha runat="server" ID="fechaConstanciaEjecutoriada" Requerid="true" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="auto-style1">Juzgado que profiere la sentencia</td>
                <td style="width: 55%">Departamento de despacho Judicial
                </td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtJuzgadoSentencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDepartamentoDespacho" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="auto-style2">Municipio del Despacho Judicial</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtMunicipioJudicial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlrelacionBienes">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="auto-style4">Relación de Bienes
                </td>
        </table>
        <asp:Panel runat="server" ID="pnlInformacionMueble">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td class="tdTitulos">Información del Mueble o Inmueble</td>
                </tr>
                <%--<table width="90%" align="center">--%>
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvwInformacionMueble" AutoGenerateColumns="False" AllowPaging="True"
                                OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging"
                                OnSorting="gvwDocumentacionRecibida_OnSorting" OnRowDataBound="gvwInformacionMueble_RowDataBound"
                                GridLines="None" Width="100%" DataKeyNames="IdMuebleInmueble" CellPadding="0" Height="16px" Enabled="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Tipo de bien" DataField="TipoBien" SortExpression="TipoBien" />
                                    <asp:BoundField HeaderText="Subtipo de bien" DataField="NombreSubTipoBien" SortExpression="NombreSubTipoBien" />
                                    <asp:BoundField HeaderText="Clase de bien" DataField="NombreClaseBien" SortExpression="NombreClaseBien" />
                                    <asp:BoundField HeaderText="Estado del bien" DataField="EstadoBien" SortExpression="EstadoBien" />
                                    <asp:BoundField HeaderText="Descripción del bien" DataField="DescripcionBien" SortExpression="DescripcionBien" ItemStyle-CssClass="descripcionGrilla"/>
                                    <asp:BoundField HeaderText="Fecha de adjudicación" DataField="FechaAdjudicado" DataFormatString="{0:dd/MM/yyyy}" SortExpression="FechaAdjudicado" />
                                    <asp:BoundField HeaderText="Fecha de escritura" DataField="FechaEscritura" SortExpression="FechaEscritura" />
                                    <asp:BoundField HeaderText="Número de escritura" DataField="NumeroEscritura" SortExpression="NumeroEscritura" />
                                    <asp:BoundField HeaderText="Notaria donde se registra" DataField="Notaria" SortExpression="Notaria" />
                                    <asp:BoundField HeaderText="Número de Registro" DataField="NumeroRegistro" SortExpression="NumeroRegistro" />
                                    <asp:BoundField HeaderText="Fecha de Ingreso al ICBF" DataField="FechaIngresoICBF" SortExpression="FechaIngresoICBF" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="false"/>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfoEscrituracion">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información de Escrituración</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2" class="auto-style3"></td>
                </tr>
                <tr class="rowB">
                    <td>Fecha de escritura</td>
                    <td style="width: 55%">Número de escritura</td>
                </tr>
                <tr class="rowA">
                    <td>
                        <div>
                            <uc1:fecha runat="server" ID="fechaEscritura" Requerid="true" Enabled="false" />
                        </div>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroEscritura" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Notaria donde se registra
                    </td>
                    <td style="width: 55%">Número de registro</td>
                </tr>
                <tr class="rowA">
                    <td class="auto-style1">
                        <asp:TextBox runat="server" ID="txtNotariaRegistra" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroRegistro" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td class="auto-style1">Fecha de ingreso al ICBF</td>
                    <td style="width: 55%">Fecha de Instrumentos Públicos</td>
                </tr>
                <tr class="rowA">
                    <td>
                        <div>
                            <uc1:fecha runat="server" ID="fechaIngresoICBF" Requerid="true" Enabled="false" />
                        </div>
                    </td>
                    <td colspan="2">
                        <uc1:fecha runat="server" ID="fechaInstrumentosPublicos" Requerid="true" Enabled="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInformacionTitulo">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td class="tdTitulos">Información del Título de Valor</td>
                </tr>
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvwInformacionTitulo" AutoGenerateColumns="False" AllowPaging="True"
                                OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" 
                                OnSorting="gvwInformacionTitulo_Sorting"
                                GridLines="None" Width="100%" DataKeyNames="IdTituloValor" CellPadding="0" Height="16px" 
                                OnRowDataBound="gvwInformacionTitulo_RowDataBound" Enabled="false">
                                <Columns>
                                    <asp:BoundField HeaderText="Tipo de título" DataField="nombreTitulo" SortExpression="nombreTitulo" />
                                    <asp:BoundField HeaderText="Estado del bien" DataField="estado" SortExpression="estado" />
                                    <asp:BoundField HeaderText="Descripción de bien" DataField="DescripcionTitulo" SortExpression="DescripcionTitulo" ItemStyle-CssClass="descripcionGrilla"/>
                                    <asp:BoundField HeaderText="Fecha de adjudicación" DataField="FechaAdjudicado" DataFormatString="{0:dd/MM/yyyy}" SortExpression="FechaAdjudicado" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlComunicacionAdmin">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Comunicación Administrativa y Financiera
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha de la Comunicación</td>
            </tr>
            <tr class="rowA">
                <td>
                    <div>
                        <uc1:fecha runat="server" ID="fechaComunicacionFinanciera" Requerid="true" Enabled="false" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación solicitada
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Documento soporte de la denuncia *
                </td>
                <td>Fecha de solicitud *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="false"
                        DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                    </asp:DropDownList>

                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaSolicitud" Requerid="true" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Observaciones al documento solicitado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
        <table width="90%" align="center">
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos">Documentación recibida</td>
            </tr>
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                            OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" 
                            GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px">
                            <Columns>
                                <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="FechaSolicitud" />
                                <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla"/>
                                <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="FechaRecibidoGrilla" />
                                <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla"/>
                               <asp:TemplateField HeaderText="Nombre del Archivo">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkVerArchivo" runat="server" CommandArgument='<%# Eval("RutaArchivo") %>' Text='<%# Eval("NombreArchivo")%>' OnClick="lnkVerArchivo_Click" AutoPostBack="true" TabIndex="-1"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlArchivo" Width="90%" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; 
                left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackgroundCssClass="Background" Visible="false"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">

                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">
                                    <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="always">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnDescargar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-1 align-center">
                                    <div>
                                        <asp:ImageButton ID="btnCerrarPop" runat="server" Style="text-decoration: none; float: right; 
                                            margin-right: 10px; top: 10px; position: relative; width: 20px; height: 20px;" alt="h"
                                            OnClick="btnCerrarPop_Click" ImageUrl="../../../Image/btn/close.png" />
                                    </div>
                                </div>
                            </div>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
</asp:Content>
