﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>04/07/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Contains the logic of new elements
/// </summary>
public partial class Page_Mostrencos_RegistrarProtocolizacion_Add : GeneralWeb
{

    #region VARIABLES

    /// <summary>
    /// vFileName attribute
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// vIdDenunciaBien attribute
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// vIdCalidadDenuncianteDetalle attribute
    /// </summary>
    private int vIdCalidadDenuncianteDetalle;

    /// <summary>
    /// vIdResolucion attribute
    /// </summary>
    private int vIdResolucion;

    /// <summary>
    /// Toolbar html attribute
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// pageName attribute
    /// </summary>
    private string pageName = "Mostrencos/RegistrarProtocolizacion";

    /// <summary>
    /// vMostrencosService service
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region LOAD

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object render</param>
    /// <param name="e">object contains events</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Primer metodo de carga de la pagina
    /// </summary>
    /// <param name="sender">Object render</param>
    /// <param name="e">Object event container</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.CargarDatosIniciales();
        }
        else
        {
            this.toolBar.LipiarMensajeError();
            this.toolBar.OcultarBotonGuardar(true);
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.EstablecerTitulos("Protocolización");
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            this.btnAplicar.Visible = false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.pnlArchivo.Visible = false;
            this.toolBar.MostrarBotonNuevo(false);
            this.toolBar.MostrarBotonEditar(false);
            this.btnDescargar.Visible = false;
            this.gvwInfoBien.TabIndex = -1;
            List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
            this.ViewState["DocumentosEliminar"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            if (this.vIdDenunciaBien != 0)
            {
                if (this.GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    this.toolBar.MostrarMensajeGuardado();

                this.RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(this.vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString() : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;

                var Protocolizacion = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 20 && x.IdActuacion == 26 && x.IdAccion == 39);
                var Escritura = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 20 && x.IdActuacion == 27 && x.IdAccion == 40);

                if (Protocolizacion == null && Escritura == null)
                {
                    this.toolBar.OcultarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(false);
                }
                else
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(true);
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                this.CargarGrillas();
                this.SetSessionParameter("DocumentarBienDenunciado.Denuncia", vRegistroDenuncia);

                TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
                vTipoDocumento.NombreTipoDocumento = "SELECCIONE";
                vlstTipoDocumentosBien = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
                vlstTipoDocumentosBien.Add(vTipoDocumento);

                this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.ddlTipoDocumento.DataBind();

                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        this.tipoDenun.Value = "2";
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(this.vIdDenunciaBien);
                        this.pnlInfoDecisionAdjudicacion.Visible = false;
                        this.pnlInformacionMuebleInmueble.Visible = true;
                        this.pnlInfoEscrituracion.Visible = true;
                        this.fechaComunicacionFinanciera.Focus();
                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        {
                            this.Label1.Visible = true;
                            this.Label2.Visible = true;
                            this.pnlInfoDecisionAdjudicacion.Visible = true;
                            InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(this.vIdDenunciaBien);
                            if ((vInfoAdjudicacion.FechaSentencia != null) && (vInfoAdjudicacion.FechaSentencia != new DateTime()))
                            {
                                this.fechaSentencia.Date = Convert.ToDateTime(vInfoAdjudicacion.FechaSentencia);
                            }
                            this.txtJuzgadoSentencia.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            this.txtNombreJuzgado.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            this.txtMunicipioJudicial.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                            this.txtDepartamentoDespacho.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;
                            this.fechaSentencia.Enabled = true;
                            this.fechaConstanciaEjecutoriada.Enabled = true;
                            this.rblSentidoSentencia.Enabled = true;
                        }
                        else
                        {
                            this.fechaSentencia.Enabled = true;
                            this.rblSentidoSentencia.Enabled = true;
                        }

                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        this.tipoDenun.Value = "1";
                        this.Label1.Visible = true;
                        this.Label2.Visible = true;
                        InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(this.vIdDenunciaBien);
                        this.pnlInfoDecisionAdjudicacion.Visible = true;
                        this.pnlInformacionMuebleInmueble.Visible = true;
                        this.pnlInfoEscrituracion.Visible = false;
                        this.txtJuzgadoSentencia.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        this.txtNombreJuzgado.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        this.txtMunicipioJudicial.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                        this.txtDepartamentoDespacho.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;

                        if ((vInfoAdjudicacion.FechaSentencia != null) && (vInfoAdjudicacion.FechaSentencia != new DateTime()))
                        {
                            this.fechaSentencia.Date = Convert.ToDateTime(vInfoAdjudicacion.FechaSentencia);
                        }
                        this.rblSentidoSentencia.Enabled = true;
                        this.fechaConstanciaEjecutoriada.Enabled = true;
                        this.fechaSentencia.Enabled = true;
                    }
                }

            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Desactiva paneles y campos
    /// </summary>
    private void DeshabilitarCamposyPaneles()
    {
        this.fechaSentencia.Enabled = false;
        this.fechaComunicacionFinanciera.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = false;
        this.rblSentidoSentencia.Enabled = true;
        this.gwvInfoTipoTitulo.Enabled = false;
        this.gvwInfoBien.Enabled = false;
        this.btnAplicarMuebleInmueble.Enabled = false;
        this.btnInfoDenuncia.Enabled = false;
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitadosCache()
    {
        try
        {
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(Convert.ToInt32(this.ddlTipoDocumento.SelectedValue));
                    vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                    vDocumentacionSolicitada.NombreEstado = "Solicitado";
                    if (item.NombreTipoDocumento == vDocumentacionSolicitada.NombreTipoDocumento)
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentacionSolicitada.NombreArchivo = string.Empty;
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = 0;
                vDocumentacionSolicitada.EsNuevo = true;
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrillaDocumentos();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }
            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int IdCalidadDenunciante = 10;
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(IdCalidadDenunciante);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }

            if (vListaDocumentosSolicitados.Count > 0)
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                vListaDocumentos = vListaDocumentosSolicitados.Where(x => x.IdEstadoDocumento != 6).ToList();
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentos;
                this.gvwDocumentacionRecibida.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    public void CargarGrillas()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));


            List<InfoMuebleProtocolizacion> vListInfoMueble = new List<InfoMuebleProtocolizacion>();// vMostrencosService.ConsultarInfoMuebleXIdBienDenuncia(vIdDenunciaBien);
            List<ResultFiltroTitulo> vListInfoTitulo = new List<ResultFiltroTitulo>(); //vMostrencosService.ConsultarTituloXIdDenuncia(vIdDenunciaBien);

            List<InfoMuebleProtocolizacion> vListInfoMuebles = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
            List<ResultFiltroTitulo> vListInfoTitulos = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];

            if (vListInfoMuebles != null)
            {
                vListInfoMueble = vListInfoMuebles;
            }
            else
            {
                vListInfoMueble = vMostrencosService.ConsultarInfoMuebleXIdBienDenuncia(vIdDenunciaBien);
            }
            if (vListInfoTitulos != null)
            {
                vListInfoTitulo = vListInfoTitulos;
            }
            else
            {
                vListInfoTitulo = vMostrencosService.ConsultarTituloXIdDenuncia(vIdDenunciaBien);
            }

            if (vListInfoMueble.Count > 0)
            {
                for (int i = 0; i < vListInfoMueble.Count; i++)
                {
                    List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(vListInfoMueble[i].TipoBien);
                    var subtipo = vListaSubTipoBien.Find(x => x.IdSubTipoBien == vListInfoMueble[i].SubTipoBien);
                    vListInfoMueble[i].SubTipoBien = subtipo.IdSubTipoBien;
                    vListInfoMueble[i].NombreSubTipoBien = subtipo.NombreSubTipoBien;
                    List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(vListInfoMueble[i].SubTipoBien.ToString());
                    vListInfoMueble[i].ClaseBien = vListaClaseBien[0].IdClaseBien;
                    vListInfoMueble[i].NombreClaseBien = vListaClaseBien[0].NombreClaseBien;
                    vListInfoMueble[i].NombreTipoBien = (vListInfoMueble[i].TipoBien.Equals("3")) ? vListInfoMueble[i].NombreTipoBien = "INMUEBLE" : vListInfoMueble[i].NombreTipoBien = "MUEBLE";
                    if (vListInfoMueble[i].FechaEscritura != null)
                    {
                        vListInfoMueble[i].FechaEscritura = (!vListInfoMueble[i].FechaEscritura.Equals(string.Empty)) ? Convert.ToDateTime(vListInfoMueble[i].FechaEscritura).ToString("dd/MM/yyyy") : string.Empty;
                    }
                    else
                    {
                        vListInfoMueble[i].FechaEscritura = string.Empty;
                    }
                }
            }


            if (vListInfoMueble.Count > 0)
            {
                ViewState["SortedgvwInfoBien"] = vListInfoMueble;
                this.gvwInfoBien.DataSource = vListInfoMueble;
                this.gvwInfoBien.DataBind();
            }
            else
            {
                this.gvwInfoBien.EmptyDataText = EmptyDataText();
                this.gvwInfoBien.DataBind();
            }

            if (vListInfoTitulo.Count > 0)
            {
                ViewState["SortedgwvInfoTipoTitulo"] = vListInfoTitulo;
                this.gwvInfoTipoTitulo.DataSource = vListInfoTitulo;
                this.gwvInfoTipoTitulo.DataBind();
            }
            else
            {
                this.gwvInfoTipoTitulo.EmptyDataText = EmptyDataText();
                this.gwvInfoTipoTitulo.DataBind();
            }

            this.CargarGrillaHistorico(vIdDenunciaBien);

            List<DocumentosSolicitadosDenunciaBien> vLstBaseDatos = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            List<DocumentosSolicitadosDenunciaBien> vLstTemporal = new List<DocumentosSolicitadosDenunciaBien>();

            var vTempDocumentosSolicitados = GetSessionParameter("listaDocumentosSolicitados");
            if ((!vTempDocumentosSolicitados.ToString().Contains("")) || (vTempDocumentosSolicitados.ToString() != string.Empty))
            {
                vLstTemporal = (List<DocumentosSolicitadosDenunciaBien>)GetSessionParameter("listaDocumentosSolicitados");
                vLstBaseDatos.AddRange(vLstTemporal);
            }

            ViewState["SortedgvwDocumentacionRecibida"] = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vLstBaseDatos;
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Carga la grilla de la documentacion
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlDocumentacionSolicitada.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlDocumentacionSolicitada.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="vIdDenunciaBien"></param>
    protected void CargarGrillaHistorico(int vIdDenunciaBien)
    {
        List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
        foreach (HistoricoEstadosDenunciaBien item in vHistorico)
        {
            Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
            item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
            item.Responsable = vUsuario.PrimerNombre;
            if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
            {
                item.Responsable += " " + vUsuario.SegundoNombre;
            }

            item.Responsable += " " + vUsuario.PrimerApellido;
            if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
            {
                item.Responsable += " " + vUsuario.SegundoApellido;
            }
        }
        this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
        this.tbHistoria.DataSource = vHistorico;
        this.tbHistoria.DataBind();
    }

    /// <summary>
    /// Evento de cargar la lista de clase bien
    /// </summary>
    /// <param name="pIdSubTipoBien"></param>
    private void CargarListaClasebien(string pIdSubTipoBien)
    {
        try
        {
            List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(pIdSubTipoBien);
            vListaClaseBien = vListaClaseBien.OrderBy(p => p.NombreClaseBien).ToList();
            this.ddlClasebien.DataSource = vListaClaseBien;
            this.ddlClasebien.DataTextField = "NombreClaseBien";
            this.ddlClasebien.DataValueField = "IdClaseBien";
            this.ddlClasebien.DataBind();
            this.ddlClasebien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClasebien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento de cargar la lista de tipo subBien
    /// </summary>
    /// <param name="pIdTipoBien"></param>
    private void CargarListaSubTipoBien(string pIdTipoBien)
    {
        try
        {
            List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(pIdTipoBien);
            vListaSubTipoBien = vListaSubTipoBien.OrderBy(p => p.NombreSubTipoBien).ToList();
            this.ddlSubTipoBien.DataSource = vListaSubTipoBien;
            this.ddlSubTipoBien.DataTextField = "NombreSubTipoBien";
            this.ddlSubTipoBien.DataValueField = "IdSubTipoBien";
            this.ddlSubTipoBien.DataBind();
            this.ddlSubTipoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlSubTipoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura los valores para cargar la documentacion
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            toolBar.LipiarMensajeError();
            var IdDocumento = GetSessionParameter("Mostrencos.Protocolizacion.IdDocumento").GetType().Name;
            if (IdDocumento != "String")
            {
                if (ValidarDocumentacionRecibida() == false)
                {
                    int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("Mostrencos.Protocolizacion.IdDocumento");
                    int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                    bool vExiste = false;
                    if (this.fulArchivoRecibido.HasFile)
                    {
                        bool respPdf = this.fulArchivoRecibido.FileName.Contains(".pdf");
                        bool respImg = this.fulArchivoRecibido.FileName.Contains(".jpg");
                        string NombreArchivo = string.Empty;
                        if (respPdf)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }
                        else if (respImg)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }

                        if (NombreArchivo.Length > 50)
                        {
                            throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                        }
                        DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                        this.CargarArchivo(vDocumentoCargado);
                        this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", vDocumentoCargado);
                    }
                    DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                    if (vListaDocumentacion == null)
                    {
                        vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
                    }
                    if (vListaDocumentacion.Count() > 0)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                        else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.EsNuevo)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                        else if (IdDocumentosSolicitadosDenunciaBien != 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.IdDocumentosSolicitadosDenunciaBien != IdDocumentosSolicitadosDenunciaBien).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                    }

                    if (!vExiste)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien > 0)
                        {
                            bool vArchivoEsValido = false;
                            if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                            {
                                vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                if (vDocumentacionSolicitada == null)
                                    vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();

                                vArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);

                                if (vArchivoEsValido)
                                {
                                    if (IdDocumentosSolicitadosDenunciaBien == 0)
                                    {
                                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                    }
                                    else
                                    {
                                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                                    }

                                    vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                                    vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                                    vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                                    vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                                    vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                    this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", null);
                                }
                            }
                            else
                            {
                                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                                vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                            }

                            if (vArchivoEsValido)
                            {
                                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
                                if (!vDocumentacionSolicitada.EsNuevo)
                                {
                                    DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                                    vListaDocumentacion.Remove(vDocumentacion);
                                    vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                                }
                                vListaDocumentacion.Add(vDocumentacionSolicitada);
                                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                                this.CargarGrillaDocumentos();
                                this.txtObservacionesSolicitado.Enabled = true;
                                this.FechaSolicitud.Enabled = true;
                                this.ddlTipoDocumento.Enabled = true;
                                this.txtObservacionesSolicitado.Text = string.Empty;
                                this.FechaSolicitud.InitNull = true;
                                this.lblValidacionFechaSolicitud.Visible = false;
                                this.rbtEstadoDocumento.ClearSelection();
                                this.FechaRecibido.InitNull = true;
                                this.ddlTipoDocumento.SelectedValue = "-1";
                                this.txtObservacionesRecibido.Text = string.Empty;
                                this.lblNombreArchivo.Visible = false;
                            }
                        }
                    }
                }
            }
            else
            {
                if (this.ValidarDocumentacionSolicitada() == false)
                {
                    toolBar.LipiarMensajeError();
                    if (this.AdicionarDocumentosSolicitadosCache())
                    {
                        this.CargarGrillaDocumentosSolicitadosYRecibidos();
                        this.ddlTipoDocumento.SelectedValue = "-1";
                    }
                }
                else
                {
                    //toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 MB");
                }

                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = IdDenunciaBien + "/" + this.fulArchivoRecibido.FileName;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien.FileApoderado") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }

            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// GridView Documentos Solicitados y Recibidos
    /// </summary>
    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
        var Data = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        ViewState["SortedgvwDocumentacionRecibida"] = Data;
        gvwDocumentacionRecibida.DataSource = Data;
        gvwDocumentacionRecibida.DataBind();

    }

    /// <summary>
    /// Control de los botones en el toolbar
    /// </summary>
    public void ControlBotones()
    {
        this.toolBar.MostrarBotonNuevo(true);
        this.toolBar.MostrarBotonEditar(true);
        this.toolBar.OcultarBotonGuardar(false);
    }

    /// <summary>
    /// Control de campos luego de guardar
    /// </summary>
    /// <param name="activar"></param>
    public void ControlDeCamposDocGeneral(bool activar)
    {
        this.fechaSentencia.Enabled = activar;
        this.fechaConstanciaEjecutoriada.Enabled = activar;
        this.rblSentidoSentencia.Enabled = activar;
        this.fechaComunicacionFinanciera.Enabled = activar;
        this.fulArchivoRecibido.Enabled = activar;
        this.gvwDocumentacionRecibida.Enabled = activar;
        this.gvwInfoBien.Enabled = activar;
        this.gwvInfoTipoTitulo.Enabled = activar;
        this.btnInfoDenuncia.Enabled = activar;
        this.FechaSolicitud.Enabled = activar;

    }

    /// <summary>
    /// Control de campos luego de guardar
    /// </summary>
    /// <param name="activar"></param>
    public void ControlDeCamposDocSolicitada(bool activar)
    {
        this.ddlTipoDocumento.Enabled = activar;
        this.FechaSolicitud.Enabled = activar;
        this.txtObservacionesSolicitado.Enabled = activar;
    }

    /// <summary>
    /// Control de campos luego de guardar
    /// </summary>
    /// <param name="activar"></param>
    public void ControlDeCamposDocRecibida(bool activar)
    {
        this.rbtEstadoDocumento.Enabled = activar;
        this.FechaRecibido.Enabled = activar;
        this.fulArchivoRecibido.Enabled = activar;
        this.txtObservacionesRecibido.Enabled = activar;
        this.btnAdd.Enabled = activar;
        this.btnAplicar.Enabled = activar;
    }

    /// <summary>
    /// Control del panel documentación recibida, botones add y aplicar
    /// </summary>
    private void ControlPanelDocRecibida(bool activar)
    {
        this.btnAplicar.Visible = activar;
        this.rbtEstadoDocumento.Enabled = activar;
        this.FechaRecibido.Enabled = activar;
        this.fulArchivoRecibido.Enabled = activar;
        this.txtObservacionesRecibido.Enabled = activar;
    }

    /// <summary>
    /// Control de panel documentación solicitada
    /// </summary>
    /// <param name="activa">Estado del campo</param>
    private void ControlPanelDocSolicitada(bool activa)
    {
        this.btnAdd.Visible = false;
        this.btnAplicar.Visible = activa;
        this.txtObservacionesSolicitado.Enabled = activa;
        this.FechaSolicitud.Enabled = activa;
        this.ddlTipoDocumento.Enabled = activa;
    }

    /// <summary>
    /// Método para guardar un RegistroDenuncia Nuevo o Editado.
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));

            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            if (vRegistroDenuncia.TipoTramite != null)
            {
                bool bandera = true;
                if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                {
                    if (this.rblSentidoSentencia.SelectedValue == "")
                    {
                        this.reqSentido.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.reqSentido.Visible = false;
                    }
                    DateTime fechaComunicacion = this.fechaConstanciaEjecutoriada.Date;
                    if (fechaComunicacion == new DateTime())
                    {
                        this.reqFecha.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.reqFecha.Visible = false;
                    }
                }
                else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                {
                    InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                    if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                    {
                        if (this.rblSentidoSentencia.SelectedValue == "")
                        {
                            this.reqSentido.Visible = true;
                            bandera = false;
                        }
                        else
                        {
                            this.reqSentido.Visible = false;
                        }
                        DateTime fechaComunicacion = this.fechaConstanciaEjecutoriada.Date;
                        if (fechaComunicacion == new DateTime())
                        {
                            this.reqFecha.Visible = true;
                            bandera = false;
                        }
                        else
                        {
                            this.reqFecha.Visible = false;
                        }
                    }
                }
                else
                {
                    this.reqSentido.Visible = false;
                    this.reqFecha.Visible = false;
                    bandera = true;
                }

                if (bandera)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);

                        string vJuzgadoSentencia = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        string vNombreJuzgado = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        string vMunicipioJudicial = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                        string vDepartamentoDespacho = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;

                        DateTime vfechaSentencia = this.fechaSentencia.Date;
                        TramiteJudicial vTramiteJudicial = new TramiteJudicial();


                        if (this.rblSentidoSentencia.SelectedValue.Equals("radioAdjudicada"))
                        {
                            vTramiteJudicial.SentidoSentencia = "Adjudicada";
                            vTramiteJudicial.IdSentidoSentencia = 1;

                        }
                        else if (this.rblSentidoSentencia.SelectedValue.Equals("radioNoAdjudicada"))
                        {
                            vTramiteJudicial.SentidoSentencia = "No Adjudicada";
                            vTramiteJudicial.IdSentidoSentencia = 2;
                        }
                        else if (this.rblSentidoSentencia.SelectedValue.Equals("radioAdjudicadaParcial"))
                        {
                            vTramiteJudicial.SentidoSentencia = "Adjudicada parcialmente";
                            vTramiteJudicial.IdSentidoSentencia = 3;
                        }

                        vTramiteJudicial.IdTipoTramite = 1;
                        vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                        vTramiteJudicial.TipoTramite = "JUDICIAL";
                        vTramiteJudicial.FechaSentencia = vfechaSentencia;
                        vTramiteJudicial.UsuarioModifica = this.GetSessionUser().NombreUsuario; ;
                        vTramiteJudicial.FechaModifica = DateTime.Now;
                        vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                        if (this.fechaComunicacionFinanciera.Date != new DateTime())
                        {
                            vTramiteJudicial.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                        }
                        else
                        {
                            this.fechaComunicacionFinanciera.InitNull = true;
                            vTramiteJudicial.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                        }

                        this.InformacionAudioria(vTramiteJudicial, this.pageName, SolutionPage.Add);
                        int vResultadoTrx = this.vMostrencosService.ActualizarTramiteJudicialxIdDenunciaBien(vTramiteJudicial);
                        if (vResultadoTrx > 0)
                        {
                            /// Grilla Titulo Valor

                            List<ResultFiltroTitulo> vListInfoTitulo = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];
                            if (vListInfoTitulo != null)
                            {
                                vListInfoTitulo.ForEach(item =>
                                {
                                    if (item.Modificado)
                                    {
                                        this.InformacionAudioria(vTramiteJudicial, this.pageName, SolutionPage.Add);
                                        this.vMostrencosService.ActualizarTituloBienxIdDenuncia(item.estado, vIdDenunciaBien, item.FechaAdjudicados, item.IdTituloValor);
                                    }
                                });
                            }


                            /// Grilla Mueble

                            List<InfoMuebleProtocolizacion> vListInfoMueble = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                            if (vListInfoMueble != null)
                            {
                                vListInfoMueble.ForEach(item =>
                                {
                                    if (item.Modificado)
                                    {
                                        this.InformacionAudioria(vTramiteJudicial, this.pageName, SolutionPage.Add);
                                        this.vMostrencosService.ActualizarEstadoBienxIdDenuncia(item.EstadoBien, vIdDenunciaBien, item.FechaAdjudicado, item.IdMuebleInmueble);
                                    }
                                });
                            }

                            if (vListInfoTitulo == null && vListInfoMueble == null)
                            {
                                DateTime fechaComunicacion = this.fechaComunicacionFinanciera.Date;
                                if (fechaComunicacion == new DateTime())
                                {
                                    this.InformacionAudioria(vTramiteJudicial, this.pageName, SolutionPage.Add);
                                    this.vMostrencosService.ActualizarFechaComunicacionTitulo(vIdDenunciaBien, fechaComunicacion);
                                }
                            }


                            this.ActualizarHistorico(15, 20, 39, 26);
                            this.DeshabilitarCamposyPaneles();
                            this.toolBar.MostrarBotonConsultar();
                            this.txtObservacionesRecibido.Text = string.Empty;
                            this.txtObservacionesSolicitado.Enabled = true;
                            this.FechaSolicitud.InitNull = true;
                            this.FechaSolicitud.Enabled = false;
                            this.ddlTipoDocumento.SelectedValue = "-1";
                            this.ddlTipoDocumento.Enabled = true;
                            this.txtObservacionesSolicitado.Text = string.Empty;
                            this.lblValidacionFechaSolicitud.Visible = false;
                            this.rblSentidoSentencia.Enabled = false;
                            this.rbtEstadoDocumento.ClearSelection();
                            this.FechaRecibido.InitNull = true;
                            this.ddlTipoDocumento.SelectedValue = "-1";
                            this.txtObservacionesRecibido.Text = string.Empty;
                            this.lblNombreArchivo.Visible = false;
                            this.lblNombreArchivo.Text = string.Empty;
                            this.btnAplicar.Visible = false;
                            this.btnAdd.Visible = false;
                            this.toolBar.OcultarBotonBuscar(false);
                            this.toolBar.MostrarBotonNuevo(true);
                            this.toolBar.MostrarBotonEditar(true);
                            this.toolBar.OcultarBotonGuardar(false);

                        }
                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        {
                            InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);

                            string vJuzgadoSentencia = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            string vNombreJuzgado = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                            string vMunicipioJudicial = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                            string vDepartamentoDespacho = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;

                            DateTime vfechaSentencia = this.fechaSentencia.Date;
                            TramiteJudicial vTramiteJudicial = new TramiteJudicial();


                            if (this.rblSentidoSentencia.SelectedValue.Equals("radioAdjudicada"))
                            {
                                vTramiteJudicial.SentidoSentencia = "Adjudicada";
                                vTramiteJudicial.IdSentidoSentencia = 1;

                            }
                            else if (this.rblSentidoSentencia.SelectedValue.Equals("radioNoAdjudicada"))
                            {
                                vTramiteJudicial.SentidoSentencia = "No Adjudicada";
                                vTramiteJudicial.IdSentidoSentencia = 2;
                            }
                            else if (this.rblSentidoSentencia.SelectedValue.Equals("radioAdjudicadaParcial"))
                            {
                                vTramiteJudicial.SentidoSentencia = "Adjudicada parcialmente";
                                vTramiteJudicial.IdSentidoSentencia = 3;
                            }

                            vTramiteJudicial.IdTipoTramite = 1;
                            vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                            vTramiteJudicial.TipoTramite = "JUDICIAL";
                            vTramiteJudicial.FechaSentencia = vfechaSentencia;
                            vTramiteJudicial.UsuarioModifica = this.GetSessionUser().NombreUsuario; ;
                            vTramiteJudicial.FechaModifica = DateTime.Now;
                            vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien;
                            if (this.fechaComunicacionFinanciera.Date != new DateTime() && this.fechaComunicacionFinanciera.Date.ToString() != "1/01/1900 12:00:00 a. m.")
                            {
                                vTramiteJudicial.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                            }

                            this.InformacionAudioria(vTramiteJudicial, this.pageName, SolutionPage.Add);
                            int vResultadoTrx = this.vMostrencosService.ActualizarTramiteJudicialxIdDenunciaBien(vTramiteJudicial);
                        }
                        List<ResultFiltroTitulo> vListInfoTitulo = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];
                        if (vListInfoTitulo != null)
                        {
                            vListInfoTitulo.ForEach(item =>
                            {
                                if (item.Modificado)
                                {
                                    this.InformacionAudioria(vListInfoTitulo[0], this.pageName, SolutionPage.Add);
                                    this.vMostrencosService.ActualizarTituloBienxIdDenuncia(item.estado, vIdDenunciaBien, item.FechaAdjudicados, item.IdTituloValor);
                                }
                            });
                        }

                        List<InfoMuebleProtocolizacion> vListInfoMueble = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                        if (vListInfoMueble != null)
                        {
                            vListInfoMueble.ForEach(item =>
                            {
                                if (item.Modificado)
                                {
                                    this.InformacionAudioria(vListInfoMueble[0], this.pageName, SolutionPage.Add);
                                    this.vMostrencosService.ActualizarEstadoBienxIdDenuncia(item.EstadoBien, vIdDenunciaBien, item.FechaAdjudicado, item.IdMuebleInmueble);
                                    DateTime FechaEscritura = (!item.FechaEscritura.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaEscritura) : Convert.ToDateTime("01/01/1900");
                                    decimal NumeroEscritura = (!item.NumeroEscritura.Equals(string.Empty)) ? Convert.ToDecimal(item.NumeroEscritura) : 0;
                                    decimal NumeroRegistro = (!item.NumeroRegistro.Equals(string.Empty)) ? Convert.ToDecimal(item.NumeroRegistro) : 0;
                                    DateTime FechaIngresoICBF = (!item.FechaIngresoICBF.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaIngresoICBF) : Convert.ToDateTime("01/01/1900");
                                    DateTime FechaInstrumentosPublicos = (!item.FechaInstrumentosPublicos.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaInstrumentosPublicos.ToString()) : Convert.ToDateTime("01/01/1900");
                                    DateTime FechaComunicacion = (!item.FechaComunicacion.Equals(string.Empty)) ? Convert.ToDateTime(item.FechaComunicacion.ToString()) : Convert.ToDateTime("01/01/1900");
                                    this.InformacionAudioria(vListInfoMueble[0], this.pageName, SolutionPage.Add);
                                    this.vMostrencosService.ActualizarTramiteNotarialxIdDenuncia(vIdDenunciaBien, FechaEscritura, NumeroEscritura, NumeroRegistro, FechaIngresoICBF,
                                            FechaInstrumentosPublicos, FechaComunicacion);
                                }
                            });
                        }

                        if (vListInfoTitulo == null && vListInfoMueble == null)
                        {
                            DateTime fechaComunicacion = this.fechaComunicacionFinanciera.Date;
                            if (fechaComunicacion == new DateTime())
                            {
                                this.InformacionAudioria(new TramiteNotarial(), this.pageName, SolutionPage.Add);
                                this.vMostrencosService.ActualizarFechaComunicacionTitulo(vIdDenunciaBien, fechaComunicacion);
                            }
                        }

                        this.ActualizarHistorico(15, 20, 40, 27);
                        this.DeshabilitarCamposyPaneles();
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.txtObservacionesSolicitado.Enabled = true;
                        this.FechaSolicitud.InitNull = true;
                        this.FechaSolicitud.Enabled = false;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.ddlTipoDocumento.Enabled = true;
                        this.txtObservacionesSolicitado.Text = string.Empty;
                        this.lblValidacionFechaSolicitud.Visible = false;
                        this.rblSentidoSentencia.Enabled = false;
                        this.rbtEstadoDocumento.ClearSelection();
                        this.FechaRecibido.InitNull = true;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.lblNombreArchivo.Visible = false;
                        this.lblNombreArchivo.Text = string.Empty;
                        this.btnAplicar.Visible = false;
                        this.btnAdd.Visible = false;
                        this.radioAdjudicado.Enabled = false;
                        this.radioNoAdjudicado.Enabled = false;
                        this.toolBar.MostrarBotonNuevo(true);
                        this.toolBar.MostrarBotonEditar(true);
                        this.toolBar.OcultarBotonGuardar(false);
                    }

                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];
                    string vRuta = string.Empty;

                    if (vListaDocumentos != null)
                    {
                        if (vListaDocumentosEliminar != null)
                        {
                            foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosEliminar)
                            {
                                if (item.IdDocumentosSolicitadosDenunciaBien > 0)
                                {
                                    DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
                                    vdo.IdDocumentosSolicitadosDenunciaBien = item.IdDocumentosSolicitadosDenunciaBien;
                                    vdo.IdEstadoDocumento = 6;
                                    vdo.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                                    vdo.RutaArchivo = "";
                                    this.InformacionAudioria(item, this.pageName, SolutionPage.Add);
                                    this.vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(vdo);
                                }
                            }
                            this.ViewState["DocumentosEliminar"] = null;
                        }

                        vListaDocumentos.ForEach(item =>
                        {
                            item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                            this.InformacionAudioria(item, this.pageName, SolutionPage.Edit);
                            if (item.EsNuevo == true)
                                this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                            else
                                this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
                        });
                    }

                    this.CargarGrillas();
                    this.ControlDeCamposDocRecibida(false);
                    this.ControlDeCamposDocGeneral(false);
                    this.toolBar.OcultarBotonBuscar(false);
                    this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda archivos fisico en el servidor
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    /// <param name="pPosition"></param>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        bool Esresultado = false;
        try
        {
            HttpFileCollection files = Request.Files;
            if (files.Count > 0)
            {
                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                string extJgp = ".JPG".ToLower();
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos.");
                }
                int tamano = this.fulArchivoRecibido.FileName.Length;
                if (extFile != extPdf && extFile != extJgp)
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Mb");
                }

                HttpPostedFile file = files[pPosition];
                if (file.ContentLength > 0)
                {
                    string rutaParcial = string.Empty;
                    string carpetaBase = string.Empty;
                    string filePath = string.Empty;
                    if (extFile.Equals(".pdf"))
                    {
                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");

                    }
                    else if (extFile.ToLower().Equals(".jpg"))
                    {

                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                    }
                    bool EsexisteCarpeta = System.IO.Directory.Exists(carpetaBase);
                    if (!EsexisteCarpeta)
                    {
                        System.IO.Directory.CreateDirectory(carpetaBase);
                        Esresultado = true;
                    }
                    this.vFileName = rutaParcial;
                    file.SaveAs(filePath);
                    Esresultado = true;
                }
            }
        }
        catch (HttpRequestValidationException httpex)
        {
            throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return Esresultado;
    }

    /// <summary>
    /// Guardar el documento.
    /// </summary>
    /// <param name="pDocumento">The p documento.</param>
    public void GuardarDocumento(DocumentosSolicitadosDenunciaBien pDocumento)
    {
        try
        {
            if (pDocumento.IdDocumentosSolicitadosDenunciaBien == 0)
            {
                this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(pDocumento);
                this.CargarGrillas();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Habilitar panel de recibidos
    /// </summary>
    /// <param name="EsHabilitado"></param>
    private void HabilitarPanelRecibido(Boolean EsHabilitado)
    {
        try
        {
            this.rbtEstadoDocumento.Enabled = EsHabilitado;
            this.fulArchivoRecibido.Enabled = EsHabilitado;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Limpia la informacion del panel de la informacion del mueble e inmueble
    /// </summary>
    private void LimpiarInformacionMuebleInmueble()
    {
        this.ddlTipoBien.SelectedValue = "-1";
        this.ddlSubTipoBien.SelectedValue = "-1";
        this.ddlClasebien.SelectedValue = "-1";
        this.radioAdjudicado.Checked = false;
        this.radioNoAdjudicado.Checked = false;
        this.dtFechaAdjudicacionBien.Date = new DateTime();
        this.txtDescripcionBien.Text = string.Empty;
        this.radioAdjudicado.Checked = false;
        this.radioAdjudicado.Enabled = false;
        this.radioNoAdjudicado.Checked = false;
        this.radioNoAdjudicado.Enabled = false;
        this.btnAplicarMuebleInmueble.Visible = false;
        this.dtFechaAdjudicacionBien.InitNull = true;

        this.txtFechaEscritura.InitNull = true;
        this.txtNumeroEscritura.Text = null;
        this.txtNotariaDondeRegistra.Text = null;
        this.txtNumeroRegistro.Text = null;
        this.txtFechaIngresoICBF.InitNull = true;
        this.txtFechaInstrumentosPublicos.InitNull = true;

        this.txtFechaEscritura.Enabled = false;
        this.txtNumeroEscritura.Enabled = false;
        this.txtNotariaDondeRegistra.Enabled = false;
        this.txtNumeroRegistro.Enabled = false;
        this.txtFechaIngresoICBF.Enabled = false;
        this.txtFechaInstrumentosPublicos.Enabled = false;
        this.fechaComunicacionFinanciera.InitNull = true;
    }

    /// <summary>
    /// Limpia la informacion del panel del titulo de valor
    /// </summary>
    private void LimpiarInformacionTituloValor()
    {
        this.txtTipoTitulo.Text = string.Empty;
        this.txtFechaAdjudicacionTitulo.InitNull = true;
        this.txtFechaAdjudicacionTitulo.Enabled = false;
        this.txtDescripcionBienTitulo.Text = string.Empty;
        this.rbnEstadoBnTituloAdjudicado.Checked = false;
        this.rbnEstadoBnTituloAdjudicado.Enabled = false;
        this.rbnEstadoBnTituloNoAdjudicado.Checked = false;
        this.rbnEstadoBnTituloNoAdjudicado.Enabled = false;
        this.btnAplicarActualizacionTitulo.Visible = false;
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The Direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Validar Documentación Recibida
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblrqfEstadoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblrqfEstadoRecibido.Visible = false;

                if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblrqfFechaRecibido.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblrqfFechaRecibido.Visible = false;
                    vEsrequerido = false;

                    //Validar Campo Fecha Recibido
                    if (this.FechaRecibido.Date > DateTime.Now)
                    {
                        this.lblValidacionFechaMaxima.Visible = true;
                        vEsrequerido = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblValidacionFechaMaxima.Visible = false;
                        vEsrequerido = false;
                    }

                    //Validar Campo Fecha Recibido
                    if (this.FechaRecibido.Date < this.FechaSolicitud.Date)
                    {
                        this.lblValidacionFechaSolicitud.Visible = true;
                        vEsrequerido = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblValidacionFechaSolicitud.Visible = false;
                        vEsrequerido = false;
                    }
                }

                //Validar Campo Nombre Archivo
                if (this.fulArchivoRecibido.FileName == "")
                {
                    this.lblRqfNombreArchivo.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblRqfNombreArchivo.Visible = false;
                    vEsrequerido = false;
                }

                //Validar Observaciones de documento recibido
                if (this.txtObservacionesRecibido.Text == string.Empty)
                {
                    this.lblObservacionesRecibido.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblObservacionesRecibido.Visible = false;
                    vEsrequerido = false;
                }
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Solicitada
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionSolicitada()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblReqTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblReqTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblReqFechaSolicitud.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblReqFechaSolicitud.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento cuando se selecciona un elemento de la lista de SubTipoBien
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSubTipoBien_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!this.ddlSubTipoBien.SelectedValue.Equals("-1"))
        {
            this.CargarListaClasebien(this.ddlSubTipoBien.SelectedValue);
            string vTipoParametro = this.ddlSubTipoBien.SelectedValue.Substring(0, 3);

            if (!vTipoParametro.Equals("308"))
            {
                this.hfTipoParametro.Value = vTipoParametro;
                this.hfCodigoParametro.Value = this.ddlSubTipoBien.SelectedValue;
            }
        }
    }

    /// <summary>
    /// Evento cuando se selecciona un elemento de la lista de tipo de bien
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoBien_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (this.ddlTipoBien.SelectedValue)
        {
            case "-1":
                this.hfTipoParametro.Value = string.Empty;
                break;

            case "3":
                this.CargarListaSubTipoBien("3");
                break;

            case "2":
                this.CargarListaSubTipoBien("2");
                break;
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbHistoria.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.tbHistoria.DataSource = vList;
                this.tbHistoria.DataBind();
            }
            else
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                this.CargarGrillaHistorico(vIdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();
        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }
        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdFase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdFase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdActuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdActuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdAccion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdAccion).ToList();
                }

                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.tbHistoria.DataSource = vResult;
        this.tbHistoria.DataBind();
    }

    /// <summary>
    /// Evento que se ejecuta cuando se selecciona un comando de la grilla de Info muebles
    /// </summary>
    /// <param name="sender">sender attribute</param>
    /// <param name="e">e attribute</param>
    protected void gvwInfoBien_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();

            string vSentidoSentencia = (this.rblSentidoSentencia.SelectedItem != null) ? this.rblSentidoSentencia.SelectedItem.Text : "";
            if (e.CommandName == "Editar")
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                this.btnAplicarActualizacionTitulo.Enabled = true;
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        this.idBien.Text = gvr.Cells[15].Text;
                        string vEstadoBien = gvr.Cells[6].Text;
                        if (vEstadoBien.ToUpper().Equals("ADJUDICADO"))
                        {
                            this.radioAdjudicado.Checked = true;
                            this.radioNoAdjudicado.Checked = false;
                            this.dtFechaAdjudicacionBien.Enabled = true;
                        }
                        else if (vEstadoBien.ToUpper().Equals("NO ADJUDICADO"))
                        {
                            this.radioAdjudicado.Checked = false;
                            this.radioNoAdjudicado.Checked = true;
                            this.dtFechaAdjudicacionBien.Enabled = false;
                            this.dtFechaAdjudicacionBien.InitNull = true;
                        }

                        if (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper().Equals("RECHAZADO") && vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper().Equals("JUDICIAL"))
                        {
                            if (vSentidoSentencia.ToUpper().Equals("ADJUDICADA") || vSentidoSentencia.ToUpper().Equals("ADJUDICADA PARCIALMENTE"))
                            {
                                this.radioAdjudicado.Enabled = true;
                                this.radioNoAdjudicado.Enabled = true;
                                string vTipoBien = gvr.Cells[0].Text;
                                string vSubTipoBien = gvr.Cells[2].Text;
                                string vClaseBien = gvr.Cells[4].Text;
                                string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                                string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                                this.CargarListaSubTipoBien(vTipoBien);
                                this.CargarListaClasebien(vSubTipoBien);
                                this.ddlTipoBien.SelectedValue = vTipoBien;
                                this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                                this.ddlClasebien.SelectedValue = vClaseBien;
                                this.txtDescripcionBien.Text = vDescripcionBien;
                                if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                                {
                                    this.dtFechaAdjudicacionBien.InitNull = true;
                                }
                                else
                                {
                                    this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                                }
                                this.btnAplicarMuebleInmueble.Visible = true;

                                if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                                {
                                    DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                                    this.txtFechaEscritura.Date = fechaEscrituracion;
                                }
                                string numeroEscritura = gvr.Cells[7].Text;
                                string notariaDondeRegistra = gvr.Cells[11].Text;
                                this.txtNumeroEscritura.Text = numeroEscritura;
                                this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                            }
                            else
                            {
                                string vTipoBien = gvr.Cells[0].Text;
                                string vSubTipoBien = gvr.Cells[2].Text;
                                string vClaseBien = gvr.Cells[4].Text;
                                string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                                string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                                this.CargarListaSubTipoBien(vTipoBien);
                                this.CargarListaClasebien(vSubTipoBien);
                                this.ddlTipoBien.SelectedValue = vTipoBien;
                                this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                                this.ddlClasebien.SelectedValue = vClaseBien;
                                this.txtDescripcionBien.Text = vDescripcionBien;
                                if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                                {
                                    this.dtFechaAdjudicacionBien.InitNull = true;
                                }
                                else
                                {
                                    this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                                }
                                //this.btnAplicarMuebleInmueble.Visible = true;

                                if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                                {
                                    DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                                    this.txtFechaEscritura.Date = fechaEscrituracion;
                                }
                                string numeroEscritura = gvr.Cells[7].Text;
                                string notariaDondeRegistra = gvr.Cells[11].Text;
                                this.txtNumeroEscritura.Text = numeroEscritura;
                                this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                            }
                        }
                        else
                        {
                            if (gvr.Cells[6].Text.ToUpper().Equals("ADJUDICADO") && (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                           && vInfoEscrituracion.FechaSolemnizacion.Date != new DateTime())
                            {
                                this.radioAdjudicado.Enabled = true;
                                this.radioNoAdjudicado.Enabled = true;
                                string vTipoBien = gvr.Cells[0].Text;
                                string vSubTipoBien = gvr.Cells[2].Text;
                                string vClaseBien = gvr.Cells[4].Text;
                                string vDescripcionBien = gvr.Cells[4].Text;
                                string vFechaAdjudicadoBien = (gvr.Cells[8].Text).Equals("&nbsp;") ? null : gvr.Cells[8].Text;
                                string vFechaEscritura = (gvr.Cells[9].Text).Equals("&nbsp;") ? null : gvr.Cells[9].Text;
                                string vFechaIngreso = (gvr.Cells[13].Text).Equals("&nbsp;") ? null : gvr.Cells[13].Text;

                                this.CargarListaSubTipoBien(vTipoBien);
                                this.CargarListaClasebien(vSubTipoBien);
                                this.ddlTipoBien.SelectedValue = vTipoBien;
                                this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                                this.ddlClasebien.SelectedValue = vClaseBien;
                                this.txtDescripcionBien.Text = vDescripcionBien;
                                if (vFechaAdjudicadoBien != null)
                                {
                                    this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                                }
                                else
                                {
                                    this.dtFechaAdjudicacionBien.InitNull = true;
                                }

                                if (vFechaEscritura != null)
                                {
                                    DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                                    this.txtFechaEscritura.Date = fechaEscrituracion;
                                }
                                else
                                {
                                    this.txtFechaEscritura.InitNull = true;
                                }

                                if (vFechaIngreso != null)
                                {
                                    DateTime fechaingresoICBF = DateTime.Parse(gvr.Cells[13].Text);
                                    this.txtFechaIngresoICBF.Date = fechaingresoICBF;
                                }
                                else
                                {
                                    this.txtFechaIngresoICBF.InitNull = true;
                                }

                                this.btnAplicarMuebleInmueble.Visible = true;

                                string numeroEscritura = (gvr.Cells[10].Text).Equals("&nbsp;") ? null : gvr.Cells[10].Text;
                                string numeroRegistro = (gvr.Cells[12].Text).Equals("&nbsp;") ? null : gvr.Cells[12].Text;
                                string fechaInst = (gvr.Cells[14].Text).Equals("&nbsp;") ? null : gvr.Cells[14].Text;

                                if (fechaInst != null)
                                {
                                    DateTime fechaInstr = DateTime.Parse(fechaInst);
                                    this.txtFechaInstrumentosPublicos.Date = fechaInstr;
                                }
                                else
                                {
                                    this.txtFechaInstrumentosPublicos.InitNull = true;
                                }

                                string notariaDondeRegistra = gvr.Cells[11].Text;
                                this.txtNumeroRegistro.Text = numeroRegistro;
                                this.txtNumeroEscritura.Text = numeroEscritura;
                                this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                            }
                            else
                            {
                                this.radioAdjudicado.Enabled = true;
                                this.radioNoAdjudicado.Enabled = true;
                                string vTipoBien = gvr.Cells[0].Text;
                                string vSubTipoBien = gvr.Cells[2].Text;
                                string vClaseBien = gvr.Cells[4].Text;
                                string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                                string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                                this.CargarListaSubTipoBien(vTipoBien);
                                this.CargarListaClasebien(vSubTipoBien);
                                this.ddlTipoBien.SelectedValue = vTipoBien;
                                this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                                this.ddlClasebien.SelectedValue = vClaseBien;
                                this.txtDescripcionBien.Text = vDescripcionBien;
                                if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                                {
                                    this.dtFechaAdjudicacionBien.InitNull = true;
                                }
                                else
                                {
                                    this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                                }
                                //this.btnAplicarMuebleInmueble.Visible = true;

                                if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                                {
                                    DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                                    this.txtFechaEscritura.Date = fechaEscrituracion;
                                }
                                string numeroEscritura = gvr.Cells[7].Text;
                                string notariaDondeRegistra = gvr.Cells[11].Text;
                                this.txtNumeroEscritura.Text = numeroEscritura;
                                this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                                this.btnAplicarMuebleInmueble.Visible = true;
                            }

                        }

                        if (vEstadoBien.ToUpper().Equals("ADJUDICADO") && (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                            && !vInfoEscrituracion.FechaSolemnizacion.ToString().Equals("1900-01-01 00:00:00.000"))
                        {
                            this.CamposEscriturizacion(true);
                        }
                        //else
                        //{
                        //    this.CamposEscriturizacion(false);
                        //    this.toolBar.MostrarMensajeError("El estado del bien debe ser Adjudicado, la Decisión del Trámite Notarial debe ser Aceptado y debe tener registrado la Fecha de Solemnización");
                        //}
                    }
                    else if ((vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL") && vSentidoSentencia.ToUpper().Equals("ADJUDICADA")) || (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL") && vSentidoSentencia.ToUpper().Equals("ADJUDICADA PARCIALMENTE")))
                    {
                        this.radioAdjudicado.Enabled = true;
                        this.radioNoAdjudicado.Enabled = true;
                        string vTipoBien = gvr.Cells[0].Text;
                        string vSubTipoBien = gvr.Cells[2].Text;
                        string EstadoBien = gvr.Cells[6].Text;
                        string vClaseBien = gvr.Cells[4].Text;
                        string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                        string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                        this.idBien.Text = gvr.Cells[15].Text;
                        this.CargarListaSubTipoBien(vTipoBien);
                        this.CargarListaClasebien(vSubTipoBien);
                        this.ddlTipoBien.SelectedValue = vTipoBien;
                        this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                        this.ddlClasebien.SelectedValue = vClaseBien;
                        this.txtDescripcionBien.Text = vDescripcionBien;


                        if (EstadoBien.ToUpper().Equals("ADJUDICADO"))
                        {
                            this.radioAdjudicado.Checked = true;
                            this.radioNoAdjudicado.Checked = false;
                            this.dtFechaAdjudicacionBien.Enabled = true;
                        }
                        else if (EstadoBien.ToUpper().Equals("NO ADJUDICADO"))
                        {
                            this.radioAdjudicado.Checked = false;
                            this.radioNoAdjudicado.Checked = true;
                            this.dtFechaAdjudicacionBien.Enabled = false;
                            this.dtFechaAdjudicacionBien.InitNull = true;
                        }



                        if (vFechaAdjudicadoBien != "" && vFechaAdjudicadoBien != "&nbsp;")
                        {
                            this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                        }
                        else
                        {
                            this.dtFechaAdjudicacionBien.InitNull = true;
                        }
                        this.btnAplicarMuebleInmueble.Visible = true;
                    }
                    else
                    {
                        string vTipoBien = gvr.Cells[0].Text;
                        string vSubTipoBien = gvr.Cells[2].Text;
                        string vClaseBien = gvr.Cells[4].Text;
                        string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                        string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                        this.CargarListaSubTipoBien(vTipoBien);
                        this.CargarListaClasebien(vSubTipoBien);
                        this.ddlTipoBien.SelectedValue = vTipoBien;
                        this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                        this.ddlClasebien.SelectedValue = vClaseBien;
                        this.txtDescripcionBien.Text = vDescripcionBien;
                        if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                        {
                            this.dtFechaAdjudicacionBien.InitNull = true;
                        }
                        else
                        {
                            this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                        }
                        //this.btnAplicarMuebleInmueble.Visible = true;

                        if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                        {
                            DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                            this.txtFechaEscritura.Date = fechaEscrituracion;
                        }
                        string numeroEscritura = gvr.Cells[7].Text;
                        string notariaDondeRegistra = gvr.Cells[11].Text;
                        this.txtNumeroEscritura.Text = numeroEscritura;
                        this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                    }
                }
                else
                {
                    string vTipoBien = gvr.Cells[0].Text;
                    string vSubTipoBien = gvr.Cells[2].Text;
                    string vClaseBien = gvr.Cells[4].Text;
                    string vDescripcionBien = ((Label)gvr.FindControl("lbDescripcionMueble")).Text;
                    string vFechaAdjudicadoBien = gvr.Cells[8].Text;
                    this.CargarListaSubTipoBien(vTipoBien);
                    this.CargarListaClasebien(vSubTipoBien);
                    this.ddlTipoBien.SelectedValue = vTipoBien;
                    this.ddlSubTipoBien.SelectedValue = vSubTipoBien;
                    this.ddlClasebien.SelectedValue = vClaseBien;
                    this.txtDescripcionBien.Text = vDescripcionBien;
                    if (String.IsNullOrEmpty(vFechaAdjudicadoBien) || vFechaAdjudicadoBien == "&nbsp;")
                    {
                        this.dtFechaAdjudicacionBien.InitNull = true;
                    }
                    else
                    {
                        this.dtFechaAdjudicacionBien.Date = Convert.ToDateTime(vFechaAdjudicadoBien);
                    }
                    //this.btnAplicarMuebleInmueble.Visible = true;

                    if (!gvr.Cells[9].Text.Equals(string.Empty) && !gvr.Cells[9].Text.Equals("&nbsp;"))
                    {
                        DateTime fechaEscrituracion = DateTime.Parse(gvr.Cells[9].Text);
                        this.txtFechaEscritura.Date = fechaEscrituracion;
                    }
                    string numeroEscritura = gvr.Cells[7].Text;
                    string notariaDondeRegistra = gvr.Cells[11].Text;
                    this.txtNumeroEscritura.Text = numeroEscritura;
                    this.txtNotariaDondeRegistra.Text = notariaDondeRegistra;
                }
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CamposEscriturizacion(bool estado)
    {

        this.txtFechaEscritura.Enabled = estado;
        this.txtNumeroEscritura.Enabled = estado;
        this.txtNotariaDondeRegistra.Enabled = false;
        this.txtNumeroRegistro.Enabled = estado;
        this.txtFechaIngresoICBF.Enabled = estado;
        this.txtFechaInstrumentosPublicos.Enabled = estado;

    }

    /// <summary>
    /// Validación para el valor por defecto de la fecha adjudicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInfoBien_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int LoopCounter;

        // Variable for starting index. Use this to make sure the tabindexes start at a higher
        // value than any other controls above the gridview. 
        // Header row indexes will be 110, 111, 112...
        // First data row will be 210, 211, 212... 
        // Second data row 310, 311, 312 .... and so on
        int tabIndexStart = 10;

        for (LoopCounter = 0; LoopCounter < e.Row.Cells.Count; LoopCounter++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                // Check to see if the cell contains any controls
                if (e.Row.Cells[LoopCounter].Controls.Count > 0)
                {
                    // Set the TabIndex. Increment RowIndex by 2 because it starts at -1
                    ((LinkButton)e.Row.Cells[LoopCounter].Controls[0]).TabIndex = -1;//short.Parse((e.Row.RowIndex + 2).ToString() + tabIndexStart++.ToString());
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.TabIndex = -1;
            if (e.Row.Cells[8].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[8].Text.Equals("01/01/0001") || e.Row.Cells[8].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[8].Text = "";
            }
        }
    }

    /// <summary>
    /// Ordenamiento grilla información del bien
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInfoBien_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<InfoMuebleProtocolizacion> vList = new List<InfoMuebleProtocolizacion>();
        List<InfoMuebleProtocolizacion> vResult = new List<InfoMuebleProtocolizacion>();

        if (ViewState["SortedgvwInfoBien"] != null)
        {
            vList = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
        }

        if (e.SortExpression.Equals("TipoBien"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.TipoBien).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.TipoBien).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("SubTipoBien"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.SubTipoBien).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.SubTipoBien).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("ClaseBien"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.ClaseBien).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.ClaseBien).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("EstadoBien"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.EstadoBien).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.EstadoBien).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("DescripcionBien"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.DescripcionBien).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.DescripcionBien).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("FechaAdjudicado"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.FechaAdjudicado).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.FechaAdjudicado).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("FechaEscritura"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.FechaEscritura).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.FechaEscritura).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("NumeroEscritura"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NumeroEscritura).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NumeroEscritura).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("Notaria"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.Notaria).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.Notaria).ToList();
                Direction = SortDirection.Ascending;
            }
        }

        if (e.SortExpression.Equals("NumeroRegistro"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NumeroRegistro).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NumeroRegistro).ToList();
            }
        }

        if (e.SortExpression.Equals("FechaIngresoICBF"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.FechaIngresoICBF).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.FechaIngresoICBF).ToList();
            }
        }

        ViewState["SortedgvwInfoBien"] = vResult;
        this.gvwInfoBien.DataSource = vResult;
        this.gvwInfoBien.DataBind();
    }

    /// <summary>
    /// Evento que se ejecuta cuando se selecciona un comando de la grilla de Tipo titulo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gwvInfoTipoTitulo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Editar")
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
                this.btnAplicarActualizacionTitulo.Visible = true;

                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        this.idTitulo.Text = gvr.Cells[4].Text;
                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() == "JUDICIAL"))
                        {
                            this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                            this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                            this.txtTipoTitulo.Text = gvr.Cells[0].Text; ;
                            this.txtDescripcionBienTitulo.Text = ((Label)gvr.FindControl("lbDescripcionTitulo")).Text;
                            if (gvr.Cells[1].Text.ToUpper().Equals("ADJUDICADO"))
                            {
                                this.rbnEstadoBnTituloAdjudicado.Checked = true;
                                this.rbnEstadoBnTituloNoAdjudicado.Checked = false;
                                this.txtFechaAdjudicacionTitulo.Enabled = true;
                            }
                            else if (gvr.Cells[1].Text.ToUpper().Equals("NO ADJUDICADO"))
                            {
                                this.rbnEstadoBnTituloAdjudicado.Checked = false;
                                this.rbnEstadoBnTituloNoAdjudicado.Checked = true;
                                this.txtFechaAdjudicacionTitulo.Enabled = false;
                                this.txtFechaAdjudicacionTitulo.InitNull = true;
                            }
                            if (!gvr.Cells[3].Text.Equals("&nbsp;"))
                            {
                                this.txtFechaAdjudicacionTitulo.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                            }
                            else
                            {
                                this.txtFechaAdjudicacionTitulo.InitNull = true;
                            }
                        }
                        else
                        {
                            this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                            this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                            string tipoTitulo = gvr.Cells[0].Text;
                            string descripcionBien = ((Label)gvr.FindControl("lbDescripcionTitulo")).Text;
                            if (gvr.Cells[1].Text.ToUpper().Equals("ADJUDICADO"))
                            {
                                this.rbnEstadoBnTituloAdjudicado.Checked = true;
                                this.rbnEstadoBnTituloNoAdjudicado.Checked = false;
                                this.txtFechaAdjudicacionTitulo.Enabled = true;
                            }
                            else if (gvr.Cells[1].Text.ToUpper().Equals("NO ADJUDICADO"))
                            {
                                this.rbnEstadoBnTituloAdjudicado.Checked = false;
                                this.rbnEstadoBnTituloNoAdjudicado.Checked = true;
                                this.txtFechaAdjudicacionTitulo.Enabled = false;
                                this.txtFechaAdjudicacionTitulo.InitNull = true;
                            }
                            if (!gvr.Cells[3].Text.Equals("&nbsp;"))
                            {
                                DateTime fechaAdjudicacion = DateTime.Parse(gvr.Cells[3].Text);
                                this.txtFechaAdjudicacionTitulo.Date = fechaAdjudicacion;
                            }
                            else
                            {
                                this.txtFechaAdjudicacionTitulo.InitNull = true;
                            }
                            this.txtTipoTitulo.Text = tipoTitulo;
                            this.txtDescripcionBienTitulo.Text = descripcionBien;
                        }
                    }
                    else if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("JUDICIAL"))
                    {
                        this.idTitulo.Text = gvr.Cells[4].Text;
                        this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                        this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                        this.rbnEstadoBnTituloAdjudicado.Enabled = true;
                        this.rbnEstadoBnTituloNoAdjudicado.Enabled = true;
                        this.txtTipoTitulo.Text = gvr.Cells[0].Text;
                        this.txtDescripcionBienTitulo.Text = ((Label)gvr.FindControl("lbDescripcionTitulo")).Text;

                        if (gvr.Cells[1].Text.ToUpper().Equals("ADJUDICADO"))
                        {
                            this.rbnEstadoBnTituloAdjudicado.Checked = true;
                            this.rbnEstadoBnTituloNoAdjudicado.Checked = false;
                            this.txtFechaAdjudicacionTitulo.Enabled = true;
                        }
                        else if (gvr.Cells[1].Text.ToUpper().Equals("NO ADJUDICADO"))
                        {
                            this.rbnEstadoBnTituloAdjudicado.Checked = false;
                            this.rbnEstadoBnTituloNoAdjudicado.Checked = true;
                            this.txtFechaAdjudicacionTitulo.Enabled = false;
                            this.txtFechaAdjudicacionTitulo.InitNull = true;
                        }

                        if (gvr.Cells[3].Text != "&nbsp;")
                        {
                            this.txtFechaAdjudicacionTitulo.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                        }
                        else
                        {
                            this.txtFechaAdjudicacionTitulo.InitNull = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Validación para el valor por defecto de la fecha adjudicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gwvInfoTipoTitulo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int LoopCounter;

        // Variable for starting index. Use this to make sure the tabindexes start at a higher
        // value than any other controls above the gridview. 
        // Header row indexes will be 110, 111, 112...
        // First data row will be 210, 211, 212... 
        // Second data row 310, 311, 312 .... and so on
        int tabIndexStart = 10;

        for (LoopCounter = 0; LoopCounter < e.Row.Cells.Count; LoopCounter++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                // Check to see if the cell contains any controls
                if (e.Row.Cells[LoopCounter].Controls.Count > 0)
                {
                    // Set the TabIndex. Increment RowIndex by 2 because it starts at -1
                    ((LinkButton)e.Row.Cells[LoopCounter].Controls[0]).TabIndex = -1;//short.Parse((e.Row.RowIndex + 2).ToString() + tabIndexStart++.ToString());
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.TabIndex = -1;
            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("01/01/0001") || e.Row.Cells[3].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    /// <summary>
    /// Ordena alfabeticamente la grilla de la documentacion recibida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
                Direction = SortDirection.Descending;
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
                Direction = SortDirection.Ascending;
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    // Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                    Direction = SortDirection.Descending;
                }
                else
                {
                    // Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                    Direction = SortDirection.Ascending;
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        // Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                        Direction = SortDirection.Descending;
                    }
                    else
                    {
                        // Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                        Direction = SortDirection.Ascending;
                    }
                }
            }
        }

        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Evento selección del check Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void radioAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
        {
            InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
            if (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper().Equals("RECHAZADO") && vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper().Equals("JUDICIAL"))
            {
                this.dtFechaAdjudicacionBien.Enabled = true;
            }
            else
            {
                if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                                           && vInfoEscrituracion.FechaSolemnizacion.Date != new DateTime())
                {
                    this.dtFechaAdjudicacionBien.Enabled = true;
                    this.CamposEscriturizacion(true);
                }
                else
                {
                    this.dtFechaAdjudicacionBien.Enabled = true;
                    this.toolBar.MostrarMensajeError("El estado del bien debe ser Adjudicado, la Decisión del Trámite Notarial debe ser Aceptado y debe tener registrado la Fecha de Solemnización");
                }
            }
        }
        else
        {
            this.dtFechaAdjudicacionBien.Enabled = true;
            //this.toolBar.MostrarMensajeError("El estado del bien debe ser Adjudicado, la Decisión del Trámite Notarial debe ser Aceptado y debe tener registrado la Fecha de Solemnización");
        }

    }

    /// <summary>
    /// Evento del Check No adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void radioNoAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        this.dtFechaAdjudicacionBien.Enabled = false;
        this.dtFechaAdjudicacionBien.InitNull = true;
        this.CamposEscriturizacion(false);
    }

    /// <summary>
    /// Evento selección Titulo Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbnEstadoBnTituloAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        this.txtFechaAdjudicacionTitulo.Enabled = true;
    }

    /// <summary>
    /// Evento selección Titulo No Adjudicado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbnEstadoBnTituloNoAdjudicado_CheckedChanged(object sender, EventArgs e)
    {
        this.txtFechaAdjudicacionTitulo.Enabled = false;
        this.txtFechaAdjudicacionTitulo.InitNull = true;
    }

    /// <summary>
    /// Evento que se dispara cuando se selecciona un tipo de estado del documento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbtEstadoDocumento_TextChanged(object sender, EventArgs e)
    {
        this.ObliFeRe.Visible = true;
        this.ObliNoAr.Visible = true;
        this.ObliObDoRe.Visible = true;
    }



    #endregion

    #region BOTONES

    /// <summary>
    /// Handles the Click event of the btnAdd control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        if (ValidarDocumentacionSolicitada() == false)
        {
            toolBar.LipiarMensajeError();
            if (AdicionarDocumentosSolicitadosCache())
            {
            }
        }
        else
        {
            //toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAplicar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);

            if (vIdDocumentoDenuncia != 0)
            {
                if (this.ValidarDocumentacionRecibida() == false)
                {
                    if (GuardarArchivos(vIdDocumentoDenuncia, 0))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                        vListaDocumentosSolicitados.ForEach(item =>
                        {
                            if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoDenuncia)
                            {
                                item.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                item.NombreEstado = (Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue) == 3) ? "Aceptado" : "Devuelto";
                                item.FechaSolicitud = new DateTime(FechaSolicitud.Date.Year, FechaSolicitud.Date.Month, FechaSolicitud.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text;
                                item.IdTipoDocumentoSoporteDenuncia = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                item.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.FechaRecibidoGrilla = DateTime.Today.ToString("dd/MM/yyyy");
                                item.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                                item.NombreArchivo = fulArchivoRecibido.FileName;
                                item.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName;
                            }
                        });
                        this.txtObservacionesSolicitado.Enabled = true;
                        this.FechaSolicitud.Enabled = true;
                        this.ddlTipoDocumento.Enabled = true;
                        this.txtObservacionesSolicitado.Text = string.Empty;
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.FechaSolicitud.InitNull = true;
                        this.FechaRecibido.InitNull = true;
                        this.rbtEstadoDocumento.SelectedIndex = -1;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.btnAplicar.Visible = false;
                        this.btnAdd.Visible = true;
                        this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                        this.CargarGrillaDocumentos();
                    }

                    this.lblNombreArchivo.Text = string.Empty;
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento para ejecutar el boton que actualiza la informacion del titulo de valor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicarActualizacionTitulo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            string vEstadoBien = string.Empty;
            DateTime? vFechaAdjudica = null;
            int pIdTitulo = Convert.ToInt32(this.idTitulo.Text);
            if (rbnEstadoBnTituloAdjudicado.Checked)
            {
                vEstadoBien = rbnEstadoBnTituloAdjudicado.Text;
                vFechaAdjudica = this.txtFechaAdjudicacionTitulo.Date;
            }
            else if (rbnEstadoBnTituloNoAdjudicado.Checked)
            {
                vEstadoBien = rbnEstadoBnTituloNoAdjudicado.Text;
                vFechaAdjudica = null;
            }

            List<ResultFiltroTitulo> vListInfoTitulo = (List<ResultFiltroTitulo>)ViewState["SortedgwvInfoTipoTitulo"];
            vListInfoTitulo.ForEach(item =>
            {
                if (item.IdTituloValor == pIdTitulo)
                {
                    item.estado = vEstadoBien;
                    item.FechaAdjudicados = vFechaAdjudica;
                    item.Modificado = true;
                }
            });
            ViewState["SortedgwvInfoTipoTitulo"] = vListInfoTitulo;

            this.CargarGrillas();
            this.LimpiarInformacionTituloValor();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento para ejecutar el boton de aplicar los cambios de mueble e inmueble
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicarMuebleInmueble_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            string vEstadoBien = string.Empty;
            DateTime? vFechaAdjudica = new DateTime();
            int vIdMueble = Convert.ToInt32(this.idBien.Text);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
            bool bandera = true;
            if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
            {

                if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                               && vInfoEscrituracion.FechaSolemnizacion.Date != new DateTime())
                {
                    string NumeroEscritura = this.txtNumeroEscritura.Text;
                    string NumeroRegistro = this.txtNumeroRegistro.Text;

                    DateTime FechaEscritura = this.txtFechaEscritura.Date;
                    DateTime FechaIngresoICBF = this.txtFechaIngresoICBF.Date;
                    DateTime FechaInstrumentosPublicos = this.txtFechaInstrumentosPublicos.Date;


                    if (FechaEscritura == new DateTime())
                    {
                        this.Label3.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label3.Visible = false;
                    }

                    if (FechaIngresoICBF == new DateTime())
                    {
                        this.Label6.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label6.Visible = false;
                    }

                    if (FechaInstrumentosPublicos == new DateTime())
                    {
                        this.Label7.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label7.Visible = false;
                    }

                    if (NumeroEscritura.Equals(string.Empty))
                    {
                        this.Label4.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label4.Visible = false;
                    }

                    if (NumeroRegistro.Equals(string.Empty))
                    {
                        this.Label5.Visible = true;
                        bandera = false;
                    }
                    else
                    {
                        this.Label5.Visible = false;
                    }
                }
            }


            if (bandera)
            {
                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.ToUpper().Equals("NOTARIAL"))
                    {
                        //InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);

                        if ((vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() != "RECHAZADO") && (vInfoEscrituracion.DecisionTramiteNotarialRechazado.ToUpper() != "JUDICIAL"))
                        {
                            if (radioAdjudicado.Checked && (vInfoEscrituracion.DecisionTramiteNotarial.ToUpper() == "ACEPTADO")
                               && vInfoEscrituracion.FechaSolemnizacion != new DateTime())
                            {
                                List<InfoMuebleProtocolizacion> vListInfoMuebles = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                                vListInfoMuebles.ForEach(item =>
                                {
                                    if (item.IdMuebleInmueble == vIdMueble)
                                    {
                                        item.FechaEscritura = this.txtFechaEscritura.Date.ToString("dd/MM/yyyy");
                                        item.NumeroEscritura = (this.txtNumeroEscritura.Text).Equals(string.Empty) ? "0" : this.txtNumeroEscritura.Text;
                                        item.NumeroRegistro = (this.txtNumeroRegistro.Text).Equals(string.Empty) ? "0" : this.txtNumeroRegistro.Text;
                                        item.FechaIngresoICBF = this.txtFechaIngresoICBF.Date;
                                        item.FechaInstrumentosPublicos = this.txtFechaInstrumentosPublicos.Date;
                                        item.FechaComunicacion = this.fechaComunicacionFinanciera.Date;
                                        item.Modificado = true;
                                    }
                                });
                                ViewState["SortedgvwInfoBien"] = vListInfoMuebles;
                            }
                        }
                        
                    }
                }

                if (radioAdjudicado.Checked)
                {
                    vEstadoBien = radioAdjudicado.Text;
                    vFechaAdjudica = this.dtFechaAdjudicacionBien.Date;
                }
                else if (radioNoAdjudicado.Checked)
                {
                    vEstadoBien = radioNoAdjudicado.Text;
                    vFechaAdjudica = null;
                }

                List<InfoMuebleProtocolizacion> vListInfoMueble = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInfoBien"];
                vListInfoMueble.ForEach(item =>
                {
                    if (item.IdMuebleInmueble == vIdMueble)
                    {
                        item.EstadoBien = vEstadoBien;
                        item.FechaAdjudicado = vFechaAdjudica;
                        item.Modificado = true;
                    }
                });

                ViewState["SortedgvwInfoBien"] = vListInfoMueble;
                this.CargarGrillas();
                this.dtFechaAdjudicacionBien.Enabled = false;
                this.LimpiarInformacionMuebleInmueble();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cerrar el modal de mueble-inmueble
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarInmuebleModal_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlPopUpHistorico.Visible = false;
    }

    /// <summary>
    /// Cerrar panel de documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarPop_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    /// <summary>
    /// Redirige a la pantalla de consulta
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Descarga de documentos recibidos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarProtocolizacion/Edit.aspx");
        //NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Edita documentos en la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        ////Control de paneles
        this.ControlPanelDocRecibida(true);
        this.ControlPanelDocSolicitada(true);
        this.btnAplicar.Visible = true;
        List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
        ImageButton vEditDocumento = (ImageButton)sender;
        string[] ArrayEditDocumento = vEditDocumento.CommandArgument.Split('|');
        int vIdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(ArrayEditDocumento[0]);
        DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
        vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentosSolicitadosDenunciaBien).FirstOrDefault().EsNuevo = false;

        var Tipo = vListaDocumento.Find(x => x.NombreTipoDocumento == ArrayEditDocumento[1]);
        if (Tipo != null)
        {
            this.hfIdDocumentoSolocitado.Value = vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            ddlTipoDocumento.SelectedValue = Tipo.IdTipoDocumentoBienDenunciado.ToString();

            if (ArrayEditDocumento[2].ToString() == "1/01/0001 12:00:00 a. m.")
            {
                this.FechaSolicitud.InitNull = true;
            }
            else
            {
                this.FechaSolicitud.Date = Convert.ToDateTime(ArrayEditDocumento[2]);
            }

            txtObservacionesSolicitado.Text = ArrayEditDocumento[3];
            if (ArrayEditDocumento[5] != "")
            {
                FechaRecibido.Date = Convert.ToDateTime(ArrayEditDocumento[5]);
            }
            else
            {
                FechaRecibido.Date = DateTime.Now;
            }

            if (ArrayEditDocumento[4] == "3" || ArrayEditDocumento[4] == "5")
            {
                if (ArrayEditDocumento[4] == "3")
                {
                    this.rbtEstadoDocumento.SelectedValue = "3";
                    this.ObliFeRe.Visible = true;
                    this.ObliNoAr.Visible = true;
                    this.ObliObDoRe.Visible = true;
                }
                if (ArrayEditDocumento[4] == "5")
                {
                    this.rbtEstadoDocumento.SelectedValue = "5";
                    this.ObliFeRe.Visible = true;
                    this.ObliNoAr.Visible = true;
                    this.ObliObDoRe.Visible = true;
                }

            }
            else
            {
                this.ObliFeRe.Visible = false;
                this.ObliNoAr.Visible = false;
                this.ObliObDoRe.Visible = false;
            }

            lblNombreArchivo.Text = ArrayEditDocumento[6];
            txtObservacionesRecibido.Text = ArrayEditDocumento[7];
            lblNombreArchivo.Visible = true;
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
        }
    }

    /// <summary>
    /// Elimina documentos de la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton vIdDeleteDocumento = (ImageButton)sender;
            DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
            vdo.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vIdDeleteDocumento.CommandArgument);
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];

            foreach (var item in vListaDocumentosSolicitados.ToList())
            {
                if (item.IdDocumentosSolicitadosDenunciaBien == vdo.IdDocumentosSolicitadosDenunciaBien)
                {
                    vListaDocumentosEliminar.Add(item);
                    vListaDocumentosSolicitados.Remove(item);
                }
            }

            this.ViewState["DocumentosEliminar"] = vListaDocumentosEliminar;
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
            gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
            gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// Abre el panel información de la denuncia
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnInfoDenuncia_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlPopUpHistorico.Visible = true;
    }

    /// <summary>
    /// Redirige a la pagina de nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Redigire a la pagina de retorno, limpiando todas las variables de sesion
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle");
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteAdd");
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdResolucion");
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Ver el documento adjuntado en la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imgDocument_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = true;
    }

    /// <summary>
    /// ver los archivos cargados en la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkVerArchivo_Click(object sender, EventArgs e)
    {
        try
        {
            this.pnlArchivo.Visible = true;
            bool EsVerArchivo = false;

            string vRuta = string.Empty;
            LinkButton imageButton = (LinkButton)sender;
            TableCell tableCell = (TableCell)imageButton.Parent;
            GridViewRow row = (GridViewRow)tableCell.Parent;
            gvwDocumentacionRecibida.SelectedIndex = row.RowIndex;
            int fila = row.RowIndex;

            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[fila].Value);
            List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();

            EsVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo;

            if (!EsVerArchivo)
            {
                EsVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo;
            }

            if (EsVerArchivo)
            {
                this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vDocumento.RutaArchivo.ToString();
                if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion

    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    protected void gvwDocumentacionRecibida_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    // Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    // Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        // Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        // Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Control de los datos recibidos por el gridview documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int LoopCounter;

        // Variable for starting index. Use this to make sure the tabindexes start at a higher
        // value than any other controls above the gridview. 
        // Header row indexes will be 110, 111, 112...
        // First data row will be 210, 211, 212... 
        // Second data row 310, 311, 312 .... and so on
        int tabIndexStart = 10;

        for (LoopCounter = 0; LoopCounter < e.Row.Cells.Count; LoopCounter++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                // Check to see if the cell contains any controls
                if (e.Row.Cells[LoopCounter].Controls.Count > 0)
                {
                    // Set the TabIndex. Increment RowIndex by 2 because it starts at -1
                    ((LinkButton)e.Row.Cells[LoopCounter].Controls[0]).TabIndex = -1;//short.Parse((e.Row.RowIndex + 2).ToString() + tabIndexStart++.ToString());
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[1].Text.Equals("1/01/0001") || e.Row.Cells[1].Text.Equals("01/01/0001"))
            {
                e.Row.Cells[1].Text = "";
            }
        }
    }

    /// <summary>
    /// Método void ActualizarHistorico
    /// </summary>
    private void ActualizarHistorico(int estado, int fase, int accion, int actuacion)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = estado;
        pHistoricoEstadosDenunciaBien.IdFase = fase;
        pHistoricoEstadosDenunciaBien.IdAccion = accion;
        pHistoricoEstadosDenunciaBien.IdActuacion = actuacion;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.Fase = 1.ToString();
        pHistoricoEstadosDenunciaBien.Accion = 2.ToString();
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.pageName, SolutionPage.Add);
        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
    }
}