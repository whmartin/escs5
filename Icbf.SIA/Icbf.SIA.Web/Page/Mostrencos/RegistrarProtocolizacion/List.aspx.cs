﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.IO;

public partial class Page_Mostrencos_RegistrarProtocolizacion_List : GeneralWeb
{
    #region Variables

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdCalidadDenuncianteDetalle;

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdResolucion;

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarProtocolizacion";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region Eventos

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Page load method
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.CargarDatosIniciales();
        }

        MaintainScrollPositionOnPostBack = true;
    }


    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Ordenar alfabeticamente en la grilla de documentacion recibida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    // Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    // Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        // Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        // Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Redirecciona a la pagina de nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento que retorna a la pagina de consultas
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle");
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteAdd");
        RemoveSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdResolucion");
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Cerrar panel ver documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarPop_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    /// <summary>
    /// Descargar documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// Ver archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkVerArchivo_Click(object sender, EventArgs e)
    {
        try
        {
            this.pnlArchivo.Visible = true;
            bool vVerArchivo = false;
            string vRuta = string.Empty;
            LinkButton imageButton = (LinkButton)sender;
            TableCell tableCell = (TableCell)imageButton.Parent;
            GridViewRow row = (GridViewRow)tableCell.Parent;
            gvwDocumentacionRecibida.SelectedIndex = row.RowIndex;
            int fila = row.RowIndex;
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[fila].Value);

            List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
            vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo;

            if (!vVerArchivo)
            {
                vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo;
            }

            if (vVerArchivo)
            {
                this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vDocumento.RutaArchivo.ToString();
                if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Validación para el valor por defecto de la fecha adjudicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionTitulo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.Equals("01/01/0001"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    /// <summary>
    /// Validación para el valor por defecto de la fecha adjudicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwInformacionMueble_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[5].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[5].Text.Equals("01/01/0001") || e.Row.Cells[5].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[5].Text = "";
            }
        }
    }

    #endregion

    #region Metodos

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Protocolización");
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                //Elige si debe ocultar el botón nuevo y editar
                EstadoDenuncia vEstadoDenuncia = vMostrencosService.ConsultarEstadoDenunciaPorId((this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien).IdEstadoDenuncia));
                if ((vEstadoDenuncia.NombreEstadoDenuncia == "ARCHIVADO") || (vEstadoDenuncia.NombreEstadoDenuncia == "ANULADA"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(false);
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.pnlArchivo.Visible = false;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {

                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                {
                    toolBar.MostrarMensajeGuardado();
                    RemoveSessionParameter("DocumentarBienDenunciado.Guardado");
                }

                // Consulto la información de la denuncia
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

                if (vRegistroDenuncia.IdEstado == 11 || vRegistroDenuncia.IdEstado == 13 || vRegistroDenuncia.IdEstado == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString() : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                // this.ActualizarHistorico(15, 20, 40, 27);
                var Protocolizacion = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 20 && x.IdActuacion == 26 && x.IdAccion == 39);
                var Escritura = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 20 && x.IdActuacion == 27 && x.IdAccion == 40);

                if (Protocolizacion == null && Escritura == null)
                {
                    this.toolBar.OcultarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(false);
                }
                else
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(true);
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                // Se llena la grilla con los documentos cuyo estado sea diferente al inicial y el eliminado (0)
                ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                this.gvwDocumentacionRecibida.DataBind();

                if (vRegistroDenuncia.TipoTramite != null)
                {
                    if (vRegistroDenuncia.TipoTramite.Equals("NOTARIAL"))
                    {
                        TramiteNotarial tramiteNotarial = new TramiteNotarial();
                        tramiteNotarial = this.vMostrencosService.ConsultarTramiteNotarial(vIdDenunciaBien);

                        InfoEscrituracion vInfoEscrituracion = this.vMostrencosService.ConsultarDatosInfoEscrituracion(vIdDenunciaBien);
                        this.pnlInfoDecisionAdjudicacion.Visible = false;
                        this.pnlInfoEscrituracion.Visible = true;

                        //Llena los campos de info de escrituración.
                        if ((vInfoEscrituracion.FechaEscritura != null) && (vInfoEscrituracion.FechaEscritura != new DateTime()))
                        {
                            this.fechaEscritura.Date = Convert.ToDateTime(vInfoEscrituracion.FechaEscritura);
                        }

                        this.txtNumeroEscritura.Text = vInfoEscrituracion.NumeroEscritura.ToString();
                        this.txtNotariaRegistra.Text = !string.IsNullOrEmpty(vInfoEscrituracion.Notaria) ? vInfoEscrituracion.Notaria : string.Empty;
                        this.txtNumeroRegistro.Text = vInfoEscrituracion.NumeroRegistro.ToString();
                        //this.fechaEscritura.Date = Convert.ToDateTime(tramiteNotarial.fe);
                        if ((vInfoEscrituracion.FechaIngresoICBF != null) && (vInfoEscrituracion.FechaIngresoICBF != new DateTime()))
                        {
                            this.fechaIngresoICBF.Date = Convert.ToDateTime(vInfoEscrituracion.FechaIngresoICBF);
                        }

                        if ((vInfoEscrituracion.FechaInstrumentosPublicos != null) && (vInfoEscrituracion.FechaInstrumentosPublicos != new DateTime()))
                        {
                            this.fechaInstrumentosPublicos.Date = Convert.ToDateTime(vInfoEscrituracion.FechaInstrumentosPublicos);
                        }
                        if ((tramiteNotarial.FechaComunicacion != null) && (tramiteNotarial.FechaComunicacion != new DateTime()))
                        {
                            this.fechaComunicacionFinanciera.Date = Convert.ToDateTime(tramiteNotarial.FechaComunicacion);
                        }

                    }
                    else if (vRegistroDenuncia.TipoTramite.Equals("JUDICIAL"))
                    {
                        TramiteJudicial tramiteJudicial = new TramiteJudicial();
                        tramiteJudicial = this.vMostrencosService.ConsultaTramiteJudicial(vIdDenunciaBien);
                        InfoAdjudicacion vInfoAdjudicacion = this.vMostrencosService.ConsultarDatosInfoAdjudicacion(vIdDenunciaBien);
                        this.pnlInfoDecisionAdjudicacion.Visible = true;
                        this.pnlInfoEscrituracion.Visible = false;

                        if ((vInfoAdjudicacion.FechaSentencia != null) && (vInfoAdjudicacion.FechaSentencia != new DateTime()))
                            this.fechaSentencia.Date = Convert.ToDateTime(vInfoAdjudicacion.FechaSentencia);

                        if ((vInfoAdjudicacion.FechaConstanciaEjecutoria != null) && (vInfoAdjudicacion.FechaConstanciaEjecutoria != new DateTime()))
                            this.fechaConstanciaEjecutoriada.Date = Convert.ToDateTime(vInfoAdjudicacion.FechaConstanciaEjecutoria);

                        if (vInfoAdjudicacion.SentidoSentencia != null)
                        {
                            if (vInfoAdjudicacion.SentidoSentencia == "Adjudicada")
                                this.radioAdjudicada.Checked = true;

                            if (vInfoAdjudicacion.SentidoSentencia == "No Adjudicada")
                                this.radioNoAdjudicada.Checked = true;

                            if (vInfoAdjudicacion.SentidoSentencia == "Adjudicada parcialmente")
                                this.radioAdjudicadaParcial.Checked = true;
                        }
                        if ((tramiteJudicial.FechaComunicacion != null) && (tramiteJudicial.FechaComunicacion != new DateTime()))
                        {
                            this.fechaComunicacionFinanciera.Date = Convert.ToDateTime(tramiteJudicial.FechaComunicacion);
                        }

                        this.txtJuzgadoSentencia.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        this.txtNombreJuzgado.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreJuzgado) ? vInfoAdjudicacion.NombreJuzgado : string.Empty;
                        this.txtMunicipioJudicial.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreMunicipio) ? vInfoAdjudicacion.NombreMunicipio : string.Empty;
                        this.txtDepartamentoDespacho.Text = !string.IsNullOrEmpty(vInfoAdjudicacion.NombreDepartamento) ? vInfoAdjudicacion.NombreDepartamento : string.Empty;
                        this.fechaEscritura.Date = Convert.ToDateTime(tramiteJudicial.FechaComunicacion);
                    }
                }

                this.CargarGrillas();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar las grillas.
    /// </summary>
    public void CargarGrillas()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien"));

            List<InfoMuebleProtocolizacion> vListInfoMueble = vMostrencosService.ConsultarInfoMuebleXIdBienDenuncia(vIdDenunciaBien);

            if (vListInfoMueble.Count > 0)
            {
                for (int i = 0; i < vListInfoMueble.Count; i++)
                {
                    vListInfoMueble[i].FechaIngresoICBF = (vListInfoMueble[i].FechaIngresoICBF).Equals("1/01/0001 12:00:00 a. m.") ? null : vListInfoMueble[i].FechaIngresoICBF;
                    List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(vListInfoMueble[i].TipoBien);
                    var subtipo = vListaSubTipoBien.Find(x => x.IdSubTipoBien == vListInfoMueble[i].SubTipoBien.ToString());
                    vListInfoMueble[i].NombreSubTipoBien = subtipo.NombreSubTipoBien;
                    List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(vListInfoMueble[i].SubTipoBien.ToString());
                    vListInfoMueble[i].NombreClaseBien = vListaClaseBien[0].NombreClaseBien;
                    vListInfoMueble[i].TipoBien = (vListInfoMueble[i].TipoBien.Equals("3")) ? vListInfoMueble[i].TipoBien = "INMUEBLE" : vListInfoMueble[i].TipoBien = "MUEBLE";
                    if (vListInfoMueble[i].FechaEscritura != null)
                    {
                        vListInfoMueble[i].FechaEscritura = (!vListInfoMueble[i].FechaEscritura.Equals(string.Empty)) ? Convert.ToDateTime(vListInfoMueble[i].FechaEscritura).ToString("dd/MM/yyyy") : string.Empty;
                    }
                    else
                    {
                        vListInfoMueble[i].FechaEscritura = string.Empty;
                    }

                    ViewState["SortedgvwInformacionMueble"] = vListInfoMueble;

                }
            }

            List<ResultFiltroTitulo> vListInfoTitulo = vMostrencosService.ConsultarTituloXIdDenuncia(vIdDenunciaBien);

            if (vListInfoMueble.Count > 0)
            {
                this.gvwInformacionMueble.DataSource = vListInfoMueble;
                this.gvwInformacionMueble.DataBind();
            }
            else
            {
                this.gvwInformacionMueble.EmptyDataText = EmptyDataText();
                this.gvwInformacionMueble.DataBind();
            }

            if (vListInfoTitulo.Count > 0)
            {
                ViewState["SortedgvwInfoTitulo"] = vListInfoTitulo;
                this.gvwInformacionTitulo.DataSource = vListInfoTitulo;
                this.gvwInformacionTitulo.DataBind();
            }
            else
            {
                this.gvwInformacionTitulo.EmptyDataText = EmptyDataText();
                this.gvwInformacionTitulo.DataBind();
            }

            // Se llena la grilla con los documentos cuyo estado sea diferente al inicial y el eliminado (0)
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    #endregion

    protected void gvwInformacionMueble_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<InfoMuebleProtocolizacion> vResult = new List<InfoMuebleProtocolizacion>();
        List<InfoMuebleProtocolizacion> vList = new List<InfoMuebleProtocolizacion>();

        if (ViewState["SortedgvwInformacionMueble"] != null)
        {
            vList = (List<InfoMuebleProtocolizacion>)ViewState["SortedgvwInformacionMueble"];
        }

        if (e.SortExpression.Equals("TipoBien"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.TipoBien).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.TipoBien).ToList();
            }
        }

        if (e.SortExpression.Equals("NombreSubTipoBien"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreSubTipoBien).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreSubTipoBien).ToList();
            }
        }

        if (e.SortExpression.Equals("NombreClaseBien"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreClaseBien).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreClaseBien).ToList();
            }
        }

        if (e.SortExpression.Equals("EstadoBien"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.EstadoBien).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.EstadoBien).ToList();
            }
        }

        if (e.SortExpression.Equals("DescripcionBien"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.DescripcionBien).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.DescripcionBien).ToList();
            }
        }

        if (e.SortExpression.Equals("FechaEscritura"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.FechaEscritura).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.FechaEscritura).ToList();
            }
        }

        if (e.SortExpression.Equals("NumeroEscritura"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NumeroEscritura).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NumeroEscritura).ToList();
            }
        }

        if (e.SortExpression.Equals("Notaria"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.Notaria).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.Notaria).ToList();
            }
        }

        if (e.SortExpression.Equals("NumeroRegistro"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NumeroRegistro).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NumeroRegistro).ToList();
            }
        }

        if (e.SortExpression.Equals("FechaIngresoICBF"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.FechaIngresoICBF).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.FechaIngresoICBF).ToList();
            }
        }

    }


    protected void gvwInformacionTitulo_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<ResultFiltroTitulo> vResult = new List<ResultFiltroTitulo>();
        List<ResultFiltroTitulo> vList = new List<ResultFiltroTitulo>();

        if (ViewState["SortedgvwInfoTitulo"] != null)
        {
            vList = (List<ResultFiltroTitulo>)ViewState["SortedgvwInfoTitulo"];
        }

        if (e.SortExpression.Equals("nombreTitulo"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.nombreTitulo).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.nombreTitulo).ToList();
            }
        }

        if (e.SortExpression.Equals("estado"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.estado).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.estado).ToList();
            }
        }

        if (e.SortExpression.Equals("DescripcionTitulo"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.DescripcionTitulo).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.DescripcionTitulo).ToList();
            }
        }

        if (e.SortExpression.Equals("FechaAdjudicado"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.FechaAdjudicado).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.FechaAdjudicado).ToList();
            }
        }
    }
}