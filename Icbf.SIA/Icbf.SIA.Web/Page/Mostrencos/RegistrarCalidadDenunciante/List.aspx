﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_RegistrarCalidadDenunciante_List" %>

<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<script runat="server">

    protected void gvwHistoricoCalidadDenunciante_Sorting(object sender, GridViewSortEventArgs e)
    {

    }
</script>



<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }
        .descripcionGrilla {
        word-break: break-all;
        }
    </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *          
                </td>
                <td>Fecha radicado en correspondencia *  
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *
                </td>
                <td>Número de identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Primer nombre *
                </td>
                <td style="width: 55%">Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45%">Primer apellido *
                </td>
                <td style="width: 55%">Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;" >
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg"  />
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlHistorico">
        <table width="90%" align="center">
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwHistoricoCalidadDenunciante" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames ="IdReconocimientoCalidadDenunciante" CellPadding="0" Height="16px" AllowSorting="True"
                        OnSelectedIndexChanged="gvwHistoricoCalidadDenunciante_SelectedIndexChanged" 
                        OnPageIndexChanging="gvwHistoricoCalidadDenunciantea_OnPageIndexChanged" OnSorting="gvwHistoricoCalidadDenunciante_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Decisión reconocimiento" DataField="ReconocimientoCalidad" SortExpression="ReconocimientoCalidad" />
                            <asp:BoundField HeaderText="Número resolución" DataField="NumeroResolucion" SortExpression="NumeroResolucion" />
                            <asp:BoundField HeaderText="Fecha resolución" DataField="FechaResolucion" SortExpression="FechaResolucion"  DataFormatString="{0:d}"/>
                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" SortExpression="Observaciones" ItemStyle-CssClass="descripcionGrilla"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" HorizontalAlign="Center" />
                        <PagerStyle HorizontalAlign="Center" />
                        <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
