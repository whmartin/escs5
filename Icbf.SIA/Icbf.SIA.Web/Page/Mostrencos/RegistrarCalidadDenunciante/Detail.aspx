﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_RegistrarCalidadDenunciante_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        /*.rbl { margin-right: 10px;}*/
        .rbl label {min-width: 80px;}
        .rbl input {min-width: 20px;}

        .auto-style1 {
            width: 45%;
        }

        .auto-style2 {
            height: 27px;
        }

        /*.ajax__calendar_day {
            text-decoration-line: underline;
        }

        .ajax__calendar_invalid .ajax__calendar_day {
            background: #d5d5d5;
            cursor: not-allowed;
            text-decoration: none !important;
        }*/

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .descripcionGrilla {
        word-break: break-all;
        }

        </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *
                </td>
                <td>Fecha radicado en correspondencia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *
                </td>
                <td>Número de identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Primer nombre *
                </td>
                <td style="width: 55%">Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45%">Primer apellido *
                </td>
                <td style="width: 55%">Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td> 
                <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:HiddenField runat="server" ID="IdCalidadDenuncianteField"/>
    </asp:Panel>

    <asp:Panel runat="server" ID="InfoResolReconoce" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Resolución
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2"> Decisión de reconocimiento de calidad de denunciante *
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:RadioButton ID="RadioReconoceA" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="Reconoce" Enabled="false" />
                    <asp:RadioButton ID="RadioNoReconoceA" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="No Reconoce" Enabled="false"/>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="auto-style1"> Número de la resolución *</td>
                <td style="width: 55%">Fecha resolución *
                </td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtNumeroResolA" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaResolA" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="auto-style2"> Observaciones</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesA" MaxLength="256" TextMode="MultiLine" Rows="8" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="InfoResolNoReconoce" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Resolución
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Decisión de reconocimiento de calidad de denunciante *
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:RadioButton ID="RadioReconoceB" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="Reconoce" Enabled="false"/>
                    <asp:RadioButton ID="RadioNoReconoceB" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="No Reconoce" Enabled="false"/>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2" style="padding-left: 104px;">
                    <asp:RadioButton ID="RadioArchiva" runat="server" CssClass="rbl" GroupName="DetalleDenuncia" Text="Archiva" Enabled="false" />
                    <asp:RadioButton ID="RadioContinuar" runat="server" CssClass="rbl" GroupName="DetalleDenuncia" Text="Continuar de oficio" Enabled="false" />
                    <asp:RadioButton ID="RadioTermina" runat="server" CssClass="rbl" GroupName="DetalleDenuncia" Text="Termina" style="margin-left: 20px;" Enabled="false" />
                    <asp:Label runat="server" ID="lblDetalleDenunciante" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1"> Número de la resolución *</td>
                <td style="width: 55%">Fecha resolución *
                </td>
            </tr>
            <tr class="rowB">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtNumeroResolB" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaResolB" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2" class="auto-style2"> Observaciones</td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesB" MaxLength="256" TextMode="MultiLine" Rows="8" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="plnNotificacion" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Notificación Citación
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td> ¿Se notifica? *</td>
                <td>Fecha de Notificación Personal
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButton CssClass="rbl" ID="RadioSenotificaSI" runat="server" GroupName="Notificacion" Text="Si" Enabled="false" />
                    <asp:RadioButton CssClass="rbl" ID="RadioSenotificaNO" runat="server" GroupName="Notificacion" Text="No" Enabled="false" Checked="true" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaNotificacion" Requerid="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"> Fecha de Aviso</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtFechaAviso" Requerid="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td> Observacion Envío Citación</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionCitacion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="3">Documentación solicitada
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td>
                    Documento soporte de la denuncia *
                </td>
                <td>
                    Fecha de solicitud *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:CompareValidator ControlToValidate="ddlTipoDocumento" ID="reqTipoDocumento" ValidationGroup="DocumentacionSolicitada" 
                        SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" runat="server" Operator="NotEqual" ValueToCompare="-1" Type="Integer" />
                    <br />
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="true"
                        DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="FechaSolicitud" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Image/btn/add.gif" />
                    <asp:ImageButton ID="btnAplicar" runat="server" ImageUrl="~/Image/btn/apply.png" Visible="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Observaciones al documento solicitado *
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionesSolicitado" ControlToValidate="txtObservacionesSolicitado" ValidationGroup="DocumentacionSolicitada" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox runat="server" ID="txtObservacionesSolicitado" Enabled="true" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionRecibida" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado del documento *
                </td>
                <td>
                    Fecha de recibido *                
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RequiredFieldValidator runat="server" ID="reqEstadoDocumento" ControlToValidate="rbtEstadoDocumento" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal" CssClass="rbl">
                        <asp:ListItem Value="3">Aceptado</asp:ListItem>
                        <asp:ListItem Value="5">Devuelto</asp:ListItem>
                    </asp:RadioButtonList>  
                </td>
                <td>
                    <asp:TextBox runat="server" ID="FechaRecibido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre de archivo * 
                </td>
                <td></td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RequiredFieldValidator runat="server" ID="reqArchivoRecibido" ControlToValidate="fulArchivoRecibido" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:FileUpload ID="fulArchivoRecibido" runat="server" Enabled="false" />
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento recibido *
                </td>
            </tr>

            <tr class="rowA">
                <td colspan="2">
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionesRecibido" ControlToValidate="txtObservacionesRecibido" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" Enabled="true"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />

                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" AllowSorting="True"
                        OnSorting="gvwDocumentacionRecibida_OnSorting"
                        OnRowCommand="gvwDocumentacionRecibida_RowCommand"
                        GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px" >
                        <Columns>
                            <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumentoBienDenunciado.NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Estado del documento" DataField="EstadoDocumento.NombreEstadoDocumento" SortExpression="NombreEstadoDocumento" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" SortExpression="FechaRecibido" />
                            <asp:TemplateField HeaderText="Nombre archivo" SortExpression="NombreArchivo">
                                <ItemTemplate>
                                    <asp:Button runat="server" CssClass="lnkArchivoDescargar" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlArchivo" CssClass="popuphIstorico hidden" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <div class="col-md-5 align-center">
                            <h4>
                                <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-md-5 align-center">
                            <asp:HiddenField ID="hfIdArchivo" runat="server" />
                            <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                        </div>
                        <div class="col-md-1 align-center">
                            <a class="btnCerrarPopArchivo" style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative;">
                                <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png"></img>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                        <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                        <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function () { 
            $(document).on('click', '.btnCerrarPopArchivo', function () {
                $('.popuphIstorico').addClass('hidden');
            });
        });
    </script>

</asp:Content>