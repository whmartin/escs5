﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarCalidadDenunciante_Add : GeneralWeb
{
    #region Variables

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Entidad del Documento Solicitado
    /// </summary>
    private DocumentosSolicitadosDenunciaBien vDocumentoSolicitado;

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdCalidadDenuncianteDetalle;

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarCalidadDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region Eventos

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlArchivo.CssClass = "popuphIstorico hidden";
        this.pnlPopUpHistorico.CssClass = "PopUpHistorico hidden";
        this.RadioReconoceA.Focus();

        if (!Page.IsPostBack)
        {
            this.CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Metodo para ir a lapagina RegistrarDocumentosBienDenunciado/Detail
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Metodo para ir a la pagina Edit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// MEtodo para guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        try
        {
            if (this.RadioNoReconoceB.Checked)
            {
                if (this.txtFechaResolB.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.feInvB.Visible = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.feInvB.Visible = false;
                    this.Guardar();
                }
            }

            else if (this.RadioReconoceA.Checked)
            {
                if (this.txtFechaResolA.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.fechaInv.Visible = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.fechaInv.Visible = false;
                    this.Guardar();
                }
            }
        }
        catch (Exception)
        {
            this.fechaInv.Visible = true;
            this.toolBar.LipiarMensajeError();
            return;
        }
    }

    /// <summary>
    /// Metodo para adjuntar el documento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();
        try
        {
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.fcInvSoli.Visible = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.fcInvSoli.Visible = false;
                toolBar.LipiarMensajeError();
                int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);
                if (vIdDocumentoDenuncia != 0)
                {
                    if (GuardarArchivos(vIdDocumentoDenuncia, 0))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                        vListaDocumento.ForEach(item =>
                        {
                            if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoDenuncia)
                            {
                                item.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                item.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                                //item.DocumentacionCompleta = this.rbtDocumentacionCompleta.SelectedValue;
                                item.NombreArchivo = fulArchivoRecibido.FileName;
                                item.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName;
                            }
                        });
                        this.txtObservacionesSolicitado.Enabled = true;
                        this.FechaSolicitud.Enabled = true;
                        this.ddlTipoDocumento.Enabled = true;
                        this.txtObservacionesSolicitado.Text = string.Empty;
                        this.FechaSolicitud.InitNull = true;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.pnlDocumentacionRecibida.Enabled = false;
                        this.btnAplicar.Visible = false;
                        this.btnAdd.Visible = true;
                        this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                        CargarGrilla();
                    }
                }
            }
        }
        catch (Exception)
        {
            this.toolBar.LipiarMensajeError();
            return;
        }
    }

    /// <summary>
    /// Metodo para cargar el documento en la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();
        try
        {
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.fcInvSoli.Visible = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.fcInvSoli.Visible = false;
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                if (vListaDocumentacion.Exists(x => x.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)))
                    throw new Exception("El Registro ya existe");

                Random rnd = new Random();
                int id = rnd.Next(1, 100);
                DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien()
                {
                    IdDocumentosSolicitadosDenunciaBien = id,
                    FechaSolicitud = this.FechaSolicitud.Date,
                    FechaSolicitudGrilla = this.FechaSolicitud.Date.ToString("dd/MM/yyyy"),
                    IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue),
                    TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)),
                    ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper(),
                    EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado"),
                    IdEstadoDocumento = 2,
                    UsuarioCrea = this.GetSessionUser().NombreUsuario,
                    FechaCrea = DateTime.Now,
                    EsNuevo = true
                };
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrilla();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }

        }
        catch (Exception)
        {
            this.toolBar.LipiarMensajeError();
            return;
        }

    }

    /// <summary>
    /// Metodo para descargar el archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        string vNombreArchivo = this.hfNombreArchivo.Value;
        bool EsDescargar = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vIdArchivo + "/" + vNombreArchivo));
        string vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vIdArchivo + "/" + vNombreArchivo;
        if (EsDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();
        }
        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Metodo para los eventos de la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Remove(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                this.CargarGrilla();
            }
            else if (e.CommandName.Equals("Editar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                this.btnAdd.Visible = false;
                this.btnAplicar.Visible = true;
                this.hfIdDocumentoSolocitado.Value = vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
                this.ddlTipoDocumento.SelectedValue = vDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
                this.FechaSolicitud.Date = Convert.ToDateTime(vDocumentoSolicitado.FechaSolicitud);
                this.txtObservacionesSolicitado.Text = vDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
                this.pnlDocumentacionRecibida.Enabled = true;
                foreach (ListItem item in this.rbtEstadoDocumento.Items)
                {
                    if (vDocumentoSolicitado.EstadoDocumento.CodigoEstadoDocumento == item.Value)
                    {
                        item.Selected = true;
                        break;
                    }
                }
                //foreach (ListItem item in this.rbtDocumentacionCompleta.Items)
                //{
                //    if (vDocumentoSolicitado.DocumentacionCompleta == item.Value)
                //    {
                //        item.Selected = true;
                //        break;
                //    }
                //}
                this.FechaRecibido.Date = vDocumentoSolicitado.FechaRecibido ?? DateTime.Now;
                this.txtObservacionesRecibido.Text = vDocumentoSolicitado.ObservacionesDocumentacionRecibida;
            }
            else if (e.CommandName.Equals("Ver"))
            {
                this.btnDescargar.Visible = false;
                bool EsVerArchivo = false;
                string vRuta = string.Empty;
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                EsVerArchivo = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vDocumento.RutaArchivo;
                this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                this.pnlArchivo.CssClass = "popuphIstorico";
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vDocumento.NombreArchivo.ToString();
                if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para el paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Metodo para el orden de los registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }
        if (e.SortExpression.Equals("NombreEstado"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                vResult = vList.OrderBy(a => a.NombreEstado).ToList();
            }
            else
            {
                vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaRecibidoGrilla"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreArchivo"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                    }
                    else
                    {
                        vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                    }
                }
                else
                {
                    if (e.SortExpression.Equals("ObservacionesDocumentacionRecibida"))
                    {
                        if (this.Direction == SortDirection.Ascending)
                        {
                            vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                        }
                        else
                        {
                            vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                        }
                    }
                }
            }
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Metodo para mostrar y ocultar controles 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RadioNoReconoceA_CheckedChanged(object sender, EventArgs e)
    {
        this.InfoResolNoReconoce.Visible = true;
        this.InfoResolReconoce.Visible = false;
        this.RadioNoReconoceB.Checked = true;
        this.RadioNoReconoceB.Focus();
        this.RadioReconoceB.Checked = false;
    }

    /// <summary>
    /// Metodo para mostrar y ocultar controles
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RadioReconoceB_CheckedChanged(object sender, EventArgs e)
    {
        this.InfoResolNoReconoce.Visible = false;
        this.InfoResolReconoce.Visible = true;
        this.RadioReconoceA.Checked = true;
        this.RadioReconoceA.Focus();
        this.RadioNoReconoceA.Checked = false;
    }

    #endregion

    #region Metodos

    /// <summary>
    /// Direction
    /// </summary>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo Inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Calidad del Denunciante");
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += btnConsultar_Click;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para guardar
    /// </summary>
    private void Guardar()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            RegistroCalidadDenunciante vregistroCalidadDenunciante = new RegistroCalidadDenunciante();
            if (vListaDocumentos.Count == 0)
                throw new Exception("Debe adicionar un documento.");

            vregistroCalidadDenunciante.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdDenunciaBien"));
            vregistroCalidadDenunciante.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
            if (InfoResolReconoce.Visible.Equals(true))
            {
                vregistroCalidadDenunciante.ReconocimientoCalidad = "Reconoce";
                vregistroCalidadDenunciante.Observaciones = Convert.ToString(this.txtObservacionesA.Text);
                vregistroCalidadDenunciante.FechaResolucion = this.txtFechaResolA.Date;
                vregistroCalidadDenunciante.NumeroResolucion = Convert.ToInt32(this.txtNumeroResolA.Text);
            }

            if (InfoResolNoReconoce.Visible.Equals(true))
            {
                vregistroCalidadDenunciante.ReconocimientoCalidad = "No Reconoce";
                if (this.rdbArchiva.SelectedValue == "1")
                    vregistroCalidadDenunciante.NombreDetalle = "Archiva";
                else if (this.rdbArchiva.SelectedValue == "2")
                    vregistroCalidadDenunciante.NombreDetalle = "Continuar de oficio";
                else
                    vregistroCalidadDenunciante.NombreDetalle = "Termina";

                vregistroCalidadDenunciante.Observaciones = Convert.ToString(this.txtObservacionesB.Text);
                vregistroCalidadDenunciante.FechaResolucion = this.txtFechaResolB.Date;
                vregistroCalidadDenunciante.NumeroResolucion = Convert.ToInt32(this.txtNumeroResolB.Text);
            }

            this.InformacionAudioria(vregistroCalidadDenunciante, this.PageName, SolutionPage.Add);
            int IdReconocimientoCalidadDenunciante = this.vMostrencosService.InsertarCalidadDenunciante(vregistroCalidadDenunciante);
            if (IdReconocimientoCalidadDenunciante == -1)
            {
                throw new Exception("El registro ya existe");
            }
            else
            {
                if (RadioSenotificaSI.Checked)
                {
                    this.InsertarHistoricoContratoParticipacionEconomica(vregistroCalidadDenunciante.IdDenunciaBien, 15, 21, 3, 31);
                }

                if (RadioNoReconoceB.Checked)
                {
                    if (this.rdbArchiva.SelectedValue == "2")
                    {
                        this.InsertarHistoricoContratoParticipacionEconomica(vregistroCalidadDenunciante.IdDenunciaBien, 17, 7, 3, 24);
                    }
                    else
                    {
                        this.InsertarHistoricoContratoParticipacionEconomica(vregistroCalidadDenunciante.IdDenunciaBien, 18, 7, 3, 24);
                    }
                }
                else
                {
                    this.InsertarHistoricoContratoParticipacionEconomica(vregistroCalidadDenunciante.IdDenunciaBien, 16, 7, 3, 24);
                }
            }


            vListaDocumentos.ForEach(item =>
            {
                item.IdDenunciaBien = vregistroCalidadDenunciante.IdDenunciaBien;
                item.IdCalidadDenunciante = IdReconocimientoCalidadDenunciante;

                this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                if (item.EsNuevo == true)
                    this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                else
                    vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
            });
            this.SetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle", IdReconocimientoCalidadDenunciante);
            String ValorExitoso = "La información ha sido guardada exitosamente";
            Response.Redirect("../../Mostrencos/RegistrarCalidadDenunciante/Detail.aspx?valor=" + ValorExitoso, false);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para guardar archivos
    /// </summary>
    /// <param name="pIdDenunciaBien"></param>
    /// <param name="pPosition"></param>
    /// <returns></returns>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        bool Esresultado = false;
        try
        {
            HttpFileCollection files = Request.Files;
            if (files.Count > 0)
            {
                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                string extJgp = ".JPG".ToLower();
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos.");
                }
                int tamano = this.fulArchivoRecibido.FileName.Length;
                if (extFile != extPdf && extFile != extJgp)
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Mb");
                }

                HttpPostedFile file = files[pPosition];
                if (file.ContentLength > 0)
                {
                    string rutaParcial = string.Empty;
                    string carpetaBase = string.Empty;
                    string filePath = string.Empty;
                    if (extFile.Equals(".pdf"))
                    {
                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarCalidadDenunciante/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                    }
                    else if (extFile.ToLower().Equals(".jpg"))
                    {
                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarCalidadDenunciante/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                    }
                    bool EsexisteCarpeta = System.IO.Directory.Exists(carpetaBase);
                    if (!EsexisteCarpeta)
                    {
                        System.IO.Directory.CreateDirectory(carpetaBase);
                        return true;
                    }
                    this.vFileName = rutaParcial;
                    file.SaveAs(filePath);
                    Esresultado = true;
                }
            }
        }
        catch (HttpRequestValidationException httpex)
        {
            throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return Esresultado;
    }

    /// <summary>
    /// Cargar datos inciales de los formularios
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdDenunciaBien"));

            if (vIdDenunciaBien != 0)
            {
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString() : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vRegistroDenuncia.PRIMERNOMBRE) ? vRegistroDenuncia.PRIMERNOMBRE : string.Empty;
                this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vRegistroDenuncia.SEGUNDONOMBRE) ? vRegistroDenuncia.SEGUNDONOMBRE : string.Empty;
                this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vRegistroDenuncia.PRIMERAPELLIDO) ? vRegistroDenuncia.PRIMERAPELLIDO : string.Empty;
                this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vRegistroDenuncia.SEGUNDOAPELLIDO) ? vRegistroDenuncia.SEGUNDOAPELLIDO : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                string TipoPersona = vDenunciaBien.Tercero.NombreTipoPersona;
                Tercero ObjEntidad = this.vMostrencosService.ConsultarTipoEntidadPorIdTercero(vDenunciaBien.Tercero.IdTercero);
                if (vRegistroDenuncia.Existe || TipoPersona.Equals("Juridica"))
                {
                    this.InfoResolNoReconoce.Visible = true;
                    this.InfoResolReconoce.Visible = false;
                    this.RadioNoReconoceB.Checked = true;
                    this.RadioNoReconoceB.Focus();
                    this.RadioReconoceB.Checked = false;
                    this.RadioReconoceB.Enabled = false;
                    this.RadioNoReconoceB.Enabled = false;
                }
                else
                {
                    this.InfoResolReconoce.Visible = true;
                    this.InfoResolNoReconoce.Visible = false;
                    this.RadioNoReconoceA.Checked = false;
                    this.RadioReconoceA.Checked = true;
                    this.RadioReconoceA.Focus();
                }
                this.CargarListaDesplegable();
                this.CargarGrilla();
                this.CargarGrillaHistorico(vIdDenunciaBien);
            }
            if (RadioSenotificaSI.Checked)
            {
                this.ObliFeNoPe.Visible = true;
                this.ObliFeAv.Visible = true;
                this.ObliEnCi.Visible = true;
            }
            else
            {
                this.ObliFeNoPe.Visible = false;
                this.ObliFeAv.Visible = false;
                this.ObliEnCi.Visible = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }
                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
            this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// Carga la información de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlTipoDocumento.DataSource = vListaDocumento;
            this.ddlTipoDocumento.DataTextField = "NombreTipoDocumento";
            this.ddlTipoDocumento.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlTipoDocumento.DataBind();
            this.ddlTipoDocumento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoDocumento.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Insertar historico de la denuncia
    /// </summary>
    private void InsertarHistoricoContratoParticipacionEconomica(int vIdDenunciaBien, int Estado, int Actuacion, int fase, int accion)
    {
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = Estado;
        pHistoricoEstadosDenunciaBien.IdFase = fase;
        pHistoricoEstadosDenunciaBien.IdAccion = accion;
        pHistoricoEstadosDenunciaBien.IdActuacion = Actuacion;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.Fase = fase.ToString();
        pHistoricoEstadosDenunciaBien.Accion = accion.ToString();
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;

        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, SolutionPage.Add);
        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
    }

    #endregion
}