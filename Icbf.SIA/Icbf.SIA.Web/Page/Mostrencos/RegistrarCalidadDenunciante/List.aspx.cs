﻿//-----------------------------------------------------------------------
// <copyright file="Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Edit.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.Linq;

public partial class Page_Mostrencos_RegistrarCalidadDenunciante_List : GeneralWeb
{
    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarCalidadDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.MostrarBotonConsultar();
        toolBar.MostrarBotonNuevo(true);
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            CargarDatos();
        }
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwHistoricoCalidadDenunciante_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvwHistoricoCalidadDenunciante.SelectedRow);
    }

    /// <summary>
    /// guarda datos com: idDenuncia,Numero de radicado denuncia y fecha de denuncia para ser lelvados al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvwHistoricoCalidadDenunciante.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle", strValue);
            NavigateTo(SolutionPage.Detail, false);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.SetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle", 0);
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.IdEstado == 11 || vRegistroDenuncia.IdEstado == 13 || vRegistroDenuncia.IdEstado == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString() : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString())
                    ? (vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                        ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy")
                        : string.Empty)
                    : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vRegistroDenuncia.PRIMERNOMBRE) ? vRegistroDenuncia.PRIMERNOMBRE : string.Empty;
                this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vRegistroDenuncia.SEGUNDONOMBRE) ? vRegistroDenuncia.SEGUNDONOMBRE : string.Empty;
                this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vRegistroDenuncia.PRIMERAPELLIDO) ? vRegistroDenuncia.PRIMERAPELLIDO : string.Empty;
                this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vRegistroDenuncia.SEGUNDOAPELLIDO) ? vRegistroDenuncia.SEGUNDOAPELLIDO : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;
                List<RegistroCalidadDenunciante> vRegistroCalidadDenunciante = vMostrencosService.ConsultarCalidadDenunciante(vIdDenunciaBien);
                this.ViewState["SortedgvgvwHistoricoCalidadDenunciante"] = vRegistroCalidadDenunciante;
                this.gvwHistoricoCalidadDenunciante.Visible = true;
                this.gvwHistoricoCalidadDenunciante.DataSource = vRegistroCalidadDenunciante;
                this.gvwHistoricoCalidadDenunciante.DataBind();
                if (vRegistroCalidadDenunciante.Count > 0)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo cargar Datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdDenunciaBien"));
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Calidad del Denunciante");
            toolBar.eventoBuscar += btnConsultar_Click;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.gvwHistoricoCalidadDenunciante.PageSize = PageSize();
            this.gvwHistoricoCalidadDenunciante.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Metodo para ir a la pagina Add
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo para ir a la pagina RegistrarDocumentosBienDenunciado/Detail
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Metodo para el paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistoricoCalidadDenunciantea_OnPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        this.gvwHistoricoCalidadDenunciante.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvgvwHistoricoCalidadDenunciante"] != null)
        {
            List<RegistroCalidadDenunciante> vList = (List<RegistroCalidadDenunciante>)this.ViewState["SortedgvgvwHistoricoCalidadDenunciante"];
            gvwHistoricoCalidadDenunciante.DataSource = vList;
            gvwHistoricoCalidadDenunciante.DataBind();
        }
        else
        {
            ViewState["SortedgvgvwHistoricoCalidadDenunciante"] = vMostrencosService.ConsultarCalidadDenunciante(vIdDenunciaBien);
            this.gvwHistoricoCalidadDenunciante.DataSource = vMostrencosService.ConsultarCalidadDenunciante(vIdDenunciaBien);
            this.gvwHistoricoCalidadDenunciante.DataBind();
        }
    }


    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoCalidadDenunciante_Sorting(object sender, GridViewSortEventArgs e)
    {
        /*
         List<RegistroCalidadDenunciante> vRegistroCalidadDenunciante = vMostrencosService.ConsultarCalidadDenunciante(vIdDenunciaBien);
                this.ViewState["SortedgvgvwHistoricoCalidadDenunciante"] = vRegistroCalidadDenunciante;
                this.gvwHistoricoCalidadDenunciante.Visible = true;
         */
        List<RegistroCalidadDenunciante> vRegistroCalidadDenunciante = new List<RegistroCalidadDenunciante>();
        List<RegistroCalidadDenunciante> vRegistroCalidadDenuncianteList = new List<RegistroCalidadDenunciante>();

        if (ViewState["SortedgvgvwHistoricoCalidadDenunciante"] != null)
        {
            vRegistroCalidadDenunciante = (List<RegistroCalidadDenunciante>)ViewState["SortedgvgvwHistoricoCalidadDenunciante"];
        }

        switch (e.SortExpression)
        {
            case "ReconocimientoCalidad":
                if (this.Direction == SortDirection.Ascending)
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderBy(a => a.ReconocimientoCalidad).ToList();
                }
                else
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderByDescending(a => a.ReconocimientoCalidad).ToList();
                }
                break;

            case "NumeroResolucion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderBy(a => a.NumeroResolucion).ToList();
                }
                else
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderByDescending(a => a.NumeroResolucion).ToList();
                }
                break;

            case "FechaResolucion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderBy(a => a.FechaResolucion).ToList();
                }
                else
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderByDescending(a => a.FechaResolucion).ToList();
                }
                break;

            case "Observaciones":
                if (this.Direction == SortDirection.Ascending)
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderBy(a => a.Observaciones).ToList();
                }
                else
                {
                    vRegistroCalidadDenuncianteList = vRegistroCalidadDenunciante.OrderByDescending(a => a.Observaciones).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        ViewState["SortedgvgvwHistoricoCalidadDenunciante"] = vRegistroCalidadDenuncianteList;
        this.gvwHistoricoCalidadDenunciante.DataSource = vRegistroCalidadDenuncianteList;
        this.gvwHistoricoCalidadDenunciante.DataBind();
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

}