﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;


public partial class Page_Mostrencos_RegistrarCalidadDenunciante_Detail : GeneralWeb
{
    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Calidad de la denuncia
    /// </summary>
    private int vIdCalidadDenuncianteDetalle;

    /// <summary>
    /// Entidad de la solicitud del bien denunciado
    /// </summary>
    private DocumentosSolicitadosDenunciaBien vDocumentoSolicitado;

    /// <summary>
    /// Nombre del archivo
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarCalidadDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region Eventos

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            CargarDatos();

            string MsgOk = Request.QueryString["valor"];

            if (MsgOk != null)
            {
                this.toolBar.MostrarMensajeGuardado(MsgOk);
            }
        }
    }

    /// <summary>
    /// Metodo para el paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Metodo para el orden de los registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Metodo para los eventos de la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Ver"))
            {
                this.btnDescargar.Visible = false;
                bool ESVerArchivo = false;
                string vRuta = string.Empty;
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                ESVerArchivo = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vDocumento.RutaArchivo;
                this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                this.pnlArchivo.CssClass = "popuphIstorico";
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodopara ir a la pagina List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Metodopara ir a la pagina Add
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodopara ir a la pagina RegistrarDocumentosBienDenunciado/Detail
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Metodo para ir a la pagina Edit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// Metodo para descargar el archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        bool EsDescargar = false;
        string vRuta = string.Empty;
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
        if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
        {
            EsDescargar = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
            vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
        }
        else
        {
            EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }
        }
        if (EsDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();
        }
        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    #endregion

    #region Metodos

    /// <summary>
    /// Metodo Inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Calidad del Denunciante");
            toolBar.eventoBuscar += btnConsultar_Click;
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            vIdCalidadDenuncianteDetalle = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle"));
            RegistroCalidadDenuncianteCitacion vRegistroCalidadDenuncianteCitacion = vMostrencosService.ConsultarCalidadDenuncianteCitacion(vIdCalidadDenuncianteDetalle);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdDenunciaBien"));
            this.pnlDocumentacionSolicitada.Enabled = false;
            if (vIdDenunciaBien != 0)
            {
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.IdEstado == 11 || vRegistroDenuncia.IdEstado == 13 || vRegistroDenuncia.IdEstado == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString() : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vRegistroDenuncia.PRIMERNOMBRE) ? vRegistroDenuncia.PRIMERNOMBRE : string.Empty;
                this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vRegistroDenuncia.SEGUNDONOMBRE) ? vRegistroDenuncia.SEGUNDONOMBRE : string.Empty;
                this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vRegistroDenuncia.PRIMERAPELLIDO) ? vRegistroDenuncia.PRIMERAPELLIDO : string.Empty;
                this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vRegistroDenuncia.SEGUNDOAPELLIDO) ? vRegistroDenuncia.SEGUNDOAPELLIDO : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;
                vIdCalidadDenuncianteDetalle = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteDetalle"));
                if (vIdCalidadDenuncianteDetalle == 0)
                {
                    this.toolBar.MostrarMensajeGuardado();
                    vIdCalidadDenuncianteDetalle = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdCalidadDenuncianteAdd"));
                }
                RegistroCalidadDenuncianteCitacion vRegistroCalidadDenuncianteCitacion = vMostrencosService.ConsultarCalidadDenuncianteCitacion(vIdCalidadDenuncianteDetalle);
                this.IdCalidadDenuncianteField.Value = vRegistroCalidadDenuncianteCitacion.IdResolucion.ToString();
                if (vRegistroCalidadDenuncianteCitacion.ReconocimientoCalidadDenunciante == "Reconoce")
                {

                    InfoResolReconoce.Visible = true;
                    InfoResolNoReconoce.Visible = false;
                    RadioReconoceA.Checked = true;
                    txtNumeroResolA.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.NumeroResolucion.ToString()) ? vRegistroCalidadDenuncianteCitacion.NumeroResolucion.ToString() : string.Empty;
                    this.txtFechaResolA.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.FechaResolucion.ToString()) ? (
                   vRegistroCalidadDenuncianteCitacion.FechaResolucion != Convert.ToDateTime("01/01/0001")
                   ? vRegistroCalidadDenuncianteCitacion.FechaResolucion.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                    this.txtObservacionesA.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.Observaciones) ? vRegistroCalidadDenuncianteCitacion.Observaciones : string.Empty;
                    this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }
                else
                {
                    InfoResolReconoce.Visible = false;
                    InfoResolNoReconoce.Visible = true;
                    RadioNoReconoceB.Checked = true;

                    if (vRegistroCalidadDenuncianteCitacion.NombreDetalle == "Archiva")
                        this.RadioArchiva.Checked = true;
                    else if (vRegistroCalidadDenuncianteCitacion.NombreDetalle == "Continuar de oficio")
                        this.RadioContinuar.Checked = true;
                    else
                    {
                        this.toolBar.OcultarBotonEditar(false);
                        this.RadioTermina.Checked = true;
                    }
                    txtNumeroResolB.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.NumeroResolucion.ToString()) ? vRegistroCalidadDenuncianteCitacion.NumeroResolucion.ToString() : string.Empty;
                    this.txtFechaResolB.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.FechaResolucion.ToString()) ? (
                   vRegistroCalidadDenuncianteCitacion.FechaResolucion != Convert.ToDateTime("01/01/0001")
                   ? vRegistroCalidadDenuncianteCitacion.FechaResolucion.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                    this.txtObservacionesB.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.Observaciones) ? vRegistroCalidadDenuncianteCitacion.Observaciones : string.Empty;
                }
                if (vRegistroCalidadDenuncianteCitacion.SeNotifica == "True")
                {
                    this.RadioSenotificaSI.Checked = true;
                    this.RadioSenotificaNO.Checked = false;
                }
                else if (vRegistroCalidadDenuncianteCitacion.SeNotifica == "False")
                {
                    this.RadioSenotificaNO.Checked = true;
                    this.RadioSenotificaSI.Checked = false;
                }
                this.txtFechaNotificacion.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.FechaNotificacion.ToString()) ? (
                   vRegistroCalidadDenuncianteCitacion.FechaNotificacion != Convert.ToDateTime("01/01/0001")
                   ? vRegistroCalidadDenuncianteCitacion.FechaNotificacion.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtFechaAviso.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.FechaAviso.ToString()) ? (
                   vRegistroCalidadDenuncianteCitacion.FechaAviso != Convert.ToDateTime("01/01/0001")
                   ? vRegistroCalidadDenuncianteCitacion.FechaAviso.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtObservacionCitacion.Text = !string.IsNullOrEmpty(vRegistroCalidadDenuncianteCitacion.ObservacionCitacion) ? vRegistroCalidadDenuncianteCitacion.ObservacionCitacion : string.Empty;
                this.CargarListaDesplegable();
                this.CargarGrilla();

                //fse 4 - actuacion 9 - accion 13
                var Contrato = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 4 && x.IdActuacion == 9 && x.IdAccion == 13);
                if (Contrato != null)
                {
                    toolBar.OcultarBotonEditar(false);
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Direction
    /// </summary>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo para cargar datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int IdCalidadDenunciante = int.Parse(IdCalidadDenuncianteField.Value);
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(IdCalidadDenunciante);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();

                vListaDocumentos = vListaDocumentosSolicitados.Where(x => x.IdEstadoDocumento != 6).ToList();
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();

                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentos;
                this.gvwDocumentacionRecibida.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// Carga la información de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlTipoDocumento.DataSource = vListaDocumento;
            this.ddlTipoDocumento.DataTextField = "NombreTipoDocumento";
            this.ddlTipoDocumento.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlTipoDocumento.DataBind();
            this.ddlTipoDocumento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoDocumento.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}