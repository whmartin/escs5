﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Edit.aspx.cs" Inherits="Page_Mostrencos_RegistrarCalidadDenunciante_Edit" %>

<%--<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>--%>
<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;

        }

        .descripcionGrilla {
        word-break: break-all;
        }

        .rbl label {min-width: 80px;}
        .rbl input {min-width: 20px;}

        .auto-style1 { width: 45%; }

        .auto-style2 { height: 27px; }

        .auto-style3 { height: 22px; }

        .auto-style4 {
            width: 50%;
            height: 95px;
        }

        .auto-style5 { height: 95px; }
    </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *
                </td>
                <td>Fecha radicado en correspondencia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *
                </td>
                <td>Número de identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Primer nombre *
                </td>
                <td style="width: 55%">Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45%">Primer apellido *
                </td>
                <td style="width: 55%">Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="cursor:pointer;width: 16px;height: 16px;text-decoration: none;margin-right: 10px;top: -6px;position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfIdDocumentoSolocitado" runat="server" />
    </asp:Panel>

    <asp:Panel runat="server" ID="InfoResolReconoce">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">
                    Información de la Resolución
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Decisión de reconocimiento de calidad de denunciante *
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:RadioButton ID="RadioReconoceA" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="Reconoce" Enabled="false" />
                    <asp:RadioButton ID="RadioNoReconoceA" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="No Reconoce" Enabled="false" OnCheckedChanged="RadioNoReconoceA_CheckedChanged" AutoPostBack="true"/>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="auto-style1">
                    Número de la resolución *
                    <asp:RequiredFieldValidator runat="server" ID="reqNumeroResolA" ControlToValidate="txtNumeroResolA" ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                </td>
                <td style="width: 55%">
                    Fecha resolución *
                      <asp:Label ID="fechaInv" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="reqtxtFechaResolA2"  ForeColor="Red"
                        ControlToValidate="txtFechaResolA$txtFecha"
                        ClientValidationFunction="validateTodaymenos1" ValidationGroup="btnGuardar"
                        ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar.">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtNumeroResolA" Enabled="true" MaxLength="8" Requerid="true" ControlToValidate="lblNumeroResolA"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="valNumeroResolA" runat="server" TargetControlID="txtNumeroResolA" FilterType="Numbers" />
                </td>
                <td>
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator5"  ForeColor="Red"
                        ControlToValidate="txtFechaResolA$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                      <br />
                    <uc1:fecha runat="server" ID="txtFechaResolA" Requerid="false"/>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="auto-style2"> Observaciones</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesA" MaxLength="512" TextMode="MultiLine" Rows="8" Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtObservacionesA"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <asp:HiddenField runat="server" ID="IdCalidadDenuncianteField"/>
            <asp:HiddenField runat="server" ID="hdIdReconocimientoCalidadDenunciante"/>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="InfoResolNoReconoce">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">
                    Información de la Resolución
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Decisión de reconocimiento de calidad de denunciante *
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:RadioButton ID="RadioReconoceB" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="Reconoce" Enabled="true" OnCheckedChanged="RadioReconoceB_CheckedChanged" AutoPostBack="true"/>
                    <asp:RadioButton ID="RadioNoReconoceB" runat="server" CssClass="rbl" GroupName="ClaseDenuncia" Text="No Reconoce" Enabled="true"/>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2" style="padding-left: 104px;">
                    <asp:RequiredFieldValidator runat="server" ID="reqArchiva" ControlToValidate="rdbArchiva" ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:RadioButtonList ID="rdbArchiva" runat="server" CssClass="rbl" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">Archiva</asp:ListItem>
                        <asp:ListItem Value="2">Continuar de oficio</asp:ListItem>
                        <asp:ListItem Value="3">Termina</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="auto-style1">
                    Número de la resolución *
                    <asp:RequiredFieldValidator runat="server" ID="reqNumeroResolB" ControlToValidate="txtNumeroResolB" ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                </td>
                <td style="width: 55%">
                    Fecha resolución *
                    <asp:RequiredFieldValidator ID="reqtxtFechaResolB" runat="server" ControlToValidate="txtFechaResolB$txtFecha" ForeColor="Red" 
                        ErrorMessage="Campo requerido" ValidationGroup="btnGuardar" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="reqtxtFechaResolB2"  ForeColor="Red"
                        ControlToValidate="txtFechaResolB$txtFecha"
                        ClientValidationFunction="validateTodaymenos1" ValidationGroup="btnGuardar"
                        ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar.">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtNumeroResolB" TabIndex="1"  Enabled="true" MaxLength="8" ControlToValidate="lblNumeroResolB"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="valNumeroResolB" runat="server" TargetControlID="txtNumeroResolB" FilterType="Numbers" />
                </td>
                <td>
                    <uc1:fecha runat="server" ID="txtFechaResolB" TabIndex="3" Requerid="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="auto-style2"> Observaciones</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesB" TabIndex="5"  Enabled="true" MaxLength="512" TextMode="MultiLine" Rows="8" Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtObservacionesB"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="plnNotificacion">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Notificación Citación
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td> ¿Se notifica? *</td>
                <td>
                    Fecha de Notificación Personal  <asp:Label runat="server" ID="ObliFeNoPe" Visible="false">*</asp:Label>
                    <asp:Label ID="FcValiPerso" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="valFechaNotificacion"  ForeColor="Red"
                        ControlToValidate="txtFechaNotificacion$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                        ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar.">
                    </asp:CustomValidator>
                     <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator2"  ForeColor="Red"
                        ControlToValidate="txtFechaNotificacion$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButton CssClass="rbl" ID="RadioSenotificaSI" runat="server" GroupName="Notificacion" Text="Si" Enabled="true" OnCheckedChanged="RadioSenotificaSI_CheckedChanged" AutoPostBack="true"  />
                    <asp:RadioButton CssClass="rbl" ID="RadioSenotificaNO" runat="server" GroupName="Notificacion" Text="No" Enabled="true" OnCheckedChanged="RadioSenotificaNO_CheckedChanged" AutoPostBack="true" />
                </td>
                <td>
                    <uc1:fecha runat="server" ID="txtFechaNotificacion" Requerid="false" Enabled="false" EnableViewState="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Fecha de Aviso <asp:Label runat="server" ID="ObliFeAv" Visible="false">*</asp:Label>
                     <asp:Label ID="FcValidAviso" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="valFechaAviso"  ForeColor="Red"
                        ControlToValidate="txtFechaAviso$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                        ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar.">
                    </asp:CustomValidator>
                     <br />
                   <asp:CustomValidator runat="server"
                        ID="CustomValidator1"  ForeColor="Red"
                        ControlToValidate="txtFechaAviso$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <uc1:fecha runat="server" ID="txtFechaAviso" Requerid="false" Enabled="false" EnableViewState="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td> 
                    Observacion Envío Citación <asp:Label runat="server" ID="ObliEnCi" Visible="false">*</asp:Label>
                     <asp:Label ID="rqObserCita" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionCitacion" ControlToValidate="txtObservacionCitacion" ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" Visible="false" ></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox runat="server" ID="txtObservacionCitacion" Enabled="false" TextMode="MultiLine" MaxLength="256" Rows="8" Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="3">
                    Documentación solicitada
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td>
                    Documento soporte de la denuncia *
                    <asp:CompareValidator ControlToValidate="ddlTipoDocumento" ID="reqTipoDocumento" ValidationGroup="DocumentacionSolicitada" 
                        SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" runat="server" Operator="NotEqual" ValueToCompare="-1" Type="Integer" />
                </td>
                <td>
                  <%--  <asp:Label ID="ValFeSol" runat="server" Text="Fecha Inválida" ForeColor="Red" Visible="false"></asp:Label>
                    <br />--%>
                    <asp:CustomValidator runat="server"
                                ID="reqFechaSolicitudMayor"  ForeColor="Red"
                                ControlToValidate="FechaSolicitud$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="DocumentacionSolicitada"
                                ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar">
                    </asp:CustomValidator>
                    <br />
                    Fecha de solicitud *
                    <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" ControlToValidate="FechaSolicitud$txtFecha" ForeColor="Red" 
                        ErrorMessage="Campo requerido" ValidationGroup="DocumentacionSolicitada" ></asp:RequiredFieldValidator>
                    <br />
                         <asp:CustomValidator runat="server"
                        ID="CustomValidator3"  ForeColor="Red"
                        ControlToValidate="FechaSolicitud$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlTipoDocumento" TabIndex="7" runat="server" Enabled="true"
                        DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                    </asp:DropDownList>
                </td>
                <td>
                    <uc1:fecha ID="FechaSolicitud" Requerid="false" TabIndex="9" runat="server" Enabled="true" EnableViewState="true"/>
                </td>
                <td>
                    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Image/btn/add.gif" OnClick="btnAdd_Click" CausesValidation="true" ValidationGroup="DocumentacionSolicitada" />
                    <asp:ImageButton ID="btnAplicar" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicar_Click" Visible="false" ValidationGroup="DocumentacionRequerida" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Observaciones al documento solicitado *
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionesSolicitado" ControlToValidate="txtObservacionesSolicitado" ValidationGroup="DocumentacionSolicitada" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtObservacionesSolicitado" TabIndex="11" Enabled="true" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtObservacionesSolicitado"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionRecibida" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">
                    Documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado del documento *
                    <asp:RequiredFieldValidator runat="server" ID="reqEstadoDocumento" ControlToValidate="rbtEstadoDocumento" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                </td>
                <td>
                    Fecha de recibido <asp:Label runat="server" ID="ObliFeRe" Visible="false">*</asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="reqFechaRecibida" ControlToValidate="FechaRecibido$txtFecha" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                     <asp:Label ID="ValFeRec" runat="server" Text="Fecha Inválida" ForeColor="Red" Visible="false"></asp:Label>
                    <asp:CustomValidator runat="server"
                        ID="reqFechaRecibida2"  ForeColor="Red"
                        ControlToValidate="FechaRecibido$txtFecha"
                        ClientValidationFunction="validateDateResolucion" ValidationGroup="DocumentacionRequerida"
                        ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar.">
                    </asp:CustomValidator>
                     <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator4"  ForeColor="Red"
                        ControlToValidate="FechaRecibido$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAplicar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" AutoPostBack="true" RepeatDirection="Horizontal" CssClass="rbl" OnSelectedIndexChanged="rbtEstadoDocumento_SelectedIndexChanged">
                        <asp:ListItem Value="3">Aceptado</asp:ListItem>
                        <asp:ListItem Value="5">Devuelto</asp:ListItem>
                    </asp:RadioButtonList>  
                </td>
                <td>                    
                    <uc1:fecha runat="server" ID="FechaRecibido" Requerid="false" Enabled="false"/>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre de archivo <asp:Label runat="server" ID="ObliNoAr" Visible="false">*</asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="reqArchivoRecibido" ControlToValidate="fulArchivoRecibido" 
                        ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" 
                        ForeColor="Red" ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                </td>
                <td> </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:FileUpload ID="fulArchivoRecibido" Enabled="false" runat="server" TabIndex="22" />
                </td>
                <td></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Observaciones al documento recibido <asp:Label runat="server" ID="ObliObDoRe" Visible="false">*</asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionesRecibido" ControlToValidate="txtObservacionesRecibido" 
                        ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" 
                        ErrorMessage="Campo Requerido" ></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" Enabled="true" onkeypress="return CheckLength();"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="gvwDocumentacionRecibida_PageIndexChanging" AllowSorting="True"
                        OnSorting="gvwDocumentacionRecibida_Sorting"
                        GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px"
                        OnRowCommand="gvwDocumentacionRecibida_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumentoBienDenunciado.NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Estado del documento" DataField="EstadoDocumento.NombreEstadoDocumento" SortExpression="NombreEstadoDocumento" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" SortExpression="FechaRecibido" />
                            <asp:TemplateField HeaderText="Nombre archivo" SortExpression="NombreArchivo">
                                <ItemTemplate>
                                    <asp:Button runat="server" CssClass="lnkArchivoDescargar" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" Enabled="true" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Eliminar" CommandArgument="<%# Container.DataItemIndex%>" OnClientClick="return confirm('¿Está seguro de eliminar la información?');" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument="<%# Container.DataItemIndex%>" ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Editar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DocumentacionCompleta" ItemStyle-CssClass="Ocultar_" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlArchivo" CssClass="popuphIstorico hidden" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; 
        border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <div class="col-md-5 align-center">
                            <h4>
                                <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-md-5 align-center">
                            <asp:HiddenField ID="hfIdArchivo" runat="server" />
                            <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                            <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                        </div>
                        <div class="col-md-1 align-center">
                            <a class="btnCerrarArchivo" style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative;">
                                <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png"></img>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                        <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                        <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="PopUpHistorico hidden" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 15%; width: 70%; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width:100%;">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                    </div>
                    <div>
                        <a class="btnCerrarPop" style="width: 16px;height: 16px;text-decoration: none;float: right;margin-right: 10px;top: -10px;position: relative;">
                            <img style="width: 20px;height: 20px;" alt="h" src="../../../Image/btn/close.png">
                        </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;</div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; height:200px; align-content: center">
                        <asp:GridView ID="gvwHistoricoDenuncia" runat="server" Visible="true" AutoGenerateColumns="False" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstadoDenuncia" />
                                <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Responsable" DataField="Responsable" />
                                <asp:BoundField HeaderText="Fase" DataField="Fase" />
                                <asp:BoundField HeaderText="Actuacion" DataField="Actuacion" />
                                <asp:BoundField HeaderText="Acción" DataField="Accion" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.PopUpHistorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.PopUpHistorico').addClass('hidden');
            });
            $(document).on('click', '.btnCerrarArchivo', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('#cphCont_txtObservacionesA').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionesA").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionesA").value.substr(0, 512);
                }
            });

            $('#cphCont_txtObservacionesB').on('input', function (e) {
                if (document.getElementById("#cphCont_txtObservacionesB").value.length > 512) {
                    this.value = document.getElementById("#cphCont_txtObservacionesB").value.substr(0, 512);
                }
            });

            $('#cphCont_txtObservacionesSolicitado').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionesSolicitado").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionesSolicitado").value.substr(0, 512);
                }
            });

            $('#cphCont_txtObservacionCitacion').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionCitacion").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionCitacion").value.substr(0, 512);
                }
            });

            $('#cphCont_txtObservacionesRecibido').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionesRecibido").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionesRecibido").value.substr(0, 512);
                }
            });          
            
        });

        function validateDate(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

            if (DateResolucionA != '') {

                var today = new Date();
                if (DateResolucionA > today) {
                    arguments.IsValid = false;
                }
                else if (fechaResolucionA[2] == "0000") {
                    arguments.IsValid = true;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function validateTodaymenos1(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

            if (DateResolucionA != '') {

                var today = new Date();
                var yesterday = new Date();
                yesterday.setDate(today.getDate() - 1);

                if (DateResolucionA > yesterday) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function validateFechaInvalida(source, arguments) {

            debugger;
            
            //if (document.getElementById(source.controltovalidate).disabled) {
                var EnteredDate = arguments.Value;
                var fechaResolucionA = EnteredDate.split('/');
                if (parseInt(fechaResolucionA[2]) < 1900 || fechaResolucionA[2] == '0000') {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            //}
        }

        function validateDateResolucion(source, arguments) {

            var EnteredDate = arguments.Value; //for javascript
            var fechaRecibido = EnteredDate.split('/');
            var DateB = new Date(fechaRecibido[2], fechaRecibido[1] - 1 , fechaRecibido[0])

            var fechaSolicitud = document.getElementById('cphCont_FechaSolicitud_txtFecha').value;
            var fechaSolicitada = fechaSolicitud.split('/');
            var DateA = new Date(fechaSolicitada[2], fechaSolicitada[1] - 1, fechaSolicitada[0]);

            if (DateA != '' && DateB != '') {

                var today = new Date();
                if (DateB < DateA) {
                    arguments.IsValid = false;
                } else if (DateB > today) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }
        
    </style>
</asp:Content>