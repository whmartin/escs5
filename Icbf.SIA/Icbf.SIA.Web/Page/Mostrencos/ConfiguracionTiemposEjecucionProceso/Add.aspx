<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ConfiguracionTiemposEjecucionProceso_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdConfiguracionTiemposEjecucionProceso" runat="server" />
    <asp:UpdatePanel ID="upConfiguracionTiemposEjecucionProceso" runat="server">
        <ContentTemplate>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>Fase del Proceso *
                <asp:CompareValidator runat="server" ID="cvFaseProceso" ControlToValidate="ddlFaseProceso"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                    </td>
                    <td>Actuaci&oacute;n del Proceso *
                <asp:CompareValidator runat="server" ID="cvActuacionProceso" ControlToValidate="ddlActuacionProceso"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFaseProceso" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFaseProceso_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlActuacionProceso" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlActuacionProceso_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Acci&oacute;n del Proceso *
                <asp:CompareValidator runat="server" ID="cvAccionProceso" ControlToValidate="ddlAccionProceso"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                    </td>
                    <td>Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlAccionProceso"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2"></td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Configuración de Tiempos</td>
                </tr>
                <tr class="rowA">
                    <td colspan="2"></td>
                </tr>
                <tr class="rowB">
                    <td>Calcular D&iacute;as Ejecuci&oacute;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvCalcularDiasEjecucion" ControlToValidate="rblCalcularDiasEjecucion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td>Tiempo Ejecuci&oacute;n Fase, Actuaci&oacute;n y Acci&oacute;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvTiempoEjecucionFAA" ControlToValidate="txtTiempoEjecucionFAA"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" ID="cvTiempoEjecucionFAA" ControlToValidate="txtTiempoEjecucionFAA" Display="Dynamic"
                            SetFocusOnError="true" ErrorMessage="El Tiempo Ejecución Fase, Actuación y Acción debe ser mayor o igual a uno (1)"
                            ValidationGroup="btnGuardar" ForeColor="Red" Operator="GreaterThan" ValueToCompare="000"></asp:CompareValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblCalcularDiasEjecucion" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">Calendario</asp:ListItem>
                            <asp:ListItem Value="0">H&aacute;biles</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTiempoEjecucionFAA" MaxLength="3"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftTiempoEjecuionFAA" runat="server" TargetControlID="txtTiempoEjecucionFAA"
                            FilterType="Numbers" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Calcular D&iacute;as Alerta</td>
                    <td>Tiempo Ejecuci&oacute;n Alerta
                <asp:CompareValidator runat="server" ID="cvTiempoEjecucionAlerta" ControlToValidate="txtTiempoEjecucionAlerta"
                    SetFocusOnError="true" ErrorMessage="El Tiempo Ejecución Alerta debe ser mayor o igual a uno (1)" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:RadioButtonList runat="server" ID="rblCalcularDiasAlerta" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">Calendario</asp:ListItem>
                            <asp:ListItem Value="0">H&aacute;biles</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTiempoEjecucionAlerta" MaxLength="3"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftTiempoEjecucionAlerta" runat="server" TargetControlID="txtTiempoEjecucionAlerta"
                            FilterType="Numbers" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
