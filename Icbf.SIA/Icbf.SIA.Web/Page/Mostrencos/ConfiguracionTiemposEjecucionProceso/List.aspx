﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConfiguracionTiemposEjecucionProceso_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Fase del Proceso</td>
                <td>Actuaci&oacute;n del Proceso</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtFaseProceso" MaxLength="80"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftFaseProceso" runat="server" TargetControlID="txtFaseProceso"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtActuacionProceso" MaxLength="80"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftActuacionProceso" runat="server" TargetControlID="txtActuacionProceso"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
                </td>
            </tr>
            <tr class="rowB">
                <td>Acci&oacute;n del Proceso</td>
                <td>Estado</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtAccionProceso" MaxLength="80"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftAccionProceso" runat="server" TargetControlID="txtAccionProceso"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">Activo</asp:ListItem>
                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                        <asp:ListItem Selected="True" Value="-1">Todos</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConfiguracionTiemposEjecucionProceso" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdConfiguracionTiemposEjecucionProceso" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvConfiguracionTiemposEjecucionProceso_PageIndexChanging" OnSelectedIndexChanged="gvConfiguracionTiemposEjecucionProceso_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fase del Proceso" DataField="Fase" />
                            <asp:BoundField HeaderText="Actuaci&oacute;n del Proceso" DataField="Actuacion" />
                            <asp:BoundField HeaderText="Acci&oacute;n del Proceso" DataField="Accion" />
                            <asp:BoundField HeaderText="Tiempo de Ejecuci&oacute;n" DataField="TiempoEjecucioFaseActuacionAccion" />
                            <asp:BoundField HeaderText="Tiempo Alerta" DataField="TiempoEjecucionAlerta" />
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <%# Eval("Estado").ToString() == "1" ? "Activo" : "Inactivo" %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
