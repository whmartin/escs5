﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ConfiguracionTiemposEjecucionProceso_Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

public partial class Page_ConfiguracionTiemposEjecucionProceso_Detail : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Declaración de Toolbar
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Path de la Página
    /// </summary>
    private string PageName = "Mostrencos/ConfiguracionTiemposEjecucionProceso";

    /// <summary>
    /// Servicio vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param> 
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Evento para validar el acceso a la página Add y tipo de transacción
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento para validar el acceso a la página Add(Editar) y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.SetSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso", hfIdConfiguracionTiemposEjecucionProceso.Value);
        this.NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento para validar el acceso a la página List y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    #endregion

    #region MÉTODOS

    /// <summary>
    /// Método para cargar los datos del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdConfiguracionTiemposEjecucionProceso = Convert.ToInt32(GetSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso"));
            RemoveSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso");

            if (GetSessionParameter("ConfiguracionTiemposEjecucionProceso.Guardado").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado();
            }

            RemoveSessionParameter("ConfiguracionTiemposEjecucionProceso.Guardado");
            RemoveSessionParameter("ConfiguracionTiemposEjecucionProceso");

            ConfiguracionTiemposEjecucionProceso vConfiguracionTiemposEjecucionProceso = new ConfiguracionTiemposEjecucionProceso();
            vConfiguracionTiemposEjecucionProceso = vMostrencosService.ConsultarConfiguracionTiemposEjecucionProceso(vIdConfiguracionTiemposEjecucionProceso);

            this.hfIdConfiguracionTiemposEjecucionProceso.Value = vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso.ToString();
            this.ddlFaseProceso.Items.Insert(0, new ListItem(vConfiguracionTiemposEjecucionProceso.Fase, vConfiguracionTiemposEjecucionProceso.IdFase.ToString()));
            this.ddlActuacionProceso.Items.Insert(0, new ListItem(vConfiguracionTiemposEjecucionProceso.Actuacion, vConfiguracionTiemposEjecucionProceso.IdActuacion.ToString()));
            this.ddlAccionProceso.Items.Insert(0, new ListItem(vConfiguracionTiemposEjecucionProceso.Accion, vConfiguracionTiemposEjecucionProceso.IdAccion.ToString()));
            this.rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            this.rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            this.rblEstado.SelectedValue = Convert.ToInt32(vConfiguracionTiemposEjecucionProceso.Estado).ToString();
            this.rblCalcularDiasEjecucion.Items.Insert(0, new ListItem("Calendario", "1"));
            this.rblCalcularDiasEjecucion.Items.Insert(1, new ListItem("H&aacute;biles", "0"));
            this.rblCalcularDiasEjecucion.SelectedValue = Convert.ToInt32(vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion).ToString();
            this.txtTiempoEjecucionFAA.Text = vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion.ToString();
            this.rblCalcularDiasAlerta.Items.Insert(0, new ListItem("Calendario", "1"));
            this.rblCalcularDiasAlerta.Items.Insert(1, new ListItem("H&aacute;biles", "0"));

            if (vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta != null)
            {
                this.rblCalcularDiasAlerta.SelectedValue = Convert.ToInt32(vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta).ToString();
            }
            
            this.txtTiempoEjecucionAlerta.Text = vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta.ToString();

            this.ObtenerAuditoria(PageName, hfIdConfiguracionTiemposEjecucionProceso.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = this.GetAuditLabel(vConfiguracionTiemposEjecucionProceso.UsuarioCrea, vConfiguracionTiemposEjecucionProceso.FechaCrea, vConfiguracionTiemposEjecucionProceso.UsuarioModifica, vConfiguracionTiemposEjecucionProceso.FechaModifica);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            this.toolBar.EstablecerTitulos("Configuracion Tiempos Ejecución Proceso", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}
