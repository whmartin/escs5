//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ConfiguracionTiemposEjecucionProceso_Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

public partial class Page_ConfiguracionTiemposEjecucionProceso_Add : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Declaración de Toolbar
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Path de la Página
    /// </summary>
    private string PageName = "Mostrencos/ConfiguracionTiemposEjecucionProceso";

    /// <summary>
    /// Servicio vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;

        if (Request.QueryString["oP"] == "E")
        {
            this.vSolutionPage = SolutionPage.Edit;
        }

        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.ddlFaseProceso.Focus();
                this.CargarDatosIniciales();

                if (Request.QueryString["oP"] == "E")
                {
                    this.CargarRegistro();
                }
            }
        }
    }

    /// <summary>
    /// Evento para Almacenar la información del formulario
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página Add y tipo de transacción
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento para validar el acceso a la página List y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento para Cargar la Lista de opciones de Actuaciones x IdFase
    /// </summary>
    /// <param name="sender">The DropDownList</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void ddlFaseProceso_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.ddlFaseProceso.SelectedValue != "-1")
        {
            int vIdFase = Convert.ToInt32(this.ddlFaseProceso.SelectedValue);

            List<Actuacion> vActuacion = this.vMostrencosService.ConsultarActuacionesActivasPorIdFase(vIdFase);

            vActuacion.Insert(0, new Actuacion() { Nombre = "SELECCIONE", IdActuacion = -1 });
            this.ddlActuacionProceso.DataSource = vActuacion;
            this.ddlActuacionProceso.DataTextField = "Nombre";
            this.ddlActuacionProceso.DataValueField = "IdActuacion";
            this.ddlActuacionProceso.DataBind();
            this.ddlActuacionProceso.SelectedValue = "-1";
        }
        else
        {
            this.ddlActuacionProceso.Items.Clear();
            this.ddlActuacionProceso.Items.Insert(0, new ListItem("SELECCIONE", "-1"));

            this.ddlAccionProceso.Items.Clear();
            this.ddlAccionProceso.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }

        this.ddlFaseProceso.Focus();
    }

    /// <summary>
    /// Evento para Cargar la Lista de opciones de Acciones x IdActuacion
    /// </summary>
    /// <param name="sender">The DropDownList</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void ddlActuacionProceso_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.ddlActuacionProceso.SelectedValue != "-1")
        {
            int vIdActuacion = Convert.ToInt32(this.ddlActuacionProceso.SelectedValue);

            List<Accion> vAccion = this.vMostrencosService.ConsultarAccionesActivasPorIdFase(vIdActuacion);

            vAccion.Insert(0, new Accion() { Nombre = "SELECCIONE", IdAccion = -1 });
            this.ddlAccionProceso.DataSource = vAccion;
            this.ddlAccionProceso.DataTextField = "Nombre";
            this.ddlAccionProceso.DataValueField = "IdAccion";
            this.ddlAccionProceso.DataBind();
            this.ddlAccionProceso.SelectedValue = "-1";
        }
        else
        {
            this.ddlAccionProceso.Items.Clear();
            this.ddlAccionProceso.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }

        this.ddlActuacionProceso.Focus();
    }

    #endregion

    #region MÉTODOS

    /// <summary>
    /// Método que almacena la información del formulario
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ConfiguracionTiemposEjecucionProceso vConfiguracionTiemposEjecucionProceso = new ConfiguracionTiemposEjecucionProceso();

            vConfiguracionTiemposEjecucionProceso.IdFase = Convert.ToInt32(ddlFaseProceso.SelectedValue);
            vConfiguracionTiemposEjecucionProceso.IdActuacion = Convert.ToInt32(ddlActuacionProceso.SelectedValue);
            vConfiguracionTiemposEjecucionProceso.IdAccion = Convert.ToInt32(ddlAccionProceso.SelectedValue);
            vConfiguracionTiemposEjecucionProceso.Estado = Convert.ToByte(rblEstado.SelectedValue);
            vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion = Convert.ToByte(rblCalcularDiasEjecucion.SelectedValue);
            vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion = Convert.ToInt32(txtTiempoEjecucionFAA.Text);
            
            if (rblCalcularDiasAlerta.SelectedValue != "")
            {
                vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta = Convert.ToByte(rblCalcularDiasAlerta.SelectedValue);
            }

            if (!string.IsNullOrEmpty(txtTiempoEjecucionAlerta.Text))
            {
                vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta = Convert.ToInt32(txtTiempoEjecucionAlerta.Text);
            }

            InformacionAudioria(vConfiguracionTiemposEjecucionProceso, this.PageName, vSolutionPage);

            if (Request.QueryString["oP"] == "E")
            {
                vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso = Convert.ToInt32(hfIdConfiguracionTiemposEjecucionProceso.Value);
                vConfiguracionTiemposEjecucionProceso.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vConfiguracionTiemposEjecucionProceso, this.PageName, vSolutionPage);
                vResultado = vMostrencosService.ModificarConfiguracionTiemposEjecucionProceso(vConfiguracionTiemposEjecucionProceso);
            }
            else
            {
                vConfiguracionTiemposEjecucionProceso.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vConfiguracionTiemposEjecucionProceso, this.PageName, vSolutionPage);
                vResultado = vMostrencosService.InsertarConfiguracionTiemposEjecucionProceso(vConfiguracionTiemposEjecucionProceso);
            }

            if (vResultado == -1)
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }

            if (vResultado == 0)
            {
                this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 0)
            {
                SetSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso", vResultado);
                SetSessionParameter("ConfiguracionTiemposEjecucionProceso.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Configuración Tiempos Ejecución Proceso", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga la información de ConfiguracionTiemposEjecucionProceso
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdConfiguracionTiemposEjecucionProceso = Convert.ToInt32(GetSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso"));
            RemoveSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso");

            ConfiguracionTiemposEjecucionProceso vConfiguracionTiemposEjecucionProceso = new ConfiguracionTiemposEjecucionProceso();
            vConfiguracionTiemposEjecucionProceso = vMostrencosService.ConsultarConfiguracionTiemposEjecucionProceso(vIdConfiguracionTiemposEjecucionProceso);

            this.hfIdConfiguracionTiemposEjecucionProceso.Value = vConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso.ToString();
            this.ddlFaseProceso.SelectedValue = vConfiguracionTiemposEjecucionProceso.IdFase.ToString();
            this.rblEstado.SelectedValue = Convert.ToInt32(vConfiguracionTiemposEjecucionProceso.Estado).ToString();
            this.rblCalcularDiasEjecucion.SelectedValue = Convert.ToInt32(vConfiguracionTiemposEjecucionProceso.CalculoDiasEjecucion).ToString();
            this.txtTiempoEjecucionFAA.Text = vConfiguracionTiemposEjecucionProceso.TiempoEjecucioFaseActuacionAccion.ToString();
            this.txtTiempoEjecucionAlerta.Text = vConfiguracionTiemposEjecucionProceso.TiempoEjecucionAlerta.ToString();

            List<Actuacion> vActuacion = this.vMostrencosService.ConsultarActuacionesActivasPorIdFase(Convert.ToInt32(this.ddlFaseProceso.SelectedValue));
            vActuacion.Insert(0, new Actuacion() { Nombre = "SELECCIONE", IdActuacion = -1 });
            this.ddlActuacionProceso.DataSource = vActuacion;
            this.ddlActuacionProceso.DataTextField = "Nombre";
            this.ddlActuacionProceso.DataValueField = "IdActuacion";
            this.ddlActuacionProceso.DataBind();
            this.ddlActuacionProceso.SelectedValue = vConfiguracionTiemposEjecucionProceso.IdActuacion.ToString();

            List<Accion> vAccion = this.vMostrencosService.ConsultarAccionesActivasPorIdFase(Convert.ToInt32(this.ddlActuacionProceso.SelectedValue));
            vAccion.Insert(0, new Accion() { Nombre = "SELECCIONE", IdAccion = -1 });
            this.ddlAccionProceso.DataSource = vAccion;
            this.ddlAccionProceso.DataTextField = "Nombre";
            this.ddlAccionProceso.DataValueField = "IdAccion";
            this.ddlAccionProceso.DataBind();
            this.ddlAccionProceso.SelectedValue = vConfiguracionTiemposEjecucionProceso.IdAccion.ToString();

            if (vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta != null)
            {
                this.rblCalcularDiasAlerta.SelectedValue = Convert.ToInt32(vConfiguracionTiemposEjecucionProceso.CalculoDiasAlerta).ToString();
            }

            this.ObtenerAuditoria(PageName, hfIdConfiguracionTiemposEjecucionProceso.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = this.GetAuditLabel(vConfiguracionTiemposEjecucionProceso.UsuarioCrea, vConfiguracionTiemposEjecucionProceso.FechaCrea, vConfiguracionTiemposEjecucionProceso.UsuarioModifica, vConfiguracionTiemposEjecucionProceso.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        List<FasesDenuncia> vFasesDenuncia = this.vMostrencosService.ConsultarFasesActivas();

        vFasesDenuncia.Insert(0, new FasesDenuncia() { NombreFase = "SELECCIONE", IdFase = -1 });
        this.ddlFaseProceso.DataSource = vFasesDenuncia;
        this.ddlFaseProceso.DataTextField = "NombreFase";
        this.ddlFaseProceso.DataValueField = "IdFase";
        this.ddlFaseProceso.DataBind();
        this.ddlFaseProceso.SelectedValue = "-1";

        this.ddlAccionProceso.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        this.ddlActuacionProceso.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
    }

    #endregion
}
