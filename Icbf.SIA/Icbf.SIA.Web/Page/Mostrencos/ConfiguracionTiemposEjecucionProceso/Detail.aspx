﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ConfiguracionTiemposEjecucionProceso_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdConfiguracionTiemposEjecucionProceso" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Fase del Proceso *</td>
            <td>Actuaci&oacute;n del Proceso *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlFaseProceso" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlActuacionProceso" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Acci&oacute;n del Proceso *</td>
            <td>Estado *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlAccionProceso" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2"></td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">Configuración de Tiempos</td>
        </tr>
        <tr class="rowA">
            <td colspan="2"></td>
        </tr>
        <tr class="rowB">
            <td>Calcular D&iacute;as Ejecuci&oacute;n *</td>
            <td>Tiempo Ejecuci&oacute;n Fase, Actuaci&oacute;n y Acci&oacute;n *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblCalcularDiasEjecucion" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTiempoEjecucionFAA" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Calcular D&iacute;as Alerta</td>
            <td>Tiempo Ejecuci&oacute;n Alerta</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblCalcularDiasAlerta" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTiempoEjecucionAlerta" Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
