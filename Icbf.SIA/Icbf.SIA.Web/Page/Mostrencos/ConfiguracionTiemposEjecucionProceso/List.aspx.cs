﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ConfiguracionTiemposEjecucionProceso_List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>09/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

public partial class Page_ConfiguracionTiemposEjecucionProceso_List : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Declaración de Toolbar
    /// </summary>
    private masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Path de la Página
    /// </summary>
    private string PageName = "Mostrencos/ConfiguracionTiemposEjecucionProceso";

    /// <summary>
    /// Servicio vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            if (!this.Page.IsPostBack)
            {
                this.txtFaseProceso.Focus();
            }
        }
    }

    /// <summary>
    /// Evento para lanzar la búsqueda 
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página Add y tipo de transacción
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento para exportar la grilla a excel
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void toolBar_eventoExcel(object sender, EventArgs e)
    {
        if (this.gvConfiguracionTiemposEjecucionProceso.Rows.Count == 0)
        {
            return;
        }

        this.Buscar();

        List<ConfiguracionTiemposEjecucionProceso> vListado = this.gvConfiguracionTiemposEjecucionProceso.DataSource as List<ConfiguracionTiemposEjecucionProceso>;
        AsistenteExcel.GenerarXLS(
        this.Response,
        AsistenteExcel.ConvertirListaEnTextoHtml(
        vListado,
        new AsistenteExcel.InformacionCelda()
        {
            Columna = "Fase",
            Titulo = "Fase del Proceso",
        },
        new AsistenteExcel.InformacionCelda()
        {
            Columna = "Actuacion",
            Titulo = "Actuaci&oacute;n del Proceso"
        },
        new AsistenteExcel.InformacionCelda()
        {
            Columna = "Accion",
            Titulo = "Acci&oacute;n del Proceso"
        },
        new AsistenteExcel.InformacionCelda()
        {
            Columna = "TiempoEjecucioFaseActuacionAccion",
            Titulo = "Tiempo de Ejecuci&oacute;n"
        },
        new AsistenteExcel.InformacionCelda()
        {
            Columna = "TiempoEjecucionAlerta",
            Titulo = "Tiempo Alerta"
        },
        new AsistenteExcel.InformacionCelda()
        {
            Columna = "Estado",
            Formato = "Activo;;Inactivo"
        }),
        "ConfigurarTiemposBienes.xls");
    }

    /// <summary>
    /// Método para el manejo de cambio del Control
    /// </summary>
    /// <param name="sender">The Grilla gvConfiguracionTiemposEjecucionProceso</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvConfiguracionTiemposEjecucionProceso_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvConfiguracionTiemposEjecucionProceso.SelectedRow);
    }

    /// <summary>
    /// Método que página la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvConfiguracionTiemposEjecucionProceso</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvConfiguracionTiemposEjecucionProceso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvConfiguracionTiemposEjecucionProceso.PageIndex = e.NewPageIndex;

        if (this.Session["SortedView"] != null)
        {
            List<ConfiguracionTiemposEjecucionProceso> vList = (List<ConfiguracionTiemposEjecucionProceso>)this.Session["SortedView"];
            this.gvConfiguracionTiemposEjecucionProceso.DataSource = vList;
            this.gvConfiguracionTiemposEjecucionProceso.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    #endregion

    #region MÉTODOS

    /// <summary>
    /// Metodo que consulta la lista de los registros de ConfiguracionTiemposEjecucionProceso
    /// </summary>
    /// <returns>The vLista</returns>
    private void Buscar()
    {
        try
        {
            List<ConfiguracionTiemposEjecucionProceso> vLista = new List<ConfiguracionTiemposEjecucionProceso>();

            string vFase = string.Empty;
            string vActuacion = string.Empty;
            string vAccion = string.Empty;
            byte? vEstado = null;

            vFase = !string.IsNullOrEmpty(this.txtFaseProceso.Text) ? this.txtFaseProceso.Text : vFase;
            vActuacion = !string.IsNullOrEmpty(this.txtActuacionProceso.Text) ? this.txtActuacionProceso.Text : vActuacion;
            vAccion = !string.IsNullOrEmpty(this.txtAccionProceso.Text) ? this.txtAccionProceso.Text : vAccion;

            if (this.rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToByte(this.rblEstado.SelectedValue);
            }
            
            vLista = vMostrencosService.ConsultarListaConfiguracionTiemposEjecucionProceso(vFase, vActuacion, vAccion, vEstado);
            this.Session["SortedView"] = vLista;

            this.gvConfiguracionTiemposEjecucionProceso.DataSource = vLista;
            this.gvConfiguracionTiemposEjecucionProceso.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            this.toolBar.eventoExcel += this.toolBar_eventoExcel;

            this.gvConfiguracionTiemposEjecucionProceso.PageSize = PageSize();
            this.gvConfiguracionTiemposEjecucionProceso.EmptyDataText = EmptyDataText();

            this.toolBar.EstablecerTitulos("Configuración Tiempos Ejecución Proceso", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que envia al detalle para consultar la información
    /// </summary>
    /// <param name="pRow">The pRow</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvConfiguracionTiemposEjecucionProceso.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("ConfiguracionTiemposEjecucionProceso.IdConfiguracionTiemposEjecucionProceso", strValue);
            this.NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}
