﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Edit.aspx.cs" Inherits="Page_Mostrencos_RegistrarParticipacionEconomica_Edit" %>

<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            background-color: #808080;
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 520px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .OcultarTerceros_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .descripcionGrilla {
            word-break: break-all;
        }

        .ajax__calendar_invalid .ajax__calendar_day {
            background: #d5d5d5;
            cursor: not-allowed;
            text-decoration: none !important;
        }
    </style>

    <asp:UpdatePanel runat="server" ID="up1">

        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de radicado de la denuncia *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado en correspondencia *             
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha radicado en correspondencia  *            
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Tipo de Identificación *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Número de identificación *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">

                        <tr class="rowB">
                            <td>&nbsp; Primer nombre *
                            </td>
                            <td class="auto-style1">&nbsp;</td>
                            <td>Segundo nombre *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>&nbsp; Primer apellido *
                            </td>
                            <td class="auto-style1"></td>
                            <td>Segundo apellido *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanelRazonSocial">
                        <tr class="rowB">
                            <td colspan="2">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">&nbsp;&nbsp;Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnInfoDenuncia" runat="server" ImageUrl="~/Image/btn/info.jpg" Visible="true"
                                Height="16px" Width="16px" OnClick="btnInfoDenuncia_Click" AutoPostBack="true" ToolTip="Ver Detalle" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Valor de la Denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextValordelaDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfIdDocumentoSolocitado" runat="server" />
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlgrvHistoricoDenuncia" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 15%; width: 70%; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                                <div>
                                    <asp:ImageButton ID="btnCerrarInmuebleModal" runat="server" Style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;" alt="h" OnClick="btnCerrarInmuebleModal_Click" ImageUrl="../../../Image/btn/close.png" />
                                </div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                           <div style="overflow-y: auto; overflow-x: auto; height: 200px; align-content: center">
                               <asp:GridView ID="grvHistoricoDenuncia" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False"
                                    AllowPaging="true" AllowSorting="true" GridLines="None"
                                    OnSorting="GvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="GvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                         <asp:BoundField HeaderText="Estado" DataField="NombreEstadoDenuncia" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" />
                                        <asp:BoundField HeaderText="Accion" DataField="Accion" />
                                        <asp:BoundField HeaderText="Actuacion" DataField="Actuacion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionPagoParticipacion">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3" class="tdTitulos">Información Pago de Participación Económica</td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 50%">Modalidad de Pago *                          
                             <asp:Label ID="lblCampoRequeridoModalidadPago" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList ID="rbtModalidadPago" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnTextChanged="rbtModalidadPago_TextChanged">
                                <asp:ListItem Value="2">Parcial</asp:ListItem>
                                <asp:ListItem Value="3">Total</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlRelacionOrdenesPago">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAplicarPago" />
                    </Triggers>
                </asp:UpdatePanel>
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3" class="tdTitulos">Relación Órdenes de Pago</td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha solicitud liquidación *                
                            <asp:Label ID="lblCampoRequeridoFechaSolicitudLiquidacion" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator2" ForeColor="Red"
                                ControlToValidate="FechaSolicitudLiquidacion$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAplicarPago"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                        </td>
                        <td colspan="2">Número de resolución de pago *
                            <asp:Label ID="lblCampoRequeridoNumeroResolucionPago" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaSolicitudLiquidacion" Enabled="true" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroResolucionPago" Enabled="true" MaxLength="15"></asp:TextBox></td>

                        <Ajax:FilteredTextBoxExtender ID="ftetxtNumeroResolucionPago" TargetControlID="txtNumeroResolucionPago" runat="server"
                            FilterType="Numbers" Enabled="True">
                        </Ajax:FilteredTextBoxExtender>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha resolución de pago *                
                            <asp:Label ID="lblCampoRequeridoFechaResolucion" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label runat="server" ID="Label2" Visible="false" Text="La fecha seleccionada no debe ser posterior a la fecha actual. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="Label3" Visible="false" Text="La fecha seleccionada no debe ser anterior a la fecha de solicitud. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator3" ForeColor="Red"
                                ControlToValidate="FechaResolucionPago$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAplicarPago"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                        </td>
                        <td colspan="2">Valor a pagar *
                            <asp:Label ID="reqValorAPagar" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaResolucionPago" Enabled="true" />
                        </td>
                        <td>
                        <asp:TextBox runat="server" ID="txtValorPagar" Enabled="true" MaxLength="19"
                                onchange="SetDecimal();" onkeydown="SetDecimal();" onkeyup="SetDecimal();" class="formatoDecimalFactor"></asp:TextBox></td>

                        <Ajax:FilteredTextBoxExtender ID="ftetxtValorPagar" TargetControlID="txtValorPagar" runat="server"
                           FilterType="Numbers,Custom" Enabled="True" ValidChars=",.">
                        </Ajax:FilteredTextBoxExtender>
                    </tr>
                    <tr class="rowB">
                        <td>Número Comprobante de Pago *
                            <asp:Label ID="lblValidacionComprobanteDePago" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td colspan="2">Valor Pagado *
                            <asp:Label ID="reqValorPagado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroComprobantePago" Enabled="true" MaxLength="20"></asp:TextBox></td>

                        <Ajax:FilteredTextBoxExtender ID="ftetxtNumeroComprobantePago" TargetControlID="txtNumeroComprobantePago" runat="server"
                            FilterType="Numbers,Custom" Enabled="True">
                        </Ajax:FilteredTextBoxExtender>

                        <td>
                            <asp:TextBox runat="server" ID="txtValorPagado" Enabled="true" MaxLength="19"
                                onchange="SetDecimal();" onkeydown="SetDecimal();" onkeyup="SetDecimal();" class="formatoDecimalFactor"></asp:TextBox></td>

                        <Ajax:FilteredTextBoxExtender ID="ftetxtValorPagado" TargetControlID="txtValorPagado" runat="server"
                            FilterType="Numbers,Custom" Enabled="True" ValidChars=",.">
                        </Ajax:FilteredTextBoxExtender>
                        <td>
                            <asp:ImageButton ID="btnAplicarPago" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicarPago_Click"
                                Height="25px" Width="28px" ToolTip="Aplicar" Visible="true" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlCostos">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="6"></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="grvRelacionOrdenesPago" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" ShowHeader="true" Visible="true" CellPadding="0" Height="16px" PageSize="10"
                                DataKeyNames="IdOrdenesPago"
                                OnRowDataBound="gvwDocumentacionRecibida_RowDataBound"
                                OnSorting="grvRelacionOrdenesPago_Sorting"
                                OnRowCommand="grvRelacionOrdenesPago_RowCommand"
                                OnPageIndexChanging="grvRelacionOrdenesPago_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField DataField="IdOrdenesPago" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Fecha solicitud liquidación" DataField="FechaSolicitudLiquidacion" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField HeaderText="Número de resolución de pago" DataField="NumeroResolucion" DataFormatString="{0:0}" HtmlEncode="false" />
                                    <asp:BoundField HeaderText="Fecha resolución de pago" DataField="FechaResolucionPago" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField HeaderText="Valor a pagar" DataField="ValorAPagar" DataFormatString="{0:c2}" />
                                    <asp:BoundField HeaderText="Número Comprobante de Pago" DataField="NumeroComprobantePago" DataFormatString="{0:0}" HtmlEncode="false" />
                                    <asp:BoundField HeaderText="Valor Pagado" DataField="ValorPagado" DataFormatString="{0:c2}" />
                                    <asp:BoundField DataField="IdModalidadPago.IdModalidadPago" ItemStyle-CssClass="Ocultar_" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditarRelacionDePago" runat="server" CommandName="Editar" ImageUrl="~/Image/btn/edit.gif"
                                                Height="16px" Width="16px" ToolTip="Editar" Enable="true"
                                                CommandArgument='<%# Eval("NumeroComprobantePago")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEliminarRelacionDePago" runat="server" CommandName="Eliminar" ImageUrl="~/Image/btn/delete.gif"
                                                Height="16px" Width="16px" ToolTip="Eliminar" Enable="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <panel runat="server" id="pnlCostoTotal">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="4"></td>
                    </tr>
                    <tr class="rowA">
                        <td width="50%"></td>   
                        <td colspan="3">Valor Total a Pagar Participación Económica
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtTotalaPagar" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td width="50%"></td>
                        <td colspan="3">Valor Total Pagado Participación Económica
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtTotalPagado" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
            </panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                        <td></td>

                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Documento Soporte de la Denuncia *
                            <asp:Label ID="lblCampoRequeridoTipoDocumentoSolicitado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label ID="lblTipoDocumentoSolicitado" runat="server" Text="Documento ya relacionado" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de solicitud *
                           <asp:Label runat="server" ID="lblrqfFechaSolicitud" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator1" ForeColor="Red"
                                ControlToValidate="FechaSolicitud$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAdicionarDocumento"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="auto-style4">
                            <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="true"
                                DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento" Height="30px" Width="234px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaSolicitud" Enabled="true" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnAdicionarDocumento" runat="server" ImageUrl="~/Image/btn/add.gif"
                                Height="25px" Width="28px" ToolTip="Adicionar" Enable="true" OnClick="btnAdicionarDocumento_Click"
                                ValidationGroup="btnAdicionarDocumento" />

                            <asp:ImageButton ID="btnAplicar" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicar_Click" Visible="false" ValidationGroup="btnAplicar"/>
                            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAplicar" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Observaciones al documento solicitado
                              <asp:Label ID="lblCampoRequeridoObservacionesDocumentoSolicitado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtObservacionesSolicitado"
                                Enabled="true" TextMode="MultiLine" MaxLength="512" Rows="8"
                                Width="600px" Height="100" Style="resize: none" onkeyup="return CheckLength('cphCont_txtObservacionesSolicitado');"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                TargetControlID="txtObservacionesSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="always">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAdicionarDocumento" />
                    </Triggers>
                </asp:UpdatePanel>
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="2">Documentación recibida
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 50%">Estado del documento
                        <asp:Label ID="lblCampoRequeridoEstadoDocumento" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Fecha de recibido <asp:Label runat="server" ID="ObliFeRe" Visible="false">*</asp:Label>
                            <asp:Label runat="server" ID="lblrqfFechaRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaRecibidoMaxima" Visible="false" Text="La fecha seleccionada no debe ser mayor a la fecha actual. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblRecibidoMenorSolicitud" Visible="false" Text="La fecha de recibido no debe ser anterior a la fecha de solicitud. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator13" ForeColor="Red"
                                ControlToValidate="FechaRecibido$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnAplicar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbtEstadoDocumento_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="3">Aceptado</asp:ListItem>
                                <asp:ListItem Value="5">Devuelto</asp:ListItem>
                            </asp:RadioButtonList>
                           
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaRecibido" Requerid="false" ErrorMessage="Campo Requerido" Enabled="true" Date="<%# DateTime.Today %>" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Nombre de archivo
                            <asp:Label runat="server" ID="ObliNoAr" Visible="false">*</asp:Label>
                            <asp:Label runat="server" ID="lblRqfNombreArchivo" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:FileUpload ID="fulArchivoRecibido" runat="server" />
                            <br />
                            <asp:Label ID="lblNombreArchivo" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento recibido
                            <asp:Label runat="server" ID="ObliObDoRe" Visible="false">*</asp:Label>
                            <asp:Label runat="server" ID="lblRqfObservacionesRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px"
                                Height="100" Style="resize: none" Enabled="true" onkeyup="return CheckLength('cphCont_txtObservacionesRecibido');">
                            </asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                TargetControlID="txtObservacionesRecibido"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="gvwHistorico" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien"
                                OnPageIndexChanging="gvwHistorico_PageIndexChanging"
                                OnRowCommand="gvwHistorico_RowCommand" CellPadding="0" Height="16px"
                                OnRowDataBound="gvwHistorico_RowDataBound"
                                AllowSorting="False" OnSorting="gvwHistorico_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                                    <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla" />
                                    <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" DataFormatString="{0:d}" SortExpression="FechaRecibidoGrilla" />
                                    <asp:BoundField DataField="FechaRecibidoGrilla" ItemStyle-CssClass="Ocultar_"/>
                                    <asp:BoundField DataField="NombreArchivo" SortExpression="NombreArchivo" ItemStyle-CssClass="Ocultar_"/>
                                    <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla" />
                                    <asp:TemplateField HeaderText="Nombre archivo" ItemStyle-CssClass="descripcionGrilla" SortExpression="NombreArchivo">
                                        <ItemTemplate>
                                            <asp:Button runat="server" CssClass="lnkArchivoDescargar descripcionGrilla" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" Enabled="true" ImageUrl="~/Image/btn/delete.gif"
                                                Height="16px" Width="16px" ToolTip="Eliminar" OnClientClick="return confirm('¿Está seguro de eliminar la información?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" ImageUrl="~/Image/btn/edit.gif"
                                                Height="16px" Width="16px" ToolTip="Editar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr hidden>
                        <td>
                            <asp:Button ID="btnVerArchivo" runat="server" Visible="true" AutoPostBack="true" Text="prueba" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlArchivo" Width="90%" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackgroundCssClass="Background"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">

                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">

                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="always">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnDescargar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-1 align-center">
                                    <div>
                                        <asp:ImageButton ID="btnCerrarPanelAr" runat="server" Style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative; width: 20px; height: 20px;"
                                            alt="h"
                                            OnClick="btnCerrarPanelAr_Click" ImageUrl="../../../Image/btn/close.png" />
                                    </div>
                                </div>
                            </div>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="../../../Scripts/autoNumeric.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('click', '.btnCerrarPopArchivo', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('#cphCont_txtObservacionesRecibido').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionesRecibido").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionesRecibido").value.substr(0, 512);
                }
            });

            //$('.formatoDecimalFactor').autoNumeric('init', {
            //    vMin: '0.00', vMax: '9999999999999999999.99', aSep: '.', aDec: ',',
            //});

        });
        function CheckLength(pin) {
            var textbox = document.getElementById(pin).value;
            if (textbox.trim().length >= 512) {
                document.getElementById(pin).value = textbox.substr(0, 512);
                return false;
            }
            else {
                return true;
            }
        }

        function SetDecimal() {
            $('.formatoDecimalFactor').autoNumeric('init', {
                vMin: '0.00', vMax: '9999999999999999999.99', aSep: '.', aDec: ',',
            });
        }

        function validateFechaInvalida(source, arguments) {
                    var EnteredDate = arguments.Value;
                    var fechaResolucionA = EnteredDate.split('/');
                    if (parseInt(fechaResolucionA[2]) < 1900 || fechaResolucionA[2] == '0000') {
                        arguments.IsValid = false;
                    }
                    else {
                        arguments.IsValid = true;
                    }
        }

    </script>
</asp:Content>



