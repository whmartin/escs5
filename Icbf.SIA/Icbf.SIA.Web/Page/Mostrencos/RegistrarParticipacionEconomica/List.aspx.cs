﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>15/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarParticipacionEconomica_List : GeneralWeb
{
    #region VARIABLES

    /// <summary>
    /// Variable tipo ToolBar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Codigo identificador de la denuncia
    /// </summary>
    private int vIdDenunciaBien;

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento Load de la página
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento Pre Init
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro EvenArg</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Paginación de registros
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro EvenArg</param>
    protected void grvInformacionPagos_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvInformacionPagos.PageIndex = e.NewPageIndex;
        this.CargarGrillaRelacionDePagos();
    }

    /// <summary>
    /// Paginación de registros
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro EvenArg</param>
    protected void grvInformacionPagos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(grvInformacionPagos.SelectedRow);
        NavigateTo(SolutionPage.Detail);
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Metodo inicial para cargar datos en el list
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Pago de Participación Económica");
            this.pnlgrvInformacion.Visible = false;

            toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cardar los datos de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            if ((vRegistroDenuncia.IdEstado == 11) || (vRegistroDenuncia.IdEstado == 13) || (vRegistroDenuncia.IdEstado == 19))
            {
                this.toolBar.OcultarBotonNuevo(true);
                this.toolBar.MostrarBotonEditar(false);
            }
            if (vIdDenunciaBien != 0)
            {
                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m.") && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.CodDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.CodDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;

                    if (vlstDenunciaBienTitulo.Count() > 0)
                    {
                        string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                        this.TextValordelaDenuncia.Text = valorDenuncia;
                    }
                    else
                    {
                        this.TextValordelaDenuncia.Text = "0,00";
                    }

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }
            }

            List<OrdenesPago> vListaOrdenesPagoDenunciaBien = this.vMostrencosService.ConsultarOrdenesPagoDenunciaBien(vIdDenunciaBien);

            this.txtTotalaPagar.Text = vListaOrdenesPagoDenunciaBien.Sum(item => item.ValorAPagar).ToString("C2");
            this.txtTotalPagado.Text = vListaOrdenesPagoDenunciaBien.Sum(item => item.ValorPagado).ToString("C2");

            if (vListaOrdenesPagoDenunciaBien.Count() > 0)
            {
                this.pnlgrvInformacion.Visible = true;
                this.grvInformacionPagos.Visible = true;
                this.grvInformacionPagos.DataSource = vListaOrdenesPagoDenunciaBien;
                this.grvInformacionPagos.DataBind();
            }
            else
            {
                this.grvInformacionPagos.PageSize = PageSize();
                this.grvInformacionPagos.EmptyDataText = EmptyDataText();
                this.grvInformacionPagos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar la grilla de Relación de pagos
    /// </summary>
    public void CargarGrillaRelacionDePagos()
    {
        List<OrdenesPago> vListaOrdenesPagoDenunciaBien = this.vMostrencosService.ConsultarOrdenesPagoDenunciaBien(Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien")));

        this.txtTotalaPagar.Text = vListaOrdenesPagoDenunciaBien.Sum(item => item.ValorAPagar).ToString("C2");
        this.txtTotalPagado.Text = vListaOrdenesPagoDenunciaBien.Sum(item => item.ValorPagado).ToString("C2");

        if (vListaOrdenesPagoDenunciaBien.Count() > 0)
        {
            this.pnlgrvInformacion.Visible = true;
            this.grvInformacionPagos.Visible = true;
            this.grvInformacionPagos.DataSource = vListaOrdenesPagoDenunciaBien;
            this.grvInformacionPagos.DataBind();
        }
        else
        {
            this.grvInformacionPagos.PageSize = PageSize();
            this.grvInformacionPagos.EmptyDataText = EmptyDataText();
            this.grvInformacionPagos.DataBind();
        }
    }

    /// <summary>
    /// Metodo para al dar click en detalle de la grilla guarde el ID del contrato  
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIdOrdenesPago = string.Empty;
            string strValueUsuarioCrea = string.Empty;


            if (grvInformacionPagos.DataKeys[rowIndex].Values["IdOrdenesPago"] != null)
            {
                strIdOrdenesPago = grvInformacionPagos.DataKeys[rowIndex].Values["IdOrdenesPago"].ToString();
            }

            SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdOrdenesPago", strIdOrdenesPago);

            if (strValueUsuarioCrea.Trim().ToUpper() == GetSessionUser().NombreUsuario.Trim().ToUpper())
            {
                SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.EsUsuarioAutenticado", true);
            }
            else
            {
                SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.EsUsuarioAutenticado", false);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion  

    #region BOTONES

    /// <summary>
    /// Metodo redirige a la pantalla list
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Metodo redirige a la pantalla nuevo
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo redirige a la pantalla detalle de resgistrar documentos bien denuncido
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (e.Row.Cells[i].Controls.Count > 0)
                {
                    ((LinkButton)e.Row.Cells[i].Controls[0]).TabIndex = -1;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[1].Text.Equals("01/01/0001")
                || e.Row.Cells[1].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[1].Text = "";
            }

            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("01/01/0001")
                || e.Row.Cells[3].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    #endregion   
}