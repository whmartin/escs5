﻿//-----------------------------------------------------------------------
// <copyright file="Page_Mostrencos_RegistrarParticipacionEconomica_Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Page_Mostrencos_RegistrarParticipacionEconomica_Add.aspx.cs</summary>
// <author>INGENIAN</author>
// <date>19/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Globalization;

public partial class Page_Mostrencos_RegistrarParticipacionEconomica_Add : GeneralWeb
{
    #region VARIABLES

    /// <summary>
    /// Variable tipo ToolBar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarParticipacionEconomica";

    /// <summary>
    /// Codigo identificador de la denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Contrato
    /// </summary>
    private int IdContrato;

    /// <summary>
    /// Valida documentación previo a Guardar
    /// </summary>
    public bool valida;

    /// <summary>
    /// Bandera de estado edición
    /// </summary>
    public bool vEdita = false;

    public static decimal vNumeroComprobante;

    /// <summary>
    /// File Name
    /// </summary>
    private string vFileName = string.Empty;

    #endregion

    #region EVENTOS  

    /// <summary>
    /// Evento Pre Init
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro EvenArg</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Load de la página
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                toolBar.MostrarBotonEditar(false);
            }
            else
                this.toolBar.OcultarBotonEditar(false);
        }
    }

    /// <summary>
    /// Paginación de la grilla documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void gvwHistorico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwHistorico.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwHistorico"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            gvwHistorico.DataSource = vList;
            gvwHistorico.DataBind();
        }
        else
        {
            ViewState["SortedgvwHistorico"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.gvwHistorico.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.gvwHistorico.DataBind();
        }
    }

    /// <summary>
    /// Eventos de los botones del grid Documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void gvwHistorico_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosRecibidos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosRecibidosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentacionRecibidaEliminar"];

        if (e.CommandName.Equals("Ver"))
        {
            this.btnDescargar.Visible = false;
            this.pnlArchivo.Visible = true;
            bool vVerArchivo = false;
            string vRuta = string.Empty;
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwHistorico.DataKeys[vRow].Value);

            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            DocumentosSolicitadosDenunciaBien vArchivo = vList.Find(x => x.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado);
            this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
            this.hfNombreArchivo.Value = vArchivo.NombreArchivo.ToString();

            vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;

            if (!vVerArchivo)
            {
                vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }

            if (vVerArchivo)
            {
                this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vArchivo.RutaArchivo.ToString();
                if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
        if (e.CommandName == "Eliminar")
        {
            pnlArchivo.CssClass = "popuphIstorico hidden";
            GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
            vdo.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(gvr.Cells[0].Text);



            foreach (var item in vListaDocumentosRecibidos.ToList())
            {
                if (item.IdDocumentosSolicitadosDenunciaBien == vdo.IdDocumentosSolicitadosDenunciaBien)
                {
                    vListaDocumentosRecibidosEliminar.Add(item);
                    vListaDocumentosRecibidos.Remove(item);
                }
            }

            this.ViewState["DocumentacionRecibidaEliminar"] = vListaDocumentosRecibidosEliminar;
            this.ViewState["SortedgvwHistorico"] = vListaDocumentosRecibidos;
            gvwHistorico.DataSource = vListaDocumentosRecibidos;
            gvwHistorico.DataBind();
        }
        else if (e.CommandName == "Editar")
        {
            this.btnAdicionarDocumento.Visible = false;
            this.btnAplicar.Visible = true;
            this.FechaRecibido.Date = DateTime.Now;
            this.pnlDocumentacionRecibida.Enabled = true;
            this.fulArchivoRecibido.Enabled = true;
            GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

            int vIdEstado = Convert.ToInt32(gvr.Cells[5].Text);
            SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDocumento", Convert.ToInt32(gvr.Cells[0].Text));

            int vIdDocumentoSolicitado = Convert.ToInt32(gvr.Cells[0].Text);
            DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
            //vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;

            if (vDocumentoSolicitado.IdEstadoDocumento == 2)
            {
                vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = true;
            }
            else
            {
                vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
            }

            this.hfIdDocumentoSolocitado.Value = vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();

            if (vIdEstado != 0) // Estado mayor a solicitado
            {
                this.ddlTipoDocumento.SelectedValue = Convert.ToString(gvr.Cells[1].Text);

                if ((gvr.Cells[3].Text).Equals(""))
                {
                    this.FechaSolicitud.InitNull = true;
                }
                else
                {
                    this.FechaSolicitud.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                }

                this.lblNombreArchivo.Text = Convert.ToString(gvr.Cells[9].Text);

                this.lblNombreArchivo.Visible = true;

                if ((gvr.Cells[7].Text).Equals("&nbsp;"))
                {
                    this.FechaRecibido.Date = DateTime.Now;
                }
                else
                {
                    this.FechaRecibido.Date = Convert.ToDateTime(gvr.Cells[7].Text);
                }

                this.txtObservacionesSolicitado.Text = (gvr.Cells[4].Text).Equals("&nbsp;") ? null : gvr.Cells[4].Text;
                this.txtObservacionesRecibido.Text = (gvr.Cells[10].Text).Equals("&nbsp;") ? null : gvr.Cells[10].Text;
                this.btnAdicionarDocumento.Enabled = true;
                this.FechaRecibido.Enabled = true;

                foreach (ListItem item in this.rbtEstadoDocumento.Items)
                {
                    if (vIdEstado == Convert.ToInt32(item.Value))
                    {
                        item.Selected = true;
                        this.ObliFeRe.Visible = true;
                        this.ObliNoAr.Visible = true;
                        this.ObliObDoRe.Visible = true;
                        break;
                    }
                }

                SetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia", Convert.ToInt32(gvr.Cells[0].Text));
                toolBar.LipiarMensajeError();
            }
            else { }
        }
    }

    /// <summary>
    /// Orden de los registros en la grilla documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void gvwHistorico_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwHistorico"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwHistorico"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreEstado"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                    }
                }
            }
        }

        //if (e.SortExpression.Equals("FechaRecibidoGrilla"))
        //{
        //    if (this.direction == SortDirection.Ascending)
        //    {
        //        ////Ascendente
        //        vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
        //    }
        //    else
        //    {
        //        ////Descendente
        //        vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
        //    }
        //}
        //else
        //{
        //    if (e.SortExpression.Equals("NombreArchivo"))
        //    {
        //        if (this.direction == SortDirection.Ascending)
        //        {
        //            ////Ascendente
        //            vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
        //        }
        //        else
        //        {
        //            ////Descendente
        //            vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
        //        }
        //    }
        //}

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwHistorico"] = vResult;
        this.gvwHistorico.DataSource = vResult;
        this.gvwHistorico.DataBind();
    }

    /// <summary>
    /// Paginación del grid Relación de Pago
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void grvRelacionOrdenesPago_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvRelacionOrdenesPago.PageIndex = e.NewPageIndex;
        this.CargarGrillaRelacionOrdenesDePago();
    }

    /// <summary>
    /// Evento para editar y eliminar registros del GridView Ordenes de apago
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void grvRelacionOrdenesPago_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Eliminar"))
            {
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                OrdenesPago vdo = new OrdenesPago();
                vdo.IdOrdenesPago = Convert.ToInt32(gvr.Cells[0].Text);
                SetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago", vdo.IdOrdenesPago);

                List<OrdenesPago> vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

                foreach (var item in vListaOrdenesDePago.ToList())
                {
                    if (item.IdOrdenesPago == vdo.IdOrdenesPago)
                    {
                        vListaOrdenesDePago.Remove(item);
                    }
                }

                this.txtTotalaPagar.Text = vListaOrdenesDePago.Sum(item => item.ValorAPagar).ToString("C2");
                this.txtTotalPagado.Text = vListaOrdenesDePago.Sum(item => item.ValorPagado).ToString("C2");

                this.SumarPagos(vListaOrdenesDePago);
                this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesDePago;
                grvRelacionOrdenesPago.DataSource = vListaOrdenesDePago;
                grvRelacionOrdenesPago.DataBind();

            }

            if (e.CommandName.Equals("Editar"))
            {
                this.vEdita = true;
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                OrdenesPago vdo = new OrdenesPago();
                vdo.IdOrdenesPago = Convert.ToInt32(gvr.Cells[0].Text);
                SetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago", vdo.IdOrdenesPago);

                if (vdo.IdOrdenesPago != 0) //Estado mayor a solicitado
                {

                    this.FechaSolicitudLiquidacion.Date = Convert.ToDateTime(gvr.Cells[1].Text);
                    this.txtNumeroResolucionPago.Text = Convert.ToString(gvr.Cells[2].Text);
                    this.FechaResolucionPago.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                    this.txtValorPagar.Text = Convert.ToString(gvr.Cells[4].Text).ToString();
                    this.txtNumeroComprobantePago.Text = Convert.ToString(gvr.Cells[5].Text).ToString();
                    vNumeroComprobante = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                    this.txtValorPagado.Text = Convert.ToString(gvr.Cells[6].Text);
                    //this.rbtModalidadPago.SelectedValue = Convert.ToString(gvr.Cells[7].Text);
                    this.btnAplicarPago.Visible = true;
                    this.btnAdicionarPago.Visible = false;
                    this.pnlRelacionOrdenesPago.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Orden de registros en el GridView
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void grvRelacionOrdenesPago_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    /// <summary>
    /// Botón aplicar Relación de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnAplicarPago_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.ActualizarListaDeRelacionDePagos();
            this.LimpiarPanelRelacionDePago();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Actualizar la lista de relación de pagos en el gridview
    /// </summary>
    public void ActualizarListaDeRelacionDePagos()
    {
        try
        {
            toolBar.LipiarMensajeError();
            var OrdenDePago = GetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago").GetType().Name;
            if (OrdenDePago != "String")
            {
                if (this.ValidarCamposEnRelacionOrdenesDePago() == false)
                {
                    if (this.ValidarValorDeCamposRelacionOrdenesDePago() == false)
                    {
                        this.btnAplicarPago.Visible = false;
                        this.btnAdicionarPago.Visible = true;
                        int IdOrdenDePago = (int)GetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago");
                        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                        //List<OrdenesPago> vListaOrdenesDePagoEditar = (List<OrdenesPago>)this.ViewState["OrdenesDePagoEditar"];
                        List<OrdenesPago> vListaOrdenesPagoDenunciaBien = this.vMostrencosService.ConsultarOrdenesPagoDenunciaBien(vIdDenunciaBien);
                        //var ExisteNumeroResolucion = vListaOrdenesPagoDenunciaBien.Find(x => x.NumeroResolucion == Convert.ToInt32(this.txtNumeroResolucionPago.Text));
                        List<OrdenesPago> vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
                        int ExisteIddenuncia = vListaOrdenesPagoDenunciaBien.Where(x => x.NumeroResolucion == Convert.ToInt32(this.txtNumeroResolucionPago.Text)).Count();
                        if (vListaOrdenesDePago.Count() > 0)
                        {
                            if (vListaOrdenesDePago.Count() > 1)
                            {
                                int ExisteGrilla = vListaOrdenesDePago.Where(x => x.NumeroResolucion == Convert.ToInt32(this.txtNumeroResolucionPago.Text)).Count();

                                bool bandera = false;
                                foreach (OrdenesPago item in vListaOrdenesDePago)
                                {
                                    if (item.IdOrdenesPago == IdOrdenDePago)
                                    {
                                        if (item.NumeroResolucion != Convert.ToDecimal(this.txtNumeroResolucionPago.Text))
                                        {
                                            bandera = true;
                                        }
                                         else if (item.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text)
                                            || item.FechaSolicitudLiquidacion != this.FechaSolicitudLiquidacion.Date 
                                            || item.FechaResolucionPago != this.FechaResolucionPago.Date 
                                            || item.NumeroComprobantePago != Convert.ToDecimal(this.txtNumeroComprobantePago.Text) 
                                            || item.ValorAPagar != Convert.ToDecimal(this.txtValorPagar.Text) 
                                            || item.ValorPagado != Convert.ToDecimal(this.txtValorPagado.Text))
                                        {
                                            bandera = false;
                                        }
                                        else
                                        {
                                            bandera = true;
                                        }
                                    }
                                }


                                //int Existe = vListaOrdenesDePago.Where(b => b.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text)
                                //        && b.FechaSolicitudLiquidacion != this.FechaSolicitudLiquidacion.Date
                                //        && b.FechaResolucionPago != this.FechaResolucionPago.Date && b.NumeroComprobantePago != Convert.ToDecimal(this.txtNumeroComprobantePago.Text)
                                //        && b.ValorAPagar != Convert.ToDecimal(this.txtValorPagar.Text) && b.ValorPagado != Convert.ToDecimal(this.txtValorPagado.Text)).Count();

                                if (bandera)
                                {
                                    if (ExisteGrilla == 1 || ExisteIddenuncia > 0)
                                    {
                                        this.toolBar.MostrarMensajeError("El registro ya existe");
                                    }
                                    else{
                                        foreach (OrdenesPago item in vListaOrdenesDePago.Where(x => x.IdOrdenesPago == IdOrdenDePago))
                                        {
                                            item.FechaSolicitudLiquidacion = Convert.ToDateTime(this.FechaSolicitudLiquidacion.Date);
                                            item.NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucionPago.Text);
                                            item.FechaResolucionPago = Convert.ToDateTime(this.FechaResolucionPago.Date);
                                            item.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                                            item.ValorPagado = Convert.ToDecimal(this.txtValorPagado.Text);
                                            item.ValorAPagar = Convert.ToDecimal(this.txtValorPagar.Text);
                                            this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesDePago;
                                            this.txtTotalaPagar.Text = vListaOrdenesDePago.Sum(a => a.ValorAPagar).ToString("C2");
                                            this.txtTotalPagado.Text = vListaOrdenesDePago.Sum(a => a.ValorPagado).ToString("C2");
                                            this.grvRelacionOrdenesPago.DataSource = vListaOrdenesDePago;
                                            this.grvRelacionOrdenesPago.DataBind();
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (OrdenesPago item in vListaOrdenesDePago.Where(x => x.IdOrdenesPago == IdOrdenDePago))
                                    {
                                        item.FechaSolicitudLiquidacion = Convert.ToDateTime(this.FechaSolicitudLiquidacion.Date);
                                        item.NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucionPago.Text);
                                        item.FechaResolucionPago = Convert.ToDateTime(this.FechaResolucionPago.Date);
                                        item.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                                        item.ValorPagado = Convert.ToDecimal(this.txtValorPagado.Text);
                                        item.ValorAPagar = Convert.ToDecimal(this.txtValorPagar.Text);
                                        this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesDePago;
                                        this.txtTotalaPagar.Text = vListaOrdenesDePago.Sum(a => a.ValorAPagar).ToString("C2");
                                        this.txtTotalPagado.Text = vListaOrdenesDePago.Sum(a => a.ValorPagado).ToString("C2");
                                        this.grvRelacionOrdenesPago.DataSource = vListaOrdenesDePago;
                                        this.grvRelacionOrdenesPago.DataBind();
                                    }
                                }
                            }
                            else{
                                if (ExisteIddenuncia > 0)
                                {
                                    this.toolBar.MostrarMensajeError("El registro ya existe");
                                }
                                else
                                {
                                    foreach (OrdenesPago item in vListaOrdenesDePago.Where(x => x.IdOrdenesPago == IdOrdenDePago))
                                    {
                                        item.FechaSolicitudLiquidacion = Convert.ToDateTime(this.FechaSolicitudLiquidacion.Date);
                                        item.NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucionPago.Text);
                                        item.FechaResolucionPago = Convert.ToDateTime(this.FechaResolucionPago.Date);
                                        item.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                                        item.ValorPagado = Convert.ToDecimal(this.txtValorPagado.Text);
                                        item.ValorAPagar = Convert.ToDecimal(this.txtValorPagar.Text);
                                        this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesDePago;
                                        this.txtTotalaPagar.Text = vListaOrdenesDePago.Sum(a => a.ValorAPagar).ToString("C2");
                                        this.txtTotalPagado.Text = vListaOrdenesDePago.Sum(a => a.ValorPagado).ToString("C2");
                                        this.grvRelacionOrdenesPago.DataSource = vListaOrdenesDePago;
                                        this.grvRelacionOrdenesPago.DataBind();
                                    }
                                }

                            }
                            //int Existe = vListaOrdenesDePago.Where(x => x.NumeroResolucion == Convert.ToInt32(this.txtNumeroResolucionPago.Text)).Count();
                            //int ExisteIddenuncia = vListaOrdenesPagoDenunciaBien.Where(x => x.NumeroResolucion == Convert.ToInt32(this.txtNumeroResolucionPago.Text)).Count();

                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Validar Relación de Campos Si se resgistra el Número de resolución
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarCamposEnRelacionOrdenesDePago()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            if (this.rbtModalidadPago.SelectedValue == "")
            {
                this.lblCampoRequeridoModalidadPago.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoModalidadPago.Visible = false;
                vEsrequerido = false;
            }

            try
            {
                if (this.FechaSolicitudLiquidacion.Date.ToString().Equals(new DateTime().ToString()) || this.FechaSolicitudLiquidacion.Date.ToString().Equals("01/01/1900 12:00:00 a. m.")
                    || this.FechaSolicitudLiquidacion.Date.ToString().Equals("01/01/0001 00:00:00") || this.FechaSolicitudLiquidacion.Date.ToString().Equals("1/01/1900 12:00:00 a. m."))
                {
                    this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = false;
                    vEsrequerido = false;
                }
            }
            catch (Exception)
            {
                this.CustomValidator2.IsValid = false;
                vEsrequerido = true;
                count = count + 1;
            }

            try
            {
                if (this.FechaResolucionPago.Date.ToString().Equals(new DateTime().ToString()) || this.FechaResolucionPago.Date.ToString().Equals("01/01/1900 12:00:00 a. m.")
                    || this.FechaResolucionPago.Date.ToString().Equals("01/01/0001 00:00:00") || this.FechaResolucionPago.Date.ToString().Equals("1/01/1900 12:00:00 a. m."))
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = false;
                    vEsrequerido = false;
                }
            }
            catch (Exception)
            {
                this.CustomValidator3.IsValid = false;
                vEsrequerido = true;
                count = count + 1;
            }



            if (this.txtNumeroResolucionPago.Text.Equals(string.Empty))
            {
                this.lblCampoRequeridoNumeroResolucionPago.Visible = true;
                count = count + 1;
                vEsrequerido = false;
            }
            else
            {
                this.lblCampoRequeridoNumeroResolucionPago.Visible = false;
            }

            if (this.txtValorPagar.Text.Equals(string.Empty))
            {
                this.reqValorAPagar.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.reqValorAPagar.Visible = false;
            }

            if (this.txtNumeroComprobantePago.Text.Equals(string.Empty))
            {
                this.lblValidacionComprobanteDePago.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.lblValidacionComprobanteDePago.Visible = false;
            }

            if (this.txtValorPagado.Text.Equals(string.Empty))
            {
                this.reqValorPagado.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.reqValorPagado.Visible = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido = false;
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validación del valor de la denuncia
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarValorDeCamposRelacionOrdenesDePago()
    {
        int count = 0;
        bool vEsMayor = false;
        string campo = "";

        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(vIdDenunciaBien);
            List<OrdenesPago> vListaOrdenesPago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

            decimal valorDenuncia = 0;
            decimal valorAPagar;
            decimal valorPagado;
            decimal TotalValorAPagar;
            decimal TotalValorPagado;

            bool convValorAPagar = decimal.TryParse(this.txtValorPagar.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out valorAPagar);
            bool convValorPagado = decimal.TryParse(this.txtValorPagado.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out valorPagado);
            bool convTotalValorAPagar = decimal.TryParse(this.txtTotalaPagar.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out TotalValorAPagar);
            bool convTotalValorPagado = decimal.TryParse(this.txtTotalPagado.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out TotalValorPagado);

            if (vlstDenunciaBienTitulo != null)
            {
                valorDenuncia = vlstDenunciaBienTitulo.ValorDenuncia;
            }
            else
            {
                valorDenuncia = 0;
            }

            if (convValorAPagar)
            {
                if (valorAPagar.ToString() != string.Empty)
                {
                    if (valorAPagar > valorDenuncia)
                    {
                        campo = "valor a pagar";
                        this.toolBar.MostrarMensajeError("El " + campo + " no debe superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }

            }

            if (convValorPagado)
            {
                if (this.txtValorPagado.Text.ToString() != "")
                {
                    if (valorPagado > Convert.ToDecimal(valorDenuncia))
                    {
                        decimal valor3 = decimal.Parse(this.txtValorPagado.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint);
                        campo = "valor pagado";
                        this.toolBar.MostrarMensajeError("El " + campo + " no debe superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }
            }

            if (convTotalValorAPagar)
            {
                if (TotalValorAPagar.ToString() != string.Empty)
                {
                    if (TotalValorAPagar > Convert.ToDecimal(valorDenuncia))
                    {
                        campo = "total a pagar";
                        this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }
            }

            if (convTotalValorPagado)
            {
                if (TotalValorPagado.ToString() != string.Empty)
                {
                    if (TotalValorPagado > Convert.ToDecimal(valorDenuncia))
                    {
                        campo = "total pagado";
                        this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }
            }
            else

            if (count > 0)
            {
                vEsMayor = true;
            }
            else
            {
                vEsMayor = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsMayor;
    }

    /// <summary>
    /// Evento Estado del documento
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ObliFeRe.Visible = true;
        this.ObliNoAr.Visible = true;
        this.ObliObDoRe.Visible = true;
    }

    /// <summary>
    /// Radio butons para selección de la modalidad de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void rbtModalidadPago_TextChanged(object sender, EventArgs e)
    {
        this.lblCampoRequeridoModalidadPago.Visible = false;
        if (this.rbtModalidadPago.SelectedValue == "2")
        {
            this.LimpiarPanelRelacionDePago();
            grvRelacionOrdenesPago.Columns[grvRelacionOrdenesPago.Columns.Count - 1].Visible = true;
            grvRelacionOrdenesPago.Columns[grvRelacionOrdenesPago.Columns.Count - 2].Visible = true;
            this.btnAdicionarPago.Visible = true;
        }

        if (this.rbtModalidadPago.SelectedValue == "3")
        {
            this.LimpiarPanelRelacionDePago();
            grvRelacionOrdenesPago.Columns[grvRelacionOrdenesPago.Columns.Count - 1].Visible = false;
            grvRelacionOrdenesPago.Columns[grvRelacionOrdenesPago.Columns.Count - 2].Visible = false;
            this.btnAdicionarPago.Visible = false;
            this.ViewState["SortedgrvRelacionOrdenesPago"] = new List<OrdenesPago>();
            List<OrdenesPago> vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
            this.grvRelacionOrdenesPago.DataSource = vListaOrdenesDePago;
            this.grvRelacionOrdenesPago.DataBind();
            this.txtTotalaPagar.Text = "";
            this.txtTotalPagado.Text = "";
            this.pnlCostoTotal.Visible = false;
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.grvHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.grvHistoricoDenuncia.DataSource = vList;
                this.grvHistoricoDenuncia.DataBind();
            }
            else
            {
                this.CargarDatosHistoricoDenuncia();
            }
            this.pnlgrvHistoricoDenuncia.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }
                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }
                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }
                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }
                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }
                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        this.pnlgrvHistoricoDenuncia.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.grvHistoricoDenuncia.DataSource = vResult;
        this.grvHistoricoDenuncia.DataBind();
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    #endregion

    #region METODOS       

    protected void btnInfoDenuncia_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.Visible = true;
    }

    protected void btnCerrarInmuebleModal_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.Visible = false;
    }

    /// <summary>
    /// Actualizar Historial de documentación recibida
    /// </summary>
    public bool ActualizarHistoricoDocumentacionRecibida()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

        int IdHistoricoDocumentoContratoParticipacionEconomica;
        int IdHistoricoDocumentoContratoParticipacionEconomicaDetalle = 0;
        int IdHistoricoDocumentoContratoParticipacionEconomicaNuevo = 0;
        int IdDocumentosSolicitados = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDocumentosSolicitadosDenunciaBienNuevo"));

        if (GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle").ToString() != string.Empty)
        {
            IdHistoricoDocumentoContratoParticipacionEconomicaDetalle = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle"));
        }

        if (GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo").ToString() != string.Empty)
        {
            IdHistoricoDocumentoContratoParticipacionEconomicaNuevo = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo"));
        }

        if (IdHistoricoDocumentoContratoParticipacionEconomicaDetalle != 0)
        {
            IdHistoricoDocumentoContratoParticipacionEconomica = IdHistoricoDocumentoContratoParticipacionEconomicaDetalle;
        }
        else
        {
            IdHistoricoDocumentoContratoParticipacionEconomica = IdHistoricoDocumentoContratoParticipacionEconomicaNuevo;
        }

        HistoricoDocumentosSolicitadosDenunciaBien pHistoricoDocumentos = new HistoricoDocumentosSolicitadosDenunciaBien();

        pHistoricoDocumentos.IdHistoricoDocumentosSolicitadosDenunciaBien = IdHistoricoDocumentoContratoParticipacionEconomica;
        pHistoricoDocumentos.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitados;
        pHistoricoDocumentos.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlTipoDocumento.SelectedValue);
        pHistoricoDocumentos.IdEstadoDocumento = Convert.ToInt32(rbtEstadoDocumento.SelectedValue);
        pHistoricoDocumentos.FechaSolicitud = FechaSolicitud.Date;
        pHistoricoDocumentos.FechaRecibido = FechaRecibido.Date;
        pHistoricoDocumentos.NombreArchivo = fulArchivoRecibido.FileName;
        pHistoricoDocumentos.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaCrea = DateTime.Now;
        pHistoricoDocumentos.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaModifica = DateTime.Now;

        int HistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistoricoDocumentos);
        if (HistoricoDocumentosRecibidos > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitados()
    {
        try
        {
            bool response = false;
            int cont = 0;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(vIdDenunciaBien);
            foreach (var item in vListaDocumentosSolicitados)
            {
                var count = vListaDocumentosSolicitadosYRecibidos.Find(x => x.NombreTipoDocumento == item.NombreTipoDocumento);
                if (count != null)
                {

                }
                else
                {
                    DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosYRecibidos();
                    pDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
                    pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = item.IdTipoDocumentoBienDenunciado;
                    pDocumentosSolicitadosDenunciaBien.FechaSolicitud = item.FechaSolicitud.Date;
                    pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = item.ObservacionesDocumentacionSolicitada;
                    pDocumentosSolicitadosDenunciaBien.NombreArchivo = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.RutaArchivo = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta = null;
                    pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 1;
                    pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pDocumentosSolicitadosDenunciaBien.FechaCrea = DateTime.Now;
                    pDocumentosSolicitadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pDocumentosSolicitadosDenunciaBien.FechaModifica = DateTime.Now;
                    int InsertarDocumentos = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienRelacionDePagos(pDocumentosSolicitadosDenunciaBien);
                    pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien = InsertarDocumentos;
                    InsertarHistoricoDocumentacionRecibida(pDocumentosSolicitadosDenunciaBien);
                    cont += 1;
                }
            }
            if (vListaDocumentosSolicitados.Count() == cont)
                response = true;

            return response;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Adicionar Relación de Pago
    /// </summary>
    /// <returns></returns>
    public bool AdicionarRelacionDePago()
    {
        try
        {
            bool response = false;
            int cont = 0;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

            List<OrdenesPago> vListaOrdenesDePago = new List<OrdenesPago>();
            vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

            foreach (var itemRelacionDePago in vListaOrdenesDePago)
            {

                int count = vListaOrdenesDePago.Count();

                if (count < 0)
                {
                    ////
                }
                else
                {
                    OrdenesPago pOrdenDePago = new OrdenesPago();
                    pOrdenDePago.IdModalidadPago.IdModalidadPago = itemRelacionDePago.IdModalidadPago.IdModalidadPago;
                    pOrdenDePago.IdDenunciaBien = vIdDenunciaBien;
                    pOrdenDePago.FechaSolicitudLiquidacion = itemRelacionDePago.FechaSolicitudLiquidacion;
                    pOrdenDePago.NumeroResolucion = itemRelacionDePago.NumeroResolucion;
                    pOrdenDePago.FechaResolucionPago = itemRelacionDePago.FechaResolucionPago;
                    pOrdenDePago.ValorAPagar = !string.IsNullOrEmpty(itemRelacionDePago.ValorAPagar.ToString()) ? Convert.ToDecimal(itemRelacionDePago.ValorAPagar.ToString()) : 0;
                    pOrdenDePago.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text.ToString());
                    pOrdenDePago.ValorPagado = !string.IsNullOrEmpty(itemRelacionDePago.ValorPagado.ToString()) ? Convert.ToDecimal(itemRelacionDePago.ValorPagado.ToString()) : 0;
                    pOrdenDePago.Estado = itemRelacionDePago.Estado;
                    pOrdenDePago.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pOrdenDePago.FechaCrea = DateTime.Now;
                    pOrdenDePago.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pOrdenDePago.FechaModifica = DateTime.Now;

                    int InsertarRelacionPagos = this.vMostrencosService.InsertarOrdenesDePago(pOrdenDePago);
                }
                cont += 1;
            }

            if (vListaOrdenesDePago.Count() == cont)
            {
                response = true;
            }
            return response;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitadosCache()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue))
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                vDocumentacionSolicitada.IdDenunciaBien = vIdDenunciaBien;
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.NombreEstado = PrimeraLetraMayuscula(vDocumentacionSolicitada.EstadoDocumento.NombreEstadoDocumento);
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentacionSolicitada.EsNuevo = true;
                //this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(vDocumentacionSolicitada);
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwHistorico"] = vListaDocumentacion;
                this.CargarGrillaDocumentacion();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }

            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public int AdicionarOrdenesDePago()
    {
        try
        {
            int cont = 0;
            int respuesta = 0;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

            List<OrdenesPago> vListaOrdenesDePago = new List<OrdenesPago>();
            vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
            if (vListaOrdenesDePago.Count() > 0)
            {
                foreach (var item in vListaOrdenesDePago)
                {
                    OrdenesPago pOrdenDePago = new OrdenesPago();
                    pOrdenDePago.IdModalidadPago.IdModalidadPago = Convert.ToInt32(this.rbtModalidadPago.SelectedValue);
                    pOrdenDePago.IdDenunciaBien = vIdDenunciaBien;
                    pOrdenDePago.FechaSolicitudLiquidacion = item.FechaSolicitudLiquidacion;
                    pOrdenDePago.NumeroResolucion = item.NumeroResolucion;
                    pOrdenDePago.FechaResolucionPago = item.FechaResolucionPago;
                    pOrdenDePago.ValorAPagar = item.ValorAPagar;
                    pOrdenDePago.NumeroComprobantePago = item.NumeroComprobantePago;
                    pOrdenDePago.ValorPagado = item.ValorPagado;
                    pOrdenDePago.Estado = true;
                    pOrdenDePago.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pOrdenDePago.FechaCrea = DateTime.Now;
                    pOrdenDePago.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pOrdenDePago.FechaModifica = DateTime.Now;
                    this.InformacionAudioria(pOrdenDePago, this.PageName, vSolutionPage);
                    respuesta = this.vMostrencosService.InsertarOrdenesDePago(pOrdenDePago);
                    SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdOrdenesPago", respuesta);

                    if (respuesta > 0)
                    {
                        if (item.FechaSolicitudLiquidacion != new DateTime())
                        {
                            this.InsertarHistoricoOrdenesDePago(1);
                        }
                        if (item.IdModalidadPago.IdModalidadPago != 2 || item.IdModalidadPago.IdModalidadPago != 3)
                        {
                            this.InsertarHistoricoOrdenesDePago(2);
                        }
                    }
                    cont += 1;
                }
            }
            return respuesta;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return 0;
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarRelacionOrdenesDePagoCache()
    {
        try
        {
            this.lblCampoRequeridoNumeroResolucionPago.Visible = false;
            bool vExiste = false;
            int count = 0;
            bool vEsMayor = false;
            string campo = string.Empty;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            OrdenesPago vOrdenesDePago = new OrdenesPago();
            List<OrdenesPago> vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

            DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(this.vIdDenunciaBien);
            decimal valorDenuncia = 0;

            if (vlstDenunciaBienTitulo != null)
            {
                valorDenuncia = vlstDenunciaBienTitulo.ValorDenuncia;
            }
            else
            {
                valorDenuncia = 0;
            }

            if (vListaOrdenesDePago.Count > 0)
            {
                if (this.vEdita)
                {
                    foreach (OrdenesPago item in vListaOrdenesDePago.Where(x => x.NumeroComprobantePago == vNumeroComprobante))
                    {
                        List<OrdenesPago> vListaOrdenesPago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

                        if (this.txtValorPagado.Text.ToString() != "")
                        {
                            if (Convert.ToDecimal(this.txtValorPagado.Text.ToString()) > Convert.ToDecimal(valorDenuncia))
                            {
                                campo = "valor pagado";
                                this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                                vEsMayor = true;
                                count = count + 1;
                            }

                            if (Convert.ToDecimal(this.txtValorPagado.Text.ToString()) + Convert.ToDecimal(vListaOrdenesPago.Sum(a => a.ValorPagado)) > Convert.ToDecimal(valorDenuncia))
                            {
                                campo = "valor pagado";
                                this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                                vEsMayor = true;
                                count = count + 1;
                            }
                        }
                        else
                        {
                            vEsMayor = false;
                        }

                        if (this.txtTotalaPagar.Text.ToString() != "")
                        {
                            if (Convert.ToDecimal(vListaOrdenesPago.Sum(a => a.ValorAPagar)) > Convert.ToDecimal(valorDenuncia))
                            {
                                campo = "total a pagar";
                                this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                                vEsMayor = true;
                                count = count + 1;
                            }

                            if ((Convert.ToDecimal(vListaOrdenesPago.Sum(a => item.ValorAPagar)) + Convert.ToDecimal(this.txtValorPagar.Text)) > Convert.ToDecimal(valorDenuncia))
                            {
                                campo = "total a pagar";
                                this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                                vEsMayor = true;
                                count = count + 1;
                            }
                        }

                        else
                        {
                            vEsMayor = false;
                        }

                        if (count > 0)
                        {
                            vEsMayor = true;
                        }
                        else
                        {
                            vEsMayor = false;
                        }

                        if (vEsMayor == false)
                        {
                            item.FechaSolicitudLiquidacion = Convert.ToDateTime(this.FechaSolicitudLiquidacion.Date);
                            item.NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucionPago.Text);
                            item.FechaResolucionPago = Convert.ToDateTime(this.FechaResolucionPago.Date);
                            item.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                            item.ValorPagado = Convert.ToDecimal(this.txtValorPagado.Text);
                            item.ValorAPagar = Convert.ToDecimal(this.txtValorPagar.Text);
                            vExiste = true;
                        }
                    }
                }
                else
                {
                    foreach (OrdenesPago item in vListaOrdenesDePago)
                    {
                        if (item.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text))
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                            break;
                        }
                    }
                }
            }

            if (!vExiste)
            {
                if (this.validarCamposValor() == false)
                {
                    OrdenesPago Orden = new OrdenesPago();
                    List<OrdenesPago> vListaOrdenesDePagoDB = new List<OrdenesPago>();
                    vListaOrdenesDePagoDB = this.vMostrencosService.ConsultarOrdenesPagoParticipacionPorIdDenuncia(vIdDenunciaBien);
                    Orden = vListaOrdenesDePagoDB.Find(x => x.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text.ToString()));
                    if (Orden != null)
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        vExiste = true;
                    }
                    else
                    {
                        vOrdenesDePago.IdOrdenesPago = vListaOrdenesDePago.Count() > 0 ? vListaOrdenesDePago.Count() + 1 : 1;
                        vOrdenesDePago.IdModalidadPago.IdModalidadPago = Convert.ToInt32(this.rbtModalidadPago.SelectedValue.ToString());
                        vOrdenesDePago.FechaSolicitudLiquidacion = this.FechaSolicitudLiquidacion.Date;
                        vOrdenesDePago.NumeroResolucion = !string.IsNullOrEmpty(this.txtNumeroResolucionPago.Text.ToString()) ? Convert.ToDecimal(this.txtNumeroResolucionPago.Text.ToString()) : 0;
                        vOrdenesDePago.FechaResolucionPago = this.FechaResolucionPago.Date;
                        vOrdenesDePago.ValorAPagar = !string.IsNullOrEmpty(this.txtValorPagar.Text.ToString()) ? Convert.ToDecimal(this.txtValorPagar.Text.ToString()) : 0;
                        vOrdenesDePago.NumeroComprobantePago = !string.IsNullOrEmpty(this.txtNumeroComprobantePago.Text.ToString()) ? Convert.ToDecimal(this.txtNumeroComprobantePago.Text.ToString()) : 0;
                        vOrdenesDePago.ValorPagado = !string.IsNullOrEmpty(this.txtValorPagado.Text.ToString()) ? Convert.ToDecimal(this.txtValorPagado.Text.ToString()) : 0;
                        vOrdenesDePago.Estado = true;
                        vListaOrdenesDePago.Add(vOrdenesDePago);
                        this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesDePago;
                        this.CargarGrillaRelacionOrdenesDePago();
                    }

                }
            }

            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Megas");
                }

                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = IdDenunciaBien + "/" + this.fulArchivoRecibido.FileName;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien.FileApoderado") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }

            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Metodo que carga la grilla historico e la denuncia
    /// </summary>
    public void CargarDatosHistoricoDenuncia()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.grvHistoricoDenuncia.DataSource = vHistorico;
            this.grvHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Captura los valores para cargar la documentacion
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);
            var IdDocumento = GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDocumento").GetType().Name;
            if (IdDocumento != "String")
            {
                if (ValidarDocumentacionRecibida() == false)
                {
                    int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDocumento");
                    int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                    bool vExiste = false;
                    if (this.fulArchivoRecibido.HasFile)
                    {
                        bool respPdf = this.fulArchivoRecibido.FileName.Contains(".pdf");
                        bool respImg = this.fulArchivoRecibido.FileName.Contains(".jpg");
                        string NombreArchivo = string.Empty;
                        if (respPdf)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }
                        else if (respImg)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }

                        if (NombreArchivo.Length > 50)
                        {
                            throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                        }
                        DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                        this.CargarArchivo(vDocumentoCargado);
                        this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", vDocumentoCargado);
                    }
                    DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                    if (vListaDocumentacion == null)
                    {
                        vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
                    }
                    if (vListaDocumentacion.Count() > 0)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                        else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.EsNuevo)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                        else if (IdDocumentosSolicitadosDenunciaBien != 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.IdDocumentosSolicitadosDenunciaBien != IdDocumentosSolicitadosDenunciaBien).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El registro ya existe");
                            vExiste = true;
                        }
                    }

                    if (!vExiste)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien > 0)
                        {
                            if (GuardarArchivos(vIdDocumentoDenuncia, 0))
                            {
                                bool vArchivoEsValido = false;
                                if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                                {
                                    vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                    if (vDocumentacionSolicitada == null)
                                        vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();

                                    vArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);

                                    if (vArchivoEsValido)
                                    {
                                        if (IdDocumentosSolicitadosDenunciaBien == 0)
                                        {
                                            vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                        }
                                        else
                                        {
                                            vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                                        }

                                        vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                                        vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                        vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                                        vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                                        vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                                        vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                                        vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                        this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", null);
                                    }
                                }
                                else
                                {
                                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                    vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                    vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                                }

                                if (vArchivoEsValido)
                                {
                                    vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                                    vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                                    vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
                                    if (!vDocumentacionSolicitada.EsNuevo)
                                    {
                                        DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                        vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                                        vListaDocumentacion.Remove(vDocumentacion);
                                        vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                                    }
                                    vListaDocumentacion.Add(vDocumentacionSolicitada);
                                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                                    this.CargarGrillaDocumentos();
                                    this.txtObservacionesSolicitado.Enabled = true;
                                    this.FechaSolicitud.Enabled = true;
                                    this.ddlTipoDocumento.Enabled = true;
                                    this.txtObservacionesSolicitado.Text = string.Empty;
                                    this.FechaSolicitud.InitNull = true;
                                    this.lblValFechaRecibidoMayor.Visible = false;
                                    this.rbtEstadoDocumento.ClearSelection();
                                    this.FechaRecibido.InitNull = true;
                                    this.txtObservacionesRecibido.Text = string.Empty;
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                if (this.ValidarDocumentacionSolicitada() == false)
                {
                    toolBar.LipiarMensajeError();
                    if (this.AdicionarDocumentosSolicitadosCache())
                    {
                        this.CargarGrillaDocumentosSolicitadosYRecibidos();
                        this.ddlTipoDocumento.SelectedValue = "-1";
                    }
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo carga los datos iniciales de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.rbtModalidadPago.Focus();
            this.pnlArchivo.Visible = false;
            this.ConfiguracionInicialDeControles();
            this.ViewState["SortedgvwHistorico"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.ViewState["DocumentacionRecibidaEliminar"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.ViewState["DocumentacionRecibidaEditar"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.ViewState["SortedgrvRelacionOrdenesPago"] = new List<OrdenesPago>();
            this.ViewState["OrdenesDePagoEliminar"] = new List<OrdenesPago>();
            this.ViewState["OrdenesDePagoEditar"] = new List<OrdenesPago>();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);

            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            this.CargarDatosHistoricoDenuncia();

            if ((vRegistroDenuncia.IdEstado != 11) || (vRegistroDenuncia.IdEstado != 13) || (vRegistroDenuncia.IdEstado != 19))
            {
                this.toolBar.OcultarBotonNuevo(true);
                this.toolBar.MostrarBotonEditar(false);
            }

            if (vIdDenunciaBien != 0)
            {
                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != (new DateTime()).ToString() ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != (new DateTime()).ToString()) && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.CodDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.CodDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;

                    // Se asigna el valor de la denuncia
                    if (vlstDenunciaBienTitulo.Count() > 0)
                    {
                        string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                        this.TextValordelaDenuncia.Text = valorDenuncia;
                    }
                    else
                    {
                        this.TextValordelaDenuncia.Text = "0,00";
                    }

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }

                List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
                TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
                vTipoDocumento.NombreTipoDocumento = "SELECCIONE";
                vTipoDocumento.Estado = "ACTIVO";
                vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
                vlstTipoDocumentosBien.Add(vTipoDocumento);
                vlstTipoDocumentosBien = vlstTipoDocumentosBien.Where(p => p.Estado == "ACTIVO").ToList();
                vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
                this.CargarGrillaDocumentacion();
                this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.ddlTipoDocumento.DataBind();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrillaDocumentacion()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados;
                this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                this.gvwHistorico.DataBind();
            }
            else
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                List<DocumentosSolicitadosDenunciaBien> vListaArchivos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                ViewState["SortedgvwHistorico"] = vListaArchivos;
                this.gvwHistorico.DataSource = vListaArchivos;
                this.gvwHistorico.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// Carga la grilla de la documentacion
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlDocumentacionSolicitada.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                this.gvwHistorico.DataBind();
            }
            else
            {
                this.pnlDocumentacionSolicitada.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// GridView Documentos Solicitados
    /// </summary>q
    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (((List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"]).Count == 0)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                this.gvwHistorico.DataBind();
            }
            else
            {
                this.pnlGridDocumentacionRecibida.Visible = false;
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    /// <summary>
    /// Cargar a la grilla los datos de historico de documentación
    /// </summary>
    public void CargarGrillaHistoricoDocumentacion()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DocumentosSolicitadosDenunciaBien> vListaArchivos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            ViewState["SortedgvwHistorico"] = vListaArchivos;
            this.gvwHistorico.DataSource = vListaArchivos;
            this.gvwHistorico.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// GridView Documentos Solicitados
    /// </summary>q
    public void CargarGrillaRelacionOrdenesDePago()
    {
        try
        {
            List<OrdenesPago> vListaOrdenesPago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
            if (vListaOrdenesPago.Count > 0)
            {
                this.pnlCostos.Visible = true;
                vListaOrdenesPago = vListaOrdenesPago.OrderBy(p => p.FechaSolicitudLiquidacion).ToList();
                this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesPago;

                this.txtTotalaPagar.Text = vListaOrdenesPago.Sum(item => item.ValorAPagar).ToString("C2");
                this.txtTotalPagado.Text = vListaOrdenesPago.Sum(item => item.ValorPagado).ToString("C2");

                this.grvRelacionOrdenesPago.DataSource = vListaOrdenesPago;
                this.grvRelacionOrdenesPago.DataBind();

                this.LimpiarPanelRelacionDePago();
            }
            else
            {
                this.pnlCostos.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    private void SumarPagos(List<OrdenesPago> vListaOrdenesPago)
    {

        if (vListaOrdenesPago.Count > 0)
        {
            this.pnlCostos.Visible = true;
            this.txtTotalaPagar.Text = vListaOrdenesPago.Sum(item => item.ValorAPagar).ToString("C2");
            this.txtTotalPagado.Text = vListaOrdenesPago.Sum(item => item.ValorPagado).ToString("C2");
            this.LimpiarPanelRelacionDePago();
        }
        else
        {
            this.txtTotalaPagar.Text = "0";
            this.txtTotalPagado.Text = "0";
            this.pnlCostos.Visible = false;
        }
    }
    /// <summary>
    /// Activar controles y paneles en la vista
    /// </summary>
    public void ConfiguracionInicialDeControles()
    {
        this.btnAdicionarPago.Visible = false;
        this.FechaRecibido.Enabled = false;
        this.fulArchivoRecibido.Enabled = false;
        this.pnlDocumentacionRecibida.Enabled = false;
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlCostoTotal.Visible = false;
        this.pnlDocumentacionSolicitada.Enabled = true;
        this.pnlRelacionOrdenesPago.Enabled = true;
    }

    /// <summary>
    /// Deshabilitar Paneles y campos
    /// </summary>
    protected void DeshabilitarPanelesyCampos()
    {
        this.txtDescripcion.Enabled = false;
        this.pnlInformacionPagoParticipacion.Enabled = false;
        this.pnlRelacionOrdenesPago.Enabled = false;
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlDocumentacionRecibida.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = false;
        this.FechaRecibido.Enabled = false;
        this.FechaSolicitud.Enabled = false;
        this.fulArchivoRecibido.Enabled = false;
        this.gvwHistorico.Enabled = false;

    }

    /// <summary>
    /// Metodo para habiliatar botones
    /// </summary>
    public void HabilitarBotones()
    {
        toolBar.MostrarBotonNuevo(true);
        toolBar.MostrarBotonEditar(true);
        toolBar.OcultarBotonGuardar(false);
    }

    /// <summary>
    /// Metodo inicial para cargar datos en el list
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Pago de Participación Económica");
            this.pnlCostos.Visible = false;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

            this.CargarDatosHistoricoDenuncia();

            this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            this.toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Insertar Histotico de Ordenes de Pago
    /// </summary>
    public int InsertarHistoricoOrdenesDePago(int tipoHistorico)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        int respuesta = 0;

        if (tipoHistorico == 1)
        {
            pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
            pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
            pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
            pHistoricoEstadosDenunciaBien.IdFase = 19; //// 19 Pago de participación económica
            pHistoricoEstadosDenunciaBien.IdActuacion = 23; //// 23 Pago
            pHistoricoEstadosDenunciaBien.IdAccion = 34;  //// 34 solicitar liquidacion - 35 Registro de pagos
            pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15; //// 1 Registrado
            this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, SolutionPage.Add);
            respuesta = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBienGeneral(pHistoricoEstadosDenunciaBien);
        }
        if (tipoHistorico == 2)
        {
            pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
            pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
            pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
            pHistoricoEstadosDenunciaBien.IdFase = 19; //// 19 Pago de participación económica
            pHistoricoEstadosDenunciaBien.IdActuacion = 23; //// 23 Pago
            pHistoricoEstadosDenunciaBien.IdAccion = 35;  //// 34 solicitar liquidacion - 35 Registro de pagos
            pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15; //// 1 Registrado
            this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, SolutionPage.Add);
            respuesta = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBienGeneral(pHistoricoEstadosDenunciaBien);
        }
        return respuesta;
    }

    /// <summary>
    /// Metodo para insertar el historico de documentacion recibida
    /// </summary>
    /// <param name="vIdDocumentosSolicitadosDenunciaBien"></param>
    public void InsertarHistoricoDocumentacionRecibida(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoDocumentosSolicitadosDenunciaBien pHistoricoDocumentos = new HistoricoDocumentosSolicitadosDenunciaBien();
        pHistoricoDocumentos.IdDocumentosSolicitadosDenunciaBien = pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien;
        pHistoricoDocumentos.IdTipoDocumentoBienDenunciado = pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado;
        pHistoricoDocumentos.IdEstadoDocumento = 1;
        pHistoricoDocumentos.FechaSolicitud = FechaSolicitud.Date;
        pHistoricoDocumentos.FechaRecibido = FechaRecibido.Date;
        pHistoricoDocumentos.NombreArchivo = fulArchivoRecibido.FileName;
        pHistoricoDocumentos.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaCrea = DateTime.Now;
        pHistoricoDocumentos.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoDocumentos, this.PageName, SolutionPage.Add);
        int HistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistoricoDocumentos);
        SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo", pHistoricoDocumentos.IdHistoricoDocumentosSolicitadosDenunciaBien);
        RemoveSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle");
    }

    /// <summary>
    /// Limpiar panel de Relación de Pago
    /// </summary>
    private void LimpiarPanelRelacionDePago()
    {
        this.FechaSolicitudLiquidacion.InitNull = true;
        this.txtNumeroResolucionPago.Text = "";
        this.FechaResolucionPago.InitNull = true;
        this.txtValorPagar.Text = "";
        this.txtNumeroComprobantePago.Text = "";
        this.txtValorPagado.Text = "";
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Validar Documentación Solicitada
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarDocumentacionSolicitada()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            //Validar Fecha Solicitud
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrqfFechaSolicitud.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblrqfFechaSolicitud.Visible = false;
                vEsrequerido = false;
            }
            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Recibida
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Fecha Recibido
            if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrqfFechaRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblrqfFechaRecibido.Visible = false;
                vEsrequerido = false;

                //Validar Campo Fecha Recibido
                if (this.FechaRecibido.Date > DateTime.Now)
                {
                    this.lblValFechaRecibidoMayor.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblValFechaRecibidoMayor.Visible = false;
                    vEsrequerido = false;
                }
            }

            //Validar Campo Nombre Archivo
            if (this.fulArchivoRecibido.FileName == "")
            {
                this.lblCampoRequeridoNombreArchivo.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoNombreArchivo.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Observaciones Documento Recibido
            if (this.txtObservacionesRecibido.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Relación de Ordenes de Pago
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarCamposRelacionOrdenesDePago()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            if (this.rbtModalidadPago.SelectedValue == "")
            {
                this.lblCampoRequeridoModalidadPago.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoModalidadPago.Visible = false;
                vEsrequerido = false;
            }

            try
            {
                if (this.FechaSolicitudLiquidacion.Date.ToString().Equals(new DateTime().ToString()) || this.FechaSolicitudLiquidacion.Date.ToString().Equals("01/01/1900 12:00:00 a. m.")
                    || this.FechaSolicitudLiquidacion.Date.ToString().Equals("01/01/0001 00:00:00") || this.FechaSolicitudLiquidacion.Date.ToString().Equals("1/01/1900 12:00:00 a. m."))
                {
                    this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = false;
                    vEsrequerido = false;
                }
            }
            catch (Exception)
            {
                this.CustomValidator2.IsValid = false;
                vEsrequerido = true;
                count = count + 1;
            }

            try
            {
                if (this.FechaResolucionPago.Date.ToString().Equals(new DateTime().ToString()) || this.FechaResolucionPago.Date.ToString().Equals("01/01/1900 12:00:00 a. m.")
                    || this.FechaResolucionPago.Date.ToString().Equals("01/01/0001 00:00:00") || this.FechaResolucionPago.Date.ToString().Equals("1/01/1900 12:00:00 a. m."))
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = false;
                    vEsrequerido = false;
                }
            }
            catch (Exception)
            {
                this.CustomValidator3.IsValid = false;
                vEsrequerido = true;
                count = count + 1;
            }



            if (this.txtNumeroResolucionPago.Text.Equals(string.Empty))
            {
                this.lblCampoRequeridoNumeroResolucionPago.Visible = true;
                count = count + 1;
                vEsrequerido = false;
            }
            else
            {
                this.lblCampoRequeridoNumeroResolucionPago.Visible = false;
            }

            if (this.txtValorPagar.Text.Equals(string.Empty))
            {
                this.reqValorAPagar.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.reqValorAPagar.Visible = false;
            }

            if (this.txtNumeroComprobantePago.Text.Equals(string.Empty))
            {
                this.lblValidacionComprobanteDePago.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.lblValidacionComprobanteDePago.Visible = false;
            }

            if (this.txtValorPagado.Text.Equals(string.Empty))
            {
                this.reqValorPagado.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.reqValorPagado.Visible = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido = false;
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Relación de Campos Si se resgistra el Número de resolución
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarCamposValoraPagarValorPagado()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            if (this.txtNumeroResolucionPago.Text != string.Empty)
            {
                if (this.FechaResolucionPago.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = false;
                    vEsrequerido = false;
                }
            }
            else
            {
                vEsrequerido = false;
            }

            if (this.FechaSolicitudLiquidacion.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validación del valor de la denuncia
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool validarCamposValor()
    {
        int count = 0;
        bool vEsMayor = false;
        string campo = "";

        try
        {
            DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(this.vIdDenunciaBien);
            List<OrdenesPago> vListaOrdenesPago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

            decimal valorDenuncia = 0;

            if (vlstDenunciaBienTitulo != null)
            {
                valorDenuncia = vlstDenunciaBienTitulo.ValorDenuncia;
            }
            else
            {
                valorDenuncia = 0;
            }

            if (this.txtValorPagar.Text.ToString() != "")
            {
                if (Convert.ToDecimal(this.txtValorPagar.Text.ToString()) > valorDenuncia)
                {
                    campo = "valor a pagar";
                    this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                    vEsMayor = true;
                    count = count + 1;
                }
            }
            else
            {
                vEsMayor = false;
            }

            if (this.txtValorPagado.Text.ToString() != "")
            {
                if (Convert.ToDecimal(this.txtValorPagado.Text.ToString()) > Convert.ToDecimal(valorDenuncia))
                {
                    campo = "valor pagado";
                    this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                    vEsMayor = true;
                    count = count + 1;
                }

                if (Convert.ToDecimal(this.txtValorPagado.Text.ToString()) + Convert.ToDecimal(vListaOrdenesPago.Sum(item => item.ValorPagado)) > Convert.ToDecimal(valorDenuncia))
                {
                    campo = "valor pagado";
                    this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                    vEsMayor = true;
                    count = count + 1;
                }
            }
            else
            {
                vEsMayor = false;
            }

            if (this.txtTotalaPagar.Text.ToString() != "")
            {
                if (Convert.ToDecimal(vListaOrdenesPago.Sum(item => item.ValorAPagar)) > Convert.ToDecimal(valorDenuncia))
                {
                    campo = "total a pagar";
                    this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                    vEsMayor = true;
                    count = count + 1;
                }

                if ((Convert.ToDecimal(vListaOrdenesPago.Sum(item => item.ValorAPagar)) + Convert.ToDecimal(this.txtValorPagar.Text)) > Convert.ToDecimal(valorDenuncia))
                {
                    campo = "total a pagar";
                    this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                    vEsMayor = true;
                    count = count + 1;
                }
            }
            else
            {
                vEsMayor = false;
            }

            if (this.txtTotalPagado.Text.ToString() != "")
            {
                if (Convert.ToDecimal(vListaOrdenesPago.Sum(item => item.ValorPagado)) > Convert.ToDecimal(valorDenuncia))
                {
                    campo = "total pagado";
                    this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                    vEsMayor = true;
                    count = count + 1;
                }
            }
            else
            {
                vEsMayor = false;
            }
            if (count > 0)
            {
                vEsMayor = true;
            }
            else
            {
                vEsMayor = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsMayor;
    }

    #endregion

    #region BOTONES  

    /// <summary>
    /// Adicionar documento
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnAdicionarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        this.toolBar.MostrarBotonEditar(false);
        this.toolBar.LipiarMensajeError();
        if (ValidarDocumentacionSolicitada() == false)
        {

            if (AdicionarDocumentosSolicitadosCache())
            {
                CargarGrillaDocumentosSolicitadosYRecibidos();
            }
        }
    }

    /// <summary>
    /// Adicionar relación de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnAdicionarPago_Click(object sender, ImageClickEventArgs e)
    {
        this.toolBar.MostrarBotonEditar(false);
        this.toolBar.LipiarMensajeError();
        this.pnlCostoTotal.Visible = true;
        if (this.ValidarCamposRelacionOrdenesDePago() == false)
        {
            this.pnlCostoTotal.Visible = true;
            if (this.AdicionarRelacionOrdenesDePagoCache())
            {
                this.vEdita = false;
            }
        }
    }

    /// <summary>
    /// Aplicar cambios en editar documentación solicitada
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.ActualizarListaDeDocumentacionRecibida();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo redirige a la pantalla list
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Boton Editar
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.lblCampoRequeridoModalidadPago.Visible = false;
            if (this.rbtModalidadPago.SelectedValue == "")
            {
                this.lblCampoRequeridoModalidadPago.Visible = true;
            }
            else
            {
                toolBar.LipiarMensajeError();
                toolBar.MostrarBotonEditar(false);
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

                List<OrdenesPago> vListaOrdenesPagoDenunciaBien = this.vMostrencosService.ConsultarOrdenesPagoDenunciaBien(vIdDenunciaBien);
                var PagoParcial = vListaOrdenesPagoDenunciaBien.Find(x => x.IdModalidadPago.IdModalidadPago == 2);
                if ((this.rbtModalidadPago.SelectedValue == "2" && PagoParcial != null)
                    || (this.rbtModalidadPago.SelectedValue == "2" && vListaOrdenesPagoDenunciaBien.Count == 0))
                {
                    OrdenesPago vOrdenDePago = new OrdenesPago();
                    List<OrdenesPago> vOrdenesDePago = new List<OrdenesPago>();

                    vOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosRecibidosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentacionRecibidaEliminar"];
                    vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
                    this.lblCampoRequeridoModalidadPago.Visible = false;
                    if (this.rbtModalidadPago.SelectedValue == "2")
                    {
                        if (vOrdenesDePago.Count > 0)
                        {
                            if (this.validarCamposValor() == false)
                            {
                                if (vListaDocumentosRecibidosEliminar == null)
                                {
                                    vListaDocumentosRecibidosEliminar = new List<DocumentosSolicitadosDenunciaBien>();
                                }

                                if (vListaDocumentosRecibidosEliminar.Count() > 0)
                                {
                                    foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosRecibidosEliminar)
                                    {
                                        item.IdEstadoDocumento = 6;
                                        vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(item);
                                    }
                                }
                                if (vListaDocumentosSolicitados.Count() > 0)
                                {
                                    vListaDocumentosSolicitados.ForEach(item =>
                                    {
                                        item.IdDenunciaBien = vIdDenunciaBien;
                                        this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);

                                        if (item.EsNuevo == true)
                                        {
                                            if (item.UsuarioCrea == null)
                                            {
                                                item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                                            }

                                            if (item.NombreArchivo != null && item.RutaArchivo != null)
                                            {
                                                this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                                            }
                                            else
                                                this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                                        }
                                        else
                                        {
                                            this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
                                        }

                                    });
                                }

                                if (this.rbtModalidadPago.SelectedValue.ToString() == "2")
                                {
                                    if (vOrdenesDePago.Count() > 0)
                                    {
                                        int RespuestaGuardarRelacionDePago = AdicionarOrdenesDePago();

                                        if (RespuestaGuardarRelacionDePago > 0)
                                        {
                                            this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                                            this.LimpiarPanelRelacionDePago();
                                            this.pnlDocumentacionSolicitada.Enabled = false;
                                            this.pnlCostos.Visible = true;
                                            this.pnlCostoTotal.Visible = true;
                                            this.DeshabilitarPanelesyCampos();
                                            this.HabilitarBotones();
                                        }
                                        if (RespuestaGuardarRelacionDePago == -1)
                                        {
                                            this.toolBar.MostrarMensajeError("El registro ya existe");
                                            this.pnlCostos.Visible = false;
                                            this.pnlCostoTotal.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        this.toolBar.MostrarMensajeError("Debe Adiccionar un Pago");
                                    }
                                }
                            }
                            this.grvRelacionOrdenesPago.Enabled = false;
                            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                            if ((vRegistroDenuncia.IdEstado == 11) || (vRegistroDenuncia.IdEstado == 13) || (vRegistroDenuncia.IdEstado == 19))
                            {
                                this.toolBar.OcultarBotonGuardar(false);
                                this.toolBar.MostrarBotonNuevo(false);
                                this.toolBar.MostrarBotonEditar(false);
                            }
                        }
                    }
                    else
                    {
                        this.lblCampoRequeridoModalidadPago.Visible = true;
                    }
                }
                else if ((this.rbtModalidadPago.SelectedValue == "3" && PagoParcial == null)
                    || (this.rbtModalidadPago.SelectedValue == "3" && vListaOrdenesPagoDenunciaBien.Count == 0))
                {
                    if (this.ValidarCamposRelacionOrdenesDePago() == false)
                    {
                        OrdenesPago vOrdenDePago = new OrdenesPago();
                        List<OrdenesPago> vOrdenesDePago = new List<OrdenesPago>();
                        vOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosRecibidosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentacionRecibidaEliminar"];
                        vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
                        this.lblCampoRequeridoModalidadPago.Visible = false;
                        if (this.rbtModalidadPago.SelectedValue == "3")
                        {
                            if (this.ValidarCamposRelacionOrdenesDePago() == false)
                            {
                                if (this.validarCamposValor() == false)
                                {
                                    if (vListaDocumentosRecibidosEliminar == null)
                                    {
                                        vListaDocumentosRecibidosEliminar = new List<DocumentosSolicitadosDenunciaBien>();
                                    }

                                    if (vListaDocumentosRecibidosEliminar.Count() > 0)
                                    {
                                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosRecibidosEliminar)
                                        {
                                            item.IdEstadoDocumento = 6;
                                            vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(item);
                                        }
                                    }
                                    if (vListaDocumentosSolicitados.Count() > 0)
                                    {
                                        vListaDocumentosSolicitados.ForEach(item =>
                                        {
                                            item.IdDenunciaBien = vIdDenunciaBien;
                                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);

                                            if (item.EsNuevo == true)
                                            {
                                                if (item.UsuarioCrea == null)
                                                {
                                                    item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                                                }

                                                if (item.NombreArchivo != null && item.RutaArchivo != null)
                                                {
                                                    this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                                                }
                                                else
                                                    this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                                            }
                                            else
                                            {
                                                this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
                                            }

                                        });
                                    }

                                    if (this.rbtModalidadPago.SelectedValue.ToString() == "3")
                                    {
                                        if (this.ValidarCamposValoraPagarValorPagado() == false)
                                        {
                                            bool EsAdicionarRelacionDePagosCache = AdicionarRelacionOrdenesDePagoCache();

                                            if (EsAdicionarRelacionDePagosCache == false)
                                            {
                                                int RespuestaGuardarRelacionDePago = AdicionarOrdenesDePago();

                                                if (RespuestaGuardarRelacionDePago > 0)
                                                {
                                                    bool adicion = AdicionarDocumentosSolicitados();
                                                    this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                                                    this.pnlDocumentacionSolicitada.Enabled = false;
                                                    this.pnlCostos.Visible = true;
                                                    this.pnlCostoTotal.Visible = true;
                                                    this.DeshabilitarPanelesyCampos();
                                                    this.HabilitarBotones();
                                                }
                                                if (RespuestaGuardarRelacionDePago == -1)
                                                {
                                                    this.toolBar.MostrarMensajeError("El registro ya existe");
                                                    this.pnlCostos.Visible = false;
                                                    this.pnlCostoTotal.Visible = false;
                                                }
                                            }
                                        }
                                    }
                                }
                                this.grvRelacionOrdenesPago.Enabled = false;
                                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                                if ((vRegistroDenuncia.IdEstado == 11) || (vRegistroDenuncia.IdEstado == 13) || (vRegistroDenuncia.IdEstado == 19))
                                {
                                    this.toolBar.OcultarBotonGuardar(false);
                                    this.toolBar.MostrarBotonNuevo(false);
                                    this.toolBar.MostrarBotonEditar(false);
                                }
                            }
                        }
                        else
                        {
                            this.lblCampoRequeridoModalidadPago.Visible = true;
                        }
                    }

                }
                else
                {
                    toolBar.MostrarMensajeError("La denuncia ya tiene pagos parciales asociados");
                }
            }


        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Botón descargar documento
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/" + vNombreArchivo));
                vRuta = "../RegistrarCalidadDenunciante/" + vNombreArchivo;
            }
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vNombreArchivo));
                vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vNombreArchivo;
            }
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../ContratoParticipacionEconomica/Archivos/" + vNombreArchivo));
                vRuta = "../ContratoParticipacionEconomica/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo cargar los datos correspondientes a la pantalla de editar
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("../RegistrarParticipacionEconomica/Edit.aspx");
    }

    /// <summary>
    /// Metodo redirige a la pantalla detalle de resgistrar documentos bien denuncido
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #endregion

    /// <summary>
    /// Actualizar la lista de Documentacion Recibida
    /// </summary>
    public void ActualizarListaDeDocumentacionRecibida()
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];

            if (this.ValidarDocumentacionSolicitadayRecibida() == false)
            {
                if (this.GuardarArchivos(vIdDocumentoDenuncia, 0))
                {
                    vListaDocumentosSolicitados.ForEach(item =>
                    {
                        if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoDenuncia)
                        {
                            item.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                            item.FechaSolicitud = new DateTime(FechaSolicitud.Date.Year, FechaSolicitud.Date.Month, FechaSolicitud.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                            item.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text;
                            item.IdTipoDocumentoSoporteDenuncia = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                            item.NombreTipoDocumento = this.ddlTipoDocumento.SelectedItem.Text;
                            item.NombreEstado = this.rbtEstadoDocumento.SelectedItem.Text;
                            item.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                            item.FechaRecibidoGrilla = item.FechaRecibido.Value.ToString("dd/MM/yyyy");
                            item.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                            item.NombreArchivo = fulArchivoRecibido.FileName;
                            item.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName;
                        }
                    });
                    this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados;
                    this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                    this.gvwHistorico.DataBind();
                    this.txtObservacionesSolicitado.Enabled = true;
                    this.FechaSolicitud.Enabled = true;
                    this.ddlTipoDocumento.Enabled = true;
                    this.ddlTipoDocumento.SelectedValue = "-1";
                    this.txtObservacionesSolicitado.Text = string.Empty;
                    this.FechaSolicitud.InitNull = true;
                    this.lblValFechaRecibidoMayor.Visible = false;
                    this.rbtEstadoDocumento.ClearSelection();
                    this.FechaRecibido.InitNull = true;
                    this.txtObservacionesRecibido.Text = string.Empty;
                    this.lblNombreArchivo.Visible = false;
                    this.btnAplicar.Visible = false;
                    this.btnAdicionarDocumento.Visible = true;
                    this.FechaRecibido.InitNull = true;
                }
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Validar documentacion solicitada y recibida
    /// </summary>
    /// <returns>Resultado de la validación</returns>
    public bool ValidarDocumentacionSolicitadayRecibida()
    {
        Boolean vPuedeGuardar = true;
        int count = 0;
        int vIdDocumentoDenuncia = (int)GetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
        if (vIdDocumentoDenuncia != 0)
        {
            if (this.rbtEstadoDocumento.SelectedValue != "")
            {
                if (this.ddlTipoDocumento.SelectedValue == "-1")
                {
                    this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                    vPuedeGuardar = false;
                }
                if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblrqfFechaSolicitud.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    this.lblrqfFechaSolicitud.Visible = false;
                    vPuedeGuardar = false;
                }

                if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblrqfFechaRecibido.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    this.lblrqfFechaRecibido.Visible = false;
                    vPuedeGuardar = false;

                    if (this.FechaRecibido.Date > DateTime.Now)
                    {
                        this.lblValFechaRecibidoMayor.Visible = true;
                        vPuedeGuardar = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblValFechaRecibidoMayor.Visible = false;
                        vPuedeGuardar = false;
                    }

                    if (this.FechaRecibido.Date < this.FechaSolicitud.Date)
                    {
                        this.lblRecibidoMenorSolicitud.Visible = true;
                        vPuedeGuardar = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblRecibidoMenorSolicitud.Visible = false;
                        vPuedeGuardar = false;
                    }
                }

                if (this.txtObservacionesRecibido.Text == "")
                {
                    this.lblRqfObservacionesRecibido.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    vPuedeGuardar = false;
                    this.lblRqfObservacionesRecibido.Visible = false;
                }

                if (!fulArchivoRecibido.HasFile)
                {
                    this.lblRqfNombreArchivo.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    vPuedeGuardar = false;
                    this.lblRqfNombreArchivo.Visible = false;
                }
            }
            if (this.rbtEstadoDocumento.SelectedValue == "")
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vPuedeGuardar = true;
                count = count + 1;
            }
            else
            {
                vPuedeGuardar = false;
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
            }
            if (count > 0)
            {
                vPuedeGuardar = true;
            }
            else
            {
                vPuedeGuardar = false;
            }
        }
        return vPuedeGuardar;
    }

    /// <summary>
    /// Guardars the archivos.
    /// </summary>
    /// <param name="pIdDenunciaBien">The p identifier denuncia bien.</param>
    /// <param name="pPosition">The p position.</param>
    /// <returns></returns>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        try
        {
            bool respuesta = true;
            HttpFileCollection files = Request.Files;

            if (files.Count > 0)
            {
                bool espPdf = this.fulArchivoRecibido.FileName.Contains(".pdf");
                bool espImg = this.fulArchivoRecibido.FileName.Contains(".jpg");
                string NombreArchivo = string.Empty;

                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                string extImg = ".JPG".ToLower();

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);

                int tamanoNombre = this.fulArchivoRecibido.FileName.Length;
                NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoNombre - 4));

                if (NombreArchivo.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }

                if (extFile != extPdf && extFile != extImg)
                {
                    this.toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF ó JPG");
                    respuesta = false;
                }

                if (vtamArchivo > 4096)
                {
                    this.toolBar.MostrarMensajeError("El tamaño máximo del archivo es de 4 MB");
                    respuesta = false;
                }

                HttpPostedFile file = files[pPosition];

                if (file.ContentLength > 0)
                {
                    if (respuesta)
                    {
                        string rutaParcial = string.Empty;
                        string carpetaBase = string.Empty;
                        string filePath = string.Empty;

                        if (espPdf)
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        }
                        else if (espImg)
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        }

                        bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                        if (!existeCarpeta)
                        {
                            System.IO.Directory.CreateDirectory(carpetaBase);
                        }

                        this.vFileName = rutaParcial;
                        file.SaveAs(filePath);
                    }
                }
            }

            return respuesta;
        }
        catch (HttpRequestValidationException)
        {
            throw new Exception("Se detectó un posible archivo peligroso.");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Cerrar la ventana modal del archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarPanelAr_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    /// <summary>
    /// Control de los datos recibidos por el gridview documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistorico_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("1/01/0001") || e.Row.Cells[3].Text.Equals("01/01/1900") || e.Row.Cells[3].Text.Equals((new DateTime()).ToString()))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (e.Row.Cells[i].Controls.Count > 0)
                {
                    ((LinkButton)e.Row.Cells[i].Controls[0]).TabIndex = -1;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[1].Text.Equals("01/01/0001")
                || e.Row.Cells[1].Text.Equals("01/01/1900") || e.Row.Cells[1].Text.Equals((new DateTime()).ToString()))
            {
                e.Row.Cells[1].Text = "";
            }

            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("01/01/0001")
                || e.Row.Cells[3].Text.Equals("01/01/1900") || e.Row.Cells[3].Text.Equals(new DateTime().ToString()))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    public string PrimeraLetraMayuscula(string Palabra)
    {
        if (Palabra != null)
        {
            if (Palabra.Length > 1)
            {
                Palabra = Palabra.ToLower();
                Palabra = string.Concat(Palabra.Substring(0, 1).ToUpper(), Palabra.Substring(1, Palabra.Length - 1));
            }
            else if (Palabra.Length == 1)
            {
                Palabra = Palabra.ToUpper();
            }
        }
        return Palabra;
    }
}