﻿//-----------------------------------------------------------------------
// <copyright file="Page_Mostrencos_RegistrarParticipacionEconomica_Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Page_Mostrencos_RegistrarParticipacionEconomica_Edit.aspx.cs</summary>
// <author>INGENIAN</author>
// <date>19/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarParticipacionEconomica_Edit : GeneralWeb
{
    #region VARIABLES

    /// <summary>
    /// Variable tipo ToolBar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Codigo identificador de la denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Contrato
    /// </summary>
    private int IdContrato;

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    protected void btnInfoDenuncia_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.Visible = true;
    }

    protected void btnCerrarInmuebleModal_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.Visible = false;
    }

    #endregion

    #region EVENTOS  

    /// <summary>
    /// Evento Pre Init
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro EvenArg</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Load de la página
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }

    /// <summary>
    /// Link para visualizar el documento
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void lnkVerArchivo_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Evento changing del gridview documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void grvDocumentacionRecibida_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// Evento page changing del gridview historico de la denuncia
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void grvHistoricoDenuncia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    ///  Paginación de la grilla Ordenes de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void grvRelacionOrdenesPago_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// Paginación de la grilla historico de la documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void gvwHistorico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwHistorico.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwHistorico"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            gvwHistorico.DataSource = vList;
            gvwHistorico.DataBind();
        }
        else
        {
            ViewState["SortedgvwHistorico"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataBind();
        }
    }

    /// <summary>
    /// Evento de los botones de la grilla Ordenes de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void grvRelacionOrdenesPago_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Eliminar")
            {
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                OrdenesPago vdo = new OrdenesPago();
                vdo.IdOrdenesPago = Convert.ToInt32(gvr.Cells[0].Text);
                SetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago", vdo.IdOrdenesPago);

                List<OrdenesPago> vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
                List<OrdenesPago> vListaOrdenesDePagoEliminar = (List<OrdenesPago>)this.ViewState["OrdenesDePagoEliminar"];

                foreach (var item in vListaOrdenesDePago.ToList())
                {
                    if (item.IdOrdenesPago == vdo.IdOrdenesPago)
                    {
                        vListaOrdenesDePagoEliminar.Add(item);
                        vListaOrdenesDePago.Remove(item);
                    }
                }
                this.txtTotalaPagar.Text = vListaOrdenesDePago.Sum(item => item.ValorAPagar).ToString("C2");
                this.txtTotalPagado.Text = vListaOrdenesDePago.Sum(item => item.ValorPagado).ToString("C2");

                this.ViewState["OrdenesDePagoEliminar"] = vListaOrdenesDePagoEliminar;
                this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenesDePago;
                grvRelacionOrdenesPago.DataSource = vListaOrdenesDePago;
                grvRelacionOrdenesPago.DataBind();
            }
            else if (e.CommandName == "Editar")
            {
                this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = false;
                this.lblCampoRequeridoFechaResolucion.Visible = false;
                this.lblCampoRequeridoNumeroResolucionPago.Visible = false;
                this.lblValidacionComprobanteDePago.Visible = false;
                this.reqValorPagado.Visible = false;
                this.reqValorAPagar.Visible = false;
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                OrdenesPago vdo = new OrdenesPago();
                vdo.IdOrdenesPago = Convert.ToInt32(gvr.Cells[0].Text);
                SetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago", vdo.IdOrdenesPago);

                if (vdo.IdOrdenesPago != 0)
                {
                    if (!gvr.Cells[1].Text.Equals(string.Empty))
                    {
                        this.FechaSolicitudLiquidacion.Date = Convert.ToDateTime(gvr.Cells[1].Text);
                    }
                    else
                    {
                        this.FechaSolicitudLiquidacion.InitNull = true;
                    }

                    if (!gvr.Cells[3].Text.Equals(string.Empty))
                    {
                        this.FechaResolucionPago.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                    }
                    else
                    {
                        this.FechaResolucionPago.InitNull = true;
                    }

                    this.txtNumeroResolucionPago.Text = Convert.ToString(gvr.Cells[2].Text);

                    this.txtValorPagar.Text = Convert.ToString(gvr.Cells[4].Text).ToString();
                    this.txtNumeroComprobantePago.Text = Convert.ToString(gvr.Cells[5].Text).ToString();
                    this.txtValorPagado.Text = Convert.ToString(gvr.Cells[6].Text);
                    this.rbtModalidadPago.SelectedValue = Convert.ToString(gvr.Cells[7].Text);
                    this.btnAplicarPago.Enabled = true;
                    this.pnlRelacionOrdenesPago.Enabled = true;
                    this.FechaSolicitudLiquidacion.Enabled = true;
                    this.FechaResolucionPago.Enabled = true;
                    this.txtValorPagar.Enabled = true;
                }

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    /// <summary>
    /// Evento de los botones de la grilla historico de la documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void gvwHistorico_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosRecibidos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosRecibidosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentacionRecibidaEliminar"];

        if (e.CommandName.Equals("Ver"))
        {
            this.btnDescargar.Visible = false;
            bool vVerArchivo = false;
            this.pnlArchivo.Visible = true;
            string vRuta = string.Empty;
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwHistorico.DataKeys[vRow].Value);
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            DocumentosSolicitadosDenunciaBien vArchivo = vList.Find(x => x.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado);

            this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
            this.hfNombreArchivo.Value = vArchivo.NombreArchivo.ToString();

            vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;

            if (!vVerArchivo)
            {
                vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }

            if (vVerArchivo)
            {
                this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vArchivo.RutaArchivo.ToString();
                if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }

        }
        if (e.CommandName == "Eliminar")
        {
            pnlArchivo.CssClass = "popuphIstorico hidden";
            GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
            vdo.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(gvr.Cells[0].Text);
            vdo.IdEstadoDocumento = 6;
            vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(vdo);
            this.CargarGrillaHistoricoDocumentacion();
        }
        else if (e.CommandName == "Editar")
        {
            this.btnAdicionarDocumento.Visible = false;
            this.btnAplicar.Visible = true;
            GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

            int vIdDocumentoSolicitado = Convert.ToInt32(gvr.Cells[0].Text);
            DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
            //vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;

            if (vDocumentoSolicitado.IdEstadoDocumento == 2)
            {
                vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = true;
            }
            else
            {
                vListaDocumentosRecibidos.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
            }

            this.hfIdDocumentoSolocitado.Value = vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();

            int vIdEstado = Convert.ToInt32(gvr.Cells[5].Text);
            if (vIdEstado != 0) // Estado mayor a solicitado
            {
                this.ddlTipoDocumento.SelectedValue = Convert.ToString(gvr.Cells[1].Text);

                if ((gvr.Cells[3].Text).Equals(""))
                {
                    this.FechaSolicitud.InitNull = true;
                }
                else
                {
                    this.FechaSolicitud.Date = Convert.ToDateTime(gvr.Cells[3].Text);
                }

                if ((gvr.Cells[7].Text).Equals("&nbsp;"))
                {
                    this.FechaRecibido.Date = DateTime.Now;
                }
                else
                {
                    this.FechaRecibido.Date = Convert.ToDateTime(gvr.Cells[7].Text);
                }
                this.lblNombreArchivo.Text = Convert.ToString(gvr.Cells[9].Text);
                this.lblNombreArchivo.Visible = true;
                this.txtObservacionesSolicitado.Text = HttpContext.Current.Server.HtmlDecode(Convert.ToString(gvr.Cells[4].Text));
                this.txtObservacionesRecibido.Text = (gvr.Cells[10].Text).Equals("&nbsp;") ? null : gvr.Cells[10].Text;
                this.btnAdicionarDocumento.Enabled = true;
                this.pnlDocumentacionRecibida.Enabled = true;
                this.FechaRecibido.Enabled = true;

                foreach (ListItem item in this.rbtEstadoDocumento.Items)
                {
                    if (vIdEstado == Convert.ToInt32(item.Value))
                    {
                        item.Selected = true;
                        this.ObliFeRe.Visible = true;
                        this.ObliNoAr.Visible = true;
                        this.ObliObDoRe.Visible = true;
                        break;
                    }
                }

                SetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia", Convert.ToInt32(gvr.Cells[0].Text));
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Orden de items en la grilla Historico de documentación
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void gvwHistorico_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwHistorico"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwHistorico"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreEstado"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                    }
                }
            }
        }

        if (e.SortExpression.Equals("FechaRecibidoGrilla"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("NombreArchivo"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
            }
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwHistorico"] = vResult;
        this.gvwHistorico.DataSource = vResult;
        this.gvwHistorico.DataBind();
    }

    /// <summary>
    /// Orden de items en la grilla Ordenes de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void grvRelacionOrdenesPago_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    /// <summary>
    /// Evento de selección del radiobuton Modalidad de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void rbtModalidadPago_TextChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Selección de radio buton Estado del documento
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ObliFeRe.Visible = true;
        this.ObliNoAr.Visible = true;
        this.ObliObDoRe.Visible = true;
    }


    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.grvHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.grvHistoricoDenuncia.DataSource = vList;
                this.grvHistoricoDenuncia.DataBind();
            }
            else
            {
                this.CargarDatosHistoricoDenuncia();
            }
            this.pnlgrvHistoricoDenuncia.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }
                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }
                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }
                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }
                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }
                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        this.pnlgrvHistoricoDenuncia.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.grvHistoricoDenuncia.DataSource = vResult;
        this.grvHistoricoDenuncia.DataBind();
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Actualizar la lista de relación de pagos en el gridview
    /// </summary>
    public void ActualizarListaDeRelacionDePagos()
    {
        try
        {
            toolBar.LipiarMensajeError();
            var OrdenDePago = GetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago").GetType().Name; ;
            if (OrdenDePago != "String")
            {
                if (this.ValidarCamposEnRelacionOrdenesDePago() == false)
                {
                    if (this.ValidarValorDeCamposRelacionOrdenesDePago() == false)
                    {
                        int IdOrdenDePago = (int)GetSessionParameter("Mostrencos.RelacionParticipacionEconomica.vIdRelacionDePago");
                        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                        List<OrdenesPago> vListaOrdenDePago = this.vMostrencosService.ConsultarOrdenesPagoParticipacionPorIdOrdenPago(IdOrdenDePago);
                        //ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenDePago;
                        List<OrdenesPago> vListaOrdenesDePagoEditar = (List<OrdenesPago>)this.ViewState["OrdenesDePagoEditar"];
                        List<OrdenesPago> vListaOrdenesPagoDenunciaBien = this.vMostrencosService.ConsultarOrdenesPagoDenunciaBien(vIdDenunciaBien);
                        var ExisteNumeroResolucion = vListaOrdenesPagoDenunciaBien.Find(x => x.NumeroResolucion == Convert.ToInt32(this.txtNumeroResolucionPago.Text));
                        if (vListaOrdenDePago == null)
                        {
                            vListaOrdenDePago = new List<OrdenesPago>();
                        }
                        if (vListaOrdenDePago.Count() > 0)
                        {
                            foreach (OrdenesPago item in vListaOrdenDePago.Where(x => x.IdOrdenesPago == IdOrdenDePago))
                            {
                                int itemDos = vListaOrdenesPagoDenunciaBien.Where(a => a.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text) && a.IdOrdenesPago != IdOrdenDePago).Count();

                                int validateLisTemp = vListaOrdenesDePagoEditar.Where(b => b.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text) 
                                && b.IdOrdenesPago == IdOrdenDePago && b.FechaSolicitudLiquidacion == this.FechaSolicitudLiquidacion.Date
                                && b.FechaResolucionPago == this.FechaResolucionPago.Date && b.NumeroComprobantePago == Convert.ToDecimal(this.txtNumeroComprobantePago.Text)
                                && b.ValorAPagar == Convert.ToDecimal(this.txtValorPagar.Text) && b.ValorPagado == Convert.ToDecimal(this.txtValorPagado.Text)).Count();

                                int validateLisTempDos = vListaOrdenesDePagoEditar.Where(b => b.NumeroResolucion == Convert.ToDecimal(this.txtNumeroResolucionPago.Text)
                                && b.IdOrdenesPago == IdOrdenDePago || b.FechaSolicitudLiquidacion != this.FechaSolicitudLiquidacion.Date
                                || b.FechaResolucionPago != this.FechaResolucionPago.Date || b.NumeroComprobantePago != Convert.ToDecimal(this.txtNumeroComprobantePago.Text)
                                || b.ValorAPagar != Convert.ToDecimal(this.txtValorPagar.Text) || b.ValorPagado != Convert.ToDecimal(this.txtValorPagado.Text)).Count();

                                if (itemDos > 0)
                                {
                                    this.toolBar.MostrarMensajeError("El registro ya existe");
                                }
                                else
                                {
                                    if (validateLisTemp == 0)
                                    {
                                        if (validateLisTempDos > 0)
                                        {
                                            item.FechaSolicitudLiquidacion = Convert.ToDateTime(this.FechaSolicitudLiquidacion.Date);
                                            item.NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucionPago.Text);
                                            item.FechaResolucionPago = Convert.ToDateTime(this.FechaResolucionPago.Date);
                                            item.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                                            item.ValorPagado = Convert.ToDecimal(this.txtValorPagado.Text);
                                            item.ValorAPagar = Convert.ToDecimal(this.txtValorPagar.Text);
                                            vListaOrdenesDePagoEditar = new List<OrdenesPago> ();
                                            vListaOrdenesDePagoEditar.Add(item);
                                            this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenDePago;
                                            this.ViewState["OrdenesDePagoEditar"] = vListaOrdenesDePagoEditar;
                                            this.txtTotalaPagar.Text = vListaOrdenesDePagoEditar.Sum(a => a.ValorAPagar).ToString("C2");
                                            this.txtTotalPagado.Text = vListaOrdenesDePagoEditar.Sum(a => a.ValorPagado).ToString("C2");
                                            this.grvRelacionOrdenesPago.DataSource = vListaOrdenesDePagoEditar;
                                            this.grvRelacionOrdenesPago.DataBind();
                                        }
                                        else
                                        {
                                            item.FechaSolicitudLiquidacion = Convert.ToDateTime(this.FechaSolicitudLiquidacion.Date);
                                            item.NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucionPago.Text);
                                            item.FechaResolucionPago = Convert.ToDateTime(this.FechaResolucionPago.Date);
                                            item.NumeroComprobantePago = Convert.ToDecimal(this.txtNumeroComprobantePago.Text);
                                            item.ValorPagado = Convert.ToDecimal(this.txtValorPagado.Text);
                                            item.ValorAPagar = Convert.ToDecimal(this.txtValorPagar.Text);
                                            vListaOrdenesDePagoEditar.Add(item);
                                            this.ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenDePago;
                                            this.ViewState["OrdenesDePagoEditar"] = vListaOrdenesDePagoEditar;
                                            this.txtTotalaPagar.Text = vListaOrdenesDePagoEditar.Sum(a => a.ValorAPagar).ToString("C2");
                                            this.txtTotalPagado.Text = vListaOrdenesDePagoEditar.Sum(a => a.ValorPagado).ToString("C2");
                                            this.grvRelacionOrdenesPago.DataSource = vListaOrdenesDePagoEditar;
                                            this.grvRelacionOrdenesPago.DataBind();
                                        }
                                        
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public int ActualizarRelacionDePago()
    {
        try
        {
            //bool response = false;
            int cont = 0;
            int respuesta = 0;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

            //Consultar Relaciones de Pago cargadas en la grilla
            List<OrdenesPago> vListaOrdenesDePago = new List<OrdenesPago>();
            vListaOrdenesDePago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];
            DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(vIdDenunciaBien);

            if (vListaOrdenesDePago.Count() > 0)
            {
                foreach (OrdenesPago item in vListaOrdenesDePago)
                {
                    OrdenesPago pOrdenDePago = new OrdenesPago();
                    pOrdenDePago.IdOrdenesPago = item.IdOrdenesPago;
                    pOrdenDePago.IdModalidadPago.IdModalidadPago = item.IdModalidadPago.IdModalidadPago;
                    pOrdenDePago.IdDenunciaBien = vIdDenunciaBien;
                    pOrdenDePago.FechaSolicitudLiquidacion = item.FechaSolicitudLiquidacion;
                    pOrdenDePago.NumeroResolucion = item.NumeroResolucion;
                    pOrdenDePago.FechaResolucionPago = item.FechaResolucionPago;
                    pOrdenDePago.ValorAPagar = item.ValorAPagar;
                    pOrdenDePago.NumeroComprobantePago = item.NumeroComprobantePago;
                    pOrdenDePago.ValorPagado = item.ValorPagado;
                    pOrdenDePago.Estado = true;
                    pOrdenDePago.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pOrdenDePago.FechaCrea = DateTime.Now;
                    pOrdenDePago.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pOrdenDePago.FechaModifica = DateTime.Now;
                    respuesta = this.vMostrencosService.ActualizarOrdenesDePago(pOrdenDePago);

                    decimal valorDenuncia = 0;

                    if (vlstDenunciaBienTitulo != null)
                    {
                        valorDenuncia = Convert.ToDecimal(vlstDenunciaBienTitulo.ValorDenuncia);
                    }
                    else
                    {
                        valorDenuncia = 0;
                    }

                    decimal varlorTotalAPagar = Convert.ToDecimal(vListaOrdenesDePago.Sum(x => x.ValorAPagar));
                    decimal valorTotalPagado = Convert.ToDecimal(vListaOrdenesDePago.Sum(x => x.ValorPagado));

                    if (respuesta > 0)
                    {
                        if (item.FechaSolicitudLiquidacion != new DateTime())
                        {
                            this.InsertarHistoricoOrdenesDePago(19, 23, 34);
                        }

                        if (item.IdModalidadPago.IdModalidadPago != 2 || item.IdModalidadPago.IdModalidadPago != 3)
                        {
                            this.InsertarHistoricoOrdenesDePago(19, 23, 35);
                        }

                        if (pOrdenDePago.ValorAPagar == pOrdenDePago.ValorPagado || pOrdenDePago.ValorAPagar == valorTotalPagado)
                        {
                            int Actualizado = Convert.ToInt32(this.vMostrencosService.EditarDenunciaBienFase(vIdDenunciaBien));
                        }
                    }
                    cont += 1;
                }
            }

            return respuesta;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return 0;
        }
    }

    /// <summary>
    /// Deshabilitar Paneles y campos
    /// </summary>
    protected void DeshabilitarPanelesyCampos()
    {
        this.txtDescripcion.Enabled = false;
        this.pnlInformacionPagoParticipacion.Enabled = false;
        this.pnlRelacionOrdenesPago.Enabled = false;
        this.grvRelacionOrdenesPago.Enabled = false;
        this.gvwHistorico.Enabled = false;
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlDocumentacionRecibida.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = false;
        this.FechaRecibido.Enabled = false;
        this.FechaSolicitud.Enabled = false;
        this.fulArchivoRecibido.Enabled = false;
    }

    /// <summary>
    /// Editar registro de la Relación Orden de Pago
    /// </summary>
    /// <returns>Resultado de la operación</returns>
    public int EditarOrdenRelacionDePago()
    {
        int respuesta = 0;
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

        //Consultar Relaciones de Pago cargadas en la grilla
        List<OrdenesPago> vListaOrdenesDePagoEditar = new List<OrdenesPago>();
        vListaOrdenesDePagoEditar = (List<OrdenesPago>)this.ViewState["OrdenesDePagoEditar"];
        DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(vIdDenunciaBien);

        if (vListaOrdenesDePagoEditar.Count() > 0)
        {
            foreach (OrdenesPago item in vListaOrdenesDePagoEditar)
            {
                OrdenesPago pOrdenDePago = new OrdenesPago();
                pOrdenDePago.IdOrdenesPago = item.IdOrdenesPago;
                pOrdenDePago.IdModalidadPago.IdModalidadPago = item.IdModalidadPago.IdModalidadPago;
                pOrdenDePago.IdDenunciaBien = vIdDenunciaBien;
                pOrdenDePago.FechaSolicitudLiquidacion = item.FechaSolicitudLiquidacion;
                pOrdenDePago.NumeroResolucion = item.NumeroResolucion;
                pOrdenDePago.FechaResolucionPago = item.FechaResolucionPago;
                pOrdenDePago.ValorAPagar = item.ValorAPagar;
                pOrdenDePago.NumeroComprobantePago = item.NumeroComprobantePago;
                pOrdenDePago.ValorPagado = item.ValorPagado;
                pOrdenDePago.Estado = true;
                pOrdenDePago.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pOrdenDePago.FechaCrea = DateTime.Now;
                pOrdenDePago.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pOrdenDePago.FechaModifica = DateTime.Now;
                this.InformacionAudioria(pOrdenDePago, this.PageName, SolutionPage.Edit);
                respuesta = this.vMostrencosService.ActualizarOrdenesDePago(pOrdenDePago);
            }
        }

        return respuesta;
    }

    /// <summary>
    /// Eliminar registro de la Relación Orden de Pago
    /// </summary>
    /// <param name="Estado">Parámetro Estado</param>
    /// <returns>Resultado de la operación</returns>
    public int EliminarOrdenRelacionDePago(bool Estado)
    {
        int respuesta = 0;
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

        //Consultar Relaciones de Pago cargadas en la grilla
        List<OrdenesPago> vListaOrdenesDePagoEliminar = new List<OrdenesPago>();
        vListaOrdenesDePagoEliminar = (List<OrdenesPago>)this.ViewState["OrdenesDePagoEliminar"];
        DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(vIdDenunciaBien);

        if (vListaOrdenesDePagoEliminar.Count() > 0)
        {
            foreach (OrdenesPago item in vListaOrdenesDePagoEliminar)
            {
                OrdenesPago pOrdenDePago = new OrdenesPago();
                pOrdenDePago.IdOrdenesPago = item.IdOrdenesPago;
                pOrdenDePago.IdModalidadPago.IdModalidadPago = item.IdModalidadPago.IdModalidadPago;
                pOrdenDePago.IdDenunciaBien = vIdDenunciaBien;
                pOrdenDePago.FechaSolicitudLiquidacion = item.FechaSolicitudLiquidacion;
                pOrdenDePago.NumeroResolucion = item.NumeroResolucion;
                pOrdenDePago.FechaResolucionPago = item.FechaResolucionPago;
                pOrdenDePago.ValorAPagar = item.ValorAPagar;
                pOrdenDePago.NumeroComprobantePago = item.NumeroComprobantePago;
                pOrdenDePago.ValorPagado = item.ValorPagado;
                pOrdenDePago.Estado = Estado;
                pOrdenDePago.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pOrdenDePago.FechaCrea = DateTime.Now;
                pOrdenDePago.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pOrdenDePago.FechaModifica = DateTime.Now;
                this.InformacionAudioria(pOrdenDePago, this.PageName, SolutionPage.Edit);
                respuesta = this.vMostrencosService.ActualizarOrdenesDePago(pOrdenDePago);
            }
        }

        return respuesta;
    }

    /// <summary>
    /// Limpiar panel de Relación de Pago
    /// </summary>
    private void LimpiarPanelRelacionDePago()
    {
        this.FechaSolicitudLiquidacion.InitNull = true;
        this.txtNumeroResolucionPago.Text = "";
        this.FechaResolucionPago.InitNull = true;
        this.txtValorPagar.Text = "";
        this.txtNumeroComprobantePago.Text = "";
        this.txtValorPagado.Text = "";
        this.FechaSolicitudLiquidacion.Enabled = true;
        this.FechaResolucionPago.Enabled = true;
    }

    /// <summary>
    /// Metodo inicial para cargar datos en el list
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Pago de Participación Económica");

            this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            this.toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Insertar Histotico de Ordenes de Pago
    /// </summary>
    public int InsertarHistoricoOrdenesDePago(int Fase, int Actuacion, int Accion)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        int respuesta = 0;
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.IdFase = Fase; //// 19 Pago de participación económica
        pHistoricoEstadosDenunciaBien.IdActuacion = Actuacion; //// 23 Pago
        pHistoricoEstadosDenunciaBien.IdAccion = Accion;  //// 34 solicitar liquidacion - 35 Registro de pagos
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 1; //// 1 Registrado
        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, SolutionPage.Add);
        respuesta = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBienGeneral(pHistoricoEstadosDenunciaBien);

        return respuesta;
    }

    /// <summary>
    /// Metodo que carga la grilla historico e la denuncia
    /// </summary>
    public void CargarDatosHistoricoDenuncia()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.grvHistoricoDenuncia.DataSource = vHistorico;
            this.grvHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo para cargar los datos de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.pnlArchivo.Visible = false;
            this.toolBar.LipiarMensajeError();
            this.toolBar.MostrarBotonEditar(false);
            this.toolBar.MostrarBotonNuevo(false);
            this.ConfiguracionInicialPanelesCampos();
            this.ddlTipoDocumento.Focus();
            this.ViewState["SortedgvwHistorico"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.ViewState["DocumentacionRecibidaEliminar"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.ViewState["SortedgrvRelacionOrdenesPago"] = new List<OrdenesPago>();
            this.ViewState["OrdenesDePagoEliminar"] = new List<OrdenesPago>();
            this.ViewState["OrdenesDePagoEditar"] = new List<OrdenesPago>();
            this.CargarDatosHistoricoDenuncia();
            this.CargarHistoricoRelacionDePagos();
            this.CargarGrillaHistoricoDocumentacion();

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);
            if (vIdDenunciaBien != 0)
            {
                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != (new DateTime()).ToString() ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != (new DateTime()).ToString()) && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                    this.FechaRecibido.Date = Convert.ToDateTime(DateTime.Now);

                    // Se asigna el valor de la denuncia
                    if (vlstDenunciaBienTitulo.Count() > 0)
                    {
                        string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                        this.TextValordelaDenuncia.Text = valorDenuncia;
                    }
                    else
                    {
                        this.TextValordelaDenuncia.Text = "0,00";
                    }

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }

                List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
                TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
                vTipoDocumento.NombreTipoDocumento = "SELECCIONE";
                vTipoDocumento.Estado = "ACTIVO";
                vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
                vlstTipoDocumentosBien.Add(vTipoDocumento);
                vlstTipoDocumentosBien = vlstTipoDocumentosBien.Where(p => p.Estado == "ACTIVO").ToList();
                vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
                this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.ddlTipoDocumento.DataBind();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar a la grilla los datos de historico de documentación
    /// </summary>
    public void CargarGrillaHistoricoDocumentacion()
    {
        try
        {
            ////Se llena la grilla con los todos documentos de la denuncia (Todos los estados)
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DocumentosSolicitadosDenunciaBien> vListaArchivos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            ViewState["SortedgvwHistorico"] = vListaArchivos;
            this.gvwHistorico.DataSource = vListaArchivos;
            this.gvwHistorico.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Metodo para registros
    /// </summary>
    private void CargarHistoricoRelacionDePagos()
    {
        try
        {
            toolBar.LipiarMensajeError();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));

            int vIdOrdenPago = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdOrdenesPago"));
            if (vIdOrdenPago != 0)
            {
                List<OrdenesPago> vListaOrdenDePago = this.vMostrencosService.ConsultarOrdenesPagoParticipacionPorIdOrdenPago(vIdOrdenPago);
                ViewState["SortedgrvRelacionOrdenesPago"] = vListaOrdenDePago;

                this.txtTotalaPagar.Text = vListaOrdenDePago.Sum(item => item.ValorAPagar).ToString("C2");
                this.txtTotalPagado.Text = vListaOrdenDePago.Sum(item => item.ValorPagado).ToString("C2");

                if (vListaOrdenDePago.Any(x => x.IdModalidadPago.IdModalidadPago == 2))
                {
                    this.rbtModalidadPago.SelectedValue = "2";
                }

                if (vListaOrdenDePago.Any(x => x.IdModalidadPago.IdModalidadPago == 3))
                {
                    this.rbtModalidadPago.SelectedValue = "3";
                }

                this.grvRelacionOrdenesPago.DataSource = vListaOrdenDePago;
                this.grvRelacionOrdenesPago.DataBind();
            }



        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    /// <summary>
    /// Lista de Tipos de Documento
    /// </summary>
    public void CargarListaTipoDocumento()
    {
        List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
        TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
        vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
        vTipoDocumento.NombreTipoDocumento = "SELECCIONE";
        vTipoDocumento.Estado = "ACTIVO";
        vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
        vlstTipoDocumentosBien.Add(vTipoDocumento);
        vlstTipoDocumentosBien = vlstTipoDocumentosBien.Where(p => p.Estado == "ACTIVO").ToList();
        vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
        this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
        this.ddlTipoDocumento.SelectedValue = "-1";
        this.ddlTipoDocumento.DataBind();
    }

    /// <summary>
    /// Control de paneles en Editar
    /// </summary>
    public void ConfiguracionInicialPanelesCampos()
    {
        this.pnlRelacionOrdenesPago.Enabled = true;
        this.pnlDocumentacionRecibida.Enabled = true;
        this.btnAplicarPago.Visible = true;
        this.pnlDocumentacionSolicitada.Enabled = true;

        this.btnAplicar.Visible = false;
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlgrvHistoricoDenuncia.Visible = false;
        this.pnlInformacionPagoParticipacion.Enabled = false;
        this.txtDescripcion.Enabled = false;



        grvRelacionOrdenesPago.Columns[grvRelacionOrdenesPago.Columns.Count - 1].Visible = true;
        grvRelacionOrdenesPago.Columns[grvRelacionOrdenesPago.Columns.Count - 2].Visible = true;
    }

    /// <summary>
    /// Guardars the archivos.
    /// </summary>
    /// <param name="pIdDenunciaBien">The p identifier denuncia bien.</param>
    /// <param name="pPosition">The p position.</param>
    /// <returns></returns>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        try
        {
            bool respuesta = true;
            HttpFileCollection files = Request.Files;

            if (files.Count > 0)
            {
                bool espPdf = this.fulArchivoRecibido.FileName.Contains(".pdf");
                bool espImg = this.fulArchivoRecibido.FileName.Contains(".jpg");
                string NombreArchivo = string.Empty;

                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                string extImg = ".JPG".ToLower();

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);

                int tamanoNombre = this.fulArchivoRecibido.FileName.Length;
                NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoNombre - 4));

                if (NombreArchivo.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }

                if (extFile != extPdf && extFile != extImg)
                {
                    this.toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF ó JPG");
                    respuesta = false;
                }

                if (vtamArchivo > 4096)
                {
                    this.toolBar.MostrarMensajeError("El tamaño máximo del archivo es de 4 MB");
                    respuesta = false;
                }

                HttpPostedFile file = files[pPosition];

                if (file.ContentLength > 0)
                {
                    if (respuesta)
                    {
                        string rutaParcial = string.Empty;
                        string carpetaBase = string.Empty;
                        string filePath = string.Empty;

                        if (espPdf)
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        }
                        else if (espImg)
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        }

                        bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                        if (!existeCarpeta)
                        {
                            System.IO.Directory.CreateDirectory(carpetaBase);
                        }

                        this.vFileName = rutaParcial;
                        file.SaveAs(filePath);
                    }
                }
            }

            return respuesta;
        }
        catch (HttpRequestValidationException)
        {
            throw new Exception("Se detectó un posible archivo peligroso.");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    
    /// <summary>
    /// Validar Relación de Campos Si se resgistra el Número de resolución
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarCamposEnRelacionOrdenesDePago()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            if (this.rbtModalidadPago.SelectedValue == "")
            {
                this.lblCampoRequeridoModalidadPago.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoModalidadPago.Visible = false;
                vEsrequerido = false;
            }

            try
            {
                if (this.FechaSolicitudLiquidacion.Date.ToString().Equals(new DateTime().ToString()) || this.FechaSolicitudLiquidacion.Date.ToString().Equals("01/01/1900 12:00:00 a. m.")
                    || this.FechaSolicitudLiquidacion.Date.ToString().Equals("01/01/0001 00:00:00") || this.FechaSolicitudLiquidacion.Date.ToString().Equals("1/01/1900 12:00:00 a. m.")) 
                {
                    this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaSolicitudLiquidacion.Visible = false;
                    vEsrequerido = false;
                }
            }
            catch (Exception)
            {
                this.CustomValidator2.IsValid = false;
                vEsrequerido = true;
                count = count + 1;
            }

            try
            {
                if (this.FechaResolucionPago.Date.ToString().Equals(new DateTime().ToString()) || this.FechaResolucionPago.Date.ToString().Equals("01/01/1900 12:00:00 a. m.")
                    || this.FechaResolucionPago.Date.ToString().Equals("01/01/0001 00:00:00") || this.FechaResolucionPago.Date.ToString().Equals("1/01/1900 12:00:00 a. m."))
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoFechaResolucion.Visible = false;
                    vEsrequerido = false;
                }
            }
            catch (Exception)
            {
                this.CustomValidator3.IsValid = false;
                vEsrequerido = true;
                count = count + 1;
            }



            if (this.txtNumeroResolucionPago.Text.Equals(string.Empty))
            {
                this.lblCampoRequeridoNumeroResolucionPago.Visible = true;
                count = count + 1;
                vEsrequerido = false;
            }
            else
            {
                this.lblCampoRequeridoNumeroResolucionPago.Visible = false;
            }

            if (this.txtValorPagar.Text.Equals(string.Empty))
            {
                this.reqValorAPagar.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.reqValorAPagar.Visible = false;
            }

            if (this.txtNumeroComprobantePago.Text.Equals(string.Empty))
            {
                this.lblValidacionComprobanteDePago.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.lblValidacionComprobanteDePago.Visible = false;
            }

            if (this.txtValorPagado.Text.Equals(string.Empty))
            {
                this.reqValorPagado.Visible = true;
                vEsrequerido = false;
                count = count + 1;
            }
            else
            {
                this.reqValorPagado.Visible = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido = false;
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar documentacion solicitada y recibida
    /// </summary>
    /// <returns>Resultado de la validación</returns>
    public bool ValidarDocumentacionSolicitadayRecibida()
    {
        Boolean vPuedeGuardar = true;
        int count = 0;
        int vIdDocumentoDenuncia = (int)GetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
        if (vIdDocumentoDenuncia != 0)
        {
            if (this.rbtEstadoDocumento.SelectedValue != "")
            {
                //Validar Tipo Documento
                if (this.ddlTipoDocumento.SelectedValue == "-1")
                {
                    this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                    vPuedeGuardar = false;
                }
                //Validar Fecha Solicitud
                if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblrqfFechaSolicitud.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    this.lblrqfFechaSolicitud.Visible = false;
                    vPuedeGuardar = false;
                }

                //Validar Campo Fecha Recibido
                if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblrqfFechaRecibido.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    this.lblrqfFechaRecibido.Visible = false;
                    vPuedeGuardar = false;

                    //Validar Campo Fecha Recibido
                    if (this.FechaRecibido.Date > DateTime.Now)
                    {
                        this.lblValidacionFechaRecibidoMaxima.Visible = true;
                        vPuedeGuardar = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblValidacionFechaRecibidoMaxima.Visible = false;
                        vPuedeGuardar = false;
                    }

                    //Validar Campo Fecha Recibido menor a fecha de solicitud
                    if (this.FechaRecibido.Date < this.FechaSolicitud.Date)
                    {
                        this.lblRecibidoMenorSolicitud.Visible = true;
                        vPuedeGuardar = true;
                        count = count + 1;
                    }
                    else
                    {
                        this.lblRecibidoMenorSolicitud.Visible = false;
                        vPuedeGuardar = false;
                    }
                }

                ////Validar Observaciones Documento Recibida
                if (this.txtObservacionesRecibido.Text == "")
                {
                    this.lblRqfObservacionesRecibido.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    vPuedeGuardar = false;
                    this.lblRqfObservacionesRecibido.Visible = false;
                }

                ////Validar archivo cargardo
                if (!fulArchivoRecibido.HasFile)
                {
                    this.lblRqfNombreArchivo.Visible = true;
                    vPuedeGuardar = true;
                    count = count + 1;
                }
                else
                {
                    vPuedeGuardar = false;
                    this.lblRqfNombreArchivo.Visible = false;
                }
                //if (count > 0)
                //{
                //    vPuedeGuardar = true;
                //}
                //else
                //{
                //    vPuedeGuardar = false;
                //}
            }
            if (this.rbtEstadoDocumento.SelectedValue == "")
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vPuedeGuardar = true;
                count = count + 1;
            }
            else
            {
                vPuedeGuardar = false;
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
            }
            ////Validar Tipo Documento
            //if (this.ddlTipoDocumento.SelectedValue == "-1")
            //{
            //    this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
            //    vPuedeGuardar = true;
            //    count = count + 1;
            //}
            //else
            //{
            //    this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
            //    vPuedeGuardar = false;
            //}
            ////Validar Fecha Solicitud
            //if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            //{
            //    this.lblrqfFechaSolicitud.Visible = true;
            //    vPuedeGuardar = true;
            //    count = count + 1;
            //}
            //else
            //{
            //    this.lblrqfFechaSolicitud.Visible = false;
            //    vPuedeGuardar = false;
            //}
            if (count > 0)
            {
                vPuedeGuardar = true;
            }
            else
            {
                vPuedeGuardar = false;
            }
        }
        return vPuedeGuardar;
    }

    /// <summary>
    /// Validación del valor de la denuncia
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarValorDeCamposRelacionOrdenesDePago()
    {
        int count = 0;
        bool vEsMayor = false;
        string campo = "";

        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            DenunciaMuebleTitulo vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenunciaPorIdDenuncia(vIdDenunciaBien);
            List<OrdenesPago> vListaOrdenesPago = (List<OrdenesPago>)this.ViewState["SortedgrvRelacionOrdenesPago"];

            decimal valorDenuncia = 0;
            decimal valorAPagar;
            decimal valorPagado;
            decimal TotalValorAPagar;
            decimal TotalValorPagado;

            bool convValorAPagar = decimal.TryParse(this.txtValorPagar.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out valorAPagar);
            bool convValorPagado = decimal.TryParse(this.txtValorPagado.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out valorPagado);
            bool convTotalValorAPagar = decimal.TryParse(this.txtTotalaPagar.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out TotalValorAPagar);
            bool convTotalValorPagado = decimal.TryParse(this.txtTotalPagado.Text.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out TotalValorPagado);

            if (vlstDenunciaBienTitulo != null)
            {
                valorDenuncia = vlstDenunciaBienTitulo.ValorDenuncia;
            }
            else
            {
                valorDenuncia = 0;
            }

            if (convValorAPagar)
            {
                if (valorAPagar.ToString() != string.Empty)
                {
                    if (valorAPagar > valorDenuncia)
                    {
                        campo = "valor a pagar";
                        this.toolBar.MostrarMensajeError("El " + campo + " no debe superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }

            }

            if (convValorPagado)
            {
                if (this.txtValorPagado.Text.ToString() != "")
                {
                    if (valorPagado > Convert.ToDecimal(valorDenuncia))
                    {
                        decimal valor3 = decimal.Parse(this.txtValorPagado.Text.ToString(), NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint);
                        campo = "valor pagado";
                        this.toolBar.MostrarMensajeError("El " + campo + " no debe superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }
            }

            if (convTotalValorAPagar)
            {
                if (TotalValorAPagar.ToString() != string.Empty)
                {
                    if (TotalValorAPagar > Convert.ToDecimal(valorDenuncia))
                    {
                        campo = "total a pagar";
                        this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }
            }

            if (convTotalValorPagado)
            {
                if (TotalValorPagado.ToString() != string.Empty)
                {
                    if (TotalValorPagado > Convert.ToDecimal(valorDenuncia))
                    {
                        campo = "total pagado";
                        this.toolBar.MostrarMensajeError("El " + campo + " no puede superar el valor de la denuncia");
                        vEsMayor = true;
                        count = count + 1;
                    }
                    else
                    {
                        vEsMayor = false;
                    }
                }
            }
            else

            if (count > 0)
            {
                vEsMayor = true;
            }
            else
            {
                vEsMayor = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsMayor;
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    #endregion

    #region BOTONES   

    /// <summary>
    /// Botón adicionar documento
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnAdicionarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.MostrarBotonEditar(false);
        if (ValidarDocumentacionSolicitada() == false)
        {
            toolBar.LipiarMensajeError();
            if (AdicionarDocumentosSolicitadosCache())
            {
                CargarGrillaDocumentosSolicitadosYRecibidos();
            }
        }

    }

    /// <summary>
    /// Aplicar cambios en documentación recibida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];

            if (this.ValidarDocumentacionSolicitadayRecibida() == false)
            {
                if (this.GuardarArchivos(vIdDocumentoDenuncia, 0))
                {
                    vListaDocumentosSolicitados.ForEach(item =>
                    {
                        if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoDenuncia)
                        {
                            item.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                            item.FechaSolicitud = new DateTime(FechaSolicitud.Date.Year, FechaSolicitud.Date.Month, FechaSolicitud.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                            item.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text;
                            item.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                            item.NombreTipoDocumento = this.ddlTipoDocumento.SelectedItem.Text;
                            item.NombreEstado = this.rbtEstadoDocumento.SelectedItem.Text;
                            item.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                            item.FechaRecibidoGrilla = item.FechaRecibido.Value.ToString("dd/MM/yyyy");
                            item.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                            item.NombreArchivo = fulArchivoRecibido.FileName;
                            item.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName;
                        }
                    });
                    this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados;
                    this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                    this.gvwHistorico.DataBind();
                    this.txtObservacionesSolicitado.Enabled = true;
                    this.FechaSolicitud.Enabled = true;
                    this.ddlTipoDocumento.Enabled = true;
                    this.ddlTipoDocumento.SelectedValue = "-1";
                    this.txtObservacionesSolicitado.Text = string.Empty;
                    this.FechaSolicitud.InitNull = true;
                    this.lblValidacionFechaRecibidoMaxima.Visible = false;
                    this.rbtEstadoDocumento.ClearSelection();
                    this.FechaRecibido.InitNull = true;
                    this.txtObservacionesRecibido.Text = string.Empty;
                    this.btnAplicar.Visible = false;
                    this.btnAdicionarDocumento.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Botón aplicar Relación de pago
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnAplicarPago_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.ActualizarListaDeRelacionDePagos();
            this.LimpiarPanelRelacionDePago();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cerrar modal de historico de la denuncia
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnCerrarHistorico_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.Visible = false;
    }

    /// <summary>
    /// Metodo redirige a la pantalla list
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Botón descargar documento
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/" + vNombreArchivo));
                vRuta = "../RegistrarCalidadDenunciante/" + vNombreArchivo;
            }
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vNombreArchivo));
                vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vNombreArchivo;
            }
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../ContratoParticipacionEconomica/Archivos/" + vNombreArchivo));
                vRuta = "../ContratoParticipacionEconomica/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo cargar los datos correspondientes a la pantalla de editar
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        //NavigateTo(SolutionPage.Edit);
        this.NavigateTo("../RegistrarParticipacionEconomica/Edit.aspx");
    }

    /// <summary>
    /// Boton Editar
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            int respuestaActualizacion = this.EditarOrdenRelacionDePago();
            int respuestaEliminar = this.EliminarOrdenRelacionDePago(false);
            if (respuestaActualizacion > 0)
            {
                this.DeshabilitarPanelesyCampos();
                this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            }
            if (respuestaEliminar > 0)
            {
                this.DeshabilitarPanelesyCampos();
                this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            }
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
            vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];

            vListaDocumentos.ForEach(item =>
            {
                item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);

                if (item.EsNuevo == true && string.IsNullOrEmpty(item.NombreArchivo))
                {

                    if (item.UsuarioCrea == null)
                    {
                        item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    }

                    if (item.NombreArchivo != null && item.RutaArchivo != null)
                    {
                        this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                    }
                    else

                        this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                }
                else if (item.EsNuevo == true && item.NombreArchivo != "" && item.IdTipoDocumentoSoporteDenuncia == -1)
                {
                    int IdDocumentosSolicitadosDenunciaBien = 0;
                    int IdTipoDocumentoBienDenunciado = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienObtieneId(item, ref IdDocumentosSolicitadosDenunciaBien);
                    item.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                    this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                }
                else if (!string.IsNullOrEmpty(item.NombreArchivo))
                {
                    this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                }
            });
            this.DeshabilitarPanelesyCampos();
            this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            if ((vRegistroDenuncia.IdEstado != 11) && (vRegistroDenuncia.IdEstado != 13) && (vRegistroDenuncia.IdEstado != 19))
            {
                this.toolBar.MostrarBotonNuevo(true);
                this.toolBar.MostrarBotonEditar(true);
                this.toolBar.OcultarBotonGuardar(false);
            }
            else
            {
                this.toolBar.OcultarBotonGuardar(false);
            }
            this.btnInfoDenuncia.Enabled = false;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Abrir el historico de la denuncia
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnHistorico_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.Visible = true;
    }

    /// <summary>
    /// Metodo redirige a la pantalla nuevo
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo redirige a la pantalla detalle de resgistrar documentos bien denuncido
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #endregion    

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwHistorico"] == null)
            {
                int IdCalidadDenunciante = 10;
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(IdCalidadDenunciante);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            }

            if (vListaDocumentosSolicitados.Count > 0)
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                vListaDocumentos = vListaDocumentosSolicitados.Where(x => x.IdEstadoDocumento != 6).ToList();
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwHistorico.DataSource = vListaDocumentos;
                this.gvwHistorico.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Cerrar panel archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarPanelAr_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitadosCache()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue))
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {

                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                vDocumentacionSolicitada.IdTipoDocumentoSoporteDenuncia = -1;
                vDocumentacionSolicitada.IdDenunciaBien = vIdDenunciaBien;
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.NombreEstado = PrimeraLetraMayuscula(vDocumentacionSolicitada.EstadoDocumento.NombreEstadoDocumento);
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentacionSolicitada.EsNuevo = true;
                //this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(vDocumentacionSolicitada);
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwHistorico"] = vListaDocumentacion;
                this.CargarGrillaDocumentacion();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;

                //vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                //vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                //vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                //vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                //vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                //vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                //vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                //vDocumentacionSolicitada.NombreEstado = vDocumentacionSolicitada.EstadoDocumento.NombreEstadoDocumento;
                //vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                //vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                //vDocumentacionSolicitada.EsNuevo = true;
                //vListaDocumentacion.Add(vDocumentacionSolicitada);
                //this.ViewState["SortedgvwHistorico"] = vListaDocumentacion;
                //this.CargarGrillaDocumentacion();
                ////this.gvwHistorico.DataSource = vListaDocumentacion;
                ////this.gvwHistorico.DataBind();
                //this.ddlTipoDocumento.SelectedValue = "-1";
                //this.FechaSolicitud.InitNull = true;
                //this.txtObservacionesSolicitado.Text = string.Empty;
            }

            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Validar Documentación Solicitada
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarDocumentacionSolicitada()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            //Validar Fecha Solicitud
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrqfFechaSolicitud.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblrqfFechaSolicitud.Visible = false;
                vEsrequerido = false;
            }
            //Validar Observaciones Documento Solciitado
            //if (this.txtObservacionesSolicitado.Text == "")
            //{
            //    this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = true;
            //    vEsrequerido = true;
            //    count = count + 1;
            //}
            //else
            //{
            //    this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = false;
            //    vEsrequerido = false;
            //}
            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// GridView Documentos Solicitados
    /// </summary>q
    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (((List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"]).Count == 0)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                this.gvwHistorico.DataBind();
            }
            else
            {
                this.pnlGridDocumentacionRecibida.Visible = false;
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrillaDocumentacion()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados;
                this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
                this.gvwHistorico.DataBind();
            }
            else
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
                List<DocumentosSolicitadosDenunciaBien> vListaArchivos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                ViewState["SortedgvwHistorico"] = vListaArchivos;
                this.gvwHistorico.DataSource = vListaArchivos;
                this.gvwHistorico.DataBind();
                ////this.pnlGridDocumentacionRecibida.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }

        //try
        //{
        //    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
        //    if (vListaDocumentosSolicitados.Count > 0)
        //    {
        //        this.pnlGridDocumentacionRecibida.Visible = true;
        //        vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
        //        this.ViewState["SortedgvwHistorico"] = vListaDocumentosSolicitados;
        //        this.gvwHistorico.DataSource = vListaDocumentosSolicitados;
        //        this.gvwHistorico.DataBind();
        //    }
        //    else
        //    {
        //        this.pnlGridDocumentacionRecibida.Visible = false;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    this.toolBar.MostrarMensajeError(ex.Message);
        //    throw ex;
        //}
    }

    /// <summary>
    /// Control de la data enviada al gridview documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistorico_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("1/01/0001") || e.Row.Cells[3].Text.Equals("01/01/1900") || e.Row.Cells[3].Text.Equals((new DateTime()).ToString()))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (e.Row.Cells[i].Controls.Count > 0)
                {
                    ((LinkButton)e.Row.Cells[i].Controls[0]).TabIndex = -1;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[1].Text.Equals("01/01/0001")
                || e.Row.Cells[1].Text.Equals("01/01/1900") || e.Row.Cells[1].Text.Equals((new DateTime()).ToString()))
            {
                e.Row.Cells[1].Text = "";
            }

            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("01/01/0001")
                || e.Row.Cells[3].Text.Equals("01/01/1900") || e.Row.Cells[3].Text.Equals((new DateTime()).ToString()))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    public string PrimeraLetraMayuscula(string Palabra)
    {
        if (Palabra != null)
        {
            if (Palabra.Length > 1)
            {
                Palabra = Palabra.ToLower();
                Palabra = string.Concat(Palabra.Substring(0, 1).ToUpper(), Palabra.Substring(1, Palabra.Length - 1));
            }
            else if (Palabra.Length == 1)
            {
                Palabra = Palabra.ToUpper();
            }
        }
        return Palabra;
    }
}