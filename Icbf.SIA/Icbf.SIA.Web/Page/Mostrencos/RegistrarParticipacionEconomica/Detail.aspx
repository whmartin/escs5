﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_RegistrarParticipacionEconomica_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
       .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            background-color: #808080;
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 520px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .OcultarTerceros_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de radicado de la denuncia *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado en correspondencia *              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha radicado en correspondencia *              
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Tipo de Identificación *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Número de identificación *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                        <tr class="rowB">
                            <td>&nbsp; Primer nombre *
                            </td>
                            <td class="auto-style1">&nbsp;</td>
                            <td>Segundo nombre *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>&nbsp; Primer apellido *
                            </td>
                            <td class="auto-style1"></td>
                            <td>Segundo apellido *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="PanelRazonSocial">
                        <tr class="rowB">
                            <td colspan="2">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">&nbsp;&nbsp;Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Valor de la Denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextValordelaDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionPago">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3" class="tdTitulos">Información Pago de Participación Económica</td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 50%">Modalidad de Pago *                          
                             <asp:Label ID="lblCampoRequeridoModalidadPago" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtModalidadPago" RepeatDirection="Horizontal" Enabled="false" AutoPostBack="false">
                                <asp:ListItem Value="2">Parcial</asp:ListItem>
                                <asp:ListItem Value="3">Total</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlRelacionOrdenesPago">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3" class="tdTitulos">Relación Ordenes de Pago</td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3">&nbsp;&nbsp;Información Contrato</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="grvInformacionPagos" AutoGenerateColumns="False" AllowPaging="True" 
                                GridLines="None" Width="100%" ShowHeader="true" Visible="true" DataKeyNames="IdOrdenesPago" CellPadding="0" Height="16px" PageSize="10"
                                OnRowDataBound="gvwDocumentacionRecibida_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderText="Fecha solicitud liquidación" DataField="FechaSolicitudLiquidacion"  DataFormatString="{0:dd/MM/yyyy}"/>
                                    <asp:BoundField HeaderText="Número de resolución de pago" DataField="NumeroResolucion" DataFormatString="{0:0}" HtmlEncode="false"/>
                                    <asp:BoundField HeaderText="Fecha resolución de pago" DataField="FechaResolucionPago" DataFormatString="{0:dd/MM/yyyy}"/>
                                    <asp:BoundField HeaderText="Valor a pagar" DataField="ValorAPagar" DataFormatString="{0:c2}" />
                                    <asp:BoundField HeaderText="Número de comprobante de pago" DataField="NumeroComprobantePago" DataFormatString="{0:0}" HtmlEncode="false" />
                                    <asp:BoundField HeaderText="Valor Pagado" DataField="ValorPagado" DataFormatString="{0:c2}"/>
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron contratos asociados a la denuncia
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlCostos">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td width="50%"></td>   
                        <td colspan="3">Valor Total a Pagar Participación Económica
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtTotalaPagar" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td width="50%"></td>
                        <td colspan="3">Valor Total Pagado Participación Económica
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtTotalPagado" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Documento soporte de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de solicitud *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="auto-style4">
                            <asp:DropDownList ID="ddlTipoDocumentoSolicitado" runat="server" Enabled="false"
                                DataValueField="TipoDocumento" DataTextField="NomTipoDocumento" Height="16px" Width="226px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaSolicitudSolicitado" Requerid="true" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Observaciones al documento solicitado *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txbObservacionesSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Documentación recibida</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                          <asp:GridView runat="server" ID="gvwHistorico" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien"
                          OnPageIndexChanging="gvwHistorico_PageIndexChanging" OnRowCommand="gvwHistorico_RowCommand"
                          AllowSorting="False" OnSorting="gvwHistorico_Sorting" OnRowDataBound="gvwHistorico_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                            <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" DataFormatString="{0:d}" SortExpression="FechaRecibidoGrilla" />                            
                            <asp:BoundField DataField="FechaRecibidoGrilla" ItemStyle-CssClass="Ocultar_"/>
                            <asp:BoundField DataField="NombreArchivo" ItemStyle-CssClass="Ocultar_"/>
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla"/>     
                            <asp:TemplateField HeaderText="Nombre archivo" ItemStyle-CssClass="descripcionGrilla" SortExpression="NombreArchivo">
                                        <ItemTemplate>
                                            <asp:Button runat="server" CssClass="lnkArchivoDescargar descripcionGrilla" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                        </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                            
                        </td>
                    </tr>

                </table>
            </asp:Panel>
             <asp:Panel runat="server" ID="pnlArchivo" Width="90%" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackgroundCssClass="Background"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">

                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">

                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="always">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnDescargar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-1 align-center">
                                    <div>
                                        <asp:ImageButton ID="btnCerrarPanelAr" runat="server" Style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative; width: 20px; height: 20px;"
                                            alt="h"
                                            OnClick="btnCerrarPanelAr_Click" ImageUrl="../../../Image/btn/close.png" />
                                    </div>
                                </div>
                            </div>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 248px;
        }

        .auto-style4 {
            width: 259px;
        }
    </style>
</asp:Content>

