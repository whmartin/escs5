﻿//-----------------------------------------------------------------------
// <copyright file="Page_Mostrencos_RegistrarParticipacionEconomica_Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Page_Mostrencos_RegistrarParticipacionEconomica_Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>19/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarParticipacionEconomica_Detail : GeneralWeb
{

    #region VARIABLES

    /// <summary>
    /// Variable tipo ToolBar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Codigo identificador de la denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Contrato
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private int IdContrato;

    #endregion

    #region EVENTOS  

    /// <summary>
    /// Evento Pre Init
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro EvenArg</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Load de la página
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            toolBar.LipiarMensajeError();
            CargarDatosIniciales();
        }
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Metodo inicial para cargar datos en el list
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Pago de Participación Económica");
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            this.toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar datos de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.pnlArchivo.Visible = false;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
            List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);
            this.ViewState["SortedgvwHistorico"] = new List<DocumentosSolicitadosDenunciaBien>();
            this.CargarGrillaHistoricoDocumentacion();
            if (vIdDenunciaBien != 0)
            {
                if (this.vIdDenunciaBien != 0)
                {
                    RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

                    if ((vRegistroDenuncia.IdEstado == 11) || (vRegistroDenuncia.IdEstado == 13) || (vRegistroDenuncia.IdEstado == 19))
                    {
                        this.toolBar.OcultarBotonNuevo(true);
                        this.toolBar.MostrarBotonEditar(false);
                    }
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m.") && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.CodDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.CodDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;

                    // Se asigna el valor de la denuncia
                    if (vlstDenunciaBienTitulo.Count() > 0)
                    {
                        string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                        this.TextValordelaDenuncia.Text = valorDenuncia;
                    }
                    else
                    {
                        this.TextValordelaDenuncia.Text = "0,00";
                    }

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }

                int vIdOrdenPago = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdOrdenesPago"));
                if (vIdOrdenPago != 0)
                {
                    List<OrdenesPago> vListaOrdenPago = this.vMostrencosService.ConsultarOrdenesPagoParticipacionPorIdOrdenPago(vIdOrdenPago);

                    this.txtTotalaPagar.Text = vListaOrdenPago.Sum(item => item.ValorAPagar).ToString("C2");
                    this.txtTotalPagado.Text = vListaOrdenPago.Sum(item => item.ValorPagado).ToString("C2"); ;

                    if (vListaOrdenPago.Any(x => x.IdModalidadPago.IdModalidadPago == 2))
                    {
                        this.rbtModalidadPago.SelectedValue = "2";
                    }

                    if (vListaOrdenPago.Any(x => x.IdModalidadPago.IdModalidadPago == 3))
                    {
                        this.rbtModalidadPago.SelectedValue = "3";
                    }

                    if (vListaOrdenPago.Count() > 0)
                    {
                        this.grvInformacionPagos.Visible = true;
                        this.grvInformacionPagos.DataSource = vListaOrdenPago;
                        this.grvInformacionPagos.DataBind();
                    }

                    else
                    {
                        this.grvInformacionPagos.PageSize = PageSize();
                        this.grvInformacionPagos.EmptyDataText = EmptyDataText();
                        this.grvInformacionPagos.DataBind();
                    }

                }

                string vMensaje = Convert.ToString(this.GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.Mensaje"));

                if (!string.IsNullOrEmpty(vMensaje))
                {
                    this.toolBar.MostrarMensajeGuardado(vMensaje);
                    this.SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.Mensaje", string.Empty);
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar a la grilla los datos de historico de documentación
    /// </summary>
    public void CargarGrillaHistoricoDocumentacion()
    {
        ////Se llena la grilla con los todos documentos de la denuncia (Todos los estados)
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien"));
        List<DocumentosSolicitadosDenunciaBien> vListaArchivos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        ViewState["SortedgvwHistorico"] = vListaArchivos;
        this.gvwHistorico.DataSource = vListaArchivos;
        this.gvwHistorico.DataBind();
    }

    #endregion  

    #region BOTONES   

    /// <summary>
    /// Metodo redirige a la pantalla nuevo
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Boton Editar
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        //this.NavigateTo(SolutionPage.Edit);
        this.NavigateTo("../RegistrarParticipacionEconomica/Edit.aspx");
    }

    /// <summary>
    /// Metodo redirige a la pantalla list
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Metodo redirige a la pantalla detalle de resgistrar documentos bien denuncido
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #endregion

    protected void gvwHistorico_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void gvwHistorico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }


    protected void gvwHistorico_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Ver"))
        {
            this.btnDescargar.Visible = false;
            this.pnlArchivo.Visible = true;
            bool vVerArchivo = false;
            string vRuta = string.Empty;
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwHistorico.DataKeys[vRow].Value);
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            DocumentosSolicitadosDenunciaBien vArchivo = vList.Find(x => x.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado);
            this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
            this.hfNombreArchivo.Value = vArchivo.NombreArchivo.ToString();
            if (vArchivo.IdApoderado == null || vArchivo.IdCausante == null)
            {
                if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
                {
                    vVerArchivo = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
                    vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
                }
                else
                {
                    vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
                    vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
                    }
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarCalidadDenunciante/" + vArchivo.RutaArchivo;
                    }
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vArchivo.RutaArchivo;
                    }
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo;
                    }
                }

                if (vVerArchivo)
                {
                    this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                    if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                    {
                        this.ifmVerAdchivo.Visible = true;
                        this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                        this.imgDodumento.Visible = false;
                        this.imgDodumento.ImageUrl = string.Empty;
                        this.btnDescargar.Visible = true;
                    }
                    else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                    {
                        this.ifmVerAdchivo.Visible = false;
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.imgDodumento.Visible = true;
                        this.imgDodumento.ImageUrl = vRuta;
                        this.btnDescargar.Visible = true;
                    }
                }

                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
    }

    protected void btnCerrarPanelAr_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        bool vDescargar = false;
        string vRuta = string.Empty;
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
        if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
        {
            vDescargar = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
            vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
        }
        else
        {
            vDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
            if (!vDescargar)
            {
                vDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }
        }

        if (vDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();

        }
    }

    /// <summary>
    /// Control de la data enviada al gridview documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistorico_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("1/01/0001") || e.Row.Cells[3].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (e.Row.Cells[i].Controls.Count > 0)
                {
                    ((LinkButton)e.Row.Cells[i].Controls[0]).TabIndex = -1;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[0].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[0].Text.Equals("01/01/0001")
                || e.Row.Cells[0].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[0].Text = "";
            }

            if (e.Row.Cells[2].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[2].Text.Equals("01/01/0001")
                || e.Row.Cells[2].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[2].Text = "";
            }
        }
    }
}