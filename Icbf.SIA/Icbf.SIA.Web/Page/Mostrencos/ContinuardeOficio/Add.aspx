﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_ContinuardeOficio_List" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>

               <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado denuncia *</td>
                        <td>Fecha de Radicado de la Denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en Correspondencia *</td>
                        <td>Fecha Radicado en Correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de Identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Primer Nombre *</td>
                        <td>Segundo Nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Primer Apellido *</td>
                        <td>Segundo Apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Descripción de la denuncia *</td>
                        <td style="width: 55%">Histórico de la denuncia
                         <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                                 <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                             </a>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" ScrollBars="None" Style="background-color: White; 
            border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 25%; left: 15%; width: 800px; background-color: white; 
            border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView ID="tbHistoria" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False" 
                                    AllowPaging="true" AllowSorting="true" GridLines="None" 
                                    OnSorting="GvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="GvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                        <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlContinuarDeOficio">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información Continua de oficio </td>
                    </tr>

                    <tr class="rowB">
                        <td>Motivo *
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="rblMotivo"
                                ValidationGroup="btnGuardar" runat="server" ForeColor="Red"
                                ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td>Número de Resolución *
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtNumeroResolucion"
                                    ValidationGroup="btnGuardar" runat="server" ForeColor="Red"
                                    ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblMotivo">
                                <asp:listitem value="1" Selected ="true" Text="Se niega la calidad del denunciante"/>
                                <asp:listitem value="2" Text="Se revoca la calidad del denunciante"/>
                                <asp:listitem value="3" Text="Denuncia presentada por entidad pública"/>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroResolucion"  Enabled="true" MaxLength="20"></asp:TextBox>
                            <ajax:FilteredTextBoxExtender ID="fbBoxFormato" runat="server" TargetControlID="txtNumeroResolucion"
                        FilterType="Numbers,Custom" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Observaciones *
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtObservacion"
                                 ValidationGroup="btnGuardar" runat="server" ForeColor="Red"
                                 ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtObservacion"
                                TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px"
                                Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftObservacion" runat="server" TargetControlID="txtObservacion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });

            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('#cphCont_txtObservacion').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacion").value.length > 250) {
                    this.value = document.getElementById("cphCont_txtObservacion").value.substr(0, 250);
                }
            });
        });
        function CheckLength() {
            var textbox = document.getElementById("cphCont_txtDescripcion").value;
            if (textbox.trim().length >= 250) {
                return false;
            }
            else {
                return true;
            }
        }
     </script>
</asp:Content>