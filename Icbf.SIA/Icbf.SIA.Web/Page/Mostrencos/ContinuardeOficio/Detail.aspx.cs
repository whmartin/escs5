﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_ContinuardeOficio_List : GeneralWeb
{
    #region Variables

    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ContinuardeOficio";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    private int vIdDenunciaBien;

    #endregion


    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    #endregion

    #region Metodos

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Continuar de Oficio");
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.BtnRetornar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.BtnGuardar_Click);

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));

            //TODO: Freddy: validaciones 
            //Valida el estado de la denuncia (=archivado o =anulada)
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;

            if (vRegistroDenuncia.IdEstado != 11 && vRegistroDenuncia.IdEstado != 13 && vRegistroDenuncia.IdEstado != 19)
            {
                this.toolBar.eventoNuevo += new ToolBarDelegate(this.BtnNuevo_Click);
                this.toolBar.eventoEditar += new ToolBarDelegate(this.BtnEditar_Click);
            }
            this.toolBar.OcultarBotonGuardar(false);
            HabilitarTodosControles(false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));

            if (this.vIdDenunciaBien != 0)
            {
                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.CodigoTipoIdentificacion == "NIT")
                {
                    this.rblMotivo.SelectedValue = "3";
                    this.rblMotivo.Enabled = false;
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = vRegistroDenuncia.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vRegistroDenuncia.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001 12:00:00 a. m.")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
                List<ContinuarOficio> vLstContinuarOficio = this.vMostrencosService.ConsultarContinuarOficios(vIdDenunciaBien, 0);
                if (vLstContinuarOficio.Count > 0)
                {
                    this.toolBar.MostrarBotonNuevo(false);
                    ContinuarOficio vContinuarOficio = vLstContinuarOficio.Last();
                    this.txtObservacion.Text = vContinuarOficio.Observaciones;
                    this.txtNumeroResolucion.Text = Convert.ToString(vContinuarOficio.NumeroResolucion);
                    this.rblMotivo.SelectedValue = Convert.ToString(vContinuarOficio.IdMotivoContinuarOficio);
                }

                string vMensaje = this.GetSessionParameter("Mostrencos.ContinuarOficio.Mensaje").ToString();
                if (!string.IsNullOrEmpty(vMensaje))
                {
                    this.toolBar.MostrarMensajeGuardado(vMensaje);
                    this.SetSessionParameter("Mostrencos.ContinuarOficio.Mensaje", null);
                }

            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void HabilitarTodosControles(Boolean habilitar)
    {
        HabilitarControl(this.rblMotivo, habilitar);
        HabilitarControl(this.txtNumeroResolucion, habilitar);
        HabilitarControl(this.txtObservacion, habilitar);
    }

    private void HabilitarControl(WebControl control, Boolean habilitar)
    {
        control.Enabled = habilitar;
    }
    #endregion


    #region BOTONES
    protected void BtnEditar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Edit.aspx");
    }

    protected void BtnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void BtnGuardar_Click(object sender, EventArgs e)
    {
        toolBar.MostrarMensajeError("Metodo no implementado");
    }

    protected void BtnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #endregion
}