﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_ContinuardeOficio_List : GeneralWeb
{
    #region Variables

    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ContinuardeOficio";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    private int vIdDenunciaBien;

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    //protected void BtnEditar_Click(object sender, EventArgs e)
    //{
    //    toolBar.MostrarMensajeError("Metodo no implementado");
    //}

    //protected void BtnNuevo_Click(object sender, EventArgs e)
    //{
    //    toolBar.MostrarMensajeError("Metodo no implementado");
    //}

    protected void BtnGuardar_Click(object sender, EventArgs e)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));
        ContinuarOficio vContinuarOficio = ObtenerContinuarOficio();
        if (ValidarRegistroNoExistente(vIdDenunciaBien, vContinuarOficio.IdMotivoContinuarOficio))
        {
            if (GuardarInformacion(vContinuarOficio))
            {
                this.ActualizarHistorico();

                this.SetSessionParameter("Mostrencos.ContinuarOficio.Mensaje", "La información ha sido guardada exitosamente");
                Response.Redirect("../../Mostrencos/ContinuardeOficio/Detail.aspx");
            }
            else
            {
                toolBar.MostrarMensajeError("Ocurrio un error al guardar la información");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("El registro ya existe");
        }
    }

    protected void BtnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #endregion

    #region Metodos

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Continuar de Oficio");
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.BtnRetornar_Click);
            //this.toolBar.eventoNuevo += new ToolBarDelegate(this.BtnNuevo_Click);
            //this.toolBar.eventoEditar += new ToolBarDelegate(this.BtnEditar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.BtnGuardar_Click);

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));

            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") ||
                vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("TERMINADO") ||
                vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA"))
            {
                this.toolBar.MostrarBotonNuevo(false);
                this.toolBar.MostrarBotonEditar(false);
            }
            this.HabilitarTodosControles(true);

            DenunciaBien vDenunciaBien = vMostrencosService.ConsultarDatosDenunciaBien(vMostrencosService.ConsultaCompletaDenunciaBienPorId(vIdDenunciaBien));
            var TipoPersona = vDenunciaBien.Tercero.NombreTipoPersona;
            Tercero tercero = new Tercero();
            tercero = vMostrencosService.ConsultarTipoEntidadPorIdTercero(vDenunciaBien.Tercero.IdTercero);
            //if (tercero.Entidad == "PUBLICO")
            //{
            //    rblMotivo.SelectedIndex = 2;
            //   HabilitarControl(rblMotivo, false);
            //}else 
            if (TipoPersona == "JURIDICA")
            {
                rblMotivo.SelectedIndex = 1;
                HabilitarControl(rblMotivo, false);
            }
            rblMotivo.ClearSelection();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));

            if (this.vIdDenunciaBien != 0)
            {
                this.CargarGrillaHistorico(vIdDenunciaBien);
                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.CodigoTipoIdentificacion == "NIT")
                {
                    this.rblMotivo.SelectedValue = "3";
                    this.rblMotivo.Enabled = false;
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = vRegistroDenuncia.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vRegistroDenuncia.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;

                this.txtDescripcion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.DescripcionDenuncia) ? vRegistroDenuncia.DescripcionDenuncia : string.Empty;
                this.fecRaCorrespondencia.Text = vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vRegistroDenuncia.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;

                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void HabilitarTodosControles(Boolean habilitar)
    {
        HabilitarControl(this.rblMotivo, habilitar);
        HabilitarControl(this.txtNumeroResolucion, habilitar);
        HabilitarControl(this.txtObservacion, habilitar);
    }

    private void HabilitarControl(WebControl control, Boolean habilitar)
    {
        control.Enabled = habilitar;
    }

    private Boolean ValidarRegistroNoExistente(int? pIdDenunciaBien, int? pIdContinuarOficio)
    {
        bool bandera = false;
        List<ContinuarOficio> vContinuarOficio = vMostrencosService.ConsultarContinuarOficios(pIdDenunciaBien, pIdContinuarOficio);
        bandera = (vContinuarOficio.Count > 0) ? false : true;
        return bandera;
    }

    private Boolean GuardarInformacion(ContinuarOficio pContinuarOficio)
    {
        bool bandera = false;
        this.InformacionAudioria(pContinuarOficio, this.PageName, vSolutionPage);
        int vResultado = vMostrencosService.InsertarContinuarOficio(pContinuarOficio);
        bandera = (vResultado > 0) ? true : false;
        return bandera;
    }

    private ContinuarOficio ObtenerContinuarOficio()
    {
        ContinuarOficio vContinuarOficio = new ContinuarOficio
        {
            IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien")),
            IdMotivoContinuarOficio = ObtenerIdMotivoContinuarOficio(this.rblMotivo.SelectedValue),
            NumeroResolucion = Convert.ToDecimal(this.txtNumeroResolucion.Text),
            Observaciones = this.txtObservacion.Text.ToString(),
            UsuarioCrea = this.GetSessionUser().NombreUsuario,
            IdUsuarioCrea = this.GetSessionUser().IdUsuario
        };
        return vContinuarOficio;
    }

    private int ObtenerIdMotivoContinuarOficio(string idControl)
    {
        switch (idControl)
        {
            case "1":
                return 1;
            case "2":
                return 2;
            case "3":
                return 3;
            default:
                return 0;
        }
    }

    /// <summary>
    /// Método void ActualizarHistorico
    /// </summary>
    private void ActualizarHistorico()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15;
        pHistoricoEstadosDenunciaBien.IdFase = 3;
        pHistoricoEstadosDenunciaBien.IdAccion = 32;
        pHistoricoEstadosDenunciaBien.IdActuacion = 22;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.Fase = 1.ToString();
        pHistoricoEstadosDenunciaBien.Accion = 2.ToString();
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.tbHistoria.DataSource = vHistorico;
            this.tbHistoria.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbHistoria.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.tbHistoria.DataSource = vList;
                this.tbHistoria.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }
                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }
                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }
                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }
                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }
                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.tbHistoria.DataSource = vResult;
        this.tbHistoria.DataBind();
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    #endregion

}