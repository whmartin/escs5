//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenuncia_Detail .</summary>
// <author>INGENIAN SOFTWARE[Darío Alfredo Beltrán Camacho]</author>
// <date>03/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Business;
using Icbf.SIA.Business.IBusiness;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Globalization;

/// <summary>
/// Clase parcial para el Detail de RegistroDenuncia.
/// </summary>
public partial class Page_RegistroDenuncia_Detail : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Clase para evento de la toolbar
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Path de la página.
    /// </summary>
    private string PageName = "Mostrencos/RegistroDenuncia";

    /// <summary>
    /// Clase vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Parametro de respuesta
    /// </summary>
    private string pRespuestaEncuesta = string.Empty;

    /// <summary>
    /// Parametro de numero de identificacion denunciante
    /// </summary>
    private string pNumeroIdentificacion = string.Empty;

    /// <summary>
    /// Parametro de nombre del denunciante
    /// </summary>
    private string pNombre = string.Empty;

    /// <summary>
    /// Parametro de correo del denunciante
    /// </summary>
    private string pCorreo = string.Empty;

    /// <summary>
    /// Variable de Datos referencia
    /// </summary>
    private string vDatosReferencia = string.Empty;

    /// <summary>
    /// Parametro de MD5
    /// </summary>
    private string pMD5 = string.Empty;

    /// <summary>
    /// Parametro de Fecha
    /// </summary>
    private string pFecha;

    /// <summary>
    /// Parametro de Hora
    /// </summary>
    private string pHora;

    /// <summary>
    /// Parametro para el nombre del tramite
    /// </summary>
    private string pNombreTramite = string.Empty;

    /// <summary>
    /// Parametro para el numero de la encuesta
    /// </summary>
    private int pNumeroEncuesta = 1;

    /// <summary>
    /// Parametro para el codigo del Sistema de informacion
    /// </summary>
    private int vCodigoSistemaInformacion = 0;

    /// <summary>
    /// Variable de paso para convertir el valor de string a entero en el Sistema de Informacion
    /// </summary>
    private string vStringCodigoSistemaInformacion = string.Empty;

    /// <summary>
    /// Parametro para el codigo del Tramite
    /// </summary>
    private int vCodigoTramite = 0;

    /// <summary>
    /// Variable de paso para convertir el valor de string a entero en el Tramite
    /// </summary>
    private string vStringCodigoTramite = string.Empty;
    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.CargarDatosIniciales();
                this.CargarDatos();
            }
        }

        //string vRol = GetSessionUser().Rol.ToUpper();

        //if (vRol == "RADICADOR")
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
        {
            pnlInformacionCorrespondencia.Visible = true;
        }
    }

    /// <summary>
    /// Evento para validar el acceso a la página Add y tipo de transacción
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento para validar el acceso a la página editar y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", hfIdDenunciaBien.Value);
        this.SetSessionParameter("RegistroDenuncia.Denuncia", hlDenuncia.Text);
        this.SetSessionParameter("RegistroDenuncia.Cedula", hlCedula.Text);
        this.SetSessionParameter("RegistroDenuncia.Defuncion", hlDefuncion.Text);
        this.NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento para validar el acceso a la página List y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento para mostrar La encuesta que llena el usuario que finalizo el registro de la denuncia
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnSi_Encuesta(object sender, EventArgs e)
    {
        ////Api Tramite
        vStringCodigoTramite = ConfigurationManager.AppSettings["CodigoTramite"];
        vCodigoTramite = Convert.ToInt32(vStringCodigoTramite);
        ITramiteBLL vITramiteBLL = new TramiteBLL();
        List<Tramite> vListaTramite = vITramiteBLL.GetTramite(vCodigoTramite);
        int vIdTramite = vListaTramite.FirstOrDefault().IdTramite;
        pNombreTramite = vListaTramite.FirstOrDefault().NombreTramite;

        ////Api Sistema de Informacion
        vStringCodigoSistemaInformacion = ConfigurationManager.AppSettings["CodigoSistemaInformacion"];
        vCodigoSistemaInformacion = Convert.ToInt32(vStringCodigoSistemaInformacion);
        ISistemaInformacionBLL vISistemaInformacionBLL = new SistemaInformacionBLL();
        List<SistemaInformacion> vListaSistemaInformacion = vISistemaInformacionBLL.GetSistemaInformacion(vCodigoSistemaInformacion);
        int vIdSistemaInformacion = vListaSistemaInformacion.FirstOrDefault().IdSistemaInformacion;
        pRespuestaEncuesta = "SI";
        pNumeroIdentificacion = txtNumeroIdentificacion.Text;
        if (txtIdTipoDocIdentifica.Text == "NIT")
        {
            pNombre = txtPrimerNombre.Text;
        }
        else
        {
            pNombre = txtPrimerNombre.Text + " " + txtSegundoNombre.Text
                + " " + txtPrimerApellido.Text + " " + txtSegundoApellido.Text;
        }

        pCorreo = txtCorreoElectronico.Text;
        pFecha = DateTime.Now.ToString("yyyy/MM/dd");
        pHora = DateTime.Now.ToString("HH");
        vDatosReferencia = pNumeroIdentificacion + vIdSistemaInformacion + pFecha + pHora + "ICBF";
        using (MD5 pMd5Hash = MD5.Create())
        {
            pMD5 = GetMd5Hash(pMd5Hash, vDatosReferencia);
        }
        iFrameEncuestas.Attributes.Add(
            "src",
            ConfigurationManager.AppSettings["URLEncuesta"]
            + ConfigurationManager.AppSettings["URLEncuestaMostrencos"]
            + "&pRespuestaUsuario=" + pRespuestaEncuesta
            + "&pNumeroIdentificacion=" + pNumeroIdentificacion
            + "&pNombre=" + pNombre + "&pCorreo=" + pCorreo
            + "&pDatosReferenciaServicio=" + pMD5
            + "&pIdentificacionSI=" + vIdSistemaInformacion
            + "&pIdentificacionTramite=" + vIdTramite
            + "&pNombreTramite=" + pNombreTramite
            + "&pFecha=" + pFecha + "&pHora=" + pHora + "&pNumeroEncuesta="
            + pNumeroEncuesta + "&embed=1");

        dFrameEncuestas.Visible = true;

    }

    /// <summary>
    /// Evento para Ocultar el popup y enviar parametros al aplicativo de encuestas
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnNo_Encuesta(object sender, EventArgs e)
    {
        ////Api Tramite
        vStringCodigoTramite = ConfigurationManager.AppSettings["CodigoTramite"];
        vCodigoTramite = Convert.ToInt32(vStringCodigoTramite);
        ITramiteBLL vITramiteBLL = new TramiteBLL();
        List<Tramite> vListaTramite = vITramiteBLL.GetTramite(vCodigoTramite);
        int vIdTramite = vListaTramite.FirstOrDefault().IdTramite;

        ////Api Sistema de Informacion
        vCodigoSistemaInformacion = Convert.ToInt32(ConfigurationManager.AppSettings["CodigoSistemaInformacion"]);
        ISistemaInformacionBLL vISistemaInformacionBLL = new SistemaInformacionBLL();
        List<SistemaInformacion> vListaSistemaInformacion = vISistemaInformacionBLL.GetSistemaInformacion(vCodigoSistemaInformacion);
        int vIdSistemaInformacion = vListaSistemaInformacion.FirstOrDefault().IdSistemaInformacion;

        pRespuestaEncuesta = "NO";
        pFecha = DateTime.Now.ToString("yyyy/MM/dd");
        pHora = DateTime.Now.ToString("HH");
        pNumeroIdentificacion = txtNumeroIdentificacion.Text;
        vDatosReferencia = pNumeroIdentificacion + vIdSistemaInformacion + pFecha + pHora + "ICBF";
        using (MD5 md5Hash = MD5.Create())
        {
            pMD5 = GetMd5Hash(md5Hash, vDatosReferencia);
        }

        iFrameEncuestas.Attributes.Add(
            "src", 
            ConfigurationManager.AppSettings["URLEncuesta"]
            + ConfigurationManager.AppSettings["URLEncuestaMostrencos"]
            + "&pRespuestaUsuario="+ pRespuestaEncuesta + "&pNumeroIdentificacion="+pNumeroIdentificacion
            +"&pNombre=&pCorreo=&pDatosReferenciaServicio="
            + pMD5 + "&pIdentificacionSI=" + vIdSistemaInformacion 
            + "&pIdentificacionTramite=" + vIdTramite
            + "&pFecha=" + pFecha + "&pHora=" + pHora + "&pNumeroEncuesta=" 
            + pNumeroEncuesta + "&embed=1");
        dFrameEncuestas.Visible = true;
        MPopExtEncuesta.Hide();
    }

    /// <summary>
    /// Evento para ocultar la ventana emergente
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnCerrar_Popup(object sender, EventArgs e)
    {
        MPopExtEncuesta.Hide();
    }
    #endregion

    #region METODOS

    /// <summary>
    /// Metodo que carga los registros según el vIdSolicitudAclaracion
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("RegistroDenuncia.IdDenunciaBien"));
            string vRadicadoDenuncia = Convert.ToString(GetSessionParameter("RegistroDenuncia.RadicadoDenuncia"));
            this.RemoveSessionParameter("RegistroDenuncia.IdDenunciaBien");

            MPopExtEncuesta.Show();

            if (GetSessionParameter("RegistroDenuncia.Guardado").ToString() == "1" && GetSessionParameter("CorreoCoordinador").ToString() == ";")
            {
                this.toolBar.MostrarMensajeGuardado("Registro de denuncia exitoso. Su denuncia ha sido registrada con el código(" + vRadicadoDenuncia + "). Por favor confirmar con la Regional más cercana");
            }
            else if (GetSessionParameter("RegistroDenuncia.Guardado").ToString() == "1" && GetSessionParameter("CorreoCoordinador").ToString() != ";")
            {
                this.toolBar.MostrarMensajeGuardado("Registro de denuncia exitoso. Su denuncia ha sido registrada con el código(" + vRadicadoDenuncia + ")");
            }

            if (GetSessionParameter("RegistroDenuncia.Guardado").ToString() == "2")
            {
                this.toolBar.MostrarMensajeGuardado("Actualización de datos exitoso");
            }

            this.RemoveSessionParameter("RegistroDenuncia");
            this.RemoveSessionParameter("RegistroDenuncia.Guardado");
            this.RemoveSessionParameter("CorreoCoordinador");

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarRegistroDenuncia(vIdDenunciaBien);

            if (vRegistroDenuncia.CodDocumento == "NIT")
            {
                lblPrimerNombre.Text = "Razón social";
                txtPrimerNombre.Text = vRegistroDenuncia.RazonSocial;
                lblSegundoNombre.Visible = false;
                txtSegundoNombre.Visible = false;
                lblPrimerApellido.Visible = false;
                txtPrimerApellido.Visible = false;
                lblSegundoApellido.Visible = false;
                txtSegundoApellido.Visible = false;
            }
            else
            {
                lblPrimerNombre.Text = "Primer nombre";
                txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                lblSegundoNombre.Visible = true;
                txtSegundoNombre.Visible = true;
                lblPrimerApellido.Visible = true;
                txtPrimerApellido.Visible = true;
                lblSegundoApellido.Visible = true;
                txtSegundoApellido.Visible = true;
                txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
            }

            hfIdDenunciaBien.Value = vRegistroDenuncia.IdDenunciaBien.ToString();
            txtIdTipoDocIdentifica.Text = vRegistroDenuncia.CodDocumento;
            txtNumeroIdentificacion.Text = vRegistroDenuncia.NUMEROIDENTIFICACION;
            txtIdTipoPersona.Text = vRegistroDenuncia.NombreTipoPersona;
            txtIdDepartamento.Text = vRegistroDenuncia.NombreDepartamento;
            txtIdMunicipio.Text = vRegistroDenuncia.NombreMunicipio;

            if (vRegistroDenuncia.IdZona == "1")
            {
                txtIdZona.Text = "Urbano";
            }
            else if (vRegistroDenuncia.IdZona == "2")
            {
                txtIdZona.Text = "Rural";
            }

            txtDireccion.Text = vRegistroDenuncia.Direccion;
            txtIndicativo.Text = vRegistroDenuncia.Indicativo;
            txtTelefono.Text = vRegistroDenuncia.Telefono;
            txtCelular.Text = vRegistroDenuncia.Celular;
            txtCorreoElectronico.Text = vRegistroDenuncia.CORREOELECTRONICO;

            if (vRegistroDenuncia.Acepto == true)
            {
                chkAcepto.Checked = true;
            }

            if (txtIdTipoDocIdentifica.Text == "NIT")
            {
                lblCedulaDenunciante.Text = "RUT *";
            }
            else
            {
                lblCedulaDenunciante.Text = "Cédula de ciudadania del denunciante *";
            }

            txtDescripcionDenuncia.Text = vRegistroDenuncia.DescripcionDenuncia;
            txtIdRegionalUbicacion.Text = vRegistroDenuncia.NombreRegional;

            this.CargarDenuncia();
            this.CargarCedula();
            this.CargarDefuncion();
            this.CargarOtrosDocumentos();

            //string vRol = GetSessionUser().Rol.ToUpper();

            //if (vRol == "RADICADOR")
            if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
            {
                if (vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna == "S")
                {
                    cbDenunciaRecibidaCorrespondenciaPorInterna.Checked = true;
                    txtRadicadoCorrespondencia.Text = vRegistroDenuncia.RadicadoCorrespondencia;

                    if (vRegistroDenuncia.FechaRadicadoCorrespondencia == Convert.ToDateTime("01/01/0001"))
                    {
                        txtFechaRadicadoCorrespondencia.Text = string.Empty;
                    }
                    else
                    {
                        txtFechaRadicadoCorrespondencia.Text = Convert.ToString(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                    }

                    txtHoraRadicadoCorrespondencia.Text = vRegistroDenuncia.HoraRadicadoCorrespondencia;

                    pnlDatosRadicado.Visible = true;
                }
                else
                {
                    pnlDatosRadicado.Visible = false;
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Denuncia en el control para descargarlo
    /// </summary>
    private void CargarDenuncia()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 1);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                hlDenuncia.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            }

            //if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo != null)
            //{
            //    hlDenuncia.NavigateUrl = "Archivos/" + vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            //}
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Cedula en el control para descargarlo
    /// </summary>
    private void CargarCedula()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 2);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                hlCedula.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            }

            //if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo != null)
            //{
            //    hlCedula.NavigateUrl = "Archivos/" + vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            //}
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Defunción en el control para descargarlo
    /// </summary>
    private void CargarDefuncion()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 3);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                hlDefuncion.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            }

            //if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo != null)
            //{
            //    hlDefuncion.NavigateUrl = "Archivos/" + vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            //}
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que consulta la lista de documentos de la sección otros
    /// </summary>
    /// <returns>Lista de documentos Otros</returns>
    private List<DocumentosSolicitadosDenunciaBien> CargarOtrosDocumentos()
    {
        List<DocumentosSolicitadosDenunciaBien> vLista = new List<DocumentosSolicitadosDenunciaBien>();

        try
        {
            int vIdDenuncia = Convert.ToInt32(hfIdDenunciaBien.Value);

            vLista = this.vMostrencosService.ConsultarOtrosDocumentos(vIdDenuncia, 4);
            this.Session["SortedView"] = vLista;

            if (vLista != null)
            {
                gvOtrosDocumentos.DataSource = vLista;
                gvOtrosDocumentos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

        return vLista;
    }

    /// <summary>
    /// Método para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);

            this.toolBar.EstablecerTitulos("Gesti&oacute;n de denuncias", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.RemoveSessionParameter("RegistroDenuncia");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para consultar usuario de acuerdo a programa/función/rol
    /// </summary>
    /// <param name="pNombreFuncion">The Nombre a filtrar</param>
    /// <returns>EsFlag boolean</returns>
    private bool ConsultarUsuarioProgramaFuncionRol(string pNombreFuncion)
    {
        ////Roles Permitidos
        ////CU 82
        ////Denunciante: Insertar
        ////Radicador: Consultar, Insertar, Editar
        bool EsFlag = false;
        List<Icbf.Seguridad.Entity.ProgramaFuncion> vListProgramaFuncion = new List<Icbf.Seguridad.Entity.ProgramaFuncion>();

        if (this.ViewState["PROGRAMAFUNCION"] == null)
        {
            vListProgramaFuncion = RolUtil.ListarRolesUsuario(GetSessionUser().NombreUsuario, "Mostrencos/RegistroDenuncia");
            this.ViewState["PROGRAMAFUNCION"] = vListProgramaFuncion;
        }
        else
        {
            vListProgramaFuncion = (List<Icbf.Seguridad.Entity.ProgramaFuncion>)this.ViewState["PROGRAMAFUNCION"];
        }

        Icbf.Seguridad.Entity.ProgramaFuncion vProgramaFuncion = vListProgramaFuncion.Find(p => p.NombreFuncion.Trim().ToUpper() == pNombreFuncion.Trim().ToUpper());

        if (vProgramaFuncion != null)
        {
            EsFlag = true;
        }

        return EsFlag;
    }

    /// <summary>
    /// Método para cifrar string en MD5
    /// </summary>
    /// <param name="pMd5Hash"></param>
    /// <param name="pInput"></param>
    /// <returns>Cadena tipo StringBuilder</returns>
    private string GetMd5Hash(MD5 pMd5Hash, string pInput)
    {
        byte[] vData = pMd5Hash.ComputeHash(Encoding.UTF8.GetBytes(pInput));
        StringBuilder sBuilder = new StringBuilder();

        for (int i = 0; i < vData.Length; i++)
        {
            sBuilder.Append(vData[i].ToString("x2"));
        }

        return sBuilder.ToString();
    }

    #endregion
}