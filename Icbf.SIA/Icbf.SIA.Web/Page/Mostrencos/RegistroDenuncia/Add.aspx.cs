//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenuncia_Add .</summary>
// <author>INGENIAN SOFTWARE[Darío Alfredo Beltrán Camacho]</author>
// <date>03/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Web.Security;
using Icbf.RUBO.Entity;
using Icbf.Seguridad.Entity;

/// <summary>
/// Clase parcial para el Add de RegistroDenuncia.
/// </summary>
public partial class Page_RegistroDenuncia_Add : GeneralWeb
{
    #region DECLARACIÓN DE OBJETOS Y VARIABLES

    /// <summary>
    /// Clase para evento de la toolbar
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Clase vMostrencosService
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Path de la página.
    /// </summary>
    private string PageName = "Mostrencos/RegistroDenuncia";

    SeguridadService vSeguridadService = new SeguridadService();

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la página
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
        {
            this.toolBar.OcultarBotonBuscar(false);
        }

        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
        {
            vSolutionPage = SolutionPage.Edit;
        }

        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
                {
                    pnlInformacionCorrespondencia.Visible = true;
                    this.chkAcepto.Checked = true;
                    this.chkDenunciaRecibidaCorrespondenciaPorInterna.Checked = true;

                    if (chkAcepto.Checked == true)
                    {
                        AceptarTerminosYCondiciones(true);
                    }
                    else
                    {
                        AceptarTerminosYCondiciones(false);
                    }

                    if (chkDenunciaRecibidaCorrespondenciaPorInterna.Checked == true)
                    {
                        lblRadicadoCorrespondencia.Visible = true;
                        txtRadicadoCorrespondencia.Visible = true;
                        lblFechaRadicadoCorrespondencia.Visible = true;
                        txtFechaRadicadoCorrespondencia.Visible = true;
                        lblHoraRadicadoCorrespondencia.Visible = true;
                        txtHoraRadicadoCorrespondencia.Visible = true;
                        pnlDatosRadicado.Visible = true;
                    }
                    else
                    {
                        lblRadicadoCorrespondencia.Visible = false;
                        txtRadicadoCorrespondencia.Visible = false;
                        lblFechaRadicadoCorrespondencia.Visible = false;
                        txtFechaRadicadoCorrespondencia.Visible = false;
                        lblHoraRadicadoCorrespondencia.Visible = false;
                        txtHoraRadicadoCorrespondencia.Visible = false;
                        pnlDatosRadicado.Visible = false;
                    }
                }
                else if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
                {
                    this.toolBar.OcultarBotonBuscar(false);
                    this.ddlIdTipoDocIdentifica.Visible = false;
                    this.txtNumeroIdentificacion.Enabled = false;
                    this.btnConsultar.Visible = false;
                    this.txtTipoIdentificacion.Visible = true;
                    this.txtNumeroIdentificacion.Text = GetSessionUser().NumeroDocumento;

                    int vTipoDocIdentifica = GetSessionUser().IdTipoDocumento;

                    if (vTipoDocIdentifica == 1)
                    {
                        txtTipoIdentificacion.Text = "CC";
                    }
                    else if (vTipoDocIdentifica == 2)
                    {
                        txtTipoIdentificacion.Text = "CE";
                    }
                    else
                    {
                        txtTipoIdentificacion.Text = "NIT";
                    }

                    this.Buscar();
                }

                this.Session["lstOtrosDocumentos"] = new List<DocumentosSolicitadosDenunciaBien>();
                //this.Session["lstDocumentosOtros"] = null;
                this.CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                {
                    btnDenuncia.Enabled = true;
                    btnCedula.Enabled = true;
                    btnDefuncion.Enabled = true;
                    btnNuevoDocumento.Enabled = true;
                    this.CargarRegistro();
                    AceptarTerminosYCondiciones(true);
                }
            }
            else
            {
                this.Session["lstOtrosDocumentos"] = this.Session["lstOtrosDocumentos"];
            }
        }
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// Evento que limpia los controles de la pantalla
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnLimpiarPantalla_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento para lanzar la búsqueda 
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento para lanzar el metodo de consulta 
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnConsultar_Click(object sender, ImageClickEventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Método que almacena en sesión los documentos de la sección Otros
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevoDocumento_Click(object sender, ImageClickEventArgs e)
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
        {
            this.toolBar.OcultarBotonBuscar(false);
        }

        string pdfExt = System.IO.Path.GetExtension(fuDocumentoOtros.PostedFile.FileName);
        if (pdfExt.ToUpper() != ".PDF")
        {
            toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
        }
        else
        {
            toolBar.LipiarMensajeError();
            int pdfTam = fuDocumentoOtros.PostedFile.ContentLength;
            if (pdfTam > (2048 * 1024))
            {
                toolBar.MostrarMensajeError("El tamano del archivo PDF  máximo es de 2048 KB");
            }
            else
            {
                //this.Session["lstDocumentosOtros"] = new List<DocumentosSolicitadosDenunciaBien>();

                string vObservacionesDocumento = txtObservacionesDocumento.Text.ToUpper();
                string vNombreArchivo = fuDocumentoOtros.FileName;
                byte[] bytesArchivo = fuDocumentoOtros.FileBytes;

                if (!string.IsNullOrEmpty(vNombreArchivo))
                {
                    List<DocumentosSolicitadosDenunciaBien> lstDocumentosOtros = (List<DocumentosSolicitadosDenunciaBien>)Session["lstOtrosDocumentos"];
                    DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien()
                    {
                        ObservacionesDocumento = vObservacionesDocumento,
                        NombreArchivo = vNombreArchivo,
                        bytes = bytesArchivo,
                        ExisteArchivo = false
                    };

                    //this.CargarOtrosDocumentos();

                    lstDocumentosOtros.Add(vDocumentosSolicitadosDenunciaBien);
                    this.Session["lstOtrosDocumentos"] = lstDocumentosOtros;

                    gvOtrosDocumentos.DataSource = lstDocumentosOtros;
                    gvOtrosDocumentos.DataBind();
                    this.txtObservacionesDocumento.Text = string.Empty;

                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert(mensaje);", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "pageLoad();", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "eliminarDocumento(control);", true);
                }
            }
        }
    }

    /// <summary>
    /// Método que elimina un documento de la sesión.
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnEliminarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> lstDocumentosOtros = (List<DocumentosSolicitadosDenunciaBien>)Session["lstOtrosDocumentos"];
        int id = Convert.ToInt32(hfIdDocomentoOtros.Value);

        DocumentosSolicitadosDenunciaBien eliminar;

        if (id > 0)
        {
            eliminar = lstDocumentosOtros.Single(x => x.IdDocumentosSolicitadosDenunciaBien == id);
        }
        else
        {
            int idx = Convert.ToInt32(hfIndiceDocumentosOtros.Value);
            eliminar = lstDocumentosOtros[idx];
        }

        if (eliminar != null)
        {
            lstDocumentosOtros.Remove(eliminar);
        }

        this.gvOtrosDocumentos.DataSource = lstDocumentosOtros;
        this.gvOtrosDocumentos.DataBind();

        this.Session["lstOtrosDocumentos"] = lstDocumentosOtros;

        this.hfIndiceDocumentosOtros.Value = string.Empty;
        this.hfIdDocomentoOtros.Value = string.Empty;
    }

    /// <summary>
    /// Método que habilita controles dependiendo el estado
    /// </summary>
    /// <param name="sender">The Checked</param>
    /// <param name="e">The CheckedChanged</param>
    protected void chkAcepto_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAcepto.Checked == true)
        {
            AceptarTerminosYCondiciones(true);
        }
        else
        {
            AceptarTerminosYCondiciones(false);
        }
    }

    /// <summary>
    /// Método que valida controles dependiendo del estado del Check
    /// </summary>
    /// <param name="sender">The Check</param>
    /// <param name="e">The Click</param>
    protected void chkDenunciaRecibidaCorrespondenciaPorInterna_CheckedChanged(object sender, EventArgs e)
    {
        if (chkDenunciaRecibidaCorrespondenciaPorInterna.Checked == true)
        {
            lblRadicadoCorrespondencia.Visible = true;
            txtRadicadoCorrespondencia.Visible = true;
            lblFechaRadicadoCorrespondencia.Visible = true;
            txtFechaRadicadoCorrespondencia.Visible = true;
            lblHoraRadicadoCorrespondencia.Visible = true;
            txtHoraRadicadoCorrespondencia.Visible = true;
            pnlDatosRadicado.Visible = true;
        }
        else
        {
            lblRadicadoCorrespondencia.Visible = false;
            txtRadicadoCorrespondencia.Visible = false;
            lblFechaRadicadoCorrespondencia.Visible = false;
            txtFechaRadicadoCorrespondencia.Visible = false;
            lblHoraRadicadoCorrespondencia.Visible = false;
            txtHoraRadicadoCorrespondencia.Visible = false;
            pnlDatosRadicado.Visible = false;
        }
    }

    /// <summary>
    /// Método que almacena en sesión el nombre y los bytes del archivo Denuncia
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnDenuncia_Click(object sender, ImageClickEventArgs e)
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
        {
            this.toolBar.OcultarBotonBuscar(false);
        }

        if (fuDenuncia.HasFile)
        {
            lblValidaDenuncia.Text = string.Empty;
            string pdfExt = System.IO.Path.GetExtension(fuDenuncia.PostedFile.FileName);
            if (pdfExt.ToUpper() == ".PDF")
            {
                toolBar.LipiarMensajeError();
                int pdfTam = fuDenuncia.PostedFile.ContentLength;
                if (pdfTam < (2048 * 1024))
                {
                    this.Session["nombreArchivoDenuncia"] = fuDenuncia.FileName;
                    this.Session["bytesArchivoDenuncia"] = fuDenuncia.FileBytes;
                    lblDenuncia.Text = Convert.ToString(this.Session["nombreArchivoDenuncia"]);
                    this.txtDenuncia.Text = Convert.ToString(this.Session["nombreArchivoDenuncia"]);
                    this.msgError.Style.Add("display", "none");
                    toolBar.LipiarMensajeError();
                }
                else
                {
                    toolBar.MostrarMensajeError("El tamano del archivo PDF  máximo es de 2048 KB");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
            }
        }
        else
        {
            lblValidaDenuncia.Visible = true;
        }
    }

    /// <summary>
    /// Método que almacena en sesión el nombre y los bytes del archivo Cedula
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnCedula_Click(object sender, ImageClickEventArgs e)
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
        {
            this.toolBar.OcultarBotonBuscar(false);
        }

        if (fuCedula.HasFile)
        {
            lblValidaCedula.Text = string.Empty;
            string pdfExt = System.IO.Path.GetExtension(fuCedula.PostedFile.FileName);
            if (pdfExt.ToUpper() == ".PDF")
            {
                toolBar.LipiarMensajeError();
                int pdfTam = fuCedula.PostedFile.ContentLength;
                if (pdfTam < (2048 * 1024))
                {
                    this.Session["nombreArchivoCedula"] = fuCedula.FileName;
                    this.Session["bytesArchivoCedula"] = fuCedula.FileBytes;
                    lblCedula.Text = Convert.ToString(this.Session["nombreArchivoCedula"]);
                    this.txtCedula.Text = Convert.ToString(this.Session["nombreArchivoCedula"]);
                    this.msgError.Style.Add("display", "none");
                }
                else
                {
                    toolBar.MostrarMensajeError("El tamano del archivo PDF  máximo es de 2048 KB");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
            }
        }
        else
        {
            lblValidaCedula.Visible = true;
        }
    }

    /// <summary>
    /// Método que almacena en sesión el nombre y los bytes del archivo Defunción
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnDefuncion_Click(object sender, ImageClickEventArgs e)
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
        {
            this.toolBar.OcultarBotonBuscar(false);
        }

        string pdfExt = System.IO.Path.GetExtension(fuDefuncion.PostedFile.FileName);
        if (pdfExt.ToUpper() == ".PDF")
        {
            toolBar.LipiarMensajeError();
            int pdfTam = fuDefuncion.PostedFile.ContentLength;
            if (pdfTam < (2048 * 1024))
            {
                this.Session["nombreArchivoDefuncion"] = fuDefuncion.FileName;
                this.Session["bytesArchivoDefuncion"] = fuDefuncion.FileBytes;
                lblDefuncion.Text = Convert.ToString(this.Session["nombreArchivoDefuncion"]);
            }
            else
            {
                toolBar.MostrarMensajeError("El tamano del archivo PDF  máximo es de 2048 KB");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
        }
    }

    #endregion

    #region METODOS
    public void AceptarTerminosYCondiciones(Boolean estado)
    {
        if (estado)
        {
            txtDescripcionDenuncia.Enabled = true;
            ddlIdRegionalUbicacion.Enabled = true;
            fuCedula.Enabled = true;
            btnCedula.Enabled = true;
            fuDefuncion.Enabled = true;
            btnDefuncion.Enabled = true;
            btnDenuncia.Enabled = true;
            fuDenuncia.Enabled = true;
            txtObservacionesDocumento.Enabled = true;
            fuDocumentoOtros.Enabled = true;
            btnNuevoDocumento.Enabled = true;
            chkDenunciaRecibidaCorrespondenciaPorInterna.Enabled = true;

            this.CargarRegionales();
        }
        else
        {
            txtDescripcionDenuncia.Enabled = false;
            ddlIdRegionalUbicacion.Enabled = false;
            fuCedula.Enabled = false;
            btnCedula.Enabled = false;
            fuDefuncion.Enabled = false;
            btnDefuncion.Enabled = false;
            fuDenuncia.Enabled = false;
            btnDenuncia.Enabled = false;
            txtObservacionesDocumento.Enabled = false;
            fuDocumentoOtros.Enabled = false;
            btnNuevoDocumento.Enabled = false;
            chkDenunciaRecibidaCorrespondenciaPorInterna.Enabled = false;
            ddlIdRegionalUbicacion.Items.Clear();
        }
    }

    /// <summary>
    /// Método para guardar un RegistroDenuncia Nuevo o Editado.
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (chkAcepto.Checked == false)
            {
                lblValidaAcepto.Visible = true;
                return;
            }

            var culture = System.Globalization.CultureInfo.CurrentCulture;
            int vResultado;
            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();

            //string RadicadoDenuncia = this.RadicadoDenuncia();

            //vRegistroDenuncia.RadicadoDenuncia = RadicadoDenuncia;
            vRegistroDenuncia.Acepto = (!chkAcepto.Checked) ? false : true;
            vRegistroDenuncia.DescripcionDenuncia = Convert.ToString(txtDescripcionDenuncia.Text);
            vRegistroDenuncia.IdRegionalUbicacion = Convert.ToInt32(ddlIdRegionalUbicacion.SelectedValue);

            string variable = GetSessionUser().Rol;

            if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
            {
                if (chkDenunciaRecibidaCorrespondenciaPorInterna.Checked)
                {
                    vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna = "S";
                }
                else
                {
                    vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna = "N";
                }

                if (txtRadicadoCorrespondencia.Text != null)
                {
                    vRegistroDenuncia.RadicadoCorrespondencia = txtRadicadoCorrespondencia.Text;
                }

                if (txtFechaRadicadoCorrespondencia.Date != null)
                {
                    vRegistroDenuncia.FechaRadicadoCorrespondencia = Convert.ToDateTime(txtFechaRadicadoCorrespondencia.Date);
                }

                if (txtHoraRadicadoCorrespondencia.Text != string.Empty)
                {
                    vRegistroDenuncia.HoraRadicadoCorrespondencia = txtHoraRadicadoCorrespondencia.Text;
                }
            }

            vRegistroDenuncia.IdEstado = 1;
            vRegistroDenuncia.FechaRadicadoDenuncia = DateTime.Now;

            if (Request.QueryString["oP"] == "E")
            {
                int vIdTipoDocIdentifica = 0;
                string vNumeroIdentificacion = null;

                if (txtIdTipoDocIdentifica.Text == "CC")
                {
                    vIdTipoDocIdentifica = 1;
                }
                else if (txtIdTipoDocIdentifica.Text == "CE")
                {
                    vIdTipoDocIdentifica = 2;
                }
                else
                {
                    vIdTipoDocIdentifica = 7;
                }

                if (txtNumeroIdentificacion.Text != string.Empty)
                {
                    vNumeroIdentificacion = Convert.ToString(txtNumeroIdentificacion.Text);
                }

                //hfIdTercero.Value = vRegistroDenuncia.IdTercero.ToString();
                //this.SetSessionParameter("RegistroDenuncia.IdTercero", hfIdTercero.Value);

                //vRegistroDenuncia.IdTercero = Convert.ToInt32(hfIdTercero.Value);
                vRegistroDenuncia.IdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);
                vRegistroDenuncia.UsuarioModifica = GetSessionUser().NombreUsuario;
                this.InformacionAudioria(vRegistroDenuncia, this.PageName, this.vSolutionPage);
                vResultado = this.vMostrencosService.ModificarRegistroDenuncia(vRegistroDenuncia);

                if (vResultado != null)
                {
                    int vResultadoArchivo = 0;
                    DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();

                    if (this.Session["nombreArchivoDenuncia"] != null && this.Session["bytesArchivoDenuncia"] != null)
                    {
                        string vNombreArchivoDenuncia = Convert.ToString(Session["nombreArchivoDenuncia"]);
                        byte[] vbytesArchivoDenuncia = (byte[])Session["bytesArchivoDenuncia"];

                        if (!this.GuardarArchivos(vResultado, 0, vNombreArchivoDenuncia, vbytesArchivoDenuncia))
                        {
                            return;
                        }

                        //vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vResultado;
                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 1;
                        vDocumentosSolicitadosDenunciaBien.UsuarioModifica = Convert.ToString(GetSessionUser().IdUsuario.ToString());
                        vDocumentosSolicitadosDenunciaBien.RutaArchivo = vResultado + @"\" + vNombreArchivoDenuncia;

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                    }

                    if (this.Session["nombreArchivoCedula"] != null && this.Session["bytesArchivoCedula"] != null)
                    {
                        string vNombreArchivoCedula = Convert.ToString(Session["nombreArchivoCedula"]);
                        byte[] vbytesArchivoCedula = (byte[])Session["bytesArchivoCedula"];

                        if (!this.GuardarArchivos(vResultado, 0, vNombreArchivoCedula, vbytesArchivoCedula))
                        {
                            return;
                        }

                        //vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vResultado;
                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);
                        vDocumentosSolicitadosDenunciaBien.UsuarioModifica = Convert.ToString(GetSessionUser().IdUsuario.ToString());
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 2;
                        vDocumentosSolicitadosDenunciaBien.RutaArchivo = vResultado + @"\" + vNombreArchivoCedula;

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                    }

                    if (this.Session["nombreArchivoDefuncion"] != null && this.Session["bytesArchivoDefuncion"] != null)
                    {
                        string vNombreArchivoDefuncion = Convert.ToString(Session["nombreArchivoDefuncion"]);
                        byte[] vbytesArchivoDefuncion = (byte[])Session["bytesArchivoDefuncion"];
                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

                        if (!this.GuardarArchivoDefuncion(vDocumentosSolicitadosDenunciaBien.IdDenunciaBien, 0, vNombreArchivoDefuncion, vbytesArchivoDefuncion))
                        {
                            return;
                        }

                        vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 1;
                        vDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
                        vDocumentosSolicitadosDenunciaBien.NombreArchivo = "Defuncion.pdf";

                        //vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);
                        vDocumentosSolicitadosDenunciaBien.UsuarioModifica = Convert.ToString(GetSessionUser().IdUsuario.ToString());
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 3;
                        vDocumentosSolicitadosDenunciaBien.RutaArchivo = vDocumentosSolicitadosDenunciaBien.IdDenunciaBien + @"\" + vNombreArchivoDefuncion;

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                    }

                    if (this.Session["lstOtrosDocumentos"] != null)
                    {
                        List<DocumentosSolicitadosDenunciaBien> lstDocumentosOtros = (List<DocumentosSolicitadosDenunciaBien>)Session["lstOtrosDocumentos"];
                        if (lstDocumentosOtros != null && lstDocumentosOtros.Count > 0)
                        {
                            vDocumentosSolicitadosDenunciaBien.OtrosDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                            vDocumentosSolicitadosDenunciaBien.OtrosDocumentos = lstDocumentosOtros;
                        }

                        if (lstDocumentosOtros != null && lstDocumentosOtros.Count > 0)
                        {
                            vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);
                            vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 4;
                            vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 3;
                            vDocumentosSolicitadosDenunciaBien.UsuarioCrea = Convert.ToString(GetSessionUser().IdUsuario.ToString());

                            this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                            /// PRIMERO ELIMINA LOS DOCUMENTOS DE TIPO OTRO
                            this.vMostrencosService.EliminarDenunciaDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);
                            /// SE INSERTAN LOS DOCUMENTOS TIPO OTROS DE NUEVO
                            vResultadoArchivo = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienOtros(vDocumentosSolicitadosDenunciaBien);


                            if (vResultadoArchivo == 0)
                            {
                                this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                            }
                            else if (vResultadoArchivo == 1)
                            {
                                foreach (DocumentosSolicitadosDenunciaBien vOtrosDocumentos in lstDocumentosOtros)
                                {
                                    if (!vOtrosDocumentos.ExisteArchivo)
                                    {
                                        string rutaParcial = string.Format("{0}\\{1}", vResultado, System.IO.Path.GetFileName(vOtrosDocumentos.NombreArchivo));
                                        string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistroDenuncia/Archivos/"), vResultado);
                                        string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileName(vOtrosDocumentos.NombreArchivo));

                                        bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                                        if (!existeCarpeta)
                                        {
                                            System.IO.Directory.CreateDirectory(carpetaBase);
                                        }

                                        File.WriteAllBytes(filePath, vOtrosDocumentos.bytes);
                                    }
                                }

                                //this.EnviarNotificacion(vRegistroDenuncia);
                                this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", vRegistroDenuncia.IdDenunciaBien);
                                this.SetSessionParameter("RegistroDenuncia.RadicadoDenuncia", vRegistroDenuncia.RadicadoDenuncia);
                                this.SetSessionParameter("RegistroDenuncia.Guardado", "2");
                                this.RemoveSessionParameter("RegistroDenuncia.IdTercero");
                                this.NavigateTo(SolutionPage.Detail);
                            }
                        }
                    }

                    //this.EnviarNotificacion(vRegistroDenuncia);
                    this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", vRegistroDenuncia.IdDenunciaBien);
                    this.SetSessionParameter("RegistroDenuncia.RadicadoDenuncia", vRegistroDenuncia.RadicadoDenuncia);
                    this.SetSessionParameter("RegistroDenuncia.Guardado", "2");
                    this.RemoveSessionParameter("RegistroDenuncia.IdTercero");
                    this.NavigateTo(SolutionPage.Detail);

                }
                else
                {
                    this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
            }
            else
            {
                string RadicadoDenuncia = this.RadicadoDenuncia();
                vRegistroDenuncia.RadicadoDenuncia = RadicadoDenuncia;
                int vIdTercero = Convert.ToInt32(GetSessionParameter("RegistroDenuncia.IdTercero"));
                //this.RemoveSessionParameter("RegistroDenuncia.IdTercero");
                vRegistroDenuncia.IdTercero = vIdTercero;
                vRegistroDenuncia.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
                this.InformacionAudioria(vRegistroDenuncia, this.PageName, this.vSolutionPage);
                vResultado = this.vMostrencosService.InsertarRegistroDenuncia(vRegistroDenuncia);

                vRegistroDenuncia.NombreMostrar = txtPrimerNombre.Text + " " + txtSegundoNombre.Text + " " + txtPrimerApellido.Text + " " + txtSegundoApellido.Text;

                if (vResultado != null)
                {
                    int vResultadoArchivo;
                    int vResultadoHistorico;
                    DocumentosSolicitadosDenunciaBien vDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosDenunciaBien();
                    HistoricoEstadosDenunciaBien vHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();

                    vHistoricoEstadosDenunciaBien.IdDenunciaBien = vResultado;
                    vHistoricoEstadosDenunciaBien.IdEstadoDenuncia = vRegistroDenuncia.IdEstado;
                    vHistoricoEstadosDenunciaBien.IdUsuarioCrea = Convert.ToInt32(GetSessionUser().IdUsuario.ToString());
                    vHistoricoEstadosDenunciaBien.UsuarioCrea = this.GetSessionUser().NombreUsuario;

                    vResultadoHistorico = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(vHistoricoEstadosDenunciaBien);

                    if (this.Session["nombreArchivoDenuncia"] != null && this.Session["bytesArchivoDenuncia"] != null)
                    {
                        string vNombreArchivoDenuncia = Convert.ToString(Session["nombreArchivoDenuncia"]);
                        byte[] vbytesArchivoDenuncia = (byte[])Session["bytesArchivoDenuncia"];

                        if (!this.GuardarArchivos(vResultado, 0, vNombreArchivoDenuncia, vbytesArchivoDenuncia))
                        {
                            return;
                        }

                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vResultado;
                        vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 1;
                        vDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 1;
                        vDocumentosSolicitadosDenunciaBien.NombreArchivo = "Denuncia.pdf";
                        vDocumentosSolicitadosDenunciaBien.RutaArchivo = vResultado + @"\" + vNombreArchivoDenuncia;

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                    }

                    if (this.Session["nombreArchivoCedula"] != null && this.Session["bytesArchivoCedula"] != null)
                    {
                        string vNombreArchivoCedula = Convert.ToString(Session["nombreArchivoCedula"]);
                        byte[] vbytesArchivoCedula = (byte[])Session["bytesArchivoCedula"];

                        if (!this.GuardarArchivos(vResultado, 0, vNombreArchivoCedula, vbytesArchivoCedula))
                        {
                            return;
                        }

                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vResultado;
                        vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 1;
                        vDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 2;
                        vDocumentosSolicitadosDenunciaBien.NombreArchivo = "Cedula.pdf";
                        vDocumentosSolicitadosDenunciaBien.RutaArchivo = vResultado + @"\" + vNombreArchivoCedula;

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                    }

                    if (this.Session["nombreArchivoDefuncion"] != null && this.Session["bytesArchivoDefuncion"] != null)
                    {
                        string vNombreArchivoDefuncion = Convert.ToString(Session["nombreArchivoDefuncion"]);
                        byte[] vbytesArchivoDefuncion = (byte[])Session["bytesArchivoDefuncion"];

                        if (!this.GuardarArchivos(vResultado, 0, vNombreArchivoDefuncion, vbytesArchivoDefuncion))
                        {
                            return;
                        }

                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vResultado;
                        vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 1;
                        vDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 3;
                        vDocumentosSolicitadosDenunciaBien.NombreArchivo = "Defuncion.pdf";
                        vDocumentosSolicitadosDenunciaBien.RutaArchivo = vResultado + @"\" + vNombreArchivoDefuncion;

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                    }

                    if (this.Session["lstOtrosDocumentos"] != null)
                    {
                        List<DocumentosSolicitadosDenunciaBien> lstDocumentosOtros = (List<DocumentosSolicitadosDenunciaBien>)Session["lstOtrosDocumentos"];
                        if (lstDocumentosOtros != null && lstDocumentosOtros.Count > 0)
                        {
                            vDocumentosSolicitadosDenunciaBien.OtrosDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                            vDocumentosSolicitadosDenunciaBien.OtrosDocumentos = lstDocumentosOtros;
                        }
                        else
                        {
                            this.EnviarNotificacion(vRegistroDenuncia);
                            this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", vRegistroDenuncia.IdDenunciaBien);
                            this.SetSessionParameter("RegistroDenuncia.RadicadoDenuncia", vRegistroDenuncia.RadicadoDenuncia);
                            this.SetSessionParameter("RegistroDenuncia.Guardado", "1");
                            this.RemoveSessionParameter("RegistroDenuncia.IdTercero");
                            this.NavigateTo(SolutionPage.Detail);
                        }

                        vDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vResultado;
                        vDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = 4;
                        vDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = 3;
                        vDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().IdUsuario.ToString();

                        this.InformacionAudioria(vDocumentosSolicitadosDenunciaBien, this.PageName, this.vSolutionPage);
                        vResultadoArchivo = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienOtros(vDocumentosSolicitadosDenunciaBien);

                        if (vResultadoArchivo == 0)
                        {
                            this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                        else if (vResultadoArchivo == 1)
                        {
                            foreach (DocumentosSolicitadosDenunciaBien vOtrosDocumentos in lstDocumentosOtros)
                            {
                                string rutaParcial = string.Format("{0}\\{1}", vResultado, System.IO.Path.GetFileName(vOtrosDocumentos.NombreArchivo));
                                string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistroDenuncia/Archivos/"), vResultado);
                                string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileName(vOtrosDocumentos.NombreArchivo));

                                bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                                if (!existeCarpeta)
                                {
                                    System.IO.Directory.CreateDirectory(carpetaBase);
                                }

                                File.WriteAllBytes(filePath, vOtrosDocumentos.bytes);
                            }

                            this.EnviarNotificacion(vRegistroDenuncia);
                            this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", vRegistroDenuncia.IdDenunciaBien);
                            this.SetSessionParameter("RegistroDenuncia.RadicadoDenuncia", vRegistroDenuncia.RadicadoDenuncia);
                            this.SetSessionParameter("RegistroDenuncia.Guardado", "1");
                            this.RemoveSessionParameter("RegistroDenuncia.IdTercero");
                            this.NavigateTo(SolutionPage.Detail);
                        }
                    }

                    this.EnviarNotificacion(vRegistroDenuncia);
                    this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", vRegistroDenuncia.IdDenunciaBien);
                    this.SetSessionParameter("RegistroDenuncia.RadicadoDenuncia", vRegistroDenuncia.RadicadoDenuncia);
                    this.SetSessionParameter("RegistroDenuncia.Guardado", "1");
                    this.RemoveSessionParameter("RegistroDenuncia.IdTercero");
                    this.NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    this.toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los controles y eventos iniciales.
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            if (Request.QueryString["oP"] != "E")
            {
                this.toolBar.eventoLimpiar += new ToolBarDelegate(this.btnLimpiarPantalla_Click);
            }

            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.msgError.Style.Add("display", "none");

            this.toolBar.EstablecerTitulos("Gesti&oacute;n de denuncias", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos de una RegistroDenuncia por el Id.
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("RegistroDenuncia.IdDenunciaBien"));
            this.RemoveSessionParameter("RegistroDenuncia.Id");

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarRegistroDenuncia(vIdDenunciaBien);

            btnConsultar.Visible = false;
            ddlIdTipoDocIdentifica.Visible = false;
            txtIdTipoDocIdentifica.Visible = true;
            txtNumeroIdentificacion.Enabled = false;
            txtDescripcionDenuncia.Enabled = true;
            ddlIdRegionalUbicacion.Enabled = true;
            fuDenuncia.Enabled = true;
            fuCedula.Enabled = true;
            fuDefuncion.Enabled = true;
            txtObservacionesDocumento.Enabled = true;
            fuDocumentoOtros.Enabled = true;
            chkDenunciaRecibidaCorrespondenciaPorInterna.Enabled = true;
            lblRadicadoCorrespondencia.Visible = true;
            txtRadicadoCorrespondencia.Visible = true;
            lblFechaRadicadoCorrespondencia.Visible = true;
            txtFechaRadicadoCorrespondencia.Visible = true;
            lblHoraRadicadoCorrespondencia.Visible = true;
            txtHoraRadicadoCorrespondencia.Visible = true;

            hfIdDenunciaBien.Value = vRegistroDenuncia.IdDenunciaBien.ToString();
            txtIdTipoDocIdentifica.Text = vRegistroDenuncia.CodDocumento.ToString();
            txtNumeroIdentificacion.Text = vRegistroDenuncia.NUMEROIDENTIFICACION;
            txtIdTipoPersona.Text = vRegistroDenuncia.NombreTipoPersona;
            txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
            txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
            txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
            txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
            txtIdDepartamento.Text = vRegistroDenuncia.NombreDepartamento;
            txtIdMunicipio.Text = vRegistroDenuncia.NombreMunicipio;
            if (vRegistroDenuncia.IdZona == "1")
            {
                txtIdZona.Text = "Urbano";
            }
            else if (vRegistroDenuncia.IdZona == "2")
            {
                txtIdZona.Text = "Rural";
            }

            txtDireccion.Text = vRegistroDenuncia.Direccion;
            txtIndicativo.Text = vRegistroDenuncia.Indicativo;
            txtTelefono.Text = vRegistroDenuncia.Telefono;
            txtCelular.Text = vRegistroDenuncia.Celular;
            txtCorreoElectronico.Text = vRegistroDenuncia.CORREOELECTRONICO;
            if (vRegistroDenuncia.Acepto == true)
            {
                chkAcepto.Checked = true;
            }

            txtDescripcionDenuncia.Text = vRegistroDenuncia.DescripcionDenuncia;

            if (vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna == "S")
            {
                chkDenunciaRecibidaCorrespondenciaPorInterna.Checked = true;
                txtRadicadoCorrespondencia.Text = vRegistroDenuncia.RadicadoCorrespondencia;
                txtFechaRadicadoCorrespondencia.Date = Convert.ToDateTime(vRegistroDenuncia.FechaRadicadoCorrespondencia);
                txtHoraRadicadoCorrespondencia.Text = vRegistroDenuncia.HoraRadicadoCorrespondencia;

                pnlDatosRadicado.Visible = true;
            }
            else
            {
                pnlDatosRadicado.Visible = false;
            }

            lblDenuncia.Text = Convert.ToString(GetSessionParameter("RegistroDenuncia.Denuncia"));
            lblCedula.Text = Convert.ToString(GetSessionParameter("RegistroDenuncia.Cedula"));
            lblDefuncion.Text = Convert.ToString(GetSessionParameter("RegistroDenuncia.Defuncion"));

            txtDenuncia.Text = lblDenuncia.Text;
            txtCedula.Text = lblCedula.Text;

            this.RemoveSessionParameter("RegistroDenuncia.Denuncia");
            this.RemoveSessionParameter("RegistroDenuncia.Cedula");
            this.RemoveSessionParameter("RegistroDenuncia.Defuncion");

            this.CargarRegionales();
            ddlIdRegionalUbicacion.SelectedValue = Convert.ToString(vRegistroDenuncia.IdRegionalUbicacion);
            this.CargarOtrosDocumentos();

            //lblDenuncia.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.

            ((Label)this.toolBar.FindControl("lblAuditoria")).Text = this.GetAuditLabel(vRegistroDenuncia.UsuarioCrea, vRegistroDenuncia.FechaCrea, vRegistroDenuncia.UsuarioModifica, vRegistroDenuncia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos en los controles.
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            string vTiposDocumentos = ConfigurationManager.AppSettings["TiposDocumentoGlobal"];

            List<TiposDocumentosGlobal> vLstTipoIdentificacion = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(vTiposDocumentos);
            ddlIdTipoDocIdentifica.Items.Clear();
            ddlIdTipoDocIdentifica.DataSource = vLstTipoIdentificacion;
            ddlIdTipoDocIdentifica.DataValueField = "IdTipoDocumento";
            ddlIdTipoDocIdentifica.DataTextField = "CodDocumento";
            ddlIdTipoDocIdentifica.DataBind();
            ddlIdTipoDocIdentifica.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para realizar la búsqueda
    /// </summary>
    private void Buscar()
    {
        try
        {
            int vIdTipoDocIdentifica = 0;
            string vNumeroIdentificacion = null;

            if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
            {
                if (ddlIdTipoDocIdentifica.SelectedValue != "-1")
                {
                    vIdTipoDocIdentifica = Convert.ToInt32(ddlIdTipoDocIdentifica.SelectedValue);
                }

                if (txtNumeroIdentificacion.Text != string.Empty)
                {
                    vNumeroIdentificacion = Convert.ToString(txtNumeroIdentificacion.Text);
                }
            }
            else if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
            {
                vIdTipoDocIdentifica = GetSessionUser().IdTipoDocumento;
                vNumeroIdentificacion = GetSessionUser().NumeroDocumento;
            }

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarRegistroDenunciante(vIdTipoDocIdentifica, vNumeroIdentificacion);
            if (vRegistroDenuncia.IdTercero == null)
            {
                this.msgError.InnerText = "No se encontraron datos, debe registrarse como tercero";
                this.msgError.Style.Add("display", "block");
                this.lbRegistrarTercero.Visible = true;
            }
            else
            {
                this.lbRegistrarTercero.Visible = false;
                this.msgError.Style.Add("display", "none");

                hfIdTercero.Value = vRegistroDenuncia.IdTercero.ToString();
                this.SetSessionParameter("RegistroDenuncia.IdTercero", hfIdTercero.Value);
                txtIdTipoPersona.Text = vRegistroDenuncia.NombreTipoPersona;
                if (vIdTipoDocIdentifica == 7)
                {
                    lblPrimerNombre.Text = "Razón social";
                    txtPrimerNombre.Text = vRegistroDenuncia.RazonSocial;
                    lblSegundoNombre.Visible = false;
                    txtSegundoNombre.Visible = false;
                    lblPrimerApellido.Visible = false;
                    txtPrimerApellido.Visible = false;
                    lblSegundoApellido.Visible = false;
                    txtSegundoApellido.Visible = false;
                    lblCedulaDenunciante.Text = "RUT *";
                }
                else
                {
                    lblPrimerNombre.Text = "Primer nombre";
                    txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                    lblSegundoNombre.Visible = true;
                    txtSegundoNombre.Visible = true;
                    lblPrimerApellido.Visible = true;
                    txtPrimerApellido.Visible = true;
                    lblSegundoApellido.Visible = true;
                    txtSegundoApellido.Visible = true;
                    lblCedulaDenunciante.Text = "Cédula de ciudadania del denunciante *";
                }

                txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                txtIdDepartamento.Text = vRegistroDenuncia.NombreDepartamento;
                txtIdMunicipio.Text = vRegistroDenuncia.NombreMunicipio;
                if (vRegistroDenuncia.IdZona == "1")
                {
                    txtIdZona.Text = "Urbano";
                }
                else if (vRegistroDenuncia.IdZona == "2")
                {
                    txtIdZona.Text = "Rural";
                }

                txtDireccion.Text = vRegistroDenuncia.Direccion;
                txtIndicativo.Text = vRegistroDenuncia.Indicativo;
                txtTelefono.Text = vRegistroDenuncia.Telefono;
                txtCelular.Text = vRegistroDenuncia.Celular;
                txtCorreoElectronico.Text = vRegistroDenuncia.CORREOELECTRONICO;
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga las regionales
    /// </summary>
    private void CargarRegionales()
    {
        try
        {
            List<Regionales> vLstRegionales = this.vMostrencosService.ConsultarRegionales();
            ddlIdRegionalUbicacion.Items.Clear();
            ddlIdRegionalUbicacion.DataSource = vLstRegionales;
            ddlIdRegionalUbicacion.DataValueField = "IdRegional";
            ddlIdRegionalUbicacion.DataTextField = "NombreRegional";
            ddlIdRegionalUbicacion.DataBind();
            ddlIdRegionalUbicacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que guarda los archivos PDF.
    /// </summary>
    /// <param name="pIdDenunciaBien">The IdDenunciaBien</param>
    /// <param name="pPosition">The Position</param>
    /// <param name="pNombreArchivo">The NombreArchivo</param>
    /// <param name="pbytesArchivo">The bytesArchivo</param>
    /// <returns>Resultado de la Operación</returns>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition, string pNombreArchivo, byte[] pbytesArchivo)
    {
        try
        {
            string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileName(pNombreArchivo));
            string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistroDenuncia/Archivos/"), pIdDenunciaBien);
            string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileName(pNombreArchivo));

            bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

            if (!existeCarpeta)
            {
                System.IO.Directory.CreateDirectory(carpetaBase);
            }

            File.WriteAllBytes(filePath, pbytesArchivo);

            return true;
        }
        catch (HttpRequestValidationException)
        {
            this.toolBar.MostrarMensajeError("Se detectó un posible archivo peligroso.");
            return false;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError("Error cargando archivo. " + ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Método que guarda los archivos PDF.
    /// </summary>
    /// <param name="pIdDenunciaBien">The IdDenunciaBien</param>
    /// <param name="pPosition">The Position</param>
    /// <param name="pNombreArchivo">The NombreArchivo</param>
    /// <param name="pbytesArchivo">The bytesArchivo</param>
    /// <returns>Resultado de la Operación</returns>
    private bool GuardarArchivoDefuncion(int? pIdDenunciaBien, int pPosition, string pNombreArchivo, byte[] pbytesArchivo)
    {
        try
        {
            string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileName(pNombreArchivo));
            string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistroDenuncia/Archivos/"), pIdDenunciaBien);
            string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileName(pNombreArchivo));

            bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

            if (!existeCarpeta)
            {
                System.IO.Directory.CreateDirectory(carpetaBase);
            }

            File.WriteAllBytes(filePath, pbytesArchivo);

            return true;
        }
        catch (HttpRequestValidationException)
        {
            this.toolBar.MostrarMensajeError("Se detectó un posible archivo peligroso.");
            return false;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError("Error cargando archivo. " + ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Envia notificación al guardar el registro de la denuncia
    /// </summary>
    /// <param name="pRegistroDenuncia">Entidad pRegistroDenuncia</param>
    private void EnviarNotificacion(RegistroDenuncia pRegistroDenuncia)
    {
        int Regional = Convert.ToInt32(ddlIdRegionalUbicacion.SelectedValue);
        string keys = string.Empty;
        string vCorreoCoordinadorJuridico = string.Empty;

        var vRolesFuncion = vMostrencosService.ConsultarRolesPorProgramaFuncion("GESTIÓN DE DENUNCIAS", ConfigurationManager.AppSettings["RolCoordinadorJuridico"]);

        if (vRolesFuncion.Any())
        {
            foreach (Rol vRol in vRolesFuncion)
            {
                if(!string.IsNullOrEmpty(keys))
                {
                    keys = string.Concat(keys,",");
                }

                var vUsuarioRol = Roles.GetUsersInRole(vRol.NombreRol);
                if (vUsuarioRol != null)
                {
                    foreach (string item in vUsuarioRol)
                    {
                        if(Membership.GetUser(item) != null)
                        {
                            keys = string.Concat(keys, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? "" : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
                        }
                    }

                    keys = (!string.IsNullOrEmpty(keys)) ? keys.Remove(keys.Length - 2) : keys;

                    vCorreoCoordinadorJuridico = vMostrencosService.ConsultarCorreoCoordinadorJuridico(Regional, keys);
                }
            }
        }

        if (vCorreoCoordinadorJuridico == string.Empty)
        {
            string keysAdmin = string.Empty;

            var vRolesFuncionAdmin = vMostrencosService.ConsultarRolesPorProgramaFuncion("GESTIÓN DE DENUNCIAS", ConfigurationManager.AppSettings["RolAdministrador"]);
            if (vRolesFuncionAdmin.Any())
            {
                foreach (Rol vRolAdmin in vRolesFuncionAdmin)
                {
                    if(!string.IsNullOrEmpty(keysAdmin))
                    {
                        keysAdmin = string.Concat(keysAdmin,",");
                    }

                    var vAdminRol = Roles.GetUsersInRole(vRolAdmin.NombreRol);

                    foreach (string item in vAdminRol)
                    {
                        if(Membership.GetUser(item) != null)
                        {
                            keysAdmin = string.Concat(keysAdmin, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? "" : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
                        }
                    }

                    keysAdmin = (!string.IsNullOrEmpty(keysAdmin)) ? keysAdmin.Remove(keysAdmin.Length - 2) : keys;

                    vCorreoCoordinadorJuridico = vMostrencosService.ConsultarCorreoAdministrador(keysAdmin);
                }
            }
        }


        //string vCorreoElectronico = GetSessionUser().CorreoElectronico;
        string vCorreoElectronico = txtCorreoElectronico.Text;
        string vEmailPara = vCorreoElectronico;
        string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
        string vAsunto = string.Empty;
        string vMensaje = string.Empty;
        string vMensajeJuridico = string.Empty;
        int vIdUsuario = 0;
        string vArchivo = string.Empty;
        bool vEnviaCorreoUssuario = true;

        vAsunto = HttpContext.Current.Server.HtmlDecode("Nueva asignaci&oacute;n de denuncia");

        string vRadicadoDenuncia = pRegistroDenuncia.RadicadoDenuncia;
        DateTime vFechaRadicado = Convert.ToDateTime(pRegistroDenuncia.FechaRadicadoDenuncia);

        //string vNombreDenunciante = pRegistroDenuncia.NombreMostrar + " " + pRegistroDenuncia.SEGUNDONOMBRE + " " + pRegistroDenuncia.PRIMERAPELLIDO + " " + pRegistroDenuncia.SEGUNDOAPELLIDO;
        string vNombreDenunciante = pRegistroDenuncia.NombreMostrar;

        vMensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B> Recepci&oacute;n de Denuncia </B> </CENTER> </font> ") +
                    "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + vNombreDenunciante + " <br/> <br/> El ICBF ha recibido la denuncia de Bienes Vacantes, Mostrencos y " +
                    "<br/> Vocaciones hereditarias, los datos de la misma son los siguientes:" +
                    "<br/> <br/> <br/> <B> Radicado de la denuncia:  </B>" + "&nbsp;&nbsp;" + vRadicadoDenuncia +
                    "<br/> <br/> <B> Fecha y Hora de radicado:   </B>" + "&nbsp;&nbsp;" + Convert.ToString(vFechaRadicado.ToString() +
                    HttpContext.Current.Server.HtmlDecode("<br/> <br/> Cualquier inquietud, p&oacute;ngase en contacto con Asesor&iacute;a Jur&iacute;dica, en el, " +
                    "<br/> n&uacute;mero telefonico: 4377630 extensi&oacute;n 100149 en la ciudad de Bogot&aacute; D.C. </div>"));

        bool resultUno;
        string vCorreoCoordinador = string.Empty;
        string vNombreCoordinadorJuridico = string.Empty;
        vCorreoCoordinadorJuridico = vCorreoCoordinadorJuridico + ';';
        while (vCorreoCoordinadorJuridico.Length > 0)
        {
            if (vCorreoCoordinadorJuridico.IndexOf(';') > 0)
            {
                vCorreoCoordinador = vCorreoCoordinadorJuridico.Substring(0, vCorreoCoordinadorJuridico.IndexOf(';'));
                vCorreoCoordinadorJuridico = vCorreoCoordinadorJuridico.Substring(vCorreoCoordinadorJuridico.IndexOf(';') + 1, vCorreoCoordinadorJuridico.Length - vCorreoCoordinadorJuridico.IndexOf(';') - 1);
            }
            else
            {
                vCorreoCoordinador = vCorreoCoordinadorJuridico;
                vCorreoCoordinadorJuridico = string.Empty;
            }

            vCorreoCoordinador = vCorreoCoordinador.Replace(" ", "");
            vNombreCoordinadorJuridico = vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoCoordinador);
            vMensajeJuridico = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B> Recepci&oacute;n de Denuncia </B> </CENTER> </font> ") +
                        "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + vNombreCoordinadorJuridico + " <br/> <br/> El ICBF ha recibido la denuncia de Bienes Vacantes, Mostrencos y " +
                        "<br/> Vocaciones hereditarias, los datos de la misma son los siguientes:" +
                        "<br/> <br/> <br/> <B> Radicado de la denuncia:  </B>" + "&nbsp;&nbsp;" + vRadicadoDenuncia +
                        "<br/> <br/> <B> Fecha y Hora de radicado:   </B>" + "&nbsp;&nbsp;" + Convert.ToString(vFechaRadicado.ToString() +
                        HttpContext.Current.Server.HtmlDecode("<br/> <br/> Cualquier inquietud, p&oacute;ngase en contacto con Asesor&iacute;a Jur&iacute;dica, en el, " +
                        "<br/> n&uacute;mero telefonico: 4377630 extensi&oacute;n 100149 en la ciudad de Bogot&aacute; D.C.") +
                        HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para adelantar la gesti&oacute;n necesaria.</div>"));

            if (vCorreoCoordinadorJuridico.IndexOf(';') <= 0)
            {
                vCorreoCoordinadorJuridico = string.Empty;
            }

            if (vEmailPara.ToUpper().Equals(vCorreoCoordinador))
            {
                vEnviaCorreoUssuario = false;
            }

            resultUno = CorreoUtil.EnviarCorreo(vCorreoCoordinador, vNombreOrigen, vAsunto, vMensajeJuridico, vIdUsuario, vArchivo);
        }

        this.SetSessionParameter("CorreoCoordinador", vCorreoCoordinador);

        if (vEnviaCorreoUssuario)
        {
            bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
        }
    }

    /// <summary>
    /// Método que genera el código del Radicado de la Denuncia
    /// </summary>
    /// <returns>Código del Registro de la denuncia</returns>
    private string RadicadoDenuncia()
    {
        int vRegistroDenuncia = this.vMostrencosService.ConsultarConsecutivoDenuncia();
        int vAnio = this.vMostrencosService.ConsultarAnioUltimoRegistro();
        string Consecutivo = string.Empty;
        int Suma = 0;
        string SiglaSolicitud = string.Empty;

        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
        {
            SiglaSolicitud = "RM";
        }
        else
        {
            SiglaSolicitud = "RW";
        }

        string Vigencia = Convert.ToString(DateTime.Now.Year.ToString());
        int AnioActual = DateTime.Now.Year;
        string Regional = Convert.ToString(ddlIdRegionalUbicacion.SelectedValue);

        if (vAnio != AnioActual)
        {
            Consecutivo = "00001";
        }
        else
        {
            if (vRegistroDenuncia < 9)
            {
                Suma = vRegistroDenuncia + 1;
                Consecutivo = "0000" + Convert.ToString(Suma);
            }
            else if (vRegistroDenuncia >= 9 && vRegistroDenuncia < 99)
            {
                Suma = vRegistroDenuncia + 1;
                Consecutivo = "000" + Convert.ToString(Suma);
            }
            else if (vRegistroDenuncia >= 99 && vRegistroDenuncia < 999)
            {
                Suma = vRegistroDenuncia + 1;
                Consecutivo = "00" + Convert.ToString(Suma);
            }
            else if (vRegistroDenuncia >= 999)
            {
                Suma = vRegistroDenuncia + 1;
                Consecutivo = "0" + Convert.ToString(Suma);
            }
        }

        string CodigoRegional = vMostrencosService.ConsultarRegionalPorId(Regional).CodigoRegional;

        if (CodigoRegional.Length < 2)
        {
            CodigoRegional = "0" + CodigoRegional;
        }

        string RadicadoDenuncia = SiglaSolicitud + Vigencia + CodigoRegional + Consecutivo;
        return RadicadoDenuncia;
    }

    /// <summary>
    /// Método que carga el archivo Denuncia en el control para descargarlo
    /// </summary>
    private void CargarDenuncia()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 1);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                this.Session["nombreArchivoDenuncia"] = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString(); ;
                //this.Session["bytesArchivoDenuncia"] = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.tis;
                lblDenuncia.Text = Convert.ToString(this.Session["nombreArchivoDenuncia"]);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Cedula en el control para descargarlo
    /// </summary>
    private void CargarCedula()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 2);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                this.Session["nombreArchivoCedula"] = fuCedula.FileName;
                this.Session["bytesArchivoCedula"] = fuCedula.FileBytes;
                lblCedula.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo.ToString();
            }

        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Defunción en el control para descargarlo
    /// </summary>
    private void CargarDefuncion()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 3);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {

                this.Session["nombreArchivoDefuncion"] = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
                //this.Session["bytesArchivoDefuncion"] = fuDefuncion.FileBytes;
                lblDefuncion.Text = Convert.ToString(this.Session["nombreArchivoDefuncion"]);
                lblDefuncion.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo.ToString();
            }

        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que consulta la lista de documentos de la sección otros
    /// </summary>
    /// <returns>Lista de documentos Otros</returns>
    private List<DocumentosSolicitadosDenunciaBien> CargarOtrosDocumentos()
    {
        List<DocumentosSolicitadosDenunciaBien> vLista = new List<DocumentosSolicitadosDenunciaBien>();
        try
        {
            int vIdDenuncia = Convert.ToInt32(hfIdDenunciaBien.Value);

            vLista = this.vMostrencosService.ConsultarOtrosDocumentos(vIdDenuncia, 4);
            this.Session["lstOtrosDocumentos"] = vLista;

            if (vLista != null)
            {
                gvOtrosDocumentos.DataSource = vLista;
                gvOtrosDocumentos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

        return vLista;
    }

    /// <summary>
    /// Método para consultar usuario de acuerdo a programa/función/rol
    /// </summary>
    /// <param name="pNombreFuncion">The Nombre a filtrar</param>
    /// <returns>EsFlag boolean</returns>
    private bool ConsultarUsuarioProgramaFuncionRol(string pNombreFuncion)
    {
        ////Roles Permitidos
        ////CU 82
        ////Radicador - Administrador - AdministradorSIA: Consultar, Insertar, Editar
        bool EsFlag = false;
        List<Icbf.Seguridad.Entity.ProgramaFuncion> vListProgramaFuncion = new List<Icbf.Seguridad.Entity.ProgramaFuncion>();

        if (this.ViewState["PROGRAMAFUNCION"] == null)
        {
            vListProgramaFuncion = RolUtil.ListarRolesUsuario(GetSessionUser().NombreUsuario, "Mostrencos/RegistroDenuncia");
            this.ViewState["PROGRAMAFUNCION"] = vListProgramaFuncion;
        }
        else
        {
            vListProgramaFuncion = (List<Icbf.Seguridad.Entity.ProgramaFuncion>)this.ViewState["PROGRAMAFUNCION"];
        }

        Icbf.Seguridad.Entity.ProgramaFuncion vProgramaFuncion = vListProgramaFuncion.Find(p => p.NombreFuncion.Trim().ToUpper() == pNombreFuncion.Trim().ToUpper());

        if (vProgramaFuncion != null)
        {
            EsFlag = true;
        }

        return EsFlag;
    }

    protected void ddlIdTipoDocIdentifica_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtNumeroIdentificacion.Text = string.Empty;
        txtIdTipoPersona.Text = string.Empty;
        txtPrimerNombre.Text = string.Empty;
        txtSegundoNombre.Text = string.Empty;
        txtPrimerApellido.Text = string.Empty;
        txtSegundoApellido.Text = string.Empty;
        txtIdDepartamento.Text = string.Empty;
        txtIdMunicipio.Text = string.Empty;
        txtIdZona.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        txtIndicativo.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtCelular.Text = string.Empty;
        txtCorreoElectronico.Text = string.Empty;
    }

    #endregion
}