<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RegistroDenuncia_Add" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>
<%--<%@ Register Src="~/General/General/Control/grillaDocumentosFu.ascx" TagPrefix="uc1" TagName="grillaDocumentosFu" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel ID="upOtrosDocumentos" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hfIdDenunciaBien" runat="server" />
            <asp:HiddenField ID="hfIdTercero" runat="server" />
            <table width="100%" style="margin: 10px auto;">
                <tr>
                    <td align="center">
                        <span id="msgError" class="lbE" runat="server" style="display: none;"></span>
                    </td>
                </tr>
            </table>
            <asp:LinkButton runat="server" ID="lbRegistrarTercero" Text="Registrar Tercero" 
                PostBackUrl="~/Page/Proveedor/GestionTercero/List.aspx" Visible="false" CssClass="btnRegistroTercero" ></asp:LinkButton>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información básica
                    </td>
                </tr>
                <tr class="rowA">
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td>Tipo de identificación *
                <asp:CompareValidator runat="server" ID="cvTipoIdentificacion" ControlToValidate="ddlIdTipoDocIdentifica"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnConsultar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                        <asp:CompareValidator runat="server" ID="cvTipoIdentificacionGuardar" ControlToValidate="ddlIdTipoDocIdentifica"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                            ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                    </td>
                    <td>Número de identificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacion" ControlToValidate="txtNumeroIdentificacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnConsultar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacionGuardar" ControlToValidate="txtNumeroIdentificacion"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdTipoDocIdentifica" AutoPostBack="true"
                            Width="163px" OnSelectedIndexChanged="ddlIdTipoDocIdentifica_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" ID="txtIdTipoDocIdentifica" Enabled="false" Visible="false"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Visible="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="256"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        <asp:ImageButton ID="btnConsultar" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnConsultar_Click" ValidationGroup="btnConsultar" />
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Tipo de persona o asociación</td>
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtIdTipoPersona" Enabled="false"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:Label runat="server" Text="Primer nombre" ID="lblPrimerNombre"></asp:Label></td>
                    <td>
                        <asp:Label runat="server" Text="Segundo nombre" ID="lblSegundoNombre"></asp:Label></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:Label runat="server" Text="Primer apellido" ID="lblPrimerApellido"></asp:Label></td>
                    <td>
                        <asp:Label runat="server" Text="Segundo apellido" ID="lblSegundoApellido"></asp:Label></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información de residencia
                    </td>
                </tr>
                <tr class="rowA">
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td>Departamento
                    </td>
                    <td>Municipio
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtIdDepartamento" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtIdMunicipio" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Zona</td>
                    <td>Dirección</td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtIdZona" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDireccion" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Indicativo</td>
                    <td>Teléfono</td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtIndicativo" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTelefono" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowA">
                    <td class="auto-style1">Celular</td>
                    <td class="auto-style1">Correo electrónico</td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtCelular" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCorreoElectronico" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Información de la denuncia
                    </td>
                </tr>
                <tr class="rowA">
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">DECLARO VOLUNTARIAMENTE DE BUENA FE, EN NOMBRE PROPIO O DE UN TERCERO, BAJO LA GRAVEDAD DE JURAMENTO,
                LA EXISTENCIA DE UNA VOCACIÓN HEREDITARIA, BIEN VACANTE O MOSTRENCO REPRESENTADA EN LOS DOCUMENTOS
                QUE SE ANEXAN, ASI COMO EL PROPOSITO DE CELEBRAR EL RESPECTIVO CONTRATO DE PARTICIPACION Y EN SENTIDO
                OBTENER LA DECLARACION JUDICIAL DE LOS BIENES DENUNCIADOS HASTA SU ADJUDICACION AL ICBF
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <th></th>
                                <th align="center" style="width: 100px;">
                                    <asp:CheckBox runat="server"
                                        ID="chkAcepto"
                                        Text="Acepto *"
                                        CssClass="chkAcepto" OnCheckedChanged="chkAcepto_CheckedChanged" AutoPostBack="true" />
                                    <asp:Label ID="lblValidaAcepto" runat="server" Visible="false" Text="Campo Requerido" ForeColor="Red" Font-Bold="false"></asp:Label>
                                    <%--<asp:CustomValidator ID="cvAcepto" ErrorMessage="Campo Requerido"
                                ForeColor="Red" ClientValidationFunction="ValidateCheckBox" runat="server" ValidationGroup="btnGuardar" />--%>
                                </th>
                                <th></th>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <th></th>
                                <th align="center" style="width: 161px;">
                                    <%--<asp:CustomValidator ID="cvAcepto" ErrorMessage="Campo Requerido" ControlToValidate="chkAcepto" EnableClientScript="true"
                                ForeColor="Red" OnServerValidate="checkCheckBox" runat="server" ClientValidationFunction="CheckBoxRequired_ClientValidate" />--%>
                                </th>
                                <th></th>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Descripción de la Denuncia *
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Image/btn/helpIcon.png" AlternateText="Descripción de la denuncia: Por  favor realice la descripción que desee para los bienes a denunciar." ToolTip="Descripción de la denuncia: Por  favor realice la descripción que desee para los bienes a denunciar." />
                        <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionDenuncia" ControlToValidate="txtDescripcionDenuncia"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>

                    <td></td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        <asp:TextBox runat="server"
                            ID="txtDescripcionDenuncia"
                            TextMode="MultiLine"
                            Enabled="false"
                            CssClass="DescripcionDenuncia txtArea_validate"
                            MaxLength="512"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftDescripcionDenuncia" runat="server" TargetControlID="txtDescripcionDenuncia"
                            FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Departamento de ubicación *
                        <asp:CompareValidator runat="server" ID="rfvIdRegionalUbicacion" ControlToValidate="ddlIdRegionalUbicacion"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                            ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                    </td>
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdRegionalUbicacion" Enabled="false" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td></td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Documento digital / certificados a adjuntar
                    </td>
                </tr>
                <tr class="rowA">
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td class="tdTitulos">Nombre del Documento
                    </td>
                    <td class="tdTitulos">Documento
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Denuncia con datos del denunciante y de los bienes denunciados *
                    </td>
                    <td>
                        <label style="width: 220px; display: block; padding-right: 10px; font-weight: lighter;">
                            <asp:FileUpload runat="server" ID="fuDenuncia" Enabled="false" data-maxfile="2048" CssClass="multiFileDocBasico jsDocBasico"
                                size="480" Style="width: 480px; max-width: 100%; padding-right: 10px;" />
                        </label>
                        <asp:Label ID="lblValidaDenuncia" runat="server" Visible="false" Text="Campo Requerido" ForeColor="Red" Font-Bold="false"></asp:Label>
                        <asp:ImageButton ID="btnDenuncia" runat="server" ImageUrl="~/Image/btn/apply.png"
                            Height="16px" Width="16px" ToolTip="Agregar Denuncia" Enabled="false" OnClick="btnDenuncia_Click" />
                        <div style="overflow-x: auto; width: 480px; max-width: 480px;">
                            <asp:Label runat="server" ID="lblDenuncia" CssClass="lblDocumentos"></asp:Label>
                        </div>
                        <div style="display: none;">
                            <asp:TextBox ID="txtDenuncia" runat="server"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator runat="server" ID="rfvTxtDenuncia" ControlToValidate="txtDenuncia"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:Label runat="server" Text="Cédula de ciudadania del denunciante *" ID="lblCedulaDenunciante"></asp:Label></td>
                    <td>
                        <label style="width: 220px; display: block; padding-right: 10px; font-weight: lighter;">
                            <asp:FileUpload runat="server" ID="fuCedula" Enabled="false" data-maxfile="2048" CssClass="multiFileDocBasico jsDocBasico"
                                size="480" Style="width: 480px; max-width: 100%; padding-right: 10px;" />
                        </label>
                        <asp:Label ID="lblValidaCedula" runat="server" Visible="false" Text="Campo Requerido" ForeColor="Red" Font-Bold="false"></asp:Label>
                        <div style="overflow-x: auto; width: 480px; max-width: 480px;">
                            <asp:Label runat="server" ID="lblCedula" CssClass="lblDocumentos"></asp:Label>
                        </div>
                        <asp:ImageButton ID="btnCedula" runat="server" ImageUrl="~/Image/btn/apply.png"
                            Height="16px" Width="16px" ToolTip="Agregar Cedula" Enabled="false" OnClick="btnCedula_Click" />
                        <div style="display: none;">
                            <asp:TextBox ID="txtCedula" runat="server"></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCedula" ControlToValidate="txtCedula"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>Certificado de defunción
                    </td>
                    <td>
                        <label style="width: 220px; display: block; padding-right: 10px; font-weight: lighter;">
                            <asp:FileUpload runat="server" ID="fuDefuncion" Enabled="false" data-maxfile="2048" CssClass="multiFileDocBasico jsDocBasico"
                                size="480" Style="width: 480px; max-width: 100%; padding-right: 10px;" />
                        </label>
                        <div style="overflow-x: auto; width: 480px; max-width: 480px;">
                            <asp:Label runat="server" ID="lblDefuncion" CssClass="lblDocumentos"></asp:Label>
                        </div>
                        <asp:ImageButton ID="btnDefuncion" runat="server" ImageUrl="~/Image/btn/apply.png"
                            Height="16px" Width="16px" ToolTip="Agregar Certificado de defunción" Enabled="false" OnClick="btnDefuncion_Click" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" class="tdTitulos">Otros documentos
                    </td>
                </tr>
                <tr class="rowA">
                    <td></td>
                </tr>
                <tr class="rowA">
                    <td class="tdTitulos">Observación al Documento
                    </td>
                    <td class="tdTitulos">Documento
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:HiddenField runat="server" ID="hfIdDocomentoOtros" />
                        <asp:HiddenField runat="server" ID="hfIndiceDocumentosOtros" />
                        <asp:HiddenField ID="hfOtrosDocumentos" runat="server" />
                        <asp:TextBox runat="server" ID="txtObservacionesDocumento" CssClass="DescripcionDenuncia txtArea_validate" MaxLength="512"
                            onkeyDown="checkTextAreaMaxLength(this,event,'512');" TextMode="MultiLine" Width="416px" Enabled="false"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftObservacionesDocumento" runat="server" TargetControlID="txtObservacionesDocumento"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    </td>
                    <td>
                        <%--<uc1:grillaDocumentosFu runat="server" id="OtrosDocumentosFu" />--%>
                        <asp:FileUpload runat="server" ID="fuDocumentoOtros" Enabled="false" />
                        <asp:ImageButton ID="btnNuevoDocumento" runat="server" ImageUrl="~/Image/btn/add.gif"
                            Height="16px" Width="16px" ToolTip="Agregar documento" OnClick="btnNuevoDocumento_Click" Enabled="false" />
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:GridView ID="gvOtrosDocumentos" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" DataKeyNames="IdDenunciaBien"
                            CellPadding="0">
                            <Columns>
                                <asp:BoundField DataField="ObservacionesDocumento" HeaderText="Observación al documento" ItemStyle-Width="25%" />
                                <asp:TemplateField HeaderText="Documento" ItemStyle-Width="70%">
                                    <ItemTemplate>
                                        <div class="lblDocumentos">
                                            <%# Eval("NombreArchivo") %>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Image/btn/delete.gif"
                                            Height="16px" Width="16px" ToolTip="Eliminar"
                                            idDocumentosSolicitadosDenunciaBien='<%# Eval("IdDocumentosSolicitadosDenunciaBien") %>'
                                            indice='<%# Container.DataItemIndex %>'
                                            OnClick="btnEliminarDocumento_Click"
                                            OnClientClick="return eliminarDocumento(this);"
                                            CssClass="imgDeleteArchicoCss" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#81BA3D" />
                        </asp:GridView>
                        <asp:GridView ID="GvDocsOtros" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" DataKeyNames="IdDenunciaBien"
                            CellPadding="0" Visible="false">
                            <Columns>
                                <asp:BoundField DataField="ObservacionesDocumento" HeaderText="Observación al documento" ItemStyle-Width="25%" />
                                <asp:TemplateField HeaderText="Documento" ItemStyle-Width="70%">
                                    <ItemTemplate>
                                        <div class="lblDocumentos">
                                            <%# Eval("NombreArchivo") %>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Image/btn/delete.gif"
                                            Height="16px" Width="16px" ToolTip="Eliminar"
                                            idDocumentosSolicitadosDenunciaBien='<%# Eval("IdDocumentosSolicitadosDenunciaBien") %>'
                                            indice='<%# Container.DataItemIndex %>'
                                            OnClick="btnEliminarDocumento_Click"
                                            OnClientClick="return eliminarDocumento(this);"
                                            CssClass="imgDeleteArchicoCss" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#81BA3D" />
                        </asp:GridView>
                    </td>
                </tr>
                <asp:Panel runat="server" Visible="false" ID="pnlInformacionCorrespondencia">
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de correspondencia ICBF
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:CheckBox runat="server"
                                ID="chkDenunciaRecibidaCorrespondenciaPorInterna"
                                Text="¿Denuncia recibida por correspondencia interna? *"
                                Enabled="false"
                                CssClass="cbRegistroDenuncia"
                                AutoPostBack="true" OnCheckedChanged="chkDenunciaRecibidaCorrespondenciaPorInterna_CheckedChanged" />
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Image/btn/helpIcon.png" AlternateText="Correspondencia interna:por favor seleccione este campo, solamente si la denuncia de los bienes fue presentada ante la oficina de correspondencia  del Instituto de Bienestar Familiar"
                                alt="Correspondencia interna:por favor seleccione este campo,\br solamente si la denuncia de los bienes fue presentada ante la oficina de \br correspondencia  del Instituto de Bienestar Familiar" ToolTip="Correspondencia interna: Por favor seleccione este campo, solamente si la denuncia de los bienes fue presentada ante la oficina de correspondencia  del Instituto de Bienestar Familiar" />
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDatosRadicado">
                    <tr class="rowA">
                        <td>
                            <asp:Label runat="server" Text="Radicado en correspondencia" ID="lblRadicadoCorrespondencia" Visible="false"></asp:Label></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Visible="false" MaxLength="15"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FFFTTT" runat="server" TargetControlID="txtRadicadoCorrespondencia"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/123456789áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:Label runat="server" Text="Fecha de radicado en correspondencia" ID="lblFechaRadicadoCorrespondencia" Visible="false"></asp:Label>
                            <asp:CustomValidator runat="server"
                                ID="valDateRange" ForeColor="Red"
                                ControlToValidate="txtFechaRadicadoCorrespondencia$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La fecha digitada es posterior a la actual. Por favor revisar" />
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="txtFechaRadicadoCorrespondencia" Visible="false" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:Label runat="server" Text="Hora de radicado en correspondencia" ID="lblHoraRadicadoCorrespondencia" Visible="false"></asp:Label>
                            <Ajax:MaskedEditValidator ID="mevtxtHoraRadicadoCorrespondencia" runat="server" ControlExtender="meetxtHoraRadicadoCorrespondencia"
                                ControlToValidate="txtHoraRadicadoCorrespondencia" InvalidValueMessage="La hora digitada no es correcta. Por favor revisar" SetFocusOnError="true"
                                ForeColor="Red" ValidationGroup="btnGuardar" MaximumValue="23:59:59">
                            </Ajax:MaskedEditValidator>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtHoraRadicadoCorrespondencia" Visible="false"></asp:TextBox></td>
                        <Ajax:MaskedEditExtender ID="meetxtHoraRadicadoCorrespondencia" runat="server" CultureDateFormat="hh:mm:ss" UserTimeFormat="TwentyFourHour"
                            CultureTimePlaceholder=":" AcceptAMPM="false" Enabled="True" Mask="99:99:99" AutoComplete="false" MessageValidatorTip="true"
                            MaskType="Time" TargetControlID="txtHoraRadicadoCorrespondencia">
                        </Ajax:MaskedEditExtender>
                    </tr>
                </asp:Panel>
            </table>
            <%--<Ajax:ToolkitScriptManager ID="tsm" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="~/Scripts/multifile/jquery.MultiFileV2.js" />
                    <asp:ScriptReference Path="~/Scripts/multifile/JavaScript.js" />
                </Scripts>
            </Ajax:ToolkitScriptManager>--%>
            <script type="text/javascript">
                function validateDate(source, arguments) {
                    debugger;
                    var EnteredDate = arguments.Value; //for javascript

                    var date = EnteredDate.substring(0, 2);
                    var month = EnteredDate.substring(3, 5);
                    var year = EnteredDate.substring(6, 10);

                    var myDate = new Date(year, month - 1, date);

                    var today = new Date();

                    if (myDate > today) {
                        arguments.IsValid = false;
                    }
                    else {
                        arguments.IsValid = true;
                    }

                }

                var $divMsgErrorAclaracion;

                $(document).ready(function () {
                    $divMsgErrorAclaracion = $('[id*=divMsgError]');
                });

                function checkTextAreaMaxLength(textBox, e, length) {

                    var mLen = textBox["MaxLength"];
                    if (null == mLen)
                        mLen = length;

                    var maxLength = parseInt(mLen);
                    if (!checkSpecialKeys(e)) {
                        if (textBox.value.length > maxLength - 1) {
                            if (window.event)//IE
                                e.returnValue = false;
                            else//Firefox
                                e.preventDefault();
                        }
                    }
                }

                function checkSpecialKeys(e) {
                    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                        return false;
                    else
                        return true;
                }

                function alert(mensaje) {
                    var lblError = $("#lblError");
                    if (lblError != undefined) {
                        lblError.hide();
                    }
                    $("#msgError").text(mensaje);
                    $divMsgErrorAclaracion.show();
                }

                function pageLoad() {
                    $(function () {
                        $divMsgErrorAclaracion.hide();

                        $('.multiFileDocBasico').MultiFile({
                            accept: 'pdf', max: 1, STRING: {
                                remove: '<img src="../../../Image/btn/delete.gif" id="eliminarArchivo" onclick="return emilimar(event);" height="16" width="16" alt="borrar"/>',
                                selected: '$file',
                                denied: 'El formato del archivo debe ser PDF',
                                duplicate: 'Archivo ya seleccionado:\n$file!',
                                toobig: 'El Tamaño del archivo no debe superar 2 Megas',
                                toomany: 'Demasiados archivos seleccionados (Máximo: $max)'
                            }
                        });

                        $('.multiFileDocOtros').MultiFile({
                            accept: 'pdf', max: 100, STRING: {
                                remove: '<img src="../../../Image/btn/delete.gif" id="eliminarArchivo" onclick="return emilimar(event);" height="16" width="16" alt="borrar"/>',
                                selected: '$file',
                                denied: 'El formato del archivo debe ser PDF',
                                duplicate: 'Archivo ya seleccionado:\n$file!',
                                toobig: 'El Tamaño del archivo no debe superar 2 Megas',
                                toomany: 'Demasiados archivos seleccionados (Máximo: $max)'
                            }
                        });
                    });
                }

                $(document).on('click', '.imgDeleteArchicoCss', function (event) {
                    emilimar(event);
                });

                function emilimar(evento) {
                    var vResult = confirm("¿Está seguro de que desea eliminar la información?");
                    if (vResult == true) {
                        return true;
                    }
                    else {
                        evento.stopPropagation();
                        evento.preventDefault();
                        return false;
                    }
                }

                $(document).on('change', 'input.jsDocBasico', function () {
                    if (typeof this.files !== 'undefined') {
                        var tamaño = this.files[0].size;
                        if (tamaño < 2048576) {
                            if (typeof $divMsgErrorAclaracion !== 'undefined') {
                                if ($divMsgErrorAclaracion.is(':visible')) {
                                    $divMsgErrorAclaracion.hide();
                                    return;
                                }
                            }
                        }
                    }
                });

                $(document).on('change', 'input.jsDocOtros', function () {
                    if (typeof this.files !== 'undefined') {
                        var tamaño = this.files[0].size;
                        if (tamaño < 2048576) {
                            if (typeof $divMsgErrorAclaracion !== 'undefined') {
                                if ($divMsgErrorAclaracion.is(':visible')) {
                                    $divMsgErrorAclaracion.hide();
                                    return;
                                }
                            }
                        }
                    }
                });
            </script>
            <script type="text/javascript">
                function eliminarDocumento(control) {
                    document.getElementById('<%= hfIndiceDocumentosOtros.ClientID %>').value = control.attributes["indice"].value;
                    document.getElementById('<%= hfIdDocomentoOtros.ClientID %>').value = control.attributes["idDocumentosSolicitadosDenunciaBien"].value;
                    document.getElementById('<%= btnNuevoDocumento.ClientID %>').style.display = "block";
                    document.getElementById('<%= txtObservacionesDocumento.ClientID %>').value = '';
                    document.getElementById('<%= fuDocumentoOtros.ClientID %>').fileName = '';

                    return true;
                }
            </script>
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnNuevoDocumento" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="chkAcepto" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="chkDenunciaRecibidaCorrespondenciaPorInterna" EventName="CheckedChanged" />
            <asp:PostBackTrigger ControlID="btnNuevoDocumento" />
            <asp:PostBackTrigger ControlID="btnDenuncia" />
            <asp:PostBackTrigger ControlID="btnCedula" />
            <asp:PostBackTrigger ControlID="btnDefuncion" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="../../../Scripts/multifile/jquery.MultiFileV2.js" type="text/javascript"></script>
    <script src="../../../Scripts/MaxLength.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            $('.DescripcionDenuncia').MaxLength({ MaxLength: 512, DisplayCharacterCount: false });
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('input keypress', '.txtArea_validate', function () { 
                var regex = new RegExp("[^a-zA-Z0-9 áéíóúÁÉÍÓÚñÑ .,@_():;]");
                var containsNonNumeric = this.value.match(regex);
                if (containsNonNumeric)
                    this.value = this.value.replace(regex, '');
            });
            $(document).on('click', '#btnGuardar', function () {
                if (Page_ClientValidate('btnGuardar')) {
                    $('#btnGuardar').addClass('hidden');
                }
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 25px;
        }
    </style>
</asp:Content>

