<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_RegistroDenuncia_Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
       
    <asp:HiddenField ID="hfIdDenunciaBien" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">Información básica
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td>Tipo de identificación *
            </td>
            <td>Número de identificación *
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoDocIdentifica" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>Tipo de persona o asociación
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoPersona" Enabled="false"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Label runat="server" Text="Primer nombre" ID="lblPrimerNombre"></asp:Label></td>
            <td>
                <asp:Label runat="server" Text="Segundo nombre" ID="lblSegundoNombre"></asp:Label></td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Label runat="server" Text="Primer apellido" ID="lblPrimerApellido"></asp:Label></td>
            <td>
                <asp:Label runat="server" Text="Segundo apellido" ID="lblSegundoApellido"></asp:Label></td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">Información de residencia
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td>Departamento
            </td>
            <td>Municipio
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdDepartamento" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdMunicipio" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>Zona
            </td>
            <td>Dirección
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdZona" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDireccion" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>Indicativo
            </td>
            <td>Teléfono
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIndicativo" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTelefono" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>Celular
            </td>
            <td>Correo electrónico
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtCelular" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCorreoElectronico" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">Información de la denuncia
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td colspan="2">DECLARO VOLUNTARIAMENTE DE BUENA FE, EN NOMBRE PROPIO O DE UN TERCERO, BAJO LA GRAVEDAD DE JURAMENTO,
                LA EXISTENCIA DE UNA VOCACIÓN HEREDITARIA, BIEN VACANTE O MOSTRENCO REPRESENTADA EN LOS DOCUMENTOS
                QUE SE ANEXAN, ASI COMO EL PROPOSITO DE CELEBRAR EL RESPECTIVO CONTRATO DE PARTICIPACION Y EN SENTIDO
                OBTENER LA DECLARACION JUDICIAL DE LOS BIENES DENUNCIADOS HASTA SU ADJUDICACION AL ICBF
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <table style="width: 100%">
                    <tr>
                        <th></th>
                        <th align="center" class="auto-style1">
                            <asp:CheckBox runat="server"
                                ID="chkAcepto"
                                Text="Acepto *"
                                Enabled="false"
                                CssClass="cbRegistroDenuncia" />
                        </th>
                        <th></th>
                    </tr>
                </table>

            </td>
        </tr>
        <tr class="rowA">
            <td>Descripción de la Denuncia *
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionDenuncia" TextMode="MultiLine" Enabled="false" CssClass="DescripcionDenuncia"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>Departamento de ubicación *
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdRegionalUbicacion" Enabled="false" Width="200"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">Documento digital / certificados a adjuntar
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td class="tdTitulos">Nombre del Documento
            </td>
            <td class="tdTitulos">Documento
            </td>
        </tr>
        <tr class="rowA">
            <td>Denuncia con datos del denunciante y de los bienes denunciados *
            </td>
            <td>
                <%--<asp:HyperLink runat="server" ID="hlDenuncia" Target="_blank" Enabled="false"></asp:HyperLink>--%>
                <asp:Label runat="server" ID="hlDenuncia"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Label runat="server" Text="Cédula de ciudadania del denunciante *" ID="lblCedulaDenunciante"></asp:Label></td>
            <td>
                <%--<asp:HyperLink runat="server" ID="hlCedula" Target="_blank" Enabled="false"></asp:HyperLink>--%>
                <asp:Label runat="server" ID="hlCedula"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>Certificado de defunción
            </td>
            <td>
                <%--<asp:HyperLink runat="server" ID="hlDefuncion" Target="_blank" Enabled="false"></asp:HyperLink>--%>
                <asp:Label runat="server" ID="hlDefuncion"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">Otros documentos
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:GridView ID="gvOtrosDocumentos" runat="server" AutoGenerateColumns="False" HeaderStyle-HorizontalAlign="Center" DataKeyNames="IdDenunciaBien" HorizontalAlign="Center" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="ObservacionesDocumento" HeaderText="Observaciones del documento" />
                        <asp:BoundField DataField="NombreArchivo" HeaderText="Documento" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowAG" />
                    <HeaderStyle CssClass="GrillaOtrosDocs" HorizontalAlign="Center" BackColor="#81BA3D" />
                    <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
        <asp:Panel runat="server" Visible="false" ID="pnlInformacionCorrespondencia">
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de correspondencia ICBF
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:CheckBox runat="server"
                        ID="cbDenunciaRecibidaCorrespondenciaPorInterna"
                        Text="¿Denuncia recibida por correspondencia interna? *"
                        Enabled="false"
                        CssClass="cbRegistroDenuncia" />
                </td>
            </tr>
            <asp:Panel runat="server" ID="pnlDatosRadicado">
                <tr class="rowA">
                    <td>
                        <asp:Label runat="server" Text="Radicado en correspondencia" ID="lblRadicadoCorrespondencia"></asp:Label></td>
                    <td>
                        <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:Label runat="server" Text="Fecha de radicado en correspondencia" ID="lblFechaRadicadoCorrespondencia"></asp:Label></td>
                    <td>
                        <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox></td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:Label runat="server" Text="Hora de radicado en correspondencia" ID="lblHoraRadicadoCorrespondencia"></asp:Label></td>
                    <td>
                        <asp:TextBox runat="server" ID="txtHoraRadicadoCorrespondencia" Enabled="false"></asp:TextBox></td>
                    <Ajax:MaskedEditExtender ID="meetxtHoraRadicadoCorrespondencia" runat="server" CultureDateFormat="hh:mm:ss" UserTimeFormat="TwentyFourHour"
                        CultureTimePlaceholder=":" AcceptAMPM="false" Enabled="True" Mask="99:99:99" AutoComplete="false" MessageValidatorTip="true"
                        MaskType="Time" TargetControlID="txtHoraRadicadoCorrespondencia">
                    </Ajax:MaskedEditExtender>
                    <Ajax:MaskedEditValidator ID="mevtxtHoraRadicadoCorrespondencia" runat="server" ControlExtender="meetxtHoraRadicadoCorrespondencia"
                        ControlToValidate="txtHoraRadicadoCorrespondencia" InvalidValueMessage="La hora digitada no es correcta. Por favor revisar" SetFocusOnError="true"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="btnGuardar" MaximumValue="23:59:59">
                    </Ajax:MaskedEditValidator>
                </tr>
            </asp:Panel>
        </asp:Panel>
    </table>

    <div style="display: none;">
        <asp:Button ID="ButtonShowPopup" runat="server" Text="Button" />
    </div>
    <div>

        <Ajax:ModalPopupExtender ID="MPopExtEncuesta" runat="server" BackgroundCssClass="modalBackground"
            PopupControlID="pContenedorPopEncuesta" TargetControlID="ButtonShowPopup" BehaviorID="ModalPopupExtenderRpt">
        </Ajax:ModalPopupExtender>
    </div>
    <asp:UpdatePanel ID="pContenedorPopEncuesta" CssClass="pContenedorPopEncuesta" runat="server"
        ScrollBars="None" Style="display: none">
        <ContentTemplate>
            <div class="popupestilo">
                ¿Desea calificar el trámite?
              <div>
                  <asp:Button ID="btnSiEncuesta" runat="server" Text="Si" OnClick="btnSi_Encuesta" />
                  <asp:Button ID="btnNoEncuesta" runat="server" Text="No" OnClick="btnNo_Encuesta" />
              </div>
            </div>
            <div id="dFrameEncuestas" runat="server" visible="false">
                <table width="700px" class="bBotoncerrar">
                    <tr class="rowA">
                        <td style="width: 96.15%; text-align: left;"></td>
                        <td style="vertical-align: top;">
                            <asp:ImageButton ID="btnCerrarEncuesta" runat="server" OnClick="btnCerrar_Popup" ImageUrl="~/Image/btn/cancel.jpg" Style="float: right; width: 25px; height: 25px"></asp:ImageButton>
                        </td>
                    </tr>
                </table>
                <iframe id="iFrameEncuestas" runat="server" class="iframeEncuestas"></iframe>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSiEncuesta" EventName="click" />
        </Triggers>

    </asp:UpdatePanel>


</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 115px;
        }

        .modalBackground {
            background-color: #CCCCCC;
            filter: alpha(opacity=10);
            opacity: 0.3;
        }

        .iframeEncuestas {
            position: fixed;
            z-index: 100001;
            left: 200px;
            top: 121px;
            width: 700px;
            height: 359px;
            border: 1px solid #CCCCCC;
            box-shadow: 1px 6px 16px 6px rgba(171,169,171,1);
            -webkit-box-shadow: 1px 6px 16px 6px rgba(171,169,171,1);
            -moz-box-shadow: 1px 6px 16px 6px rgba(171,169,171,1);
            -ms-box-shadow: 1px 6px 16px 6px rgba(171,169,171,1);
        }

        .bBotoncerrar {
            position: fixed;
            right: 229px;
            top: 90px;
            border: 1px solid #CCCCCC;
            box-shadow: 1px 1px 17px 4px rgba(171,169,171,1);
            -webkit-box-shadow: 1px 1px 17px 4px rgba(171,169,171,1);
            -moz-box-shadow: 1px 1px 17px 4px rgba(171,169,171,1);
            -ms-box-shadow: 1px 1px 17px 4px rgba(171,169,171,1);
        }

        .pContenedorPopEncuesta {
            width: 200px;
            height: 150px;
            border: 1px solid #CCCCCC;
            margin: 0 auto;
            background: white;
        }

        .popupestilo {
            overflow: auto !important;
            width: 200px;
            height: 100px;
            border: 1px solid #8a8686;
            background-color: white;
            text-align: center;
            -webkit-box-shadow: 1px 2px 21px 10px rgba(171,169,171,1);
            -moz-box-shadow: 1px 2px 21px 10px rgba(171,169,171,1);
            box-shadow: 1px 2px 21px 10px rgba(171,169,171,1);
        }
    </style>
</asp:Content>

