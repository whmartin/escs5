//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenuncia_List .</summary>
// <author>INGENIAN SOFTWARE[Dar�o Alfredo Beltr�n Camacho]</author>
// <date>03/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase parcial para el List de RegistroDenuncia.
/// </summary>
public partial class Page_RegistroDenuncia_List : GeneralWeb
{
    #region DECLARACI�N DE OBJETOS Y VARIABLES

    /// <summary>
    /// Variable de configuraci�n que contiene el nombre del Rol Denunciante.
    /// </summary>
    ///private const string RolDenunciante = "RolDenunciante";

    /// <summary>
    /// Variable de configuraci�n que contiene el nombre del Rol Radicador.
    /// </summary>
    ///private const string RolRadicador = "RolRadicador";

    /// <summary>
    /// Clase para evento de la toolbar
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Clase vMostrencosService
    /// </summary>
    private string PageName = "Mostrencos/RegistroDenuncia";

    /// <summary>
    /// Path de la p�gina.
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    public SortDirection direction
    {
        get
        {
            if (this.Session["directionState"] == null)
            {
                this.Session["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.Session["directionState"];
        }

        set
        {
            this.Session["directionState"] = value;
        }
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para inicializar la p�gina
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolDenunciante"]))
        {
            this.NavigateTo(SolutionPage.Add);
        }
            
        this.Iniciar();
    }

    /// <summary>
    /// Evento de carga de la p�gina
    /// </summary>
    /// <param name="sender">The page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        ddlIdTipoDocIdentifica.Focus();
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.MostraSeccion();
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento para lanzar la b�squeda 
    /// </summary>
    /// <param name="sender">The button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina Add y tipo de transacci�n
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    #endregion

    #region METODOS

    /// <summary>
    /// M�todo para el manejo de cambio del Control
    /// </summary>
    /// <param name="sender">The Grilla gvRegistroDenuncia</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvRegistroDenuncia_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(gvRegistroDenuncia.SelectedRow);
    }

    /// <summary>
    /// M�todo que p�gina la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvRegistroDenuncia</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvRegistroDenuncia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroDenuncia.PageIndex = e.NewPageIndex;
        if (this.Session["SortedView"] != null)
        {
            List<RegistroDenuncia> vList = (List<RegistroDenuncia>)this.Session["SortedView"];
            this.gvRegistroDenuncia.DataSource = vList;
            this.gvRegistroDenuncia.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    /// <summary>
    /// M�todo que Ordena las columnas de la grilla
    /// </summary>
    /// <param name="sender">The Grilla gvRegistroDenuncia</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvRegistroDenuncia_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<RegistroDenuncia> vList = new List<RegistroDenuncia>();

        if (this.Session["SortedView"] != null)
        {
            vList = (List<RegistroDenuncia>)this.Session["SortedView"];
        }
        else
        {
            vList = this.Buscar();
        }

        switch (e.SortExpression)
        {
            case "RadicadoDenuncia":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.RadicadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.RadicadoDenuncia).ToList();
                }

                break;
            case "FechaRadicadoDenuncia":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.FechaRadicadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.FechaRadicadoDenuncia).ToList();
                }

                break;
            case "IdEstado":
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.IdEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.IdEstado).ToList();
                }

                break;
            default:
                break;
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        this.Session["SortedView"] = vList;
        this.gvRegistroDenuncia.DataSource = vList;
        this.gvRegistroDenuncia.DataBind();
    }

    /// <summary>
    /// Metodo que consulta la lista de los registros de la denuncia
    /// </summary>
    /// <returns>The vLista</returns>
    private List<RegistroDenuncia> Buscar()
    {
        List<RegistroDenuncia> vLista = new List<RegistroDenuncia>();

        try
        {
            int? vIdTipoDocIdentifica = null;
            string vNumeroIdentificacion = null;
            string vDenunciante = null;
            string vRadicadoDenuncia = null;

            if (ddlIdTipoDocIdentifica.SelectedValue != "-1")
            {
                vIdTipoDocIdentifica = Convert.ToInt32(ddlIdTipoDocIdentifica.SelectedValue);
            }

            if (txtNumeroIdentificacion.Text != string.Empty)
            {
                vNumeroIdentificacion = Convert.ToString(txtNumeroIdentificacion.Text);
            }

            if (txtDenunciante.Text != string.Empty)
            {
                vDenunciante = Convert.ToString(txtDenunciante.Text);
            }

            if (txtRadicadoDenuncia.Text != string.Empty)
            {
                vRadicadoDenuncia = Convert.ToString(txtRadicadoDenuncia.Text);
            }

            vLista = this.vMostrencosService.ConsultarRegistroDenuncias(vIdTipoDocIdentifica, vNumeroIdentificacion, vDenunciante, vRadicadoDenuncia);
            this.Session["SortedView"] = vLista;

            gvRegistroDenuncia.DataSource = vLista;
            gvRegistroDenuncia.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

        return vLista;
    }

    /// <summary>
    /// M�todo para inicializar los controles
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            string vRol = GetSessionUser().Rol.ToUpper();

            ////if (vRol == "CONSULTARDENUNCIANTE")
            ////if (vRol == (ConfigurationManager.AppSettings["ConsultarDenunciante"]))
            if (!this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["ConsultarDenunciante"]))
            {
                
            }
            else
            {
                this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            }

            gvRegistroDenuncia.PageSize = this.PageSize();
            gvRegistroDenuncia.EmptyDataText = this.EmptyDataText();

            this.toolBar.EstablecerTitulos("Gesti&oacute;n de denuncias", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo que envia al detalle para consultar la informaci�n
    /// </summary>
    /// <param name="pRow">The pRow</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRegistroDenuncia.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("RegistroDenuncia.IdDenunciaBien", strValue);
            this.NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los datos iniciales del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("RegistroDenuncia.Eliminado").ToString() == "1")
            {
                this.toolBar.MostrarMensajeEliminado();
            }

            this.RemoveSessionParameter("RegistroDenuncia.Eliminado");

            //if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolRadicador"]))
            //{
                string vTiposDocumentos = ConfigurationManager.AppSettings["TiposDocumentoGlobal"];
                //List<TipoIdentificacion> vLstTipoIdentificacion = this.vMostrencosService.ConsultarTipoIdentificacion();
                List<TiposDocumentosGlobal> vLstTipoIdentificacion = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(vTiposDocumentos);
                ddlIdTipoDocIdentifica.Items.Clear();
                ddlIdTipoDocIdentifica.DataSource = vLstTipoIdentificacion;
                ddlIdTipoDocIdentifica.DataValueField = "IdTipoDocumento";
                ddlIdTipoDocIdentifica.DataTextField = "CodDocumento";
                ddlIdTipoDocIdentifica.DataBind();
                ddlIdTipoDocIdentifica.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            //}
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Obtiene todos los roles RolDenunciante por el config.
    /// </summary>
    /// <returns>Variable de configuraci�n</returns>
    //private string[] ObtenerRolDenunciante()
    //{
    //    var vRolesRolDenunciante = ConfigurationManager.AppSettings["RolDenunciante"].Split(',');
    //    return vRolesRolDenunciante;
    //}

    /// <summary>
    /// Obtiene todos los roles RolRadicador por el config.
    /// </summary>
    /// <returns>Variable de configuraci�n</returns>
    //private string[] ObtenerRolRadicador()
    //{
    //    var vRolesRolRadicador = ConfigurationManager.AppSettings["RolRadicador"].Split(',');
    //    return vRolesRolRadicador;
    //}

    /// <summary>
    /// M�todo para consultar usuario de acuerdo a programa/funci�n/rol
    /// </summary>
    /// <param name="pNombreFuncion">The Nombre a filtrar</param>
    /// <returns>EsFlag boolean</returns>
    private bool ConsultarUsuarioProgramaFuncionRol(string pNombreFuncion)
    {
        ////Roles Permitidos
        ////CU 82
        ////Denunciante: Insertar
        ////Radicador: Consultar, Insertar, Editar
        bool EsFlag = false;
        List<Icbf.Seguridad.Entity.ProgramaFuncion> vListProgramaFuncion = new List<Icbf.Seguridad.Entity.ProgramaFuncion>();

        if (this.ViewState["PROGRAMAFUNCION"] == null)
        {
            vListProgramaFuncion = RolUtil.ListarRolesUsuario(GetSessionUser().NombreUsuario, "Mostrencos/RegistroDenuncia");
            this.ViewState["PROGRAMAFUNCION"] = vListProgramaFuncion;
        }
        else
        {
            vListProgramaFuncion = (List<Icbf.Seguridad.Entity.ProgramaFuncion>)this.ViewState["PROGRAMAFUNCION"];
        }

        Icbf.Seguridad.Entity.ProgramaFuncion vProgramaFuncion = vListProgramaFuncion.Find(p => p.NombreFuncion.Trim().ToUpper() == pNombreFuncion.Trim().ToUpper());

        if (vProgramaFuncion != null)
        {
            EsFlag = true;
        }

        return EsFlag;
    }

    /// <summary>
    /// M�todo que oculta la secci�n Consultar al rol Denunciante
    /// </summary>
    private void MostraSeccion()
    {
        if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["ConsultarDenunciante"]))
        {
            this.pnlConsulta.Visible = true;
            this.pnlConsulta.Visible = true;
            this.toolBar.LipiarMensajeError();
        }
        else
        {
            this.pnlConsulta.Visible = false;
            this.pnlConsulta.Visible = false;
            this.toolBar.MostrarMensajeError("Usted no tiene permisos para esta secci&oacute;n");
        }
    }

    #endregion
}