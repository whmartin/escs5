<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RegistroDenuncia_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Tipo de identificación *
                <asp:CompareValidator runat="server" ID="cvTipoIdentificacion" ControlToValidate="ddlIdTipoDocIdentifica"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
                <td>Número de identificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacion" ControlToValidate="txtNumeroIdentificacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                    <Ajax:FilteredTextBoxExtender runat="server"
                        ID="ftNumeroIdentificacion"
                        TargetControlID="txtNumeroIdentificacion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdTipoDocIdentifica" Width="163px"></asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="256"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Denunciante
                </td>
                <td>Radicado de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDenunciante" MaxLength="256"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server"
                        ID="FilteredTextBoxExtender1"
                        TargetControlID="txtDenunciante"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" MaxLength="13"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server"
                        ID="FilteredTextBoxExtender2"
                        TargetControlID="txtRadicadoDenuncia"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegistroDenuncia" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDenunciaBien" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRegistroDenuncia_PageIndexChanging" OnSelectedIndexChanged="gvRegistroDenuncia_SelectedIndexChanged"
                        HorizontalAlign="Center" AllowSorting="true" OnSorting="gvRegistroDenuncia_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Radicado denuncia" DataField="RadicadoDenuncia" SortExpression="RadicadoDenuncia" />
                            <asp:BoundField HeaderText="Tipo identificación" DataField="CodDocumento" />
                            <asp:BoundField HeaderText="Número identificación" DataField="NUMEROIDENTIFICACION" />
                            <asp:BoundField HeaderText="Nombre / Razón social" DataField="NombreMostrar" />
                            <asp:BoundField HeaderText="Fecha radicado denuncia" DataField="FechaRadicadoDenuncia" SortExpression="FechaRadicadoDenuncia" />
                            <asp:BoundField HeaderText="Departamento ubicación" DataField="NombreDepartamento" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstadoDenuncia" SortExpression="IdEstado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" HorizontalAlign="Center" />
                        <PagerStyle HorizontalAlign="Center" />
                        <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
