﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_TrasladarDenuncia_Add" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%"></td>
                        <td style="width: 55%">Histórico de solicitudes
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Radicado de la denuncia
                        </td>
                        <td>Departamento de ubicación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamentoUbicacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Tipo de identificación
                        </td>
                        <td>No de identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td style="width: 45%">Nombre / Razón social
                        </td>
                        <td style="width: 55%">Fecha de radicado denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Abogado asignado
                        </td>
                        <td style="width: 55%">Fecha asignación abogado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtAbogadoAsignado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfechaAsignacionAbogado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Estado
                        </td>
                        <td style="width: 55%">Fecha estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionTraslado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del traslado
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Número de traslado *                             
                        </td>
                        <td>Fecha de traslado *
                            <asp:Label ID="lblRequeridoFechaTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroTraslado" MaxLength="5" Enabled="false" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroTraslado" runat="server" TargetControlID="txtNumeroTraslado"
                            FilterType="Numbers"/>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaTraslado" Requerid ="true" ErrorMessage="" Enabled="false" />
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Motivo del traslado *
                            <asp:Label ID="lblRequeridoMotivoTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Regional a la que se traslada *
                            <asp:Label ID="lblRequeridoRegionalTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList ID="radMotivoTraslado" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="radMotivoTraslado_SelectedIndexChanged">
                                <asp:ListItem Value="ACCIONES">Acciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="VALORES POR COMPETENCIA">Valores por competencia</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlRegionaltraslado" Width="250px"></asp:DropDownList>
                        </td>
                    </tr>                 
                    <tr class="rowB">
                        <td style="width: 45%" colspan="2">Descripción del motivo de traslado
                         <asp:Label ID="lblCantidadMaximaCaracteres" runat="server" Text="Debe ingresar hasta un maximo de 1000 caracteres" ForeColor="Red" Visible="false"></asp:Label>

                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionTraslado" CssClass="Validar" TextMode="MultiLine" Rows="8" Width="100%" Height="100" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcionTraslado" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>                    
                </table>
            </asp:Panel>            
        </ContentTemplate>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.Validar').attr("maxlength", 1000);
        });
            </script>
</asp:Content>