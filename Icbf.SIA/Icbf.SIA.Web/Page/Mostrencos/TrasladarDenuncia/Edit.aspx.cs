﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_TrasladarDenuncia_Edit
/// </summary>
public partial class Page_Mostrencos_TrasladarDenuncia_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/TrasladarDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Edit;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("ListTraslado.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvTrasladosDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<TrasladoDenuncia> vList = new List<TrasladoDenuncia>();
        List<TrasladoDenuncia> vResult = new List<TrasladoDenuncia>();

        if (this.ViewState["SortedgvTrasladosDenuncia"] != null)
        {
            vList = (List<TrasladoDenuncia>)this.ViewState["SortedgvTrasladosDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NumeroTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NumeroTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NumeroTraslado).ToList();
                }

                break;

            case "FechaTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaTraslado).ToList();
                }

                break;

            case "MotivoTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.MotivoTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.MotivoTraslado).ToList();
                }

                break;

            case "NombreRegional":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.RegionalDestino.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.RegionalDestino.NombreRegional).ToList();
                }

                break;

            case "vEstadoTrasladoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }

                break;

            case "DescripcionMotivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.DescripcionMotivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.DescripcionMotivo).ToList();
                }

                break;

            case "NombreRegionalOrigen":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.RegionalOrigen.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.RegionalOrigen.NombreRegional).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        this.ViewState["SortedgvTrasladosDenuncia"] = vResult;
        this.gvTrasladosDenuncia.DataSource = vResult;
        this.gvTrasladosDenuncia.DataBind();
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvTrasladosDenuncia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvTrasladosDenuncia.PageIndex = e.NewPageIndex;
        this.gvTrasladosDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvTrasladosDenuncia"] != null)
        {
            List<RegistroDenuncia> vList = (List<RegistroDenuncia>)this.ViewState["SortedgvTrasladosDenuncia"];
            this.gvTrasladosDenuncia.DataSource = vList;
            this.gvTrasladosDenuncia.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    /// <summary>
    /// evento SelectedIndexChanged de radio button
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void rblMotivoTraslado_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (this.rblMotivoTraslado.SelectedValue.ToUpper().Equals("ACCIONES"))
            {
                this.ddlRegionaltraslado.SelectedValue = "34";
                this.ddlRegionaltraslado.Enabled = false;
            }
            else
            {
                this.ddlRegionaltraslado.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Trasladar denuncia", "Add");
            this.FechaTraslado.NoChecarFormato();
            this.gvTrasladosDenuncia.PageSize = this.PageSize();
            this.gvTrasladosDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.Buscar();
            this.CargarListaDesplegable();
            string vPermisos = Convert.ToString(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso"));
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            int vIdTrasladoDenuncia = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdTrasladoDenuncia"));

            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
            this.txtDepartamentoUbicacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Regional.NombreRegional) ? vDenunciaBien.Regional.NombreRegional.ToUpper() : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
            this.txtNombreRazonSocial.Text = vDenunciaBien.NombreMostrar;
            this.txtFechaRadicado.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy") : string.Empty;
            this.txtAbogadoAsignado.Text = vDenunciaBien.Abogados.NombreAbogado;
            this.txtfechaAsignacionAbogado.Text = vDenunciaBien.FechaAsignacionAbogado.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaAsignacionAbogado).ToString("dd/MM/yyyy") : string.Empty;
            this.txtEstado.Text = !string.IsNullOrEmpty(vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString()) ? vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString() : string.Empty;
            this.txtFechaEstado.Text = vDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count > 0 ? vDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.IdDenunciaBien).LastOrDefault().FechaCrea.ToString("dd/MM/yyyy") : this.txtfechaAsignacionAbogado.Text;
            TrasladoDenuncia vTrasladoDenuncia = vDenunciaBien.ListaTrasladoDenuncia.Where(p => p.IdTrasladoDenuncia == vIdTrasladoDenuncia).FirstOrDefault();
            this.txtNumeroTraslado.Text = vTrasladoDenuncia.NumeroTraslado.ToString();
            this.FechaTraslado.Date = vTrasladoDenuncia.FechaTraslado;
            this.rblMotivoTraslado.SelectedValue = vTrasladoDenuncia.MotivoTraslado;
            this.ddlRegionaltraslado.SelectedValue = vTrasladoDenuncia.IdRegionalTraslada.ToString();
            this.txtDescripcionTraslado.Text = vTrasladoDenuncia.DescripcionMotivo;
            this.txtNumeroTraslado.Text = vTrasladoDenuncia.NumeroTraslado.ToString();
            if (vPermisos.Equals("Abogado"))
            {
                this.MostrarDatosAgogado();
            }

            if (vPermisos.Equals("Coordinador Origen"))
            {
                this.MostrarDatosJuridicoSolicitante();
            }

            if (vPermisos.Equals("Coordinador Destino"))
            {
                this.rblSolicitudTraslado.SelectedValue = vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen;
                this.txtObservacionesSolicitante.Text = vTrasladoDenuncia.ObservacionesOrigen;
                this.MostrarDatosJuridicoDestino();
            }

            if (vTrasladoDenuncia.MotivoTraslado.Equals("ACCIONES"))
            {
                this.ddlRegionaltraslado.Enabled = false;
            }
            else
            {
                this.ddlRegionaltraslado.Enabled = true;
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Muestra la sección que el abogado puede ver
    /// </summary>
    private void MostrarDatosAgogado()
    {
        try
        {
            this.pnlAprobacionDestino.Visible = false;
            this.pnlAprobacionSolicitante.Visible = false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Muestar la sección que el director juridico destino puede ver
    /// </summary>
    private void MostrarDatosJuridicoDestino()
    {
        try
        {
            this.rblMotivoTraslado.Enabled = false;
            this.ddlRegionaltraslado.Enabled = false;
            this.txtDescripcionTraslado.Enabled = false;
            this.rblSolicitudTraslado.Enabled = false;
            this.txtObservacionesSolicitante.Enabled = false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// muestra la sección que el director juridico solicitante puede ver
    /// </summary>
    private void MostrarDatosJuridicoSolicitante()
    {
        try
        {
            this.pnlAprobacionDestino.Visible = false;
            this.rblMotivoTraslado.Enabled = false;
            this.ddlRegionaltraslado.Enabled = false;
            this.txtDescripcionTraslado.Enabled = false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga el contenido de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        List<Regionales> vListaRegionales = this.vMostrencosService.ConsultarRegionales();
        this.ddlRegionaltraslado.DataSource = vListaRegionales.OrderBy(p => p.NombreRegional);
        this.ddlRegionaltraslado.DataValueField = "IdRegional";
        this.ddlRegionaltraslado.DataTextField = "NombreRegional";
        this.ddlRegionaltraslado.DataBind();
        this.ddlRegionaltraslado.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        this.ddlRegionaltraslado.SelectedValue = "-1";
    }

    /// <summary>
    /// guarda la informacion en la base de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (!this.ExistenCamposRequeridos())
            {
                int vResultado;
                TrasladoDenuncia vTrasladoDenuncia = this.CapturarValores();
                if (this.GetCorreoCoordinadorJuridico(vTrasladoDenuncia.IdRegionalOrigen, true) != null)
                {
                    if (this.GetCorreoCoordinadorJuridico(vTrasladoDenuncia.IdRegionalTraslada, false) != null)
                    {
                        this.InformacionAudioria(vTrasladoDenuncia, this.PageName, SolutionPage.Add);
                        string vPermisos = Convert.ToString(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso"));
                        this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
                        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien);
                        if (vPermisos.Equals("Coordinador Destino"))
                        {
                            if (vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino.Equals("Si"))
                            {
                                vDenunciaBien.IdRegionalUbicacion = vTrasladoDenuncia.IdRegionalTraslada;
                            }
                            else
                            {
                                vDenunciaBien.IdRegionalUbicacion = vTrasladoDenuncia.IdRegionalOrigen;
                            }
                        }

                        if (vPermisos.Equals("Coordinador Origen"))
                        {
                            if (vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen.Equals("Si"))
                            {
                                vDenunciaBien.IdRegionalUbicacion = vTrasladoDenuncia.IdRegionalTraslada;
                            }
                        }

                        if (vPermisos.Equals("Abogado"))
                        {
                            vResultado = this.vMostrencosService.EditarTrasladoDenuncia(vTrasladoDenuncia);
                        }
                        else
                        {
                            vDenunciaBien.IdEstadoDenuncia = vTrasladoDenuncia.IdEstadoTraslado;
                            HistoricoEstadosDenunciaBien vHistorico = new HistoricoEstadosDenunciaBien();
                            vHistorico.IdDenunciaBien = vTrasladoDenuncia.IdDenunciaBien;
                            vHistorico.IdEstadoDenuncia = vTrasladoDenuncia.IdEstadoTraslado;
                            vHistorico.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                            vHistorico.IdUsuarioCrea = this.GetSessionUser().IdUsuario;
                            vResultado = this.vMostrencosService.EditarTrasladoDenuncia(vTrasladoDenuncia, vDenunciaBien, vHistorico);
                        }                        

                        if (vResultado == 0)
                        {
                            this.toolBar.MostrarMensajeError("No fue posible editar el taslado. Por favor inténtelo nuevamente.");
                        }
                        else
                        {
                            this.EnviarNotificacion(vTrasladoDenuncia);
                            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Mensaje", "Solicitud registrada correctamente");
                            this.NavigateTo("DetailTraslado.aspx");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// captura los datos diligenciados por el usuario
    /// </summary>
    /// <returns>una variable de tipo TrasladoDenuncia con los datos capturados</returns>
    private TrasladoDenuncia CapturarValores()
    {
        try
        {
            int vIdTrasladoDenuncia = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdTrasladoDenuncia"));
            TrasladoDenuncia vTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorId(vIdTrasladoDenuncia);
            string vPermisos = Convert.ToString(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso"));
            if (vPermisos.Equals("Abogado"))
            {
                vTrasladoDenuncia.MotivoTraslado = this.rblMotivoTraslado.SelectedValue;
                vTrasladoDenuncia.NumeroTraslado = Convert.ToInt32(this.txtNumeroTraslado.Text);
                vTrasladoDenuncia.FechaTraslado = this.FechaTraslado.Date;
                vTrasladoDenuncia.IdRegionalTraslada = Convert.ToInt32(this.ddlRegionaltraslado.Text);
                vTrasladoDenuncia.DescripcionMotivo = this.txtDescripcionTraslado.Text.Trim().ToUpper();
            }

            if (vPermisos.Equals("Coordinador Origen"))
            {
                string vSolicitudTraslado = this.rblSolicitudTraslado.SelectedValue;
                vTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen = vSolicitudTraslado;
                vTrasladoDenuncia.ObservacionesOrigen = this.txtObservacionesSolicitante.Text;
                if (vSolicitudTraslado.Equals("Si"))
                {
                    vTrasladoDenuncia.IdEstadoTraslado = this.vMostrencosService.ConsultarEstadosDenuncia().Where(p => p.NombreEstadoDenuncia.Equals("TRASLADO SOLICITADO APROBADO")).FirstOrDefault().IdEstadoDenuncia;
                }
                else
                {
                    vTrasladoDenuncia.IdEstadoTraslado = this.vMostrencosService.ConsultarEstadosDenuncia().Where(p => p.NombreEstadoDenuncia.Equals("TRASLADO SOLICITADO NEGADO")).FirstOrDefault().IdEstadoDenuncia;
                }
            }

            if (vPermisos.Equals("Coordinador Destino"))
            {
                string vTrasladoAprobado = this.rblTrasladoAprobado.SelectedValue;
                vTrasladoDenuncia.SolicitudTrasladoAprobadoDestino = vTrasladoAprobado;
                vTrasladoDenuncia.ObservacionesDestino = this.txtObservacionesDestino.Text;
                if (vTrasladoAprobado.Equals("Si"))
                {
                    vTrasladoDenuncia.IdEstadoTraslado = this.vMostrencosService.ConsultarEstadosDenuncia().Where(p => p.NombreEstadoDenuncia.Equals("TRASLADADA")).FirstOrDefault().IdEstadoDenuncia;
                }
                else
                {
                    vTrasladoDenuncia.IdEstadoTraslado = this.vMostrencosService.ConsultarEstadosDenuncia().Where(p => p.NombreEstadoDenuncia.Equals("TRASLADO NEGADO")).FirstOrDefault().IdEstadoDenuncia;
                }
            }

            vTrasladoDenuncia.UsuarioModifica = this.GetSessionUser().IdUsuario.ToString();
            return vTrasladoDenuncia;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    /// <returns> true si hay campos requeridos</returns>
    private bool ExistenCamposRequeridos()
    {
        bool vEsrequerido = false;
        try
        {
            string vPermisos = Convert.ToString(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso"));
            if (vPermisos.Equals("Abogado"))
            {                

                if (string.IsNullOrEmpty(this.rblMotivoTraslado.SelectedValue))
                {
                    this.lblRequeridoMotivoTraslado.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoMotivoTraslado.Visible = false;
                    vEsrequerido = false;
                }

                if (this.ddlRegionaltraslado.Text.Equals("-1"))
                {
                    this.lblRequeridoRegionalTraslado.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoRegionalTraslado.Visible = false;
                    vEsrequerido = false;
                }

                if (this.txtDescripcionTraslado.Text.Length > 1000)
                {
                    this.lblCantidadMaximaCaracteres.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblCantidadMaximaCaracteres.Visible = false;
                    vEsrequerido = false;
                }
            }

            if (vPermisos.Equals("Coordinador Origen"))
            {
                if (string.IsNullOrEmpty(this.rblSolicitudTraslado.SelectedValue))
                {
                    this.lblrequeridoSolicitudTraslado.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblrequeridoSolicitudTraslado.Visible = false;
                    vEsrequerido = false;
                }

                if (this.txtObservacionesSolicitante.Text.Length > 1000)
                {
                    this.lblCantidadCaracteresObservacionesSolicitante.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblCantidadCaracteresObservacionesSolicitante.Visible = false;
                    vEsrequerido = false;
                }
            }

            if (vPermisos.Equals("Coordinador Destino"))
            {
                if (string.IsNullOrEmpty(this.rblTrasladoAprobado.SelectedValue))
                {
                    this.lblRequeridoTrasladoAprobado.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoTrasladoAprobado.Visible = false;
                    vEsrequerido = false;
                }

                if (this.txtObservacionesDestino.Text.Length > 1000)
                {
                    this.lblCantidadMaximaObservacionesDestino.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblCantidadMaximaObservacionesDestino.Visible = false;
                    vEsrequerido = false;
                }
            }
            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
            return vEsrequerido;
        }
    }

    // <summary>
    /// Envia notificación al abogado de que tiene asignada una denuncia 
    /// </summary>
    /// <param name="pAbogado">Entidad AsignacionAbogado</param>
    private void EnviarNotificacion(TrasladoDenuncia pTrasladoDenuncia)
    {
        try
        {
            bool result;
            string vPermisos = Convert.ToString(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso"));
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            int vIdRegionalDestino = pTrasladoDenuncia.IdRegionalTraslada;
            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(this.vIdDenunciaBien);
            string vCorreoCoordinadorOrigen;
            string vCorreoCoordinadorDestino;
            string vNombreCoordinadorDestino;
            string vNombreCoordinadorOrigen;
            int vIdAbobado = vDenunciaBien.IdAbogado;
            string vCorreoAbogado = new SIAService().ConsultarUsuario(this.vMostrencosService.ConsultarAbogadoXId(vIdAbobado).IdUsuario).CorreoElectronico;
            string vNombreAbogado = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoAbogado.Trim());
            string vEmailPara = string.Empty;
            string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
            string vAsunto = HttpContext.Current.Server.HtmlDecode("Nueva solicitud de traslado de denuncia");
            string vMensaje = string.Empty;
            int vIdUsuario = 0;
            string vArchivo = string.Empty;
            string vRadicado = vDenunciaBien.RadicadoDenuncia;

            if (vPermisos.Equals("Abogado"))
            {
                vCorreoCoordinadorOrigen = this.GetCorreoCoordinadorJuridico(pTrasladoDenuncia.IdRegionalOrigen, true);
                vNombreCoordinadorOrigen = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoCoordinadorOrigen.Trim());
                vMensaje = "<div style='color: #000000;'> <font color='black'> <CENTER> <B> Solicitud traslado de denuncia </B> </CENTER> </font> " +
                          "<br/> <br/> <font color='black'> <B> Apreciado (a) </B> </font>" +
                          "<br/> <br/>" + vNombreCoordinadorOrigen.Trim() +
                          " <br/> <br/> El Abogado " + vNombreAbogado.Trim() + " solicita el traslado de la siguiente denuncia:" +
                          " <br/> <br/> Radicado de la denuncia " + vRadicado +
                          "<br/> <br/> <br/> <B>  No. de solicitud de traslado:  </B>" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.NumeroTraslado.ToString("D3") +
                          "<br/> <br/> <B> Fecha de solicitud de traslado:   </B>" + "&nbsp;&nbsp;" + pTrasladoDenuncia.FechaTraslado.Date.ToString("dd/MM/yyyy") +
                          HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria </div>");
                result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
            }

            if (vPermisos.Equals("Coordinador Origen"))
            {
                vCorreoCoordinadorOrigen = GetSessionUser().CorreoElectronico.Trim();
                vNombreCoordinadorOrigen = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoCoordinadorOrigen);
                vCorreoCoordinadorDestino = this.GetCorreoCoordinadorJuridico(pTrasladoDenuncia.IdRegionalTraslada, false);
                if (pTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen.Equals("Si"))
                {
                    vNombreCoordinadorDestino = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoCoordinadorDestino.Trim());
                    vEmailPara = vCorreoCoordinadorDestino + "," + vCorreoAbogado;
                    vMensaje = "<div style='color: #000000;'> <font color='black'> <CENTER> <B> Solicitud traslado de denuncia </B> </CENTER> </font> " +
                        "<br/> <br/> <font color='black'> <B> Apreciados (as) </B> </font>" +
                        " <br/> <br/>" + vNombreAbogado.Trim() +
                        " <br/> <br/>" + vNombreCoordinadorDestino.Trim() +
                        " <br/> <br/> El Abogado " + vNombreAbogado.Trim() + " solicita el traslado de la siguiente denuncia:" +
                        " <br/> <br/> Radicado de la denuncia " + vRadicado +
                        "<br/> <br/> <br/> <B>  No. de solicitud de traslado:  </B>" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.NumeroTraslado.ToString("D3") +
                        "<br/> <br/> <B> Fecha de solicitud de traslado:   </B>" + "&nbsp;&nbsp;" + pTrasladoDenuncia.FechaTraslado.Date.ToString("dd/MM/yyyy") +
                        HttpContext.Current.Server.HtmlDecode(" <br/> <br/> El Abogado " + vNombreCoordinadorOrigen + " , determin&oacute;:") +
                        " <br/> <br/> ¿Traslado aprobado?" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen +
                        HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria </div>");
                    result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
                }
                else
                {
                    vEmailPara = vCorreoAbogado;
                    vMensaje = "<div style='color: #000000;'> <font color='black'> <CENTER> <B> Solicitud traslado de denuncia </B> </CENTER> </font> " +
                        "<br/> <br/> <font color='black'> <B> Apreciado (a) </B> </font> " +
                        " <br/> <br/>" + vNombreAbogado +
                        " <br/> <br/> El Abogado " + vNombreAbogado + " solicita el traslado de la siguiente denuncia:" +
                        " <br/> <br/> Radicado de la denuncia " + vRadicado +
                        "<br/> <br/> <br/> <B>  No. de solicitud de traslado:  </B>" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.NumeroTraslado.ToString("D3") +
                        "<br/> <br/> <B> Fecha de solicitud de traslado:   </B>" + "&nbsp;&nbsp;" + pTrasladoDenuncia.FechaTraslado.Date.ToString("dd/MM/yyyy") +
                        HttpContext.Current.Server.HtmlDecode(" <br/> <br/> El Abogado " + vNombreCoordinadorOrigen + " , determin&oacute;:") +
                        " <br/> <br/> ¿Traslado aprobado?" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.SolicitudTrasladoAprobadoOrigen +
                        HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria </div>");
                    result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
                }

            }

            if (vPermisos.Equals("Coordinador Destino"))
            {
                vCorreoCoordinadorOrigen = this.GetCorreoCoordinadorJuridico(pTrasladoDenuncia.IdRegionalOrigen, true);
                vCorreoCoordinadorDestino = this.GetSessionUser().CorreoElectronico.Trim();
                vNombreCoordinadorDestino = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoCoordinadorDestino);
                vNombreCoordinadorOrigen = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vCorreoCoordinadorOrigen.Trim());
                vEmailPara = vCorreoCoordinadorOrigen + "," + vCorreoAbogado;
                vMensaje = "<div style='color: #000000;'> <font color='black'> <CENTER> <B> Solicitud traslado de denuncia </B> </CENTER> </font> " +
                    "<br/> <br/> <font color='black'> <B> Apreciados (as) </B> </font> " +
                    " <br/> <br/>" + vNombreAbogado +
                    " <br/> <br/>" + vNombreCoordinadorOrigen +
                    " <br/> <br/> La solicitud de traslado: " +
                    " <br/> <br/> Radicado de la denuncia " + vRadicado +
                    "<br/> <br/> <br/> <B>  No. de solicitud de traslado:  </B>" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.NumeroTraslado.ToString("D3") +
                    "<br/> <br/> <B> Fecha de solicitud de traslado:   </B>" + "&nbsp;&nbsp;" + pTrasladoDenuncia.FechaTraslado.Date.ToString("dd/MM/yyyy") +
                    HttpContext.Current.Server.HtmlDecode(" <br/> <br/> El Abogado " + vNombreCoordinadorDestino + " , determin&oacute;:") +
                    " <br/> <br/> ¿Traslado aprobado?" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.SolicitudTrasladoAprobadoDestino +
                    HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria </div>");
                result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// obtiene el correo del cordinador juridico
    /// </summary>
    /// <param name="pIdRegionalDestino">id de la regional</param>
    /// <param name="pkeys">keys de busqueda</param>
    /// <returns>correo del coordinador juridico</returns>
    private string GetCorreoCoordinadorJuridico(int pIdRegional, bool pEsOrigen)
    {
        string vCorreoCoordinadorJuridicoRegional = string.Empty;
        try
        {
            string vkeys = string.Empty;
            List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", "COORDINADOR JURIDICO");
            List<string> vUsuarioRol = new List<string>();
            foreach (Icbf.Seguridad.Entity.Rol item in vListaRoles)
            {
                vUsuarioRol.AddRange(Roles.GetUsersInRole(item.NombreRol).ToList());
            }

            foreach (string item in vUsuarioRol)
            {
                vkeys = string.Concat(vkeys, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? string.Empty : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
            }

            vkeys = vkeys.Trim().TrimEnd(',');
            vCorreoCoordinadorJuridicoRegional = this.vMostrencosService.ConsultarCorreoCoordinadorJuridico(pIdRegional, vkeys);

            if (vCorreoCoordinadorJuridicoRegional.Split(';').Count() > 1)
            {
                string vMensaje = pEsOrigen ? "Existe  más de un coordinador para la regional origen" : "Existe  más de un coordinador para la regional destino";
                vCorreoCoordinadorJuridicoRegional = null;
                throw new Exception(vMensaje);
            }

            return vCorreoCoordinadorJuridicoRegional;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vCorreoCoordinadorJuridicoRegional;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Busca la informacion que coisida con los parámetros de busqueda
    /// </summary>
    private void Buscar()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            List<TrasladoDenuncia> vListaTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorIdDenunciaBien(this.vIdDenunciaBien);
            foreach (TrasladoDenuncia item in vListaTrasladoDenuncia)
            {
                item.RegionalDestino = this.vMostrencosService.ConsultarRegionalDelAbogado(item.IdRegionalTraslada);
                item.RegionalOrigen = this.vMostrencosService.ConsultarRegionalDelAbogado(item.IdRegionalOrigen);
                item.EstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoTraslado);
            }
            this.ViewState["SortedgvTrasladosDenuncia"] = vListaTrasladoDenuncia;
            this.gvTrasladosDenuncia.DataSource = vListaTrasladoDenuncia;
            this.gvTrasladosDenuncia.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
}