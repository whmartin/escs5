﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_TrasladarDenuncia_DetailTraslado
/// </summary>
public partial class Page_Mostrencos_TrasladarDenuncia_DetailTraslado : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/TrasladarDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento Lista desplegable Ir a
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        bool vEsAbogado = this.VerificarRol("ABOGADO");
        bool vEsCoordinadorJuridico = this.VerificarRol("Coordinador Juridico");

        this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
        int vIdTrasladoDenuncia = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdTrasladoDenuncia"));
        TrasladoDenuncia vTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorId(vIdTrasladoDenuncia);
        int vIdRegionalUsuario = Convert.ToInt32(this.GetSessionUser().IdRegional);
        int vIdRegionalDestino = vTrasladoDenuncia.IdRegionalTraslada;
        int vIdRegionalOrigen = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien).IdRegionalUbicacion;
        string vEstadoTraslado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vTrasladoDenuncia.IdEstadoTraslado).NombreEstadoDenuncia;

        if (vEsAbogado && vIdRegionalUsuario == vIdRegionalOrigen && vEstadoTraslado.Equals("TRASLADO SOLICITADO"))
        {
            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso", "Abogado");
            this.NavigateTo("Edit.aspx");
        }

        if (vEsCoordinadorJuridico && vIdRegionalOrigen == vIdRegionalUsuario && vEstadoTraslado.Equals("TRASLADO SOLICITADO"))
        {
            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso", "Coordinador Origen");
            this.NavigateTo("Edit.aspx");
        }

        if (vEsCoordinadorJuridico && vIdRegionalUsuario == vIdRegionalDestino && vEstadoTraslado.Equals("TRASLADO SOLICITADO APROBADO"))
        {
            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso", "Coordinador Destino");
            this.NavigateTo("Edit.aspx");
        }

        if (!vEstadoTraslado.Equals("TRASLADO SOLICITADO APROBADO") && !vEstadoTraslado.Equals("TRASLADO SOLICITADO") && !vEstadoTraslado.Equals("TRASLADO SOLICITADO"))
        {
            this.toolBar.MostrarMensajeError("El traslado no se puede editar por que la denuncia esta en estado "+vEstadoTraslado);
        }
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvTrasladosDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<TrasladoDenuncia> vList = new List<TrasladoDenuncia>();
        List<TrasladoDenuncia> vResult = new List<TrasladoDenuncia>();

        if (this.ViewState["SortedgvTrasladosDenuncia"] != null)
        {
            vList = (List<TrasladoDenuncia>)this.ViewState["SortedgvTrasladosDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NumeroTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NumeroTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NumeroTraslado).ToList();
                }

                break;

            case "FechaTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaTraslado).ToList();
                }

                break;

            case "MotivoTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.MotivoTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.MotivoTraslado).ToList();
                }

                break;

            case "NombreRegional":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.RegionalDestino.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.RegionalDestino.NombreRegional).ToList();
                }

                break;

            case "vEstadoTrasladoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }

                break;

            case "DescripcionMotivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.DescripcionMotivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.DescripcionMotivo).ToList();
                }

                break;

            case "NombreRegionalOrigen":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.RegionalOrigen.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.RegionalOrigen.NombreRegional).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        this.ViewState["SortedgvTrasladosDenuncia"] = vResult;
        this.gvTrasladosDenuncia.DataSource = vResult;
        this.gvTrasladosDenuncia.DataBind();
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvTrasladosDenuncia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvTrasladosDenuncia.PageIndex = e.NewPageIndex;
        this.gvTrasladosDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvTrasladosDenuncia"] != null)
        {
            List<RegistroDenuncia> vList = (List<RegistroDenuncia>)this.ViewState["SortedgvTrasladosDenuncia"];
            this.gvTrasladosDenuncia.DataSource = vList;
            this.gvTrasladosDenuncia.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("ListTraslado.aspx");
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.EstablecerTitulos("Trasladar denuncia", "Detail");
            this.gvTrasladosDenuncia.PageSize = this.PageSize();
            this.gvTrasladosDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Busca la informacion que coisida con los parámetros de busqueda
    /// </summary>
    private void Buscar()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            List<TrasladoDenuncia> vListaTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorIdDenunciaBien(this.vIdDenunciaBien);
            foreach (TrasladoDenuncia item in vListaTrasladoDenuncia)
            {
                item.RegionalDestino = this.vMostrencosService.ConsultarRegionalDelAbogado(item.IdRegionalTraslada);
                item.RegionalOrigen = this.vMostrencosService.ConsultarRegionalDelAbogado(item.IdRegionalOrigen);
                item.EstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoTraslado);
            }
            this.ViewState["SortedgvTrasladosDenuncia"] = vListaTrasladoDenuncia;
            this.gvTrasladosDenuncia.DataSource = vListaTrasladoDenuncia;
            this.gvTrasladosDenuncia.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.CargarListaDesplegable();
            this.Buscar();
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            int vIdTrasladoDenuncia = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdTrasladoDenuncia"));
            if (this.vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                this.txtDepartamentoUbicacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Regional.NombreRegional) ? vDenunciaBien.Regional.NombreRegional.ToUpper() : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                this.txtNombreRazonSocial.Text = vDenunciaBien.NombreMostrar;
                this.txtFechaRadicado.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy") : string.Empty;
                this.txtAbogadoAsignado.Text = vDenunciaBien.Abogados.NombreAbogado;
                this.txtfechaAsignacionAbogado.Text = vDenunciaBien.FechaAsignacionAbogado.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaAsignacionAbogado).ToString("dd/MM/yyyy") : string.Empty;
                this.txtEstado.Text = !string.IsNullOrEmpty(vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString()) ? vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString() : string.Empty;
                this.txtFechaEstado.Text = vDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count > 0 ? vDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.IdDenunciaBien).LastOrDefault().FechaCrea.ToString("dd/MM/yyyy") : this.txtfechaAsignacionAbogado.Text;
                TrasladoDenuncia vTrasladoDenuncia = vDenunciaBien.ListaTrasladoDenuncia.Where(p => p.IdTrasladoDenuncia == vIdTrasladoDenuncia).FirstOrDefault();
                this.txtNumeroTraslado.Text = vTrasladoDenuncia.NumeroTraslado.ToString();
                this.FechaTraslado.Date = vTrasladoDenuncia.FechaTraslado;
                this.radMotivoTraslado.SelectedValue = vTrasladoDenuncia.MotivoTraslado;
                this.ddlRegionaltraslado.SelectedValue = vTrasladoDenuncia.IdRegionalTraslada.ToString();
                this.txtDescripcionTraslado.Text = vTrasladoDenuncia.DescripcionMotivo;
            }

            string vMensaje = (string)this.GetSessionParameter("Mostrencos.TrasladarDenuncia.Mensaje");
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga el contenido de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        List<Regionales> vListaRegionales = this.vMostrencosService.ConsultarRegionales();
        this.ddlRegionaltraslado.DataSource = vListaRegionales.OrderBy(p => p.NombreRegional);
        this.ddlRegionaltraslado.DataValueField = "IdRegional";
        this.ddlRegionaltraslado.DataTextField = "NombreRegional";
        this.ddlRegionaltraslado.DataBind();
        this.ddlRegionaltraslado.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        this.ddlRegionaltraslado.SelectedValue = "-1";
    }

    /// <summary>
    /// captura los datos diligenciados por el usuario
    /// </summary>
    /// <returns>una variable de tipo TrasladoDenuncia con los datos capturados</returns>
    private TrasladoDenuncia CapturarValores()
    {
        try
        {
            TrasladoDenuncia vTrasladoDenuncia = new TrasladoDenuncia();
            vTrasladoDenuncia.NumeroTraslado = Convert.ToInt32(this.txtNumeroTraslado.Text);
            vTrasladoDenuncia.FechaTraslado = this.FechaTraslado.Date;
            vTrasladoDenuncia.MotivoTraslado = this.radMotivoTraslado.SelectedValue;
            vTrasladoDenuncia.IdRegionalTraslada = Convert.ToInt32(this.ddlRegionaltraslado.Text);
            vTrasladoDenuncia.DescripcionMotivo = this.txtDescripcionTraslado.Text.Trim().ToUpper();
            vTrasladoDenuncia.UsuarioCrea = this.GetSessionUser().IdUsuario.ToString();
            return vTrasladoDenuncia;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private void ValidarCampos()
    {
        bool vEsrequerido = false;
        try
        {
            if (this.ddlRegionaltraslado.Text.Equals("-1"))
            {
                this.lblRequeridoRegionalTraslado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoRegionalTraslado.Visible = false;
                vEsrequerido = false;
            }

            if (string.IsNullOrEmpty(this.radMotivoTraslado.SelectedValue))
            {
                this.lblRequeridoMotivoTraslado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoMotivoTraslado.Visible = false;
                vEsrequerido = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// verifica si el usuario es de un rol especifico
    /// </summary>
    /// <param name="pRol">rol a buscar</param>
    /// <returns>true o false</returns>
    public bool VerificarRol(string pRol)
    {
        List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", pRol.ToUpper());
        string a = this.GetSessionUser().Rol;
        string[] vRoles = this.GetSessionUser().Rol.Split(';');
        foreach (string item in vRoles)
        {
            if (vListaRoles.Where(p => p.NombreRol.ToUpper() == item.ToUpper()).Count() > 0)
            {
              return true;                
            }
        }

        return false;
    }
    #endregion
}