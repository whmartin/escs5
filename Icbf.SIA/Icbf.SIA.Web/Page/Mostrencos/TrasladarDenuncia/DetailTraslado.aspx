﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="DetailTraslado.aspx.cs" Inherits="Page_Mostrencos_TrasladarDenuncia_DetailTraslado" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%"></td>
                        <td style="width: 55%">Histórico de solicitudes
                 <asp:ImageButton CssClass="btnPopUpHistorico" ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado de la denuncia
                        </td>
                        <td>Departamento de ubicación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamentoUbicacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de identificación
                        </td>
                        <td>No de identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td style="width: 45%">Nombre / Razón social
                        </td>
                        <td style="width: 55%">Fecha de radicado denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Abogado asignado
                        </td>
                        <td style="width: 55%">Fecha asignación abogado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtAbogadoAsignado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfechaAsignacionAbogado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Estado
                        </td>
                        <td style="width: 55%">Fecha estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionTraslado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del traslado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Número de traslado                             
                        </td>
                        <td>Fecha de traslado
                            <asp:Label ID="lblRequeridoFechaTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroTraslado" MaxLength="5" Enabled="false" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroTraslado" runat="server" TargetControlID="txtNumeroTraslado"
                                FilterType="Numbers" />
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaTraslado" Requerid="true" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Motivo del traslado
                            <asp:Label ID="lblRequeridoMotivoTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Regional a la que traslada
                            <asp:Label ID="lblRequeridoRegionalTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList ID="radMotivoTraslado" runat="server" Enabled="false" RepeatDirection="Horizontal">
                                <asp:ListItem Value="ACCIONES">Acciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="VALORES POR COMPETENCIA">Valores por competencia</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlRegionaltraslado" Enabled="false" Width="250px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%" colspan="2">Descripción del motivo del traslado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionTraslado" TextMode="MultiLine" Enabled="false" MaxLength="1000" Rows="8" Width="100%" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
       <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" Width="600px"  Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 10%; left: 20%; background-color: white; border: 1px solid #dfdfdf;height:400px"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <asp:Panel runat="server" ID="pnlLista">
                    <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <span style="color: black; font-weight: bold; top: 10px; position: relative">Histórico de solicitudes</span>
                    </div>
                    <div>
                        <a class="btnCerrarPop" style="width: 16px;height: 16px;text-decoration: none;float: right;margin-right: 10px;top: -10px;position: relative;">
                            <img style="width: 20px;height: 20px;" alt="h" src="../../../Image/btn/close.png">
                        </a></div>
                </td>
            </tr>
                        <tr class="rowA">
                            <td colspan="2"></td>
                        </tr>
                        <tr class="rowAG">
                            <td>
                                <div style="overflow-y: auto; overflow-x: auto; width: 600px; max-height:400px" align-content: center">
                                <asp:GridView ID="gvTrasladosDenuncia" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IdTrasladoDenuncia" GridLines="None" Height="16px" OnPageIndexChanging="gvTrasladosDenuncia_PageIndexChanging" OnSorting="gvTrasladosDenuncia_OnSorting" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="NumeroTraslado" HeaderText="No. De solicitud traslado" SortExpression="NumeroTraslado" />
                                        <asp:BoundField DataField="FechaTraslado" DataFormatString="{0:d}" HeaderText="Fecha" SortExpression="FechaTraslado" />
                                        <asp:BoundField DataField="RegionalOrigen.NombreRegional" HeaderText="Regional origen" SortExpression="NombreRegionalOrigen" />
                                        <asp:BoundField DataField="RegionalDestino.NombreRegional" HeaderText="Regional destino" SortExpression="NombreRegional" />
                                        <asp:BoundField DataField="MotivoTraslado" HeaderText="Motivo traslado" SortExpression="MotivoTraslado" />
                                        <asp:BoundField DataField="DescripcionMotivo" HeaderText="Descripción" SortExpression="DescripcionMotivo" />
                                        <asp:BoundField DataField="EstadoDenuncia.NombreEstadoDenuncia" HeaderText="Estado" SortExpression="vEstadoTrasladoDenuncia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                                    </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });
        });
    </script>
</asp:Content>


