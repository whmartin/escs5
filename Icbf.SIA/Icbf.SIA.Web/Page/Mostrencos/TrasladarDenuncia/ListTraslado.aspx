﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="ListTraslado.aspx.cs" Inherits="Page_Mostrencos_TrasladarDenuncia_ListTraslado" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%"></td>
                        <td style="width: 55%">Histórico de solicitudes
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Radicado de la denuncia
                        </td>
                        <td>Departamento de ubicación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamentoUbicacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Tipo de identificación
                        </td>
                        <td>No de identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td style="width: 45%">Nombre / Razón social
                        </td>
                        <td style="width: 55%">Fecha de radicado denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Abogado asignado
                        </td>
                        <td style="width: 55%">Fecha asignación abogado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtAbogadoAsignado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfechaAsignacionAbogado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Estado
                        </td>
                        <td style="width: 55%">Fecha estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionTraslado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del traslado
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Número de traslado 
                        </td>
                        <td>Fecha de traslado
                            <asp:Label ID="lblRequeridoFechaTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label ID="lblFechaInvalida" runat="server" Text="Fecha Inválida" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroTraslado" MaxLength="5" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroTraslado" runat="server" TargetControlID="txtNumeroTraslado"
                            FilterType="Numbers"/>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaTraslado" />
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Motivo del traslado *
                            <asp:Label ID="lblRequeridoMotivoTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Regional a la que&nbsp; se traslada *
                            <asp:Label ID="lblRequeridoregionalTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList ID="radMotivoTraslado" runat="server" OnSelectedIndexChanged="radMotivoTraslado_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                <asp:ListItem Value="ACCIONES" Selected="True">Acciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="VALORES POR COMPETENCIA">Valores por competencia</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlRegionaltraslado" Enabled="false" Width="250px"></asp:DropDownList>
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>                    
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
     <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTrasladosDenuncia" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTrasladoDenuncia" CellPadding="0" Height="16px" OnSorting="gvTrasladosDenuncia_OnSorting"
                        OnPageIndexChanging="gvTrasladosDenuncia_PageIndexChanging" OnSelectedIndexChanged="gvTrasladosDenuncia_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número de traslado" DataField="NumeroTraslado" SortExpression="NumeroTraslado" />
                            <asp:BoundField HeaderText="Fecha de traslado" DataField="FechaTraslado" SortExpression="FechaTraslado" DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Motivo del traslado" DataField="MotivoTraslado" SortExpression="MotivoTraslado"/>
                            <asp:BoundField HeaderText="Regional a la que se traslada" DataField="RegionalDestino.NombreRegional" SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Estado traslado"  DataField="EstadoDenuncia.NombreEstadoDenuncia" SortExpression="vEstadoTrasladoDenuncia"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>


