﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Edit.aspx.cs" Inherits="Page_Mostrencos_TrasladarDenuncia_Edit" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%"></td>
                        <td style="width: 55%">Histórico de solicitudes
                 <asp:ImageButton ID="btnHistorico" CssClass="btnPopUpHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Radicado de la denuncia
                        </td>
                        <td>Departamento de ubicación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamentoUbicacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Tipo de identificación
                        </td>
                        <td>No de identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td style="width: 45%">Nombre / Razón social
                        </td>
                        <td style="width: 55%">Fecha de radicado denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Abogado asignado
                        </td>
                        <td style="width: 55%">Fecha asignación abogado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtAbogadoAsignado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtfechaAsignacionAbogado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Estado
                        </td>
                        <td style="width: 55%">Fecha estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaEstado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionTraslado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del traslado
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>
                
                    <tr class="rowB">
                        <td>Número de traslado *                             
                        </td>
                        <td>Fecha de traslado *
                            <asp:Label ID="lblRequeridoFechaTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroTraslado" MaxLength="5" Enabled="false" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroTraslado" runat="server" TargetControlID="txtNumeroTraslado"
                            FilterType="Numbers"/>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaTraslado" Requerid ="true" Enabled="false" />
                        </td>
                    </tr>                   
                    <tr class="rowB">
                        <td>Motivo del traslado *
                            <asp:Label ID="lblRequeridoMotivoTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Regional a la que se traslada *
                            <asp:Label ID="lblRequeridoRegionalTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList ID="rblMotivoTraslado" runat="server" OnSelectedIndexChanged="rblMotivoTraslado_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                <asp:ListItem Value="ACCIONES">Acciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="VALORES POR COMPETENCIA">Valores por competencia</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlRegionaltraslado" Width="250px"></asp:DropDownList>
                        </td>
                    </tr>                 
                    <tr class="rowB">
                        <td style="width: 45%" colspan="2">Descripción del motivo de traslado
                             <asp:Label ID="lblCantidadMaximaCaracteres" runat="server" Text="Debe ingresar hasta un máximo de 1000 caracteres" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcionTraslado" CssClass="Validar" TextMode="MultiLine" Rows="8" Width="100%" Height="100" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftDescripcionTraslado" runat="server" TargetControlID="txtDescripcionTraslado" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>                    
                </table>
            </asp:Panel>
             <asp:Panel runat="server" ID="pnlAprobacionSolicitante">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Aprobación del traslado Coordinador jurídico solicitante
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>          
                    <tr class="rowB">
                        <td style="width:45%">¿Solicitud de traslado aprobado? *
                            <asp:Label ID="lblrequeridoSolicitudTraslado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td style="width:55%; text-align:left">
                            <asp:RadioButtonList ID="rblSolicitudTraslado" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Si">Si&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                    </tr>                                 
                    <tr class="rowB">
                        <td style="width: 45%" colspan="2">Observaciones
                             <asp:Label ID="lblCantidadCaracteresObservacionesSolicitante" runat="server" Text="Debe ingresar hasta un máximo de 1000 caracteres" ForeColor="Red" Visible="false"></asp:Label>

                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesSolicitante" CssClass="Validar" TextMode="MultiLine" Rows="8" Width="100%" Height="100" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftObservacionesSolicitante" runat="server" TargetControlID="txtObservacionesSolicitante" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>                    
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlAprobacionDestino">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Aprobación del traslado Coordinador jurídico destino
                        </td>
                    </tr>                    
                    <tr class="rowA">
                        <td colspan="2">
                            </td>
                    </tr>          
                    <tr class="rowB">
                        <td style="width:45%">¿Traslado aprobado? *
                            <asp:Label ID="lblRequeridoTrasladoAprobado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td style="width:55%; text-align:left">
                            <asp:RadioButtonList ID="rblTrasladoAprobado" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Si">Si&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                    </tr>                                 
                    <tr class="rowB">
                        <td style="width: 45%" colspan="2">Observaciones
                         <asp:Label ID="lblCantidadMaximaObservacionesDestino" runat="server" Text="Debe ingresar hasta un máximo de 1000 caracteres" ForeColor="Red" Visible="false"></asp:Label>

                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesDestino" TextMode="MultiLine" CssClass="Validar" Rows="8" Width="100%" Height="100" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftObservacionesDestino" runat="server" TargetControlID="txtObservacionesDestino" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>                    
                </table>
            </asp:Panel>            
        </ContentTemplate>
    </asp:UpdatePanel>  
       
        <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" Width="302px" ScrollBars="Horizontal" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 30%; width: 500px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <asp:Panel runat="server" ID="pnlLista">
                    <table width="90%" align="center">
                        <tr class="rowA">
                            <td colspan="2"></td>
                        </tr>
                        <tr class="rowB">
                            <td class="tdTitulos" style="text-align: center; width:90%">Histórico de solicitudes</td>
                            <td vertical-align: central">
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img alt="h" src="../../../Image/btn/close.png" style="width: 20px; height: 20px;"> </img>
                                </a>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2"></td>
                        </tr>
                        <tr class="rowAG">
                            <td>
                                <asp:GridView ID="gvTrasladosDenuncia" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IdTrasladoDenuncia" GridLines="None" Height="16px" OnPageIndexChanging="gvTrasladosDenuncia_PageIndexChanging" OnSorting="gvTrasladosDenuncia_OnSorting" Width="100%">
                                    <Columns>                                        
                                        <asp:BoundField DataField="NumeroTraslado" HeaderText="No. De solicitud traslado" SortExpression="NumeroTraslado" />
                                        <asp:BoundField DataField="FechaTraslado" DataFormatString="{0:d}" HeaderText="Fecha" SortExpression="FechaTraslado" />
                                        <asp:BoundField DataField="RegionalOrigen.NombreRegional" HeaderText="Regional origen" SortExpression="NombreRegionalOrigen" />
                                        <asp:BoundField DataField="RegionalDestino.NombreRegional" HeaderText="Regional destino" SortExpression="NombreRegional" />
                                        <asp:BoundField DataField="MotivoTraslado" HeaderText="Motivo traslado" SortExpression="MotivoTraslado" />
                                        <asp:BoundField DataField="DescripcionMotivo" HeaderText="Descripción" SortExpression="DescripcionMotivo" />
                                        <asp:BoundField DataField="EstadoDenuncia.NombreEstadoDenuncia" HeaderText="Estado" SortExpression="vEstadoTrasladoDenuncia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('.Validar').attr("maxlength", 1000);
        });
    </script>
</asp:Content>


