﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_TrasladarDenuncia_Add
/// </summary>
public partial class Page_Mostrencos_TrasladarDenuncia_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/TrasladarDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("ListTraslado.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        this.Guardar();
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Permiso", "Abogado");
        this.NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// evento SelectedIndexChanged de radio button
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void radMotivoTraslado_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (this.radMotivoTraslado.SelectedValue.ToUpper().Equals("ACCIONES"))
            {
                this.ddlRegionaltraslado.SelectedValue = "34";
                this.ddlRegionaltraslado.Enabled = false;
            }
            else
            {
                this.ddlRegionaltraslado.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Trasladar denuncia", "Add");
            this.FechaTraslado.NoChecarFormato();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.CargarListaDesplegable();
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            if (this.vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);

                if (vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 11 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 13 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                this.txtDepartamentoUbicacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Regional.NombreRegional) ? vDenunciaBien.Regional.NombreRegional.ToUpper() : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                this.txtNombreRazonSocial.Text = vDenunciaBien.NombreMostrar;
                this.txtFechaRadicado.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy") : string.Empty;
                this.txtAbogadoAsignado.Text = vDenunciaBien.Abogados.NombreAbogado;
                this.txtfechaAsignacionAbogado.Text = vDenunciaBien.FechaAsignacionAbogado.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaAsignacionAbogado).ToString("dd/MM/yyyy") : string.Empty;
                this.txtEstado.Text = !string.IsNullOrEmpty(vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString()) ? vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString() : string.Empty;
                this.txtFechaEstado.Text = vDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count > 0 ? vDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.IdDenunciaBien).LastOrDefault().FechaCrea.ToString("dd/MM/yyyy") : this.txtfechaAsignacionAbogado.Text;
            }

            this.FechaTraslado.Date = DateTime.Now;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los datos de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        List<Regionales> vListaRegionales = this.vMostrencosService.ConsultarRegionales();
        this.ddlRegionaltraslado.DataSource = vListaRegionales.OrderBy(p => p.NombreRegional);
        this.ddlRegionaltraslado.DataValueField = "IdRegional";
        this.ddlRegionaltraslado.DataTextField = "NombreRegional";
        this.ddlRegionaltraslado.DataBind();
        this.ddlRegionaltraslado.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        this.ddlRegionaltraslado.SelectedValue = "-1";
    }

    /// <summary>
    /// guarda la informacion en la base de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            if (!this.ExistenCamposRequeridos())
            {
                if (this.PuedeRegistrarTraslado())
                {
                    int vResultado;
                    TrasladoDenuncia vTrasladoDenuncia = this.CapturarValores();
                    if (this.GetCorreoCoordinadorJuridico(vTrasladoDenuncia.IdRegionalOrigen, true) != null)
                    {
                        this.InformacionAudioria(vTrasladoDenuncia, this.PageName, SolutionPage.Add);
                        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vTrasladoDenuncia.IdDenunciaBien);
                        vDenunciaBien.IdEstadoDenuncia = vTrasladoDenuncia.IdEstadoTraslado;
                        HistoricoEstadosDenunciaBien vHistorico = new HistoricoEstadosDenunciaBien();
                        vHistorico.IdDenunciaBien = vTrasladoDenuncia.IdDenunciaBien;
                        vHistorico.IdEstadoDenuncia = vTrasladoDenuncia.IdEstadoTraslado;
                        vHistorico.UsuarioCrea = GetSessionUser().NombreUsuario;
                        vHistorico.IdUsuarioCrea = GetSessionUser().IdUsuario;
                        this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(vHistorico);
                        vResultado = this.vMostrencosService.InsertarTrasladoDenuncia(vTrasladoDenuncia, vHistorico);
                        if (vResultado == 0)
                        {
                            this.toolBar.MostrarMensajeError("No fue posible registrar el traslado. Por favor inténtelo nuevamente.");
                        }
                        else
                        {
                            vTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorId(vResultado);
                            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.IdTrasladoDenuncia", vResultado);
                            this.EnviarNotificacion(vTrasladoDenuncia);
                            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.Mensaje", "Solicitud registrada correctamente");
                            this.NavigateTo("DetailTraslado.aspx");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Valida si se puede registrar el traslado
    /// </summary>
    /// <returns>true o false</returns>
    private bool PuedeRegistrarTraslado()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            List<TrasladoDenuncia> vListaTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorIdDenunciaBien(this.vIdDenunciaBien);

            if (vListaTrasladoDenuncia.Count() > 0)
            {
                int vIdEstadoTrasladoDenuncia = vListaTrasladoDenuncia.OrderBy(p => p.IdTrasladoDenuncia).LastOrDefault().IdEstadoTraslado;
                string vNombreEstadoTrasladoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vIdEstadoTrasladoDenuncia).NombreEstadoDenuncia;
                return vNombreEstadoTrasladoDenuncia.Equals("TRASLADO NEGADO") || vNombreEstadoTrasladoDenuncia.Equals("TRASLADO SOLICITADO NEGADO");
            }
            else
            {
                return true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// captura los datos diligenciados por el usuario
    /// </summary>
    /// <returns>una variable de tipo TrasladoDenuncia con los datos capturados</returns>
    private TrasladoDenuncia CapturarValores()
    {
        try
        {
            TrasladoDenuncia vTrasladoDenuncia = new TrasladoDenuncia();
            vTrasladoDenuncia.IdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            vTrasladoDenuncia.IdEstadoTraslado = this.vMostrencosService.ConsultarEstadosDenuncia().Where(p => p.NombreEstadoDenuncia.Equals("TRASLADO SOLICITADO")).FirstOrDefault().IdEstadoDenuncia;
            vTrasladoDenuncia.FechaTraslado = DateTime.Now;
            vTrasladoDenuncia.MotivoTraslado = this.radMotivoTraslado.SelectedValue.ToUpper();
            vTrasladoDenuncia.IdRegionalOrigen = Convert.ToInt32(GetSessionUser().IdRegional);
            vTrasladoDenuncia.IdRegionalTraslada = Convert.ToInt32(this.ddlRegionaltraslado.Text);
            vTrasladoDenuncia.DescripcionMotivo = this.txtDescripcionTraslado.Text.Trim().ToUpper();
            vTrasladoDenuncia.UsuarioCrea = GetSessionUser().NombreUsuario;
            return vTrasladoDenuncia;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposRequeridos()
    {
        bool vEsrequerido = false;
        try
        {
            if (string.IsNullOrEmpty(this.radMotivoTraslado.SelectedValue))
            {
                this.lblRequeridoMotivoTraslado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoMotivoTraslado.Visible = false;
                vEsrequerido = false;
            }

            if (this.ddlRegionaltraslado.Text.Equals("-1"))
            {
                this.lblRequeridoRegionalTraslado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoRegionalTraslado.Visible = false;
                vEsrequerido = false;
            }

            if (this.txtDescripcionTraslado.Text.Length > 1000)
            {
                this.lblCantidadMaximaCaracteres.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblCantidadMaximaCaracteres.Visible = false;
                vEsrequerido = false;
            }

            return false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// Envia notificación al guardar del traslado
    /// </summary>
    /// <param name="pTrasladoDenuncia">Entidad TrasladoDenuncia</param>
    private void EnviarNotificacion(TrasladoDenuncia pTrasladoDenuncia)
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            string vEmailPara = this.GetCorreoCoordinadorJuridico(pTrasladoDenuncia.IdRegionalOrigen, true);
            string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
            string vAsunto = string.Empty;
            string vMensaje = string.Empty;
            int vIdUsuario = 0;
            string vArchivo = string.Empty;
            string vNombreCoordinadorJuridicoOrigen = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(vEmailPara.Trim());
            string email = GetSessionUser().CorreoElectronico;
            string vNombreAbogado = this.vMostrencosService.ConsultarNombreCoordinadorJuridico(email.Trim());
            vAsunto = HttpContext.Current.Server.HtmlDecode("Nueva solicitud de traslado de denuncia");
            string vRadicado = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien).RadicadoDenuncia;
            vMensaje = "<div style='color: #000000;'> <font color='black'> <CENTER> <B> Solicitud traslado de denuncia </B> </CENTER> </font> " +
                       "<br/> <br/> <font color='black'> <B> Apreciado (a) </B> </font>" +
                       "<br/> <br/>" + vNombreCoordinadorJuridicoOrigen.Trim() +
                       " <br/> <br/> El Abogado " + vNombreAbogado.Trim() + " solicita el traslado de la siguiente denuncia:" +
                       " <br/> <br/> Radicado de la denuncia " + vRadicado +
                       "<br/> <br/> <br/> <B>  No. de solicitud de traslado:  </B>" + "&nbsp;&nbsp;&nbsp;" + pTrasladoDenuncia.NumeroTraslado.ToString("D3") +
                       "<br/> <br/> <B> Fecha de solicitud de traslado:   </B>" + "&nbsp;&nbsp;" + pTrasladoDenuncia.FechaTraslado.Date.ToString("dd/MM/yyyy") +
                       HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria </div>");
            bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// obtiene el correo del cordinador juridico
    /// </summary>
    /// <param name="pIdRegionalDestino">id de la regional</param>
    /// <param name="pkeys">keys de busqueda</param>
    /// <returns>correo del coordinador juridico</returns>
    private string GetCorreoCoordinadorJuridico(int pIdRegional, bool pEsOrigen)
    {
        string vCorreoCoordinadorJuridicoRegional = string.Empty;
        try
        {
            string vkeys = string.Empty;
            List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", "COORDINADOR JURIDICO");
            List<string> vUsuarioRol = new List<string>();
            foreach (Icbf.Seguridad.Entity.Rol item in vListaRoles)
            {
                vUsuarioRol.AddRange(Roles.GetUsersInRole(item.NombreRol).ToList());
            }

            foreach (string item in vUsuarioRol)
            {
                vkeys = string.Concat(vkeys, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? string.Empty : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
            }

            vkeys = vkeys.Trim().TrimEnd(',');
            vCorreoCoordinadorJuridicoRegional = this.vMostrencosService.ConsultarCorreoCoordinadorJuridico(pIdRegional, vkeys);

            if (vCorreoCoordinadorJuridicoRegional.Split(';').Count() > 1)
            {
                string vMensaje = pEsOrigen ? "Existe  más de un coordinador para la regional origen" : "Existe  más de un coordinador para la regional destino";
                throw new Exception(vMensaje);
            }

            return vCorreoCoordinadorJuridicoRegional;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }
    #endregion

}