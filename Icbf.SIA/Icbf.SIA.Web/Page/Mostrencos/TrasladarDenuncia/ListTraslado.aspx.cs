﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_TrasladarDenuncia_ListTraslado
/// </summary>
public partial class Page_Mostrencos_TrasladarDenuncia_ListTraslado : GeneralWeb
{
    /// <summary>
    /// Declaracion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/TrasladarDenuncia";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
            else
                this.DespuesDeBuscar();
        }
    }

    /// <summary>
    /// Evento Lista desplegable Ir a
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
        string vEstadoDenunciaBien = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenunciaBien.IdEstadoDenuncia).NombreEstadoDenuncia;
        List<TrasladoDenuncia> vListaTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenunciaPorIdDenunciaBien(this.vIdDenunciaBien);
        List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", "ABOGADO");
        bool vEsAbogado = false;
        string[] vRoles = this.GetSessionUser().Rol.Split(';');
        foreach (string item in vRoles)
        {
            if (vListaRoles.Where(p => p.NombreRol.ToUpper() == item.ToUpper()).Count() > 0)
            {
                vEsAbogado = true;
                break;
            }
        }

        if (vListaTrasladoDenuncia.Count() > 0)
        {
            int vIdEstadoTrasladoDenuncia = vListaTrasladoDenuncia.OrderBy(p => p.IdTrasladoDenuncia).LastOrDefault().IdEstadoTraslado;
            if ((vEstadoDenunciaBien.Equals("TRASLADO NEGADO") || vEstadoDenunciaBien.Equals("TRASLADO SOLICITADO NEGADO") || vEstadoDenunciaBien.Equals("ASIGNADA")) && vEsAbogado)
            {
                this.NavigateTo(SolutionPage.Add);
            }
            else
            {
                if (vEsAbogado)
                {
                    this.toolBar.MostrarMensajeError("No se puede registrar una nueva solicitud, la denuncia se encuentra en estado: " + vEstadoDenunciaBien);
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No puede registrar una nueva solicitud, su rol no es abogado");
                }
            }
        }
        else
        {
            if (vEsAbogado)
            {
                if (vEstadoDenunciaBien.ToUpper().Equals("ASIGNADA"))
                {
                    this.NavigateTo(SolutionPage.Add);
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No se puede registrar una la solicitud de traslado, la denuncia se encuentra en estado: " + vEstadoDenunciaBien);
                }
            }
            else
            {
                this.toolBar.MostrarMensajeError("No puede registrar una nueva solicitud, su rol no es abogado");
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvTrasladosDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<TrasladoDenuncia> vList = new List<TrasladoDenuncia>();
        List<TrasladoDenuncia> vResult = new List<TrasladoDenuncia>();

        if (this.ViewState["SortedgvTrasladosDenuncia"] != null)
        {
            vList = (List<TrasladoDenuncia>)ViewState["SortedgvTrasladosDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NumeroTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NumeroTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NumeroTraslado).ToList();
                }

                break;

            case "FechaTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaTraslado).ToList();
                }

                break;

            case "MotivoTraslado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.MotivoTraslado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.MotivoTraslado).ToList();
                }

                break;

            case "NombreRegional":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.RegionalDestino.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.RegionalDestino.NombreRegional).ToList();
                }

                break;

            case "vEstadoTrasladoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.ViewState["SortedgvTrasladosDenuncia"] = vResult;
        this.gvTrasladosDenuncia.DataSource = vResult;
        this.gvTrasladosDenuncia.DataBind();
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvTrasladosDenuncia_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(gvTrasladosDenuncia.SelectedRow);
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvTrasladosDenuncia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvTrasladosDenuncia.PageIndex = e.NewPageIndex;
        this.gvTrasladosDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvTrasladosDenuncia"] != null)
        {
            List<RegistroDenuncia> vList = (List<RegistroDenuncia>)this.ViewState["SortedgvTrasladosDenuncia"];
            this.gvTrasladosDenuncia.DataSource = vList;
            this.gvTrasladosDenuncia.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    /// <summary>
    /// evento SelectedIndexChanged de radio button
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void radMotivoTraslado_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (this.radMotivoTraslado.SelectedValue.ToUpper().Equals("ACCIONES"))
            {
                this.ddlRegionaltraslado.SelectedValue = "34";
                this.ddlRegionaltraslado.Enabled = false;
            }
            else
            {
                this.ddlRegionaltraslado.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Obtiene o establece de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.EstablecerTitulos("Trasladar denuncia", "List");
            this.gvTrasladosDenuncia.PageSize = this.PageSize();
            this.gvTrasladosDenuncia.EmptyDataText = this.EmptyDataText();
            this.FechaTraslado.NoChecarFormato();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            if (this.vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);

                if (vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 11 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 13 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }
                else
                {
                    this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                this.txtDepartamentoUbicacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Regional.NombreRegional) ? vDenunciaBien.Regional.NombreRegional.ToUpper() : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                this.txtNombreRazonSocial.Text = vDenunciaBien.NombreMostrar;
                this.txtFechaRadicado.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy") : string.Empty;
                this.txtAbogadoAsignado.Text = vDenunciaBien.Abogados.NombreAbogado;
                this.txtfechaAsignacionAbogado.Text = vDenunciaBien.FechaAsignacionAbogado.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaAsignacionAbogado).ToString("dd/MM/yyyy") : string.Empty;
                this.txtEstado.Text = !string.IsNullOrEmpty(vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString()) ? vDenunciaBien.EstadoDenuncia.NombreEstadoDenuncia.ToString() : string.Empty;
                this.txtFechaEstado.Text = vDenunciaBien.ListaHistoricoEstadosDenunciaBien.Count > 0 ? vDenunciaBien.ListaHistoricoEstadosDenunciaBien.OrderBy(p => p.IdDenunciaBien).LastOrDefault().FechaCrea.ToString("dd/MM/yyyy") : this.txtfechaAsignacionAbogado.Text;

                List<Regionales> vListaRegionales = this.vMostrencosService.ConsultarRegionales();
                this.ddlRegionaltraslado.DataSource = vListaRegionales.OrderBy(p => p.NombreRegional);
                this.ddlRegionaltraslado.DataValueField = "IdRegional";
                this.ddlRegionaltraslado.DataTextField = "NombreRegional";
                this.ddlRegionaltraslado.DataBind();
                this.ddlRegionaltraslado.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
                this.ddlRegionaltraslado.SelectedValue = "34";
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Control del botón nuevo luego de hacer click en buscar
    /// </summary>
    private void DespuesDeBuscar()
    {
        this.vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien);

        if (vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 11 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 13 || vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 19)
        {
            this.toolBar.OcultarBotonNuevo(true);
            this.toolBar.MostrarBotonEditar(false);
        }
    }


    /// <summary>
    /// Captura la información de la fila seleccionada en la grilla
    /// </summary>
    /// <param name="pRow">fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvTrasladosDenuncia.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.TrasladarDenuncia.IdTrasladoDenuncia", strValue);
            this.NavigateTo("DetailTraslado.aspx");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Busca la informacion que coisida con los parámetros de busqueda
    /// </summary>
    private void Buscar()
    {
        try
        {
            if (!this.ExistenCamposRequeridos())
            {
                this.pnlLista.Visible = true;
                TrasladoDenuncia vTrasladoDenuncia = this.CapturarValores();
                List<TrasladoDenuncia> vListaTrasladoDenuncia = this.vMostrencosService.ConsultarTrasladoDenuncia(vTrasladoDenuncia);
                foreach (TrasladoDenuncia item in vListaTrasladoDenuncia)
                {
                    item.RegionalDestino = this.vMostrencosService.ConsultarRegionalDelAbogado(item.IdRegionalTraslada);
                    item.RegionalOrigen = this.vMostrencosService.ConsultarRegionalDelAbogado(item.IdRegionalOrigen);
                    item.EstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoTraslado);
                }

                this.ViewState["SortedgvTrasladosDenuncia"] = vListaTrasladoDenuncia;
                this.gvTrasladosDenuncia.DataSource = vListaTrasladoDenuncia;
                this.gvTrasladosDenuncia.DataBind();
            }
            else
            {
                this.pnlLista.Visible = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposRequeridos()
    {
        bool vEsrequerido = false;
        try
        {
            try
            {
                DateTime vFecha = Convert.ToDateTime(this.FechaTraslado.Date);
                this.lblFechaInvalida.Visible = false;
                vEsrequerido = false;
            }
            catch (Exception)
            {
                this.lblFechaInvalida.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }

            if ((this.FechaTraslado.Date != Convert.ToDateTime("1/01/1900")) && (this.FechaTraslado.Date > DateTime.Today.Date))
            {
                throw new Exception("La fecha seleccionada no debe ser posterior a la actual. Por favor revisar");
            }
            else
            {
                this.toolBar.LipiarMensajeError();
            }

            if (this.ddlRegionaltraslado.Text.Equals("-1"))
            {
                this.lblRequeridoregionalTraslado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoregionalTraslado.Visible = false;
                vEsrequerido = false;
            }

            this.toolBar.LipiarMensajeError();
            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
            return vEsrequerido;
        }
    }

    /// <summary>
    /// capura los valores diligenciados y los retorna en una variable de tipo TrasladoDenuncia
    /// </summary>
    /// <returns>variable de tipo TrasladoDenuncia</returns>
    private TrasladoDenuncia CapturarValores()
    {
        try
        {
            TrasladoDenuncia vTrasladoDenuncia = new TrasladoDenuncia();
            vTrasladoDenuncia.IdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien"));
            vTrasladoDenuncia.NumeroTraslado = string.IsNullOrEmpty(this.txtNumeroTraslado.Text) ? -1 : Convert.ToInt32(this.txtNumeroTraslado.Text);
            vTrasladoDenuncia.IdRegionalTraslada = Convert.ToInt32(this.ddlRegionaltraslado.Text);
            vTrasladoDenuncia.FechaTraslado = this.FechaTraslado.Date;
            vTrasladoDenuncia.MotivoTraslado = this.radMotivoTraslado.SelectedValue.ToUpper();
            return vTrasladoDenuncia;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }
    #endregion
}