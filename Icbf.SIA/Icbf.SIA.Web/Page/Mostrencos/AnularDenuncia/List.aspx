﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_AnularDenuncia_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">   

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *
                </td>
                <td>Fecha de radicado de la denuncia *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *              
                </td>
                <td>Fecha radicado en correspondencia *              
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de identificación *
                </td>
                <td>Número identificación *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td >Primer nombre *
                </td>
                <td >Segundo nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td >Primer apellido *
                </td>
                <td >Segundo apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3" Enabled="true">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">Descripción de la denuncia</td>
                <td style="width: 50%">Histórico de la denuncia
                 <a  style="width: 16px; height: 16px;" >
                     <asp:Image alt="h" src="../../../Image/btn/info.jpg" id="btnHistorico" style="width: 20px; height: 20px;" runat="server" />
                    </a>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel1" Enabled="false">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="TextBox2" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="700px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelInfoAnulacion">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Información de la anulación
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">Motivo de la Anulación *
                    <asp:Label ID="lblRequeridoEstadoAnulacion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rbtEstadoAnulacion" RepeatDirection="Horizontal" AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="rbtEstadoAnulacion_SelectedIndexChanged">
                        <asp:ListItem Value="1">Denuncia duplicada</asp:ListItem>
                        <asp:ListItem Value="0">Otros</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Descripción de la anulación&nbsp;<asp:Label ID="lblDesAnu" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                 <asp:Label ID="lblCantidadCaracteresRecibido" runat="server" Text="Debe ingresar hasta un máximo de 512 caracteres" ForeColor="Red" Visible="false"></asp:Label>
                 <asp:Label ID="LabelDescrpcion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesRecibido" Enabled="false" CssClass="Validar2" TextMode="MultiLine" Rows="8" Width="1000px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObservacionesRecibido" runat="server" TargetControlID="txtObservacionesRecibido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelAprobacionAnulacion">
         <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Aprobación de la anulación
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">¿Anulación Aprobada? *
                    <asp:Label ID="lblRequeridoAprobacionAnulacion" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rbtAprobacionAnulacion" RepeatDirection="Horizontal" Enabled="false">
                        <asp:ListItem Value="1">Si</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones
                 <asp:Label ID="Label2" runat="server" Text="Debe ingresar hasta un máximo de 512 caracteres" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesAprobacion" Enabled="false" CssClass="Validar2" TextMode="MultiLine" Rows="8" Width="1000px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlGridCausantes">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Historico de anulación
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvHistoricoAnulacionDenuncia" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="0" DataKeyNames="IdSolicitudAnulacion" GridLines="None" Height="16px" OnPageIndexChanging="gvHistoricoAnulacionDenuncia_OnPageIndexChanging"
                        OnSelectedIndexChanged="gvHistoricoAnulacionDenuncia_SelectedIndexChanged" OnSorting="gvHistoricoAnulacionDenuncia_OnSorting" PageSize="10" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha Solicitud Anulación" SortExpression="FechaSolicitud" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="MotivoAnulacion" HeaderText="Motivo de la Anulación" SortExpression="MotivoAnulacion" />
                            <asp:BoundField DataField="DescripcionAnulacion" HeaderText="Descripción de la Anulación" SortExpression="DescripcionAnulacion" />
                            <asp:BoundField DataField="FechaRespuestaSolicitud" HeaderText="Fecha Respuesta Solicitud" SortExpression="FechaRespuestaSolicitud" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="AnulacionAprobada" HeaderText="¿Anulación aprobada?" SortExpression="AnulacionAprobada" />
                            <asp:BoundField DataField="Observacion" HeaderText="Observaciones" SortExpression="Observacion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" Width="500px" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 23%; width: 500px; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                    </div>
                    <div>
                        <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                            <img alt="h" src="../../../Image/btn/close.png" style="width: 20px; height: 20px;"> </img>
                        </a>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                        <asp:GridView ID="gvwHistoricoDenuncia" runat="server" Width="90%" Visible="true" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" GridLines="None"
                            OnSorting="gvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="gvwHistoricoDenuncia_OnPageIndexChanging">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('.Validar2').attr('MaxLength', 512);
        });

    </script>


</asp:Content>
