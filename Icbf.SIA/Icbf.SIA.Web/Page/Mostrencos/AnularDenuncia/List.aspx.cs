﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Session;

public partial class Page_Mostrencos_AnularDenuncia_List : GeneralWeb
{
    #region Declaración Variables
    /// <summary>
    /// The tool bar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// The page name
    /// </summary>
    string PageName = "Mostrencos/AnularDenuncia";

    /// <summary>
    /// The v mostrencos service
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// The v identifier denuncia bien/
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #endregion

    #region "Eventos"

    /// <summary>
    /// Page PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }

            this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;


            if (ConsultarFuncionRol("ABOGADO"))
            {
                if (rbtEstadoAnulacion.Enabled)
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
            }
            else if (ConsultarFuncionRol("COORDINADOR JURIDICO"))
            {
                if (rbtAprobacionAnulacion.Enabled)
                {
                    this.toolBar.MostrarBotonEditar(false);
                }
            }
            else if (ConsultarFuncionRol("ADMINISTRADOR"))
            {
                if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULACIÓN SOLICITADA"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
                if (rbtEstadoAnulacion.Enabled)
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
                if (rbtAprobacionAnulacion.Enabled)
                {
                    this.toolBar.MostrarBotonEditar(false);
                    this.toolBar.MostrarBotonNuevo(false);
                }
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;

            if (ConsultarFuncionRol("ABOGADO"))
            {
                this.toolBar.MostrarBotonNuevo(false);
                if (this.rbtEstadoAnulacion.SelectedIndex >= 0)
                {
                    if (this.rbtEstadoAnulacion.SelectedValue.Equals("0") && this.txtObservacionesRecibido.Text.Length == 0)
                    {
                        this.LabelDescrpcion.Visible = true;
                    }
                    else
                    {
                        InsertarSolicitudAnulacion();
                    }
                }
                else
                {
                    lblRequeridoEstadoAnulacion.Visible = true;
                    rbtEstadoAnulacion.ClearSelection();
                }
            }
            else if (ConsultarFuncionRol("COORDINADOR JURIDICO"))
            {
                this.toolBar.MostrarBotonEditar(false);
                if (this.rbtAprobacionAnulacion.SelectedIndex >= 0)
                {
                    if (this.rbtEstadoAnulacion.SelectedValue.Equals("0") && this.txtObservacionesRecibido.Text.Length == 0)
                    {
                        this.LabelDescrpcion.Visible = true;
                    }
                    else
                    {
                        InsertarAprobacionAnulacion();
                    }
                }
                else
                {
                    lblRequeridoAprobacionAnulacion.Visible = true;
                    rbtAprobacionAnulacion.ClearSelection();
                }
            }
            else if (ConsultarFuncionRol("ADMINISTRADOR"))
            {
                this.toolBar.MostrarBotonNuevo(false);

                if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULACIÓN SOLICITADA"))
                {
                    if (this.txtObservacionesAprobacion.Enabled)
                    {
                        if (this.rbtAprobacionAnulacion.SelectedIndex >= 0)
                        {
                            if (this.rbtEstadoAnulacion.SelectedValue.Equals("0") && this.txtObservacionesRecibido.Text.Length == 0)
                            {
                                this.LabelDescrpcion.Visible = true;
                            }
                            else
                            {
                                InsertarAprobacionAnulacion();
                            }
                        }
                        else
                        {
                            lblRequeridoAprobacionAnulacion.Visible = true;
                            rbtAprobacionAnulacion.ClearSelection();
                        }

                    }
                }
                else
                {
                    if (this.txtObservacionesRecibido.Enabled)
                    {
                        if (this.rbtEstadoAnulacion.SelectedIndex >= 0)
                        {
                            if (this.rbtEstadoAnulacion.SelectedValue.Equals("0") && this.txtObservacionesRecibido.Text.Length == 0)
                            {
                                this.LabelDescrpcion.Visible = true;
                            }
                            else
                            {
                                InsertarSolicitudAnulacion();
                            }
                        }
                        else
                        {
                            lblRequeridoEstadoAnulacion.Visible = true;
                            rbtEstadoAnulacion.ClearSelection();
                        }
                    }
                    else
                    {
                        if (this.rbtAprobacionAnulacion.SelectedIndex >= 0)
                        {
                            if (this.rbtEstadoAnulacion.SelectedValue.Equals("0") && this.txtObservacionesRecibido.Text.Length == 0)
                            {
                                this.LabelDescrpcion.Visible = true;
                            }
                            else
                            {
                                InsertarAprobacionAnulacion();
                            }
                        }
                        else
                        {
                            lblRequeridoAprobacionAnulacion.Visible = true;
                            rbtAprobacionAnulacion.ClearSelection();
                        }
                    }

                }

                this.toolBar.MostrarBotonNuevo(false);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.toolBar.MostrarBotonNuevo(false);
        this.toolBar.OcultarBotonGuardar(true);
        this.rbtEstadoAnulacion.Enabled = true;
        this.txtObservacionesRecibido.Enabled = true;
        btnHistorico.Attributes.Add("class", "btnPopUpHistorico");
        this.toolBar.LipiarMensajeError();
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.toolBar.MostrarBotonNuevo(false);
        this.toolBar.MostrarBotonEditar(false);
        this.toolBar.OcultarBotonGuardar(true);
        this.rbtAprobacionAnulacion.Enabled = true;
        this.txtObservacionesAprobacion.Enabled = true;
        btnHistorico.Attributes.Add("class", "btnPopUpHistorico");
        this.toolBar.LipiarMensajeError();
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.AnularDenuncia.IdCausante"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvHistoricoAnulacionDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvHistoricoAnulacionDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvHistoricoAnulacionDenuncia"] != null)
        {
            List<AnulacionDenuncia> vList = (List<AnulacionDenuncia>)this.ViewState["SortedgvHistoricoAnulacionDenuncia"];
            gvHistoricoAnulacionDenuncia.DataSource = vList;
            gvHistoricoAnulacionDenuncia.DataBind();
        }
        else
        {
            this.CargarGrilla();
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }
                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }
                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }
                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.IdFase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.IdFase).ToList();
                }
                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.IdActuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.IdActuacion).ToList();
                }
                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.IdAccion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.IdAccion).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvHistoricoAnulacionDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<AnulacionDenuncia> vList = new List<AnulacionDenuncia>();
        List<AnulacionDenuncia> vResult = new List<AnulacionDenuncia>();

        if (this.ViewState["SortedgvHistoricoAnulacionDenuncia"] != null)
        {
            vList = (List<AnulacionDenuncia>)this.ViewState["SortedgvHistoricoAnulacionDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "FechaRespuestaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRespuestaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRespuestaSolicitud).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }

        ViewState["SortedgvHistoricoAnulacionDenuncia"] = vResult;
        this.gvHistoricoAnulacionDenuncia.DataSource = vResult;
        this.gvHistoricoAnulacionDenuncia.DataBind();
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvHistoricoAnulacionDenuncia_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvHistoricoAnulacionDenuncia.SelectedRow);
    }

    /// <summary>
    /// rbtEstadoDocumento_SelectedIndexChanged
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void rbtEstadoAnulacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.rbtEstadoAnulacion.SelectedValue.Equals("0"))
        {
            this.lblDesAnu.Visible = true;
        }
        else
        {
            this.LabelDescrpcion.Visible = false;
            this.lblDesAnu.Visible = false;
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Iniciars this instance.
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            if (ConsultarFuncionRol("ABOGADO"))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            }
            else if (ConsultarFuncionRol("COORDINADOR JURIDICO"))
            {
                this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            }
            else if (ConsultarFuncionRol("ADMINISTRADOR"))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
                this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            }
            toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.OcultarBotonGuardar(false);
            toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            toolBar.EstablecerTitulos("Anular Denuncia", "List");
            this.gvHistoricoAnulacionDenuncia.PageSize = this.PageSize();
            this.gvHistoricoAnulacionDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Consulta la funcion asociada al rol y al programa
    /// </summary>
    /// <param name="pNombreFuncion"></param>
    /// <returns></returns>
    public bool ConsultarFuncionRol(string pNombreFuncion)
    {
        bool response = false;
        List<ProgramaFuncion> vListProgramaFuncion = new List<ProgramaFuncion>();
        vListProgramaFuncion = RolUtil.ListarRolesUsuario(GetSessionUser().NombreUsuario, this.PageName);
        ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
        vProgramaFuncion = vListProgramaFuncion.Find(p => p.NombreFuncion.Trim().ToUpper() == pNombreFuncion.Trim().ToUpper());
        if (vProgramaFuncion != null)
        {
            if (vProgramaFuncion.NombreFuncion != "")
                response = true;
        }
        return response;
    }

    /// <summary>
    /// Cargars the datos iniciales.
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA"))
            {
                if (ConsultarFuncionRol("ABOGADO"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
                else if (ConsultarFuncionRol("COORDINADOR JURIDICO"))
                {
                    this.toolBar.MostrarBotonEditar(false);
                }
                if (ConsultarFuncionRol("ABOGADO"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(false);
                }
            }
            if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULACIÓN SOLICITADA"))
            {
                if (ConsultarFuncionRol("ABOGADO"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
                if (ConsultarFuncionRol("ADMINISTRADOR"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
            }

            if (vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Count > 0)
            {
                for (int i = 0; i < vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Count; i++)
                {
                    if (vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdFase == 1
                        && vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdAccion == 2
                        && vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdActuacion == 6)
                    {
                        this.toolBar.MostrarBotonNuevo(false);
                        this.toolBar.MostrarBotonEditar(false);
                        toolBar.MostrarMensajeError("Información de solo consulta, no se puede anular la denuncia ya se realizó la verificación de la Denuncia Anterior");
                        break;
                    }
                }
            }

            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ?
                (vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001 00:00:00") ?
                vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }

            //this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            this.CargarGrilla();
            this.CargarGrillaHistorico(vIdDenunciaBien);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Consultar el rol del usuario en sesion
    /// </summary>
    /// <returns></returns>
    public string ConsultarRol()
    {
        Parameters session = (Parameters)Session["sessionParameters"];
        Usuario usuario = (Usuario)session.Tabla["App.User"];

        string roles = usuario.Rol;
        string rol = string.Empty;

        string[] rols = roles.Split(';');
        foreach (string item in rols)
        {
            if (item == "Administrador")
            {
                return item;
            }
            else if (item == "Abogado")
            {
                rol = item;
            }
            else if (item == "Coordinador Juridico")
            {
                rol = item;
            }
        }

        return rol;
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDenunciaBien"></param>
    private void CargarGrillaHistorico(int pIdDenunciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDenunciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    public void CargarGrilla()
    {
        try
        {
            this.pnlGridCausantes.Visible = true;
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));

            List<AnulacionDenuncia> vListaAnulaciones = this.vMostrencosService.ConsultarHistoricoAnulacionPorIdDenunciaBien(vIdDenunciaBien);
            if (vListaAnulaciones.Count > 0)
            {
                vListaAnulaciones = vListaAnulaciones.OrderBy(p => p.FechaCrea).ToList();
                ViewState["SortedgvHistoricoAnulacionDenuncia"] = vListaAnulaciones;
                this.gvHistoricoAnulacionDenuncia.DataSource = vListaAnulaciones;
                this.gvHistoricoAnulacionDenuncia.DataBind();
            }
            else
            {
                this.pnlGridCausantes.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.pnlGridCausantes.Visible = false;
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// guarda datos y los envia al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvHistoricoAnulacionDenuncia.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.AnularDenuncia.IdCausante", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Insertar solicitud de anulacion
    /// </summary>
    public void InsertarSolicitudAnulacion()
    {
        try
        {
            AnulacionDenuncia pAnulacionDenuncia = CapturaDatosSolicitud();
            this.InformacionAudioria(pAnulacionDenuncia, this.PageName, vSolutionPage);
            int vIdSolicitudAnulacion = this.vMostrencosService.InsertarSolicitudAnulacion(pAnulacionDenuncia);
            if (vIdSolicitudAnulacion > 0)
            {
                this.toolBar.OcultarBotonGuardar(false);
                this.rbtEstadoAnulacion.ClearSelection();
                this.rbtEstadoAnulacion.Enabled = false;
                this.txtObservacionesRecibido.Text = string.Empty;
                this.txtObservacionesRecibido.Enabled = false;
                lblRequeridoEstadoAnulacion.Visible = false;
                CargarGrilla();
                GenerarCorreoSolicitudAnulacion(vIdSolicitudAnulacion);

                this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            }
            else
            {
                this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
            }
        }
        catch (Exception ex)
        {
            this.pnlGridCausantes.Visible = false;
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Captura los datos para insertar un solicitud de anulacion
    /// </summary>
    /// <returns></returns>
    public AnulacionDenuncia CapturaDatosSolicitud()
    {
        AnulacionDenuncia pSolicitudAnulacion = new AnulacionDenuncia();
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));

        pSolicitudAnulacion.IdDenunciaBien = vIdDenunciaBien;
        pSolicitudAnulacion.IdMotivoAnulacion = Convert.ToInt32(this.rbtEstadoAnulacion.SelectedValue);
        pSolicitudAnulacion.DescripcionAnulacion = this.txtObservacionesRecibido.Text.Trim();
        pSolicitudAnulacion.UsuarioCrea = this.GetSessionUser().NombreUsuario;
        pSolicitudAnulacion.IdUsuarioCrea = this.GetSessionUser().IdUsuario;

        return pSolicitudAnulacion;
    }

    /// <summary>
    /// Genera el correo a enviar dela solicitud de anulacion
    /// </summary>
    /// <param name="vIdSolicitudAnulacion"></param>
    public void GenerarCorreoSolicitudAnulacion(int vIdSolicitudAnulacion)
    {
        AnulacionDenuncia pAnulacionDenuncia = new AnulacionDenuncia();
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
        pAnulacionDenuncia = GetCorreoCoordinadorJuridico(vIdDenunciaBien);
        pAnulacionDenuncia.Asunto = "Solicitud Anulaci&oacute;n de denuncia";

        string nombre = string.Concat(this.GetSessionUser().PrimerNombre, " ", this.GetSessionUser().SegundoNombre, " ", this.GetSessionUser().PrimerApellido, " ", this.GetSessionUser().SegundoApellido);
        string radicadoDenuncia = this.txtRadicadoDenuncia.Text;
        string cargo = ConsultarRol();
        pAnulacionDenuncia.Mensaje = HttpContext.Current.Server.HtmlDecode("<div style='background-image:url('..'\'..'\'Image'\'correo'\'marca_agua_correo.png');'> <font color='black'> <CENTER> <B> Solicitud Anulaci&oacute;n de denuncia </B> </CENTER> </font> ") +
                    "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + pAnulacionDenuncia.NombreaEnviar + " <br/> <br/>" +
                    "<br/> <br/> <font color='black'> <B> El Abogado </B> </font> " + nombre + " <br/> <br/> Solicita la anulaci&oacute;n a la siguiente denuncia: " +
                    "<br/> <br/> <br/> <B>  Radicado de la denuncia  </B>" + "&nbsp;&nbsp;&nbsp;" + radicadoDenuncia +
                    "<br/> <br/> <br/> <B>  No. de seguimiento a la solicitud de anulaci&oacute;n </B>" + "&nbsp;&nbsp;&nbsp;" + vIdSolicitudAnulacion +
                    "<br/> <br/> <B> Fecha de solicitud de anulaci&oacute;n   </B>" + "&nbsp;&nbsp;" + DateTime.Now.ToShortDateString() +
                    "<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria." + "&nbsp;&nbsp;" +
                    "<br/> <br/> Cordialmente," + "&nbsp;&nbsp;" +
                    HttpContext.Current.Server.HtmlDecode("<br/> <br/>" + nombre + "<br/> <br/>" + cargo + " </div>");

        EnviarCorreo(pAnulacionDenuncia);
    }

    /// <summary>
    /// obtiene el correo del cordinador juridico
    /// </summary>
    /// <param name="pIdRegionalDestino">id de la regional</param>
    /// <param name="pkeys">keys de busqueda</param>
    /// <returns>correo del coordinador juridico</returns>
    private AnulacionDenuncia GetCorreoCoordinadorJuridico(int vIdDenunciaBien)
    {
        AnulacionDenuncia pAnulacionDenuncia = new AnulacionDenuncia();
        try
        {
            string vkeys = string.Empty;
            List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("ANULAR DENUNCIA", "COORDINADOR JURIDICO");
            List<string> vUsuarioRol = new List<string>();
            foreach (Icbf.Seguridad.Entity.Rol item in vListaRoles)
            {
                vUsuarioRol.AddRange(System.Web.Security.Roles.GetUsersInRole(item.NombreRol).ToList());
            }

            foreach (string item in vUsuarioRol)
            {
                vkeys = string.Concat(vkeys, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? string.Empty : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
            }

            vkeys = vkeys.Trim().TrimEnd(',');

            pAnulacionDenuncia = this.vMostrencosService.ConsultarCoordinadorJuridicoSolicitudAnulacion(vIdDenunciaBien, vkeys);
            return pAnulacionDenuncia;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    /// <summary>
    /// Insertar aprobacion de la anulacion
    /// </summary>
    public void InsertarAprobacionAnulacion()
    {
        try
        {
            AnulacionDenuncia pAprobacionAnulacion = CapturaDatosAprobacion();
            this.InformacionAudioria(pAprobacionAnulacion, this.PageName, vSolutionPage);
            int vIdAprobacionAnulacion = this.vMostrencosService.InsertarAprobacionAnulacion(pAprobacionAnulacion);
            if (vIdAprobacionAnulacion > 0)
            {
                int vAprobacion = Convert.ToInt32(this.rbtAprobacionAnulacion.SelectedValue);
                string vObservacion = this.txtObservacionesAprobacion.Text.Trim();
                this.toolBar.OcultarBotonGuardar(false);
                this.rbtAprobacionAnulacion.ClearSelection();
                this.rbtAprobacionAnulacion.Enabled = false;
                this.txtObservacionesAprobacion.Text = string.Empty;
                this.txtObservacionesAprobacion.Enabled = false;
                lblRequeridoAprobacionAnulacion.Visible = false;
                CargarGrilla();
                GenerarCorreoAprobacionAnulacion(vAprobacion, vObservacion);
                this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            }
            else
            {
                this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
            }
        }
        catch (Exception ex)
        {
            this.pnlGridCausantes.Visible = false;
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Captura los datos para insertar un solicitud de anulacion
    /// </summary>
    /// <returns></returns>
    public AnulacionDenuncia CapturaDatosAprobacion()
    {
        AnulacionDenuncia pAprobacionAnulacion = new AnulacionDenuncia();
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
        pAprobacionAnulacion.IdDenunciaBien = vIdDenunciaBien;
        //pAprobacionAnulacion.IdAnulacionAprobada = (!this.rbtAprobacionAnulacion.SelectedValue.ToString().Equals(string.Empty)) ?Convert.ToInt32(this.rbtAprobacionAnulacion.SelectedValue):0;
        pAprobacionAnulacion.IdAnulacionAprobada = Convert.ToInt32(this.rbtAprobacionAnulacion.SelectedValue);
        pAprobacionAnulacion.Observacion = this.txtObservacionesAprobacion.Text.Trim();
        pAprobacionAnulacion.UsuarioCrea = this.GetSessionUser().NombreUsuario;
        pAprobacionAnulacion.IdUsuarioCrea = this.GetSessionUser().IdUsuario;

        return pAprobacionAnulacion;
    }

    /// <summary>
    /// Genera Correo para la aprobacion de la anulacion
    /// </summary>
    /// <param name="vIdSolicitudAnulacion"></param>
    public void GenerarCorreoAprobacionAnulacion(int vAprobacion, string vObservacion)
    {
        AnulacionDenuncia pAnulacionDenuncia = new AnulacionDenuncia();

        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien"));
        pAnulacionDenuncia = this.vMostrencosService.ConsultarAbogadoSolicitudAnulacion(vIdDenunciaBien);
        string aprobacion = vAprobacion == 1 ? "SI" : "NO";
        pAnulacionDenuncia.Asunto = "Solicitud Anulaci&oacute;n de denuncia";

        string nombre = string.Concat(this.GetSessionUser().PrimerNombre, " ", this.GetSessionUser().SegundoNombre, " ", this.GetSessionUser().PrimerApellido, " ", this.GetSessionUser().SegundoApellido);
        string cargo = ConsultarRol();
        pAnulacionDenuncia.Mensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'>") +
                    "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + pAnulacionDenuncia.NombreaEnviar + " <br/> <br/>" +
                    "<br/> <br/> <font color='black'> <B> El Abogado </B> </font> " + nombre + " <br/> <br/> determino: " +
                    "<br/> <br/> <br/> <B> ¿Anulaci&oacute;n aprobado?  </B>" + "&nbsp;&nbsp;&nbsp;" + aprobacion +
                    "<br/> <br/> <br/> <B>  Observaciones  </B>" + "&nbsp;&nbsp;&nbsp;" + vObservacion +
                    "<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria." + "&nbsp;&nbsp;" +
                    "<br/> <br/> Cordialmente," + "&nbsp;&nbsp;" +
                    HttpContext.Current.Server.HtmlDecode("<br/> <br/>" + nombre + "<br/> <br/>" + cargo + "</div>");

        EnviarCorreo(pAnulacionDenuncia);
    }

    /// <summary>
    /// Envia notificación al abogado de que tiene asignada una denuncia 
    /// </summary>
    /// <param name="pAbogado">Entidad AsignacionAbogado</param>
    private void EnviarCorreo(AnulacionDenuncia pAnulacionDenuncia)
    {
        string vEmailPara = pAnulacionDenuncia.Para;
        string vNombreOrigen = "pruebas.sim@icbf.gov.co";
        string vAsunto = string.Empty;
        string vMensaje = string.Empty;
        int vIdUsuario = 0;
        string vArchivo = string.Empty;
        vAsunto = HttpContext.Current.Server.HtmlDecode(pAnulacionDenuncia.Asunto);
        vMensaje = pAnulacionDenuncia.Mensaje;
        bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
    }

    #endregion    
}