﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_InformeDenunciante_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style>
        .descripcionGrilla {
            word-break: break-all;
        }
    </style>
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado denuncia *</td>
                        <td>Fecha de Radicado de la Denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en Correspondencia *</td>
                        <td>Fecha Radicado en Correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de Identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Primer Nombre *</td>
                        <td>Segundo Nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Primer Apellido *</td>
                        <td>Segundo Apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">&nbsp;&nbsp;Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" 
                                Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionInformeDenunciante">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información Informe del Denunciante</td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha del Informe *
                        </td>
                        <td></td>
                        <td>Descripción del Informe Resumen Ejecutivo *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaInforme" Requerid="true" Enabled="false" Width="250px"/>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox ID="txbDescripcionInforme" runat="server" TextMode="MultiLine" Enabled="false" Height="46px" Width="250px" 
                                Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Documento del Informe Resumen Ejecutivo *
                        </td>
                        <td></td>
                        <td>Nombre del archivo *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlDocumentoInforme" runat="server" DataTextField="DocumentoInforme" DataValueField="IdDocumentoInforme" Enabled="false" Height="24px" Width="250px" Requerid="true">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox runat="server" ID="txbNombreArchivo" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>


        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#cphCont_txtDescripcion').on('input', function (e) {
                if (document.getElementById("cphCont_txtDescripcion").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtDescripcion").value.substr(0, 512);
                }
            });
        });

        function CheckLength() {
            var textboxObservaciones = document.getElementById("cphCont_txtDescripcion").value;

            if (textbox.trim().length >= 512) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
