﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_InformeDenunciante_List" %>

<%--<%@ Register Src="~/General/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>--%>
<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .Background {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 450px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        /*.ajax__calendar_day {
            text-decoration-line: underline;
        }*/

        .ajax__calendar_invalid .ajax__calendar_day {
            background: #d5d5d5;
            cursor: not-allowed;
            text-decoration: none !important;
        }

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>

            <tr class="rowB">
                <td>Radicado denuncia *</td>
                <td>Fecha de Radicado de la Denuncia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en Correspondencia *</td>
                <td>Fecha Radicado en Correspondencia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>

            <tr class="rowB">
                <td>Tipo de Identificación *</td>
                <td>Número de Identificación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Primer Nombre *</td>
                <td>Segundo Nombre *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Primer Apellido *</td>
                <td>Segundo Apellido *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la denuncia *
                    <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                        ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 55%">Histórico de la denuncia
                    <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                        Height="16px" Width="16px" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlInformacionInformeDenunciante">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="3" class="tdTitulos">Información Informe del Denunciante
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha Inicial *
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator4" ForeColor="Red"
                        ControlToValidate="FechaHasta$txtFecha"
                        ClientValidationFunction="validateDateF" ValidationGroup="btnBuscar"
                        ErrorMessage="Campo requerido">
                    </asp:CustomValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator2" ForeColor="Red"
                        ControlToValidate="FechaDesde$txtFecha"
                        ClientValidationFunction="validateDateF" ValidationGroup="btnBuscar"
                        ErrorMessage="Fecha inválida.">
                    </asp:CustomValidator>
                </td>
                <td>Fecha Final *
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator3" ForeColor="Red"
                        ControlToValidate="FechaHasta$txtFecha"
                        ClientValidationFunction="validateDateF" ValidationGroup="btnBuscar"
                        ErrorMessage="Campo requerido">
                    </asp:CustomValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="reqFechaFinalMenor" ForeColor="Red"
                        ControlToValidate="FechaHasta$txtFecha"
                        ClientValidationFunction="validateDateF" ValidationGroup="btnBuscar"
                        ErrorMessage="Fecha inválida.">
                    </asp:CustomValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator1" ForeColor="Red"
                        ControlToValidate="FechaHasta$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnBuscar"
                        ErrorMessage="La fecha final debe ser mayor o igual a la fecha inicial.">
                    </asp:CustomValidator>

                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha ID="FechaDesde" Required="true" runat="server" />
                </td>
                <td>
                    <uc1:fecha ID="FechaHasta" Required="true" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlgrvInformacionInformeDenunciante">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="3">
                    <asp:GridView runat="server" ID="grvInformacionInformeDenunciante" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" ShowHeader="true" Visible="true" DataKeyNames="IdDenunciaBien,IdInformeDenunciante" CellPadding="0" Height="16px" PageSize="10" OnSelectedIndexChanged="grvInformacionInformeDenunciante_SelectedIndexChanged"
                        OnPageIndexChanging="grvInformacionInformeDenunciante_PageIndexChanged"
                        OnRowCommand="grvInformacionInformeDenunciante_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha del Informe" DataField="FechaInforme" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField HeaderText="Descripción del Informe resumen ejecutivo" DataField="DescripcionInforme" ItemStyle-CssClass="descripcionGrilla" />
                            <asp:BoundField HeaderText="Documento del Informe resumen ejecutivo" DataField="DocumentoInforme" />
                            <asp:TemplateField HeaderText="Nombre archivo" SortExpression="NombreArchivo">
                                <ItemTemplate>
                                    <asp:Button runat="server" CssClass="lnkArchivoDescargar" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument='<%# Eval("NombreArchivo") %>' Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlArchivo" CssClass="popuphIstorico hidden" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <div class="col-md-5 align-center">
                            <h4>
                                <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-md-5 align-center">
                            <asp:HiddenField ID="hfIdArchivo" runat="server" />
                            <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                        </div>
                        <div class="col-md-1 align-center">
                            <a class="btnCerrarPopArchivo" style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative;">
                                <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png"></img>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                        <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                        <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript">

        $(document).ready(function () {
            var txtControl = document.getElementById('cphCont_FechaDesde_txtFecha');
            txtControl.focus();

            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.PopUpHistorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.PopUpHistorico').addClass('hidden');
            });

            $(document).on('click', '.btnCerrarPopArchivo', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('#cphCont_txtDescripcion').on('input', function (e) {
                if (document.getElementById("cphCont_txtDescripcion").value.length > 15) {
                    this.value = document.getElementById("cphCont_txtDescripcion").value.substr(0, 15);
                }
            });
        });

        function validateDate(source, arguments) {

            var EnteredDate = arguments.Value; //for javascript
            var fechaFin = EnteredDate.split('/');
            var DateFin = new Date(fechaFin[2], fechaFin[1], fechaFin[0])

            var fechaDesde = document.getElementById('cphCont_FechaDesde_txtFecha').value;
            var fechaInicio = fechaDesde.split('/');
            var DateInicio = new Date(fechaInicio[2], fechaInicio[1], fechaInicio[0]);

            if (DateInicio != '' && DateFin != '') {

                if (DateInicio == DateFin) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
                if (DateInicio > DateFin) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function validateDateF(source, arguments) {

            var EnteredDate = arguments.Value; //for javascript
            var fechaFin = EnteredDate.split('/');
            var DateFin = new Date(fechaFin[2], fechaFin[1], fechaFin[0])

            var fechaDesde = document.getElementById('cphCont_FechaDesde_txtFecha').value;
            var fechaInicio = fechaDesde.split('/');
            var DateInicio = new Date(fechaInicio[2], fechaInicio[1], fechaInicio[0]);

            var DateF = new Date('01', '01', '1900');

            if (DateInicio != '' && DateFin != '') {

                if (DateInicio < DateF) {
                    arguments.IsValid = false;
                } else {
                    arguments.IsValid = true;
                }

                if (DateFin < DateF) {
                    arguments.IsValid = false;
                } else {
                    arguments.IsValid = true;
                }
            }
        }

        function SetFocus() {
            var txtControl = document.getElementById('cphCont_FechaDesde_txtFecha');
            txtControl.focus();
        }

        function CheckLength() {
            var textboxObservaciones = document.getElementById("cphCont_txtDescripcion").value;

            if (textbox.trim().length >= 512) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
