﻿//-----------------------------------------------------------------------
// <copyright file="Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Edit.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Mostrencos.Entity;
using System.IO;
using System.Configuration;

public partial class Page_Mostrencos_InformeDenunciante_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/InformeDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// File Name
    /// </summary>
    private string vFileName = string.Empty;

    #region Metodos

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Informe del Denunciante");

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));

            //Valida el estado de la denuncia si es diferente a Asignada o Dañada
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);



            if ((vRegistroDenuncia.IdEstado != 11) && (vRegistroDenuncia.IdEstado != 13))
            {
                toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

                //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                toolBar.OcultarBotonNuevo(true);
                toolBar.OcultarBotonEditar(true);
                //toolBar.MostrarBotonNuevo(false);
                //toolBar.MostrarBotonEditar(false);
            }
            else
            {
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }

            this.CargarListaTipoDocumentoSoporteDenuncia();
            this.pnlInformacionDenuncia.Enabled = false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            this.txtDescripcionInforme.Focus();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));
            this.CargarGrillaHistorico(vIdDenunciaBien);
            //List<Tercero> vTercero = new List<Tercero>();
            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
            this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }

            int IdInformeDenunciante = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante"));

            List<InformeDenunciante> vlistInformeDenuncianteXID = this.vMostrencosService.ConsultarDatosInformeDenuncianteXID(IdInformeDenunciante);
            if (vlistInformeDenuncianteXID.Count() > 0)
            {
                FechaInforme.Date = !string.IsNullOrEmpty(Convert.ToDateTime(vlistInformeDenuncianteXID[0].FechaInforme).Date.ToString()) ? vlistInformeDenuncianteXID[0].FechaInforme : DateTime.Now;
                txtDescripcionInforme.Text = !string.IsNullOrEmpty(vlistInformeDenuncianteXID[0].DescripcionInforme) ? vlistInformeDenuncianteXID[0].DescripcionInforme : string.Empty;
                ddlDocumentoInforme.Text = vlistInformeDenuncianteXID[0].IdDocumentoInforme.ToString();
                lblNombreArchivo.Text = vlistInformeDenuncianteXID[0].NombreArchivo.ToString();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaTipoDocumentoSoporteDenuncia()
    {
        List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
        vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
        vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
        this.ddlDocumentoInforme.DataSource = vlstTipoDocumentosBien;
        this.ddlDocumentoInforme.DataTextField = "NombreTipoDocumento";
        this.ddlDocumentoInforme.DataValueField = "IdTipoDocumentoBienDenunciado";
        this.ddlDocumentoInforme.DataBind();
        this.ddlDocumentoInforme.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        this.ddlDocumentoInforme.SelectedValue = "-1";


    }

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));
            int IdInformeDenunciante = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante"));
            DateTime ahora = DateTime.Now;

            //if (GuardarArchivos(vIdDenunciaBien, ahora.ToString("yyyyMMddHHMMss"), fulArchivoRecibido, "~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/") == true)
            if (this.GuardarArchivos(vIdDenunciaBien, 0))
            {

                InformeDenunciante pInformeDenunciante = new InformeDenunciante();
                pInformeDenunciante.IdInformeDenunciante = IdInformeDenunciante;
                pInformeDenunciante.IdDenunciaBien = vIdDenunciaBien;
                pInformeDenunciante.IdDocumentoInforme = Convert.ToInt32(this.ddlDocumentoInforme.SelectedValue);
                pInformeDenunciante.FechaInforme = this.FechaInforme.Date;
                pInformeDenunciante.DescripcionInforme = txtDescripcionInforme.Text;
                pInformeDenunciante.DocumentoInforme = this.ddlDocumentoInforme.SelectedItem.Text;
                pInformeDenunciante.NombreArchivo = (this.fulArchivoRecibido.FileName.Length > 0) ? this.fulArchivoRecibido.FileName : this.lblNombreArchivo.Text;
                pInformeDenunciante.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pInformeDenunciante.FechaCrea = DateTime.Now;
                pInformeDenunciante.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pInformeDenunciante.FechaModifica = DateTime.Now;
                this.InformacionAudioria(pInformeDenunciante, this.PageName, vSolutionPage);
                int IdActualizarInforme = this.vMostrencosService.ActualizarInformeDenunciante(pInformeDenunciante);

                if (IdActualizarInforme == -1)
                {
                    toolBar.MostrarMensajeError("El registro ya existe");
                    return;
                }

                SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante", pInformeDenunciante.IdInformeDenunciante);
                SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.Mensaje", "La información ha sido guardada exitosamente");
                this.NavigateTo(SolutionPage.Detail);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        bool Esresultado = false;
        try
        {
            if (this.fulArchivoRecibido.FileName.Length == 0 && this.lblNombreArchivo.Text.Length > 0)
            {
                return true;
            }
            else
            {
                HttpFileCollection files = Request.Files;
                if (files.Count > 0)
                {
                    string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                    string extPdf = ".PDF".ToLower();
                    string extJgp = ".JPG".ToLower();
                    if (this.fulArchivoRecibido.FileName.Length > 50)
                    {
                        throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos.");
                    }
                    int tamano = this.fulArchivoRecibido.FileName.Length;
                    if (extFile != extPdf && extFile != extJgp)
                    {
                        throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                    }

                    int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                    if (vtamArchivo > 4096)
                    {
                        throw new Exception("El tamaño máximo del archivo es 4 Mb");
                    }

                    HttpPostedFile file = files[pPosition];
                    if (file.ContentLength > 0)
                    {
                        string rutaParcial = string.Empty;
                        string carpetaBase = string.Empty;
                        string filePath = string.Empty;
                        if (extFile.Equals(".pdf"))
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        }
                        else if (extFile.ToLower().Equals(".jpg"))
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/  "), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        }
                        bool EsexisteCarpeta = System.IO.Directory.Exists(carpetaBase);
                        if (!EsexisteCarpeta)
                        {
                            System.IO.Directory.CreateDirectory(carpetaBase);
                            return true;
                        }
                        this.vFileName = rutaParcial;
                        file.SaveAs(filePath);
                        Esresultado = true;
                    }
                }
            }

        }
        catch (HttpRequestValidationException httpex)
        {
            throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return Esresultado;
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    #endregion
}