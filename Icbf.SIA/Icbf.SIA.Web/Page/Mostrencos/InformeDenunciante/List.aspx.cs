﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.IO;

public partial class Page_Mostrencos_InformeDenunciante_List : GeneralWeb
{
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/InformeDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    private int vIdDenunciaBien;

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script> $(document).ready(function(){ open('pagina.html', '', 'top=300,left=300,width=300,height=300'); </ script >");
        toolBar.MostrarBotonConsultar();
        toolBar.MostrarBotonNuevo(true);
        this.pnlArchivo.CssClass = "popuphIstorico hidden";

        toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }


    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            string vIdArchivo = this.hfIdArchivo.Value;

            bool vDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vIdDenunciaBien + "/" + vIdArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vIdDenunciaBien + "/" + vIdArchivo;

            if (vDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    protected void grvInformacionInformeDenunciante_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Ver"))
            {
                string rutaBase = "../RegistrarDocumentosBienDenunciado/Archivos/" + vIdDenunciaBien + "/";
                string nombrearchivo = e.CommandArgument.ToString();
                string ruta = rutaBase + nombrearchivo;

                this.hfIdArchivo.Value = nombrearchivo.ToString();


                this.lblTitlePopUp.Text = nombrearchivo;
                this.pnlArchivo.CssClass = "popuphIstorico";

                if (nombrearchivo.ToUpper().Contains("PDF") || nombrearchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", ruta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                }
                else if (nombrearchivo.ToUpper().Contains("JPG") || nombrearchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = ruta;
                }

                this.btnDescargar.Visible = true;

                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void grvInformacionInformeDenunciante_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvInformacionInformeDenunciante.PageIndex = e.NewPageIndex;

        List<InformeDenunciante> vlistInformeDenunciante = this.vMostrencosService.ConsultarDatosInformeDenunciante(vIdDenunciaBien, this.FechaDesde.Date.ToString("yyyy-MM-dd"), this.FechaHasta.Date.ToString("yyyy-MM-dd"));
        if (vlistInformeDenunciante.Count() > 0)
        {
            this.grvInformacionInformeDenunciante.DataSource = vlistInformeDenunciante;
            this.grvInformacionInformeDenunciante.DataBind();
        }
        else
        {
            this.grvInformacionInformeDenunciante.PageSize = PageSize();
            this.grvInformacionInformeDenunciante.EmptyDataText = EmptyDataText();
            this.grvInformacionInformeDenunciante.DataBind();
        }
    }

    protected void grvInformacionInformeDenunciante_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(grvInformacionInformeDenunciante.SelectedRow);
        NavigateTo(SolutionPage.Detail);
    }

    #endregion

    #region Metodos

    private void Buscar()
    {
        try
        {
            bool bandera = false;
            if (this.FechaDesde.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.CustomValidator4.IsValid = false;
                bandera = true;
            }
            if (this.FechaHasta.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.CustomValidator3.IsValid = false;
                bandera = true;
            }

            if (bandera)
            {
                return;
            }
            this.grvInformacionInformeDenunciante.PageSize = PageSize();
            this.grvInformacionInformeDenunciante.EmptyDataText = EmptyDataText();

            List<InformeDenunciante> vlistInformeDenunciante = this.vMostrencosService.ConsultarDatosInformeDenunciante(vIdDenunciaBien, this.FechaDesde.Date.ToString("yyyy-MM-dd"), this.FechaHasta.Date.ToString("yyyy-MM-dd"));
            if (vlistInformeDenunciante.Count() > 0)
            {
                this.grvInformacionInformeDenunciante.DataSource = vlistInformeDenunciante;

            }
            this.grvInformacionInformeDenunciante.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValueIdInformeDenunciante = string.Empty;


            if (grvInformacionInformeDenunciante.DataKeys[rowIndex].Values["IdInformeDenunciante"] != null)
            {
                strValueIdInformeDenunciante = grvInformacionInformeDenunciante.DataKeys[rowIndex].Values["IdInformeDenunciante"].ToString();
            }

            SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante", strValueIdInformeDenunciante);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Informe del Denunciante");

            toolBar.eventoBuscar += btnConsultar_Click;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            this.FechaDesde.HabilitarObligatoriedad(false);
            this.FechaHasta.HabilitarObligatoriedad(false);
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));
            //Valida el estado de la denuncia si es diferente a Asignada o Dañada
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);


            if ((vRegistroDenuncia.IdEstado != 11) && vRegistroDenuncia.IdEstado != 13 && (vRegistroDenuncia.IdEstado != 19))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            }


        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {

            //List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));

            if (this.vIdDenunciaBien != 0)
            {
                List<Tercero> vTercero = new List<Tercero>();
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                if (vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                }
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;

                string vMensaje = this.GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.MensajeError").ToString();
                if (!string.IsNullOrEmpty(vMensaje))
                {
                    this.toolBar.MostrarMensajeError(vMensaje);
                    this.SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.MensajeError", null);
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
                vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                //this.tbHistoria.DataSource = vDenunciaBien.ListaHistoricoEstadosDenunciaBien;
                //this.tbHistoria.DataBind();
            }
            this.FechaDesde.Focus();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

}