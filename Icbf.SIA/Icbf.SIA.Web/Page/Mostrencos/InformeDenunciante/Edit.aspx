﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Edit.aspx.cs" Inherits="Page_Mostrencos_InformeDenunciante_Edit" %>

<%--<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>--%>
<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <style type="text/css">
                .Background {
                    background-color: Black;
                    filter: alpha(opacity=90);
                    opacity: 0.8;
                }

                .Popup {
                    background-color: #FFFFFF;
                    border-width: 3px;
                    border-style: solid;
                    border-color: black;
                    padding-top: 10px;
                    padding-left: 10px;
                    width: 720px;
                    height: 450px;
                }

                .lbl {
                    font-size: 16px;
                    font-style: italic;
                    font-weight: bold;
                }
            </style>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado denuncia *</td>
                        <td>Fecha de Radicado de la Denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en Correspondencia *</td>
                        <td>Fecha Radicado en Correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de Identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Primer Nombre *</td>
                        <td>Segundo Nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Primer Apellido *</td>
                        <td>Segundo Apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Descripción de la denuncia *
                    <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                        ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                    </a>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionInformeDenunciante">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información Informe del Denunciante</td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha del Informe *
                        <td></td>
                        <td>Descripción del informe resumen ejecutivo *
                    <asp:RequiredFieldValidator runat="server" ID="reqDescripcionInforme" 
                        ControlToValidate="txtDescripcionInforme" ValidationGroup="btnGuardar" 
                        SetFocusOnError="true" Display="Dynamic" ForeColor="Red" 
                        ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaInforme" Requerid="false" Enabled="false" Width="250px"/>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox ID="txtDescripcionInforme" runat="server" TextMode="MultiLine" Enabled="true" Height="46px" Width="250px" 
                                onkeypress="return CheckLengthDescripcion();" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                            TargetControlID="txtDescripcionInforme"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                            ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Documento del Informe Resumen Ejecutivo *
                    <asp:CompareValidator ControlToValidate="ddlDocumentoInforme" ID="reqDocumentoInforme" ValidationGroup="btnGuardar"
                        SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" runat="server" Operator="NotEqual" ValueToCompare="-1" Type="Integer" />
                        </td>
                        <td></td>
                        <td>Nombre del archivo *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlDocumentoInforme" runat="server" DataTextField="NombreDocumento" DataValueField="IdTipoDocumentoSoporteDenuncia" 
                                Enabled="true" Height="24px" Width="250px" Requerid="true">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td>
                            <asp:FileUpload ID="fulArchivoRecibido" runat="server" Height="18px" Width="250px" />
                            <asp:Label runat="server" ID="lblNombreArchivo" Visible="true" Enabled="true"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="PopUpHistorico hidden" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 15%; width: 70%; background-color: white; border: 1px solid #dfdfdf;" BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;</div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; height: 200px; align-content: center">
                                <asp:GridView ID="gvwHistoricoDenuncia" runat="server" Visible="true" AutoGenerateColumns="False" GridLines="None">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" ItemStyle-Width="20%" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" ItemStyle-Width="20%" />
                                        <asp:BoundField HeaderText="Actuacion" DataField="Actuacion" ItemStyle-Width="15%" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" ItemStyle-Width="15%" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <script type="text/javascript">
                $(document).ready(function () {
                    $(document).on('click', '.btnPopUpHistorico', function () {
                        $('.PopUpHistorico').removeClass('hidden');
                    });
                    $(document).on('click', '.btnCerrarPop', function () {
                        $('.PopUpHistorico').addClass('hidden');
                    });
                    $('#cphCont_txtDescripcionInforme').on('input', function (e) {
                        if (document.getElementById("cphCont_txtDescripcionInforme").value.length > 512) {
                            this.value = document.getElementById("cphCont_txtDescripcionInforme").value.substr(0, 512);
                        }
                    });
                    $('#cphCont_txtDescripcion').on('input', function (e) {
                        if (document.getElementById("cphCont_txtDescripcion").value.length > 512) {
                            this.value = document.getElementById("cphCont_txtDescripcion").value.substr(0, 512);
                        }
                    });
                });
                //
                function CheckLength() {
                    var textboxObservaciones = document.getElementById("cphCont_txtDescripcion").value;

                    if (textboxObservaciones.trim().length >= 512) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                function CheckLengthDescripcion() {
                    var textboxDescripcion = document.getElementById("cphCont_txtDescripcionInforme").value;

                    if (textboxDescripcion.trim().length >= 512) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
