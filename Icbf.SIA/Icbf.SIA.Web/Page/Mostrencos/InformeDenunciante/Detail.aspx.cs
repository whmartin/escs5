﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_InformeDenunciante_Detail : GeneralWeb
{
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/InformeDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    private int vIdDenunciaBien;

    #region Metodos

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Informe del Denunciante");
            toolBar.eventoBuscar += btnConsultar_Click;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));

            //Valida el estado de la denuncia si es diferente a Asignada o Dañada
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            if ((vRegistroDenuncia.IdEstado != 11) && (vRegistroDenuncia.IdEstado != 13) && vRegistroDenuncia.IdEstado != 19)
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            }
            this.pnlInformacionDenuncia.Enabled = false;
            this.pnlInformacionInformeDenunciante.Enabled = false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));

        if (vIdDenunciaBien != 0)
        {
            if (this.vIdDenunciaBien != 0)
            {
                List<Tercero> vTercero = new List<Tercero>();
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
            }
        }
        int vIdInformeDenunciante = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante"));

        if (vIdInformeDenunciante > 0)
        {
            List<InformeDenunciante> vlistInformeDenuncianteXID = this.vMostrencosService.ConsultarDatosInformeDenuncianteXID(vIdInformeDenunciante);
            if (vlistInformeDenuncianteXID.Count() > 0)
            {
                fecFechaInforme.Date = !string.IsNullOrEmpty(Convert.ToDateTime(vlistInformeDenuncianteXID[0].FechaInforme).Date.ToString()) ? vlistInformeDenuncianteXID[0].FechaInforme : DateTime.Now;
                txbDescripcionInforme.Text = !string.IsNullOrEmpty(vlistInformeDenuncianteXID[0].DescripcionInforme) ? vlistInformeDenuncianteXID[0].DescripcionInforme : string.Empty;
                ddlDocumentoInforme.DataSource = vlistInformeDenuncianteXID;
                ddlDocumentoInforme.DataBind();
                txbNombreArchivo.Text = !string.IsNullOrEmpty(vlistInformeDenuncianteXID[0].NombreArchivo) ? vlistInformeDenuncianteXID[0].NombreArchivo : string.Empty;
            }
        }

        string vMensaje = Convert.ToString(this.GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.Mensaje"));
        if (!string.IsNullOrEmpty(vMensaje))
        {
            this.toolBar.MostrarMensajeGuardado(vMensaje);
            this.SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.Mensaje", string.Empty);
        }
    }

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        //NavigateTo(SolutionPage.Edit);
        Response.Redirect("../../Mostrencos/InformeDenunciante/Edit.aspx");
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        //NavigateTo(SolutionPage.List);
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        int IdInformeDenunciante = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante"));
        int IdEliminarInforme = this.vMostrencosService.EliminarInformeDenunciante(IdInformeDenunciante);
        NavigateTo(SolutionPage.List);
    }

    #endregion
}