﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Mostrencos.Entity;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web;
using System.IO;

public partial class Page_Mostrencos_InformeDenunciante_Add : GeneralWeb
{
    #region Variables

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/InformeDenunciante";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// File Name
    /// </summary>
    private string vFileName = string.Empty;

    #endregion



    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));
        if (this.validarInforme(vIdDenunciaBien))
        {
            if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
            {
                if (!IsPostBack)
                {
                    CargarDatosIniciales();
                }
            }
        }
        else
        {
            NavigateTo(SolutionPage.List);
        }

    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));

            if (this.validarInforme(vIdDenunciaBien))
            {
                this.Guardar(vIdDenunciaBien);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        bool Esresultado = false;
        try
        {
            if (this.fulArchivoRecibido.FileName.Length == 0)
            {
                return true;
            }
            else
            {
                HttpFileCollection files = Request.Files;
                if (files.Count > 0)
                {
                    string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                    string extPdf = ".PDF".ToLower();
                    string extJgp = ".JPG".ToLower();
                    if (this.fulArchivoRecibido.FileName.Length > 50)
                    {
                        throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos.");
                    }
                    int tamano = this.fulArchivoRecibido.FileName.Length;
                    if (extFile != extPdf && extFile != extJgp)
                    {
                        throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                    }

                    int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                    if (vtamArchivo > 4096)
                    {
                        throw new Exception("El tamaño máximo del archivo es 4 Mb");
                    }

                    HttpPostedFile file = files[pPosition];
                    if (file.ContentLength > 0)
                    {
                        string rutaParcial = string.Empty;
                        string carpetaBase = string.Empty;
                        string filePath = string.Empty;
                        if (extFile.Equals(".pdf"))
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        }
                        else if (extFile.ToLower().Equals(".jpg"))
                        {
                            rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                            carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/  "), pIdDenunciaBien);
                            filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        }
                        bool EsexisteCarpeta = System.IO.Directory.Exists(carpetaBase);
                        if (!EsexisteCarpeta)
                        {
                            System.IO.Directory.CreateDirectory(carpetaBase);
                            ///return true;
                        }
                        //System.IO.File.WriteAllBytes(carpetaBase + "\\" + file.FileName, file.bytes);
                        this.vFileName = rutaParcial;
                        file.SaveAs(filePath);
                        Esresultado = true;
                    }
                }
            }

        }
        catch (HttpRequestValidationException httpex)
        {
            throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return Esresultado;
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/InformeDenunciante/Edit.aspx");
    }
    #endregion

    #region Metodos

    public void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Informe del Denunciante");
            this.FechaInforme.HabilitarObligatoriedad(false);

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));

            //Valida el estado de la denuncia si es diferente a Asignada o Dañada
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);



            if ((vRegistroDenuncia.IdEstado != 11) && (vRegistroDenuncia.IdEstado != 13))
            {
                toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

                //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                toolBar.OcultarBotonNuevo(true);
                toolBar.OcultarBotonEditar(true);
                //toolBar.MostrarBotonNuevo(false);
                //toolBar.MostrarBotonEditar(false);
                toolBar.MostrarBotonEliminar(false);
            }
            else
            {
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }

            this.CargarListaTipoDocumentoSoporteDenuncia();
            this.pnlInformacionDenuncia.Enabled = false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarDatosIniciales()
    {
        try
        {
            this.txbDescripcionInforme.Focus();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien"));
            this.CargarGrillaHistorico(vIdDenunciaBien);
            if (vIdDenunciaBien != 0)
            {

                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
                this.FechaInforme.Date = this.AsignarFecha(vIdDenunciaBien);
                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    private void CargarListaTipoDocumentoSoporteDenuncia()
    {
        List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
        vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
        vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
        this.ddlDocumentoInforme.DataSource = vlstTipoDocumentosBien;
        this.ddlDocumentoInforme.DataTextField = "NombreTipoDocumento";
        this.ddlDocumentoInforme.DataValueField = "IdTipoDocumentoBienDenunciado";
        this.ddlDocumentoInforme.DataBind();
        this.ddlDocumentoInforme.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        this.ddlDocumentoInforme.SelectedValue = "-1";
    }

    private void Guardar(int IdDenuncia)
    {

        DateTime ahora = DateTime.Now;

        if (this.GuardarArchivos(vIdDenunciaBien, 0))
        {
            InformeDenunciante pInformeDenunciante = new InformeDenunciante()
            {
                IdDenunciaBien = vIdDenunciaBien,
                IdDocumentoInforme = Convert.ToInt32(this.ddlDocumentoInforme.SelectedItem.Value),
                FechaInforme = this.FechaInforme.Date,
                DescripcionInforme = txbDescripcionInforme.Text,
                DocumentoInforme = this.ddlDocumentoInforme.SelectedItem.Text,
                NombreArchivo = this.fulArchivoRecibido.FileName,
                UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper(),
                FechaCrea = ahora,
                UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper(),
                FechaModifica = ahora,
                idUsuarioCrea = GetSessionUser().IdUsuario
            };
            this.InformacionAudioria(pInformeDenunciante, this.PageName, vSolutionPage);
            int IdInformeDenunciante = this.vMostrencosService.InsertarInformeDenunciante(pInformeDenunciante);

            if (IdInformeDenunciante == -1)
            {
                toolBar.MostrarMensajeGuardado("El registro ya existe.");
                return;
            }
            SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdInformeDenunciante", pInformeDenunciante.IdInformeDenunciante);
            SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.Mensaje", "La información ha sido guardada exitosamente");
            this.NavigateTo(SolutionPage.Detail);
        }

    }

    private bool validarInforme(int IdDenuncia)
    {

        List<InformeDenunciante> vlistInformeDenuncianteIdDenuncia = this.vMostrencosService.ConsultarDatosInformeDenuncianteIdDenuncia(IdDenuncia);
        if (vlistInformeDenuncianteIdDenuncia.Count > 0)
        {
            if (vlistInformeDenuncianteIdDenuncia[vlistInformeDenuncianteIdDenuncia.Count - 1].FechaInforme > System.DateTime.Now)
            {
                SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.MensajeError", "Periodo menor a 90 días, no puede registrar el informe");
                return false;
            }
            else
            {
                return true;
            }

        }
        return true;
    }

    private DateTime AsignarFecha(int IdDenuncia)
    {
        List<InformeDenunciante> vlistInformeDenuncianteIdDenuncia = this.vMostrencosService.ConsultarDatosInformeDenuncianteIdDenuncia(IdDenuncia);
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

        if (vlistInformeDenuncianteIdDenuncia.Count > 0)
        {
            return this.CalculoDiaHabil(vlistInformeDenuncianteIdDenuncia[vlistInformeDenuncianteIdDenuncia.Count - 1].FechaInforme.AddDays(90));
        }
        else
        {
            HistoricoEstadosDenunciaBien historicoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
            historicoEstadosDenunciaBien = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 4 && x.IdActuacion == 9 && x.IdAccion == 13);
            return this.CalculoDiaHabil(historicoEstadosDenunciaBien.FechaCrea.AddDays(90));
        }
    }

    private DateTime CalculoDiaHabil(DateTime fechaInicial)
    {
        DateTime tempDate = fechaInicial;
        List<Feriados> feriados = new List<Feriados>();
        feriados = this.vMostrencosService.ObtenerFeriados();
        if (feriados.Count > 0)
        {
            while (tempDate.DayOfWeek == DayOfWeek.Saturday || tempDate.DayOfWeek == DayOfWeek.Sunday
                || feriados.Exists(t => t.fedID == tempDate))
            {
                tempDate = tempDate.AddDays(1);
            }
        }
        return tempDate;
    }



    #endregion

}