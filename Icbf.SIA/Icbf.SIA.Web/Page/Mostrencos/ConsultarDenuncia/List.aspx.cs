﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using NPOI.SS.Util;

public partial class Page_Mostrencos_ConsultarDenuncia_List : GeneralWeb
{
    #region "Variables"

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/ConsultarDenuncia";

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// vIdDenunciaBien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// vResultFiltroMueble
    /// </summary>
    List<ResultFiltroMueble> vResultFiltroMueble = new List<ResultFiltroMueble>();

    /// <summary>
    /// vResultFiltroTitulo
    /// </summary>
    List<ResultFiltroTitulo> vResultFiltroTitulo = new List<ResultFiltroTitulo>();

    /// <summary>
    /// vResultFiltroDenunciante
    /// </summary>
    List<ResultFiltroDenunciante> vResultFiltroDenunciante = new List<ResultFiltroDenunciante>();
    #endregion

    #region "Eventos"

    /// <summary>
    /// Page_PreInit
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Iniciar
    /// </summary>   
    public void Iniciar()
    {
        toolBar = (masterPrincipal)this.Master;
        toolBar.EstablecerTitulos("Verificar Denuncia Anterior");
        toolBar.eventoBuscar += BtnConsultar_Click;
        toolBar.eventoExcel += BtnExcel_Click;
        toolBar.eventoRetornar += new ToolBarDelegate(BtnRetornar_Click);
    }

    /// <summary>
    /// Page_Load
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.toolBar.LipiarMensajeError();

        if (!this.Page.IsPostBack)
        {
            this.CargarDatosIniciales();
        }
    }

    /// <summary>
    /// CkMueble_Checked
    /// </summary>   
    private void CkMueble_Checked()
    {
        toolBar.LipiarMensajeError();
        this.selecInmueble.Visible = false;
        this.selecMueble.Visible = false;
        this.selectTipoBien.Visible = false;
        this.pnlMueble.Visible = true;
        this.pnlTitulo.Visible = false;
        this.pnlDenunciante.Visible = false;
        this.CargarListaMarcaBien();
    }

    /// <summary>
    /// CkTitulo_Checked
    /// </summary> 
    private void CkTitulo_Checked()
    {
        toolBar.LipiarMensajeError();
        this.txtNroCuenta.Enabled = false;
        this.txtTituloValor.Enabled = false;
        this.dropEmisoraTitulo.Enabled = false;
        this.CargarListaTipoTitulo();
        this.pnlTitulo.Visible = true;
        this.pnlMueble.Visible = false;
        this.pnlDenunciante.Visible = false;
    }

    /// <summary>
    /// CkDenunciante_Checked
    /// </summary> 
    private void CkDenunciante_Checked()
    {
        toolBar.LipiarMensajeError();
        this.rblBuscarPor.SelectedIndex = 0;
        this.txtNroDefuncion.Enabled = false;
        this.pnlTitulo.Visible = false;
        this.pnlMueble.Visible = false;
        this.pnlDenunciante.Visible = true;
        this.cargarTipoIdentificacion();
    }

    /// <summary>
    /// DropTipoBien_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void DropTipoBien_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.dropTipoBien.Focus();
        this.LimpiarMsjTituloMueble();
        switch (this.dropTipoBien.SelectedValue)
        {
            case "-1":
                this.selectTipoBien.Visible = false;
                this.selecInmueble.Visible = false;
                this.selecMueble.Visible = false;
                this.LimpiarSubTipo();
                this.BorrarClase();
                break;

            case "3":
                this.CargarListaSubTipoBien("3");
                this.selecInmueble.Visible = true;
                this.selecMueble.Visible = false;
                this.selectTipoBien.Visible = true;
                this.BorrarClase();
                break;

            case "2":
                this.CargarListaSubTipoBien("2");
                this.selecInmueble.Visible = false;
                this.selecMueble.Visible = true;
                this.selectTipoBien.Visible = true;
                this.BorrarClase();
                break;
        }
    }

    /// <summary>
    /// BtnConsultar_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void BtnConsultar_Click(object sender, EventArgs e)
    {
        int NumRadDefuncion = 4;
        int FechaDefuncion = 5;
        this.lblRequeridoNumiden.Visible = false;
        this.lblRequeridoTipoIden.Visible = false;
        this.lblRequeridoConsultaPor.Visible = false;
        if (this.rblConsultaPor.SelectedIndex.ToString().Equals("-1"))
        {
            this.lblRequeridoConsultaPor.Visible = true;
        }
        else
        {
            this.lblRequeridoConsultaPor.Visible = false;
            if (this.rblConsultaPor.SelectedItem.Value.Equals("ckMueble"))
            {
                if (this.BusquedaMueble() != null)
                {
                    this.toolBar.LipiarMensajeError();
                    vResultFiltroMueble = (List<ResultFiltroMueble>)this.ViewState["SortedgvwMuebleInmueble"];
                    if (vResultFiltroMueble != null)
                    {
                        for (int i = 0; i < vResultFiltroMueble.Count; i++)
                        {
                            if (vResultFiltroMueble[i].fechaRadicado < vResultFiltroMueble[i].FechaDenuncia)
                            {
                                if (vResultFiltroMueble[i].tipoBien.Equals("INMUEBLE"))
                                {
                                    this.msjMuebleInmueble.Text = string.Concat("El bien denunciado ", vResultFiltroMueble[i].tipoBien,
                                    "  identificado con el número  ", vResultFiltroMueble[i].matriculaInmobiliaria,
                                    " se encuentra registrado con el radicado  ", vResultFiltroMueble[i].radicadoDenuncia,
                                    " con fecha ", vResultFiltroMueble[i].fechaRadicado,
                                    " anterior a la denuncia actual");
                                    this.msjMuebleInmueble.Visible = true;
                                    break;
                                }
                                else if (vResultFiltroMueble[i].tipoBien.Equals("MUEBLE"))
                                {
                                    this.msjMuebleInmueble.Text = string.Concat("El bien denunciado ", vResultFiltroMueble[i].tipoBien,
                                    "  identificado con el número  ", vResultFiltroMueble[i].nroIdentifica,
                                    " se encuentra registrado con el radicado  ", vResultFiltroMueble[i].radicadoDenuncia,
                                    " con fecha ", vResultFiltroMueble[i].fechaRadicado,
                                    " anterior a la denuncia actual");
                                    this.msjMuebleInmueble.Visible = true;
                                    break;
                                }
                            }
                        }
                    }

                    this.pnlFiltroMuebleInmueble.Visible = true;
                    this.tblMuebleInmueble.DataSource = vResultFiltroMueble;
                    this.tblMuebleInmueble.DataBind();
                }
            }
            else if (this.rblConsultaPor.SelectedItem.Value.Equals("ckTitulo"))
            {

                if (this.BuscarTitulo() != null)
                {
                    if (vResultFiltroTitulo != null)
                    {
                        for (int i = 0; i < vResultFiltroTitulo.Count; i++)
                        {
                            if (vResultFiltroTitulo[i].fechaRadicado < vResultFiltroTitulo[i].FechaDenuncia)
                            {
                                if (vResultFiltroTitulo[i].tipoTitulo.Equals("EFECTIVO"))
                                {
                                    this.msjTituloValor.Text = string.Concat("El bien denunciado ", vResultFiltroTitulo[i].tipoTitulo,
                                   "  identificado con el número  ", vResultFiltroTitulo[i].cuentaBancaria,
                                   " se encuentra registrado con el radicado  ", vResultFiltroTitulo[i].radicadoDenuncia,
                                   " con la fecha ", vResultFiltroTitulo[i].fechaRadicado, " anterior a la denuncia actual");
                                    this.msjTituloValor.Visible = true;
                                    break;
                                }
                                else if (vResultFiltroTitulo[i].tipoTitulo.Equals("CDT") || vResultFiltroTitulo[i].tipoTitulo.Equals("ACCIONES")
                                    || vResultFiltroTitulo[i].tipoTitulo.Equals("BONOS") || vResultFiltroTitulo[i].tipoTitulo.Equals("CHEQUES")
                                    || vResultFiltroTitulo[i].tipoTitulo.Equals("PAGARES") || vResultFiltroTitulo[i].tipoTitulo.Equals("FACTURAS"))
                                {
                                    this.msjTituloValor.Text = string.Concat("El bien denunciado ", vResultFiltroTitulo[i].tipoTitulo,
                                  "  identificado con el número  ", vResultFiltroTitulo[i].nroTituloValor,
                                  " se encuentra registrado con el radicado  ", vResultFiltroTitulo[i].radicadoDenuncia,
                                  " con la fecha ", vResultFiltroTitulo[i].fechaRadicado, " anterior a la denuncia actual");
                                    this.msjTituloValor.Visible = true;
                                    break;
                                }
                            }
                        }
                    }

                    this.toolBar.LipiarMensajeError();
                    this.pnlFiltroTituloValor.Visible = true;
                    this.tbTituloValor.DataSource = vResultFiltroTitulo;
                    this.tbTituloValor.DataBind();
                }
            }
            else if (this.rblConsultaPor.SelectedItem.Value.Equals("ckDenunciante"))
            {
                if (this.BuscarDenunciante() != null)
                {
                    this.toolBar.LipiarMensajeError();
                    this.pnlFiltroDenuncia.Visible = true;
                    if (this.rblBuscarPor.SelectedItem.Value.Equals("ckDenuncia"))
                    {
                        this.tbDenunciante.Columns[NumRadDefuncion].Visible = false;
                        this.tbDenunciante.Columns[FechaDefuncion].Visible = false;
                    }
                    else
                    {
                        this.tbDenunciante.Columns[NumRadDefuncion].Visible = true;
                        this.tbDenunciante.Columns[FechaDefuncion].Visible = true;
                    }
                    this.tbDenunciante.DataSource = vResultFiltroDenunciante;
                    this.tbDenunciante.DataBind();
                }
            }
        }
    }

    /// <summary>
    /// BtnExcel_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void BtnExcel_Click(object sender, EventArgs e)
    {
        if (this.tblMuebleInmueble.Rows.Count > 0 && this.rblConsultaPor.SelectedItem.Value.Equals("ckMueble"))
        {
            Exportar(this.tblMuebleInmueble, "MuebleInmueble");
        }
        else if (this.tbTituloValor.Rows.Count > 0 && this.rblConsultaPor.SelectedItem.Value.Equals("ckTitulo"))
        {
            Exportar(this.tbTituloValor, "TituloValor");
        }
        else if (this.tbDenunciante.Rows.Count > 0 && this.rblBuscarPor.SelectedItem.Value.Equals("ckDenuncia") && ViewState["SortedgvwDenunciante"] != null)
        {
            Exportar(this.tbDenunciante, "Denunciante");
        }
        else if (this.tbDenunciante.Rows.Count > 0 && this.rblBuscarPor.SelectedItem.Value.Equals("ckCausante") && ViewState["SortedgvwDenunciante"] != null)
        {
            Exportar(this.tbDenunciante, "Causante");
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
    }

    /// <summary>
    /// BtnRetornar_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void BtnRetornar_Click(object sender, EventArgs e)
    {
        bool valido = true;
        DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(Convert.ToInt32(GetSessionParameter("Mostrencos.VerificarDenunciaAnterior.IdDenunciaBien")));
        if (vDenuncia.IdEstadoDenuncia == 11 || vDenuncia.IdEstadoDenuncia == 13)
        {
            Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
        }
        else
        {
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(Convert.ToInt32(GetSessionParameter("Mostrencos.VerificarDenunciaAnterior.IdDenunciaBien")));

            if (vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Count > 0)
            {
                for (int i = 0; i < vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Count; i++)
                {
                    if (vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdFase == 1
                        && vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdAccion == 2
                        && vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdActuacion == 6)
                    {
                        valido = false;
                        break;
                    }
                }
            }

            if (this.ckNo.Checked && valido)
            {
                this.pnlInfRetorno.Visible = true;
            }
            else
            {
                Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
            }
        }


    }

    /// <summary>
    /// DropTipoTitulo_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void DropTipoTitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.LimpiarMsjTituloMueble();
        this.CargarListaEntidadEmisora();
        if (this.dropTipoTitulo.SelectedItem.ToString().Equals("EFECTIVO"))
        {
            this.txtNroCuenta.Enabled = true;
            this.txtTituloValor.Enabled = false;
            this.dropEmisoraTitulo.Enabled = false;
            this.txtTituloValor.Text = "";
            this.dropEmisoraTitulo.SelectedIndex = 0;
        }
        else
        {
            this.txtNroCuenta.Enabled = false;
            this.txtNroCuenta.Text = "";
            this.txtTituloValor.Enabled = true;
            this.dropEmisoraTitulo.Enabled = true;
        }
    }

    /// <summary>
    /// DropSubTipoBien_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void DropSubTipoBien_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.dropSubTipoBien.Focus();
        if (!this.dropSubTipoBien.SelectedValue.Equals("-1"))
        {
            this.CargarListaClasebien(this.dropSubTipoBien.SelectedValue);
            string vTipoParametro = this.dropSubTipoBien.SelectedValue.Substring(0, 3);
        }
        else
        {
            dropClaseBien.Items.Clear();
            dropClaseBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbHistoria.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.tbHistoria.DataSource = vList;
                this.tbHistoria.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }
                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }
                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }
                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }
                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }
                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.tbHistoria.DataSource = vResult;
        this.tbHistoria.DataBind();
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// BtnInfo_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void BtnInfo_Click(object sender, EventArgs e)
    {
        this.LimpiarCamposTVModal();
        ImageButton vl_lnkEncID = (ImageButton)sender;
        string vl_sValues = vl_lnkEncID.CommandArgument;
        List<ResultFiltroTitulo> busquedaTitulo = new List<ResultFiltroTitulo>();
        busquedaTitulo = this.vMostrencosService.BusquedaTitulo(vl_sValues);
        List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
        List<ClaseEntrada> vListaClaseEntrada = this.vMostrencosService.ConsultarClaseEntrada();
        List<EntidadBancaria> vListaEntidadBancaria = this.vMostrencosService.ConsultarEntidadBancaria();
        List<TipoCuenta> vListaTipoCuenta = this.vMostrencosService.ConsultarTipoCuenta();
        List<EntidadEmisora> vListaDepto = this.vMostrencosService.ConsultarEntidadEmisora();

        if (busquedaTitulo.Count > 0)
        {
            this.txtModalTipo.Text = busquedaTitulo[0].nombreTitulo;
            this.txtModalNroCuenta.Text = busquedaTitulo[0].cuentaBancaria;
            this.txtModalNroTitulo.Text = busquedaTitulo[0].nroTituloValor;
            this.txtModalIdenBien.Text = busquedaTitulo[0].NumeroIdentificacionBien;
            this.txtModalCanTítulos.Text = busquedaTitulo[0].CantidadTitulos;
            this.txtModalValorUni.Text = (busquedaTitulo[0].ValorUnitarioTitulo.Equals("0,00")) ? string.Empty : Convert.ToDecimal(busquedaTitulo[0].ValorUnitarioTitulo).ToString("N2");
            this.txtModalValorEfectivo.Text = (busquedaTitulo[0].ValorEfectivoEstimado.Equals("0,00")) ? string.Empty : Convert.ToDecimal(busquedaTitulo[0].ValorEfectivoEstimado).ToString("N2"); ;
            this.txtModalCantVendida.Text = busquedaTitulo[0].CantidadVendida.ToString();
            this.txtModalDesTi.Text = busquedaTitulo[0].DescripcionTitulo;
            this.txtModalDepartamento.Text = busquedaTitulo[0].Departamento;
            this.txtModalMunicipio.Text = busquedaTitulo[0].Municipio;
            this.txtModalFechaVenc.Text = (busquedaTitulo[0].FechaVencimiento.ToString("dd/MM/yyyy").Equals("01/01/0001")) ? string.Empty : busquedaTitulo[0].FechaVencimiento.ToString("dd/MM/yyyy");
            this.txtModalFechaRec.Text = (busquedaTitulo[0].FechaRecibido.ToString("dd/MM/yyyy").Equals("01/01/0001")) ? string.Empty : busquedaTitulo[0].FechaRecibido.ToString("dd/MM/yyyy");
            this.txtModalFechaVenta.Text = (busquedaTitulo[0].FechaVenta.ToString("dd/MM/yyyy").Equals("01/01/0001")) ? string.Empty : busquedaTitulo[0].FechaVenta.ToString("dd/MM/yyyy");
            this.txtModalFechaAdj.Text = (busquedaTitulo[0].FechaAdjudicado.ToString("dd/MM/yyyy").Equals("01/01/0001")) ? string.Empty : busquedaTitulo[0].FechaAdjudicado.ToString("dd/MM/yyyy");
            this.txtModalEstado.Text = busquedaTitulo[0].estado;

            if (!busquedaTitulo[0].ClaseEntrada.Equals("-1"))
            {
                for (int i = 0; i < vListaClaseEntrada.Count; i++)
                {
                    if (vListaClaseEntrada[i].IdClaseEntrada.Equals(busquedaTitulo[0].ClaseEntrada))
                    {
                        this.txtModalClaseEntra.Text = vListaClaseEntrada[i].NombreClaseEntrada;
                    }
                }
            }

            if (busquedaTitulo[0].TipoCuentaBancaria.Length > 0)
            {
                for (int i = 0; i < vListaTipoCuenta.Count; i++)
                {
                    if (vListaTipoCuenta[i].IdTipoCta.Equals(busquedaTitulo[0].TipoCuentaBancaria))
                    {
                        this.txtModalTipoCuentaBancaria.Text = vListaTipoCuenta[i].NombreTipoCta;
                    }
                }
            }

            if (busquedaTitulo[0].EntidadBancaria.Length > 0)
            {
                for (int i = 0; i < vListaEntidadBancaria.Count; i++)
                {
                    if (busquedaTitulo[0].EntidadBancaria.Equals(vListaEntidadBancaria[i].IdEntidadBancaria))
                    {
                        this.txtModalEntidadBancaria.Text = vListaEntidadBancaria[i].NombreEntidadBancaria;
                    }
                }
            }

            if (busquedaTitulo[0].EntidadEmisora.Length > 0)
            {
                for (int i = 0; i < vListaDepto.Count; i++)
                {
                    if (vListaDepto[i].IdEntidadEmisora.Equals(busquedaTitulo[0].EntidadEmisora))
                    {
                        this.txtModalEntidadEmisora.Text = vListaDepto[i].NombreEntidadEmisora;
                    }
                }
            }

            for (int i = 0; i < vListaTipoIdent.Count; i++)
            {
                if (vListaTipoIdent[i].IdTipoIdentificacionPersonaNatural.ToString().Equals(busquedaTitulo[0].TipoIdentificacionEntidadEmisora))
                {
                    this.txtModalTipoIden.Text = vListaTipoIdent[i].NombreTipoIdentificacionPersonaNatural;
                }
            }
            this.ModalTitulo.Visible = true;
        }
    }

    /// <summary>
    /// btnInfoDenuncia_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnInfoDenuncia_Click(object sender, EventArgs e)
    {
        this.LimpiarCamposMueble();
        ImageButton vl_lnkEncID = (ImageButton)sender;
        string vl_sValues = vl_lnkEncID.CommandArgument;
        List<ResultFiltroMueble> busquedaMueble = new List<ResultFiltroMueble>();
        busquedaMueble = this.vMostrencosService.BusquedaMueble(vl_sValues);

        if (busquedaMueble.Count() > 0)
        {
            List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(this.dropTipoBien.SelectedItem.Value);
            List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(busquedaMueble[0].subTipoBien);
            List<ClaseEntrada> vListaClaseEntrada = this.vMostrencosService.ConsultarClaseEntrada();
            List<EstadoFisicoBien> vListaEstadoFisicoBien = this.vMostrencosService.ConsultarEstadoFisicoBien();
            List<DestinacionEconomica> vListaDestinacionEconomica = this.vMostrencosService.ConsultarDestinacionEconomica();
            List<MarcaBien> vListaMarcaBien = this.vMostrencosService.ConsultMarcaBien();
            busquedaMueble[0].tipoBien = this.dropTipoBien.SelectedItem.Text;

            for (int i = 0; i < vListaSubTipoBien.Count; i++)
            {
                if (vListaSubTipoBien[i].IdSubTipoBien == busquedaMueble[0].subTipoBien)
                {
                    busquedaMueble[0].subTipoBien = vListaSubTipoBien[i].NombreSubTipoBien;
                    break;
                }
            }
            for (int j = 0; j < vListaClaseBien.Count; j++)
            {
                if (vListaClaseBien[j].IdClaseBien == busquedaMueble[0].ClaseBien)
                {
                    busquedaMueble[0].ClaseBien = vListaClaseBien[j].NombreClaseBien;
                    break;
                }
            }

            List<GN_VDIP> vListamun = this.vMostrencosService.ConsultarMunicipiosSeven(busquedaMueble[0].DepartamentoSeven);

            for (int i = 0; i < vListamun.Count; i++)
            {
                if (vListamun[i].CODIGO_MUNICIPIO.Equals(busquedaMueble[0].MunicipioSeven))
                {
                    busquedaMueble[0].MunicipioSeven = vListamun[i].NOMBRE_MUNICIPIO;
                    break;
                }
            }

            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarDepartamentosSeven();

            for (int i = 0; i < vListaDepto.Count; i++)
            {
                if (busquedaMueble[0].DepartamentoSeven.Equals(vListaDepto[i].CODIGO_DEPARTAMENTO))
                {
                    busquedaMueble[0].DepartamentoSeven = vListaDepto[i].NOMBRE_DEPARTAMENTO;
                    break;
                }
            }
            for (int i = 0; i < vListaClaseEntrada.Count; i++)
            {
                if (vListaClaseEntrada[i].IdClaseEntrada.Equals(busquedaMueble[0].ClaseEntrada))
                {
                    this.txtModalClaseEntrada.Text = vListaClaseEntrada[i].NombreClaseEntrada;
                    break;
                }
            }
            for (int i = 0; i < vListaEstadoFisicoBien.Count; i++)
            {
                if (vListaEstadoFisicoBien[i].IdEstadoFisicoBien.Equals(busquedaMueble[0].EstadoFisicoBien))
                {
                    this.txtModalEstadoFisico.Text = vListaEstadoFisicoBien[i].NombreEstadoFisicoBien;
                    break;
                }
            }
            for (int i = 0; i < vListaDestinacionEconomica.Count; i++)
            {
                if (vListaDestinacionEconomica[i].IdDestinacionEconomica.Equals(busquedaMueble[0].DestinacionEconomica))
                {
                    this.txtModalDestinacion.Text = vListaDestinacionEconomica[i].NombreDestinacionEconomica;
                    break;
                }
            }
            for (int i = 0; i < vListaMarcaBien.Count; i++)
            {
                if (vListaMarcaBien[i].IdMarcaBien.Equals(busquedaMueble[0].MarcaBien))
                {
                    this.txtModalMarca.Text = vListaMarcaBien[i].NombreMarcaBien;
                    break;
                }
            }
            this.txtModalTipoBien.Text = busquedaMueble[0].tipoBien;
            this.txtModalSubTipoBien.Text = busquedaMueble[0].subTipoBien;
            this.txtModalClaseBien.Text = busquedaMueble[0].ClaseBien;
            this.txtModalDepartamentoBien.Text = busquedaMueble[0].DepartamentoSeven;
            this.txtModalMunicipioBien.Text = busquedaMueble[0].MunicipioSeven;
            this.txtModalValorEstimado.Text = Convert.ToDecimal(busquedaMueble[0].ValorEstimadoComercial).ToString("N2");
            this.txtModalPorcentaje.Text = busquedaMueble[0].PorcentajePertenenciaBien;
            this.txtModalMatricula.Text = busquedaMueble[0].matriculaInmobiliaria;
            this.txtModalCedula.Text = busquedaMueble[0].cedulaCatastral;
            this.txtModalDireccion.Text = busquedaMueble[0].DireccionInmueble;
            this.txtModalNuIdenti.Text = busquedaMueble[0].nroIdentifica;
            this.txtModalFechaVentaBien.Text = !string.IsNullOrEmpty(busquedaMueble[0].FechaVenta.ToString()) ? (
                    busquedaMueble[0].FechaVenta != Convert.ToDateTime("01/01/0001")
                    ? busquedaMueble[0].FechaVenta.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtModalAdjudicacion.Text = !string.IsNullOrEmpty(busquedaMueble[0].FechaAdjudicado.ToString()) ? (
                    busquedaMueble[0].FechaAdjudicado != Convert.ToDateTime("01/01/0001")
                    ? busquedaMueble[0].FechaAdjudicado.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtModalRegistro.Text = !string.IsNullOrEmpty(busquedaMueble[0].FechaRegistro.ToString()) ? (
                    busquedaMueble[0].FechaRegistro != Convert.ToDateTime("01/01/0001")
                    ? busquedaMueble[0].FechaRegistro.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtModalEstadoBien.Text = busquedaMueble[0].estado;
            this.txtModalDescripcion.Text = busquedaMueble[0].DescripcionBien;

            this.ModalMuebleInmueble.Visible = true;
        }
    }

    /// <summary>
    /// BtnAceptar_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void BtnAceptar_Click(object sender, EventArgs e)
    {
        this.pnlInfRetorno.Visible = false;
        this.PModalExiste.Visible = true;
    }

    /// <summary>
    /// BtnCancelar_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void BtnCancelar_Click(object sender, EventArgs e)
    {
        this.pnlInfRetorno.Visible = false;
    }

    /// <summary>
    /// RblConsultaPor_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void RblConsultaPor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.rblConsultaPor.Focus();
            this.LimpiarMsjTituloMueble();
            switch (this.rblConsultaPor.SelectedItem.Value.ToString())
            {
                case "ckMueble":
                    this.LimpiarFiltroTitulo();
                    this.LimpiarFiltroDenuncuanteCausante();
                    this.dropTipoBien.ClearSelection();
                    CkMueble_Checked();
                    break;
                case "ckTitulo":
                    this.LimpiarFiltroMuebleInmueble();
                    this.LimpiarFiltroDenuncuanteCausante();
                    this.dropTipoTitulo.ClearSelection();
                    CkTitulo_Checked();
                    break;
                case "ckDenunciante":
                    this.LimpiarFiltroTitulo();
                    this.LimpiarFiltroMuebleInmueble();
                    CkDenunciante_Checked();
                    break;
                default:
                    this.lblRequeridoConsultaPor.Visible = true;
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// RblBuscarPor_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void RblBuscarPor_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.rblBuscarPor.Focus();
        this.limpiarDenuncianteCusante();
        switch (this.rblBuscarPor.SelectedItem.Value.ToString())
        {
            case "ckDenuncia":
                CkDenuncia_Checked();
                break;
            case "ckCausante":
                CkCausante_Checked();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// btnSi_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnSi_Click(object sender, EventArgs e)
    {
        ActualizarHistorico();
        this.GuardarSiNo(true);
        this.SetSessionParameter("Mostrencos.ConsultarDenunciaPrevia.Mensaje", "Se ejecutó y cerró la Actuación de Verificar Denuncia Anterior");
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// btnNo_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnNo_Click(object sender, EventArgs e)
    {
        this.ActualizarHistorico();
        this.GuardarSiNo(false);
        this.MostrarMensajeGuardadoMasterPrincipal("Se ejecutó y cerró la Actuación de Verificar Denuncia Anterior");
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void tbDenunciante_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<ResultFiltroDenunciante> vList = new List<ResultFiltroDenunciante>();
        List<ResultFiltroDenunciante> vResult = new List<ResultFiltroDenunciante>();
        if (ViewState["SortedgvwDenunciante"] != null)
        {
            vList = (List<ResultFiltroDenunciante>)ViewState["SortedgvwDenunciante"];
        }
        switch (e.SortExpression)
        {
            case "radicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.radicadoDenuncia).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.radicadoDenuncia).ToList();
                }
                break;
            case "TipoIdentificacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoIdentificacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoIdentificacion).ToList();
                }
                break;
            case "RazonSocial":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.RazonSocial).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.RazonSocial).ToList();
                }
                break;
            case "fechaRadicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.fechaRadicadoDenuncia).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.fechaRadicadoDenuncia).ToList();
                }
                break;
            case "NroRegDefuncion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NroRegDefuncion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NroRegDefuncion).ToList();
                }
                break;
            case "fechaDefuncion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.fechaDefuncion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.fechaDefuncion).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        ViewState["SortedgvwDenunciante"] = vResult;
        this.tbDenunciante.DataSource = vResult;
        this.tbDenunciante.DataBind();
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void tblMuebleInmueble_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tblMuebleInmueble.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwMuebleInmueble"] != null)
            {
                List<ResultFiltroMueble> vList = (List<ResultFiltroMueble>)this.ViewState["SortedgvwMuebleInmueble"];
                this.tblMuebleInmueble.DataSource = vList;
                this.tblMuebleInmueble.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void tbDenunciante_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbDenunciante.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwDenunciante"] != null)
            {
                List<ResultFiltroDenunciante> vList = (List<ResultFiltroDenunciante>)this.ViewState["SortedgvwDenunciante"];
                this.tbDenunciante.DataSource = vList;
                this.tbDenunciante.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void tblMuebleInmueble_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<ResultFiltroMueble> vList = new List<ResultFiltroMueble>();
        List<ResultFiltroMueble> vResult = new List<ResultFiltroMueble>();
        if (ViewState["SortedgvwMuebleInmueble"] != null)
        {
            vList = (List<ResultFiltroMueble>)ViewState["SortedgvwMuebleInmueble"];
        }
        switch (e.SortExpression)
        {
            case "radicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.radicadoDenuncia).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.radicadoDenuncia).ToList();
                }
                break;
            case "NombRazonSocial":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombRazonSocial).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombRazonSocial).ToList();
                }
                break;
            case "fechaRadicado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.fechaRadicado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.fechaRadicado).ToList();
                }
                break;
            case "TipoBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.tipoBien).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.tipoBien).ToList();
                }
                break;
            case "matriculaInmobiliaria":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.matriculaInmobiliaria).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.matriculaInmobiliaria).ToList();
                }
                break;
            case "cedulaCatastral":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.cedulaCatastral).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.cedulaCatastral).ToList();
                }
                break;
            case "nroIdentifica":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.nroIdentifica).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.nroIdentifica).ToList();
                }
                break;
            case "Observaciones":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.observaciones).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.observaciones).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwMuebleInmueble"] = vResult;
        this.tblMuebleInmueble.DataSource = vResult;
        this.tblMuebleInmueble.DataBind();
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void tbTituloValor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbTituloValor.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwTituloValor"] != null)
            {
                List<ResultFiltroTitulo> vList = (List<ResultFiltroTitulo>)this.ViewState["SortedgvwTituloValor"];
                this.tbTituloValor.DataSource = vList;
                this.tbTituloValor.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void tbTituloValor_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<ResultFiltroTitulo> vList = new List<ResultFiltroTitulo>();
        List<ResultFiltroTitulo> vResult = new List<ResultFiltroTitulo>();
        if (ViewState["SortedgvwTituloValor"] != null)
        {
            vList = (List<ResultFiltroTitulo>)ViewState["SortedgvwTituloValor"];
        }
        switch (e.SortExpression)
        {
            case "radicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.radicadoDenuncia).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.radicadoDenuncia).ToList();
                }
                break;
            case "NombRazonSocial":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombRazonSocial).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombRazonSocial).ToList();
                }
                break;
            case "fechaRadicado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.fechaRadicado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.fechaRadicado).ToList();
                }
                break;
            case "tipoTitulo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.tipoTitulo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.tipoTitulo).ToList();
                }
                break;
            case "cuentaBancaria":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.cuentaBancaria).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.cuentaBancaria).ToList();
                }
                break;
            case "nroTituloValor":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.nroTituloValor).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.nroTituloValor).ToList();
                }
                break;
            case "Observaciones":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.observaciones).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.observaciones).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        ViewState["SortedgvwTituloValor"] = vResult;
        this.tbTituloValor.DataSource = vResult;
        this.tbTituloValor.DataBind();
    }

    /// <summary>
    /// ibCerrarModal_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">ImageClickEventArgs</param>
    protected void ibCerrarModal_Click(object sender, ImageClickEventArgs e)
    {
        this.ModalTitulo.Visible = false;
    }

    /// <summary>
    /// btnCerrarInmuebleModal_Click
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">ImageClickEventArgs</param>
    protected void btnCerrarInmuebleModal_Click(object sender, ImageClickEventArgs e)
    {
        this.ModalMuebleInmueble.Visible = false;
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.rblConsultaPor.Focus();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.VerificarDenunciaAnterior.IdDenunciaBien"));
            this.CargarGrillaHistorico(vIdDenunciaBien);
            this.CargarListaEntidadEmisora();
            this.CargarListaTipoTitulo();
            this.BorrarClase();
            if (vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;

                DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                if (vDenuncia.IdEstadoDenuncia == 11 || vDenuncia.IdEstadoDenuncia == 13)
                {
                    toolBar.OcultarBotonBuscar(false);
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                if (vDenunciaBien.ExisteDenuncia)
                {
                    this.ckSi.Checked = true;
                    this.ckNo.Checked = false;
                }
                else
                {
                    this.ckSi.Checked = false;
                    this.ckNo.Checked = true;
                }

                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            }
            string vMensaje = (string)this.GetSessionParameter("Mostrencos.ConsultarDenuncia.Mensaje");
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.ConsultarDenuncia.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Exportar(GridView vGridView, string tipo)
    {
        toolBar.LipiarMensajeError();
        DataTable datos = new DataTable();
        Boolean vPaginador = vGridView.AllowPaging;
        Boolean vPaginadorError = vGridView.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        vGridView.AllowPaging = false;

        if (tipo.Equals("MuebleInmueble"))
        {
            vResultFiltroMueble = (List<ResultFiltroMueble>)this.ViewState["SortedgvwMuebleInmueble"];
            datos = ManejoControlesContratos.GenerarDataTable(vGridView, vResultFiltroMueble);
        }
        else if (tipo.Equals("TituloValor"))
        {
            vResultFiltroTitulo = (List<ResultFiltroTitulo>)this.ViewState["SortedgvwTituloValor"];
            datos = ManejoControlesContratos.GenerarDataTable(vGridView, vResultFiltroTitulo);
        }
        else
        {
            vResultFiltroDenunciante = (List<ResultFiltroDenunciante>)this.ViewState["SortedgvwDenunciante"];
            datos = ManejoControlesContratos.GenerarDataTable(vGridView, vResultFiltroDenunciante);
        }

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                if (this.rblConsultaPor.SelectedItem.Value.Equals("ckDenunciante"))
                {

                    for (int i = 0; i < vGridView.Columns.Count; i++)
                    {
                        dt.Columns.Add(vGridView.Columns[i].HeaderText);
                    }
                    foreach (GridViewRow row in vGridView.Rows)
                    {
                        DataRow dr = dt.NewRow();
                        for (int j = 0; j < vGridView.Columns.Count; j++)
                        {
                            dr[vGridView.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                        }

                        dt.Rows.Add(dr);
                    }

                    if (this.rblBuscarPor.SelectedItem.Value.Equals("ckDenuncia"))
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {

                                if (dt.Columns[j].Caption.ToString().Equals("Número registro defunción")
                                    || dt.Columns[j].Caption.ToString().Equals("Fecha defunción"))
                                {
                                    dt.Columns.RemoveAt(j);
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 1; i < vGridView.Columns.Count; i++)
                    {
                        dt.Columns.Add(vGridView.Columns[i].HeaderText);
                    }
                    foreach (GridViewRow row in vGridView.Rows)
                    {
                        DataRow dr = dt.NewRow();
                        for (int j = 1; j < vGridView.Columns.Count; j++)
                        {
                            dr[vGridView.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                        }

                        dt.Rows.Add(dr);
                    }
                }

                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, tipo);
                vGridView.AllowPaging = vPaginador;
            }
            else
            {
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");

            }
        }
        else
        {
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
    }

    private void CkDenuncia_Checked()
    {
        this.txtNroDefuncion.Enabled = false;
    }

    private void CkCausante_Checked()
    {
        this.txtNroDefuncion.Enabled = true;
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.tbHistoria.DataSource = vHistorico;
            this.tbHistoria.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    private void CargarListaClasebien(string pIdSubTipoBien)
    {
        try
        {
            List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(pIdSubTipoBien);
            vListaClaseBien = vListaClaseBien.OrderBy(p => p.NombreClaseBien).ToList();
            this.dropClaseBien.DataSource = vListaClaseBien;
            this.dropClaseBien.DataTextField = "NombreClaseBien";
            this.dropClaseBien.DataValueField = "IdClaseBien";
            this.dropClaseBien.DataBind();
            this.dropClaseBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.dropClaseBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de SubTipo Bien
    /// </summary>
    private void CargarListaSubTipoBien(string pIdTipoBien)
    {
        try
        {
            List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(pIdTipoBien);
            vListaSubTipoBien = vListaSubTipoBien.OrderBy(p => p.NombreSubTipoBien).ToList();
            this.dropSubTipoBien.DataSource = vListaSubTipoBien;
            this.dropSubTipoBien.DataTextField = "NombreSubTipoBien";
            this.dropSubTipoBien.DataValueField = "IdSubTipoBien";
            this.dropSubTipoBien.DataBind();
            this.dropSubTipoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.dropSubTipoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Tipo Titulo
    /// </summary>
    private void CargarListaTipoTitulo()
    {
        try
        {
            if (this.dropTipoTitulo.Items.Count == 1)
            {
                List<TipoTitulo> vListaTipoTitulo = this.vMostrencosService.ConsultarTipoTitulo();
                vListaTipoTitulo = vListaTipoTitulo.OrderBy(p => p.NombreTipoTitulo).ToList();
                this.dropTipoTitulo.DataSource = vListaTipoTitulo;
                this.dropTipoTitulo.DataTextField = "NombreTipoTitulo";
                this.dropTipoTitulo.DataValueField = "IdTipoTitulo";
                this.dropTipoTitulo.DataBind();
                this.dropTipoTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
                this.dropTipoTitulo.SelectedValue = "-1";
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Entidad Emisora
    /// </summary>
    private void CargarListaEntidadEmisora()
    {
        try
        {
            if (this.dropEmisoraTitulo.Items.Count == 1)
            {
                List<EntidadEmisora> vListaDepto = this.vMostrencosService.ConsultarEntidadEmisora();
                vListaDepto = vListaDepto.OrderBy(p => p.NombreEntidadEmisora).ToList();
                this.dropEmisoraTitulo.DataSource = vListaDepto;
                this.dropEmisoraTitulo.DataTextField = "NombreEntidadEmisora";
                this.dropEmisoraTitulo.DataValueField = "IdEntidadEmisora";
                this.dropEmisoraTitulo.DataBind();
                this.dropEmisoraTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
                this.dropEmisoraTitulo.SelectedValue = "-1";
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método List<ResultFiltroDenunciante> BuscarDenunciante
    /// </summary>
    private List<ResultFiltroDenunciante> BuscarDenunciante()
    {
        if (this.CamposRequeridosCausantes())
        {
            this.pnlFiltroMuebleInmueble.Visible = false;
            this.pnlFiltroTituloValor.Visible = false;

            FiltroDenunciante vfiltroDenunciante = new FiltroDenunciante();
            vfiltroDenunciante.RadicadoDenuncia = this.txtRadicadoDenuncia.ToString();

            if (this.rblBuscarPor.SelectedItem.Value.Equals("ckDenuncia"))
            {
                vfiltroDenunciante.Denuncia = true;
            }
            else if (this.rblBuscarPor.SelectedItem.Value.Equals("ckCausante"))
            {
                vfiltroDenunciante.Denuncia = false;
            }
            if (this.txtNroIdentificacion.Text == string.Empty)
            {
                vfiltroDenunciante.NroIdentificacion = null;
            }
            else
            {
                vfiltroDenunciante.NroIdentificacion = this.txtNroIdentificacion.Text;
            }
            if (this.txtNroDefuncion.Text == string.Empty)
            {
                vfiltroDenunciante.RegistroDefuncion = "0";
            }
            else
            {
                vfiltroDenunciante.RegistroDefuncion = this.txtNroDefuncion.Text;
            }
            vfiltroDenunciante.RazonSocial = this.txtRazonSocial.Text;
            vfiltroDenunciante.TipoIdentificacion = this.dropTipoIdentificacion.SelectedValue.ToString();
            vfiltroDenunciante.RadicadoDenuncia = this.txtRadicadoDenuncia.Text.ToString();
            vResultFiltroDenunciante = new List<ResultFiltroDenunciante>();

            if (this.rblBuscarPor.SelectedItem.Value.Equals("ckDenuncia"))
            {
                vResultFiltroDenunciante = this.vMostrencosService.filtroDenunciante(vfiltroDenunciante);
            }
            else if (this.rblBuscarPor.SelectedItem.Value.Equals("ckCausante"))
            {
                vResultFiltroDenunciante = this.vMostrencosService.filtroCausante(vfiltroDenunciante);
            }

            if (vResultFiltroDenunciante.Count > 0)
            {
                if (!this.ckSi.Checked)
                {
                    this.GuardarSiNo(true);
                    this.ckSi.Checked = true;
                    this.ckNo.Checked = false;
                }
                this.ViewState["SortedgvwDenunciante"] = vResultFiltroDenunciante;
                return vResultFiltroDenunciante;
            }
            else
            {
                this.ViewState["SortedgvwDenunciante"] = null;
                List<ResultFiltroDenunciante> vacio = new List<ResultFiltroDenunciante>();
                return vacio;
            }

        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Método List<ResultFiltroTitulo> BuscarTitulo
    /// </summary>
    private List<ResultFiltroTitulo> BuscarTitulo()
    {
        if (this.CamposRequeridosTitulos())
        {
            this.pnlFiltroMuebleInmueble.Visible = false;
            this.pnlFiltroDenuncia.Visible = false;
            FiltroTitulo vfiltroTitulo = new FiltroTitulo();
            vfiltroTitulo.RadicadoDenuncia = this.txtRadicadoDenuncia.Text.ToString();
            vfiltroTitulo.tipoTitulo = this.dropTipoTitulo.SelectedItem.Text.ToString();
            vfiltroTitulo.nroCuentaBancaria = (this.txtNroCuenta.Text.Length > 0) ? vfiltroTitulo.nroCuentaBancaria = this.txtNroCuenta.Text : vfiltroTitulo.nroCuentaBancaria = "0";
            vfiltroTitulo.entidadEmisora = (this.dropEmisoraTitulo.SelectedItem.Text.Equals("SELECCIONE")) ? vfiltroTitulo.entidadEmisora = string.Empty : vfiltroTitulo.entidadEmisora = this.dropEmisoraTitulo.SelectedItem.Value;
            vfiltroTitulo.nroTituloValor = (this.txtTituloValor.Text.Length > 0) ? vfiltroTitulo.nroTituloValor = this.txtTituloValor.Text : vfiltroTitulo.nroTituloValor = "0";
            vResultFiltroTitulo = new List<ResultFiltroTitulo>();
            vResultFiltroTitulo = this.vMostrencosService.filtroTitulo(vfiltroTitulo);
            if (vResultFiltroTitulo.Count > 0)
            {
                if (!this.ckSi.Checked)
                {
                    this.GuardarSiNo(true);
                    this.ckSi.Checked = true;
                    this.ckNo.Checked = false;
                }
                this.ViewState["SortedgvwTituloValor"] = vResultFiltroTitulo;
                return vResultFiltroTitulo;
            }
            else
            {
                this.msjTituloValor.Visible = false;
                this.ViewState["SortedgvwTituloValor"] = null;
                List<ResultFiltroTitulo> vacio = new List<ResultFiltroTitulo>();
                return vacio;
            }


        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Método List<ResultFiltroMueble> BusquedaMueble
    /// </summary>
    private List<ResultFiltroMueble> BusquedaMueble()
    {
        if (this.CamposRequeridosMueble())
        {
            this.pnlFiltroTituloValor.Visible = false;
            this.pnlFiltroDenuncia.Visible = false;
            FiltroMueble vfiltroMueble = new FiltroMueble();
            vfiltroMueble.RadicadoDenuncia = this.txtRadicadoDenuncia.Text;
            vfiltroMueble.tipoBien = this.dropTipoBien.SelectedItem.Value;
            vfiltroMueble.subTipoBien = (this.dropSubTipoBien.SelectedItem.Text.Equals("SELECCIONE")) ? vfiltroMueble.subTipoBien = string.Empty : vfiltroMueble.subTipoBien = this.dropSubTipoBien.SelectedItem.Value;
            vfiltroMueble.claseBien = (this.dropClaseBien.SelectedItem.Text.Equals("SELECCIONE")) ? vfiltroMueble.claseBien = string.Empty : vfiltroMueble.claseBien = this.dropClaseBien.SelectedItem.Value;
            vfiltroMueble.nroIdenticacionBien = this.txtNumIdentificacion.Text;
            vfiltroMueble.matriculaInmobiliaria = this.txtMatricula.Text;
            vfiltroMueble.marcaBien = (this.dropMarcaBien.SelectedItem.Text.Equals("SELECCIONE")) ? vfiltroMueble.marcaBien = string.Empty : vfiltroMueble.marcaBien = this.dropMarcaBien.SelectedItem.Value;
            vfiltroMueble.cedulaCatastral = (this.txtCedCatastrasl.Text.Length > 0) ? vfiltroMueble.cedulaCatastral = Convert.ToInt64(this.txtCedCatastrasl.Text) : vfiltroMueble.cedulaCatastral = 0;
            vResultFiltroMueble = new List<ResultFiltroMueble>();
            vResultFiltroMueble = this.vMostrencosService.filtroMueble(vfiltroMueble);
            if (vResultFiltroMueble.Count > 0)
            {
                if (!this.ckSi.Checked)
                {
                    this.GuardarSiNo(true);
                    this.ckSi.Checked = true;
                    this.ckNo.Checked = false;
                }
                for (int a = 0; a < vResultFiltroMueble.Count(); a++)
                {
                    List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(this.dropTipoBien.SelectedItem.Value);
                    List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(vResultFiltroMueble[a].subTipoBien);
                    vResultFiltroMueble[a].tipoBien = this.dropTipoBien.SelectedItem.Text;

                    for (int i = 0; i < vListaSubTipoBien.Count; i++)
                    {
                        if (vListaSubTipoBien[i].IdSubTipoBien == vResultFiltroMueble[a].subTipoBien)
                        {
                            vResultFiltroMueble[a].subTipoBien = vListaSubTipoBien[i].NombreSubTipoBien;
                            break;
                        }
                    }
                    for (int j = 0; j < vListaClaseBien.Count; j++)
                    {
                        if (vListaClaseBien[j].IdClaseBien == vResultFiltroMueble[a].ClaseBien)
                        {
                            vResultFiltroMueble[a].ClaseBien = vListaClaseBien[j].NombreClaseBien;
                            break;
                        }
                    }

                    //municipio
                    List<GN_VDIP> vListamun = this.vMostrencosService.ConsultarMunicipiosSeven(vResultFiltroMueble[a].DepartamentoSeven);

                    for (int i = 0; i < vListamun.Count; i++)
                    {
                        if (vListamun[i].CODIGO_MUNICIPIO.Equals(vResultFiltroMueble[a].MunicipioSeven))
                        {
                            vResultFiltroMueble[a].MunicipioSeven = vListamun[i].NOMBRE_MUNICIPIO;
                            break;
                        }
                    }

                    //Departamento
                    List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarDepartamentosSeven();
                    for (int i = 0; i < vListaDepto.Count; i++)
                    {
                        if (vResultFiltroMueble[a].DepartamentoSeven.Equals(vListaDepto[i].CODIGO_DEPARTAMENTO))
                        {
                            vResultFiltroMueble[a].DepartamentoSeven = vListaDepto[i].NOMBRE_DEPARTAMENTO;
                            break;
                        }
                    }
                }
                this.ViewState["SortedgvwMuebleInmueble"] = vResultFiltroMueble;
                return vResultFiltroMueble;
            }
            else
            {
                this.msjMuebleInmueble.Visible = false;
                this.ViewState["SortedgvwMuebleInmueble"] = null;
                List<ResultFiltroMueble> vacio = new List<ResultFiltroMueble>();
                return vacio;
            }
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Método bool CamposRequeridosMueble
    /// </summary>
    private bool CamposRequeridosMueble()
    {
        if (this.dropTipoBien.SelectedItem.Text.Equals("SELECCIONE"))
        {
            this.lblRequeridTipoBien.Visible = true;
            return false;
        }
        else
        {
            this.lblRequeridTipoBien.Visible = false;
            return true;
        }

    }

    /// <summary>
    /// Método bool CamposRequeridosTitulos
    /// </summary>
    private bool CamposRequeridosTitulos()
    {
        if (this.dropTipoTitulo.SelectedItem.Text.Equals("SELECCIONE"))
        {
            this.lblTipoTitulo.Visible = true;
            return false;
        }
        else
        {
            this.lblTipoTitulo.Visible = false;
            return true;
        }
    }

    /// <summary>
    /// Método bool CamposRequeridosCausantes
    /// </summary>
    private bool CamposRequeridosCausantes()
    {
        bool validate = false;

        if (!this.rblBuscarPor.SelectedIndex.ToString().Equals("-1"))
        {
            this.lblBuscarCausanteDenun.Visible = false;
            if (this.txtNroIdentificacion.Text.Length == 0)
            {
                this.lblRequeridoNumiden.Visible = true;
                validate = false;
            }
            else
            {
                this.lblRequeridoNumiden.Visible = false;
                validate = true;
            }
            if (this.dropTipoIdentificacion.SelectedItem.Text.Equals("SELECCIONE"))
            {
                this.lblRequeridoTipoIden.Visible = true;
                validate = false;
            }
            else
            {
                this.lblRequeridoTipoIden.Visible = false;
                validate = true;
            }
        }
        else
        {
            this.lblBuscarCausanteDenun.Visible = true;
            validate = false;
        }
        return validate;

    }

    /// <summary>
    /// Método void LimpiarCamposTVModal
    /// </summary>
    private void LimpiarCamposTVModal()
    {

        this.txtModalTipo.Text = string.Empty;
        this.txtModalNroCuenta.Text = string.Empty;
        this.txtModalEntidadBancaria.Text = string.Empty;
        this.txtModalTipoCuentaBancaria.Text = string.Empty;
        this.txtModalDepartamento.Text = string.Empty;
        this.txtModalMunicipio.Text = string.Empty;
        this.txtModalValorEfectivo.Text = string.Empty;
        this.txtModalNroTitulo.Text = string.Empty;
        this.txtModalEntidadEmisora.Text = string.Empty;
        this.txtModalTipoIden.Text = string.Empty;
        this.txtModalIdenBien.Text = string.Empty;
        this.txtModalCanTítulos.Text = string.Empty;
        this.txtModalValorUni.Text = string.Empty;
        this.txtModalFechaVenc.Text = string.Empty;
        this.txtModalFechaRec.Text = string.Empty;
        this.txtModalFechaVenta.Text = string.Empty;
        this.txtModalCantVendida.Text = string.Empty;
        this.txtModalClaseEntra.Text = string.Empty;
        this.txtModalFechaAdj.Text = string.Empty;
        this.txtModalEstado.Text = string.Empty;
        this.txtModalDesTi.Text = string.Empty;
    }

    /// <summary>
    /// Método void LimpiarCamposMueble
    /// </summary>
    private void LimpiarCamposMueble()
    {
        this.txtModalTipoBien.Text = string.Empty;
        this.txtModalSubTipoBien.Text = string.Empty;
        this.txtModalClaseBien.Text = string.Empty;
        this.txtModalDepartamentoBien.Text = string.Empty;
        this.txtModalMunicipioBien.Text = string.Empty;
        this.txtModalClaseEntrada.Text = string.Empty;
        this.txtModalEstadoFisico.Text = string.Empty;
        this.txtModalValorEstimado.Text = string.Empty;
        this.txtModalPorcentaje.Text = string.Empty;
        this.txtModalDestinacion.Text = string.Empty;
        this.txtModalMatricula.Text = string.Empty;
        this.txtModalCedula.Text = string.Empty;
        this.txtModalDireccion.Text = string.Empty;
        this.txtModalNuIdenti.Text = string.Empty;
        this.txtModalMarca.Text = string.Empty;
        this.txtModalFechaVentaBien.Text = string.Empty;
        this.txtModalAdjudicacion.Text = string.Empty;
        this.txtModalRegistro.Text = string.Empty;
        this.txtModalEstadoBien.Text = string.Empty;
        this.txtModalDescripcion.Text = string.Empty;

    }

    /// <summary>
    /// Método void ActualizarHistorico
    /// </summary>
    private void ActualizarHistorico()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.VerificarDenunciaAnterior.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15; //ESTADO REGISTRADA //vRegistroDenuncia.IdEstado;
        pHistoricoEstadosDenunciaBien.IdFase = 1; //Fase = Analisis;
        pHistoricoEstadosDenunciaBien.IdAccion = 2; //Accion = VERIFICAR DENUNCIA ANTERIOR;
        pHistoricoEstadosDenunciaBien.IdActuacion = 6; //Actuacion = VERIFICAR DENUNCIA ANTERIOR;        
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.Fase = 1.ToString();
        pHistoricoEstadosDenunciaBien.Accion = 2.ToString();
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, vSolutionPage);
        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
    }

    /// <summary>
    /// Método void GuardarSiNo
    /// </summary>
    private void GuardarSiNo(bool SiNo)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.VerificarDenunciaAnterior.IdDenunciaBien"));
        this.vMostrencosService.Sino(SiNo, vIdDenunciaBien);
    }

    /// <summary>
    /// Método void BorrarClase
    /// </summary>
    private void BorrarClase()
    {
        if (this.dropSubTipoBien.SelectedValue.Equals("-1"))
        {
            dropClaseBien.Items.Clear();
            dropClaseBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }
    }

    /// <summary>
    /// Método void CargarListaMarcaBien
    /// </summary>
    private void CargarListaMarcaBien()
    {
        try
        {
            if (this.dropMarcaBien.Items.Count == 1)
            {
                List<MarcaBien> vListaMarcaBien = this.vMostrencosService.ConsultMarcaBien();
                vListaMarcaBien = vListaMarcaBien.OrderBy(p => p.NombreMarcaBien).ToList();
                this.dropMarcaBien.DataSource = vListaMarcaBien;
                this.dropMarcaBien.DataTextField = "NombreMarcaBien";
                this.dropMarcaBien.DataValueField = "IdMarcaBien";
                this.dropMarcaBien.DataBind();
                this.dropMarcaBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
                this.dropMarcaBien.SelectedValue = "-1";
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método void LimpiarSubTipo
    /// </summary>
    private void LimpiarSubTipo()
    {
        this.dropSubTipoBien.Items.Clear();
        this.dropSubTipoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
    }

    /// <summary>
    /// Método void cargarTipoIdentificacion
    /// </summary>
    private void cargarTipoIdentificacion()
    {
        if (this.dropTipoIdentificacion.Items.Count == 1)
        {
            List<TipoIdentificacion> vTipoIdentificacion = new List<TipoIdentificacion>();
            vTipoIdentificacion = this.vMostrencosService.ConsultarTipoIdentificacion();
            for (int i = 0; i < vTipoIdentificacion.Count; i++)
            {
                if (vTipoIdentificacion[i].CodigoTipoIdentificacionPersonaNatural != null && vTipoIdentificacion[i].CodigoTipoIdentificacionPersonaNatural != string.Empty)
                {
                    this.dropTipoIdentificacion.Items.Add(vTipoIdentificacion[i].CodigoTipoIdentificacionPersonaNatural);
                }
            }
        }
    }

    /// <summary>
    /// Método void LimpiarFiltroTitulo
    /// </summary>
    private void LimpiarFiltroTitulo()
    {
        this.txtNroCuenta.Text = string.Empty;
        this.txtTituloValor.Text = string.Empty;
        this.dropEmisoraTitulo.SelectedValue = "-1";
    }

    /// <summary>
    /// Método void LimpiarFiltroMuebleInmueble
    /// </summary>
    private void LimpiarFiltroMuebleInmueble()
    {
        this.txtNumIdentificacion.Text = string.Empty;
        this.txtCedCatastrasl.Text = string.Empty;
        this.txtMatricula.Text = string.Empty;
    }

    /// <summary>
    /// Método void LimpiarFiltroDenuncuanteCausante
    /// </summary>
    private void LimpiarFiltroDenuncuanteCausante()
    {
        this.txtNroIdentificacion.Text = string.Empty;
        this.txtNroDefuncion.Text = string.Empty;
        this.txtRazonSocial.Text = string.Empty;
    }

    /// <summary>
    /// Método void LimpiarMsjTituloMueble
    /// </summary>
    private void LimpiarMsjTituloMueble()
    {
        this.msjMuebleInmueble.Visible = false;
        this.msjTituloValor.Visible = false;
    }

    /// <summary>
    /// Método void limpiarDenuncianteCusante
    /// </summary>
    private void limpiarDenuncianteCusante()
    {
        this.ViewState["SortedgvwDenunciante"] = null;
        this.pnlFiltroDenuncia.Visible = false;
        DataTable ds = new DataTable();
        ds = null;
        tbDenunciante.DataSource = ds;
        tbDenunciante.DataBind();
    }

    #endregion
}