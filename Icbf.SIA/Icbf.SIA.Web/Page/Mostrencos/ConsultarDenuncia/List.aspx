﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs"
    Inherits="Page_Mostrencos_ConsultarDenuncia_List" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:UpdatePanel runat="server" ID="up1" >
        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado denuncia *</td>
                        <td>Fecha de Radicado de la Denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en Correspondencia *</td>
                        <td>Fecha Radicado en Correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de Identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Primer Nombre *</td>
                        <td>Segundo Nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Primer Apellido *</td>
                        <td>Segundo Apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Descripción de la Denuncia *</td>
                        <td>Histórico de la Denuncia
                             <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                                 <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                             </a>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" TextMode="multiline" Columns="100" Rows="5" ID="txtDescripcion" Enabled="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" ScrollBars="None" Style="background-color: White; 
            border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 25%; left: 15%; width: 800px; background-color: white; 
            border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                  </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView ID="tbHistoria" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False" 
                                    AllowPaging="true" AllowSorting="true" GridLines="None" 
                                    OnSorting="GvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="GvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                        <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlExiste">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Existe Denuncia Anterior</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>¿Existe Denuncia Anterior?</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CheckBox runat="server" ID="ckSi" Text="Si" Enabled="false" />
                            <asp:CheckBox runat="server" ID="ckNo" Text="No" Enabled="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlConsultar">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Consultar</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Consultar por *
                             <asp:Label ID="lblRequeridoConsultaPor" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                        </td>

                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblConsultaPor" RepeatLayout="Table" RepeatColumns="3" Width="60%" OnSelectedIndexChanged="RblConsultaPor_SelectedIndexChanged" AutoPostBack="true">
                                <asp:listitem value="ckMueble" Text="Mueble o inmueble" />
                                <asp:listitem value="ckTitulo" Text="Título valor"/>
                                <asp:listitem value="ckDenunciante" Text="Denunciante o causante"/>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlMueble" Visible="false">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Tipo de Bien *
                        <asp:Label ID="lblRequeridTipoBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                        </td>
                        <td>Subtipo de Bien</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="dropTipoBien" AutoPostBack="true" OnSelectedIndexChanged="DropTipoBien_SelectedIndexChanged" Width="250px">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                                <asp:ListItem Value="3">INMUEBLE</asp:ListItem>
                                <asp:ListItem Value="2">MUEBLE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="dropSubTipoBien" AutoPostBack="true" OnSelectedIndexChanged="DropSubTipoBien_SelectedIndexChanged" Width="250px">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Clase de Bien</td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="dropClaseBien" Width="250px">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                    </tr>
                    <div runat="server" id="selectTipoBien">
                        <tr class="rowB">
                            <td>Número identificación del bien</td>
                            <td></td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtNumIdentificacion" Width="250px" MaxLength="20"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftTxtNumIdentificacion" runat="server"
                                    TargetControlID="txtNumIdentificacion" FilterType="numbers,Custom,LowercaseLetters,UppercaseLetters"
                                    ValidChars="áéíóúÁÉÍÓÚ " />
                            </td>
                            <td></td>
                        </tr>
                    </div>
                    <div runat="server" id="selecMueble">
                        <tr class="rowB">
                            <td>Marca del Bien</td>
                            <td></td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:DropDownList runat="server" ID="dropMarcaBien" Width="250px">
                                    <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                    </div>

                    <div runat="server" id="selecInmueble">
                        <tr class="rowB">
                            <td>Cédula catastral</td>
                            <td>Matricula inmobiliaria</td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtCedCatastrasl" Width="250px" MaxLength="20"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                    TargetControlID="txtCedCatastrasl" FilterType="numbers" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtMatricula" Width="250px" MaxLength="20"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                    TargetControlID="txtMatricula" FilterType="numbers,Custom,LowercaseLetters,UppercaseLetters"
                                    ValidChars="áéíóúÁÉÍÓÚ " />
                            </td>
                        </tr>
                    </div>

                    <asp:Panel runat="server" ID="pnlFiltroMuebleInmueble">
                       <table width="90%" align="center">
                           <tr>
                               <td>
                                    <div class="bg-warning text-center">
                                        <span class="alert-warning"><asp:Literal id="msjMuebleInmueble" runat="server" Visible="false" /></span>
                                    </div>
                                </td>
                           </tr>
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="tblMuebleInmueble" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                        GridLines="None" Width="100%" CellPadding="0" Height="16px" OnSorting="tblMuebleInmueble_Sorting"
                                        OnPageIndexChanging="tblMuebleInmueble_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnInfoDenuncia" CssClass="btnVerMueble" runat="server" ImageUrl="~/Image/btn/info.jpg" Visible="true"
                                                        Height="16px" Width="16px" OnClick="btnInfoDenuncia_Click" CommandArgument='<%#Eval("IDMUEBLEINMUEBLE")%>' AutoPostBack="true" ToolTip="Ver Detalle" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Radicado denuncia" DataField="radicadoDenuncia" SortExpression="radicadoDenuncia" />
                                            <asp:BoundField HeaderText="Nombre o razón social" DataField="NombRazonSocial" SortExpression="NombRazonSocial" />
                                            <asp:BoundField HeaderText="Fecha Radicado" DataField="fechaRadicado" SortExpression="fechaRadicado" />
                                            <asp:BoundField HeaderText="Tipo de Bien" DataField="TipoBien" SortExpression="TipoBien" />
                                            <asp:BoundField HeaderText="Matricula inmobiliaria" DataField="matriculaInmobiliaria" SortExpression="matriculaInmobiliaria" />
                                            <asp:BoundField HeaderText="Cédula catastral" DataField="cedulaCatastral" SortExpression="cedulaCatastral" />
                                            <asp:BoundField HeaderText="Número identificación del bien" DataField="nroIdentifica" SortExpression="nroIdentifica" />
                                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" SortExpression="Observaciones" />
                                        </Columns>
                                         <EmptyDataTemplate>
                                            No se encontraron datos, verifique por favor
                                        </EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="ModalMuebleInmueble" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 1%; 
                left: 15%; width: 800px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Detalle mueble o inmueble</span>
                            </div>
                            <div>
                                <asp:ImageButton ID="btnCerrarInmuebleModal" runat="server" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;" alt="h" OnClick="btnCerrarInmuebleModal_Click" ImageUrl="../../../Image/btn/close.png" />
                            </div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 800px; height: 300px; align-content: center">
                                <table width="80%" align="center">
                                    <tr class="rowA">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Tipo de bien *</td>
                                        <td>Subtipo de bien *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalTipoBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalSubTipoBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Clase de bien *</td>
                                        <td>Departamento *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalClaseBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalDepartamentoBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Municipio *</td>
                                        <td>Clase de entrada *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalMunicipioBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalClaseEntrada" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Estado físico del bien *</td>
                                        <td>Valor estimado comercial *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalEstadoFisico" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalValorEstimado" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Porcentaje de pertenencia del bien *</td>
                                        <td>Destinación económica *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalPorcentaje" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalDestinacion" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Matricula inmobiliaria *</td>
                                        <td>Cédula catastral *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalMatricula" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalCedula" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Dirección del inmueble *</td>
                                        <td>Número identificación del bien *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalDireccion" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalNuIdenti" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Marca del bien *</td>
                                        <td>Fecha de venta *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalMarca" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalFechaVentaBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Fecha Adjudicación *</td>
                                        <td>Fecha de Regisro *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalAdjudicacion" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalRegistro" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Estado *</td>
                                        <td>Descripción del bien *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalEstadoBien" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" TextMode="multiline" Columns="100" Rows="5" ID="txtModalDescripcion" 
                                                Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
             
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTitulo" Visible="false">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Tipo de Titulo *
                            <asp:Label ID="lblTipoTitulo" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                        </td>
                        <td>No. cuenta bancaria</td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="dropTipoTitulo" AutoPostBack="true"
                                OnSelectedIndexChanged="DropTipoTitulo_SelectedIndexChanged" Width="250px">
                                <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNroCuenta" Width="250px" MaxLength="32" Enabled="false"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNuemeroCuentaBancaria" runat="server" TargetControlID="txtNroCuenta"
                            FilterType="Numbers" />
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>No. título valor</td>
                        <td>Entidad Emisora del Título</td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTituloValor" MaxLength="16" Enabled="false" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroTitulo" runat="server" TargetControlID="txtTituloValor"
                        FilterType="Numbers" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="dropEmisoraTitulo" Width="250px">
                                <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="pnlFiltroTituloValor">
                        <table width="90%" align="center">
                            <tr>
                               <td>
                                    <div class="bg-warning text-center">
                                        <span class="alert-warning"><asp:Literal id="msjTituloValor" runat="server" Visible="false" /></span>
                                    </div>
                                </td>
                           </tr>
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="tbTituloValor" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                        GridLines="None" Width="100%" CellPadding="0" Height="16px" OnSorting="tbTituloValor_Sorting" 
                                        OnPageIndexChanging="tbTituloValor_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnInfo" CssClass="btnTitulo" runat="server" ImageUrl="~/Image/btn/info.jpg" Visible="true"
                                                        Height="16px" Width="16px" OnClick="BtnInfo_Click" CommandArgument='<%#Eval("IdTituloValor")%>' AutoPostBack="true" ToolTip="Ver Detalle" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Radicado denuncia" DataField="radicadoDenuncia" SortExpression="radicadoDenuncia" />
                                            <asp:BoundField HeaderText="Nombre o razón social" DataField="NombRazonSocial" SortExpression="NombRazonSocial" />
                                            <asp:BoundField HeaderText="Fecha Radicado" DataField="fechaRadicado" SortExpression="fechaRadicado" />
                                            <asp:BoundField HeaderText="Tipo Título" DataField="tipoTitulo" SortExpression="tipoTitulo" />
                                            <asp:BoundField HeaderText="Cuenta Bancaria" DataField="cuentaBancaria" SortExpression="cuentaBancaria" />
                                            <asp:BoundField HeaderText="Nro. Título Valor" DataField="nroTituloValor" SortExpression="nroTituloValor" />
                                            <asp:BoundField HeaderText="Observaciones" DataField="observaciones" SortExpression="observaciones" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron datos, verifique por favor
                                        </EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="ModalTitulo" Width="700px"  ScrollBars="None"
                Style="background-color: White; height: 400px; border-color: White; border-width: 2px; border-style: Solid; position: absolute; 
                z-index: 999999; top: 2%; left: 15%; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; 
                align-content: center; text-align: center; height: 100%">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Detalle T&iacute;tulo Valor</span>
                            </div>
                            <div>
                                <asp:ImageButton ID="ibCerrarModal" runat="server" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; 
                                    top: -10px; position: relative;" alt="h" OnClick="ibCerrarModal_Click" ImageUrl="../../../Image/btn/close.png" />
                           </div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; left: 5%; overflow-x: auto; width: 700px; height: 400px; align-content: center">
                                <table width="100%" align="center">
                                    <tr class="rowA">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Tipo de título *</td>
                                        <td>No. cuenta bancaria</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalTipo" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalNroCuenta" Enabled="false" Width="250px" MaxLength="32"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                TargetControlID="txtModalNroCuenta" FilterType="numbers" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Entidad bancaria</td>
                                        <td>Tipo de cuenta bancaria</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalEntidadBancaria" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalTipoCuentaBancaria" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Departamento</td>
                                        <td>Municipio</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalDepartamento" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalMunicipio" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Valor estimado en efectivo</td>
                                        <td>Número del título valor</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalValorEfectivo" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalNroTitulo" Enabled="false" Width="250px" MaxLength="35"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                TargetControlID="txtModalNroTitulo" FilterType="numbers" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>Entidad emisora</td>
                                        <td>Tipo identificación entidad emisora</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalEntidadEmisora" Enabled="false" Width="250px" MaxLength="64"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                TargetControlID="txtModalEntidadEmisora" FilterType="numbers,Custom,LowercaseLetters,UppercaseLetters"
                                                ValidChars="áéíóúÁÉÍÓÚ " />
                                        </td>
                                        <td>
                                             <asp:TextBox runat="server" ID="txtModalTipoIden" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Número identificación del bien</td>
                                        <td>Cantidad de títulos</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalIdenBien" Enabled="false" Width="250px" MaxLength="20"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                TargetControlID="txtModalIdenBien" FilterType="numbers" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalCanTítulos" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Valor unitario del título</td>
                                        <td>Fecha de vencimiento</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalValorUni" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalFechaVenc" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Fecha de recibido</td>
                                        <td>Fecha de venta</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalFechaRec" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalFechaVenta" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Cantidad vendida</td>
                                        <td>Clase de entrada</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalCantVendida" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                         <td>
                                            <asp:TextBox runat="server" ID="txtModalClaseEntra" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Fecha de Adjudicación</td>
                                        <td>Estado *</td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalFechaAdj" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtModalEstado" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>Descripción del título</td>
                                        <td></td>
                                    </tr>
                                    <tr class="rowA">
                                         <td>
                                            <asp:TextBox runat="server" TextMode="multiline" Columns="100" Rows="5" ID="txtModalDesTi" Enabled="false" 
                                                Width="250px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDenunciante" Visible="false">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Buscar por *
                            <asp:Label ID="lblBuscarCausanteDenun" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                             <asp:RadioButtonList runat="server" ID="rblBuscarPor" RepeatLayout="Table" RepeatColumns="2" Width="60%" OnSelectedIndexChanged="RblBuscarPor_SelectedIndexChanged" AutoPostBack="true">
                                <asp:listitem value="ckDenuncia" Text="Denunciante" Selected="True"/>
                                <asp:listitem value="ckCausante" Text="Causante"/>
                            </asp:RadioButtonList>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo identificación *
                            <asp:Label ID="lblRequeridoTipoIden" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                        </td>
                        <td>Número identificación *
                            <asp:Label ID="lblRequeridoNumiden" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="dropTipoIdentificacion" Width="250px">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNroIdentificacion" MaxLength="20" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Número de registro de defunción</td>
                        <td>Nombre o razón social</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNroDefuncion" Width="250px" MaxLength="16"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                TargetControlID="txtNroDefuncion" FilterType="Numbers" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtRazonSocial" Width="250px" MaxLength="120"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                TargetControlID="txtRazonSocial" FilterType="UppercaseLetters, LowercaseLetters, Custom"
                                ValidChars="áéíóú ÁÉÍÓÚ" />
                        </td>
                    </tr>
                    <asp:Panel runat="server" ID="pnlFiltroDenuncia">
                        <table width="90%" align="center">
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="tbDenunciante" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                        GridLines="None" Width="100%" CellPadding="0" Height="16px" OnSorting="tbDenunciante_Sorting" 
                                        OnPageIndexChanging="tbDenunciante_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField HeaderText="Radicado denuncia" DataField="radicadoDenuncia" SortExpression="radicadoDenuncia" />
                                            <asp:BoundField HeaderText="Tipo de Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                                            <asp:BoundField HeaderText="Nombre o razón social" DataField="RazonSocial" SortExpression="RazonSocial" />
                                            <asp:BoundField HeaderText="Fecha Radicado denuncia" DataField="fechaRadicadoDenuncia" SortExpression="fechaRadicadoDenuncia" />
                                            <asp:BoundField HeaderText="Número registro defunción" DataField="NroRegDefuncion" SortExpression="NroRegDefuncion" />
                                            <asp:BoundField HeaderText="Fecha defunción" DataField="fechaDefuncion" SortExpression="fechaDefuncion" DataFormatString="{0:dd/MM/yyyy}" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron datos, verifique por favor
                                        </EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </table>
            </asp:Panel> 

            <asp:Panel runat="server" ID="pnlInfRetorno" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; 
                z-index: 999999; top: 10%; left: 30%; width: 400px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                  
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 400px; height: 120px; align-content: center">
                                <table width="90%" align="center">
                                    <tr class="text-center">
                                        <td colspan="6">Seguro que Desea Regresar</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">¿Desea cerrar la acción Verificar Denuncia Anterior de la fase de análisis?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Button ID="BtnAceptar" class="center-block" OnClick="BtnAceptar_Click" runat="server" Visible="true" AutoPostBack="true" Text="Aceptar" />
                                        </td>
                                        <td colspan="3">
                                            <asp:Button ID="BtnCancelar" class="center-block" OnClick="BtnCancelar_Click" runat="server" Visible="true" AutoPostBack="true" Text="Cancelar" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PModalExiste" ScrollBars="None"
                Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; 
                z-index: 999999; top: 10%; left: 30%; width: 400px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                  
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 400px; height: 120px; align-content: center">
                                <table width="90%" align="center">
                                   <tr class="text-center">
                                        <td colspan="6">Seguro que Desea Regresar</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">¿Existe denuncia anterior?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Button ID="btnSi" class="center-block" OnClick="btnSi_Click" runat="server" Visible="true" AutoPostBack="true" Text="Si" />
                                        </td>
                                        <td colspan="3">
                                            <asp:Button ID="btnNo" class="center-block" OnClick="btnNo_Click" runat="server" Visible="true" AutoPostBack="true" Text="No" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                 
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.popuphIstorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });
        });
     
    </script>
</asp:Content>
