//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ReporteDenuncias_List.</summary>
// <author>INGENIAN SOFTWARE[Adrian Augusto Fernandez Anzola]</author>
// <date>19/11/2016 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Globalization;

/// <summary>
/// Clase parcial para gestionar la consulta de ReporteDenuncias.
/// </summary>
public partial class Page_ReporteDenuncias_List : GeneralWeb
{
    /// <summary>
    /// Instancia de masterPrincipal.
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Path de la p�gina.
    /// </summary>
    private string vPageName = "Mostrencos/ReporteDenuncias";

    /// <summary>
    /// Referencia a SigepcypService.
    /// </summary>
    private MostrencosService vMostrencosService = new MostrencosService();

    public List<int> vlDepartamentoUbicacion = new List<int>();

    public List<int> vlEstado = new List<int>();

    public int postvar = 0;

    #region METODOS

    /// <summary>
    /// M�todo para consultar informaci�n para el Reporte.
    /// </summary>
    private void Buscar()
    {
        try
        {
            string titulo = string.Empty;
            string mensaje = string.Empty;
            ReporteDenunciaFiltros vFiltros = new ReporteDenunciaFiltros();
            DateTime vRegistradodesde = Convert.ToDateTime("1/1/1753");
            DateTime vRegistradoHasta = Convert.ToDateTime("1/1/1753");
            DateTime vRegistrada = Convert.ToDateTime("1/1/1753");
            DateTime vBase = Convert.ToDateTime("1/01/1901");

            if (ddlIdTipoPersonaAsociacion.SelectedValue != "")
            {
                List<string> TipoPersona = new List<string>();
                foreach (ListItem item in ddlIdTipoPersonaAsociacion.Items)
                {
                    if (item.Selected)
                    {
                        TipoPersona.Add(item.Value);
                    }
                }
                vFiltros.TipoPersonaAsociacion = string.Join("|", TipoPersona);
            }

            if (ddlIdTipoIdentificacion.SelectedValue != "" && ddlIdTipoIdentificacion.SelectedValue != "SELECCIONE")
            {
                vFiltros.TipoIdentificacion = Convert.ToInt32(ddlIdTipoIdentificacion.SelectedValue);
            }
            else
            {
                vFiltros.TipoIdentificacion = null;
            }

            if (txtNumeroIdentificacion.Text != string.Empty)
            {
                vFiltros.NumeroIdentificacion = (txtNumeroIdentificacion.Text);
            }
            else
            {
                vFiltros.NumeroIdentificacion = null;
            }

            var session = GetSessionUser();

            if (this.ConsultarFuncionRol("ABOGADO") || this.ConsultarFuncionRol("COORDINADOR JURIDICO") || this.ConsultarFuncionRol("Asesor Juridica"))
            {
                vFiltros.DepartamentoUbicacion = session.IdRegional.ToString();
            }
            else if (this.ConsultarFuncionRol("ADMINISTRADOR") || this.ConsultarFuncionRol("Reporte CoordinadorNacional"))
            {
                if (ddlDepartamentoUbicacion.SelectedValue != null)
                {
                    List<string> vUbicacion = new List<string>();
                    foreach (ListItem item in ddlDepartamentoUbicacion.Items)
                    {
                        if (item.Selected)
                        {
                            vUbicacion.Add(item.Value);
                        }
                    }
                    vFiltros.DepartamentoUbicacion = string.Join("|", vUbicacion);
                }
            }
            else
            {
                return;
            }

            if (ddlEstado.SelectedValue != "")
            {
                List<string> vEstado = new List<string>();
                foreach (ListItem item in ddlEstado.Items)
                {
                    if (item.Selected)
                    {
                        vEstado.Add(item.Value);
                    }
                }
                vFiltros.Estado = string.Join("|", vEstado);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(cuFechaDesde.Date)) & Convert.ToString(cuFechaDesde.Date).ToLower() != "dd/mm/aaaa")
            {
                vRegistrada = cuFechaDesde.Date;

                int result = DateTime.Compare(vRegistrada, vBase);

                if (result < 0)
                {
                    this.toolBar.MostrarMensajeError("Fecha Invalida");
                    return;
                }
            }

            if (!string.IsNullOrEmpty(Convert.ToString(cuFechaHasta.Date)) & Convert.ToString(cuFechaDesde.Date).ToLower() != "dd/mm/aaaa")
            {
                vRegistrada = cuFechaHasta.Date;

                int result = DateTime.Compare(vRegistrada, vBase);

                if (result < 0)
                {
                    this.toolBar.MostrarMensajeError("Fecha Invalida");
                    return;
                }
            }

            if (!string.IsNullOrEmpty(Convert.ToString(cuFechaDesde.Date)) && Convert.ToString(cuFechaDesde.Date).ToLower() != "dd/mm/aaaa")
            {
                if (cuFechaDesde.Date > DateTime.Today)
                {
                    this.toolBar.MostrarMensajeError("La fecha seleccionada no es correcta. Por favor revisar.");
                    return;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(cuFechaHasta.Date)) && (cuFechaDesde.Date > cuFechaHasta.Date))
                {
                    this.toolBar.MostrarMensajeError("La fecha seleccionada no es correcta. Por favor revisar.");
                    return;
                }
                else
                {
                    vFiltros.RegistradoDesde = cuFechaDesde.Date;
                }

            }

            if (!string.IsNullOrEmpty(Convert.ToString(cuFechaHasta.Date)) && Convert.ToString(cuFechaHasta.Date).ToLower() != "dd/mm/aaaa")
            {
                if (cuFechaHasta.Date > DateTime.Today)
                {
                    this.toolBar.MostrarMensajeError("La fecha seleccionada no es correcta. Por favor revisar.");
                    return;
                }

                if (!string.IsNullOrEmpty(Convert.ToString(cuFechaDesde.Date)) && (cuFechaHasta.Date < cuFechaDesde.Date))
                {
                    this.toolBar.MostrarMensajeError("La fecha seleccionada no es correcta. Por favor revisar.");
                    return;
                }
                else
                {
                    vFiltros.RegistradoHasta = cuFechaHasta.Date;
                }

            }

            if (ddlFase.SelectedValue != "-1")
            {
                vFiltros.Fase = Convert.ToInt32(ddlFase.SelectedItem.Value);
            }

            if (chkVacante.Checked || chkMostrenco.Checked || chkVocacionHereditaria.Checked)
            {
                List<string> vListaClase = new List<string>();
                if (chkVacante.Checked)
                    vListaClase.Add(Convert.ToInt32(EnumeradoresDenuncia.EnumClaseDenuncia.Vacante).ToString());
                if (chkMostrenco.Checked)
                    vListaClase.Add(Convert.ToInt32(EnumeradoresDenuncia.EnumClaseDenuncia.Mostrenco).ToString());
                if (chkVocacionHereditaria.Checked)
                    vListaClase.Add(Convert.ToInt32(EnumeradoresDenuncia.EnumClaseDenuncia.VocacionHereditaria).ToString());
                vFiltros.ClaseDenuncia = string.Join(",", vListaClase);
            }

            if (!rbTodos.Checked)
            {
                if (rbReconoce.Checked)
                    vFiltros.DecisionReconociemiento = rbReconoce.Text;
                else if (rbNoReconoce.Checked)
                    vFiltros.DecisionReconociemiento = rbNoReconoce.Text;
            }

            if (!rdbModalidadPagoTodos.Checked)
            {
                if (rdbModalidadPagoParcial.Checked)
                    vFiltros.ModalidadPago = Convert.ToInt32(EnumeradoresDenuncia.EnumModalidadPago.Parcial);
                else if (rdbModalidadPagoTotal.Checked)
                    vFiltros.ModalidadPago = Convert.ToInt32(EnumeradoresDenuncia.EnumModalidadPago.Total);
            }

            vFiltros.TipoTramite = rdbNotarial.Checked ? 2 : rdbJudicial.Checked ? 1 : 0;

            titulo = "Reporte Denuncias";

            Report objReport = new Report("ReporteDenuncias", true, this.vPageName, titulo);

            int vCantidadRegistros = this.vMostrencosService.ConsultarReporteDenunciass(vFiltros);

            if (vCantidadRegistros > 0)
            {
                if (!string.IsNullOrEmpty(vFiltros.TipoPersonaAsociacion))
                {
                    objReport.AddParameter("TipoPersonaAsociacion", vFiltros.TipoPersonaAsociacion.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("TipoPersonaAsociacion", valores);
                }

                objReport.AddParameter("TipoIdentificacion", vFiltros.TipoIdentificacion.HasValue ? vFiltros.TipoIdentificacion.Value.ToString() : null);

                if (!string.IsNullOrEmpty(vFiltros.NumeroIdentificacion))
                {
                    objReport.AddParameter("NumeroIdentificacion", vFiltros.NumeroIdentificacion.ToString());
                }
                else
                {
                    int? vNumerodeIdentificacion = null;
                    objReport.AddParameter("NumeroIdentificacion", vNumerodeIdentificacion.HasValue ? vNumerodeIdentificacion.Value.ToString() : null);
                }


                if (!string.IsNullOrEmpty(vFiltros.DepartamentoUbicacion))
                {
                    objReport.AddParameter("DepartamentoUbicacion", vFiltros.DepartamentoUbicacion.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("DepartamentoUbicacion", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.Estado))
                {
                    objReport.AddParameter("Estado", vFiltros.Estado);
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("Estado", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.RegistradoDesde.ToString("dd/MM/yyyy")))
                {
                    objReport.AddParameter("RegistradoDesde", vFiltros.RegistradoDesde.ToString("dd/MM/yyyy"));
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("RegistradoDesde", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.RegistradoHasta.ToString("dd/MM/yyyy")))
                {
                    objReport.AddParameter("RegistradoHasta", vFiltros.RegistradoHasta.ToString("dd/MM/yyyy"));
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("RegistradoHasta", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.Fase.ToString()))
                {
                    objReport.AddParameter("Fase", vFiltros.Fase.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("Fase", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.ClaseDenuncia))
                {
                    objReport.AddParameter("ClaseDenuncia", vFiltros.ClaseDenuncia);
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("ClaseDenuncia", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.DecisionReconociemiento))
                {
                    objReport.AddParameter("DecisionReconociemiento", vFiltros.DecisionReconociemiento);
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("DecisionReconociemiento", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.TipoTramite.ToString()))
                {
                    objReport.AddParameter("TipoTramite", vFiltros.TipoTramite.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("TipoTramite", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.DecisionTramiteNot.ToString()))
                {
                    objReport.AddParameter("DecisionTramiteNot", vFiltros.DecisionTramiteNot.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("DecisionTramiteNot", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.ResultadoDesicionTramiteNot.ToString()))
                {
                    objReport.AddParameter("ResultadoDesicionTramiteNot", vFiltros.ResultadoDesicionTramiteNot.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("ResultadoDesicionTramiteNot", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.TerminacionAnormalTramiteNot.ToString()))
                {
                    objReport.AddParameter("TerminacionAnormalTramiteNot", vFiltros.TerminacionAnormalTramiteNot.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("TerminacionAnormalTramiteNot", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.DecisionAuto))
                {
                    objReport.AddParameter("DecisionAuto", vFiltros.DecisionAuto);
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("DecisionAuto", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.ResultadoDecisionAuto.ToString()))
                {
                    objReport.AddParameter("ResultadoDecisionAuto", vFiltros.ResultadoDecisionAuto.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("ResultadoDecisionAuto", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.SentidoSentencia))
                {
                    objReport.AddParameter("SentidoSentencia", vFiltros.SentidoSentencia);
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("SentidoSentencia", valores);
                }

                if (!string.IsNullOrEmpty(vFiltros.ModalidadPago.ToString()))
                {
                    objReport.AddParameter("ModalidadPago", vFiltros.ModalidadPago.ToString());
                }
                else
                {
                    string valores = null;
                    objReport.AddParameter("ModalidadPago", valores);
                }

                this.SetSessionParameter("Report", objReport);

                this.NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
            }
            else
            {
                throw new Exception("No se encontraron datos, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para carga los datos iniciales de la p�gina.
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);

            this.gvReporteDenuncias.PageSize = PageSize();
            this.gvReporteDenuncias.EmptyDataText = EmptyDataText();

            this.toolBar.EstablecerTitulos("Reporte Denuncias", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para obtener un registro de la grilla.
    /// </summary>
    /// <param name="pRow">Fila de la grilla</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvReporteDenuncias.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("ReporteDenuncias.IdReporteDenuncias", strValue);
            this.NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para obtener la sesi�n e inicializar controles.
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.RemoveSessionParameter("ReporteDenuncias.Guardado");

            string vRol = GetSessionUser().Rol.ToUpper();

            if (this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAbogado"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAdministrador"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolAsesorOficinaJuridica"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["RolCoordinadorJuridico"]) || this.ConsultarUsuarioProgramaFuncionRol(ConfigurationManager.AppSettings["Reporte CoordinadorNacional"]))
            {
                ddlIdTipoPersonaAsociacion.Focus();

                List<Icbf.Mostrencos.Entity.ReporteDenuncias> vListaPersonaAsociacion = this.vMostrencosService.ConsultarTipoPersonaAsociacion();
                foreach (ReporteDenuncias vTipoPersonaAsociacion in vListaPersonaAsociacion)

                { ddlIdTipoPersonaAsociacion.Items.Insert(0, new ListItem(vTipoPersonaAsociacion.NombreTipoPersonaAsociacion.ToUpper(), vTipoPersonaAsociacion.IdTipoPersonaAsociacion.ToString())); }

                List<Icbf.Mostrencos.Entity.ReporteDenuncias> vListaEstado = this.vMostrencosService.ConsultarEstadoDenuncia();
                foreach (ReporteDenuncias vEstado in vListaEstado)
                {
                    if (vEstado.NombreEstado != "")
                        ddlEstado.Items.Insert(0, new ListItem(vEstado.NombreEstado.ToUpper(), vEstado.Estado.ToString()));
                }

                List<Icbf.Mostrencos.Entity.ReporteDenuncias> vListaDptos = this.vMostrencosService.ConsultarDepartamentoUbicacion();
                foreach (ReporteDenuncias vDpto in vListaDptos)
                { ddlDepartamentoUbicacion.Items.Insert(0, new ListItem(vDpto.DepartamentoUbicacion.ToUpper(), vDpto.IdDepartamentoUbicacion.ToString())); }

                List<FasesDenuncia> vListaFaseDenuncias = this.vMostrencosService.ConsultarFasesDenuncia();
                foreach (FasesDenuncia vFase in vListaFaseDenuncias)
                { ddlFase.Items.Insert(0, new ListItem(vFase.NombreFase.ToUpper(), vFase.IdFase.ToString())); }
                { ddlFase.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
                { ddlFase.SelectedValue = "-1"; }
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para consultar usuario de acuerdo a programa/funci�n/rol
    /// </summary>
    /// <param name="pNombreFusion">Nombre a filtrar</param>
    /// <returns>EsFlag bollean</returns>private bool
    private bool ConsultarUsuarioProgramaFuncionRol(string pNombreFuncion)
    {
        bool EsFlag = false;
        List<Icbf.Seguridad.Entity.ProgramaFuncion> vListProgramaFuncion = new List<Icbf.Seguridad.Entity.ProgramaFuncion>();

        if (this.ViewState["PROGRAMAFUNCION"] == null)
        {
            vListProgramaFuncion = RolUtil.ListarRolesUsuario(GetSessionUser().NombreUsuario, "Mostrencos/ReporteDenuncias");
            this.ViewState["PROGRAMAFUNCION"] = vListProgramaFuncion;
        }
        else
        {
            vListProgramaFuncion = (List<Icbf.Seguridad.Entity.ProgramaFuncion>)this.ViewState["PROGRAMAFUNCION"];
        }
        Icbf.Seguridad.Entity.ProgramaFuncion vProgramaFuncion = new Icbf.Seguridad.Entity.ProgramaFuncion();

        if (!string.IsNullOrWhiteSpace(pNombreFuncion))
        {
            vProgramaFuncion = vListProgramaFuncion.Find(p => p.NombreFuncion.Trim().ToUpper() == pNombreFuncion.Trim().ToUpper());

        }
        if (vProgramaFuncion != null)
        {
            EsFlag = true;
        }

        return EsFlag;
    }

    /// <summary>
    /// Consulta la funcion asociada al rol y al programa
    /// </summary>
    /// <param name="pNombreFuncion"></param>
    /// <returns></returns>
    public bool ConsultarFuncionRol(string pNombreFuncion)
    {
        bool response = false;
        List<Icbf.Seguridad.Entity.ProgramaFuncion> vListProgramaFuncion = new List<Icbf.Seguridad.Entity.ProgramaFuncion>();
        vListProgramaFuncion = RolUtil.ListarRolesUsuario(GetSessionUser().NombreUsuario, this.vPageName);
        Icbf.Seguridad.Entity.ProgramaFuncion vProgramaFuncion = new Icbf.Seguridad.Entity.ProgramaFuncion();
        vProgramaFuncion = vListProgramaFuncion.Find(p => p.NombreFuncion.Trim().ToUpper() == pNombreFuncion.Trim().ToUpper());
        if (vProgramaFuncion != null)
        {
            if (vProgramaFuncion.NombreFuncion != "")
                response = true;
        }
        return response;
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.vPageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SomestartupScript",
                    "$(function () {" +
                        "$('[id*=ddlIdTipoPersonaAsociacion]').multiselect({ " +
                            "nonSelectedText: 'SELECCIONE'," +
                            "allSelectedText: 'SELECCIONADO TODOS'," +
                            "selectAllText: 'SELECCIONAR TODOS'," +
                            "nSelectedText:'SELECCIONADOS'," +
                            "includeSelectAllOption: true" +
                        "});" +
                    "});" +
                    "$(function() { " +
                        "$('[id*=ddlDepartamentoUbicacion]').multiselect({ " +
                         "nonSelectedText: 'SELECCIONE'," +
                            "allSelectedText: 'SELECCIONADO TODOS'," +
                            "selectAllText: 'SELECCIONAR TODOS'," +
                            "nSelectedText:'SELECCIONADOS'," +
                            "includeSelectAllOption: true" +
                        "});" +
                    "});" +
                    "$(function() {" +
                        "$('[id*=ddlEstado]').multiselect({" +
                         "nonSelectedText: 'SELECCIONE'," +
                            "allSelectedText: 'SELECCIONADO TODOS'," +
                            "selectAllText: 'SELECCIONAR TODOS'," +
                            "nSelectedText:'SELECCIONADOS'," +
                            "includeSelectAllOption: true" +
                        "});" +
                    "}); ", true);
            }
        }
    }

    /// <summary>
    /// Evento de seleccion del Tipo de Persona por Asociaci�n
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlIdTipoPersonaAsociacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            postvar++;
            int contNit = 0;
            int contCC = 0;
            ddlIdTipoIdentificacion.Items.Clear();
            { ddlIdTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
            { ddlIdTipoIdentificacion.SelectedValue = "-1"; }
            string vTiposDocumentos = ConfigurationManager.AppSettings["TiposDocumentoGlobal"];
            List<TiposDocumentosGlobal> vListaTipoIdentificacion = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(vTiposDocumentos);

            var Nit = vListaTipoIdentificacion.Find(x => x.IdTipoDocumento == 7);
            var Cc = vListaTipoIdentificacion.Find(x => x.IdTipoDocumento == 1);
            var Ce = vListaTipoIdentificacion.Find(x => x.IdTipoDocumento == 2);

            foreach (ListItem lista in ddlIdTipoPersonaAsociacion.Items)
            {

                var item = ddlIdTipoPersonaAsociacion.Items.FindByValue(ddlIdTipoPersonaAsociacion.SelectedValue);

                if (item != null)
                {
                    if (lista.Selected)
                    {
                        var ExisteCC = ddlIdTipoIdentificacion.Items.FindByValue(Cc.IdTipoDocumento.ToString());
                        var ExisteCe = ddlIdTipoIdentificacion.Items.FindByValue(Ce.IdTipoDocumento.ToString());
                        var ExisteNit = ddlIdTipoIdentificacion.Items.FindByValue(Nit.IdTipoDocumento.ToString());
                        var ExisteSeleccione = ddlIdTipoIdentificacion.Items.FindByValue("-1");

                        if (lista.Value == "1")
                        {
                            if (ExisteCC == null || ExisteCe == null)
                            {
                                ddlIdTipoIdentificacion.Items.Insert(0, new ListItem(Cc.CodDocumento.ToUpper(), Cc.IdTipoDocumento.ToString()));
                                ddlIdTipoIdentificacion.Items.Insert(0, new ListItem(Ce.CodDocumento.ToUpper(), Ce.IdTipoDocumento.ToString()));
                                contCC--;
                            }
                            else
                            {
                                contCC++;
                            }
                        }

                        else
                        {
                            if (ExisteNit == null)
                            {
                                ddlIdTipoIdentificacion.Items.Insert(0, new ListItem(Nit.CodDocumento.ToUpper(), Nit.IdTipoDocumento.ToString()));
                                contNit--;
                            }
                            else
                            {
                                contNit++;
                            }
                        }
                    }
                    else
                    {
                        if (lista.Value == "2" || lista.Value == "3" || lista.Value == "4")
                        {
                            var ExisteNit = ddlIdTipoIdentificacion.Items.FindByValue(Nit.IdTipoDocumento.ToString());

                            if (postvar == 2 && contNit == 0)
                            {
                                ddlIdTipoIdentificacion.Items.Remove(ddlIdTipoIdentificacion.Items.FindByText("NIT"));
                            }

                        }
                        if (lista.Value == "1")
                        {
                            var ExisteNit = ddlIdTipoIdentificacion.Items.FindByValue(Nit.IdTipoDocumento.ToString());

                            if (postvar == 2 && contCC == 0)
                            {
                                ddlIdTipoIdentificacion.Items.Remove(ddlIdTipoIdentificacion.Items.FindByText("CC"));
                                ddlIdTipoIdentificacion.Items.Remove(ddlIdTipoIdentificacion.Items.FindByText("CE"));
                            }

                        }
                    }
                }

            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para el manejo de cambio del control.
    /// </summary>
    /// <param name="sender">Grilla gvReporteDenuncias</param>
    /// <param name="e">El SelectedIndexChanged</param>
    protected void gvReporteDenuncias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvReporteDenuncias.SelectedRow);
    }

    /// <summary>
    /// M�todo para el manejo de la paginaci�n de al Grilla.
    /// </summary>
    /// <param name="sender">Grilla gvReporteDenuncias</param>
    /// <param name="e">El PageIndexChanging</param>
    protected void gvReporteDenuncias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReporteDenuncias.PageIndex = e.NewPageIndex;
        this.Buscar();
    }

    protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            postvar++;
            foreach (ListItem item in ddlEstado.Items)
            {
                if (item.Selected)
                {
                    var esiste = vlEstado.Find(x => x.Equals(item));
                    if (esiste == 0 && postvar == 2)
                    {
                        vlEstado.Add(Convert.ToInt32(item.Value));
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void ddlDepartamentoUbicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            postvar++;
            foreach (ListItem item in ddlDepartamentoUbicacion.Items)
            {
                if (item.Selected)
                {
                    var esiste = vlDepartamentoUbicacion.Find(x => x.Equals(item));
                    if (esiste == 0 && postvar == 2)
                    {
                        vlDepartamentoUbicacion.Add(Convert.ToInt32(item.Value));
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Evento para guardar el registro creado o editado.
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Evento para navegar al formulario de nuevo registro.
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.Add);
    }

    #endregion



    protected void rdbDesicionRechazado_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbDesicionRechazado.Checked)
        {
            rdbResultadoTodos.Enabled = true;
            rdbResultadoJudicial.Enabled = true;
            rdbResultadoTerminado.Enabled = true;
        }
        else
        {
            rdbResultadoTodos.Enabled = false;
            rdbResultadoTodos.Checked = true;
            rdbResultadoJudicial.Enabled = false;
            rdbResultadoJudicial.Checked = false;
            rdbResultadoTerminado.Enabled = false;
            rdbResultadoTerminado.Checked = false;
        }
    }

    protected void chkDesicionAutoInadmitida_CheckedChanged(object sender, EventArgs e)
    {
        if (chkDesicionAutoInadmitida.Checked)
        {
            rdbResultadoAutoTodos.Enabled = true;
            rdbResultadoAutoSubsanar.Enabled = true;
            rdbResultadoAutoNoSubsanar.Enabled = true;
        }
        else
        {
            rdbResultadoAutoTodos.Enabled = false;
            rdbResultadoAutoTodos.Checked = true;
            rdbResultadoAutoSubsanar.Enabled = false;
            rdbResultadoAutoSubsanar.Checked = false;
            rdbResultadoAutoNoSubsanar.Enabled = false;
            rdbResultadoAutoNoSubsanar.Checked = false;
        }
    }
}

