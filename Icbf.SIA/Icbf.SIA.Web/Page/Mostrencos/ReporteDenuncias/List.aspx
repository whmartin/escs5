<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ReporteDenuncias_List" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Tipo de persona o asociaci&oacute;n 
                        </td>
                        <td>Tipo de identificaci&oacute;n
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:ListBox ID="ddlIdTipoPersonaAsociacion" runat="server" Width="300px" SelectionMode="Multiple"
                                OnSelectedIndexChanged="ddlIdTipoPersonaAsociacion_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlIdTipoIdentificacion">
                                <asp:ListItem Text="SELECCIONE" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>N&uacute;mero identificaci&oacute;n
                        </td>
                        <td>Regional
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="256"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion" 
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                        <td>
                            <asp:ListBox ID="ddlDepartamentoUbicacion" runat="server" Width="300px" SelectionMode="Multiple"
                                OnSelectedIndexChanged="ddlDepartamentoUbicacion_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Estado *
                            <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="ddlEstado"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>Registrado desde *
                    <asp:RequiredFieldValidator runat="server" ID="rfvcuFechaDesde" ControlToValidate="cuFechaDesde$txtFecha"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:ListBox ID="ddlEstado" runat="server" Width="300px" SelectionMode="Multiple"
                                OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                        </td>
                        <td>
                            <asp:CompareValidator ID="cvFecha" runat="server" Type="Date" Operator="DataTypeCheck"
                                ControlToValidate="cuFechaDesde$txtFecha" ErrorMessage="Fecha inv&aacute;lida." SetFocusOnError="True"
                                ValidationGroup="btnBuscar" Display="Dynamic" ForeColor="Red">
                            </asp:CompareValidator>
                            <uc1:fecha ID="cuFechaDesde" runat="server" />
                        </td>
                    </tr>

                    <tr class="rowB">

                        <td>Registrado hasta *
                    <asp:RequiredFieldValidator runat="server" ID="rfvcuFechaHasta" ControlToValidate="cuFechaHasta$txtFecha"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>Clase de Denuncia
                     
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Date" Operator="DataTypeCheck"
                                ControlToValidate="cuFechaHasta$txtFecha" ErrorMessage="Fecha inv&aacute;lida." SetFocusOnError="True"
                                ValidationGroup="btnBuscar" Display="Dynamic" ForeColor="Red">
                            </asp:CompareValidator>
                            <uc1:fecha ID="cuFechaHasta" runat="server" />

                        </td>
                        <td>
                            <asp:CheckBox ID="chkVacante" runat="server" GroupName="grpClaseDenuncia" Text="Vacante" Enabled="true" />
                            <asp:CheckBox ID="chkMostrenco" runat="server" GroupName="grpClaseDenuncia" Text="Mostrenco" Enabled="true" />
                            <asp:CheckBox ID="chkVocacionHereditaria" runat="server" GroupName="grpClaseDenuncia" Text="Vocación hereditaria" Enabled="true" />
                        </td>
                    </tr>
                    <tr class="rowB">

                        <td>Fase
                    
                        </td>
                        <td>Decisión de reconocimiento de calidad de denunciante
                    
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="ddlFase"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbTodos" runat="server" GroupName="grpCalidadDenunciante" Text="Todos" Enabled="true" Checked="true" />
                            <asp:RadioButton ID="rbReconoce" runat="server" GroupName="grpCalidadDenunciante" Text="Reconoce" Enabled="true" />
                            <asp:RadioButton ID="rbNoReconoce" runat="server" GroupName="grpCalidadDenunciante" Text="No Reconoce" Enabled="true" />
                        </td>
                    </tr>

                    <tr class="rowB">

                        <td>Tipo de Trámite
                    
                        </td>
                        <td>Decisión del trámite notarial
                    
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButton ID="rdbTodos" runat="server" GroupName="grpTipoTramite" Text="Todos" Enabled="true" Checked="true" />
                            <asp:RadioButton ID="rdbNotarial" runat="server" GroupName="grpTipoTramite" Text="Notarial" Enabled="true" />
                            <asp:RadioButton ID="rdbJudicial" runat="server" GroupName="grpTipoTramite" Text="Judicial" Enabled="true" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rdbDesicionTodos" runat="server" GroupName="grpDecisionTramite" Text="Todos" Enabled="true" AutoPostBack="true" OnCheckedChanged="rdbDesicionRechazado_CheckedChanged" Checked="true" />
                            <asp:RadioButton ID="rdbDesicionAceptados" runat="server" GroupName="grpDecisionTramite" Text="Aceptado" Enabled="true" AutoPostBack="true" OnCheckedChanged="rdbDesicionRechazado_CheckedChanged" />
                            <asp:RadioButton ID="rdbDesicionRechazado" runat="server" GroupName="grpDecisionTramite" Text="Rechazado" Enabled="true" OnCheckedChanged="rdbDesicionRechazado_CheckedChanged" AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr class="rowB">

                        <td>Resultado de la Decisión del trámite notarial
                    
                        </td>
                        <td>Terminación Anormal del Trámite Notarial
                    
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButton ID="rdbResultadoTodos" runat="server" GroupName="grpResultadoDecision" Text="Todos" Enabled="false" Checked="true" />
                            <asp:RadioButton ID="rdbResultadoJudicial" runat="server" GroupName="grpResultadoDecision" Text="Judicial" Enabled="false" />
                            <asp:RadioButton ID="rdbResultadoTerminado" runat="server" GroupName="grpResultadoDecision" Text="Terminado" Enabled="false" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkTerminacionAnormal" runat="server" Text="Herederos de mayor derecho" Enabled="true" />
                        </td>
                    </tr>
                    <tr class="rowB">

                        <td>Decisión del Auto
                    
                        </td>
                        <td>Resultado de la Decisión del Auto
                    
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CheckBox ID="chkDesicionAutoAdmitida" runat="server" GroupName="grpDesicionAuto" Text="Admitida" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkDesicionAutoInadmitida_CheckedChanged" />
                            <asp:CheckBox ID="chkDesicionAutoInadmitida" runat="server" GroupName="grpDesicionAuto" Text="Inadmitida" Enabled="true" OnCheckedChanged="chkDesicionAutoInadmitida_CheckedChanged" AutoPostBack="true" />
                            <asp:CheckBox ID="chkDesicionAutoRechazada" runat="server" GroupName="grpDesicionAuto" Text="Rechazada" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkDesicionAutoInadmitida_CheckedChanged" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rdbResultadoAutoTodos" runat="server" GroupName="grpResultadoDecisionAuto" Text="Todos" Enabled="false" Checked="true" />
                            <asp:RadioButton ID="rdbResultadoAutoSubsanar" runat="server" GroupName="grpResultadoDecisionAuto" Text="Subsanar" Enabled="false" />
                            <asp:RadioButton ID="rdbResultadoAutoNoSubsanar" runat="server" GroupName="grpResultadoDecisionAuto" Text="No subsanar" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">

                        <td>Sentido de la Sentencia
                    
                        </td>
                        <td>Modalidad de Pago
                    
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CheckBox ID="chkSentidoSentenciaAdjudicada" runat="server" GroupName="grpSentidoSentencia" Text="Adjudicada" Enabled="true" />
                            <asp:CheckBox ID="chkSentidoSentenciaNoAdjudicada" runat="server" GroupName="grpSentidoSentencia" Text="No Adjudicada" Enabled="true" />
                            <asp:CheckBox ID="chkSentidoSentenciaParcialmente" runat="server" GroupName="grpSentidoSentencia" Text="Adjudicada parcialmente" Enabled="true" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rdbModalidadPagoTodos" runat="server" GroupName="grpModalidadPago" Text="Todos" Enabled="true" Checked="true" />
                            <asp:RadioButton ID="rdbModalidadPagoParcial" runat="server" GroupName="grpModalidadPago" Text="Parcial" Enabled="true" />
                            <asp:RadioButton ID="rdbModalidadPagoTotal" runat="server" GroupName="grpModalidadPago" Text="Total" Enabled="true" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlLista">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvReporteDenuncias" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdReporteDenuncias" CellPadding="0" Height="16px"
                                OnPageIndexChanging="gvReporteDenuncias_PageIndexChanging" OnSelectedIndexChanged="gvReporteDenuncias_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                Height="16px" Width="16px" ToolTip="Detalle" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Tipo de Persona o asociaci�n" DataField="IdTipoPersonaAsociacion" />
                                    <asp:BoundField HeaderText="Tipo de Identificaci�n" DataField="IdTipoIdentificacion" />
                                    <asp:BoundField HeaderText="N�mero Identificaci�n" DataField="NumeroIdentificacion" />
                                    <asp:BoundField HeaderText="Departamento Ubicacion" DataField="DepartamentoUbicacion" />
                                    <asp:BoundField HeaderText="Estado" DataField="Estado" />
                                    <asp:BoundField HeaderText="Registrado desde" DataField="FechaInicial" />
                                    <asp:BoundField HeaderText="Fecha Final" DataField="FechaFinal" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <script type="text/javascript">
                $(document).ready(function () {

                    $(function () {
                        $('[id*=ddlIdTipoPersonaAsociacion]').multiselect({
                            nonSelectedText: "SELECCIONE",
                            allSelectedText: "SELECCIONADO TODOS",
                            selectAllText: "SELECCIONAR TODOS",
                            includeSelectAllOption: true
                        });
                    });
                    $(function () {
                        $('[id*=ddlDepartamentoUbicacion]').multiselect({
                            nonSelectedText: "SELECCIONE",
                            allSelectedText: "SELECCIONADO TODOS",
                            nSelectedText: "SELECCIONADOS",
                            selectAllText: "SELECCIONAR TODOS",
                            includeSelectAllOption: true
                        });
                    });

                    $(function () {
                        $('[id*=ddlEstado]').multiselect({
                            nonSelectedText: "SELECCIONE",
                            allSelectedText: "SELECCIONADO TODOS",
                            nSelectedText: "SELECCIONADOS",
                            selectAllText: "SELECCIONAR TODOS",
                            includeSelectAllOption: true
                        });
                    });
                });

                function CheckLength() {
                    var textboxNumeroIdentificacion = document.getElementById("cphCont_txtNumeroIdentificacion").value;
                    if (textboxNumeroIdentificacion.trim().length >= 8) {
                        return false;
                    }
                    else {
                        return true;
                    }

                }

            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
