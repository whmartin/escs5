﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;


public partial class Page_Mostrencos_InformacionApoderado_Detail : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionApoderado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ValidateAccess(toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Edit.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        this.NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnNuevo_Click control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        bool EsExisteApoderadoActivo = false;
        this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        List<Apoderados> vListApoderados = new List<Apoderados>();
        vListApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(this.vIdDenunciaBien);
        foreach (Apoderados item in vListApoderados)
        {
            if (!string.IsNullOrEmpty(item.EstadoApoderado))
            {
                if (item.EstadoApoderado.Equals("ACTIVO"))
                {
                    EsExisteApoderadoActivo = true;
                    break;
                }
            }
        }
        if (!EsExisteApoderadoActivo)
        {
            this.NavigateTo(SolutionPage.Add);
        }
        else
        {
            this.toolBar.MostrarMensajeError("El estado de los apoderados debe ser Inactivo para poder crear un nuevo apoderado");
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnDescargar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        bool EsDescargar = false;
        string vRuta = string.Empty;
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
        if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
        {
            EsDescargar = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
            vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
        }
        else
        {
            EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }
        }
        if (EsDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();
        }
        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentos;
        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vListaDocumentos;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            vListaDocumentos = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentos;
            this.gvwDocumentacionRecibida.DataSource = vListaDocumentos;
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitudGrilla).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitudGrilla).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.EstablecerTitulos("Información del Apoderado", "Detail");
            this.gvwDocumentacionRecibida.PageSize = this.PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar datos de la denuncia
    /// </summary>
    /// <param name="pApoderado"></param>
    private void CargarDatosDenuncia(Apoderados pApoderado)
    {
        try
        {
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(pApoderado.IdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(pApoderado.IdDenunciaBien).IdEstadoDenuncia).NombreEstadoDenuncia;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA"))
            {
                this.toolBar.MostrarBotonNuevo(false);
                this.toolBar.MostrarBotonEditar(false);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);

            if (vDenuncia.IdEstadoDenuncia == 11 || vDenuncia.IdEstadoDenuncia == 13 || vDenuncia.IdEstadoDenuncia == 19)
            {
                this.toolBar.OcultarBotonNuevo(true);
                this.toolBar.MostrarBotonEditar(false);
            }

            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(vApoderado.IdApoderado);
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento != "ELIMINADO").OrderByDescending(p => p.IdDocumentosSolicitadosDenunciaBien).ToList();
            if (vApoderado.IdDenunciaBien != 0)
            {
                this.CargarListaDesplegable();
                this.CargarDatosDenuncia(vApoderado);
                this.CargarDatosApoderado(vApoderado);
                this.CargarGrillaDocumentos();
                LoadHistoricoDenuncia(vApoderado.IdDenunciaBien, gvwHistoricoDenuncia);
                this.VerCampos(this.rblExisteApoderadoSi.Checked);
            }
            string vMensaje = Convert.ToString(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.Mensaje"));
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// oculta  campos de la vista
    /// </summary>
    /// <param name="pVer">identifica si se ven o no</param>
    private void VerCampos(bool pVer)
    {
        this.lblDenuncianteApoderado.Visible = pVer;
        this.rblDenuncianteEsApoderadoSi.Visible = pVer;
        this.rblDenuncianteEsApoderadoNo.Visible = pVer;
        this.lblFechaInicioPoder.Visible = pVer;
        this.lblFechaFinPoder.Visible = pVer;
        this.FechaInicioPoder.Visible = pVer;
        this.FechaFinPoder.Visible = pVer;
        this.lblObservaciones.Visible = pVer;
        this.txtObservaciones.Visible = pVer;
        this.pnlInformacionApoderado.Visible = pVer;
        this.pnlDocumentacionSolicitada.Visible = pVer;
    }

    /// <summary>
    /// Carga la informacion del apoderado
    /// </summary>
    /// <param name="pApoderado">informacion del apoderado</param>
    private void CargarDatosApoderado(Apoderados pApoderado)
    {
        try
        {
            if (pApoderado.IdTercero != null)
            {
                pApoderado = this.vMostrencosService.ConsultarInfirmacionApoderados(pApoderado);
                this.txtNumeroIdentificacionApoderado.Text = pApoderado.Tercero.NumeroIdentificacion;
                this.txtTipoPersona.Text = pApoderado.Tercero.NombreTipoPersona;
                this.txtPrimerNombreApoderado.Text = pApoderado.Tercero.PrimerNombre;
                if (pApoderado.Tercero.NombreTipoPersona != "NATURAL")
                {
                    string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString()).FirstOrDefault().NomTipoDocumento;
                    this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString()));
                    this.txtPrimerNombreApoderado.Text = pApoderado.Tercero.RazonSocial;
                }
                this.ddlTipoIdentificacion.SelectedValue = pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString();
                this.txtSegundoNombreApoderado.Text = pApoderado.Tercero.SegundoNombre;
                this.txtPrimerApellidoApoderado.Text = pApoderado.Tercero.PrimerApellido;
                this.txtSegundoApellidoApoderado.Text = pApoderado.Tercero.SegundoApellido;
                this.txtDireccion.Text = pApoderado.Tercero.Direccion;
                this.txtDepartamento.Text = pApoderado.Tercero.NombreDepartamento;
                this.txtMunicipio.Text = pApoderado.Tercero.NombreMunicipio;
                this.txtEmail.Text = pApoderado.Tercero.CorreoElectronico;
                this.txtIndicativo.Text = pApoderado.Tercero.Indicativo;
                this.txttelefono.Text = pApoderado.Tercero.Telefono;
                this.txtCelular.Text = pApoderado.Tercero.Celular;
            }
            this.txtNumeroTarjetaProfesional.Text = !string.IsNullOrEmpty(pApoderado.NumeroTarjetaProfesional) ? pApoderado.NumeroTarjetaProfesional : string.Empty;
            if (!string.IsNullOrEmpty(pApoderado.EstadoApoderado))
            {
                this.rblEstadoApoderado.SelectedValue = pApoderado.EstadoApoderado;
            }
            if (pApoderado.ExisteApoderado.ToUpper().Equals("SI"))
            {
                this.rblExisteApoderadoSi.Checked = true;
            }
            else
            {
                this.rblExisteApoderadoNo.Checked = true;
            }
            if (!string.IsNullOrEmpty(pApoderado.DenuncianteEsApoderado))
            {

                if (pApoderado.DenuncianteEsApoderado.ToUpper().Equals("SI"))
                {
                    this.rblDenuncianteEsApoderadoSi.Checked = true;
                }
                else
                {
                    this.rblDenuncianteEsApoderadoNo.Checked = true;
                }
            }
            if (pApoderado.FechaFinPoder != null && pApoderado.FechaFinPoder != Convert.ToDateTime("1/01/1900"))
            {
                this.FechaFinPoder.Date = Convert.ToDateTime(pApoderado.FechaFinPoder).Date;
            }
            if (pApoderado.FechaInicioPoder != null && pApoderado.FechaInicioPoder != Convert.ToDateTime("1/01/1900"))
            {
                this.FechaInicioPoder.Date = Convert.ToDateTime(pApoderado.FechaInicioPoder).Date;
            }
            this.txtObservaciones.Text = !string.IsNullOrEmpty(pApoderado.Observaciones) ? pApoderado.Observaciones : string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar lista desplegable 
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar la grilla de documentacion
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    #endregion

    /// <summary>
    /// Grilla RowCommand
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Ver"))
            {
                this.btnDescargar.Visible = false;
                bool EsVerArchivo = false;
                string vRuta = string.Empty;
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
                DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdDocumentoSolicitado);
                if (vArchivo.IdApoderado == null || vArchivo.IdCausante == null)
                {
                    if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
                    {
                        EsVerArchivo = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
                        vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
                    }
                    else
                    {
                        EsVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;

                        if (!EsVerArchivo)
                        {
                            EsVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                            vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
                        }
                    }
                    if (EsVerArchivo)
                    {
                        this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                        this.pnlArchivo.CssClass = "popuphIstorico";
                        this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                        if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                        {
                            this.ifmVerAdchivo.Visible = true;
                            this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                            this.imgDodumento.Visible = false;
                            this.imgDodumento.ImageUrl = string.Empty;
                            this.btnDescargar.Visible = true;
                        }
                        else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                        {
                            this.ifmVerAdchivo.Visible = false;
                            this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                            this.imgDodumento.Visible = true;
                            this.imgDodumento.ImageUrl = vRuta;
                            this.btnDescargar.Visible = true;
                        }
                    }
                    if (!this.btnDescargar.Visible)
                    {
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                        this.pnlArchivo.CssClass = "popuphIstorico";
                        this.btnDescargar.Visible = false;
                        this.imgDodumento.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}