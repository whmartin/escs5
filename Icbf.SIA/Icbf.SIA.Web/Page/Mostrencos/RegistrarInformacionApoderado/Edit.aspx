﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Edit.aspx.cs" Inherits="Page_Mostrencos_InformacionApoderado_Edit" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .rbl td {
            padding-right: 20px !important;
        }

        .rbl input {
            min-width: 20px;
        }

        .ajax__calendar_day {
            text-decoration-line: none;
        }

        .ajax__calendar_invalid .ajax__calendar_day {
            background: #d5d5d5;
            cursor: not-allowed;
            text-decoration: none !important;
        }

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }
    </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia
                </td>
                <td>Fecha de radicado de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia               
                </td>
                <td>Fecha radicado en correspondencia               
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación
                </td>
                <td>Número de identificación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Primer nombre
                </td>
                <td>Segundo nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Primer apellido
                </td>
                <td>Segundo apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDescripcionDenincia">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">Descripción de la denuncia</td>
                <td style="width: 50%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" CssClass="Validar2" TextMode="MultiLine" MaxLength="512" Rows="8" Width="700px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel1">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del Poder</td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">¿Existe apoderado por parte del denunciante? *
                </td>
                <td>
                    <label runat="server" id="lblDenuncianteApoderado" visible="true">
                        ¿Denunciante es apoderado? *</label>
                </td>
            </tr>
            <tr class="rowA">
                <td style="text-align: center">
                    <asp:RadioButton ID="rblExisteApoderadoSi" AutoPostBack="true" runat="server" OnCheckedChanged="rblExisteApoderadoSi_CheckedChanged" Text="Si" />
                    <asp:RadioButton ID="rblExisteApoderadoNo" AutoPostBack="true" runat="server" OnCheckedChanged="rblExisteApoderadoNo_CheckedChanged" Text="No" />
                </td>
                <td style="text-align: center">
                    <asp:RadioButton ID="rblDenuncianteEsApoderadoSi" AutoPostBack="true" runat="server" OnCheckedChanged="rblDenuncianteEsApoderadoSi_CheckedChanged" Text="Si" />
                    <asp:RadioButton ID="rblDenuncianteEsApoderadoNo" AutoPostBack="true" runat="server" OnCheckedChanged="rblDenuncianteEsApoderadoNo_CheckedChanged" Text="No" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <label runat="server" id="lblFechaInicioPoder" visible="true">Fecha inicio de poder *</label>
                </td>
                <td>
                    <label runat="server" id="lblFechaFinPoder" visible="true">Fecha final de poder </label>
                </td>
            </tr>
            <tr class="rowA">
                <td>

                    <uc1:fecha runat="server" ID="FechaInicioPoder" Requerid="true" Visible="true" Width="250px" ValidationGroup="btnGuardar" />
                </td>
                <td>
                    <asp:Label runat="server" ID="reqFechaInvalida" CssClass="reqFechaInvalida hidden" Text="La fecha final del poder no puede ser menor a la fecha inicial del poder." ForeColor="Red"></asp:Label>
                    <br />
                    <uc1:fecha runat="server" ID="FechaFinPoder" Requerid="false" Visible="true" Width="250px" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <label runat="server" id="lblObservaciones" visible="true">Observaciones</label>
                </td>
                <td></td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" Visible="true" ID="txtObservaciones" CssClass="Validar2" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservaciones"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlInformacionApoderado" Visible="false">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del Apoderado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Tipo de persona             
                </td>
                <td>Tipo de identificación *
                </td>

            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoPersona" MaxLength="20" Text="NATURAL" Width="250px" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:CompareValidator ControlToValidate="ddlTipoIdentificacion" ID="reqTipoIdentificacion" ValidationGroup="BuscarApoderado"
                        SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" runat="server" Operator="NotEqual" ValueToCompare="-1" Type="Integer" />
                    <br />
                    <asp:DropDownList ID="ddlTipoIdentificacion" runat="server" Enabled="false" Width="250px"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Número de identificación *
                </td>
                <td>Primer nombre         
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RequiredFieldValidator runat="server" ID="reqNumeroIdentificacionApoderado" ControlToValidate="txtNumeroIdentificacionApoderado" ValidationGroup="BuscarApoderado" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionApoderado" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                    <asp:ImageButton ID="btnBuscar" OnClick="btnBuscar_Click" Enabled="false" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif" CausesValidation="true" ValidationGroup="BuscarApoderado" />
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacionApoderado" runat="server" TargetControlID="txtNumeroIdentificacionApoderado"
                        FilterType="Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombreApoderado" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Segundo nombre
                </td>
                <td>Primer apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombreApoderado" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellidoApoderado" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Segundo apellido
                </td>
                <td>Dirección
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellidoApoderado" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDireccion" MaxLength="150" Width="250px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Departamento
                </td>
                <td>Municipio
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDepartamento" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtMunicipio" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Correo eletrónico
                </td>
                <td>Indicativo
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtEmail" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIndicativo" Enabled="false" MaxLength="1" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Teléfono
                </td>
                <td>Celular
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txttelefono" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCelular" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">Número de tarjeta profesional *
                </td>
                <td>Estado del apoderado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RequiredFieldValidator ID="reqNumeroTarjetaProfesional" ControlToValidate="txtNumeroTarjetaProfesional"
                        ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox ID="txtNumeroTarjetaProfesional" runat="server" MaxLength="16" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNumeroTarjetaProfesional"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="" />

                </td>
                <td>
                    <asp:RequiredFieldValidator runat="server" ID="reqEstadoApoderado" ControlToValidate="rblEstadoApoderado" ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RadioButtonList ID="rblEstadoApoderado" Enabled="false" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="ACTIVO">Activo</asp:ListItem>
                        <asp:ListItem Value="INACTIVO">Inactivo</asp:ListItem>
                    </asp:RadioButtonList>
                </td>

            </tr>
        </table>
        <asp:HiddenField ID="hfIdTercero" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="tdTitulos" colspan="3">Documentación Solicitada
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="3"></td>
            </tr>
            <tr class="rowB">
                <td>Documento soporte de la denuncia *
                </td>
                <td colspan="2">Fecha de solicitud *
                </td>
            </tr>
            <tr class="rowA">
                <td style="width: 46%">
                    <asp:CompareValidator ControlToValidate="ddlDocumentoSoporte" ID="reqDocumentoSoporte" ValidationGroup="DocumentacionSolicitada"
                        SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido" runat="server" Operator="NotEqual" ValueToCompare="-1" Type="Integer" />
                    <br />
                    <asp:DropDownList ID="ddlDocumentoSoporte" runat="server" Width="250px"></asp:DropDownList></td>
                <td>
                    <uc1:fecha ID="FechaSolicitud" Requerid="true" ErrorMessage="Campo Requerido" runat="server" ValidationGroup="DocumentacionSolicitada" />
                </td>
                <td style="width: 150px; background-color: white; text-align: center">
                    <asp:ImageButton ID="btnAgregar" runat="server" ImageUrl="~/Image/btn/add.gif" OnClick="btnAgregar_Click" CausesValidation="true" ValidationGroup="DocumentacionSolicitada" />
                    <asp:ImageButton ID="btnAplicar" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicar_Click" CausesValidation="true" ValidationGroup="DocumentacionRequerida" Visible="false" />
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento solicitado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="3">
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionesSolicitado" ControlToValidate="txtObservacionesSolicitado" ValidationGroup="DocumentacionSolicitada" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox runat="server" ID="txtObservacionesSolicitado" CssClass="Validar2" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObserbacionesDocumentoSolicitado" runat="server" TargetControlID="txtObservacionesSolicitado"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentacionRecibida" Visible="false">
        <table width="90%" align="center">
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Estado del documento
                    <asp:Label ID="lblRequeridoEstadoDocumento" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
                <td>Fecha de recibido 
                    <asp:Label ID="lblRequeridoFechaRecibido" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RequiredFieldValidator runat="server" ID="reqEstadoDocumento" ControlToValidate="rbtEstadoDocumento" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal" Enabled="true" CssClass="rbl" OnSelectedIndexChanged="rbtEstadoDocumento_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="3">Aceptado</asp:ListItem>
                        <asp:ListItem Value="5">Devuelto</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaRecibido" Requerid="true" ErrorMessage="Campo Requerido" ValidationGroup="DocumentacionRequerida" Enabled="true" Date="<%# DateTime.Today %>" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Nombre de archivo 
                    <asp:Label ID="lblRequeridoNombreArchivo" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
                <td></td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RequiredFieldValidator runat="server" ID="reqArchivoRecibido" ControlToValidate="fulArchivoRecibido" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:FileUpload ID="fulArchivoRecibido" Enabled="true" runat="server" />
                </td>
                <td></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento recibido 
                    <asp:Label ID="lblCantidadCaracteresRecibido" runat="server" Text="*" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>

            <tr class="rowA">
                <td colspan="2">
                    <asp:RequiredFieldValidator runat="server" ID="reqObservacionesRecibido" ControlToValidate="txtObservacionesRecibido" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox runat="server" ID="txtObservacionesRecibido" Enabled="true" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtObservacionesRecibido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" AllowSorting="True"
                        OnSorting="gvwDocumentacionRecibida_Sorting"
                        GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px"
                        OnRowCommand="gvwDocumentacionRecibida_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumentoBienDenunciado.NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" />
                            <asp:BoundField HeaderText="Estado del documento" DataField="EstadoDocumento.NombreEstadoDocumento" SortExpression="NombreEstadoDocumento" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" DataFormatString="{0:d}" SortExpression="FechaRecibido" />
                            <asp:BoundField HeaderText="Nombre archivo" DataField="NombreArchivo" SortExpression="NombreArchivo" />
                                <%--<ItemTemplate>
                                    <asp:Button runat="server" CssClass="lnkArchivoDescargar" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" Enabled="true" CommandArgument="<%# Container.DataItemIndex%>" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Eliminar" OnClientClick="return confirm('¿Está seguro de eliminar la información?');" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument="<%# Container.DataItemIndex%>" ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Editar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlArchivo" CssClass="popuphIstorico hidden" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <div class="col-md-5 align-center">
                            <h4>
                                <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-md-5 align-center">
                            <asp:HiddenField ID="hfIdArchivo" runat="server" />
                            <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                        </div>
                        <div class="col-md-1 align-center">
                            <a class="btnCerrarPopArchivo" style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative;">
                                <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png"></img>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                        <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                        <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="PopUpHistorico hidden" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 15%; width: 70%; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                    </div>
                    <div>
                        <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                            <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                        </a>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; height: 200px; align-content: center">
                        <asp:GridView ID="gvwHistoricoDenuncia" runat="server" Visible="true" AutoGenerateColumns="False" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstadoDenuncia" ItemStyle-Width="15%" />
                                <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="15%" />
                                <asp:BoundField HeaderText="Responsable" DataField="Responsable" ItemStyle-Width="20%" />
                                <asp:BoundField HeaderText="Fase" DataField="Fase" ItemStyle-Width="20%" />
                                <asp:BoundField HeaderText="Actuacion" DataField="Actuacion" ItemStyle-Width="15%" />
                                <asp:BoundField HeaderText="Acción" DataField="Accion" ItemStyle-Width="15%" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.PopUpHistorico').removeClass('hidden');
            });
            $(document).on('click', '.btnCerrarPop', function () {
                $('.PopUpHistorico').addClass('hidden');
            });
            $(document).on('click', '.btnCerrarPopArchivo', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('.Validar2').attr('MaxLength', 512);

            $('#cphCont_FechaFinPoder_txtFecha').change(function () {

                var fechaFin = $('#cphCont_FechaFinPoder_txtFecha').val().split('/');
                var DateFin = new Date(fechaFin[2], fechaFin[1], fechaFin[0])

                var fechaInicio = $('#cphCont_FechaInicioPoder_txtFecha').val().split('/');
                var DateInicio = new Date(fechaInicio[2], fechaInicio[1], fechaInicio[0]);

                if (DateInicio) {
                    if (DateFin <= DateInicio) {
                        $('.reqFechaInvalida').removeClass('hidden');
                        $('#cphCont_FechaFinPoder_txtFecha').val("");
                        return false;
                    }
                    else {
                        $('.reqFechaInvalida').addClass('hidden');
                    }
                }
            });


            $(document).on('click', '#btnGuardar', function () {

                var fechaFin = $('#cphCont_FechaFinPoder_txtFecha').val().split('/');
                var DateFin = new Date(fechaFin[2], fechaFin[1], fechaFin[0])

                var fechaInicio = $('#cphCont_FechaInicioPoder_txtFecha').val().split('/');
                var DateInicio = new Date(fechaInicio[2], fechaInicio[1], fechaInicio[0]);

                if (DateInicio) {
                    if (DateFin <= DateInicio) {
                        $('.reqFechaInvalida').removeClass('hidden');
                        $('#cphCont_FechaFinPoder_txtFecha').val("");
                        return false;
                    }
                    else {
                        $('.reqFechaInvalida').addClass('hidden');
                    }
                }
            });
        });
    </script>
</asp:Content>