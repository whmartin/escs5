﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_InformacionApoderado_List" %>

<%@ Register Src="~/General/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">   

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia
                </td>
                <td>Fecha de radicado de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia               
                </td>
                <td>Fecha radicado en correspondencia               
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación
                </td>
                <td>Número de identificación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td >Primer nombre
                </td>
                <td >Segundo nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td >Primer apellido
                </td>
                <td >Segundo apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">Descripción de la denuncia</td>
                <td style="width: 50%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="width: 16px; height: 16px;">
                        <img alt="h" src="../../../Image/btn/info.jpg" style="width: 20px; height: 20px;"> </img>
                    </a>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="700px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlGridApoderados">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvApoderados" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                        CellPadding="0" DataKeyNames="IdApoderado" GridLines="None" Height="16px" OnPageIndexChanging="gvApoderados_OnPageIndexChanging"
                        OnSelectedIndexChanged="gvApoderados_SelectedIndexChanged" OnSorting="gvApoderados_OnSorting" PageSize="10" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" Height="16px" ImageUrl="~/Image/btn/info.jpg" ToolTip="Detalle" Width="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NombreMostrar" HeaderText="Nombre Apoderado" SortExpression="NombreMostrar" />
                            <asp:BoundField DataField="NumeroTarjetaProfesional" HeaderText="Número de tarjeta profesional" SortExpression="NumeroTarjetaProfesional" />
                            <asp:BoundField DataField="DenuncianteEsApoderado" HeaderText="Denunciante es Apoderado" SortExpression="DenuncianteEsApoderado" />
                            <asp:BoundField DataField="FechaInicioPoder" HeaderText="Fecha inicio de poder" SortExpression="FechaInicioPoder" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="FechaFinPoder" HeaderText="Fecha final de poder" SortExpression="FechaFinPoder" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" SortExpression="Observaciones" />
                            <asp:BoundField DataField="EstadoApoderado" HeaderText="Estado" SortExpression="EstadoApoderado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlPopUpHistorico" CssClass="popupHistorico hidden" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 15%; width: 70%; background-color: white; border: 1px solid #dfdfdf;"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width:100%;">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                    </div>
                    <div>
                        <a class="btnCerrarPop" style="width: 16px;height: 16px;text-decoration: none;float: right;margin-right: 10px;top: -10px;position: relative;">
                            <img style="width: 20px;height: 20px;" alt="h" src="../../../Image/btn/close.png">
                        </a>
                    </div>
                </td>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; height:250px; align-content: center">
                        <asp:GridView ID="gvwHistoricoDenuncia" runat="server" Visible="true" AutoGenerateColumns="False" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstadoDenuncia" />
                                <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Responsable" DataField="UsuarioCrea" />
                                <asp:BoundField HeaderText="Fase" DataField="Fase" />
                                <asp:BoundField HeaderText="Actuacion" DataField="Actuacion" />
                                <asp:BoundField HeaderText="Acción" DataField="Accion" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
