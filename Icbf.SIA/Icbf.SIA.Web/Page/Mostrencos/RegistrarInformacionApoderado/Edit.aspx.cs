﻿//-----------------------------------------------------------------------
// <copyright file="Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Edit.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_InformacionApoderado_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Entidad del Documento Solicitado
    /// </summary>
    private DocumentosSolicitadosDenunciaBien vDocumentoSolicitado;

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionApoderado";

    /// <summary>
    /// The v identifier denuncia bien/
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlArchivo.CssClass = "popuphIstorico hidden";
        this.pnlPopUpHistorico.CssClass = "PopUpHistorico hidden";
        this.FechaSolicitud.ValidationGroup = "DocumentacionSolicitada";
        this.FechaRecibido.ValidationGroup = "DocumentacionRequerida";
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Edit))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
            this.toolBar.LipiarMensajeError();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        List<Apoderados> vListApoderados = new List<Apoderados>();
        vListApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(this.vIdDenunciaBien);
        if (vListApoderados.Where(x => x.EstadoApoderado.Equals("ACTIVO")).ToList().Count() > 0)
        {
            this.toolBar.MostrarMensajeError("El estado de los apoderados debe ser Inactivo para poder crear un nuevo apoderado");
            return;
        }
        this.NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Handles the Click event of the btnDescargar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        bool EsDescargar = false;
        string vRuta = string.Empty;
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
        if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
        {
            EsDescargar = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
            vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
        }
        else
        {
            EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }
        }
        if (EsDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();
        }
        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAgregar_Click(object sender, ImageClickEventArgs e)
    {
        AddDocument();
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            CapturarValoresDocumentacion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderado_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            if (this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).ExisteApoderado.Equals("SI"))
            {
                this.toolBar.LipiarMensajeError();
                if (this.rblDenuncianteEsApoderadoSi.Checked)
                {
                    this.rblDenuncianteEsApoderadoSi.Checked = true;
                    this.rblDenuncianteEsApoderadoNo.Checked = false;
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                    this.Buscar(vDenunciaBien.IdTercero);
                    this.txtNumeroTarjetaProfesional.Text = string.Empty;
                    this.rblEstadoApoderado.SelectedValue = "ACTIVO";
                }
                else
                {
                    this.rblDenuncianteEsApoderadoSi.Checked = false;
                    this.rblDenuncianteEsApoderadoNo.Checked = true;
                    Apoderados vAporado = this.GetSessionParameter("Mostrencos.RegistrarApoderado.Apoderado") as Apoderados;
                    this.CargarDatosTercero(vAporado.Tercero);
                    this.txtNumeroTarjetaProfesional.Text = vAporado.NumeroTarjetaProfesional;
                }
                this.CambiarEstadoApoderado();
            }
            else
            {
                this.CargarListaDesplegable();
                this.toolBar.LipiarMensajeError();
                if (this.rblDenuncianteEsApoderadoSi.Checked)
                {
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                    this.Buscar(vDenunciaBien.IdTercero);
                    this.ddlTipoIdentificacion.Enabled = false;
                    this.txtNumeroIdentificacionApoderado.Enabled = false;
                    this.btnBuscar.Enabled = false;
                    this.rblEstadoApoderado.SelectedValue = "ACTIVO";
                }
                else
                {
                    this.LimpiarDatosTercero();
                    this.rblEstadoApoderado.ClearSelection();
                    this.btnBuscar.Enabled = true;
                    this.ddlTipoIdentificacion.Enabled = true;
                    this.txtNumeroIdentificacionApoderado.Enabled = true;
                }
                this.CambiarEstadoApoderado();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.Buscar(0);
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoNo.Checked = !this.rblDenuncianteEsApoderadoSi.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoSi.Checked = !this.rblDenuncianteEsApoderadoNo.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoNo.Checked = !this.rblExisteApoderadoSi.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoSi.Checked = !this.rblExisteApoderadoNo.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        this.Guardar();
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
            this.gvwDocumentacionRecibida.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
            if (e.CommandName.Equals("Eliminar"))
            {
                DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien()
                {
                    IdDocumentosSolicitadosDenunciaBien = vIdDocumentoSolicitado,
                    IdEstadoDocumento = 6,
                };

                vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(vdo);
                LoadDocument(vIdDenunciaBien, gvwDocumentacionRecibida);
            }
            if (e.CommandName.Equals("Editar"))
            {
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                this.ddlDocumentoSoporte.SelectedValue = Convert.ToString(gvr.Cells[1].Text);
                this.FechaSolicitud.Date = Convert.ToDateTime(gvr.Cells[4].Text);
                this.txtObservacionesSolicitado.Text = HttpContext.Current.Server.HtmlDecode
                    (Convert.ToString(gvr.Cells[5].Text));
                this.txtObservacionesSolicitado.Enabled = false;
                this.FechaSolicitud.Enabled = false;
                this.ddlDocumentoSoporte.Enabled = false;
                this.pnlDocumentacionRecibida.Visible = true;
                this.btnAplicar.Visible = true;
                this.btnAgregar.Visible = false;
                this.FechaRecibido.fechasMayoresA(Convert.ToDateTime(gvr.Cells[4].Text));
                this.FechaRecibido.fechasMenoresA(DateTime.Today);
                int IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(gvr.Cells[0].Text);
                SetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia", IdDocumentosSolicitadosDenunciaBien);
            }
            else if (e.CommandName.Equals("Ver"))
            {
                this.btnDescargar.Visible = false;
                bool ESVerArchivo = false;
                string vRuta = string.Empty;
                List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
                DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdDocumentoSolicitado);
                if (vArchivo.IdApoderado == null || vArchivo.IdCausante == null)
                {
                    if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
                    {
                        ESVerArchivo = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
                        vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
                    }
                    else
                    {
                        ESVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
                        if (!ESVerArchivo)
                        {
                            ESVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                            vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
                        }
                    }
                    if (ESVerArchivo)
                    {
                        this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                        this.pnlArchivo.CssClass = "popuphIstorico";
                        this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                        if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                        {
                            this.ifmVerAdchivo.Visible = true;
                            this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                            this.imgDodumento.Visible = false;
                            this.imgDodumento.ImageUrl = string.Empty;
                            this.btnDescargar.Visible = true;
                        }
                        else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                        {
                            this.ifmVerAdchivo.Visible = false;
                            this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                            this.imgDodumento.Visible = true;
                            this.imgDodumento.ImageUrl = vRuta;
                            this.btnDescargar.Visible = true;
                        }
                    }
                    if (!this.btnDescargar.Visible)
                    {
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                        this.pnlArchivo.CssClass = "popuphIstorico";
                        this.btnDescargar.Visible = false;
                        this.imgDodumento.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Metodo para mostrar y ocultar controles
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
        {
            lblRequeridoFechaRecibido.Visible = true;
            lblRequeridoNombreArchivo.Visible = true;
            lblCantidadCaracteresRecibido.Visible = true;
        }
        else
        {
            lblRequeridoFechaRecibido.Visible = false;
            lblRequeridoNombreArchivo.Visible = false;
            lblCantidadCaracteresRecibido.Visible = false;
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.EstablecerTitulos("Información del Apoderado", "Add");
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            if (vApoderado.IdDenunciaBien != 0)
            {
                this.CargarListaDesplegable();
                bool EsExisteApoderado = vApoderado.ExisteApoderado.Equals("SI");
                if (EsExisteApoderado)
                    this.CargarDatosApoderado(vApoderado);

                this.rblExisteApoderadoSi.Checked = true;
                this.rblExisteApoderadoNo.Checked = false;
                this.rblExisteApoderadoSi.Enabled = false;
                this.rblExisteApoderadoNo.Enabled = false;
                CargarGrillaDocumentos();
                LoadHistoricoDenuncia(vApoderado.IdDenunciaBien, gvwHistoricoDenuncia);
                this.FechaRecibido.Date = DateTime.Now;
                this.VerCampos(true);
            }
            this.rblDenuncianteEsApoderadoSi.Focus();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los datos del Apoderado
    /// </summary>
    /// <param name="pApoderado">Información del Apoderado</param>
    private void CargarDatosApoderado(Apoderados pApoderado)
    {
        try
        {
            this.SetSessionParameter("Mostrencos.RegistrarApoderado.Apoderado", pApoderado);
            if (pApoderado.IdTercero != null)
            {
                this.hfIdTercero.Value = pApoderado.IdTercero.ToString();
                pApoderado = this.vMostrencosService.ConsultarInfirmacionApoderados(pApoderado);
                this.txtNumeroIdentificacionApoderado.Text = pApoderado.Tercero.NumeroIdentificacion;
                this.txtTipoPersona.Text = pApoderado.Tercero.NombreTipoPersona;
                this.txtPrimerNombreApoderado.Text = pApoderado.Tercero.PrimerNombre;
                if (pApoderado.Tercero.NombreTipoPersona != "NATURAL")
                {
                    string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString()).FirstOrDefault().NomTipoDocumento;
                    this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString()));
                    this.txtPrimerNombreApoderado.Text = pApoderado.Tercero.RazonSocial;
                }
                this.ddlTipoIdentificacion.SelectedValue = pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString();
                this.txtSegundoNombreApoderado.Text = pApoderado.Tercero.SegundoNombre;
                this.txtPrimerApellidoApoderado.Text = pApoderado.Tercero.PrimerApellido;
                this.txtSegundoApellidoApoderado.Text = pApoderado.Tercero.SegundoApellido;
                this.txtDireccion.Text = pApoderado.Tercero.Direccion;
                this.txtDepartamento.Text = pApoderado.Tercero.NombreDepartamento;
                this.txtMunicipio.Text = pApoderado.Tercero.NombreMunicipio;
                this.txtEmail.Text = pApoderado.Tercero.CorreoElectronico;
                this.txtIndicativo.Text = pApoderado.Tercero.Indicativo;
                this.txttelefono.Text = pApoderado.Tercero.Telefono;
                this.txtCelular.Text = pApoderado.Tercero.Celular;
                this.CambiarEstadoApoderado();
            }

            this.txtNumeroTarjetaProfesional.Text = !string.IsNullOrEmpty(pApoderado.NumeroTarjetaProfesional) ? pApoderado.NumeroTarjetaProfesional : string.Empty;
            if (!string.IsNullOrEmpty(pApoderado.EstadoApoderado))
                this.rblEstadoApoderado.SelectedValue = pApoderado.EstadoApoderado;

            if (pApoderado.ExisteApoderado.ToUpper().Equals("SI"))
                this.rblExisteApoderadoSi.Checked = true;
            else
                this.rblExisteApoderadoNo.Checked = true;

            if (!string.IsNullOrEmpty(pApoderado.DenuncianteEsApoderado))
            {
                if (pApoderado.DenuncianteEsApoderado.ToUpper().Equals("SI"))
                    this.rblDenuncianteEsApoderadoSi.Checked = true;
                else
                    this.rblDenuncianteEsApoderadoNo.Checked = true;
            }
            if (pApoderado.FechaFinPoder != null && pApoderado.FechaFinPoder != Convert.ToDateTime("1/01/1900"))
                this.FechaFinPoder.Date = Convert.ToDateTime(pApoderado.FechaFinPoder).Date;

            if (pApoderado.FechaInicioPoder != null && pApoderado.FechaInicioPoder != Convert.ToDateTime("1/01/1900"))
                this.FechaInicioPoder.Date = Convert.ToDateTime(pApoderado.FechaInicioPoder).Date;

            this.txtObservaciones.Text = !string.IsNullOrEmpty(pApoderado.Observaciones) ? pApoderado.Observaciones : string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los datos de la denuncia
    /// </summary>
    /// <param name="pApoderado">Información del Apoderado</param>
    private void CargarDatosDenuncia(Apoderados pApoderado)
    {
        try
        {
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(pApoderado.IdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(pApoderado.IdDenunciaBien).IdEstadoDenuncia).NombreEstadoDenuncia;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;

            if (vRegistroDenuncia.NombreEstadoDenuncia.Equals("Archivado") || vRegistroDenuncia.NombreEstadoDenuncia.Equals("Anulado"))
                this.toolBar.MostrarBotonNuevo(false);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información del la lista desplegable
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();

            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(vIdApoderado);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }

            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();

            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    ///// <summary>
    ///// Metodo para cargarla grilla de documentos
    ///// </summary>
    //private void CargarGrillaDocumentos()
    //{
    //    try
    //    {
    //        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
    //        if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
    //        {
    //            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
    //            Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
    //            vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(vIdApoderado);
    //            vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
    //        }
    //        else
    //        {
    //            vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
    //        }
    //        vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
    //        if (vListaDocumentosSolicitados.Count > 0)
    //        {
    //            this.pnlGrid.Visible = true;
    //            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
    //            this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
    //            this.gvwDocumentacionRecibida.DataBind();
    //        }
    //        else
    //        {
    //            this.pnlGrid.Visible = false;
    //        }
    //    }
    //    catch (Exception)
    //    {

    //        throw;
    //    }
    //}

    /// <summary>
    /// Guarda la información del caudante
    /// </summary>
    private void Guardar()
    {
        try
        {
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            int vResultado;
            string vRuta = string.Empty;
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            Apoderados vApoderado = this.CapturarValoresApoderado();
            if (vApoderado != null)
            {
                this.InformacionAudioria(vApoderado, this.PageName, SolutionPage.Edit);
                vResultado = this.vMostrencosService.EditarApoderado(vApoderado);
                if (vResultado > 0)
                {
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];
                    if (vListaDocumentosEliminar != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosEliminar)
                        {
                            item.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("ELIMINADO").IdEstadoDocumento;
                            item.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }
                        this.ViewState["DocumentosEliminar"] = null;
                    }
                    if (vListaDocumentos != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                        {
                            vRuta = Server.MapPath("~/Page/Mostrencos/" + item.RutaArchivo);
                            if (!string.IsNullOrEmpty(item.NombreArchivo))
                            {
                                item.IdApoderado = vApoderado.IdApoderado;
                                item.IdDenunciaBien = vApoderado.IdDenunciaBien;
                                item.IdCausante = null;
                                this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                                this.vMostrencosService.InsertarDocumentoRecibidoApoderado(item);

                            }
                            else if (item.IdApoderado == null)
                            {
                                item.IdApoderado = vApoderado.IdApoderado;
                                item.IdDenunciaBien = vApoderado.IdDenunciaBien;
                                item.IdCausante = null;
                                this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                                vResultado = this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                                item.IdDocumentosSolicitadosDenunciaBien = vResultado;
                            }
                            if (!string.IsNullOrEmpty(item.RutaArchivo))
                            {
                                if (item.bytes != null)
                                {
                                    if (!System.IO.Directory.Exists(vRuta))
                                    {
                                        System.IO.Directory.CreateDirectory(vRuta);
                                    }
                                    System.IO.File.WriteAllBytes(vRuta + "\\" + item.NombreArchivo, item.bytes);
                                }
                            }
                            else
                            {
                                item.RutaArchivo = string.Empty;
                                item.NombreArchivo = string.Empty;
                            }
                            vResultado = this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }

                        this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.Mensaje", "La información ha sido guardada exitosamente");
                        this.NavigateTo(SolutionPage.Detail);
                    }
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la información del Apoderado
    /// </summary>
    /// <returns>variable de tipo Apoderados con la información capturada</returns>
    private Apoderados CapturarValoresApoderado()
    {
        Apoderados vApoderado = null;
        try
        {
            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            if (this.rblExisteApoderadoSi.Checked)
            {
                this.CambiarEstadoApoderado();
                vApoderado = new Apoderados()
                {
                    IdApoderado = vIdApoderado,
                    IdTercero = vApoderado.IdTercero,
                    FechaInicioPoder = this.FechaInicioPoder.Date,
                    NumeroTarjetaProfesional = this.txtNumeroTarjetaProfesional.Text.Trim(),
                    Observaciones = this.txtObservaciones.Text,
                    DenuncianteEsApoderado = this.rblDenuncianteEsApoderadoSi.Checked ? "SI" : "NO",
                    EstadoApoderado = this.rblEstadoApoderado.SelectedValue,
                    UsuarioCrea = this.GetSessionUser().NombreUsuario,
                    IdUsuarioCrea = this.GetSessionUser().IdUsuario,
                    IdDenunciaBien = vIdDenunciaBien,
                    ExisteApoderado = this.rblExisteApoderadoSi.Checked ? "SI" : "NO"
                };
                if (this.FechaFinPoder.Date != Convert.ToDateTime("1/01/1900"))
                    vApoderado.FechaFinPoder = FechaFinPoder.Date;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

        return vApoderado;
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }
                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Megas");
                }
                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = "RegistarInformacionApoderado/Archivos/" + pDocumentoSolicitado.IdApoderado;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }
            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// oculta  campos de la vista
    /// </summary>
    /// <param name="pVer">identifica si se ven o no</param>
    private void VerCampos(bool pVer)
    {
        this.pnlInformacionApoderado.Visible = pVer;
        this.pnlDocumentacionSolicitada.Visible = pVer;
        this.pnlDocumentacionRecibida.Visible = pVer;
        this.FechaFinPoder.Enabled = pVer;
        this.FechaInicioPoder.Enabled = pVer;
        this.txtObservaciones.Enabled = pVer;
        if (pVer)
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
        }
    }

    /// <summary>
    /// Evento SelectedIndexChanged del rblExisteApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderado_CheckedChanged(object sender, EventArgs e)
    {
        bool EsVisible = this.rblExisteApoderadoSi.Checked;
        this.rblExisteApoderadoSi.Checked = EsVisible;
        this.rblExisteApoderadoNo.Checked = !EsVisible;
        VerCampos(EsVisible);
    }

    /// <summary>
    /// busca un tercero
    /// </summary>
    /// <param name="pIdTercero">id del tercero que se busca</param>
    private void Buscar(int pIdTercero)
    {
        try
        {
            bool EsExistenCamposRequeridos = false;
            Tercero vTercero = new Tercero();
            if (pIdTercero == 0)
            {
                vTercero = this.vMostrencosService.ConsultarTercero(Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue), this.txtNumeroIdentificacionApoderado.Text, this.txtTipoPersona.Text);
            }
            else
            {
                vTercero = this.vMostrencosService.ConsultarTerceroXId(pIdTercero);
            }
            if (!EsExistenCamposRequeridos)
            {
                if (vTercero.IdTercero != 0)
                {
                    this.CargarDatosTercero(vTercero);
                    this.toolBar.LipiarMensajeError();
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// cambia el etado del apoderado
    /// </summary>
    public void CambiarEstadoApoderado()
    {
        try
        {
            if (this.FechaFinPoder.Date == Convert.ToDateTime("01/01/1900") || this.FechaFinPoder.Date > DateTime.Now.Date)
            {
                this.rblEstadoApoderado.SelectedValue = "ACTIVO";
            }
            else
            {
                this.rblEstadoApoderado.SelectedValue = "INACTIVO";
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Carga los datos del tercero
    /// </summary>
    /// <param name="pTercero">Variable de tipo tercero con los datos a cargar</param>
    private void CargarDatosTercero(Tercero pTercero)
    {
        try
        {
            this.hfIdTercero.Value = pTercero.IdTercero.ToString();
            this.txtTipoPersona.Text = pTercero.NombreTipoPersona;
            this.txtPrimerNombreApoderado.Text = pTercero.PrimerNombre;
            if (pTercero.NombreTipoPersona != "NATURAL")
            {
                string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pTercero.IdTipoDocIdentifica.ToString()).FirstOrDefault().NomTipoDocumento;
                this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pTercero.IdTipoDocIdentifica.ToString()));
                this.txtPrimerNombreApoderado.Text = pTercero.RazonSocial;
            }
            this.ddlTipoIdentificacion.SelectedValue = pTercero.IdTipoDocIdentifica.ToString();
            this.txtNumeroIdentificacionApoderado.Text = pTercero.NumeroIdentificacion;
            this.txtSegundoNombreApoderado.Text = pTercero.SegundoNombre;
            this.txtPrimerApellidoApoderado.Text = pTercero.PrimerApellido;
            this.txtSegundoApellidoApoderado.Text = pTercero.SegundoApellido;
            this.txtDireccion.Text = pTercero.Direccion;
            this.txtDepartamento.Text = pTercero.NombreDepartamento;
            this.txtMunicipio.Text = pTercero.NombreMunicipio;
            this.txtEmail.Text = pTercero.CorreoElectronico;
            this.txtIndicativo.Text = pTercero.Indicativo;
            this.txttelefono.Text = pTercero.Telefono;
            this.txtCelular.Text = pTercero.Celular;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Limpia los datos del tercero
    /// </summary>
    private void LimpiarDatosTercero()
    {
        try
        {
            this.hfIdTercero.Value = string.Empty;
            this.txtTipoPersona.Text = "NATURAL";
            this.ddlTipoIdentificacion.SelectedValue = "-1";
            this.txtNumeroIdentificacionApoderado.Text = string.Empty;
            this.txtPrimerNombreApoderado.Text = string.Empty;
            this.txtSegundoNombreApoderado.Text = string.Empty;
            this.txtPrimerApellidoApoderado.Text = string.Empty;
            this.txtSegundoApellidoApoderado.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtDepartamento.Text = string.Empty;
            this.txtMunicipio.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtIndicativo.Text = string.Empty;
            this.txttelefono.Text = string.Empty;
            this.txtCelular.Text = string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Valida las fechas fguardar
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void ValidarFechas(Apoderados pApoderado)
    {
        try
        {
            if (pApoderado.FechaFinPoder != null)
            {
                if (Convert.ToDateTime(pApoderado.FechaFinPoder).Date > Convert.ToDateTime(pApoderado.FechaInicioPoder).Date && Convert.ToDateTime(pApoderado.FechaFinPoder).Date > DateTime.Now.Date)
                {
                    pApoderado.EstadoApoderado = "ACTIVO";
                }
                else
                {
                    pApoderado.EstadoApoderado = "INACTIVO";
                }
            }
            else if (Convert.ToDateTime(pApoderado.FechaInicioPoder).Date < DateTime.Now.Date)
            {
                pApoderado.EstadoApoderado = "ACTIVO";
            }
            else
            {
                pApoderado.EstadoApoderado = "INACTIVO";
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// valida que las fechas no esten vacias
    /// </summary>
    private void ValidarCamposFecha()
    {
        if (this.FechaRecibido.Date == Convert.ToDateTime("01/01/1900"))
        {
            this.FechaRecibido.Date = DateTime.Now.Date;
        }
        if (this.FechaSolicitud.Date == Convert.ToDateTime("01/01/1900"))
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
        }
    }

    /// <summary>
    /// Cargar documentos
    /// </summary>
    private void AddDocument()
    {
        try
        {
            bool EsExiste = false;
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion == null)
                vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();

            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue))
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        EsExiste = true;
                        break;
                    }
                }
            }
            if (!EsExiste)
            {
                vDocumentoSolicitado.IdDenunciaBien = IdDenunciaBien;
                vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion == null || vListaDocumentacion.Count() == 0 ? 1 : vListaDocumentacion.Count() + 1;
                vDocumentoSolicitado.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlDocumentoSoporte.SelectedValue.ToString());
                vDocumentoSolicitado.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentoSolicitado.IdTipoDocumentoBienDenunciado);
                vDocumentoSolicitado.TipoDocumento = ddlDocumentoSoporte.SelectedItem.Text.ToString();
                vDocumentoSolicitado.NombreTipoDocumento = ddlDocumentoSoporte.SelectedItem.Text.ToString();
                vDocumentoSolicitado.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentoSolicitado.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentoSolicitado.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentoSolicitado.IdEstadoDocumento = vDocumentoSolicitado.EstadoDocumento.IdEstadoDocumento; //Estado del documento --> SOLICITADO
                vDocumentoSolicitado.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentoSolicitado.EsNuevo = true;
                vListaDocumentacion.Add(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                CargarGrillaDocumentos();
                ddlDocumentoSoporte.SelectedValue = "-1";
                txtObservacionesSolicitado.Text = string.Empty;
                FechaSolicitud.InitNull = true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para guardar documentos
    /// </summary>
    /// <param name="vIdDocumentoDenuncia"></param>
    /// <param name="vIdDenunciaBien"></param>
    private void SaveSolicitudDocumento(int vIdDocumentoDenuncia, int vIdDenunciaBien)
    {
        string fechaArchivo = DateTime.Now.ToString("ddMMyyyyHHmm");
        if (fulArchivoRecibido.HasFile)
        {
            if (this.fulArchivoRecibido.FileName.Length > 50)
            {
                throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
            }
            DocumentosSolicitadosDenunciaBien vDocumento = new DocumentosSolicitadosDenunciaBien();
            vDocumento.IdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            this.CargarArchivo(vDocumento);
            vDocumento.IdDocumentosSolicitadosDenunciaBien = vIdDocumentoDenuncia;
            vDocumento.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
            vDocumento.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            vDocumento.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
            this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", vDocumento);
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
            {
                vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                vDocumentacionSolicitada.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre(this.rbtEstadoDocumento.SelectedValue).IdEstadoDocumento;
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
            }
            else
            {
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
            }
            vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue);
            vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
            vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
            vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
            vListaDocumentacion.Add(vDocumentacionSolicitada);
            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
            this.CargarGrillaDocumentos();
        }
        this.txtObservacionesSolicitado.Enabled = true;
        this.FechaSolicitud.Enabled = true;
        this.ddlDocumentoSoporte.Enabled = true;
        this.txtObservacionesSolicitado.Text = string.Empty;
        this.FechaSolicitud.InitNull = true;
        this.ddlDocumentoSoporte.SelectedIndex = -1;
        this.pnlDocumentacionRecibida.Visible = false;
        this.btnAplicar.Visible = false;
        this.btnAgregar.Visible = true;
    }

    /// <summary>
    /// Metodo para capturar valores de documentacion
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            bool EsExiste = false;
            if (this.fulArchivoRecibido.HasFile)
            {
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }
                DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                this.CargarArchivo(vDocumentoCargado);
                this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", vDocumentoCargado);
            }
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion == null)
            {
                vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
            }
            if (vListaDocumentacion.Count() > 0)
            {
                if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue)).Count() > 0)
                {
                    this.toolBar.MostrarMensajeError("El Registro ya existe");
                    EsExiste = true;
                }
                else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue) && p.EsNuevo)).Count() > 0)
                {
                    this.toolBar.MostrarMensajeError("El Registro ya existe");
                    EsExiste = true;
                }
            }
            if (!EsExiste || IdDocumentosSolicitadosDenunciaBien > 0)
            {
                bool EsArchivoEsValido = false;
                if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                {
                    vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                    if (vDocumentacionSolicitada == null)
                        vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();

                    EsArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);
                    if (EsArchivoEsValido)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien == 0)
                        {
                            vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                        }
                        else
                        {
                            vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                        }

                        vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                        vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                        vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                        vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                        vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                        vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                        vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                        this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", null);
                    }
                }
                else
                {
                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                    vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                    vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                }
                if (EsArchivoEsValido || !EsExiste)
                {
                    vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue);
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                    vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                    vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
                    if (!vDocumentacionSolicitada.EsNuevo)
                    {
                        DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                        vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                        vListaDocumentacion.Remove(vDocumentacion);
                        vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                    }
                    vListaDocumentacion.Add(vDocumentacionSolicitada);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                    CargarGrillaDocumentos();
                    this.txtObservacionesSolicitado.Enabled = true;
                    this.FechaSolicitud.Enabled = true;
                    this.ddlDocumentoSoporte.Enabled = true;
                    this.txtObservacionesSolicitado.Text = string.Empty;
                    this.FechaSolicitud.InitNull = true;
                    this.ddlDocumentoSoporte.SelectedIndex = -1;
                    this.pnlDocumentacionRecibida.Visible = false;
                    this.btnAplicar.Visible = false;
                    this.btnAgregar.Visible = true;
                }
            };
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}