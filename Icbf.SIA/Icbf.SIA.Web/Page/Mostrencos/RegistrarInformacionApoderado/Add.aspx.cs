﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.IO;

public partial class Page_Mostrencos_InformacionApoderado_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Datos del tercero
    /// </summary>
    private Tercero vTercero;

    /// <summary>
    /// Entidad del Documento Solicitado
    /// </summary>
    private DocumentosSolicitadosDenunciaBien vDocumentoSolicitado;

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionApoderado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        this.CambiarEstadoApoderado();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.toolBar.LipiarMensajeError();
        this.pnlArchivo.CssClass = "popuphIstorico hidden";
        this.pnlPopUpHistorico.CssClass = "PopUpHistorico hidden";
        this.FechaSolicitud.ValidationGroup = "DocumentacionSolicitada";
        this.FechaRecibido.ValidationGroup = "DocumentacionRequerida";
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Edit))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
            this.toolBar.LipiarMensajeError();
        }
    }

    /// <summary>
    /// Metodo para abrie el popup
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnHistorico_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAgregar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            bool EsExiste = false;
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue))
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        EsExiste = true;
                        break;
                    }
                }
            }
            if (!EsExiste)
            {
                vDocumentoSolicitado.IdDenunciaBien = IdDenunciaBien;
                vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion == null || vListaDocumentacion.Count() == 0 ? 1 : vListaDocumentacion.Count() + 1;
                vDocumentoSolicitado.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlTipoDocumento.SelectedValue.ToString());
                vDocumentoSolicitado.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentoSolicitado.IdTipoDocumentoBienDenunciado);
                vDocumentoSolicitado.TipoDocumento = ddlTipoDocumento.SelectedItem.Text.ToString();
                vDocumentoSolicitado.NombreTipoDocumento = ddlTipoDocumento.SelectedItem.Text.ToString();
                vDocumentoSolicitado.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentoSolicitado.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentoSolicitado.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentoSolicitado.IdEstadoDocumento = vDocumentoSolicitado.EstadoDocumento.IdEstadoDocumento; //Estado del documento --> SOLICITADO
                vDocumentoSolicitado.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentoSolicitado.EsNuevo = true;
                vListaDocumentacion.Add(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrillaDocumentosSolicitados();
                ddlTipoDocumento.SelectedValue = "-1";
                txtObservacionesSolicitado.Text = string.Empty;
                FechaSolicitud.InitNull = true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAplicar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            CapturarValoresDocumentacion();
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnDescarga control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        bool EsDescargar = false;
        string vRuta = string.Empty;
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
        if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
        {
            EsDescargar = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
            vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
        }
        else
        {
            EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }
        }
        if (EsDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();
        }
        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.Buscar(0);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Guardar();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
            this.gvwDocumentacionRecibida.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla RowCommand
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
            if (e.CommandName == "Eliminar")
            {
                DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien()
                {
                    IdDocumentosSolicitadosDenunciaBien = vIdDocumentoSolicitado,
                    IdEstadoDocumento = 6,
                };
                vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(vdo);
                LoadDocument(vIdDenunciaBien, gvwDocumentacionRecibida);
            }
            else if (e.CommandName == "Editar")
            {
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                this.ddlTipoDocumento.SelectedValue = vDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
                this.FechaSolicitud.Date = Convert.ToDateTime(vDocumentoSolicitado.FechaSolicitud);
                this.txtObservacionesSolicitado.Text = vDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
                this.txtObservacionesSolicitado.Enabled = false;
                this.FechaSolicitud.Enabled = false;
                this.ddlTipoDocumento.Enabled = false;
                this.pnlDocumentacionRecibida.Visible = true;
                this.btnAplicar.Visible = true;
                this.btnAgregar.Visible = false;
                this.FechaRecibido.fechasMayoresA(Convert.ToDateTime(vDocumentoSolicitado.FechaSolicitud));
                this.FechaRecibido.fechasMenoresA(DateTime.Today);
                int IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien);
                SetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia", IdDocumentosSolicitadosDenunciaBien);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
            }
            else if (e.CommandName.Equals("Ver"))
            {
                this.btnDescargar.Visible = false;
                bool EsVerArchivo = false;
                string vRuta = string.Empty;
                List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
                DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdDocumentoSolicitado);
                if (vArchivo.IdApoderado == null || vArchivo.IdCausante == null)
                {
                    if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
                    {
                        EsVerArchivo = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
                        vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
                    }
                    else
                    {
                        EsVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
                        if (!EsVerArchivo)
                        {
                            EsVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                            vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
                        }
                    }
                    if (EsVerArchivo)
                    {
                        this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                        this.pnlArchivo.CssClass = "popuphIstorico";
                        this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                        if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                        {
                            this.ifmVerAdchivo.Visible = true;
                            this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                            this.imgDodumento.Visible = false;
                            this.imgDodumento.ImageUrl = string.Empty;
                            this.btnDescargar.Visible = true;
                        }
                        else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                        {
                            this.ifmVerAdchivo.Visible = false;
                            this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                            this.imgDodumento.Visible = true;
                            this.imgDodumento.ImageUrl = vRuta;
                            this.btnDescargar.Visible = true;
                        }
                    }
                    if (!this.btnDescargar.Visible)
                    {
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                        this.pnlArchivo.CssClass = "popuphIstorico";
                        this.btnDescargar.Visible = false;
                        this.imgDodumento.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }
        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }
        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }
        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Evento SelectedIndexChanged del rblExisteApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderado_CheckedChanged(object sender, EventArgs e)
    {
        this.ValidacionChecked();
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoNo.Checked = !this.rblDenuncianteEsApoderadoSi.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoSi.Checked = !this.rblDenuncianteEsApoderadoNo.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoNo.Checked = !this.rblExisteApoderadoSi.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoSi.Checked = !this.rblExisteApoderadoNo.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
        //this.pnlDocumentacionRecibida.Visible = true;
    }

    /// <summary>
    /// Evento SelectedIndexChanged del rblDenuncianteEsApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderado_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            if (this.rblDenuncianteEsApoderadoSi.Checked)
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                this.IdTercero.Value = vDenunciaBien.IdTercero.ToString();
                this.Buscar(vDenunciaBien.IdTercero);
                this.ddlTipoIdentificacion.Enabled = false;
                this.txtNumeroIdentificacionApoderado.Enabled = false;
                this.btnBuscar.Enabled = false;
                this.rblEstadoApoderado.SelectedValue = "ACTIVO";
            }
            else
            {
                this.LimpiarDatosTercero();
                this.rblEstadoApoderado.ClearSelection();
                this.btnBuscar.Enabled = true;
                this.ddlTipoIdentificacion.Enabled = true;
                this.txtNumeroIdentificacionApoderado.Enabled = true;
            }
            this.CambiarEstadoApoderado();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para mostrar y ocultar controles
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
        {
            lblRequeridoFechaRecibido.Visible = true;
            lblRequeridoNombreArchivo.Visible = true;
            lblCantidadCaracteresRecibido.Visible = true;
        }
        else
        {
            lblRequeridoFechaRecibido.Visible = false;
            lblRequeridoNombreArchivo.Visible = false;
            lblCantidadCaracteresRecibido.Visible = false;
        }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
                this.ViewState["directionState"] = SortDirection.Descending;

            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Información del Apoderado", "Add");
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
            this.gvwHistoricoDenuncia.PageSize = this.PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            FechaSolicitud.fechasMenoresA(DateTime.Today);
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            List<Apoderados> vListaApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(vIdDenunciaBien);
            if (vListaApoderados.Count() > 0)
            {
                if (vListaApoderados.Where(p => p.ExisteApoderado.Equals("SI")).Count() > 0)
                {
                    this.rblExisteApoderadoNo.Enabled = false;
                    this.rblExisteApoderadoSi.Enabled = false;
                    this.rblExisteApoderadoSi.Checked = true;
                    this.rblExisteApoderadoNo.Checked = false;
                }
            }
            this.ValidacionChecked();
            this.FechaSolicitud.Date = DateTime.Now;
            this.CargarListaDesplegable();
            CargarGrillaDocumentosSolicitados();
            LoadHistoricoDenuncia(vIdDenunciaBien, gvwHistoricoDenuncia);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlTipoDocumento.DataSource = vListaDocumento;
            this.ddlTipoDocumento.DataTextField = "NombreTipoDocumento";
            this.ddlTipoDocumento.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlTipoDocumento.DataBind();
            this.ddlTipoDocumento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoDocumento.SelectedValue = "-1";
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrillaDocumentosSolicitados()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGridDocumentacionRecibida.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    ///// <summary>
    ///// Metodo para cargar los documentos de la grilla
    ///// </summary>
    //private void CargarGrillaDocumentosSolicitados()
    //{
    //    try
    //    {
    //        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
    //        if (vListaDocumentosSolicitados.Count > 0)
    //        {
    //            this.pnlGridDocumentacionRecibida.Visible = true;
    //            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
    //            this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
    //            this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
    //            this.gvwDocumentacionRecibida.DataBind();
    //        }
    //        else
    //        {
    //            this.pnlGridDocumentacionRecibida.Visible = false;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        this.toolBar.MostrarMensajeError(ex.Message);
    //        throw ex;
    //    }
    //}

    /// <summary>
    /// Guarda la informacion del Apoderado
    /// </summary>
    private void Guardar()
    {
        try
        {
            string vRuta = string.Empty;
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentos.Count == 0 && this.rblExisteApoderadoSi.Checked)
            {
                this.toolBar.MostrarMensajeError("No se ha agregado ningún documento solicitado");
                return;
            }
            if (rblExisteApoderadoNo.Checked == true)
            {
                NavigateTo(SolutionPage.List);
                return;
            }
            Apoderados vApoderado = this.CapturarValoresApoderado();
            int vIdTercero = Convert.ToInt32(vApoderado.IdTercero);
            if (vApoderado != null)
            {
                if (this.vMostrencosService.ValidarDuplicidadApoderado(vIdTercero, IdDenunciaBien, vApoderado.IdApoderado))
                {
                    this.toolBar.MostrarMensajeError("El registro ya existe");
                    return;
                }
                this.InformacionAudioria(vApoderado, this.PageName, SolutionPage.Add);
                int vResultado = this.vMostrencosService.InsertarApoderado(vApoderado);
                if (vResultado > 0)
                {
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];
                    if (vListaDocumentosEliminar != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosEliminar)
                        {
                            item.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("ELIMINADO").IdEstadoDocumento;
                            item.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }

                        this.ViewState["DocumentosEliminar"] = null;
                    }
                    if (vListaDocumentos != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                        {
                            vRuta = Server.MapPath("~/Page/Mostrencos/" + item.RutaArchivo);
                            if (item.IdApoderado == 0 || item.IdApoderado == null)
                            {
                                item.IdApoderado = vResultado;
                                item.IdDenunciaBien = vApoderado.IdDenunciaBien;
                                item.IdCausante = null;
                                this.InformacionAudioria(item, this.PageName, SolutionPage.Add);
                                int vId = this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                            }
                            if (!string.IsNullOrEmpty(item.RutaArchivo))
                            {
                                if (item.bytes != null)
                                {
                                    if (!System.IO.Directory.Exists(vRuta))
                                    {
                                        System.IO.Directory.CreateDirectory(vRuta);
                                    }
                                    System.IO.File.WriteAllBytes(vRuta + "\\" + item.NombreArchivo, item.bytes);
                                }
                            }
                            else
                            {
                                item.RutaArchivo = string.Empty;
                                item.NombreArchivo = string.Empty;
                            }
                        }
                    }
                    this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado", vResultado);
                    this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.Mensaje", "La información ha sido guardada exitosamente");
                    this.NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la información del Apoderado
    /// </summary>
    /// <returns>variable de tipo Apoderados con la información capturada</returns>
    private Apoderados CapturarValoresApoderado()
    {
        Apoderados vApoderado = null;
        try
        {
            int vIdApoderado = int.Parse(IdTercero.Value.ToString());
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            if (this.rblExisteApoderadoSi.Checked)
            {
                this.CambiarEstadoApoderado();
                vApoderado = new Apoderados()
                {
                    IdTercero = int.Parse(IdTercero.Value.ToString()),
                    FechaInicioPoder = this.FechaInicioPoder.Date,
                    NumeroTarjetaProfesional = this.txtNumeroTarjetaProfesional.Text.Trim(),
                    Observaciones = this.txtObservaciones.Text,
                    DenuncianteEsApoderado = this.rblDenuncianteEsApoderadoSi.Checked ? "SI" : "NO",
                    EstadoApoderado = this.rblEstadoApoderado.SelectedValue,
                    UsuarioCrea = this.GetSessionUser().NombreUsuario,
                    IdUsuarioCrea = this.GetSessionUser().IdUsuario,
                    IdDenunciaBien = vIdDenunciaBien,
                    ExisteApoderado = this.rblExisteApoderadoSi.Checked ? "SI" : "NO"
                };
                if (this.FechaFinPoder.Date != Convert.ToDateTime("1/01/1900"))
                    vApoderado.FechaFinPoder = FechaFinPoder.Date;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
        return vApoderado;
    }

    /// <summary>
    /// Metodo para buscar
    /// </summary>
    /// <param name="pIdTercero"></param>
    private void Buscar(int pIdTercero)
    {
        try
        {
            bool EsExistenCamposRequeridos = false;
            vTercero = new Tercero();
            if (pIdTercero == 0)
            {
                if (!EsExistenCamposRequeridos)
                {
                    vTercero = this.vMostrencosService.ConsultarTercero(Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue), this.txtNumeroIdentificacionApoderado.Text, this.txtTipoPersona.Text);
                }
            }
            else
            {
                vTercero = this.vMostrencosService.ConsultarTerceroXId(pIdTercero);
            }
            if (!EsExistenCamposRequeridos)
            {
                if (vTercero.IdTercero != 0)
                {
                    this.CargarDatosTercero(vTercero);
                    this.toolBar.LipiarMensajeError();
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Limpia los datos del tercero
    /// </summary>
    private void LimpiarDatosTercero()
    {
        try
        {
            this.CargarListaDesplegable();
            this.txtTipoPersona.Text = "NATURAL";
            this.ddlTipoIdentificacion.SelectedValue = "-1";
            this.txtNumeroIdentificacionApoderado.Text = string.Empty;
            this.txtPrimerNombreApoderado.Text = string.Empty;
            this.txtSegundoNombreApoderado.Text = string.Empty;
            this.txtPrimerApellidoApoderado.Text = string.Empty;
            this.txtSegundoApellidoApoderado.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtDepartamento.Text = string.Empty;
            this.txtMunicipio.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtIndicativo.Text = string.Empty;
            this.txttelefono.Text = string.Empty;
            this.txtCelular.Text = string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// cambia el etado del apoderado
    /// </summary>
    public void CambiarEstadoApoderado()
    {
        try
        {
            if (this.FechaFinPoder.Date == Convert.ToDateTime("01/01/1900") || this.FechaFinPoder.Date > DateTime.Now.Date)
            {
                this.rblEstadoApoderado.SelectedValue = "ACTIVO";
            }
            else
            {
                this.rblEstadoApoderado.SelectedValue = "INACTIVO";
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Carga los datos del tercero
    /// </summary>
    /// <param name="pTercero">Variable de tipo tercero con los datos a cargar</param>
    private void CargarDatosTercero(Tercero pTercero)
    {
        try
        {
            this.txtTipoPersona.Text = pTercero.NombreTipoPersona;
            this.txtPrimerNombreApoderado.Text = pTercero.PrimerNombre;
            this.IdTercero.Value = pTercero.IdTercero.ToString();
            if (pTercero.NombreTipoPersona != "NATURAL")
            {
                string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pTercero.IdTipoDocIdentifica.ToString()).FirstOrDefault().NomTipoDocumento;
                this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pTercero.IdTipoDocIdentifica.ToString()));
                this.txtPrimerNombreApoderado.Text = pTercero.RazonSocial;
            }
            this.ddlTipoIdentificacion.SelectedValue = pTercero.IdTipoDocIdentifica.ToString();
            this.txtNumeroIdentificacionApoderado.Text = pTercero.NumeroIdentificacion;
            this.txtSegundoNombreApoderado.Text = pTercero.SegundoNombre;
            this.txtPrimerApellidoApoderado.Text = pTercero.PrimerApellido;
            this.txtSegundoApellidoApoderado.Text = pTercero.SegundoApellido;
            this.txtDireccion.Text = pTercero.Direccion;
            this.txtDepartamento.Text = pTercero.NombreDepartamento;
            this.txtMunicipio.Text = pTercero.NombreMunicipio;
            this.txtEmail.Text = pTercero.CorreoElectronico;
            this.txtIndicativo.Text = pTercero.Indicativo;
            this.txttelefono.Text = pTercero.Telefono;
            this.txtCelular.Text = pTercero.Celular;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// valida que se debe hacer cuando se selecciona un valor del campo existe apoderado
    /// </summary>
    public void ValidacionChecked()
    {
        bool EsVisible = this.rblExisteApoderadoSi.Checked;
        this.pnlInformacionApoderado.Visible = EsVisible;
        this.pnlDocumentacionSolicitada.Visible = EsVisible;
        this.pnlDocumentacionRecibida.Visible = EsVisible;
        this.pnlApoderado.Visible = EsVisible;
        if (EsVisible)
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
            this.rblExisteApoderadoSi.Checked = true;
            this.rblExisteApoderadoNo.Checked = false;
        }
        else
        {
            this.rblExisteApoderadoSi.Checked = false;
            this.rblExisteApoderadoNo.Checked = true;
        }
        lblDenuncianteApoderado.Visible = EsVisible;
        rblDenuncianteEsApoderadoSi.Visible = EsVisible;
        rblDenuncianteEsApoderadoNo.Visible = EsVisible;
    }

    /// <summary>
    /// Metodopara cargar archivos
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    /// <returns></returns>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }
                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Mb");
                }
                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = "RegistarInformacionApoderado/Archivos/" + pDocumentoSolicitado.IdCausante;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }
            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Metodo para capturar valores de documentacion
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            bool EsExiste = false;
            if (this.fulArchivoRecibido.HasFile)
            {
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }
                DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                this.CargarArchivo(vDocumentoCargado);
                this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", vDocumentoCargado);
            }
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion == null)
            {
                vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
            }
            if (vListaDocumentacion.Count() > 0)
            {
                if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)).Count() > 0)
                {
                    this.toolBar.MostrarMensajeError("El Registro ya existe");
                    EsExiste = true;
                }
                else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.EsNuevo)).Count() > 0)
                {
                    this.toolBar.MostrarMensajeError("El Registro ya existe");
                    EsExiste = true;
                }
            }
            if (!EsExiste || IdDocumentosSolicitadosDenunciaBien > 0)
            {
                bool EsArchivoEsValido = false;
                if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                {
                    vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                    if (vDocumentacionSolicitada == null)
                        vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();

                    EsArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);
                    if (EsArchivoEsValido)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien == 0)
                        {
                            vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                        }
                        else
                        {
                            vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                        }

                        vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                        vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                        vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                        vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                        vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                        vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                        vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                        this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", null);
                    }
                }
                else
                {
                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                    vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                    vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                }
                if (EsArchivoEsValido || !EsExiste)
                {
                    vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                    vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                    vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
                    if (!vDocumentacionSolicitada.EsNuevo)
                    {
                        DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                        vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                        vListaDocumentacion.Remove(vDocumentacion);
                        vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                    }
                    vListaDocumentacion.Add(vDocumentacionSolicitada);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                    CargarGrillaDocumentosSolicitados();
                    this.txtObservacionesSolicitado.Enabled = true;
                    this.FechaSolicitud.Enabled = true;
                    this.ddlTipoDocumento.Enabled = true;
                    this.txtObservacionesSolicitado.Text = string.Empty;
                    this.FechaSolicitud.InitNull = true;
                    this.ddlTipoDocumento.SelectedIndex = -1;
                    this.pnlDocumentacionRecibida.Visible = false;
                    this.btnAplicar.Visible = false;
                    this.btnAgregar.Visible = true;
                }
            };
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    //private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    //{
    //    try
    //    {
    //        if (this.fulArchivoRecibido.HasFile)
    //        {
    //            string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
    //            if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
    //            {
    //                throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
    //            }

    //            int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
    //            if (vtamArchivo > 4096)
    //            {
    //                throw new Exception("El tamaño máximo del archivo es 4 Mb");
    //            }

    //            pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
    //            pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
    //            pDocumentoSolicitado.RutaArchivo = "RegistarInformacionApoderado/Archivos/" + pDocumentoSolicitado.IdCausante;
    //            return true;
    //        }
    //        else
    //        {
    //            DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado") as DocumentosSolicitadosDenunciaBien;
    //            if (vDocumento != null)
    //            {

    //                pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
    //                pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
    //                pDocumentoSolicitado.bytes = vDocumento.bytes;
    //                return true;
    //            }
    //        }

    //        return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
    //    }
    //    catch (Exception ex)
    //    {
    //        this.toolBar.MostrarMensajeError(ex.Message);
    //        throw;
    //    }
    //}

    //private void CapturarValoresDocumentacion()
    //{
    //    try
    //    {
    //        int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
    //        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));

    //        bool vExiste = false;
    //        if (this.fulArchivoRecibido.HasFile)
    //        {
    //            if (this.fulArchivoRecibido.FileName.Length > 50)
    //            {
    //                throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
    //            }
    //            DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
    //            this.CargarArchivo(vDocumentoCargado);
    //            this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", vDocumentoCargado);
    //        }

    //        DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
    //        List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];

    //        if (vListaDocumentacion == null)
    //        {
    //            vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
    //        }
    //        if (vListaDocumentacion.Count() > 0)
    //        {
    //            if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)).Count() > 0)
    //            {
    //                this.toolBar.MostrarMensajeError("El Registro ya existe");
    //                vExiste = true;
    //            }
    //            else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.EsNuevo)).Count() > 0)
    //            {
    //                this.toolBar.MostrarMensajeError("El Registro ya existe");
    //                vExiste = true;
    //            }
    //        }

    //        if (!vExiste || IdDocumentosSolicitadosDenunciaBien > 0)
    //        {
    //            bool vArchivoEsValido = false;
    //            if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
    //            {
    //                vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
    //                if (vDocumentacionSolicitada == null)
    //                    vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();

    //                vArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);

    //                if (vArchivoEsValido)
    //                {
    //                    if (IdDocumentosSolicitadosDenunciaBien == 0)
    //                    {
    //                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
    //                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
    //                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
    //                    }
    //                    else
    //                    {
    //                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
    //                    }

    //                    vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
    //                    vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
    //                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
    //                    vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
    //                    vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
    //                    vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
    //                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
    //                    this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", null);
    //                }
    //            }
    //            else
    //            {
    //                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
    //                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
    //                vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
    //                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
    //                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
    //                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
    //                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
    //            }

    //            if (vArchivoEsValido || !vExiste)
    //            {
    //                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
    //                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
    //                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
    //                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
    //                if (!vDocumentacionSolicitada.EsNuevo)
    //                {
    //                    DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
    //                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
    //                    vListaDocumentacion.Remove(vDocumentacion);
    //                    vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
    //                }
    //                vListaDocumentacion.Add(vDocumentacionSolicitada);
    //                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
    //                CargarGrillaDocumentosSolicitados();
    //                this.txtObservacionesSolicitado.Enabled = true;
    //                this.FechaSolicitud.Enabled = true;
    //                this.ddlTipoDocumento.Enabled = true;

    //                this.txtObservacionesSolicitado.Text = string.Empty;
    //                this.FechaSolicitud.InitNull = true;
    //                this.ddlTipoDocumento.SelectedIndex = -1;

    //                this.pnlDocumentacionRecibida.Visible = false;

    //                this.btnAplicar.Visible = false;
    //                this.btnAgregar.Visible = true;
    //            }
    //        };
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    #endregion
}