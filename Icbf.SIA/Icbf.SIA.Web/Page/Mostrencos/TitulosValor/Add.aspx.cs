﻿using System;
using System.Web.UI;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_TipoTitulo_Add : GeneralWeb
{
    masterPrincipal toolBar;

    MostrencosService vMostrencosService = new MostrencosService();

    string PageName = "Mostrencos/TitulosValor";

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.txtNombreTitulo.Focus();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdTipoTitulo = Convert.ToInt32(GetSessionParameter("TitulosValor.IdTipoTitulo"));
            TipoTitulo vTipoTitulo = vMostrencosService.ConsultarTipoTituloPorId(vIdTipoTitulo);
            txtNombreTitulo.Text = vTipoTitulo.Nombre;
            chkEstadoTitulo.Items.FindByValue(Convert.ToInt32(vTipoTitulo.Estado).ToString()).Selected = true;
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.EstablecerTitulos("Parametrizar Tipos de Título");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        TipoTitulo tipoTitulo = new TipoTitulo()
        {
            Nombre = txtNombreTitulo.Text,
            Estado = Convert.ToBoolean(Convert.ToInt32(chkEstadoTitulo.SelectedValue)),
            UsuarioCrea = GetSessionUser().NombreUsuario,
            UsuarioModifica = GetSessionUser().NombreUsuario

        };
        int result = 0;
        if (Request.QueryString["oP"] == "E")
        {
            tipoTitulo.IdTipoTitulo = Convert.ToInt32(GetSessionParameter("TitulosValor.IdTipoTitulo"));
            this.InformacionAudioria(tipoTitulo, this.PageName, vSolutionPage);
            result = vMostrencosService.ActualizarTipoTitulo(tipoTitulo);
            if (result == 0)
            {
                SetSessionParameter("TitulosValor.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
            }
        }
        else
        {
            this.InformacionAudioria(tipoTitulo, this.PageName, vSolutionPage);
            result = vMostrencosService.IngresarTipoTitulo(tipoTitulo);
            if (result != 0)
            {
                SetSessionParameter("TitulosValor.IdTipoTitulo", result);
                SetSessionParameter("TitulosValor.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
            }
        }
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
}