﻿using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Mostrencos_TipoTitulo_Detail : GeneralWeb
{
    masterPrincipal toolBar = new masterPrincipal();

    string PageName = "Mostrencos/TitulosValor";

    MostrencosService vMostrencosService = new MostrencosService();

    private int vIdTipoTitulo;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.EstablecerTitulos("Parametrizar Tipos de Título");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("TitulosValor.IdTipoTitulo");
        NavigateTo(SolutionPage.List);
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("TitulosValor.IdTipoTitulo");
        NavigateTo(SolutionPage.Add);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vIdTipoTitulo = Convert.ToInt32(GetSessionParameter("TitulosValor.IdTipoTitulo"));
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            vIdTipoTitulo = Convert.ToInt32(GetSessionParameter("TitulosValor.IdTipoTitulo"));
            if (vIdTipoTitulo != 0)
            {
                if (GetSessionParameter("TitulosValor.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                RemoveSessionParameter("TitulosValor.Guardado");

                TipoTitulo vTipoTitulo = vMostrencosService.ConsultarTipoTituloPorId(vIdTipoTitulo);
                txtNombreTitulo.Text = vTipoTitulo.Nombre;
                chkEstadoTitulo.Items.FindByValue(Convert.ToInt32(vTipoTitulo.Estado).ToString()).Selected = true;
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}