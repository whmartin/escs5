﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_TipoTitulo_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre Tipo de Título
            </td>
            <td>
                Estado Tipo de Título *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreTitulo" MaxLength="100" Width="300" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="VlNombreTitulo" runat="server" TargetControlID="txtNombreTitulo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="chkEstadoTitulo" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo"></asp:ListItem>
                    <asp:ListItem Text="Inactivo"></asp:ListItem>
                    <asp:ListItem Text="Todos" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">              
                <td>
                    <asp:GridView runat="server" ID="gvTipoTitulos" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="gvTipoTitulos_SelectedIndexChanged"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoTitulo" CellPadding="0" Height="16px" OnSorting="gvTipoTitulos_Sorting" OnPageIndexChanging="gvTipoTitulos_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre Tipo de Título" DataField="Nombre" SortExpression="Nombre" />
                            <asp:TemplateField HeaderText="Estado Tipo de Título" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# (Boolean.Parse(Eval("Estado").ToString())) ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
