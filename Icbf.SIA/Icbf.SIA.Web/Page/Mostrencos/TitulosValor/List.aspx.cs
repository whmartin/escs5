﻿using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Mostrencos_TipoTitulo_List : GeneralWeb
{
    masterPrincipal toolBar;

    MostrencosService vMostrencosService = new MostrencosService();

    string PageName = "Mostrencos/TitulosValor";

    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.txtNombreTitulo.Focus();
            }
        }
    }

    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoExcel += new ToolBarDelegate(this.btnExportar_Click);
            this.toolBar.EstablecerTitulos("Parametrizar Tipos de Título");
            this.gvTipoTitulos.PageSize = PageSize();
            this.gvTipoTitulos.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvTipoTitulos.Rows.Count > 0)
        {
            toolBar.LipiarMensajeError();

            DataTable datos = new DataTable();
            Boolean vPaginador = this.gvTipoTitulos.AllowPaging;
            Boolean vPaginadorError = gvTipoTitulos.AllowPaging;
            ExportarExcel vExportarExcel = new ExportarExcel();
            gvTipoTitulos.AllowPaging = false;

            List<TipoTitulo> vTiposTítulos = ((List<TipoTitulo>)this.ViewState["SortedgvTipoTitulos"]).ToList();

            datos = Utilidades.GenerarDataTable(this.gvTipoTitulos, vTiposTítulos);

            if (datos != null)
            {
                if (datos.Rows.Count > 0)
                {
                    GridView datosexportar = new GridView();
                    datosexportar.DataSource = datos;
                    datosexportar.DataBind();
                    vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "TiposTítulos");
                    gvTipoTitulos.AllowPaging = vPaginador;
                }
                else
                    toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
            }
            else
                toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    private void Buscar()
    {
        toolBar.LipiarMensajeError();
        try
        {
            bool? estado = null;
            switch (chkEstadoTitulo.Text)
            {
                case "Activo":
                    estado = true;
                    break;
                case "Inactivo":
                    estado = false;
                    break;
            }
            List<TipoTitulo> vListaTipoTitulo = this.vMostrencosService.ConsultarTipoTitulo(txtNombreTitulo.Text, estado);
            this.ViewState["SortedgvTipoTitulos"] = vListaTipoTitulo;
            this.gvTipoTitulos.Visible = true;
            this.gvTipoTitulos.DataSource = vListaTipoTitulo;
            this.gvTipoTitulos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoTitulos_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<TipoTitulo> vList = new List<TipoTitulo>();
        List<TipoTitulo> vResult = new List<TipoTitulo>();

        if (this.ViewState["SortedgvTipoTitulos"] != null)
        {
            vList = (List<TipoTitulo>)this.ViewState["SortedgvTipoTitulos"];
        }

        switch (e.SortExpression)
        {
            case "Nombre":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderBy(a => a.Nombre).ToList();
                else
                    vResult = vList.OrderByDescending(a => a.Nombre).ToList();
                break;

            case "Estado":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderByDescending(a => a.Estado).ToList();
                else
                    vResult = vList.OrderBy(a => a.Estado).ToList();
                break;
        }

        if (this.Direction == SortDirection.Ascending)
            this.Direction = SortDirection.Descending;
        else
            this.Direction = SortDirection.Ascending;

        this.ViewState["SortedgvTipoTitulos"] = vResult;
        this.gvTipoTitulos.DataSource = vResult;
        this.gvTipoTitulos.DataBind();
    }

    protected void gvTipoTitulos_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvTipoTitulos.SelectedRow);
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvTipoTitulos.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("TitulosValor.IdTipoTitulo", strValue);
            this.SetSessionParameter("TitulosValor.Guardado", "0");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoTitulos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvTipoTitulos.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvTipoTitulos"] != null)
        {
            List<TipoTitulo> vList = (List<TipoTitulo>)this.ViewState["SortedgvTipoTitulos"];
            this.gvTipoTitulos.DataSource = vList;
            this.gvTipoTitulos.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }
}