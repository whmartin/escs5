﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_TipoTitulo_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            Nombre Tipo de Título *
                        </td>
                        <td>
                            Estado Tipo de Título *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreTitulo" MaxLength="100" Width="300" Enabled="false" ></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="VlNombreTitulo" runat="server" TargetControlID="txtNombreTitulo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="chkEstadoTitulo" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0" runat="server"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
