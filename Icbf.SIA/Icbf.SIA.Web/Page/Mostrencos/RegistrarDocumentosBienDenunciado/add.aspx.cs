﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.Configuration;
using System.IO;

public partial class Page_Mostrencos_RegistrarDocumentosBienDenunciado_add : GeneralWeb
{
    #region Declaración Variables 
    /// <summary>
    /// The tool bar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// The page name
    /// </summary>
    string PageName = "Mostrencos/RegistrarDocumentosBienDenunciado";

    /// <summary>
    /// The v mostrencos service
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// The v identifier denuncia bien/
    /// </summary>
    private int vIdDenunciaBien;
    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #endregion

    #region "Eventos"

    /// <summary>
    /// Page PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }

        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        string evento = Convert.ToString(GetSessionParameter("DocumentarBienDenunciado.Evento"));

        if (!string.IsNullOrEmpty(evento) && evento == "Adjuntar")
        {
            RemoveSessionParameter("DocumentarBienDenunciado.Evento");
            Adjuntar();
        }


    }

    /// <summary>
    /// Handles the RowCommand event of the gvwDocumentacionRecibida control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewCommandEventArgs"/> instance containing the event data.</param>
    /// <exception cref="Exception">El registro seleccionado ya fue gestionado y no se puede editar, por favor realice una nueva solicitud</exception>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            if (e.CommandName == "Eliminar")
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));/// consulto historico

                if (vDenunciaBien.IdEstadoDenuncia != 11)
                {
                    GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
                    vdo.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(gvr.Cells[0].Text);
                    vdo.IdEstadoDocumento = 6;
                    vMostrencosService.ActualizarEstadoDocumentosSolicitadosDenunciaBien(vdo);
                    CargarGrillas();
                }
            }
            else if (e.CommandName == "Editar")
            {
                GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                int vIdEstado = Convert.ToInt32(gvr.Cells[5].Text);
                if (vIdEstado == 2) //Estado mayor a solicitado
                {
                    this.ddlTipoDocumento.SelectedValue = Convert.ToString(gvr.Cells[1].Text);
                    this.FechaSolicitud.Text = Convert.ToString(gvr.Cells[3].Text);
                    this.txtObservacionesSolicitado.Text = HttpContext.Current.Server.HtmlDecode(Convert.ToString(gvr.Cells[4].Text));
                    //this.txtObservacionesSolicitado.Enabled = false;
                    //this.FechaSolicitud.Enabled = false;
                    //this.ddlTipoDocumento.Enabled = false;
                    //this.btnAplicar.Enabled = true;
                    this.btnAplicar.Visible = true;
                    this.btnAdd.Visible = false;
                    HabilitarPanelRecibido(true);
                    LimpiarCamposTercero();
                    DeshabilitarCamposTercero();
                    SetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia", Convert.ToInt32(gvr.Cells[0].Text));
                    toolBar.LipiarMensajeError();
                }
                else
                {
                    throw new Exception("El registro seleccionado ya fue gestionado y no se puede editar, por favor realice una nueva solicitud");
                }

            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwHistorico_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Ver"))
        {
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwHistorico.DataKeys[vRow].Value);
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            DocumentosSolicitadosDenunciaBien vArchivo = vList.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();

            if (vArchivo.RutaArchivo.Split('/').Count() > 2)
            {
                this.ifmVerAdchivo.Attributes.Add("src", vArchivo.RutaArchivo);
            }
            else
            {
                this.ifmVerAdchivo.Attributes.Add("src", "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo);
            }

            this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
            this.pnlArchivo.CssClass = "popupFile";
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    /// <summary>
    /// Grila Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvDocumentacionDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvDocumentacionDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgrvDocumentacionDenuncia"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgrvDocumentacionDenuncia"];
            grvDocumentacionDenuncia.DataSource = vList;
            grvDocumentacionDenuncia.DataBind();
        }
        else
        {
            ViewState["SortedgrvDocumentacionDenuncia"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.grvDocumentacionDenuncia.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.grvDocumentacionDenuncia.DataBind();
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistorico_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwHistorico.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwHistorico"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            gvwHistorico.DataSource = vList;
            gvwHistorico.DataBind();
        }
        else
        {
            ViewState["SortedgvwHistorico"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataBind();
        }
    }

    /// <summary>
    /// Seleccion de Registros en la Grilla Otros Documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvOtrosDocumentos_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvOtrosDocumentos.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgrvOtrosDocumentos"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgrvOtrosDocumentos"];
            grvOtrosDocumentos.DataSource = vList;
            grvOtrosDocumentos.DataBind();
        }
        else
        {
            ViewState["SortedgrvOtrosDocumentos"] = vMostrencosService.ConsultarOtrosDocumentos(vIdDenunciaBien, -1);
            this.grvOtrosDocumentos.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgrvOtrosDocumentos"];
            this.grvOtrosDocumentos.DataBind();
        }
    }

    /// <summary>
    /// Ordenamiento de Registros en Grilla Documentación Recibida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }

        if (e.SortExpression.Equals("NombreEstado"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreEstado).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaRecibidoGrilla"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreArchivo"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                    }
                }
                else
                {
                    if (e.SortExpression.Equals("ObservacionesDocumentacionRecibida"))
                    {
                        if (this.Direction == SortDirection.Ascending)
                        {
                            ////Ascendente
                            vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                        }
                        else
                        {
                            ////Descendente
                            vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                        }
                    }
                }
            }

        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (e.SortExpression.Equals("RazonSocial"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.Tercero.RazonSocial).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.Tercero.RazonSocial).ToList();
            }
        }

        if (e.SortExpression.Equals("FechaOficio"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.FechaOficio).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.FechaOficio).ToList();
            }
        }

        if (e.SortExpression.Equals("AsuntoOficio"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.AsuntoOficio).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.AsuntoOficio).ToList();
            }
        }

        if (e.SortExpression.Equals("ResumenOficio"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.ResumenOficio).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.ResumenOficio).ToList();
            }
        }

        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CustomersGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));/// consulto historico
        if (vDenunciaBien.IdEstadoDenuncia == 11)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var text2 = e.Row.Cells[16];
                text2.Enabled = false;
            }
        }
    }

    /// <summary>
    /// Ordenamiento de Registros en la Grilla Historico
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistorico_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwHistorico"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwHistorico"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreEstado"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                    }
                }
            }
        }

        if (e.SortExpression.Equals("FechaRecibidoGrilla"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("NombreArchivo"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
            }



        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwHistorico"] = vResult;
        this.gvwHistorico.DataSource = vResult;
        this.gvwHistorico.DataBind();
    }

    /// <summary>
    /// Paginación de registros en la Grilla del Pop Up Busqueda Terceros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvBusquedaTerceros_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvBusquedaTerceros.PageIndex = e.NewPageIndex;
        List<Tercero> vlstTerceros = this.vMostrencosService.ConsultarJuridicoTercerosPorNombre(this.txbNombreEntidad.Text);
        if (vlstTerceros.Count() > 0)
        {
            grvBusquedaTerceros.DataSource = vlstTerceros;
            grvBusquedaTerceros.DataBind();
        }
        modalPopupInformacionTerceros.Show();
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    /// <summary>
    /// Selección de Registros en la Grilla del Pop Up Busqueda Terceros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvBusquedaTerceros_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(grvBusquedaTerceros.SelectedRow);
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Iniciars this instance.
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoAdjuntar += new ToolBarDelegate(btnAdjuntar_Click);
            toolBar.EstablecerTitulos("Documentar denuncia");
            this.FechaSolicitud.Enabled = false;
            this.FechaSolicitud.Text = DateTime.Today.Date.ToString("dd/MM/yyyy");
            this.grvDocumentacionDenuncia.PageSize = PageSize();
            this.grvDocumentacionDenuncia.EmptyDataText = EmptyDataText();
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.Columns[11].Visible = false;
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
            this.gvwHistorico.PageSize = PageSize();
            this.gvwHistorico.EmptyDataText = EmptyDataText();
            this.btnAplicar.Visible = false;
            this.btnAdd.Visible = true;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the datos iniciales.
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            DeshabilitarCamposTercero();

            List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            if (vRegistroDenuncia.DocumentacionCompleta == true)
            {
                this.rblDocumentacionCompleta.SelectedValue = "1";
            }
            else
            {
                this.rblDocumentacionCompleta.SelectedValue = "0";
            }

            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));/// consulto historico
            List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(this.vIdDenunciaBien);
            var validaDocumentacion = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 1 && x.IdAccion == 2);
            var ControlTerceros = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdAccion == 2 && x.IdFase == 1 && x.IdActuacion == 6);

            SetSessionParameter("DocumentarBienDenunciado.ExisteTercero", ControlTerceros);
            //Validar TODO
            if (ControlTerceros != null && vDenunciaBien.ExisteDenuncia == false && vRegistroDenuncia.DocumentacionCompleta == false)
            {
                this.HabilitarCamposTercero();
                this.gvwDocumentacionRecibida.Enabled = true;
                this.rblDocumentacionCompleta.Enabled = true;
            }
            else
            {
                this.rblDocumentacionCompleta.Enabled = false;
            }

            if (vDenunciaBien.IdEstadoDenuncia == 11 || vDenunciaBien.IdEstadoDenuncia == 13 || vDenunciaBien.IdEstadoDenuncia == 19)
            {
                this.PanelClaseDenuncia.Enabled = false;
                this.Panel3.Enabled = false;
            }

            if (vDenunciaBien.ExisteDenuncia)
            {
                var CalidadDenuncianteNoReconoce = vMostrencosService.ConsultarCalidadDenunciante(this.vIdDenunciaBien).Find(x => x.ReconocimientoCalidad == "No Reconoce");
                bool EsCalidadDenuncianteReconoce = vMostrencosService.ConsultarCalidadDenunciante(this.vIdDenunciaBien).Any(x => x.ReconocimientoCalidad == "Reconoce");
                if (EsCalidadDenuncianteReconoce)
                {
                    this.rblDocumentacionCompleta.Enabled = true;
                }
            }

            vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            this.hfCorreo.Value = !string.IsNullOrEmpty(vRegistroDenuncia.CORREOELECTRONICO) ? vRegistroDenuncia.CORREOELECTRONICO : string.Empty;
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                   vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                   ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;

            // Se asigna el valor de la denuncia
            if (vlstDenunciaBienTitulo.Count() > 0)
            {
                string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                this.TextValordelaDenuncia.Text = valorDenuncia;
            }
            else
            {
                this.TextValordelaDenuncia.Text = "0,00";
            }

            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.PRIMERNOMBRE.ToString() + " " + vRegistroDenuncia.SEGUNDONOMBRE.ToString() + " " + vRegistroDenuncia.PRIMERAPELLIDO.ToString() + " " + vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
                vRegistroDenuncia.NombreMostrar = vRegistroDenuncia.RazonSocial.ToString();
            }
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia.ToString();
            if (vRegistroDenuncia.ClaseDenuncia == "1")
            {
                rblClaseDenuncia.SelectedIndex = 0;
            }
            else if (vRegistroDenuncia.ClaseDenuncia == "2")
            {
                rblClaseDenuncia.SelectedIndex = 1;
            }
            else
            {
                rblClaseDenuncia.SelectedIndex = 2;
            }

            this.CargarGrillas();
            SetSessionParameter("DocumentarBienDenunciado.Denuncia", vRegistroDenuncia);
            TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
            vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
            vTipoDocumento.NombreTipoDocumento = "Seleccione";
            vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
            vlstTipoDocumentosBien.Add(vTipoDocumento);
            HabilitarPanelRecibido(false);
            this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
            this.ddlTipoDocumento.SelectedValue = "-1";
            this.ddlTipoDocumento.DataBind();

            List<DocumentosSolicitadosDenunciaBien> listDoc = new List<DocumentosSolicitadosDenunciaBien>();
            listDoc = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            //listDoc  POR DEFECTO SELECCIONA DOCUMENTACIÓN COMPLETA COMO "NO"
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guardars the todo.
    /// </summary>
    public void GuardarTodo()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vlist = new List<DocumentosSolicitadosDenunciaBien>();

            RegistroDenuncia vDenuncia = new RegistroDenuncia();
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            vDenuncia.IdDenunciaBien = vIdDenunciaBien;
            vDenuncia.DescripcionDenuncia = this.txtDescripcion.Text;
            vDenuncia.ClaseDenuncia = rblClaseDenuncia.SelectedValue.ToString();
            vMostrencosService.ModificarDenunciaDocumentosSolicitadosDenunciaBien(vDenuncia);

            if (this.rblDocumentacionCompleta.SelectedValue == "1")
            {
                this.InsertarHistorico();
                this.ActualizarDocumentacionCompleta(vIdDenunciaBien, true);
            }
            else if (this.rblDocumentacionCompleta.SelectedValue == "0")
            {
                this.ActualizarDocumentacionCompleta(vIdDenunciaBien, false);
            }

            foreach (GridViewRow vRow in gvwDocumentacionRecibida.Rows)
            {
                DocumentosSolicitadosDenunciaBien vDoc = new DocumentosSolicitadosDenunciaBien();
                vDoc.NombreTipoDocumento = Convert.ToString(vRow.Cells[2].Text);
                vDoc.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vRow.Cells[0].Text);
                vDoc.FechaSolicitud = string.IsNullOrEmpty(vRow.Cells[3].Text) || vRow.Cells[3].Text == @"&nbsp;" ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(vRow.Cells[3].Text);
                vDoc.ObservacionesDocumentacionSolicitada = Convert.ToString(vRow.Cells[4].Text);
                vDoc.IdEstadoDocumento = Convert.ToInt32(vRow.Cells[5].Text);
                try
                {
                    vDoc.FechaRecibido = Convert.ToDateTime(vRow.Cells[7].Text);
                }
                catch (Exception)
                {

                    vDoc.FechaRecibido = Convert.ToDateTime("01/01/0001");
                }

                vDoc.NombreArchivo = Convert.ToString(vRow.Cells[8].Text);
                vDoc.ObservacionesDocumentacionRecibida = Convert.ToString(vRow.Cells[9].Text);
                //GuardarDocumento(vDoc);  
                vlist.Add(vDoc);
            }
            toolBar.MostrarMensajeGuardado();
            EnviarNotificacion(vlist);
            toolBar.OcultarBotonGuardar(true);
            pnlDocumentacionSolicitada.Enabled = true;
            HabilitarPanelRecibido(false);
            SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
            SetSessionParameter("DocumentarBienDenunciado.Guardado", "1");
            NavigateTo(SolutionPage.Detail);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guardars the documento.
    /// </summary>
    /// <param name="pDocumento">The p documento.</param>
    public void GuardarDocumento(DocumentosSolicitadosDenunciaBien pDocumento)
    {
        try
        {
            if (pDocumento.IdDocumentosSolicitadosDenunciaBien == 0)
            {
                vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(pDocumento);
                CargarGrillas();
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Adjuntars this instance.
    /// </summary>
    public void Adjuntar()
    {
        try
        {
            pnlInformacionDenuncia.Enabled = false;
            PanelNombrePersonaNatural.Enabled = false;
            PanelRazonSocial.Enabled = false;
            PanelClaseDenuncia.Enabled = false;
            pnlDocumentacionDenuncia.Enabled = false;
            //pnlDocumentacionSolicitada.Enabled = false;
            HabilitarPanelRecibido(true);
            pnlGridDocumentacionRecibida.Enabled = true;
            pnlHistorico.Enabled = false;
            this.txtObservacionesSolicitado.Enabled = false;
            this.FechaSolicitud.Enabled = false;
            this.FechaRecibido.Enabled = true;
            FechaRecibido.Date = DateTime.Now;
            this.ddlTipoDocumento.Enabled = false;
            this.btnAplicar.Enabled = false;
            this.btnAplicar.Visible = true;
            this.txtDescripcion.Enabled = false;
            this.btnAdd.Visible = false;
            gvwDocumentacionRecibida.Columns[11].Visible = true;
            gvwDocumentacionRecibida.Columns[10].Visible = false;
            toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.LipiarMensajeError();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    public void CargarGrillas()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));

            this.CargarGrillaHistorico(vIdDenunciaBien);
            ////Se llena la grilla con los documentos cuyo estado sea inicial (1) - Documentos de la denuncia 
            ViewState["SortedgrvDocumentacionDenuncia"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.grvDocumentacionDenuncia.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.grvDocumentacionDenuncia.DataBind();

            //// Se llena la grilla con los documentos cuyo estado sea diferente al inicial y el eliminado (0)
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();

            ////Se llena la grilla con los todos documentos de la denuncia (Todos los estados)
            ViewState["SortedgvwHistorico"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataBind();

            ViewState["SortedgrvOtrosDocumentos"] = vMostrencosService.ConsultarOtrosDocumentos(vIdDenunciaBien, -1);
            this.grvOtrosDocumentos.DataSource = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgrvOtrosDocumentos"];
            this.grvOtrosDocumentos.DataBind();

        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Guardars the archivos.
    /// </summary>
    /// <param name="pIdDenunciaBien">The p identifier denuncia bien.</param>
    /// <param name="pPosition">The p position.</param>
    /// <returns></returns>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        try
        {
            HttpFileCollection files = Request.Files;

            if (files.Count > 0)
            {
                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                if (extFile != extPdf)
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
                }
                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 2048)
                {
                    throw new Exception("El tamaño del archivo PDF permitido es de 2.048 KB");
                }
                HttpPostedFile file = files[pPosition];

                if (file.ContentLength > 0)
                {
                    string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                    string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                    string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");

                    bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                    if (!existeCarpeta)
                    {
                        System.IO.Directory.CreateDirectory(carpetaBase);
                    }

                    this.vFileName = rutaParcial;
                    file.SaveAs(filePath);
                }
            }

            return true;
        }
        catch (HttpRequestValidationException)
        {
            throw new Exception("Se detectó un posible archivo peligroso.");
            //this.toolBar.MostrarMensajeError("Se detectó un posible archivo peligroso.");
            //return false;
        }
        catch (Exception ex)
        {
            throw ex;
            //this.toolBar.MostrarMensajeError(ex.Message);
            //return false;
        }
    }

    /// <summary>
    /// Envia notificación al guardar el registro de la denuncia
    /// </summary>
    /// <param name="pRegistroDenuncia">Entidad pRegistroDenuncia</param>
    private void EnviarNotificacion(List<DocumentosSolicitadosDenunciaBien> pListDocs)
    {
        RegistroDenuncia vDenuncia = (RegistroDenuncia)GetSessionParameter("DocumentarBienDenunciado.Denuncia");
        //string vEmailPara = vDenuncia.CORREOELECTRONICO;
        string vEmailPara = hfCorreo.Value;
        string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"];
        string vAsunto = string.Empty;
        string vMensaje = string.Empty;
        int vIdUsuario = 0;
        string vArchivo = string.Empty;

        vAsunto = HttpContext.Current.Server.HtmlDecode("Solicitud de documentación para la denuncia.");

        string vdocumentos = string.Empty;
        vdocumentos = "<table width='90%' > <tr><td><B>Documento&nbsp;&nbsp;</B></td><td><B>Observaci&oacuten</B></td></tr>";
        foreach (DocumentosSolicitadosDenunciaBien vdoc in pListDocs)
        {
            vdocumentos = vdocumentos + "<tr><td>" + vdoc.NombreTipoDocumento + "&nbsp;&nbsp;</td><td>" + vdoc.ObservacionesDocumentacionSolicitada + "</td></tr>";
            //vdocumentos = vdocumentos + vdoc.NombreTipoDocumento + " &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;   " + vdoc.ObservacionesDocumentacionSolicitada + " </br>";
        }
        vMensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B> Documentaci&oacute;n de la Denuncia </B> </CENTER> </font> ") +
                    "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + vDenuncia.NombreMostrar + " <br/> <br/> La siguiente denuncia: " +
                    "<br/> " +
                    "<br/> <br/> <br/> <B> Radicado de la denuncia:  </B>" + "&nbsp;&nbsp;" + vDenuncia.RadicadoDenuncia +
                    "<br/> <br/> <B> Fecha y Hora de radicado:   </B>" + "&nbsp;&nbsp;" + Convert.ToString(vDenuncia.FechaRadicadoDenuncia.ToString() +
                    "<br/> <br/> <br/> Requiere los siguientes documentos: <br/>" +
                    "<br/> " + vdocumentos +
                     "</table> " +
                    HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor hacerlos llegar lo más pronto posible al abogado encargado de esta denuncia. "));


        bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
    }

    /// <summary>
    /// Habilitar panel de recibidos
    /// </summary>
    /// <param name="EsHabilitado"></param>
    private void HabilitarPanelRecibido(Boolean EsHabilitado)
    {
        try
        {
            this.rbtEstadoDocumento.Enabled = EsHabilitado;
            this.FechaRecibido.Enabled = EsHabilitado;
            this.fulArchivoRecibido.Enabled = EsHabilitado;
            this.rbtDocumentacionCompleta.Enabled = EsHabilitado;
            this.txtObservacionesRecibido.Enabled = EsHabilitado;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Insertar Historico de los Estados de la Denuncia Bien
    /// </summary>
    /// <returns></returns>
    public int InsertarHistorico()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15;
        pHistoricoEstadosDenunciaBien.IdFase = 1;
        pHistoricoEstadosDenunciaBien.IdAccion = 38;
        pHistoricoEstadosDenunciaBien.IdActuacion = 1;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.Fase = 1.ToString();
        pHistoricoEstadosDenunciaBien.Accion = 2.ToString();
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
        return IdHistoricoDocumentosSolicitados;
    }

    /// <summary>
    /// Actualizar la Fase de la Denuncia Bien
    /// </summary>
    /// <returns></returns>
    public int ActualizarDocumentacionCompleta(int pIdDenunciaBien, bool DocCompleta)
    {
        //int Actualizado = Convert.ToInt32(this.vMostrencosService.EditarDenunciaBienFase(vIdDenunciaBien));
        return this.vMostrencosService.EditarDocumentacionCompleta(pIdDenunciaBien, DocCompleta);
    }

    /// <summary>
    /// Metodo para seleccionar registros de la Grilla Busqueda Terceros en el Pop Up
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        int rowIndex = pRow.RowIndex;
        string strValueIdTercero;
        SetSessionParameter("Mostrencos.RegistrarDocumentosBienDenunciado.IdTercero", null);
        try
        {
            if (grvBusquedaTerceros.DataKeys[rowIndex].Values["IDTERCERO"] != null)
            {
                strValueIdTercero = grvBusquedaTerceros.DataKeys[rowIndex].Values["IDTERCERO"].ToString();
                SetSessionParameter("Mostrencos.RegistrarDocumentosBienDenunciado.IdTercero", strValueIdTercero);

            }
            txtEntidadOficiarSolicitado.Text = ((Label)pRow.FindControl("lblRazonSocial")).Text;
            HabilitarCamposTercero();
            pnlArchivo.CssClass = "popuphIstorico hidden";
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Validación de campos en el Pop Up
    /// </summary>
    /// <returns></returns>
    private bool ValidarCamposPopUp()
    {
        bool vEsrequerido = false;
        int count = 0;

        try
        {
            //Validar Campo Vigencia
            if (this.txbNombreEntidad.Text == "")
            {
                //this.lblCampoRequeridoNombreEntidad.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                //this.lblCampoRequeridoNombreEntidad.Visible = false;
                vEsrequerido = false;
            }


            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Habilitar campos de la Sección Terceros
    /// </summary>
    public void HabilitarCamposTercero()
    {
        this.fecFechaOficioSolicitado.Enabled = true;
        this.btnBuscarDetalleSolicitado.Enabled = true;
        //this.txtEntidadOficiarSolicitado.Enabled = true;
        this.txtAsuntoOficioSolicitado.Enabled = true;
        this.txtResumenEjecutivoSolicitado.Enabled = true;
        //this.btnBuscarDetalleSolicitado.Enabled = true;
    }

    /// <summary>
    ///  Deshabilitar campos de la Sección Terceros
    /// </summary>
    public void DeshabilitarCamposTercero()
    {
        this.fecFechaOficioSolicitado.Enabled = false;
        //this.txtEntidadOficiarSolicitado.Enabled = false;
        this.txtAsuntoOficioSolicitado.Enabled = false;
        this.txtResumenEjecutivoSolicitado.Enabled = false;
        //this.btnBuscarDetalleSolicitado.Enabled = false;
    }

    /// <summary>
    ///  Limpiar campos de la Sección Terceros
    /// </summary>
    public void LimpiarCamposTercero()
    {
        this.fecFechaOficioSolicitado.Enabled = false;
        this.txtEntidadOficiarSolicitado.Text = "";
        this.txtAsuntoOficioSolicitado.Text = "";
        this.txtResumenEjecutivoSolicitado.Text = "";
        //this.btnBuscarDetalleSolicitado.Enabled = false;
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        GuardarTodo();
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAdjuntar_Click(object sender, EventArgs e)
    {
        Adjuntar();
    }

    /// <summary>
    /// Handles the Click event of the btnAdd control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));/// consulto historico
            var ControlTerceros = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdAccion == 2 && x.IdFase == 1 && x.IdActuacion == 6);

            this.fcInvSoli.Visible = false;
            if (ddlTipoDocumento.SelectedValue == "-1")
            {
                lblReqTipoDocumentoSolicitado.Visible = true;
                return;
            }
            else
            {
                lblReqTipoDocumentoSolicitado.Visible = false;
            }

            if (this.fecFechaOficioSolicitado.Date == Convert.ToDateTime("1/01/1900 12:00:00 a. m.")
                && ControlTerceros == null && vDenunciaBien.ExisteDenuncia == true)
            {
                this.fcInvSoli.Visible = true;
                return;
            }
            else
            {
                this.fcInvSoli.Visible = false;
            }

            DateTime fechaSolicitud = Convert.ToDateTime(FechaSolicitud.Text);
            DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = new DocumentosSolicitadosDenunciaBien();
            vDocumentoSolicitado.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien = 0; //Nuevo
            vDocumentoSolicitado.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlTipoDocumento.SelectedValue.ToString());
            vDocumentoSolicitado.NombreTipoDocumento = ddlTipoDocumento.SelectedItem.Text.ToString();
            vDocumentoSolicitado.FechaSolicitud = new DateTime(fechaSolicitud.Year, fechaSolicitud.Month, fechaSolicitud.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            vDocumentoSolicitado.ObservacionesDocumentacionSolicitada = txtObservacionesSolicitado.Text;
            vDocumentoSolicitado.NombreArchivo = string.Empty;
            vDocumentoSolicitado.RutaArchivo = string.Empty;
            vDocumentoSolicitado.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
            vDocumentoSolicitado.FechaCrea = DateTime.Today;
            vDocumentoSolicitado.IdEstadoDocumento = 2;
            if (txtEntidadOficiarSolicitado.Text.Equals(string.Empty))
            {
                vDocumentoSolicitado.Tercero.IdTercero = 0;
            }
            else
            {
                vDocumentoSolicitado.Tercero.IdTercero = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarDocumentosBienDenunciado.IdTercero"));
            }
            vDocumentoSolicitado.FechaOficio = (this.fecFechaOficioSolicitado.Date == Convert.ToDateTime("1/01/1900 12:00:00 a. m.")) ? vDocumentoSolicitado.FechaOficio : this.fecFechaOficioSolicitado.Date;
            vDocumentoSolicitado.AsuntoOficio = txtAsuntoOficioSolicitado.Text;
            vDocumentoSolicitado.ResumenOficio = txtResumenEjecutivoSolicitado.Text;
            GuardarDocumento(vDocumentoSolicitado);

            ddlTipoDocumento.SelectedValue = "-1";
            FechaSolicitud.Text = DateTime.Today.Date.ToString("dd/MM/yyyy");
            txtObservacionesSolicitado.Text = string.Empty;
            var terc = GetSessionParameter("DocumentarBienDenunciado.ExisteTercero");
            this.LimpiarCamposTercero();
            //   HabilitarCamposTercero();
            CargarGrillas();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Boton de Consulta de la Ventana Modal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultarVentanaModal_Click(object sender, ImageClickEventArgs e)
    {
        List<Tercero> vlstTerceros = this.vMostrencosService.ConsultarJuridicoTercerosPorNombre(this.txbNombreEntidad.Text);

        if (ValidarCamposPopUp() == false)
        {
            if (vlstTerceros.Count() > 0)
            {
                grvBusquedaTerceros.DataSource = vlstTerceros;
                grvBusquedaTerceros.DataBind();
            }
            else
            {
                this.grvBusquedaTerceros.PageSize = PageSize();
                this.grvBusquedaTerceros.EmptyDataText = EmptyDataText();
                this.grvBusquedaTerceros.Visible = true;
                this.grvBusquedaTerceros.DataBind();
                //ShowHeaderWhenEmpty = "True" EmptyDataText = "No records Found"
            }
            modalPopupInformacionTerceros.Show();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAplicar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();
            lblValidacionFechaSolicitud.Visible = false;
            lblValidacionFechaMaxima.Visible = false;

            if (ddlTipoDocumento.SelectedValue == "-1")
            {
                lblReqTipoDocumentoSolicitado.Visible = true;
                return;
            }
            else
            {
                lblReqTipoDocumentoSolicitado.Visible = false;
            }

            try
            {
                Convert.ToDateTime(FechaRecibido.Date);
                lblFechaInvalida.Visible = false;
            }
            catch (Exception)
            {
                lblFechaInvalida.Visible = true;
                return;
            }
            if (this.FechaRecibido.Date > DateTime.Now.Date)
            {
                this.lblValidacionFechaMaxima.Visible = true;
                return;
            }
            else
            {
                this.lblValidacionFechaMaxima.Visible = false;
            }

            if (this.FechaRecibido.Date < Convert.ToDateTime(this.FechaSolicitud.Text).Date)
            {
                this.lblValidacionFechaSolicitud.Visible = true;
                return;
            }
            else
            {
                this.lblValidacionFechaSolicitud.Visible = false;
            }

            Boolean vPuedeGuardar = true;
            int vIdDocumentoDenuncia = (int)GetSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
            if (vIdDocumentoDenuncia != 0)
            {
                if (!fulArchivoRecibido.HasFile)
                {
                    lblRqfNombreArchivo.Visible = true;
                    vPuedeGuardar = false;
                }
                else
                {
                    lblRqfNombreArchivo.Visible = false;
                }

                if (FechaRecibido.Date == (Convert.ToDateTime("01/01/1900")))
                {
                    lblrqfFechaRecibido.Visible = true;
                    vPuedeGuardar = false;
                }
                else
                {
                    lblrqfFechaRecibido.Visible = false;
                }
                if (rbtDocumentacionCompleta.SelectedValue != "0" && rbtDocumentacionCompleta.SelectedValue != "1")
                {
                    this.lblrqfDocumentacionCompleta.Visible = true;
                    vPuedeGuardar = false;
                }
                else
                {
                    this.lblrqfDocumentacionCompleta.Visible = false;
                }
                if (rbtEstadoDocumento.SelectedValue != "3" && rbtEstadoDocumento.SelectedValue != "5")
                {
                    this.lblrqfEstadoRecibido.Visible = true;
                    vPuedeGuardar = false;
                }
                else
                {
                    this.lblrqfEstadoRecibido.Visible = false;
                }
                GuardarArchivos(vIdDocumentoDenuncia, 0);
                if (vPuedeGuardar)
                {
                    DocumentosSolicitadosYRecibidos vDocumento = new DocumentosSolicitadosYRecibidos();
                    vDocumento.IdDocumentosSolicitadosDenunciaBien = vIdDocumentoDenuncia;
                    //Documentacion Solicitada Modificada
                    DateTime fechaSolicitud = Convert.ToDateTime(FechaSolicitud.Text);
                    vDocumento.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlTipoDocumento.SelectedValue.ToString());
                    vDocumento.NombreTipoDocumento = ddlTipoDocumento.SelectedItem.Text.ToString();
                    vDocumento.FechaSolicitud = new DateTime(fechaSolicitud.Year, fechaSolicitud.Month, fechaSolicitud.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    vDocumento.ObservacionesDocumentacionSolicitada = txtObservacionesSolicitado.Text;
                    //Documentacion Solicitada Modificada
                    vDocumento.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                    vDocumento.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    vDocumento.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                    vDocumento.DocumentacionCompleta = this.rbtDocumentacionCompleta.SelectedValue;
                    vDocumento.NombreArchivo = fulArchivoRecibido.FileName.Replace(".pdf", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf").Replace(".PDF", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                    vDocumento.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName.Replace(".pdf", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf").Replace(".PDF", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                    //vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(vDocumento);
                    vMostrencosService.EditarDocumentosSolicitadosYRecibidosDenunciaBien(vDocumento);

                    CargarGrillas();

                    ddlTipoDocumento.SelectedValue = "-1";
                    txtObservacionesSolicitado.Text = string.Empty;

                    this.rbtEstadoDocumento.ClearSelection();
                    this.rbtDocumentacionCompleta.ClearSelection();
                    this.txtObservacionesRecibido.Text = string.Empty;
                    this.rbtEstadoDocumento.Enabled = false;
                    this.rbtDocumentacionCompleta.Enabled = false;
                    this.txtObservacionesRecibido.Enabled = false;
                    this.FechaRecibido.InitNull = true;
                    this.FechaRecibido.Enabled = false;
                    this.btnAplicar.Visible = false;
                    this.btnAdd.Visible = true;

                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    /// <summary>
    /// Evento de selección documentación completa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rblDocumentacionCompleta_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (this.rblDocumentacionCompleta.SelectedValue == "0")
        {
            //this.rblDocumentacionCompleta.Enabled = false;
        }
    }
}