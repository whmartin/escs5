﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RegistrarDocumentosBienDenunciado_List" %>

<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<%--<%@ Register Src="~/General/General/Control/fechaFestivos.ascx" TagPrefix="uc1" TagName="fecha" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Departamento ubicación *
                  <asp:RequiredFieldValidator ID="rfvDepartamento" ControlToValidate="txtDepartamento"
                      ValidationGroup="btnBuscar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td>Estado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtDepartamento" Enabled="false" MaxLength="64" Width="300"></asp:TextBox>

                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlEstado" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged"></asp:DropDownList>

                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Registrado desde *
                <asp:Label ID="lblCampoRequeridoFechaDesde" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <br />
                            <asp:Label ID="lblErrorFechaDesde" runat="server" Text="La fecha seleccionada no es correcta. Por favor revisar" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Registrado hasta *
                <asp:Label ID="lblCampoRequeridoFechaHasta" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <br />
                            <asp:Label ID="lblErrorFechaHasta" runat="server" Text="La fecha seleccionada no es correcta. Por favor revisar" ForeColor="Red" Visible="false"></asp:Label>

                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CompareValidator ID="cvFecha" runat="server" Type="Date" Operator="DataTypeCheck"
                                ControlToValidate="FechaDesde$txtFecha" ErrorMessage="Fecha inv&aacute;lida." SetFocusOnError="True"
                                ValidationGroup="btnBuscar" Display="Dynamic" ForeColor="Red">
                            </asp:CompareValidator>

                            <uc1:fecha runat="server" ID="FechaDesde" Requerid="true" />
                        </td>
                        <td>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Date" Operator="DataTypeCheck"
                                ControlToValidate="FechaHasta$txtFecha" ErrorMessage="Fecha inv&aacute;lida." SetFocusOnError="True"
                                ValidationGroup="btnBuscar" Display="Dynamic" ForeColor="Red">
                            </asp:CompareValidator>
                            <uc1:fecha runat="server" ID="FechaHasta" Requerid="true" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Radicado de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" MaxLength="13"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegistrarDocumentosBienDenunciado" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDenunciaBien" CellPadding="0" Height="16px" OnSorting="gvRegistrarDocumentosBienDenunciado_OnSorting"
                        OnPageIndexChanging="gvRegistrarDocumentosBienDenunciado_PageIndexChanging" OnSelectedIndexChanged="gvRegistrarDocumentosBienDenunciado_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Radicado denuncia" DataField="RadicadoDenuncia" SortExpression="RadicadoDenuncia" />
                            <asp:BoundField HeaderText="Tipo de identificación" DataField="Tercero.TiposDocumentosGlobal.NomTipoDocumento" SortExpression="NombreTipoIdentificacion" />
                            <asp:BoundField HeaderText="Número de identificación" DataField="Tercero.NumeroIdentificacion" SortExpression="NUMEROIDENTIFICACION" />
                            <asp:BoundField HeaderText="Nombre / Razón social" DataField="NombreMostrar" SortExpression="NombreMostrar" />
                            <asp:BoundField HeaderText="Fecha de radicado denuncia" DataField="FechaRadicadoDenuncia" SortExpression="FechaRadicadoDenuncia" DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Departamento ubicación" DataField="Regional.NombreRegional" SortExpression="NombreRegionalUbicacion" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoDenuncia.NombreEstadoDenuncia" SortExpression="NombreEstadoDenuncia" />
                            <asp:BoundField HeaderText="Fecha Estado" DataField="FechaEstado" SortExpression="FechaEstado" DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Abogado asignado" DataField="Abogados.NombreAbogado" SortExpression="NombreAbogado" />
                            <asp:BoundField HeaderText="Fecha asignación abogado" DataField="Abogados.FechaAsignacionAbogado" SortExpression="FechaAsignacionAbogado" DataFormatString="{0:d}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
