﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_RegistrarDocumentosBienDenunciado_List
/// </summary>
public partial class Page_RegistrarDocumentosBienDenunciado_List : GeneralWeb
{
    /// <summary>
    /// Declaracion Toolbar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarDocumentosBienDenunciado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// preinicia la pagina
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Carga la pagina
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.SetSessionParameter("DocumentarBienDenunciado.Guardado", "0");
        SolutionPage vSolutionPage = SolutionPage.List;
        this.FechaDesde.Focus();
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// evento del boton Buscar
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Busca las denuncias filtrando siempre por la regional del usuario que inició la sesión y lass denuncias en estado REGISTRADA
    /// además de los filtros opcionales como fechas desde y hasta y numero de radicado de denuncia
    /// </summary>
    private void Buscar()
    {
        bool vEsrequerido = false;
        try
        {
            string vRadicadoDenuncia = string.Empty;

            if (this.txtRadicadoDenuncia.Text == string.Empty)
            {
                vRadicadoDenuncia = "-1";
            }
            else
            {
                vRadicadoDenuncia = this.txtRadicadoDenuncia.Text;
            }

            try
            {
                if (this.FechaDesde.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblCampoRequeridoFechaDesde.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblCampoRequeridoFechaDesde.Visible = false;
                    vEsrequerido = false;
                }

                if (this.FechaDesde.Date > DateTime.Today.Date)
                {
                    this.lblErrorFechaDesde.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblErrorFechaDesde.Visible = false;
                }

                if (this.FechaHasta.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblCampoRequeridoFechaHasta.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblCampoRequeridoFechaHasta.Visible = false;
                    vEsrequerido = false;
                }

                if (this.FechaHasta.Date > DateTime.Today.Date)
                {
                    this.lblErrorFechaHasta.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblErrorFechaHasta.Visible = false;
                }

                if ((this.FechaDesde.Date > this.FechaHasta.Date) && (this.FechaDesde.Date != Convert.ToDateTime("1/01/1900")))
                {
                    this.lblErrorFechaDesde.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblErrorFechaDesde.Visible = false;
                }
            }
            catch (Exception)
            {
                this.toolBar.LipiarMensajeError();
                return;
            }

            bool vEsAbogado = this.ConsultarRol("ABOGADO");
            bool vEsAdministrador = this.ConsultarRol("ADMINISTRADOR");
            bool vEsCoordinador = this.ConsultarRol("COORDINADOR JURIDICO");
            int vIdAbogado = 0;
            if (!vEsAdministrador && !vEsAdministrador && vEsAbogado)
            {
                vIdAbogado = this.vMostrencosService.ConsultarAbogadosPorIdUsuario(this.GetSessionUser().IdUsuario).IdAbogado;
            }

            List<DenunciaBien> vListaDenunciaBien = this.vMostrencosService.ConsultarDenunciaBien(Convert.ToInt32(this.GetSessionUser().IdRegional), Convert.ToInt32(this.ddlEstado.Text), FechaDesde.Date.ToString("yyyy-MM-dd"), FechaHasta.Date.ToString("yyyy-MM-dd"), vRadicadoDenuncia, vIdAbogado);
            this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"] = vListaDenunciaBien;
            this.gvRegistrarDocumentosBienDenunciado.Visible = true;
            this.gvRegistrarDocumentosBienDenunciado.DataSource = vListaDenunciaBien;
            this.gvRegistrarDocumentosBienDenunciado.DataBind();


        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            this.gvRegistrarDocumentosBienDenunciado.Visible = false;
        }
    }

    private bool ConsultarRol(string pRol)
    {
        List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.vMostrencosService.ConsultarRolesPorProgramaFuncion("Documentar denuncia", pRol);
        string[] vRoles = this.GetSessionUser().Rol.Split(';');
        foreach (string item in vRoles)
        {
            if (vListaRoles.Where(p => p.NombreRol.ToUpper() == item.ToUpper()).Count() > 0)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Establece los botones y datos iniciales de la entidad, tales como : Nomre de la regional y Estado REGISTRADo de la denuncia
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.EstablecerTitulos("Documentar denuncia");
            this.gvRegistrarDocumentosBienDenunciado.PageSize = PageSize();
            this.gvRegistrarDocumentosBienDenunciado.EmptyDataText = EmptyDataText();
            this.FechaDesde.HabilitarObligatoriedad(false);
            this.FechaHasta.HabilitarObligatoriedad(false);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// guarda datos com: idDenuncia,Numero de radicado denuncia y fecha de denuncia para ser lelvados al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvRegistrarDocumentosBienDenunciado.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", strValue);
            this.SetSessionParameter("DocumentarBienDenunciado.Guardado", "0");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvRegistrarDocumentosBienDenunciado_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvRegistrarDocumentosBienDenunciado.SelectedRow);
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvRegistrarDocumentosBienDenunciado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvRegistrarDocumentosBienDenunciado.PageIndex = e.NewPageIndex;
        this.gvRegistrarDocumentosBienDenunciado.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"] != null)
        {
            List<DenunciaBien> vList = (List<DenunciaBien>)this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"];
            this.gvRegistrarDocumentosBienDenunciado.DataSource = vList;
            this.gvRegistrarDocumentosBienDenunciado.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (this.GetSessionUser().IdRegional == null)
            {
                this.toolBar.MostrarMensajeError("El usuario no pertenece a ninguna regional, por favor revisar");
            }
            else
            {
                Regional vRegional = vMostrencosService.ConsultarRegionalDelAbogado(Convert.ToInt32(this.GetSessionUser().IdRegional));
                this.txtDepartamento.Text = vRegional.NombreRegional;
            }

            this.ddlEstado.DataSource = this.vMostrencosService.ConsultarEstadosDenuncia();
            this.ddlEstado.DataTextField = "NombreEstadoDenuncia";
            this.ddlEstado.DataValueField = "IdEstadoDenuncia";
            this.ddlEstado.DataBind();
            this.ddlEstado.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvRegistrarDocumentosBienDenunciado_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DenunciaBien> vList = new List<DenunciaBien>();
        List<DenunciaBien> vResult = new List<DenunciaBien>();

        if (this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"] != null)
        {
            vList = (List<DenunciaBien>)this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"];
        }

        switch (e.SortExpression)
        {
            case "RadicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.RadicadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.RadicadoDenuncia).ToList();
                }

                break;

            case "NombreTipoIdentificacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Tercero.TiposDocumentosGlobal.NomTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Tercero.TiposDocumentosGlobal.NomTipoDocumento).ToList();
                }

                break;

            case "NUMEROIDENTIFICACION":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Tercero.NumeroIdentificacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Tercero.NumeroIdentificacion).ToList();
                }

                break;

            case "NombreMostrar":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreMostrar).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreMostrar).ToList();
                }

                break;

            case "FechaRadicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaRadicadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaRadicadoDenuncia).ToList();
                }

                break;

            case "NombreRegionalUbicacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Regional.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Regional.NombreRegional).ToList();
                }

                break;

            case "NombreEstadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }

                break;

            case "NombreAbogado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Abogados.NombreAbogado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Abogados.NombreAbogado).ToList();
                }

                break;

            case "FechaAsignacionAbogado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Abogados.FechaAsignacionAbogado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Abogados.FechaAsignacionAbogado).ToList();
                }

                break;

            case "FechaEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaEstado).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"] = vResult;
        this.gvRegistrarDocumentosBienDenunciado.DataSource = vResult;
        this.gvRegistrarDocumentosBienDenunciado.DataBind();
    }

    protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strValue = ddlEstado.SelectedValue;
        this.SetSessionParameter("Denuncia.IdEstadoDenuncia", strValue);
    }
}