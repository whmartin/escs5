﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_RegistrarDocumentosBienDenunciado_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
         .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            background-color: #808080;
            /*border-width: 3px;
            border-style: solid;
            border-color: black;*/
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 520px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .OcultarTerceros_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>

    <asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la denuncia *</td>
                <td>Fecha de radicado de la denuncia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en correspondencia *</td>
                <td>Fecha radicado en correspondencia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *</td>
                <td>Número de identificación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45.5%">Primer nombre *</td>
                <td style="width: 60%">Segundo nombre *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45.5%">Primer apellido *</td>
                <td style="width: 60%">Segundo apellido *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo requerido"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr class="rowB">
                <td>Valor de la Denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="TextValordelaDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelClaseDenuncia">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="tdTitulos">Clase de denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowB">
                <td>Clase de esta denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButton ID="rdbVacante" runat="server" GroupName="ClaseDenuncia" Text="Vacante" Enabled="false" />
                    <asp:RadioButton ID="rdbMostrenco" runat="server" GroupName="ClaseDenuncia" Text="Mostrenco" Enabled="false" />
                    <asp:RadioButton ID="rdbVocacion" runat="server" GroupName="ClaseDenuncia" Text="Vocación hereditaria" Enabled="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="tdTitulos">Documentación de la denuncia
                </td>
            </tr>
            <tr class="rowB">
                <td></td>
            </tr>
            <tr class="rowB">
                <td>¿Documentación completa?
                    <asp:RadioButtonList runat="server" ID="rblDocumentacionCompleta" RepeatLayout="Table" RepeatColumns="2" Width="10%" 
                                AutoPostBack="true">
                                <asp:ListItem Value="1" Text="Si" />
                                <asp:ListItem Value="0" Text="No" />
                   </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:GridView runat="server" ID="grvDocumentacionDenuncia" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvDocumentacionDenuncia_OnPageIndexChanging"
                        GridLines="None" Width="100%" ShowHeader="false" Visible="true" ItemStyle-CssClass="descripcionGrilla" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:BoundField HeaderText="" DataField="NombreTipoDocumento" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="lnkArchivoDescargar" Enabled="false" CommandName="Descargar" Text='<%# Eval("NombreArchivo") %>' NavigateUrl='<%# "../RegistroDenuncia/Archivos/" + Eval("RutaArchivo") %>' Target="_blank"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowBG">
                <td>
                    <asp:GridView runat="server" ID="grvOtrosDocumentos" AutoGenerateColumns="False" OnPageIndexChanging="grvOtrosDocumentos_OnPageIndexChanging"
                        GridLines="None" Width="100%" Visible="true" CellPadding="0" Height="16px" AllowPaging="True" PageSize="5">
                        <Columns>
                            <asp:BoundField HeaderText="Observaciones del documento" DataField="ObservacionesDocumento" />
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" ID="lnkArchivoDescargar" Enabled="false" CommandName="Descargar" Text='<%# Eval("NombreArchivo") %>' NavigateUrl='<%# "../RegistroDenuncia/Archivos/" + Eval("RutaArchivo") %>' Target="_blank"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación solicitada
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Documento soporte de la denuncia *
                </td>
                <td>Fecha de solicitud *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="false"
                        DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                    </asp:DropDownList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaSolicitud" Requerid="true" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Observaciones al documento solicitado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Nombre Entidad a Oficiar&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="btnBuscarDetalleSolicitado" runat="server" AutoPostBack="true" ImageUrl="~/Image/btn/list.png" 
                                Visible="true" Enabled="false" Height="22px" Width="22px" />
                </td>
                <td>Fecha de Oficio *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtEntidadOficiarSolicitado" runat="server" Enabled="false" Height="24px" Width="247px"></asp:TextBox>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="fecFechaOficioSolicitado" Requerid="true" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Asunto del Oficio</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtAsuntoOficioSolicitado" runat="server" Enabled="false" Height="24px" Width="247px"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr class="rowB">
                <td>Resumen Ejecutivo del Oficio</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtResumenEjecutivoSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" Enable="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <cc1:ModalPopupExtender ID="modalPopupInformacionTerceros" runat="server" PopupControlID="pnlConsultarInformacionTerceros" TargetControlID="btnBuscarDetalleSolicitado"
        CancelControlID="btnCerrarBusqueda" BackgroundCssClass="Background">
    </cc1:ModalPopupExtender>

    <asp:Panel ID="pnlConsultarInformacionTerceros" runat="server" CssClass="Popup" align="center" Style="display: block">
        <table width="90%" align="center" style="background-color: white; color: black; margin: auto;">


            <tr class="rowB">
                <td class="tdTitulos" colspan="6" style="text-align: left;">Busqueda Entidades                            
                            <asp:ImageButton ID="btnCerrarBusqueda" runat="server" ImageUrl="~/Image/btn/close.png" Visible="true" Height="23px" Width="23px" class="close alin align-content:center" />
                </td>
            </tr>

            <tr class="rowA">
                <td>&nbsp; Nombre Entidad *
                             <asp:Label ID="lblCampoRequeridoNombreEntidad" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                </td>
                <td></td>
            </tr>
            <tr class="rowB">
                <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbNombreEntidad" Enabled="true"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="btnConsultarVentanaModal" runat="server" ImageUrl="~/Image/btn/list.png" Visible="true"
                        AutoPostBack="true" Height="22px" Width="22px" OnClick="btnConsultarVentanaModal_Click" />
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:GridView runat="server" ID="grvBusquedaTerceros" AutoGenerateColumns="False"
                        GridLines="None" Width="100%" DataKeyNames="IDTERCERO" CellPadding="0" Height="16px" AllowSorting="True" AllowPaging="true" 
                        PageSize="10" OnPageIndexChanging="grvBusquedaTerceros_PageIndexChanged" OnSelectedIndexChanged="grvBusquedaTerceros_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre Entidad" DataField="RazonSocial" />
                            <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                            <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                            <asp:BoundField HeaderText="Dirección" DataField="Direccion" />
                        </Columns>
                        <asp:EmptyDataTemplate>
                            No se encontraron datos, verifique por favor  
                        </asp:EmptyDataTemplate>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>

        </table>

    </asp:Panel>


    <asp:Panel runat="server" ID="Panel1">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td style="width: 50%">Estado del documento *
                </td>
                <td>Fecha de recibido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButton ID="rbtAceptado" runat="server" GroupName="grpEstadoRecibido" Text="Aceptado" Enabled="false" />
                    <asp:RadioButton ID="rbtDevuelto" runat="server" GroupName="grpEstadoRecibido" Text="Devuelto" Enabled="false" />
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FecharRecibido" Requerid="true" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Nombre de archivo *
                </td>
                <td>¿Documentación completa? *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:FileUpload ID="fulArchivoRecibido" runat="server" Enabled="false" />
                </td>
                <td>
                    <asp:RadioButton ID="rbtSi" runat="server" GroupName="grpDocumentacionCompleta" Text="Si" Enabled="false" />
                    <asp:RadioButton ID="rbtNo" runat="server" GroupName="grpDocumentacionCompleta" Text="No" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Observaciones al documento recibido
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesRecibido" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
           

            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
        </table>
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                        OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging" AllowSorting="True"
                        OnSorting="gvwDocumentacionRecibida_OnSorting" GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" SortExpression="FechaRecibidoGrilla" />
                            <asp:BoundField HeaderText="Nombre archivo" DataField="NombreArchivo" ItemStyle-CssClass="descripcionGrilla" SortExpression="NombreArchivo" />
                            <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla" />
                            <asp:BoundField DataField="Tercero.IdTercero" SortExpression="IdTercero" ItemStyle-CssClass="Ocultar_" />
                            <asp:BoundField HeaderText="Nombre Entidad a Oficiar" DataField="Tercero.RazonSocial" SortExpression="RazonSocial" />
                            <asp:BoundField HeaderText="Fecha de Oficio" DataField="FechaOficio" SortExpression="FechaOficio" />
                            <asp:BoundField HeaderText="Asunto del Oficio" DataField="AsuntoOficio" SortExpression="AsuntoOficio" ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField HeaderText="Resumen Ejecutivo del Oficio" DataField="ResumenOficio" SortExpression="ResumenOficio" ItemStyle-CssClass="descripcionGrilla"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel2">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="tdTitulos" colspan="2">Histórico documentación recibida
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvwHistorico" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvwHistorico_OnPageIndexChanging"
                        GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" OnRowCommand="gvwHistorico_RowCommand" CellPadding="0" Height="16px"
                        AllowSorting="True" OnSorting="gvwHistorico_OnSorting">
                        <Columns>
                            <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" DataFormatString="{0:d}" SortExpression="FechaSolicitud" />
                            <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                            <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" DataFormatString="{0:d}" SortExpression="FechaRecibidoGrilla" />
                            <%--<asp:BoundField HeaderText="Fecha de recibido" DataFormatString="{0:dd/MM/yyyy}" DataField="FechaRecibido" />--%>
                            <%--<asp:BoundField HeaderText="Nombre archivo" DataField="NombreArchivo" SortExpression="NombreArchivo" />--%>
                            <asp:TemplateField HeaderText="Nombre archivo" ItemStyle-CssClass="descripcionGrilla" SortExpression="NombreArchivo">
                                <ItemTemplate>
                                    <asp:Button runat="server" CssClass="lnkArchivoDescargar descripcionGrilla" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlArchivo" CssClass="popuphIstorico hidden" Width="90%" ScrollBars="None" 
        Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; 
        top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;" BackgroundCssClass="Background"
        BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
        <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
            <tr>
                <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                    <div>
                        <div class="col-md-5 align-center">
                            <h4>
                                <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                            </h4>
                        </div>
                        <div class="col-md-5 align-center">
                            <asp:HiddenField ID="hfIdArchivo" runat="server" />
                            <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                        </div>
                        <div class="col-md-1 align-center">
                            <a class="btnCerrarPop" style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative;">
                                <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png"></img>
                            </a>
                        </div>
                    </div>
            </tr>
            <tr style="padding: 5px;">
                <td style="text-align: center;">
                    <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                        <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                        <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.btnCerrarPop', function () {
                $('.popuphIstorico').addClass('hidden');
            });
        });
    </script>
</asp:Content>


