﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="add.aspx.cs" Inherits="Page_Mostrencos_RegistrarDocumentosBienDenunciado_add" %>

<%--<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>--%>
<%@ Register Src="~/General/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register Src="~/General/General/Control/fechaEdit.ascx" TagPrefix="uc1" TagName="fechaSN" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Popup {
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 350px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
        }
         .descripcionGrilla {
        word-break: break-all;
        }
        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .OcultarTerceros_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }
    </style>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <asp:HiddenField ID="hfCorreo" runat="server"></asp:HiddenField>
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado de la denuncia *</td>
                        <td>Fecha de radicado de la denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en correspondencia *</td>
                        <td>Fecha radicado en correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Primer nombre *</td>
                        <td style="width: 55%">Segundo nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 45%">Primer apellido *</td>
                        <td style="width: 55%">Segundo apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="Panel3">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                            <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtDescripcion"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                    <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                        <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" class="Background" />
                    </a>
                        </td>
                        <caption>
                        </caption>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" MaxLength="512" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Valor de la Denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextValordelaDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelClaseDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos">Clase de denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>Clase de esta denuncia *
               <asp:RequiredFieldValidator ID="rfvV" ControlToValidate="rblClaseDenuncia"
                   ValidationGroup="btnGuardar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblClaseDenuncia" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Vacante</asp:ListItem>
                                <asp:ListItem Value="2">Mostrenco</asp:ListItem>
                                <asp:ListItem Value="3">Vocación hereditaria</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos">Documentación de la denuncia
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>¿Documentación completa? 
                            <asp:RadioButtonList runat="server" ID="rblDocumentacionCompleta" RepeatLayout="Table" RepeatColumns="2" Width="10%" 
                                OnSelectedIndexChanged="rblDocumentacionCompleta_SelectedIndexChanged1" AutoPostBack="true">
                                <asp:ListItem Value="1" Text="Si" />
                                <asp:ListItem Value="0" Text="No" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="grvDocumentacionDenuncia" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" ShowHeader="false" Visible="true" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px">
                                <Columns>
                                    <asp:BoundField HeaderText="" DataField="NombreTipoDocumento" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkArchivoDescargar" Text='<%# Eval("NombreArchivo") %>' NavigateUrl='<%# "../RegistroDenuncia/Archivos/"  + Eval("RutaArchivo") %>' Target="_blank"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="rowBG">
                        <td>
                            <asp:GridView runat="server" ID="grvOtrosDocumentos" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grvOtrosDocumentos_OnPageIndexChanging"
                                GridLines="None" Width="100%" Visible="true" CellPadding="0" Height="16px" PageSize="5">
                                <Columns>
                                    <asp:BoundField HeaderText="Observaciones del documento" DataField="ObservacionesDocumento" />
                                    <asp:TemplateField HeaderText="Documento">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkArchivoDescargar" Enabled="true" CommandName="Descargar" Text='<%# Eval("NombreArchivo") %>' NavigateUrl='<%# "../RegistroDenuncia/Archivos/" + GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien").ToString() + "/" +  Eval("NombreArchivo") %>' Target="_blank"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Documento soporte de la denuncia *
                    <asp:Label runat="server" ID="lblReqTipoDocumentoSolicitado" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                        <td colspan="2">Fecha de solicitud *
                     <asp:Label runat="server" ID="lblReqFechaSolicitud" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>

                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlTipoDocumento" runat="server"
                                DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="FechaSolicitud" Enabled="false"></asp:TextBox>
                            <%--<uc1:fecha runat="server" ID="FechaSolicitud"/>--%>                   
                        </td>
                        <td>
                            <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Image/btn/add.gif" OnClick="btnAdd_Click" />
                            <asp:ImageButton ID="btnAplicar" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicar_Click" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento solicitado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtObservacionesSolicitado" TextMode="MultiLine" MaxLength="512" Rows="8" onkeypress="return CheckLength();" 
                                Width="600px" Height="100" Style="resize: none"></asp:TextBox>

                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                TargetControlID="txtObservacionesSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Nombre Entidad a Oficiar&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="btnBuscarDetalleSolicitado" runat="server" ImageUrl="~/Image/btn/list.png" AutoPostBack="true" Visible="true" Enabled="false" Height="22px" Width="22px" />
                        </td>
                        <td>
                            Fecha de Oficio *
                            <asp:Label ID="fcInvSoli" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox ID="txtEntidadOficiarSolicitado" runat="server" Enabled="false" Height="24px" Width="247px" MaxLength="300"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="vlfecFechaOficioSolicitado" runat="server" Text="Fecha Inválida" ForeColor="Red" Visible="false"></asp:Label>
                            <br />
                            <uc1:fechaSN runat="server" ID="fecFechaOficioSolicitado" Requerid="true" Enabled="true" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Asunto del Oficio</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox ID="txtAsuntoOficioSolicitado" runat="server" Enabled="true" Height="24px" Width="247px" MaxLength="100"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAsuntoOficioSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="rowB">
                        <td>Resumen Ejecutivo del Oficio</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtResumenEjecutivoSolicitado" Enabled="true" TextMode="MultiLine" MaxLength="256"
                                Rows="8" Width="600px" Height="100" Style="resize: none" Enable="false" onkeyup="return CheckLength();" onkeypress="return CheckLength();"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtResumenEjecutivoSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <cc1:ModalPopupExtender ID="modalPopupInformacionTerceros" runat="server" PopupControlID="pnlConsultarInformacionTerceros" TargetControlID="btnBuscarDetalleSolicitado"
                CancelControlID="btnCerrarBusqueda">
            </cc1:ModalPopupExtender>

            <asp:Panel ID="pnlConsultarInformacionTerceros" runat="server" CssClass="Popup" align="center" Style="display: block">
                <table width="100%" align="center" style="background-color: white; color: black; margin: auto;">
                    <tr>
                        <td colspan="6" style="text-align: left;">Búsqueda Entidades      
                   <asp:ImageButton ID="btnCerrarBusqueda" runat="server" ImageUrl="~/Image/btn/close.png" Visible="true" Height="23px"
                       Width="23px" class="close alin align-content:center" />
                            <hr size="3px" color="black">
                        </td>

                    </tr>

                    <tr>
                        <td>&nbsp; Nombre Entidad *</td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;
                    <asp:TextBox runat="server" ID="txbNombreEntidad" Enabled="true" Requerid="true" MaxLength="300" Width="300px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredtxbNombreEntidad" runat="server"
                                TargetControlID="txbNombreEntidad" FilterType="LowercaseLetters,UppercaseLetters" ValidChars=" " />
                            <asp:RequiredFieldValidator ID="rqvNombreEntidad" runat="server" ControlToValidate="txbNombreEntidad" ErrorMessage="Campo Requerido" ForeColor="Red" ValidationGroup="Tercero">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnConsultarVentanaModal" runat="server" ImageUrl="~/Image/btn/list.png" Visible="true"
                                AutoPostBack="true" Height="22px" Width="22px" OnClick="btnConsultarVentanaModal_Click" ValidationGroup="Tercero" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView runat="server" ID="grvBusquedaTerceros" AutoGenerateColumns="False"
                                    GridLines="None" Width="100%" DataKeyNames="IDTERCERO" CellPadding="0" Height="16px" AllowSorting="True" AllowPaging="True"
                                    OnPageIndexChanging="grvBusquedaTerceros_PageIndexChanged" OnSelectedIndexChanged="grvBusquedaTerceros_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                                    Height="16px" Width="16px" ToolTip="Seleccionar" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre Entidad">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRazonSocial" runat="server" Text='<%# Bind("RazonSocial") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Departamento">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepartamento" runat="server" Text='<%# Bind("Departamento") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Municipio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMunicipio" runat="server" Text='<%# Bind("Municipio") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dirección">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDireccion" runat="server" Text='<%# Bind("Direccion") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <asp:EmptyDataTemplate>
                                        No se encontraron datos, verifique por favor  
                                    </asp:EmptyDataTemplate>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>

                </table>

            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="2">Documentación recibida
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 50%">Estado del documento *
                    <asp:Label runat="server" ID="lblrqfEstadoRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                        <td>Fecha de recibido *                
                    <asp:Label runat="server" ID="lblFechaInvalida" Visible="false" Text="Fecha Inválida" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblrqfFechaRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaMaxima" Visible="false" Text="La fecha seleccionada no debe ser posterior a la fecha actual. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaSolicitud" Visible="false" Text="La fecha seleccionada no debe ser anterior a la fecha de solicitud. Por favor revisar" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal">
                                <asp:ListItem Value="3">Aceptado</asp:ListItem>
                                <asp:ListItem Value="5">Devuelto</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaRecibido" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Nombre de archivo * 
                    <asp:Label runat="server" ID="lblRqfNombreArchivo" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                        <td>¿Documentación completa? *
                    <asp:Label runat="server" ID="lblrqfDocumentacionCompleta" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:FileUpload ID="fulArchivoRecibido" runat="server" />
                            <asp:UpdatePanel ID="UpnlDocumento" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAplicar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtDocumentacionCompleta" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento recibido
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" 
                                Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />

                        </td>
                    </tr>

                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" OnPageIndexChanging="gvwDocumentacionRecibida_OnPageIndexChanging"
                                GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px" AllowPaging="True" PageSize="10"
                                OnRowCommand="gvwDocumentacionRecibida_RowCommand" AllowSorting="True" OnSorting="gvwDocumentacionRecibida_OnSorting" Enabled="false" onrowdatabound="CustomersGridView_RowDataBound" >
                                <Columns>
                                    <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" SortExpression="FechaSolicitud" DataFormatString="{0:d}" />
                                    <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla"/>
                                    <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" SortExpression="FechaRecibidoGrilla" DataFormatString="{0:d}" />
                                    <asp:BoundField HeaderText="Nombre archivo" DataField="NombreArchivo" SortExpression="NombreArchivo" />
                                    <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" />

                                    <asp:BoundField DataField="Tercero.IdTercero" SortExpression="Tercero.IdTercero" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Administracion" DataField="Tercero.IdTercero" SortExpression="Tercero" />
                                    <asp:BoundField HeaderText="Nombre Entidad a Oficiar" DataField="Tercero.RazonSocial" SortExpression="RazonSocial" />
                                    <asp:BoundField HeaderText="Fecha del Oficio" DataField="FechaOficio" SortExpression="FechaOficio" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField HeaderText="Asunto del Oficio" DataField="AsuntoOficio" SortExpression="AsuntoOficio" ItemStyle-CssClass="descripcionGrilla"/>
                                    <asp:BoundField HeaderText="Resumen Ejecutivo del Oficio" DataField="ResumenOficio" SortExpression="ResumenOficio" ItemStyle-CssClass="descripcionGrilla"/>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" Enabled="true" ImageUrl="~/Image/btn/delete.gif"
                                                Height="16px" Width="16px" ToolTip="Eliminar" OnClientClick="return confirm('¿Está seguro de eliminar la información?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" ImageUrl="~/Image/btn/edit.gif"
                                                Height="16px" Width="16px" ToolTip="Editar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlHistorico">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="2">Histórico documentación recibida
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="gvwHistorico" AutoGenerateColumns="False" OnPageIndexChanging="gvwHistorico_OnPageIndexChanging"
                                GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvwHistorico_OnSorting">
                                <Columns>
                                    <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitudGrilla" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="FechaSolicitud" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibidoGrilla" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="FechaRecibidoGrilla" />
                                    <asp:BoundField HeaderText="Nombre archivo" DataField="NombreArchivo" SortExpression="NombreArchivo" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlArchivo" CssClass="popupFile hidden" Width="1000px" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 7760px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                            </div>
                            <div>
                                <a class="btnCerrarPopFile" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;</div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" width="100%" height="100%"></iframe>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div style="display: none;">
                <asp:Button ID="ButtonRpt" runat="server" Text="" />
            </div>

            <asp:Panel ID="pnlPopUpHistorico" CssClass="popuphIstorico hidden" runat="server" Style="background-color: White; border-color: White; border-width: 2px; 
                border-style: Solid; position: fixed; z-index: 999999; top: 25%; left: 15%; width: 800px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width:800px;">
                    <tr>
                        <td style="background-color: #FFFFFF; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <a class="btnCerrarPop" style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;">
                                    <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png">
                                </a>
                     &nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView ID="gvwHistoricoDenuncia" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False"
                                    AllowPaging="true" AllowSorting="true" GridLines="None"
                                    OnSorting="gvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="gvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                        <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <script type="text/javascript">
                $(document).ready(function () {

                    $(document).on('click', '.btnPopUpHistorico', function () {
                        $('.popuphIstorico').removeClass('hidden');
                    });
                    $(document).on('click', '.btnCerrarPop', function () {
                        $('.popuphIstorico').addClass('hidden');
                    });

                    $(document).on('click', '.btnCerrarPopFile', function () {
                        $('.popupFile').addClass('hidden');
                    });


                    $('#cphCont_txtResumenEjecutivoSolicitado').on('input', function (e) {
                        if (document.getElementById("cphCont_txtResumenEjecutivoSolicitado").value.length > 256) {
                            this.value = document.getElementById("cphCont_txtResumenEjecutivoSolicitado").value.substr(0, 256);
                        }
                    });

                    $('#cphCont_txtObservacionesRecibido').on('input', function (e) {
                        if (document.getElementById("cphCont_txtObservacionesRecibido").value.length > 512) {
                            this.value = document.getElementById("cphCont_txtObservacionesRecibido").value.substr(0, 512);
                        }
                    });
                });
                function CheckLength() {
                    var textbox = document.getElementById("cphCont_txtResumenEjecutivoSolicitado").value;
                    if (textbox.trim().length >= 256) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                function validateFechaInvalida(source, arguments) {
                    var EnteredDate = arguments.Value;
                    var fechaResolucionA = EnteredDate.split('/');
                    if (parseInt(fechaResolucionA[2]) < 1900 || fechaResolucionA[2] == '0000') {
                        arguments.IsValid = false;
                    }
                    else {
                        arguments.IsValid = true;
                    }
                }
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }
    </style>
</asp:Content>
