﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarDocumentosBienDenunciado_Detail : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarDocumentosBienDenunciado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlArchivo.CssClass = "popuphIstorico hidden";
        DeshabilitarCamposTercero();
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));

        SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!string.IsNullOrEmpty(GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString()) &&
                GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
            {
                toolBar.OcultarBotonBuscar(true);
                toolBar.OcultarBotonAdjuntar(true);
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(System.Web.UI.Page), "Script", "$(document).ready(function(){$('#btnEditar').addClass('hidden');})", true);


            }
            else if (!string.IsNullOrEmpty(GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString()) &&
                GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "0")
            {
                toolBar.OcultarBotonBuscar(false);
                toolBar.OcultarBotonAdjuntar(false);

            }

            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Evento Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);

    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAdjuntar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        SetSessionParameter("DocumentarBienDenunciado.Evento", "Adjuntar");
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwHistorico_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Ver"))
        {
            this.btnDescargar.Visible = false;
            bool vVerArchivo = false;
            string vRuta = string.Empty;
            int vRow = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwHistorico.DataKeys[vRow].Value);
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdDocumentoSolicitado);
            if (vArchivo.IdApoderado == null || vArchivo.IdCausante == null)
            {
                if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
                {
                    vVerArchivo = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
                    vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
                }
                else
                {
                    vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
                    vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
                    }
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarCalidadDenunciante/" + vArchivo.RutaArchivo;
                    }
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../RegistrarCalidadDenunciante/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../RegistrarCalidadDenunciante/Archivos/" + vArchivo.RutaArchivo;
                    }
                    if (!vVerArchivo)
                    {
                        vVerArchivo = File.Exists(Server.MapPath("../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo));
                        vRuta = "../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo;
                    }
                }

                if (vVerArchivo)
                {
                    this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                    if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                    {
                        this.ifmVerAdchivo.Visible = true;
                        this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                        this.imgDodumento.Visible = false;
                        this.imgDodumento.ImageUrl = string.Empty;
                        this.btnDescargar.Visible = true;
                    }
                    else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                    {
                        this.ifmVerAdchivo.Visible = false;
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.imgDodumento.Visible = true;
                        this.imgDodumento.ImageUrl = vRuta;
                        this.btnDescargar.Visible = true;
                    }
                }

                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.pnlArchivo.CssClass = "popuphIstorico";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
    }

    /// <summary>
    /// Grila Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvDocumentacionDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvDocumentacionDenuncia.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgrvDocumentacionDenuncia"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgrvDocumentacionDenuncia"];
            grvDocumentacionDenuncia.DataSource = vList;
            grvDocumentacionDenuncia.DataBind();
        }
        else
        {
            ViewState["SortedgrvDocumentacionDenuncia"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.grvDocumentacionDenuncia.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
            this.grvDocumentacionDenuncia.DataBind();
        }
    }

    /// <summary>
    /// descarga el archivo que se esta visualizando
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        bool vDescargar = false;
        string vRuta = string.Empty;
        int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
        if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
        {
            vDescargar = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
            vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
        }
        else
        {
            vDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
            if (!vDescargar)
            {
                vDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
            }
        }

        if (vDescargar)
        {
            string path = Server.MapPath(vRuta);
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Response.TransmitFile(path);
            Response.End();

        }

        this.pnlArchivo.CssClass = "popuphIstorico";
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Evento Lista desplegable Ir a
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void ddlExtends_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            switch (((DropDownList)sender).Text)
            {
                case "TrasladarDenuncia":
                    this.SetSessionParameter("Mostrencos.TrasladarDenuncia.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../TrasladarDenuncia/ListTraslado.aspx", false);
                    break;

                case "InformacionCausante":
                    this.SetSessionParameter("Mostrencos.RegistarInformacionCausante.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistarInformacionCausante/List.aspx", false);
                    break;

                case "ReistroBienesDenunciado":
                    this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistrarBienesDenunciados/List.aspx", false);
                    break;

                case "VerificarDenunciaAnterior":
                    this.SetSessionParameter("Mostrencos.VerificarDenunciaAnterior.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../ConsultarDenuncia/List.aspx", false);
                    break;

                case "RegistrarCalidadDenunciante":
                    this.SetSessionParameter("Mostrencos.RegistrarCalidadDenunciante.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistrarCalidadDenunciante/List.aspx", false);
                    break;

                case "AnularDenuncia":
                    this.SetSessionParameter("Mostrencos.AnularDenuncia.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../AnularDenuncia/List.aspx", false);
                    break;

                case "Contrato":
                    this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien", vIdDenunciaBien);
                    this.GetSessionParameter("Denuncia.IdEstadoDenuncia");
                    this.NavigateTo("../ContratoParticipacionEconomica/List.aspx", false);
                    break;

                case "ReasignarAbogado":
                    this.SetSessionParameter("Mostrencos.ReasignarAbogado.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../ReasignarAbogado/ListReasignacion.aspx", false);
                    break;

                case "InformacionApoderado":
                    this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistrarInformacionApoderado/List.aspx", false);
                    break;

                case "InformeDenunciante":
                    this.SetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../InformeDenunciante/List.aspx", false);
                    break;

                case "Continuar":
                    this.SetSessionParameter("Mostrencos.ContinuardeOficio.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../ContinuardeOficio/Detail.aspx", false);
                    break;

                case "RegistrarParticipacionEconomica":
                    this.SetSessionParameter("Mostrencos.RegistrarParticipacionEconomica.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistrarParticipacionEconomica/List.aspx", false);
                    break;

                case "RegistrarDestinacionBienes":
                        this.SetSessionParameter("Mostrencos.RegistrarDestinacionBienes.IdDenunciaBien", vIdDenunciaBien);
                        this.NavigateTo("../RegistrarDestinacionBienes/List.aspx", false);
                    break;

                case "TipoTramite":
                    if (this.validateTipoTramite(vIdDenunciaBien))
                    {
                        this.SetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien", vIdDenunciaBien);
                        this.NavigateTo("../RegistrarTipoTramite/List.aspx", false);
                    }
                    break;

                case "Protocolizacion":
                    this.SetSessionParameter("Mostrencos.Protocolizacion.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistrarProtocolizacion/List.aspx", false);
                    break;

                default:
                    this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
                    this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx", false);
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwHistorico_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwHistorico.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvwHistorico"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            gvwHistorico.DataSource = vList;
            gvwHistorico.DataBind();
        }
        else
        {
            ViewState["SortedgvwHistorico"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
            this.gvwHistorico.DataBind();
        }
    }

    protected void grvOtrosDocumentos_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvOtrosDocumentos.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgrvOtrosDocumentos"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgrvOtrosDocumentos"];
            grvOtrosDocumentos.DataSource = vList;
            grvOtrosDocumentos.DataBind();
        }
        else
        {
            ViewState["SortedgrvOtrosDocumentos"] = vMostrencosService.ConsultarOtrosDocumentos(vIdDenunciaBien, -1);
            this.grvOtrosDocumentos.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgrvOtrosDocumentos"];
            this.grvOtrosDocumentos.DataBind();
        }
    }

    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }

        if (e.SortExpression.Equals("NombreEstado"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreEstado).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaRecibidoGrilla"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreArchivo"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                    }
                }
                else
                {
                    if (e.SortExpression.Equals("ObservacionesDocumentacionRecibida"))
                    {
                        if (this.direction == SortDirection.Ascending)
                        {
                            ////Ascendente
                            vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                        }
                        else
                        {
                            ////Descendente
                            vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                        }
                    }
                }

                if (e.SortExpression.Equals("RazonSocial"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.Tercero.RazonSocial).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.Tercero.RazonSocial).ToList();
                    }
                }

                if (e.SortExpression.Equals("FechaOficio"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.FechaOficio).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.FechaOficio).ToList();
                    }
                }

                if (e.SortExpression.Equals("AsuntoOficio"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.AsuntoOficio).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.AsuntoOficio).ToList();
                    }
                }

                if (e.SortExpression.Equals("ResumenOficio"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.ResumenOficio).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.ResumenOficio).ToList();
                    }
                }

            }

        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    protected void gvwHistorico_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwHistorico"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwHistorico"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("NombreEstado"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        ////Ascendente
                        vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                    }
                    else
                    {
                        ////Descendente
                        vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                    }
                }
            }
        }

        if (e.SortExpression.Equals("FechaRecibidoGrilla"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                ////Ascendente
                vResult = vList.OrderBy(a => a.FechaRecibidoGrilla).ToList();
            }
            else
            {
                ////Descendente
                vResult = vList.OrderByDescending(a => a.FechaRecibidoGrilla).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("NombreArchivo"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
            }
        }

        if (this.direction == SortDirection.Ascending)
        {
            this.direction = SortDirection.Descending;
        }
        else
        {
            this.direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwHistorico"] = vResult;
        this.gvwHistorico.DataSource = vResult;
        this.gvwHistorico.DataBind();
    }
    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            this.toolBar.eventoCambiarOpcion += new ToolBarDelegate(this.ddlExtends_SelectedIndexChanged);
            toolBar.EstablecerTitulos("Documentar denuncia");
            toolBar.eventoAdjuntar += btnAdjuntar_Click;
            toolBar.eventoBuscar += btnConsultar_Click;
            this.toolBar.LimpiarOpcionesAdicionales();
            foreach (ListItem item in this.CargarListaOpcionesIrA())
            {
                this.toolBar.AgregarOpcionAdicional(item);
            }

            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));

            this.grvDocumentacionDenuncia.PageSize = PageSize();
            this.grvDocumentacionDenuncia.EmptyDataText = EmptyDataText();
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
            this.gvwHistorico.PageSize = PageSize();
            this.gvwHistorico.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            this.rblDocumentacionCompleta.SelectedValue = "0";
            if (vIdDenunciaBien != 0)
            {

                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                string vMensaje = this.GetSessionParameter("Mostrencos.ConsultarDenunciaPrevia.Mensaje").ToString();
                if (!string.IsNullOrEmpty(vMensaje))
                {
                    this.toolBar.MostrarMensajeGuardado(vMensaje);
                    this.SetSessionParameter("Mostrencos.ConsultarDenunciaPrevia.Mensaje", null);
                }

                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

                if (vRegistroDenuncia.DocumentacionCompleta == true)
                {
                    this.rblDocumentacionCompleta.SelectedValue = "1";
                }
                else
                {
                    this.rblDocumentacionCompleta.SelectedValue = "0";
                }

                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));

                //Validar RadioButon de Documentación Completa
                var validaDocumentacion = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 1 && x.IdAccion == 2);

                var ControlTerceros = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdAccion == 2 && x.IdFase == 1 && x.IdActuacion == 6);


                if (ControlTerceros != null)
                {
                    //HabilitarCamposTercero();
                }

                this.rblDocumentacionCompleta.Enabled = false;

                //Calcular Suma Valor Denuncia
                List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(this.vIdDenunciaBien);

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;

                // Se asigna el valor de la denuncia
                if (vlstDenunciaBienTitulo.Count() > 0)
                {
                    //string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString()) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString() : "0,00";
                    string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                    this.TextValordelaDenuncia.Text = valorDenuncia;
                }
                else
                {
                    this.TextValordelaDenuncia.Text = "0";
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia.ToString();
                if (vRegistroDenuncia.ClaseDenuncia == "1")
                {
                    rdbVacante.Checked = true;
                }
                else if (vRegistroDenuncia.ClaseDenuncia == "2")
                {
                    rdbMostrenco.Checked = true;
                }
                else
                {
                    rdbVocacion.Checked = true;
                }

                ////Se llena la grilla con los documentos cuyo estado sea inicial (1) - Documentos de la denuncia 
                ViewState["SortedgrvDocumentacionDenuncia"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
                this.grvDocumentacionDenuncia.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 1);
                this.grvDocumentacionDenuncia.DataBind();

                TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
                vTipoDocumento.NombreTipoDocumento = "Seleccione";
                vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
                vlstTipoDocumentosBien.Add(vTipoDocumento);

                this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.ddlTipoDocumento.DataBind();

                //// Se llena la grilla con los documentos cuyo estado sea diferente al inicial y el eliminado (0)
                ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                this.gvwDocumentacionRecibida.DataBind();

                ViewState["SortedgrvOtrosDocumentos"] = vMostrencosService.ConsultarOtrosDocumentos(vIdDenunciaBien, -1);
                this.grvOtrosDocumentos.DataSource = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgrvOtrosDocumentos"];
                this.grvOtrosDocumentos.DataBind();

                ////Se llena la grilla con los todos documentos de la denuncia (Todos los estados)
                List<DocumentosSolicitadosDenunciaBien> vListaArchivos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, -1);
                ViewState["SortedgvwHistorico"] = vListaArchivos;
                this.gvwHistorico.DataSource = vListaArchivos;
                this.gvwHistorico.DataBind();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void HabilitarCamposTercero()
    {
        this.fecFechaOficioSolicitado.Enabled = true;
        this.btnBuscarDetalleSolicitado.Enabled = true;
        this.txtEntidadOficiarSolicitado.Enabled = true;
        this.txtAsuntoOficioSolicitado.Enabled = true;
        this.txtResumenEjecutivoSolicitado.Enabled = true;
    }

    public void DeshabilitarCamposTercero()
    {
        this.fecFechaOficioSolicitado.Enabled = false;
        this.txtEntidadOficiarSolicitado.Enabled = false;
        this.txtAsuntoOficioSolicitado.Enabled = false;
        this.txtResumenEjecutivoSolicitado.Enabled = false;
    }

    /// <summary>
    /// Metodo cargar Datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            ////Codgio para cargar datos 
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carla los datos que se van a mistrar en la opcion Ir a..
    /// </summary>
    /// <returns></returns>
    public ListItemCollection CargarListaOpcionesIrA()
    {
        List<Apoderados> vListApoderados = new List<Apoderados>();
        ListItemCollection vlista = new ListItemCollection();
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        //VALIDA EL TIPO DE PERSONA "JURIDICA"
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
        List<RegistroCalidadDenunciante> vRegistroCalidadDenunciante = vMostrencosService.ConsultarCalidadDenunciante(vIdDenunciaBien);
        RegistroCalidadDenuncianteCitacion vRegistroCalidadDenuncianteCitacion = new RegistroCalidadDenuncianteCitacion();
        if (vRegistroCalidadDenunciante.Count > 0)
        {
            vRegistroCalidadDenuncianteCitacion = vMostrencosService.ConsultarCalidadDenuncianteCitacion(vRegistroCalidadDenunciante[0].IdReconocimientoCalidadDenunciante);
        }
        var TipoPersona = vDenunciaBien.Tercero.NombreTipoPersona;

        ////Estado actual de la denuncia
        var estadoActualDenuncia = vDenunciaBien.EstadoDenuncia.IdEstadoDenuncia;

        //// VALIDA EL ESTADO DEL CONTRATO POR MEDIO DEL ID DE LA DENUNCIA
        List<ContratoParticipacionEconomica> vListaDeContratos = vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);

        bool ContratoDiferenteTerminado = false;
        if (vListaDeContratos.Count > 0)
        {
            if (vListaDeContratos[vListaDeContratos.Count - 1].EstadoContrato.Equals("Terminado"))
            {
                ContratoDiferenteTerminado = true;
            }
        }

        //var ContratoDiferenteTerminado = vListaDeContratos.FindLast(x => x.EstadoContrato.Equals("Terminado"));



        int ContratoAsociado = vListaDeContratos.Count();

        //TRAE EL TIPO DE ENTIDAD POR EL ID TEL TERCERO
        var ObjEntidad = this.vMostrencosService.ConsultarTipoEntidadPorIdTercero(vDenunciaBien.Tercero.IdTercero);

        vlista.Add(new ListItem("Registro de Bienes Denunciados", "ReistroBienesDenunciado"));
        vlista.Add(new ListItem("Información del Apoderado", "InformacionApoderado"));
        vlista.Add(new ListItem("Información del Causante", "InformacionCausante"));
        vlista.Add(new ListItem("Reasignar Abogado", "ReasignarAbogado"));
        vlista.Add(new ListItem("Registrar Calidad Denunciante", "RegistrarCalidadDenunciante"));
        vlista.Add(new ListItem("Contrato de Participación Económica", "Contrato"));
        vlista.Add(new ListItem("Continuar de Oficio", "Continuar"));
        vlista.Add(new ListItem("Definir Tipo de Tramite", "TipoTramite"));
        vlista.Add(new ListItem("Registrar Informe del Denunciante", "InformeDenunciante"));
        vlista.Add(new ListItem("Verificar Denuncia Anterior", "VerificarDenunciaAnterior"));
        vlista.Add(new ListItem("Protocolización", "Protocolizacion"));
        #region CU 154 - 192
        //// Se añade una nueva opción en la lista para el Sprint 7
        //if (this.ValidaFaseAccion(vIdDenunciaBien, 21, 41))
        //{
        //    vlista.Add(new ListItem("Participación Económica", "RegistrarParticipacionEconomica"));
        //}
        vlista.Add(new ListItem("Registrar Participación Económica", "RegistrarParticipacionEconomica"));
        if (this.ValidaFaseAccion(vIdDenunciaBien, 20, null))
        {
            vlista.Add(new ListItem("Registrar Destinación de Bienes", "RegistrarDestinacionBienes"));
        }

        #endregion
        vlista.Add(new ListItem("Anular Denuncia", "AnularDenuncia"));
        vlista.Add(new ListItem("Trasladar Denuncia", "TrasladarDenuncia"));


        //TODO: Freddy: Opciones de menu Ir a...

        var Contrato = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 6);
        var Calidad = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 1 && x.IdActuacion == 6 && x.IdAccion == 2);
        var CalidadDenunciante = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 3 && x.IdActuacion == 7 && x.IdAccion == 9);
        var ValidofaseCalidadDenunciante = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 3);
        var ContratoNoTerminado = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 4 && x.IdActuacion == 9 && x.IdAccion == 13);

        ////VALIDA LA ACCION, FASE Y ACTUACION PARA IR A "REGISTRAR INFORME DEL DENUNCIANTE"
        var InformeDenunciante = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 4 && x.IdActuacion == 9 && x.IdAccion == 13);

        ////Validación para "Registrar Informe del Denunciante"
        var ContratoParticipacionEnHistorico = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdActuacion == 9);

        //// Validación para la opción de "Tipo Tramite"
        var valTipoTramite = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdActuacion == 9 && x.IdFase == 4 && x.IdAccion == 13);
        var valTipoTramite2 = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdActuacion == 7 && x.IdFase == 3 && x.IdAccion == 9);
        vListApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(this.vIdDenunciaBien);
        var ApoderadosActivos = vListApoderados.Where(x => x.EstadoApoderado.Equals("ACTIVO")).ToList().Count() > 0;

        //// Validación en historico de la denuncia para "Continuar oficio"
        var continuarOficioVal = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdEstadoDenuncia == 13 || x.IdEstadoDenuncia == 11 || x.IdEstadoDenuncia == 19);

        var AnalisisJuridico = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 18);

        //Crear llave tramite judicial y notarial
        var TramiteJudicial = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 18 && x.IdActuacion == 24 && x.IdAccion == 36);
        var ValidaProtocolizacion = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 18);
        //var DocumentacionNoCompleta = vMostrencosService.ConsultarDocumentosXIdDenuncia(this.vIdDenunciaBien, -1).Find(j => j.DocumentacionCompleta == null);

        var DocumentacionNoCompleta = vRegistroDenuncia.DocumentacionCompleta;
        bool CalidadDenuncianteNoReconoce = vMostrencosService.ConsultarCalidadDenunciante(this.vIdDenunciaBien).Any(x => x.ReconocimientoCalidad == "No Reconoce");
        //var CalidadDenuncianteNoReconoce = vMostrencosService.ConsultarCalidadDenunciante(this.vIdDenunciaBien).Find(x => x.ReconocimientoCalidad == "No Reconoce");

        //// Validación "Continuar de Oficio"
        bool EsDenunciaAnterior = vDenunciaBien.ExisteDenuncia;

        if (vRegistroDenuncia.ClaseDenuncia == "1" || vRegistroDenuncia.ClaseDenuncia == "2")
        {
            vlista.Remove(new ListItem("Información del Causante", "InformacionCausante"));
        }
        if (Contrato != null)
        {
            vlista.Remove(new ListItem("Contrato de Participación Económica", "Contrato"));
        }
        if (vRegistroCalidadDenuncianteCitacion.IdRegistro == 0)
        {
            if (vDenunciaBien.ExisteDenuncia || vRegistroCalidadDenuncianteCitacion.ReconocimientoCalidadDenunciante == null)
            {
                vlista.Remove(new ListItem("Contrato de Participación Económica", "Contrato"));
            }
            else
            {
                if (!vRegistroCalidadDenuncianteCitacion.ReconocimientoCalidadDenunciante.Equals("Reconoce"))
                {
                    vlista.Remove(new ListItem("Contrato de Participación Económica", "Contrato"));
                }
            }
        }

        if (Calidad == null)
        {
            vlista.Remove(new ListItem("Registrar Calidad Denunciante", "RegistrarCalidadDenunciante"));
        }
        if (TipoPersona != "NATURAL")
        {
            vlista.Remove(new ListItem("Registrar Calidad Denunciante", "RegistrarCalidadDenunciante"));
        }
        #region CU148


        //Ajuste 3 bitacora 148
        if (InformeDenunciante == null || ContratoDiferenteTerminado && ContratoAsociado >= 0)
        {
            vlista.Remove(new ListItem("Registrar Informe del Denunciante", "InformeDenunciante"));
        }

        //// Ajuste 2 bitacora 148 
        //if ((valTipoTramite == null && valTipoTramite2 == null) || (ContratoDiferenteTerminado != null) || (ValidofaseCalidadDenunciante == null))
        if ((ContratoDiferenteTerminado) || (ValidofaseCalidadDenunciante == null))
        {
            vlista.Remove(new ListItem("Definir Tipo de Tramite", "TipoTramite"));
        }

        //Ajuste 4
        if (AnalisisJuridico == null)
        {
            vlista.Remove(new ListItem("Protocolización", "Protocolizacion"));
        }

        //Ajuste protocolizacion
        if (ValidaProtocolizacion == null)
        {
            vlista.Remove(new ListItem("Protocolización", "Protocolizacion"));
        }

        if (EsDenunciaAnterior || CalidadDenuncianteNoReconoce == true || DocumentacionNoCompleta == true)
        {
            vlista.Remove(new ListItem("Continuar de Oficio", "Continuar"));
        }

        #endregion
        return vlista;
    }

    #endregion

    protected void RadioButtonddy_CheckedChanged(object sender, EventArgs e)
    {
    }

    public int InsertarHistorico()
    {
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
        HistoricoEstadosDenunciaBien vhistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();

        vhistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        vhistoricoEstadosDenunciaBien.IdEstadoDenuncia = vDenunciaBien.IdEstadoDenuncia;
        vhistoricoEstadosDenunciaBien.IdFase = 1;
        vhistoricoEstadosDenunciaBien.IdAccion = 1;
        vhistoricoEstadosDenunciaBien.IdActuacion = 4;
        vhistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        vhistoricoEstadosDenunciaBien.UsuarioCrea = Convert.ToString(GetSessionUser().UsuarioCreacion.ToString());

        int Guardado = Convert.ToInt32(this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(vhistoricoEstadosDenunciaBien));
        return Guardado;
    }

    public int ActualizarDenunciaBien()
    {
        int Actualizado = Convert.ToInt32(this.vMostrencosService.EditarDenunciaBienFase(vIdDenunciaBien));
        return Actualizado;
    }

    protected void btnConsultarVentanaModal_Click(object sender, ImageClickEventArgs e)
    {
        List<Tercero> vlstTerceros = this.vMostrencosService.ConsultarJuridicoTercerosPorNombre(this.txbNombreEntidad.Text);

        if (ValidarCamposPopUp() == false)
        {
            if (vlstTerceros.Count() > 0)
            {
                grvBusquedaTerceros.DataSource = vlstTerceros;
                grvBusquedaTerceros.DataBind();
            }
            else
            {
                this.grvBusquedaTerceros.PageSize = PageSize();
                this.grvBusquedaTerceros.EmptyDataText = EmptyDataText();
                this.grvBusquedaTerceros.Visible = true;
                this.grvBusquedaTerceros.DataBind();
            }
            modalPopupInformacionTerceros.Show();
        }
    }

    protected void grvBusquedaTerceros_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(grvBusquedaTerceros.SelectedRow);
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }


    private void SeleccionarRegistro(GridViewRow pRow)
    {
        int rowIndex = pRow.RowIndex;
        string strValueIdTercero;
        try
        {
            if (grvBusquedaTerceros.DataKeys[rowIndex].Values["IDTERCERO"] != null)
            {
                strValueIdTercero = grvBusquedaTerceros.DataKeys[rowIndex].Values["IDTERCERO"].ToString();
                SetSessionParameter("Mostrencos.RegistrarDocumentosBienDenunciado.IdTercero", strValueIdTercero);
            }

            txtEntidadOficiarSolicitado.Text = grvBusquedaTerceros.Rows[rowIndex].Cells[1].Text;
            HabilitarCamposTercero();
            pnlArchivo.CssClass = "popuphIstorico hidden";
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void grvBusquedaTerceros_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvBusquedaTerceros.PageIndex = e.NewPageIndex;
        List<Tercero> vlstTerceros = this.vMostrencosService.ConsultarJuridicoTercerosPorNombre(this.txbNombreEntidad.Text);
        if (vlstTerceros.Count() > 0)
        {
            grvBusquedaTerceros.DataSource = vlstTerceros;
            grvBusquedaTerceros.DataBind();
        }
        modalPopupInformacionTerceros.Show();
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    private bool ValidarCamposPopUp()
    {
        bool vEsrequerido = false;
        int count = 0;

        try
        {
            //Validar Campo Vigencia
            if (this.txbNombreEntidad.Text == "")
            {
                this.lblCampoRequeridoNombreEntidad.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoNombreEntidad.Visible = false;
                vEsrequerido = false;
            }


            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    private bool validateTipoTramite(int pIdDenunciaBien)
    {
        List<Apoderados> vListaApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(pIdDenunciaBien);
        if (vListaApoderados.Count() > 0)
        {
            if (vListaApoderados.Where(p => p.EstadoApoderado.Equals("ACTIVO")).Count() > 0)
            {
                return true;
            }
            else
            {
                this.toolBar.MostrarMensajeGuardado("Es obligatorio tener registrado  un apoderado");
                return false;
            }
        }
        else
        {
            this.toolBar.MostrarMensajeGuardado("Es obligatorio tener registrado  un apoderado");
            return false;
        }
    }

    /// <summary>
    /// Método que valida si la denuncia ya ejecuto la Fse de "Protocolización" y la acción Comunicación Administrativa y Financiera
    /// </summary>
    /// <param name="pIdDenunciaBien">Id de la Denuncia</param>
    /// <returns>Resultadod e la operación (Bool)</returns>
    private bool ValidaFaseAccion(int pIdDenunciaBien, int pFase, int? pAccion)
    {
        List<HistoricoEstadosDenunciaBien> vListaHistorico = this.vMostrencosService.ValidaFaseAccion(pIdDenunciaBien, pFase, pAccion);

        if (vListaHistorico.Count() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

