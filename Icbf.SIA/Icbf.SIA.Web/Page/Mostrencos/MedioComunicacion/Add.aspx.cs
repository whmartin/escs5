﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_MedioComunicacion_Add : GeneralWeb
{
    /// <summary>
    /// toolBar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/MedioComunicacion";

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.txtNombreMedioComunicacion.Focus();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Metodo para cargar registros
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdMedioComunicacion = Convert.ToInt32(GetSessionParameter("MedioComunicacion.IdMedioComunicacion"));
            MedioComunicacion vMedioComunicacion = vMostrencosService.ConsultarMedioComunicacionPorId(vIdMedioComunicacion);
            txtNombreMedioComunicacion.Text = vMedioComunicacion.Nombre;
            chkMedioComunicacion.Items.FindByValue(Convert.ToInt32(vMedioComunicacion.Estado).ToString()).Selected = true;
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    /// <summary>
    /// Metodo inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.EstablecerTitulos("Parametrizar Medio de Comunicación");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina Add
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo para guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        MedioComunicacion medioComunicacion = new MedioComunicacion()
        {
            Nombre = txtNombreMedioComunicacion.Text,
            Estado = Convert.ToBoolean(Convert.ToInt32(chkMedioComunicacion.SelectedValue)),
            UsuarioCrea = GetSessionUser().NombreUsuario,
            UsuarioModifica = GetSessionUser().NombreUsuario
        };
        int result = 0;
        if (Request.QueryString["oP"] == "E")
        {
            medioComunicacion.IdMedioComunicacion = Convert.ToInt32(GetSessionParameter("MedioComunicacion.IdMedioComunicacion"));
            this.InformacionAudioria(medioComunicacion, this.PageName, vSolutionPage);
            result = vMostrencosService.ActualizarMedioComunicacion(medioComunicacion);
            if (result == 0)
            {
                SetSessionParameter("MedioComunicacion.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
            }
        }
        else
        {
            this.InformacionAudioria(medioComunicacion, this.PageName, vSolutionPage);
            result = vMostrencosService.IngresarMedioComunicacion(medioComunicacion);
            if (result != 0)
            {
                SetSessionParameter("MedioComunicacion.IdMedioComunicacion", result);
                SetSessionParameter("MedioComunicacion.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
            }
        }
    }

    /// <summary>
    /// Metodo para ir ala pagina de List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
}