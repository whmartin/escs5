﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_MedioComunicacion_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre Medio de Comunicación
            </td>
            <td>
                Estado Medio de Comunicación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreMedioComunicacion" MaxLength="100" Width="300" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreMedioComunicacion" runat="server"
                    TargetControlID="txtNombreMedioComunicacion" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                    ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="chkMedioComunicacion" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo"></asp:ListItem>
                    <asp:ListItem Text="Inactivo"></asp:ListItem>
                    <asp:ListItem Text="Todos" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">              
                <td>
                    <asp:GridView runat="server" ID="gvMedioComunicacion" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="gvMedioComunicacion_SelectedIndexChanged"
                        GridLines="None" Width="100%" DataKeyNames="IdMedioComunicacion" CellPadding="0" Height="16px" OnSorting="gvMedioComunicacion_Sorting" OnPageIndexChanging="gvMedioComunicacion_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre Medio de Comunicación" DataField="Nombre" SortExpression="Nombre" />
                            <asp:TemplateField HeaderText="Estado Medio de Comunicación" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# (Boolean.Parse(Eval("Estado").ToString())) ? "Activo" : "Inactivo" %>'></asp:Label> 
                                </ItemTemplate>
                            </asp:TemplateField>
                           </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
