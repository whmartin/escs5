﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_MedioComunicacion_List : GeneralWeb
{
    /// <summary>
    /// toolBar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/MedioComunicacion";

    /// <summary>
    /// Direction
    /// </summary>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.txtNombreMedioComunicacion.Focus();
            }
        }
    }

    /// <summary>
    /// Metodo inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoExcel += new ToolBarDelegate(this.btnExportar_Click);
            this.toolBar.EstablecerTitulos("Parametrizar Medio de Comunicación");
            this.gvMedioComunicacion.PageSize = PageSize();
            this.gvMedioComunicacion.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para exportar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvMedioComunicacion.Rows.Count > 0)
        {
            toolBar.LipiarMensajeError();
            DataTable datos = new DataTable();
            Boolean vPaginador = this.gvMedioComunicacion.AllowPaging;
            Boolean vPaginadorError = gvMedioComunicacion.AllowPaging;
            ExportarExcel vExportarExcel = new ExportarExcel();
            gvMedioComunicacion.AllowPaging = false;
            List<MedioComunicacion> vListaMedioComunicacion = ((List<MedioComunicacion>)this.ViewState["SortedgvMedioComunicacion"]).ToList();
            datos = Utilidades.GenerarDataTable(this.gvMedioComunicacion, vListaMedioComunicacion);
            if (datos != null)
            {
                if (datos.Rows.Count > 0)
                {
                    GridView datosexportar = new GridView();
                    datosexportar.DataSource = datos;
                    datosexportar.DataBind();
                    vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "MedioComunicacion");
                    gvMedioComunicacion.AllowPaging = vPaginador;
                }
                else
                    toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
            }
            else
                toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina de Add 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo para llamar 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Metodo para buscar medios de comunicacion
    /// </summary>
    private void Buscar()
    {
        toolBar.LipiarMensajeError();
        try
        {
            bool? estado = null;
            switch (chkMedioComunicacion.Text)
            {
                case "Activo":
                    estado = true;
                    break;
                case "Inactivo":
                    estado = false;
                    break;
            }
            List<MedioComunicacion> vListaMedioComunicacion = this.vMostrencosService.ConsultarMedioComunicacion(txtNombreMedioComunicacion.Text, estado).OrderBy(a => a.Nombre).ToList();
            this.ViewState["SortedgvMedioComunicacion"] = vListaMedioComunicacion;
            this.gvMedioComunicacion.Visible = true;
            this.gvMedioComunicacion.DataSource = vListaMedioComunicacion;
            this.gvMedioComunicacion.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para organizar los registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvMedioComunicacion_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<MedioComunicacion> vList = new List<MedioComunicacion>();
        List<MedioComunicacion> vResult = new List<MedioComunicacion>();
        if (this.ViewState["SortedgvMedioComunicacion"] != null)
        {
            vList = (List<MedioComunicacion>)this.ViewState["SortedgvMedioComunicacion"];
        }

        switch (e.SortExpression)
        {
            case "Nombre":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderBy(a => a.Nombre).ToList();
                else
                    vResult = vList.OrderByDescending(a => a.Nombre).ToList();
                break;

            case "Estado":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderByDescending(a => a.Estado).ToList();
                else
                    vResult = vList.OrderBy(a => a.Estado).ToList();
                break;
        }

        if (this.Direction == SortDirection.Ascending)
            this.Direction = SortDirection.Descending;
        else
            this.Direction = SortDirection.Ascending;

        this.ViewState["SortedgvMedioComunicacion"] = vResult;
        this.gvMedioComunicacion.DataSource = vResult;
        this.gvMedioComunicacion.DataBind();
    }

    /// <summary>
    /// Metodo que llama al metodo SeleccionarRegistro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvMedioComunicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvMedioComunicacion.SelectedRow);
    }

    /// <summary>
    /// Metodo para seleccionar un registro
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvMedioComunicacion.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("MedioComunicacion.IdMedioComunicacion", strValue);
            this.SetSessionParameter("MedioComunicacion.Guardado", "0");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para paginar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvMedioComunicacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvMedioComunicacion.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvMedioComunicacion"] != null)
        {
            List<MedioComunicacion> vList = (List<MedioComunicacion>)this.ViewState["SortedgvMedioComunicacion"];
            this.gvMedioComunicacion.DataSource = vList;
            this.gvMedioComunicacion.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }
}