﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_MedioComunicacion_Detail : GeneralWeb
{
    /// <summary>
    /// toolBar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/MedioComunicacion";

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// vIdMedioComunicacion
    /// </summary>
    private int vIdMedioComunicacion;

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Metodo inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.EstablecerTitulos("Parametrizar Medio de Comunicación");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina de List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("MedioComunicacion.IdMedioComunicacion");
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Metodo para ir a la pagina de Add
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("MedioComunicacion.IdMedioComunicacion");
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo para ir a la pagina de Edit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        vIdMedioComunicacion = Convert.ToInt32(GetSessionParameter("MedioComunicacion.IdMedioComunicacion"));
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Metodo para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdMedioComunicacion = Convert.ToInt32(GetSessionParameter("MedioComunicacion.IdMedioComunicacion"));
            if (vIdMedioComunicacion != 0)
            {
                if (GetSessionParameter("MedioComunicacion.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                RemoveSessionParameter("MedioComunicacion.Guardado");

                MedioComunicacion vMedioComunicacion = vMostrencosService.ConsultarMedioComunicacionPorId(vIdMedioComunicacion);
                txtNombreMedioComunicacion.Text = vMedioComunicacion.Nombre;
                chkMedioComunicacion.Items.FindByValue(Convert.ToInt32(vMedioComunicacion.Estado).ToString()).Selected = true;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}