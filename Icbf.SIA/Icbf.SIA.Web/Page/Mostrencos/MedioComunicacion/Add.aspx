﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_MedioComunicacion_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            Nombre Medio de Comunicación *
                            <asp:RequiredFieldValidator ID="rfvMedioComunicacion" ControlToValidate="txtNombreMedioComunicacion" runat="server"
                                ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            Estado Medio de Comunicación *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                             <asp:TextBox runat="server" ID="txtNombreMedioComunicacion" MaxLength="100" Width="300" ></asp:TextBox>
                             <Ajax:FilteredTextBoxExtender ID="ftNombreMedioComunicacion" runat="server"
                                TargetControlID="txtNombreMedioComunicacion" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                ValidChars="áéíóúÁÉÍÓÚñÑ " />
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="chkMedioComunicacion" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
