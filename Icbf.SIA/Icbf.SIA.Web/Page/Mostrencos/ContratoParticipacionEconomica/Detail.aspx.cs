﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_ContratoParticipacionEconomica_Detail : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ContratoParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Contrato
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private int IdContrato;

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.MostrarBotonConsultar();
        toolBar.MostrarBotonNuevo(true);
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    #region METODOS

    /// <summary>
    /// Metodo inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.EstablecerTitulos("Contrato Participación Económica");
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            if ((vRegistroDenuncia.IdEstado != 11) && (vRegistroDenuncia.IdEstado != 13))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                toolBar.eventoBuscar += btnConsultar_Click;
            }
            else
            {
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }
            this.pnlInformacionDenuncia.Visible = true;
            this.pnlRelacionarContrato.Visible = true;
            this.pnlInformacionContrato.Visible = true;
            this.pnlDocumentacionSolicitada.Visible = true;
            this.pnlDocumentacionRecibida.Visible = true;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar datos de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    if (vDenunciaBien.IdEstadoDenuncia == 11 || vDenunciaBien.IdEstadoDenuncia == 13 || vDenunciaBien.IdEstadoDenuncia == 19)
                    {
                        this.toolBar.OcultarBotonNuevo(true);
                        this.toolBar.MostrarBotonEditar(false);
                    }
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m.") && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.txtNumeroIdentificacion.Text += "-" + vDenunciaBien.Tercero.DigitoVerificacion.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }

                int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
                if (vIdContrato != 0)
                {
                    List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
                    var contrato = vListaContratoDenunciaBien.Find(x => x.IdContrato == vIdContrato);
                    if (contrato != null)
                    {
                        txbVigencia.Text = !string.IsNullOrEmpty(contrato.Vigencia.ToString()) ? contrato.Vigencia.ToString() : string.Empty;
                        txbRegional.Text = !string.IsNullOrEmpty(contrato.NombreRegional) ? contrato.NombreRegional : string.Empty;
                        txbNumeroContrato.Text = !string.IsNullOrEmpty(contrato.NumeroContrato) ? contrato.NumeroContrato : string.Empty;
                        fecFechaSuscripcion.Date = contrato.FechaSuscripcion;
                        fecFechaInicio.Date = contrato.FechaInicioEjecucion;
                        fecFechaTerminacionInicial.Date = contrato.FechaTerminacionInicial;
                        fechFechaTerminacionFinal.Date = contrato.FechaTerminacionFinal;

                        //Validacion Como esta en Contratos
                        if (fechFechaTerminacionFinal.Date != null)
                        {
                            var diferenciaFechas = string.Empty;
                            ObtenerDiferenciaFechas(fecFechaInicio.Date, fechFechaTerminacionFinal.Date, out diferenciaFechas);
                            var items1 = diferenciaFechas.Split('|');
                            txbDias.Text = items1[2];
                            txbMeses.Text = items1[1];
                            txbAnios.Text = items1[0];
                        }
                        else if (fecFechaTerminacionInicial.Date != null)
                        {
                            var diferenciaFechas = string.Empty;
                            ObtenerDiferenciaFechas(fecFechaInicio.Date, fecFechaTerminacionInicial.Date, out diferenciaFechas);
                            var items1 = diferenciaFechas.Split('|');
                            txbDias.Text = items1[2];
                            txbMeses.Text = items1[1];
                            txbAnios.Text = items1[0];
                        }
                        txbEstadoContrato.Text = !string.IsNullOrEmpty(contrato.EstadoContrato.ToString()) ? contrato.EstadoContrato.ToString() : string.Empty;
                    }
                    List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(contrato.IdContratoParticipacionEconomica);
                    if (vListaDocumentosSolicitadosYRecibidos.Count() > 0)
                    {
                        ddlTipoDocumentoSolicitado.DataSource = vListaDocumentosSolicitadosYRecibidos;
                        ddlTipoDocumentoSolicitado.DataBind();
                        ddlTipoDocumentoSolicitado.Text = vListaDocumentosSolicitadosYRecibidos.LastOrDefault().TipoDocumento.ToString();
                        fecFechaSolicitudSolicitado.Date = vListaDocumentosSolicitadosYRecibidos.LastOrDefault().FechaSolicitud;
                        txbObservacionesSolicitado.Text = vListaDocumentosSolicitadosYRecibidos.LastOrDefault().ObservacionesDocumentacionSolicitada;
                        vListaDocumentosSolicitadosYRecibidos = vListaDocumentosSolicitadosYRecibidos.Where(p => p.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
                        ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitadosYRecibidos;
                        grvDocumentacionRecibida.DataSource = vListaDocumentosSolicitadosYRecibidos;
                        grvDocumentacionRecibida.DataBind();
                    }
                }
                string vMensaje = Convert.ToString(this.GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.Mensaje"));
                if (!string.IsNullOrEmpty(vMensaje))
                {
                    this.toolBar.MostrarMensajeGuardado(vMensaje);
                    this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.Mensaje", string.Empty);
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para obtener fecha
    /// </summary>
    /// <param name="fechaInicial"></param>
    /// <param name="fechafinal"></param>
    /// <returns></returns>
    private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
    {
        fechafinal = fechafinal.AddDays(1);

        int result = 0;
        int ai, mi, di;
        int af, mf, df;

        ai = fechaInicial.Year;
        mi = fechaInicial.Month;
        di = fechaInicial.Day;

        af = fechafinal.Year;
        mf = fechafinal.Month;
        df = fechafinal.Day;


        if (di == 31 || (mi == 2 && di > 27))
            di = 30;
        if (df > 27 && mf == 2)
            df = 30;
        if (df == 31 && di < 30)
        {
            mf++;
            df = 1;
        }
        else if (df == 31)
            df = 30;
        else
        {
            if (di == 31 || (mi == 2 && di > 27))
                di = 30;
            if (df == 31 || (mf == 2 && df > 27))
                df = 30;
        }

        if (Math.Abs(af - ai) == 0)
            result = (mf - mi) * 30 + df - di;
        else
            result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


        return result;
    }

    /// <summary>
    /// Metodo para obtener diferencia de fechas
    /// </summary>
    /// <param name="fechaInicio"></param>
    /// <param name="fechaFin"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    public static bool ObtenerDiferenciaFechas(DateTime fechaInicio, DateTime fechaFin, out string result)
    {
        bool isValid = true;
        result = string.Empty;
        try
        {
            int dias360 = ObtenerdiasEntreFechas360(fechaInicio, fechaFin);

            decimal dias360toMonth = dias360 / 30;

            int meses = (int)(Math.Round(dias360toMonth, 0, MidpointRounding.ToEven));

            //int anio = meses / 12;

            //meses = meses % 12;

            int dias = dias360 - (meses * 30);

            result = "0|" + meses + "|" + dias;
        }
        catch (Exception ex)
        {
            isValid = false;
        }

        return isValid;
    }

    //private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
    //{
    //    fechafinal = fechafinal.AddDays(1);

    //    int result = 0;
    //    int ai, mi, di;
    //    int af, mf, df;

    //    ai = fechaInicial.Year;
    //    mi = fechaInicial.Month;
    //    di = fechaInicial.Day;

    //    af = fechafinal.Year;
    //    mf = fechafinal.Month;
    //    df = fechafinal.Day;


    //    if (di == 31 || (mi == 2 && di > 27))
    //        di = 30;
    //    if (df > 27 && mf == 2)
    //        df = 30;
    //    if (df == 31 && di < 30)
    //    {
    //        mf++;
    //        df = 1;
    //    }
    //    else if (df == 31)
    //        df = 30;
    //    else
    //    {
    //        if (di == 31 || (mi == 2 && di > 27))
    //            di = 30;
    //        if (df == 31 || (mf == 2 && df > 27))
    //            df = 30;
    //    }

    //    if (Math.Abs(af - ai) == 0)
    //        result = (mf - mi) * 30 + df - di;
    //    else
    //        result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


    //    return result;
    //}

    //public static bool ObtenerDiferenciaFechas(DateTime fechaInicio, DateTime fechaFin, out string result)
    //{
    //    bool isValid = true;
    //    result = string.Empty;
    //    try
    //    {
    //        int dias360 = ObtenerdiasEntreFechas360(fechaInicio, fechaFin);

    //        decimal dias360toMonth = dias360 / 30;

    //        int meses = (int)(Math.Round(dias360toMonth, 0, MidpointRounding.ToEven));

    //        //int anio = meses / 12;

    //        //meses = meses % 12;

    //        int dias = dias360 - (meses * 30);

    //        result = "0|" + meses + "|" + dias;
    //    }
    //    catch (Exception ex)
    //    {
    //        isValid = false;
    //    }

    //    return isValid;
    //}

    #endregion

    #region BOTONES

    /// <summary>
    /// Boton Consultar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Boton Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
        bool valida = false;
        foreach (var item in vListaContratoDenunciaBien)
        {
            if (item.IDEstadoContrato != 7)
            {
                valida = true;
            };
        }

        if (valida == true)
        {
            string Valor = "No se puede relacionar un contrato, el contrato asociado a la denuncia debe estar en estado Terminado";
            toolBar.MostrarMensajeError(Valor);
        }
        else
        {
            NavigateTo(SolutionPage.Add);
        }
    }

    /// <summary>
    /// Boton Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        //int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
        //SetSessionParameter("ContratoParticipacionEconomica.IdContrato", vIdContrato.ToString());
        Response.Redirect("../../Mostrencos/ContratoParticipacionEconomica/Edit.aspx");
    }

    /// <summary>
    /// Boton Retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }
    #endregion
}