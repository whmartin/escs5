﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_ContratoParticipacionEconomica_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de radicado de la denuncia *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado en correspondencia *              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha radicado en correspondencia *              
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Tipo de Identificación *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Número de identificación *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                        <tr class="rowB">
                            <td>&nbsp; Primer nombre *
                            </td>
                            <td class="auto-style1">&nbsp;</td>
                            <td>Segundo nombre *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>&nbsp; Primer apellido *
                            </td>
                            <td class="auto-style1"></td>
                            <td>Segundo apellido *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="PanelRazonSocial">
                        <tr class="rowB">
                            <td colspan="2">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">&nbsp;&nbsp;Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlRelacionarContrato">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Relacionar Contrato</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3">&nbsp;&nbsp;Información Contrato</td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txbInformacionContrato" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionContrato">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de Contrato</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Vigencia
                        </td>
                        <td class="auto-style1"></td>
                        <td>Regional
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbVigencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txbRegional" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Número de Contrato              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de Suscripción              
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbNumeroContrato" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaSuscripcion" Requerid="False" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Plazo Inicial de Ejecución           
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de Inicio
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaInicio" Requerid="False" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Días&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:TextBox runat="server" ID="txbDias" Enabled="false" Width="93px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Meses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txbMeses" Enabled="false" Width="93px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Años&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txbAnios" Enabled="false" Width="93px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha de Terminación Inicial              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de Terminación Final               
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;&nbsp; 
                            <uc1:fecha runat="server" ID="fecFechaTerminacionInicial" Requerid="False" Enabled="false" />
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fechFechaTerminacionFinal" Requerid="False" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Estado del Contrato             
                        </td>
                        <td class="auto-style1"></td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbEstadoContrato" Enabled="false" Width="166px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Documento soporte de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de solicitud *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="auto-style4">
                            <asp:DropDownList ID="ddlTipoDocumentoSolicitado" runat="server" Enabled="false"
                                DataValueField="TipoDocumento" DataTextField="NomTipoDocumento" Height="16px" Width="226px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaSolicitudSolicitado" Requerid="true" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Observaciones al documento solicitado
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txbObservacionesSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Documentación recibida</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="grvDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" ShowHeader="true" Visible="true" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px" PageSize="10">
                                <Columns>
                                    <asp:BoundField HeaderText="Tipo de documento" DataField="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstadoDocumento" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Nombre de archivo" DataField="NombreArchivo" />
                                    <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/edit.gif"
                                                Height="16px" Width="16px" ToolTip="Editar" Enable="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/delete.gif"
                                                Height="16px" Width="16px" ToolTip="Eliminar" Enable="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 248px;
        }

        .auto-style4 {
            width: 259px;
        }
    </style>
</asp:Content>

