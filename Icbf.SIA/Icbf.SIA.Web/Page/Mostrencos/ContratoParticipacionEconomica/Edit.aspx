﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Edit.aspx.cs" Inherits="Page_Mostrencos_ContratoParticipacionEconomica_Edit" %>

<%@ Register Src="~/General/General/Control/fechaEdit.ascx" TagPrefix="uc1" TagName="fecha" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        .Background {
        }

        .Popup {
            background-color: #FFFFFF;
            padding-top: 10px;
            padding-left: 10px;
            width: 720px;
            height: 450px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }
    </style>

    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de radicado de la denuncia *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado en correspondencia *              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha radicado en correspondencia *              
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Tipo de Identificación *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Número de identificación *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                        <tr class="rowB">
                            <td>&nbsp; Primer nombre *
                            </td>
                            <td class="auto-style1">&nbsp;</td>
                            <td>Segundo nombre *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>&nbsp; Primer apellido *
                            </td>
                            <td class="auto-style1"></td>
                            <td>Segundo apellido *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="PanelRazonSocial">
                        <tr class="rowB">
                            <td colspan="2">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">&nbsp;&nbsp;Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                             <asp:ImageButton runat="server" OnClick="btnAbrirHistorico_Click" ImageUrl="../../../Image/btn/info.jpg" Style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: 1px; position: relative;"></asp:ImageButton>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlgrvHistoricoDenuncia" CssClass="popUpHistoricoDenuncia hidden" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 30%; left: 25%; width: 50%; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 650px;">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf; height: 56px; width: 100%;">
                            <div style="width: 90%; float: left; overflow-y: auto; overflow-x: auto;">
                                <span style="color: black; font-weight: bold; top: 10px; margin-right: -40px;">Hist&oacute;rico de la denuncia

                                </span>

                            </div>
                            <div style="width: 10%; float: right; overflow-y: auto; overflow-x: auto;">
                                <asp:ImageButton runat="server" OnClick="btnCerrarHistorico_Click" ImageUrl="../../../Image/btn/close.png" Style="cursor: pointer; width: 20px; 
                                    height: 20px; margin-right: -30px;"></asp:ImageButton>
                            </div>

                        </td>

                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; height: 200px; align-content: center">
                                <asp:GridView runat="server" ID="grvHistoricoDenuncia" AutoGenerateColumns="False"
                                    GridLines="None" Width="100%" DataKeyNames="IdHistoricoEstadoDenunciaBien" AllowSorting="True" AllowPaging="true" PageSize="10"
                                    OnPageIndexChanging="grvHistoricoDenuncia_PageIndexChanged">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado Denuncia" DataField="NombreEstadoDenuncia" />
                                        <asp:BoundField HeaderText="Fecha de Creación" DataField="FechaCrea" DataFormatString="{0:dd-MM-yyyy}" />
                                        <asp:BoundField HeaderText="Responsable" DataField="UsuarioCrea" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" />
                                        <asp:BoundField HeaderText="Accion" DataField="Accion" />
                                        <asp:BoundField HeaderText="Actuacion" DataField="Actuacion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <asp:EmptyDataTemplate>
                                        No se encontraron datos, verifique por favor  
                                    </asp:EmptyDataTemplate>
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>

                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <script type="text/javascript">
                $(document).ready(function () {
                    $(document).on('click', '.btnCerrarPopArchivo', function () {
                        $('.popuphIstorico').addClass('hidden');
                    });

                    //$('#cphCont_txtDescripcion').on('input', function (e) {
                    //    if (document.getElementById("cphCont_txtDescripcion").value.length > 512) {
                    //        this.value = document.getElementById("cphCont_txtDescripcion").value.substr(0, 512);
                    //    }
                    //});

                });
                //function CheckLengthDes() {
                //    var textbox = document.getElementById("cphCont_txtDescripcion").value;
                //    if (textbox.trim().length >= 512) {
                //        return false;
                //    }
                //    else {
                //        return true;
                //    }
                //}
            </script>

            <asp:Panel runat="server" ID="pnlRelacionarContrato">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Relacionar Contrato</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3">&nbsp;&nbsp;Información Contrato</td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txbInformacionContrato" Enabled="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInformacionContrato">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de Contrato</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Vigencia
                        </td>
                        <td class="auto-style1"></td>
                        <td>Regional
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbVigencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txbRegional" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Número de Contrato              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de Suscripción              
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbNumeroContrato" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaSuscripcion" Required="false" Enabled="false" ValidationGroup="" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Plazo Inicial de Ejecución           
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de Inicio
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaInicio" Required="false" Enabled="false" ValidationGroup="" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Días&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:TextBox runat="server" ID="txbDias" Enabled="false" Width="93px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Meses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txbMeses" Enabled="false" Width="93px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp;&nbsp; Años&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txbAnios" Enabled="false" Width="93px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha de Terminación Inicial              
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de Terminación Final               
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="fecFechaTerminacionInicial" Required="false" Enabled="false" ValidationGroup="" />
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="fechFechaTerminacionFinal" Required="false" Enabled="false" Visible="False" ValidationGroup="" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Estado del Contrato             
                        </td>
                        <td class="auto-style1"></td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txbEstadoContrato" Enabled="false" Width="166px"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Documento soporte de la denuncia *
                            <asp:Label ID="lblCampoRequeridoTipoDocumentoSolicitado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de solicitud *
                            <asp:Label ID="lblCampoRequeridoFechaDocumentoSolicitado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                         <br/>
                      <asp:CustomValidator runat="server"
                        ID="CustomValidator4"  ForeColor="Red"
                        ControlToValidate="FechaSolicitud$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="auto-style4">
                            <asp:DropDownList ID="ddlTipoDocumento" runat="server"
                                DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento" Height="30px" Width="234px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaSolicitud" required="false" />
                        </td> 
                        <td class="auto-style4">
                            <asp:ImageButton ID="btnAplicar"  runat="server" ImageUrl="~/Image/btn/apply.png"
                                Height="25px" Width="28px" ToolTip="Aplicar" OnClick="btnAplicar_Click" Visible="false"/>
                            
                            <asp:ImageButton ID="btnAdicionar" runat="server" ImageUrl="~/Image/btn/add.gif"
                                Height="25px" Width="28px" ToolTip="Adicionar" Enable="true" OnClick="btnAdicionar_Click" visible="true"/>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAdicionar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="auto-style4">Observaciones al documento solicitado
                              <asp:Label ID="lblCampoRequeridoObservacionesDocumentoSolicitado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtObservacionesSolicitado"
                                TextMode="MultiLine" MaxLength="512" Rows="8"
                                Width="600px" Height="100" Style="resize: none" onkeypress="return CheckLength();" onkeyup="return CheckLength();"></asp:TextBox>

                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                TargetControlID="txtObservacionesSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />

                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="2">Documentación recibida
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td style="width: 50%">Estado del documento *
                          
                             <asp:Label ID="lblCampoRequeridoEstadoDocumento" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>

                        </td>
                        <td>Fecha de recibido <asp:Label ID="req3" runat="server" Text="*" Visible="false"></asp:Label>                
                            <asp:Label ID="lblCampoRequeridoFechaRecibido" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaMaxima" Visible="false" Text="La fecha seleccionada no debe ser posterior a la fecha actual. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaSolicitud" Visible="false" Text="La fecha seleccionada no debe ser anterior a la fecha de solicitud. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:CustomValidator runat="server"
                                ID="reqFechaRecibida2"  ForeColor="Red"
                                ControlToValidate="FechaRecibido$txtFecha"
                                ClientValidationFunction="validateDateResolucion" ValidationGroup="btnAplicar"
                                ErrorMessage="La fecha seleccionada no debe ser posterior a la fecha actual. Por favor revisar.">
                            </asp:CustomValidator>
                                 <br/>
                      <asp:CustomValidator runat="server"
                        ID="CustomValidator1"  ForeColor="Red"
                        ControlToValidate="FechaRecibido$txtFecha"
                        ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                        ErrorMessage="Fecha inv&aacute;lida">
                    </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="rbtEstadoDocumento_SelectedIndexChanged">
                                <asp:ListItem Value="3">Aceptado</asp:ListItem>
                                <asp:ListItem Value="5">Devuelto</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaRecibido" Requerid="false" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Nombre de archivo <asp:Label ID="req1" runat="server" Text="*" Visible="false"></asp:Label>
                     <asp:Label ID="lblCampoRequeridoNombreArchivo" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:FileUpload ID="fulArchivoRecibido" runat="server" Height="18px" Width="298px" />
                            <br />
                            <asp:Label ID="lblNombreArchivo" runat="server" Visible="false"></asp:Label>
                            <asp:UpdatePanel ID="UpnlDocumento" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAplicar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento recibido
                          <asp:Label ID="lblCampoRequeridoObservacionesDocumentoRecibido" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" 
                                Height="100" Style="resize: none" Enabled="false" onkeypress="return CheckLengthReci();" onkeyup="return CheckLengthReci();">
                            </asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                TargetControlID="txtObservacionesRecibido"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                                ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="grvDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                GridLines="None" Width="100%" ShowHeader="true" Visible="true" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px" PageSize="10"
                                OnPageIndexChanging="grvDocumentacionRecibida_PageIndexChanged">
                                <Columns>
                                    <asp:BoundField HeaderText="Tipo de documento" DataField="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstadoDocumento" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:TemplateField HeaderText="Nombre del Archivo">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkVerArchivo" runat="server" CommandArgument='<%# Eval("RutaArchivo") %>' Text='<%# Eval("NombreArchivo")%>' OnClick="lnkVerArchivo_Click" AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditarDocumento" runat="server" CommandName="Select" ImageUrl="~/Image/btn/edit.gif"
                                                Height="16px" Width="16px" ToolTip="Editar" Enable="true" CommandArgument='<%# Eval("IdDocumentosSolicitadosDenunciaBien") +"|"+ Eval("NombreTipoDocumento") +"|"+ Eval("FechaSolicitud") +"|"+ Eval("ObservacionesDocumentacionSolicitada") +"|"+ Eval("IdEstadoDocumento") +"|"+ Eval("FechaRecibido") +"|"+ Eval("NombreArchivo") +"|"+ Eval("ObservacionesDocumentacionRecibida") %>' 
                                                OnClick="btnEditarDocumento_Click" AutoPostBack="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEliminarDocumento" runat="server" CommandName="Delete" ImageUrl="~/Image/btn/delete.gif"
                                                Height="16px" Width="16px" ToolTip="Eliminar" Enable="true" CommandArgument='<%# Eval("IdDocumentosSolicitadosDenunciaBien") %>' OnClick="btnEliminarDocumento_Click" AutoPostBack="true" OnClientClick="return confirm('¿Desea eliminar el Documento ?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr hidden>
                        <td>
                            <asp:Button ID="btnVerArchivo" runat="server" Visible="true" AutoPostBack="true" Text="prueba" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlArchivo" EnableViewState="false" CssClass="popuphIstorico hidden" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                </div>
                                <div class="col-md-1 align-center">
                                    <a class="btnCerrarPop" onclick="btnCerrarHistorico_Click" style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative;">
                                        <img style="width: 20px; height: 20px;" alt="h" src="../../../Image/btn/close.png"></img>
                                    </a>
                                </div>
                            </div>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnDescargar" />
                                </Triggers>
                            </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <script type="text/javascript">
                $(document).ready(function () {
                    $(document).on('click', '.btnCerrarPop', function () {
                        $('.popuphIstorico').addClass('hidden');
                    });
                    $('#cphCont_txtObservacionesSolicitado').on('input', function (e) {
                        if (document.getElementById("cphCont_txtObservacionesSolicitado").value.length > 512) {
                            this.value = document.getElementById("cphCont_txtObservacionesSolicitado").value.substr(0, 512);
                        }
                    });
                    $('#cphCont_txtObservacionesRecibido').on('input', function (e) {
                        if (document.getElementById("cphCont_txtObservacionesRecibido").value.length > 512) {
                            this.value = document.getElementById("cphCont_txtObservacionesRecibido").value.substr(0, 512);
                        }
                    });
                });
                function CheckLength() {
                    var textbox = document.getElementById("cphCont_txtObservacionesSolicitado").value;
                    if (textbox.trim().length >= 512) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                function CheckLengthReci() {
                    var textbox = document.getElementById("cphCont_txtObservacionesRecibido").value;
                    if (textbox.trim().length >= 512) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                function validateDate(source, arguments) {

                    var EnteredDate = arguments.Value;
                    var fechaResolucionA = EnteredDate.split('/');
                    var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

                    if (DateResolucionA != '') {

                        var today = new Date();
                        if (DateResolucionA > today) {
                            arguments.IsValid = false;
                        }
                        else {
                            arguments.IsValid = true;
                        }
                    }
                }

                function validateTodaymenos1(source, arguments) {
                    var EnteredDate = arguments.Value;
                    var fechaResolucionA = EnteredDate.split('/');
                    var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])
                    if (DateResolucionA != '') {
                        var today = new Date();
                        var yesterday = new Date();
                        yesterday.setDate(today.getDate() - 1);
                        if (DateResolucionA > yesterday) {
                            arguments.IsValid = false;
                        }
                        else {
                            arguments.IsValid = true;
                        }
                    }
                }

                function validateFechaInvalida(source, arguments) {
                    var EnteredDate = arguments.Value;
                    var fechaResolucionA = EnteredDate.split('/');
                    if (parseInt(fechaResolucionA[2]) < 1900 || fechaResolucionA[2] == '0000') {
                        arguments.IsValid = false;
                    }
                    else {
                        arguments.IsValid = true;
                    }
                }

                function validateDateResolucion(source, arguments) {

                    var EnteredDate = arguments.Value;
                    var fechaRecibido = EnteredDate.split('/');
                    var DateB = new Date(fechaRecibido[2], fechaRecibido[1] - 1, fechaRecibido[0])

                    var fechaSolicitud = document.getElementById('cphCont_FechaSolicitud_txtFecha').value;
                    var fechaSolicitada = fechaSolicitud.split('/');
                    var DateA = new Date(fechaSolicitada[2], fechaSolicitada[1] - 1, fechaSolicitada[0]);

                    if (DateA != '' && DateB != '') {

                        var today = new Date();
                        if (DateB < DateA) {
                            arguments.IsValid = false;
                        } else if (DateB > today) {
                            arguments.IsValid = false;
                        }
                        else {
                            arguments.IsValid = true;
                        }
                    }
                }

            </script>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function validateDateResolucion(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaRecibido = EnteredDate.split('/');
            var DateB = new Date(fechaRecibido[2], fechaRecibido[1] - 1, fechaRecibido[0])

            if (DateB != '') {

                var today = new Date();
                if (DateB > today) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 248px;
        }

        .auto-style4 {
            width: 259px;
        }
    </style>
</asp:Content>
