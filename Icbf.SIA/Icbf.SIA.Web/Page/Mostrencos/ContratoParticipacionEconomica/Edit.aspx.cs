﻿//-----------------------------------------------------------------------
// <copyright file="Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Edit.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.IO;

public partial class Page_Mostrencos_ContratoParticipacionEconomica_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ContratoParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// File Name
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    #region METODOS

    /// <summary>
    /// Metodo Inicial
    /// </summary>
    public void Iniciar()
    {
        toolBar = (masterPrincipal)this.Master;
        toolBar.EstablecerTitulos("Contrato Participación Económica");
        toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
        toolBar.eventoBuscar += btnConsultar_Click;
        toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

        toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
        toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
        toolBar.MostrarBotonNuevo(false);
        toolBar.MostrarBotonEditar(false);

        CargarDatosHistoricoDenuncia();
        MostrarPaneles();
        HabilitarPaneles();
    }

    /// <summary>
    /// Metodo para cargar los datos de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m.") && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.txtNumeroIdentificacion.Text += "-" + vDenunciaBien.Tercero.DigitoVerificacion.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();

                }

                int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
                vListaContratoDenunciaBien = vListaContratoDenunciaBien.Where(p => p.IdContrato == vIdContrato).ToList();
                if (vListaContratoDenunciaBien.Count() > 0)
                {
                    txbVigencia.Text = !string.IsNullOrEmpty(vListaContratoDenunciaBien.FirstOrDefault().Vigencia.ToString()) ? vListaContratoDenunciaBien.FirstOrDefault().Vigencia.ToString() : string.Empty;
                    txbRegional.Text = !string.IsNullOrEmpty(vListaContratoDenunciaBien.FirstOrDefault().NombreRegional) ? vListaContratoDenunciaBien.FirstOrDefault().NombreRegional : string.Empty;
                    txbNumeroContrato.Text = !string.IsNullOrEmpty(vListaContratoDenunciaBien.FirstOrDefault().NumeroContrato) ? vListaContratoDenunciaBien.FirstOrDefault().NumeroContrato : string.Empty;
                    fecFechaSuscripcion.Date = vListaContratoDenunciaBien.FirstOrDefault().FechaSuscripcion;
                    fecFechaInicio.Date = vListaContratoDenunciaBien.FirstOrDefault().FechaInicioEjecucion;
                    fecFechaTerminacionInicial.Date = vListaContratoDenunciaBien.FirstOrDefault().FechaTerminacionInicial;
                    fechFechaTerminacionFinal.Date = vListaContratoDenunciaBien.FirstOrDefault().FechaTerminacionFinal;

                    //Validacion Como esta en Contratos
                    if (fechFechaTerminacionFinal.Date != null)
                    {

                        var diferenciaFechas = string.Empty;

                        ObtenerDiferenciaFechas(fecFechaInicio.Date, fechFechaTerminacionFinal.Date, out diferenciaFechas);

                        var items1 = diferenciaFechas.Split('|');

                        txbDias.Text = items1[2];
                        txbMeses.Text = items1[1];
                        txbAnios.Text = items1[0];
                    }
                    else if (fecFechaTerminacionInicial.Date != null)
                    {

                        var diferenciaFechas = string.Empty;

                        ObtenerDiferenciaFechas(fecFechaInicio.Date, fecFechaTerminacionInicial.Date, out diferenciaFechas);

                        var items1 = diferenciaFechas.Split('|');

                        txbDias.Text = items1[2];
                        txbMeses.Text = items1[1];
                        txbAnios.Text = items1[0];
                    }

                    txbEstadoContrato.Text = !string.IsNullOrEmpty(vListaContratoDenunciaBien.FirstOrDefault().EstadoContrato.ToString()) ? vListaContratoDenunciaBien.FirstOrDefault().EstadoContrato.ToString() : string.Empty;
                }

                CargarListaTipoDocumento();

                List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(vListaContratoDenunciaBien.FirstOrDefault().IdContratoParticipacionEconomica);
                vListaDocumentosSolicitadosYRecibidos = vListaDocumentosSolicitadosYRecibidos.Where(p => p.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
                ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitadosYRecibidos;
                grvDocumentacionRecibida.DataSource = vListaDocumentosSolicitadosYRecibidos;
                grvDocumentacionRecibida.DataBind();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para obtener fecha
    /// </summary>
    /// <param name="fechaInicial"></param>
    /// <param name="fechafinal"></param>
    /// <returns></returns>
    private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
    {
        fechafinal = fechafinal.AddDays(1);

        int result = 0;
        int ai, mi, di;
        int af, mf, df;

        ai = fechaInicial.Year;
        mi = fechaInicial.Month;
        di = fechaInicial.Day;

        af = fechafinal.Year;
        mf = fechafinal.Month;
        df = fechafinal.Day;


        if (di == 31 || (mi == 2 && di > 27))
            di = 30;
        if (df > 27 && mf == 2)
            df = 30;
        if (df == 31 && di < 30)
        {
            mf++;
            df = 1;
        }
        else if (df == 31)
            df = 30;
        else
        {
            if (di == 31 || (mi == 2 && di > 27))
                di = 30;
            if (df == 31 || (mf == 2 && df > 27))
                df = 30;
        }

        if (Math.Abs(af - ai) == 0)
            result = (mf - mi) * 30 + df - di;
        else
            result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


        return result;
    }

    /// <summary>
    /// Metodo para obtener diferencia de fechas
    /// </summary>
    /// <param name="fechaInicio"></param>
    /// <param name="fechaFin"></param>
    /// <param name="result"></param>
    /// <returns></returns>
    public static bool ObtenerDiferenciaFechas(DateTime fechaInicio, DateTime fechaFin, out string result)
    {
        bool isValid = true;
        result = string.Empty;
        try
        {
            int dias360 = ObtenerdiasEntreFechas360(fechaInicio, fechaFin);

            decimal dias360toMonth = dias360 / 30;

            int meses = (int)(Math.Round(dias360toMonth, 0, MidpointRounding.ToEven));

            //int anio = meses / 12;

            //meses = meses % 12;

            int dias = dias360 - (meses * 30);

            result = "0|" + meses + "|" + dias;
        }
        catch (Exception ex)
        {
            isValid = false;
        }

        return isValid;
    }

    /// <summary>
    /// Cargar Historico de Denuncia a GridView
    /// </summary>
    public void CargarDatosHistoricoDenuncia()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));

        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.grvHistoricoDenuncia.DataSource = vHistorico;
            this.grvHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Lista de Tipos de Documento
    /// </summary>
    public void CargarListaTipoDocumento()
    {
        List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
        TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
        vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
        vTipoDocumento.NombreTipoDocumento = "SELECCIONE";
        vTipoDocumento.Estado = "ACTIVO";
        vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
        vlstTipoDocumentosBien.Add(vTipoDocumento);
        vlstTipoDocumentosBien = vlstTipoDocumentosBien.Where(p => p.Estado == "ACTIVO").ToList();
        vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
        this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
        this.ddlTipoDocumento.SelectedValue = "-1";
        this.ddlTipoDocumento.DataBind();
    }

    /// <summary>
    /// GridView Documentos Solicitados y Recibidos
    /// </summary>
    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
        int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));

        if (vIdContrato != 0)
        {
            List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
            var contrato = vListaContratoDenunciaBien.Find(x => x.IdContrato == vIdContrato);
            var Data = vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(contrato.IdContratoParticipacionEconomica);
            Data = Data.Where(p => p.Estado == "ACTIVO").ToList();
            ViewState["SortedgvwDocumentacionRecibida"] = Data;
            grvDocumentacionRecibida.DataSource = Data;
            grvDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Validación de campos requeridos
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionSolicitadaYRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;

        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Fecha Recibido
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Observaciones Documento Solciitado
            if (this.txtObservacionesSolicitado.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Fecha Recibido
            if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaRecibido.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Nombre Archivo
            if (this.fulArchivoRecibido.FileName == "")
            {
                this.lblCampoRequeridoNombreArchivo.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoNombreArchivo.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Observaciones Documento Recibido
            if (this.txtObservacionesRecibido.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Solicitada
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionSolicitada()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Fecha Recibido
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Observaciones Documento Solciitado
            if (this.txtObservacionesSolicitado.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Recibida
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Fecha Recibido
            if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaRecibido.Visible = false;
                vEsrequerido = false;

                //Validar Campo Fecha Recibido
                if (this.FechaRecibido.Date > DateTime.Now)
                {
                    this.lblValidacionFechaMaxima.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblValidacionFechaMaxima.Visible = false;
                    vEsrequerido = false;
                }

                //Validar Campo Fecha Recibido
                if (this.FechaRecibido.Date < this.FechaSolicitud.Date)
                {
                    this.lblValidacionFechaSolicitud.Visible = true;
                    vEsrequerido = true;
                    count = count + 1;
                }
                else
                {
                    this.lblValidacionFechaSolicitud.Visible = false;
                    vEsrequerido = false;
                }
            }

            //Validar Campo Nombre Archivo
            if (this.fulArchivoRecibido.FileName == "")
            {
                this.lblCampoRequeridoNombreArchivo.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoNombreArchivo.Visible = false;
                vEsrequerido = false;
            }

            //Validar Campo Observaciones Documento Recibido
            if (this.txtObservacionesRecibido.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Actualizar Historial de documentación recibida
    /// </summary>
    public bool ActualizarHistoricoDocumentacionRecibida()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

        int IdHistoricoDocumentoContratoParticipacionEconomica;
        int IdHistoricoDocumentoContratoParticipacionEconomicaDetalle = 0;
        int IdHistoricoDocumentoContratoParticipacionEconomicaNuevo = 0;
        int IdDocumentosSolicitados = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDocumentosSolicitadosDenunciaBienNuevo"));

        if (GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle").ToString() != string.Empty)
        {
            IdHistoricoDocumentoContratoParticipacionEconomicaDetalle = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle"));
        }

        if (GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo").ToString() != string.Empty)
        {
            IdHistoricoDocumentoContratoParticipacionEconomicaNuevo = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo"));
        }

        if (IdHistoricoDocumentoContratoParticipacionEconomicaDetalle != 0)
        {
            IdHistoricoDocumentoContratoParticipacionEconomica = IdHistoricoDocumentoContratoParticipacionEconomicaDetalle;
        }
        else
        {
            IdHistoricoDocumentoContratoParticipacionEconomica = IdHistoricoDocumentoContratoParticipacionEconomicaNuevo;
        }

        HistoricoDocumentosSolicitadosDenunciaBien pHistoricoDocumentos = new HistoricoDocumentosSolicitadosDenunciaBien();

        pHistoricoDocumentos.IdHistoricoDocumentosSolicitadosDenunciaBien = IdHistoricoDocumentoContratoParticipacionEconomica;
        pHistoricoDocumentos.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitados;
        pHistoricoDocumentos.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlTipoDocumento.SelectedValue);
        pHistoricoDocumentos.IdEstadoDocumento = Convert.ToInt32(rbtEstadoDocumento.SelectedValue);
        pHistoricoDocumentos.FechaSolicitud = FechaSolicitud.Date;
        pHistoricoDocumentos.FechaRecibido = FechaRecibido.Date;
        pHistoricoDocumentos.NombreArchivo = fulArchivoRecibido.FileName;
        pHistoricoDocumentos.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaCrea = DateTime.Now;
        pHistoricoDocumentos.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaModifica = DateTime.Now;

        int HistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistoricoDocumentos);
        if (HistoricoDocumentosRecibidos > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Habilitar controles y paneles
    /// </summary>
    public void MostrarPaneles()
    {
        this.pnlInformacionDenuncia.Visible = true;
        this.pnlRelacionarContrato.Visible = true;
        this.pnlInformacionContrato.Visible = true;
        this.pnlDocumentacionSolicitada.Visible = true;
        this.pnlDocumentacionSolicitada.Enabled = true;
        this.pnlDocumentacionRecibida.Enabled = true;
        this.pnlGridDocumentacionRecibida.Visible = true;
        this.FechaRecibido.Enabled = true;
        this.txtObservacionesRecibido.Enabled = true;
    }

    /// <summary>
    /// Deshabilitar controles y paneles
    /// </summary>
    public void HabilitarPaneles()
    {
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlRelacionarContrato.Enabled = false;
        this.pnlInformacionContrato.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = true;
        this.pnlGridDocumentacionRecibida.Enabled = true;

        //this.btnAplicar.Enabled = false;
        //this.ddlTipoDocumento.Enabled = false;
        //this.FechaSolicitud.Enabled = false;
        //this.txtObservacionesSolicitado.Enabled = false;
    }

    /// <summary>
    /// Deshabilitar Campos
    /// </summary>
    protected void DeshabilitarCampos()
    {
        this.txtDescripcion.Enabled = false;
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlInformacionContrato.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = false;
        this.pnlDocumentacionRecibida.Enabled = false;
        this.FechaRecibido.Enabled = false;
        this.fulArchivoRecibido.Enabled = false;
        this.pnlInformacionContrato.Visible = false;
    }

    /// <summary>
    /// Habilitar Campos
    /// </summary>
    public void HabilitarBotones()
    {
        toolBar.MostrarBotonNuevo(true);
        toolBar.MostrarBotonEditar(true);
        toolBar.OcultarBotonGuardar(false);
    }

    #endregion

    /// <summary>
    /// Guarda la información del caudante
    /// </summary>
    private void Guardar()
    {
        try
        {
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            int vResultado;
            string vRuta = string.Empty;
            int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
            List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
            var contrato = vListaContratoDenunciaBien.Find(x => x.IdContrato == vIdContrato);
            List<DocumentosSolicitadosYRecibidos> vListaDocumentos = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["SortedgvwDocumentacionRecibida"];

            if (vListaDocumentos != null)
            {
                List<DocumentosSolicitadosYRecibidos> vListaDocumentosEliminar = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["DocumentosEliminar"];
                if (vListaDocumentosEliminar != null)
                {
                    foreach (DocumentosSolicitadosYRecibidos item in vListaDocumentosEliminar)
                    {
                        if (item.IdDocumentosSolicitadosDenunciaBien > 0)
                        {
                            item.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("ELIMINADO").IdEstadoDocumento;
                            item.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                            item.RutaArchivo = "";
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            this.vMostrencosService.EditarDocumentosSolicitadosYRecibidosDenunciaBien(item);
                        }
                    }
                    this.ViewState["DocumentosEliminar"] = null;
                }
                if (vListaDocumentos != null)
                {
                    foreach (DocumentosSolicitadosYRecibidos item in vListaDocumentos)
                    {
                        if (item.RutaArchivo == null)
                        {
                            if (item.IdDocumentosSolicitadosDenunciaBien <= 0)
                            {
                                if (string.IsNullOrEmpty(item.NombreArchivo))
                                {
                                    item.IdContratoParticipacionEcnomica = contrato.IdContratoParticipacionEconomica;
                                    this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                                    vResultado = AdicionarDocumentosSolicitados(item);
                                    item.IdDocumentosSolicitadosDenunciaBien = vResultado;
                                }
                            }
                        }
                        else
                        {
                            vRuta = Server.MapPath("~/Page/Mostrencos/ContratoParticipacionEconomica/Archivos/" + contrato.IdContratoParticipacionEconomica);
                            if (!string.IsNullOrEmpty(item.RutaArchivo))
                            {
                                if (item.bytes != null)
                                {
                                    if (!System.IO.Directory.Exists(vRuta))
                                    {
                                        System.IO.Directory.CreateDirectory(vRuta);
                                    }

                                    System.IO.File.WriteAllBytes(vRuta + "\\" + item.NombreArchivo, item.bytes);
                                }
                            }
                            else
                            {
                                item.RutaArchivo = string.Empty;
                                item.NombreArchivo = string.Empty;
                            }
                            item.IdContratoParticipacionEcnomica = contrato.IdContratoParticipacionEconomica;
                            vResultado = this.vMostrencosService.EditarDocumentosSolicitadosYRecibidosDenunciaBien(item);

                            HistoricoDocumentosSolicitadosDenunciaBien pHistoricoDocumentos = new HistoricoDocumentosSolicitadosDenunciaBien();

                            pHistoricoDocumentos.IdHistoricoDocumentosSolicitadosDenunciaBien = 0;
                            pHistoricoDocumentos.IdDocumentosSolicitadosDenunciaBien = item.IdDocumentosSolicitadosDenunciaBien;
                            pHistoricoDocumentos.IdTipoDocumentoBienDenunciado = Convert.ToInt32(item.IdTipoDocumentoBienDenunciado);
                            pHistoricoDocumentos.IdEstadoDocumento = Convert.ToInt32(item.IdEstadoDocumento);
                            pHistoricoDocumentos.FechaSolicitud = DateTime.Now;
                            pHistoricoDocumentos.FechaRecibido = DateTime.Now;
                            pHistoricoDocumentos.NombreArchivo = item.NombreArchivo;
                            pHistoricoDocumentos.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                            pHistoricoDocumentos.FechaCrea = DateTime.Now;
                            pHistoricoDocumentos.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                            pHistoricoDocumentos.FechaModifica = DateTime.Now;

                            int HistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistoricoDocumentos);
                        }

                    }

                    RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                    vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(IdDenunciaBien).NombreEstadoDenuncia;
                    bool vResponse = false;
                    if (vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Count > 0)
                    {
                        for (int i = 0; i < vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Count; i++)
                        {
                            if (vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdFase == 4
                                && vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdAccion == 12
                                && vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien[i].IdActuacion == 9)
                            {
                                vResponse = true;
                            }
                        }
                    }

                    int IdHistoricoDocumentosRecibidos = 0;

                    if (vResponse == false)
                    {
                        IdHistoricoDocumentosRecibidos = InsertarHistoricoEstadoDocumentacionRecibida();
                        if (IdHistoricoDocumentosRecibidos > 0)
                        {
                            this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.Mensaje", "La información ha sido guardada exitosamente");
                            this.NavigateTo(SolutionPage.Detail);
                        }
                        else
                            toolBar.MostrarMensajeError("Se ha presentado un error al actualizar");

                    }
                    else
                    {
                        this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.Mensaje", "La información ha sido guardada exitosamente");
                        this.NavigateTo(SolutionPage.Detail);
                    }


                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo que inserta el historico de la documentacion recibida
    /// </summary>
    /// <returns></returns>
    public int InsertarHistoricoEstadoDocumentacionRecibida()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15;
        pHistoricoEstadosDenunciaBien.IdFase = 4;
        pHistoricoEstadosDenunciaBien.IdActuacion = 9;
        pHistoricoEstadosDenunciaBien.Fase = 4.ToString();
        pHistoricoEstadosDenunciaBien.Accion = 9.ToString();
        pHistoricoEstadosDenunciaBien.IdAccion = 12;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, SolutionPage.Add);
        int IdHistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoEstadosDenunciaXContratoParticipacionDocumentacionRecibida(pHistoricoEstadosDenunciaBien);
        return IdHistoricoDocumentosRecibidos;
    }

    /// <summary>
    /// subir archicos y relación de tipo de documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdicionar_Click(object sender, EventArgs e)
    {
        if (txbNumeroContrato.Text != "")
        {

            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/0001"))
            {
                this.CustomValidator4.IsValid = false;
                return;
            }
            else if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/0001"))
            {
                this.CustomValidator1.IsValid = false;
                return;
            }
            else if (ValidarDocumentacionSolicitada() == false)
            {
                toolBar.LipiarMensajeError();
                if (AdicionarDocumentosSolicitadosCache())
                {
                    CargarGrillaDocumentosSolicitadosYRecibidos();
                }
            }
            else
            {
                toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Debe seleccionar un Número de Contrato");
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public int AdicionarDocumentosSolicitados(DocumentosSolicitadosYRecibidos VContratoParticipacionEcnomica)
    {
        try
        {
            int response = 0;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosYRecibidos();
            pDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
            pDocumentosSolicitadosDenunciaBien.IdContratoParticipacionEcnomica = VContratoParticipacionEcnomica.IdContratoParticipacionEcnomica;
            pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = VContratoParticipacionEcnomica.IdTipoDocumentoBienDenunciado;
            pDocumentosSolicitadosDenunciaBien.FechaSolicitud = VContratoParticipacionEcnomica.FechaSolicitud.Date;
            pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = VContratoParticipacionEcnomica.ObservacionesDocumentacionSolicitada;
            pDocumentosSolicitadosDenunciaBien.NombreArchivo = string.Empty;
            pDocumentosSolicitadosDenunciaBien.RutaArchivo = string.Empty;
            pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta = null;
            pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = string.Empty;
            pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento = string.Empty;
            pDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
            pDocumentosSolicitadosDenunciaBien.FechaCrea = DateTime.Now;
            pDocumentosSolicitadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
            pDocumentosSolicitadosDenunciaBien.FechaModifica = DateTime.Now;
            int InsertarDocumentos = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosDenunciaBien);
            if (InsertarDocumentos > 0)
                response = InsertarDocumentos;

            return response;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return 0;
        }
    }

    #region EVENTOS

    /// <summary>
    /// Metodo para el paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvHistoricoDenuncia_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvHistoricoDenuncia.PageIndex = e.NewPageIndex;
        CargarDatosHistoricoDenuncia();
        this.pnlgrvHistoricoDenuncia.CssClass = "popUpHistoricoDenuncia";
        this.pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    /// <summary>
    /// Check Boxes Estado del Documento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.req1.Visible = true;
        this.req3.Visible = true;
    }

    /// <summary>
    /// Metodo del paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvDocumentacionRecibida_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvDocumentacionRecibida.PageIndex = e.NewPageIndex;
        CargarGrillaDocumentosSolicitadosYRecibidos();
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Boton Aplicar Carga Documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicar_Click(object sender, EventArgs e)
    {
        try
        {
            this.btnAplicar.Visible = false;
            this.btnAdicionar.Visible = true;
            this.CapturarValoresDocumentacion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para habilitar el historico de la denuncia
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAbrirHistorico_Click(object sender, EventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.CssClass = "popUpHistoricoDenuncia";
    }

    /// <summary>
    /// Metodo para desabilitar el historico de la denuncia
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCerrarHistorico_Click(object sender, EventArgs e)
    {
        this.pnlgrvHistoricoDenuncia.CssClass = "popUpHistoricoDenuncia hidden";
    }

    /// <summary>
    /// Captura los valores para cargar la documentacion
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            toolBar.LipiarMensajeError();
            var IdDocumento = GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.vIdDocumentosSolicitadosDenunciaBien").GetType().Name;
            if (IdDocumento != "String")
            {
                if (ValidarDocumentacionRecibida() == false)
                {
                    int IdDocumentosSolicitadosDenunciaBien = (int)GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.vIdDocumentosSolicitadosDenunciaBien");
                    int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
                    bool vExiste = false;
                    if (this.fulArchivoRecibido.HasFile)
                    {
                        bool respPdf = this.fulArchivoRecibido.FileName.Contains(".pdf");
                        bool respImg = this.fulArchivoRecibido.FileName.Contains(".jpg");
                        string NombreArchivo = string.Empty;
                        if (respPdf)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }
                        else if (respImg)
                        {
                            int tamanoArchivo = this.fulArchivoRecibido.FileName.Length;
                            NombreArchivo = this.fulArchivoRecibido.FileName.Substring(0, (tamanoArchivo - 4));
                        }

                        if (NombreArchivo.Length > 50)
                        {
                            throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                        }
                        DocumentosSolicitadosYRecibidos vDocumentoCargado = new DocumentosSolicitadosYRecibidos();
                        this.CargarArchivo(vDocumentoCargado);
                        this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", vDocumentoCargado);
                    }
                    DocumentosSolicitadosYRecibidos vDocumentacionSolicitada = new DocumentosSolicitadosYRecibidos();
                    List<DocumentosSolicitadosYRecibidos> vListaDocumentacion = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["SortedgvwDocumentacionRecibida"];
                    if (vListaDocumentacion == null)
                    {
                        vListaDocumentacion = new List<DocumentosSolicitadosYRecibidos>();
                    }
                    if (vListaDocumentacion.Count() > 0)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El Registro ya existe");
                            vExiste = true;
                        }
                        else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.EsNuevo)).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El Registro ya existe");
                            vExiste = true;
                        }
                        else if (IdDocumentosSolicitadosDenunciaBien != 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue) && p.IdDocumentosSolicitadosDenunciaBien != IdDocumentosSolicitadosDenunciaBien).Count() > 0)
                        {
                            this.toolBar.MostrarMensajeError("El Registro ya existe");
                            vExiste = true;
                        }
                    }

                    if (!vExiste)
                    {
                        if (IdDocumentosSolicitadosDenunciaBien > 0)
                        {
                            bool vArchivoEsValido = false;
                            if (!string.IsNullOrEmpty(this.rbtEstadoDocumento.SelectedValue))
                            {
                                vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                if (vDocumentacionSolicitada == null)
                                    vDocumentacionSolicitada = new DocumentosSolicitadosYRecibidos();

                                vArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);

                                if (vArchivoEsValido)
                                {
                                    if (IdDocumentosSolicitadosDenunciaBien == 0)
                                    {
                                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                                vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                    }
                                    else
                                    {
                                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitadosDenunciaBien;
                                    }

                                    vDocumentacionSolicitada.FechaRecibido = this.FechaRecibido.Date;
                                    vDocumentacionSolicitada.IdEstadoDocumento = Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue);
                                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                                    vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                                    vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                                    vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                                    vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                    this.SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado", null);
                                }
                            }
                            else
                            {
                                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                                vDocumentacionSolicitada.EsNuevo = IdDocumentosSolicitadosDenunciaBien == 0;
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                            vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                            }

                            if (vArchivoEsValido)
                            {
                                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.ToUpper();
                                if (!vDocumentacionSolicitada.EsNuevo)
                                {
                                    DocumentosSolicitadosYRecibidos vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == IdDocumentosSolicitadosDenunciaBien).FirstOrDefault();
                                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                                    vListaDocumentacion.Remove(vDocumentacion);
                                    vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                                }
                                vListaDocumentacion.Add(vDocumentacionSolicitada);
                                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                                CargarGrillaDocumentos();
                                this.txtObservacionesSolicitado.Enabled = true;
                                this.FechaSolicitud.Enabled = true;
                                this.ddlTipoDocumento.Enabled = true;

                                this.txtObservacionesSolicitado.Text = string.Empty;
                                this.FechaSolicitud.InitNull = true;
                                this.ddlTipoDocumento.SelectedValue = "-1";
                                this.lblValidacionFechaMaxima.Visible = false;
                                this.lblValidacionFechaSolicitud.Visible = false;
                                this.rbtEstadoDocumento.ClearSelection();
                                this.FechaRecibido.InitNull = true;
                                this.txtObservacionesRecibido.Text = string.Empty;
                            }
                        }
                    }
                }
            }
            else
            {
                if (ValidarDocumentacionSolicitada() == false)
                {
                    toolBar.LipiarMensajeError();
                    if (AdicionarDocumentosSolicitadosCache())
                    {
                        CargarGrillaDocumentosSolicitadosYRecibidos();
                        this.ddlTipoDocumento.SelectedValue = "-1";
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitadosCache()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            bool vExiste = false;
            DocumentosSolicitadosYRecibidos vDocumentacionSolicitada = new DocumentosSolicitadosYRecibidos();
            List<DocumentosSolicitadosYRecibidos> vListaDocumentacion = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosYRecibidos item in vListaDocumentacion)
                {
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(Convert.ToInt32(this.ddlTipoDocumento.SelectedValue));
                    vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                    if (item.NombreTipoDocumento == vDocumentacionSolicitada.NombreTipoDocumento)
                    {
                        this.toolBar.MostrarMensajeError("El Registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.NombreEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.NombreEstadoDocumento;
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentacionSolicitada.NombreArchivo = string.Empty;
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = 0;
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrillaDocumentos();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }

            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosYRecibidos pDocumentoSolicitado)
    {
        try
        {
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
            List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
            var contrato = vListaContratoDenunciaBien.Find(x => x.IdContrato == vIdContrato);

            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Megas");
                }

                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = contrato.IdContratoParticipacionEconomica + "/" + this.fulArchivoRecibido.FileName;
                return true;
            }
            else
            {
                DocumentosSolicitadosYRecibidos vDocumento = this.GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.FileApoderado") as DocumentosSolicitadosYRecibidos;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }

            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Carga la grilla de la documentacion
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitados = new List<DocumentosSolicitadosYRecibidos>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int vIdContrato = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdContrato"));
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
                var contrato = vListaContratoDenunciaBien.Find(x => x.IdContrato == vIdContrato);
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(contrato.IdContratoParticipacionEconomica);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlDocumentacionSolicitada.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosYRecibidos>() : vListaDocumentosSolicitados;
                this.grvDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.grvDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlDocumentacionSolicitada.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Link para visualización de Archivos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkVerArchivo_Click(object sender, EventArgs e)
    {
        try
        {
            bool vVerArchivo = false;
            string vRuta = string.Empty;
            LinkButton imageButton = (LinkButton)sender;
            TableCell tableCell = (TableCell)imageButton.Parent;
            GridViewRow row = (GridViewRow)tableCell.Parent;
            grvDocumentacionRecibida.SelectedIndex = row.RowIndex;
            int fila = row.RowIndex;
            int vIdDocumentoSolicitado = Convert.ToInt32(this.grvDocumentacionRecibida.DataKeys[fila].Value);
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
            DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdDocumentoSolicitado);
            int IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
            List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);
            var contrato = vListaContratoDenunciaBien.Find(x => x.IdContrato == vIdContrato);
            vVerArchivo = File.Exists(Server.MapPath("../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo;
            if (vVerArchivo)
            {
                this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                this.pnlArchivo.CssClass = "popuphIstorico";
                this.pnlArchivo.EnableViewState = true;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
            }
            else
            {
                this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                this.pnlArchivo.CssClass = "popuphIstorico";
                this.imgDodumento.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// descarga el archivo que se esta visualizando
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            bool vDescargar = false;
            string vRuta = string.Empty;
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdArchivo);
            vDescargar = File.Exists(Server.MapPath("../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo));
            vRuta = "../ContratoParticipacionEconomica/Archivos/" + vArchivo.RutaArchivo;
            if (vDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                HttpContext.Current.Response.TransmitFile(path);
                HttpContext.Current.Response.End();
            }
            this.pnlArchivo.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Boton Editar Documento GridView
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditarDocumento_Click(object sender, EventArgs e)
    {
        this.pnlDocumentacionRecibida.Visible = true;
        this.btnAplicar.Enabled = true;
        this.btnAplicar.Visible = true;
        this.btnAdicionar.Visible = false;
        ImageButton vEditDocumento = (ImageButton)sender;
        string[] ArrayEditDocumento = vEditDocumento.CommandArgument.Split('|');
        int vIdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(ArrayEditDocumento[0]);
        SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.vIdDocumentosSolicitadosDenunciaBien", vIdDocumentosSolicitadosDenunciaBien);
        var vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        var Tipo = vListaDocumentosSolicitadosYRecibidos.Find(x => x.NombreTipoDocumento == ArrayEditDocumento[1]);
        if (Tipo != null)
        {
            ddlTipoDocumento.SelectedValue = Tipo.IdTipoDocumentoBienDenunciado.ToString();
            FechaSolicitud.Date = Convert.ToDateTime(ArrayEditDocumento[2]);
            txtObservacionesSolicitado.Text = ArrayEditDocumento[3];
            if (ArrayEditDocumento[5] != "")
            {
                FechaRecibido.Date = Convert.ToDateTime(ArrayEditDocumento[5]);
            }
            else
            {
                FechaRecibido.Date = DateTime.Now;
            }
            lblNombreArchivo.Text = ArrayEditDocumento[6];
            txtObservacionesRecibido.Text = ArrayEditDocumento[7];
            lblNombreArchivo.Visible = true;
            btnAplicar.Visible = true;
            pnlArchivo.CssClass = "popuphIstorico hidden";
        }
    }

    /// <summary>
    /// Boton Eliminar Documento Gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminarDocumento_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton vIdDeleteDocumento = (ImageButton)sender;
            DocumentosSolicitadosDenunciaBien vdo = new DocumentosSolicitadosDenunciaBien();
            vdo.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(vIdDeleteDocumento.CommandArgument);
            List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitados = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["SortedgvwDocumentacionRecibida"];
            List<DocumentosSolicitadosYRecibidos> vListaDocumentosEliminar = (List<DocumentosSolicitadosYRecibidos>)this.ViewState["DocumentosEliminar"];
            List<DocumentosSolicitadosYRecibidos> vListaDocumentosEliminarMemoria = new List<DocumentosSolicitadosYRecibidos>();
            foreach (var item in vListaDocumentosSolicitados)
            {
                if (item.IdDocumentosSolicitadosDenunciaBien == vdo.IdDocumentosSolicitadosDenunciaBien)
                {
                    item.NombreEstadoDocumento = "ELIMINADO";
                }
            }
            if (vListaDocumentosEliminar != null)
            {
                vListaDocumentosEliminarMemoria = vListaDocumentosSolicitados.Where(p => p.NombreEstadoDocumento.ToUpper() == "ELIMINADO").ToList();
                vListaDocumentosEliminar.Add(vListaDocumentosEliminarMemoria.FirstOrDefault());
            }
            else
            {
                vListaDocumentosEliminar = vListaDocumentosSolicitados.Where(p => p.NombreEstadoDocumento.ToUpper() == "ELIMINADO").ToList();
            }
            this.ViewState["DocumentosEliminar"] = vListaDocumentosEliminar;
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
            grvDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
            grvDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Boton Nuevo ToolBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Boton Guardar ToolBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_ClickBK(object sender, EventArgs e)
    {
        try
        {
            if (txbNumeroContrato.Text != "")
            {
                int vIdContrato = Convert.ToInt32(GetSessionParameter("ContratoParticipacionEconomica.IdContrato"));
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                var ExisteDocumentos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                var validaDocumentos = ExisteDocumentos.Find(x => x.FechaRecibido == null || x.ObservacionesDocumentacionRecibida == null);
                List<ContratoParticipacionEconomica> vlstContratoParticipacionEconomica = this.vMostrencosService.ConsultarExistenciaContratoXDenuncia(vIdDenunciaBien, vIdContrato);
                if (vlstContratoParticipacionEconomica.Count() > 0)
                {
                    if (ExisteDocumentos.Count() > 0)
                    {
                        if (validaDocumentos == null)
                        {
                            foreach (var item in ExisteDocumentos)
                            {
                                int IdHistoricoDocumentoContratoParticipacionEconomica;
                                int IdHistoricoDocumentoContratoParticipacionEconomicaDetalle = 0;
                                int IdHistoricoDocumentoContratoParticipacionEconomicaNuevo = 0;
                                int IdDocumentosSolicitados = item.IdDocumentosSolicitadosDenunciaBien;
                                if (GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle").ToString() != string.Empty)
                                {
                                    IdHistoricoDocumentoContratoParticipacionEconomicaDetalle = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarInformedeldenunciante.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle"));
                                }
                                if (GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo").ToString() != string.Empty)
                                {
                                    IdHistoricoDocumentoContratoParticipacionEconomicaNuevo = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo"));
                                }
                                if (IdHistoricoDocumentoContratoParticipacionEconomicaDetalle != 0)
                                {
                                    IdHistoricoDocumentoContratoParticipacionEconomica = IdHistoricoDocumentoContratoParticipacionEconomicaDetalle;
                                }
                                else
                                {
                                    IdHistoricoDocumentoContratoParticipacionEconomica = IdHistoricoDocumentoContratoParticipacionEconomicaNuevo;
                                }
                                HistoricoDocumentosSolicitadosDenunciaBien pHistoricoDocumentos = new HistoricoDocumentosSolicitadosDenunciaBien();
                                pHistoricoDocumentos.IdHistoricoDocumentosSolicitadosDenunciaBien = IdHistoricoDocumentoContratoParticipacionEconomica;
                                pHistoricoDocumentos.IdDocumentosSolicitadosDenunciaBien = IdDocumentosSolicitados;
                                pHistoricoDocumentos.IdTipoDocumentoBienDenunciado = Convert.ToInt32(item.IdTipoDocumentoBienDenunciado);
                                pHistoricoDocumentos.IdEstadoDocumento = Convert.ToInt32(item.IdEstadoDocumento);
                                pHistoricoDocumentos.FechaSolicitud = DateTime.Now;
                                pHistoricoDocumentos.FechaRecibido = DateTime.Now;
                                pHistoricoDocumentos.NombreArchivo = item.NombreArchivo;
                                pHistoricoDocumentos.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                                pHistoricoDocumentos.FechaCrea = DateTime.Now;
                                pHistoricoDocumentos.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                                pHistoricoDocumentos.FechaModifica = DateTime.Now;
                                int HistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistoricoDocumentos);
                            }
                            String ok = "La información ha sido guardada exitosamente";
                            Response.Redirect("../../Mostrencos/ContratoParticipacionEconomica/List.aspx?ok=" + ok, false);
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("Hay documentos en la lista que aún no tienen archivos adjuntos");
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("Debe Agregar al menos un tipo de documento");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        this.Guardar();
    }

    /// <summary>
    /// Boton Consultar ToolBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Boton Retornar ToolBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Boton Editar ToolBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }

    #endregion
}