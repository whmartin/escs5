﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_ContratoParticipacionEconomica_List" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">



    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de la denuncia
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado de la denuncia *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha de radicado de la denuncia *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Radicado en correspondencia *             
                        </td>
                        <td class="auto-style1"></td>
                        <td>Fecha radicado en correspondencia  *            
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>&nbsp; Tipo de Identificación *
                        </td>
                        <td class="auto-style1"></td>
                        <td>Número de identificación *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                        <td class="auto-style1"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">

                        <tr class="rowB">
                            <td>&nbsp; Primer nombre *
                            </td>
                            <td class="auto-style1">&nbsp;</td>
                            <td>Segundo nombre *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>&nbsp; Primer apellido *
                            </td>
                            <td class="auto-style1"></td>
                            <td>Segundo apellido *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="auto-style1"></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>

               <asp:Panel runat="server" ID="PanelRazonSocial">

                        <tr class="rowB">
                            <td colspan="2">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">&nbsp;&nbsp;Descripción de la denuncia *
                <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                    ValidationGroup="btnEditar" runat="server" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 55%">Histórico de la denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">&nbsp;
                            <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlgrvContratoParticipacion">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="3" class="tdTitulos">Información de Contrato</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:GridView runat="server" ID="grvContratoParticipacion" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" ShowHeader="true" Visible="true" DataKeyNames="IdContrato" CellPadding="0" Height="16px" PageSize="10" OnSelectedIndexChanged="grvContratoParticipacion_SelectedIndexChanged"
                                OnPageIndexChanging="grvContratoParticipacion_PageIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                    <asp:BoundField HeaderText="Regional" DataField="NombreRegional" />
                                    <asp:BoundField HeaderText="Número de Contrato" DataField="NumeroContrato" />
                                    <asp:BoundField HeaderText="Fecha de Suscripción" DataField="FechaSuscripcion" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Fecha de Inicio" DataField="FechaInicioEjecucion" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Fecha de Terminación Inicial" DataField="FechaTerminacionInicial" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Fecha de Terminación Final" DataField="FechaTerminacionFinal" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField HeaderText="Estado del Contrato" DataField="EstadoContrato" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron contratos asociados a la denuncia
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


