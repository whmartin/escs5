﻿//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs</summary>
// <author>INGENIAN</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_ContratoParticipacionEconomica_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ContratoParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Id Contrato
    /// </summary>
    private int IdContrat;

    /// <summary>
    /// Valida documentación previo a Guardar
    /// </summary>
    public bool valida;

    /// <summary>
    /// File Name
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    #region METODOS

    /// <summary>
    /// Metodo Inicial
    /// </summary>
    public void Iniciar()
    {
        toolBar = (masterPrincipal)this.Master;
        toolBar.EstablecerTitulos("Contrato Participación Económica");
        toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
        toolBar.eventoBuscar += btnConsultar_Click;
        toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
        toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
        toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
        toolBar.MostrarBotonNuevo(false);
        toolBar.MostrarBotonEditar(false);
        this.pnlRelacionarContrato.Visible = true;
        this.pnlDocumentacionSolicitada.Visible = true;
        this.pnlGridDocumentacionRecibida.Visible = true;
        HabilitarPaneles();
        CargarDatosHistoricoDenuncia();
        toolBar.LipiarMensajeError();
    }

    /// <summary>
    /// Metodo carga los datos iniciales de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            HabilitarPaneles();
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                if (GetSessionParameter("DocumentarBienDenunciado.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("DocumentarBienDenunciado.Guardado");

                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m.") && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;
                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.txtNumeroIdentificacion.Text += "-" + vDenunciaBien.Tercero.DigitoVerificacion.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }
                List<Regionales> vlstRegionales = null;
                Regionales vTipoRegionales = new Regionales();
                vTipoRegionales.CodigoRegional = "-1";
                vTipoRegionales.NombreRegional = "Seleccione";
                vlstRegionales = vMostrencosService.ConsultarRegionalesAll();
                vlstRegionales.Add(vTipoRegionales);
                this.ddlRegionalBusqueda.DataSource = vlstRegionales;
                this.ddlRegionalBusqueda.SelectedValue = "-1";
                this.ddlRegionalBusqueda.DataBind();

                List<Vigencias> vlstVigencias = null;
                Vigencias vTipoVigencias = new Vigencias();
                vTipoVigencias.IdVigencia = -1;
                vTipoVigencias.AcnoVigencia = "Seleccione";
                vlstVigencias = vMostrencosService.ConsultarVigenciasAll();
                vlstVigencias.Add(vTipoVigencias);

                this.ddlVigenciaBusqueda.DataSource = vlstVigencias;
                this.ddlVigenciaBusqueda.SelectedValue = "-1";
                this.ddlVigenciaBusqueda.DataBind();

                List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
                TipoDocumentoBienDenunciado vTipoDocumento = new TipoDocumentoBienDenunciado();
                vTipoDocumento.IdTipoDocumentoBienDenunciado = -1;
                vTipoDocumento.NombreTipoDocumento = "SELECCIONE";
                vTipoDocumento.Estado = "ACTIVO";
                vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
                vlstTipoDocumentosBien.Add(vTipoDocumento);
                vlstTipoDocumentosBien = vlstTipoDocumentosBien.Where(p => p.Estado == "ACTIVO").ToList();
                vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
                this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.ddlTipoDocumento.DataBind();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo que carga la grilla historico e la denuncia
    /// </summary>
    public void CargarDatosHistoricoDenuncia()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));

        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.grvHistoricoDenuncia.DataSource = vHistorico;
            this.grvHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Validación de campos obligatorios Documentos
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionSolicitadaYRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }

            //Validar Fecha Recibido
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            //Validar Observaciones Documento Solciitado
            if (this.txtObservacionesSolicitado.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Fecha Recibido
            if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaRecibido.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Nombre Archivo
            if (this.fulArchivoRecibido.FileName == "")
            {
                this.lblCampoRequeridoNombreArchivo.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoNombreArchivo.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Observaciones Documento Recibido
            if (this.txtObservacionesRecibido.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = false;
                vEsrequerido = false;
            }
            if (count > 0)
            {
                vEsrequerido = true;
                valida = false;
            }
            else
            {
                vEsrequerido = false;
                valida = true;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Solicitada
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionSolicitada()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            //Validar Fecha Recibido
            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            //Validar Observaciones Documento Solciitado
            if (this.txtObservacionesSolicitado.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoSolicitado.Visible = false;
                vEsrequerido = false;
            }
            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Validar Documentación Recibida
    /// </summary>
    /// <returns></returns>
    private bool ValidarDocumentacionRecibida()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Estado Documento
            if (this.rbtEstadoDocumento.SelectedValue == string.Empty)
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoEstadoDocumento.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Fecha Recibido
            if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblCampoRequeridoFechaRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoFechaRecibido.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Nombre Archivo
            if (this.fulArchivoRecibido.FileName == "")
            {
                this.lblCampoRequeridoNombreArchivo.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoNombreArchivo.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Observaciones Documento Recibido
            if (this.txtObservacionesRecibido.Text == "")
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoObservacionesDocumentoRecibido.Visible = false;
                vEsrequerido = false;
            }
            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Relación de documentos a la solicitud
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumento()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosYRecibidos();
        List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(vIdDenunciaBien);
        var Data = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        var count = Data.Find(x => x.NombreTipoDocumento == ddlTipoDocumento.SelectedItem.Text);
        if (count != null)
        {
            toolBar.MostrarMensajeError("El tipo de documento que intenta adicionar ya se encuentra relacionado");
            return false;
        }
        else
        {
            if (GuardarArchivos(vIdDenunciaBien, 0) == true)
            {
                pDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
                pDocumentosSolicitadosDenunciaBien.IdCausante = 74;
                pDocumentosSolicitadosDenunciaBien.IdApoderado = 67;
                pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoSoporteDenuncia = Convert.ToInt32(ddlTipoDocumento.SelectedValue);
                pDocumentosSolicitadosDenunciaBien.FechaSolicitud = FechaSolicitud.Date;
                pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = txtObservacionesSolicitado.Text;
                pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = Convert.ToInt32(rbtEstadoDocumento.SelectedValue);
                pDocumentosSolicitadosDenunciaBien.FechaRecibido = FechaRecibido.Date;
                pDocumentosSolicitadosDenunciaBien.NombreArchivo = fulArchivoRecibido.FileName.Replace(".pdf", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf").Replace(".PDF", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                pDocumentosSolicitadosDenunciaBien.RutaArchivo = vIdDenunciaBien + @"/" + fulArchivoRecibido.FileName.Replace(".pdf", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf").Replace(".PDF", DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = txtObservacionesRecibido.Text;
                pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento = txtObservacionesRecibido.Text;
                pDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pDocumentosSolicitadosDenunciaBien.FechaCrea = DateTime.Now;
                pDocumentosSolicitadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                pDocumentosSolicitadosDenunciaBien.FechaModifica = DateTime.Now;
                int InsertarDocumentos = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosDenunciaBien);
                InsertarHistoricoDocumentacionRecibida(pDocumentosSolicitadosDenunciaBien.IdDocumentosSolicitadosDenunciaBien);
            }
            return true;
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitados(int IdContratoParticipacionEcnomica)
    {
        try
        {
            bool response = false;
            int cont = 0;
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            //Consultar documentos cargados en la grilla
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            List<DocumentosSolicitadosYRecibidos> vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(IdContratoParticipacionEcnomica);
            foreach (var item in vListaDocumentosSolicitados)
            {
                var count = vListaDocumentosSolicitadosYRecibidos.Find(x => x.NombreTipoDocumento == item.NombreTipoDocumento);
                if (count != null)
                {

                }
                else
                {
                    DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosYRecibidos();
                    pDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
                    pDocumentosSolicitadosDenunciaBien.IdContratoParticipacionEcnomica = IdContratoParticipacionEcnomica;
                    pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = item.IdTipoDocumentoBienDenunciado;
                    pDocumentosSolicitadosDenunciaBien.FechaSolicitud = item.FechaSolicitud.Date;
                    pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = item.ObservacionesDocumentacionSolicitada;
                    pDocumentosSolicitadosDenunciaBien.NombreArchivo = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.RutaArchivo = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.DocumentacionCompleta = null;
                    pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento = string.Empty;
                    pDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pDocumentosSolicitadosDenunciaBien.FechaCrea = DateTime.Now;
                    pDocumentosSolicitadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
                    pDocumentosSolicitadosDenunciaBien.FechaModifica = DateTime.Now;
                    int InsertarDocumentos = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosDenunciaBien);
                    cont += 1;
                }
            }
            if (vListaDocumentosSolicitados.Count() == cont)
                response = true;

            return response;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Adicionar Documentos Solicitados
    /// </summary>
    /// <returns></returns>
    public bool AdicionarDocumentosSolicitadosCache()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue))
                    {
                        this.toolBar.MostrarMensajeError("El Registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.NombreEstado = vDocumentacionSolicitada.EstadoDocumento.NombreEstadoDocumento;
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrilla();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }

            return vExiste;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGridDocumentacionRecibida.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                if (vDocumentoSolicitado.EsNuevo)
                {
                    vListaDocumento.Remove(vDocumentoSolicitado);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                }
                else
                {
                    List<DocumentosSolicitadosDenunciaBien> vDocumentosEliminar = new List<DocumentosSolicitadosDenunciaBien>();
                    vListaDocumento.Remove(vDocumentoSolicitado);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                    vDocumentosEliminar.Add(vDocumentoSolicitado);
                    this.ViewState["DocumentosEliminar"] = vDocumentosEliminar;
                }
                this.CargarGrillaDocumentosSolicitadosYRecibidos();
            }

            if (e.CommandName.Equals("Editar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }

                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }

        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Metodo que se utiliza para guardar el documento
    /// </summary>
    /// <param name="pDocumento"></param>
    public void GuardarDocumento(DocumentosSolicitadosDenunciaBien pDocumento)
    {
        try
        {
            if (pDocumento.IdDocumentosSolicitadosDenunciaBien == 0)
            {
                vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(pDocumento);
                CargarGrillaDocumentosSolicitadosYRecibidos();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// GridView Documentación Recivida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        CargarGrillaDocumentosSolicitadosYRecibidos();
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// GridView Documentos Solicitados
    /// </summary>
    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (((List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"]).Count == 0)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGridDocumentacionRecibida.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGridDocumentacionRecibida.Visible = false;
            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    /// <summary>
    /// Crear Directorio y Subir Archivos
    /// </summary>
    /// <param name="pIdDenunciaBien">No Denuncia</param>
    /// <param name="pPosition">Indice</param>
    /// <returns></returns>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        {
            try
            {
                HttpFileCollection files = Request.Files;
                if (files.Count > 0)
                {
                    string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                    string extPdf = ".PDF".ToLower();
                    if (extFile != extPdf)
                    {
                        throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
                    }
                    int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                    if (vtamArchivo > 2048)
                    {
                        throw new Exception("El tamaño del archivo PDF permitido es de 2.048 KB");
                    }
                    HttpPostedFile file = files[pPosition];

                    if (file.ContentLength > 0)
                    {
                        string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                        string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");

                        bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                        if (!existeCarpeta)
                        {
                            System.IO.Directory.CreateDirectory(carpetaBase);
                        }

                        this.vFileName = rutaParcial;
                        file.SaveAs(filePath);
                    }
                }

                return true;
            }
            catch (HttpRequestValidationException)
            {
                throw new Exception("Se detectó un posible archivo peligroso.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    /// <summary>
    /// Metodo para insertar el historico de documentacion recibida
    /// </summary>
    /// <param name="vIdDocumentosSolicitadosDenunciaBien"></param>
    public void InsertarHistoricoDocumentacionRecibida(int vIdDocumentosSolicitadosDenunciaBien)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoDocumentosSolicitadosDenunciaBien pHistoricoDocumentos = new HistoricoDocumentosSolicitadosDenunciaBien();
        pHistoricoDocumentos.IdDocumentosSolicitadosDenunciaBien = vIdDocumentosSolicitadosDenunciaBien;
        pHistoricoDocumentos.IdTipoDocumentoSoporteDenuncia = Convert.ToInt32(ddlTipoDocumento.SelectedValue);
        pHistoricoDocumentos.IdEstadoDocumento = null;
        pHistoricoDocumentos.FechaSolicitud = FechaSolicitud.Date;
        pHistoricoDocumentos.FechaRecibido = FechaRecibido.Date;
        pHistoricoDocumentos.NombreArchivo = fulArchivoRecibido.FileName;
        pHistoricoDocumentos.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaCrea = DateTime.Now;
        pHistoricoDocumentos.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoDocumentos.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoDocumentos, this.PageName, SolutionPage.Add);
        int HistoricoDocumentosRecibidos = this.vMostrencosService.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistoricoDocumentos);
        SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienNuevo", pHistoricoDocumentos.IdHistoricoDocumentosSolicitadosDenunciaBien);
        RemoveSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdHistoricoDocumentosSolicitadosDenunciaBienDetalle");
    }

    /// <summary>
    /// Insertar historico de participacion economica
    /// </summary>
    /// <returns></returns>
    public int InsertarHistoricoContratoParticipacionEconomica()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, SolutionPage.Add);
        int HistoricoContratoParticipacionEconomica = this.vMostrencosService.InsertarHistoricoEstadosDenunciaXContratoParticipacion(pHistoricoEstadosDenunciaBien);
        return HistoricoContratoParticipacionEconomica;
    }

    /// <summary>
    /// Metodo pa insertar el contrato de participacion economica
    /// </summary>
    /// <returns></returns>
    public int InsertarContratoParticipacionEconomica()
    {
        int vIdContrato = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdContrato"));
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien"));
        ContratoParticipacionEconomica pContratoParticipacionEconomica = new ContratoParticipacionEconomica();
        int contrato = IdContrat;
        pContratoParticipacionEconomica.IdDenunciaBien = vIdDenunciaBien;
        pContratoParticipacionEconomica.IdContrato = vIdContrato;
        pContratoParticipacionEconomica.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pContratoParticipacionEconomica.FechaCrea = DateTime.Now;
        pContratoParticipacionEconomica.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pContratoParticipacionEconomica.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pContratoParticipacionEconomica, this.PageName, SolutionPage.Add);
        int InsertarContratoParticipacionEconomica = this.vMostrencosService.InsertarContratoParticipacionEconomica(pContratoParticipacionEconomica);
        return InsertarContratoParticipacionEconomica;
    }

    /// <summary>
    /// Validación de campos en la ventana modal
    /// </summary>
    /// <returns></returns>
    private bool ValidarCamposPopUp()
    {
        bool vEsrequerido = false;
        int count = 0;
        try
        {
            //Validar Campo Vigencia
            if (this.ddlVigenciaBusqueda.SelectedValue == "-1")
            {
                this.lblCampoRequeridoVigencia.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoVigencia.Visible = false;
                vEsrequerido = false;
            }
            //Validar Campo Regional
            if (this.ddlRegionalBusqueda.SelectedValue == "-1")
            {
                this.lblCampoRequeridoRegional.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoRegional.Visible = false;
                vEsrequerido = false;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    /// <summary>
    /// Activar controles y paneles en la vista
    /// </summary>
    public void HabilitarPaneles()
    {
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlRelacionarContrato.Enabled = true;
        this.pnlInformacionContrato.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = true;
        this.pnlDocumentacionRecibida.Enabled = false;
        this.txbInformacionContrato.Enabled = false;
        this.FechaRecibido.Enabled = true;
        this.fulArchivoRecibido.Enabled = true;
        this.pnlInformacionContrato.Visible = false;
        this.FechaRecibido.Enabled = false;
        this.fulArchivoRecibido.Enabled = false;
    }

    /// <summary>
    /// Deshabilitar controles y paneles en la vista
    /// </summary>
    protected void DeshabilitarCampos()
    {
        this.txtDescripcion.Enabled = false;
        this.pnlInformacionDenuncia.Enabled = false;
        this.pnlInformacionContrato.Enabled = false;
        this.pnlDocumentacionRecibida.Enabled = false;
        this.pnlDocumentacionSolicitada.Enabled = false;
        this.FechaRecibido.Enabled = false;
        this.FechaSolicitud.Enabled = false;
        this.fulArchivoRecibido.Enabled = false;
        this.pnlInformacionContrato.Visible = false;
    }

    /// <summary>
    /// Metodo para habiliatar botones
    /// </summary>
    public void HabilitarBotones()
    {
        toolBar.MostrarBotonNuevo(true);
        toolBar.MostrarBotonEditar(true);
        toolBar.OcultarBotonGuardar(false);
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Metodo del paginador 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvHistoricoDenuncia_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvHistoricoDenuncia.PageIndex = e.NewPageIndex;
        CargarDatosHistoricoDenuncia();
        pnlArchivo.CssClass = "popuphIstorico";
    }

    /// <summary>
    /// Metodo consultr imagen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultarVentanaModal_Click(object sender, ImageClickEventArgs e)
    {
        this.lblCampoRequeridoInformacionContrato.Visible = false;
        if (ValidarCamposPopUp() == false)
        {
            List<ContratoParticipacionEconomica> vlstContratoParticipacionEconomicaMultiplesCriterios = this.vMostrencosService.ConsultarContratoXDenunciaMultiplesParametros(Convert.ToInt32(ddlVigenciaBusqueda.SelectedValue), Convert.ToInt32(ddlRegionalBusqueda.SelectedValue), txbNumeroContratoBusqueda.Text);
            if (vlstContratoParticipacionEconomicaMultiplesCriterios.Count() > 0)
            {
                grvBusquedaContratos.DataSource = vlstContratoParticipacionEconomicaMultiplesCriterios;
                grvBusquedaContratos.DataBind();
            }
            else
            {
                this.grvBusquedaContratos.PageSize = PageSize();
                this.grvBusquedaContratos.EmptyDataText = EmptyDataText();
                this.grvBusquedaContratos.Visible = true;
                this.grvBusquedaContratos.DataBind();
            }
        }
        modalPopupInformacionContrato.Show();
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    /// <summary>
    /// Metodo 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvBusquedaContratos_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvBusquedaContratos.PageIndex = e.NewPageIndex;
        List<ContratoParticipacionEconomica> vlstContratoParticipacionEconomicaMultiplesCriterios = this.vMostrencosService.ConsultarContratoXDenunciaMultiplesParametros(Convert.ToInt32(ddlVigenciaBusqueda.SelectedValue), Convert.ToInt32(ddlRegionalBusqueda.SelectedValue), txbNumeroContratoBusqueda.Text);
        if (vlstContratoParticipacionEconomicaMultiplesCriterios.Count() > 0)
        {
            grvBusquedaContratos.DataSource = vlstContratoParticipacionEconomicaMultiplesCriterios;
            grvBusquedaContratos.DataBind();
        }
        modalPopupInformacionContrato.Show();
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    /// <summary>
    /// Metodo selecionar registro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvBusquedaContratos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(grvBusquedaContratos.SelectedRow);
        pnlArchivo.CssClass = "popuphIstorico hidden";
    }

    /// <summary>
    /// Metodo para cargar lainformacion del registro cargado
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        int rowIndex = pRow.RowIndex;
        string strValueIdContrato;
        try
        {
            if (grvBusquedaContratos.DataKeys[rowIndex].Values["IdContrato"] != null)
            {
                strValueIdContrato = grvBusquedaContratos.DataKeys[rowIndex].Values["IdContrato"].ToString();
                IdContrat = int.Parse(grvBusquedaContratos.DataKeys[rowIndex].Values["IdContrato"].ToString());
                SetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdContrato", strValueIdContrato);
            }

            var lbVigencia = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbVigencia");
            txbVigencia.Text = lbVigencia.Text;
            var lbRegional = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbRegional");
            txbRegional.Text = lbRegional.Text;

            var lbNumeroContrato = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbNumeroContrato");
            txbNumeroContrato.Text = lbNumeroContrato.Text == "&nbsp;" ? "" : lbNumeroContrato.Text;

            var lbFechaSuscripcion = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbFechaSuscripcion");
            txtFechaSuscripcion.Text = lbFechaSuscripcion.Text;

            var lbFechaInicioEjecucion = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbFechaInicioEjecucion");
            txtFechaInicio.Text = lbFechaInicioEjecucion.Text;

            var lbFechaTerminacionInicial = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbFechaTerminacionInicial");
            txtFechaTerminiacionInicial.Text = lbFechaTerminacionInicial.Text;

            var lbFechaTerminacionFinal = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbFechaTerminacionFinal");
            txtFechaTerminacionFinal.Text = lbFechaTerminacionFinal.Text;

            //Validacion Como esta en Contratos
            if (lbFechaTerminacionFinal.Text != null)
            {
                string sFechaTerminacionFinal = lbFechaTerminacionFinal.Text.Substring(0, 10);
                string sFechaInicioEjecucion = lbFechaInicioEjecucion.Text.Substring(0, 10);

                var diferenciaFechas = string.Empty;

                DateTime FechaInicioEjecucion = DateTime.ParseExact(sFechaInicioEjecucion, "dd-MM-yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture);

                DateTime FechaTerminacionFinal = DateTime.ParseExact(sFechaTerminacionFinal, "dd-MM-yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture);

                ObtenerDiferenciaFechas(FechaInicioEjecucion.Date, FechaTerminacionFinal.Date, out diferenciaFechas);

                var items1 = diferenciaFechas.Split('|');

                txbDias.Text = items1[2];
                txbMeses.Text = items1[1];
                txbAnios.Text = items1[0];
            }
            else if (lbFechaTerminacionInicial.Text != null)
            {
                string sFechaTerminacionInicial = lbFechaTerminacionInicial.Text.Substring(0, 10);
                string sFechaInicioEjecucion = lbFechaInicioEjecucion.Text.Substring(0, 10);
                var diferenciaFechas = string.Empty;

                DateTime FechaInicioEjecucion = DateTime.ParseExact(sFechaTerminacionInicial, "dd-MM-yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture);

                DateTime FechaTerminacionInicial = DateTime.ParseExact(sFechaInicioEjecucion, "dd-MM-yyyy",
                                                        System.Globalization.CultureInfo.InvariantCulture);

                ObtenerDiferenciaFechas(FechaInicioEjecucion.Date, FechaTerminacionInicial.Date, out diferenciaFechas);
                var items1 = diferenciaFechas.Split('|');
                txbDias.Text = items1[2];
                txbMeses.Text = items1[1];
                txbAnios.Text = items1[0];
            }

            var lbEstadoContrato = (Label)grvBusquedaContratos.Rows[rowIndex].FindControl("lbEstadoContrato");
            txbEstadoContrato.Text = lbEstadoContrato.Text;
            this.pnlConsultarInformacionContrato.Visible = false;

            this.pnlRelacionarContrato.Visible = false;
            this.pnlInformacionContrato.Visible = true;
            pnlArchivo.CssClass = "popuphIstorico hidden";
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
    {
        fechafinal = fechafinal.AddDays(1);

        int result = 0;
        int ai, mi, di;
        int af, mf, df;

        ai = fechaInicial.Year;
        mi = fechaInicial.Month;
        di = fechaInicial.Day;

        af = fechafinal.Year;
        mf = fechafinal.Month;
        df = fechafinal.Day;


        if (di == 31 || (mi == 2 && di > 27))
            di = 30;
        if (df > 27 && mf == 2)
            df = 30;
        if (df == 31 && di < 30)
        {
            mf++;
            df = 1;
        }
        else if (df == 31)
            df = 30;
        else
        {
            if (di == 31 || (mi == 2 && di > 27))
                di = 30;
            if (df == 31 || (mf == 2 && df > 27))
                df = 30;
        }

        if (Math.Abs(af - ai) == 0)
            result = (mf - mi) * 30 + df - di;
        else
            result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


        return result;
    }

    public static bool ObtenerDiferenciaFechas(DateTime fechaInicio, DateTime fechaFin, out string result)
    {
        bool isValid = true;
        result = string.Empty;
        try
        {
            int dias360 = ObtenerdiasEntreFechas360(fechaInicio, fechaFin);

            decimal dias360toMonth = dias360 / 30;

            int meses = (int)(Math.Round(dias360toMonth, 0, MidpointRounding.ToEven));

            int dias = dias360 - (meses * 30);

            result = "0|" + meses + "|" + dias;
        }
        catch (Exception ex)
        {
            isValid = false;
        }

        return isValid;
    }

    ///// <summary>
    ///// Metodo para obtener fecha
    ///// </summary>
    ///// <param name="fechaInicial"></param>
    ///// <param name="fechafinal"></param>
    ///// <returns></returns>
    //private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
    //{
    //    fechafinal = fechafinal.AddDays(1);

    //    int result = 0;
    //    int ai, mi, di;
    //    int af, mf, df;

    //    ai = fechaInicial.Year;
    //    mi = fechaInicial.Month;
    //    di = fechaInicial.Day;

    //    af = fechafinal.Year;
    //    mf = fechafinal.Month;
    //    df = fechafinal.Day;


    //    if (di == 31 || (mi == 2 && di > 27))
    //        di = 30;
    //    if (df > 27 && mf == 2)
    //        df = 30;
    //    if (df == 31 && di < 30)
    //    {
    //        mf++;
    //        df = 1;
    //    }
    //    else if (df == 31)
    //        df = 30;
    //    else
    //    {
    //        if (di == 31 || (mi == 2 && di > 27))
    //            di = 30;
    //        if (df == 31 || (mf == 2 && df > 27))
    //            df = 30;
    //    }

    //    if (Math.Abs(af - ai) == 0)
    //        result = (mf - mi) * 30 + df - di;
    //    else
    //        result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


    //    return result;
    //}

    ///// <summary>
    ///// Metodo para obtener diferencia de fechas
    ///// </summary>
    ///// <param name="fechaInicio"></param>
    ///// <param name="fechaFin"></param>
    ///// <param name="result"></param>
    ///// <returns></returns>
    //public static bool ObtenerDiferenciaFechas(DateTime fechaInicio, DateTime fechaFin, out string result)
    //{
    //    bool isValid = true;
    //    result = string.Empty;
    //    try
    //    {
    //        int dias360 = ObtenerdiasEntreFechas360(fechaInicio, fechaFin);

    //        decimal dias360toMonth = dias360 / 30;

    //        int meses = (int)(Math.Round(dias360toMonth, 0, MidpointRounding.ToEven));

    //        int dias = dias360 - (meses * 30);

    //        result = "0|" + meses + "|" + dias;
    //    }
    //    catch (Exception ex)
    //    {
    //        isValid = false;
    //    }

    //    return isValid;
    //}

    #endregion

    #region BOTONES

    /// <summary>
    /// subir archicos y relación de tipo de documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdicionar_Click(object sender, EventArgs e)
    {
        if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/0001"))
        {
            this.CustomValidator1.IsValid = false;
            return;
        }
        else if (txbNumeroContrato.Text != "")
        {
            if (ValidarDocumentacionSolicitada() == false)
            {
                toolBar.LipiarMensajeError();
                if (AdicionarDocumentosSolicitadosCache())
                {
                    CargarGrillaDocumentosSolicitadosYRecibidos();
                }
            }
            else
            {
                toolBar.MostrarMensajeError("Debe diligenciar los datos requeridos");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Debe seleccionar un Número de Contrato");
        }
    }

    /// <summary>
    /// Metodo para ver el archivo cargado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkVerArchivo_Click(object sender, EventArgs e)
    {
        bool vVerArchivo = false;
        string vRuta = string.Empty;
        LinkButton imageButton = (LinkButton)sender;
        TableCell tableCell = (TableCell)imageButton.Parent;
        GridViewRow row = (GridViewRow)tableCell.Parent;
        gvwDocumentacionRecibida.SelectedIndex = row.RowIndex;
        int fila = row.RowIndex;
        int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[fila].Value);
        List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwHistorico"];
        DocumentosSolicitadosDenunciaBien vArchivo = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorId(vIdDocumentoSolicitado);
        if (vArchivo.IdApoderado == null || vArchivo.IdCausante == null)
        {
            if (vArchivo.RutaArchivo.Contains("RegistarInformacionApoderado") || vArchivo.RutaArchivo.Contains("RegistarInformacionCausante"))
            {
                vVerArchivo = File.Exists(Server.MapPath("../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo));
                vRuta = "../" + vArchivo.RutaArchivo + "/" + vArchivo.NombreArchivo;
            }
            else
            {
                vVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo));
                vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vArchivo.RutaArchivo;
                if (!vVerArchivo)
                {
                    vVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo));
                    vRuta = "../RegistroDenuncia/Archivos/" + vArchivo.RutaArchivo;
                }
            }

            if (vVerArchivo)
            {
                this.lblTitlePopUp.Text = vArchivo.NombreArchivo;
                this.pnlArchivo.CssClass = "popuphIstorico";
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                if (vArchivo.RutaArchivo.ToUpper().Contains("PDF") || vArchivo.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    //this.btnDescargar.Visible = true;
                }
                else if (vArchivo.RutaArchivo.ToUpper().Contains("JPG") || vArchivo.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    //this.btnDescargar.Visible = true;
                }
            }
            else
            {
                this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                this.pnlArchivo.CssClass = "popuphIstorico";
                this.imgDodumento.Visible = false;
            }
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina de nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo para guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txbNumeroContrato.Text != "")
            {
                int vIdContrato = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdContrato"));
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));

                List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];

                if (vListaDocumentosSolicitados.Count() > 0)
                {
                    List<ContratoParticipacionEconomica> vlstContratoParticipacionEconomica = this.vMostrencosService.ConsultarExistenciaContratoXDenuncia(vIdDenunciaBien, vIdContrato);
                    int contratoxdenuncia = vlstContratoParticipacionEconomica.Count();

                    if (contratoxdenuncia > 0)
                    {
                        string Valor = "El registro ya existe";
                        Response.Redirect("../../Mostrencos/ContratoParticipacionEconomica/List.aspx?valor=" + Valor, false);
                    }
                    else
                    {
                        var terminado = vlstContratoParticipacionEconomica.Find(x => x.IDEstadoContrato != 7);
                        if (terminado == null)
                        {
                            toolBar.LipiarMensajeError();
                            int IdContratoParticipacionEconomica = InsertarContratoParticipacionEconomica();
                            if (IdContratoParticipacionEconomica > 0)
                            {
                                bool adicion = AdicionarDocumentosSolicitados(IdContratoParticipacionEconomica);

                                if (adicion == true)
                                {
                                    int IdInsertarHistoricoContratoParticipacionEconomica = InsertarHistoricoContratoParticipacionEconomica();
                                    if (IdInsertarHistoricoContratoParticipacionEconomica > 0)
                                    {
                                        toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                                        this.pnlDocumentacionSolicitada.Enabled = false;
                                        SetSessionParameter("ContratoParticipacionEconomica.IdContrato", vIdContrato);
                                        DeshabilitarCampos();
                                        HabilitarBotones();
                                    }
                                }
                            }
                            else
                            {
                                string Valor = "No se puede relacionar un contrato, el contrato asociado a la denuncia debe estar en estado Terminado";
                                Response.Redirect("../../Mostrencos/ContratoParticipacionEconomica/List.aspx?valor=" + Valor, false);
                            }
                        }
                        else
                        {
                            string Valor = "No se puede relacionar un contrato, el contrato asociado a la denuncia debe estar en estado Terminado";
                            toolBar.MostrarMensajeError(Valor);
                        }
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("Debe Adiccionar un Documento");
                }

            }
            else
            {
                this.lblCampoRequeridoInformacionContrato.Visible = true;
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para volver a listar del modulo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Boton retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Metodo para ir a la pagina de editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/ContratoParticipacionEconomica/Edit.aspx");
    }
    #endregion
}