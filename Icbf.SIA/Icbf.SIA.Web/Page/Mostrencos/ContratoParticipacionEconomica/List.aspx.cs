﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;

public partial class Page_Mostrencos_ContratoParticipacionEconomica_List : GeneralWeb
{
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/ContratoParticipacionEconomica";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Codigo identificador de la denuncia
    /// </summary>
    private int vIdDenunciaBien;

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            toolBar.MostrarBotonConsultar();
            toolBar.MostrarBotonNuevo(true);
            string MsgError = Request.QueryString["valor"];
            string MsgOk = Request.QueryString["ok"];

            if (MsgError != null)
            {
                toolBar.MostrarMensajeError(MsgError);
            }

            if (MsgOk != null)
            {
                toolBar.MostrarMensajeGuardado(MsgOk);
            }

            if (!IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Metodo inicial para cargar datos en el list
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Contrato Participación Económica");
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);

            if ((vRegistroDenuncia.IdEstado != 11) && (vRegistroDenuncia.IdEstado != 13))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }
            else
            {
                toolBar.eventoBuscar += btnConsultar_Click;
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cardar los datos de la vista
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.ContratoParticipacionEconomica.IdDenunciaBien"));

            if (vIdDenunciaBien != 0)
            {
                if (this.vIdDenunciaBien != 0)
                {
                    List<Tercero> vTercero = new List<Tercero>();
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));

                    if (vDenunciaBien.IdEstadoDenuncia == 11 || vDenunciaBien.IdEstadoDenuncia == 13 || vDenunciaBien.IdEstadoDenuncia == 19)
                    {
                        this.toolBar.OcultarBotonNuevo(true);
                        this.toolBar.MostrarBotonEditar(false);
                    }

                    this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoDenuncia) ? vDenunciaBien.RadicadoDenuncia : string.Empty;
                    this.txtFechaRadicadoDenuncia.Text = vDenunciaBien.FechaRadicadoDenuncia.ToString() != "01/01/0001 12:00:00 a.m." ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoDenuncia).ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                    this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vDenunciaBien.RadicadoCorrespondencia) ? vDenunciaBien.RadicadoCorrespondencia : string.Empty;
                    this.txtDescripcion.Text = !string.IsNullOrEmpty(vDenunciaBien.DescripcionDenuncia) ? vDenunciaBien.DescripcionDenuncia : string.Empty;
                    this.txtFechaRadicadoCorrespondencia.Text = (vDenunciaBien.FechaRadicadoCorrespondencia.ToString() != "01/01/0001 12:00:00 a.m.") && (vDenunciaBien.FechaRadicadoCorrespondencia.HasValue) ? Convert.ToDateTime(vDenunciaBien.FechaRadicadoCorrespondencia).ToString("dd/MM/yyyy") : string.Empty;
                    this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento) ? vDenunciaBien.Tercero.TiposDocumentosGlobal.NomTipoDocumento : string.Empty;
                    this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.NumeroIdentificacion) ? vDenunciaBien.Tercero.NumeroIdentificacion : string.Empty;

                    if (vDenunciaBien.Tercero.IdTipoDocIdentifica != 7)
                    {
                        this.txtPrimerNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerNombre) ? vDenunciaBien.Tercero.PrimerNombre : string.Empty;
                        this.txtSegundoNombre.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoNombre) ? vDenunciaBien.Tercero.SegundoNombre : string.Empty;
                        this.txtPrimerApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.PrimerApellido) ? vDenunciaBien.Tercero.PrimerApellido : string.Empty;
                        this.txtSegundoApellido.Text = !string.IsNullOrEmpty(vDenunciaBien.Tercero.SegundoApellido) ? vDenunciaBien.Tercero.SegundoApellido : string.Empty;
                        this.PanelNombrePersonaNatural.Visible = true;
                        this.PanelRazonSocial.Visible = false;
                    }
                    else
                    {
                        this.txtRazonSocial.Text = vDenunciaBien.Tercero.RazonSocial.ToString();
                        this.txtNumeroIdentificacion.Text += "-" + vDenunciaBien.Tercero.DigitoVerificacion.ToString();
                        this.PanelNombrePersonaNatural.Visible = false;
                        this.PanelRazonSocial.Visible = true;
                    }
                    vTercero = this.vMostrencosService.ConsultarTerceroTodos();
                }
            }

            List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);

            if (vListaContratoDenunciaBien.Count() > 0)
            {
                this.grvContratoParticipacion.Visible = true;
                this.grvContratoParticipacion.DataSource = vListaContratoDenunciaBien;
                this.grvContratoParticipacion.DataBind();
            }
            else
            {
                this.grvContratoParticipacion.PageSize = PageSize();
                this.grvContratoParticipacion.EmptyDataText = EmptyDataText();
                this.grvContratoParticipacion.DataBind();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para al dar click en detalle de la grilla guarde el ID del contrato  
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValueIdContrato = string.Empty;
            string strValueUsuarioCrea = string.Empty;


            if (grvContratoParticipacion.DataKeys[rowIndex].Values["IdContrato"] != null)
            {
                strValueIdContrato = grvContratoParticipacion.DataKeys[rowIndex].Values["IdContrato"].ToString();
            }

            SetSessionParameter("ContratoParticipacionEconomica.IdContrato", strValueIdContrato);

            if (strValueUsuarioCrea.Trim().ToUpper() == GetSessionUser().NombreUsuario.Trim().ToUpper())
            {
                SetSessionParameter("ContratoParticipacionEconomica.EsUsuarioAutenticado", true);
            }
            else
            {
                SetSessionParameter("ContratoParticipacionEconomica.EsUsuarioAutenticado", false);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Metodo redirige a la pantalla detalle
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvContratoParticipacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(grvContratoParticipacion.SelectedRow);
        NavigateTo(SolutionPage.Detail);
    }

    /// <summary>
    /// Metodo paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvContratoParticipacion_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        grvContratoParticipacion.PageIndex = e.NewPageIndex;

        List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);

        if (vListaContratoDenunciaBien.Count() > 0)
        {
            this.grvContratoParticipacion.Visible = true;
            this.grvContratoParticipacion.DataSource = vListaContratoDenunciaBien;
            this.grvContratoParticipacion.DataBind();
        }
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Metodo redirige a la pantalla list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Metodo redirige a la pantalla nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = this.vMostrencosService.ConsultarContratoXDenuncia(vIdDenunciaBien);

        bool valida = false;
        foreach (var item in vListaContratoDenunciaBien)
        {
            if (item.IDEstadoContrato != 7)
            {
                valida = true;
            };
        }

        if (valida == true)
        {
            string Valor = "No se puede relacionar un contrato, el contrato asociado a la denuncia debe estar en estado Terminado";
            toolBar.MostrarMensajeError(Valor);
        }
        else
        {
            NavigateTo(SolutionPage.Add);
        }
    }

    /// <summary>
    /// Metodo redirige a la pantalla detalle de resgistrar documentos bien denuncido
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Mostrencos/RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    #endregion
}