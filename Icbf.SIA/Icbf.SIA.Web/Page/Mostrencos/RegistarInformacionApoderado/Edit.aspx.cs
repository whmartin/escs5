﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.IO;

public partial class Page_Mostrencos_InformacionApoderado_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionApoderado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlPopUpHistorico.CssClass = "popuphIstorico hidden";
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Edit))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }

            this.toolBar.LipiarMensajeError();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        bool vExisteApoderadoActivo = false;
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        List<Apoderados> vListApoderados = new List<Apoderados>();
        vListApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(vIdDenunciaBien);
        foreach (Apoderados item in vListApoderados)
        {
            if (!string.IsNullOrEmpty(item.EstadoApoderado))
            {
                if (item.EstadoApoderado.Equals("ACTIVO"))
                {
                    vExisteApoderadoActivo = true;
                    break;
                }
            }
        }
        if (!vExisteApoderadoActivo)
        {
            this.NavigateTo(SolutionPage.Add);
        }
        else
        {
            this.toolBar.MostrarMensajeError("El estado de los apoderados debe ser Inactivo para poder crear un nuevo apoderado");
        }
    }

    /// <summary>
    /// Evento CheckedChanged del rbtEstadoDocumentoAceptadoo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rbtEstadoDocumentoAceptado_CheckedChanged(object sender, EventArgs e)
    {
        this.rbtEstadoDocumentoDevuelto.Checked = !this.rbtEstadoDocumentoAceptado.Checked;
        this.lblAsterFecha.Visible = true;
        this.lblAsterNombre.Visible = true;
        this.lblAsterObservacion.Visible = true;
    }

    /// <summary>
    /// Evento CheckedChanged del rbtEstadoDocumentoDevuelto
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rbtEstadoDocumentoDevuelto_CheckedChanged(object sender, EventArgs e)
    {
        this.rbtEstadoDocumentoAceptado.Checked = !this.rbtEstadoDocumentoDevuelto.Checked;
        this.lblAsterFecha.Visible = true;
        this.lblAsterNombre.Visible = true;
        this.lblAsterObservacion.Visible = true;
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderado_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            if (this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).ExisteApoderado.Equals("SI"))
            {
                this.toolBar.LipiarMensajeError();
                if (this.rblDenuncianteEsApoderadoSi.Checked)
                {
                    this.rblDenuncianteEsApoderadoSi.Checked = true;
                    this.rblDenuncianteEsApoderadoNo.Checked = false;
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                    this.Buscar(vDenunciaBien.IdTercero);
                    this.txtNumeroTarjetaProfesional.Text = string.Empty;
                    this.rblEstadoApoderado.SelectedValue = "ACTIVO";
                }
                else
                {
                    this.rblDenuncianteEsApoderadoSi.Checked = false;
                    this.rblDenuncianteEsApoderadoNo.Checked = true;
                    Apoderados vAporado = this.GetSessionParameter("Mostrencos.RegistrarApoderado.Apoderado") as Apoderados;
                    this.CargarDatosTercero(vAporado.Tercero);
                    this.txtNumeroTarjetaProfesional.Text = vAporado.NumeroTarjetaProfesional;
                }

                this.CambiarEstadoApoderado();
            }
            else
            {
                this.CargarListaDesplegable();
                this.toolBar.LipiarMensajeError();
                if (this.rblDenuncianteEsApoderadoSi.Checked)
                {
                    DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                    this.Buscar(vDenunciaBien.IdTercero);
                    this.ddlTipoIdentificacion.Enabled = false;
                    this.txtNumeroIdentificacionApoderado.Enabled = false;
                    this.btnBuscar.Enabled = false;
                    this.rblEstadoApoderado.SelectedValue = "ACTIVO";
                }
                else
                {
                    this.LimpiarDatosTercero();
                    this.rblEstadoApoderado.ClearSelection();
                    this.btnBuscar.Enabled = true;
                    this.ddlTipoIdentificacion.Enabled = true;
                    this.txtNumeroIdentificacionApoderado.Enabled = true;
                }

                this.CambiarEstadoApoderado();
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.Buscar(0);
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoNo.Checked = !this.rblDenuncianteEsApoderadoSi.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoSi.Checked = !this.rblDenuncianteEsApoderadoNo.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoNo.Checked = !this.rblExisteApoderadoSi.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoSi.Checked = !this.rblExisteApoderadoNo.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.CapturarValoresDocumentacion();           
            this.ValidarCamposFecha();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Guardar();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
            this.gvwDocumentacionRecibida.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento RowCommand de la grilla
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);

                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                if (vDocumentoSolicitado.EsNuevo)
                {
                    vListaDocumento.Remove(vDocumentoSolicitado);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                }
                else
                {
                    List<DocumentosSolicitadosDenunciaBien> vDocumentosEliminar = new List<DocumentosSolicitadosDenunciaBien>();
                    vListaDocumento.Remove(vDocumentoSolicitado);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                    vDocumentosEliminar.Add(vDocumentoSolicitado);
                    this.ViewState["DocumentosEliminar"] = vDocumentosEliminar;
                }

                this.CargarGrillaDocumentos();
                this.LimpiarCampos();
                this.FecharRecibido.Date = DateTime.Now.Date;
                this.ValidarCamposFecha();
            }

            if (e.CommandName.Equals("Editar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                this.LimpiarCampos();
                this.CargarDatosDocumentoSolocitado(vDocumentoSolicitado);
                this.CargarDatosDocumentoRecibido(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                this.rblEstadoApoderado.Enabled = true;
                this.rbtEstadoDocumentoDevuelto.Enabled = true;
                this.rbtEstadoDocumentoAceptado.Enabled = true;
                this.FecharRecibido.Date = vDocumentoSolicitado.FechaSolicitud.Date;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }

                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }



        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }

        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }


    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Información del Apoderado", "Add");
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
            if (vApoderado.IdDenunciaBien != 0)
            {
                this.CargarListaDesplegable();
                bool vExisteApoderado = vApoderado.ExisteApoderado.Equals("SI");
                if (vExisteApoderado)
                {
                    this.CargarDatosApoderado(vApoderado);
                }

                this.rblExisteApoderadoSi.Checked = true;
                this.rblExisteApoderadoNo.Checked = false;
                this.rblExisteApoderadoSi.Enabled = false;
                this.rblExisteApoderadoNo.Enabled = false;
                this.hfIdDocumentoSolocitado.Value = "0";
                this.CargarDatosDenuncia(vApoderado);
                this.CargarGrillaDocumentos();
                this.CargarGrillaHistorico(vApoderado.IdDenunciaBien);
                this.FecharRecibido.Date = DateTime.Now;

                this.VerCampos(true);

            }

            this.rblDenuncianteEsApoderadoSi.Focus();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();

            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(vIdApoderado);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }

            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }

        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }


    /// <summary>
    /// Carga los datos del Apoderado
    /// </summary>
    /// <param name="pApoderado">Información del Apoderado</param>
    private void CargarDatosApoderado(Apoderados pApoderado)
    {
        try
        {
            this.SetSessionParameter("Mostrencos.RegistrarApoderado.Apoderado", pApoderado);
            if (pApoderado.IdTercero != null)
            {
                this.hfIdTercero.Value = pApoderado.IdTercero.ToString();
                pApoderado = this.vMostrencosService.ConsultarInfirmacionApoderados(pApoderado);
                this.txtNumeroIdentificacionApoderado.Text = pApoderado.Tercero.NumeroIdentificacion;
                this.txtTipoPersona.Text = pApoderado.Tercero.NombreTipoPersona;
                this.txtPrimerNombreApoderado.Text = pApoderado.Tercero.PrimerNombre;
                if (pApoderado.Tercero.NombreTipoPersona != "NATURAL")
                {
                    string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString()).FirstOrDefault().NomTipoDocumento;
                    this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString()));
                    this.txtPrimerNombreApoderado.Text = pApoderado.Tercero.RazonSocial;
                }
                this.ddlTipoIdentificacion.SelectedValue = pApoderado.Tercero.TiposDocumentosGlobal.IdTipoDocumento.ToString();
                this.txtSegundoNombreApoderado.Text = pApoderado.Tercero.SegundoNombre;
                this.txtPrimerApellidoApoderado.Text = pApoderado.Tercero.PrimerApellido;
                this.txtSegundoApellidoApoderado.Text = pApoderado.Tercero.SegundoApellido;
                this.txtDireccion.Text = pApoderado.Tercero.Direccion;
                this.txtDepartamento.Text = pApoderado.Tercero.NombreDepartamento;
                this.txtMunicipio.Text = pApoderado.Tercero.NombreMunicipio;
                this.txtEmail.Text = pApoderado.Tercero.CorreoElectronico;
                this.txtIndicativo.Text = pApoderado.Tercero.Indicativo;
                this.txttelefono.Text = pApoderado.Tercero.Telefono;
                this.txtCelular.Text = pApoderado.Tercero.Celular;
            }

            this.txtNumeroTarjetaProfesional.Text = !string.IsNullOrEmpty(pApoderado.NumeroTarjetaProfesional) ? pApoderado.NumeroTarjetaProfesional : string.Empty;
            if (!string.IsNullOrEmpty(pApoderado.EstadoApoderado))
            {
                this.rblEstadoApoderado.SelectedValue = pApoderado.EstadoApoderado;
            }

            if (pApoderado.ExisteApoderado.ToUpper().Equals("SI"))
            {
                this.rblExisteApoderadoSi.Checked = true;
            }
            else
            {
                this.rblExisteApoderadoNo.Checked = true;
            }

            if (!string.IsNullOrEmpty(pApoderado.DenuncianteEsApoderado))
            {

                if (pApoderado.DenuncianteEsApoderado.ToUpper().Equals("SI"))
                {
                    this.rblDenuncianteEsApoderadoSi.Checked = true;
                }
                else
                {
                    this.rblDenuncianteEsApoderadoNo.Checked = true;
                }
            }


            if (pApoderado.FechaFinPoder != null && pApoderado.FechaFinPoder != Convert.ToDateTime("1/01/1900"))
            {
                this.FechaFinPoder.Date = Convert.ToDateTime(pApoderado.FechaFinPoder).Date;
            }

            if (pApoderado.FechaInicioPoder != null && pApoderado.FechaInicioPoder != Convert.ToDateTime("1/01/1900"))
            {
                this.FechaInicioPoder.Date = Convert.ToDateTime(pApoderado.FechaInicioPoder).Date;
            }

            this.txtObservaciones.Text = !string.IsNullOrEmpty(pApoderado.Observaciones) ? pApoderado.Observaciones : string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los datos de la denuncia
    /// </summary>
    /// <param name="pApoderado">Información del Apoderado</param>
    private void CargarDatosDenuncia(Apoderados pApoderado)
    {
        try
        {
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(pApoderado.IdDenunciaBien);
            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }

            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(pApoderado.IdDenunciaBien).IdEstadoDenuncia).NombreEstadoDenuncia;
            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.Equals("Archivado") || vRegistroDenuncia.NombreEstadoDenuncia.Equals("Anulado"))
            {
                this.toolBar.MostrarBotonNuevo(false);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información del la lista desplegable
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";

            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la información del caudante
    /// </summary>
    private void Guardar()
    {
        try
        {
            string vRuta = string.Empty;
            int vResultado;
            this.FecharRecibido.Date = DateTime.Now.Date;
            if (this.rbtEstadoDocumentoAceptado.Checked || this.rbtEstadoDocumentoDevuelto.Checked)
            {
                this.CapturarValoresDocumentacion();
            }
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            Apoderados vApoderado = this.CapturarValoresApoderado();
            if (vApoderado != null)
            {
                this.ValidarFechas(vApoderado);
                this.InformacionAudioria(vApoderado, this.PageName, SolutionPage.Edit);
                vResultado = this.vMostrencosService.EditarApoderado(vApoderado);
                if (vResultado > 0)
                {
                    List<DocumentosSolicitadosDenunciaBien> vListaDocumentosEliminar = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["DocumentosEliminar"];
                    if (vListaDocumentosEliminar != null)
                    {
                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentosEliminar)
                        {
                            item.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("ELIMINADO").IdEstadoDocumento;
                            item.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }

                        this.ViewState["DocumentosEliminar"] = null;
                    }

                    if (vListaDocumentos != null)
                    {

                        foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                        {
                            vRuta = Server.MapPath("~/Page/Mostrencos/" + item.RutaArchivo);
                            if (item.IdDocumentosSolicitadosDenunciaBien < 0)
                            {
                                item.IdApoderado = vApoderado.IdApoderado;
                                item.IdDenunciaBien = vApoderado.IdDenunciaBien;
                                item.IdCausante = null;
                                vResultado = this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                                item.IdDocumentosSolicitadosDenunciaBien = vResultado;
                            }

                            this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                            if (!string.IsNullOrEmpty(item.RutaArchivo))
                            {
                                if (item.bytes != null)
                                {
                                    if (!System.IO.Directory.Exists(vRuta))
                                    {
                                        System.IO.Directory.CreateDirectory(vRuta);
                                    }

                                    System.IO.File.WriteAllBytes(vRuta + "\\" + item.NombreArchivo, item.bytes);
                                }
                            }
                            else
                            {
                                item.RutaArchivo = string.Empty;
                                item.NombreArchivo = string.Empty;
                            }

                            vResultado = this.vMostrencosService.EditarDocumentosSolicitadosDenunciaBien(item);
                        }
                    }

                    this.SetSessionParameter("Mostrencos.RegistrarInformacionApoderado.Mensaje", "La información ha sido guardada exitosamente");
                    this.NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// captura los datos de la documentación 
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            bool vExiste = false;
            if (this.fulArchivoRecibido.HasFile)
            {
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }
                DocumentosSolicitadosDenunciaBien vDocumentoCargado = new DocumentosSolicitadosDenunciaBien();
                vDocumentoCargado.IdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarArchivo(vDocumentoCargado);
                this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", vDocumentoCargado);
                this.lblNombreArchivo.Visible = true;
                this.lblNombreArchivo.Text = this.fulArchivoRecibido.FileName;
            }


            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (!ExistenCamposDocumentacionRequeridos())
            {
                if (vListaDocumentacion == null)
                {
                    vListaDocumentacion = new List<DocumentosSolicitadosDenunciaBien>();
                }
                if (vListaDocumentacion.Count > 0)
                {
                    if (Convert.ToInt32(this.hfIdDocumentoSolocitado.Value) == 0 && vListaDocumentacion.Where(p => p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue)).Count() > 0)
                    {
                        this.toolBar.MostrarMensajeError("El Registro ya existe");
                        vExiste = true;
                    }
                    else if (vListaDocumentacion.Where(p => (p.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue) && p.EsNuevo)).Count() > 0)
                    {
                        this.toolBar.MostrarMensajeError("El Registro ya existe");
                        vExiste = true;
                    }
                }                

                if (!vExiste || !this.hfIdDocumentoSolocitado.Value.Equals("0"))
                {
                    bool vArchivoEsValido = false;
                    if (this.rbtEstadoDocumentoAceptado.Checked || this.rbtEstadoDocumentoDevuelto.Checked)
                    {
                        int vIdDocumentosSolicitados = Convert.ToInt32(this.hfIdDocumentoSolocitado.Value);
                        vDocumentacionSolicitada = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentosSolicitados).FirstOrDefault();
                        if (vDocumentacionSolicitada == null)
                        {
                            vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
                            vDocumentacionSolicitada.IdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                        }

                        vDocumentacionSolicitada.IdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                        vArchivoEsValido = this.CargarArchivo(vDocumentacionSolicitada);
                        if (vArchivoEsValido)
                        {
                            if (this.hfIdDocumentoSolocitado.Value.Equals("0"))
                            {
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                            }
                            else
                            {
                                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = Convert.ToInt32(this.hfIdDocumentoSolocitado.Value);
                            }

                            vDocumentacionSolicitada.FechaRecibido = this.FecharRecibido.Date;
                            vDocumentacionSolicitada.IdEstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre(this.rbtEstadoDocumentoAceptado.Checked ? "ACEPTADO" : "DEVUELTO").IdEstadoDocumento;
                            vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(vDocumentacionSolicitada.IdEstadoDocumento));
                            vDocumentacionSolicitada.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text.ToUpper();
                            vDocumentacionSolicitada.UsuarioModifica = vDocumentacionSolicitada.UsuarioCrea != null ? this.GetSessionUser().NombreUsuario : null;
                            vDocumentacionSolicitada.UsuarioCrea = vDocumentacionSolicitada.UsuarioCrea ?? this.GetSessionUser().NombreUsuario;
                            vDocumentacionSolicitada.EsNuevo = this.hfIdDocumentoSolocitado.Value.Equals("0");
                            this.lblNombreArchivo.Visible = false;
                            this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado", null);
                        }
                    }
                    else
                    {
                        vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                        vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                        vDocumentacionSolicitada.EsNuevo = this.hfIdDocumentoSolocitado.Value.Equals("0");
                        vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ?
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien >= 0 ? -1 :
                                    vListaDocumentacion.OrderBy(p => p.IdDocumentosSolicitadosDenunciaBien).FirstOrDefault().IdDocumentosSolicitadosDenunciaBien - 1 : -1;
                        vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    }

                    if (vArchivoEsValido || !vExiste)
                    {
                        vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue);
                        vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                        vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                        vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObserbacionesDocumentoSolicitado.Text.ToUpper();
                        if (!vDocumentacionSolicitada.EsNuevo)
                        {
                            DocumentosSolicitadosDenunciaBien vDocumentacion = vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == Convert.ToInt32(this.hfIdDocumentoSolocitado.Value)).FirstOrDefault();
                            vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacion.IdEstadoDocumento;
                            vListaDocumentacion.Remove(vDocumentacion);
                            vDocumentacionSolicitada.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                        }
                        vListaDocumentacion.Add(vDocumentacionSolicitada);
                        this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                        this.CargarGrillaDocumentos();
                        this.LimpiarCampos();
                        this.FecharRecibido.Date = DateTime.Now.Date;
                        this.FechaSolicitud.Date = DateTime.Now.Date;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    private void LimpiarCampos()
    {
        try
        {
            this.lblFechaRecibido.Visible = false;
            this.lblCantidadCaracteresSolicitado.Visible = false;
            this.lblrequeridoDocumentoSoporte.Visible = false;
            this.lblRequeridoFechaRecibido.Visible = false;
            this.lblNombreArchivo.Visible = false;
            this.lblRequeridoNombreArchivo.Visible = false;
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            this.txtObserbacionesDocumentoSolicitado.Text = string.Empty;
            this.rbtEstadoDocumentoAceptado.Checked = false;
            this.rbtEstadoDocumentoDevuelto.Checked = false;
            this.txtObservacionesRecibido.Text = string.Empty;
            this.hfIdDocumentoSolocitado.Value = "0";
            this.lblAsterFecha.Visible = false;
            this.lblAsterNombre.Visible = false;
            this.lblAsterObservacion.Visible = false;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Captura la información del Apoderado
    /// </summary>
    /// <returns>variable de tipo Apoderados con la información capturada</returns>
    private Apoderados CapturarValoresApoderado()
    {
        try
        {
            int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
            Apoderados vApoderado = this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado);
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            if (!this.ExistenCamposApoderadoRequeridos(true))
            {
                int vIdTercero = Convert.ToInt32(this.hfIdTercero.Value);

                if (this.vMostrencosService.ValidarDuplicidadApoderado(vIdTercero, vIdDenunciaBien, vApoderado.IdApoderado))
                {
                    this.toolBar.MostrarMensajeError("El registro ya existe");
                    return null;
                }
                else
                {
                    vApoderado.IdTercero = vIdTercero;
                    if (this.FechaFinPoder.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        vApoderado.FechaFinPoder = null;
                    }
                    else
                    {
                        vApoderado.FechaFinPoder = this.FechaFinPoder.Date;
                    }

                    vApoderado.FechaInicioPoder = this.FechaInicioPoder.Date;
                    vApoderado.NumeroTarjetaProfesional = this.txtNumeroTarjetaProfesional.Text.Trim();
                    vApoderado.Observaciones = this.txtObservaciones.Text;
                    vApoderado.DenuncianteEsApoderado = this.rblDenuncianteEsApoderadoSi.Checked ? "SI" : "NO";
                    vApoderado.EstadoApoderado = this.rblEstadoApoderado.SelectedValue;
                    vApoderado.IdDenunciaBien = vIdDenunciaBien;
                    vApoderado.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                    vApoderado.ExisteApoderado = this.rblExisteApoderadoSi.Checked ? "SI" : "NO";
                    return vApoderado;
                }

            }

            return null;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    /// <summary>
    /// Carga el archivo
    /// </summary>
    /// <param name="pDocumentoSolicitado">variable a la que se le asigna los datos del archivo</param>
    private bool CargarArchivo(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            if (this.fulArchivoRecibido.HasFile)
            {
                string vExtFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                if (vExtFile.ToUpper() != ".PDF" && vExtFile.ToUpper() != ".JPG")
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 Megas");
                }

                pDocumentoSolicitado.bytes = this.fulArchivoRecibido.FileBytes;
                pDocumentoSolicitado.NombreArchivo = this.fulArchivoRecibido.FileName;
                pDocumentoSolicitado.RutaArchivo = "RegistarInformacionApoderado/Archivos/" + pDocumentoSolicitado.IdApoderado;
                return true;
            }
            else
            {
                DocumentosSolicitadosDenunciaBien vDocumento = this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.FileApoderado") as DocumentosSolicitadosDenunciaBien;
                if (vDocumento != null)
                {
                    pDocumentoSolicitado.NombreArchivo = vDocumento.NombreArchivo;
                    pDocumentoSolicitado.RutaArchivo = vDocumento.RutaArchivo;
                    pDocumentoSolicitado.bytes = vDocumento.bytes;
                    return true;
                }
            }

            return !string.IsNullOrEmpty(pDocumentoSolicitado.NombreArchivo);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposDocumentacionRequeridos()
    {
        bool vEsrequerido = false;
        try
        {
            if (this.ddlDocumentoSoporte.SelectedValue.Equals("-1"))
            {
                this.lblrequeridoDocumentoSoporte.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblrequeridoDocumentoSoporte.Visible = false;
                vEsrequerido = false;
            }

            try
            {
                DateTime vFecha = Convert.ToDateTime(this.FechaSolicitud.Date);
                this.lblFechaInvalida.Visible = false;
                vEsrequerido = false;
            }
            catch (Exception)
            {
                this.lblFechaInvalida.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }

            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrequeridoFechaSolicitud.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblrequeridoFechaSolicitud.Visible = false;
                vEsrequerido = false;
            }

            if (this.txtObserbacionesDocumentoSolicitado.Text.Length > 512)
            {
                this.lblCantidadCaracteresSolicitado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblCantidadCaracteresSolicitado.Visible = false;
                vEsrequerido = false;
            }

            if (this.rbtEstadoDocumentoAceptado.Checked || this.rbtEstadoDocumentoDevuelto.Checked)
            {

                if (!this.rbtEstadoDocumentoAceptado.Checked && !this.rbtEstadoDocumentoDevuelto.Checked)
                {
                    this.lblRequeridorbtEstadoDocumento.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridorbtEstadoDocumento.Visible = false;
                    vEsrequerido = false;
                }


                if (this.FecharRecibido.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblRequeridoFechaRecibido.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoFechaRecibido.Visible = false;
                    vEsrequerido = false;
                }

                try
                {
                    DateTime vFecha = Convert.ToDateTime(this.FecharRecibido.Date);
                    this.lblFechaRecibido.Visible = false;
                    vEsrequerido = false;
                }
                catch (Exception)
                {
                    this.lblFechaInvalida.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }

                if (this.FecharRecibido.Date < this.FechaSolicitud.Date)
                {
                    vEsrequerido = true;
                    throw new Exception("La fecha de recibido debe ser igual o mayor a la fecha de la solicitud.");
                }
                else
                {
                    this.lblFechaRecibido.Visible = false;
                    vEsrequerido = false;
                }

                if (!fulArchivoRecibido.HasFile && !this.lblNombreArchivo.Visible)
                {
                    this.lblRequeridoNombreArchivo.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoNombreArchivo.Visible = false;
                    vEsrequerido = false;
                }

                if (string.IsNullOrEmpty(this.txtObservacionesRecibido.Text))
                {
                    this.lblRequeridoObservacionesRecibido.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoObservacionesRecibido.Visible = false;
                    vEsrequerido = false;
                }
            }

            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido && string.IsNullOrEmpty(ex.Message))
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        this.CambiarEstadoApoderado();
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposApoderadoRequeridos(bool pEsGuardar)
    {
        bool vEsrequerido = false;
        bool vLimpiarMensaje = true;
        try
        {
            this.lblRequeridoNumIdent.Visible = false;
            this.lblRequeridoTipoIdent.Visible = false;
            this.lblCantidadCaracteresSolicitado.Visible = false;
            this.lblrequeridoFechaSolicitud.Visible = false;
            this.lblrequeridoDocumentoSoporte.Visible = false;
            this.lblFechaInvalida.Visible = false;
            this.lblRequeridoNumeroTarjetaProfesional.Visible = false;

            if (this.ddlTipoIdentificacion.SelectedValue.Equals("-1"))
            {
                this.lblRequeridoTipoIdent.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoTipoIdent.Visible = false;
                vEsrequerido = false;
            }

            if (string.IsNullOrEmpty(this.txtNumeroIdentificacionApoderado.Text))
            {
                this.lblRequeridoNumIdent.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoNumIdent.Visible = false;
                vEsrequerido = false;
            }

            if (pEsGuardar)
            {
                if (string.IsNullOrEmpty(this.txtNumeroTarjetaProfesional.Text))
                {
                    this.lblRequeridoNumeroTarjetaProfesional.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoNumeroTarjetaProfesional.Visible = false;
                    vEsrequerido = false;
                }

                if (string.IsNullOrEmpty(this.rblEstadoApoderado.SelectedValue))
                {
                    this.lblRequeridoEstadoApoderado.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoEstadoApoderado.Visible = false;
                    vEsrequerido = false;
                }

                try
                {
                    DateTime vFecha = Convert.ToDateTime(this.FechaInicioPoder.Date);
                    this.lblValidFechaInicioPoder.Visible = false;
                    vEsrequerido = false;
                }
                catch (Exception)
                {
                    this.lblValidFechaInicioPoder.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }

                try
                {
                    DateTime vFecha = Convert.ToDateTime(this.FechaFinPoder.Date);
                    this.lblValidFechaFinPoder.Visible = false;
                    vEsrequerido = false;
                }
                catch (Exception)
                {
                    this.lblValidFechaFinPoder.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }

                if (this.FechaInicioPoder.Date == Convert.ToDateTime("1/01/1900"))
                {
                    this.lblRequeridoFechaInicioPoder.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoFechaInicioPoder.Visible = false;
                    vEsrequerido = false;
                }

                if (this.FechaInicioPoder.Date != Convert.ToDateTime("1/01/1900") && this.FechaFinPoder.Date != Convert.ToDateTime("1/01/1900"))
                {
                    if (this.FechaFinPoder.Date < this.FechaInicioPoder.Date)
                    {
                        vEsrequerido = true;
                        vLimpiarMensaje = false;
                        throw new Exception("La Fecha final de poder no puede ser menor a la fecha inicial del poder");
                    }
                    else
                    {
                        this.lblValidFechaFinPoder.Visible = false;
                        vEsrequerido = false;
                    }
                }
                else
                {
                    this.lblValidFechaFinPoder.Visible = false;
                    vEsrequerido = false;
                }

                if (!this.rblDenuncianteEsApoderadoSi.Checked && !this.rblDenuncianteEsApoderadoNo.Checked)
                {
                    this.lblRequeridodenuncianteEsApoderado.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridodenuncianteEsApoderado.Visible = false;
                    vEsrequerido = false;
                }

                if (this.rbtEstadoDocumentoAceptado.Checked || this.rbtEstadoDocumentoDevuelto.Checked || !this.ddlDocumentoSoporte.SelectedValue.Equals("-1"))
                {
                    vEsrequerido = this.ExistenCamposDocumentacionRequeridos();
                }
            }

            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido && vLimpiarMensaje)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// carga los datos de los documentos recibidos
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void CargarDatosDocumentoRecibido(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            this.FecharRecibido.Date = pDocumentoSolicitado.FechaRecibido != null ? Convert.ToDateTime(pDocumentoSolicitado.FechaRecibido) : DateTime.Now;
            if (pDocumentoSolicitado.EstadoDocumento.NombreEstadoDocumento.ToUpper().Equals("ACEPTADO"))
            {
                this.rbtEstadoDocumentoAceptado.Checked = true;
            }
            else if (pDocumentoSolicitado.EstadoDocumento.NombreEstadoDocumento.ToUpper().Equals("DEVUELTO"))
            {
                this.rbtEstadoDocumentoDevuelto.Checked = true;
            }

            this.txtObservacionesRecibido.Text = pDocumentoSolicitado.ObservacionesDocumentacionRecibida;
            this.lblNombreArchivo.Text = pDocumentoSolicitado.NombreArchivo;
            this.lblNombreArchivo.Visible = !string.IsNullOrEmpty(this.lblNombreArchivo.Text);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }

    }

    /// <summary>
    /// carga los datos de los documentos solicitados 
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void CargarDatosDocumentoSolocitado(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            this.hfIdDocumentoSolocitado.Value = pDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            this.ddlDocumentoSoporte.SelectedValue = pDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
            this.FechaSolicitud.Date = Convert.ToDateTime(pDocumentoSolicitado.FechaSolicitud);
            this.txtObserbacionesDocumentoSolicitado.Text = pDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// oculta  campos de la vista
    /// </summary>
    /// <param name="pVer">identifica si se ven o no</param>
    private void VerCampos(bool pVer)
    {
        //this.lblDenuncianteApoderado.Visible = pVer;
        //this.rblDenuncianteEsApoderado.Visible = pVer;
        //this.lblFechaInicioPoder.Visible = pVer;
        //this.lblFechaFinPoder.Visible = pVer;
        //this.FechaInicioPoder.Visible = pVer;
        //this.FechaFinPoder.Visible = pVer;
        //this.lblObservaciones.Visible = pVer;
        //this.txtObservaciones.Visible = pVer;
        this.pnlInformacionApoderado.Visible = pVer;
        this.pnlDocumentacionSolicitada.Visible = pVer;
        this.pnlDocumentacionRecibida.Visible = pVer;
        this.FechaFinPoder.Enabled = pVer;
        this.FechaInicioPoder.Enabled = pVer;
        this.txtObservaciones.Enabled = pVer;
        if (pVer)
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
        }
    }

    /// <summary>
    /// Evento SelectedIndexChanged del rblExisteApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderado_CheckedChanged(object sender, EventArgs e)
    {
        bool vEsVisible = this.rblExisteApoderadoSi.Checked;
        this.rblExisteApoderadoSi.Checked = vEsVisible;
        this.rblExisteApoderadoNo.Checked = !vEsVisible;
        VerCampos(vEsVisible);
    }

    /// <summary>
    /// busca un tercero
    /// </summary>
    /// <param name="pIdTercero">id del tercero que se busca</param>
    private void Buscar(int pIdTercero)
    {
        try
        {
            bool vExistenCamposRequeridos = false;
            Tercero vTercero = new Tercero();
            if (pIdTercero == 0)
            {
                vExistenCamposRequeridos = this.ExistenCamposApoderadoRequeridos(false);
                if (!vExistenCamposRequeridos)
                {
                    vTercero = this.vMostrencosService.ConsultarTercero(Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue), this.txtNumeroIdentificacionApoderado.Text, this.txtTipoPersona.Text);
                }
            }
            else
            {
                vTercero = this.vMostrencosService.ConsultarTerceroXId(pIdTercero);
            }

            if (!vExistenCamposRequeridos)
            {
                if (vTercero.IdTercero != 0)
                {
                    this.CargarDatosTercero(vTercero);
                    this.toolBar.LipiarMensajeError();
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// cambia el etado del apoderado
    /// </summary>
    public void CambiarEstadoApoderado()
    {
        try
        {
            if (this.FechaFinPoder.Date == Convert.ToDateTime("01/01/1900") || this.FechaFinPoder.Date > DateTime.Now.Date)
            {
                this.rblEstadoApoderado.SelectedValue = "ACTIVO";
            }
            else
            {
                this.rblEstadoApoderado.SelectedValue = "INACTIVO";
            }
        }
        catch (Exception)
        {
            throw;
        }

    }

    /// <summary>
    /// Carga los datos del tercero
    /// </summary>
    /// <param name="pTercero">Variable de tipo tercero con los datos a cargar</param>
    private void CargarDatosTercero(Tercero pTercero)
    {
        try
        {
            this.hfIdTercero.Value = pTercero.IdTercero.ToString();
            this.txtTipoPersona.Text = pTercero.NombreTipoPersona;
            this.txtPrimerNombreApoderado.Text = pTercero.PrimerNombre;
            if (pTercero.NombreTipoPersona != "NATURAL")
            {
                string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pTercero.IdTipoDocIdentifica.ToString()).FirstOrDefault().NomTipoDocumento;
                this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pTercero.IdTipoDocIdentifica.ToString()));
                this.txtPrimerNombreApoderado.Text = pTercero.RazonSocial;
            }

            this.ddlTipoIdentificacion.SelectedValue = pTercero.IdTipoDocIdentifica.ToString();
            this.txtNumeroIdentificacionApoderado.Text = pTercero.NumeroIdentificacion;
            this.txtSegundoNombreApoderado.Text = pTercero.SegundoNombre;
            this.txtPrimerApellidoApoderado.Text = pTercero.PrimerApellido;
            this.txtSegundoApellidoApoderado.Text = pTercero.SegundoApellido;
            this.txtDireccion.Text = pTercero.Direccion;
            this.txtDepartamento.Text = pTercero.NombreDepartamento;
            this.txtMunicipio.Text = pTercero.NombreMunicipio;
            this.txtEmail.Text = pTercero.CorreoElectronico;
            this.txtIndicativo.Text = pTercero.Indicativo;
            this.txttelefono.Text = pTercero.Telefono;
            this.txtCelular.Text = pTercero.Celular;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Limpia los datos del tercero
    /// </summary>
    private void LimpiarDatosTercero()
    {
        try
        {
            this.hfIdTercero.Value = string.Empty;
            this.txtTipoPersona.Text = "NATURAL";
            this.ddlTipoIdentificacion.SelectedValue = "-1";
            this.txtNumeroIdentificacionApoderado.Text = string.Empty;
            this.txtPrimerNombreApoderado.Text = string.Empty;
            this.txtSegundoNombreApoderado.Text = string.Empty;
            this.txtPrimerApellidoApoderado.Text = string.Empty;
            this.txtSegundoApellidoApoderado.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtDepartamento.Text = string.Empty;
            this.txtMunicipio.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtIndicativo.Text = string.Empty;
            this.txttelefono.Text = string.Empty;
            this.txtCelular.Text = string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Valida las fechas fguardar
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void ValidarFechas(Apoderados pApoderado)
    {
        try
        {
            if (pApoderado.FechaFinPoder != null)
            {
                if (Convert.ToDateTime(pApoderado.FechaFinPoder).Date > Convert.ToDateTime(pApoderado.FechaInicioPoder).Date && Convert.ToDateTime(pApoderado.FechaFinPoder).Date > DateTime.Now.Date)
                {
                    pApoderado.EstadoApoderado = "ACTIVO";
                }
                else
                {
                    pApoderado.EstadoApoderado = "INACTIVO";
                }
            }
            else if (Convert.ToDateTime(pApoderado.FechaInicioPoder).Date < DateTime.Now.Date)
            {
                pApoderado.EstadoApoderado = "ACTIVO";
            }
            else
            {
                pApoderado.EstadoApoderado = "INACTIVO";
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// valida que las fechas no esten vacias
    /// </summary>
    private void ValidarCamposFecha()
    {

        if (this.FecharRecibido.Date == Convert.ToDateTime("01/01/1900"))
        {
            this.FecharRecibido.Date = DateTime.Now.Date;
        }

        if (this.FechaSolicitud.Date == Convert.ToDateTime("01/01/1900"))
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
        }
    }
    #endregion
}