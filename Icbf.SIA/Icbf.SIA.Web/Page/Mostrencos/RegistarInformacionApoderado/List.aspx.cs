﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.Configuration;
using System.IO;

public partial class Page_Mostrencos_InformacionApoderado_List : GeneralWeb
{
    #region Declaración Variables
    /// <summary>
    /// The tool bar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// The page name
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionApoderado";

    /// <summary>
    /// The v mostrencos service
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// The v identifier denuncia bien/
    /// </summary>
    private int vIdDenunciaBien;
    /// <summary>
    /// Contiene el del archivo .PDF a cargar.
    /// </summary>
    private string vFileName = string.Empty;

    #endregion

    #region "Eventos"

    /// <summary>
    /// Page PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        bool vExisteApoderadoActivo=false;
        this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        List<Apoderados> vListApoderados = new List<Apoderados>();
        vListApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(this.vIdDenunciaBien);
        foreach (Apoderados item in vListApoderados)
        {
            if (!string.IsNullOrEmpty(item.EstadoApoderado))
            {
                if (item.EstadoApoderado.Equals("ACTIVO"))
                {
                    vExisteApoderadoActivo = true;
                    break;
                }
            }
        }
        if (!vExisteApoderadoActivo)
        {
            this.NavigateTo(SolutionPage.Add);
        }
        else
        {
            this.toolBar.MostrarMensajeError("El estado de los apoderados debe ser Inactivo para poder crear un nuevo apoderado");
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvApoderados_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvApoderados.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvApoderados"] != null)
        {
            List<Apoderados> vList = (List<Apoderados>)this.ViewState["SortedgvApoderados"];
            gvApoderados.DataSource = vList;
            gvApoderados.DataBind();
        }
        else
        {
            this.CargarGrilla();
        }
    }

    protected void gvApoderados_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<Apoderados> vList = new List<Apoderados>();
        List<Apoderados> vResult = new List<Apoderados>();

        if (this.ViewState["SortedgvApoderados"] != null)
        {
            vList = (List<Apoderados>)this.ViewState["SortedgvApoderados"];
        }

        switch (e.SortExpression)
        {
            case "NombreMostrar":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreMostrar).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreMostrar).ToList();
                }

                break;

            case "NumeroTarjetaProfesional":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NumeroTarjetaProfesional).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NumeroTarjetaProfesional).ToList();
                }

                break;

            case "DenuncianteEsApoderado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.DenuncianteEsApoderado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.DenuncianteEsApoderado).ToList();
                }

                break;

            case "FechaInicioPoder":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaInicioPoder).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaInicioPoder).ToList();
                }

                break;

            case "FechaFinPoder":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaFinPoder).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaFinPoder).ToList();
                }

                break;

            case "Observaciones":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Observaciones).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Observaciones).ToList();
                }

                break;

            case "EstadoApoderado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoApoderado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoApoderado).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }

        ViewState["SortedgvApoderados"] = vResult;
        this.gvApoderados.DataSource = vResult;
        this.gvApoderados.DataBind();
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvApoderados_SelectedIndexChanged(object sender, EventArgs e)
    {    
        this.SeleccionarRegistro(this.gvApoderados.SelectedRow);
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Iniciars this instance.
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            toolBar.EstablecerTitulos("Información del Apoderado","List");
            this.gvApoderados.PageSize = this.PageSize();
            this.gvApoderados.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
   
    /// <summary>
    /// Cargars the datos iniciales.
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;
            if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA"))
            {
                this.toolBar.MostrarBotonNuevo(false);
            }

            this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
            this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
            this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ?
                (vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001 00:00:00") ? 
                vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
            this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
            this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
            if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
            {
                this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
                this.PanelNombrePersonaNatural.Visible = true;
                this.PanelRazonSocial.Visible = false;
            }
            else
            {
                this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial;
                this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                this.PanelNombrePersonaNatural.Visible = false;
                this.PanelRazonSocial.Visible = true;
            }

            this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia; 
            this.CargarGrilla();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    public void CargarGrilla()
    {
        try
        {
            this.pnlGridApoderados.Visible = true;
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            List<Apoderados> vListaApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(this.vIdDenunciaBien);
            if (vListaApoderados.Count > 0)
            {
                for (int i = 0; i < vListaApoderados.Count(); i++)
                {
                    vListaApoderados[i] = this.vMostrencosService.ConsultarInfirmacionApoderados(vListaApoderados[i]);
                    if(vListaApoderados[i].FechaFinPoder == Convert.ToDateTime("1/01/1900"))
                    {
                        vListaApoderados[i].FechaFinPoder = null;
                    }
                }
                vListaApoderados = vListaApoderados.OrderBy(p => p.NombreMostrar).ToList();
                ViewState["SortedgvApoderados"] = vListaApoderados;
                this.gvApoderados.DataSource = vListaApoderados;
                this.gvApoderados.DataBind();
            }
            else
            {
                this.pnlGridApoderados.Visible = false;
            }           
        }
        catch (Exception ex)
        {
            this.pnlGridApoderados.Visible = false;
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Guardars the archivos.
    /// </summary>
    /// <param name="pIdDenunciaBien">The p identifier denuncia bien.</param>
    /// <param name="pPosition">The p position.</param>
    /// <returns></returns>
    //private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    //{
    //    try
    //    {
    //        HttpFileCollection files = Request.Files;

    //        if (files.Count > 0)
    //        {
    //            string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
    //            string extPdf = ".PDF".ToLower();
    //            if (extFile != extPdf)
    //            {
    //                throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
    //            }
    //            int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
    //            if (vtamArchivo > 2048)
    //            {
    //                throw new Exception("El tamaño del archivo PDF permitido es de 2.048 KB");
    //            }
    //            HttpPostedFile file = files[pPosition];

    //            if (file.ContentLength > 0)
    //            {
    //                string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
    //                string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
    //                string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");

    //                bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

    //                if (!existeCarpeta)
    //                {
    //                    System.IO.Directory.CreateDirectory(carpetaBase);
    //                }

    //                this.vFileName = rutaParcial;
    //                file.SaveAs(filePath);
    //            }
    //        }

    //        return true;
    //    }
    //    catch (HttpRequestValidationException)
    //    {
    //        throw new Exception("Se detectó un posible archivo peligroso.");
    //        //this.toolBar.MostrarMensajeError("Se detectó un posible archivo peligroso.");
    //        //return false;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //        //this.toolBar.MostrarMensajeError(ex.Message);
    //        //return false;
    //    }
    //}

    /// <summary>
    /// guarda datos y los envia al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvApoderados.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion    
    
}