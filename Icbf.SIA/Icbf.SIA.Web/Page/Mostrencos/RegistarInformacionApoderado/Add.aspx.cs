﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using System.Web.Script.Services;
using System.Web.Services;

public partial class Page_Mostrencos_InformacionApoderado_Add : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarInformacionApoderado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        this.CambiarEstadoApoderado();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlPopUpHistorico.CssClass = "popuphIstorico hidden";
        this.toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }

            this.hfLimpiarFechaSolicitud.Value = "0";
            this.ValidarCamposFecha();
        }
    }

    protected void btnHistorico_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Handles the Click event of the btnAgregar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAgregar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.CapturarValoresDocumentacion();
            this.ValidarCamposFecha();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.Buscar(0);
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Guardar();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
            this.gvwDocumentacionRecibida.DataSource = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            this.gvwDocumentacionRecibida.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla RowCommand
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vLista.Remove(vDocumento);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vLista;
                this.CargarGrilla();
            }
            if (e.CommandName.Equals("Editar"))
            {
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                this.LimpiarCampos();
                this.hfLimpiarFechaSolicitud.Value = "0";
                this.hfLimpiarFechaRecibido.Value = "0";
                this.CargarDatosDocumentoSolocitado(vDocumentoSolicitado);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        switch (e.SortExpression)
        {
            case "NombreTipoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                }

                break;

            case "FechaSolicitud":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
                break;

            case "ObservacionesDocumentacionSolicitada":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                }
                break;

            case "NombreEstadoDocumento":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoDocumento.NombreEstadoDocumento).ToList();
                }
                break;

            case "FechaRecibido":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaRecibido).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaRecibido).ToList();
                }
                break;

            case "NombreArchivo":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreArchivo).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreArchivo).ToList();
                }
                break;

            case "ObservacionesDocumentacionRecibida":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionRecibida).ToList();
                }
                break;
        }



        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        if (vResult.Count() == 0)
        {
            vResult.AddRange(vList);
        }

        ViewState["SortedgvwDocumentacionRecibida"] = vResult;
        this.gvwDocumentacionRecibida.DataSource = vResult;
        this.gvwDocumentacionRecibida.DataBind();
    }

    /// <summary>
    /// Evento SelectedIndexChanged del rblExisteApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderado_CheckedChanged(object sender, EventArgs e)
    {
        this.ValidacionChecked();
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoNo.Checked = !this.rblDenuncianteEsApoderadoSi.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblDenuncianteEsApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        this.rblDenuncianteEsApoderadoSi.Checked = !this.rblDenuncianteEsApoderadoNo.Checked;
        this.rblDenuncianteEsApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoSi
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoSi_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoNo.Checked = !this.rblExisteApoderadoSi.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento CheckedChanged del rblExisteApoderadoNo
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblExisteApoderadoNo_CheckedChanged(object sender, EventArgs e)
    {
        rblExisteApoderadoSi.Checked = !this.rblExisteApoderadoNo.Checked;
        rblExisteApoderado_CheckedChanged(sender, e);
    }

    /// <summary>
    /// Evento SelectedIndexChanged del rblDenuncianteEsApoderado
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void rblDenuncianteEsApoderado_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            if (this.rblDenuncianteEsApoderadoSi.Checked)
            {
                int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                this.Buscar(vDenunciaBien.IdTercero);
                this.ddlTipoIdentificacion.Enabled = false;
                this.txtNumeroIdentificacionApoderado.Enabled = false;
                this.btnBuscar.Enabled = false;
                this.rblEstadoApoderado.SelectedValue = "ACTIVO";
            }
            else
            {
                this.LimpiarDatosTercero();
                this.rblEstadoApoderado.ClearSelection();
                this.btnBuscar.Enabled = true;
                this.ddlTipoIdentificacion.Enabled = true;
                this.txtNumeroIdentificacionApoderado.Enabled = true;
            }

            this.CambiarEstadoApoderado();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }

            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Información del Apoderado", "Add");
            this.gvwDocumentacionRecibida.PageSize = PageSize();
            this.gvwDocumentacionRecibida.EmptyDataText = EmptyDataText();
            this.gvwHistoricoDenuncia.PageSize = this.PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            int vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            }

            List<Apoderados> vListaApoderados = this.vMostrencosService.ConsultarApoderadosPorIdDenunciaBien(vIdDenunciaBien);
            if (vListaApoderados.Count() > 0)
            {
                if (vListaApoderados.Where(p => p.ExisteApoderado.Equals("SI")).Count() > 0)
                {
                    this.rblExisteApoderadoNo.Enabled = false;
                    this.rblExisteApoderadoSi.Enabled = false;
                    this.rblExisteApoderadoSi.Checked = true;
                    this.rblExisteApoderadoNo.Checked = false;
                    this.ValidacionChecked();
                    this.rblDenuncianteEsApoderadoSi.Focus();
                }
                else
                {
                    this.rblExisteApoderadoSi.Focus();
                }
            }
            else
            {
                this.rblExisteApoderadoSi.Focus();
            }

            this.hfIdDocumentoSolocitado.Value = "0";
            this.CargarListaDesplegable();
            this.CargarGrilla();
            this.CargarGrillaHistorico(vIdDenunciaBien);

        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas desplegables
    /// </summary>
    private void CargarListaDesplegable()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vListaDocumento = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoActivos();
            vListaDocumento = vListaDocumento.OrderBy(p => p.NombreTipoDocumento).ToList();
            this.ddlDocumentoSoporte.DataSource = vListaDocumento;
            this.ddlDocumentoSoporte.DataTextField = "NombreTipoDocumento";
            this.ddlDocumentoSoporte.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlDocumentoSoporte.DataBind();
            this.ddlDocumentoSoporte.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDocumentoSoporte.SelectedValue = "-1";

            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoIdentificacion.DataSource = vListaTipoIdent;
            this.ddlTipoIdentificacion.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoIdentificacion.DataBind();
            this.ddlTipoIdentificacion.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoIdentificacion.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlGrid.Visible = true;
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlGrid.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    /// <summary>
    /// Guarda la informacion del Apoderado
    /// </summary>
    private void Guardar()
    {
        int vResultado = 0;
        try
        {
            Apoderados vApoderado = this.CapturarValoresApoderado();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vApoderado != null)
            {
                if (vListaDocumentos.Count == 0 && this.rblExisteApoderadoSi.Checked)
                {
                    this.toolBar.MostrarMensajeError("No se ha agregado ningún documento solicitado");
                }
                else
                {
                    this.ValidarFechas(vApoderado);
                    this.InformacionAudioria(vApoderado, this.PageName, SolutionPage.Add);
                    vResultado = this.vMostrencosService.InsertarApoderado(vApoderado);
                    if (vResultado > 0)
                    {
                        if (vApoderado.ExisteApoderado.Equals("SI"))
                        {
                            foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentos)
                            {
                                item.IdApoderado = vResultado;
                                item.IdDenunciaBien = vApoderado.IdDenunciaBien;
                                item.IdCausante = null;
                                this.InformacionAudioria(item, this.PageName, SolutionPage.Add);
                                this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                            }
                        }

                        this.SetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado", vResultado);
                        this.SetSessionParameter("Mostrencos.RegistrarInformacionApoderado.Mensaje", "La información ha sido guardada exitosamente");
                        this.NavigateTo(SolutionPage.Detail);

                    }
                    else
                    {
                        this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la infortación del Apoderado
    /// </summary>
    private void CapturarValoresDocumentacion()
    {
        try
        {
            this.LimpiarLabelsTercero();
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (!ExistenCamposDocumentacionRequeridos())
            {
                if (vListaDocumentacion.Count > 0)
                {
                    if (!string.IsNullOrEmpty(this.hfIdDocumentoSolocitado.Value))
                    {
                        if (this.hfIdDocumentoSolocitado.Value != "0")
                        {
                            vListaDocumentacion.Remove(vListaDocumentacion.Where(p => p.IdDocumentosSolicitadosDenunciaBien == Convert.ToInt32(this.hfIdDocumentoSolocitado.Value)).First());
                        }
                    }
                    foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                    {
                        if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue))
                        {
                            this.toolBar.MostrarMensajeError("El Registro ya existe");
                            vExiste = true;
                            break;
                        }
                    }
                }

                if (!vExiste)
                {
                    vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                    vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                    vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlDocumentoSoporte.SelectedValue);
                    vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                    vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObserbacionesDocumentoSolicitado.Text.Trim().ToUpper();
                    vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                    vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                    vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    vListaDocumentacion.Add(vDocumentacionSolicitada);
                    this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                    this.CargarGrilla();
                    this.ddlDocumentoSoporte.SelectedValue = "-1";
                    this.FechaSolicitud.Date = DateTime.Now.Date;
                    this.txtObserbacionesDocumentoSolicitado.Text = string.Empty;
                    this.hfIdDocumentoSolocitado.Value = "0";
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la información del Apoderado
    /// </summary>
    /// <returns>variable de tipo Apoderados con la información capturada</returns>
    private Apoderados CapturarValoresApoderado()
    {
        try
        {
            Apoderados vApoderado = new Apoderados();
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdDenunciaBien"));
            if (!this.ExistenCamposApoderadoRequeridos(true))
            {
                if (this.rblExisteApoderadoSi.Checked)
                {
                    int vIdTercero = Convert.ToInt32(this.hfIdTercero.Value);

                    if (this.vMostrencosService.ValidarDuplicidadApoderado(vIdTercero, vIdDenunciaBien, 0))
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        return null;
                    }
                    else
                    {
                        vApoderado.IdTercero = vIdTercero;
                        if (this.FechaFinPoder.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            vApoderado.FechaFinPoder = null;
                        }
                        else
                        {
                            vApoderado.FechaFinPoder = this.FechaFinPoder.Date;
                        }
                        vApoderado.FechaInicioPoder = this.FechaInicioPoder.Date;
                        vApoderado.NumeroTarjetaProfesional = this.txtNumeroTarjetaProfesional.Text.Trim();
                        vApoderado.Observaciones = this.txtObservaciones.Text;
                        vApoderado.DenuncianteEsApoderado = this.rblDenuncianteEsApoderadoSi.Checked ? "SI" : "NO";
                        vApoderado.EstadoApoderado = this.rblEstadoApoderado.SelectedValue;
                        vApoderado.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                        vApoderado.IdUsuarioCrea = this.GetSessionUser().IdUsuario;
                    }
                }

                vApoderado.IdDenunciaBien = vIdDenunciaBien;
                vApoderado.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vApoderado.IdUsuarioCrea = this.GetSessionUser().IdUsuario;
                vApoderado.ExisteApoderado = this.rblExisteApoderadoSi.Checked ? "SI" : "NO";
                return vApoderado;
            }

            return null;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposDocumentacionRequeridos()
    {
        bool vEsrequerido = false;
        try
        {
            if (this.ddlDocumentoSoporte.SelectedValue.Equals("-1"))
            {
                this.lblrequeridoDocumentoSoporte.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblrequeridoDocumentoSoporte.Visible = false;
                vEsrequerido = false;
            }

            try
            {
                DateTime vFecha = Convert.ToDateTime(this.FechaSolicitud.Date);
                this.lblFechaInvalida.Visible = false;
                vEsrequerido = false;
            }
            catch (Exception)
            {
                this.lblFechaInvalida.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }

            if (this.FechaSolicitud.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.lblrequeridoFechaSolicitud.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblrequeridoFechaSolicitud.Visible = false;
                vEsrequerido = false;
            }

            if (this.txtObserbacionesDocumentoSolicitado.Text.Length > 512)
            {
                this.lblCantidadCaracteresSolicitado.Visible = true;
                vEsrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblCantidadCaracteresSolicitado.Visible = false;
                vEsrequerido = false;
            }

            return false;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    /// <summary>
    /// Valida que los campos requeridos se diligencien
    /// </summary>
    private bool ExistenCamposApoderadoRequeridos(bool pEsGuardar)
    {
        bool vEsrequerido = false;
        bool vLimpiarMensaje = true;
        try
        {
            this.lblRequeridoNumIdent.Visible = false;
            this.lblRequeridoTipoIdent.Visible = false;
            this.lblCantidadCaracteresSolicitado.Visible = false;
            this.lblrequeridoFechaSolicitud.Visible = false;
            this.lblrequeridoDocumentoSoporte.Visible = false;
            this.lblFechaInvalida.Visible = false;
            this.lblRequeridoNumeroTarjetaProfesional.Visible = false;

            if (this.rblExisteApoderadoSi.Checked)
            {
                if (pEsGuardar)
                {

                    if (!this.rblDenuncianteEsApoderadoNo.Checked && !this.rblDenuncianteEsApoderadoSi.Checked)
                    {
                        this.lblRequeridodenuncianteEsApoderado.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridodenuncianteEsApoderado.Visible = false;
                        vEsrequerido = false;
                    }

                    try
                    {
                        DateTime vFecha = Convert.ToDateTime(this.FechaInicioPoder.Date);
                        this.lblValidFechaInicioPoder.Visible = false;
                        vEsrequerido = false;
                    }
                    catch (Exception)
                    {
                        this.lblValidFechaInicioPoder.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }

                    try
                    {
                        DateTime vFecha = Convert.ToDateTime(this.FechaFinPoder.Date);
                        this.lblValidFechaFinPoder.Visible = false;
                        vEsrequerido = false;
                    }
                    catch (Exception)
                    {
                        this.lblValidFechaFinPoder.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }

                    if (this.FechaInicioPoder.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        this.lblRequeridoFechaInicioPoder.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoFechaInicioPoder.Visible = false;
                        vEsrequerido = false;
                    }

                    if (this.FechaInicioPoder.Date != Convert.ToDateTime("1/01/1900") && this.FechaFinPoder.Date != Convert.ToDateTime("1/01/1900"))
                    {
                        if (this.FechaFinPoder.Date < this.FechaInicioPoder.Date)
                        {
                            vEsrequerido = true;
                            vLimpiarMensaje = false;
                            throw new Exception("La Fecha final de poder no puede ser menor a la fecha inicial del poder");
                        }
                        else
                        {
                            this.lblValidFechaFinPoder.Visible = false;
                            vEsrequerido = false;
                        }
                    }
                    else
                    {
                        this.lblValidFechaFinPoder.Visible = false;
                        vEsrequerido = false;
                    }

                    if (this.ddlTipoIdentificacion.SelectedValue.Equals("-1"))
                    {
                        this.lblRequeridoTipoIdent.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoTipoIdent.Visible = false;
                        vEsrequerido = false;
                    }

                    if (string.IsNullOrEmpty(this.txtNumeroIdentificacionApoderado.Text))
                    {
                        this.lblRequeridoNumIdent.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoNumIdent.Visible = false;
                        vEsrequerido = false;
                    }

                    if (string.IsNullOrEmpty(this.txtNumeroTarjetaProfesional.Text))
                    {
                        this.lblRequeridoNumeroTarjetaProfesional.Visible = true;
                        vEsrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoNumeroTarjetaProfesional.Visible = false;
                        vEsrequerido = false;
                    }

                    if (string.IsNullOrEmpty(this.rblEstadoApoderado.SelectedValue))
                    {
                        Apoderados vApoderado = new Apoderados();
                        vApoderado.FechaFinPoder = this.FechaFinPoder.Date;
                        vApoderado.FechaInicioPoder = this.FechaInicioPoder.Date;
                        this.ValidarFechas(vApoderado);
                        this.rblEstadoApoderado.SelectedValue = vApoderado.EstadoApoderado;
                    }

                    if (string.IsNullOrEmpty(this.hfIdTercero.Value.ToString()))
                    {
                        this.btnBuscar_Click(new Object(), new ImageClickEventArgs(1, 1));
                        vEsrequerido = true;
                    }
                }

                if (this.ddlTipoIdentificacion.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoTipoIdent.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoTipoIdent.Visible = false;
                    vEsrequerido = false;
                }

                if (string.IsNullOrEmpty(this.txtNumeroIdentificacionApoderado.Text))
                {
                    this.lblRequeridoNumIdent.Visible = true;
                    vEsrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoNumIdent.Visible = false;
                    vEsrequerido = false;
                }
            }

            return vEsrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return vEsrequerido;
        }
        catch (Exception ex)
        {
            if (vEsrequerido && vLimpiarMensaje)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }

            return vEsrequerido;
        }
    }

    private void Buscar(int pIdTercero)
    {
        try
        {
            bool vExistenCamposRequeridos = false;
            Tercero vTercero = new Tercero();
            if (pIdTercero == 0)
            {
                vExistenCamposRequeridos = this.ExistenCamposApoderadoRequeridos(false);
                if (!vExistenCamposRequeridos)
                {
                    vTercero = this.vMostrencosService.ConsultarTercero(Convert.ToInt32(this.ddlTipoIdentificacion.SelectedValue), this.txtNumeroIdentificacionApoderado.Text, this.txtTipoPersona.Text);
                }
            }
            else
            {
                vTercero = this.vMostrencosService.ConsultarTerceroXId(pIdTercero);
            }

            if (!vExistenCamposRequeridos)
            {
                if (vTercero.IdTercero != 0)
                {
                    this.CargarDatosTercero(vTercero);
                    this.toolBar.LipiarMensajeError();
                }
                else
                {
                    this.toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Limpia los datos del tercero
    /// </summary>
    private void LimpiarDatosTercero()
    {
        try
        {
            this.CargarListaDesplegable();
            this.hfIdTercero.Value = string.Empty;
            this.txtTipoPersona.Text = "NATURAL";
            this.ddlTipoIdentificacion.SelectedValue = "-1";
            this.txtNumeroIdentificacionApoderado.Text = string.Empty;
            this.txtPrimerNombreApoderado.Text = string.Empty;
            this.txtSegundoNombreApoderado.Text = string.Empty;
            this.txtPrimerApellidoApoderado.Text = string.Empty;
            this.txtSegundoApellidoApoderado.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtDepartamento.Text = string.Empty;
            this.txtMunicipio.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtIndicativo.Text = string.Empty;
            this.txttelefono.Text = string.Empty;
            this.txtCelular.Text = string.Empty;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// cambia el etado del apoderado
    /// </summary>
    public void CambiarEstadoApoderado()
    {
        try
        {
            if (this.FechaFinPoder.Date == Convert.ToDateTime("01/01/1900") || this.FechaFinPoder.Date > DateTime.Now.Date)
            {
                this.rblEstadoApoderado.SelectedValue = "ACTIVO";
            }
            else
            {
                this.rblEstadoApoderado.SelectedValue = "INACTIVO";
            }
        }
        catch (Exception)
        {
            throw;
        }

    }

    /// <summary>
    /// Limpia los Labels del tercero
    /// </summary>
    private void LimpiarLabelsTercero()
    {
        try
        {
            this.lblRequeridoTipoIdent.Visible = false;
            this.lblRequeridoNumIdent.Visible = false;
            this.lblRequeridoEstadoApoderado.Visible = false;
            this.lblRequeridoNumeroTarjetaProfesional.Visible = false;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los datos del tercero
    /// </summary>
    /// <param name="pTercero">Variable de tipo tercero con los datos a cargar</param>
    private void CargarDatosTercero(Tercero pTercero)
    {
        try
        {
            this.hfIdTercero.Value = pTercero.IdTercero.ToString();
            this.txtTipoPersona.Text = pTercero.NombreTipoPersona;
            this.txtPrimerNombreApoderado.Text = pTercero.PrimerNombre;
            if (pTercero.NombreTipoPersona != "NATURAL")
            {
                string vNombreTipoPersona = this.vMostrencosService.ConsultarTiposIdentificacionGlobal(pTercero.IdTipoDocIdentifica.ToString()).FirstOrDefault().NomTipoDocumento;
                this.ddlTipoIdentificacion.Items.Insert(this.ddlTipoIdentificacion.Items.Count, new ListItem(vNombreTipoPersona, pTercero.IdTipoDocIdentifica.ToString()));
                this.txtPrimerNombreApoderado.Text = pTercero.RazonSocial;
            }

            this.ddlTipoIdentificacion.SelectedValue = pTercero.IdTipoDocIdentifica.ToString();
            this.txtNumeroIdentificacionApoderado.Text = pTercero.NumeroIdentificacion;
            this.txtSegundoNombreApoderado.Text = pTercero.SegundoNombre;
            this.txtPrimerApellidoApoderado.Text = pTercero.PrimerApellido;
            this.txtSegundoApellidoApoderado.Text = pTercero.SegundoApellido;
            this.txtDireccion.Text = pTercero.Direccion;
            this.txtDepartamento.Text = pTercero.NombreDepartamento;
            this.txtMunicipio.Text = pTercero.NombreMunicipio;
            this.txtEmail.Text = pTercero.CorreoElectronico;
            this.txtIndicativo.Text = pTercero.Indicativo;
            this.txttelefono.Text = pTercero.Telefono;
            this.txtCelular.Text = pTercero.Celular;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// limpia los campos
    /// </summary>
    private void LimpiarCampos()
    {
        try
        {

            this.lblCantidadCaracteresSolicitado.Visible = false;
            this.lblrequeridoDocumentoSoporte.Visible = false;
            this.lblRequeridoFechaRecibido.Visible = false;
            this.lblRequeridoNombreArchivo.Visible = false;
            this.ddlDocumentoSoporte.SelectedValue = "-1";
            this.txtObserbacionesDocumentoSolicitado.Text = string.Empty;
            this.rbtEstadoDocumentoAceptado.Checked = false;
            this.rbtEstadoDocumentoDevuelto.Checked = false;
            this.txtObservacionesRecibido.Text = string.Empty;
            this.hfLimpiarFechaSolicitud.Value = "1";
            this.hfIdDocumentoSolocitado.Value = "0";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga los datos de los documentos solicitados 
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void CargarDatosDocumentoSolocitado(DocumentosSolicitadosDenunciaBien pDocumentoSolicitado)
    {
        try
        {
            this.hfIdDocumentoSolocitado.Value = pDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
            this.ddlDocumentoSoporte.SelectedValue = pDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
            this.FechaSolicitud.Date = Convert.ToDateTime(pDocumentoSolicitado.FechaSolicitud);
            this.txtObserbacionesDocumentoSolicitado.Text = pDocumentoSolicitado.ObservacionesDocumentacionSolicitada;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Valida las fechas fguardar
    /// </summary>
    /// <param name="pDocumentoSolicitado"></param>
    private void ValidarFechas(Apoderados pApoderado)
    {
        try
        {
            if (this.rblExisteApoderadoSi.Checked)
            {
                if (pApoderado.FechaFinPoder != null || pApoderado.FechaFinPoder != Convert.ToDateTime("1/01/1900"))
                {
                    if (Convert.ToDateTime(pApoderado.FechaFinPoder).Date > Convert.ToDateTime(pApoderado.FechaInicioPoder).Date && Convert.ToDateTime(pApoderado.FechaFinPoder).Date > DateTime.Now.Date)
                    {
                        pApoderado.EstadoApoderado = "ACTIVO";
                    }
                    else
                    {
                        pApoderado.EstadoApoderado = "INACTIVO";
                    }
                }
                else if (Convert.ToDateTime(pApoderado.FechaInicioPoder).Date < DateTime.Now.Date)
                {
                    pApoderado.EstadoApoderado = "ACTIVO";
                }
                else
                {
                    pApoderado.EstadoApoderado = "INACTIVO";
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// valida que se debe hacer cuando se selecciona un valor del campo existe apoderado
    /// </summary>
    public void ValidacionChecked()
    {
        bool vEsVisible = this.rblExisteApoderadoSi.Checked;
        this.pnlInformacionApoderado.Visible = vEsVisible;
        this.pnlDocumentacionSolicitada.Visible = vEsVisible;
        if (vEsVisible)
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
            this.rblExisteApoderadoSi.Checked = true;
            this.rblExisteApoderadoNo.Checked = false;
        }
        else
        {
            this.rblExisteApoderadoSi.Checked = false;
            this.rblExisteApoderadoNo.Checked = true;
        }

        this.lblDenuncianteApoderado.Visible = vEsVisible;
        this.lblFechaFinPoder.Visible = vEsVisible;
        this.lblFechaInicioPoder.Visible = vEsVisible;
        this.rblDenuncianteEsApoderadoSi.Visible = vEsVisible;
        this.rblDenuncianteEsApoderadoNo.Visible = vEsVisible;
        this.FechaInicioPoder.Visible = vEsVisible;
        this.FechaFinPoder.Visible = vEsVisible;
        this.lblObservaciones.Visible = vEsVisible;
        this.txtObservaciones.Visible = vEsVisible;
    }

    /// <summary>
    /// valida que las fechas no esten vacias
    /// </summary>
    private void ValidarCamposFecha()
    {
        if (this.FechaSolicitud.Date == Convert.ToDateTime("01/01/1900"))
        {
            this.FechaSolicitud.Date = DateTime.Now.Date;
        }
    }

    #endregion


}