<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_AsignacionAbogado_List" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>

<%--<%@ Register Src="~/General/General/Control/fechaFestivos.ascx" TagPrefix="uc1" TagName="fecha" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Departamento Ubicaci&oacute;n *
                </td>
                <td>Estado *
                   <asp:CustomValidator runat="server"
                        ID="CustomValidator2"  ForeColor="Red"
                        ControlToValidate="ddlEstado"
                        ClientValidationFunction="ValidEstado" ValidationGroup="btnBuscar"
                        ErrorMessage="Campo Requerido" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDepartamento" Enabled="false" Width="50%"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlEstado"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Registrado Desde*
                    <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacion" ControlToValidate="cuFechaDesde$txtFecha"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                    ID="valDateRange"  ForeColor="Red"
                    ControlToValidate="cuFechaDesde$txtFecha"
                    ClientValidationFunction="validateDate" ValidationGroup="btnBuscar"
                    ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar" />
                </td>
                <td>Registrado Hasta*
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="cuFechaHasta$txtFecha"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator1"  ForeColor="Red"
                        ControlToValidate="cuFechaHasta$txtFecha"
                        ClientValidationFunction="validateDate" ValidationGroup="btnBuscar"
                        ErrorMessage="La fecha seleccionada no es correcta. Por favor revisar" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:CompareValidator ID="cvFecha" runat="server" Type="Date" Operator="DataTypeCheck"
                            ControlToValidate="cuFechaDesde$txtFecha" ErrorMessage="Fecha inv&aacute;lida." SetFocusOnError="True"
                            ValidationGroup="btnBuscar" Display="Dynamic" ForeColor="Red">
                        </asp:CompareValidator>
                    <uc1:fecha ID="cuFechaDesde" runat="server" />

                </td>
                <td>
                      <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Date" Operator="DataTypeCheck"
                            ControlToValidate="cuFechaHasta$txtFecha" ErrorMessage="Fecha inv&aacute;lida." SetFocusOnError="True"
                            ValidationGroup="btnBuscar" Display="Dynamic" ForeColor="Red">
                        </asp:CompareValidator>
                    <uc1:fecha ID="cuFechaHasta" runat="server" />

                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la Denuncia
                <Ajax:FilteredTextBoxExtender ID="FFFTTT" runat="server" TargetControlID="txtNumeroRadicado"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/123456789������������ .,@_():;" />
               <%--<asp:RegularExpressionValidator ID="regexpName" runat="server"
                   ErrorMessage="Formato incorrecto"
                   ControlToValidate="txtNumeroRadicado" ForeColor="Red"
                   ValidationExpression="^[0-9a-zA-Z]+$" ValidationGroup="btnBuscar" />--%>
                </td>
                <td></td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroRadicado" MaxLength="20"></asp:TextBox>
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAsignacionAbogado" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDenunciaBien" CellPadding="0" Height="16px" OnSorting="gvAsignacionAbogado_Sorting"
                        OnPageIndexChanging="gvAsignacionAbogado_PageIndexChanging" OnSelectedIndexChanged="gvAsignacionAbogado_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Radicado denuncia" DataField="RadicadoDenuncia" SortExpression="RadicadoDenuncia" />
                            <asp:BoundField HeaderText="Tipo de identificaci&oacute;n" DataField="Tercero.TiposDocumentosGlobal.NomTipoDocumento" SortExpression="NombreTipoIdentificacion" />
                            <asp:BoundField HeaderText="Numero Identificaci&oacute;n" DataField="Tercero.NumeroIdentificacion" SortExpression="NUMEROIDENTIFICACION"/>
                            <asp:BoundField HeaderText="Nombre / Raz&oacute;n social" DataField="NombreMostrar" SortExpression="NombreMostrar"/>
                            <asp:BoundField HeaderText="Fecha de radicado denuncia"  DataField="FechaRadicadoDenuncia" SortExpression="FechaRadicadoDenuncia" DataFormatString="{0:d}"/>                            
                            <asp:BoundField HeaderText="Departamento ubicaci&oacute;n" DataField="Regional.NombreRegional" SortExpression="NombreRegionalUbicacion"/>
                             <asp:BoundField HeaderText="Estado" DataField="EstadoDenuncia.NombreEstadoDenuncia" SortExpression="NombreEstadoDenuncia"/>
                            <asp:BoundField HeaderText="Fecha Estado" DataField="FechaEstado" SortExpression="FechaEstado" DataFormatString="{0:d}"/>
                            <asp:BoundField HeaderText="Abogado asignado" DataField="Abogados.NombreAbogado" SortExpression="NombreAbogado"/>
                            <asp:BoundField HeaderText="Fecha asignaci&oacute;n abogado"  DataField="Abogados.FechaAsignacionAbogado" SortExpression="FechaAsignacionAbogado" DataFormatString="{0:d}"/>
                           </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript">
        function validateDate(source, arguments) {
            //debugger;
            var EnteredDate = arguments.Value; //for javascript
            
            var date = EnteredDate.substring(0, 2);
            var month = EnteredDate.substring(3, 5);
            var year = EnteredDate.substring(6, 10);

            var myDate = new Date(year, month - 1, date);

            var fechaDesde = document.getElementById('cphCont_cuFechaHasta_txtFecha').value;
            var fechaHasta = document.getElementById('cphCont_cuFechaDesde_txtFecha').value;


            var today = new Date();

            if (myDate > today) {
                arguments.IsValid = false;
            }
            else if (fechaDesde != '' && fechaHasta != '') {
                var dateD = fechaDesde.substring(0, 2);
                var monthD = fechaDesde.substring(3, 5);
                var yearD = fechaDesde.substring(6, 10);
                var myDateD = new Date(yearD, monthD - 1, dateD);

                var dateH = fechaHasta.substring(0, 2);
                var monthH = fechaHasta.substring(3, 5);
                var yearH = fechaHasta.substring(6, 10);
                var myDateH = new Date(yearH, monthH - 1, dateH);

                if (myDateH > myDateD) {
                    arguments.IsValid = false;
                }

            }
            else {
                arguments.IsValid = true;
            }

        }

        function ValidEstado(source, arguments) {
            if (arguments.Value == "-1") {
                arguments.IsValid = false;
            }
        }

    </script>
</asp:Content>
