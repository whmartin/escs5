//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_AsignacionAbogado_List
/// </summary>
public partial class Page_AsignacionAbogado_List : GeneralWeb
{
    /// <summary>
    /// Declaracion Toolbar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/AsignacionAbogado";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// preinicia la pagina
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Carga la pagina
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// evento del boton Buscar
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.toolBar.LipiarMensajeError();
        this.SaveState(this.Master, PageName);
        this.Buscar();
    }

    /// <summary>
    /// Evento que se activa al realizar click sobre la cabecera de alguna columna de la grilla,
    /// Ordenando la columna seleccionada
    /// </summary>
    /// <param name="sender">The Grid view</param>
    /// <param name="e">The Sorting</param>
    protected void gvAsignacionAbogado_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DenunciaBien> vList = new List<DenunciaBien>();
        vList = this.Buscar();

        switch (e.SortExpression)
        {
            case "RadicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.RadicadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.RadicadoDenuncia).ToList();
                }

                break;

            case "NombreTipoIdentificacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.Tercero.TiposDocumentosGlobal.NomTipoDocumento).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.Tercero.TiposDocumentosGlobal.NomTipoDocumento).ToList();
                }

                break;

            case "NUMEROIDENTIFICACION":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.Tercero.NumeroIdentificacion).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.Tercero.NumeroIdentificacion).ToList();
                }

                break;

            case "NombreMostrar":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.NombreMostrar).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.NombreMostrar).ToList();
                }

                break;

            case "FechaRadicadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.FechaRadicadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.FechaRadicadoDenuncia).ToList();
                }

                break;

            case "NombreRegionalUbicacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.Regional.NombreRegional).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.Regional.NombreRegional).ToList();
                }

                break;

            case "NombreEstadoDenuncia":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.EstadoDenuncia.NombreEstadoDenuncia).ToList();
                }

                break;

            case "NombreAbogado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.Abogados.NombreAbogado).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.Abogados.NombreAbogado).ToList();
                }

                break;

            case "FechaAsignacionAbogado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.Abogados.FechaAsignacionAbogado).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.Abogados.FechaAsignacionAbogado).ToList();
                }

                break;

            case "FechaEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vList = vList.OrderBy(a => a.FechaEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vList = vList.OrderByDescending(a => a.FechaEstado).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.gvAsignacionAbogado.DataSource = vList;
        this.gvAsignacionAbogado.DataBind();
    }

    /// <summary>
    /// Busca las denuncias filtrando siempre por la regional del usuario que inici� la sesi�n y lass denuncias en estado REGISTRADA
    /// adem�s de los filtros opcionales como fechas desde y hasta y numero de radicado de denuncia
    /// </summary>
    private List<DenunciaBien> Buscar()
    {
        List<DenunciaBien> vLista = new List<DenunciaBien>();
        try
        {
            String vNumeroRadicado = null;
            if (txtNumeroRadicado.Text != "")
            {
                vNumeroRadicado = Convert.ToString(txtNumeroRadicado.Text);
            }
            else
            {
                vNumeroRadicado = "-1";
            }

            int vIdAbogado = this.GetSessionUser().Rol.Equals("Abogado") ? this.vMostrencosService.ConsultarAbogadosPorIdUsuario(this.GetSessionUser().IdUsuario).IdAbogado : 0;
            vLista = this.vMostrencosService.ConsultarDenunciaBien(Convert.ToInt32(this.GetSessionUser().IdRegional), Convert.ToInt32(this.ddlEstado.Text), this.cuFechaDesde.Date.ToString("yyyy-MM-dd"), this.cuFechaHasta.Date.ToString("yyyy-MM-dd"), vNumeroRadicado, vIdAbogado);
            this.gvAsignacionAbogado.DataSource = vLista;
            this.gvAsignacionAbogado.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

        return vLista;
    }

    /// <summary>
    /// Establece los botones y datos iniciales de la entidad, tales como : Nomre de la regional y Estado REGISTRADo de la denuncia
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);

            this.gvAsignacionAbogado.PageSize = this.PageSize();
            this.gvAsignacionAbogado.EmptyDataText = this.EmptyDataText();

            this.toolBar.EstablecerTitulos("Asignar Denuncia", SolutionPage.List.ToString());

            Icbf.SIA.Entity.Usuario vusuario = (Usuario)(this.GetSessionParameter("App.User"));
            this.txtDepartamento.Text = this.vMostrencosService.CosultarRegionalUsuario(vusuario.IdRegional ?? 0);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// guarda datos com: idDenuncia,Numero de radicado denuncia y fecha de denuncia para ser lelvados al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvAsignacionAbogado.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("AsignacionAbogado.IdDenuncia", strValue);

            this.SetSessionParameter("AsignacionAbogado.NumeroDen", this.gvAsignacionAbogado.Rows[rowIndex].Cells[1].Text);
            this.SetSessionParameter("AsignacionAbogado.FechaDen", this.gvAsignacionAbogado.Rows[rowIndex].Cells[5].Text);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvAsignacionAbogado_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvAsignacionAbogado.SelectedRow);
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvAsignacionAbogado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvAsignacionAbogado.PageIndex = e.NewPageIndex;
        this.Buscar();
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            if (this.GetSessionParameter("AsignacionAbogado.Guardado").ToString() == "1")
                this.toolBar.MostrarMensajeGuardado("Abogado asignado a la denuncia exitoso");
            this.RemoveSessionParameter("AsignacionAbogado.Guardado");
            this.ddlEstado.DataSource = this.vMostrencosService.ConsultarEstadosDenuncia();
            this.ddlEstado.DataTextField = "NombreEstadoDenuncia";
            this.ddlEstado.DataValueField = "IdEstadoDenuncia";
            this.ddlEstado.DataBind();
            this.ddlEstado.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value> 
    public SortDirection Direction
    {
        get
        {
            if (this.Session["directionState"] == null)
            {
                this.Session["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.Session["directionState"];
        }

        set
        {
            this.Session["directionState"] = value;
        }
    }
}
