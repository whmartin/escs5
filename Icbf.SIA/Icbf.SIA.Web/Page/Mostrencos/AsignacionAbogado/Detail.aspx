<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_AsignacionAbogado_Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="Server">
<script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
<script type="text/javascript" src="../../../Scripts/Utilidades.js"></script>

<asp:HiddenField ID="hfIdDenunciaBien" runat="server" />
<asp:HiddenField runat="server" ID="hfNumeroDen" />
<asp:HiddenField runat="server" ID="hffechaDen" />

    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Informaci&oacute;n b&aacute;sica
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                Tipo de identificaci&oacute;n *
            </td>
            <td>
                N&uacute;mero de identificaci&oacute;n *
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoDocIdentifica"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Tipo de persona o asociaci&oacute;n
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoPersona"  Enabled="false"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Label runat="server" Text="Primer nombre" ID="lblPrimerNombre"></asp:Label></td>
            <td>
                <asp:Label runat="server" Text="Segundo nombre" ID="lblSegundoNombre"></asp:Label></td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Label runat="server" Text="Primer apellido" ID="lblPrimerApellido"></asp:Label></td>
            <td>
                <asp:Label runat="server" Text="Segundo apellido" ID="lblSegundoApellido"></asp:Label></td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Informaci&oacute;n de residencia
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                Departamento
            </td>
            <td>
                Municipio
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdDepartamento"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdMunicipio"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Zona
            </td>
            <td>
                Direcci&oacute;n
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIdZona"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDireccion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Indicativo
            </td>
            <td>
                Tel&eacute;fono
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtIndicativo"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTelefono"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Celular
            </td>
            <td>
                Correo electr&oacute;nico
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtCelular"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCorreoElectronico"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Informaci&oacute;n de la denuncia
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                DECLARO VOLUNTARIAMENTE DE BUENA FE, EN NOMBRE PROPIO O DE UN TERCERO, BAJO LA GRAVEDAD DE JURAMENTO,
                LA EXISTENCIA DE UNA VOCACI�N HEREDITARIA, BIEN VACANTE O MOSTRENCO REPRESENTADA EN LOS DOCUMENTOS
                QUE SE ANEXAN, ASI COMO EL PROPOSITO DE CELEBRAR EL RESPECTIVO CONTRATO DE PARTICIPACION Y EN SENTIDO
                OBTENER LA DECLARACION JUDICIAL DE LOS BIENES DENUNCIADOS HASTA SU ADJUDICACION AL ICBF
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <table style="width:100%">
                    <tr>
                        <th></th>
                        <th align="center" class="auto-style1">
                            <asp:CheckBox runat="server" 
                                ID="chkAcepto" 
                                Text="Acepto *" 
                                Enabled="false"
                                CssClass="cbRegistroDenuncia"/>
                        </th> 
                        <th></th>
                    </tr>
                </table>
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Descripci&oacute;n de la Denuncia *
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionDenuncia" TextMode="MultiLine"  Enabled="false" CssClass="DescripcionDenuncia"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Departamento de ubicaci&oacute;n *
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdRegionalUbicacion"  Enabled="false" Width="50%"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Documento digital / certificados a adjuntar
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td class="tdTitulos">
                Nombre del Documento
            </td>
            <td class="tdTitulos">
                Documento
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Denuncia con datos del denunciante y de los bienes denunciados *
            </td>
            <td>
                <%--<asp:HyperLink runat="server" ID="hlDenuncia"></asp:HyperLink>--%>
                <asp:Label runat="server" ID="hlDenuncia"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Label runat="server" Text=" C&eacute;dula de ciudadania del denunciante *" ID="lblCedulaDenunciante"></asp:Label>
            </td>
            <td>
                <%--<asp:HyperLink runat="server" ID="hlCedula"></asp:HyperLink>--%>
                <asp:Label runat="server" ID="hlCedula"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Certificado de defunci&oacute;n
            </td>
            <td>
                <%--<asp:HyperLink runat="server" ID="hlDefuncion"></asp:HyperLink>--%>
                <asp:Label runat="server" ID="hlDefuncion"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Otros documentos
            </td>
        </tr>
       <tr class="rowA">
            <td colspan="2">
                <asp:GridView ID="gvOtrosDocumentos" runat="server" AutoGenerateColumns="False" HeaderStyle-HorizontalAlign="Center" DataKeyNames="IdDenunciaBien" HorizontalAlign="Center" Width="100%" >
                    <Columns>
                        <asp:BoundField DataField="ObservacionesDocumento" HeaderText="Observaci&oacute;nes del documento" />
                        <asp:BoundField DataField="NombreArchivo" HeaderText="Documento" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowAG" />
                    <HeaderStyle CssClass="GrillaOtrosDocs" HorizontalAlign="Center" BackColor="#81BA3D" />
                    <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Informaci&oacute;n de correspondencia ICBF
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBox runat="server" 
                    ID="cbDenunciaRecibidaCorrespondenciaPorInterna" 
                    Text="&iquest;Denuncia recibida por correspondencia interna? *" 
                    Enabled="false"
                    CssClass="cbRegistroDenuncia"/>
            </td>
        </tr>
        <tr class="rowA">
                <td>
                    <asp:Label runat="server" Text="Radicado en correspondencia" ID="lblRadicadoCorrespondencia"></asp:Label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:Label runat="server" Text="Fecha de radicado en correspondencia" ID="lblFechaRadicadoCorrespondencia"></asp:Label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:Label runat="server" Text="Hora de radicado en correspondencia" ID="lblHoraRadicadoCorrespondencia"></asp:Label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtHoraRadicadoCorrespondencia" Enabled="false"></asp:TextBox></td>
                <Ajax:MaskedEditExtender ID="meetxtHoraRadicadoCorrespondencia" runat="server" CultureDateFormat="hh:mm:ss" UserTimeFormat="TwentyFourHour"
                    CultureTimePlaceholder=":" AcceptAMPM="false" Enabled="True" Mask="99:99:99" AutoComplete="false" MessageValidatorTip="true"
                    MaskType="Time" TargetControlID="txtHoraRadicadoCorrespondencia">
                </Ajax:MaskedEditExtender>
                <Ajax:MaskedEditValidator ID="mevtxtHoraRadicadoCorrespondencia" runat="server" ControlExtender="meetxtHoraRadicadoCorrespondencia"
                    ControlToValidate="txtHoraRadicadoCorrespondencia" InvalidValueMessage="La hora digitada no es correcta. Por favor revisar" SetFocusOnError="true"
                    Display="Dynamic" ForeColor="Red" ValidationGroup="btnGuardar" MaximumValue="23:59:59">
                </Ajax:MaskedEditValidator>
            </tr>



         <tr class="rowB">
            <td colspan="2" class="tdTitulos">
                Asignaci&oacute;n Abogado
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nombre de Abogado *  
                <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                  ControlToValidate="txtNombreAbogado"
                  ErrorMessage="Campo Requerido"
                  ForeColor="Red" ValidationGroup="btnGuardar"> 
                </asp:RequiredFieldValidator>             
            </td>
            <td>
                Fecha de Asignaci&oacute;n del Abogado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbogado"  Enabled="false" Width="50%"></asp:TextBox>
                <asp:Image ID="imgBuscaAbogado" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        Style="cursor: hand" ToolTip="Buscar" Visible="True" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaAsignacion"  Enabled="false" Width="50%"></asp:TextBox>
            </td>
        </tr>
    </table>

    <div style="display: none;padding-left:200px;">
        <ajax:ModalPopupExtender ID="ModalPopupExtenderAbogado" runat="server" BackgroundCssClass="modalBackground"
            PopupControlID="pnlConsultaAbogadoPopUp" TargetControlID="imgBuscaAbogado" BehaviorID="ModalPopupExtenderAbogado">
        </ajax:ModalPopupExtender>
    </div>

    <asp:UpdatePanel ID="pnlConsultaAbogadoPopUp" runat="server" BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px"
        HorizontalAlign="Center" Width="200px" Height="250px"
        ScrollBars="None" Style="display: none; background-color: white" ChildrenAsTriggers="true" UpdateMode="Conditional">

        <ContentTemplate>
                <table cellpadding="0" cellspacing="0" id="fT">
                <tr>
                    <td>
                        <h2> <asp:Image ID="imgPrograma" ImageUrl="~/Image/main/prog-list.png" runat="server"
                                        alt="programa" CssClass="CI" />
                                    <asp:Label ID="lblTitulo" runat="server">Buscar Abogado</asp:Label></h2>
                    </td>
                    <td>
                        <asp:ImageButton ID="btnBuscarAbogados" runat="server" runat="server" ImageUrl="~/Image/btn/list.png"  ValidationGroup="btnBuscarAbogados" OnClick="btnBuscarAbogados_Click" Style="float: right; width: 25px; height: 25px"></asp:ImageButton>
                    </td>  
                    <td>
                        <asp:ImageButton ID="ImageButtonCerrarPopUp" runat="server" ImageUrl="~/Image/btn/cancel.jpg" OnClick="ImageButtonCerrarPopUp_Click" ValidationGroup="ImageButtonCerrarPopUp_Click" Style="width: 25px; height: 25px"></asp:ImageButton>
                    </td>               
                </tr>
                </table>

                <table cellpadding="0" cellspacing="0">
                <tr class="rowB">
                    <td style="padding-left:30px">
                        Nombre del Abogado*
                        <asp:RequiredFieldValidator id="rqVAlidateEmpty" runat="server"
                          ControlToValidate="txtNombreAbogadoConsulta"
                          ErrorMessage=" Campo Requerido"
                          ForeColor="Red" ValidationGroup="btnBuscarAbogados">
                        </asp:RequiredFieldValidator>
                        <Ajax:FilteredTextBoxExtender ID="ftObservacionesDocumento" runat="server" TargetControlID="txtNombreAbogadoConsulta"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;" />
                    </td>          
                    <td>
                        Profesi&oacute;n
                         <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProfesion"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;" />
                    </td>           
                </tr>
                <tr class="rowA">
                    <td style="padding-left:30px"> <asp:TextBox runat="server" ID="txtNombreAbogadoConsulta" MaxLength="30" ></asp:TextBox></td>          
                    <td> <asp:TextBox runat="server" ID="txtProfesion" MaxLength="30" ></asp:TextBox></td>           
                </tr>

               <tr>
                    <td style="text-align: left;" colspan="4">
                        <div style="overflow-y: auto; overflow-x: auto; height: 250px;">
                            <asp:GridView ID="gvwConsultaAbogados" runat="server" Visible="true" 
                                DataKeyNames="IdAbogado,TipoFuncionario,IdUsuario,IdRegionalAbogado,NombreRegionalAbogado,Correo" 
                                AllowSorting="True" AllowPaging="true"
                                AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                                Width="99%" EnableViewState="true" OnSelectedIndexChanged="gvwConsultaAbogados_SelectedIndexChanged"
                                OnPageIndexChanging="gvwConsultaAbogados_PageIndexChanging"
                                 EmptyDataText="No se encontraron resultados, verifique por favor">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                   <asp:TemplateField>
                                        <ItemTemplate>
                                        <asp:ImageButton ID="btnApply" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                            Height="16px" Width="16px" ToolTip="Aplicar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="Nombre" DataField="NombreAbogado" />
                                    <asp:BoundField HeaderText="Identificaci&oacute;n" DataField="Identificacion" />
                                    <asp:BoundField HeaderText="Profesi&oacute;n" DataField="Profesion" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblErrorPopup" runat="server" Text="" Style="color: red"></asp:Label>
                        <asp:Label ID="lblInfoPopup" runat="server" Text="" Style="color: green"></asp:Label>
                    </td>
                </tr>
            </table>

            <asp:HiddenField runat="server" ID="hfIdAbogado" />
            <asp:HiddenField runat="server" ID="hfNombreAbogado" />
            <asp:HiddenField runat="server" ID="hfIdRegional" />
            <asp:HiddenField runat="server" ID="hfNombreregional" />
            <asp:HiddenField runat="server" ID="hfTipoFuncionario" />
            <asp:HiddenField runat="server" ID="hfIdUsuario" />
            <asp:HiddenField runat="server" ID="hfIdentificacion" />
            <asp:HiddenField runat="server" ID="hfCorreo" />

        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="ImageButtonCerrarPopUp" />
            <asp:PostBackTrigger ControlID="gvwConsultaAbogados" />
            <asp:AsyncPostBackTrigger ControlID="btnBuscarAbogados" EventName="click" />

        </Triggers>

    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <link href="../../../Styles/modalPopupExtender.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 115px;
        }
    </style>
</asp:Content>



