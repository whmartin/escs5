using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using System.Configuration;
using System.Globalization;

public partial class Page_AsignacionAbogado_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Mostrencos/AsignacionAbogado";
    MostrencosService vMostrencosService = new MostrencosService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    #region Events
    /// <summary>
    /// Evento que cierra el popup de la búsqueda de Abogados.
    /// </summary>
    /// <param name="sender">The Sender</param>
    /// <param name="e">E<see cref="ImageClickEventArgs"/>Instancia que contiene los datos del evento.</param>
    protected void ImageButtonCerrarRpt_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            CerrarPopUp();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
   
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void gvwConsultaAbogados_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvwConsultaAbogados.SelectedRow);
    }
    protected void gvwConsultaAbogados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvwConsultaAbogados.PageIndex = e.NewPageIndex;
        Buscar();
        this.ModalPopupExtenderAbogado.Show();
    }
    protected void btnBuscarAbogados_Click(object sender, ImageClickEventArgs e)
    {
        //SaveState(this.Master, PageName);
        Buscar();
    }
   
    protected void ImageButtonCerrarPopUp_Click(object sender, ImageClickEventArgs e)
    {
        CerrarPopUp();
    }
    #endregion

    #region Methods

    // <summary>
    /// Carga los datos relacionados con la denuncia, tales como: Datos de tercero, documentos y losd e la misma denuncia
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            String vIdDenuncia = Convert.ToString(GetSessionParameter("AsignacionAbogado.IdDenuncia"));
            RemoveSessionParameter("AsignacionAbogado.IdDenuncia");
            hfIdDenunciaBien.Value = vIdDenuncia;

            hfNumeroDen.Value = GetSessionParameter("AsignacionAbogado.NumeroDen") == null ? string.Empty : Convert.ToString(GetSessionParameter("AsignacionAbogado.NumeroDen"));
            hffechaDen.Value = GetSessionParameter("AsignacionAbogado.FechaDen") == null ? string.Empty : Convert.ToString(GetSessionParameter("AsignacionAbogado.FechaDen"));
            RemoveSessionParameter("AsignacionAbogado.NumeroDen");
            RemoveSessionParameter("AsignacionAbogado.FechaDen");            

            if (GetSessionParameter("AsignacionAbogado.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("AsignacionAbogado");

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarRegistroDenuncia(Convert.ToInt32(vIdDenuncia));

            hffechaDen.Value = vRegistroDenuncia.FechaRadicadoDenuncia.ToString();

            if (vRegistroDenuncia.CodDocumento == "NIT")
            {
                lblPrimerNombre.Text = "Razón social";
                txtPrimerNombre.Text = vRegistroDenuncia.RazonSocial;
                lblSegundoNombre.Visible = false;
                txtSegundoNombre.Visible = false;
                lblPrimerApellido.Visible = false;
                txtPrimerApellido.Visible = false;
                lblSegundoApellido.Visible = false;
                txtSegundoApellido.Visible = false;
            }
            else
            {
                lblPrimerNombre.Text = "Primer nombre";
                txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE;
                lblSegundoNombre.Visible = true;
                txtSegundoNombre.Visible = true;
                lblPrimerApellido.Visible = true;
                txtPrimerApellido.Visible = true;
                lblSegundoApellido.Visible = true;
                txtSegundoApellido.Visible = true;
                txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE;
                txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO;
                txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO;
            }

            hfIdDenunciaBien.Value = vRegistroDenuncia.IdDenunciaBien.ToString();
            txtIdTipoDocIdentifica.Text = vRegistroDenuncia.CodDocumento;
            txtNumeroIdentificacion.Text = vRegistroDenuncia.NUMEROIDENTIFICACION;
            txtIdTipoPersona.Text = vRegistroDenuncia.NombreTipoPersona;
            txtIdDepartamento.Text = vRegistroDenuncia.NombreDepartamento;
            txtIdMunicipio.Text = vRegistroDenuncia.NombreMunicipio;

            if (vRegistroDenuncia.IdZona == "1")
            {
                txtIdZona.Text = "Urbano";
            }
            else if (vRegistroDenuncia.IdZona == "2")
            {
                txtIdZona.Text = "Rural";
            }

            txtDireccion.Text = vRegistroDenuncia.Direccion;
            txtIndicativo.Text = vRegistroDenuncia.Indicativo;
            txtTelefono.Text = vRegistroDenuncia.Telefono;
            txtCelular.Text = vRegistroDenuncia.Celular;
            txtCorreoElectronico.Text = vRegistroDenuncia.CORREOELECTRONICO;

            if (vRegistroDenuncia.Acepto == true)
            {
                chkAcepto.Checked = true;
            }

            if (txtIdTipoDocIdentifica.Text == "NIT")
            {
                lblCedulaDenunciante.Text = "RUT *";
            }
            else
            {
                lblCedulaDenunciante.Text = "Cédula de ciudadania del denunciante *";
            }

            txtDescripcionDenuncia.Text = vRegistroDenuncia.DescripcionDenuncia;
            txtIdRegionalUbicacion.Text = vRegistroDenuncia.NombreRegional;

            this.CargarDenuncia();
            this.CargarCedula();
            this.CargarDefuncion();
            this.CargarOtrosDocumentos();

            if (vRegistroDenuncia.DenunciaRecibidaCorrespondenciaPorInterna == "S")
            {
                cbDenunciaRecibidaCorrespondenciaPorInterna.Checked = true;
                txtRadicadoCorrespondencia.Text = vRegistroDenuncia.RadicadoCorrespondencia;
                txtFechaRadicadoCorrespondencia.Text = Convert.ToString(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture));
                txtHoraRadicadoCorrespondencia.Text = vRegistroDenuncia.HoraRadicadoCorrespondencia;

            }
            if (vRegistroDenuncia.IdEstadoDenuncia != 1)
            {
                toolBar.OcultarBotonGuardar(false);
                imgBuscaAbogado.Visible = false;
                txtNombreAbogado.Text = vRegistroDenuncia.Abogado.NombreAbogado;
                txtFechaAsignacion.Text = Convert.ToString(vRegistroDenuncia.Abogado.FechaAsignacionAbogado);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Denuncia en el control para descargarlo
    /// </summary>
    private void CargarDenuncia()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 1);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                hlDenuncia.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            }

        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Cedula en el control para descargarlo
    /// </summary>
    private void CargarCedula()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 2);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                hlCedula.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            }

        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga el archivo Defunción en el control para descargarlo
    /// </summary>
    private void CargarDefuncion()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(hfIdDenunciaBien.Value);

            RegistroDenuncia vRegistroDenuncia = new RegistroDenuncia();
            vRegistroDenuncia = this.vMostrencosService.ConsultarDocumentosBasicos(vIdDenunciaBien, 3);

            if (vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.NombreArchivo != null)
            {
                hlDefuncion.Text = vRegistroDenuncia.DocumentosSolicitadosDenunciaBien.RutaArchivo.ToString();
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que consulta la lista de documentos de la sección otros
    /// </summary>
    /// <returns>Lista de documentos Otros</returns>
    private List<DocumentosSolicitadosDenunciaBien> CargarOtrosDocumentos()
    {
        List<DocumentosSolicitadosDenunciaBien> vLista = new List<DocumentosSolicitadosDenunciaBien>();

        try
        {
            int vIdDenuncia = Convert.ToInt32(hfIdDenunciaBien.Value);

            vLista = this.vMostrencosService.ConsultarOtrosDocumentos(vIdDenuncia, 4);
            this.Session["SortedView"] = vLista;

            if (vLista != null)
            {
                gvOtrosDocumentos.DataSource = vLista;
                gvOtrosDocumentos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

        return vLista;
    }
   
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Asignar denuncia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            txtFechaAsignacion.Text = DateTime.Now.ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Buscar()
    {
        Icbf.SIA.Entity.Usuario vUsuario = (Usuario)(GetSessionParameter("App.User"));
        AsignacionAbogado pConsultaAbogados = new AsignacionAbogado();
        pConsultaAbogados.IdRegionalAbogado = vUsuario.IdRegional ?? 0;
        pConsultaAbogados.NombreAbogado = txtNombreAbogadoConsulta.Text == string.Empty ? null : txtNombreAbogadoConsulta.Text;
        pConsultaAbogados.Profesion = txtProfesion.Text == string.Empty ? null : txtProfesion.Text;

        gvwConsultaAbogados.DataSource = vMostrencosService.ConsultarAbogados(pConsultaAbogados);
        gvwConsultaAbogados.DataBind();

    }

    /// <summary>
    /// método invocado desde el evento Selectedindex de la grilla Maestra, guarda los datos llave y direcciona la página de detalles
    /// </summary>
    /// <param name="pRow">Recibe el GridViewRow seleccionado de la grilla maestra</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvwConsultaAbogados.DataKeys[rowIndex].Value.ToString();
            
            hfIdAbogado.Value = strValue;
            hfTipoFuncionario.Value = gvwConsultaAbogados.DataKeys[rowIndex].Values[1].ToString().Replace(",", "");
            hfIdUsuario.Value = Convert.ToInt32(gvwConsultaAbogados.DataKeys[rowIndex].Values[2]).ToString().Replace(",", "");
            hfIdRegional.Value = string.IsNullOrEmpty(gvwConsultaAbogados.DataKeys[rowIndex].Values[3].ToString()) 
                                    ? "0"
                                    : Convert.ToInt32(gvwConsultaAbogados.DataKeys[rowIndex].Values[3]).ToString().Replace(",", "");
            hfNombreregional.Value = gvwConsultaAbogados.DataKeys[rowIndex].Values[4] == null ? "" : gvwConsultaAbogados.DataKeys[rowIndex].Values[4].ToString().Replace(",", "");
            hfCorreo.Value = gvwConsultaAbogados.DataKeys[rowIndex].Values[5] == null ? "" : gvwConsultaAbogados.DataKeys[rowIndex].Values[5].ToString().Replace(",", "");
            hfNombreAbogado.Value = Convert.ToString(gvwConsultaAbogados.Rows[rowIndex].Cells[1].Text).Replace(",", "");
            hfIdentificacion.Value = Convert.ToString(gvwConsultaAbogados.Rows[rowIndex].Cells[2].Text).Replace(",", "");

            txtNombreAbogado.Text = gvwConsultaAbogados.Rows[rowIndex].Cells[1].Text;

            CerrarPopUp();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CerrarPopUp()
    {
        txtNombreAbogadoConsulta.Text = string.Empty;
        txtProfesion.Text = string.Empty;
        gvwConsultaAbogados.DataSource = null;
        this.ModalPopupExtenderAbogado.Hide();
    }

    /// <summary>
    /// Método que guarda el abogado en la Entidad Abogados y actualiza el idAbogado, fechaAsignacion y estado de la denuncia la Entidad DenunciaBien
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;

            AsignacionAbogado vAsignacionAbogado = new AsignacionAbogado();
            vAsignacionAbogado.Identificacion = Convert.ToString(hfIdentificacion.Value);
            vAsignacionAbogado.TipoFuncionario = Convert.ToString(hfTipoFuncionario.Value);
            vAsignacionAbogado.IdUsuario = Convert.ToInt32(hfIdUsuario.Value);
            vAsignacionAbogado.IdRegionalAbogado = string.IsNullOrEmpty(hfIdRegional.Value) ? 0 : Convert.ToInt32(hfIdRegional.Value);
            vAsignacionAbogado.NombreRegionalAbogado = Convert.ToString(hfNombreregional.Value);
            vAsignacionAbogado.NombreAbogado = hfNombreAbogado.Value.ToString();
            vAsignacionAbogado.FechaAsignacionAbogado = DateTime.Now;
            vAsignacionAbogado.IdDenuncia = Convert.ToInt32(hfIdDenunciaBien.Value);
            vAsignacionAbogado.FechaCrea = DateTime.Now;
            vAsignacionAbogado.UsuarioCrea = GetSessionUser().IdUsuario.ToString();
            vAsignacionAbogado.Correo = Convert.ToString(hfCorreo.Value);
            InformacionAudioria(vAsignacionAbogado, this.PageName, vSolutionPage);
            vResultado = vMostrencosService.InsertarAsignacionAbogado(vAsignacionAbogado);
          
            if ( vResultado == 0 )
            {
                toolBar.MostrarMensajeError("No fue posible asignar abogado a la denuncia. Por favor inténtelo nuevamente.");
            }
            else //if (vResultado == 1)
            {
                this.EnviarNotificacion(vAsignacionAbogado);
                SetSessionParameter("AsignacionAbogado.IdAbogado", vAsignacionAbogado.IdAbogado);
                SetSessionParameter("AsignacionAbogado.Guardado", "1");
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    // <summary>
    /// Envia notificación al abogado de que tiene asignada una denuncia 
    /// </summary>
    /// <param name="pAbogado">Entidad AsignacionAbogado</param>
    private void EnviarNotificacion(AsignacionAbogado pAbogado)
    {
        string vEmailPara = pAbogado.Correo; //"lorena.guarnizo@ingenian.com";
        string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
        string vAsunto = string.Empty;
        string vMensaje = string.Empty;
        int vIdUsuario = 0;
        string vArchivo = string.Empty;

        vAsunto = HttpContext.Current.Server.HtmlDecode("Nueva asignaci&oacute;n de denuncia");

        string vNumerDenuncia = hfNumeroDen.Value == null ? string.Empty : hfNumeroDen.Value;
        DateTime vFechaDenuncia = Convert.ToDateTime(hffechaDen.Value.ToString());

        vMensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B> Asignaci&oacute;n de Denuncia </B> </CENTER> </font> ") +
                    "<br/> <br/> <font color='black'> <B> Apreciado(a) </B> </font> " + pAbogado.NombreAbogado + " <br/> <br/> Ha sido asignado a la siguiente denuncia: " +
                    "<br/> <br/> <br/> <B>  Radicado de la denuncia:  </B>" + "&nbsp;&nbsp;&nbsp;" + vNumerDenuncia +
                    "<br/> <br/> <B> Fecha y hora de radicado:   </B>" + "&nbsp;&nbsp;" + Convert.ToString(vFechaDenuncia) +
                    HttpContext.Current.Server.HtmlDecode("<br/> <br/> Por favor tener en cuenta para realizar la gesti&oacute;n necesaria </div>");    
        bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);
        //if (result)
        //{
        //    toolBar.MostrarMensajeError("El correro no pudo ser enviado. Por favor revisar ");
        //}
    }
    
    #endregion
}
