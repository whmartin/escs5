﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_RegistrarBienesDenunciados_List" %>

<%@ Register Src="~/General/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
    
<asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la Denuncia *</td>
                <td>Fecha de Radicado de la Denuncia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en Correspondencia *</td>
                <td>Fecha Radicado en Correspondencia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *</td>
                <td>Número de Identificación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Primer Nombre *</td>
                <td>Segundo Nombre *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Primer Apellido *</td>
                <td>Segundo Apellido *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la Denuncia *</td>
                <td style="width: 55%">Histórico de la Denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
               <tr class="rowB">
                <td>Valor de la Denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="TextValordelaDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
  
    <asp:Panel runat="server" ID="pnlInformacionInmueble">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2">
                </td>
            </tr> 
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del Mueble e Inmueble
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>           
            <tr>
                <td>
                    <asp:GridView ID="gvwInformacionInmueble" runat="server" AllowPaging="True" OnSelectedIndexChanged="gvwInformacionInmueble_SelectedIndexChanged" AllowSorting="True" OnPageIndexChanging="gvwInformacionInmueble_PageIndexChanging"
                         AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IdMuebleInmueble" GridLines="None" OnSorting="gvwInformacionInmueble_OnSorting" 
                        Height="16px" PageSize="10" Width="100%">
                        <Columns>                            
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" Height="16px" ImageUrl="~/Image/btn/info.jpg" ToolTip="Detalle" Width="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NombreTipoBien" HeaderText="Tipo de Bien" SortExpression="NombreTipoBien" />
                            <asp:BoundField DataField="NombreSubTipoBien" HeaderText="Subtipo de Bien" SortExpression="NombreSubTipoBien" />
                            <asp:BoundField DataField="NombreClaseBien" HeaderText="Clase de Bien" SortExpression="NombreClaseBien" />
                            <asp:BoundField DataField="EstadoBien" HeaderText="Estado del Bien" SortExpression="EstadoBien" />
                            <asp:BoundField DataField="DescripcionBien" HeaderText="Descripción del Bien" SortExpression="DescripcionBien" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="Panel1">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2">
                </td>
            </tr> 
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del Título Valor
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>           
            <tr>
                <td>
                    <asp:GridView ID="gvwInformacionTitulo" runat="server" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="gvwInformacionTitulo_SelectedIndexChanged" OnPageIndexChanging="gvwInformacionTitulo_PageIndexChanging"
                         AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IdTituloValor" OnSorting ="gvwInformacionTitulo_OnSorting"
                         GridLines="None" Height="16px"  PageSize="10" Width="100%">
                        <Columns>
                            
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" Height="16px" ImageUrl="~/Image/btn/info.jpg" ToolTip="Detalle" Width="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TipoTitulo.NombreTipoTitulo" HeaderText="Tipo de título" SortExpression="NombreTipoTitulo" />                            
                            <asp:BoundField DataField="EstadoBien" HeaderText="Estado del bien" SortExpression="EstadoBien" />
                            <asp:BoundField DataField="DescripcionTitulo" HeaderText="Descripción del bien" SortExpression="DescripcionTitulo" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
