﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_RegistrarBienesDenunciados_Detail" %>

<%@ Register Src="~/General/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
    
<asp:Panel runat="server" ID="pnlInformacionDenuncia">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Radicado de la Denuncia *</td>
                <td>Fecha de Radicado de la Denuncia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Radicado en Correspondencia *</td>
                <td>Fecha radicado en Correspondencia *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRadicadoCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Identificación *</td>
                <td>Número de Identificación *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Primer Nombre *</td>
                <td>Segundo Nombre *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Primer Apellido *</td>
                <td>Segundo Apellido *</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelRazonSocial">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtRazonSocial" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 45%">Descripción de la Denuncia *</td>
                <td style="width: 55%">Histórico de la Denuncia
                 <asp:ImageButton ID="btnHistorico" runat="server" ImageUrl="~/Image/btn/info.jpg" Enabled="false"
                     Height="16px" Width="16px" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                </td>
            </tr>
               <tr class="rowB">
                <td>Valor de la Denuncia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="TextValordelaDenuncia" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
  
     <asp:Panel ID="pnlBienDenunciado" runat="server">
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Bien Denunciado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButton ID="RbtBienDenunciadoM" runat="server" Text="Mueble o inmueble" TextAlign="Right" Enabled="false"/>
                    &nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="RbtBienDenunciadoT" runat="server" Text="Título valor" TextAlign="Right" Enabled="false"/>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlInformacionMueble" Visible="true">
        <asp:HiddenField ID="hfTipoParametro" runat="server" />
        <asp:HiddenField ID="hfCodigoParametro" runat="server" />
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del Mueble e Inmueble
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Tipo de bien&nbsp;
                    <asp:Label ID="lblRequeridoTipoBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Subtipo de bien&nbsp;
                    <asp:Label ID="lblRequeridoSubTipoBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoBien" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                        <asp:ListItem Value="3">INMUEBLE</asp:ListItem>
                        <asp:ListItem Value="2">MUEBLE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlSubTipoBien" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Clase de bien&nbsp;  
                   <asp:Label ID="lblRequeridoClasebien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Departamento&nbsp;   
                   <asp:Label ID="lblRequeridoDepartamento" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlClasebien" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDepartamento"  Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Municipio&nbsp;
                   <asp:Label ID="lblRequeridoMunicipio" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Clase de Entrada
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlMunicipio" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlClaseEntrada" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Estado Físico del Bien&nbsp;
                    <asp:Label ID="lblRequeridoEstadoFisicoBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Valor Estimado Comercial&nbsp;
                    <asp:Label ID="lblRequeridoValorEstimadoMueble" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlEstadoFisicoBien" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtValorEstimadoMueble"  Enabled="false" MaxLength="19" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fbBoxFormato" runat="server" TargetControlID="txtValorEstimadoMueble"
                        FilterType="Numbers, Custom" ValidChars=".," />
                </td>
            </tr>
            <tr class="rowB">
                <td>Porcentaje de Pertenencia del Bien
                    <asp:Label ID="lblRequeridoPorcentajePertenencia" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Destinación Económica</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlPorcentajePertenencia" Enabled="false" Width="250px">
                        <asp:ListItem Value="0">0</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDestinacionEconomica" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Matricula Inmobiliaria <asp:Label ID="lblAsterMatricula" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoMatriculainmibiliaria" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Cédula Catastral</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtMatriculainmibiliaria" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftMatriculainmibiliaria" runat="server" TargetControlID="txtMatriculainmibiliaria"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCedulaCatastral" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftCedulaCatastral" runat="server" TargetControlID="txtCedulaCatastral"
                        FilterType="Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Dirección del Inmueble <asp:Label ID="lblAsterDireccion" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoDireccionInmueble" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Número Identificación del Bien <asp:Label ID="lblAsterNumeroIdentificacion" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoNumeroIdentificacionBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDireccionInmueble" MaxLength="150" Width="250px" Enabled="false"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDireccionInmueble" runat="server" TargetControlID="txtDireccionInmueble"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionBien" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacionBien" runat="server" TargetControlID="txtNumeroIdentificacionBien"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Marca del bien <asp:Label ID="lblAsterMarcaBien" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoMarcaBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Fecha de Venta
                    <asp:Label ID="lblFormatoInvalidoFechaVenta" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlMarcaBien" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaVenta" Requerid="false" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha de Adjudicado
                    <asp:Label ID="lblFormatoInvalidoFechaAdjudicado" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
                <td>Fecha de Registro
                    <asp:Label ID="lblFormatoInvalidoFechaRegistro" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha runat="server" ID="FechaAdjudicado" Requerid="false" Enabled="false" />
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaRegistro" Requerid="false" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Estado del Bien&nbsp;
                    <asp:Label ID="lblRequeridoEstadoBien" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtEstadoBien" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Descripción del Bien</td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcionBien" CssClass="Validar2" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" Enabled="false"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDescripcionBien" runat="server" TargetControlID="txtDescripcionBien"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlInformacionTitulo" Visible="false">
        <asp:HiddenField ID="hfCodigoProducto" runat="server" />
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td colspan="2" class="tdTitulos">Información del Título Valor
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
            <tr class="rowB">
                <td>Tipo de Título&nbsp; 
                    <asp:Label ID="lblRequeridoTipoTitulo" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>No. Cuenta Bancaria <asp:Label ID="lblAsterNumeroCuenta" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoNuemeroCuentaBancaria" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoTitulo" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNuemeroCuentaBancaria" Width="250px" MaxLength="32" Enabled="false"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNuemeroCuentaBancaria" runat="server" TargetControlID="txtNuemeroCuentaBancaria"
                        FilterType="Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Entidad Bancaria <asp:Label ID="lblAsterEntidadBancaria" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;   
                    <asp:Label ID="lblRequeridoEntidadBancaria" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Tipo de Cuenta Bancaria <asp:Label ID="lblAsterTipoCuenta" runat="server" Text="*" Visible="false"></asp:Label>&nbsp; 
                    <asp:Label ID="lblRequeridoTipoCuentaBancaria" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlEntidadBancaria" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoCuentaBancaria" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Departamento</td>
                <td>Municipio</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlDepartamentoTitulo" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlMunicipioTitulo" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Valor Efectivo Estimado
                </td>
                <td>Número del Título Valor <asp:Label ID="lblAsterNumTitulo" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoNumeroTitulo" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtValorEstimadoTitulo"  MaxLength="19" Enabled="false" Width="250px"></asp:TextBox>
                    <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtValorEstimadoTitulo"
                        FilterType="Custom,Numbers" ValidChars=".," />--%>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroTitulo" MaxLength="16" Enabled="false" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroTitulo" runat="server" TargetControlID="txtNumeroTitulo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Entidad Emisora</td>
                <td>Tipo identificación Entidad Emisora</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlEntidadEmisora" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoEntidadEmisora" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Número identificación del bien <asp:Label ID="lblAsterNumIdentiValor" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                    <asp:Label ID="lblRequeridoNumeroIdentificacionBienTitulo" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
                <td>Cantidad de Títulos</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionBienTitulo" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCantidadTitulos" MaxLength="8" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Valor Unitario del Título</td>
                <td>Fecha de Vencimiento
                    <asp:Label ID="lblFormatoInvalidoFechaVencimiento" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtValorUnitarioTitulo"  MaxLength="19" Enabled="false" Width="250px"></asp:TextBox>
                    <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtValorUnitarioTitulo"
                        FilterType="Custom,Numbers" ValidChars=".," />--%>
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaVencimiento" Requerid="false" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha Recibido 
                    <asp:Label ID="lblFormatoInvalidoFechaRecibido" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
                <td>Fecha de Venta
                    <asp:Label ID="lblFormatoInvalidoFechaVentaTitulo" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha runat="server" ID="FechaRecibido" Requerid="false" Enabled="false" />
                </td>
                <td>
                    <uc1:fecha runat="server" ID="FechaVentaTitulo" Requerid="false" Enabled="false" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Cantidad Vendida</td>
                <td>Clase de Entrada</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtCantidadVendida" MaxLength="15" Enabled="false" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftCantidadVendida" runat="server" TargetControlID="txtCantidadVendida"
                        FilterType="Numbers" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlClaseEntradaTitulo" Enabled="false" Width="250px">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha de Adjudicado
                    <asp:Label ID="lblFormatoInvalidoFechaAdjudicacionTitulo" runat="server" ForeColor="Red" Text="El formato del campo es invalido" Visible="false"></asp:Label>
                </td>
                <td>Estado del Bien&nbsp; 
                    <asp:Label ID="lblRequeridoEstadoBienTitulo" runat="server" ForeColor="Red" Text="Campo Requerido" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha runat="server" ID="FechaAdjudicacionTitulo" Requerid="false" Enabled="false" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtEstadoBienTitulo" MaxLength="20" Enabled="false" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td style="width: 45%" colspan="2">Descripción del título
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtDescripcionTitulo" Enabled="false" CssClass="Validar2" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDescripcionTitulo" runat="server" TargetControlID="txtDescripcionTitulo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
