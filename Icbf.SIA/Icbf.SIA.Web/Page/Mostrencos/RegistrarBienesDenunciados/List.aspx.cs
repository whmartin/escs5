﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_RegistrarBienesDenunciados_List
/// </summary>
public partial class Page_Mostrencos_RegistrarBienesDenunciados_List : GeneralWeb
{
    /// <summary>
    /// Obtiene o establece de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarBienesDenunciados";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region Eventos

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        this.toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            this.ValidarPermisos();
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        try
        {
            this.NavigateTo("List.aspx");
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Add.aspx");
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwInformacionTitulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwInformacionTitulo.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvwInformacionTitulo"] != null)
        {
            List<TituloValor> vList = (List<TituloValor>)this.ViewState["SortedgvwInformacionTitulo"];
            this.gvwInformacionTitulo.DataSource = vList;
            this.gvwInformacionTitulo.DataBind();
        }
        else
        {
            this.CargarGrillaTitulo();
        }
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwInformacionInmueble_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwInformacionInmueble.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvwInformacionInmueble"] != null)
        {
            List<MuebleInmueble> vList = (List<MuebleInmueble>)this.ViewState["SortedgvwInformacionInmueble"];
            this.gvwInformacionInmueble.DataSource = vList;
            this.gvwInformacionInmueble.DataBind();
        }
        else
        {
            this.CargarGrillaInmueble();
        }
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwInformacionInmueble_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = this.gvwInformacionInmueble.SelectedRow.RowIndex;
            string strValue = this.gvwInformacionInmueble.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo", "MUEBLE");
            this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void gvwInformacionTitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = this.gvwInformacionTitulo.SelectedRow.RowIndex;
            string strValue = this.gvwInformacionTitulo.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo", "TITULO");
            this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvwInformacionInmueble_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<MuebleInmueble> vList = new List<MuebleInmueble>();
        List<MuebleInmueble> vResult = new List<MuebleInmueble>();
        if (this.ViewState["SortedgvwInformacionInmueble"] != null)
        {
            vList = (List<MuebleInmueble>)ViewState["SortedgvwInformacionInmueble"];
        }

        switch (e.SortExpression)
        {
            case "NombreTipoBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreTipoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreTipoBien).ToList();
                }

                break;

            case "NombreSubTipoBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreSubTipoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreSubTipoBien).ToList();
                }

                break;

            case "NombreClaseBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreClaseBien).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreClaseBien).ToList();
                }

                break;

            case "EstadoBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.EstadoBien).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.EstadoBien).ToList();
                }

                break;

            case "DescripcionBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.DescripcionBien).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.DescripcionBien).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.ViewState["SortedgvwInformacionInmueble"] = vResult;
        this.gvwInformacionInmueble.DataSource = vResult;
        this.gvwInformacionInmueble.DataBind();
    }

    /// <summary>
    /// evento de ordenamiento de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewSortEventArgs</param>
    protected void gvwInformacionTitulo_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<TituloValor> vList = new List<TituloValor>();
        List<TituloValor> vResult = new List<TituloValor>();

        if (this.ViewState["SortedgvwInformacionTitulo"] != null)
        {
            vList = (List<TituloValor>)ViewState["SortedgvwInformacionTitulo"];
        }

        switch (e.SortExpression)
        {
            case "NombreTipoTitulo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.TipoTitulo.NombreTipoTitulo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.TipoTitulo.NombreTipoTitulo).ToList();
                }

                break;

            case "EstadoBien":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.EstadoBien).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.EstadoBien).ToList();
                }

                break;

            case "DescripcionTitulo":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.DescripcionTitulo).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.DescripcionTitulo).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.ViewState["SortedgvwInformacionTitulo"] = vResult;
        this.gvwInformacionTitulo.DataSource = vResult;
        this.gvwInformacionTitulo.DataBind();
    }
    #endregion

    #region Metodos

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                var Calidad = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 3);
                if (Calidad != null)
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }

                if (vDenuncia.IdEstadoDenuncia == 11 || vDenuncia.IdEstadoDenuncia == 13 || vDenuncia.IdEstadoDenuncia == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }

                vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;
                List<MuebleInmueble> vMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorIdDenunciaBien(vIdDenunciaBien);
                List<TituloValor> vTituloValor = this.vMostrencosService.ConsultarTituloValorPorIdDenunciaBien(vIdDenunciaBien);
                int vMuebles = vMuebleInmueble != null ? vMuebleInmueble.Count() : 0;
                int vTitulos = vTituloValor != null ? vTituloValor.Count() : 0;
                if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ADJUDICADO"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);
                // Se asigna el valor de la denuncia
                if (vlstDenunciaBienTitulo.Count() > 0)
                {
                    //string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString()) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString() : "0,00";
                    string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                    this.TextValordelaDenuncia.Text = valorDenuncia;
                }
                else
                {
                    this.TextValordelaDenuncia.Text = "0,00";
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            }
            this.CargarGrillaInmueble();
            this.CargarGrillaTitulo();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la informacion de las anulaciones
    /// </summary>
    private void CargarGrillaInmueble()
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
        List<MuebleInmueble> vListaMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorIdDenunciaBien(vIdDenunciaBien);
        vListaMuebleInmueble = vListaMuebleInmueble.OrderBy(p => p.NombreTipoBien).ToList();
        this.ViewState["SortedgvwInformacionInmueble"] = vListaMuebleInmueble;
        this.gvwInformacionInmueble.DataSource = vListaMuebleInmueble;
        this.gvwInformacionInmueble.DataBind();
    }

    /// <summary>
    /// Carga la informacion de las anulaciones
    /// </summary>
    private void CargarGrillaTitulo()
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
        List<TituloValor> vListaAnulaciones = this.vMostrencosService.ConsultarTituloValorPorIdDenunciaBien(vIdDenunciaBien);
        vListaAnulaciones = vListaAnulaciones.OrderBy(p => p.TipoTitulo.NombreTipoTitulo).ToList();
        this.ViewState["SortedgvwInformacionTitulo"] = vListaAnulaciones;
        this.gvwInformacionTitulo.DataSource = vListaAnulaciones;
        this.gvwInformacionTitulo.DataBind();
    }

    /// <summary>
    /// valida si se pude o no mostrar los botone sd enueno y editar
    /// </summary>
    private void ValidarPermisos()
    {
        bool EsMostrarBoton = false;
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
        EsMostrarBoton = (vDenunciaBien.IdEstadoDenuncia != 11 && vDenunciaBien.IdEstadoDenuncia != 13) || vDenunciaBien.IdEstadoDenuncia == 14;
        this.toolBar.MostrarBotonNuevo(EsMostrarBoton);
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.EstablecerTitulos("Registro de Bienes", "List");
            this.gvwInformacionTitulo.PageSize = this.PageSize();
            this.gvwInformacionTitulo.EmptyDataText = this.EmptyDataText();
            this.gvwInformacionInmueble.PageSize = this.PageSize();
            this.gvwInformacionInmueble.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}