﻿//-----------------------------------------------------------------------
// <copyright file="Edit.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Edit.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarBienesDenunciados_Edit : GeneralWeb
{
    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarBienesDenunciados";

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Servicio
    /// </summary>
    SIAService vSIAService = new SIAService();

    #region "Eventos"

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.pnlPopUpHistorico.CssClass = "popuphIstorico hidden";
        if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.Add))
        {
            this.toolBar.LipiarMensajeError();
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Metodo para mostrar el popup del historico
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnHistorico_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnAdjuntar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Add.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo("List.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            this.Guardar();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// ddlDepartamentoTitulo_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void ddlDepartamentoTitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!this.ddlDepartamentoTitulo.SelectedValue.Equals("-1"))
        {
            this.CargarListaMunicipioTitulo(this.ddlDepartamentoTitulo.SelectedValue);
        }
        this.ddlDepartamentoTitulo.Focus();
    }

    /// <summary>
    /// ddlDepartamento_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!this.ddlDepartamento.SelectedValue.Equals("-1"))
        {
            this.CargarListaMunicipio(this.ddlDepartamento.SelectedValue);
        }
        this.ddlDepartamento.Focus();
    }

    /// <summary>
    /// habilita los campos sebun el tipo de bien
    /// </summary> 
    private void habilitarCamposMueble(string pTipoBien)
    {
        this.HabilitarCamposGeneralesMueble();
        switch (pTipoBien)
        {
            case "1":
                this.HabilitarCamposInmueble(true);
                this.lblAsterMatricula.Visible = true;
                this.lblAsterDireccion.Visible = true;
                this.lblAsterNumeroIdentificacion.Visible = false;
                this.lblAsterMarcaBien.Visible = false;
                break;

            case "2":
                this.HabilitarCamposInmueble(false);
                this.lblAsterMatricula.Visible = false;
                this.lblAsterDireccion.Visible = false;
                this.lblAsterNumeroIdentificacion.Visible = true;
                this.lblAsterMarcaBien.Visible = true;
                this.txtNumeroIdentificacionBien.Enabled = true;
                break;
        }
    }

    /// <summary>
    /// ddlTipoTitulo_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    private void HabilitarCamposTitulo(string pTipoTitulo)
    {
        this.HabilitarCamposGeneralesTitulo();
        switch (pTipoTitulo)
        {
            case "1":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloAcciones();
                this.lblAsterNumTitulo.Visible = true;
                break;

            case "2":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloBonos();
                this.lblAsterNumTitulo.Visible = true;
                this.lblAsterNumIdentiValor.Visible = true;
                break;

            case "3":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloCDT();
                this.lblAsterNumTitulo.Visible = true;
                this.lblAsterEntidadBancaria.Visible = true;
                break;

            case "4":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloChequesPagares();
                this.lblAsterNumTitulo.Visible = true;
                this.lblAsterNumIdentiValor.Visible = true;
                break;

            case "5":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloFacturas();
                this.lblAsterNumTitulo.Visible = true;
                this.lblAsterNumIdentiValor.Visible = true;
                break;

            case "6":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloEfectivo();
                this.lblAsterNumeroCuenta.Visible = true;
                this.lblAsterTipoCuenta.Visible = true;
                this.lblAsterEntidadBancaria.Visible = true;

                break;

            case "7":
                this.LimpiarCamposMuebleTituloValor(false);
                this.HabilitarCamposTituloChequesPagares();
                this.lblAsterNumTitulo.Visible = true;
                this.lblAsterNumIdentiValor.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.gvwHistoricoDenuncia.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.gvwHistoricoDenuncia.DataSource = vList;
                this.gvwHistoricoDenuncia.DataBind();
            }
            else
            {
                int vIdCausante = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionCausante.IdCausante"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarCausantesPorIdCaudante(vIdCausante).IdDenunciaBien);
            }
            this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void gvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }

                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }

                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }

                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdFase).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdFase).ToList();
                }

                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdActuacion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdActuacion).ToList();
                }

                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    ////Ascendente
                    vResult = vList.OrderBy(a => a.IdAccion).ToList();
                }
                else
                {
                    ////Descendente
                    vResult = vList.OrderByDescending(a => a.IdAccion).ToList();
                }

                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        this.pnlPopUpHistorico.CssClass = "popuphIstorico";
        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.gvwHistoricoDenuncia.DataSource = vResult;
        this.gvwHistoricoDenuncia.DataBind();
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }

            vHistorico = vHistorico.OrderByDescending(p => p.FechaCrea).ToList();
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.gvwHistoricoDenuncia.DataSource = vHistorico;
            this.gvwHistoricoDenuncia.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoGuardar += new ToolBarDelegate(this.btnGuardar_Click);
            this.toolBar.EstablecerTitulos("Registro de Bienes", "Edit");
            this.gvwHistoricoDenuncia.PageSize = this.PageSize();
            this.gvwHistoricoDenuncia.EmptyDataText = this.EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.toolBar.LipiarMensajeError();
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            var Calidad = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 3);
            if (Calidad != null)
            {
                this.toolBar.MostrarBotonNuevo(false);
            }
            int vId = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id"));
            string vTipo = this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo").ToString();
            string vMensaje = this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Mensaje").ToString();
            this.CargarDatosDenuncia();
            if (vTipo.Equals("MUEBLE"))
            {
                this.RbtBienDenunciadoM.Checked = true;
                this.RbtBienDenunciadoT.Checked = false;
                this.pnlInformacionMueble.Visible = true;
                this.pnlInformacionTitulo.Visible = false;
                this.CargarDatosMueble(vId);
            }
            else
            {
                this.RbtBienDenunciadoM.Checked = false;
                this.RbtBienDenunciadoT.Checked = true;
                this.pnlInformacionMueble.Visible = false;
                this.pnlInformacionTitulo.Visible = true;
                this.CargarDatosTitulo(vId);
            }
            this.CargarGrillaHistorico(vIdDenunciaBien);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion del titulo o valor
    /// </summary>
    /// <param name="pIdTituloValor">id del titulo o valor</param>
    private void CargarDatosTitulo(int pIdTituloValor)
    {
        try
        {
            TituloValor vTituloValor = this.vMostrencosService.ConsultarTituloValorPorIdTituloValor(pIdTituloValor);
            this.CargarListasTitulo(vTituloValor);
            this.HabilitarCamposTitulo(vTituloValor.IdTipoTitulo.ToString());
            this.hfCodigoProducto.Value = vTituloValor.CodigoProducto;
            this.ddlTipoTitulo.SelectedValue = vTituloValor.IdTipoTitulo.ToString();
            this.ddlClaseEntradaTitulo.SelectedValue = !string.IsNullOrEmpty(vTituloValor.ClaseEntrada) ? vTituloValor.ClaseEntrada : "-1";
            this.txtDescripcionTitulo.Text = vTituloValor.DescripcionTitulo;
            this.txtEstadoBienTitulo.Text = vTituloValor.EstadoBien;
            this.hfEstadoBienTitulo.Value = vTituloValor.EstadoBien;
            switch (vTituloValor.IdTipoTitulo)
            {
                case 1:
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    this.txtCantidadVendida.Text = vTituloValor.CantidadVendida;
                    break;

                case 2:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    this.txtCantidadVendida.Text = vTituloValor.CantidadVendida;
                    break;

                case 3:
                    this.ddlEntidadBancaria.SelectedValue = vTituloValor.EntidadBancaria;
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    break;

                case 4:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    break;

                case 5:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    break;

                case 6:
                    this.txtNuemeroCuentaBancaria.Text = vTituloValor.NumeroCuentaBancaria;
                    this.ddlEntidadBancaria.SelectedValue = vTituloValor.EntidadBancaria;
                    this.ddlTipoCuentaBancaria.SelectedValue = !string.IsNullOrEmpty(vTituloValor.TipoCuentaBancaria) ? vTituloValor.TipoCuentaBancaria : "-1";
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    break;

                case 7:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga las listas desplegables del titulo o valor
    /// </summary>
    /// <param name="pTituloValor">variable con la informacion del titulo o valor</param>
    private void CargarListasTitulo(TituloValor pTituloValor)
    {
        try
        {
            this.CargarListaDepartamentoTitulo();
            if (pTituloValor.IdDepartamento != null)
            {
                this.ddlDepartamentoTitulo.SelectedValue = pTituloValor.IdDepartamento;
                this.CargarListaMunicipioTitulo(pTituloValor.IdDepartamento);
            }
            this.CargarListaTipoCuenta();
            this.CargarListaEntidadBancaria();
            this.CargarListaEntidadEmisora();
            this.CargarListaTipoEntidadEmisora();
            this.CargarListaTipoTitulo();
            this.CargarListaClaseEntradaTitulo();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga la informacion del mueble o inmueble
    /// </summary>
    /// <param name="pIdMuebleInmueble">id del mueble o inmueble</param>
    private void CargarDatosMueble(int pIdMuebleInmueble)
    {
        try
        {
            MuebleInmueble vMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorId(pIdMuebleInmueble);
            this.CargarListasMuebleInmueble(vMuebleInmueble);
            this.habilitarCamposMueble(vMuebleInmueble.IdTipoBien);
            this.ddlTipoBien.SelectedValue = vMuebleInmueble.IdTipoBien.ToString();
            this.ddlSubTipoBien.SelectedValue = vMuebleInmueble.IdSubTipoBien.ToString();
            this.ddlClasebien.SelectedValue = vMuebleInmueble.IdClaseBien.ToString();
            this.ddlDepartamento.SelectedValue = vMuebleInmueble.IdDepartamento;
            this.ddlMunicipio.SelectedValue = vMuebleInmueble.IdMunicipio.ToString();
            this.ddlClaseEntrada.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.ClaseEntrada) ? vMuebleInmueble.ClaseEntrada : "-1";
            this.ddlEstadoFisicoBien.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.EstadoFisicoBien) ? vMuebleInmueble.EstadoFisicoBien : "-1";
            this.txtValorEstimadoMueble.Text = vMuebleInmueble.ValorEstimadoComercial.ToString("N2");
            this.ddlPorcentajePertenencia.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.PorcentajePertenenciaBien) ? vMuebleInmueble.PorcentajePertenenciaBien : "-1";
            this.ddlDestinacionEconomica.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.DestinacionEconomica) ? vMuebleInmueble.DestinacionEconomica : "-1";
            this.txtNumeroIdentificacionBien.Text = vMuebleInmueble.NumeroIdentificacionInmueble;
            this.txtEstadoBien.Text = vMuebleInmueble.EstadoBien;
            this.hfEstadoBien.Value = vMuebleInmueble.EstadoBien;
            this.txtDescripcionBien.Text = vMuebleInmueble.DescripcionBien;
            this.hfTipoParametro.Value = vMuebleInmueble.TipoParametro;
            this.hfCodigoParametro.Value = vMuebleInmueble.TipoParametro;
            if (vMuebleInmueble.IdTipoBien.Equals("3"))
            {
                this.txtMatriculainmibiliaria.Text = vMuebleInmueble.MatriculaInmobiliaria;
                this.txtCedulaCatastral.Text = vMuebleInmueble.CedulaCatastral;
                this.txtDireccionInmueble.Text = vMuebleInmueble.DireccionInmueble;
            }
            else
            {
                this.ddlMarcaBien.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.MarcaBien) ? vMuebleInmueble.MarcaBien : "-1";
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga las listas desplegables del mueble o inmueble
    /// </summary>
    /// <param name="pMuebleInmueble">variable de tipo mueble o inmueble</param>
    private void CargarListasMuebleInmueble(MuebleInmueble pMuebleInmueble)
    {
        try
        {
            this.CargarListaSubTipoBien(pMuebleInmueble.IdTipoBien);
            this.CargarListaClasebien(pMuebleInmueble.IdSubTipoBien);
            this.CargarListaDepartamento();
            this.CargarListaMunicipio(pMuebleInmueble.IdDepartamento);
            this.CargarListaClaseEntrada();
            this.CargarListaEstadoBien();
            this.CargarListaDestinacionEconomica();
            this.CargarListaMarcaBien();
            this.CargarListaPorcentajePertenencia();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga la informacion de la denuncia
    /// </summary>
    private void CargarDatosDenuncia()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
            if (vIdDenunciaBien != 0)
            {
                DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;
                if ((vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA")) && !vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ADJUDICADO"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);
                // Se asigna el valor de la denuncia
                if (vlstDenunciaBienTitulo.Count() > 0)
                {
                    //string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString()) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString() : "0,00";
                    string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                    this.TextValordelaDenuncia.Text = valorDenuncia;
                }
                else
                {
                    this.TextValordelaDenuncia.Text = "0,00";
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Carga la información de las listas clase de bien
    /// </summary>
    private void CargarListaClasebien(string pIdSubTipoBien)
    {
        try
        {
            List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(pIdSubTipoBien);
            vListaClaseBien = vListaClaseBien.OrderBy(p => p.NombreClaseBien).ToList();
            this.ddlClasebien.DataSource = vListaClaseBien;
            this.ddlClasebien.DataTextField = "NombreClaseBien";
            this.ddlClasebien.DataValueField = "IdClaseBien";
            this.ddlClasebien.DataBind();
            this.ddlClasebien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClasebien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas calase de entrada
    /// </summary>
    private void CargarListaClaseEntrada()
    {
        try
        {
            List<ClaseEntrada> vListaClaseEntrada = this.vMostrencosService.ConsultarClaseEntrada();
            vListaClaseEntrada = vListaClaseEntrada.OrderBy(p => p.NombreClaseEntrada).ToList();
            this.ddlClaseEntrada.DataSource = vListaClaseEntrada;
            this.ddlClaseEntrada.DataTextField = "NombreClaseEntrada";
            this.ddlClaseEntrada.DataValueField = "IdClaseEntrada";
            this.ddlClaseEntrada.DataBind();
            this.ddlClaseEntrada.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClaseEntrada.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas calase de entrada del titulo o valor
    /// </summary>
    private void CargarListaClaseEntradaTitulo()
    {
        try
        {
            List<ClaseEntrada> vListaClaseEntrada = this.vMostrencosService.ConsultarClaseEntrada();
            vListaClaseEntrada = vListaClaseEntrada.OrderBy(p => p.NombreClaseEntrada).ToList();
            this.ddlClaseEntradaTitulo.DataSource = vListaClaseEntrada;
            this.ddlClaseEntradaTitulo.DataTextField = "NombreClaseEntrada";
            this.ddlClaseEntradaTitulo.DataValueField = "IdClaseEntrada";
            this.ddlClaseEntradaTitulo.DataBind();
            this.ddlClaseEntradaTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClaseEntradaTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de departamentos
    /// </summary>
    private void CargarListaDepartamento()
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarDepartamentosSeven();
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_DEPARTAMENTO).ToList();
            this.ddlDepartamento.DataSource = vListaDepto;
            this.ddlDepartamento.DataTextField = "NOMBRE_DEPARTAMENTO";
            this.ddlDepartamento.DataValueField = "CODIGO_DEPARTAMENTO";
            this.ddlDepartamento.DataBind();
            this.ddlDepartamento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDepartamento.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de municipios
    /// </summary>
    private void CargarListaMunicipio(string pIdDepto)
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarMunicipiosSeven(pIdDepto);
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_MUNICIPIO).ToList();
            if (pIdDepto.Equals("11"))
            {
                GN_VDIP vGN_VDIP = vListaDepto.Where(p => p.CODIGO_MUNICIPIO.Equals("1")).FirstOrDefault();
                vListaDepto.Clear();
                vListaDepto.Add(vGN_VDIP);
            }
            this.ddlMunicipio.DataSource = vListaDepto;
            this.ddlMunicipio.DataTextField = "NOMBRE_MUNICIPIO";
            this.ddlMunicipio.DataValueField = "CODIGO_MUNICIPIO";
            this.ddlMunicipio.DataBind();
            this.ddlMunicipio.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlMunicipio.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de departamentos titulo
    /// </summary>
    private void CargarListaDepartamentoTitulo()
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarDepartamentos();
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_DEPARTAMENTO).ToList();
            this.ddlDepartamentoTitulo.DataSource = vListaDepto;
            this.ddlDepartamentoTitulo.DataTextField = "NOMBRE_DEPARTAMENTO";
            this.ddlDepartamentoTitulo.DataValueField = "CODIGO_DEPARTAMENTO";
            this.ddlDepartamentoTitulo.DataBind();
            this.ddlDepartamentoTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDepartamentoTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Municipios Titulo
    /// </summary>
    private void CargarListaMunicipioTitulo(string pIdDepto)
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarMunicipios(pIdDepto);
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_MUNICIPIO).ToList();
            this.ddlMunicipioTitulo.DataSource = vListaDepto;
            this.ddlMunicipioTitulo.DataTextField = "NOMBRE_MUNICIPIO";
            this.ddlMunicipioTitulo.DataValueField = "CODIGO_MUNICIPIO";
            this.ddlMunicipioTitulo.DataBind();
            this.ddlMunicipioTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlMunicipioTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas detipo cuenta
    /// </summary>
    private void CargarListaTipoCuenta()
    {
        try
        {
            List<TipoCuenta> vListaTipoCuenta = this.vMostrencosService.ConsultarTipoCuenta();
            vListaTipoCuenta = vListaTipoCuenta.OrderBy(p => p.NombreTipoCta).ToList();
            this.ddlTipoCuentaBancaria.DataSource = vListaTipoCuenta;
            this.ddlTipoCuentaBancaria.DataTextField = "NombreTipoCta";
            this.ddlTipoCuentaBancaria.DataValueField = "IdTipoCta";
            this.ddlTipoCuentaBancaria.DataBind();
            this.ddlTipoCuentaBancaria.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoCuentaBancaria.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Edtidad Bancaria
    /// </summary>
    private void CargarListaEntidadBancaria()
    {
        try
        {
            List<EntidadBancaria> vListaEntidadBancaria = this.vMostrencosService.ConsultarEntidadBancaria();
            vListaEntidadBancaria = vListaEntidadBancaria.OrderBy(p => p.NombreEntidadBancaria).ToList();
            this.ddlEntidadBancaria.DataSource = vListaEntidadBancaria;
            this.ddlEntidadBancaria.DataTextField = "NombreEntidadBancaria";
            this.ddlEntidadBancaria.DataValueField = "IdEntidadBancaria";
            this.ddlEntidadBancaria.DataBind();
            this.ddlEntidadBancaria.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEntidadBancaria.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Entidad Emisora
    /// </summary>
    private void CargarListaEntidadEmisora()
    {
        try
        {
            List<EntidadEmisora> vListaDepto = this.vMostrencosService.ConsultarEntidadEmisora();
            vListaDepto = vListaDepto.OrderBy(p => p.NombreEntidadEmisora).ToList();
            this.ddlEntidadEmisora.DataSource = vListaDepto;
            this.ddlEntidadEmisora.DataTextField = "NombreEntidadEmisora";
            this.ddlEntidadEmisora.DataValueField = "IdEntidadEmisora";
            this.ddlEntidadEmisora.DataBind();
            this.ddlEntidadEmisora.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEntidadEmisora.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Estado Bien
    /// </summary>
    private void CargarListaEstadoBien()
    {
        try
        {
            this.ddlClasebien.SelectedValue = "-1";
            List<EstadoFisicoBien> vListaEstadoFisicoBien = this.vMostrencosService.ConsultarEstadoFisicoBien();
            vListaEstadoFisicoBien = vListaEstadoFisicoBien.OrderBy(p => p.NombreEstadoFisicoBien).ToList();
            this.ddlEstadoFisicoBien.DataSource = vListaEstadoFisicoBien;
            this.ddlEstadoFisicoBien.DataTextField = "NombreEstadoFisicoBien";
            this.ddlEstadoFisicoBien.DataValueField = "IdEstadoFisicoBien";
            this.ddlEstadoFisicoBien.DataBind();
            this.ddlEstadoFisicoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEstadoFisicoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Marca Bien
    /// </summary>
    private void CargarListaMarcaBien()
    {
        try
        {
            List<MarcaBien> vListaMarcaBien = this.vMostrencosService.ConsultMarcaBien();
            vListaMarcaBien = vListaMarcaBien.OrderBy(p => p.NombreMarcaBien).ToList();
            this.ddlMarcaBien.DataSource = vListaMarcaBien;
            this.ddlMarcaBien.DataTextField = "NombreMarcaBien";
            this.ddlMarcaBien.DataValueField = "IdMarcaBien";
            this.ddlMarcaBien.DataBind();
            this.ddlMarcaBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlMarcaBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Porcentaje Pertenencia
    /// </summary>
    private void CargarListaPorcentajePertenencia()
    {
        try
        {
            for (int i = 1; i <= 100; i++)
            {
                this.ddlPorcentajePertenencia.Items.Insert(i, new ListItem(i.ToString(), i.ToString()));
            }
            this.ddlPorcentajePertenencia.SelectedValue = "0";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de SubTipo Bien
    /// </summary>
    private void CargarListaSubTipoBien(string pIdTipoBien)
    {
        try
        {
            List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(pIdTipoBien);
            vListaSubTipoBien = vListaSubTipoBien.OrderBy(p => p.NombreSubTipoBien).ToList();
            this.ddlSubTipoBien.DataSource = vListaSubTipoBien;
            this.ddlSubTipoBien.DataTextField = "NombreSubTipoBien";
            this.ddlSubTipoBien.DataValueField = "IdSubTipoBien";
            this.ddlSubTipoBien.DataBind();
            this.ddlSubTipoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlSubTipoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Tipo Entidad Emisora
    /// </summary>
    private void CargarListaTipoEntidadEmisora()
    {
        try
        {
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoEntidadEmisora.DataSource = vListaTipoIdent;
            this.ddlTipoEntidadEmisora.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoEntidadEmisora.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoEntidadEmisora.DataBind();
            this.ddlTipoEntidadEmisora.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoEntidadEmisora.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Tipo Titulo
    /// </summary>
    private void CargarListaTipoTitulo()
    {
        try
        {
            List<TipoTitulo> vListaTipoTitulo = this.vMostrencosService.ConsultarTipoTitulo();
            vListaTipoTitulo = vListaTipoTitulo.OrderBy(p => p.NombreTipoTitulo).ToList();
            this.ddlTipoTitulo.DataSource = vListaTipoTitulo;
            this.ddlTipoTitulo.DataTextField = "NombreTipoTitulo";
            this.ddlTipoTitulo.DataValueField = "IdTipoTitulo";
            this.ddlTipoTitulo.DataBind();
            this.ddlTipoTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Destinacion Economica
    /// </summary>
    private void CargarListaDestinacionEconomica()
    {
        try
        {
            List<DestinacionEconomica> vListaDestinacionEconomica = this.vMostrencosService.ConsultarDestinacionEconomica();
            vListaDestinacionEconomica = vListaDestinacionEconomica.OrderBy(p => p.NombreDestinacionEconomica).ToList();
            this.ddlDestinacionEconomica.DataSource = vListaDestinacionEconomica;
            this.ddlDestinacionEconomica.DataTextField = "NombreDestinacionEconomica";
            this.ddlDestinacionEconomica.DataValueField = "IdDestinacionEconomica";
            this.ddlDestinacionEconomica.DataBind();
            this.ddlDestinacionEconomica.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDestinacionEconomica.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la informacion del causante
    /// </summary>
    private void Guardar()
    {
        int vResultado = 0;
        try
        {
            string vTipo = this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo").ToString();
            if (vTipo.Equals("MUEBLE"))
            {
                MuebleInmueble vMuebleInmueble = this.CapturarValoresMuebleInmueble();
                if (vMuebleInmueble != null)
                {
                    this.InformacionAudioria(vMuebleInmueble, this.PageName, SolutionPage.Add);
                    vResultado = this.vMostrencosService.EditarMuebleInmueble(vMuebleInmueble);
                    if (vResultado > 0)
                    {
                        this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id", vMuebleInmueble.IdMuebleInmueble);
                        this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo", "MUEBLE");
                        this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Mensaje", "La información ha sido guardada exitosamente");
                        this.NavigateTo(SolutionPage.Detail);
                    }
                    else
                    {
                        this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                    }
                }
            }
            else if (vTipo.Equals("TITULO"))
            {
                TituloValor vTituloValor = this.CapturarValoresTituloValor();
                if (vTituloValor != null)
                {
                    this.InformacionAudioria(vTituloValor, this.PageName, SolutionPage.Add);
                    vResultado = this.vMostrencosService.EditarTituloValor(vTituloValor);
                    if (vResultado > 0)
                    {
                        this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id", vTituloValor.IdTituloValor);
                        this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo", "TITULO");
                        this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Mensaje", "La información ha sido guardada exitosamente");
                        this.NavigateTo(SolutionPage.Detail);
                    }
                    else
                    {
                        this.toolBar.MostrarMensajeError("No fue posible guardar la información. Por favor inténtelo nuevamente.");
                    }
                }
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Captura la infortación del MuebleInmueble
    /// </summary>
    private MuebleInmueble CapturarValoresMuebleInmueble()
    {
        try
        {
            if (!this.ExistenDatosObligatorios())
            {
                int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
                int vId = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id"));
                string vBienDenunciado = this.RbtBienDenunciadoM.Checked ? this.ddlTipoBien.SelectedItem.Text.ToUpper() : this.ddlTipoTitulo.SelectedItem.Text.ToUpper();
                string vDato = this.ObtenerDato(true);
                string vMensaje = this.vMostrencosService.ValidarExistenciaBienDenunciadoOtrasRegionales(vIdDenunciaBien, vBienDenunciado, vDato);
                if (string.IsNullOrEmpty(vMensaje))
                {
                    this.hfTipoParametro.Value = this.ddlSubTipoBien.SelectedValue.Substring(0, 3);
                    this.hfCodigoParametro.Value = hfTipoParametro.Value.Equals("308") ? this.ddlClasebien.SelectedValue : this.ddlClasebien.SelectedValue;
                    MuebleInmueble vMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorId(vId);
                    vMuebleInmueble.IdDepartamento = this.ddlDepartamento.SelectedValue;
                    vMuebleInmueble.IdMunicipio = this.ddlMunicipio.SelectedValue;
                    vMuebleInmueble.ClaseEntrada = this.ddlClaseEntrada.SelectedValue;
                    vMuebleInmueble.EstadoFisicoBien = this.ddlEstadoFisicoBien.SelectedValue;
                    vMuebleInmueble.ValorEstimadoComercial = Convert.ToDecimal(this.txtValorEstimadoMueble.Text.Trim());
                    vMuebleInmueble.PorcentajePertenenciaBien = this.ddlPorcentajePertenencia.SelectedValue;
                    vMuebleInmueble.DestinacionEconomica = this.ddlDestinacionEconomica.SelectedValue;
                    vMuebleInmueble.NumeroIdentificacionInmueble = this.txtNumeroIdentificacionBien.Text.Trim().ToUpper();
                    vMuebleInmueble.EstadoBien = "REGISTRO DENUNCIA";
                    vMuebleInmueble.DescripcionBien = this.txtDescripcionBien.Text.Trim().ToUpper();
                    vMuebleInmueble.CodigoParametros = this.hfCodigoParametro.Value.Trim().ToUpper();
                    vMuebleInmueble.TipoParametro = this.hfTipoParametro.Value.Trim().ToUpper();
                    vMuebleInmueble.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                    if (vMuebleInmueble.IdTipoBien.Equals("2"))
                    {
                        vMuebleInmueble.MarcaBien = this.ddlMarcaBien.SelectedValue;
                    }
                    else if (vMuebleInmueble.IdTipoBien.Equals("3"))
                    {
                        vMuebleInmueble.MatriculaInmobiliaria = this.txtMatriculainmibiliaria.Text.Trim().ToUpper();
                        vMuebleInmueble.CedulaCatastral = this.txtCedulaCatastral.Text.Trim().ToUpper();
                        vMuebleInmueble.DireccionInmueble = this.txtDireccionInmueble.Text.Trim().ToUpper();
                    }
                    return vMuebleInmueble;
                }
                else
                {
                    throw new Exception(vMensaje);
                }
            }
            return null;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    /// <summary>
    /// obtiene el dato que se va a consultar
    /// </summary>
    /// <param name="pBienDenunciado">tipo de bien denunciado</param>
    /// <returns>dato a consultar</returns>
    private string ObtenerDato(bool pEsMueble)
    {
        try
        {
            string vDato = string.Empty;
            if (pEsMueble)
            {
                if (this.ddlTipoBien.SelectedValue.Equals("3"))
                {
                    vDato = this.txtMatriculainmibiliaria.Text;
                }
                else
                {
                    vDato = this.txtNumeroIdentificacionBien.Text;
                }
            }
            else if (this.ddlTipoTitulo.SelectedValue.Equals("6"))
            {
                vDato = this.txtNuemeroCuentaBancaria.Text;
            }
            else
            {
                vDato = this.txtNumeroTitulo.Text;
            }
            return vDato;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Lismpia los campos de la seccion de mueble Inmieble
    /// </summary>
    private void LimpiarCamposMuebleInmueble()
    {
        this.ddlTipoBien.SelectedValue = "-1";
        this.ddlSubTipoBien.SelectedValue = "-1";
        this.ddlClasebien.SelectedValue = "-1";
        this.ddlDepartamento.SelectedValue = "-1";
        this.ddlDepartamento.Enabled = false;
        this.ddlMunicipio.SelectedValue = "-1";
        this.ddlMunicipio.Enabled = false;
        this.ddlClaseEntrada.SelectedValue = "-1";
        this.ddlClaseEntrada.Enabled = false;
        this.ddlEstadoFisicoBien.SelectedValue = "-1";
        this.ddlEstadoFisicoBien.Enabled = false;
        this.txtValorEstimadoMueble.Text = string.Empty;
        this.txtValorEstimadoMueble.Enabled = false;
        this.ddlPorcentajePertenencia.SelectedValue = "-1";
        this.ddlPorcentajePertenencia.Enabled = false;
        this.ddlDestinacionEconomica.SelectedValue = "-1";
        this.ddlDestinacionEconomica.Enabled = false;
        this.txtMatriculainmibiliaria.Text = string.Empty;
        this.txtMatriculainmibiliaria.Enabled = false;
        this.txtCedulaCatastral.Text = string.Empty;
        this.txtCedulaCatastral.Enabled = false;
        this.txtDireccionInmueble.Text = string.Empty;
        this.txtDireccionInmueble.Enabled = false;
        this.txtNumeroIdentificacionBien.Text = string.Empty;
        this.txtNumeroIdentificacionBien.Enabled = false;
        this.ddlMarcaBien.SelectedValue = "-1";
        this.ddlMarcaBien.Enabled = false;
        this.FechaVenta.InitNull = true;
        this.FechaVenta.Enabled = false;
        this.FechaAdjudicado.InitNull = true;
        this.FechaAdjudicado.Enabled = false;
        this.FechaRegistro.InitNull = true;
        this.FechaRegistro.Enabled = false;
        this.txtEstadoBien.Text = "REGISTRO DENUNCIA";
        this.txtEstadoBien.Enabled = false;
        this.txtDescripcionBien.Text = string.Empty;
        this.txtDescripcionBien.Enabled = false;
        this.lblFormatoInvalidoFechaAdjudicado.Visible = false;
        this.lblFormatoInvalidoFechaVenta.Visible = false;
        this.lblFormatoInvalidoFechaRegistro.Visible = false;
        this.lblRequeridoClasebien.Visible = false;
        this.lblRequeridoDepartamento.Visible = false;
        this.lblRequeridoDireccionInmueble.Visible = false;
        this.lblRequeridoEstadoBien.Visible = false;
        this.lblRequeridoEstadoFisicoBien.Visible = false;
        this.lblRequeridoMarcaBien.Visible = false;
        this.lblRequeridoMatriculainmibiliaria.Visible = false;
        this.lblRequeridoMunicipio.Visible = false;
        this.lblRequeridoNumeroIdentificacionBien.Visible = false;
        this.lblRequeridoPorcentajePertenencia.Visible = false;
        this.lblRequeridoSubTipoBien.Visible = false;
        this.lblRequeridoTipoBien.Visible = false;
        this.lblRequeridoValorEstimadoMueble.Visible = false;
        this.lblAsterMatricula.Visible = false;
        this.lblAsterDireccion.Visible = false;
        this.lblAsterNumeroIdentificacion.Visible = false;
        this.lblAsterMarcaBien.Visible = false;
    }

    /// <summary>
    /// Lismpia los campos de la seccion de titulo o Valor
    /// </summary>
    private void LimpiarCamposMuebleTituloValor(bool pLimpiarTodos)
    {
        this.txtNuemeroCuentaBancaria.Text = string.Empty;
        this.txtNuemeroCuentaBancaria.Enabled = false;
        this.ddlEntidadBancaria.SelectedValue = "-1";
        this.ddlEntidadBancaria.Enabled = false;
        this.ddlTipoCuentaBancaria.SelectedValue = "-1";
        this.ddlTipoCuentaBancaria.Enabled = false;
        this.ddlDepartamentoTitulo.SelectedValue = "-1";
        this.ddlDepartamentoTitulo.Enabled = false;
        this.ddlMunicipioTitulo.SelectedValue = "-1";
        this.ddlMunicipioTitulo.Enabled = false;
        this.txtValorEstimadoTitulo.Text = string.Empty;
        this.txtValorEstimadoTitulo.Enabled = false;
        this.txtNumeroTitulo.Text = string.Empty;
        this.txtNumeroTitulo.Enabled = false;
        this.ddlEntidadEmisora.SelectedValue = "-1";
        this.ddlEntidadEmisora.Enabled = false;
        this.ddlTipoEntidadEmisora.SelectedValue = "-1";
        this.ddlTipoEntidadEmisora.Enabled = false;
        this.txtNumeroIdentificacionBienTitulo.Text = string.Empty;
        this.txtNumeroIdentificacionBienTitulo.Enabled = false;
        this.txtCantidadTitulos.Text = string.Empty;
        this.txtCantidadTitulos.Enabled = false;
        this.txtValorUnitarioTitulo.Text = string.Empty;
        this.txtValorUnitarioTitulo.Enabled = false;
        this.FechaVencimiento.InitNull = true;
        this.FechaVencimiento.Enabled = false;
        this.FechaRecibido.InitNull = true;
        this.FechaRecibido.Enabled = false;
        this.txtCantidadVendida.Text = string.Empty;
        this.txtCantidadVendida.Enabled = false;
        this.lblAsterNumIdentiValor.Visible = false;
        this.lblAsterTipoCuenta.Visible = false;
        this.lblAsterEntidadBancaria.Visible = false;
        this.lblAsterNumTitulo.Visible = false;
        this.lblAsterNumeroCuenta.Visible = false;

        if (pLimpiarTodos)
        {
            this.ddlTipoTitulo.SelectedValue = "-1";
            this.FechaVentaTitulo.InitNull = true;
            this.FechaVentaTitulo.Enabled = false;
            this.ddlClaseEntrada.SelectedValue = "-1";
            this.ddlClaseEntrada.Enabled = false;
            this.FechaAdjudicacionTitulo.InitNull = true;
            this.FechaAdjudicacionTitulo.Enabled = false;
            this.txtEstadoBienTitulo.Text = "REGISTRO DENUNCIA";
            this.txtEstadoBienTitulo.Enabled = false;
            this.txtDescripcionTitulo.Text = string.Empty;
            this.txtDescripcionTitulo.Enabled = false;
        }

        this.OcultarLabelsRequeridos();
    }

    /// <summary>
    /// Habilita o dehabilita los campos de la seccion del mieble o inmueble cuando se seleciiona la obsion Inmueble
    /// </summary>
    /// <param name="pEsInMueble">identifica si se habilita o se deshabilita</param>
    private void HabilitarCamposInmueble(bool pEsInMueble)
    {
        this.txtMatriculainmibiliaria.Enabled = pEsInMueble;
        this.txtCedulaCatastral.Enabled = pEsInMueble;
        this.txtDireccionInmueble.Enabled = pEsInMueble;
        this.FechaRegistro.Enabled = false;
        this.ddlMarcaBien.Enabled = !pEsInMueble;
        this.txtEstadoBien.Enabled = false;
        this.txtNumeroIdentificacionBien.Enabled = pEsInMueble;
        this.FechaRegistro.InitNull = true;
        this.txtEstadoBien.Text = "REGISTRO DENUNCIA";
        this.txtNumeroIdentificacionBien.Text = string.Empty;
        this.txtMatriculainmibiliaria.Text = string.Empty;
        this.txtCedulaCatastral.Text = string.Empty;
        this.txtDireccionInmueble.Text = string.Empty;
        this.ddlMarcaBien.SelectedValue = "-1";
        this.FechaRegistro.InitNull = true;
    }

    /// <summary>
    /// habilita todos los campos que son comunes cuado es un mueble y cuando es un inmueble
    /// </summary>
    private void HabilitarCamposGeneralesMueble()
    {
        this.ddlDepartamento.Enabled = true;
        this.ddlMunicipio.Enabled = true;
        this.ddlClaseEntrada.Enabled = true;
        this.ddlEstadoFisicoBien.Enabled = true;
        this.txtValorEstimadoMueble.Enabled = true;
        this.ddlPorcentajePertenencia.Enabled = true;
        this.ddlDestinacionEconomica.Enabled = true;
        this.FechaVenta.Enabled = false;
        this.FechaAdjudicado.Enabled = false;
        this.txtDescripcionBien.Enabled = true;
        this.ddlDepartamento.Focus();
    }

    /// <summary>
    /// Habilita los campos generales de la seccion titulo o valor
    /// </summary>
    private void HabilitarCamposGeneralesTitulo()
    {
        this.FechaVentaTitulo.Enabled = false;
        this.ddlClaseEntradaTitulo.Enabled = true;
        this.FechaAdjudicacionTitulo.Enabled = false;
        this.txtDescripcionTitulo.Enabled = true;
    }

    /// <summary>
    /// Habilita los campos de la seccion titulo o valor cuado en tipo de titulo es Efectivo
    /// </summary>
    private void HabilitarCamposTituloEfectivo()
    {
        this.txtNuemeroCuentaBancaria.Enabled = true;
        this.ddlEntidadBancaria.Enabled = true;
        this.ddlTipoCuentaBancaria.Enabled = true;
        this.ddlDepartamentoTitulo.Enabled = true;
        this.ddlMunicipioTitulo.Enabled = true;
        this.txtValorEstimadoTitulo.Enabled = true;
        this.txtEstadoBienTitulo.Enabled = false;
        this.txtNuemeroCuentaBancaria.Focus();
    }

    /// <summary>
    /// Habilita los campos de la seccion titulo o valor cuado en tipo de titulo es CDT
    /// </summary>
    private void HabilitarCamposTituloCDT()
    {
        this.ddlEntidadBancaria.Enabled = true;
        this.ddlDepartamentoTitulo.Enabled = true;
        this.ddlMunicipioTitulo.Enabled = true;
        this.txtValorEstimadoTitulo.Enabled = true;
        this.txtNumeroTitulo.Enabled = true;
        this.ddlEntidadEmisora.Enabled = true;
        this.txtCantidadTitulos.Enabled = true;
        this.txtValorUnitarioTitulo.Enabled = true;
        this.ddlEntidadBancaria.Focus();
    }

    /// <summary>
    /// Habilita los campos de la seccion titulo o valor cuado en tipo de titulo es Acciones
    /// </summary>
    private void HabilitarCamposTituloAcciones()
    {
        this.txtNumeroTitulo.Enabled = true;
        this.ddlEntidadEmisora.Enabled = true;
        this.txtCantidadTitulos.Enabled = true;
        this.txtValorUnitarioTitulo.Enabled = true;
        this.txtCantidadVendida.Enabled = true;
        this.txtNumeroTitulo.Focus();
    }

    /// <summary>
    /// Habilita los campos de la seccion titulo o valor cuado en tipo de titulo es Bonos 
    /// </summary>
    private void HabilitarCamposTituloBonos()
    {
        this.ddlDepartamentoTitulo.Enabled = true;
        this.ddlMunicipioTitulo.Enabled = true;
        this.txtValorEstimadoTitulo.Enabled = true;
        this.txtNumeroTitulo.Enabled = true;
        this.ddlEntidadEmisora.Enabled = true;
        this.ddlTipoEntidadEmisora.Enabled = true;
        this.txtNumeroIdentificacionBienTitulo.Enabled = true;
        this.txtCantidadTitulos.Enabled = true;
        this.txtValorUnitarioTitulo.Enabled = true;
        this.txtCantidadVendida.Enabled = true;
        this.ddlDepartamentoTitulo.Focus();
    }

    /// <summary>
    /// Habilita los campos de la seccion titulo o valor cuado en tipo de titulo es cheques y Pagares
    /// </summary>
    private void HabilitarCamposTituloChequesPagares()
    {
        this.ddlDepartamentoTitulo.Enabled = true;
        this.ddlMunicipioTitulo.Enabled = true;
        this.txtValorEstimadoTitulo.Enabled = true;
        this.txtNumeroTitulo.Enabled = true;
        this.ddlEntidadEmisora.Enabled = true;
        this.ddlTipoEntidadEmisora.Enabled = true;
        this.txtNumeroIdentificacionBienTitulo.Enabled = true;
        this.txtCantidadTitulos.Enabled = true;
        this.txtValorUnitarioTitulo.Enabled = true;
        this.ddlDepartamentoTitulo.Focus();
    }

    /// <summary>
    /// Habilita los campos de la seccion titulo o valor cuado en tipo de titulo es Facturas
    /// </summary>
    private void HabilitarCamposTituloFacturas()
    {
        this.ddlDepartamentoTitulo.Enabled = true;
        this.ddlMunicipioTitulo.Enabled = true;
        this.txtNumeroTitulo.Enabled = true;
        this.ddlEntidadEmisora.Enabled = true;
        this.ddlTipoEntidadEmisora.Enabled = true;
        this.txtNumeroIdentificacionBienTitulo.Enabled = true;
        this.txtCantidadTitulos.Enabled = true;
        this.txtValorUnitarioTitulo.Enabled = true;
        this.FechaVencimiento.Enabled = false;
        this.FechaRecibido.Enabled = false;
        this.ddlDepartamentoTitulo.Focus();
    }

    /// <summary>
    /// identifica si hay datos requeridos no diligenciados
    /// </summary>
    /// <returns> true si existen o false en caso contrario</returns>
    private bool ExistenDatosObligatorios()
    {
        bool Esrequerido = false;
        try
        {
            if (!this.RbtBienDenunciadoM.Checked && !this.RbtBienDenunciadoT.Checked)
            {
                this.lblRequeridoBienDenunciado.Visible = true;
                Esrequerido = true;
                throw new Exception(string.Empty);
            }
            else
            {
                this.lblRequeridoBienDenunciado.Visible = false;
                Esrequerido = false;
            }
            if (this.RbtBienDenunciadoM.Checked)
            {
                if (this.ddlTipoBien.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoTipoBien.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoTipoBien.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlSubTipoBien.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoSubTipoBien.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoSubTipoBien.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlClasebien.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoClasebien.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoClasebien.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlDepartamento.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoDepartamento.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoDepartamento.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlMunicipio.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoMunicipio.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoMunicipio.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlEstadoFisicoBien.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoEstadoFisicoBien.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoEstadoFisicoBien.Visible = false;
                    Esrequerido = false;
                }

                if (string.IsNullOrEmpty(this.txtValorEstimadoMueble.Text))
                {
                    this.lblRequeridoValorEstimadoMueble.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoValorEstimadoMueble.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlPorcentajePertenencia.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoPorcentajePertenencia.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoPorcentajePertenencia.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlTipoBien.SelectedValue.Equals("3"))
                {
                    if (string.IsNullOrEmpty(this.txtMatriculainmibiliaria.Text))
                    {
                        this.lblRequeridoMatriculainmibiliaria.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoMatriculainmibiliaria.Visible = false;
                        Esrequerido = false;
                    }

                    if (string.IsNullOrEmpty(this.txtDireccionInmueble.Text))
                    {
                        this.lblRequeridoDireccionInmueble.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoDireccionInmueble.Visible = false;
                        Esrequerido = false;
                    }
                }

                if (this.ddlTipoBien.SelectedValue.Equals("2"))
                {
                    if (string.IsNullOrEmpty(this.txtNumeroIdentificacionBien.Text))
                    {
                        this.lblRequeridoNumeroIdentificacionBien.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoNumeroIdentificacionBien.Visible = false;
                        Esrequerido = false;
                    }

                    if (this.ddlMarcaBien.SelectedValue.Equals("-1"))
                    {
                        this.lblRequeridoMarcaBien.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoMarcaBien.Visible = false;
                        Esrequerido = false;
                    }
                }
            }
            else if (this.RbtBienDenunciadoT.Checked)
            {
                this.OcultarLabelsRequeridos();
                if (this.ddlTipoTitulo.SelectedValue.Equals("-1"))
                {
                    this.lblRequeridoTipoTitulo.Visible = true;
                    Esrequerido = true;
                    throw new Exception(string.Empty);
                }
                else
                {
                    this.lblRequeridoTipoTitulo.Visible = false;
                    Esrequerido = false;
                }

                if (this.ddlTipoTitulo.SelectedValue.Equals("6"))
                {
                    if (string.IsNullOrEmpty(this.txtNuemeroCuentaBancaria.Text))
                    {
                        this.lblRequeridoNuemeroCuentaBancaria.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoNuemeroCuentaBancaria.Visible = false;
                        Esrequerido = false;
                    }

                    if (this.ddlTipoCuentaBancaria.SelectedValue.Equals("-1"))
                    {
                        this.lblRequeridoTipoCuentaBancaria.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoTipoCuentaBancaria.Visible = false;
                        Esrequerido = false;
                    }
                }

                if (this.ddlTipoTitulo.SelectedValue.Equals("6") || this.ddlTipoTitulo.SelectedValue.Equals("3"))
                {
                    if (this.ddlEntidadBancaria.SelectedValue.Equals("-1"))
                    {
                        this.lblRequeridoEntidadBancaria.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoEntidadBancaria.Visible = false;
                        Esrequerido = false;
                    }
                }

                if (!this.ddlTipoTitulo.SelectedValue.Equals("6"))
                {
                    if (string.IsNullOrEmpty(this.txtNumeroTitulo.Text))
                    {
                        this.lblRequeridoNumeroTitulo.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoNumeroTitulo.Visible = false;
                        Esrequerido = false;
                    }
                }
                if (!this.ddlTipoTitulo.SelectedValue.Equals("6") && !this.ddlTipoTitulo.SelectedValue.Equals("3") && !this.ddlTipoTitulo.SelectedValue.Equals("1"))
                {
                    if (string.IsNullOrEmpty(this.txtNumeroIdentificacionBienTitulo.Text))
                    {
                        this.lblRequeridoNumeroIdentificacionBienTitulo.Visible = true;
                        Esrequerido = true;
                        throw new Exception(string.Empty);
                    }
                    else
                    {
                        this.lblRequeridoNumeroIdentificacionBienTitulo.Visible = false;
                        Esrequerido = false;
                    }
                }

                if (this.ddlTipoTitulo.SelectedValue.Equals("7"))
                {
                    Esrequerido = false;

                    if (string.IsNullOrEmpty(this.txtNumeroTitulo.Text))
                    {
                        this.lblRequeridoNumeroTitulo.Visible = true;
                        Esrequerido = true;
                    }

                    if (string.IsNullOrEmpty(this.txtNumeroIdentificacionBienTitulo.Text))
                    {
                        this.lblRequeridoNumeroIdentificacionBienTitulo.Visible = true;
                        Esrequerido = true;
                    }
                }
            }
            else
            {
                Esrequerido = true;
            }
            return Esrequerido;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return Esrequerido;
        }
        catch (Exception ex)
        {
            if (Esrequerido)
            {
                this.toolBar.LipiarMensajeError();
            }
            else
            {
                this.toolBar.MostrarMensajeError(ex.Message);
            }
            return Esrequerido;
        }
    }

    /// <summary>
    /// Captura la infortación del TituloValor
    /// </summary>
    private TituloValor CapturarValoresTituloValor()
    {
        try
        {
            if (!this.ExistenDatosObligatorios())
            {
                int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
                int vId = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id"));
                string vBienDenunciado = this.RbtBienDenunciadoM.Checked ? this.ddlTipoBien.SelectedItem.Text.ToUpper() : this.ddlTipoTitulo.SelectedItem.Text.ToUpper();
                string vDato = this.ObtenerDato(false);
                if (!this.vMostrencosService.ValidarExistenciaBienDenunciado(vIdDenunciaBien, vBienDenunciado, vDato, vId))
                {
                    string vMensaje = this.vMostrencosService.ValidarExistenciaBienDenunciadoOtrasRegionales(vIdDenunciaBien, vBienDenunciado, vDato);
                    if (string.IsNullOrEmpty(vMensaje))
                    {
                        TituloValor vTituloValor = this.vMostrencosService.ConsultarTituloValorPorIdTituloValor(vId);
                        vTituloValor.ClaseEntrada = this.ddlClaseEntradaTitulo.SelectedValue;
                        vTituloValor.DescripcionTitulo = this.txtDescripcionTitulo.Text.Trim().ToUpper();
                        vTituloValor.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                        vTituloValor.EstadoBien = "REGISTRO DENUNCIA";
                        switch (vTituloValor.IdTipoTitulo.ToString())
                        {
                            case "1":
                                vTituloValor.NumeroTituloValor = this.txtNumeroTitulo.Text.Trim().ToUpper();
                                vTituloValor.IdEntidadEmisora = Convert.ToInt32(this.ddlEntidadEmisora.SelectedValue);
                                vTituloValor.CantidadTitulos = this.txtCantidadTitulos.Text.Trim();
                                if (!string.IsNullOrEmpty(this.txtValorUnitarioTitulo.Text))
                                {
                                    vTituloValor.ValorUnidadTitulo = Convert.ToDecimal(this.txtValorUnitarioTitulo.Text.Trim());
                                }

                                vTituloValor.CantidadVendida = this.txtCantidadVendida.Text.Trim();
                                break;

                            case "2":
                                if (!this.ddlMunicipioTitulo.SelectedValue.Equals("-1"))
                                {
                                    vTituloValor.IdMunicipio = this.ddlMunicipioTitulo.SelectedValue;
                                    vTituloValor.IdDepartamento = this.ddlDepartamentoTitulo.SelectedValue;
                                }

                                if (!string.IsNullOrEmpty(this.txtValorEstimadoTitulo.Text))
                                {
                                    vTituloValor.ValorEfectivoEstimado = Convert.ToDecimal(this.txtValorEstimadoTitulo.Text.Trim());
                                }

                                vTituloValor.NumeroTituloValor = this.txtNumeroTitulo.Text.Trim().ToUpper();
                                vTituloValor.IdEntidadEmisora = Convert.ToInt32(this.ddlEntidadEmisora.SelectedValue);
                                vTituloValor.IdTipoIdentificacionEntidadEmisora = Convert.ToInt32(this.ddlTipoEntidadEmisora.SelectedValue);
                                vTituloValor.NumeroIdentificacionBien = this.txtNumeroIdentificacionBienTitulo.Text.Trim();
                                vTituloValor.CantidadTitulos = this.txtCantidadTitulos.Text.Trim();
                                if (!string.IsNullOrEmpty(this.txtValorUnitarioTitulo.Text))
                                {
                                    vTituloValor.ValorUnidadTitulo = Convert.ToDecimal(this.txtValorUnitarioTitulo.Text.Trim());
                                }

                                vTituloValor.CantidadVendida = this.txtCantidadVendida.Text.Trim();
                                break;

                            case "3":
                                vTituloValor.EntidadBancaria = this.ddlEntidadBancaria.SelectedValue;
                                if (!this.ddlMunicipioTitulo.SelectedValue.Equals("-1"))
                                {
                                    vTituloValor.IdMunicipio = this.ddlMunicipioTitulo.SelectedValue;
                                    vTituloValor.IdDepartamento = this.ddlDepartamentoTitulo.SelectedValue;
                                }

                                if (!string.IsNullOrEmpty(this.txtValorEstimadoTitulo.Text))
                                {
                                    vTituloValor.ValorEfectivoEstimado = Convert.ToDecimal(this.txtValorEstimadoTitulo.Text.Trim());
                                }

                                vTituloValor.NumeroTituloValor = this.txtNumeroTitulo.Text.Trim();
                                vTituloValor.IdEntidadEmisora = Convert.ToInt32(this.ddlEntidadEmisora.SelectedValue);
                                vTituloValor.CantidadTitulos = this.txtCantidadTitulos.Text.Trim();
                                if (!string.IsNullOrEmpty(this.txtValorUnitarioTitulo.Text))
                                {
                                    vTituloValor.ValorUnidadTitulo = Convert.ToDecimal(this.txtValorUnitarioTitulo.Text.Trim());
                                }
                                break;

                            case "4":
                                if (!this.ddlMunicipioTitulo.SelectedValue.Equals("-1"))
                                {
                                    vTituloValor.IdMunicipio = this.ddlMunicipioTitulo.SelectedValue;
                                    vTituloValor.IdDepartamento = this.ddlDepartamentoTitulo.SelectedValue;
                                }

                                if (!string.IsNullOrEmpty(this.txtValorEstimadoTitulo.Text))
                                {
                                    vTituloValor.ValorEfectivoEstimado = Convert.ToDecimal(this.txtValorEstimadoTitulo.Text.Trim());
                                }

                                vTituloValor.NumeroTituloValor = this.txtNumeroTitulo.Text.Trim().ToUpper();
                                vTituloValor.IdEntidadEmisora = Convert.ToInt32(this.ddlEntidadEmisora.SelectedValue);
                                vTituloValor.IdTipoIdentificacionEntidadEmisora = Convert.ToInt32(this.ddlTipoEntidadEmisora.SelectedValue);
                                vTituloValor.NumeroIdentificacionBien = this.txtNumeroIdentificacionBienTitulo.Text.Trim();
                                vTituloValor.CantidadTitulos = this.txtCantidadTitulos.Text.Trim();
                                if (!string.IsNullOrEmpty(this.txtValorUnitarioTitulo.Text))
                                {
                                    vTituloValor.ValorUnidadTitulo = Convert.ToDecimal(this.txtValorUnitarioTitulo.Text.Trim());
                                }
                                break;

                            case "5":
                                if (!this.ddlMunicipioTitulo.SelectedValue.Equals("-1"))
                                {
                                    vTituloValor.IdMunicipio = this.ddlMunicipioTitulo.SelectedValue;
                                    vTituloValor.IdDepartamento = this.ddlDepartamentoTitulo.SelectedValue;
                                }

                                vTituloValor.NumeroTituloValor = this.txtNumeroTitulo.Text.Trim().ToUpper();
                                vTituloValor.IdEntidadEmisora = Convert.ToInt32(this.ddlEntidadEmisora.SelectedValue); ;
                                vTituloValor.IdTipoIdentificacionEntidadEmisora = Convert.ToInt32(this.ddlTipoEntidadEmisora.SelectedValue);
                                vTituloValor.NumeroIdentificacionBien = this.txtNumeroIdentificacionBienTitulo.Text.Trim();
                                vTituloValor.CantidadTitulos = this.txtCantidadTitulos.Text.Trim();
                                if (!string.IsNullOrEmpty(this.txtValorUnitarioTitulo.Text))
                                {
                                    vTituloValor.ValorUnidadTitulo = Convert.ToDecimal(this.txtValorUnitarioTitulo.Text.Trim());
                                }
                                break;

                            case "6":
                                vTituloValor.NumeroCuentaBancaria = this.txtNuemeroCuentaBancaria.Text.Trim();
                                vTituloValor.EntidadBancaria = this.ddlEntidadBancaria.SelectedValue;
                                vTituloValor.TipoCuentaBancaria = this.ddlTipoCuentaBancaria.SelectedValue;
                                if (!this.ddlMunicipioTitulo.SelectedValue.Equals("-1"))
                                {
                                    vTituloValor.IdMunicipio = this.ddlMunicipioTitulo.SelectedValue;
                                    vTituloValor.IdDepartamento = this.ddlDepartamentoTitulo.SelectedValue;
                                }

                                if (!string.IsNullOrEmpty(this.txtValorEstimadoTitulo.Text))
                                {
                                    vTituloValor.ValorEfectivoEstimado = Convert.ToDecimal(this.txtValorEstimadoTitulo.Text.Trim());
                                }
                                break;

                            case "7":
                                if (!this.ddlMunicipioTitulo.SelectedValue.Equals("-1"))
                                {
                                    vTituloValor.IdMunicipio = this.ddlMunicipioTitulo.SelectedValue;
                                    vTituloValor.IdDepartamento = this.ddlDepartamentoTitulo.SelectedValue;
                                }

                                if (!string.IsNullOrEmpty(this.txtValorEstimadoTitulo.Text))
                                {
                                    vTituloValor.ValorEfectivoEstimado = Convert.ToDecimal(this.txtValorEstimadoTitulo.Text.Trim());
                                }

                                vTituloValor.NumeroTituloValor = this.txtNumeroTitulo.Text.Trim().ToUpper();
                                vTituloValor.IdEntidadEmisora = Convert.ToInt32(this.ddlEntidadEmisora.SelectedValue);
                                vTituloValor.IdTipoIdentificacionEntidadEmisora = Convert.ToInt32(this.ddlTipoEntidadEmisora.SelectedValue);
                                vTituloValor.NumeroIdentificacionBien = this.txtNumeroIdentificacionBienTitulo.Text.Trim();
                                vTituloValor.CantidadTitulos = this.txtCantidadTitulos.Text.Trim();
                                if (!string.IsNullOrEmpty(this.txtValorUnitarioTitulo.Text))
                                {
                                    vTituloValor.ValorUnidadTitulo = Convert.ToDecimal(this.txtValorUnitarioTitulo.Text.Trim());
                                }
                                break;
                        }

                        return vTituloValor;
                    }
                    else
                    {
                        throw new Exception(vMensaje);
                    }
                }
                else
                {
                    throw new Exception("El registro ya existe");
                }
            }

            return null;
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    /// <summary>
    /// Oculta los mensajes de requerido
    /// </summary>
    private void OcultarLabelsRequeridos()
    {
        this.lblFormatoInvalidoFechaAdjudicacionTitulo.Visible = false;
        this.lblFormatoInvalidoFechaRecibido.Visible = false;
        this.lblFormatoInvalidoFechaVencimiento.Visible = false;
        this.lblFormatoInvalidoFechaVentaTitulo.Visible = false;
        this.lblRequeridoEntidadBancaria.Visible = false;
        this.lblRequeridoEstadoBienTitulo.Visible = false;
        this.lblRequeridoNuemeroCuentaBancaria.Visible = false;
        this.lblRequeridoNumeroIdentificacionBienTitulo.Visible = false;
        this.lblRequeridoTipoCuentaBancaria.Visible = false;
        this.lblRequeridoTipoTitulo.Visible = false;
    }

    #endregion
}