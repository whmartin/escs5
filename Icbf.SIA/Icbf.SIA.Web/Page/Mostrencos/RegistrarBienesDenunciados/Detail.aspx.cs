﻿//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Detail.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase del formulario Page_Mostrencos_RegistrarBienesDenunciados_List
/// </summary>
public partial class Page_Mostrencos_RegistrarBienesDenunciados_Detail : GeneralWeb
{
    /// <summary>
    /// Obtiene o establece de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// Pagina
    /// </summary>
    string PageName = "Mostrencos/RegistrarBienesDenunciados";

    /// <summary>
    /// Servicio
    /// </summary>
    SIAService vSIAService = new SIAService();

    /// <summary>
    /// Servicio
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    #region Eventos

    /// <summary>
    /// PreInit
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Evento Page Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        this.toolBar.LipiarMensajeError();
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            this.ValidarPermisos();
            if (!this.Page.IsPostBack)
            {
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Handles the Click event of the btnBuscar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
        this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", vIdDenunciaBien);
        this.NavigateTo("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnConsultar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        try
        {
            this.NavigateTo("List.aspx");
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnGuardar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Add.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnEditar control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        this.NavigateTo("Edit.aspx");
    }

    #endregion

    #region Metodos

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
            DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
            RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
            var Calidad = vRegistroDenuncia.ListaHistoricoEstadosDenunciaBien.Find(x => x.IdFase == 3);
            if (Calidad != null)
            {
                this.toolBar.MostrarBotonNuevo(false);
            }
            if (vDenuncia.IdEstadoDenuncia == 11 || vDenuncia.IdEstadoDenuncia == 13 || vDenuncia.IdEstadoDenuncia == 19)
            {
                this.toolBar.MostrarBotonNuevo(false);
                this.toolBar.MostrarBotonEditar(false);
            }

            int vId = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Id"));
            string vTipo = this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Tipo").ToString();
            string vMensaje = this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.Mensaje").ToString();
            this.CargarDatosDenuncia();
            if (vTipo.Equals("MUEBLE"))
            {
                this.RbtBienDenunciadoM.Checked = true;
                this.RbtBienDenunciadoT.Checked = false;
                this.pnlInformacionMueble.Visible = true;
                this.pnlInformacionTitulo.Visible = false;
                this.CargarDatosMueble(vId);
            }
            else
            {
                this.RbtBienDenunciadoM.Checked = false;
                this.RbtBienDenunciadoT.Checked = true;
                this.pnlInformacionMueble.Visible = false;
                this.pnlInformacionTitulo.Visible = true;
                this.CargarDatosTitulo(vId);
            }

            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.RegistrarBienDenunciado.Mensaje", null);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// carga la informacion del titulo o valor
    /// </summary>
    /// <param name="pIdTituloValor">id del titulo o valor</param>
    private void CargarDatosTitulo(int pIdTituloValor)
    {
        try
        {
            TituloValor vTituloValor = this.vMostrencosService.ConsultarTituloValorPorIdTituloValor(pIdTituloValor);
            if (vTituloValor.EstadoBien.Equals("ADJUDICADO"))
            {
                toolBar.OcultarBotonEditar(false);
            }
            this.CargarListasTitulo(vTituloValor);
            this.hfCodigoProducto.Value = vTituloValor.CodigoProducto;
            this.ddlTipoTitulo.SelectedValue = vTituloValor.IdTipoTitulo.ToString();
            if (vTituloValor.FechaVenta != null)
            {
                this.FechaVentaTitulo.Date = Convert.ToDateTime(vTituloValor.FechaVenta).Date;
            }

            this.ddlClaseEntradaTitulo.SelectedValue = !string.IsNullOrEmpty(vTituloValor.ClaseEntrada) ? vTituloValor.ClaseEntrada : "-1";

            if (vTituloValor.FechaAdjudicado != null)
            {
                this.FechaAdjudicacionTitulo.Date = Convert.ToDateTime(vTituloValor.FechaAdjudicado).Date;
            }

            this.txtDescripcionTitulo.Text = vTituloValor.DescripcionTitulo;
            this.txtEstadoBienTitulo.Text = vTituloValor.EstadoBien;



            switch (vTituloValor.IdTipoTitulo)
            {
                case 1:
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    this.txtCantidadVendida.Text = vTituloValor.CantidadVendida;
                    break;

                case 2:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    this.txtCantidadVendida.Text = vTituloValor.CantidadVendida;
                    break;

                case 3:
                    this.ddlEntidadBancaria.SelectedValue = vTituloValor.EntidadBancaria;
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");

                    break;

                case 4:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");

                    break;

                case 5:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");
                    if (vTituloValor.FechaVencimiento != null)
                    {
                        this.FechaVencimiento.Date = Convert.ToDateTime(vTituloValor.FechaVencimiento);
                    }

                    if (vTituloValor.FechaRecibido != null)
                    {
                        this.FechaRecibido.Date = Convert.ToDateTime(vTituloValor.FechaRecibido);
                    }

                    break;

                case 6:
                    this.txtNuemeroCuentaBancaria.Text = vTituloValor.NumeroCuentaBancaria;
                    this.ddlEntidadBancaria.SelectedValue = vTituloValor.EntidadBancaria;
                    this.ddlTipoCuentaBancaria.SelectedValue = !string.IsNullOrEmpty(vTituloValor.TipoCuentaBancaria) ? vTituloValor.TipoCuentaBancaria : "-1";
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");

                    break;

                case 7:
                    if (vTituloValor.IdMunicipio != null)
                    {
                        this.ddlDepartamentoTitulo.SelectedValue = vTituloValor.IdDepartamento;
                        this.ddlMunicipioTitulo.SelectedValue = vTituloValor.IdMunicipio.ToString();
                    }

                    this.txtValorEstimadoTitulo.Text = vTituloValor.ValorEfectivoEstimado.ToString("N2");
                    this.txtNumeroTitulo.Text = vTituloValor.NumeroTituloValor;
                    this.ddlEntidadEmisora.SelectedValue = vTituloValor.IdEntidadEmisora == 0 ? "-1" : vTituloValor.IdEntidadEmisora.ToString();
                    this.ddlTipoEntidadEmisora.SelectedValue = vTituloValor.IdTipoIdentificacionEntidadEmisora == 0 ? "-1" : vTituloValor.IdTipoIdentificacionEntidadEmisora.ToString();
                    this.txtNumeroIdentificacionBienTitulo.Text = vTituloValor.NumeroIdentificacionBien;
                    this.txtCantidadTitulos.Text = vTituloValor.CantidadTitulos;
                    this.txtValorUnitarioTitulo.Text = vTituloValor.ValorUnidadTitulo.ToString("N2");

                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga las listas desplegables del titulo o valor
    /// </summary>
    /// <param name="pTituloValor">variable con la informacion del titulo o valor</param>
    private void CargarListasTitulo(TituloValor pTituloValor)
    {
        try
        {
            this.CargarListaDepartamentoTitulo();
            if (pTituloValor.IdDepartamento != null)
            {
                this.ddlDepartamentoTitulo.SelectedValue = pTituloValor.IdDepartamento;
                this.CargarListaMunicipioTitulo(pTituloValor.IdDepartamento);
            }

            this.CargarListaTipoCuenta();
            this.CargarListaEntidadBancaria();
            this.CargarListaEntidadEmisora();
            this.CargarListaTipoEntidadEmisora();
            this.CargarListaTipoTitulo();
            this.CargarListaClaseEntradaTitulo();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga la informacion del mueble o inmueble
    /// </summary>
    /// <param name="pIdMuebleInmueble">id del mueble o inmueble</param>
    private void CargarDatosMueble(int pIdMuebleInmueble)
    {
        try
        {
            MuebleInmueble vMuebleInmueble = this.vMostrencosService.ConsultarMuebleInmueblePorId(pIdMuebleInmueble);
            this.CargarListasMuebleInmueble(vMuebleInmueble);
            this.ddlDepartamento.SelectedValue = vMuebleInmueble.IdDepartamento;
            this.ddlTipoBien.SelectedValue = vMuebleInmueble.IdTipoBien.ToString();
            this.ddlSubTipoBien.SelectedValue = vMuebleInmueble.IdSubTipoBien.ToString();
            this.ddlClasebien.SelectedValue = vMuebleInmueble.IdClaseBien.ToString();
            this.ddlMunicipio.SelectedValue = vMuebleInmueble.IdMunicipio.ToString();
            this.ddlClaseEntrada.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.ClaseEntrada) ? vMuebleInmueble.ClaseEntrada : "-1";
            this.ddlEstadoFisicoBien.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.EstadoFisicoBien) ? vMuebleInmueble.EstadoFisicoBien : "-1";
            this.txtValorEstimadoMueble.Text = vMuebleInmueble.ValorEstimadoComercial.ToString("N2");
            this.ddlPorcentajePertenencia.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.PorcentajePertenenciaBien) ? vMuebleInmueble.PorcentajePertenenciaBien : "-1";
            this.ddlDestinacionEconomica.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.DestinacionEconomica) ? vMuebleInmueble.DestinacionEconomica : "-1";
            this.txtNumeroIdentificacionBien.Text = vMuebleInmueble.NumeroIdentificacionInmueble;
            this.txtEstadoBien.Text = vMuebleInmueble.EstadoBien;
            if (vMuebleInmueble.EstadoBien.Equals("ADJUDICADO"))
            {
                toolBar.OcultarBotonEditar(false);
            }
            this.txtDescripcionBien.Text = vMuebleInmueble.DescripcionBien;
            this.hfTipoParametro.Value = vMuebleInmueble.TipoParametro;
            this.hfCodigoParametro.Value = vMuebleInmueble.TipoParametro;

            if (vMuebleInmueble.FechaVenta != null)
            {
                this.FechaVenta.Date = Convert.ToDateTime(vMuebleInmueble.FechaVenta).Date;
            }

            if (vMuebleInmueble.FechaAdjudicado != null)
            {
                this.FechaAdjudicado.Date = Convert.ToDateTime(vMuebleInmueble.FechaAdjudicado).Date;
            }

            if (vMuebleInmueble.IdTipoBien.Equals("3"))
            {
                this.txtMatriculainmibiliaria.Text = vMuebleInmueble.MatriculaInmobiliaria;
                this.txtCedulaCatastral.Text = vMuebleInmueble.CedulaCatastral;
                this.txtDireccionInmueble.Text = vMuebleInmueble.DireccionInmueble;
            }
            else
            {
                this.ddlMarcaBien.SelectedValue = !string.IsNullOrEmpty(vMuebleInmueble.MarcaBien) ? vMuebleInmueble.MarcaBien : "-1";
                if (vMuebleInmueble.FechaRegistro != null)
                {
                    this.FechaRegistro.Date = Convert.ToDateTime(vMuebleInmueble.FechaRegistro).Date;
                }
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga las listas desplegables del mueble o inmueble
    /// </summary>
    /// <param name="pMuebleInmueble">variable de tipo mueble o inmueble</param>
    private void CargarListasMuebleInmueble(MuebleInmueble pMuebleInmueble)
    {
        try
        {
            this.CargarListaSubTipoBien(pMuebleInmueble.IdTipoBien);
            this.CargarListaClasebien(pMuebleInmueble.IdSubTipoBien);
            this.CargarListaDepartamento();
            this.CargarListaMunicipio(pMuebleInmueble.IdDepartamento);
            this.CargarListaClaseEntrada();
            this.CargarListaEstadoBien();
            this.CargarListaDestinacionEconomica();
            this.CargarListaMarcaBien();
            this.CargarListaPorcentajePertenencia();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// carga la informacion de la denuncia
    /// </summary>
    private void CargarDatosDenuncia()
    {
        try
        {
            int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));

            if (vIdDenunciaBien != 0)
            {
                DenunciaBien vDenuncia = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
                RegistroDenuncia vRegistroDenuncia = this.vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                vRegistroDenuncia.NombreEstadoDenuncia = this.vMostrencosService.ConsultarEstadoDenunciaPorId(vDenuncia.IdEstadoDenuncia).NombreEstadoDenuncia;
                if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ARCHIVADO") || vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ANULADA"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(false);
                }

                if (vRegistroDenuncia.NombreEstadoDenuncia.ToUpper().Equals("ADJUDICADO"))
                {
                    this.toolBar.MostrarBotonNuevo(false);
                    this.toolBar.MostrarBotonEditar(true);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.txtFechaRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia : string.Empty;
                this.txtFechaRadicadoCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != new DateTime()
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                List<DenunciaMuebleTitulo> vlstDenunciaBienTitulo = this.vMostrencosService.ConsultarValorSumaDenuncia(vIdDenunciaBien);
                // Se asigna el valor de la denuncia
                if (vlstDenunciaBienTitulo.Count() > 0)
                {
                    //string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString()) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString() : "0,00";
                    string valorDenuncia = !string.IsNullOrEmpty(vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2")) ? vlstDenunciaBienTitulo[0].ValorDenuncia.ToString("C2") : "0,00";
                    this.TextValordelaDenuncia.Text = valorDenuncia;
                }
                else
                {
                    this.TextValordelaDenuncia.Text = "0,00";
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocial.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// valida si se pude o no mostrar los botone sd enueno y editar
    /// </summary>
    private void ValidarPermisos()
    {
        bool EsMostrarBoton = false;
        int vIdDenunciaBien = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistrarBienDenunciado.IdDenunciaBien"));
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDenunciaBienXIdDenunciaBien(vIdDenunciaBien);
        EsMostrarBoton = (vDenunciaBien.IdEstadoDenuncia != 11 && vDenunciaBien.IdEstadoDenuncia != 13) || vDenunciaBien.IdEstadoDenuncia == 14;
        this.toolBar.MostrarBotonNuevo(EsMostrarBoton);
    }

    /// <summary>
    /// Metodo iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoRetornar += new ToolBarDelegate(this.btnRetornar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(this.btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoEditar += new ToolBarDelegate(this.btnEditar_Click);
            this.toolBar.EstablecerTitulos("Registro de Bienes", "Edit");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas clase de bien
    /// </summary>
    private void CargarListaClasebien(string pIdSubTipoBien)
    {
        try
        {
            List<ClaseBien> vListaClaseBien = this.vMostrencosService.ConsultarClaseBien(pIdSubTipoBien);
            vListaClaseBien = vListaClaseBien.OrderBy(p => p.NombreClaseBien).ToList();
            this.ddlClasebien.DataSource = vListaClaseBien;
            this.ddlClasebien.DataTextField = "NombreClaseBien";
            this.ddlClasebien.DataValueField = "IdClaseBien";
            this.ddlClasebien.DataBind();
            this.ddlClasebien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClasebien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas calase de entrada
    /// </summary>
    private void CargarListaClaseEntrada()
    {
        try
        {
            List<ClaseEntrada> vListaClaseEntrada = this.vMostrencosService.ConsultarClaseEntrada();
            vListaClaseEntrada = vListaClaseEntrada.OrderBy(p => p.NombreClaseEntrada).ToList();
            this.ddlClaseEntrada.DataSource = vListaClaseEntrada;
            this.ddlClaseEntrada.DataTextField = "NombreClaseEntrada";
            this.ddlClaseEntrada.DataValueField = "IdClaseEntrada";
            this.ddlClaseEntrada.DataBind();
            this.ddlClaseEntrada.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClaseEntrada.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas calase de entrada del titulo o valor
    /// </summary>
    private void CargarListaClaseEntradaTitulo()
    {
        try
        {
            List<ClaseEntrada> vListaClaseEntrada = this.vMostrencosService.ConsultarClaseEntrada();
            vListaClaseEntrada = vListaClaseEntrada.OrderBy(p => p.NombreClaseEntrada).ToList();
            this.ddlClaseEntradaTitulo.DataSource = vListaClaseEntrada;
            this.ddlClaseEntradaTitulo.DataTextField = "NombreClaseEntrada";
            this.ddlClaseEntradaTitulo.DataValueField = "IdClaseEntrada";
            this.ddlClaseEntradaTitulo.DataBind();
            this.ddlClaseEntradaTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlClaseEntradaTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de departamentos
    /// </summary>
    private void CargarListaDepartamento()
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarDepartamentosSeven();
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_DEPARTAMENTO).ToList();
            this.ddlDepartamento.DataSource = vListaDepto;
            this.ddlDepartamento.DataTextField = "NOMBRE_DEPARTAMENTO";
            this.ddlDepartamento.DataValueField = "CODIGO_DEPARTAMENTO";
            this.ddlDepartamento.DataBind();
            this.ddlDepartamento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDepartamento.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de municipios
    /// </summary>
    private void CargarListaMunicipio(string pIdDepto)
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarMunicipiosSeven(pIdDepto);
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_MUNICIPIO).ToList();
            this.ddlMunicipio.DataSource = vListaDepto;
            this.ddlMunicipio.DataTextField = "NOMBRE_MUNICIPIO";
            this.ddlMunicipio.DataValueField = "CODIGO_MUNICIPIO";
            this.ddlMunicipio.DataBind();
            this.ddlMunicipio.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlMunicipio.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de departamentos titulo
    /// </summary>
    private void CargarListaDepartamentoTitulo()
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarDepartamentos();
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_DEPARTAMENTO).ToList();
            this.ddlDepartamentoTitulo.DataSource = vListaDepto;
            this.ddlDepartamentoTitulo.DataTextField = "NOMBRE_DEPARTAMENTO";
            this.ddlDepartamentoTitulo.DataValueField = "CODIGO_DEPARTAMENTO";
            this.ddlDepartamentoTitulo.DataBind();
            this.ddlDepartamentoTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDepartamentoTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Municipios Titulo
    /// </summary>
    private void CargarListaMunicipioTitulo(string pIdDepto)
    {
        try
        {
            List<GN_VDIP> vListaDepto = this.vMostrencosService.ConsultarMunicipios(pIdDepto);
            vListaDepto = vListaDepto.OrderBy(p => p.NOMBRE_MUNICIPIO).ToList();
            this.ddlMunicipioTitulo.DataSource = vListaDepto;
            this.ddlMunicipioTitulo.DataTextField = "NOMBRE_MUNICIPIO";
            this.ddlMunicipioTitulo.DataValueField = "CODIGO_MUNICIPIO";
            this.ddlMunicipioTitulo.DataBind();
            this.ddlMunicipioTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlMunicipioTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas detipo cuenta
    /// </summary>
    private void CargarListaTipoCuenta()
    {
        try
        {
            List<TipoCuenta> vListaTipoCuenta = this.vMostrencosService.ConsultarTipoCuenta();
            vListaTipoCuenta = vListaTipoCuenta.OrderBy(p => p.NombreTipoCta).ToList();
            this.ddlTipoCuentaBancaria.DataSource = vListaTipoCuenta;
            this.ddlTipoCuentaBancaria.DataTextField = "NombreTipoCta";
            this.ddlTipoCuentaBancaria.DataValueField = "IdTipoCta";
            this.ddlTipoCuentaBancaria.DataBind();
            this.ddlTipoCuentaBancaria.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoCuentaBancaria.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Edtidad Bancaria
    /// </summary>
    private void CargarListaEntidadBancaria()
    {
        try
        {
            List<EntidadBancaria> vListaEntidadBancaria = this.vMostrencosService.ConsultarEntidadBancaria();
            vListaEntidadBancaria = vListaEntidadBancaria.OrderBy(p => p.NombreEntidadBancaria).ToList();
            this.ddlEntidadBancaria.DataSource = vListaEntidadBancaria;
            this.ddlEntidadBancaria.DataTextField = "NombreEntidadBancaria";
            this.ddlEntidadBancaria.DataValueField = "IdEntidadBancaria";
            this.ddlEntidadBancaria.DataBind();
            this.ddlEntidadBancaria.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEntidadBancaria.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Entidad Emisora
    /// </summary>
    private void CargarListaEntidadEmisora()
    {
        try
        {
            List<EntidadEmisora> vListaDepto = this.vMostrencosService.ConsultarEntidadEmisora();
            vListaDepto = vListaDepto.OrderBy(p => p.NombreEntidadEmisora).ToList();
            this.ddlEntidadEmisora.DataSource = vListaDepto;
            this.ddlEntidadEmisora.DataTextField = "NombreEntidadEmisora";
            this.ddlEntidadEmisora.DataValueField = "IdEntidadEmisora";
            this.ddlEntidadEmisora.DataBind();
            this.ddlEntidadEmisora.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEntidadEmisora.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Estado Bien
    /// </summary>
    private void CargarListaEstadoBien()
    {
        try
        {
            this.ddlClasebien.SelectedValue = "-1";
            List<EstadoFisicoBien> vListaEstadoFisicoBien = this.vMostrencosService.ConsultarEstadoFisicoBien();
            vListaEstadoFisicoBien = vListaEstadoFisicoBien.OrderBy(p => p.NombreEstadoFisicoBien).ToList();
            this.ddlEstadoFisicoBien.DataSource = vListaEstadoFisicoBien;
            this.ddlEstadoFisicoBien.DataTextField = "NombreEstadoFisicoBien";
            this.ddlEstadoFisicoBien.DataValueField = "IdEstadoFisicoBien";
            this.ddlEstadoFisicoBien.DataBind();
            this.ddlEstadoFisicoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlEstadoFisicoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Marca Bien
    /// </summary>
    private void CargarListaMarcaBien()
    {
        try
        {
            List<MarcaBien> vListaMarcaBien = this.vMostrencosService.ConsultMarcaBien();
            vListaMarcaBien = vListaMarcaBien.OrderBy(p => p.NombreMarcaBien).ToList();
            this.ddlMarcaBien.DataSource = vListaMarcaBien;
            this.ddlMarcaBien.DataTextField = "NombreMarcaBien";
            this.ddlMarcaBien.DataValueField = "IdMarcaBien";
            this.ddlMarcaBien.DataBind();
            this.ddlMarcaBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlMarcaBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Porcentaje Pertenencia
    /// </summary>
    private void CargarListaPorcentajePertenencia()
    {
        try
        {
            for (int i = 1; i <= 100; i++)
            {
                this.ddlPorcentajePertenencia.Items.Insert(i, new ListItem(i.ToString(), i.ToString()));
            }

            this.ddlPorcentajePertenencia.SelectedValue = "0";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de SubTipo Bien
    /// </summary>
    private void CargarListaSubTipoBien(string pIdTipoBien)
    {
        try
        {
            List<SubTipoBien> vListaSubTipoBien = this.vMostrencosService.ConsultarSubTipoBien(pIdTipoBien);
            vListaSubTipoBien = vListaSubTipoBien.OrderBy(p => p.NombreSubTipoBien).ToList();
            this.ddlSubTipoBien.DataSource = vListaSubTipoBien;
            this.ddlSubTipoBien.DataTextField = "NombreSubTipoBien";
            this.ddlSubTipoBien.DataValueField = "IdSubTipoBien";
            this.ddlSubTipoBien.DataBind();
            this.ddlSubTipoBien.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlSubTipoBien.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Tipo Entidad Emisora
    /// </summary>
    private void CargarListaTipoEntidadEmisora()
    {
        try
        {
            List<TipoIdentificacion> vListaTipoIdent = this.vMostrencosService.ConsultarTipoIdentificacion();
            vListaTipoIdent = vListaTipoIdent.Where(p => p.IdTipoIdentificacionPersonaNatural != 3).OrderBy(p => p.NombreTipoIdentificacionPersonaNatural).ToList();
            this.ddlTipoEntidadEmisora.DataSource = vListaTipoIdent;
            this.ddlTipoEntidadEmisora.DataTextField = "NombreTipoIdentificacionPersonaNatural";
            this.ddlTipoEntidadEmisora.DataValueField = "IdTipoIdentificacionPersonaNatural";
            this.ddlTipoEntidadEmisora.DataBind();
            this.ddlTipoEntidadEmisora.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoEntidadEmisora.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Tipo Titulo
    /// </summary>
    private void CargarListaTipoTitulo()
    {
        try
        {
            List<TipoTitulo> vListaTipoTitulo = this.vMostrencosService.ConsultarTipoTitulo();
            vListaTipoTitulo = vListaTipoTitulo.OrderBy(p => p.NombreTipoTitulo).ToList();
            this.ddlTipoTitulo.DataSource = vListaTipoTitulo;
            this.ddlTipoTitulo.DataTextField = "NombreTipoTitulo";
            this.ddlTipoTitulo.DataValueField = "IdTipoTitulo";
            this.ddlTipoTitulo.DataBind();
            this.ddlTipoTitulo.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoTitulo.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga la información de las listas de Destinacion Economica
    /// </summary>
    private void CargarListaDestinacionEconomica()
    {
        try
        {
            List<DestinacionEconomica> vListaDestinacionEconomica = this.vMostrencosService.ConsultarDestinacionEconomica();
            vListaDestinacionEconomica = vListaDestinacionEconomica.OrderBy(p => p.NombreDestinacionEconomica).ToList();
            this.ddlDestinacionEconomica.DataSource = vListaDestinacionEconomica;
            this.ddlDestinacionEconomica.DataTextField = "NombreDestinacionEconomica";
            this.ddlDestinacionEconomica.DataValueField = "IdDestinacionEconomica";
            this.ddlDestinacionEconomica.DataBind();
            this.ddlDestinacionEconomica.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlDestinacionEconomica.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}