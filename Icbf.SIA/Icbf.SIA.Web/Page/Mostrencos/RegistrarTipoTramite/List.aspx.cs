﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs</summary>
// <author>INGENIAN</author>
// <date>29/01/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Business.IBusiness;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
//using Icbf.SIA.Integration.ServiceConsumer;

public partial class Page_Mostrencos_RegistrarTipoTramite_List : GeneralWeb
{

    #region Variables

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/RegistrarTipoTramite";

    /// <summary>
    /// vFileName
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    #endregion

    #region Eventos

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Page_PreInit
    /// </summary>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();
            if (this.ValidateAccess(this.toolBar, this.PageName, SolutionPage.List))
            {
                if (!IsPostBack)
                {
                    this.CargarDatosIniciales();
                    this.CargarGrillaDocumentosSolicitadosYRecibidos();
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    protected void btnAdicionar_Click(object sender, EventArgs e)
    {
        this.AdicionarDocumento();
        this.CargarGrillaDocumentosSolicitadosYRecibidos();
    }

    protected void CheckAceptado_CheckedChanged(object sender, EventArgs e)
    {
        this.CamposJudicial.Visible = false;
        this.CheckRechazado.Checked = false;
    }

    protected void CheckRechazado_CheckedChanged(object sender, EventArgs e)
    {
        this.CamposJudicial.Visible = true;
        this.CheckAceptado.Checked = false;
    }

    /// <summary>
    /// evento SelectedIndexChange de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void grvDocumentacionRecibida_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.grvDocumentacionRecibida.SelectedRow);
    }

    /// <summary>
    /// Evento PageIndexChanging de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void grvDocumentacionRecibida_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvDocumentacionRecibida.PageIndex = e.NewPageIndex;
        this.grvDocumentacionRecibida.PageIndex = e.NewPageIndex;

        if (this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"] != null)
        {
            List<DenunciaBien> vList = (List<DenunciaBien>)this.ViewState["SortedgvRegistrarDocumentosBienDenunciado"];
            this.grvDocumentacionRecibida.DataSource = vList;
            this.grvDocumentacionRecibida.DataBind();
        }
    }

    #endregion

    #region Metodos

    /// <summary>
    /// Iniciar
    /// </summary>
    public void Iniciar()
    {
        DenunciaBien vDenunciaBien = this.ObtenerDenunciaBien();
        if (vDenunciaBien != null)
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Tipo de Trámite");
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            //Icbf.SIA.Integration.ServiceConsumer.ConsultaNotariasSC a = new ConsultaNotariasSC();
            //a.GetNotarias("NOTARIAS");

            if (vDenunciaBien.IdEstadoDenuncia != 13 && vDenunciaBien.IdEstadoDenuncia != 19)
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            }
        }
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            this.ViewState["SortedgvDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
            this.pnlTramiteNotarialAceptado.Visible = false;
            this.pnlArchivo.Visible = false;
            if (vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.TipoTramite != null)
                {
                    this.toolBar.OcultarBotonNuevo(true);

                    if (vRegistroDenuncia.TipoTramite.Equals("NOTARIAL"))
                    {
                        this.ckNotarial.Checked = true;

                    }
                    else if (vRegistroDenuncia.TipoTramite.Equals("JUDICIAL"))
                    {
                        this.ckJudicial.Checked = true;
                    }
                }
                else
                {
                    this.toolBar.OcultarBotonEditar(false);
                }

                if (vRegistroDenuncia.IdEstado == 11 || vRegistroDenuncia.IdEstado == 13 || vRegistroDenuncia.IdEstado == 19)
                {
                    this.toolBar.OcultarBotonNuevo(true);
                    this.toolBar.MostrarBotonEditar(false);
                }
                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;

                if (vRegistroDenuncia.ClaseDenuncia == "1")
                {
                    this.rblTipoDeProceso.SelectedValue = "DECLARACIONVACANTE";
                }
                else if (vRegistroDenuncia.ClaseDenuncia == "2")
                {
                    this.rblTipoDeProceso.SelectedValue = "DECLARACIONMOSTRENCOS";
                }
                else
                {
                    this.rblTipoDeProceso.SelectedValue = "SUCESION";
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }
            }
            this.ObtenerTipoTramite(vIdDenunciaBien);
            string vMensaje = (string)this.GetSessionParameter("Mostrencos.RegistrarTipoTramite.Mensaje");
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.RegistrarTipoTramite.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void AdicionarDocumento()
    {
        DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien = new DocumentosSolicitadosYRecibidos();
        this.GuardarArchivos(vIdDenunciaBien, 0);
        pDocumentosSolicitadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pDocumentosSolicitadosDenunciaBien.IdCausante = 74;
        pDocumentosSolicitadosDenunciaBien.IdApoderado = 67;
        pDocumentosSolicitadosDenunciaBien.IdTipoDocumentoBienDenunciado = Convert.ToInt32(ddlTipoDocumento.SelectedValue);
        pDocumentosSolicitadosDenunciaBien.FechaSolicitud = FechaSolicitud.Date;
        pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionSolicitada = txtObservacionesSolicitado.Text;
        pDocumentosSolicitadosDenunciaBien.IdEstadoDocumento = Convert.ToInt32(rbtEstadoDocumento.SelectedValue);
        pDocumentosSolicitadosDenunciaBien.FechaRecibido = FechaRecibido.Date;
        pDocumentosSolicitadosDenunciaBien.NombreArchivo = fulArchivoRecibido.FileName;
        pDocumentosSolicitadosDenunciaBien.RutaArchivo = GetSessionParameter("RegistroTipoTramite.RutaArchivo").ToString();
        pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = txtObservacionesRecibido.Text;
        pDocumentosSolicitadosDenunciaBien.ObservacionesDocumento = txtObservacionesRecibido.Text;
        pDocumentosSolicitadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pDocumentosSolicitadosDenunciaBien.FechaCrea = DateTime.Now;
        pDocumentosSolicitadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pDocumentosSolicitadosDenunciaBien.FechaModifica = DateTime.Now;
        int InsertarDocumentos = this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosDenunciaBien);
    }

    private DenunciaBien ObtenerDenunciaBien()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        if (this.vIdDenunciaBien != 0)
        {
            try
            {
                return this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
            }
            catch (Exception)
            {
                throw;
            }
        }
        return null;
    }

    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitadosYRecibidos = this.vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        if (vListaDocumentosSolicitadosYRecibidos.Count > 0)
        {
            this.ViewState["SortedgvDocumentacionRecibida"] = vListaDocumentosSolicitadosYRecibidos;
            grvDocumentacionRecibida.DataSource = vListaDocumentosSolicitadosYRecibidos;
            grvDocumentacionRecibida.DataBind();
        }
    }

    private void GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        try
        {
            if (fulArchivoRecibido.HasFile)
            {
                HttpFileCollection files = Request.Files;

                if (files.Count > 0)
                {
                    string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                    string extPdf = ".PDF".ToLower();
                    if (extFile != extPdf)
                    {
                        throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
                    }
                    int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                    if (vtamArchivo > 2048)
                    {
                        throw new Exception("El tamaño del archivo PDF permitido es de 2.048 KB");
                    }
                    HttpPostedFile file = files[pPosition];

                    if (file.ContentLength > 0)
                    {
                        string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");
                        string carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        string filePath = string.Format("{0}{1}", carpetaBase, Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMyyyyHHmm") + ".pdf");

                        bool existeCarpeta = Directory.Exists(carpetaBase);

                        if (!existeCarpeta)
                        {
                            Directory.CreateDirectory(carpetaBase);
                        }
                        this.vFileName = rutaParcial;
                        file.SaveAs(filePath);
                        this.SetSessionParameter("RegistroTipoTramite.RutaArchivo", rutaParcial);
                    }
                }
            }
        }
        catch (HttpRequestValidationException)
        {
            throw new Exception("Se detectó un posible archivo peligroso.");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// guarda datos com: idDenuncia,Numero de radicado denuncia y fecha de denuncia para ser lelvados al formulario de detalles y direcciona a este.
    /// </summary>
    /// <param name="pRow">The Button</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.grvDocumentacionRecibida.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("DocumentarBienDenunciado.IdDenunciaBien", strValue);
            this.SetSessionParameter("DocumentarBienDenunciado.Guardado", "0");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void ObtenerTipoTramite(int IdDenunciaBien)
    {
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBienDecision(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(IdDenunciaBien));

        TramiteJudicial tramiteJudicial = new TramiteJudicial();
        TramiteNotarial tramiteNotarial = new TramiteNotarial();
        TramiteNotarialAceptado tramiteNotarialAceptado = new TramiteNotarialAceptado();

        DesicionAuto desicionAuto = new DesicionAuto();
        DecisionAutoInadmitida decisionAutoInadmitida = new DecisionAutoInadmitida();
        DecisionRecurso decisionRecurso = new DecisionRecurso();

        tramiteJudicial = this.vMostrencosService.ConsultaTramiteJudicial(IdDenunciaBien);
        tramiteNotarial = this.vMostrencosService.ConsultarTramiteNotarial(IdDenunciaBien);

        if (tramiteNotarial.IdTramiteNotarial > 0)
        {

            this.pnlTramiteNotarial.Visible = true;

            if (tramiteNotarial.IdNotaria != "-1")
            {
                Notarias vNotarias = this.vMostrencosService.ConsultarNotarias().Find(x => x.CodNotaria == tramiteNotarial.IdNotaria);
                //Notarias vNotarias = ConsultarNotarias().Find(x => x.CodNotaria == tramiteNotarial.IdNotaria);
                tramiteNotarialAceptado = this.vMostrencosService.ConsultarTramiteNotarialAceptado(tramiteNotarial.IdTramiteNotarial);

                this.DropNotaria.Items.Add(new ListItem
                {
                    Text = string.Concat(
                   vNotarias.NombreNotaria, " ",
                   vNotarias.DepartamentoNotaria, " ",
                   vNotarias.CiudadNotaria),
                    Value = vNotarias.CodNotaria
                });
                this.DropNotaria.SelectedValue = tramiteNotarial.IdNotaria;
                this.TextDepaNotaria.Text = vNotarias.DepartamentoNotaria;
                this.TextMunicipioNotaria.Text = vNotarias.CiudadNotaria;
                this.FechaActoTramite.Date = tramiteNotarial.FechaActaTramiteNotarial;

                if (tramiteNotarial.DecisionTramiteNotarial.Length > 0)
                {
                    if (tramiteNotarial.DecisionTramiteNotarial.Equals("RECHAZADO"))
                    {
                        this.CheckAceptado.Checked = false;
                        this.CheckRechazado.Checked = true;
                        this.CamposJudicial.Visible = true;

                        if (tramiteNotarial.DecisionTramiteNotarialRechazado.Equals("JUDICIAL"))
                        {
                            this.CheckJudicial.Checked = true;
                            this.CheckTerminado.Checked = false;
                        }
                        else if (tramiteNotarial.DecisionTramiteNotarialRechazado.Equals("TERMINADO"))
                        {
                            this.CheckJudicial.Checked = false;
                            this.CheckTerminado.Checked = true;
                        }
                    }
                    else if (tramiteNotarial.DecisionTramiteNotarial.Equals("ACEPTADO"))
                    {
                        this.CheckAceptado.Checked = true;
                        this.CheckRechazado.Checked = false;
                        this.pnlTramiteNotarialAceptado.Visible = true;
                        if (tramiteNotarialAceptado.IdTramiteNotarialAceptado > 0)
                        {
                            this.pnlTramiteNotarialAceptado.Visible = true;
                            this.pnlTramiteJudicial.Visible = false;
                            this.FechaEdictoEmpl.Date = tramiteNotarialAceptado.FechaEdictoEmplazatorio;
                            this.FechaPubliEdicto.Date = tramiteNotarialAceptado.FechaPublicacionEdicto;
                            this.FechaComHacienda.Date = tramiteNotarialAceptado.FechaComunicacionScretariaHacienda;
                            this.FechaComDian.Date = tramiteNotarialAceptado.FechaComunicacionDian;
                            this.FechaSolemnizacion.Date = tramiteNotarialAceptado.FechaSolemnizacion;
                            if (tramiteNotarialAceptado.TerminacionAnormalTramiteNotarial != null)
                            {
                                this.CheckTerminacionAnormal.Checked = (tramiteNotarialAceptado.TerminacionAnormalTramiteNotarial.Equals("1")) ? true : false;
                            }

                        }
                    }
                }

            }

            

        }
        if (tramiteJudicial.IdTramiteJudicial > 0)
        {
            DespachoJudicial despacho = new DespachoJudicial();
            if (tramiteJudicial.IdDespachoJudicial != -1)
            {
                despacho = this.DespachoporID(tramiteJudicial.IdDespachoJudicial);
                this.DropNombDespacho.Text = tramiteJudicial.DespachoJudicialAsignado;
                this.DropNombDespacho.Items.Add(new ListItem
                {
                    Text = string.Concat(
                   despacho.Nombre, " ",
                   despacho.Departamento.NombreDepartamento, " ",
                   despacho.Municipio.NombreMunicipio),
                    Value = despacho.IdDespachoJudicial.ToString()
                });
                this.TextNombDespacho.Text = despacho.Nombre;
                this.TextMunDespacho.Text = despacho.Municipio.NombreMunicipio;
            }
            this.FechaRadicaJudicial.Date = tramiteJudicial.FechaRadicacion;
            this.TextSentencia.Text = tramiteJudicial.Sentencia;
            if (tramiteJudicial.FechaSentencia.ToString().Equals("1/01/0001 12:00:00 a. m."))
            {
                this.FechaDecisionAuto.Inicializar("", false);
            }
            else
            {
                this.FechaDecisionAuto.Date = tramiteJudicial.FechaSentencia;
            }

            if (vDenunciaBien.desicionAuto.NombreDecisionAuto != null)
            {
                if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Admitida"))
                {
                    this.rblDesicionDelAuto.SelectedValue = "CheckAdmitida";
                }
                else if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Inadmitida"))
                {
                    this.rblDesicionDelAuto.SelectedValue = "CheckInadmitida";
                    if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Length > 0)
                    {
                        this.CamposSubsanar.Visible = true;
                        if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Equals("Subsanar"))
                        {
                            this.rblSubSanar.SelectedValue = "CheckSubsanar";
                        }
                        else if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Equals("No Subsanar"))
                        {
                            this.rblSubSanar.SelectedValue = "CheckNoSubsanar";
                        }
                    }
                }
                else if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Rechazada"))
                {
                    this.rblDesicionDelAuto.SelectedValue = "CheckRechazada";
                    if (vDenunciaBien.decisionRecurso.Nombre != null)
                    {
                        if (vDenunciaBien.decisionRecurso.Nombre.Length > 0)
                        {
                            if (vDenunciaBien.decisionRecurso.Nombre.Equals("Aceptada"))
                            {
                                this.RadioButtonList2.SelectedValue = "CheckAceptada";
                            }
                            else if (vDenunciaBien.decisionRecurso.Nombre.Equals("Rechazada"))
                            {
                                this.RadioButtonList2.SelectedValue = "CheckDecisionRechazada";
                            }
                        }
                    }

                }
                if (vDenunciaBien.InterposicionRecurso)
                {
                    this.rblDesicionDelRecurso.SelectedValue = "CheckSi";
                }
                else
                {
                    this.rblDesicionDelRecurso.SelectedValue = "CheckNo";
                }
            }
            this.pnlTramiteJudicial.Visible = true;
        }

    }


    public List<Notarias> ConsultarNotarias()
    {
        IConsultaNotariasBLL iConsultaNotariasBLL = new Icbf.SIA.Business.ConsultaNotariasBLL();
        Notarias objNotarias = new Notarias();
        List<Notarias> listNotarias = new List<Notarias>();

        List<Notaria> listNotaria = iConsultaNotariasBLL.GetNotarias("");

        foreach (Notaria item in listNotaria)
        {
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            listNotarias.Add(objNotarias);
        }
        return listNotarias;
    }


    private DespachoJudicial DespachoporID(int IdDespacho)
    {
        DespachoJudicial despachoJudicial = this.vMostrencosService.ConsultarDespachoJudicialPorId(IdDespacho);
        return despachoJudicial;
    }

    #endregion

    /// <summary>
    /// Controles del Gridview Documentación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Ver"))
        {
            this.btnDescargar.Visible = false;
            bool EsVerArchivo = false;
            this.pnlArchivo.Visible = true;
            string vRuta = string.Empty;
            int vRowIndex = Convert.ToInt32(e.CommandArgument);
            int vIdDocumentoSolicitado = Convert.ToInt32(this.grvDocumentacionRecibida.DataKeys[vRowIndex].Value);

            List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvDocumentacionRecibida"];
            DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();

            EsVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo));
            vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo;

            if (!EsVerArchivo)
            {
                EsVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo;
            }

            if (EsVerArchivo)
            {
                this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                this.hfNombreArchivo.Value = vDocumento.RutaArchivo.ToString();
                if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                {
                    this.ifmVerAdchivo.Visible = true;
                    this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                    this.imgDodumento.Visible = false;
                    this.imgDodumento.ImageUrl = string.Empty;
                    this.btnDescargar.Visible = true;
                }
                else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                {
                    this.ifmVerAdchivo.Visible = false;
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.imgDodumento.Visible = true;
                    this.imgDodumento.ImageUrl = vRuta;
                    this.btnDescargar.Visible = true;
                }
                if (!this.btnDescargar.Visible)
                {
                    this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                    this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                    this.btnDescargar.Visible = false;
                    this.imgDodumento.Visible = false;
                }
            }
        }
    }

    /// <summary>
    /// Cerrar ventana modal del archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void IdCerrarPanelAr_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    /// <summary>
    /// Descargar documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            //bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vIdArchivo + "/" + vNombreArchivo));
            //= "../RegistrarDocumentosBienDenunciado/Archivos/" + vIdArchivo + "/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void grvDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("01/01/0001") || e.Row.Cells[3].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }
}