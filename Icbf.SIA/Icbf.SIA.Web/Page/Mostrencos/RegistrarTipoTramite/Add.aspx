﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs"
    Inherits="Page_Mostrencos_RegistrarTipoTramite_Add" %>

<%--<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>--%>
<%@ Register Src="~/Page/General/Control/fechaBuscar.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <style type="text/css">
        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .rbl label {
            min-width: 80px;
        }

        .rbl input {
            min-width: 20px;
        }

        .auto-style1 {
            width: 45%;
        }

        .auto-style2 {
            height: 27px;
        }

        .auto-style3 {
            height: 22px;
        }

        .auto-style4 {
            width: 50%;
            height: 95px;
        }

        .auto-style5 {
            height: 95px;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>

    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado denuncia *</td>
                        <td>Fecha de Radicado de la Denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en Correspondencia *</td>
                        <td>Fecha Radicado en Correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de Identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Primer Nombre *</td>
                        <td>Segundo Nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Primer Apellido *</td>
                        <td>Segundo Apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Descripción de la Denuncia *</td>
                        <td>Histórico de la Denuncia                            
                            <asp:ImageButton ID="btnInfoDenuncia" CssClass="btnVerMueble" runat="server" ImageUrl="~/Image/btn/info.jpg" Visible="true"
                                Height="16px" Width="16px" OnClick="btnInfoDenuncia_Click" AutoPostBack="true" ToolTip="Ver Detalle" />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" TextMode="multiline" Columns="100" Rows="5" ID="txtDescripcion" Enabled="false" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfIdDocumentoSolocitado" runat="server" />
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlPopUpHistorico" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 25%; left: 15%; width: 800px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center; width: 100%;">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <span style="color: black; font-weight: bold; top: 10px; position: relative">Hist&oacute;rico de la denuncia</span>
                            </div>
                            <div>
                                <div>
                                    <asp:ImageButton ID="btnCerrarInmuebleModal" runat="server" Style="width: 16px; height: 16px; text-decoration: none; float: right; margin-right: 10px; top: -10px; position: relative;" alt="h" OnClick="btnCerrarInmuebleModal_Click" ImageUrl="../../../Image/btn/close.png" />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 100%; height: 200px; align-content: center">
                                <asp:GridView ID="tbHistoria" Width="100%" runat="server" Visible="true" AutoGenerateColumns="False"
                                    AllowPaging="true" AllowSorting="true" GridLines="None"
                                    OnSorting="GvwHistoricoDenuncia_OnSorting" OnPageIndexChanging="GvwHistoricoDenuncia_OnPageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                                        <asp:BoundField HeaderText="Fecha Estado" DataField="FechaCrea" SortExpression="FechaCrea" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Responsable" DataField="Responsable" SortExpression="Responsable" />
                                        <asp:BoundField HeaderText="Fase" DataField="Fase" SortExpression="Fase" />
                                        <asp:BoundField HeaderText="Actuación" DataField="Actuacion" SortExpression="Actuacion" />
                                        <asp:BoundField HeaderText="Acción" DataField="Accion" SortExpression="Accion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramite">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2" class="auto-style1"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de Trámite *
                            <asp:Label ID="lblCampoRequeridoTipoTramite" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblConsultaPor" RepeatLayout="Table" RepeatColumns="3" Width="60%"
                                OnSelectedIndexChanged="RblConsultaPor_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="ckNotarial" Text="Notarial" />
                                <asp:ListItem Value="ckJudicial" Text="Judicial" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramiteNotarial">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite Notarial</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Notaria
                            <label runat="server" id="ReqNotaria" visible="false">*</label>
                            <asp:RequiredFieldValidator runat="server" ID="rfvDropNomina" ControlToValidate="DropNotaria"
                                SetFocusOnError="true" ErrorMessage=" Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                ForeColor="Red" InitialValue="-1" Enabled="false"></asp:RequiredFieldValidator>
                        </td>
                        <td>Departamento de la Notaria
                            <asp:RequiredFieldValidator runat="server" ID="TextDepaNotariaValidator" ControlToValidate="TextDepaNotaria"
                                SetFocusOnError="true" ErrorMessage=" Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="DropNotaria" AutoPostBack="true" Enabled="false" Width="250px"
                                OnSelectedIndexChanged="DropNotaria_SelectedIndexChanged">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="TextDepaNotaria" Width="250px" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Municipio de la Notaria
                            <asp:RequiredFieldValidator runat="server" ID="MunicipioValidator" ControlToValidate="TextMunicipioNotaria"
                                SetFocusOnError="true" ErrorMessage=" Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
                        </td>
                        <td>Decisión del Trámite Notarial
                            <label runat="server" id="ReqDeciTra" visible="false">*</label>
                            <asp:Label ID="lblJudicial" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextMunicipioNotaria" Requerid="true" Width="250px" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="RadioButtonRechazado" OnSelectedIndexChanged="RadioButtonRechazado_SelectedIndexChanged"
                                RepeatLayout="Table" RepeatColumns="3" AutoPostBack="true">
                                <asp:ListItem Value="CheckAceptado" Text="Aceptado" />
                                <asp:ListItem Value="CheckRechazado" Text="Rechazado" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="reqtxtNumeroResolA2" ForeColor="Red"
                                ControlToValidate="FechaActoTramite$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha de Acta de Trámite Notarial debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha del Acta del Trámite Notarial
                            <label runat="server" id="ReqFechaActa" visible="false">*</label>
                            <asp:Label ID="lblFechaActaTramiteNotarial" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>

                        </td>
                        <td id="CamposJudicial" runat="server" visible="false">

                            <asp:RadioButtonList runat="server" ID="RadioButtonJudiTer" RepeatLayout="Table" RepeatColumns="3"
                                AutoPostBack="true" OnSelectedIndexChanged="RadioButtonJudiTer_SelectedIndexChanged">
                                <asp:ListItem Value="CheckJudicial" Text="Judicial" />
                                <asp:ListItem Value="CheckTerminado" Text="Terminado" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator7" ForeColor="Red"
                                ControlToValidate="FechaActoTramite$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <uc1:fecha runat="server" ID="FechaActoTramite" Requerid="false" Enabled="false" Width="250px" />
                        </td>
                        <td></td>
                    </tr>

                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramiteNotarialAceptado" Visible="false">
                <table width="90%" align="center">

                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite Notarial Aceptado</td>
                    </tr>

                    <tr class="rowB">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator1" ForeColor="Red"
                                ControlToValidate="FechaEdictoEmpl$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha del Edito Emplazatorio debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha Edicto Emplazatorio *
                            <asp:RequiredFieldValidator ID="rfvFechaEdictoEmp" runat="server" ControlToValidate="FechaEdictoEmpl$txtFecha"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator2" ForeColor="Red"
                                ControlToValidate="FechaPubliEdicto$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha de Publicación del Edicto debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha de Publicación del Edicto *
                            <asp:RequiredFieldValidator ID="rfvFechaEdictoPublic" runat="server" ControlToValidate="FechaPubliEdicto$txtFecha"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator8" ForeColor="Red"
                                ControlToValidate="FechaEdictoEmpl$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <uc1:fecha runat="server" ID="FechaEdictoEmpl" Requerid="false" Enabled="false" Width="250px" />
                        </td>
                        <td>                            
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator9" ForeColor="Red"
                                ControlToValidate="FechaPubliEdicto$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <uc1:fecha runat="server" ID="FechaPubliEdicto" Requerid="false" Enabled="false" Width="250px" />
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator3" ForeColor="Red"
                                ControlToValidate="FechaComHacienda$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha Comunicación Secretaria de Hacienda debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha Comunicación Secretaria de Hacienda *
                            <asp:RequiredFieldValidator ID="rfvFechaSecretariaH" runat="server" ControlToValidate="FechaComHacienda$txtFecha"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator10" ForeColor="Red"
                                ControlToValidate="FechaComDian$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha Comunicación DIAN debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha Comunicación DIAN *
                            <asp:RequiredFieldValidator ID="rfvComunicadoDIAN" runat="server" ControlToValidate="FechaComDian$txtFecha"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator11" ForeColor="Red"
                                ControlToValidate="FechaComHacienda$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <uc1:fecha runat="server" ID="FechaComHacienda" Requerid="false" Enabled="false" Width="250px" />
                        </td>
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator4" ForeColor="Red"
                                ControlToValidate="FechaComDian$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <uc1:fecha runat="server" ID="FechaComDian" Requerid="false" Enabled="false" Width="250px" />
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator5" ForeColor="Red"
                                ControlToValidate="FechaSolemnizacion$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage="La Fecha de Solemnización debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha de Solemnización *
                            <asp:RequiredFieldValidator ID="rfvSolemnizacion" runat="server" ControlToValidate="FechaSolemnizacion$txtFecha"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:CustomValidator runat="server" ID="CustomValidator14" ForeColor="Red"
                                ClientValidationFunction="requiredCheck" ValidationGroup="btnGuardar"
                                ErrorMessage="Campo Requerido">
                            </asp:CustomValidator>
                            <br />
                            Terminación Anormal del Trámite Notarial *
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator12" ForeColor="Red"
                                ControlToValidate="FechaSolemnizacion$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="btnGuardar"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <uc1:fecha runat="server" ID="FechaSolemnizacion" Requerid="false" Enabled="false" Width="250px" />
                        </td>
                        <td>
                            <asp:CheckBox runat="server" ID="CheckTerminacionAnormal" Text="Herederos de Mayor Derecho" />
                        </td>
                    </tr>

                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramiteJudicial">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite Judicial</td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del Trámite Judicial</td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha de Radicación *
                            <asp:RequiredFieldValidator ID="rfvRadicacion" runat="server" ControlToValidate="FechaRadicaJudicial$txtFecha" Enabled="false"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator16" ForeColor="Red"
                                ControlToValidate="FechaRadicaJudicial$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage=" La Fecha de Radicación debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>

                        </td>
                        <td>Nombre del despacho judicial Asignado *
                            <asp:RequiredFieldValidator ID="rfvDespacho" runat="server" ControlToValidate="DropNombDespacho" InitialValue="-1" Enabled="false"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaRadicaJudicial" Requerid="false" Enabled="false" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="DropNombDespacho" AutoPostBack="true" Enabled="false" Width="250px"
                                OnSelectedIndexChanged="DropNombDespacho_SelectedIndexChanged">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Departamento del Juzgado</td>
                        <td>Municipio del Juzgado</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextDepaDespacho" Width="250px" Enabled="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="TextMunJuzgado" Width="250px" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de Proceso</td>
                        <td>Sentencia *
                            <asp:RequiredFieldValidator ID="rfvSentencia" runat="server" ControlToValidate="TextSentencia" Enabled="false"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <div style="margin-top: 10px">
                                <asp:RadioButtonList runat="server" ID="rblTipoDeProceso" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                                    <asp:ListItem Value="SUCESION" Text="Sucesión" />
                                    <asp:ListItem Value="DECLARACIONVACANTE" Text="Declarativo vacante" />
                                    <asp:ListItem Value="DECLARACIONMOSTRENCOS" Text="Declarativo mostrenco" />
                                </asp:RadioButtonList>
                            </div>
                        </td>
                        <td>
                            <asp:TextBox runat="server" TextMode="multiline" Style="resize: none" Columns="100" Width="250px" Rows="5" ID="TextSentencia"
                                onkeypress="return CheckLength();" onkeyup="return CheckLength();" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información Demanda Radicada</td>
                    </tr>

                    <tr class="rowB">
                        <td>Fecha Decisión del Auto *
                            <asp:RequiredFieldValidator ID="rfvFechaDesicion" runat="server" ControlToValidate="FechaDecisionAuto$txtFecha" Enabled="false"
                                ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator15" ForeColor="Red"
                                ControlToValidate="FechaDecisionAuto$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="btnGuardar"
                                ErrorMessage=" La Fecha Decisión del Auto debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>

                        </td>
                        <td>Decisión del Auto *
                            <asp:RequiredFieldValidator ID="rfvDesicion" runat="server" ControlToValidate="rblDesicionDelAuto"
                                Enabled="false" ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido"
                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:Label ID="lblDesAuto" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaDecisionAuto" Requerid="false" Enabled="false" />
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblDesicionDelAuto" RepeatLayout="Table" OnSelectedIndexChanged="rblDesicionDelAuto_SelectedIndexChanged" 
                                RepeatColumns="3" AutoPostBack="true">
                                <asp:ListItem Value="CheckAdmitida" Text="Admitida" />
                                <asp:ListItem Value="CheckInadmitida" Text="Inadmitida" />
                                <asp:ListItem Value="CheckRechazada" Text="Rechazada" />
                            </asp:RadioButtonList>
                            <br />
                            <div id="CamposSubsanar" runat="server" visible="false">

                                <asp:RadioButtonList runat="server" ID="rblSubSanar" RepeatLayout="Table" RepeatColumns="3" AutoPostBack="true"
                                    OnSelectedIndexChanged="rblSubSanar_SelectedIndexChanged">
                                    <asp:ListItem Value="CheckSubsanar" Text="Subsanar" />
                                    <asp:ListItem Value="CheckNoSubsanar" Text="No subsanar" />
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                    <div id="pnlInter" runat="server" visible="false">
                        <tr class="rowB">
                            <td>¿Presenta la Interposición de Recurso? *
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rblDesicionDelRecurso"
                                    ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <div id="lblDesicionDelRecurso" runat="server" visible="false">
                                    Decisión del Recurso *
                                    <asp:RequiredFieldValidator ID="rfvRechazada1" runat="server" ControlToValidate="RadioButtonList2"
                                    ValidationGroup="btnGuardar" ErrorMessage="Campo Requerido" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:RadioButtonList runat="server" ID="rblDesicionDelRecurso" RepeatLayout="Table" RepeatColumns="3">
                                    <asp:ListItem Value="CheckSi" Text="Sí" />
                                    <asp:ListItem Value="CheckNo" Text="No" />
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <div id="pnlDesicionDelRecurso" runat="server" visible="false">
                                    <asp:RadioButtonList runat="server" ID="RadioButtonList2" RepeatLayout="Table" RepeatColumns="3" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="CheckAceptada" Text="Aceptado" />
                                    <asp:ListItem Value="CheckDecisionRechazada" Text="Rechazado" />
                                    </asp:RadioButtonList>
                               </div>
                                
                            </td>
                        </tr>
                    </div>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Documento soporte de la denuncia *
                            <asp:Label ID="lblCampoRequeridoTipoDocumentoSolicitado" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:CustomValidator runat="server"
                                ID="reqFechaSolicitudMayor" ForeColor="Red"
                                ControlToValidate="FechaSolicitud$txtFecha"
                                ClientValidationFunction="validateDate" ValidationGroup="DocumentacionSolicitada"
                                ErrorMessage="La Fecha de Solicitud debe ser menor o igual a la fecha actual.">
                            </asp:CustomValidator>
                            <br />
                            Fecha de solicitud *
                    <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" ControlToValidate="FechaSolicitud$txtFecha" ForeColor="Red"
                        ErrorMessage="Campo requerido" ValidationGroup="DocumentacionSolicitada"></asp:RequiredFieldValidator>
                            <br />
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator6" ForeColor="Red"
                                ControlToValidate="FechaSolicitud$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="DocumentacionSolicitada"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator17" ForeColor="Red"
                                ControlToValidate="FechaSolicitud$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="DocumentacionRequerida"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="true"
                                DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <uc1:fecha ID="FechaSolicitud" runat="server" Enabled="true" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnAdd" TabIndex="-1" runat="server" ImageUrl="~/Image/btn/add.gif" OnClick="btnAdd_Click" CausesValidation="true" ValidationGroup="DocumentacionSolicitada" />
                            <asp:ImageButton ID="btnAplicar" TabIndex="-1" runat="server" ImageUrl="~/Image/btn/apply.png" OnClick="btnAplicar_Click" Visible="false" ValidationGroup="DocumentacionRequerida" />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAdd" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAplicar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Observaciones al documento solicitado</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtObservacionesSolicitado" Enabled="true" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px"
                                Height="100" Style="resize: none" onkeypress="return CheckLength();"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtObservacionesSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2"></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida" Enabled="false">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="2">Documentación recibida
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Estado del documento *
                    <asp:RequiredFieldValidator runat="server" ID="reqEstadoDocumento" ControlToValidate="rbtEstadoDocumento" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td>Fecha de recibido
                            <asp:Label runat="server" ID="ObliFeRe" Visible="false">*</asp:Label>
                            <asp:RequiredFieldValidator runat="server" ID="reqFechaRecibida" ControlToValidate="FechaRecibido$txtFecha" ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                            <br />
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator13" ForeColor="Red"
                                ControlToValidate="FechaRecibido$txtFecha"
                                ClientValidationFunction="validateFechaInvalida" ValidationGroup="DocumentacionRequerida"
                                ErrorMessage="Fecha inv&aacute;lida">
                            </asp:CustomValidator>
                            <br />
                            <asp:CustomValidator runat="server"
                                ID="CustomValidator18" ForeColor="Red"
                                ControlToValidate="FechaRecibido$txtFecha"
                                ClientValidationFunction="validateTodaymenos1" ValidationGroup="DocumentacionSolicitada"
                                ErrorMessage="La Fecha de Solicitud debe ser menor a la fecha actual.">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" AutoPostBack="true" RepeatDirection="Horizontal" CssClass="rbl" OnSelectedIndexChanged="rbtEstadoDocumento_SelectedIndexChanged">
                                <asp:ListItem Value="3">Aceptado</asp:ListItem>
                                <asp:ListItem Value="5">Devuelto</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaRecibido" Requerid="false" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Nombre de archivo
                            <asp:Label runat="server" ID="ObliNoAr" Visible="false">*</asp:Label>
                            <asp:Label runat="server" ID="lblRqfNombreArchivo" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:FileUpload ID="fulArchivoRecibido" Enabled="false" runat="server" />
                            <asp:Label ID="lblNombreArchivo" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento recibido
                            <asp:Label runat="server" ID="ObliObDoRe" Visible="false">*</asp:Label>
                            <asp:RequiredFieldValidator runat="server" ID="reqObservacionesRecibido" ControlToValidate="txtObservacionesRecibido"
                                ValidationGroup="DocumentacionRequerida" SetFocusOnError="true" Display="Dynamic" ForeColor="Red"
                                ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px" Height="100" Style="resize: none" Enabled="true" onkeypress="return CheckLength();"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida" TabIndex="-1" TabStop="true">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvwDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                                OnPageIndexChanging="gvwDocumentacionRecibida_PageIndexChanging" AllowSorting="True"
                                OnSorting="gvwDocumentacionRecibida_Sorting" OnRowDataBound="gvwDocumentacionRecibida_RowDataBound"
                                GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px"
                                OnRowCommand="gvwDocumentacionRecibida_RowCommand" ondblclick="this.blur();" TabStop="true" TabIndex="-1">
                                <Columns>
                                    <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" SortExpression="FechaSolicitud" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla" />
                                    <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" SortExpression="FechaRecibido" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:TemplateField HeaderText="Nombre archivo" SortExpression="NombreArchivo">
                                        <ItemTemplate>
                                            <asp:Button runat="server" TabIndex="-1" CssClass="lnkArchivoDescargar" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" Enabled="true" ImageUrl="~/Image/btn/delete.gif" TabIndex="-1"
                                                Height="16px" Width="16px" ToolTip="Eliminar" CommandArgument="<%# Container.DataItemIndex%>" OnClientClick="return confirm('¿Está seguro de eliminar la información?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument="<%# Container.DataItemIndex%>" TabIndex="-1" ImageUrl="~/Image/btn/edit.gif"
                                                Height="16px" Width="16px" ToolTip="Editar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DocumentacionCompleta" ItemStyle-CssClass="Ocultar_" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlArchivo" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">
                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="always">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnDescargar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-1 align-center">
                                    <div>
                                        <asp:ImageButton ID="IdCerrarPanelAr" runat="server" Style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative; width: 20px; height: 20px;"
                                            alt="h"
                                            OnClick="IdCerrarPanelAr_Click" ImageUrl="../../../Image/btn/close.png" />
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function () {

            document.getElementById("cphCont_FechaRadicaJudicial_txtFecha").focus();


            $(document).on('click', '.btnPopUpHistorico', function () {
                $('.PopUpHistorico').removeClass('hidden');
            });

            $(document).on('click', '.btnCerrarArchivo', function () {
                $('.popuphIstorico').addClass('hidden');
            });

            $('#cphCont_TextSentencia').on('input', function (e) {
                if (document.getElementById("cphCont_TextSentencia").value.length > 250) {
                    this.value = document.getElementById("cphCont_TextSentencia").value.substr(0, 250);
                }
            });

            $('#cphCont_txtObservacionesSolicitado').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionesSolicitado").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionesSolicitado").value.substr(0, 512);
                }
            });

            $('#cphCont_txtObservacionesRecibido').on('input', function (e) {
                if (document.getElementById("cphCont_txtObservacionesRecibido").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtObservacionesRecibido").value.substr(0, 512);
                }
            });
        });

        function validateTodaymenos1(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

            if (DateResolucionA != '') {

                var today = new Date();
                var yesterday = new Date();
                yesterday.setDate(today.getDate() - 1);

                if (DateResolucionA > yesterday) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function CheckLength() {
            var textbox = document.getElementById("cphCont_TextSentencia").value;
            if (textbox.trim().length >= 250) {
                document.getElementById("cphCont_TextSentencia").value = textbox.substr(0, 250);
                return false;
            }
            else {
                return true;
            }
        }

        function CheckLengtRecibido(pin) {
            var textbox = document.getElementById(pin).value;
            if (textbox.trim().length >= 512) {
                document.getElementById(pin).value = textbox.substr(0, 512);
                return false;
            }
            else {
                return true;
            }
        }

        function LengthSolicitado() {
            var textbox = document.getElementById("cphCont_txtObservacionesSolicitado").value;
            if (textbox.trim().length >= 512) {
                return false;
            }
            else {
                return true;
            }
        }

        function validateDate(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            var DateResolucionA = new Date(fechaResolucionA[2], fechaResolucionA[1] - 1, fechaResolucionA[0])

            if (DateResolucionA != '') {

                var today = new Date();
                if (DateResolucionA > today) {
                    arguments.IsValid = false;
                }
                else if (fechaResolucionA[2] == "0000") {
                    arguments.IsValid = true;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function validateFechaInvalida(source, arguments) {
            var EnteredDate = arguments.Value;
            var fechaResolucionA = EnteredDate.split('/');
            if (parseInt(fechaResolucionA[2]) < 1900 || fechaResolucionA[2] == '0000') {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function validateDateResolucion(source, arguments) {

            var EnteredDate = arguments.Value;
            var fechaRecibido = EnteredDate.split('/');
            var DateB = new Date(fechaRecibido[2], fechaRecibido[1] - 1, fechaRecibido[0])

            var fechaSolicitud = document.getElementById('cphCont_FechaSolicitud_txtFecha').value;
            var fechaSolicitada = fechaSolicitud.split('/');
            var DateA = new Date(fechaSolicitada[2], fechaSolicitada[1] - 1, fechaSolicitada[0]);

            if (DateA != '' && DateB != '') {

                var today = new Date();
                if (DateB < DateA) {
                    arguments.IsValid = false;
                } else if (DateB > today) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;
                }
            }
        }

        function SetFocus() {
            var txtControl = document.getElementById('cphCont_FechaRadicaJudicial_txtFecha');
            txtControl.focus();
        }

        function requiredCheck(source, args) {
            var checkBox = document.getElementById("cphCont_CheckTerminacionAnormal");
            args.IsValid = false;
            if (checkBox.checked)
                args.IsValid = true;
        }


    </script>
</asp:Content>
