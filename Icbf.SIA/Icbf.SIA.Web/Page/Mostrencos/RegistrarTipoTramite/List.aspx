﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" 
    Inherits="Page_Mostrencos_RegistrarTipoTramite_List" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagPrefix="uc1" TagName="fecha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
    <style type="text/css">
        .Background {
            background-color: #808080;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .lnkArchivoDescargar {
            background-color: transparent;
            border: none;
            cursor: pointer;
            text-decoration: underline;
            color: blue;
        }

        .Ocultar_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .OcultarTerceros_ {
            font-size: 0;
            width: 0;
            visibility: hidden;
        }

        .descripcionGrilla {
            word-break: break-all;
        }
    </style>

    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlInformacionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información de la Denuncia</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Radicado denuncia *</td>
                        <td>Fecha de Radicado de la Denuncia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoDenuncia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRadicado" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Radicado en Correspondencia *</td>
                        <td>Fecha Radicado en Correspondencia *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtRadicadoCorres" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="fecRaCorrespondencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Tipo de Identificación *</td>
                        <td>Número de Identificación *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelNombrePersonaNatural">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Primer Nombre *</td>
                        <td>Segundo Nombre *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Primer Apellido *</td>
                        <td>Segundo Apellido *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="PanelRazonSocial">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td colspan="2">
                            <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtRazonSocialN" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDescripcionDenuncia">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>Descripción de la Denuncia *</td>
                        <td>Histórico de la Denuncia
                             <a class="btnPopUpHistorico" style="cursor: pointer; width: 16px; height: 16px; text-decoration: none; margin-right: 10px; top: -6px; position: relative;">
                                 <img style="width: 16px; height: 16px;" alt="h" src="../../../Image/btn/info.jpg" />
                             </a>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" TextMode="multiline" Columns="100" Rows="5" ID="txtDescripcion" Enabled="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramite">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de Trámite *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:CheckBox runat="server" ID="ckNotarial" Text="Notarial" Enabled="false"/>
                            <asp:CheckBox runat="server" ID="ckJudicial" Text="Judicial" Enabled="false"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramiteNotarial">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite Notarial</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Notaria</td>
                        <td>Departamento de la Notaria</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="DropNotaria" AutoPostBack="true" Width="250px" Enabled="false">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="DropNotaria"
                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                                ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="TextDepaNotaria" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Municipio de la Notaria</td>
                        <td>Decisión del Trámite Notarial</td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextMunicipioNotaria" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox runat="server" ID="CheckAceptado" Text="Aceptado" OnCheckedChanged="CheckAceptado_CheckedChanged" AutoPostBack="true" Enabled="false"/>
                            <asp:CheckBox runat="server" ID="CheckRechazado" Text="Rechazado" OnCheckedChanged="CheckRechazado_CheckedChanged" AutoPostBack="true" Enabled="false"/>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Fecha del Acta del Trámite Notarial</td>
                        <td id="CamposJudicial" runat="server" visible="false">
                            <asp:CheckBox runat="server" ID="CheckJudicial" Text="Judicial" Enabled="false"/>
                            <asp:CheckBox runat="server" ID="CheckTerminado" Text="Terminado" Enabled="false"/>
                        </td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaActoTramite" Requerid="true" Enabled="false" />
                        </td>
                        <td></td>
                    </tr>

                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramiteNotarialAceptado">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite Notarial Aceptado</td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>

                    <tr class="rowB">
                        <td>Fecha Edicto Emplazatorio</td>
                        <td>Fecha de Publicación del Edicto</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaEdictoEmpl" Requerid="true" Enabled="false" />
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaPubliEdicto" Requerid="true" Enabled="false" />
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Fecha Comunicación Secretaria de Hacienda</td>
                        <td>Fecha Comunicación DIAN</td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaComHacienda" Requerid="true" Enabled="false" />
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaComDian" Requerid="true" Enabled="false" />
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Fecha de Solemnización</td>
                        <td>Terminación Anormal del Trámite Notarial</td>
                    </tr>

                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaSolemnizacion" Requerid="true" Enabled="false" />
                        </td>
                        <td>
                            <asp:CheckBox runat="server" ID="CheckTerminacionAnormal" Text="Herederos de Mayor Derecho" Enabled="false"/>
                        </td>
                    </tr>

                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTramiteJudicial">
                <table width="90%" align="center">
                    <tr class="rowA">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Trámite Judicial</td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información del Trámite Judicial</td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha de Radicación</td>
                        <td>Nombre del Despacho judicial Asignado</td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:fecha runat="server" ID="FechaRadicaJudicial" Requerid="true" Enabled="false" />
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="DropNombDespacho" AutoPostBack="true" Width="250px" Enabled="false"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr class="rowB">
                        <td>Departamento del Juzgado</td>
                        <td>Municipio del Juzgado</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="TextNombDespacho" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="TextMunDespacho" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo de Proceso</td>
                        <td>Sentencia</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblTipoDeProceso" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                                <asp:ListItem Value="SUCESION" Text="Sucesión" />
                                <asp:ListItem Value="DECLARACIONVACANTE" Text="Declaración Vacante" />
                                <asp:ListItem Value="DECLARACIONMOSTRENCOS" Text="Declaración Mostrencos" />
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="TextSentencia" Enabled="false" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2" class="tdTitulos">Información Demanda Radicada</td>
                    </tr>
                    <tr class="rowB">
                        <td>Fecha Decisión del Auto</td>
                        <td>Decisión del Auto</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha runat="server" ID="FechaDecisionAuto" Requerid="true" Enabled="false" />
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblDesicionDelAuto" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                                <asp:listitem value="CheckAdmitida" Text="Admitida" />
                                <asp:listitem value="CheckInadmitida" Text="Inadmitida"/>         
                                <asp:listitem value="CheckRechazada" Text="Rechazada"/>
                            </asp:RadioButtonList>
                            <br />
                            <div id="CamposSubsanar" runat="server" visible="false">
                                <asp:RadioButtonList runat="server" ID="rblSubSanar" RepeatLayout="Table" RepeatColumns="3">
                                <asp:ListItem Value="CheckSubsanar" Text="Subsanar" />
                                <asp:ListItem Value="CheckNoSubsanar" Text="No subsanar" />
                            </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>¿Presenta la Interposición de Recurso?</td>
                        <td>Decisión del Recurso</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblDesicionDelRecurso" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                                <asp:listitem value="CheckSi" Text="Sí" />
                                <asp:listitem value="CheckNo" Text="No"/>         
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="RadioButtonList2" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                                <asp:listitem value="CheckAceptada" Text="Aceptada" />
                                <asp:listitem value="CheckDecisionRechazada" Text="Rechazada"/>         
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionSolicitada">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="3">Documentación solicitada</td>
                    </tr>
                    <tr class="rowB">
                        <td>Tipo documento de bien *</td>
                        <td>Fecha de solicitud *</td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="false"
                                DataValueField="IdTipoDocumentoBienDenunciado" DataTextField="NombreTipoDocumento"  Width="250px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaSolicitud" Requerid="true" Enabled="false" />
                        </td>
                        <td>
                             <asp:ImageButton ID="btnAdicionar" runat="server" ImageUrl="~/Image/btn/add.gif"
                                Height="25px" Width="28px" ToolTip="Adicionar" Enabled="false" OnClick="btnAdicionar_Click"/>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento solicitado *
                              <asp:Label ID="Label1" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false" Enabled="false"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtObservacionesSolicitado" Enabled="false" TextMode="MultiLine" MaxLength="512" Rows="8"
                                Width="600px" Height="100" Style="resize: none"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtObservacionesSolicitado"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDocumentacionRecibida">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td class="tdTitulos" colspan="2">Documentación recibida</td>
                    </tr>
                    <tr class="rowB">
                        <td>Estado del documento *
                            <asp:Label ID="lblrqfEstadoRecibido" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td>Fecha de recibido *                
                        <asp:Label runat="server" ID="lblFechaInvalida" Visible="false" Text="Fecha Inválida" ForeColor="Red" Enabled="false"></asp:Label>
                            <asp:Label runat="server" ID="lblrqfFechaRecibido" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaMaxima" Visible="false" Text="La fecha seleccionada no debe ser posterior a la fecha actual. Por favor revisar" ForeColor="Red"></asp:Label>
                            <asp:Label runat="server" ID="lblValidacionFechaSolicitud" Visible="false" Text="La fecha seleccionada no debe ser anterior a la fecha de solicitud. Por favor revisar" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbtEstadoDocumento" RepeatDirection="Horizontal" Enabled="false">
                                <asp:ListItem Value="3">Aceptado</asp:ListItem>
                                <asp:ListItem Value="5">Devuelto</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <uc1:fecha runat="server" ID="FechaRecibido" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Nombre de archivo * 
                        <asp:Label runat="server" ID="lblRqfNombreArchivo" Visible="false" Text="Campo Requerido" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:FileUpload ID="fulArchivoRecibido" runat="server" Enabled="false"/>
                            <asp:UpdatePanel ID="UpnlDocumento" runat="server" UpdateMode="always">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAdicionar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">Observaciones al documento recibido
                          <asp:Label ID="txtObservacionesRecibidoLabel1" runat="server" Text="Campo Requerido" ForeColor="Red" Visible="false"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtObservacionesRecibido" TextMode="MultiLine" MaxLength="512" Rows="8" Width="600px"
                                Height="100" Style="resize: none" Enabled="false">
                            </asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtObservacionesRecibido"
                                FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlGridDocumentacionRecibida" TabIndex="-1" TabStop="true">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="grvDocumentacionRecibida" AutoGenerateColumns="False" AllowPaging="True"
                                OnPageIndexChanging="grvDocumentacionRecibida_PageIndexChanging" AllowSorting="False"
                                GridLines="None" Width="100%" DataKeyNames="IdDocumentosSolicitadosDenunciaBien" CellPadding="0" Height="16px"
                                OnRowCommand="grvDocumentacionRecibida_RowCommand" ondblclick="this.blur();" TabStop="true" TabIndex="-1"
                                OnRowDataBound="grvDocumentacionRecibida_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="IdDocumentosSolicitadosDenunciaBien" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField DataField="IdTipoDocumentoBienDenunciado" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Tipo documento" DataField="NombreTipoDocumento" SortExpression="NombreTipoDocumento" />
                                    <asp:BoundField HeaderText="Fecha solicitud" DataField="FechaSolicitud" SortExpression="FechaSolicitud" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField HeaderText="Observaciones solicitud" DataField="ObservacionesDocumentacionSolicitada" SortExpression="ObservacionesDocumentacionSolicitada" ItemStyle-CssClass="descripcionGrilla" />
                                    <asp:BoundField DataField="IdEstadoDocumento" ItemStyle-CssClass="Ocultar_" />
                                    <asp:BoundField HeaderText="Estado del documento" DataField="NombreEstado" SortExpression="NombreEstado" />
                                    <asp:BoundField HeaderText="Fecha recibido" DataField="FechaRecibido" SortExpression="FechaRecibido" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField HeaderText="Observaciones recibido" DataField="ObservacionesDocumentacionRecibida" SortExpression="ObservacionesDocumentacionRecibida" ItemStyle-CssClass="descripcionGrilla" />
                                    <asp:TemplateField HeaderText="Nombre archivo" SortExpression="NombreArchivo">
                                        <ItemTemplate>
                                            <asp:Button runat="server" TabIndex="-1" CssClass="lnkArchivoDescargar" ID="lnkArchivoDescargar" CommandName="Ver" CommandArgument="<%# Container.DataItemIndex%>" Text='<%# Eval("NombreArchivo") %>'></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlArchivo" Width="90%" ScrollBars="None" Style="background-color: White; border-color: White; border-width: 2px; border-style: Solid; position: fixed; z-index: 999999; top: 8.5%; left: 4%; width: 1000px; height: 460px; background-color: white; border: 1px solid #dfdfdf;"
                BackColor="white" BorderColor="white" BorderStyle="Solid" BorderWidth="2px" Visible="false">
                <table style="-moz-align-content: center; -o-align-content: center; -webkit-align-content: center; align-content: center; text-align: center">
                    <tr>
                        <td style="background-color: #f6f6f6; border-bottom: 1px solid #dfdfdf;">
                            <div>
                                <div class="col-md-5 align-center">
                                    <h4>
                                        <asp:Label runat="server" ID="lblTitlePopUp"></asp:Label>
                                    </h4>
                                </div>
                                <div class="col-md-5 align-center">
                                    <asp:HiddenField ID="hfIdArchivo" runat="server" />
                                    <asp:HiddenField ID="hfNombreArchivo" runat="server" />
                                    <asp:Button ID="btnDescargar" Visible="false" runat="server" Text="Descargar" OnClick="btnDescargar_Click" Style="top: 6px; position: relative;" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="always">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnDescargar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="col-md-1 align-center">
                                    <div>
                                        <asp:ImageButton ID="IdCerrarPanelAr" runat="server" Style="text-decoration: none; float: right; margin-right: 10px; top: 10px; position: relative; width: 20px; height: 20px;"
                                            alt="h"
                                            OnClick="IdCerrarPanelAr_Click" ImageUrl="../../../Image/btn/close.png" />
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="padding: 5px;">
                        <td style="text-align: center;">
                            <div style="overflow-y: auto; overflow-x: auto; width: 1000px; height: 420px; align-content: center">
                                <iframe runat="server" id="ifmVerAdchivo" visible="false" style="width: 100%; height: 100%"></iframe>
                                <asp:Image ID="imgDodumento" runat="server" Visible="false" Style="width: auto; height: auto" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
