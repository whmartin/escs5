﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs</summary>
// <author>INGENIAN</author>
// <date>29/01/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Business.IBusiness;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_RegistrarTipoTramite_Add : GeneralWeb
{
    #region VARIABLES

    /// <summary>
    /// Declararion Toolbar
    /// </summary>
    masterPrincipal toolBar = new masterPrincipal();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/RegistrarTipoTramite";

    /// <summary>
    /// vFileName
    /// </summary>
    private string vFileName = string.Empty;

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// Id Denuncia Bien
    /// </summary>
    private int vIdDenunciaBien;

    #endregion

    #region LOAD PAGE

    /// <summary>
    /// Page_PreInit
    /// </summary>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Load de la página
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (!Page.IsPostBack)
        {
            this.pnlPopUpHistorico.Visible = false;
        }
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.CargarDatosIniciales();
                this.BlockJudicial();
            }
            else
            {
                this.toolBar.OcultarBotonNuevo(true);
            }
            if (Request.QueryString["oP"] == "E")
            {
                toolBar.LipiarMensajeError();
                this.toolBar.OcultarBotonEditar(false);
                this.toolBar.MostrarBotonConsultar();
                this.toolBar.OcultarBotonNuevo(true);
                this.rblConsultaPor.Enabled = false;
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
                this.toolBar.OcultarBotonEditar(false);

                TramiteNotarial tramiteNotarial = new TramiteNotarial();
                tramiteNotarial = this.vMostrencosService.ConsultarTramiteNotarial(vIdDenunciaBien);


                if (this.rblConsultaPor.SelectedValue == "ckNotarial")
                {
                    if (tramiteNotarial.DecisionTramiteNotarial.Equals("RECHAZADO") && tramiteNotarial.DecisionTramiteNotarialRechazado.Equals("JUDICIAL"))
                    {
                        this.DesBlockNotarial();
                        this.DesBlockjudicial();
                        //this.RadioButtonRechazado.Enabled = false;
                        this.DropNotaria.Focus();
                    }
                    else
                    {
                        this.DesBlockNotarial();
                        this.DropNotaria.Focus();
                    }

                }
                else
                {
                    this.DesBlockjudicial();
                    this.FechaRadicaJudicial.Focus();
                    this.RadioButtonRechazado.Enabled = false;
                }
                this.toolBar.OcultarBotonGuardar(true);
            }
            else
            {
                this.rblConsultaPor.Focus();
                this.toolBar.OcultarBotonEditar(false);
                this.toolBar.OcultarBotonBuscar(false);
                RemoveSessionParameter("DocumentarBienDenunciado.IdDocumentoDenuncia");
            }
        }
    }

    /// <summary>
    /// Iniciar
    /// </summary>
    public void Iniciar()
    {
        this.toolBar = (masterPrincipal)this.Master;
        this.toolBar.EstablecerTitulos("Tipo de Trámite");
        this.toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
        this.toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
        this.toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
        this.toolBar.eventoBuscar += new ToolBarDelegate(btnConsultar_Click);
        this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
    }

    /// <summary>
    /// Metodo Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
            this.ViewState["SortedgvwDocumentacionRecibida"] = new List<DocumentosSolicitadosDenunciaBien>();


            if (vIdDenunciaBien != 0)
            {
                DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBien(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(this.vIdDenunciaBien));
                RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
                if (vRegistroDenuncia.TipoTramite != null)
                {
                    this.toolBar.OcultarBotonNuevo(true);

                    if (vRegistroDenuncia.TipoTramite.Equals("NOTARIAL"))
                    {
                        this.rblConsultaPor.SelectedValue = "ckNotarial";

                    }
                    else if (vRegistroDenuncia.TipoTramite.Equals("JUDICIAL"))
                    {
                        this.rblConsultaPor.SelectedValue = "ckJudicial";
                    }
                }
                else
                {
                    this.toolBar.OcultarBotonEditar(false);
                    this.toolBar.OcultarBotonNuevo(true);
                }

                this.txtRadicadoDenuncia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoDenuncia) ? vRegistroDenuncia.RadicadoDenuncia : string.Empty;
                this.fecRadicado.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoDenuncia.ToString()) ? vRegistroDenuncia.FechaRadicadoDenuncia.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                this.txtRadicadoCorres.Text = !string.IsNullOrEmpty(vRegistroDenuncia.RadicadoCorrespondencia) ? vRegistroDenuncia.RadicadoCorrespondencia.ToString() : string.Empty;
                this.fecRaCorrespondencia.Text = !string.IsNullOrEmpty(vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString()) ? (
                    vRegistroDenuncia.FechaRadicadoCorrespondencia != Convert.ToDateTime("01/01/0001")
                    ? vRegistroDenuncia.FechaRadicadoCorrespondencia.ToString("dd/MM/yyyy") : string.Empty) : string.Empty;
                this.txtTipoIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.CodigoTipoIdentificacion) ? vRegistroDenuncia.CodigoTipoIdentificacion : string.Empty;
                this.txtNumeroIdentificacion.Text = !string.IsNullOrEmpty(vRegistroDenuncia.NUMEROIDENTIFICACION) ? vRegistroDenuncia.NUMEROIDENTIFICACION : string.Empty;

                if (vRegistroDenuncia.ClaseDenuncia == "1")
                {
                    this.rblTipoDeProceso.SelectedValue = "DECLARACIONVACANTE";
                }
                else if (vRegistroDenuncia.ClaseDenuncia == "2")
                {
                    this.rblTipoDeProceso.SelectedValue = "DECLARACIONMOSTRENCOS";
                }
                else
                {
                    this.rblTipoDeProceso.SelectedValue = "SUCESION";
                }

                if (vRegistroDenuncia.IDTIPODOCIDENTIFICA != 7)
                {
                    this.txtPrimerNombre.Text = vRegistroDenuncia.PRIMERNOMBRE.ToString();
                    this.txtSegundoNombre.Text = vRegistroDenuncia.SEGUNDONOMBRE.ToString();
                    this.txtPrimerApellido.Text = vRegistroDenuncia.PRIMERAPELLIDO.ToString();
                    this.txtSegundoApellido.Text = vRegistroDenuncia.SEGUNDOAPELLIDO.ToString();
                    this.PanelNombrePersonaNatural.Visible = true;
                    this.PanelRazonSocial.Visible = false;
                }
                else
                {
                    this.txtRazonSocialN.Text = vRegistroDenuncia.RazonSocial.ToString();
                    this.txtNumeroIdentificacion.Text += "-" + vRegistroDenuncia.DIGITOVERIFICACION.ToString();
                    this.PanelNombrePersonaNatural.Visible = false;
                    this.PanelRazonSocial.Visible = true;
                }

                this.pnlTramiteJudicial.Visible = true;
                this.pnlTramiteNotarial.Visible = true;
                this.CustomValidator6.Visible = true;
                this.CustomValidator17.Visible = false;
                this.RadioButtonRechazado.Enabled = false;
                this.pnlTramiteNotarialAceptado.Visible = false;
                this.cargarTipoDocumentos();
                this.CargarNotarias();
                this.CargarDespachos();
                this.CargarGrillaHistorico(vIdDenunciaBien);
                this.CargarGrillas();
                this.txtDescripcion.Text = vRegistroDenuncia.DescripcionDenuncia;
                this.ObtenerTipoTramite(vIdDenunciaBien);
            }

            string vMensaje = (string)this.GetSessionParameter("Mostrencos.RegistrarTipoTramite.Mensaje");
            if (!string.IsNullOrEmpty(vMensaje))
            {
                this.toolBar.MostrarMensajeGuardado(vMensaje);
                this.SetSessionParameter("Mostrencos.RegistrarTipoTramite.Mensaje", string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }

    }

    #endregion

    #region EVENTOS

    protected void DropNombDespacho_SelectedIndexChanged(object sender, EventArgs e)
    {
        DespachoJudicial despachoJudicial = this.vMostrencosService.ConsultarDespachoJudicialPorId(Convert.ToInt32(this.DropNombDespacho.SelectedValue.ToString()));
        this.TextDepaDespacho.Text = despachoJudicial.Departamento.NombreDepartamento;
        this.TextMunJuzgado.Text = despachoJudicial.Municipio.NombreMunicipio;
        this.DropNombDespacho.Focus();
    }

    protected void DropNotaria_SelectedIndexChanged(object sender, EventArgs e)
    {
        Notarias vNotarias = this.vMostrencosService.ConsultarNotarias().Find(x => x.CodNotaria == this.DropNotaria.SelectedValue.ToString());
        //Notarias vNotarias = ConsultarNotarias().Find(x => x.CodNotaria == this.DropNotaria.SelectedValue.ToString());
        this.TextDepaNotaria.Text = vNotarias.DepartamentoNotaria;
        this.TextMunicipioNotaria.Text = vNotarias.CiudadNotaria;
        this.DropNotaria.Focus();
    }

    protected void RadioButtonJudiTer_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RadioButtonJudiTer.Focus();
            this.lblJudicial.Visible = false;
            switch (this.RadioButtonJudiTer.SelectedItem.Value.ToString())
            {
                case "CheckTerminado":
                    this.BlockJudicial();
                    break;
                case "CheckJudicial":
                    this.pnlTramiteJudicial.Enabled = true;
                    this.pnlTramiteJudicial.Visible = true;
                    this.DesBlockjudicial();
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }


    }

    /// <summary>
    /// Evento Seleccion rechazdo
    /// </summary>
    protected void RadioButtonRechazado_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.RadioButtonRechazado.Focus();
        if (this.RadioButtonRechazado.SelectedItem.Value.Equals("CheckAceptado"))
        {
            this.pnlTramiteNotarialAceptado.Enabled = true;
            this.pnlTramiteNotarialAceptado.Visible = true;
            //this.rblDesicionDelAuto.SelectedValue = "CheckAdmitida";
            this.CamposJudicial.Visible = false;
        }
        else if (this.RadioButtonRechazado.SelectedItem.Value.Equals("CheckRechazado"))
        {
            this.CamposJudicial.Visible = true;
            this.pnlTramiteNotarialAceptado.Visible = false;
        }

    }

    /// <summary>
    /// RblConsultaPor_SelectedIndexChanged
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected void RblConsultaPor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.rblConsultaPor.Focus();
            switch (this.rblConsultaPor.SelectedItem.Value.ToString())
            {
                case "ckNotarial":
                    this.rblDesicionDelAuto.SelectedValue = null;
                    this.TNotarial();
                    this.LimpiarCamposJudicial();
                    if (this.RadioButtonRechazado.SelectedValue == "CheckRechazado" && this.RadioButtonJudiTer.SelectedValue == "CheckJudicial")
                    {
                        this.RadioButtonRechazado.SelectedValue = null;
                        this.RadioButtonJudiTer.SelectedValue = null;
                        this.CamposJudicial.Visible = false;
                    }
                    break;
                case "ckJudicial":
                    this.DesBlockjudicial();
                    this.LimpiarCamposNotarial();
                    this.TJudicial();
                    if (this.RadioButtonRechazado.SelectedValue == "CheckRechazado" && this.RadioButtonJudiTer.SelectedValue == "CheckJudicial")
                    {
                        this.RadioButtonRechazado.SelectedValue = null;
                        this.RadioButtonJudiTer.SelectedValue = null;
                        this.CamposJudicial.Visible = false;
                    }
                    break;
                default:
                    this.pnlTramite.Visible = true;
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Evento Seleccion Desicion del Auto
    /// </summary>
    protected void rblDesicionDelAuto_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.lblDesAuto.Visible = false;
        this.rblDesicionDelAuto.Focus();
        if (this.rblDesicionDelAuto.SelectedItem.Value.Equals("CheckInadmitida"))
        {
            this.CamposSubsanar.Visible = true;
            this.pnlInter.Visible = false;
        }
        else if (this.rblDesicionDelAuto.SelectedItem.Value.Equals("CheckRechazada"))
        {
            this.CamposSubsanar.Visible = false;
            this.pnlInter.Visible = true;
            this.pnlDesicionDelRecurso.Visible = true;
            this.lblDesicionDelRecurso.Visible = true;
        }
        else
        {
            this.CamposSubsanar.Visible = false;
            this.pnlInter.Visible = false;
        }
    }


    protected void rbtEstadoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ObliFeRe.Visible = true;
        this.ObliNoAr.Visible = true;
        this.ObliObDoRe.Visible = true;
    }



    #endregion

    #region METODOS

    /// <summary>
    /// Método void ActualizarHistorico
    /// </summary>
    private void ActualizarHistorico(int fase, int accion, int actuacion)
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        RegistroDenuncia vRegistroDenuncia = vMostrencosService.ConsultarDenunciaBienXId(vIdDenunciaBien);
        HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien = new HistoricoEstadosDenunciaBien();
        pHistoricoEstadosDenunciaBien.IdDenunciaBien = vIdDenunciaBien;
        pHistoricoEstadosDenunciaBien.IdEstadoDenuncia = 15;
        pHistoricoEstadosDenunciaBien.IdFase = fase; // 18; 
        pHistoricoEstadosDenunciaBien.IdAccion = accion; // 37; 
        pHistoricoEstadosDenunciaBien.IdActuacion = actuacion;// 25; 
        pHistoricoEstadosDenunciaBien.IdUsuarioCrea = GetSessionUser().IdUsuario;
        pHistoricoEstadosDenunciaBien.Fase = 1.ToString();
        pHistoricoEstadosDenunciaBien.Accion = 2.ToString();
        pHistoricoEstadosDenunciaBien.UsuarioCrea = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaCrea = DateTime.Now;
        pHistoricoEstadosDenunciaBien.UsuarioModifica = GetSessionUser().NombreUsuario.Trim().ToUpper();
        pHistoricoEstadosDenunciaBien.FechaModifica = DateTime.Now;
        this.InformacionAudioria(pHistoricoEstadosDenunciaBien, this.PageName, vSolutionPage);
        int IdHistoricoDocumentosSolicitados = this.vMostrencosService.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
    }

    private void CargarDespachos()
    {
        List<DespachoJudicial> vListaDespachoJudicial = this.vMostrencosService.ConsultarDespachoJudicialTodos().OrderBy(d => d.Nombre).ThenBy(d => d.Departamento.NombreDepartamento)
            .ThenBy(d => d.Municipio.NombreMunicipio).ToList();

        vListaDespachoJudicial.ForEach(p =>
        {
            p.Nombre = p.Nombre.ToUpperInvariant();
            p.Departamento.NombreDepartamento = p.Departamento.NombreDepartamento.ToUpperInvariant();
            p.Municipio.NombreMunicipio = p.Municipio.NombreMunicipio.ToUpperInvariant();
        });

        for (int i = 0; i < vListaDespachoJudicial.Count; i++)
        {
            this.DropNombDespacho.Items.Add(new ListItem
            {
                Text = string.Concat(
                    vListaDespachoJudicial[i].Nombre, " ",
                    vListaDespachoJudicial[i].Departamento.NombreDepartamento, " ",
                    vListaDespachoJudicial[i].Municipio.NombreMunicipio),
                Value = vListaDespachoJudicial[i].IdDespachoJudicial.ToString()
            });
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    public void CargarGrillas()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
        vListaDocumentos = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);

        //vListaDocumentos.ForEach(p => 
        //{   p.NombreTipoDocumento = p.NombreTipoDocumento == null ? "" : p.NombreTipoDocumento.ToUpperInvariant();
        //    p.NombreEstado = p.NombreEstado == null ? "" : p.NombreEstado.ToUpperInvariant();
        //    p.ObservacionesDocumentacionSolicitada = p.ObservacionesDocumentacionSolicitada == null ? "" : p.ObservacionesDocumentacionSolicitada.ToUpperInvariant();
        //    p.ObservacionesDocumentacionRecibida = p.ObservacionesDocumentacionRecibida == null ? "" : p.ObservacionesDocumentacionRecibida.ToUpperInvariant();
        //});

        ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentos;
        this.gvwDocumentacionRecibida.DataSource = vListaDocumentos;
        this.gvwDocumentacionRecibida.DataBind();
    }

    private void BlockJudicial()
    {
        this.FechaRadicaJudicial.Enabled = false;
        this.rfvRadicacion.Enabled = false;
        this.DropNombDespacho.Enabled = false;
        this.rfvDespacho.Enabled = false;
        this.TextDepaDespacho.Enabled = false;
        this.TextMunJuzgado.Enabled = false;
        this.TextSentencia.Enabled = false;
        this.rfvSentencia.Enabled = false;
        this.FechaDecisionAuto.Enabled = false;
        this.rfvFechaDesicion.Enabled = false;
        this.rblTipoDeProceso.Enabled = false;
        this.rfvDesicion.Enabled = false;
        this.rblSubSanar.Enabled = false;
        this.rblDesicionDelRecurso.Enabled = false;
        this.RadioButtonList2.Enabled = false;
        this.rblDesicionDelAuto.Items[0].Enabled = false;
        this.rblDesicionDelAuto.Items[1].Enabled = false;
        this.rblDesicionDelAuto.Items[2].Enabled = false;

    }

    private void BlockNotarial()
    {
        this.DropNotaria.Enabled = false;
        this.rfvDropNomina.Enabled = false;
        this.FechaActoTramite.Enabled = false;
        //this.rfvFechaActaTramite.Enabled = false;
        this.FechaEdictoEmpl.Enabled = false;
        this.FechaPubliEdicto.Enabled = false;
        this.FechaComHacienda.Enabled = false;
        this.FechaComDian.Enabled = false;
        this.FechaSolemnizacion.Enabled = false;
        this.CheckTerminacionAnormal.Enabled = false;
        this.RadioButtonRechazado.Enabled = false;
        //this.rfvCheckDecisionTramite.Enabled = false;
        this.RadioButtonJudiTer.Enabled = false;
    }

    private void DesBlockjudicial()
    {
        this.FechaRadicaJudicial.Enabled = true;
        this.rfvRadicacion.Enabled = true;
        this.DropNombDespacho.Enabled = true;
        this.rfvDespacho.Enabled = true;
        this.TextDepaDespacho.Enabled = false;
        this.TextMunJuzgado.Enabled = false;
        this.TextSentencia.Enabled = true;
        this.rfvSentencia.Enabled = true;
        this.FechaDecisionAuto.Enabled = true;
        this.rfvFechaDesicion.Enabled = true;
        this.rblDesicionDelAuto.Items[0].Enabled = true;
        this.rblDesicionDelAuto.Items[1].Enabled = true;
        this.rblDesicionDelAuto.Items[2].Enabled = true;
        if (this.rblDesicionDelAuto.SelectedItem != null)
        {
            if (this.rblDesicionDelAuto.SelectedItem.Value.Equals("CheckRechazada"))
            {
                this.pnlInter.Visible = true;
            }
        }

        this.rfvDesicion.Enabled = true;
        this.rblSubSanar.Enabled = true;
        this.rblDesicionDelRecurso.Enabled = true;
        this.RadioButtonList2.Enabled = true;
    }

    private void DesBlockNotarial()
    {
        this.DropNotaria.Enabled = true;
        this.rfvDropNomina.Enabled = true;
        this.FechaActoTramite.Enabled = true;
        //this.rfvFechaActaTramite.Enabled = true;
        this.FechaEdictoEmpl.Enabled = true;
        this.FechaPubliEdicto.Enabled = true;
        this.FechaComHacienda.Enabled = true;
        this.FechaComDian.Enabled = true;
        this.FechaSolemnizacion.Enabled = true;
        this.CheckTerminacionAnormal.Enabled = true;
        this.RadioButtonRechazado.Enabled = true;
        //this.rfvCheckDecisionTramite.Enabled = true;
        this.RadioButtonJudiTer.Enabled = true;
    }

    private DespachoJudicial DespachoporID(int IdDespacho)
    {
        DespachoJudicial despachoJudicial = this.vMostrencosService.ConsultarDespachoJudicialPorId(IdDespacho);
        return despachoJudicial;
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    private SortDirection direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }

            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Gets or sets de la propiedad para el Ordenamiento de la Grilla.
    /// </summary>
    /// <value>The direction</value>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }
        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Cargars the grillas.
    /// </summary>
    private void CargarGrilla()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                int IdCalidadDenunciante = 10;
                vListaDocumentosSolicitados = this.vMostrencosService.ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(IdCalidadDenunciante);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }

            if (vListaDocumentosSolicitados.Count > 0)
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                vListaDocumentos = vListaDocumentosSolicitados.Where(x => x.IdEstadoDocumento != 6).ToList();
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.TipoDocumentoBienDenunciado.NombreTipoDocumento).ToList();
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentos;
                this.gvwDocumentacionRecibida.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Carga la grilla de la documentacion
    /// </summary>
    private void CargarGrillaDocumentos()
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = new List<DocumentosSolicitadosDenunciaBien>();
            if (this.ViewState["SortedgvwDocumentacionRecibida"] == null)
            {
                vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));

                vListaDocumentosSolicitados = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                vListaDocumentosSolicitados = vListaDocumentosSolicitados.Where(p => p.EstadoDocumento.NombreEstadoDocumento.ToUpper() != "ELIMINADO").ToList();
            }
            else
            {
                vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            }
            vListaDocumentosSolicitados = vListaDocumentosSolicitados.OrderBy(p => p.NombreTipoDocumento).ToList();
            if (vListaDocumentosSolicitados.Count > 0)
            {
                this.pnlDocumentacionSolicitada.Visible = true;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados == null ? new List<DocumentosSolicitadosDenunciaBien>() : vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataSource = vListaDocumentosSolicitados;
                this.gvwDocumentacionRecibida.DataBind();
            }
            else
            {
                this.pnlDocumentacionSolicitada.Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    public void CargarGrillaDocumentosSolicitadosYRecibidos()
    {
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        var Data = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
        ViewState["SortedgvwDocumentacionRecibida"] = Data;
        gvwDocumentacionRecibida.DataSource = Data;
        gvwDocumentacionRecibida.DataBind();

    }

    /// <summary>
    /// carga la informacion de la grilla historico
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    private void CargarGrillaHistorico(int pIdDeninciaBien)
    {
        try
        {
            List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDeninciaBien);
            foreach (HistoricoEstadosDenunciaBien item in vHistorico)
            {
                Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                item.Responsable = vUsuario.PrimerNombre;
                if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                {
                    item.Responsable += " " + vUsuario.SegundoNombre;
                }

                item.Responsable += " " + vUsuario.PrimerApellido;
                if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                {
                    item.Responsable += " " + vUsuario.SegundoApellido;
                }
            }
            this.ViewState["SortedgvwHistoricoDenuncia"] = vHistorico;
            this.tbHistoria.DataSource = vHistorico;
            this.tbHistoria.DataBind();
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    private void CargarNotarias()
    {

        List<Notarias> vNotarias = this.vMostrencosService.ConsultarNotarias().OrderBy(n => n.NombreNotaria).ThenBy(n => n.DepartamentoNotaria).ThenBy(n => n.CiudadNotaria).ToList();
        //List<Notarias> vNotarias = ConsultarNotarias().OrderBy(n => n.NombreNotaria).ThenBy(n => n.DepartamentoNotaria).ThenBy(n => n.CiudadNotaria).ToList();
        vNotarias.ForEach(p => { p.NombreNotaria = p.NombreNotaria.ToUpperInvariant(); });
        for (int i = 0; i < vNotarias.Count; i++)
        {
            this.DropNotaria.Items.Add(new ListItem
            {
                Text = string.Concat(
                    vNotarias[i].NombreNotaria, " ",
                    vNotarias[i].DepartamentoNotaria, " ",
                    vNotarias[i].CiudadNotaria),
                Value = vNotarias[i].CodNotaria
            });
        }

    }

    public List<Notarias> ConsultarNotarias()
    {
        IConsultaNotariasBLL iConsultaNotariasBLL = new Icbf.SIA.Business.ConsultaNotariasBLL();
        Notarias objNotarias = new Notarias();
        List<Notarias> listNotarias = new List<Notarias>();

        List<Notaria> listNotaria = iConsultaNotariasBLL.GetNotarias("");

        foreach (Notaria item in listNotaria)
        {
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            objNotarias.DepartamentoNotaria = item.DepartamentoNotaria;
            listNotarias.Add(objNotarias);
        }
        return listNotarias;
    }


    private void cargarTipoDocumentos()
    {
        try
        {
            List<TipoDocumentoBienDenunciado> vlstTipoDocumentosBien = null;
            vlstTipoDocumentosBien = vMostrencosService.ConsultarTipoDocumentoBienDenunciados();
            vlstTipoDocumentosBien = vlstTipoDocumentosBien.OrderBy(p => p.NombreTipoDocumento).ToList();
            vlstTipoDocumentosBien.ForEach(p => { p.NombreTipoDocumento = p.NombreTipoDocumento.ToUpperInvariant(); });

            this.ddlTipoDocumento.DataSource = vlstTipoDocumentosBien;
            this.ddlTipoDocumento.DataTextField = "NombreTipoDocumento";
            this.ddlTipoDocumento.DataValueField = "IdTipoDocumentoBienDenunciado";
            this.ddlTipoDocumento.DataBind();
            this.ddlTipoDocumento.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
            this.ddlTipoDocumento.SelectedValue = "-1";
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Control del panel documentación recibida, botones add y aplicar
    /// </summary>
    private void ControlPanelDocRecibida(bool activar)
    {
        this.btnAplicar.Visible = activar;
        this.rbtEstadoDocumento.Enabled = activar;
        this.FechaRecibido.Enabled = activar;
        this.fulArchivoRecibido.Enabled = activar;
        this.txtObservacionesRecibido.Enabled = activar;
    }

    /// <summary>
    /// Control de panel documentación solicitada
    /// </summary>
    /// <param name="activa">Estado del campo</param>
    private void ControlPanelDocSolicitada(bool activa)
    {
        this.btnAdd.Visible = false;
        this.txtObservacionesSolicitado.Enabled = activa;
        this.FechaSolicitud.Enabled = activa;
        this.ddlTipoDocumento.Enabled = activa;
    }

    /// <summary>
    /// Guardar Datos en opción "Nuevo"
    /// </summary>
    private void guardar()
    {
        switch (this.rblConsultaPor.SelectedItem.Value.ToString())
        {
            case "ckNotarial":
                bool bandera = true;

                if (this.RadioButtonRechazado.SelectedValue == "CheckAceptado")
                {
                    try
                    {
                        if (this.FechaEdictoEmpl.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            bandera = false;
                        }
                        if (this.FechaPubliEdicto.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            bandera = false;
                        }
                        if (this.FechaComHacienda.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            bandera = false;
                        }
                        if (this.FechaComDian.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            bandera = false;
                        }
                        if (this.FechaSolemnizacion.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            bandera = false;
                        }
                    }
                    catch (Exception)
                    {
                        bandera = false;
                    }
                }
                if (bandera)
                {
                    int guardadoTipoTramite = this.vMostrencosService.InsertarTipoTramite(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2);
                    if (guardadoTipoTramite > 0)
                    {
                        this.InsertarTramiteNotarial();
                        if (this.RadioButtonRechazado.SelectedItem != null && this.RadioButtonJudiTer.SelectedItem != null)
                        {
                            if (this.RadioButtonRechazado.SelectedItem.Value.Equals("CheckRechazado") && this.RadioButtonJudiTer.SelectedItem.Value.Equals("CheckJudicial"))
                            {
                                this.SetSessionParameter("DocGuardado", "1");
                                this.InsertarTramiteJudicial();
                            }
                        }
                    }
                    else
                    {
                        this.toolBar.MostrarMensajeError("Error Al Guardar un Trámite Notarial");
                    }
                }

                break;
            case "ckJudicial":
                int guardadoTipoTramiteJ = this.vMostrencosService.InsertarTipoTramite(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1);
                if (guardadoTipoTramiteJ > 0)
                {
                    this.InsertarTramiteJudicial();
                }
                else
                {
                    this.toolBar.MostrarMensajeError("Error Al Guardar Trámite judicial");
                }
                break;
        }
    }

    /// <summary>
    /// Guarda archivos fisico en el servidor
    /// </summary>
    /// <param name="pIdDeninciaBien"></param>
    /// <param name="pPosition"></param>
    private bool GuardarArchivos(int? pIdDenunciaBien, int pPosition)
    {
        bool Esresultado = false;
        try
        {
            HttpFileCollection files = Request.Files;
            if (files.Count > 0)
            {
                string extFile = Path.GetExtension(this.fulArchivoRecibido.FileName).ToLower();
                string extPdf = ".PDF".ToLower();
                string extJgp = ".JPG".ToLower();
                if (this.fulArchivoRecibido.FileName.Length > 50)
                {
                    throw new Exception("La longitud del nombre del archivo supera los 50 caracteres permitidos");
                }
                int tamano = this.fulArchivoRecibido.FileName.Length;
                if (extFile != extPdf && extFile != extJgp)
                {
                    throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF o JPG");
                }

                int vtamArchivo = (this.fulArchivoRecibido.FileBytes.Length / 1024);
                if (vtamArchivo > 4096)
                {
                    throw new Exception("El tamaño máximo del archivo es 4 MB");
                }

                HttpPostedFile file = files[pPosition];
                if (file.ContentLength > 0)
                {
                    string rutaParcial = string.Empty;
                    string carpetaBase = string.Empty;
                    string filePath = string.Empty;
                    if (extFile.Equals(".pdf"))
                    {
                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".pdf");

                    }
                    else if (extFile.ToLower().Equals(".jpg"))
                    {

                        rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                        carpetaBase = string.Format("{0}{1}\\", Server.MapPath("~/Page/Mostrencos/RegistrarDocumentosBienDenunciado/Archivos/"), pIdDenunciaBien);
                        filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + ".jpg");
                    }
                    bool EsexisteCarpeta = System.IO.Directory.Exists(carpetaBase);
                    if (!EsexisteCarpeta)
                    {
                        System.IO.Directory.CreateDirectory(carpetaBase);
                        Esresultado = true;
                    }
                    this.vFileName = rutaParcial;
                    file.SaveAs(filePath);
                    Esresultado = true;
                }
            }
        }
        catch (HttpRequestValidationException httpex)
        {
            throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return Esresultado;
    }

    private void InsertarTramiteJudicial()
    {
        if (this.ValidarDuplicidadjudicial(Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"))))
        {
            string tipoProceso = string.Empty;
            if (rblTipoDeProceso.SelectedValue != null)
            {
                tipoProceso = rblTipoDeProceso.SelectedValue;
            }
            TramiteJudicial vTramiteJudicial = new TramiteJudicial();
            vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
            vTramiteJudicial.IdDespachoJudicial = Convert.ToInt32(this.DropNombDespacho.SelectedValue.ToString());
            vTramiteJudicial.TipoTramite = "JUDICIAL";
            vTramiteJudicial.TipoProceso = tipoProceso;
            vTramiteJudicial.FechaRadicacion = this.FechaRadicaJudicial.Date;
            vTramiteJudicial.DespachoJudicialAsignado = this.DropNombDespacho.SelectedItem.ToString();
            vTramiteJudicial.FechaSentencia = this.FechaDecisionAuto.Date;
            vTramiteJudicial.Sentencia = this.TextSentencia.Text;
            vTramiteJudicial.UsuarioCrea = this.GetSessionUser().NombreUsuario;
            this.InformacionAudioria(vTramiteJudicial, this.PageName, vSolutionPage);

            if (this.vMostrencosService.InsertarTramiteJudicial(vTramiteJudicial) > 0)
            {
                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                string bandera = (!this.GetSessionParameter("DocGuardado").Equals(string.Empty) ? Convert.ToString(this.GetSessionParameter("DocGuardado")) : "0");
                if (bandera.Equals("0"))
                {
                    vListaDocumentos.ForEach(item =>
                    {
                        item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
                        this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);

                        if (item.EsNuevo == true)
                        {
                            if (item.UsuarioCrea == null)
                            {
                                item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                            }

                            if (item.NombreArchivo != null && item.RutaArchivo != null)
                            {
                                this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                            }
                            else
                                this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                        }
                        else
                        {
                            this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
                        }
                    });
                }

                this.SetSessionParameter("DocGuardado", null);

                if (this.rblDesicionDelAuto.SelectedValue == "CheckAdmitida")
                {
                    this.vMostrencosService.InsertarDesicionAuto(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1);
                }
                else if (this.rblDesicionDelAuto.SelectedValue == "CheckInadmitida")
                {
                    this.vMostrencosService.InsertarDesicionAuto(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2);
                    if (this.rblSubSanar.SelectedValue == "CheckSubsanar")
                    {
                        this.vMostrencosService.InsertarDesicionAutoInadmitida(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1);
                    }
                    else if (this.rblSubSanar.SelectedValue == "CheckNoSubsanar")
                    {
                        this.vMostrencosService.InsertarDesicionAutoInadmitida(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2);
                    }
                }
                else if (this.rblDesicionDelAuto.SelectedValue == "CheckRechazada")
                {
                    this.vMostrencosService.InsertarDesicionAuto(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 3);
                }

                if (this.rblDesicionDelRecurso.SelectedValue == "CheckSi")
                {
                    if (this.RadioButtonList2.SelectedValue == "CheckAceptada")
                    {
                        this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1, true);
                    }
                    else if (this.RadioButtonList2.SelectedValue == "CheckDecisionRechazada")
                    {
                        this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2, true);
                    }
                }
                else
                {
                    if (this.RadioButtonList2.SelectedValue == "CheckAceptada")
                    {
                        this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1, false);
                    }
                    else if (this.RadioButtonList2.SelectedValue == "CheckDecisionRechazada")
                    {
                        this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2, false);
                    }
                }

                this.btnInfoDenuncia.Enabled = false;
                this.pnlPopUpHistorico.Visible = false;
                this.rblConsultaPor.Enabled = false;
                this.gvwDocumentacionRecibida.Enabled = false;
                this.pnlDocumentacionRecibida.Enabled = false;
                this.pnlDocumentacionSolicitada.Enabled = false;
                this.FechaSolicitud.Enabled = false;
                this.FechaSolicitud.EnableViewState = false;
                this.pnlGridDocumentacionRecibida.Enabled = false;

                this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                this.BlockJudicial();
                this.BlockNotarial();
                this.ActualizarHistorico(18, 36, 24);
                this.toolBar.OcultarBotonNuevo(true);
                this.toolBar.OcultarBotonGuardar(false);
                this.toolBar.MostrarBotonEditar(true);
            }
            else
            {
                this.toolBar.MostrarMensajeGuardado("Error Al Guardar Trámite judicial");
            }


        }
        else
        {
            this.toolBar.MostrarMensajeError("El registro ya existe");
        }

    }

    private void InsertarTramiteNotarial()
    {

        if (this.ValidarDuplicidad(Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), this.DropNotaria.SelectedValue.ToString()))
        {
            string[] DatosNotaria = this.DropNotaria.SelectedItem.ToString().Split(' ');
            TramiteNotarial vTramiteNotarial = new TramiteNotarial();
            bool bandera = true;
            if (this.RadioButtonRechazado.SelectedValue == "CheckRechazado")
            {
                vTramiteNotarial.DecisionTramiteNotarial = "RECHAZADO";
                if (this.RadioButtonJudiTer.SelectedValue == "CheckJudicial")
                {
                    vTramiteNotarial.DecisionTramiteNotarialRechazado = "JUDICIAL";
                }
                else
                {
                    vTramiteNotarial.DecisionTramiteNotarialRechazado = "TERMINADO";
                }

            }
            else
            {
                vTramiteNotarial.DecisionTramiteNotarial = "ACEPTADO";
            }
            vTramiteNotarial.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
            vTramiteNotarial.TipoTramite = "NOTARIAL";
            vTramiteNotarial.IdNotaria = this.DropNotaria.SelectedValue.ToString();
            vTramiteNotarial.Notaria = DatosNotaria[0];
            vTramiteNotarial.FechaActaTramiteNotarial = this.FechaActoTramite.Date;
            vTramiteNotarial.FechaIngresoICBF = System.DateTime.Now;
            vTramiteNotarial.FechaInstrumentosPublicos = System.DateTime.Now;
            vTramiteNotarial.UsuarioCrea = this.GetSessionUser().NombreUsuario;
            vTramiteNotarial.FechaCrea = System.DateTime.Now;
            this.InformacionAudioria(vTramiteNotarial, this.PageName, vSolutionPage);

            int guardado = this.vMostrencosService.InsertarTramiteNotarial(vTramiteNotarial);
            if (guardado > 0)
            {
                if (this.FechaEdictoEmpl.Date == Convert.ToDateTime("1/01/1900"))
                {
                    bandera = false;
                }
                if (this.FechaPubliEdicto.Date == Convert.ToDateTime("1/01/1900"))
                {
                    bandera = false;
                }
                if (this.FechaComHacienda.Date == Convert.ToDateTime("1/01/1900"))
                {
                    bandera = false;
                }
                if (this.FechaComDian.Date == Convert.ToDateTime("1/01/1900"))
                {
                    bandera = false;
                }
                if (this.FechaSolemnizacion.Date == Convert.ToDateTime("1/01/1900"))
                {
                    bandera = false;
                }
                if (bandera)
                {
                    if (this.RadioButtonRechazado.SelectedValue == "CheckAceptado")
                    {
                        TramiteNotarialAceptado tramiteNotarialAceptado = new TramiteNotarialAceptado();
                        tramiteNotarialAceptado = this.llenarTramiteNotarialAceptado();
                        tramiteNotarialAceptado.IdTramiteNotarial = guardado;
                        this.InformacionAudioria(tramiteNotarialAceptado, this.PageName, vSolutionPage);
                        int guardadoTramite = this.vMostrencosService.InsertarTramiteNotarialAceptado(tramiteNotarialAceptado);
                    }
                }



                List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
                vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];

                vListaDocumentos.ForEach(item =>
                {
                    item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
                    this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);

                    if (item.EsNuevo == true)
                    {
                        if (item.UsuarioCrea == null)
                        {
                            item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                        }

                        if (item.NombreArchivo != null && item.RutaArchivo != null)
                        {
                            this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                        }
                        else
                            this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                    }
                    else
                    {
                        this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
                    }

                });

                this.btnInfoDenuncia.Enabled = false;
                this.pnlPopUpHistorico.Visible = false;
                this.rblConsultaPor.Enabled = false;

                this.tbHistoria.Enabled = false;
                this.pnlDocumentacionRecibida.Enabled = false;
                this.gvwDocumentacionRecibida.Enabled = false;
                this.pnlDocumentacionSolicitada.Enabled = false;
                this.pnlGridDocumentacionRecibida.Enabled = false;

                this.FechaSolicitud.Enabled = false;
                this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
                this.BlockJudicial();
                this.BlockNotarial();
                this.ActualizarHistorico(18, 37, 25);

                this.toolBar.OcultarBotonGuardar(false);
                this.toolBar.OcultarBotonNuevo(true);
                this.toolBar.MostrarBotonEditar(true);

            }
            else
            {
                this.toolBar.MostrarMensajeGuardado("Error Al Guardar un Trámite Notarial");
            }
        }
        else
        {
            this.toolBar.MostrarMensajeError("El registro ya existe");
        }
    }

    private TramiteNotarialAceptado llenarTramiteNotarialAceptado()
    {

        TramiteNotarialAceptado tramiteNotarialAceptado = new TramiteNotarialAceptado();
        tramiteNotarialAceptado.FechaEdictoEmplazatorio = this.FechaEdictoEmpl.Date;
        tramiteNotarialAceptado.FechaPublicacionEdicto = this.FechaPubliEdicto.Date;
        tramiteNotarialAceptado.FechaComunicacionScretariaHacienda = this.FechaComHacienda.Date;
        tramiteNotarialAceptado.FechaComunicacionDian = this.FechaComDian.Date;
        tramiteNotarialAceptado.FechaSolemnizacion = this.FechaSolemnizacion.Date;
        tramiteNotarialAceptado.IdTerminacionAnormalTramiteNotarial = 1;
        tramiteNotarialAceptado.TerminacionAnormalTramiteNotarial = (this.CheckTerminacionAnormal.Checked) ? "1" : "1";
        tramiteNotarialAceptado.UsuarioCrea = this.GetSessionUser().NombreUsuario;
        return tramiteNotarialAceptado;
    }

    /// <summary>
    /// Limpiar los campos del panel notarial
    /// </summary>
    public void LimpiarCamposNotarial()
    {
        this.DropNotaria.SelectedValue = "-1";
        this.TextDepaNotaria.Text = string.Empty;
        this.TextMunicipioNotaria.Text = string.Empty;
        this.RadioButtonRechazado.SelectedValue = null;
        this.RadioButtonJudiTer.SelectedValue = null;
        this.FechaActoTramite.InitNull = true;
        this.CamposJudicial.Visible = false;
        this.FechaEdictoEmpl.InitNull = true;
        this.FechaPubliEdicto.InitNull = true;
        this.FechaComHacienda.InitNull = true;
        this.FechaComDian.InitNull = true;
        this.FechaSolemnizacion.InitNull = true;
        this.CheckTerminacionAnormal.Checked = false;
        this.pnlTramiteNotarialAceptado.Visible = false;
    }

    /// <summary>
    /// Limpiar los campos del panel Judicial
    /// </summary>
    public void LimpiarCamposJudicial()
    {
        this.FechaRadicaJudicial.InitNull = true;
        this.DropNombDespacho.SelectedValue = "-1";
        this.TextDepaDespacho.Text = "";
        this.TextMunJuzgado.Text = "";
        this.TextSentencia.Text = "";
        this.FechaDecisionAuto.InitNull = true;
        this.CamposSubsanar.Visible = false;
        this.rblDesicionDelAuto.SelectedValue = null;
        this.rblSubSanar.SelectedValue = null;
        this.pnlDesicionDelRecurso.Visible = false;
        this.lblDesicionDelRecurso.Visible = false;
        this.rblDesicionDelRecurso.SelectedValue = null;
        this.RadioButtonList2.SelectedValue = null;
    }

    void ObtenerTipoTramite(int IdDenunciaBien)
    {
        DenunciaBien vDenunciaBien = this.vMostrencosService.ConsultarDatosDenunciaBienDecision(this.vMostrencosService.ConsultaCompletaDenunciaBienPorId(IdDenunciaBien));
        TramiteJudicial tramiteJudicial = new TramiteJudicial();
        TramiteNotarial tramiteNotarial = new TramiteNotarial();
        TramiteNotarialAceptado tramiteNotarialAceptado = new TramiteNotarialAceptado();
        DesicionAuto desicionAuto = new DesicionAuto();
        DecisionAutoInadmitida decisionAutoInadmitida = new DecisionAutoInadmitida();
        DecisionRecurso decisionRecurso = new DecisionRecurso();
        tramiteJudicial = this.vMostrencosService.ConsultaTramiteJudicial(IdDenunciaBien);
        tramiteNotarial = this.vMostrencosService.ConsultarTramiteNotarial(IdDenunciaBien);

        if (tramiteNotarial.IdTramiteNotarial > 0)
        {

            this.SetSessionParameter("TipoTramiteIdTramiteNotarial", tramiteNotarial.IdTramiteNotarial);
            this.pnlTramiteNotarial.Visible = true;

            if (tramiteNotarial.IdNotaria != "-1")
            {
                Notarias vNotarias = this.vMostrencosService.ConsultarNotarias().Find(x => x.CodNotaria == tramiteNotarial.IdNotaria);
                //Notarias vNotarias = ConsultarNotarias().Find(x => x.CodNotaria == tramiteNotarial.IdNotaria);
                tramiteNotarialAceptado = this.vMostrencosService.ConsultarTramiteNotarialAceptado(tramiteNotarial.IdTramiteNotarial);
                this.DropNotaria.SelectedValue = tramiteNotarial.IdNotaria;
                this.TextDepaNotaria.Text = vNotarias.DepartamentoNotaria;
                this.TextMunicipioNotaria.Text = vNotarias.CiudadNotaria;
                this.FechaActoTramite.Date = tramiteNotarial.FechaActaTramiteNotarial;
                if (tramiteNotarial.DecisionTramiteNotarial.Length > 0)
                {
                    if (tramiteNotarial.DecisionTramiteNotarial.Equals("RECHAZADO"))
                    {
                        this.RadioButtonRechazado.SelectedValue = "CheckRechazado";
                        this.CamposJudicial.Visible = true;

                        if (tramiteNotarial.DecisionTramiteNotarialRechazado.Equals("JUDICIAL"))
                        {
                            this.RadioButtonJudiTer.SelectedValue = "CheckJudicial";
                            //SE AGREGA LA PARTE JUDICIAL
                            this.DesBlockjudicial();
                            //DespachoJudicial despacho = new DespachoJudicial();
                            //this.SetSessionParameter("TipoTramiteIdTramiteJudicial", tramiteJudicial.IdTramiteJudicial);
                            //if (tramiteJudicial.IdDespachoJudicial != -1)
                            //{
                            //    despacho = this.DespachoporID(tramiteJudicial.IdDespachoJudicial);
                            //    this.DropNombDespacho.Text = tramiteJudicial.DespachoJudicialAsignado;
                            //    this.DropNombDespacho.SelectedValue = tramiteJudicial.IdDespachoJudicial.ToString();
                            //    this.TextDepaDespacho.Text = despacho.Nombre;
                            //    this.TextMunJuzgado.Text = despacho.Municipio.NombreMunicipio;
                            //}
                            //this.TextSentencia.Text = tramiteJudicial.Sentencia;
                            //this.FechaDecisionAuto.Date = tramiteJudicial.FechaSentencia;
                            //this.FechaRadicaJudicial.Date = tramiteJudicial.FechaRadicacion;

                            if (vDenunciaBien.desicionAuto.NombreDecisionAuto != null)
                            {
                                if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Admitida"))
                                {
                                    this.rblDesicionDelAuto.SelectedValue = "CheckAdmitida";
                                }
                                else if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Inadmitida"))
                                {
                                    this.rblDesicionDelAuto.SelectedValue = "CheckInadmitida";
                                    if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Length > 0)
                                    {
                                        this.CamposSubsanar.Visible = true;
                                        if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Equals("Subsanar"))
                                        {
                                            this.rblSubSanar.SelectedValue = "CheckSubsanar";
                                        }
                                        else if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Equals("No Subsanar"))
                                        {
                                            this.rblSubSanar.SelectedValue = "CheckNoSubsanar";
                                        }
                                    }
                                }
                                else if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Rechazada"))
                                {
                                    this.rblDesicionDelAuto.SelectedValue = "CheckRechazada";
                                    if (vDenunciaBien.decisionRecurso.Nombre.Length > 0)
                                    {
                                        if (vDenunciaBien.decisionRecurso.Nombre.Equals("Aceptada"))
                                        {
                                            this.RadioButtonList2.SelectedValue = "CheckAceptada";
                                        }
                                        else if (vDenunciaBien.decisionRecurso.Nombre.Equals("Rechazada"))
                                        {
                                            this.RadioButtonList2.SelectedValue = "CheckDecisionRechazada";
                                        }
                                    }
                                }
                                if (vDenunciaBien.InterposicionRecurso)
                                {
                                    this.rblDesicionDelRecurso.SelectedValue = "CheckSi";
                                }
                                else
                                {
                                    this.rblDesicionDelRecurso.SelectedValue = "CheckNo";
                                }
                            }
                            this.pnlTramiteJudicial.Visible = true;
                            //FIN
                        }
                        else if (tramiteNotarial.DecisionTramiteNotarialRechazado.Equals("TERMINADO"))
                        {
                            this.RadioButtonJudiTer.SelectedValue = "CheckTerminado";
                        }
                    }
                    else if (tramiteNotarial.DecisionTramiteNotarial.Equals("ACEPTADO"))
                    {
                        this.RadioButtonRechazado.SelectedValue = "CheckAceptado";
                        this.pnlTramiteNotarialAceptado.Visible = true;
                        this.pnlTramiteJudicial.Enabled = false;
                        this.pnlTramiteJudicial.Visible = false;

                        if (tramiteNotarialAceptado.IdTramiteNotarialAceptado > 0)
                        {
                            this.pnlTramiteNotarialAceptado.Visible = true;
                            this.pnlTramiteJudicial.Visible = false;
                            this.FechaEdictoEmpl.Date = tramiteNotarialAceptado.FechaEdictoEmplazatorio;
                            this.FechaPubliEdicto.Date = tramiteNotarialAceptado.FechaPublicacionEdicto;
                            this.FechaComHacienda.Date = tramiteNotarialAceptado.FechaComunicacionScretariaHacienda;
                            this.FechaComDian.Date = tramiteNotarialAceptado.FechaComunicacionDian;
                            this.FechaSolemnizacion.Date = tramiteNotarialAceptado.FechaSolemnizacion;
                            if (tramiteNotarialAceptado.TerminacionAnormalTramiteNotarial != null)
                            {
                                this.CheckTerminacionAnormal.Checked = (tramiteNotarialAceptado.TerminacionAnormalTramiteNotarial.Equals("1")) ? true : false;
                            }

                        }
                    }
                }
            }
        }
        else if (tramiteJudicial.IdTramiteJudicial > 0)
        {
            DespachoJudicial despacho = new DespachoJudicial();
            this.SetSessionParameter("TipoTramiteIdTramiteJudicial", tramiteJudicial.IdTramiteJudicial);
            if (tramiteJudicial.IdDespachoJudicial != -1)
            {
                despacho = this.DespachoporID(tramiteJudicial.IdDespachoJudicial);
                this.DropNombDespacho.Text = tramiteJudicial.DespachoJudicialAsignado;
                this.DropNombDespacho.SelectedValue = tramiteJudicial.IdDespachoJudicial.ToString();
                this.TextDepaDespacho.Text = despacho.Nombre;
                this.TextMunJuzgado.Text = despacho.Municipio.NombreMunicipio;
            }
            this.TextSentencia.Text = tramiteJudicial.Sentencia;
            this.FechaDecisionAuto.Date = tramiteJudicial.FechaSentencia;
            this.FechaRadicaJudicial.Date = tramiteJudicial.FechaRadicacion;

            if (vDenunciaBien.desicionAuto.NombreDecisionAuto != null)
            {
                if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Admitida"))
                {
                    this.rblDesicionDelAuto.SelectedValue = "CheckAdmitida";
                }
                else if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Inadmitida"))
                {
                    this.rblDesicionDelAuto.SelectedValue = "CheckInadmitida";
                    if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Length > 0)
                    {
                        this.CamposSubsanar.Visible = true;
                        if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Equals("Subsanar"))
                        {
                            this.rblSubSanar.SelectedValue = "CheckSubsanar";
                        }
                        else if (vDenunciaBien.decisionAutoInadmitida.NombreDecisionAutoInadmitida.Equals("No Subsanar"))
                        {
                            this.rblSubSanar.SelectedValue = "CheckNoSubsanar";
                        }
                    }
                }
                else if (vDenunciaBien.desicionAuto.NombreDecisionAuto.Equals("Rechazada"))
                {
                    this.rblDesicionDelAuto.SelectedValue = "CheckRechazada";
                    if (vDenunciaBien.decisionRecurso.Nombre.Length > 0)
                    {
                        if (vDenunciaBien.decisionRecurso.Nombre.Equals("Aceptada"))
                        {
                            this.RadioButtonList2.SelectedValue = "CheckAceptada";
                        }
                        else if (vDenunciaBien.decisionRecurso.Nombre.Equals("Rechazada"))
                        {
                            this.RadioButtonList2.SelectedValue = "CheckDecisionRechazada";
                        }
                    }
                }
                if (vDenunciaBien.InterposicionRecurso)
                {
                    this.rblDesicionDelRecurso.SelectedValue = "CheckSi";
                }
                else
                {
                    this.rblDesicionDelRecurso.SelectedValue = "CheckNo";
                }
            }
            this.pnlTramiteJudicial.Visible = true;
        }

    }

    protected void TJudicial()
    {
        this.ReqNotaria.Visible = false;
        this.ReqFechaActa.Visible = false;
        this.ReqDeciTra.Visible = false;
        this.pnlTramiteJudicial.Enabled = true;
        this.pnlTramiteNotarialAceptado.Enabled = false;
        this.pnlTramiteJudicial.Visible = true;
        this.BlockNotarial();
        this.DesBlockjudicial();
    }

    protected void TNotarial()
    {
        this.ReqNotaria.Visible = true;
        this.ReqFechaActa.Visible = true;
        this.ReqDeciTra.Visible = true;
        this.pnlTramiteJudicial.Enabled = false;
        this.rblDesicionDelAuto.SelectedValue = null;
        this.CheckTerminacionAnormal.Enabled = false;
        this.DesBlockNotarial();
        this.BlockJudicial();
    }

    private void UpdateTramiteJudicial(string id)
    {
        string tipoProceso = string.Empty;
        if (rblTipoDeProceso.SelectedValue != null)
        {
            tipoProceso = rblTipoDeProceso.SelectedValue;
        }
        TramiteJudicial vTramiteJudicial = new TramiteJudicial();
        vTramiteJudicial.IdDenunciaBien = vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        vTramiteJudicial.IdTramiteJudicial = Convert.ToInt32(id);
        vTramiteJudicial.IdDespachoJudicial = Convert.ToInt32(this.DropNombDespacho.SelectedValue.ToString());
        vTramiteJudicial.TipoTramite = "JUDICIAL";
        vTramiteJudicial.TipoProceso = tipoProceso;
        vTramiteJudicial.FechaRadicacion = this.FechaRadicaJudicial.Date;
        vTramiteJudicial.DespachoJudicialAsignado = this.DropNombDespacho.SelectedItem.ToString();
        vTramiteJudicial.FechaSentencia = this.FechaDecisionAuto.Date;
        vTramiteJudicial.Sentencia = this.TextSentencia.Text;
        vTramiteJudicial.UsuarioCrea = this.GetSessionUser().NombreUsuario;
        this.InformacionAudioria(vTramiteJudicial, this.PageName, vSolutionPage);
        if (this.vMostrencosService.UpdateTramiteJudicial(vTramiteJudicial) > 0)
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
            vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];

            vListaDocumentos.ForEach(item =>
            {
                item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
                this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                if (item.EsNuevo == true)
                {
                    if (item.UsuarioCrea == null)
                    {
                        item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    }

                    if (item.NombreArchivo != null && item.RutaArchivo != null)
                    {
                        this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                    }
                    else
                        this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                }
                else
                    this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);
            });

            if (this.rblDesicionDelAuto.SelectedValue == "CheckAdmitida")
            {
                this.vMostrencosService.InsertarDesicionAuto(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1);
            }
            else if (this.rblDesicionDelAuto.SelectedValue == "CheckInadmitida")
            {
                this.vMostrencosService.InsertarDesicionAuto(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2);
                if (this.rblSubSanar.SelectedValue == "CheckSubsanar")
                {
                    this.vMostrencosService.InsertarDesicionAutoInadmitida(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1);
                }
                else if (this.rblSubSanar.SelectedValue == "CheckNoSubsanar")
                {
                    this.vMostrencosService.InsertarDesicionAutoInadmitida(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2);
                }
            }
            else if (this.rblDesicionDelAuto.SelectedValue == "CheckRechazada")
            {
                this.vMostrencosService.InsertarDesicionAuto(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 3);
            }

            if (this.rblDesicionDelRecurso.SelectedValue == "CheckSi")
            {
                if (this.RadioButtonList2.SelectedValue == "CheckAceptada")
                {
                    this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1, true);
                }
                else if (this.RadioButtonList2.SelectedValue == "CheckDecisionRechazada")
                {
                    this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2, true);
                }
            }
            else
            {
                if (this.RadioButtonList2.SelectedValue == "CheckAceptada")
                {
                    this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 1, false);
                }
                else if (this.RadioButtonList2.SelectedValue == "CheckDecisionRechazada")
                {
                    this.vMostrencosService.InsertarDesicionRecurso(vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")), 2, false);
                }
            }
            this.pnlDocumentacionSolicitada.Enabled = false;
            this.pnlDocumentacionRecibida.Enabled = false;
            this.gvwDocumentacionRecibida.Enabled = false;
            this.BlockJudicial();
            this.BlockNotarial();
            //this.GuardarDoc(Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien")));
            this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            this.toolBar.OcultarBotonGuardar(false);
            this.toolBar.OcultarBotonNuevo(true);
            this.toolBar.MostrarBotonEditar(true);
        }
        else
        {
            this.toolBar.MostrarMensajeGuardado("Error Al Guardar Trámite judicial");
        }
        vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        //this.GuardarDoc(vIdDenunciaBien);
    }

    private void UpdateTramiteNotarial(string id)
    {
        string[] DatosNotaria = this.DropNotaria.SelectedItem.ToString().Split(' ');
        TramiteNotarial vTramiteNotarial = new TramiteNotarial();

        if (this.RadioButtonRechazado.SelectedValue == "CheckRechazado")
        {
            vTramiteNotarial.DecisionTramiteNotarial = "RECHAZADO";
            if (this.RadioButtonJudiTer.SelectedValue == "CheckJudicial")
            {
                vTramiteNotarial.DecisionTramiteNotarialRechazado = "JUDICIAL";
                this.InsertarTramiteJudicial();
            }
            else
            {
                vTramiteNotarial.DecisionTramiteNotarialRechazado = "TERMINADO";
            }
        }
        else
        {
            vTramiteNotarial.DecisionTramiteNotarial = "ACEPTADO";
            bool bandera = true;
            if (this.RadioButtonRechazado.SelectedValue == "CheckAceptado")
            {
                try
                {
                    if (this.FechaEdictoEmpl.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        bandera = false;
                    }
                    if (this.FechaPubliEdicto.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        bandera = false;
                    }
                    if (this.FechaComHacienda.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        bandera = false;
                    }
                    if (this.FechaComDian.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        bandera = false;
                    }
                    if (this.FechaSolemnizacion.Date == Convert.ToDateTime("1/01/1900"))
                    {
                        bandera = false;
                    }
                }
                catch (Exception)
                {
                    bandera = false;
                }
            }
            if (bandera)
            {
            }
        }
        vTramiteNotarial.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
        vTramiteNotarial.IdTramiteNotarial = Convert.ToInt32(id);
        vTramiteNotarial.TipoTramite = "NOTARIAL";
        vTramiteNotarial.IdNotaria = this.DropNotaria.SelectedValue.ToString();
        vTramiteNotarial.Notaria = DatosNotaria[0];
        vTramiteNotarial.FechaActaTramiteNotarial = this.FechaActoTramite.Date;
        vTramiteNotarial.FechaIngresoICBF = System.DateTime.Now;
        vTramiteNotarial.FechaInstrumentosPublicos = System.DateTime.Now;
        vTramiteNotarial.UsuarioCrea = this.GetSessionUser().NombreUsuario;
        vTramiteNotarial.FechaCrea = System.DateTime.Now;
        this.InformacionAudioria(vTramiteNotarial, this.PageName, vSolutionPage);
        int guardado = this.vMostrencosService.UpdateTramiteNotarial(vTramiteNotarial);
        if (guardado > 0)
        {
            if (this.RadioButtonRechazado.SelectedValue == "CheckAceptado")
            {
                TramiteNotarialAceptado tramiteNotarialAceptado = new TramiteNotarialAceptado();
                tramiteNotarialAceptado = this.llenarTramiteNotarialAceptado();
                tramiteNotarialAceptado.IdTramiteNotarial = guardado;
                this.InformacionAudioria(tramiteNotarialAceptado, this.PageName, vSolutionPage);
                int guardadoTramite = this.vMostrencosService.InsertarTramiteNotarialAceptado(tramiteNotarialAceptado);
            }


            List<DocumentosSolicitadosDenunciaBien> vListaDocumentos = new List<DocumentosSolicitadosDenunciaBien>();
            vListaDocumentos = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];

            vListaDocumentos.ForEach(item =>
            {
                item.IdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
                this.InformacionAudioria(item, this.PageName, SolutionPage.Edit);
                if (item.EsNuevo == true)
                {
                    if (item.UsuarioCrea == null)
                    {
                        item.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                    }
                    if (item.NombreArchivo != null && item.RutaArchivo != null)
                    {
                        this.vMostrencosService.InsertarDocumentosSolicitadosDenunciaBien(item);

                    }
                    else
                        this.vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(item);
                }
                else
                    this.vMostrencosService.ModificarDocumentosSolicitadosDenunciaBienRecibidos(item);

            });

            this.btnInfoDenuncia.Enabled = false;
            this.pnlPopUpHistorico.Visible = false;
            this.rblConsultaPor.Enabled = false;
            this.pnlDocumentacionSolicitada.Enabled = false;
            this.pnlDocumentacionRecibida.Enabled = false;
            this.gvwDocumentacionRecibida.Enabled = false;
            this.FechaSolicitud.Enabled = false;
            this.BlockJudicial();
            this.BlockNotarial();
            this.toolBar.MostrarMensajeGuardado("La información ha sido guardada exitosamente");
            this.toolBar.OcultarBotonGuardar(false);
            this.toolBar.OcultarBotonNuevo(true);
            this.toolBar.MostrarBotonEditar(true);
        }
        else
        {
            this.toolBar.MostrarMensajeGuardado("Error Al Guardar un Trámite Notarial");
        }

    }

    private bool ValidarDuplicidad(int IdDenuncia, string Notaria)
    {
        return this.vMostrencosService.validarTramiteNotarial(IdDenuncia, Notaria);
    }

    private bool ValidarDuplicidadjudicial(int IdDenuncia)
    {
        return this.vMostrencosService.validarTramiteJudicial(IdDenuncia);
    }

    /// <summary>
    /// Validar Relación de Ordenes de Pago
    /// </summary>
    /// <returns>Retorna el resultado de la validación</returns>
    private bool ValidarCamposObligatorios()
    {
        bool vEsrequerido = false;
        this.lblJudicial.Visible = false;
        this.lblDesAuto.Visible = false;
        int count = 0;
        try
        {
            if (this.rblConsultaPor.SelectedItem != null)
            {
                this.lblCampoRequeridoTipoTramite.Visible = false;
                vEsrequerido = false;

                if (this.rblConsultaPor.SelectedItem.Value.Equals("ckNotarial"))
                {
                    if (this.RadioButtonRechazado.SelectedItem != null)
                    {
                        if (this.RadioButtonRechazado.SelectedItem.Value.Equals("CheckRechazado"))
                        {
                            if (this.RadioButtonJudiTer.SelectedItem == null)
                            {
                                this.lblJudicial.Visible = true;
                                vEsrequerido = true;
                                return vEsrequerido;
                            }
                            else
                            {
                                this.lblJudicial.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        this.lblJudicial.Visible = true;
                        vEsrequerido = true;
                        return vEsrequerido;
                    }
                    try
                    {
                        if (this.FechaActoTramite.Date == Convert.ToDateTime("1/01/1900"))
                        {
                            this.lblFechaActaTramiteNotarial.Visible = true;
                            vEsrequerido = true;
                            return vEsrequerido;
                        }
                        else
                        {
                            this.lblFechaActaTramiteNotarial.Visible = false;
                        }
                    }
                    catch (Exception)
                    {
                        this.lblFechaActaTramiteNotarial.Visible = true;
                        vEsrequerido = true;
                        return vEsrequerido;
                    }

                }

                if (this.rblDesicionDelAuto.SelectedItem != null)
                {
                    if (this.rblDesicionDelAuto.SelectedItem.Value.Equals("CheckInadmitida"))
                    {
                        if (this.rblSubSanar.SelectedItem == null)
                        {
                            this.lblDesAuto.Visible = true;
                            vEsrequerido = true;
                            return vEsrequerido;
                        }
                        else
                        {
                            this.lblDesAuto.Visible = false;
                        }
                    }
                }
            }
            else
            {
                this.lblCampoRequeridoTipoTramite.Visible = true;
                vEsrequerido = true;
                count = count + 1;
            }

            if (count > 0)
            {
                vEsrequerido = true;
            }
            else
            {
                vEsrequerido = false;
            }

        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        return vEsrequerido;
    }

    #endregion

    #region GRILLAS

    /// <summary>
    /// Ordenar grilla documentacion recibida
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();

        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }

        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.direction == SortDirection.Ascending)
            {
                // Ascendente
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                // Descendente
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.direction == SortDirection.Ascending)
                {
                    // Ascendente
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    // Descendente
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.direction == SortDirection.Ascending)
                    {
                        // Ascendente
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        // Descendente
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Metodo del paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        this.CargarGrillaDocumentosSolicitadosYRecibidos();
    }

    /// <summary>
    /// Metodo para el paginador
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvwDocumentacionRecibida.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            List<DocumentosSolicitadosDenunciaBien> vList = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            gvwDocumentacionRecibida.DataSource = vList;
            gvwDocumentacionRecibida.DataBind();
        }
        else
        {
            ViewState["SortedgvwDocumentacionRecibida"] = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataSource = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
            this.gvwDocumentacionRecibida.DataBind();
        }
    }

    /// <summary>
    /// Metodo para los eventos de la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            List<DocumentosSolicitadosDenunciaBien> vListaDocumento = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (e.CommandName.Equals("Eliminar"))
            {
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                vListaDocumento.ForEach(item =>
                {
                    if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado)
                    {
                        item.EsNuevo = false;
                        item.IdEstadoDocumento = 6;
                        item.UsuarioModifica = this.GetSessionUser().NombreUsuario;
                        item.FechaModifica = DateTime.Now;
                    }
                });
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
                this.CargarGrilla();
            }
            else if (e.CommandName.Equals("Editar"))
            {
                this.CustomValidator6.Visible = false;
                this.CustomValidator17.Visible = true;
                this.fulArchivoRecibido.Enabled = true;
                int vRow = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRow].Value);
                DocumentosSolicitadosDenunciaBien vDocumentoSolicitado = vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();

                if (vDocumentoSolicitado.IdEstadoDocumento == 2)
                {
                    vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = true;
                }
                else
                {
                    vListaDocumento.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault().EsNuevo = false;
                }

                this.FechaRecibido.Enabled = true;
                this.btnAdd.Visible = false;
                this.btnAplicar.Visible = true;
                this.pnlDocumentacionRecibida.Enabled = true;

                this.ddlTipoDocumento.Enabled = true;
                this.FechaSolicitud.Enabled = true;
                this.txtObservacionesSolicitado.Enabled = true;

                this.hfIdDocumentoSolocitado.Value = vDocumentoSolicitado.IdDocumentosSolicitadosDenunciaBien.ToString();
                this.ddlTipoDocumento.SelectedValue = vDocumentoSolicitado.IdTipoDocumentoBienDenunciado.ToString();
                this.FechaSolicitud.Date = Convert.ToDateTime(vDocumentoSolicitado.FechaSolicitud);
                this.lblNombreArchivo.Text = vDocumentoSolicitado.NombreArchivo;
                this.lblNombreArchivo.Visible = true;
                this.txtObservacionesRecibido.Text = vDocumentoSolicitado.ObservacionesDocumentacionRecibida;
                this.txtObservacionesSolicitado.Text = vDocumentoSolicitado.ObservacionesDocumentacionSolicitada;

                foreach (ListItem item in this.rbtEstadoDocumento.Items)
                {
                    if (vDocumentoSolicitado.IdEstadoDocumento == Convert.ToInt32(item.Value))
                    {
                        item.Selected = true;
                        break;
                    }
                    else
                    {
                        this.rbtEstadoDocumento.ClearSelection();
                    }
                }
                this.FechaRecibido.Date = vDocumentoSolicitado.FechaRecibido ?? DateTime.Now;
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumento;
            }
            else if (e.CommandName.Equals("Ver"))
            {
                this.btnDescargar.Visible = false;
                bool EsVerArchivo = false;
                this.pnlArchivo.Visible = true;
                string vRuta = string.Empty;
                int vRowIndex = Convert.ToInt32(e.CommandArgument);
                int vIdDocumentoSolicitado = Convert.ToInt32(this.gvwDocumentacionRecibida.DataKeys[vRowIndex].Value);
                List<DocumentosSolicitadosDenunciaBien> vLista = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                DocumentosSolicitadosDenunciaBien vDocumento = vLista.Where(p => p.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoSolicitado).FirstOrDefault();
                EsVerArchivo = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo));
                vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vDocumento.RutaArchivo;

                if (!EsVerArchivo)
                {
                    EsVerArchivo = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo));
                    vRuta = "../RegistroDenuncia/Archivos/" + vDocumento.RutaArchivo;
                }

                if (EsVerArchivo)
                {
                    this.lblTitlePopUp.Text = vDocumento.NombreArchivo;
                    this.hfIdArchivo.Value = vIdDocumentoSolicitado.ToString();
                    this.hfNombreArchivo.Value = vDocumento.RutaArchivo.ToString();
                    if (vDocumento.RutaArchivo.ToUpper().Contains("PDF") || vDocumento.NombreArchivo.ToUpper().Contains("PDF"))
                    {
                        this.ifmVerAdchivo.Visible = true;
                        this.ifmVerAdchivo.Attributes.Add("src", vRuta);
                        this.imgDodumento.Visible = false;
                        this.imgDodumento.ImageUrl = string.Empty;
                        this.btnDescargar.Visible = true;
                    }
                    else if (vDocumento.RutaArchivo.ToUpper().Contains("JPG") || vDocumento.NombreArchivo.ToUpper().Contains("JPG"))
                    {
                        this.ifmVerAdchivo.Visible = false;
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.imgDodumento.Visible = true;
                        this.imgDodumento.ImageUrl = vRuta;
                        this.btnDescargar.Visible = true;
                    }
                    if (!this.btnDescargar.Visible)
                    {
                        this.ifmVerAdchivo.Attributes.Add("src", string.Empty);
                        this.lblTitlePopUp.Text = "El archivo no se encontró en la ruta especificada.";
                        this.btnDescargar.Visible = false;
                        this.imgDodumento.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para el orden de los registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvwDocumentacionRecibida_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DocumentosSolicitadosDenunciaBien> vList = new List<DocumentosSolicitadosDenunciaBien>();
        List<DocumentosSolicitadosDenunciaBien> vResult = new List<DocumentosSolicitadosDenunciaBien>();
        if (ViewState["SortedgvwDocumentacionRecibida"] != null)
        {
            vList = (List<DocumentosSolicitadosDenunciaBien>)ViewState["SortedgvwDocumentacionRecibida"];
        }
        if (e.SortExpression.Equals("NombreTipoDocumento"))
        {
            if (this.Direction == SortDirection.Ascending)
            {
                vResult = vList.OrderBy(a => a.NombreTipoDocumento).ToList();
            }
            else
            {
                vResult = vList.OrderByDescending(a => a.NombreTipoDocumento).ToList();
            }
        }
        else
        {
            if (e.SortExpression.Equals("FechaSolicitud"))
            {
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaSolicitud).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaSolicitud).ToList();
                }
            }
            else
            {
                if (e.SortExpression.Equals("ObservacionesDocumentacionSolicitada"))
                {
                    if (this.Direction == SortDirection.Ascending)
                    {
                        vResult = vList.OrderBy(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                    else
                    {
                        vResult = vList.OrderByDescending(a => a.ObservacionesDocumentacionSolicitada).ToList();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Grilla Page Change
    /// </summary>   
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            this.tbHistoria.PageIndex = e.NewPageIndex;
            if (this.ViewState["SortedgvwHistoricoDenuncia"] != null)
            {
                List<HistoricoEstadosDenunciaBien> vList = (List<HistoricoEstadosDenunciaBien>)this.ViewState["SortedgvwHistoricoDenuncia"];
                this.tbHistoria.DataSource = vList;
                this.tbHistoria.DataBind();
            }
            else
            {
                int vIdApoderado = Convert.ToInt32(this.GetSessionParameter("Mostrencos.RegistarInformacionApoderado.IdApoderado"));
                this.CargarGrillaHistorico(this.vMostrencosService.ConsultarApoderadosPorIdApoderado(vIdApoderado).IdDenunciaBien);
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// ordena los datos de la grilla
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">GridViewPageEventArgs</param>
    protected void GvwHistoricoDenuncia_OnSorting(object sender, GridViewSortEventArgs e)
    {
        List<HistoricoEstadosDenunciaBien> vList = new List<HistoricoEstadosDenunciaBien>();
        List<HistoricoEstadosDenunciaBien> vResult = new List<HistoricoEstadosDenunciaBien>();

        if (ViewState["SortedgvwHistoricoDenuncia"] != null)
        {
            vList = (List<HistoricoEstadosDenunciaBien>)ViewState["SortedgvwHistoricoDenuncia"];
        }

        switch (e.SortExpression)
        {
            case "NombreEstado":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.NombreEstado).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.NombreEstado).ToList();
                }
                break;

            case "FechaCrea":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.FechaCrea).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.FechaCrea).ToList();
                }
                break;

            case "Responsable":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Responsable).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Responsable).ToList();
                }
                break;

            case "Fase":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Fase).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Fase).ToList();
                }
                break;

            case "Actuacion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Actuacion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Actuacion).ToList();
                }
                break;

            case "Accion":
                if (this.Direction == SortDirection.Ascending)
                {
                    vResult = vList.OrderBy(a => a.Accion).ToList();
                }
                else
                {
                    vResult = vList.OrderByDescending(a => a.Accion).ToList();
                }
                break;
        }

        if (this.Direction == SortDirection.Ascending)
        {
            this.Direction = SortDirection.Descending;
        }
        else
        {
            this.Direction = SortDirection.Ascending;
        }

        ViewState["SortedgvwHistoricoDenuncia"] = vResult;
        this.tbHistoria.DataSource = vResult;
        this.tbHistoria.DataBind();
    }

    #endregion

    #region BOTONES

    /// <summary>
    /// Metodo para solicitar un documento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();
        try
        {
            vIdDenunciaBien = Convert.ToInt32(GetSessionParameter("Mostrencos.RegistrarTipoTramite.IdDenunciaBien"));
            bool vExiste = false;
            DocumentosSolicitadosDenunciaBien vDocumentacionSolicitada = new DocumentosSolicitadosDenunciaBien();
            List<DocumentosSolicitadosDenunciaBien> vListaDocumentacion = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
            if (vListaDocumentacion.Count > 0)
            {
                foreach (DocumentosSolicitadosDenunciaBien item in vListaDocumentacion)
                {
                    if (item.IdTipoDocumentoBienDenunciado == Convert.ToInt32(this.ddlTipoDocumento.SelectedValue))
                    {
                        this.toolBar.MostrarMensajeError("El registro ya existe");
                        vExiste = true;
                        break;
                    }
                }
            }

            if (!vExiste)
            {
                vDocumentacionSolicitada.IdDocumentosSolicitadosDenunciaBien = vListaDocumentacion.Count() > 0 ? vListaDocumentacion.Count() + 1 : 1;
                vDocumentacionSolicitada.IdDenunciaBien = vIdDenunciaBien;
                vDocumentacionSolicitada.FechaSolicitud = this.FechaSolicitud.Date;
                vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                vDocumentacionSolicitada.TipoDocumentoBienDenunciado = this.vMostrencosService.ConsultarTipoDocumentoBienDenunciadoPorId(vDocumentacionSolicitada.IdTipoDocumentoBienDenunciado);
                vDocumentacionSolicitada.NombreTipoDocumento = vDocumentacionSolicitada.TipoDocumentoBienDenunciado.NombreTipoDocumento;
                vDocumentacionSolicitada.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text.Trim().ToUpper();
                vDocumentacionSolicitada.EstadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorNombre("Solicitado");
                vDocumentacionSolicitada.NombreEstado = UpperCaseFirst(vDocumentacionSolicitada.EstadoDocumento.NombreEstadoDocumento.ToLowerInvariant());
                vDocumentacionSolicitada.IdEstadoDocumento = vDocumentacionSolicitada.EstadoDocumento.IdEstadoDocumento;
                vDocumentacionSolicitada.UsuarioCrea = this.GetSessionUser().NombreUsuario;
                vDocumentacionSolicitada.EsNuevo = true;
                vListaDocumentacion.Add(vDocumentacionSolicitada);
                this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentacion;
                this.CargarGrilla();
                this.ddlTipoDocumento.SelectedValue = "-1";
                this.FechaSolicitud.InitNull = true;
                this.lblNombreArchivo.Visible = false;
                this.txtObservacionesSolicitado.Text = string.Empty;
            }
        }
        catch (Exception)
        {
            this.toolBar.LipiarMensajeError();
            return;
        }
    }

    /// <summary>
    /// Metodo para cargar documento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAplicar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vIdDocumentoDenuncia = int.Parse(this.hfIdDocumentoSolocitado.Value);
            if (vIdDocumentoDenuncia != 0)
            {
                if (this.ValidarDocumentacionSolicitadayRecibida() == false)
                {
                    if (GuardarArchivos(vIdDocumentoDenuncia, 0))
                    {
                        List<DocumentosSolicitadosDenunciaBien> vListaDocumentosSolicitados = (List<DocumentosSolicitadosDenunciaBien>)this.ViewState["SortedgvwDocumentacionRecibida"];
                        TipoDocumentoBienDenunciado tipoDocumento = new TipoDocumentoBienDenunciado();
                        EstadoDocumento estadoDocumento = new EstadoDocumento();


                        vListaDocumentosSolicitados.ForEach(item =>
                        {
                            if (item.IdDocumentosSolicitadosDenunciaBien == vIdDocumentoDenuncia)
                            {
                                estadoDocumento = this.vMostrencosService.ConsultarEstadoDocumentoPorId(Convert.ToInt32(this.rbtEstadoDocumento.SelectedValue));

                                item.IdEstadoDocumento = Convert.ToInt32(estadoDocumento.IdEstadoDocumento);
                                item.NombreEstado = UpperCaseFirst(estadoDocumento.NombreEstadoDocumento.ToLowerInvariant());
                                item.FechaSolicitud = new DateTime(FechaSolicitud.Date.Year, FechaSolicitud.Date.Month, FechaSolicitud.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.ObservacionesDocumentacionSolicitada = this.txtObservacionesSolicitado.Text;
                                item.IdTipoDocumentoSoporteDenuncia = Convert.ToInt32(this.ddlTipoDocumento.SelectedValue);
                                item.FechaRecibido = new DateTime(FechaRecibido.Date.Year, FechaRecibido.Date.Month, FechaRecibido.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                item.FechaRecibidoGrilla = DateTime.Today.ToString("dd/MM/yyyy");
                                item.ObservacionesDocumentacionRecibida = this.txtObservacionesRecibido.Text;
                                item.NombreArchivo = fulArchivoRecibido.FileName;
                                item.RutaArchivo = vIdDocumentoDenuncia + @"/" + fulArchivoRecibido.FileName;
                            }
                        });
                        this.txtObservacionesSolicitado.Enabled = true;
                        this.FechaSolicitud.Enabled = true;
                        this.ddlTipoDocumento.Enabled = true;
                        this.txtObservacionesSolicitado.Text = string.Empty;
                        this.txtObservacionesRecibido.Text = string.Empty;
                        this.FechaSolicitud.InitNull = true;
                        this.FechaRecibido.Enabled = false;
                        this.FechaRecibido.InitNull = true;
                        this.fulArchivoRecibido.Enabled = false;
                        this.rbtEstadoDocumento.SelectedIndex = -1;
                        this.lblNombreArchivo.Visible = false;
                        this.ddlTipoDocumento.SelectedValue = "-1";
                        this.pnlDocumentacionRecibida.Enabled = false;
                        this.btnAplicar.Visible = false;
                        this.btnAdd.Visible = true;
                        this.CustomValidator6.Visible = true;
                        this.CustomValidator17.Visible = false;
                        this.ViewState["SortedgvwDocumentacionRecibida"] = vListaDocumentosSolicitados;
                        CargarGrilla();
                    }
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnCerrarInmuebleModal_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlPopUpHistorico.Visible = false;
    }

    /// <summary>
    /// Metodo para descargar un archivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDescargar_Click(object sender, EventArgs e)
    {
        try
        {
            int vIdArchivo = Convert.ToInt32(this.hfIdArchivo.Value);
            string vNombreArchivo = this.hfNombreArchivo.Value;

            bool EsDescargar = File.Exists(Server.MapPath("../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo));
            string vRuta = "../RegistrarDocumentosBienDenunciado/Archivos/" + vNombreArchivo;

            if (!EsDescargar)
            {
                EsDescargar = File.Exists(Server.MapPath("../RegistroDenuncia/Archivos/" + vNombreArchivo));
                vRuta = "../RegistroDenuncia/Archivos/" + vNombreArchivo;
            }

            if (EsDescargar)
            {
                string path = Server.MapPath(vRuta);
                System.IO.FileInfo toDownload = new System.IO.FileInfo(path);
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
                Response.TransmitFile(path);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo cargar los datos correspondientes a la pantalla de editar
    /// </summary>
    /// <param name="sender">Object Sender</param>
    /// <param name="e">Parámetro para el EventArgs</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            NavigateTo(SolutionPage.Edit);
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (this.ValidarCamposObligatorios() == false)
        {
            if (Request.QueryString["oP"] == "E")
            {
                string IdTramiteNo = Convert.ToString(GetSessionParameter("TipoTramiteIdTramiteNotarial"));
                string IdTramiteJu = Convert.ToString(GetSessionParameter("TipoTramiteIdTramiteJudicial"));

                if (!IdTramiteNo.Equals(string.Empty))
                {
                    this.UpdateTramiteNotarial(IdTramiteNo);
                }
                else if (!IdTramiteJu.Equals(string.Empty))
                {
                    this.UpdateTramiteJudicial(IdTramiteJu);
                }

                this.toolBar.OcultarBotonBuscar(false);
            }
            else
            {
                this.guardar();
            }
        }

    }

    protected void btnInfoDenuncia_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlPopUpHistorico.Visible = true;
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../RegistrarDocumentosBienDenunciado/Detail.aspx");
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void IdCerrarPanelAr_Click(object sender, ImageClickEventArgs e)
    {
        this.pnlArchivo.Visible = false;
    }

    protected void gvwDocumentacionRecibida_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (e.Row.Cells[i].Controls.Count > 0)
                {
                    ((LinkButton)e.Row.Cells[i].Controls[0]).TabIndex = -1;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[3].Text.Equals("01/01/0001 00:00:00") || e.Row.Cells[3].Text.Equals("01/01/0001") || e.Row.Cells[3].Text.Equals("01/01/1900"))
            {
                e.Row.Cells[3].Text = "";
            }
        }
    }

    protected static string UpperCaseFirst(string s)
    {
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        char[] a = s.ToCharArray();
        a[0] = char.ToUpper(a[0]);
        return new string(a);
    }

    /// <summary>
    /// Validar documentacion solicitada y recibida
    /// </summary>
    /// <returns>Resultado de la validación</returns>
    public bool ValidarDocumentacionSolicitadayRecibida()
    {
        Boolean vPuedeGuardar = true;
        int count = 0;

        //Validar Tipo Documento
        if (this.ddlTipoDocumento.SelectedValue == "-1")
        {
            this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
            vPuedeGuardar = true;
            count = count + 1;
        }
        else
        {
            this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
            vPuedeGuardar = false;
        }

        if (this.rbtEstadoDocumento.SelectedValue != "")
        {
            //Validar Tipo Documento
            if (this.ddlTipoDocumento.SelectedValue == "-1")
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = true;
                vPuedeGuardar = true;
                count = count + 1;
            }
            else
            {
                this.lblCampoRequeridoTipoDocumentoSolicitado.Visible = false;
                vPuedeGuardar = false;
            }
            //Validar Fecha Solicitud
            if (this.FechaRecibido.Date == Convert.ToDateTime("1/01/1900"))
            {
                this.reqFechaRecibida.Visible = true;
                vPuedeGuardar = true;
                count = count + 1;
            }
            else
            {
                this.reqFechaRecibida.Visible = false;
                vPuedeGuardar = false;
            }

            ////Validar archivo cargardo
            if (!fulArchivoRecibido.HasFile)
            {
                this.lblRqfNombreArchivo.Visible = true;
                vPuedeGuardar = true;
                count = count + 1;
            }
            else
            {
                vPuedeGuardar = false;
                this.lblRqfNombreArchivo.Visible = false;
            }
            if (count > 0)
            {
                vPuedeGuardar = true;
            }
            else
            {
                vPuedeGuardar = false;
            }
        }
        if (this.rbtEstadoDocumento.SelectedValue == "")
        {
            this.reqEstadoDocumento.Visible = true;
            vPuedeGuardar = true;
            count = count + 1;
        }
        else
        {
            vPuedeGuardar = false;
            this.reqEstadoDocumento.Visible = false;
        }
        if (count > 0)
        {
            vPuedeGuardar = true;
        }
        else
        {
            vPuedeGuardar = false;
        }
        return vPuedeGuardar;
    }

    #endregion

    protected void rblSubSanar_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.rblSubSanar.Focus();
            switch (this.rblSubSanar.SelectedItem.Value.ToString())
            {
                case "CheckNoSubsanar":
                    if (this.rblDesicionDelAuto.SelectedItem.Value.Equals("CheckInadmitida") && this.rblSubSanar.SelectedItem.Value.Equals("CheckNoSubsanar"))
                    {
                        this.rblDesicionDelAuto.SelectedValue = "CheckRechazada";
                        this.rblSubSanar.SelectedValue = null;
                        this.CamposSubsanar.Visible = false;
                        this.pnlInter.Visible = true;
                    }
                    break;

                case "CheckSubsanar":
                    if (this.rblDesicionDelAuto.SelectedItem.Value.Equals("CheckInadmitida") && this.rblSubSanar.SelectedItem.Value.Equals("CheckSubsanar"))
                    {
                        this.rblDesicionDelAuto.SelectedValue = "CheckAdmitida";
                        this.rblSubSanar.SelectedValue = null;
                        this.pnlInter.Visible = false;
                        this.CamposSubsanar.Visible = false;
                    }
                    break;

                case "CheckJudicial":
                    this.pnlTramiteJudicial.Enabled = true;
                    this.pnlTramiteJudicial.Visible = true;
                    this.DesBlockjudicial();
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Control Decisión del recurso
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RadioButtonList2.Focus();
            switch (this.RadioButtonList2.SelectedItem.Value.ToString())
            {
                case "CheckAceptada":
                    this.rblDesicionDelAuto.SelectedValue = "CheckAdmitida";
                    this.pnlInter.Visible = false;
                    this.RadioButtonList2.SelectedValue = null;
                    break;
            }
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
            throw;
        }
    }
}