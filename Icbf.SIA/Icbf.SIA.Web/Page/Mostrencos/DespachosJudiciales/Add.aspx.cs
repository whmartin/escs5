﻿
//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Add.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_DespachosJudiciales_Add : GeneralWeb
{
    #region Declaración Variables 

    /// <summary>
    /// toolBar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/DespachosJudiciales";

    #endregion

    #region Eventos

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.txtNombreDespacho.Focus();
                this.CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Metodo para ir a la pagina de Add
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo para guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        DespachoJudicial despacho = new DespachoJudicial()
        {
            Nombre = txtNombreDespacho.Text,
            Descripcion = txtDescripcion.Text,
            Departamento = new Departamento() { IdDepartamento = Convert.ToInt32(ddlDepartamento.SelectedValue) },
            Municipio = new Municipio() { IdMunicipio = Convert.ToInt32(ddlMunicipio.SelectedValue) },
            Estado = Convert.ToBoolean(Convert.ToInt32(chkEstadoDespacho.SelectedValue)),
            UsuarioCrea = GetSessionUser().NombreUsuario,
            UsuarioModifica = GetSessionUser().NombreUsuario
        };
        int result = 0;
        if (Request.QueryString["oP"] == "E")
        {
            despacho.IdDespachoJudicial = Convert.ToInt32(GetSessionParameter("DespachoJudicial.IdDespacho"));
            this.InformacionAudioria(despacho, this.PageName, vSolutionPage);
            result = vMostrencosService.ActualizarDespachoJudicial(despacho);
            if (result == 0)
            {
                SetSessionParameter("DespachoJudicial.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
            }
        }
        else
        {
            this.InformacionAudioria(despacho, this.PageName, vSolutionPage);
            result = vMostrencosService.IngresarDespachoJudicial(despacho);
            if (result != 0)
            {
                SetSessionParameter("DespachoJudicial.IdDespacho", result);
                SetSessionParameter("DespachoJudicial.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                this.toolBar.MostrarMensajeError("El registro ya existe");
            }
        }
    }

    /// <summary>
    /// Metodo para
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        LlenarMunicipios(Convert.ToInt32(ddlDepartamento.SelectedValue));
        this.ddlDepartamento.Focus();
    }

    /// <summary>
    /// Metodo para ir a la pagina de List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Metodo para registros
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdDespacho = Convert.ToInt32(GetSessionParameter("DespachoJudicial.IdDespacho"));
            DespachoJudicial vDespachoJudicial = vMostrencosService.ConsultarDespachoJudicialPorId(vIdDespacho);
            txtNombreDespacho.Text = vDespachoJudicial.Nombre;
            txtDescripcion.Text = vDespachoJudicial.Descripcion;
            ddlDepartamento.SelectedValue = vDespachoJudicial.Departamento.IdDepartamento.ToString();
            LlenarMunicipios(Convert.ToInt32(ddlDepartamento.SelectedValue));
            ddlMunicipio.SelectedValue = vDespachoJudicial.Municipio.IdMunicipio.ToString();
            chkEstadoDespacho.Items.FindByValue(Convert.ToInt32(vDespachoJudicial.Estado).ToString()).Selected = true;
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    /// <summary>
    /// Metodo para Iniciar
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnConsultar_Click);
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.EstablecerTitulos("Despachos Judiciales");
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo llenar municipios
    /// </summary>
    /// <param name="Departamento"></param>
    private void LlenarMunicipios(int? Departamento)
    {
        if (Departamento > 0)
        {
            ManejoControlesContratos Controles = new ManejoControlesContratos();
            Controles.LlenarMunicipios(ddlMunicipio, "-1", true, Departamento != -1 ? Departamento : null);
        }
        else
        {
            ddlMunicipio.Items.Clear();
            ddlMunicipio.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }

    }

    /// <summary>
    /// Metodo para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarDepartamentos();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar departamentos
    /// </summary>
    private void CargarDepartamentos()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarDepartamentos(ddlDepartamento, "-1", true);
        for (int i = 0; i < ddlDepartamento.Items.Count; i++)
        {
            ddlDepartamento.Items[i].Text = ddlDepartamento.Items[i].Text.ToUpper();
        }
    }

    /// <summary>
    /// Metodo para cargar municipios
    /// </summary>
    private void CargarMunicipios()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarMunicipios(ddlMunicipio, "-1", true, null);
        for (int i = 0; i < ddlMunicipio.Items.Count; i++)
        {
            ddlMunicipio.Items[i].Text = ddlMunicipio.Items[i].Text.ToUpper();
        }
    }

    #endregion
}