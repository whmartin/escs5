﻿
<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Mostrencos_DespachosJudiciales_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            Nombre del Despacho Judicial *
                            <asp:RequiredFieldValidator ID="rfvDespacho" ControlToValidate="txtNombreDespacho" runat="server"
                                ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            Descripción *
                            <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion" runat="server"
                                ValidationGroup="btnGuardar" SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                             <asp:TextBox runat="server" ID="txtNombreDespacho" MaxLength="100" Width="300" ></asp:TextBox>
                             <Ajax:FilteredTextBoxExtender ID="FilteredtxtNombreDespacho" runat="server"
                                TargetControlID="txtNombreDespacho" FilterType="Numbers, Custom,LowercaseLetters,UppercaseLetters"
                                ValidChars="áéíóúÁÉÍÓÚñÑ " />  
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine" Width="300" onkeypress="return CheckLength();"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server"
                                TargetControlID="txtDescripcion" FilterType="Numbers, Custom,LowercaseLetters,UppercaseLetters"
                                ValidChars="áéíóúÁÉÍÓÚñÑ .,_():;" />  
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Departamento *
                            <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="ddlDepartamento"
                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                        </td>
                        <td>
                            Municipio *
                            <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlMunicipio"
                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                            
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" AutoPostBack="true" Width="300" 
                                OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" ID="ddlDepartamento"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" Width="300" ID="ddlMunicipio">
                                <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Estado del Despacho Judicial *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="chkEstadoDespacho" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cphCont_txtDescripcion').on('input', function (e) {
                if (document.getElementById("cphCont_txtDescripcion").value.length > 512) {
                    this.value = document.getElementById("cphCont_txtDescripcion").value.substr(0, 512);
                }
            });
        });
        function CheckLength() {
            var textbox = document.getElementById("cphCont_txtDescripcion").value;
            if (textbox.trim().length >= 512) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>

</asp:Content>