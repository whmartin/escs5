﻿
//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase List.aspx.cs.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>05/06/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Mostrencos.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Mostrencos_DespachosJudiciales_List : GeneralWeb
{
    /// <summary>
    /// toolBar
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// vMostrencosService
    /// </summary>
    MostrencosService vMostrencosService = new MostrencosService();

    /// <summary>
    /// PageName
    /// </summary>
    string PageName = "Mostrencos/DespachosJudiciales";

    /// <summary>
    /// Direction
    /// </summary>
    public SortDirection Direction
    {
        get
        {
            if (this.ViewState["directionState"] == null)
            {
                this.ViewState["directionState"] = SortDirection.Descending;
            }
            return (SortDirection)this.ViewState["directionState"];
        }

        set
        {
            this.ViewState["directionState"] = value;
        }
    }

    /// <summary>
    /// Page_PreInit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        this.Iniciar();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.SetSessionParameter("DespachoJudicial", "0");
        SolutionPage vSolutionPage = SolutionPage.List;
        if (this.ValidateAccess(this.toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                this.txtNombreDespacho.Focus();
                this.CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Metodo para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarDepartamentos();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo inicial
    /// </summary>
    private void Iniciar()
    {
        try
        {
            this.toolBar = (masterPrincipal)this.Master;
            this.toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            this.toolBar.eventoBuscar += new ToolBarDelegate(this.btnBuscar_Click);
            this.toolBar.eventoExcel += new ToolBarDelegate(this.btnExportar_Click);
            this.toolBar.EstablecerTitulos("Despachos Judiciales");
            this.gvDespachoJudicial.PageSize = PageSize();
            this.gvDespachoJudicial.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para exportar los datos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvDespachoJudicial.Rows.Count > 0)
        {
            toolBar.LipiarMensajeError();
            DataTable datos = new DataTable();
            Boolean vPaginador = this.gvDespachoJudicial.AllowPaging;
            Boolean vPaginadorError = gvDespachoJudicial.AllowPaging;
            ExportarExcel vExportarExcel = new ExportarExcel();
            gvDespachoJudicial.AllowPaging = false;
            List<DespachoJudicial> vListaDespachos = ((List<DespachoJudicial>)this.ViewState["SortedgvDespachoJudicial"]).ToList();
            datos = Utilidades.GenerarDataTable(this.gvDespachoJudicial, vListaDespachos);

            if (datos != null)
            {
                if (datos.Rows.Count > 0)
                {
                    GridView datosexportar = new GridView();
                    datosexportar.DataSource = datos;
                    datosexportar.DataBind();
                    vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "DespachosJudiciales");
                    gvDespachoJudicial.AllowPaging = vPaginador;
                }
                else
                    toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
            }
            else
                toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
    }

    /// <summary>
    /// Metodo para ir a la pagina Add
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Metodo que llama al metodo Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.Buscar();
    }

    /// <summary>
    /// Metodo para buscar despachos
    /// </summary>
    private void Buscar()
    {
        toolBar.LipiarMensajeError();
        try
        {
            bool? estado = null;
            switch (chkEstadoDespacho.Text)
            {
                case "Activo":
                    estado = true;
                    break;
                case "Inactivo":
                    estado = false;
                    break;
            }
            List<DespachoJudicial> vListaDespachoJudicial = this.vMostrencosService.ConsultarDespachoJudicial(txtNombreDespacho.Text, Convert.ToInt32(this.ddlDepartamento.SelectedValue), Convert.ToInt32(this.ddlMunicipio.SelectedValue), estado);
            this.ViewState["SortedgvDespachoJudicial"] = vListaDespachoJudicial;
            this.gvDespachoJudicial.Visible = true;
            this.gvDespachoJudicial.DataSource = vListaDespachoJudicial;
            this.gvDespachoJudicial.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para cargar departamentos
    /// </summary>
    private void CargarDepartamentos()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarDepartamentos(ddlDepartamento, "-1", true);
        for (int i = 0; i < ddlDepartamento.Items.Count; i++)
        {
            ddlDepartamento.Items[i].Text = ddlDepartamento.Items[i].Text.ToUpper();
        }
    }

    /// <summary>
    /// Metodo para cargar municipios
    /// </summary>
    private void CargarMunicipios()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarMunicipios(ddlMunicipio, "-1", true, null);
        for (int i = 0; i < ddlMunicipio.Items.Count; i++)
        {
            ddlMunicipio.Items[i].Text = ddlMunicipio.Items[i].Text.ToUpper();
        }
    }

    /// <summary>
    /// Metodo para llamar el metodo llenar municipios
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        LlenarMunicipios(Convert.ToInt32(ddlDepartamento.SelectedValue));
        this.ddlDepartamento.Focus();
    }

    /// <summary>
    /// Metodo para llenar municipios
    /// </summary>
    /// <param name="Departamento"></param>
    private void LlenarMunicipios(int? Departamento)
    {
        if (Departamento > 0)
        {
            ManejoControlesContratos Controles = new ManejoControlesContratos();
            Controles.LlenarMunicipios(ddlMunicipio, "-1", true, Departamento != -1 ? Departamento : null);
            for (int i = 0; i < ddlMunicipio.Items.Count; i++)
            {
                ddlMunicipio.Items[i].Text = ddlMunicipio.Items[i].Text.ToUpper();
            }
        }
        else
        {
            ddlMunicipio.Items.Clear();
            ddlMunicipio.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }

    }

    /// <summary>
    /// Metodo para ordenar los datos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDespachoJudicial_Sorting(object sender, GridViewSortEventArgs e)
    {
        List<DespachoJudicial> vList = new List<DespachoJudicial>();
        List<DespachoJudicial> vResult = new List<DespachoJudicial>();
        if (this.ViewState["SortedgvDespachoJudicial"] != null)
        {
            vList = (List<DespachoJudicial>)this.ViewState["SortedgvDespachoJudicial"];
        }

        switch (e.SortExpression)
        {
            case "Nombre":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderBy(a => a.Nombre).ToList();
                else
                    vResult = vList.OrderByDescending(a => a.Nombre).ToList();
                break;

            case "Descripcion":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderBy(a => a.Descripcion).ToList();
                else
                    vResult = vList.OrderByDescending(a => a.Descripcion).ToList();
                break;

            case "NombreDepartamento":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderBy(a => a.Departamento.NombreDepartamento).ToList();
                else
                    vResult = vList.OrderByDescending(a => a.Departamento.NombreDepartamento).ToList();
                break;

            case "NombreMunicipio":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderBy(a => a.Municipio.NombreMunicipio).ToList();
                else
                    vResult = vList.OrderByDescending(a => a.Municipio.NombreMunicipio).ToList();
                break;

            case "Estado":
                if (this.Direction == SortDirection.Ascending)
                    vResult = vList.OrderByDescending(a => a.Estado).ToList();
                else
                    vResult = vList.OrderBy(a => a.Estado).ToList();
                break;
        }

        if (this.Direction == SortDirection.Ascending)
            this.Direction = SortDirection.Descending;
        else
            this.Direction = SortDirection.Ascending;

        this.ViewState["SortedgvDespachoJudicial"] = vResult;
        this.gvDespachoJudicial.DataSource = vResult;
        this.gvDespachoJudicial.DataBind();
    }

    /// <summary>
    /// Metodo para llamar a metodo seleccionar registro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDespachoJudicial_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SeleccionarRegistro(this.gvDespachoJudicial.SelectedRow);
    }

    /// <summary>
    /// Metodo para seleccionar un registro
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = this.gvDespachoJudicial.DataKeys[rowIndex].Value.ToString();
            this.SetSessionParameter("DespachoJudicial.IdDespacho", strValue);
            this.SetSessionParameter("DespachoJudicial.Guardado", "0");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Metodo para paginar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDespachoJudicial_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvDespachoJudicial.PageIndex = e.NewPageIndex;
        if (this.ViewState["SortedgvDespachoJudicial"] != null)
        {
            List<DespachoJudicial> vList = (List<DespachoJudicial>)this.ViewState["SortedgvDespachoJudicial"];
            this.gvDespachoJudicial.DataSource = vList;
            this.gvDespachoJudicial.DataBind();
        }
        else
        {
            this.Buscar();
        }
    }
}