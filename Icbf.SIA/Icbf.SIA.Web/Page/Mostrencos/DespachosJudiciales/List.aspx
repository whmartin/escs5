﻿
<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Mostrencos_DespachosJudiciales_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre del despacho judicial
            </td>
            <td>
                Departamento *
                <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="ddlDepartamento"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreDespacho" MaxLength="100" Width="300" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreDespacho" runat="server"
                    TargetControlID="txtNombreDespacho" FilterType="Numbers, Custom,LowercaseLetters,UppercaseLetters"
                    ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <asp:DropDownList runat="server" AutoPostBack="true" Width="300" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" ID="ddlDepartamento"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Municipio *
                <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlMunicipio"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Estado del despacho judicial *

            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" Width="300" ID="ddlMunicipio">
                     <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                </asp:DropDownList>           
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="chkEstadoDespacho" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo"></asp:ListItem>
                    <asp:ListItem Text="Inactivo"></asp:ListItem>
                    <asp:ListItem Text="Todos" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">              
                <td>
                    <asp:GridView runat="server" ID="gvDespachoJudicial" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="gvDespachoJudicial_SelectedIndexChanged"
                        GridLines="None" Width="100%" DataKeyNames="IdDespachoJudicial" CellPadding="0" Height="16px" OnSorting="gvDespachoJudicial_Sorting" OnPageIndexChanging="gvDespachoJudicial_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre del despacho judicial" DataField="Nombre" SortExpression="Nombre" ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion"  ItemStyle-CssClass="descripcionGrilla"/>
                            <asp:BoundField HeaderText="Departamento" DataField="Departamento.NombreDepartamento" SortExpression="NombreDepartamento"/>
                            <asp:BoundField HeaderText="Municipio" DataField="Municipio.NombreMunicipio" SortExpression="NombreMunicipio"/>
                            <asp:TemplateField HeaderText="Estado del despacho judicial" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# (Boolean.Parse(Eval("Estado").ToString())) ? "Activo" : "Inactivo" %>'></asp:Label> 
                                </ItemTemplate>
                            </asp:TemplateField>
                           </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <style>
    .descripcionGrilla {
        word-break: break-all;
    }
</style>
</asp:Content>
