﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Mostrencos_DespachosJudiciales_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <asp:Panel runat="server" ID="pnlConsulta">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            Nombre del despacho judicial *
                        </td>
                        <td>
                            Descripción *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreDespacho" Enabled="false" MaxLength="100" Width="300" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine" Width="300" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Departamento *
                        </td>
                        <td>
                            Municipio *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" Width="300" Enabled="false" ID="ddlDepartamento"></asp:DropDownList>
                                    
                        </td>
                        <td>
                            <asp:DropDownList runat="server" Width="300" ID="ddlMunicipio" Enabled="false"></asp:DropDownList>   
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Estado del despacho judicial *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:RadioButtonList runat="server" ID="chkEstadoDespacho" Enabled="false" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0" runat="server"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        <script type="text/javascript">
        $(document).ready(function () {
            $('#cphCont_txtDescripcion').on('blur', function () {
                document.getElementById("cphCont_txtDescripcion").value = document.getElementById("cphCont_txtDescripcion").value.substr(0, 512);;
            }); 
        });
        function CheckLength() {
            var textbox = document.getElementById("cphCont_txtDescripcion").value;
            if (textbox.trim().length >= 512) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>
        </ContentTemplate>            
    </asp:UpdatePanel>
</asp:Content>
