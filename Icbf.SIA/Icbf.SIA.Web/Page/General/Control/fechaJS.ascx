﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="fechaJS.ascx.cs" Inherits="comun_control_fechaJS" %>
<asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFecha" ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
<br />
<asp:TextBox ID="txtFecha" runat="server" Width="200px" CssClass="cF" onchange="EjecutarJS();"></asp:TextBox>
<asp:Image ID="imgCalendario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />&nbsp;
    <br />
<asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFecha$txtFecha" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator> 

<div style="display:none">
<Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
</Ajax:MaskedEditExtender>
<Ajax:CalendarExtender ID="cetxtFecha" runat="server" Format="dd/MM/yyyy" 
        PopupButtonID="imgCalendario" TargetControlID="txtFecha"></Ajax:CalendarExtender>

</div>