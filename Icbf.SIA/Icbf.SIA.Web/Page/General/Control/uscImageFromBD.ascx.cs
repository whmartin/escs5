﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

public partial class General_General_Control_uscImageFromBD : System.Web.UI.UserControl
{
    public byte[] byteArray { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (byteArray != null)
        {
            showImage(byteArray);
        }
    }

    private void showImage(byte[] byteArray)
    {
        System.Drawing.Image img = ByteArrayToImage(byteArray);
        string base64String = Convert.ToBase64String(byteArray, 0, byteArray.Length);
        Image1.ImageUrl = "data:image/png;base64," + base64String;

        //Image1.ImageUrl = @"\Icbf.NMF.Web\General\General\Control\ghImageToDB.ashx?strBase64=" + base64String;
        Image1.Visible = true;

    }



    public System.Drawing.Image ByteArrayToImage(byte[] byteArray)
    {
        MemoryStream img = new MemoryStream(byteArray);
        System.Drawing.Image returnImage = System.Drawing.Image.FromStream(img);
        return returnImage;
    }
}