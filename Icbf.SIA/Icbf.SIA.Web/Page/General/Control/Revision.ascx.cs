﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_General_Control_Revision : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void ddlAprueba_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAprueba.SelectedValue == "2")
        {
            txtObservaciones.Enabled = true;
            rfvObservaciones.Enabled = true;
            lblObservaciones.Text = "Observaciones *";
        }
        else
        {
            txtObservaciones.Enabled = false;
            rfvObservaciones.Enabled = false;
            txtObservaciones.Text = "";
            lblObservaciones.Text = "Observaciones";
        }
    }
}