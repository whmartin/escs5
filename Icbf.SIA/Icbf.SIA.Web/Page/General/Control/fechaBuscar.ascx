﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="fechaBuscar.ascx.cs" Inherits="comun_control_fecha_buscar" %>
<div style="width: 100%">
    
    <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFecha"
        SetFocusOnError="false" Enabled="False"
        Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
    
    <asp:TextBox ID="txtFecha" runat="server" CssClass="cF"></asp:TextBox>
    <asp:Image ID="imgCalendario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
        Style="cursor: hand" />&nbsp;
    <br />
    <%--<asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFecha$txtFecha" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" EnableClientScript="false"
    Operator="DataTypeCheck"  SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator> 
    --%>
    <div style="display: none">
        <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
            CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
            CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
        </Ajax:MaskedEditExtender>
        <Ajax:CalendarExtender ID="cetxtFecha" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalendario"
            TargetControlID="txtFecha" cssclass="calendario" >
        </Ajax:CalendarExtender>
    </div>
</div>
