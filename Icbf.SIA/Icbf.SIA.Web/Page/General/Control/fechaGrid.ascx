﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="fechaGrid.ascx.cs" Inherits="comun_control_fechaGrid" %>
<asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFecha" ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
<br />
<asp:TextBox ID="txtFecha" runat="server" CssClass="cF"></asp:TextBox>
&nbsp;
    <br />
<asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFecha$txtFecha" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator> 

<div style="display:none">
<Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
</Ajax:MaskedEditExtender>


</div>