﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Revision.ascx.cs" Inherits="General_General_Control_Revision" %>
<script type="text/javascript" language="javascript">
    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            //limitCount.value = limitNum - limitField.value.length;
        }
    }
</script>
<asp:UpdatePanel runat="server" ID="updRevision">
<ContentTemplate>
<center>
<Ajax:Accordion   
        ID="Accordion2"   
        HeaderCssClass="accordionHeader"  
        HeaderSelectedCssClass="accordionHeaderSelected"  
        ContentCssClass="accordionContent"   
        runat="server" Width="90%" Height="100%">  
        <Panes>
            <Ajax:AccordionPane ID="APDatosBasicos" runat="server">  
                <Header>Revisión</Header>  
                <Content>  
                    <table width="90%" align="center">
                            <tr class="rowB">
                                <td>
                                    No. Revisión
                                </td>
            
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:HiddenField runat="server" ID="hfTipo"/>
                                    <asp:TextBox runat="server" ID="txtNumRevision" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    <asp:Label ID="lblRegistro" runat="server" Text="A&ntilde;o"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtAno" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    ¿Aprobó Revisión? *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvAprueba" ControlToValidate="ddlAprueba"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator runat="server" ID="cvAprueba" ControlToValidate="ddlAprueba"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlAprueba" OnSelectedIndexChanged="ddlAprueba_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Seleccione" Value="-1" />
                                        <asp:ListItem Text="SI" Value="1" />
                                        <asp:ListItem Text="NO" Value="2" />
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    <asp:Label ID="lblObservaciones" runat="server" Text="Observaciones"></asp:Label>
                                    <asp:RequiredFieldValidator runat="server" ID="rfvObservaciones" ControlToValidate="txtObservaciones"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" onKeyDown="limitText(this,250);" 
                                        onKeyUp="limitText(this,250);" Enabled="false"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftObservaciones" runat="server" TargetControlID="txtObservaciones"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                </td>
                            </tr>
                        </table>
                    </Content>
                </Ajax:AccordionPane>
            </Panes>
        </Ajax:Accordion>
        </center>
    </ContentTemplate>
    </asp:UpdatePanel>