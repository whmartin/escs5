﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

public partial class General_General_Control_uscImageToDB : System.Web.UI.UserControl
{
    public int FileSizeAllowed { get; set; }
    public string FilesPath { get; set; }
    public string FileName { get; set; }
    //public byte[] byteArray { get; set; }
    public int ImageId { get; set; }
    public string ImageType { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ImageId != 0)
        {
            showImage(ImageId, ImageType);
            
        }
    }
    

    protected void Button1_Click(object sender, EventArgs e)
    {
        if
        (fileUp.HasFile)
        {
            try
            {
                if (!hdfNombreArchivo.Value.Equals(""))
                {
                    deleteFile(hdfNombreArchivo.Value);
                }
                double lengthMB = (fileUp.PostedFile.ContentLength) / 1000d;
                if (lengthMB > (FileSizeAllowed * 1024))
                {
                    this.customValidador.Visible = true;
                    this.customValidador.Text = "El tamaño maximo de archivo permitido es de " + (FileSizeAllowed * 1024) + "KB";
                    //Image1.ImageUrl = "";
                    Image1.Attributes["src"] = "";
                    return;
                }
                else
                {
                    string rutaArchivos = Server.MapPath(FilesPath);
                    if (!System.IO.Directory.Exists(rutaArchivos))
                    {
                        System.IO.Directory.CreateDirectory(rutaArchivos);
                    }

                    FileInfo inforFile = new FileInfo(fileUp.FileName);
                    string extension = inforFile.Extension;
                    FileName = System.IO.Path.GetRandomFileName();
                    FileName = Path.ChangeExtension(FileName, extension);
                    while (File.Exists((rutaArchivos) + FileName))
                    {
                        FileName = System.IO.Path.GetRandomFileName();
                        FileName = Path.ChangeExtension(FileName, extension);
                    }


                    fileUp.SaveAs((rutaArchivos) + FileName);
                    hdfNombreArchivo.Value = rutaArchivos + FileName;
                    System.Drawing.Image img = System.Drawing.Image.FromFile(rutaArchivos + FileName);
                    int[] dimensiones = CalculateDimensions(img.Width, img.Height);
                    img.Dispose();
                    //CalcularDimensiones
                    //Redimensionar
                    ResizeImage(dimensiones[0], dimensiones[1]);
                    //Image1.ImageUrl = FilesPath + FileName;
                    Image1.Attributes["src"] = FilesPath + FileName;
                }
            }
            catch
            (Exception exc)
            {
                this.customValidador.Visible = true;
                this.customValidador.Text = "Ocurrió un error al cargar el archivo, por favor intentelo de nuevo. " + exc.Message;

            }
        }
    }

    public void deleteFile(string ruta)
    {
        System.IO.File.Delete(ruta);
    }

    private int[] CalculateDimensions(decimal Width, decimal Height)
    {
        int maxSize = 150;
        int[] dimensions = new int[2]; //pos 0 Width  y pos  1 Height
        dimensions[0] = maxSize;
        dimensions[1] = maxSize;
        if (Width > Height)
        {
            dimensions[1] = (int)((Height / Width) * maxSize);
        }
        else if (Height > Width)
        {
            dimensions[0] = (int)((Width / Height) * maxSize);
        }

        return dimensions;
    }

    public void ResizeImage(int newWidth, int newHeight)
    {
        Bitmap oBitmap = new Bitmap(hdfNombreArchivo.Value);
        Graphics oGraphics;
        Bitmap newBitmap = new Bitmap(newWidth, newHeight);
        oGraphics = Graphics.FromImage(newBitmap);

        oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        oGraphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
        oGraphics.DrawImage(oBitmap,
           new Rectangle(0, 0, newBitmap.Width, newBitmap.Height),
           new Rectangle(0, 0, oBitmap.Width, oBitmap.Height),
           GraphicsUnit.Pixel);

        oBitmap.Dispose();
        oBitmap = newBitmap;
        oGraphics.Dispose();
        oBitmap.Save(hdfNombreArchivo.Value);
        oBitmap.Dispose();
    }

    //public System.Drawing.Image ByteArrayToImage(byte[] byteArray)
    //{
    //    MemoryStream img = new MemoryStream(byteArray);
    //    System.Drawing.Image returnImage = System.Drawing.Image.FromStream(img);
    //    return returnImage;
    //}

    private void showImage(int ImageId, string ImageType)
    {
        //csContab
        //System.Drawing.Image img = ByteArrayToImage(byteArray);
        //string base64String = Convert.ToBase64String(byteArray, 0, byteArray.Length);
        //Image1.ImageUrl = "data:image/png;base64," + base64String;
        //Image1.Attributes["ImageUrl"] = Page.ResolveClientUrl();
        Image1.Attributes["src"] = "../../../General/General/Control/ghImageFromDB.ashx?id=" + ImageId + "&imageType=" + ImageType;
        Image1.Visible = true;



    }


    private  string GetString(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

}