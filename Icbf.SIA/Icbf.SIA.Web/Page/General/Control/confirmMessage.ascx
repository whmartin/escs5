﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="confirmMessage.ascx.cs" Inherits="General_General_Control_confirmMessage" %>
<asp:HiddenField ID="hfConfirmationMessage" runat="server" ClientIDMode="AutoID" />
<asp:HiddenField ID="hfConfirmationValue" runat="server" ClientIDMode="AutoID" />
<script type="text/javascript">
    function confirmation<%=ControlId%>() {
        var result = confirm($('#<%=hfConfirmationMessage.ClientID %>').val());
        document.getElementById("<%=hfConfirmationValue.ClientID %>").value = result;
        return result;
    }
</script>
