﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_General_Control_confirmMessage : System.Web.UI.UserControl
{
    private string _ControlId;
    public string ControlId {
        get {
            if(string.IsNullOrEmpty(_ControlId))
                _ControlId = Guid.NewGuid().ToString().Replace("-", "");
            return _ControlId;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_ControlId))
            _ControlId = Guid.NewGuid().ToString().Replace("-", "");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Inicializar();
        }
    }

    protected void Inicializar()
    {
        hfConfirmationMessage.Value = "está seguro que desea guardar la información";
    }

    public string GetConfirmationFunction(bool pIncludeReturn = true)
    {
        string functionName = string.Format("confirmation{0}();", ControlId);
        if (pIncludeReturn)
            functionName = "return " + functionName;
        return functionName;
    }

    public void SetConfirmationMessage(string pMessage)
    {
        hfConfirmationMessage.Value = pMessage;
    }

    public bool IsConfirmed()
    {
        bool value = false;
        bool.TryParse(hfConfirmationValue.Value.ToString(), out value);
        return value;
    }
}