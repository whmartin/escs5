﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uscImageToDB.ascx.cs"
    Inherits="General_General_Control_uscImageToDB" %>
<script type="text/javascript">
    function showFileUpload() {
        //fileUp
        document.getElementById('cphCont_myUSC_fileUp').click();
        return false;
    }


    function selectFile() {
        //txtSimulaFileUpload
        document.getElementById('cphCont_myUSC_txtSimulaFileUpload').value = document.getElementById('cphCont_myUSC_fileUp').value;
    }
</script>
<div style="width: 100%;">
    <table>
        <tr>
            <td colspan="2">
                <asp:RegularExpressionValidator ID="rExvExcel" SetFocusOnError="True" runat="server"
                    ControlToValidate="fileUp" ErrorMessage="Solo archivos imagen" ValidationExpression="[a-zA-Z0_9].*\b(.tif|.TIF|.gif|.GIF|.jpeg|.JPEG|.jpg|.JPG|.jif|.JIF|.jfif|.JFIF|.png|.PNG)\b"
                    Display="Dynamic" ValidationGroup="validaFile" ForeColor="Red"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: top">
                <asp:HiddenField ID="hdfNombreArchivo" runat="server" Value="" />
                <asp:FileUpload ID="fileUp" runat="server" onchange="selectFile()" Style="display: none" />
                <table>
                    <tr>
                        <td style="vertical-align: top">
                            <asp:TextBox ID="txtSimulaFileUpload" runat="server"  ReadOnly="true" Text="SELECCIONAR..." BackColor="#F1F0EF"></asp:TextBox>
                            <td style="vertical-align: top">
                                <asp:ImageButton ID="imgSearch" runat="server" CausesValidation="false" ImageUrl="~/Image/other/search_images.png"
                                    OnClientClick=" return showFileUpload()" />
                            </td>
                            <td style="vertical-align: top">
                                <asp:RequiredFieldValidator ID="rfvExcel" runat="server" ControlToValidate="fileUp"
                                    ErrorMessage="*" ValidationGroup="validaFile" ForeColor="Red" />
                                <asp:ImageButton ID="Button1" runat="server" OnClick="Button1_Click" ValidationGroup="validaFile"
                                    ImageUrl="~/Image/other/uploadImg.jpg" ToolTip="Cargar Imagen" />
                            </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:center">
                            <img id="Image1" runat="server" src="~/Image/other/sinImagen.jpg" width="120" height="120" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CustomValidator runat="server" ID="customValidador" ForeColor="Red" />
            </td>
        </tr>
    </table>
</div>
