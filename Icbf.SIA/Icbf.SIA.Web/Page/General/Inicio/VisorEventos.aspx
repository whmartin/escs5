﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VisorEventos.aspx.cs" Inherits="Page_General_Inicio_VisorEventos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
    <title>: ICBF -  SIA :</title>
    <style type="text/css">
        .style1
        {
        }
    p.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="display:none"><asp:ScriptManager ID="smPagina" runat="server"></asp:ScriptManager></div>
    
    <div class='centrar'>
        <table width="80%" cellpadding="0" cellspacing="0" class="Loggin" align="center">
            <tr>
                <td colspan="2" class="header">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="C" colspan="2">
                    <p class="MsoNormal">
                        <span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;">Ha ocurrido 
                        una excepción en el servidor de aplicaciones, por favor copie el texto de la 
                        excepción y comuníquela al administrador del sistema<o:p></o:p></span></p>
                </td>
            </tr>
            <tr>
                <td class="style1" colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <img alt="logo" src="../../../Image/loggin/LogoGCB.jpg" />
                </td>
                <td class="user">
                    <asp:TextBox ID="txtError" runat="server" Height="300px" TextMode="MultiLine" 
                        Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td align="center">
                                                <a href="../../../Default.aspx" target="_parent">
                                                    <img src="../../../Image/btn/home.png" alt="Volver" title="Inicio"></a></td>
            </tr>
            <tr>
                <td colspan="2" class="Footer">
                    Instituto Colombiano de Bienestar Familiar
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
