﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogOut.aspx.cs" Inherits="Page_General_Inicio_LogOut" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
    <title>: ICBF -  SIA :</title>
</head>
<body style="font-family: Verdana; font-size: smaller; color: White" onload="">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="smPagina" runat="server">
    </asp:ScriptManager>
    <div class='centrar'>
        <table width="80%" cellpadding="0" cellspacing="0" class="Loggin" align="center">
            <tr>
                <td colspan="2" class="header">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="Logo">
                    <img alt="logo" src="../../../Image/loggin/LogoGCB.jpg" />
                </td>
                <td class="user">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" summary="">
                        <tr align="center">
                            <td valign="top" style="width: 100%; padding-top: 20px; padding-right: 20px; padding-bottom: 0px;
                                padding-left: 20px;">
                                <div>
                                    <table width="95%" align="center">
                                        <tr>
                                            <td>
                                                <span class="lbE">Su Sesión ha sido cerrada correctamente.</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;">
                                                <a href="../../../Default.aspx" target="_parent">
                                                    <img src="../../../Image/btn/home.png" alt="Volver" title="Inicio">
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Footer">
                    Instituto Colombiano de Bienestar Familiar
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
