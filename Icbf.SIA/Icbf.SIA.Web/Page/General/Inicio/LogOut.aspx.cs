﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;

public partial class Page_General_Inicio_LogOut : GeneralWeb
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StopSessionParameters();
    }
}