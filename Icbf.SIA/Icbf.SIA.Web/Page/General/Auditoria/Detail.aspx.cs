﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;

using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Service;
using Icbf.Seguridad.Entity;

/// <summary>
/// Página que despliega los registros de auditoria depediendo de la transacción que se maneja en cada módulo
/// </summary>
public partial class Page_General_Auditoria_Detail : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    SeguridadService vRUBOService = new SeguridadService();
    
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Double pIdRegistro;
        String pNombrePrograma;
        if (Request.QueryString["sI"] != null)
        {
            if (Request.QueryString["pI"] != null)
            {
                if (!Double.TryParse(Request.QueryString["pI"].ToString(), out pIdRegistro))
                {
                    toolBar.MostrarMensajeError("Código Registro invalido.");
                }
                else
                {
                    if (Request.QueryString["sI"].ToString() == "")
                    {
                        toolBar.MostrarMensajeError("Código programa invalido.");
                    }
                    else
                    {
                        pNombrePrograma = Request.QueryString["sI"].ToString();
                        List<Auditoria> vLista = vRUBOService.ConsultarAuditoria(pNombrePrograma, pIdRegistro);
                        gvAuditoria.DataSource = vLista;
                        gvAuditoria.DataBind();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.EstablecerTitulos("Auditoria Detallada", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}