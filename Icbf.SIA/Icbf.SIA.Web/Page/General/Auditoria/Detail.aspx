﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_General_Auditoria_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowAG">
            <td>
                <asp:GridView runat="server" ID="gvAuditoria" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" CellPadding="0" Height="16px" 
                    EmptyDataText="Este registro no cuenta con auditoria detallada">
                    <Columns>
                        <asp:BoundField HeaderText="Fecha" DataField="Fecha" />
                        <asp:BoundField HeaderText="Usuario" DataField="NombreUsuario" />
                        <asp:BoundField HeaderText="Tipo Operacion" DataField="Operacion" />
                        <asp:BoundField HeaderText="Datos Operacion" DataField="ParametrosOperacion" />
                        <asp:BoundField HeaderText="Procedimiento Almacenado" DataField="Tabla" />
                        <asp:BoundField HeaderText="Direccion IP" DataField="DireccionIp" />
                        <asp:BoundField HeaderText="Navegador" DataField="Navegador" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
