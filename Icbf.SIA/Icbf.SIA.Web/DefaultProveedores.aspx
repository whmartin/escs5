﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultProveedores.aspx.cs"
    Inherits="DefaultProveedores" %>

<%@ Import namespace="Subkismet"%>
<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
<%@ Register TagPrefix="ajaxCT" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript" language="javascript" src="Scripts/pI.js"></script>
    <link type="text/css" href="Styles/log.css" rel="Stylesheet" />
    <script src="Scripts/jq.min.js" type="text/javascript"></script>
    <script src="Scripts/WS.js" type="text/javascript"></script>
    <script src="Scripts/coin-slider.min.js" type="text/javascript"></script>
    <link href="Styles/coin-slider-styles.css" rel="stylesheet" type="text/css" />
    <title>.: ICBF - Proveedores</title>
    <script type="text/javascript">
        var service = new WS("DefaultProveedores.aspx", WSDataType.json);
        $(document).ready(function () {
            $("#loGeneraciones_UserName").val("");
            $("#loGeneraciones_Password").val("");
            //document.getElementById('loGeneraciones_LoginButton').setAttribute("disabled", true);
            //            service.call("RetornarContador", {}, true, function (data) {
            //                if (data[0] > data[1]) {
            //                    $("#CaptchaDiv").show();
            //                }
            //            });

                        $.ajax({
                            type: "POST",
                            url: "DefaultProveedores.aspx/RetornarContador",
                            data: "{}",
                            contentType: "application/json; chartset:utf-8",
                            dataType: "json",
                            success:
                                    function (data) {
                                        if (data.d == "1") {
                                            $("#CaptchaDiv").show();
                                        }
                                    },
                            async: true,
                            cache: false
                        });


            $('#coin-slider').coinslider({
                width: 225,
                height: 210
            });


            // APLICA WATERMARK SI EL TEXTBOX NO TIENE VALOR
            $("#loGeneraciones_UserName").each(function () {

                if ($(this).text() == "123456789")
                    $(this).val('').addClass('WaterMarkOn');
                else
                    if ($.trim($(this).val()) == '')
                        $(this).addClass('WaterMarkOn').val("123456789");
            });

            $("#loGeneraciones_Password").each(function () {

                if ($(this).text() == "Contraseña")
                    $(this).val('').addClass('WaterMarkOn');
                else
                    if ($.trim($(this).val()) == '')
                        $(this).addClass('WaterMarkOn').val("Contraseña");
            });

            // AL OBTENER EL FOCO LIMPIA TITLE Y QUITA CLASE DE MARCA
            $("#loGeneraciones_UserName").focus(function () {
                if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
            });

            $("#loGeneraciones_Password").focus(function () {
                if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
            });

            // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
            $("#loGeneraciones_Password").blur(function () {
                if ($(this).val() == '') {
                    $(this).addClass('WaterMarkOn').val("Contraseña");
                    document.getElementById('loGeneraciones_PasswordRequired').style.visibility = "visible";
                }
                else {
                    document.getElementById('loGeneraciones_PasswordRequired').style.visibility = "hidden";
                }


                //        if (esPaginaValida()) {
                //            document.getElementById("loGeneraciones_LoginButton").disabled = false;
                //            document.getElementById("loGeneraciones_LoginButton").className = "btnLog";
                //        }
                //        else {
                //            document.getElementById("loGeneraciones_LoginButton").disabled = true;
                //            document.getElementById("loGeneraciones_LoginButton").className = "btnLogDisabled";
                //        }

            });

            //            // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
            $("#loGeneraciones_UserName").blur(function () {
                if ($(this).val() == '')
                    $(this).addClass('WaterMarkOn').val("123456789");



                //

                //        if (esPaginaValida()) {
                //            document.getElementById("loGeneraciones_LoginButton").disabled = false;
                //            document.getElementById("loGeneraciones_LoginButton").className = "btnLog";
                //        }
                //        else {
                //            document.getElementById("loGeneraciones_LoginButton").disabled = "disabled";
                //            document.getElementById("loGeneraciones_LoginButton").className = "btnLogDisabled";
                //        }
            });

        });


        function validateEmail(sEmail) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(sEmail)) {
                return true;
            }
            else {
                return false;
            }
        }

        function ClientValidateUserName(source, arguments) {
            if (arguments.Value == '123456789') {
                arguments.IsValid = false;
            }
            else if (!validateEmail(arguments.Value)) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;

            }
        }


        function ClientValidatePassword(source, arguments) {
            if (arguments.Value == 'Contraseña')
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }



        function ImgRefreshTextCaptcha() {
            $("#captcha input").val("");
            return true;
        }

                function ClientValidate(source, arguments) {
                    if (arguments.Value == '123456789') {
                        arguments.IsValid = false;
                    } else {
                        arguments.IsValid = true;
                    }
                }


        function esPaginaValida() {
            if (Page_ClientValidate('loGeneraciones')) {
                return true;
            }
            else {

                return false;
            }
        }
    </script>
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
        
        
        .ModalWindow
        {
            border: solid1px#c0c0c0;
            background: #89C566;
            padding: 0px10px10px10px;
            position: absolute;
        }
        .divModalDialog
        {
            margin: 5px;
            text-align: center;
        }
        .rfvalidatorCodigo
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: Red;
        }
        .botonLog
        {
            color: White;
            background-color: #66C42F;
            border-color: #66C42F;
            height: 29px;
            width: 200px;
            font-size: 12px;
            padding: 0px 10px 0px 10px;
            margin: 5px 0px 5px 0px;
        }
        .lblMensage
        {
            font-family: Verdana;
            font-size: small;
            color: Black;
            font-weight: bold;
        }
        .cssDivMsg
        {
            text-align: justify;
            width: 225px;
        }
        .CustomValidatorCalloutStyle div, .CustomValidatorCalloutStyle td
        {
            border: solid 1px white;
            background-color: #B4C800;
            color: white;
            font-size: 11px;
        }
        .style1
        {
        }
    </style>
</head>
<body style="font-family: Verdana; font-size: smaller; color: White" onload="SetFocus('loGeneraciones_UserName');">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class='centrar'>
        <table cellpadding="0" cellspacing="0" class="Loggin">
            <tr>
                <td>
                    <div style="width:225px;height:210px;">
                        <div style="width:225px;height:210px;">
                            <div id='LogoDiv' style="width:225px;height:210px;">
                                <img alt="logo" src="Image/Proveedores/imgLoginProveedores.jpg" style="width:225px;height:210px;" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="cssDivMsg">
                        <asp:Label ID="lblMsg" runat="server" CssClass="lblMensage"></asp:Label>
                    </div>
                </td>
                <td class="user">
                    <table cellpadding="5" cellspacing="0" class="labels" width="100%">
                        <tr>
                            <td>
                                <img alt="logo" src="Image/Proveedores/LogoProveedores.jpg" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Login ID="loGeneraciones" runat="server" Width="100%" LabelStyle-Width="30%"
                                    CssClass="labels input_byclass" DisplayRememberMe="False"
                                    UserNameLabelText="" OnLoggedIn="OnLoggedIn"
                                    OnLoginError="OnLoginError" ForeColor="Black" Height="120px"
                                    FailureText="La contraseña es incorrecta, inténtelo de nuevo." LoginButtonImageUrl="~/Image/btn/apply.png"
                                    LoginButtonType="Button" OnLoggingIn="OnLoggingIn">
                                    <LabelStyle Width="30%"></LabelStyle>
                                    <LayoutTemplate>
                                        <table cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" style="height: 120px; width: 100%;">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblUser" runat="server" Text="Usuario *" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="UserName" runat="server" CssClass="caja" Width="175px" Height="20px"
                                                                    MaxLength="50"></asp:TextBox>
                                                                <asp:CustomValidator ID="customUserNameValidator" runat="server" ControlToValidate="UserName"
                                                                    Text="*" ValidationGroup="loGeneraciones" ErrorMessage="Ingrese usuario" CssClass="rfvalidatorCodigo"
                                                                    ClientValidationFunction="ClientValidate" Display="Dynamic" />
                                                               <%-- <asp:RegularExpressionValidator runat="server" ID="UserNameRequired" ErrorMessage="El usuario debe corresponder a la estructura del correo electrónico que usted registro."
                                                                    ControlToValidate="UserName" SetFocusOnError="true" ValidationGroup="loGeneraciones"
                                                                    CssClass="rfvalidatorCodigo" ValidationExpression="^[0-9a-zA-Z_\-\.]+@[0-9a-zA-Z\-\.]+\.[a-zA-Z]{2,4}$"
                                                                    Display="Dynamic">*</asp:RegularExpressionValidator>--%>
                                                                <asp:CompareValidator runat="server" ID="CVNameUserMark" ControlToValidate="UserName" Text="*"
                                                                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="loGeneraciones"  CssClass="rfvalidatorCodigo"
                                                                 ForeColor="Red" Operator="NotEqual" ValueToCompare="123456789" Display="Dynamic" ></asp:CompareValidator>

                                                              <%--  <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="server" TargetControlID="UserNameRequired"
                                                                    WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                    PopupPosition="TopLeft" />--%>
                                                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="CVNameUserMark"
                                                                    WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                    PopupPosition="TopLeft" />
                                                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="customUserNameValidator"
                                                                    WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                    PopupPosition="TopLeft" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblPassword" runat="server" Text="Contraseña *" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="Password" runat="server" CssClass="caja" TextMode="Password" Width="175px"
                                                                    Height="20px" MaxLength="50"></asp:TextBox>
                                                                <asp:CustomValidator ID="PasswordRequired" runat="server" ClientValidationFunction="ClientValidatePassword"
                                                                    ControlToValidate="Password" CssClass="rfvalidatorCodigo" ErrorMessage="Ingrese la contraseña"
                                                                    ForeColor="Red" ValidationGroup="loGeneraciones">*</asp:CustomValidator>
                                                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtenderPwd" runat="server" TargetControlID="PasswordRequired"
                                                                    WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                    PopupPosition="TopLeft" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="rfvalidatorCodigo" colspan="2" style="color: Red;">
                                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="text-align: center">
                                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btnLog"
                                                                    Text="Iniciar Sesión" ValidationGroup="loGeneraciones" Width="100%" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <%--                                                                <asp:HyperLink ID="CreateUserLink" runat="server" NavigateUrl="~/Account/RegisterProveedor.aspx"></asp:HyperLink>--%>
                                                                <asp:LinkButton ID="lnkRegister" runat="server" CausesValidation="false" Text="Registra tu cuenta de usuario"
                                                                    OnClick="lnkRegister_Click" />
                                                                <br />
                                                                <%--<asp:HyperLink ID="PasswordRecoveryLink" runat="server" NavigateUrl="~/RecoverPasswordProveedores.aspx">Olvidaste tu clave?</asp:HyperLink>--%>
                                                                <asp:LinkButton ID="PasswordRecoveryLinkButton" runat="server" CausesValidation="false"
                                                                    OnClick="PasswordRecoveryLinkButton_Click" Text="¿Olvidaste tu constraseña?" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <FailureTextStyle CssClass="rfvalidatorCodigo" />
                                    <LoginButtonStyle CssClass="btnLog" />
                                    <TextBoxStyle CssClass="caja" />
                                    <ValidatorTextStyle CssClass="rfvalidatorCodigo" />
                                </asp:Login>
                                <table cellpadding="0" style="width: 100%;">
                                    <tr align="left">
                                        <td class="style1">
                                            <asp:Label ID="LblVersion" runat="server" Text=""></asp:Label>
                                           <%-- <asp:LinkButton ID="GetActivationCodeLink" runat="server">Obtén tu código de activación</asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="CaptchaDiv"  style="display: none;">
                                    <table>
                                        <tr>
                                            <td>
                                                <label>
                                                    Si no puede visualizar la imagen vuelva a recargarla</label>
                                                <br />
                                                <asp:ImageButton ID="imgRefreshCaptcha" runat="server" ImageUrl="~/Image/btn/refresh.png"
                                                    OnClientClick="return ImgRefreshTextCaptcha();" Width="10%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="udpCaptcha" runat="server" UpdateMode="Conditional">
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="imgRefreshCaptcha" EventName="Click" />
                                                    </Triggers>
                                                    <ContentTemplate>
                                                        <sbk:CaptchaControl ID="captcha" runat="server" ErrorMessage=" : Código invalido"
                                                            CaptchaLength="5" LayoutStyle="Vertical" Display="Dynamic" SetFocusOnError="true" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red" Width="100%"
                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;<br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="5" cellspacing="0" class="IconosT">
                                    <tr>
                                        <td>
                                            <a href="http://www.dps.gov.co" target="_blank">
                                                <img alt="DPS" src="Image/loggin/DPS.jpg" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.presidencia.gov.co" target="_blank">
                                                <img alt="prosperidad" src="Image/loggin/Prosperidad.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.icbf.gov.co" target="_blank">
                                                <img alt="icbfDigital" src="Image/loggin/IcbfDigital.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.icbf.gov.co" target="_blank">
                                                <img alt="icbf" style="text-decoration: none" src="Image/loggin/LogoIcbf.png" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td class="FooterL">
                    Instituto Colombiano de Bienestar Familiar
                </td>
                <td class="FooterR">
                    Todos los Derechos Reservados - &copy;
                    <%: DateTime.Now.Year %>
                </td>
            </tr>
        </table>
    </div>
    <div>
       <%-- <asp:Panel ID="codigoActivacion" runat="server" Height="200px" Width="400px" CssClass="ModalWindow">
            <div class="divModalDialog">
                <h3>
                    Obtén tu código de activación
                </h3>
                <p>
                    Ingrese su correo y contraseña
                </p>
                <div align="center">
                    <table>
                        <tr>
                            <td>
                                Correo
                            </td>
                            <td>
                                <asp:TextBox ID="txtCorreo" runat="server" Width="148px" CssClass="input"></asp:TextBox><asp:RequiredFieldValidator
                                    ID="rfvtxtCorreo" runat="server" CssClass="rfvalidatorCodigo" ErrorMessage="*"
                                    ControlToValidate="txtCorreo"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contraseña
                            </td>
                            <td>
                                <asp:TextBox ID="txtContrasena" runat="server" Width="148px" CssClass="input" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                                    ID="rfvtxtContrasena" runat="server" CssClass="rfvalidatorCodigo" ErrorMessage="*"
                                    ControlToValidate="txtContrasena"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Label ID="lbCodigo" runat="server" Visible="False" />
                <br />
                <table align="center">
                    <tr>
                        <td>
                            <asp:Button ID="btnOkCodigo" runat="server" Text="Obtener" CssClass="botonLog" Width="100px"
                                OnClick="btnOkCodigo_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" runat="server" CausesValidation="false" Text="Salir"
                                CssClass="botonLog" Width="100px" OnClick="btnCancelar_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>--%>
       <%-- <ajaxCT:ModalPopupExtender ID="mdpCodActivacion" runat="server" TargetControlID="GetActivationCodeLink"
            PopupControlID="codigoActivacion" BackgroundCssClass="modalBackground">
        </ajaxCT:ModalPopupExtender>--%>
    </div>
    </form>
</body>
</html>
