﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecoverPasswordProveedores.aspx.cs"
    Inherits="RecoverPasswordProveedores" MasterPageFile="~/General/General/Master/MasterProveedores.master"
    MaintainScrollPositionOnPostback="true" Async="true" %>

<%@ Import Namespace="System.ComponentModel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript" src="Scripts/sP.js"></script>
    <script type="text/javascript" language="javascript" src="Scripts/pI.js"></script>
    <script src="Scripts/jq.min.js" type="text/javascript"></script>
    <script src="Scripts/WS.js" type="text/javascript"></script>
    <link type="text/css" href="Styles/log.css" rel="Stylesheet" />
    <link href="Styles/coin-slider-styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .rfvalidatorCodigo
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: Red;
        }
        .FondoTbl
        {
            border: solid1px#c0c0c0;
            background: #89C566;
            padding: 0px10px10px10px;
        }
        .style1
        {
            height: 32px;
        }
        .botonLog
        {
            color: White;
            background-color: #66C42F;
            border-color: #66C42F;
            height: 29px;
            width: 100px;
            font-size: 12px;
            padding: 0px 10px 0px 10px;
            margin: 5px 0px 5px 0px;
        }
        
        .divContenido
        {
            width: 500px;
            height: 360px;
            text-align: center;
            vertical-align: middle;
            display: table-cell;
        }
        
        table tr.rowA
        {
            background-color: #89C566;
            color: inherit;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000000;
            text-align: left;
        }
        table tr.rowB
        {
            background-color: #89C566;
            color: inherit;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000000;
            text-align: left;
        }
        
        .user
        {
            padding-left: 20px;
            padding-top: 50px;
            vertical-align: middle;
        }
        
        .CustomValidatorCalloutStyle div, .CustomValidatorCalloutStyle td
        {
            border: solid 1px white;
            background-color: #B4C800;
            color: white;
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            // APLICA WATERMARK SI EL TEXTBOX NO TIENE VALOR
            $("#ContentPrincipal_prGeneraciones_UserNameContainerID_UserName").each(function () {
                if ($(this).text() == "alguien@example.com")
                    $(this).val('').addClass('WaterMarkOn');
                else
                    if ($.trim($(this).val()) == '')
                        $(this).addClass('WaterMarkOn').val("alguien@example.com");
            });



            // AL OBTENER EL FOCO LIMPIA TITLE Y QUITA CLASE DE MARCA
            $("#ContentPrincipal_prGeneraciones_UserNameContainerID_UserName").focus(function () {
                if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
            });

            // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
            $("#ContentPrincipal_prGeneraciones_UserNameContainerID_UserName").blur(function () {
                if ($(this).val() == '')
                    $(this).addClass('WaterMarkOn').val("alguien@example.com");
                

            });
        });

        function ClientValidateUserName(source, arguments) {
            if (arguments.Value == 'alguien@example.com') {
                arguments.IsValid = false;
            }
            else if (!validateEmail(arguments.Value)) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function validateEmail(sEmail) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(sEmail)) {
                return true;
            }
            else {
                return false;
            }
        }

        function ClientValidate(source, arguments) {
            if (arguments.Value == 'alguien@example.com') {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPrincipal" runat="Server">
    <table align="center">
        <tr>
            <td>
                <div class="divContenido">
                    <table align="center" style="vertical-align: middle;">
                        <tr>
                            <td align="center" valign="middle">
                                <asp:PasswordRecovery ID="prGeneraciones" runat="server" SubmitButtonText="Enviar clave"
                                    SuccessText="Tu contraseña ha sido enviada a tu correo electrónico." UserNameInstructionText="Ingresa tu correo electrónico para que recibas tu contraseña"
                                    UserNameLabelText="Correo electrónico:" UserNameRequiredErrorMessage="El correo electrónico es requerido."
                                    UserNameTitleText="¿Olvidaste tu contraseña?" OnVerifyingUser="PasswordRecovery_VerifyingUser"
                                    BackColor="#89C566" OnSendingMail="PasswordRecovery_SendingMail" Height="246px"
                                    Width="243px">
                                    <SuccessTemplate>
                                        <table cellpadding="1" cellspacing="0" style="border-collapse: collapse; background-color: #89C566;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" style="background-color: #89C566;">
                                                        <tr>
                                                            <td>
                                                                Tu contraseña ha sido enviada a tu correo electr&oacute;nico.
                                                                <br />
                                                                <br />
                                                                <asp:ImageButton ID="btnRegresar" runat="server" ToolTip="Regresar" PostBackUrl="~/DefaultProveedores.aspx"
                                                                    ImageUrl="~/Image/btn/logout.png" Height="29px" Width="29px"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </SuccessTemplate>
                                    <UserNameTemplate>
                                        <table cellpadding="1" cellspacing="0" style="border-collapse: collapse; background-color: #89C566;
                                            text-align: center;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="2" style="background-color: #89C566; text-align: center;">
                                                        <tr>
                                                            <td align="left" class="sT">
                                                                ¿Olvidaste tu contraseña?
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="UserName">Ingrese su dirección de correo electr&oacute;nico:</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowA">
                                                            <td>
                                                                <asp:TextBox ID="UserName" runat="server" Width="200px"></asp:TextBox>
                                                                <asp:CustomValidator ID="customUserNameValidator" runat="server" ControlToValidate="UserName"
                                                                    Text="*" ValidationGroup="PasswordRecovery1" ErrorMessage="Correo electrónico en blanco, Registre un correo electrónico válido"
                                                                    CssClass="rfvalidatorCodigo" ClientValidationFunction="ClientValidate" />
                                                                <asp:RegularExpressionValidator runat="server" ID="UserNameRequired" ErrorMessage="Correo electrónico no tiene la estructura válida, inténtelo de nuevo"
                                                                    ControlToValidate="UserName" SetFocusOnError="true" ValidationGroup="PasswordRecovery1"
                                                                    CssClass="rfvalidatorCodigo" ValidationExpression="^[0-9a-zA-Z_\-\.]+@[0-9a-zA-Z\-\.]+\.[a-zA-Z]{2,4}$"
                                                                    Display="Dynamic">*</asp:RegularExpressionValidator>
                                                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="server" TargetControlID="UserNameRequired"
                                                                    WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle" />
                                                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="customUserNameValidator"
                                                                    WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle" />
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr align="right">
                                                            <td>
                                                                <asp:ImageButton ID="btnCancelar" runat="server" OnClick="btnCancelar_Click" CausesValidation="false"
                                                                    Text="Cancelar" ImageUrl="~/Image/btn/logout.png" Width="29px" Height="29px"
                                                                    ToolTip="Regresar"></asp:ImageButton>
                                                                &nbsp;
                                                                <asp:ImageButton ID="btnEnviar" runat="server" CausesValidation="true" CommandName="Submit"
                                                                    Text="Enviar" ValidationGroup="PasswordRecovery1" ImageUrl="~/Image/btn/apply.png"
                                                                    Width="29px" Height="29px" ToolTip="Enviar"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </UserNameTemplate>
                                </asp:PasswordRecovery>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
