﻿using System;
using System.Web.Security;
using Icbf.Utilities.Presentation;

/// <summary>
/// Maneja los eventos de ingreso y validación del código de activación
/// </summary>
public partial class Account_CodeActivationProveedores : GeneralWeb
{
    MembershipUser usuario;

   /// <summary>
   /// Manejador del evento carga la Pàgina
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["codAct"] != null)
        {
            string codValido = Request.QueryString["codAct"].ToString();
            if (codValido.Equals("0"))
                this.lblMsg.Visible = true;
        }


        usuario = (MembershipUser)Session["usuarioProveedorLog"];
        this.mdpCodActivacion.Show();

    }

    /// <summary>
    /// Manejador del evento Cancelar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Session.Remove("usuarioProveedorLog");
        NavigateTo(@"~/Default.aspx", true);
    }

    /// <summary>
    /// Manejador del evento del botón btnOkCodigo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOkCodigo_Click(object sender, EventArgs e)
    {
        if (this.txtCodigo.Text.Equals(usuario.Comment))
        {
            usuario.IsApproved = true;
            Membership.UpdateUser(usuario);
            //Login Por Código
            if (Membership.ValidateUser(usuario.UserName, usuario.GetPassword()))
            {
                Session.Remove("usuarioOferenteLog");
                ValidateUser(usuario.UserName, usuario.GetPassword());
                FormsAuthentication.RedirectFromLoginPage(usuario.UserName, false);
                string currentApp = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
                
                    NavigateTo(@"~/General/General/Master/MasterPrincipal.aspx", true);
                //NavigateTo(@"~/Page/Proveedor/blank.aspx", true);
            }
            else
            {
                NavigateTo(@"~/Default.aspx", true);
            }
        }
        else
        {
            NavigateTo(@"CodeActivationProveedores.aspx?codAct=0", true);
        }
    }
}