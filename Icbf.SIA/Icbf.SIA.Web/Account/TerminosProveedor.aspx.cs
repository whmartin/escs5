﻿using System;
using System.IO;
using System.Web;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

/// <summary>
/// Página que se despliega en ventana emergente que carga el documento de acuerdos y permite descargarlo en PDF.
/// </summary>
public partial class Account_TerminosProveedor : System.Web.UI.Page
{
    ProveedorService vProveedorService = new ProveedorService();

    #region eventos
    /// <summary>
    /// Manejador del evento Carga Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Acuerdos vAcuerdo = vProveedorService.ConsultarAcuerdoVigente();
            Message.InnerHtml = vAcuerdo.ContenidoHtml;
        }
    }

    /// <summary>
    /// Manejador del evento para el botón Exportar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        var html = Message.InnerHtml;

        //setting ContentType
        Response.ContentType = "application/pdf";

        //setting up PDF header and filename
        Response.AddHeader("content-disposition", "attachment;filename=TerminosCondiciones.pdf");

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //Page site and margin left, right, top, bottom is defined
        var pdfDocument = new Document(PageSize.LETTER, 20f, 20f, 20f, 20f);

        //pdf writer will get instance of document
        PdfWriter.GetInstance(pdfDocument, Response.OutputStream);

        pdfDocument.AddTitle("Términos Y Condiciones");

        pdfDocument.Open();

        pdfDocument.NewPage();

        foreach (var E in HTMLWorker.ParseToList(new StringReader(html), new StyleSheet()))
            pdfDocument.Add(E);
        
        pdfDocument.Close();

        Response.Write(pdfDocument);

        Response.End();
    }

    /// <summary>
    /// Manejador del evento del botón Imprimir
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnImprimir_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/Page/Oferente/Terminos/Add.aspx");
    }
    protected void btnSalir_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";
        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }
    #endregion
}