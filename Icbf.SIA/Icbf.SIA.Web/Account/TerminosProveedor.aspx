﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TerminosProveedor.aspx.cs" Inherits="Account_TerminosProveedor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Términos y Condiciones</title>
    <link href="../Page/Oferente/../../Styles/layout-default.css" rel="stylesheet" type="text/css" />
    <link href="../Page/Oferente/../../Styles/Oferentes.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td colspan="3" style="text-align: center">
                <strong>Acuerdo y Condiciones</strong>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: auto; width: 90%; height: 500px; border:1px solid #B4C800;">
                    <span id="Message" runat="server"></span> 
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Button Text="Salir" ID="btnSalir" runat="server" onclick="btnSalir_Click" />
            </td>
            <td style="text-align: center">
                <%--<asp:Button Text="Imprimir" ID="Button1" runat="server" OnClientClick="window.print();" />--%>
            </td>
            <td style="text-align: center">
                <asp:Button Text="Exportar PDF" ID="btnExportar" runat="server" OnClick="btnExportar_Click" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
