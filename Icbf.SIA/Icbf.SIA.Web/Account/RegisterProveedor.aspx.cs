﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Linq;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;

/// <summary>
/// Página para manejar el registro de cuentas de usuario (proveedores) nuevos
/// </summary>
public partial class Account_RegisterProveedor : GeneralWeb
{
    #region "Variable y Constantes"
    MasterProveedores toolBar;
    SIAService vRUBOService = new SIAService();
    OferenteService vOferenteService = new OferenteService();

    #endregion

    #region "Funciones y Procedimientos"

    /// <summary>
    /// Inicializa variables
    /// </summary>
    private void Inicial()
    {
        captcha.Display = ValidatorDisplay.None;
        txtCorreoElectronico.Text = string.Empty;
        txtNombreUsuario.Text = string.Empty;
        LLenarTipoPersona();
        HabilitaJuridica(false);
        HabilitaNatural(false);
        HabilitaResto(false);

        var vSeguridadService = new SeguridadService();
        lbParaQueRegistro.Text = vSeguridadService.ConsultarParametro(13) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(13).ValorParametro;

        lbPoliticaContrasena.Text = vSeguridadService.ConsultarParametro(16) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(16).ValorParametro;
    }

    /// <summary>
    /// Inicializa constructo del servicio y el objeto de la master
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (MasterProveedores)this.Master;
            new SeguridadService();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Crea un usuario para ingreso al sistema
    /// </summary>
    private void Guardar()
    {
        try
        {
            //captcha.Validate();
            //if (captcha.IsValid == false)
            //{

            //    captcha.ErrorMessage = @"Los caracteres ingresados no corresponden con la imagen mostrada, inténtelo de nuevo";
            //    toolBar.MostrarMensajeError("Los caracteres ingresados no corresponden con la imagen mostrada, inténtelo de nuevo");

            //    return;
            //}

            // Comprueba si el número de identificación ya existe para otra cuenta de usuario.
            bool existeId = ExisteIdentificacion(Convert.ToInt32(ddlTipoDoc.SelectedValue), txtNumeroDoc.Text.Trim());
            bool existeCorreoElectrnico = false;
            MembershipUserCollection oMuColection = Membership.FindUsersByEmail(txtCorreoElectronico.Text.Trim());
            if (oMuColection.Count > 0) { existeCorreoElectrnico = true; }
            string vProviderKey="";
            bool existeIdParaInterno = ExisteIdentificacionParaInterno(Convert.ToInt32(ddlTipoDoc.SelectedValue), txtNumeroDoc.Text.Trim(),out vProviderKey);

            if (existeId == true && existeCorreoElectrnico == true)
            {
                toolBar.MostrarMensajeError("La identificación y correo electrónico ya están asociados");
                SetFocus(this.txtNumeroDoc);
                return;
            }
            else
            {
                if (existeId)
                {
                    toolBar.MostrarMensajeError("La identificación ya está asociada para otra cuenta de usuario");
                    SetFocus(this.txtNumeroDoc);
                    return;
                }
                else if (existeIdParaInterno && !string.IsNullOrEmpty(vProviderKey))
                {
                    toolBar.MostrarMensajeError("La identificación ya está asociada a otro Tercero.");
                    SetFocus(this.txtNumeroDoc);
                    return;
                }
            }

            var vUsuario = new Usuario
            {
                NumeroDocumento = txtNumeroDoc.Text.ToUpper().Trim(),
                DV = txtDV.Text.Trim(),
                PrimerNombre = txtPrimerNombre.Text.ToUpper().Trim(),
                SegundoNombre = txtSegundoNombre.Text.ToUpper().Trim(),
                PrimerApellido = txtPrimerApellido.Text.ToUpper().Trim(),
                SegundoApellido = txtSegundoApellido.Text.ToUpper().Trim(),
                CorreoElectronico = txtCorreoElectronico.Text.Trim(),
                TelefonoContacto = txtCelular.Text.Trim(),
                NombreUsuario = txtNombreUsuario.Text.Trim(),
                Contrasena = txtContrasena.Text,
                IdTipoDocumento = Convert.ToInt32(ddlTipoDoc.SelectedValue),
                IdTipoPersona = Convert.ToInt32(ddlTipoPersona.SelectedValue),
                RazonSocial = txtRazonSocial.Text.ToUpper().Trim(),
                Rol = "PROVEEDORES",
                CodigoValidacion = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 15),
                Estado = true,
                UsuarioCreacion = "Administrador",
                ExisteTercero = !String.IsNullOrEmpty(vProviderKey)
            };

            Tercero vTercero = new Tercero();
            vTercero = vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(vUsuario.IdTipoDocumento, vUsuario.NumeroDocumento);

            if (vTercero.IdTercero != 0)
            {
                if (vTercero.Email.ToLower() != vUsuario.CorreoElectronico.ToLower())
                {
                    vUsuario.CorreoElectronico = vTercero.Email.ToUpper();
                    vTercero.RazonSocial = txtRazonSocial.Text.ToUpper().Trim();
                    vTercero.PrimerNombre = txtPrimerNombre.Text.ToUpper().Trim();
                    vTercero.SegundoNombre = txtSegundoNombre.Text.ToUpper().Trim();
                    vTercero.PrimerApellido = txtPrimerApellido.Text.ToUpper().Trim();
                    vTercero.SegundoApellido = txtSegundoApellido.Text.ToUpper().Trim();
                    vTercero.Celular = vUsuario.TelefonoContacto;
                    vUsuario.NombreUsuario = vTercero.Email.ToUpper();
                    vTercero.UsuarioModifica = txtCorreoElectronico.Text;

                    vOferenteService.ModificarTercero(vTercero);

                    hfExisteTercero.Value = "1";
                }
            }                           

            string name = vUsuario.IdTipoPersona == 1 ? vUsuario.PrimerNombre + " " + vUsuario.SegundoNombre + " " + vUsuario.PrimerApellido + " " + vUsuario.SegundoApellido : vUsuario.RazonSocial;

            var vSeguridadService = new SeguridadService();

            string asunto = vSeguridadService.ConsultarParametro(7) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(7).ValorParametro;
            string textoBienvenida = vSeguridadService.ConsultarParametro(21) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(21).ValorParametro;
            string paso1 = vSeguridadService.ConsultarParametro(17) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(17).ValorParametro;
            string paso2 = vSeguridadService.ConsultarParametro(18) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(18).ValorParametro;
            string paso3 = vSeguridadService.ConsultarParametro(19) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(19).ValorParametro;
            string paso4 = vSeguridadService.ConsultarParametro(20) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(20).ValorParametro;


            string remitente = ConfigurationManager.AppSettings["RemitenteProveedores"];
            string linkSitioWeb = ConfigurationManager.AppSettings["URLProveedores"];
            string linkSitioWebDenuncias = ConfigurationManager.AppSettings["URLDenuncias"];

            var vResultado = vRUBOService.InsertarUsuario(vUsuario);

            switch (vResultado)
            {
                case 0:
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    break;
                case 1:
                    Inicial();
                    EnviarMensajeBienvenida(remitente, vUsuario.CorreoElectronico, asunto, linkSitioWeb, name, vUsuario.NombreUsuario, vUsuario.Contrasena, vUsuario.CodigoValidacion, textoBienvenida, paso1, paso2, paso3, paso4, linkSitioWebDenuncias);
                    break;
                default:
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó más registros de los esperados, verifique por favor.");
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Verifica la axistencia de una identificación
    /// </summary>
    /// <param name="pTipoDocumento"></param>
    /// <param name="pDocumento"></param>
    /// <returns></returns>
    private bool ExisteIdentificacion(int pTipoDocumento, string pDocumento)
    {
        Usuario vUsuario = vRUBOService.ConsultarUsuario(pTipoDocumento, pDocumento);
        if (vUsuario != null)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Verifica la axistencia de una identificación en un Tercero creado por Interno
    /// </summary>
    /// <param name="pTipoDocumento"></param>
    /// <param name="pDocumento"></param>
    /// <returns></returns>
    private bool ExisteIdentificacionParaInterno(int pTipoDocumento, string pDocumento, out string pProviderKey)
    {
        pProviderKey = "";
        //ConsultarTerceroTipoNumeroIdentificacion
        Tercero vTercero= vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(pTipoDocumento, pDocumento);
        if (vTercero.IdTercero != 0)
        {            
            pProviderKey = vTercero.ProviderUserKey;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Inicializa valores para control lista desplegable Tipo Persona
    /// </summary>
    private void LLenarTipoPersona()
    {
        ddlTipoPersona.Items.Clear();
        ddlTipoPersona.Items.Insert(0, new ListItem("Seleccione", "-1"));
        ddlTipoPersona.Items.Insert(1, new ListItem("Natural", "1"));
        ddlTipoPersona.Items.Insert(2, new ListItem("Juridica", "2"));
        ddlTipoPersona.Items.Insert(3, new ListItem("Consorcio", "3"));
        ddlTipoPersona.Items.Insert(4, new ListItem("Unión Temporal", "4"));

    }

    /// <summary>
    /// Habilita visibilidad de controles considerados el resto
    /// </summary>
    /// <param name="pResto"></param>
    private void HabilitaResto(bool pResto)
    {
        pnlCell.Visible = pResto;
        ddlTipoDoc.Visible = pResto;
        rfvTipoDoc.Visible = pResto;
        lblTipoDocumento.Visible = pResto;
        txtNumeroDoc.Visible = pResto;
        rfvNumeroDoc.Visible = pResto;
        revNumDocCero.Visible = pResto;
        //revCC.Visible = pResto;
        //cvCC.Visible = pResto;
        //revCE.Visible = pResto;
        //cvCE.Visible = pResto;
        revPA.Visible = pResto;
        revNIT.Visible = pResto;
        //cvNIT.Visible = pResto;
        lblNumeroDoc.Visible = pResto;
        txtCelular.Text = string.Empty;
        txtNumeroDoc.Text = string.Empty;
    }

    /// <summary>
    /// Habilita controles para el caso de una personalidad natural
    /// </summary>
    /// <param name="pNatural"></param>
    private void HabilitaNatural(bool pNatural)
    {
        pnlNatural.Visible = pNatural;
        txtPrimerApellido.Text = string.Empty;
        txtPrimerNombre.Text = string.Empty;
        txtSegundoApellido.Text = string.Empty;
        txtSegundoNombre.Text = string.Empty;
        revNIT.Enabled = false;

        if (pNatural)
        {
            txtNumeroDoc.MaxLength = 20;
            txtNumeroDoc.AutoPostBack = false;
            txtDV.Text = string.Empty;
        }
    }

    /// <summary>
    /// Habilita controles para el caso de un tipo de razón social jurìdica
    /// </summary>
    /// <param name="pJuridica"></param>
    private void HabilitaJuridica(bool pJuridica)
    {
        txtRazonSocial.Visible = pJuridica;
        lblRazonSocial.Visible = pJuridica;
        rfvRazonSocial.Visible = pJuridica;
        txtDV.Visible = pJuridica;
        txtRazonSocial.Text = string.Empty;
        txtDV.Text = string.Empty;
        revNIT.Enabled = true;
        //cvCC.Enabled = false;

        if (pJuridica)
        {
            txtNumeroDoc.MaxLength = 9;
            txtNumeroDoc.AutoPostBack = true;
        }
    }

    /// <summary>
    /// Llena Tipo de documento
    /// </summary>
    /// <param name="pTipoPersona"></param>
    private void LLenarTipoDocumento(string pTipoPersona)
    {

        ddlTipoDoc.Items.Clear();

        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();


        if (pTipoPersona == "1")
        {
            foreach (TipoDocumento tD in vLTipoDoc)
            {
                if ((tD.Codigo == "CC") || (tD.Codigo == "CE"))
                {
                    ddlTipoDoc.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString()));
                }
                //se incluye Carné Diplomático
                if ((tD.Codigo == "CD"))
                {
                    ddlTipoDoc.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString()));
                }
            }
            ddlTipoDoc.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        else if (pTipoPersona == "2" || pTipoPersona == "3" || pTipoPersona == "4")
        {
            foreach (TipoDocumento tD in vLTipoDoc)
            {
                if (tD.Codigo == "NIT")
                {
                    ddlTipoDoc.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString()));
                }
            }
            ddlTipoDoc.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }

    }

    /// <summary>
    /// Cambia la visibilidad de controles de acuerdo al tipo de documentación 
    /// </summary>
    private void CambiarValidadorDocumentoUsuario()
    {
        if (ddlTipoDoc.SelectedItem.Text == "CC")
        {
            this.cvCC.Enabled = true;
            this.cvCE.Enabled = false;
            this.revPA.Enabled = false;
            this.revNIT.Enabled = false;
            this.revCD.Enabled = false;

            txtNumeroDoc.MaxLength = 11;
            txtNumeroDoc.AutoPostBack = true;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.cvCE.Enabled = true;
            this.cvCC.Enabled = false;
            this.revPA.Enabled = false;
            this.revNIT.Enabled = false;
            this.revCD.Enabled = false;

            txtNumeroDoc.MaxLength = 6;
            txtNumeroDoc.AutoPostBack = true;

        }
        else if (ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.revPA.Enabled = true;
            this.cvCE.Enabled = false;
            this.cvCC.Enabled = false;
            this.revNIT.Enabled = false;
            this.revCD.Enabled = false;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "NIT")
        {
            this.revPA.Enabled = false;
            this.cvCE.Enabled = false;
            this.cvCC.Enabled = false;
            this.revNIT.Enabled = true;
            this.revCD.Enabled = false;
            
        }
        else if (ddlTipoDoc.SelectedItem.Text == "CARNÉ DIPLOMÁTICO")
        {
            this.revPA.Enabled = false;
            this.cvCE.Enabled = false;
            this.cvCC.Enabled = false;
            this.revNIT.Enabled = false;
            this.revCD.Enabled = true;

            txtNumeroDoc.MaxLength = 17;
            txtNumeroDoc.AutoPostBack = true;

        }
    }

    /// <summary>
    /// Efectùa un càlculo de variable DIV
    /// </summary>
    private void CalcularDiV()
    {
        int lenTxtNumeroDoc = 15 - txtNumeroDoc.Text.Length;

        if (lenTxtNumeroDoc >= 0)
        {
            var strDato = Left("000000000000000", lenTxtNumeroDoc) + txtNumeroDoc.Text;
            char[] strArray = (strDato.ToCharArray());

            int result = Convert.ToInt32(Convert.ToString(strArray[14])) * 3 +
                         Convert.ToInt32(Convert.ToString(strArray[13])) * 7 +
                         Convert.ToInt32(Convert.ToString(strArray[12])) * 13 +
                         Convert.ToInt32(Convert.ToString(strArray[11])) * 17 +
                         Convert.ToInt32(Convert.ToString(strArray[10])) * 19 +
                         Convert.ToInt32(Convert.ToString(strArray[9])) * 23 +
                         Convert.ToInt32(Convert.ToString(strArray[8])) * 29 +
                         Convert.ToInt32(Convert.ToString(strArray[7])) * 37 +
                         Convert.ToInt32(Convert.ToString(strArray[6])) * 41 +
                         Convert.ToInt32(Convert.ToString(strArray[5])) * 43 +
                         Convert.ToInt32(Convert.ToString(strArray[4])) * 47 +
                         Convert.ToInt32(Convert.ToString(strArray[3])) * 53 +
                         Convert.ToInt32(Convert.ToString(strArray[2])) * 59 +
                         Convert.ToInt32(Convert.ToString(strArray[1])) * 67 +
                         Convert.ToInt32(Convert.ToString(strArray[0])) * 71;

            int residuo = result % 11;

            if (residuo == 0)
            {
                txtDV.Text = "0";
            }
            else
            {
                if (residuo == 1)
                {
                    txtDV.Text = "1";
                }
                else
                {
                    residuo = 11 - residuo;
                    txtDV.Text = Convert.ToString(residuo);
                }
            }
        } //Fin if (lenTxtNumeroDoc >= 0)
        else
        {
            txtDV.Text = "";
        } // Fin else if (lenTxtNumeroDoc >= 0)
    }

    /// <summary>
    /// Delimita cadena
    /// </summary>
    /// <param name="param"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    private static string Left(string param, int length)
    {
        string result = param.Substring(0, length);
        return result;
    }

    private static string Right(string param, int length)
    {

        int value = param.Length - length;
        string result = param.Substring(value, length);
        return result;
    }

    #endregion

    #region "Eventos"

    /// <summary>
    /// Manejador del evento Pre_init de la Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {

        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        if (!Page.IsPostBack)
        {
            Inicial();
            if (Request.QueryString["correo"] != null)
            {
                this.txtCorreoElectronico.Text = Request.QueryString["correo"].ToString();
                this.txtNombreUsuario.Text = Request.QueryString["correo"].ToString();
            }
        }

    }

    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        KeepPassword();
        CambiarValidadorDocumentoUsuario();

        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumeroDoc.Text = String.Empty;
            this.txtNumeroDoc.Focus();
        }
    }

    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        KeepPassword();

        if (ddlTipoPersona.SelectedValue == "1")
        {
            LLenarTipoDocumento(ddlTipoPersona.SelectedValue);
            HabilitaJuridica(false);
            HabilitaNatural(true);
            HabilitaResto(true);
        }
        else if (ddlTipoPersona.SelectedValue == "2")
        {
            LLenarTipoDocumento(ddlTipoPersona.SelectedValue);
            HabilitaJuridica(true);
            HabilitaNatural(false);
            HabilitaResto(true);
        }
        else if (ddlTipoPersona.SelectedValue == "3" || ddlTipoPersona.SelectedValue == "4")
        {
            LLenarTipoDocumento(ddlTipoPersona.SelectedValue);
            HabilitaJuridica(true);
            HabilitaNatural(false);
            HabilitaResto(true);
        }
        else
        {
            HabilitaJuridica(false);
            HabilitaNatural(false);
            HabilitaResto(false);
        }

        if (ddlTipoPersona.SelectedValue != "-1")
        {
            ddlTipoDoc.Focus();
        }

    }

    private void EnviarMensajeBienvenida(string pDe, string pPara, string pAsunto, string pLinkUrl, string pNombre, string pUsuario, string pContrasena, string pCodigoValidacion, string pTextoBienvenida, string pPaso1, string pPaso2, string pPaso3, string pPaso4, string plinkSitioWebDenuncias)
    {

        var myMailMessage = new MailMessage();

        myMailMessage.From = new MailAddress(pDe);
        myMailMessage.To.Add(new MailAddress(pPara));
        myMailMessage.Subject = pAsunto;

        myMailMessage.IsBodyHtml = true;


        myMailMessage.Body = "<html><body><table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'><tr><td colspan='3' " +
                            " bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td width='10%' " +
                            " bgcolor='#81BA3D'>&nbsp;</td><td><table width='100%' border='0' align='center'><tr><td width='5%'>&nbsp;</td><td  " +
                            " align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr><tr><td>&nbsp;</td><td align='center'><strong> " + pAsunto +
                            " </strong></td><td>&nbsp;</td></tr><tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'> " +
                            " &nbsp;</td></tr><tr><td>&nbsp;</td><td><strong>Apreciado(a):</strong>&nbsp;" + pNombre + "</td><td>&nbsp;</td></tr><tr><td> " +
                            " &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>" + pTextoBienvenida + "</td><td> " +
                            " &nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr> " +
                            " <tr><td>&nbsp;</td><td><table width='100%' border='0'> " +
                            " <tr><td width='50%' align='right'><strong>Código de activación:</strong> </td><td width='50%'>" + pCodigoValidacion + "</td></tr></table></td><td> " +
                            " &nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td><strong>Activa tu cuenta</strong> " +
                            "  para empezar a utilizar el sistema, es muy <strong>fácil y rápido</strong></td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td> " +
                            " &nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>" + pPaso1 + "</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td> " + pPaso2 +
                            " </td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td> " + pPaso3 +
                            " </td></tr><tr><td>&nbsp;</td><td> " + pPaso4 +
                            " </td><td>&nbsp;</td></tr><tr><td>&nbsp; " +
                            " </td><td>&nbsp;</td><td>&nbsp;</td></tr><tr> " +
                            " <tr><td colspan = '3' align = 'center'  " +
                            " ><a href='" + plinkSitioWebDenuncias + "' target='_blank'>Denuncia de Bienes</a></td></tr> " +
                            " <td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>&nbsp;</td></tr></table></td><td width='10%' bgcolor='#81BA3D'> " +
                            " &nbsp;</td></tr><tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' align='center'  " +
                            " bgcolor='#81BA3D'>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo. </td></tr><tr> " +
                            " <td colspan='3' align='center' bgcolor='#81BA3D'>Si tienes alguna duda, puedes dirigirte a nuestra sección de  " +
                            " <a href='" + pLinkUrl + "' target='_blank'>Asistencia y Soporte.</a></td></tr><tr><td colspan='3' align='center'  " +
                            " bgcolor='#81BA3D'>&nbsp;</td></tr></table></body></html>";


        var mySmtpClient = new SmtpClient();

        object userState = myMailMessage;

        mySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

        try
        {
            mySmtpClient.SendAsync(myMailMessage, userState);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo de activación fue cancelado");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
        else
        {
            CleanPassword();

            if (hfExisteTercero.Value == "1")
            { 
                hfExisteTercero.Value = "0";
                toolBar.MostrarMensajeGuardado("El correo electrónico ingresado no corresponde al existente en nuestro sistema, comuniquese con la línea gratuita nacional ICBF: 01 8000 91 80 80");
            }
            else
                toolBar.MostrarMensajeGuardado("Registrados sus datos exitosamente.  Ingrese a su correo electrónico para conocer su código de activación. Si no encuentra nuestro mensaje en su bandeja de entrada, verifique en la carpeta correos no deseados y agréguenos como remitente seguro");
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();

        captcha.CaptchaChars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";

        var guidKey = Guid.NewGuid().ToString();

        var sbScript = "";
        sbScript += "<script language='javascript'>";
        sbScript += "   RefreshTextCaptcha()";
        sbScript += "</script>";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), guidKey, sbScript, false);
    }

    protected void txtNumeroDoc_TextChanged(object sender, EventArgs e)
    {
        revNIT.ValidationGroup = "vgRevNIT";
        //cvNIT.ValidationGroup = "vgRevNIT";
        Validate("vgRevNIT");

        if (IsValid)
        {
            if (pnlNatural.Visible == false)
            {
                CalcularDiV();
                txtRazonSocial.Focus();
                revNIT.ValidationGroup = "btnGuardar";
                //cvNIT.ValidationGroup = "btnGuardar";
            }
            else
            {
                txtPrimerNombre.Focus();
            }
        }
        else
        {
            txtDV.Text = "";
            revNIT.ValidationGroup = "btnGuardar";
        }
    }

    protected void HpTerminos_Click(object sender, EventArgs e)
    {
        var mostrarPagina = new ManejoControles();

        mostrarPagina.OpenWindowPcUpdatePanel(this, "TerminosProveedor.aspx");
    }

    protected void ChkBTerminos_CheckedChanged(object sender, EventArgs e)
    {
        KeepPassword();

        if (ChkBTerminos.Checked == true)
        {
            btnGuardar.Focus();
        }

        btnGuardar.Enabled = ChkBTerminos.Checked;
    }

    private void KeepPassword()
    {
        string Contrasena = txtContrasena.Text;
        txtContrasena.Attributes.Add("value", Contrasena);

        string ConfirmarContrasena = txtConfirmarContraseña.Text;
        txtConfirmarContraseña.Attributes.Add("value", ConfirmarContrasena);
    }

    private void CleanPassword()
    {
        txtContrasena.Attributes.Remove("value");
        txtConfirmarContraseña.Attributes.Remove("value");
        ChkBTerminos.Checked = false;
        btnGuardar.Enabled = false;
    }

    #endregion

}