﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegisterProveedor.aspx.cs"
    Inherits="Account_RegisterProveedor" MasterPageFile="~/General/General/Master/MasterProveedores.master"
    Async="true" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="Subkismet" %>
<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
         
    <script src="../Scripts/sP.js" language="javascript" type="text/javascript"></script>
    <script >

        function funValidaNumIdentificacionCE(source, args) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                args.IsValid = validaNumIdentificacionCE(args.Value);
            }
        }

        function validaNumIdentificacionCE(numero) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                if (numero.length != 1 && numero.length != 2 && numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6)
                    return false;
                else
                    return true;
            }
        }

        function funValidaNumIdentificacionNit(source, args) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                args.IsValid = validaNumIdentificacionNit(args.Value);
            }
            
        }

        function validaNumIdentificacionNit(numero) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                if (numero.length != 9)
                    return false;
                else
                    return true;
            }
            
        }

        function funValidaNumIdentificacionCD(source, args) {

            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;
            args.IsValid = validaNumIdentificacionCD(args.Value);

        }

        function validaNumIdentificacionCD(numero) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                if (numero.length == 0)
                    return false;
                if (numero.length > 17)
                    return false;
                else
                    return true;
            
            }L
        }

        function funValidaNumIdentificacion(source, args) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                args.IsValid = validaNumIdentificacion(args.Value);
            }
            
        }

        function validaNumIdentificacion(numero) {
            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                if (numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6 && numero.length != 7 && numero.length != 8 && numero.length != 10 && numero.length != 11)
                    return false;
                else
                    return true;
            }
        }

        function EsNumero(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        function deshabilita() {
            if (document.getElementById("ContentPrincipal_ChkBTerminos").checked == true) {
                document.getElementById("ContentPrincipal_btnGuardar").disabled = false;
            } else {
                document.getElementById("ContentPrincipal_btnGuardar").disabled = true;
            }
        }


        function ValidaContrasena(sender, args) {
            if (args.Value.length > 7)
                args.IsValid = true;
            else
                args.IsValid = false;

        }


        function ValidaCelular(sender, args) {
            if (args.Value.length == 10)
                args.IsValid = true;
            else
                args.IsValid = false;

        }

        function CheckLength(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (args.Value.length == 2) {
                if (strSplitDatos[0] == (strSplitDatos[1])) {
                    args.IsValid = false;
                    sender.errormessage = "Caracteres iguales no válidos";
                }
            }

            if (args.Value.length < 2) {
                args.IsValid = false;
                sender.errormessage = "No cumple con la longitud permitida";
            }
        }

        function CheckLengthPrimerNombre(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");
            var flagEspacios = false;
            if (args.Value.length >= 2) {
                //----validar que no tenga espcaios al inicio y final del nombre
                for (i = 0; i <= strSplitDatos.length - 1; i++) {
                    if (strSplitDatos[i] == ' ') {
                        args.IsValid = false;
                        sender.innerHTML = "No se permiten espacios en blanco en su primer nombre";
                        flagEspacios = true;
                        break;
                    }
                }
                if (!flagEspacios) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su primer nombre";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "No cumple con la longitud permitida";
            }
        }

        function CheckLengthSegundoNombre(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (args.Value.length >= 2) {
                if (args.Value.length == 2) {
                    if (strSplitDatos[0] == (strSplitDatos[1])) {
                        args.IsValid = false;
                        sender.innerHTML = "Caracteres iguales no válidos, registre su segundo nombre";
                    }
                    else {
                        args.IsValid = true;
                    }
                }
                if (args.Value.length > 2) {
                    args.IsValid = true;
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "No cumple con la longitud permitida";
            }
        }

        function CheckLengthPrimerApellido(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");
            var flagEspacios = false;
            for (i = 0; i <= strSplitDatos.length - 1; i++) {
                if (strSplitDatos[i] == ' ') {
                    args.IsValid = false;
                    sender.innerHTML = "No se permiten espacios en blanco en su primer apellido";
                    flagEspacios = true;
                    break;
                }
            }
            if (args.Value.length >= 2) {
                if (!flagEspacios) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su primer apellido";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "No cumple con la longitud permitida";
            }
        }

        function CheckLengthSegundoApellido(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (args.Value.length >= 2) {
                if (args.Value.length == 2) {
                    if (strSplitDatos[0] == (strSplitDatos[1])) {
                        args.IsValid = false;
                        sender.innerHTML = "Caracteres iguales no válidos, registre su segundo apellido";
                    }
                    else {
                        args.IsValid = true;
                    }
                }
                if (args.Value.length > 2) {
                    args.IsValid = true;
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "No cumple con la longitud permitida";
            }
        }

        function RefreshTextCaptcha() {
            $("#" +<% = captcha.ClientID%> +" input").val("");
        }

        function showTerminosCondiciones() {
             window_showModalDialog('../../../Account/TerminosProveedor.aspx', null, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function validaNroDocumento(e) {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("ContentPrincipal_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>
    
    <style type="text/css">
        .Cell
        {
            width: 50%;
            
        }
         .popup
        {
             display: none;
            
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPrincipal" runat="Server">
    <asp:HiddenField ID="hfExisteTercero" runat="server" />
    <div id="DialogoMaster">
    </div>
    <table width="50%" align="center">
        <tr class="rowB">
            <td class="Cell">
                <h4>
                    REGISTRO CUENTA DE USUARIO</h4>
            </td>
            <td class="Cell">
                <asp:LinkButton ID="lnkParaQueRegistro" runat="server" OnClientClick="return false;">¿Para qué me registro?</asp:LinkButton>
               
                    <asp:Label runat="server" Text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum feugiat augue. Pellentesque ut metus sit amet urna pretium tincidunt. Vivamus sodales a enim tincidunt sodales. Quisque lobortis, nulla quis scelerisque posuere, libero sapien pretium libero, ac auctor nulla nisi nec erat. Curabitur imperdiet nisl et egestas faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum sit amet facilisis diam, eget elementum tortor."
                        ID="lbParaQueRegistro" CssClass="popup"></asp:Label>
                <Ajax:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="lnkParaQueRegistro"
                    BalloonPopupControlID="lbParaQueRegistro" Position="BottomRight" BalloonStyle="Rectangle"
                    BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                    UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true" />
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                &nbsp;
            </td>
            <td class="Cell">
                &nbsp;
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblCorreoElectronico" runat="server" Text="Correo electrónico * "></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvCorreoElectronico" ControlToValidate="txtCorreoElectronico"
                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ErrorMessage="Correo electrónico en blanco, Registre un correo válido "></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="revCorreoElectronico" ErrorMessage="Correo electrónico no tiene la estructura válida, inténtelo de nuevo"
                    ControlToValidate="txtCorreoElectronico" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" ValidationExpression="^[0-9a-zA-Z_\-\.]+@[0-9a-zA-Z\-\.]+\.[a-zA-Z]{2,4}$"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
            <td class="Cell">
                <asp:Label ID="lblNombreUsuario" runat="server" Text="Confirmación de correo electrónico * "></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreUsuario" ErrorMessage="Confirmación correo electrónico en blanco, registre un correo válido "
                    ControlToValidate="txtNombreUsuario" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvNombreUsuario" runat="server" ErrorMessage="La confirmación del correo electrónico no coincide"
                    ForeColor="Red" Display="Dynamic" ControlToValidate="txtNombreUsuario" ControlToCompare="txtCorreoElectronico"
                    SetFocusOnError="true" ValidationGroup="btnGuardar"></asp:CompareValidator>

            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCorreoElectronico" MaxLength="50" Width="90%" oncopy="return false;"
                    onpaste="return false;"></asp:TextBox>
                
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNombreUsuario" MaxLength="50" Width="90%" oncopy="return false;"
                    onpaste="return false;"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblContrasena" runat="server" Text="Contraseña *"></asp:Label>
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return false;">Política para crear contraseñas</asp:LinkButton>
                    <asp:CustomValidator ID="cvContrasena" runat="server" ControlToValidate="txtContrasena"
                        ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ErrorMessage="La contraseña debe tener mínimo 8 caracteres de longitud" ClientValidationFunction="ValidaContrasena"></asp:CustomValidator>
                    <asp:RequiredFieldValidator runat="server" ID="rfvContrasena" ErrorMessage="Contraseña en blanco, registre una contraseña"
                        ControlToValidate="txtContrasena" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revContrasena1" ErrorMessage="La contraseña requiere por lo menos una letra mayuscula"
                        ToolTip="Contraseña insegura." ControlToValidate="txtContrasena" SetFocusOnError="true"
                        ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^.*[A-Z]+.*$"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revContrasena2" ErrorMessage="Requiere por lo menos una letra minuscula"
                        ToolTip="Contraseña insegura." ControlToValidate="txtContrasena" SetFocusOnError="true"
                        ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^.*[a-z]+.*$"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revContrasena3" ErrorMessage="La contraseña requiere por lo menos un número"
                        ToolTip="Contraseña insegura." ControlToValidate="txtContrasena" SetFocusOnError="true"
                        ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^.*[0-9]+.*$"
                        Display="Dynamic"></asp:RegularExpressionValidator>

                </td>
                <td class="Cell">
                    <asp:Label ID="lblConfirmarContraseña" runat="server" Text="Confirmar contraseña *"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvConfirmarContraseña" ValidationGroup="btnGuardar"
                        Display="Dynamic" ForeColor="Red" ControlToValidate="txtConfirmarContraseña"
                        ErrorMessage="La confirmación de la contraseña esta en blanco, ingrese la confirmación de la contraseña"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvConfirmarContraseña" runat="server" ErrorMessage="Contraseñas no coinciden, inténtelo de nuevo"
                        Display="Dynamic" ForeColor="Red" ControlToValidate="txtConfirmarContraseña"
                        ControlToCompare="txtContrasena" ValidationGroup="btnGuardar"></asp:CompareValidator>

                </td>
            </tr>
        <tr class="rowA">
                <td class="Cell">
                    
                        <asp:Label ID="lbPoliticaContrasena" runat="server" CssClass="popup"></asp:Label><%--</asp:Panel>--%>
                    <asp:TextBox runat="server" ID="txtContrasena" TextMode="Password" Width="90%" oncopy="return false;" onpaste="return false;"></asp:TextBox>
                    
                    <Ajax:BalloonPopupExtender ID="BalloonPopupExtender1" runat="server" TargetControlID="LinkButton1"
                        BalloonPopupControlID="lbPoliticaContrasena" Position="BottomRight" BalloonStyle="Rectangle"
                        BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                        UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true"/>
                    
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtConfirmarContraseña" TextMode="Password" Width="90%" oncopy="return false;" onpaste="return false;"></asp:TextBox>
                </td>
            </tr>

        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="Label1" runat="server" Text="Tipo de persona o asociación * "></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona o asociación"
                    ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificación *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ControlToValidate="ddlTipoDoc"
                 SetFocusOnError="true" ErrorMessage="Seleccione un tipo de identificación" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged"
                    Width="90%">
                </asp:DropDownList>
                
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoDoc" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged"
                    Width="90%">
                </asp:DropDownList>
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblNumeroDoc" runat="server" Text="Número de identificación *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroDoc" ErrorMessage="Registre el número de identificación"
                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="revNumDocCero" ErrorMessage="El documento no debe empezar con cero (0)"
                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" ValidationExpression="^[A-Za-z1-9]+.*$"></asp:RegularExpressionValidator>
                <asp:CustomValidator ID="cvCC" runat="server" ControlToValidate="txtNumeroDoc"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                    ClientValidationFunction="funValidaNumIdentificacion">El número de identificación no es válido</asp:CustomValidator>
                <asp:CustomValidator ID="cvCE" runat="server" ControlToValidate="txtNumeroDoc"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                    ClientValidationFunction="funValidaNumIdentificacionCE">El número de identificación no es válido</asp:CustomValidator>
                <asp:RegularExpressionValidator runat="server" ID="revPA" ErrorMessage="Formato de Número de Documento no Perimitido"
                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" ValidationExpression="^[A-Za-z0-9]{1,16}$" Enabled="false"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="revNIT" ErrorMessage="El Nit no cumple con la logitud especifica"
                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="vgRevNIT"
                    ForeColor="Red" ValidationExpression="^[0-9]{9,9}|[0-9]{9,9}$" Enabled="true" Display="Dynamic">
                </asp:RegularExpressionValidator>
                <asp:CustomValidator ID="revCD" runat="server" ControlToValidate="txtNumeroDoc"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                    ClientValidationFunction="funValidaNumIdentificacionCD">El número de identificación no es válido</asp:CustomValidator>

            </td>
            <td class="Cell">
                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón social *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvRazonSocial" ErrorMessage="Registre su razón social"
                    ControlToValidate="txtRazonSocial" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroDoc" onkeypress="return validaNroDocumento(event)" 
                    Width="80%" OnTextChanged="txtNumeroDoc_TextChanged" AutoPostBack="True">
                </asp:TextBox>
                &nbsp;
                <asp:TextBox runat="server" ID="txtDV" Style="text-align: center;" MaxLength="1"
                    Width="5%" onkeypress="return EsNumero(event)" Enabled="False" />
                <Ajax:FilteredTextBoxExtender ID="ftNumeroDoc" runat="server" TargetControlID="txtNumeroDoc"
                    FilterType="Numbers,Custom,LowercaseLetters,UppercaseLetters" />
                
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtRazonSocial" Width="90%" MaxLength="128"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteRazonSocial" runat="server" TargetControlID="txtRazonSocial"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlNatural">
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblPrimerNombre" runat="server" Text="Primer nombre *"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Dato en blanco, Registre su primer nombre"
                        ControlToValidate="txtPrimerNombre" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator runat="server" ID="cvTxtPrimerNombre" SetFocusOnError="True"
                        ClientValidationFunction="CheckLengthPrimerNombre" ErrorMessage="Caracteres iguales no válidos, registre su primer nombre"
                        ValidationGroup="btnGuardar" ControlToValidate="txtPrimerNombre" Display="Dynamic"
                        ForeColor="Red"></asp:CustomValidator>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblSegundoNombre" runat="server" Text="Segundo nombre"></asp:Label>
                    <asp:CustomValidator runat="server" ID="cvTxtSegundoNombre" SetFocusOnError="True"
                        ClientValidationFunction="CheckLengthSegundoNombre" ErrorMessage="Caracteres iguales no válidos, registre su segundo nombre"
                        ValidationGroup="btnGuardar" ControlToValidate="txtSegundoNombre" Display="Dynamic"
                        ForeColor="Red"></asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Width="90%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Width="90%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblPrimerApellido" runat="server" Text="Primer apellido *"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvPrimerApellido" ErrorMessage="Dato en blanco, Registre su primer apellido"
                        ControlToValidate="txtPrimerApellido" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator runat="server" ID="CustomValidator1" SetFocusOnError="True"
                        ClientValidationFunction="CheckLengthPrimerApellido" ErrorMessage="Caracteres iguales no válidos, registre su primer apellido"
                        ValidationGroup="btnGuardar" ControlToValidate="txtPrimerApellido" Display="Dynamic"
                        ForeColor="Red"></asp:CustomValidator>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblSegundoApellido" runat="server" Text="Segundo apellido"></asp:Label>
                    <asp:CustomValidator runat="server" ID="cvtxtSegundoApellido" SetFocusOnError="True"
                    ClientValidationFunction="CheckLengthSegundoApellido" ErrorMessage="Caracteres iguales no válidos, registra tu segundo apellido"
                    ValidationGroup="btnGuardar" ControlToValidate="txtSegundoApellido" Display="Dynamic"
                    ForeColor="Red"></asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Width="90%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Width="90%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlCell">
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblCelular" runat="server" Text="Celular *"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvCelular" ErrorMessage="Ingrese un número de celular"
                        ControlToValidate="txtCelular" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvCelular" runat="server" ControlToValidate="txtCelular"
                        ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ErrorMessage="Este celular no es válido" ClientValidationFunction="ValidaCelular"></asp:CustomValidator>
                </td>
                <td class="Cell">
                    &nbsp;
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtCelular" onkeypress="return EsNumero(event)" MaxLength="10"
                        Width="90%"></asp:TextBox>
                    
                </td>
                <td class="Cell">
                    &nbsp;
                </td>
            </tr>
         
        </asp:Panel>
   
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBox ID="ChkBTerminos" runat="server" AutoPostBack="True" OnCheckedChanged="ChkBTerminos_CheckedChanged"  />
                Aceptar
                <asp:LinkButton ID="HpTerminos" runat="server" OnClientClick="showTerminosCondiciones(); return false;" Style="cursor: hand">Términos y condiciones del ICBF</asp:LinkButton>*
                
            </td>
        </tr>
             <tr class="rowB">
            <td colspan="2">
                <div style="border: 1px solid grey;">
                    <sbk:CaptchaControl ID="captcha" runat="server" ErrorMessage="Digite los caracteres de la imagen" 
                        CaptchaLength="7" LayoutStyle="Vertical" ValidationGroup="btnGuardar" Display="Dynamic"
                        SetFocusOnError="True" />
                    <asp:ImageButton ID="Prueba" runat="server" ImageUrl="~/Image/btn/refresh.png" OnClientClick="RefreshTextCaptcha();" />
                   
                </div>
            </td>
        </tr>
        <tr class="rowB" align="center">
            <td colspan="2">
                <asp:Button ID="btnGuardar" Enabled="False" runat="server" Text="Crear cuenta" OnClick="btnGuardar_Click" ValidationGroup="btnGuardar" />
                &nbsp;
                <asp:Button ID="btnSalir" runat="server" Text="Salir" PostBackUrl="~/DefaultDenuncias.aspx" />
            </td>
        </tr>
    </table>
</asp:Content>
