﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CodeActivationProveedores.aspx.cs"
    Inherits="Account_CodeActivationProveedores" MasterPageFile="~/General/General/Master/MasterProveedores.master" %>

<%@ Register TagPrefix="ajaxCT" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link type="text/css" href="../../../Styles/log.css" rel="Stylesheet" />
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
        
        
        .ModalWindow
        {
            border: solid1px#c0c0c0;
            background: #89C566;
            padding: 0px10px10px10px;
            position: absolute;
        }
        .divModalDialog
        {
            margin: 5px;
            text-align: center;
            font-family: Verdana;
            font-size: smaller;
            color: White;
        }
        .rfvalidatorCodigo
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: Red;
        }
        .botonLog
        {
            color: White;
            background-color: #66C42F;
            border-color: #66C42F;
            height: 29px;
            width: 200px;
            font-size: 12px;
            padding: 0px 10px 0px 10px;
            margin: 5px 0px 5px 0px;
        }
        
        .btnLogDisabled
        {
            color: Black;
            background-color: #E3E8E1;
            border-color: #E3E8E1;
            height: 29px;
            width: 200px;
            float: left;
            font-size: 12pxd padding: 0px 10px 0px 10px;
            margin: 5px 0px 5px 0px;
            opacity: 0.4;
            filter: alpha(opacity=40);
        }
    </style>
    <script src="../Scripts/jq.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ContentPrincipal_txtCodigo").keypress(function () {
                if (document.getElementById("ContentPrincipal_txtCodigo").value.length == -1) {
                    document.getElementById("ContentPrincipal_btnOkCodigo").disabled = true;
                    document.getElementById("ContentPrincipal_btnOkCodigo").className = "btnLogDisabled";
                }
                else {
                    document.getElementById("ContentPrincipal_btnOkCodigo").disabled = false;
                    document.getElementById("ContentPrincipal_btnOkCodigo").className = "botonLog";
                }

            });

            $("#ContentPrincipal_txtCodigo").bind('paste', function () {
                if (document.getElementById("ContentPrincipal_txtCodigo").value.length == -1) {
                    document.getElementById("ContentPrincipal_btnOkCodigo").disabled = true;
                    document.getElementById("ContentPrincipal_btnOkCodigo").className = "btnLogDisabled";
                }
                else {
                    document.getElementById("ContentPrincipal_btnOkCodigo").disabled = false;
                    document.getElementById("ContentPrincipal_btnOkCodigo").className = "botonLog";
                }
            });


            $("#ContentPrincipal_txtCodigo").blur(function () {
                if (document.getElementById("ContentPrincipal_txtCodigo").value.length == 0) {
                    document.getElementById("ContentPrincipal_btnOkCodigo").disabled = true;
                    document.getElementById("ContentPrincipal_btnOkCodigo").className = "btnLogDisabled";
                }
                else {
                    document.getElementById("ContentPrincipal_btnOkCodigo").disabled = false;
                    document.getElementById("ContentPrincipal_btnOkCodigo").className = "botonLog";
                }

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPrincipal" runat="Server">
    <div>
        <asp:UpdatePanel ID="udpCodAct" runat="server">
            <ContentTemplate>
                <asp:Panel ID="codigoActivacion" runat="server" Height="200px" Width="400px" CssClass="ModalWindow">
                    <div class="divModalDialog">
                        <h3>
                            Código de activación
                        </h3>
                        <p>
                            Ingrese el código de activación de su cuenta el cual fue enviado a su correo
                        </p>
                        <asp:TextBox ID="txtCodigo" runat="server" Width="148px" CssClass="input"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfvtxtCodigo" runat="server" CssClass="rfvalidatorCodigo" ErrorMessage="*" 
                            ControlToValidate="txtCodigo" ValidationGroup="grupo"></asp:RequiredFieldValidator>
                        <br />
                        <br />
                        <asp:Label ID="lblMsg" runat="server" Text="Código no valido, intentelo de nuevo"
                            Visible="false" CssClass="rfvalidatorCodigo" />
                        <br />
                        <table align="center">
                            <tr>
                                <td>
                                    <asp:Button ID="btnOkCodigo" runat="server" Text="Aceptar" CssClass="btnLogDisabled" ValidationGroup="grupo"
                                        Width="100px" OnClick="btnOkCodigo_Click" Enabled="false" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelar" runat="server" CausesValidation="false" Text="Cancelar"
                                        CssClass="botonLog" Width="100px" OnClick="btnCancelar_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <ajaxCT:ModalPopupExtender ID="mdpCodActivacion" runat="server" TargetControlID="btnShowModal"
                    PopupControlID="codigoActivacion" BackgroundCssClass="modalBackground">
                </ajaxCT:ModalPopupExtender>
                <asp:Button ID="btnShowModal" Text="SHOW" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
