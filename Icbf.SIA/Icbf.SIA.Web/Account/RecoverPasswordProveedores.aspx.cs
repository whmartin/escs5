﻿using System;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Página de recuperación de contraseña para externos, maneja los eventos de envío de correo y consulta de usuario.
/// </summary>
public partial class RecoverPasswordProveedores : GeneralWeb
{
    MasterProveedores toolBar;
    SeguridadService vSeguridadService = new SeguridadService();
    SIAService vRUBOService = new SIAService();

    //La declaramos para salvar aqui el nombre de usuario consultado hacia la BD
    //se consulta en el evento VerifyingUser y lo guardamos para que cuando se tenga que 
    //enviar el correo electrónico no volver a consultar.
    private string nombreUsuario = "";

    /// <summary>
    /// Manejador del evento PreInit de la Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// MAnejador del evento Cargar Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["correo"] != null)
            {
                this.prGeneraciones.UserName = Request.QueryString["correo"].ToString();
                this.prGeneraciones.Focus();

            }
        }
    }

    /// <summary>
    /// Inicializar variables locales
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (MasterProveedores)this.Master;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

  
    /// <summary>
    /// Recuperar password de una cuenta existente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PasswordRecovery_VerifyingUser(object sender, LoginCancelEventArgs e)
    {
        
        TextBox txtEmail = ((TextBox)prGeneraciones.UserNameTemplateContainer.FindControl("UserName"));
        Literal FailureText = ((Literal)prGeneraciones.UserNameTemplateContainer.FindControl("FailureText"));
        Usuario oUsuario = ConsultarUsuarioRecordarPass(prGeneraciones.UserName);
        if (oUsuario != null)
        {
            if (oUsuario.IdTipoPersona == 1)
                nombreUsuario = oUsuario.PrimerNombre + " " + oUsuario.SegundoNombre + " " + oUsuario.PrimerApellido + " " + oUsuario.SegundoApellido;
            else if (oUsuario.IdTipoPersona == 2)
                nombreUsuario = oUsuario.RazonSocial;
            MembershipUser usuario = Membership.GetUser(prGeneraciones.UserName);
            string nuevaClave = usuario.ResetPassword();
            this.EnviarNuevaClave(ConfigurationManager.AppSettings["RemitenteProveedores"], prGeneraciones.UserName, ConfigurationManager.AppSettings["AsuntoProveedoresPassword"], ConfigurationManager.AppSettings["URLProveedores"], nombreUsuario, nuevaClave);

        }
        else
        {
            e.Cancel = true;
            FailureText.Text = "Esta cuenta de correo no existe, si desea crear un nuevo usuario ingresa por la opción Regístra tu cuenta de usuario";
        }
    }

    protected void PasswordRecovery_SendingMail(object sender, MailMessageEventArgs e)
    {
        e.Cancel = true;
    }

    /// <summary>
    /// Enviar nueva clave de acceso
    /// </summary>
    /// <param name="pDe"></param>
    /// <param name="pPara"></param>
    /// <param name="pAsunto"></param>
    /// <param name="pLinkUrl"></param>
    /// <param name="pNombre"></param>
    /// <param name="pContrasena"></param>
    private void EnviarNuevaClave(string pDe, string pPara, string pAsunto, string pLinkUrl, string pNombre, string pContrasena)
    {

        MailMessage MyMailMessage = new MailMessage();

        MyMailMessage.From = new MailAddress(pDe);
        MyMailMessage.To.Add(new MailAddress(pPara));
        MyMailMessage.Subject = pAsunto;

        MyMailMessage.IsBodyHtml = true;


        MyMailMessage.Body = "<html><body><table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'><tr> " +
        "<td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' bgcolor='#81BA3D'>" +
        "&nbsp;</td></tr><tr><td width='10%' bgcolor='#81BA3D'>&nbsp;</td><td>" +
        "<table width='100%'border='0'align='center'><tr><td width='5%'>&nbsp;</td>" +
        "<td align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr><tr><td>&nbsp;</td>" +
        "<td align='left'><strong>Cambio de contraseña</strong></td><td>&nbsp;</td>" +
        "</tr><tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>&nbsp;</td>" +
        "</tr><tr><td>&nbsp;</td><td><strong>Apreciado(a): </strong>&nbsp;" + pNombre + "</td><td>" +
        "&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td>" +
        "<td>Su nueva contraseña temporal de acceso al sistema de Gestión de Terceros y Proveedores es:</td><td>&nbsp;</td>" +
        "</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>" +
        "<table width='100%'border='0'><tr><td width='50%' align='right'><strong>Contraseña: </strong>" +
        "</td><td width='50%'>" + pContrasena + "</td></tr></table></td><td>&nbsp;</td></tr><tr><td>&nbsp;" +
        "</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" +
        "</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>" +
        "Para ingresar al sistema de Gestión de Terceros y Proveedores haga click <a href='" + pLinkUrl + "'target='_blank'>aquí</a>." +
        "</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>" +
        "Recuerde modificar esta contraseña, una vez que inicie la sesión." +
        "</td><td>&nbsp;</td></tr> " +
        "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td width='5%'>" +
        "&nbsp;</td><td>&nbsp;</td><td width='5%'>&nbsp;</td></tr></table></td><td width='10%' bgcolor='#81BA3D'>" +
        "&nbsp;</td></tr><tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr><tr>" +
        "<td colspan='3' align='center' bgcolor='#81BA3D'>" +
        "Este correo electrónico fue enviado por un sistema automático, favor de no responderlo.</td></tr><tr>" +
        "<td colspan='3' align='center' bgcolor='#81BA3D'>" +
        "Si tienes alguna duda, puedes dirigirte a nuestra sección de <a href='" + pLinkUrl + "' target='_blank'>" +
        "Asistencia y Soporte.</a></td></tr><tr><td colspan='3' align='center' bgcolor='#81BA3D'>" +
        "&nbsp;</td></tr></table></body></html>";

        SmtpClient MySmtpClient = new SmtpClient();

        object userState = MyMailMessage;

        MySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

        try
        {
            MySmtpClient.SendAsync(MyMailMessage, userState);
            //MySmtpClient.Send(MyMailMessage);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador del evento Completar envìo de correo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        MailMessage MyMailMessage;
        MyMailMessage = (MailMessage)e.UserState;

        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo de activación fue cancelado");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
        else
        {
            toolBar.MostrarMensajeGuardado("Su nueva contraseña ha sido enviada a su correo");
        }
    }

    /// <summary>
    /// Manejador del evento para el botón Cancelar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancelar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        Response.Redirect(@"~/DefaultProveedores.aspx", false);
    }
}