﻿using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using System.Collections.Generic;

/// <summary>
/// Summary description for RolUtil
/// </summary>
public static class RolUtil
{
    public static List<ProgramaFuncion> ListarRolesUsuario(string pNombreUsuario, string pCodigoPrograma)
    {

        List<ProgramaFuncion> listFunciones = new List<ProgramaFuncion>();

        List<ProgramaFuncion> funciones = new List<ProgramaFuncion>();

        List<Rol> RolesUsuario = new List<Rol>();

        foreach (string itemRolUsuario in System.Web.Security.Roles.GetRolesForUser(pNombreUsuario))
        {

            funciones = new SeguridadService().ConsultarUsuarioProgramaFuncionRolComisiones(itemRolUsuario.Trim(), pCodigoPrograma);

            foreach (ProgramaFuncion dataRol in funciones)
            {
                listFunciones.Add(dataRol);
            }
        }

        return listFunciones;
    }
}