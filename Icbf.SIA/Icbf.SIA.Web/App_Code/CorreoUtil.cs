﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

/// <summary>
/// Class CorreoUtil
/// </summary>
public class CorreoUtil
{

    /// <summary>
    /// Configura el callback de validación del certificado para que ignore el hecho que el certificado esté
    /// autofirmado.
    /// </summary>
    static CorreoUtil()
    {
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
    }

    /// <summary>
    /// Envia un mail
    /// </summary>
    /// <param name="emailPara">Email destino</param>        
    /// <param name="nombreOrigen">*Obsoleto* se debe establecer en la configuración en la sección mailSettings 
    /// Nombre de la "persona" que envia el correo</param>
    /// <param name="asunto">Asunto del mensaje</param>
    /// <param name="mensaje">El mensaje a enviar</param>
    /// <param name="idUsuario">El id del usuario que realiza la transacción</param>
    /// <param name="archivo">Ruta del archivo adjunto</param>
    /// <returns>Indica si se envio el correo</returns>public static bool
    public static bool EnviarCorreo(string emailPara, string nombreOrigen, string asunto, string mensaje, int idUsuario, string archivo)
    {
        //// Sin destinatario el envío de correo no puede continuar
        if (string.IsNullOrEmpty(emailPara))
            throw new Exception("El email de destino no puede ser vacio");

        //// Crea una nueva instancia de mensaje de correo electrónico
        MailMessage objMensaje = new MailMessage();
        objMensaje.Subject = asunto;
        objMensaje.Body = mensaje;
        objMensaje.IsBodyHtml = true;
        objMensaje.Bcc.Add(ConfigurationManager.AppSettings["MailUser"].ToString());

        //// Por cada uno de los destinatarios en la cadena de texto lo agrega al mensaje
        foreach (string destinatario in emailPara.Split(','))
            if (!string.IsNullOrEmpty(destinatario))
                objMensaje.To.Add(destinatario);

        //// Si hay archivo entonces lo adjunta al mensaje
        if (!string.IsNullOrEmpty(archivo))
        {
            Attachment data = new Attachment(archivo, MediaTypeNames.Application.Octet);
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = File.GetCreationTime(archivo);
            disposition.ModificationDate = File.GetLastWriteTime(archivo);
            disposition.ReadDate = File.GetLastAccessTime(archivo);
            objMensaje.Attachments.Add(data);
        }

        try
        {
            //// Crea una nueva instancia de un SmtpClient con la configuración de la sección <mailSettings/>,
            //// envía el mensaje y dispone del cliente antes de continuar
            using (SmtpClient mailSMTP = new SmtpClient())
                mailSMTP.Send(objMensaje);
            return true;
        }
        catch (Exception ex)
        {
            //// Determina si el error se debe a que la cuota de envíos fue superada o es otro tipo de error.
            //// En todo caso se arroja una excepción con el mensaje y la excepción original.
            if (ex.Message.Contains("quota exceeded")) throw new Exception("Se ha excedido el límite de envíos de correos diarios.", ex);
            else throw new Exception("Error enviando el correo: " + ex.Message, ex);
        }
        finally
        {
            //// Dispone del mensaje para liberar recursos como lo es el archivo adjunto
            if (objMensaje != null) objMensaje.Dispose();
        }
    }
}
