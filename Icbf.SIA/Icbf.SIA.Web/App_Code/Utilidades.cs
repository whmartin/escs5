﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Entity;
using System.Web.UI;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;

/// <summary>
/// Summary description for CargarLista
/// </summary>
public static class Utilidades
{

    public static void LlenarDropDownList(DropDownList pControl, List<BaseDTO> pLista,bool pAscendente=true)
    {
        pControl.Items.Clear();
        if (pAscendente)
        {
            pLista= pLista.OrderBy(x => x.Name).ToList();
        }
        else
        {
            pLista= pLista.OrderByDescending(x => x.Name).ToList();
        }

        pControl.DataSource = pLista;
        pControl.DataValueField = "Id";
        pControl.DataTextField = "Name";
        pControl.DataBind();
        pControl.Items.Insert(0, new ListItem(Constantes.Seleccione, Constantes.ValorDefecto));

    }

    public static void LlenarRadioButtonList(RadioButtonList pControl, List<BaseDTO> pLista)
    {
        pControl.Items.Clear();
        pControl.DataSource = pLista.OrderBy(x => x.Name).ToList();
        pControl.DataValueField = "Id";
        pControl.DataTextField = "Name";
        pControl.DataBind();

    }

    public static void SetControls(Panel pPanel, bool pEnabled = true, bool pValoresDefecto = true)
    {
        foreach (Control ctrl in pPanel.Controls)
        {
            if (ctrl is TextBox)
            {
                (ctrl as TextBox).Text = pValoresDefecto ? string.Empty : (ctrl as TextBox).Text;
                (ctrl as TextBox).Enabled = pEnabled;
            }
            if (ctrl is DropDownList)
            {
                ListItem val = (ctrl as DropDownList).Items.FindByValue(Constantes.ValorDefecto);
                if (val == null)
                {
                    (ctrl as DropDownList).Items.Insert(0, new ListItem(Constantes.Seleccione, Constantes.ValorDefecto));

                }
                (ctrl as DropDownList).SelectedValue = pValoresDefecto ? Constantes.ValorDefecto : (ctrl as DropDownList).SelectedValue;
                (ctrl as DropDownList).Enabled = pEnabled;
            }
            if (ctrl is Panel)
            {
                SetControls(ctrl as Panel);
            }
        }

    }

    public static DataTable GenerarDataTable(GridView grvDatos, object datos)
    {
        DataTable dtDatos = new DataTable();
        // creamos las columnas de la tabla

        for (int i = 1; i < grvDatos.Columns.Count; i++)
        {
            dtDatos.Columns.Add(grvDatos.Columns[i].HeaderText);
        }
        int columnas = dtDatos.Columns.Count;
        grvDatos.DataSource = datos;
        grvDatos.DataBind();
        foreach (GridViewRow item in grvDatos.Rows)
        {
            DataRow drNewRow = dtDatos.NewRow();
            int j = 0;
            foreach (TableCell cell in item.Cells)
            {
                if (cell.Controls.Count > 1)
                {
                    if (cell.Controls[1] is Label)
                    {
                        Label lbl = cell.Controls[1] as Label;
                        drNewRow[j] = ConvertirCaracteresEspeciales(lbl.Text);
                    }
                    if (cell.Controls[1] is TextBox)
                    {
                        TextBox txt = cell.Controls[1] as TextBox;
                        drNewRow[j] = ConvertirCaracteresEspeciales(txt.Text);
                    }
                    if (cell.Controls[1] is CheckBox)
                    {
                        CheckBox chk = cell.Controls[1] as CheckBox;
                        drNewRow[j] = chk.Checked ? "X" : string.Empty;
                    }
                    if (cell.Controls[1] is ImageButton)
                    {
                        j = j - 1;
                    }
                }
                else
                {
                    drNewRow[j] = ConvertirCaracteresEspeciales(cell.Text);
                }
                j++;
            }

            dtDatos.Rows.Add(drNewRow);
        }

        return dtDatos;
    }

    public static string ConvertirCaracteresEspeciales(string pCadena)
    {
        string vCadenaNueva = pCadena.Replace("&#193;", "Á");
        vCadenaNueva = vCadenaNueva.Replace("&#201;", "É");
        vCadenaNueva = vCadenaNueva.Replace("&#205;", "Í");
        vCadenaNueva = vCadenaNueva.Replace("&#211;", "Ó");
        vCadenaNueva = vCadenaNueva.Replace("&#218;", "Ú");

        vCadenaNueva = vCadenaNueva.Replace("&#225;", "á");
        vCadenaNueva = vCadenaNueva.Replace("&#233;", "é");
        vCadenaNueva = vCadenaNueva.Replace("&#237;", "í");
        vCadenaNueva = vCadenaNueva.Replace("&#243;", "ó");
        vCadenaNueva = vCadenaNueva.Replace("&#250;", "ú");

        vCadenaNueva = vCadenaNueva.Replace("&#209;", "Ñ");
        vCadenaNueva = vCadenaNueva.Replace("&#241;", "ñ");

        vCadenaNueva = vCadenaNueva.Replace("&gt;", ">");
        vCadenaNueva = vCadenaNueva.Replace("&#61;", "=");
        vCadenaNueva = vCadenaNueva.Replace("&lt;", "<"); 
        vCadenaNueva = vCadenaNueva.Replace("&nbsp;;",string.Empty);
        vCadenaNueva = vCadenaNueva.Replace("&nbsp;", string.Empty);

        return vCadenaNueva;
    }

    public static string EliminarEspacios(string pCadena)
    {
        string result = pCadena;

        if(!string.IsNullOrEmpty(pCadena) && !string.IsNullOrWhiteSpace(pCadena) )
        {
            string[] split = new String[] { " ", "  " };

            var array = pCadena.Split(split, StringSplitOptions.RemoveEmptyEntries);

            StringBuilder cadenaResult = new StringBuilder();

            foreach(var item in array)
            {
                cadenaResult.Append(item);
                cadenaResult.Append(" ");
            }

            result = cadenaResult.ToString().TrimEnd();
        }

        return result;
    }

    public static String Compress(String pRutaDirectorio, String pNombreArchivo)
    {
        String vRutaFinal = pRutaDirectorio + pNombreArchivo + ".zip";
        Crc32 vCrc32 = new Crc32();

        using (ZipOutputStream vZipOutputStream = new ZipOutputStream(File.Create(vRutaFinal)))
        {
            vZipOutputStream.SetLevel(9); // 0 - store only to 9 - means best compression
            foreach (String vDirectory in Directory.GetDirectories(pRutaDirectorio, pNombreArchivo))
            {


                foreach (String vFile in Directory.GetFiles(vDirectory))
                {
                    using (FileStream vFileStream = File.OpenRead(vFile))
                    {
                        string entryName = vFile.Substring(pRutaDirectorio.Length);

                        byte[] buffer = new byte[vFileStream.Length];
                        vFileStream.Read(buffer, 0, buffer.Length);
                        ZipEntry vZipEntry = new ZipEntry(ZipEntry.CleanName(entryName));
                        vZipEntry.DateTime = DateTime.Now;
                        vZipEntry.Comment = pNombreArchivo;
                        vZipEntry.ZipFileIndex = 1;
                        vZipEntry.Size = vFileStream.Length;
                        vCrc32.Reset();
                        vCrc32.Update(buffer);
                        vZipEntry.Crc = vCrc32.Value;
                        vZipOutputStream.PutNextEntry(vZipEntry);
                        vZipOutputStream.Write(buffer, 0, buffer.Length);
                        vFileStream.Close();
                    }
                }

                vZipOutputStream.Finish();
            }
            vZipOutputStream.Close();
        }
        return vRutaFinal;
    }

    public static void ExportToExcel(DataTable dt, string nombreArchivo, string RutaDirectorio, bool complemento = true)
    {

        if (dt.Rows.Count > 0)
        {
            var rancomp = new Random();

            string filename = string.Empty;

            if (complemento)
            {
                string complementoArch = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + rancomp.Next(100);
                filename = complementoArch + "_" + nombreArchivo + ".xls";
            }
            else
                filename = nombreArchivo + ".xls";
            
            using (System.IO.StringWriter tw = new System.IO.StringWriter())
            {
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.

                StreamWriter archivoSalida = null;

                string rutaArchivo = RutaDirectorio + filename;
                try
                {
                    archivoSalida = new StreamWriter(rutaArchivo, false, System.Text.Encoding.Unicode);
                    archivoSalida.Write(tw.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el archivo " + nombreArchivo, ex);
                }
                finally
                {
                    if (archivoSalida != null)
                        archivoSalida.Close();
                }

            }

        }
    }

    public static string ExportToExcelFileName(DataTable dt, string nombreArchivo, string RutaDirectorio)
    {
        string filename = string.Empty;

        if (dt.Rows.Count > 0)
        {
            var rancomp = new Random();
            string complementoArch = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + rancomp.Next(100);
            filename = complementoArch + "_" + nombreArchivo + ".xls";
            using (System.IO.StringWriter tw = new System.IO.StringWriter())
            {
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.

                StreamWriter archivoSalida = null;

                string rutaArchivo = RutaDirectorio + filename;
                try
                {
                    archivoSalida = new StreamWriter(rutaArchivo, false, System.Text.Encoding.Unicode);
                    archivoSalida.Write(tw.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el archivo " + nombreArchivo, ex);
                }
                finally
                {
                    if (archivoSalida != null)
                        archivoSalida.Close();
                }

            }

        }

        return filename;
    }
}