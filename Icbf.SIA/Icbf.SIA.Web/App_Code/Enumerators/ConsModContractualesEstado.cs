﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de ConsModContractualesEstado
/// </summary>
public enum ConsModContractualesEstado
{
	REGISTRO  = 1,
    ENVIADA = 2,
    ACEPTADA = 3,
    RECHAZADA = 4,
    DEVUELTA = 5,
    INACTIVA = 6
}