﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de TipoModificacionContractual
/// </summary>
public enum TipoModificacionContractual
{
    Adicción = 1,
    Prorroga = 2,
    Modificación = 3,
    Suspención = 4,
    ReducciónValor = 5,
    Cesión = 6
}