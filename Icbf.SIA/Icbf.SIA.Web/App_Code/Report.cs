﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Clase que controla el acceso a los reportes
/// </summary>
public class Report
{

    private readonly string _strReportTitle;
    private string _strReportName;
    private string _strPreviousPageURL;
    private bool _sExportarPDF;
    private bool _sExportarWord;
    private string _nombreArchivoExp;
    private bool _boolShowExportControls;
    private List<Microsoft.Reporting.WebForms.ReportParameter> _aobjParameter = new List<ReportParameter>();


    /// <summary>
    /// Crea una instancia de un Objeto reporte
    /// </summary>
    /// <param name="ReportName">Nombre del reporte a visualizar</param>
    /// <param name="ShowExportControls">Indica al visor de reportes si habilita los controles de exportación</param>
    /// <param name="PreviousPageURL">Url de la pagina que invoca al reporte</param>
    /// <param name="ReportTitle">Titulo que se muestra en la parte superior de la pagina del reporte</param>
    public Report(string ReportName, bool ShowExportControls, string PreviousPageURL, string ReportTitle, bool exportarPDF = false, string nombreReporte = "", bool exportarWORD = false)
    {
        _strReportName = ReportName;
        _boolShowExportControls = ShowExportControls;
        _strPreviousPageURL = PreviousPageURL;
        _strReportTitle = ReportTitle;
        _sExportarPDF = exportarPDF;
        _sExportarWord = exportarWORD;
        _nombreArchivoExp = nombreReporte;
    }


    /// <summary>
    /// Retorna el titulo que se muestra en la parte superior de la pagina del reporte
    /// </summary> 
    public string ReportTitle
    {
        get
        {
            return _strReportTitle;
        }
    }


    /// <summary>
    /// Retorna el nombre del reporte a visualizar
    /// </summary>
    public string ReportName
    {
        get
        {
            return _strReportName;
        }
    }

    /// <summary>
    /// Retorna la Url de la pagina que invoca al reporte
    /// </summary>
    public string PreviousPageURL
    {
        get
        {
            return _strPreviousPageURL;
        }
    }

    public bool ExportarPDF
    {

        get
        {
            return _sExportarPDF;
        }
    }

    public bool ExportarWord
    {

        get
        {
            return _sExportarWord;
        }
    }

    public string NombreArchivoExp
    {

        get
        {
            return _nombreArchivoExp;
        }
    }

    /// <summary>
    /// Retorna el valor que indica al visor de reportes si habilita los controles de exportación
    /// </summary>
    public bool ShowExportControls
    {
        get
        {
            return _boolShowExportControls;
        }
    }


    /// <summary>
    /// Adicionar un parametro al objeto reporte
    /// </summary>
    /// <param name="parameterName">Nombre del parametro</param>
    /// <param name="parameterValue">Valor del parametro</param>
    public void AddParameter(string parameterName, string parameterValue)
    {
        _aobjParameter.Add(new Microsoft.Reporting.WebForms.ReportParameter(parameterName, parameterValue, false));
    }

    /// <summary>
    /// Elimina todos los parametros que contenga el objeto reporte
    /// </summary>
    public void ClearParameters()
    {
        _aobjParameter.Clear();
    }


    /// <summary>
    /// Retorna una lista que contiene todos los parametros del reporte
    /// </summary>
    /// <returns>Lista de parametros del reporte</returns>
    public List<Microsoft.Reporting.WebForms.ReportParameter> GetParemeters()
    {
        return _aobjParameter;
    }






}