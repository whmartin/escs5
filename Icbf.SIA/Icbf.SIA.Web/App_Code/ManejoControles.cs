﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using Icbf.Contrato.Service;

/// <summary>
/// Clase utilizada como repositorio de métodos que alimentan listas desplegables y apertura de popup.
/// </summary>
public partial class ManejoControles
{
    SIAService vRUBOService;
    ProveedorService vProveedorService;
    OferenteService vOferenteService;
    ContratoService vContratoService;
    MostrencosService vMostrencosService;

    /// <summary>
    /// Constructor de la clase ManejoControles
    /// </summary>
    public ManejoControles()
    {
        vRUBOService = new SIAService();

        vProveedorService = new ProveedorService();

        vOferenteService = new OferenteService();

        vContratoService = new ContratoService();
    }

    /// <summary>
    /// Inicializa valores Tipo documento para un control lista desplegable
    /// </summary>
    /// <param name="pDropDownList">Control</param>
    /// <param name="pPredeterminado">Valor predeterminado, valores:-1 por defecto</param>
    /// <param name="pMostrarSeleccionar">debe mostrar seleccionar</param>
    public void LlenarTipoDocumento(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "CC") || (tD.Codigo == "CE") || (tD.Codigo == "PA"))
            { pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString())); }
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores de tipo Tipo Documento F para un control lista desplegable
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocumentoF(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "CC") || (tD.Codigo == "CE") || (tD.Codigo == "NIT"))
            { pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString())); }
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores Tipo Documento RL para un control lista desplegable
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocumentoRL(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "CC") || (tD.Codigo == "CE"))
            { pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString())); }

            //se incluye Carné Diplomático
            if ((tD.Codigo == "CD"))
            {
                pDropDownList.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString()));
            }
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores de Tipo Documento N para un control lista desplegable
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocumentoN(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "NIT"))
            { pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString())); }
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    

    #region "Participantes"
    
    /// <summary>
    /// Llena una lista deplegable
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocumentoPartic(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "RC") || (tD.Codigo == "TI") || (tD.Codigo == "PA") || (tD.Codigo == "CC") || (tD.Codigo == "SD"))
            {
                pDropDownList.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString()));
            }
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    #endregion


    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pTipoBeneficiario"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocumentoBen(DropDownList pDropDownList, int pTipoBeneficiario, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if (((pTipoBeneficiario == 1) || (pTipoBeneficiario == 4)) && ((tD.Codigo == "RC") || (tD.Codigo == "TI") || (tD.Codigo == "PA") || (tD.Codigo == "SD")))
            { pDropDownList.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString())); }
            else if (((pTipoBeneficiario == 2) || (pTipoBeneficiario == 3)) && ((tD.Codigo == "TI") || (tD.Codigo == "PA") || (tD.Codigo == "CC") || (tD.Codigo == "CE") || (tD.Codigo == "SD")))
            { pDropDownList.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString())); }
            else if ((pTipoBeneficiario == -1) && ((tD.Codigo != "NIT") || (tD.Codigo != "RUT")))
            { pDropDownList.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString())); }
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    

    #region Mostrar Imagenes

    public void OpenWindowPC(System.Web.UI.Page objWebPage, string strURL)
    {
        string strScript = "<script language='javascript'>" +
            //  "var newWin=window.open ('" + strURL.Trim() + "', this.target, 'null,null,null,null,buttons=no,scrollbars=yes,location=0,menubar=no,resizable=yes,status=no,directories=no,toolbar=no'); " +

            "var newWin= window.showModalDialog('" + strURL.Trim() + "', null, 'dialogWidth:1000px;dialogHeight:600px;resizable:yes;');" +
            "</script>";


        if (objWebPage.ClientScript.IsStartupScriptRegistered("OpenWindow") == false)
        {
            objWebPage.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", strScript);
        }

    }

    /// <summary>
    /// Muestra una página en un popup
    /// </summary>
    /// <param name="objWebPage">página de origen</param>
    /// <param name="strUrl">URL a mostrar en el popup</param>
    /// <param name="dialogWidth">Ancho de la ventana</param>
    /// <param name="dialogHeight">Alto  de la ventana</param>
    public void OpenWindowPcUpdatePanel(Page objWebPage, string strUrl, int dialogWidth = 1000, int dialogHeight = 600)
    {
        var guidKey = Guid.NewGuid().ToString();

        var sbScript = "";
        sbScript += "<script language='javascript'>";
        sbScript += "window.showModalDialog('" + strUrl + "','Mensaje','status=no;dialogWidth=" + dialogWidth + ";dialogHeight=" + dialogHeight + ";resizable=no');";
        sbScript += "</script>";
        ScriptManager.RegisterClientScriptBlock(objWebPage, objWebPage.GetType(), guidKey, sbScript, false);
    }

    #endregion

    #region Visualizar Archivos

    /// <summary>
    /// Abre una nueva ventana
    /// </summary>
    /// <param name="objWebPage"></param>
    /// <param name="strUrl"></param>
    public void OpenWindowDescargaExperiencia(Page objWebPage, string strUrl)
    {
        var guidKey = Guid.NewGuid().ToString();
        var sbScript = "<script>window.showModalDialog('" + strUrl + "', null, 'dialogWidth:600px;dialogHeight:450px;resizable:yes;');</script>";
        //sbScript += "<script>";
        //sbScript += "window.open('" + strUrl + "','Mensaje','dialogWidth=1024px;dialogHeight=400px;resizable=yes');";
        //sbScript += "</script>";
        ScriptManager.RegisterClientScriptBlock(objWebPage, objWebPage.GetType(), guidKey, sbScript, false);
    }

    /// <summary>
    /// Abre una nueva ventana
    /// </summary>
    /// <param name="objWebPage"></param>
    /// <param name="strUrl"></param>
    public void OpenWindowDescarga(Page objWebPage, string strUrl) 
    {
        var guidKey = Guid.NewGuid().ToString();
        var sbScript = "";
        sbScript += "<script language='javascript'>";
        sbScript += "window.open('" + strUrl + "','Mensaje','dialogWidth=20;dialogHeight=20;resizable=no');";
        sbScript += "</script>";
        ScriptManager.RegisterClientScriptBlock(objWebPage, objWebPage.GetType(), guidKey, sbScript, false);
    }

    /// <summary>
    /// Abre una nueva ventana
    /// </summary>
    /// <param name="objWebPage"></param>
    /// <param name="strUrl"></param>
    public void OpenWindowDescarga(UserControl objWebPage, string strUrl)
    {
        var guidKey = Guid.NewGuid().ToString();
        var sbScript = "";
        sbScript += "<script language='javascript'>";
        sbScript += "window.open('" + strUrl + "','Mensaje','dialogWidth=20;dialogHeight=20;resizable=no');";
        sbScript += "</script>";
        ScriptManager.RegisterClientScriptBlock(objWebPage, objWebPage.GetType(), guidKey, sbScript, false);
    }

    /// <summary>
    /// Abre una nueva ventana para Detail de Integrante.
    /// </summary>
    /// <param name="objWebPage"></param>
    /// <param name="strUrl"></param>
    public void OpenWindowDetailIntegrante(Page objWebPage, string strUrl)
    {
        var guidKey = Guid.NewGuid().ToString();
        var sbScript = "<script>window.showModalDialog('" + strUrl + "', null, 'dialogWidth:800px;dialogHeight:6500px;resizable:yes;');</script>";
        //sbScript += "<script>";
        //sbScript += "window.open('" + strUrl + "','Mensaje','dialogWidth=1024px;dialogHeight=400px;resizable=yes');";
        //sbScript += "</script>";
        ScriptManager.RegisterClientScriptBlock(objWebPage, objWebPage.GetType(), guidKey, sbScript, false);
    }
    #endregion

    #region Proveedor

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocumentos(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumentoProveedor> vLTipoDocumento = vProveedorService.ConsultarTipoDocumentos(null);
        foreach (TipoDocumentoProveedor tD in vLTipoDocumento)
        {
            pDropDownList.Items.Add(new ListItem(tD.Descripcion.ToString(), tD.IdTipoDocumento.ToString()));
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }


    public void LlenarTiposArchivo(DropDownList pDropDownList, string pPredeterminado, bool pMostrarSeleccionar = true)
    {
        pDropDownList.Items.Clear();

        pDropDownList.Items.Add(new ListItem("Terceros Importados", "1"));

        if (pMostrarSeleccionar)
        {
            pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }

        if (pPredeterminado.Length > 0)
        {
            pDropDownList.SelectedValue = pPredeterminado.ToString();
        }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoSectorEntidad(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoSectorEntidad> vLTipoSectorEntidad = vProveedorService.ConsultarTipoSectorEntidadAll();
        foreach (TipoSectorEntidad tD in vLTipoSectorEntidad)
        {
                pDropDownList.Items.Add(new ListItem(tD.Descripcion.ToString(),tD.IdTipoSectorEntidad.ToString())); 
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pIdTipoPersona"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarRegimenTributario(DropDownList pDropDownList, int pIdTipoPersona, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Icbf.Proveedor.Entity.TipoRegimenTributario> vTipoRegimenTributario = vProveedorService.ConsultarTipoRegimenTributarioTipoPersona(pIdTipoPersona);
        foreach (Icbf.Proveedor.Entity.TipoRegimenTributario vMunicipio in vTipoRegimenTributario)
        { pDropDownList.Items.Insert(0, new ListItem(vMunicipio.Descripcion, vMunicipio.IdTipoRegimenTributario.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoEntidadAll(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? pIdTipoPersona, int? pIdSector)
    {
        pDropDownList.Items.Clear();
        List<Tipoentidad> vDepartamentos = vProveedorService.ConsultarTipoentidadsAll(pIdTipoPersona, pIdSector);
        foreach (Tipoentidad vDepartamento in vDepartamentos)
        {
            pDropDownList.Items.Insert(0, new ListItem(vDepartamento.Descripcion, vDepartamento.IdTipoentidad.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarRamaoestructuraAll(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<RamaoEstructura> vDepartamentos = vProveedorService.ConsultarRamaoEstructurasAll();
        foreach (RamaoEstructura vDepartamento in vDepartamentos)
        {
            pDropDownList.Items.Insert(0, new ListItem(vDepartamento.Descripcion, vDepartamento.IdRamaEstructura.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarNivelGobierno(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Niveldegobierno> vDNiveldegobierno = vProveedorService.ConsultarNiveldegobiernosAll();
        foreach (Niveldegobierno vNiveldegobierno in vDNiveldegobierno)
        {
            pDropDownList.Items.Insert(0, new ListItem(vNiveldegobierno.Descripcion, vNiveldegobierno.IdNiveldegobierno.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    /// <param name="pIdRamaEstructura"></param>
    public void LlenarNivelGobiernoRamaoEstructura(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int pIdRamaEstructura)
    {
        pDropDownList.Items.Clear();
        List<Niveldegobierno> vDNiveldegobierno = vProveedorService.ConsultarNiveldegobiernosRamaoEstructura(pIdRamaEstructura);
        foreach (Niveldegobierno vNiveldegobierno in vDNiveldegobierno)
        {
            pDropDownList.Items.Insert(0, new ListItem(vNiveldegobierno.Descripcion, vNiveldegobierno.IdNiveldegobierno.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarNivelOrganizacional(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int idRama, int IdNivelGob)
    {
        pDropDownList.Items.Clear();
        List<NivelOrganizacional> vDNivelOrganizacional= new List<NivelOrganizacional>();
        if(idRama!=0 && IdNivelGob!=0)
        vDNivelOrganizacional = vProveedorService.ConsultarNivelOrganizacional_PorRamaYNivelGobierno(idRama, IdNivelGob);
        else if (idRama == 0 && IdNivelGob == 0)
            vDNivelOrganizacional = vProveedorService.ConsultarNivelOrganizacionalAll();
        foreach (NivelOrganizacional vNivelOrganizacional in vDNivelOrganizacional)
        {
            pDropDownList.Items.Insert(0, new ListItem(vNivelOrganizacional.Descripcion, vNivelOrganizacional.IdNivelOrganizacional.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables, de tipo Entidad Pùblica
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void TipoEntidadPublica(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int idRama, int idNivelGob, int IdNivelOrg)
    {
        pDropDownList.Items.Clear();
        List<TipodeentidadPublica> vDTipodeentidadPublica = new List<TipodeentidadPublica>();
        if (idRama != 0 && idNivelGob != 0 && IdNivelOrg != 0)
        {
            vDTipodeentidadPublica = vProveedorService.ConsultarTipodeentidadPublicas_PorRamaNivelGobYNivelOrg(idRama, idNivelGob, IdNivelOrg);
        }
        else if (idRama == 0 && idNivelGob == 0 && IdNivelOrg == 0)
        {
            vDTipodeentidadPublica = vProveedorService.ConsultarTipodeentidadPublicasAll();
        }
        foreach (TipodeentidadPublica vTipodeentidadPublica in vDTipodeentidadPublica)
        {
            pDropDownList.Items.Insert(0, new ListItem(vTipodeentidadPublica.Descripcion, vTipodeentidadPublica.IdTipodeentidadPublica.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void TipoActividad(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipodeActividad> vDTipodeActividad = vProveedorService.ConsultarTipodeActividadAll();
        foreach (TipodeActividad vTipodeActividad in vDTipodeActividad)
        {
            if(pPredeterminado.Equals("1") && !vTipodeActividad.Descripcion.Equals("SIN ANIMO DE LUCRO"))
                pDropDownList.Items.Insert(0, new ListItem(vTipodeActividad.Descripcion, vTipodeActividad.IdTipodeActividad.ToString()));
            else if(!pPredeterminado.Equals("1"))
                pDropDownList.Items.Insert(0, new ListItem(vTipodeActividad.Descripcion, vTipodeActividad.IdTipodeActividad.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void ClaseActividad(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<ClaseActividad> vDClaseActividad = vProveedorService.ConsultarClaseActividads(null, null, true).OrderByDescending(x => x.Descripcion).ToList<ClaseActividad>();
        foreach (ClaseActividad vClaseActividad in vDClaseActividad)
        {
           pDropDownList.Items.Insert(0, new ListItem(vClaseActividad.Descripcion, vClaseActividad.IdClaseActividad.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="IdTipodeActividad"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarClasedeEntidad(DropDownList pDropDownList, int IdTipodeActividad, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<ClasedeEntidad> vClasedeEntidades = vProveedorService.ConsultarClasedeEntidadAll(IdTipodeActividad);
        foreach (ClasedeEntidad vClasedeEntidad in vClasedeEntidades)
        { pDropDownList.Items.Insert(0, new ListItem(vClasedeEntidad.Descripcion, vClasedeEntidad.IdClasedeEntidad.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegablexs
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="IdTipodeActividad"></param>
    /// <param name="pIdTipoEntidad"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    

    public void LlenarClasedeEntidadTipodeActividadTipoEntidad(DropDownList pDropDownList, int IdTipodeActividad, int pIdTipoEntidad, string pPredeterminado, Boolean pMostrarSeleccionar, int pIdTipoPersona, int pIdSector, int? pIdRegmenTributario)
    {
        pDropDownList.Items.Clear();
        List<ClasedeEntidad> vClasedeEntidades = vProveedorService.ConsultarClasedeEntidadTipodeActividadTipoEntidad2(IdTipodeActividad, pIdTipoEntidad, pIdTipoPersona, pIdSector, pIdRegmenTributario);
        foreach (ClasedeEntidad vClasedeEntidad in vClasedeEntidades)
        { pDropDownList.Items.Insert(0, new ListItem(vClasedeEntidad.Descripcion, vClasedeEntidad.IdClasedeEntidad.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoCargoEntidad(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoCargoEntidad> vTipoCargoEntidades = vProveedorService.ConsultarTipoCargoEntidads(null, null, true);
        foreach (TipoCargoEntidad vTipoCargoEntidad in vTipoCargoEntidades)
        {
            pDropDownList.Items.Insert(0, new ListItem(vTipoCargoEntidad.Descripcion, vTipoCargoEntidad.IdTipoCargoEntidad.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(CultureInfo.InvariantCulture); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoDocIdentificacion(DropDownList pDropDownList, string pPredeterminado, bool pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocIdentifica> vTipoDocIdentificas = vProveedorService.ConsultarTipoDocIdentificaAll();
        foreach (TipoDocIdentifica vTipoDocIdentifica in vTipoDocIdentificas)
        {
            pDropDownList.Items.Insert(0, new ListItem(vTipoDocIdentifica.Descripcion, vTipoDocIdentifica.IdTipoDocIdentifica.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarEstadoDatosBasicos(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<EstadoDatosBasicos> vEstadoDatosBasicos = vProveedorService.ConsultarEstadoDatosBasicossAll(true);
        foreach (EstadoDatosBasicos vTipoCargoEntidad in vEstadoDatosBasicos)
        {
            pDropDownList.Items.Insert(0, new ListItem(vTipoCargoEntidad.Descripcion, vTipoCargoEntidad.IdEstadoDatosBasicos.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarEstadoProveedor(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Icbf.Proveedor.Entity.EntidadProvOferente> vEstadoProveedor = vProveedorService.ConsultarEstadoProveedor(true);
        foreach (Icbf.Proveedor.Entity.EntidadProvOferente vEntidadEstadoProveedor in vEstadoProveedor)
        {
            pDropDownList.Items.Insert(0, new ListItem(vEntidadEstadoProveedor.Descripcion, vEntidadEstadoProveedor.IdEstadoProveedor.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    /// <param name="pIdEntidad"></param>
    public void LlenarSucursalEntidad(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int ?pIdEntidad) 
    {
        pDropDownList.Items.Clear();
        List<Sucursal> vSucursales = vProveedorService.ConsultarSucursals(pIdEntidad, 1);
        foreach (Sucursal vSucursal in vSucursales)
        {
            pDropDownList.Items.Insert(0, new ListItem(vSucursal.Nombre, vSucursal.IdSucursal.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        pDropDownList.Items.Insert(1, new ListItem("Ninguno", "0"));
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }


    #endregion

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoPersonaParameter(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoPersona> vTiposPersonas = vOferenteService.ConsultarTipoPersonas(null, null, true);
        int contadorRegistro = 0;
        foreach (TipoPersona vTipoPersona in vTiposPersonas.Where(D => D.Estado))
        {
            pDropDownList.Items.Insert(contadorRegistro, new ListItem(vTipoPersona.NombreTipoPersona, vTipoPersona.IdTipoPersona.ToString()));
            contadorRegistro = contadorRegistro + 1;
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarEstadosTercero(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Icbf.Oferente.Entity.EstadoTercero> vEstadosTercero = vProveedorService.ConsultarEstadosTerceros(true);
        foreach (Icbf.Oferente.Entity.EstadoTercero vEstadotercero in vEstadosTercero)
        {
            pDropDownList.Items.Insert(0, new ListItem(vEstadotercero.DescripcionEstado.ToUpper(), vEstadotercero.IdEstadoTercero.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }


    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarExperienciaDepartamento(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<ExperienciaDepartamento> vListExperienciaDepartamento = vOferenteService.ConsultarExperienciaDepartamento();
        foreach (ExperienciaDepartamento vExperienciaDepartamento in vListExperienciaDepartamento)
        {
            pDropDownList.Items.Insert(0, new ListItem(vExperienciaDepartamento.NombreDepartamento, vExperienciaDepartamento.IdDepartamento.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    /// <param name="pIdDepartamento"></param>
    public void LlenarExperienciaMunicipio(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int pIdDepartamento)
    {
        pDropDownList.Items.Clear();
        List<ExperienciaMunicipio> vListExperienciaMunicipio = vOferenteService.ConsultarExperienciaMunicipio(pIdDepartamento);
        foreach (ExperienciaMunicipio vExperienciaMunicipio in vListExperienciaMunicipio)
        {
            pDropDownList.Items.Insert(0, new ListItem(vExperienciaMunicipio.NombreMunicipio, vExperienciaMunicipio.IdMunicipio.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarSegmento(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        string textDdl = string.Empty;
        List<TipoCodigoUNSPSC> vTipoCodigoUNSPSC = vProveedorService.ConsultarSegmento();
        foreach (TipoCodigoUNSPSC iSegmento in vTipoCodigoUNSPSC)
        {
            textDdl = iSegmento.CodigoSegmento + " - " + iSegmento.NombreSegmento;
            pDropDownList.Items.Add(new ListItem(textDdl, iSegmento.CodigoSegmento.ToString()));
        }

        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    public void LlenarFamilia(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, String pCodigoSegmento)
    {
        pDropDownList.Items.Clear();
        string textDdl = string.Empty;
        List<TipoCodigoUNSPSC> vTipoCodigoUNSPSC = vProveedorService.ConsultarFamilia(pCodigoSegmento);
        foreach (TipoCodigoUNSPSC iSegmento in vTipoCodigoUNSPSC)
        {
            textDdl = iSegmento.CodigoFamilia + " - " + iSegmento.NombreFamilia;
            pDropDownList.Items.Add(new ListItem(textDdl, iSegmento.CodigoFamilia.ToString()));
        }

        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    public void LlenarClase(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, String pCodigoFamilia)
    {
        pDropDownList.Items.Clear();
        string textDdl = string.Empty;
        List<TipoCodigoUNSPSC> vTipoCodigoUNSPSC = vProveedorService.ConsultarClase(pCodigoFamilia);
        foreach (TipoCodigoUNSPSC iSegmento in vTipoCodigoUNSPSC)
        {
            textDdl = iSegmento.CodigoClase + " - " + iSegmento.NombreClase;
            pDropDownList.Items.Add(new ListItem(textDdl, iSegmento.CodigoClase.ToString()));
        }

        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    public void LlenarProducto(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, String pCodigoClase)
    {
        pDropDownList.Items.Clear();
        string textDdl = string.Empty;
        List<TipoCodigoUNSPSC> vTipoCodigoUNSPSC = vProveedorService.ConsultarTipoCodigoUNSPSCs(null,null,true,null,null,null,pCodigoClase);
        foreach (TipoCodigoUNSPSC iSegmento in vTipoCodigoUNSPSC)
        {
            textDdl = Convert.ToString(iSegmento.Codigo) + " - " + iSegmento.Descripcion;
            pDropDownList.Items.Add(new ListItem(textDdl, iSegmento.Codigo.ToString()));
        }

        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarTipoPersonaIntegrante(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoPersona> vTiposPersonas = vOferenteService.ConsultarTipoPersonas(null, null, true);

        foreach (TipoPersona vTipoPersona in vTiposPersonas.Where(D => D.Estado))
        {
            if ((vTipoPersona.NombreTipoPersona == "NATURAL" || vTipoPersona.NombreTipoPersona == "JURIDICA"))
            {
                pDropDownList.Items.Add(new ListItem(vTipoPersona.NombreTipoPersona, vTipoPersona.IdTipoPersona.ToString())); }
             
        }
        //pDropDownList.DataBind();
     
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarNombreRegional(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Regional> vRegional = vContratoService.ConsultarRegionals(null, null);

        vRegional = vRegional.OrderByDescending(e => e.NombreRegional).ToList();

        foreach (Regional vRegionalIncremento in vRegional)
        { pDropDownList.Items.Insert(0, new ListItem(vRegionalIncremento.NombreRegional.ToString().ToUpper(), vRegionalIncremento.IdRegional.ToString())); }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarTipoDocumentoAll(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            pDropDownList.Items.Insert(0, (new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString())));
            
        }
        //pDropDownList.DataBind(); 
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    #region REPORTE DENUNICIAS

    /// <summary>
    /// Método para Consultar los Tipos de Persona o Asociación.
    /// </summary>   
    //public void LlenarTipoPersonaAsociacion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    //{
    //    pDropDownList.Items.Clear();
    
    //List<Icbf.Mostrencos.Entity.ReporteDenuncias> vListaPersonaAsociacion = vMostrencosService.ConsultarTipoPersonaAsociacion();
    //foreach (Icbf.Mostrencos.Entity.ReporteDenuncias vTipoPersonaAsociacion in vListaPersonaAsociacion)
    //{ pDropDownList.Items.Insert(0, new ListItem(vTipoPersonaAsociacion.NombreTipoPersonaAsociacion.ToUpper(), vTipoPersonaAsociacion.IdTipoPersonaAsociacion.ToString())); }
    //if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
    //if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    //}

    #endregion REPORTEDENUNICIAS
}
