﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Service;
using System.IO;
using Icbf.SIA.Entity.Concursales;

/// <summary>
/// Clase con métodos generales para uso de DropDownList y RadioButtonList para la supervision de contratos
/// </summary>
public class ManejoControlesConcursales
{
    SIAService vSiaService;
    
    public ManejoControlesConcursales()
    {
        vSiaService = new SIAService(); 
    }

    public void LlenarComboTipoIdentificacionNMF(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vSiaService.ConsultarTiposDocumentoNMF(null,null);
        foreach(TipoDocumento tD in vLTipoDoc)
        {
           pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.Codigo.ToString())); 
        }

        if(pMostrarSeleccionar)
        pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void LlenarRegionalPCI(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Regional> vRegionals = vSiaService.ConsultarRegionalPCIs(null, null);
        foreach(Regional vRegional in vRegionals)
        { pDropDownList.Items.Insert(0, new ListItem(vRegional.NombreRegional, vRegional.CodigoRegional.ToString())); }
        //pDropDownList.DataBind();
        if(pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if(pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public int CargarArchivoFTPConcursales(FileUpload FileUploadArchivoContrato, ProcesoConcursalDocumento info)
    {
        int idDocumento = 0;

        FileUpload fuArchivo = FileUploadArchivoContrato;

        try
        {
            string filename = Path.GetFileName(fuArchivo.FileName);

            if(fuArchivo.PostedFile.ContentLength / 1024 > 4096)
                throw new GenericException("Sólo se permiten archivos inferiores a 4MB");

            if(filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() != "PDF")
                throw new GenericException("Solo se permiten archivos PDF");

            info.NombreDocumento = string.Format("{0}.Pdf",info.NombreDocumento);

            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                      System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/ProcesosConcursales/";

            info.NombreOriginal = filename;

            HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

            if(vSiaService.CrearDirectorioFTP(NombreArchivoFtp))
            {
               idDocumento = new ConcursalesService().InsertarDocumentoProcesoConcursal(info);

                bool archivoSubido = false;

                if(idDocumento > 0)
                {
                    archivoSubido = vSiaService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, info.NombreDocumento);

                    if(!archivoSubido)
                    {
                        new ConcursalesService().EliminarDocumentoProcesoConcursal(idDocumento);
                        idDocumento = 0;
                    }
                }
            }
            else
                throw new GenericException("No existe la carpeta asociada a las solicitudes de cotnrato");            
        }
        catch(Exception ex)
        {
            throw ex;
        }

        return idDocumento;
    }


}
