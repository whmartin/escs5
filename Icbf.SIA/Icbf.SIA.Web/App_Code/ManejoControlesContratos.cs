﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using WsContratosPacco;
using System.ServiceModel;
using System.IO;
using Icbf.Utilities.Exceptions;
using System.Web.UI;
using System.Text;
using Icbf.Contrato.Entity.PreContractual;

/// <summary>
/// Clase con métodos generales para uso de DropDownList y RadioButtonList
/// </summary>
public partial class ManejoControlesContratos
{
    private ContratoService vContratoService;
    private SIAService vSiaService;
    private OferenteService vOferenteService;
    private EstudioSectorCostoService vEstudioSectorCostoService;
    private WSContratosPACCOSoap _wsContratoPacco = new WSContratosPACCOSoapClient();
    private PreContractualService vPrecontractualService = new PreContractualService();

    public ManejoControlesContratos()
    {
        vContratoService = new ContratoService();
        vSiaService = new SIAService();
        vOferenteService = new OferenteService();
        vEstudioSectorCostoService = new EstudioSectorCostoService();
        vPrecontractualService = new PreContractualService();
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="combo"></param>
    public static void InicializarCombo(DropDownList combo)
    {
        combo.DataBind();
        combo.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        combo.SelectedValue = "-1";
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="combo"></param>
    /// <param name="lista"></param>
    /// <param name="dataValue"></param>
    /// <param name="dataText"></param>
    public static void LlenarComboLista(DropDownList combo, object lista, string dataValue, string dataText)
    {
        combo.DataSource = lista;
        combo.DataValueField = dataValue;
        combo.DataTextField = dataText;
        combo.DataBind();
        combo.Items.Insert(0, new ListItem("SELECCIONAR", "-1"));
        combo.SelectedValue = "-1";
    }

    public static void LlenarComboLista(ListBox combo, object lista, string dataValue, string dataText)
    {
        combo.DataSource = lista;
        combo.DataValueField = dataValue;
        combo.DataTextField = dataText;
        combo.DataBind();
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="combo"></param>
    /// <param name="lista"></param>
    /// <param name="dataValue"></param>
    /// <param name="dataText"></param>
    public static void LlenarListBoxLista(ListBox combo, object lista, string dataValue, string dataText)
    {
        combo.DataSource = lista;
        combo.DataValueField = dataValue;
        combo.DataTextField = dataText;
        combo.DataBind();
        combo.Items.Insert(0, new ListItem("Seleccione", "-1"));
        combo.SelectedValue = "-1";
    }

    public static void LlenarCheckBoxLista(CheckBoxList combo, object lista, string dataValue, string dataText)
    {
        combo.DataSource = lista;
        combo.DataValueField = dataValue;
        combo.DataTextField = dataText;
        combo.DataBind();
    }

    /// <summary>
    /// Inicializa valores para un control lista radio buttons
    /// </summary>
    /// <param name="rblist"></param>
    /// <param name="valorTrue"></param>
    /// <param name="valorFalse"></param>
    public static void ValoresTrueFalseRadioButtonList(RadioButtonList rblist, string valorTrue, string valorFalse)
    {
        rblist.Items.Insert(0, new ListItem(valorTrue, "True"));
        rblist.Items.Insert(1, new ListItem(valorFalse, "False"));
        rblist.SelectedValue = "True";
    }

    /// <summary>
    /// Inicializa valores para un control lista desplegables
    /// </summary>
    /// <param name="combo"></param>
    /// <param name="valorTrue"></param>
    /// <param name="valorFalse"></param>
    public static void ValoresTrueFalseDropDownList(DropDownList combo, string valorTrue, string valorFalse)
    {
        combo.Items.Insert(0, new ListItem("Seleccione", "-1"));
        combo.Items.Insert(1, new ListItem(valorTrue, "True"));
        combo.Items.Insert(2, new ListItem(valorFalse, "False"));
        combo.SelectedValue = "-1";
    }

    /// <summary>
    /// Inicializa valores para el control tipos amparos
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTiposAmparos(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoAmparo> vTiposAmparos = vContratoService.ConsultarTipoAmparos(null, null, true);
        foreach (TipoAmparo vTipoAmparo in vTiposAmparos)
        {
            pDropDownList.Items.Insert(0, new ListItem(vTipoAmparo.NombreTipoAmparo, vTipoAmparo.IdTipoAmparo.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para el control unidad Calculo
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarUnidadCalculo(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<UnidadCalculo> vUnidadCalculo = vContratoService.ConsultarUnidadCalculos(null, null, false);
        foreach (UnidadCalculo vUnidadCalc in vUnidadCalculo)
        {
            pDropDownList.Items.Insert(0, new ListItem(vUnidadCalc.Descripcion, vUnidadCalc.IDUnidadCalculo.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para las regionales dependiendo del usuario 
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarRegionalesUsuario(DropDownList pDropDownList, int pIdUsuario, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Regional> vRegionals = vSiaService.ConsultarRegionalsUsuario(pIdUsuario);
        foreach (Regional vRegional in vRegionals)
        { pDropDownList.Items.Insert(0, new ListItem(vRegional.NombreRegional, vRegional.IdRegional.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para la categoria contratos
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarCategoriaContrato(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<CategoriaContrato> vCategoriaContrato = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato vCategoria in vCategoriaContrato)
        { pDropDownList.Items.Insert(0, new ListItem(vCategoria.NombreCategoriaContrato, vCategoria.IdCategoriaContrato.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa valores para el tipo de contrato
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarTipoContrato(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int pIdCategoriaContrato)
    {
        pDropDownList.Items.Clear();
        List<TipoContrato> vTipoContrato = vContratoService.ConsultarTipoContratos(null, pIdCategoriaContrato, null, null, null, null, null, null);
        foreach (TipoContrato vTipo in vTipoContrato)
        { pDropDownList.Items.Insert(0, new ListItem(vTipo.NombreTipoContrato, vTipo.IdTipoContrato.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarTipoGarantia(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoGarantia> vTipoGarantia = vContratoService.ConsultarTipoGarantias(null, null);
        foreach (TipoGarantia vTipoG in vTipoGarantia)
        {
            if (vTipoG.Estado.Equals(true))
                pDropDownList.Items.Insert(0, new ListItem(vTipoG.NombreTipoGarantia, vTipoG.IdTipoGarantia.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    #region TipoPersona
    public void LlenarTipoPersonaParameter(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoPersona> vTiposPersonas = vOferenteService.ConsultarTipoPersonas(null, null, true);
        foreach (TipoPersona vTipoPersona in vTiposPersonas.Where(D => D.Estado))
        {
            if (vTipoPersona.NombreTipoPersona == "JURIDICA")
                pDropDownList.Items.Insert(0, new ListItem(vTipoPersona.NombreTipoPersona, vTipoPersona.IdTipoPersona.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    #endregion

    #region Departamento
    public void LlenarDepartamentos(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Departamento> vDepartamentos = vSiaService.ConsultarDepartamentos(null, null, null).OrderByDescending(x => x.NombreDepartamento).ToList();


        foreach (Departamento vdDepartamento in vDepartamentos)
        {
            pDropDownList.Items.Insert(0, new ListItem(vdDepartamento.NombreDepartamento, vdDepartamento.IdDepartamento.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    #endregion

    #region Municipio
    public void LlenarMunicipios(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? pIdDepartamento)
    {
        pDropDownList.Items.Clear();
        List<Municipio> vMunicipios = vSiaService.ConsultarMunicipios(pIdDepartamento, null, null);
        foreach (Municipio vMunicipio in vMunicipios)
        {
            pDropDownList.Items.Insert(0, new ListItem(vMunicipio.NombreMunicipio.ToUpper(), vMunicipio.IdMunicipio.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    #endregion

    #region Plan de Compras

    public void LlenarRegionalesPlanCompras(DropDownList pDropDownList, String pDataTexField, String pDataValueField, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();

        var vListRegionales = _wsContratoPacco.GetListaRegionales();

        var vLstWsRegional = new List<ListaRegionales>();

        vLstWsRegional.AddRange(from vlstRegional in vListRegionales
                                select new ListaRegionales()
                                {
                                    NombreRegional = vlstRegional.CodRegional.Substring(0, 2) + " - " + vlstRegional.NombreRegional,
                                    CodRegional = vlstRegional.CodRegional
                                }
                                );

        pDropDownList.DataSource = vLstWsRegional;

        pDropDownList.DataTextField = pDataTexField;
        pDropDownList.DataValueField = pDataValueField;
        pDropDownList.DataBind();
        if (pMostrarSeleccionar)
        {
            pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
            pDropDownList.SelectedValue = "-1";
        }
    }

    public void LlenarEstadosPlanCompras(DropDownList pDropDownList, int pVigencia, String pDataTexField, String pDataValueField, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();

        pDropDownList.DataSource = _wsContratoPacco.GetEstadosPlanCompras(pVigencia);
        pDropDownList.DataTextField = pDataTexField;
        pDropDownList.DataValueField = pDataValueField;
        pDropDownList.DataBind();
        if (pMostrarSeleccionar)
        {
            pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
            pDropDownList.SelectedValue = "-1";
        }
    }

    public void LlenarDependenciaPlanCompras(DropDownList pDropDownList, int pVigencia, String pDataTexField, String pDataValueField, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        try
        {
            var vGetAreas = _wsContratoPacco.GetAreas(pVigencia);
            var vListGetAreas = new List<GetAreasYCodigos_Result>();

            if (vGetAreas.Any())
            {
                vListGetAreas.AddRange(from vListAre in vGetAreas
                                       orderby vListAre.CodigoArea ascending
                                       select vListAre);
            }

            pDropDownList.DataSource = vListGetAreas;
            pDropDownList.DataTextField = pDataTexField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataBind();
            if (pMostrarSeleccionar)
            {
                pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
                pDropDownList.SelectedValue = "-1";
            }
        }
        catch (FaultException)
        {
            throw new Exception("Se produjo un error al intentar cargar las dependencias desde Pacco");
        }
    }

    public void LlenarUsuariosDuenoPlanCompras(DropDownList pDropDownList, int pVigencia, String pDataTexField, String pDataValueField, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();

        pDropDownList.DataSource = _wsContratoPacco.GetUsuariosPlanCompras(pVigencia);
        pDropDownList.DataTextField = pDataTexField;
        pDropDownList.DataValueField = pDataValueField;
        pDropDownList.DataBind();
        if (pMostrarSeleccionar)
        {
            pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
            pDropDownList.SelectedValue = "-1";
        }
    }

    public void LlenarValoresPlanCompras(DropDownList pDropDownList, int? pIdValoresContratos, String pDataTexField, String pDataValueField, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();

        var vValoresPlanComprasContratos = vContratoService.ConsultarValoresPlanComprasContratos(pIdValoresContratos);
        var vListValores = new List<PlanComprasContratos>();
        if (vValoresPlanComprasContratos.Any())
        {
            vListValores.AddRange(from vValores in vValoresPlanComprasContratos
                                  select new PlanComprasContratos
                                  {
                                      IdValoresContrato = vValores.IdValoresContrato,
                                      Desde = decimal.Parse(vValores.Desde, NumberStyles.Currency).ToString("$ #,###0.00##;($ #,###0.00##)") + " - " +
                                              decimal.Parse(vValores.Hasta, NumberStyles.Currency).ToString("$ #,###0.00##;($ #,###0.00##)")
                                  });
        }
        pDropDownList.DataSource = vListValores;
        pDropDownList.DataTextField = pDataTexField;
        pDropDownList.DataValueField = pDataValueField;
        pDropDownList.DataBind();
        if (pMostrarSeleccionar)
        {
            pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
            pDropDownList.SelectedValue = "-1";
        }
    }

    #endregion

    #region Estados Garantias
    public void LlenarEstadosGarantias(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<EstadosGarantias> veEstadosGarantias = vContratoService.ConsultarEstadosGarantiass(null, null);
        foreach (EstadosGarantias vestadGarantia in veEstadosGarantias)
        { pDropDownList.Items.Insert(0, new ListItem(vestadGarantia.DescripcionEstadoGarantia, vestadGarantia.IDEstadosGarantias.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    #endregion

    #region Tipo Identificación

    public void LlenarComboTipoIdentificacion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vSiaService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "CC") || (tD.Codigo == "CE"))
            { pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.Codigo.ToString())); }
        }
        pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }
    #endregion

    #region LlenarComboRegional

    public void LlenarComboRegional(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Regional> vLRegional = vSiaService.ConsultarRegionals(null, null);
        foreach (Regional tD in vLRegional)
        {
            pDropDownList.Items.Add(new ListItem(tD.NombreRegional, tD.CodigoRegional.ToString()));
        }
        pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    #endregion

    #region Manejo de Archivos

    public bool CargarArchivoFTPModificacionContractual(string tipoEstructura, FileUpload FileUploadArchivoContrato, int idContrato, int idConsModContractual, int idUsusario, string nombreComplemento = "")
    {
        String NombreArchivoOri;
        string NombreArchivo;
        decimal idarchivo = 0;
        Boolean ArchivoSubido;

        FileUpload fuArchivo = FileUploadArchivoContrato;

        try
        {
            string filename = Path.GetFileName(fuArchivo.FileName);

            if (fuArchivo.PostedFile.ContentLength / 1024 > 4096)
                throw new GenericException("Sólo se permiten archivos inferiores a 4MB");

            NombreArchivoOri = filename;
            NombreArchivo = filename.Substring(0, filename.IndexOf("."));
            NombreArchivo = new ContratoService().QuitarAcentos(NombreArchivo);

            string tipoArchivo = "";
            if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() != "PDF")
                throw new GenericException("Solo se permiten archivos PDF");

            if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() == "PDF")
                tipoArchivo = "General.archivoPDF";

            List<Icbf.SIA.Entity.FormatoArchivo> vFormatoArchivo = vSiaService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

            if (vFormatoArchivo.Count == 0)
            {
                throw new Exception("No se ha parametrizado los formatos de archivo");
            }

            int idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

            if (nombreComplemento == string.Empty)
                NombreArchivo = Guid.NewGuid().ToString() + "_Contratos_" + tipoEstructura + filename.Substring(filename.LastIndexOf("."));
            else
                NombreArchivo = Guid.NewGuid().ToString() + "_Contratos_" + tipoEstructura + "_" + nombreComplemento + "_" + filename.Substring(filename.LastIndexOf("."));

            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                      System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

            //Guardado del archivo en el servidor
            HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

            ArchivoSubido = vSiaService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

            if (ArchivoSubido)
            {
                ArchivoContrato RegistroControl = new ArchivoContrato();
                RegistroControl.IdUsuario = idUsusario;
                RegistroControl.IdContrato = idContrato;
                RegistroControl.IdFormatoArchivo = idformatoArchivo;
                RegistroControl.ServidorFTP = NombreArchivoFtp;
                RegistroControl.NombreArchivo = NombreArchivo;
                RegistroControl.NombreArchivoOri = NombreArchivoOri;
                RegistroControl.Estado = "C";
                RegistroControl.ResumenCarga = string.Empty;
                RegistroControl.UsuarioCrea = "AdminSIA";
                RegistroControl.TipoEstructura = tipoEstructura;
                RegistroControl.FechaCrea = DateTime.Now;
                RegistroControl.IdTabla = idConsModContractual.ToString();
                vContratoService.InsertarArchivoAnexo(RegistroControl);
                idarchivo = RegistroControl.IdArchivo;
                return ArchivoSubido;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return ArchivoSubido;
    }
    public bool CargarArchivoFTPContratos(string tipoEstructura, FileUpload FileUploadArchivoContrato, int idContrato, int idUsusario, string nombreComplemento = "")
    {
        String NombreArchivoOri;
        string NombreArchivo;
        decimal idarchivo = 0;
        Boolean ArchivoSubido;

        FileUpload fuArchivo = FileUploadArchivoContrato;

        try
        {
            string filename = Path.GetFileName(fuArchivo.FileName);

            if (fuArchivo.PostedFile.ContentLength / 1024 > 4096)
                throw new GenericException("Sólo se permiten archivos inferiores a 4MB");

            NombreArchivoOri = filename;
            NombreArchivo = filename.Substring(0, filename.IndexOf("."));
            NombreArchivo = new ContratoService().QuitarAcentos(NombreArchivo);

            string tipoArchivo = "";
            if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() != "PDF")
                throw new GenericException("Solo se permiten archivos PDF");

            if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() == "PDF")
                tipoArchivo = "General.archivoPDF";

            List<Icbf.SIA.Entity.FormatoArchivo> vFormatoArchivo = vSiaService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

            if (vFormatoArchivo.Count == 0)
            {
                throw new Exception("No se ha parametrizado los formatos de archivo");
            }

            int idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

            if (nombreComplemento == string.Empty)
                NombreArchivo = Guid.NewGuid().ToString() + "_Contratos_" + tipoEstructura + filename.Substring(filename.LastIndexOf("."));
            else
                NombreArchivo = Guid.NewGuid().ToString() + "_Contratos_" + tipoEstructura + "_" + nombreComplemento + "_" + filename.Substring(filename.LastIndexOf("."));

            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                      System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

            //Guardado del archivo en el servidor
            HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

            ArchivoSubido = vSiaService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

            if (ArchivoSubido)
            {
                ArchivoContrato RegistroControl = new ArchivoContrato();
                RegistroControl.IdUsuario = idUsusario;
                RegistroControl.IdContrato = idContrato;
                RegistroControl.IdFormatoArchivo = idformatoArchivo;
                RegistroControl.ServidorFTP = NombreArchivoFtp;
                RegistroControl.NombreArchivo = NombreArchivo;
                RegistroControl.NombreArchivoOri = NombreArchivoOri;
                RegistroControl.Estado = "C";
                RegistroControl.ResumenCarga = string.Empty;
                RegistroControl.UsuarioCrea = "AdminSIA";
                RegistroControl.TipoEstructura = tipoEstructura;
                RegistroControl.FechaCrea = DateTime.Now;
                vContratoService.InsertarArchivoAnexo(RegistroControl);
                idarchivo = RegistroControl.IdArchivo;
                return ArchivoSubido;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return ArchivoSubido;
    }

    public bool CargarArchivoFTPSolicitudContrato(string tipoEstructura, FileUpload FileUploadArchivoContrato, int idSolicitudContrato, int idTipoDocumento, string usuario)
    {
        String NombreArchivoOri;
        string NombreArchivo;
        Boolean ArchivoSubido = false;

        FileUpload fuArchivo = FileUploadArchivoContrato;

        try
        {
            string filename = Path.GetFileName(fuArchivo.FileName);

            if (fuArchivo.PostedFile.ContentLength / 1024 > 4096)
                throw new GenericException("Sólo se permiten archivos inferiores a 4MB");

            NombreArchivoOri = filename;
            NombreArchivo = filename.Substring(0, filename.IndexOf("."));
            NombreArchivo = new ContratoService().QuitarAcentos(NombreArchivo);

            if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() != "PDF")
                throw new GenericException("Solo se permiten archivos PDF");

            string result = Guid.NewGuid().ToString().Substring(0, 4);

            NombreArchivo = "SolicitudContrato_" + idSolicitudContrato.ToString() + "_" + result + "_" + tipoEstructura + filename.Substring(filename.LastIndexOf("."));

            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                      System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/SolicitudesContrato/";

            HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

            if (vSiaService.CrearDirectorioFTP(NombreArchivoFtp))
            {
                ArchivoSubido = vSiaService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

                if (ArchivoSubido)
                {
                    SolicitudContratoDocumentos sol = new SolicitudContratoDocumentos();
                    sol.IdSolicitudContrato = idSolicitudContrato;
                    sol.IdTipoDocumento = idTipoDocumento;
                    sol.NombreArchivo = NombreArchivo;
                    sol.RutaArchivo = "SolicitudesContrato/" + NombreArchivo;
                    sol.UsuarioCrea = usuario;
                    var item = vPrecontractualService.InsertarSolicitudContratoDocumentos(sol);

                    if (item == 0)
                        ArchivoSubido = false;

                    return ArchivoSubido;
                }
            }
            else
            {
                throw new GenericException("No existe la carpeta asociada a las solicitudes de cotnrato");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return ArchivoSubido;
    }

    public bool CargarArchivoFTPActividadSolicitudContrato(FileUpload FileUploadArchivoContrato, string idSolicitudContrato, int idActividad, string usuario)
    {
        String NombreArchivoOri;
        string NombreArchivo;
        Boolean ArchivoSubido = false;

        FileUpload fuArchivo = FileUploadArchivoContrato;

        try
        {
            string filename = Path.GetFileName(fuArchivo.FileName);

            if(fuArchivo.PostedFile.ContentLength / 1024 > 4096)
                throw new GenericException("Sólo se permiten archivos inferiores a 4MB");

            NombreArchivoOri = filename;
            NombreArchivo = filename.Substring(0, filename.IndexOf("."));
            NombreArchivo = new ContratoService().QuitarAcentos(NombreArchivo);

            if(filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() != "PDF")
                throw new GenericException("Solo se permiten archivos PDF");

            string result = Guid.NewGuid().ToString().Substring(0, 4);

            NombreArchivo = "SolContrato_Act_" + idSolicitudContrato.ToString() + "_" + idActividad.ToString() +"_" + result + "_" + filename.Substring(filename.LastIndexOf("."));

            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                      System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/SolicitudesContrato/"+idSolicitudContrato+"/";

            HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

            if(vSiaService.CrearDirectorioFTP(NombreArchivoFtp))
            {
                ArchivoSubido = vSiaService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

                if(ArchivoSubido)
                {
                    SolicitudContratoActividadesDesarrolloDocumentos sol = new SolicitudContratoActividadesDesarrolloDocumentos();
                    sol.IdDesarrolloActividad = idActividad;
                    sol.NombreArchivo = NombreArchivo;
                    sol.NombreOriginal = NombreArchivoOri;
                    sol.RutaArchivo = "SolicitudesContrato/" + idSolicitudContrato + "/" + NombreArchivo;
                    sol.UsuarioCrea = usuario;
                    var item = vPrecontractualService.InsertarDesarrolloActividadDocumentos(sol);

                    if(item == 0)
                        ArchivoSubido = false;

                    return ArchivoSubido;
                }
            }
            else
            {
                throw new GenericException("No existe la carpeta asociada a las solicitudes de cotnrato");
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }

        return ArchivoSubido;
    }

    #endregion

    #region EstudioSectorCoso
    public void LlenarModalidadSeleccion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<ModalidadSeleccion> vModalidadSeleccions = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion vModalidadSeleccion in vModalidadSeleccions)
        {
            pDropDownList.Items.Insert(0, new ListItem(vModalidadSeleccion.Nombre, vModalidadSeleccion.IdModalidad.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarModalidadSeleccionESCs(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<ModalidadesSeleccionESC> vModalidadSeleccions = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESCs(null, null, 1).OrderBy(x => x.ModalidadSeleccion).ToList();
        foreach (ModalidadesSeleccionESC vModalidadSeleccion in vModalidadSeleccions)
        {
            pDropDownList.Items.Add(new ListItem(vModalidadSeleccion.ModalidadSeleccion, vModalidadSeleccion.IdModalidadSeleccion.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarComplejidadIndicador(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Complejidades> vComplejidadess = vEstudioSectorCostoService.ConsultarComplejidadess(null, 0, 1).OrderBy(x => x.Nombre).ToList();
        foreach (Complejidades vComplejidades in vComplejidadess)
        {
            pDropDownList.Items.Insert(0, new ListItem(vComplejidades.Nombre, vComplejidades.IdComplejidad.ToString()));
        }
        List<ListItem> listCopy = new List<ListItem>();
        foreach (ListItem item in pDropDownList.Items)
            listCopy.Add(item);
        pDropDownList.Items.Clear();
        foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            pDropDownList.Items.Add(item);
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    public void LlenarComplejidadInterna(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Complejidades> vComplejidadess = vEstudioSectorCostoService.ConsultarComplejidadess(null, 1, 1);
        foreach (Complejidades vComplejidades in vComplejidadess)
        {
            pDropDownList.Items.Insert(0, new ListItem(vComplejidades.Nombre, vComplejidades.IdComplejidad.ToString()));
        }
        List<ListItem> listCopy = new List<ListItem>();
        foreach (ListItem item in pDropDownList.Items)
            listCopy.Add(item);
        pDropDownList.Items.Clear();
        foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            pDropDownList.Items.Add(item);
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }


    public void LlenarDireccion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<DireccionesSolicitantesESC> vDirecciones = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESCs(null, null, 1).OrderBy(x => x.DireccionSolicitante).ToList();
        foreach (DireccionesSolicitantesESC vDireccion in vDirecciones)
        {
            pDropDownList.Items.Add(new ListItem(vDireccion.DireccionSolicitante, vDireccion.IdDireccionesSolicitantes.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarDireccionPACCO(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int pVigencia)
    {
        pDropDownList.Items.Clear();

        if (pVigencia != 0)
        {
            WsContratosPacco.WSContratosPACCOSoap _wsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
            GetAreasYCodigos_Result[] vDirecciones = _wsContratosPacco.GetAreas(pVigencia);

            foreach (GetAreasYCodigos_Result vDireccion in vDirecciones)
            {
                pDropDownList.Items.Insert(0, new ListItem(vDireccion.area, vDireccion.CodigoArea.ToString()));
            }
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarModalidadPACCO(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int pVigencia)
    {
        pDropDownList.Items.Clear();

        if (pVigencia != 0)
        {
            WsContratosPacco.WSContratosPACCOSoap _wsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
            GetModalidadXVigencia_Result[] vModalidades = _wsContratosPacco.GetModalidades(pVigencia);

            foreach (GetModalidadXVigencia_Result vModalidad in vModalidades)
            {
                pDropDownList.Items.Insert(0, new ListItem(vModalidad.Modalidad, vModalidad.IdModalidad.ToString()));
            }
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarAreas(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? pIdDireccionSolicitanteESC)
    {
        pDropDownList.Items.Clear();
        List<AreasSolicitantesESC> vAreas = vEstudioSectorCostoService.ConsultarAreasSolicitantesESCs(null, pIdDireccionSolicitanteESC, null, 1);

        foreach (AreasSolicitantesESC vArea in vAreas)
        {
            pDropDownList.Items.Insert(0, new ListItem(vArea.AreaSolicitante, vArea.IdAreasSolicitantes.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarTiposEstudio(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TiposEstudio> vTiposEstudios = vEstudioSectorCostoService.ConsultarTiposEstudios(null, null, 1).OrderBy(x => x.Nombre).ToList();

        foreach (TiposEstudio vTiposEstudio in vTiposEstudios)
        {
            pDropDownList.Items.Insert(0, new ListItem(vTiposEstudio.Nombre, vTiposEstudio.IdTipoEstudio.ToString()));
        }
        List<ListItem> listCopy = new List<ListItem>();
        foreach (ListItem item in pDropDownList.Items)
            listCopy.Add(item);
        pDropDownList.Items.Clear();
        foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            pDropDownList.Items.Add(item);
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarEstadoEstudio(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<EstadoEstudio> vEstadoEstudios = vEstudioSectorCostoService.ConsultarEstadoEstudios(null, null, null, 1).OrderBy(x => x.Nombre).ToList();

        String Nombre = "";
        foreach (EstadoEstudio vEstadoEstudio in vEstadoEstudios)
        {
            Nombre = vEstadoEstudio.Nombre.ToUpper();
            if (pDropDownList.Items.FindByText(Nombre) == null)
            {
                pDropDownList.Items.Add(new ListItem(Nombre, vEstadoEstudio.IdEstadoEstudio.ToString()));
            }
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarMotivoSolicitud(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, String pNombre)
    {
        pDropDownList.Items.Clear();

        if (pNombre != null)
        {
            List<EstadoEstudio> vEstadoEstudios = vEstudioSectorCostoService.ConsultarEstadoEstudios(pNombre, null, null, 1);
            foreach (EstadoEstudio vEstadoEstudio in vEstadoEstudios)
            {
                pDropDownList.Items.Add(new ListItem(vEstadoEstudio.Motivo, vEstadoEstudio.IdEstadoEstudio.ToString()));
            }
        }

        if (pMostrarSeleccionar)
        {
            pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1"));
        }

        if (pPredeterminado != null && pPredeterminado.Length > 0)
        {
            pDropDownList.SelectedValue = pPredeterminado.ToString();
        }
    }

    public void LlenarFechaSolicitudInicial(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<int> vListaFechaSolicitudInicial = vEstudioSectorCostoService.ConsultarFechaSolicitudInicial();
        foreach (int vFecha in vListaFechaSolicitudInicial)
        {
            pDropDownList.Items.Add(new ListItem(vFecha.ToString(), vFecha.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void ConsultarAnioCierre(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<int> vListaFechaSolicitudInicial = vEstudioSectorCostoService.ConsultarAnioCierre();
        foreach (int vFecha in vListaFechaSolicitudInicial)
        {
            pDropDownList.Items.Add(new ListItem(vFecha.ToString(), vFecha.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void ConsultarFechaEjecucion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<DateTime> vListaFechaSolicitudInicial = vEstudioSectorCostoService.ConsultarFechaEjecucion();
        foreach (DateTime vFecha in vListaFechaSolicitudInicial)
        {
            pDropDownList.Items.Add(new ListItem(vFecha.ToString("dd/MM/yyyy"), vFecha.ToString("dd/MM/yyyy")));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    public void LlenarResponsablesESCs(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<ResponsablesESC> vListaResponsable = vEstudioSectorCostoService.ConsultarResponsablesESCs(null, 1).OrderBy(x => x.Responsable).ToList();
        foreach (ResponsablesESC vResponsable in vListaResponsable)
        {
            pDropDownList.Items.Add(new ListItem(vResponsable.Responsable, vResponsable.IdResponsable.ToString()));
        }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("SELECCIONE", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }


    #endregion

    #region Generar/Exportar Excel
    public static DataTable GenerarDataTable(GridView grvDatos, object datos)
    {
        DataTable dtDatos = new DataTable();
        // creamos las columnas de la tabla

        foreach (var item in grvDatos.Columns)
        {
            if (item.GetType().ToString() == "System.Web.UI.WebControls.TemplateField")
            {
                TemplateField template = (TemplateField)item;
                if (template.Visible)
                    if (template.HeaderText != string.Empty)
                        dtDatos.Columns.Add(template.HeaderText);
            }
            else if (item.GetType().ToString() == "System.Web.UI.WebControls.BoundField")
            {
                BoundField template = (BoundField)item;
                if (template.Visible)
                    if (template.HeaderText != string.Empty)
                        dtDatos.Columns.Add(template.HeaderText);
            }
        }
        int columnas = dtDatos.Columns.Count;
        // creamos los registros de la tabla
        // consultamos cuantas paginas tiene el grid view
        int paginas = grvDatos.PageCount;
        for (int i = 0; i <= paginas - 1; i++)
        {
            grvDatos.PageIndex = i;
            grvDatos.DataSource = datos;
            grvDatos.DataBind();
            foreach (GridViewRow item in grvDatos.Rows)
            {
                DataRow drNewRow = dtDatos.NewRow();
                for (int j = 0; j <= columnas - 1; j++)
                {
                    if (item.Cells[j].Controls.Count > 0)
                    {
                        foreach (Control var in item.Cells[j].Controls)
                        {
                            if (var.GetType().FullName == "System.Web.UI.WebControls.Label")
                            {
                                if (((Label)var).Visible)
                                    drNewRow[j] = ((Label)var).Text;
                            }
                            else if (var.GetType().FullName == "System.Web.UI.WebControls.TextBox")
                            {
                                if (((TextBox)var).Visible)
                                    drNewRow[j] = ((TextBox)var).Text;
                            }
                        }
                    }
                    else
                    {
                        if (true)
                        {

                        }
                        //drNewRow[j] = ((item.Cells[j].Text).Equals("&nbsp;")) ? string.Empty : item.Cells[j].Text;
                        drNewRow[j] = item.Cells[j].Text;
                    }
                }
                dtDatos.Rows.Add(drNewRow);
            }
        }
        return dtDatos;
    }
    public static string GenerarConsultaExcel(DataTable datos, string pathPlantilla, string urlPathExcel)
    {
        string pathSalida = urlPathExcel;
        // Generar Encabezado
        StringBuilder cadenaResultado;
        StreamReader archivoPlantilla = null;
        string nombreArchivo = "";
        StreamWriter archivoSalida = null;
        string lineaDatos = "";
        Random numeroArchivo;
        try
        {
            cadenaResultado = GenerarConsultaHtml(datos);
            numeroArchivo = new Random();
            nombreArchivo = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + numeroArchivo.Next(100);

            archivoPlantilla = new StreamReader(new FileStream(pathPlantilla, FileMode.Open, FileAccess.Read));
            nombreArchivo = pathSalida + nombreArchivo + ".xls";
            archivoSalida = new StreamWriter(nombreArchivo, false, System.Text.Encoding.Unicode);

            while (lineaDatos != null)
            {
                lineaDatos = archivoPlantilla.ReadLine();
                if (lineaDatos == "{HTML}")
                    archivoSalida.WriteLine(cadenaResultado);
                else
                    archivoSalida.WriteLine(lineaDatos);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el archivo " + nombreArchivo, ex);
        }
        finally
        {
            if (archivoSalida != null)
                archivoSalida.Close();
            if (archivoPlantilla != null)
                archivoPlantilla.Close();
        }
        return Path.GetFileName(nombreArchivo);
    }

    public static StringBuilder GenerarConsultaHtml(DataTable datos)
    {
        DataRow row;
        StringBuilder cadenaResultado = new StringBuilder(50000);
        try
        {
            cadenaResultado.Append("<Table class=EstiloTabla>");
            cadenaResultado.Append("<TR>");

            for (int i = 0; i <= datos.Columns.Count - 1; i++)
            {
                cadenaResultado.Append("<TD class=EstiloTitulo>" + datos.Columns[i].ColumnName);
                cadenaResultado.Append("</TD>");
            }
            cadenaResultado.Append("</TR>");


            if (datos.Rows.Count == 0)
            {
                cadenaResultado.Append("</TR></Table>");
                cadenaResultado.Append("</Table>");
                cadenaResultado.Append("<DIV class=SinDatos> La consulta no arrojó resultados. </DIV>");
            }
            else
            {
                for (int i = 0; i < datos.Rows.Count; i++)
                {
                    row = datos.Rows[i];
                    cadenaResultado.Append("<TR>");
                    for (int j = 0; j <= datos.Columns.Count - 1; j++)
                    {
                        cadenaResultado.Append("<TD aling=left>" + row.ItemArray[j].ToString());
                        cadenaResultado.Append("</TD>");
                    }
                    cadenaResultado.Append("</TR>");
                }
                cadenaResultado.Append("</Table>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Generar Consulta Html", ex);
        }
        return cadenaResultado;
    }

    public static void VisualizarArchivo(HttpResponse Response, string urlReportes, string nombreArchivo)
    {
        LimpiarCarpetaTemporal(urlReportes);
        Response.ClearContent();
        Response.ClearHeaders();
        Response.AddHeader("Pragma", "public");
        Response.AddHeader("Expires", "0");
        Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        Response.AddHeader("Content-Type", "application/force-download");
        Response.AddHeader("Content-Type", "application/download");
        Response.AddHeader("Content-Disposition", "attachment;filename=" + nombreArchivo);
        Response.WriteFile(urlReportes + "/" + nombreArchivo);
        Response.Flush();
        Response.End();
    }

    private static void LimpiarCarpetaTemporal(string urlReportes)
    {
        if (Directory.Exists(urlReportes))
        {
            string[] archivos = Directory.GetFiles(urlReportes);
            foreach (var item in archivos)
            {
                DateTime dtFechaCreacion = File.GetCreationTime(item);

            }
        }
    }

    public static List<List<string>> Split(List<string> source, int numeroRegistros)
    {
        return source
            .Select((x, i) => new { Index = i, Value = x })
            .GroupBy(x => x.Index / numeroRegistros)
            .Select(x => x.Select(v => v.Value).ToList())
            .ToList();
    }

    public static string convertirCaracteresEspeciales(string pCadena)
    {
        string vCadenaNueva = pCadena.Replace("&#193;", "Á");
        vCadenaNueva = vCadenaNueva.Replace("&#201;", "É");
        vCadenaNueva = vCadenaNueva.Replace("&#205;", "Í");
        vCadenaNueva = vCadenaNueva.Replace("&#211;", "Ó");
        vCadenaNueva = vCadenaNueva.Replace("&#218;", "Ú");

        vCadenaNueva = vCadenaNueva.Replace("&#225;", "á");
        vCadenaNueva = vCadenaNueva.Replace("&#233;", "é");
        vCadenaNueva = vCadenaNueva.Replace("&#237;", "í");
        vCadenaNueva = vCadenaNueva.Replace("&#243;", "ó");
        vCadenaNueva = vCadenaNueva.Replace("&#250;", "ú");

        vCadenaNueva = vCadenaNueva.Replace("&#209;", "Ñ");
        vCadenaNueva = vCadenaNueva.Replace("&#241;", "ñ");

        vCadenaNueva = vCadenaNueva.Replace("&gt;", ">");
        vCadenaNueva = vCadenaNueva.Replace("&#61;", "=");
        vCadenaNueva = vCadenaNueva.Replace("&lt;", "<");
        vCadenaNueva = vCadenaNueva.Replace("&nbsp;", "");
        return vCadenaNueva;
    }

    #endregion

    #region SIGUV

    public void LlenarRegional(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Regional> vRegionals = vSiaService.ConsultarRegionalPCIs(null, null);
        foreach (Regional vRegional in vRegionals)
        { pDropDownList.Items.Insert(0, new ListItem(vRegional.NombreRegional, vRegional.IdRegional.ToString())); }
        //pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    #endregion
}