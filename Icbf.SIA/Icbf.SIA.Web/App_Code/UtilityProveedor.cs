﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Provee el ContentType a descargar según el nombre de archivo
/// </summary>
public static class UtilityProveedor
{
    public static string GetContentType(string pNombreArchivo)
    {
        string[] partes = pNombreArchivo.Split('.');
        string extension = partes[1];
        string contentType = string.Empty;
        switch (extension)
        {
            case "jpg":
                contentType = "image/jpg";
                break;
            case "pdf":
                contentType = "application/pdf";
                break;
        }
        return contentType;
    }
}