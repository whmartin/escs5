﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Descripción breve de Enumerator
/// </summary>
public class Enumeradores
{
    public Enumeradores()
	{
		//
		// TODO: Agregar aquí la lógica del constructor
		//
	}
    
    public enum TipoIncidente
    {
        Ninguno = 0,
        Subsanable = 1,
        NoSubsanable = 2
    }
}