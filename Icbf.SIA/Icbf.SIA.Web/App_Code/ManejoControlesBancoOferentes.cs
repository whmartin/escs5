﻿using Icbf.Oferente.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descripción breve de ManejoControlesBancoOferentes
/// </summary>
public class ManejoControlesBancoOferentes
{

    public  static void LlenarComboTipoObservacion(DropDownList pDropDownList, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        var tipoObservaciones = new BancoOferentesService().ConsultarTipoObservaciones();

        foreach(KeyValuePair<int,string> tD in tipoObservaciones)
            pDropDownList.Items.Add(new ListItem(tD.Value, tD.Key.ToString()));

        if(pMostrarSeleccionar)
            pDropDownList.Items.Insert(0, new ListItem("SELECCIONAR", "-1"));
    }

}