﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using System.Configuration;
using System.Web.Services;
using System.Web.Security;
using Icbf.SIA.Service;

/// <summary>
/// Página de login para los usuarios internos
/// </summary> 
public partial class DefaultDenuncias : GeneralWeb
{

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.mpeMessage.Show();

        if (!IsPostBack)
        {
            GetVersion();

            //read de cocckie ----20140226 --
            HttpCookie aCookie = Request.Cookies["IdSession"];
            if (aCookie != null)
            {
                string username = Server.HtmlEncode(aCookie.Value);

                if (Session["Mysession"] != null)
                {
                    if (username == Session["Mysession"].ToString())
                    {
                        NavigateTo(@"General/General/Master/MasterPrincipal.aspx", true);
                    }
                }
            }
            //read de cocckie ----20140226 --fin
        }
    }

    /// <summary>
    /// Raises the LoggedIn event after the user logs in to the Web site and has been authenticated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoggedIn(object sender, EventArgs e)
    {
        ValidateUser(loGeneraciones.UserName, loGeneraciones.Password);

        String sessionId;
        sessionId = Session.SessionID;
        HttpCookie loginCookie1 = new HttpCookie("IdSession");
        loginCookie1.Value = sessionId;
        Response.Cookies.Add(loginCookie1);
        Session["Mysession"] = sessionId;

        NavigateTo(@"General/General/Master/MasterPrincipal.aspx", true);         
    }

    /// <summary>
    /// Raises the LoggingIn event when a user submits login information but before the authentication takes place
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoggingIn(object sender, LoginCancelEventArgs e)
    {
        HttpCookie aCookie = Request.Cookies["IdSession"];
        if (aCookie != null)
        {
            string username = Server.HtmlEncode(aCookie.Value);

            if (Session["Mysession"] != null)
            {
                if (username == Session["Mysession"].ToString())
                {
                    e.Cancel = true;
                    Response.Redirect("Error.aspx");
                }
            }
        }


        if (Session["MaxInvalidPasswordAttempts"] == null && Session["Attempts"] == null)
        {
            Session["MaxInvalidPasswordAttempts"] = Convert.ToInt16(ConfigurationManager.AppSettings["MaxInvalidPasswordAttempts"]);
            Session["Attempts"] = 1;
        }

        var usuario = Membership.GetUser(loGeneraciones.UserName);
        if (usuario != null)
        {
            this.loGeneraciones.FailureText = @"La contraseña es incorrecta, inténtelo de nuevo.";
            if (usuario.IsApproved == false)
            {
                if (loGeneraciones.Password.Equals(usuario.GetPassword()))
                {
                    SIAService vRUBOService = new SIAService();
                    if (vRUBOService.ConsultarUsuario(loGeneraciones.UserName).Rol.Split(';').Contains("PROVEEDORES"))
                    {
                        Session["usuarioProveedorLog"] = usuario;
                        NavigateTo(@"Account/CodeActivationProveedores.aspx", true);
                    }
                }
            }
        }
        else
        {
            this.loGeneraciones.FailureText = @"Usuario no existe, puede crearlo por la opción Registrarme.";
        }

    }

    /// <summary>
    /// Raises the LoginError event when a login attempt fails.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoginError(object sender, EventArgs e)
    {
        if (Convert.ToInt16(Session["Attempts"]) < Convert.ToInt16(Session["MaxInvalidPasswordAttempts"]) +1 )
        {
            Session["Attempts"] = Convert.ToInt16(Session["Attempts"]) + 1;
        }
       
    }

    /// <summary>
    /// Manejador del evento click para el botón PasswordRecovery
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PasswordRecoveryLinkButton_Click(object sender, EventArgs e)
    {
        string textoCorreo = this.loGeneraciones.UserName;

        if (!textoCorreo.Equals("") && !textoCorreo.Equals("aa@example.com"))
        {

            Response.Redirect(@"RecoverPassword.aspx?correo=" + textoCorreo);
        }
        else
        {
            Response.Redirect(@"RecoverPassword.aspx");
        }
    }
    /// <summary>
    /// Manejador del evento click para el botón Register
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkRegister_Click(object sender, EventArgs e)
    {
        string textoCorreo = this.loGeneraciones.UserName;

        if (!textoCorreo.Equals("") && !textoCorreo.Equals("aa@example.com"))
        {
            Response.Redirect(@"~/Account/RegisterProveedor.aspx?correo=" + textoCorreo);
        }
        else
        {
            Response.Redirect(@"~/Account/RegisterProveedor.aspx");
        }

    }

    protected void btnOkCodigo_Click(object sender, EventArgs e)
    {
        var usuario = Membership.GetUser(txtCorreo.Text.Trim());

        //Verifico que el usuario exista en el membership
        if (usuario != null)
        {
            //Verificamos que el usuario aún no esté activo
            if (usuario.IsApproved == false)
            {
                if (usuario.GetPassword().Equals(txtContrasena.Text.Trim()))
                {
                    lbCodigo.Visible = true;
                    lbCodigo.Text = "Su código de seguridad es: " + usuario.Comment;
                }
                else
                {
                    lbCodigo.Visible = true;
                    lbCodigo.Text = "Usuario o contraseña no válido.";
                }
            }
            else
            {
                lbCodigo.Visible = true;
                lbCodigo.Text = "El usuario ya se encuentra activo en el sistema.";
            }
        }
        else
        {
            lbCodigo.Visible = true;
            lbCodigo.Text = "Usuario o contraseña no válido.";
        }

        mdpCodActivacion.Show();
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        lbCodigo.Visible = false;
        txtCorreo.Text = string.Empty;
        txtContrasena.Text = string.Empty;
    }


    /// <summary>
    /// Obtiene versión del aplicativo
    /// </summary>
    private void GetVersion()
    {
        this.LblVersion.Text = " Versión: " + ApplicationInformation.ExecutingAssemblyVersion.ToString();
        this.LblVersion.ToolTip = ApplicationInformation.CompileDate.ToString();
    }
  
    /// <summary>
    /// Regresa valor del número máxico de intentos
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public static string RetornarContador()
    {
        string view = "0";
        if (Convert.ToInt16(HttpContext.Current.Session["Attempts"]) > Convert.ToInt16(HttpContext.Current.Session["MaxInvalidPasswordAttempts"]))
        {
            view = "1";
        }

        return view;
    }

    /// <summary>
    /// Evento para cerrar el slider
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void lbCerrar_Click(object sender, EventArgs e)
    {
        this.mpeMessage.Hide();
    }

    /// <summary>
    /// Evento para abrir el slide
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Cick</param>
    protected void lbAyuda_Click(object sender, EventArgs e)
    {
        this.mpeMessage.Show();
    }
}
