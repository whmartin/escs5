﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultF.aspx.cs" Inherits="DefaultF" %>

<%@ Import namespace="Subkismet"%>
<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript" language="javascript" src="Scripts/pI.js"></script>
    <link type="text/css" href="Styles/log.css" rel="Stylesheet" />
    <script src="Scripts/jq.min.js" type="text/javascript"></script>
    <script src="Scripts/WS.js" type="text/javascript"></script>
    <script src="Scripts/coin-slider.min.js" type="text/javascript"></script>
    <link href="Styles/coin-slider-styles.css" rel="stylesheet" type="text/css" />
    <title>.: ICBF - SIA :.</title>

   
   <script type="text/javascript" >
      var service = new WS("Default.aspx", WSDataType.json);
      $(document).ready(function () {

          $('#coin-slider').coinslider({
              width: 225,
              height: 210
          });


          // APLICA WATERMARK SI EL TEXTBOX NO TIENE VALOR
          $("#loGeneraciones_UserName").each(function () {

              if ($(this).text() == "Nombre De Usuario")
                  $(this).val('').addClass('WaterMarkOn');
              else
                  if ($.trim($(this).val()) == '')
                      $(this).addClass('WaterMarkOn').val("Nombre De Usuario");
          });

          $("#loGeneraciones_Password").each(function () {

              if ($(this).text() == "Contraseña")
                  $(this).val('').addClass('WaterMarkOn');
              else
                  if ($.trim($(this).val()) == '')
                      $(this).addClass('WaterMarkOn').val("Contraseña");
          });

          // AL OBTENER EL FOCO LIMPIA TITLE Y QUITA CLASE DE MARCA
          $("#loGeneraciones_UserName").focus(function () {
              if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
          });

          $("#loGeneraciones_Password").focus(function () {
              if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
          });

          // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
          $("#loGeneraciones_Password").blur(function () {
              if ($(this).val() == '')
                  $(this).addClass('WaterMarkOn').val("Contraseña");
          });

          // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
          $("#loGeneraciones_UserName").blur(function () {
              if ($(this).val() == '')
                  $(this).addClass('WaterMarkOn').val("Nombre De Usuario");
          });

          //          service.call("RetornarContador", {}, true, function (data) {
          //              if (data[0] > data[1]) {
          //                  $("#CaptchaDiv").show();
          //              }
          //          });

          $.ajax({
              type: "POST",
              url: "DefaultF.aspx/RetornarContador",
              data: "{}",
              contentType: "application/json; chartset:utf-8",
              dataType: "json",
              success:
                        function (data) {
                            if (data.d == "1") {
                                $("#CaptchaDiv").show();
                            }
                        },
              async: true,
              cache: false
          });
      });
   </script>


</head>
<body style="font-family: Verdana; font-size: smaller; color: White" onload="SetFocus('loGeneraciones_UserName');">
    <form id="form1" runat="server">
   
    <div class='centrar'>
        <table cellpadding="0" cellspacing="0" class="Loggin"> 
            <tr>
                <td class="Logo">
                    <div class="content">
                        <div id='coin-slider'>
                            <img alt="logo" src="Image/loggin/Img1.jpg" />
                            <img alt="logo" src="Image/loggin/Img2.jpg" />
                            <img alt="logo" src="Image/loggin/Img3.jpg" /> 
                            <img alt="logo" src="Image/loggin/Img4.jpg" />
                            <img alt="logo" src="Image/loggin/Img5.jpg" /> 
                        </div>
                    </div>
                </td>
                <td class="user">
                    <table cellpadding="5" cellspacing="0" class="labels" width="100%">
                        <tr>
                            <td>
                                <img alt="logo" src="Image/loggin/LogoCuentame.jpg" />
                            </td>
                        </tr>                     
                        <tr>
                            <td>
                                <asp:Login ID="loGeneraciones" runat="server" Width="100%" LabelStyle-Width="30%" CssClass="labels input_byclass"
                                    PasswordRecoveryUrl="RecoverPassword.aspx" CreateUserUrl="~/Page/General/RegistroUsuario/Add.aspx"
                                    DisplayRememberMe="False" UserNameLabelText="" LoginButtonText="Iniciar Sesión"  
                                    OnLoggedIn="OnLoggedIn" OnLoginError="OnLoginError" ForeColor="Black" PasswordLabelText=""
                                    Height="120px" TitleText="" FailureText="Datos de usuario invalidos. verifique por favor."
                                    LoginButtonImageUrl="~/Image/btn/apply.png" LoginButtonType="Button" PasswordRequiredErrorMessage="Campo Requerido"
                                    UserNameRequiredErrorMessage="Campo Requerido" InstructionText=" " 
                                    PasswordRecoveryText="Olvidaste tu clave?" UserName="" OnLoggingIn="OnLoggingIn" >
                                    <FailureTextStyle CssClass="error" />
                                    <LoginButtonStyle CssClass="btnLog" />
                                    <TextBoxStyle CssClass="caja" />
                                    <ValidatorTextStyle CssClass="error" />
                                </asp:Login>
                            </td>
                        </tr>
                       <%-- <tr>
                            <td>                                
                                <div id="CaptchaDiv" style="display: none;" >                                 
                                  <sbk:CaptchaControl id="captcha" 
                                    runat="server" 
                                    ErrorMessage=" :: Codigo invalido" 
                                    CaptchaLength="5"                                    
                                    LayoutStyle="Vertical"                                          
                                    />
                                </div>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red" Width="100%"
                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <asp:Label ID="LblVersion" runat="server" Text=""></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="5" cellspacing="0" class="IconosT">
                                    <tr>
                                        <td>
                                            <a href="http://www.dps.gov.co" target="_blank">
                                                <img alt="DPS" src="Image/loggin/DPS.jpg" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.presidencia.gov.co" target="_blank">
                                                <img alt="prosperidad" src="Image/loggin/Prosperidad.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.icbf.gov.co" target="_blank">
                                                <img alt="icbfDigital" src="Image/loggin/IcbfDigital.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.icbf.gov.co" target="_blank">
                                                <img alt="icbf" style="text-decoration: none" src="Image/loggin/LogoIcbf.png" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td  class="FooterL">
                    Instituto Colombiano de Bienestar Familiar
                </td>
                <td  class="FooterR">
                    Todos los Derechos Reservados - &copy; <%: DateTime.Now.Year %> 
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

