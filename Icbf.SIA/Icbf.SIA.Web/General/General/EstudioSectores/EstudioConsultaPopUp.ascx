﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EstudioConsultaPopUp.ascx.cs" Inherits="General_General_EstudioSectores_EstudioConsultaPopUp" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    
    <ContentTemplate>
        <asp:Label runat="server" Text="" ID="lblError" ForeColor="Red"></asp:Label>
        <br />
        <asp:ImageButton ID="btnBuscarPopUp" runat="server" ImageUrl="~/Image/btn/list.png" OnClick="btnBuscarPopUp_Click" Style="position: absolute; top: 5%; right: 0; left: 90%" />
        <br />
        <asp:Panel runat="server" ID="Panel1">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>Consecutivo Estudio
                    </td>
                    <td>Nombre abreviado 
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtConsecutivoEstudioPopUp" MaxLength="5" Width="90%"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftConsecutivoEstudio" runat="server" TargetControlID="txtConsecutivoEstudioPopUp"
                            FilterType="Numbers" ValidChars="" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" Width="90%"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftNombreAbreviado" runat="server" TargetControlID="txtNombreAbreviado"
                            FilterType="Custom,Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Objeto 
                    </td>
                    <td>Responsable DT o ES 
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtObjeto" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" Width="90%"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftObjeto" runat="server" TargetControlID="txtObjeto"
                            FilterType="Custom,Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdResponsableES" Width="90%"></asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Responsable EC 
                    </td>
                    <td>Modalidad de contratación
                    </td>
                </tr>
                <tr class="rowA">
                    <td>

                        <asp:DropDownList runat="server" ID="ddlIdResponsableEC" Width="90%"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Width="90%"></asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Año Fecha de solicitud inicial 
                    </td>
                    <td>Dependencia solicitante
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdFechaSolicitudInicial" Width="90%"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Width="90%"></asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">Estado
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" Width="90%"></asp:DropDownList>
                    </td>
                    <td></td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlListar">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvRegistroSolicitudEstudioSectoryCaso" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                            GridLines="None" Width="100%" DataKeyNames="IdRegistroSolicitudEstudioSectoryCaso" CellPadding="0" Height="16px"
                            OnPageIndexChanging="gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging" OnSorting="gvRegistroSolicitudEstudioSectoryCaso_Sorting" OnRowCommand="gvRegistroSolicitudEstudioSectoryCaso_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Acción">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" CommandArgument='<%#Eval("IdRegistroSolicitudEstudioSectoryCaso") %>' ImageUrl="~/Image/btn/apply.png"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Consecutivo  Estudio" DataField="IdRegistroSolicitudEstudioSectoryCaso" SortExpression="IdRegistroSolicitudEstudioSectoryCaso" />
                                <asp:BoundField HeaderText="Nombre abreviado" DataField="NombreAbreviado" SortExpression="NombreAbreviado" />
                                <asp:BoundField HeaderText="Número de reproceso" DataField="NumeroReproceso" SortExpression="NumeroReproceso" />
                                <asp:TemplateField HeaderText="Consecutivo estudio relacionado" SortExpression="ConsecutivoEstudioRelacionado">
                                    <ItemTemplate>
                                        <%# Eval("ConsecutivoEstudioRelacionado").ToString()=="0"? string .Empty:Eval("ConsecutivoEstudioRelacionado").ToString() %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha de solicitud inicial" SortExpression="FechaSolicitudInicial">
                                    <ItemTemplate>                                       
                                        <%#Eval("FechaSolicitudInicial", "{dd/MM/yyyy}")%>                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderText="Fecha de solicitud inicial" DataField="FechaSolicitudInicial"  SortExpression="FechaSolicitudInicial" />--%>
                                <asp:BoundField HeaderText="Acta, correo o Nro. Radicado" DataField="ActaCorreoNoRadicado" SortExpression="ActaCorreoNoRadicado" />
                                <asp:BoundField HeaderText="Estado" DataField="EstadoSolicitud" SortExpression="EstadoSolicitud" />
                                <asp:BoundField HeaderText="Modalidad de contratación" DataField="ModalidadSeleccion" SortExpression="ModalidadSeleccion" />
                                <asp:BoundField HeaderText="Dependencia solicitante" DataField="DireccionSolicitante" SortExpression="DireccionSolicitante" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <asp:HiddenField ID="hfNombreSolicitanteES" runat="server" />
                <asp:HiddenField ID="hfNombreSolicitanteEC" runat="server" />
            </table>
        </asp:Panel>
    </ContentTemplate>
    
</asp:UpdatePanel>
<asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; width: 100%; top: -127px; right: 0; left: 0; bottom: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Image/main/loading.gif" AlternateText="Loading ..." ToolTip="Cargando ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>

