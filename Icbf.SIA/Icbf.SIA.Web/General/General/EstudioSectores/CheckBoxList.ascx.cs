﻿using Icbf.EstudioSectorCosto.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_General_EstudioSectores_CheckBoxList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (chkList.Items.Count > 0)
        {
            chkList.Items[0].Attributes.Add("onClick", "allSelected('"+chkList.ClientID+"')");
            foreach (ListItem vItem in chkList.Items)
            {
                if (vItem.Selected)
                {
                    txtCombo.Items[0].Text = hidVal.Value;
                    break;
                }
            }
            
        }
        if (!IsPostBack)
        {
            txtCombo.Attributes.Add("onClick", "ddlPanel();");
        }


    }
    /// <summary>
    /// Set the Width of the CheckBoxList
    /// </summary>
    public int WidthCheckListBox
    {
        set
        {
            chkList.Width = value;
            Panel111.Width = value + 20;
        }
    }

    public ListItemCollection items()
    {
        ListItemCollection laLista = chkList.Items;
        return laLista;
    }

    public string GetSeleccion()
    {
        string pSelectedValues = string.Empty;
        foreach (ListItem item in chkList.Items)
        {
            if (item.Selected)
            {
                pSelectedValues += item.Value + ",";
            }
        }
        if (!pSelectedValues.Equals(string.Empty))
        {
            pSelectedValues = pSelectedValues.Substring(0, pSelectedValues.Length - 1);
        }

        return pSelectedValues;
    }

    /// <summary>
    /// Set the Width of the Combo
    /// </summary>
    public int Width
    {
        set { txtCombo.Width = value; }
        get { return (Int32)txtCombo.Width.Value; }
    }
    public bool Enabled
    {
        set { txtCombo.Enabled = value; }
    }
    /// <summary>
    /// Set the CheckBoxList font Size
    /// </summary>
    public FontUnit fontSizeCheckBoxList
    {
        set { chkList.Font.Size = value; }
        get { return chkList.Font.Size; }
    }
    /// <summary>
    /// Set the ComboBox font Size
    /// </summary>
    public FontUnit fontSizeTextBox
    {
        set { txtCombo.Font.Size = value; }
    }

    /// <summary>
    /// Add Items to the CheckBoxList
    /// </summary>
    /// <param name="dr"></param>
    /// <param name="nombreCampoTexto">Field Name of the OdbcDataReader to Show in the CheckBoxList</param>
    /// <param name="nombreCampoValor">Value Field of the OdbcDataReader to be added to each Field Name (it can be the same string of the textField)</param>
    public void AddItems(List<BaseDTO> dr)
    {
        ClearAll();
        int i = 0;
        foreach (var a in dr)
        {
            chkList.Items.Add(a.Name.ToString());
            chkList.Items[i].Value = a.Id.ToString();
            i++;
        }
        chkList.Items.Insert(0, new ListItem(Constantes.Todos, Constantes.ValorDefecto));
        chkList.Items[0].Attributes.Add("onClick", "allSelected('" + chkList.ClientID + "')");
    }


    /// <summary>
    /// Uncheck of the Items of the CheckBox
    /// </summary>
    public void unselectAllItems()
    {
        for (int i = 0; i < chkList.Items.Count; i++)
        {
            chkList.Items[i].Selected = false;
        }
    }

    /// <summary>
    /// Delete all the Items of the CheckBox;
    /// </summary>
    public void ClearAll()
    {
        //txtCombo.Text = "";
        chkList.Items.Clear();
    }


    public void SetFocus()
    {
        txtCombo.Focus();

    }


}