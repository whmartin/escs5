﻿using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Entity;

public partial class General_General_EstudioSectores_FormularioConsulta : System.Web.UI.UserControl
{
    
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();
    public long? vIdConsecutivoEstudio { get; set; }
    public string vNombreAbreviado { get; set; }
    public string vObjeto { get; set; }
    public int? vIdResponsableES { get; set; }
    public int? vIdResponsableEC { get; set; }
    public int? vIdModalidadDeSeleccion { get; set; }
    public int? vDireccionSolicitante { get; set; }
    public int? vEstado { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }

    }
    public void CargarDatosIniciales()
    {
        try
        {
            
            Utilidades.LlenarDropDownList(ddlResponsableES, vResultadoEstudioSectorService.ConsultarResponsable());
            Utilidades.LlenarDropDownList(ddlResponsableEC, vResultadoEstudioSectorService.ConsultarResponsable());
            Utilidades.LlenarDropDownList(ddlModalidadDeSeleccion, vResultadoEstudioSectorService.ConsultarModalidadSeleccion());
            Utilidades.LlenarDropDownList(ddlDireccionSolicitante, vResultadoEstudioSectorService.ConsultarDireccion());
            Utilidades.LlenarDropDownList(ddlEstado, vResultadoEstudioSectorService.ConsultarEstado());
            txtConsecutivoEstudio.Focus();

        }
        catch (UserInterfaceException ex)
        {
            lblMsgError.Text = ex.Message;
            lblMsgError.Visible = true;
        }
        catch (Exception ex)
        {
            lblMsgError.Text = ex.Message;
            lblMsgError.Visible = true;
        }
    }

    public void GetValues()
    {
        vIdConsecutivoEstudio = txtConsecutivoEstudio.Text.Equals(string.Empty) ? (long?)null : long.Parse(txtConsecutivoEstudio.Text);
        vNombreAbreviado = txtNombreAbreviado.Text.Trim().Equals(string.Empty) ? string.Empty : txtNombreAbreviado.Text;
        vObjeto = txtObjeto.Text.Trim().Equals(string.Empty) ? string.Empty : txtObjeto.Text;
        vIdResponsableES = ddlResponsableES.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlResponsableES.Text);
        vIdResponsableEC = ddlResponsableEC.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlResponsableEC.Text);
        vIdModalidadDeSeleccion = ddlModalidadDeSeleccion.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlModalidadDeSeleccion.Text);
        vDireccionSolicitante = ddlDireccionSolicitante.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlDireccionSolicitante.Text);
        vEstado = ddlEstado.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlEstado.Text);
    }
}

