﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckBoxList.ascx.cs" Inherits="General_General_EstudioSectores_CheckBoxList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc4" %>
<script type="text/javascript">
    //Script para incluir en el ComboBox1 cada item chekeado del chkListMateriales
    function CheckItem(checkBoxList) {
        var options = checkBoxList.getElementsByTagName('input');
        var arrayOfCheckBoxLabels = checkBoxList.getElementsByTagName("label");
        var s = "";
        var seleccionados = 0;
        for (i = 0; i < options.length; i++) {
            var opt = options[i];
            if (opt.checked) {
                s = s + ", " + arrayOfCheckBoxLabels[i].innerText;
                seleccionados++;
            }
            else if (opt.checked) {
                seleccionados -= 1;
            }
        }
        if (s.length > 0) {
            s = s.substring(2, s.length); //sacar la primer 'coma'
        }

        if (seleccionados > 1) {
            s = "(" + seleccionados + ") SELECCIONADOS"
        }
        if (seleccionados == 0) {
            s = "SELECCIONE";
        }

        var TxtBox = document.getElementById("<%=txtCombo.ClientID%>");
        TxtBox.children[0].innerText = s;
        document.getElementById('<%=hidVal.ClientID %>').value = s;
    }

    function allSelected(cbControl) {
        var control = document.getElementById(cbControl);
        var chkBoxCount = control.getElementsByTagName('input');
        if (chkBoxCount[0].checked) {
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = true;
            }
        }
        else {
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = false;
            }
        }
        return false;
    }

</script>
<script type="text/javascript">
    function ddlPanel() {
        document.getElementById("<%=div_panel.ClientID%>").style.display = 'block';
        var width =  $("#<%=txtCombo.ClientID%>").width()
        $("#<%=Panel111.ClientID%>").css({ top: 0, left: 0, position: 'relative' });
        $("#<%=Panel111.ClientID%>").width(width)
    }
    $(document).ready(function () {

        var lista = "#<%=txtCombo.ClientID%>";
        if ($(lista).is(':focus')) {
            $("#<%=div_panel.ClientID%>").hide();
        }
       
    });



</script>

<asp:DropDownList runat="server" ID="txtCombo" Width="250px">
    <asp:ListItem Selected="True" Value="-1" Text="SELECCIONE"></asp:ListItem>
</asp:DropDownList>
<cc4:PopupControlExtender ID="PopupControlExtender111" runat="server"
    TargetControlID="txtCombo" PopupControlID="Panel111" Position="Bottom" >
</cc4:PopupControlExtender>
<div id="div_panel" style="visibility: hidden" runat="server">
    <asp:Panel ID="Panel111" runat="server" ScrollBars="Vertical" Style="width: auto; visibility: hidden" Height="150" BackColor="White" BorderColor="Gray" BorderWidth="1" >

        <asp:CheckBoxList ID="chkList" TextAlign="Right" Style="position: relative; top: -4px;"
            runat="server"
            Height="150" onclick="CheckItem(this)">
        </asp:CheckBoxList>

    </asp:Panel>
</div>


<input type="hidden" name="hidVal" id="hidVal" runat="server" />

