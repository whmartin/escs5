﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarioFestivos.ascx.cs" Inherits="General_General_EstudioSectores_CalendarioFestivos" %>

<asp:TextBox ID="txtFecha" runat="server" CssClass="cF" onkeydown="<%# mkttxtFecha.ClientID %>" MaxLength="10"></asp:TextBox>
<asp:Image ID="imgCalendario" runat="server" name="btnCalendar" ImageUrl="~/Image/btn/Calendar.gif"
    Style="cursor: pointer" />
<asp:Label ID="cvFecha" runat="server" Text="" ForeColor="Red" Visible="true"></asp:Label>
<asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Fecha inválida" ControlToValidate="txtFecha" ForeColor="Red" MinimumValue="1753-01-01" MaximumValue="9999-12-31" Type="Date" ValidationGroup="btnGuardar"></asp:RangeValidator>
<Ajax:MaskedEditExtender ID="mkttxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha" CultureName="es-CO"></Ajax:MaskedEditExtender>

<Ajax:CalendarExtender ID="certxtFecha" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalendario" TargetControlID="txtFecha" Animated="True" ClearTime="True" />


<script type="text/javascript">
    function KeyDownHandler<%=ClientID%>(event) {

        var key = event.keyCode;
        var idTextBox = '<%=this.ClientID+"_"+txtFecha.ID%>';
        var textBox = document.getElementById(idTextBox);
        var text = document.getElementById(idTextBox).value;

        if (textBox.value.length === 0) {
            textBox.value = '';
        }

        if (textBox.value.length > 10) {
            textBox.value = '';
        }

        if (textBox.value.length == 2) {
            textBox.value = textBox.value + '/';
        }

        if (textBox.value.length == 5) {
            textBox.value = textBox.value + '/';
        }

        if (textBox.value === '__/__/____') {
            textBox.value = '';
        }


        if (event.keyCode == 8 || event.keyCode == 46) {


            var txtElement = $get(event.srcElement.id);

            var txtElementText = GetTextElementValue(event.srcElement.id);

            var txtElementCursorPosition = doGetCaretPosition(txtElement);

            var maskExtender = $find('<%=this.ClientID+"_"+mkttxtFecha.ID%>');


            var start = txtElement.selectionStart;

            var end = txtElement.selectionEnd;

            var selectedSymbols = end - start;



            if (event.keyCode == 8) //BackSpace
            {

                if (selectedSymbols > 0) {//if there is selection(more then 1 symbol)


                    var str1 = txtElementText.substr(0, start);

                    var str2 = txtElementText.substr(end);

                    var str = str1 + str2;

                    if (str.length < txtElementText.length) str = appendStrWithChar(str, txtElementText, "_");

                    SetTextElementValue(event.srcElement.id, str);

                    //txtElement.value = str;

                    maskExtender._LogicTextMask = deletePromptChars(str, "_");

                    setCaretPosition(txtElement, start);

                }

                else {

                    if ((txtElementCursorPosition - 1) >= 0) {

                        var symbol_to_delete = txtElementText[txtElementCursorPosition - 1];

                        if (symbol_to_delete == "_") {

                            setCaretPosition(txtElement, txtElementCursorPosition - 1);

                        }

                        else {

                            var str1 = txtElementText.substr(0, txtElementCursorPosition - 1);

                            var str2 = txtElementText.substr(txtElementCursorPosition);

                            var str = str1 + str2;

                            if (str.length < txtElementText.length) str = appendStrWithChar(str, txtElementText, "_");

                            SetTextElementValue(event.srcElement.id, str);

                            //txtElement.value = str;

                            maskExtender._LogicTextMask = deletePromptChars(str, "_");

                            setCaretPosition(txtElement, txtElementCursorPosition - 1);

                            //var real_text = deletePromptChars(str, "_");

                        }

                    }

                }



            }

            if (event.keyCode == 46) //Delete
            {

                if (txtElementCursorPosition >= 0 && txtElementCursorPosition < txtElementText.length

                        && ((selectedSymbols <= 1 && txtElementText[txtElementCursorPosition] != "_") || selectedSymbols > 1)) {


                    if (selectedSymbols > 1) {//if there is selection(more then 1 symbol)

                        var str1 = txtElementText.substr(0, start);

                        var str2 = txtElementText.substr(end);

                        var str = str1 + str2;

                        if (str.length < txtElementText.length) str = appendStrWithChar(str, txtElementText, "_");

                        SetTextElementValue(event.srcElement.id, str);

                        //txtElement.value = str;

                        maskExtender._LogicTextMask = deletePromptChars(str, "_");

                        setCaretPosition(txtElement, start);

                    }

                    else {//no selection or 1 symbol selected

                        var symbol_to_delete = txtElementText[txtElementCursorPosition];


                        if (symbol_to_delete != "_") {

                            var str1 = txtElementText.substr(0, txtElementCursorPosition);

                            var str2 = txtElementText.substr(txtElementCursorPosition + 1);

                            var str = str1 + str2;

                            if (str.length < txtElementText.length) str = appendStrWithChar(str, txtElementText, "_");

                            SetTextElementValue(event.srcElement.id, str);

                            //txtElement.value = str;

                            maskExtender._LogicTextMask = deletePromptChars(str, "_");

                            setCaretPosition(txtElement, txtElementCursorPosition);

                        }

                    }

                }
            }
        }
    }

    function GetTextElementValue(elementId) {

        var textBox = $get(elementId), text;

        if (textBox.AjaxControlToolkitTextBoxWrapper) {

            text = textBox.AjaxControlToolkitTextBoxWrapper.get_Value();

        }

        else {

            text = textBox.value;

        }


        return text;

    }


    function SetTextElementValue(elementId, someText) {

        var textBox = $get(elementId);

        if (textBox.AjaxControlToolkitTextBoxWrapper) {

            textBox.AjaxControlToolkitTextBoxWrapper.set_Value(someText);

        }

        else {

            textBox.value = someText;

        }

    }


    function appendStrWithChar(str, templateStr, appChar) {

        var newStr = str;

        var difference = templateStr.length - newStr.length;


        if (difference > 0) {

            for (i = 0; i < difference; i++) { newStr = newStr + "_"; }

        }

        return newStr;

    }


    function deletePromptChars(str, promptChar) {

        var newStr = str;

        for (i = 0; i < newStr.length; i++) {

            if (str[i] == promptChar) {

                newStr = newStr.substr(0, i);

                return newStr;

            }

        }

    }


    function doGetCaretPosition(ctrl) {

        var CaretPos = 0; // IE Support

        if (document.selection) {

            ctrl.focus();

            var Sel = document.selection.createRange();

            Sel.moveStart('character', -ctrl.value.length);

            CaretPos = Sel.text.length;

        }

            // Firefox support

        else if (ctrl.selectionStart || ctrl.selectionStart == '0')

            CaretPos = ctrl.selectionStart;

        return (CaretPos);

    }


    function setCaretPosition(ctrl, pos) {

        if (ctrl.setSelectionRange) {

            ctrl.focus();

            ctrl.setSelectionRange(pos, pos);

        }

        else if (ctrl.createTextRange) {

            var range = ctrl.createTextRange();

            range.collapse(true);

            range.moveEnd('character', pos);

            range.moveStart('character', pos);

            range.select();

        }

    }
</script>


<script type="text/javascript">

    function MostrarCalendario<%=ClientID%>() {

    }

    function SeleccionFecha<%=this.ClientID %>(sender, args) {
        var selectedDate = sender.get_selectedDate();
        var idValidator = '<%=this.ClientID+"_"+cvFecha.ID%>';
        var idTextBox = '<%=this.ClientID+"_"+txtFecha.ID%>';
        document.getElementById(idValidator).innerHTML = "";
        //ValidarFEstivos(selectedDate, idValidator, idTextBox);
    }

    function Clear<%=ClientID%>() {
        var vTextBoxId = '<%=this.ClientID+"_"+txtFecha.ID%>';
        var vTxt = document.getElementById(vTextBoxId);

        if (vTxt.value === "__________" || vTxt.value==="__/__/____" || vTxt.value === "_/________" ){
            vTxt.value = "";
        }
       
    }
</script>
