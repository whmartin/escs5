﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_General_EstudioSectores_CalendarioFestivos : System.Web.UI.UserControl
{
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();
    public DateTime? Date { get; set; }
    public string CompareTo { get; set; }

    public string OperatorCompare { get; set; }

    public bool EnabledCompare { get; set; }
    public List<BaseDTO> LstFestivos { get; set; }
    protected void Page_Init(object sender, EventArgs e)
    {        
        System.Web.UI.ScriptManager.GetCurrent(this.Page).EnableScriptGlobalization = true;
        System.Web.UI.ScriptManager.GetCurrent(this.Page).EnableScriptLocalization = true;        
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-CO");
        //Thread.CurrentThread.CurrentCulture = new CultureInfo("es-CO");
        if (!Page.IsPostBack)
        {
            //certxtFecha.OnClientShown = "MostrarCalendario" + ClientID;
            certxtFecha.OnClientDateSelectionChanged = "SeleccionFecha" + ClientID;
            txtFecha.Attributes.Add("onkeydown", "KeyDownHandler"+ this.ClientID + "(event)");
            txtFecha.Attributes.Add("onfocusout", "Clear" + this.ClientID + "()");
        }
    }

    public void ponerFocus()
    {
        txtFecha.Focus();
    }

    public DateTime? GetDate()
    {
        if (txtFecha.Text.Equals(string.Empty))
        {
            Date = (DateTime?)null;
        }
        else
        {
            Date = Convert.ToDateTime(txtFecha.Text);
        }
        return Date;
    }

    public void SetDate(DateTime? date)
    {

        if (date == null)
        {
            txtFecha.Text = string.Empty;
        }
        else
        {
            txtFecha.Text = date.Value.ToString("dd/MM/yyyy");
        }
    }
    public void Enabled(bool pIsEnabled)
    {
        txtFecha.Enabled = pIsEnabled;        
        certxtFecha.Enabled = pIsEnabled;
        //imgCalendario.Visible = pIsEnabled;

    }

    public bool EsDiaHabil(DateTime pLaFecha)
    {
        pLaFecha = pLaFecha.Date;
        bool vEsDiaHabil = true;
        DayOfWeek vElDia = pLaFecha.DayOfWeek;

        if (vElDia == DayOfWeek.Saturday || vElDia == DayOfWeek.Sunday)
        {
            vEsDiaHabil = false;
        }
        else
        {
            List<BaseDTO> vLstDiasFestivos = vResultadoEstudioSectorService.ConsultarDiasFestivos(pLaFecha.Year);
            vEsDiaHabil=!vLstDiasFestivos.Any(x => x.Id == pLaFecha.ToString("dd/MM/yyyy"));
                        
        }
        return vEsDiaHabil;
    }

    public DateTime GetDiaHabil(DateTime pFecha)
    {
        List<BaseDTO> vLstDiasFestivos = vResultadoEstudioSectorService.ConsultarDiasFestivos(pFecha.Year);
        return GetProximoDiaHabil(pFecha, vLstDiasFestivos);
    }
    public DateTime GetProximoDiaHabil(DateTime pFecha, List<BaseDTO> vLstDiasFestivos)
    {
        bool vEsDiaHabil = false;                   
        while (!vEsDiaHabil)
        {
            DayOfWeek vDia = pFecha.DayOfWeek;
            if (vDia == DayOfWeek.Saturday || vDia == DayOfWeek.Sunday)
            {
                vEsDiaHabil = false;
            }
            else
            {
                vEsDiaHabil = true;
            }

            ////Si es día hábil se verifica si es festivo
            if (vEsDiaHabil)
            {
                vEsDiaHabil = !vLstDiasFestivos.Any(x => x.Id == pFecha.ToString("dd/MM/yyyy"));
            }
            
            
            if (!vEsDiaHabil)
            {
                pFecha = pFecha.AddDays(1);
                pFecha=GetProximoDiaHabil(pFecha, vLstDiasFestivos);
            }
        }
        return pFecha;
    }

    public bool EsDiaFestivo()
    {
        bool vEsFestivo = true;
        if (txtFecha.Text.Equals(string.Empty))
        {
            vEsFestivo = false;
        }
        else
        {
            DateTime fecha = Convert.ToDateTime(txtFecha.Text);
            List<BaseDTO> vLstDiasFestivos = vResultadoEstudioSectorService.ConsultarDiasFestivos(fecha.Year);
            vEsFestivo = vLstDiasFestivos.Any(x => x.Id == fecha.ToString("dd/MM/yyyy"));
            DayOfWeek vDia = fecha.DayOfWeek;
            if ( vDia == DayOfWeek.Sunday)
            {
                vEsFestivo = true;
            }
        }
        
        if (vEsFestivo)
        {
            certxtFecha.SelectedDate = DateTime.Now;
            cvFecha.Text = "Día festivo, fecha inválida";
        }
        else if(cvFecha.Text!=null||cvFecha.Text!="")
        {
            cvFecha.Text = string.Empty;
        }
        return vEsFestivo;
    }

    public void SetReadOnly(bool pEsReadOnly)
    {
        txtFecha.ReadOnly = pEsReadOnly;
        certxtFecha.Enabled = false;
    }
}