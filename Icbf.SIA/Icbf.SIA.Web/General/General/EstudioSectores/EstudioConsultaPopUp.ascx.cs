﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_General_EstudioSectores_EstudioConsultaPopUp : System.Web.UI.UserControl
{
    ManejoControlesContratos vManejoControlesContratos;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    public event GridViewCommandEventHandler GetConsecutivoEstudio;
    public long IdConsecutivoEstudioConsulta { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }

    }

   

    private void CargarDatosIniciales()
    {
        try
        {
            ////Datos Pop up
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableES, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableEC, null, true);
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarFechaSolicitudInicial(ddlIdFechaSolicitudInicial, null, true);
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitante, null, true);
            vManejoControlesContratos.LlenarEstadoEstudio(ddlIdEstadoSolicitud, null, true);

        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
        }
    }

    #region POP UP
    private List<RegistroSolicitudEstudioSectoryCaso> Buscar()
    {
        try
        {
            Int32? vConsecutivoEstudio = null;
            int? vAplicaPACCO = null;
            int? vConsecutivoPACCO = null;
            int? vDireccionsolicitantePACCO = null;
            String vObjetoPACCO = null;
            int? vModalidadPACCO = null;
            Decimal? vValorPresupuestalPACCO = null;
            String vVigenciaPACCO = null;
            int? vConsecutivoEstudioRelacionado = null;
            DateTime vFechaSolicitudInicial = Convert.ToDateTime("0009-01-01");            
            String vActaCorreoNoRadicado = null;
            String vNombreAbreviado = null;
            String vNumeroReproceso = null;
            String vObjeto = null;
            String vCuentaVigenciasFuturasPACCO = null;
            String vAplicaProcesoSeleccion = null;
            int? vIdModalidadSeleccion = null;
            int? vIdTipoEstudio = null;
            int? vIdComplejidadInterna = null;
            int? vIdComplejidadIndicador = null;
            int? vIdResponsableES = null;
            int? vIdResponsableEC = null;
            String vOrdenadorGasto = null;
            int? vIdEstadoSolicitud = null;
            int? vIdMotivoSolicitud = null;
            int? vIdDireccionSolicitante = null;
            int? vIdAreaSolicitante = null;
            String vTipoValor = null;
            int? vValorPresupuestoEstimadoSolicitante = null;
            if (txtConsecutivoEstudioPopUp.Text != "")
            {
                vConsecutivoEstudio = Convert.ToInt32(txtConsecutivoEstudioPopUp.Text);
            }
            if (ddlIdFechaSolicitudInicial.SelectedValue != "-1")
            {
                vFechaSolicitudInicial = Convert.ToDateTime(ddlIdFechaSolicitudInicial.SelectedValue + "-01-01");
            }
            if (txtNombreAbreviado.Text != "")
            {
                vNombreAbreviado = Convert.ToString(txtNombreAbreviado.Text);
            }
            if (txtObjeto.Text != "")
            {
                vObjeto = Convert.ToString(txtObjeto.Text);
            }
            if (ddlIdModalidadSeleccion.SelectedValue != "-1")
            {
                vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            }
            if (ddlIdResponsableES.SelectedValue != "-1")
            {
                vIdResponsableES = Convert.ToInt32(ddlIdResponsableES.SelectedValue);
            }
            if (ddlIdResponsableEC.SelectedValue != "-1")
            {
                vIdResponsableEC = Convert.ToInt32(ddlIdResponsableEC.SelectedValue);
            }
            if (ddlIdEstadoSolicitud.SelectedValue != "-1")
            {
                vIdEstadoSolicitud = Convert.ToInt32(ddlIdEstadoSolicitud.SelectedValue);
            }
            if (ddlIdDireccionSolicitante.SelectedValue != "-1")
            {
                vIdDireccionSolicitante = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);
            }
            List<RegistroSolicitudEstudioSectoryCaso> ListaRegistroSolicitudEstudioSectoryCaso = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCasos(vConsecutivoEstudio, vAplicaPACCO, vConsecutivoPACCO, vDireccionsolicitantePACCO, vObjetoPACCO, vModalidadPACCO, vValorPresupuestalPACCO, vVigenciaPACCO, vConsecutivoEstudioRelacionado, vFechaSolicitudInicial, vActaCorreoNoRadicado, vNombreAbreviado, vNumeroReproceso, vObjeto, vCuentaVigenciasFuturasPACCO, vAplicaProcesoSeleccion, vIdModalidadSeleccion, vIdTipoEstudio, vIdComplejidadInterna, vIdComplejidadIndicador, vIdResponsableES, vIdResponsableEC, vOrdenadorGasto, vIdEstadoSolicitud, vIdMotivoSolicitud, vIdDireccionSolicitante, vIdAreaSolicitante, vTipoValor, vValorPresupuestoEstimadoSolicitante);
            if (ListaRegistroSolicitudEstudioSectoryCaso.Count == 0)
            {
                lblError.Text = "No se encontraron resultados, verifique por favor";
            }
            return ListaRegistroSolicitudEstudioSectoryCaso;
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
            return null;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
            return null;
        }
    }

    protected List<RegistroSolicitudEstudioSectoryCaso> LlenarGrilla()
    {
        lblError.Text = string.Empty;

        List<RegistroSolicitudEstudioSectoryCaso> listaSolicitudes = Buscar();
        gvRegistroSolicitudEstudioSectoryCaso.DataSource = listaSolicitudes;
        gvRegistroSolicitudEstudioSectoryCaso.DataBind();
        gvRegistroSolicitudEstudioSectoryCaso.Focus();
        foreach (GridViewRow r in gvRegistroSolicitudEstudioSectoryCaso.Rows)
        {
            ImageButton btn = r.FindControl("btnInfo") as ImageButton;            
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(btn);
        }
        return listaSolicitudes;
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }


    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCasos = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (vListaRegistroSolicitudEstudioSectoryCasos != null)
                {
                    var param = Expression.Parameter(typeof(RegistroSolicitudEstudioSectoryCaso), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<RegistroSolicitudEstudioSectoryCaso, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos;
            }

            gridViewsender.DataBind();
            foreach (GridViewRow r in gvRegistroSolicitudEstudioSectoryCaso.Rows)
            {

                ImageButton btn = r.FindControl("btnInfo") as ImageButton;
                ScriptManager.GetCurrent(Page).RegisterPostBackControl(btn);
            }
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
        }
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRegistroSolicitudEstudioSectoryCaso.DataKeys[rowIndex].Value.ToString();
           
        }
        catch (UserInterfaceException ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message; lblError.Visible = true;
        }
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRegistroSolicitudEstudioSectoryCaso.SelectedRow);
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroSolicitudEstudioSectoryCaso.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }


    protected void btnBuscarPopUp_Click(object sender, ImageClickEventArgs e)
    {
        LlenarGrilla();
        //mpeConsulta.Show();
        //pnlConsulta.Visible = true;
    }

    #endregion

    public void gvRegistroSolicitudEstudioSectoryCaso_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Select"))
        {
            IdConsecutivoEstudioConsulta = long.Parse(e.CommandArgument.ToString());
            gvRegistroSolicitudEstudioSectoryCaso.DataSource = null;
            gvRegistroSolicitudEstudioSectoryCaso.DataBind();
            this.Page.GetType().InvokeMember("Retornar", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, new object[] { IdConsecutivoEstudioConsulta });
        }
        
       
    }
    public long GetconsecutivoEstudio()
    {
        return IdConsecutivoEstudioConsulta;

    }
}