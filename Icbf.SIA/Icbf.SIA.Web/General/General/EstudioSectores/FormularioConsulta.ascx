﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormularioConsulta.ascx.cs" Inherits="General_General_EstudioSectores_FormularioConsulta" %>
<script type="text/javascript" language="javascript">
    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        }
    }
</script>
<style>
    .txtNombreAbreviadoClass
    {
        width:80%;
    }
</style>
<asp:Panel runat="server" ID="pnlConsulta">
    <asp:Label ID="lblMsgError" runat="server" Text="Label" Visible="false"></asp:Label>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Consecutivo  Estudio
            </td>
            <td>Nombre Abreviado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" class="txtNombreAbreviadoClass"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteNombreAbreviado" TargetControlID="txtNombreAbreviado" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr class="rowB">
            <td>Objeto
            </td>
            <td>Responsable DT o ES
            </td>
        </tr>
        <tr class="rowA">
            <td rowspan="3">
                <asp:TextBox runat="server" ID="txtObjeto" TextMode="MultiLine" Rows="4" Columns="30" onChange="limitText(this,400);" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteObjeto" TargetControlID="txtObjeto" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
            </td>
            <td rowspan="1">
                <asp:DropDownList runat="server" ID="ddlResponsableES"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td>Responsable EC
            </td>
            <td>Modalidad de contratación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlResponsableEC"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidadDeSeleccion"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Dependencia solicitante
            </td>
            <td>Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlDireccionSolicitante"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlEstado"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Panel>
