﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.IO;

/// <summary>
/// Págna genérica para la descarga de archivos del FTP
/// </summary>
public partial class General_General_MostrarArchivo_MostrarArchivo : GeneralWeb
{
    /// <summary>
    /// Manejdaor del evento cargar la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Iniciar();
        }
    }

    /// <summary>
    /// Descarga archico especificado
    /// </summary>
    private void Iniciar()
    {

        if (GetSessionParameter("Archivo") != null)
        {
            var Archivo = (Byte[])GetSessionParameter("Archivo");
            var NombreArchivo = (String)GetSessionParameter("NombreArchivo");
            RemoveSessionParameter("Archivo");
            RemoveSessionParameter("NombreArchivo");
            var stream = new MemoryStream(Archivo);
            
            this.Response.Clear();
            //this.Response.ContentType = "application/force-download";
            this.Response.AddHeader("Content-Disposition", "inline; filename=" + NombreArchivo);
            this.Response.BinaryWrite(stream.ToArray());
            this.Response.End();
        }


    }
}