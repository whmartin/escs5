﻿//-----------------------------------------------------------------------
// <copyright file="fecha.ascx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase fecha.</summary>
// <author>ICBF</author>
// <date>18/02/2016 0355 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



/// <summary>
/// Class partial  and description for fecha
/// </summary>
/// <summary>
/// Declaración ValidationProperty
/// </summary>
[ValidationProperty("Text")]
public partial class comun_control_fecha_festivos : System.Web.UI.UserControl
{
    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param> 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DateChange != null)
        {
            txtFecha.TextChanged += new EventHandler(this.DateChange);
        }
    }

    /// <summary>
    /// Representa el evento DateChange
    /// </summary>
    public event EventHandler DateChange;

    /// <summary>
    /// Representa _date
    /// </summary>
    private DateTime _date;

    /// <summary>
    /// Representa _month
    /// </summary>
    private int _month;

    /// <summary>
    /// Representa _day
    /// </summary>
    private int _day;

    /// <summary>
    /// Representa _year
    /// </summary>
    private int _year;

    public short TabIndex
    {
        get
        {
            return txtFecha.TabIndex;
        }
        set
        {
            this.txtFecha.TabIndex = value;
        }
    }

    /// <summary>
    /// Método para Inicializar
    /// </summary>
    /// <param name="pEtiqueta">Etiqueta a filtrar</param>
    /// <param name="pRequerido">Requerido a filtrar</param>
    public void Inicializar(string pEtiqueta, bool pRequerido)
    {
        rfvFecha.Enabled = pRequerido;
    }

    /// <summary>
    /// Método Get or Set the Date
    /// </summary>
    /// <value>The Date</value>
    public DateTime Date
    {
        get
        {
            if (txtFecha.Text.Trim() != string.Empty)
            {
                DateTime vFecha = Convert.ToDateTime("01/01/1900");
                DateTime.TryParse(txtFecha.Text.Trim(), out vFecha);
                _day = Convert.ToInt16(vFecha.Day);
                _month = Convert.ToInt16(vFecha.Month);

                if (Convert.ToInt16(vFecha.Year) > 1800)
                    _year = Convert.ToInt16(vFecha.Year);
                else
                    _year = 1900;

                _date = new DateTime(_year, _month, _day);
                return _date;
            }
            else
            {
                return Convert.ToDateTime("01/01/1900");
            }
        }

        set
        {
            if (value.Year == 1900 && value.Month == 1 && value.Day == 1)
            {
                txtFecha.Text = string.Empty;
                _date = DateTime.Now;
            }
            else
            {
                _date = value;
                _day = _date.Day;
                _month = _date.Month;
                _year = _date.Year;
                txtFecha.Text = _date.ToString();
            }
        }
    }

    /// <summary>
    /// Get the Day
    /// </summary>
    /// <value>The Day</value>
    public int Day
    {
        get { return _day; }
    }

    /// <summary>
    /// Get the Month
    /// </summary>
    /// <value>The Month</value> 
    public int Month
    {
        get { return _month; }
    }

    /// <summary>
    /// Get the Year
    /// </summary>
    /// <value>The Year</value> 
    public int Year
    {
        get { return _year; }
    }

    /// <summary>
    /// Set the Enabled
    /// </summary>
    /// <value>The Enabled</value> 
    public bool Enabled
    {
        set
        {
            this.txtFecha.Enabled = value;
            this.imgCalendario.Visible = value;
            this.cvFecha.Enabled = value;
        }
    }

    /// <summary>
    /// Get the Requerid
    /// </summary>
    /// <value>The value</value> 
    public bool Requerid
    {
        set
        {
            this.rfvFecha.Enabled = value;
        }
    }

    /// <summary>
    /// Asigna texto requerido
    /// </summary>
    public string TextoRequerido
    {
        set
        {
            this.rfvFecha.ErrorMessage = value;
        }
    }

    public string ValidationGroup
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                this.rfvFecha.ValidationGroup = value;
                this.cvFecha.ValidationGroup = value;
            }
        }
    }

    /// <summary>
    /// Set the InitNull
    /// </summary>
    /// <value>The value</value>
    public bool InitNull
    {
        set
        {
            if (value)
                this.txtFecha.Text = string.Empty;
        }
    }

    /// <summary>
    /// Get or set the Text
    /// </summary>
    /// <value>The Text</value> 
    public string Text
    {
        get
        {
            return txtFecha.Text.Trim().ToUpper();
        }

        set
        {
            this.txtFecha.Text = value;
        }
    }

    /// <summary>
    /// Set the ReadOnly
    /// </summary>
    /// <value>The true</value> 
    public bool ReadOnly
    {
        set
        {
            if (value)
            {
                this.txtFecha.ReadOnly = true;
            }
        }
    }

    ///// <summary>
    ///// Método ValidateJS
    ///// </summary>
    ///// <value>The value</value> 
    //public string ValidateJS
    //{
    //    set
    //    {
    //        if (!string.IsNullOrEmpty(value))
    //        {
    //            //this.cetxtFecha.OnClientDateSelectionChanged = value;
    //        }
    //    }
    //}

    /// <summary>
    /// Set the ClearText
    /// </summary>
    /// <value>The value</value> 
    public bool ClearText
    {
        set
        {
            if (value)
            {
                this.txtFecha.Text = string.Empty;
            }
        }
    }

    /// <summary>
    /// Set the AutoPostBack
    /// </summary>
    /// <value>The AutoPostBack</value> 
    public bool AutoPostBack
    {
        set
        {
            this.txtFecha.AutoPostBack = value;
        }
    }
}