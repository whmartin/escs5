﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Control de usuario del Icbf para manejo de fechas con calendario
/// </summary>
public partial class comun_control_fecha : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e) { }
    private DateTime _date;
    private int _month;
    private int _day;
    private int _year;
    private int _controlTabIndex;

    /// <summary>
    /// Inicializa si la validación de la fecha es requerida
    /// </summary>
    /// <param name="pEtiqueta"></param>
    /// <param name="pRequerido"></param>
    public void Inicializar(String pEtiqueta, bool pRequerido)
    {
        // rfvFecha.Enabled = pRequerido;
    }

    /// <summary>
    /// Habilita si es obligatorio la fecha
    /// </summary>
    /// <param name="obligatoriedad"></param>
    public void HabilitarObligatoriedad(bool obligatoriedad)
    {
        // rfvFecha.Enabled = obligatoriedad;
    }

    /// <summary>
    /// Deshabilitar Checar formato
    /// </summary>
    /// <param name="obligatoriedad"></param>
    public void NoChecarFormato()
    {
        cvFecha.Enabled = false;
    }
    public void SiChecarFormato()
    {
        cvFecha.Enabled = true;
    }

    /// <summary>
    /// Habilita si es obligatorio la fecha y el mensaje de error
    /// </summary>
    /// <param name="txtMsg"></param>
    /// <param name="obligatoriedad"></param>
    public void HabilitarObligatoriedadYMessage(string txtMsg, bool obligatoriedad)
    {
        //rfvFecha.Enabled = obligatoriedad;
        //rfvFecha.ErrorMessage = txtMsg;
    }
    public DateTime Date
    {
        get
        {
            if (txtFecha.Text.Trim() != "")
            {
                try
                {
                    _day = Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Day);
                    _month = Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Month);

                    if (Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Year) > 1800)
                        _year = Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Year);
                    else
                        _year = 1900;

                    _date = new DateTime(_year, _month, _day);
                }
                catch (Exception ex)
                {
                    return DateTime.MinValue;
                }

                return _date;
            }
            else
            {
                return Convert.ToDateTime("01/01/1900");
            }
        }
        set
        {

            if (value.Year == 1899 && value.Month == 12 && value.Day == 31)
            {
                txtFecha.Text = "";
                _date = DateTime.Now;
            }
            else
            {
                _date = value;
                _day = _date.Day;
                _month = _date.Month;
                _year = _date.Year;
                txtFecha.Text = _date.ToString();
            }
        }
    }

    public int TabIndex
    {
        get
        {
            return _controlTabIndex;
        }
        set
        {
            _controlTabIndex = value;
            foreach (Control c in this.Controls)
            {
                if (c is WebControl)
                { ((WebControl)c).TabIndex = (short)_controlTabIndex++; }

            }
        }
    }

    public int Day
    {
        get { return _day; }
    }

    public int Month
    {
        get { return _month; }
    }

    public int Year
    {
        get { return _year; }
    }

    public Boolean Enabled
    {
        set
        {
            this.txtFecha.Enabled = value;
            this.imgCalendario.Visible = value;
        }
    }

    public String ValidationGroup
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                // this.rfvFecha.ValidationGroup = value;
            }
        }
    }

    public Boolean Requerid
    {
        set
        {
            // this.rfvFecha.Enabled = value;
        }
    }

    public String ErrorMessage
    {
        set
        {
            //this.rfvFecha.ErrorMessage = value;
        }
    }
    public Boolean InitNull
    {
        set
        {
            if (value)
                this.txtFecha.Text = "";
        }
    }

    public Boolean EnableText
    {
        set
        {
            if (value)
                txtFecha.Enabled = value;
        }
    }

    public void fechasMayoresA(DateTime fechaInicio)
    {
        cetxtFecha.StartDate = fechaInicio;
    }

    public void fechasMenoresA(DateTime fechaFin)
    {
        cetxtFecha.EndDate = fechaFin;
    }

    public bool AutoPostBackFecha
    {
        get
        {
            return txtFecha.AutoPostBack;
        }
        set
        {

            txtFecha.AutoPostBack = value;
        }

    }

    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        this.OnFechaChange();
    }

    public event EventHandler FechaChange;

    public virtual void OnFechaChange()
    {
        if (FechaChange != null)
        {
            this.FechaChange(this, EventArgs.Empty);
        }
    }
}