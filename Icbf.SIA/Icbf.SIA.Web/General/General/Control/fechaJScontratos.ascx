﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="fechaJScontratos.ascx.cs" Inherits="General_General_Control_fechaJScontratos" %>
<asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFecha" ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnAprobar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:TextBox ID="txtFecha" runat="server" CssClass="cF" onchange="EjecutarJS(this);"  ></asp:TextBox>
<asp:Image ID="imgCalendario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />&nbsp;
    <br />
<%--<asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFecha" ErrorMessage="El formato del campo es inválido" ForeColor="Red"
    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" Display="Dynamic"></asp:CompareValidator> --%>

<div style="display:none">
<Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
</Ajax:MaskedEditExtender>
<Ajax:CalendarExtender ID="cetxtFecha" runat="server" Format="dd/MM/yyyy" 
        PopupButtonID="imgCalendario" TargetControlID="txtFecha" CssClass="calendario"></Ajax:CalendarExtender>

</div>