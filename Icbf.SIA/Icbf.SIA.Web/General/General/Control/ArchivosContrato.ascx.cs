﻿using Icbf.Contrato.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using Icbf.SIA.Service;

[PrincipalPermission(SecurityAction.Demand, Authenticated = false)]
public partial class General_General_Control_ArchivosContrato : System.Web.UI.UserControl
{
    private bool _showCreate;

    [BindableAttribute(true)]
    public bool ShowCreate
    {
        get
        {
            return this._showCreate;
        }

        set
        {
            this._showCreate = value;
        }
    }

    GeneralWeb PageWeb;

    private ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (_showCreate)
                MultiViewCrear.ActiveViewIndex = 0;
            else
                MultiViewCrear.ActiveViewIndex = -1;


        }
    }

    public void LoadInfoContratos (int idContrato)
    {
        try
        {
            PageWeb = (GeneralWeb)this.Page;
            gvArchivosContratos.EmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
            hfIdTContratoControl.Value = idContrato.ToString();
            Buscar();
        }
        catch (Exception ex)
        {
            
        }
    }

    private void Buscar()
    {
        var misArchivos = vTipoSolicitudService.ConsultarArchivosPorContrato(int.Parse(hfIdTContratoControl.Value.ToString()));
        gvArchivosContratos.DataSource = misArchivos;
        gvArchivosContratos.DataBind();
    }

    protected void gvArchivosContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvArchivosContratos.PageIndex = e.NewPageIndex;
            Buscar();
        }
        catch (Exception ex)
        {

        }
    }

    private void CargarArchivoFTP()
    {
        String NombreArchivoOri;
        string NombreArchivo;
        decimal idarchivo = 0;

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                string filename = Path.GetFileName(fuArchivo.FileName);

                if (fuArchivo.PostedFile.ContentLength/1024 > 4096)
                    throw new GenericException("Sólo se permiten archivos inferiores a 4MB");
                
                NombreArchivoOri = filename;
                NombreArchivo = filename.Substring(0, filename.IndexOf("."));
                NombreArchivo = vTipoSolicitudService.QuitarAcentos(NombreArchivo);

                string tipoArchivo = "";
                if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() != "PDF")
                    throw new GenericException("Solo se permiten archivos PDF");

                if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() =="PDF")
                    tipoArchivo = "General.archivoPDF";
                
                List<Icbf.SIA.Entity.FormatoArchivo> vFormatoArchivo = vSiaService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

                if (vFormatoArchivo.Count == 0)
                {
                    if (vFormatoArchivo.Count > 0)
                    {
                      
                       // toolBar.MostrarMensajeError("No se ha parametrizado La extensión del archivo a cargar o La extensión esta duplicada");
                        return;
                    }
                }

                int idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

                NombreArchivo = Guid.NewGuid().ToString() + "_" + DateTime.Now.Year.ToString() + "_" +
                                DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" +
                                DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" +
                                DateTime.Now.Second.ToString() + "_OfOAC_" + NombreArchivo +
                                filename.Substring(filename.LastIndexOf("."));
                string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                          System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

                //Guardado del archivo en el servidor
                HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

                Boolean ArchivoSubido = vSiaService.SubirArchivoFtp((Stream) vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

                if (ArchivoSubido)
                {
                    //Creación del registro de control del archivo subido
                    ArchivoContrato RegistroControl = new ArchivoContrato();
                    RegistroControl.IdUsuario = Convert.ToInt32(PageWeb.GetSessionUser().IdUsuario.ToString());
                    
                    RegistroControl.FechaRegistro = DateTime.Now;
                    RegistroControl.IdContrato = int.Parse(hfIdTContratoControl.Value.ToString());
                    RegistroControl.ServidorFTP = NombreArchivoFtp;
                    RegistroControl.NombreArchivo = NombreArchivo;
                    RegistroControl.NombreArchivoOri = NombreArchivoOri;
                    RegistroControl.IdFormatoArchivo = idformatoArchivo; //idFormato; 
                    RegistroControl.Estado = "C";
                    RegistroControl.ResumenCarga = string.Empty;
                    RegistroControl.UsuarioCrea = PageWeb.GetSessionUser().NombreUsuario;
                    RegistroControl.FechaCrea = DateTime.Now;
                    vTipoSolicitudService.InsertarArchivoContratoGeneral(RegistroControl);
                    idarchivo = RegistroControl.IdArchivo;
                }
            }
            catch (Exception ex)
            {
                //toolBar.MostrarMensajeError(ex.Message);
                return;
            }
        }
    }

    private void Iniciar()
    {

    }
}