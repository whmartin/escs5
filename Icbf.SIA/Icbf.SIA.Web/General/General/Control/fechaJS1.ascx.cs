﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Control de usuario del Icbf para manejo de fechas con calendario
/// </summary>
public partial class comun_control_fechaJS1 : System.Web.UI.UserControl
{
    /// <summary>
    /// Manejador del evento Carga la Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.AppRelativeVirtualPath.Contains("Experiencia"))
        {
            txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
    private DateTime _date;
    private int _month;
    private int _day;
    private int _year;

    /// <summary>
    /// Inicializa si la fecha es requerida
    /// </summary>
    /// <param name="pEtiqueta"></param>
    /// <param name="pRequerido"></param>
    public void Inicializar(String pEtiqueta, bool pRequerido)
    {
        rfvFecha.Enabled = pRequerido;
    }
    public DateTime Date
    {
        get
        {
            if (txtFecha.Text.Trim() != "")
            {
                _day = Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Day);
                _month = Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Month);

                if (Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Year) > 1800)
                    _year = Convert.ToInt16(Convert.ToDateTime(txtFecha.Text.Trim()).Year);
                else
                    _year = 1900;

                _date = new DateTime(_year, _month, _day);
                return _date;
            }
            else
            {
                return Convert.ToDateTime("01/01/1900");
            }
        }
        set
        {

            if (value.Year == 1900 && value.Month == 1 && value.Day == 1)
            {
                txtFecha.Text = "";
                _date = DateTime.Now;
            }
            else
            {
                _date = value;
                _day = _date.Day;
                _month = _date.Month;
                _year = _date.Year;
                txtFecha.Text = _date.ToString();
            }
        }
    }
    public int Day
    {
        get { return _day; }
    }
    public int Month
    {
        get { return _month; }
    }
    public int Year
    {
        get { return _year; }
    }
    public Boolean Enabled
    {
        set
        {
            this.txtFecha.Enabled = value;
            this.imgCalendario.Visible = value;
        }
    }
    public Boolean Requerid
    {
        set
        {
            this.rfvFecha.Enabled = value;
        }
    }
    public Boolean InitNull
    {
        set
        {
            if (value)
                this.txtFecha.Text = "";
        }
    }
}