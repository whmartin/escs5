﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Control transversal para módulo revisión
/// </summary>
public partial class General_General_Control_Revision : System.Web.UI.UserControl
{
    #region eventos
    
    /// <summary>
    /// Manejador del evento de una lista desplegable cuando su valor cambia
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlAprueba_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAprueba.SelectedValue == "2")
        {
            txtObservaciones.Enabled = true;
            rfvObservaciones.Enabled = true;
        }
        else
        {
            txtObservaciones.Enabled = false;
            rfvObservaciones.Enabled = false;
            txtObservaciones.Text = "";
        }
    }
    #endregion
}