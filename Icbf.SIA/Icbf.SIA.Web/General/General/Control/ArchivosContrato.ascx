﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArchivosContrato.ascx.cs" Inherits="General_General_Control_ArchivosContrato" %>
       <asp:HiddenField ID="hfIdTContratoControl" runat="server" />
<asp:MultiView ID="MultiViewCrear" runat="server">
    <asp:View ID="ViewCrear" runat="server">
        <table width="90%" align="center">
                         <tr class="rowA">
                <td> 
                    Archivo:
               </td>
            </tr>
             <tr class="rowB">
                <td>
                
                    <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                
               </td>
            </tr>
        </table>
    </asp:View>
</asp:MultiView>


<table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:GridView runat="server" ID="gvArchivosContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvArchivosContratos_PageIndexChanging" style="font-weight: 700">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />
                            <asp:BoundField HeaderText="Nombre" DataField="NombreArchivo" />
                            <asp:BoundField HeaderText="Extensión" DataField="Extension" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>                
            </td>
        </tr>
</table>
