﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="fechaFestivos.ascx.cs" Inherits="comun_control_fecha_festivos" %>


<script src="../../../Scripts/jquery-1.11.3.min.js"></script>
<%--<script src="../../../Scripts/jquery.mask.js"></script>--%>
<style>
    .date {
        position: relative;
        top: -6px;
    }
</style>
<asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFecha" Font-Bold="true" ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
<asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFecha$txtFecha" ErrorMessage="Fecha Inválida" ForeColor="Red"
    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" Display="Dynamic" ValidationGroup="btnBuscar"></asp:CompareValidator>
<br />
<asp:TextBox ID="txtFecha" runat="server" placeholder="dd/mm/aaaa" MaxLength="10" class="date" data-mask="00/00/0000"></asp:TextBox>
<asp:ImageButton ID="imgCalendario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: pointer;" />

<Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
            CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
            CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
        </Ajax:MaskedEditExtender>

<Ajax:CalendarExtender ID="cetxtFecha" runat="server" Format="dd/MM/yyyy" OnClientShown="DisableWeekend"
    PopupButtonID="imgCalendario" TargetControlID="txtFecha">
</Ajax:CalendarExtender>
<Ajax:FilteredTextBoxExtender ID="ftFecha" runat="server" TargetControlID="txtFecha"
    FilterType="Custom,Numbers" ValidChars="/" />
<script type="text/javascript">
    function WaterMarkFocus(txt, text) {
        if (txt.value == text) {
            txt.value = "";
            txt.style.color = "black";
        }
    }

    function WaterMarkBlur(txt, text) {
        if (txt.value == "") {
            txt.value = text;
            txt.style.color = "gray";
        }
    }

    var restartMonthFlag = 0;
    var restartMonthValue = 0;

    var idCalendario = '0';

    function SetID(control) {
        idCalendario = control;
    }



    function DisableWeekend(clickRow) {

        $('#' + 'cphCont_' + idCalendario + '_cetxtFecha_body').css('height', '160px');
        $('#' + 'cphCont_' + idCalendario + '_cetxtFecha_body').css('width', '160px');

        //var targetMonths = document.getElementById('cphCont_' + idCalendario + '_cetxtFecha_monthsBody').getElementsByClassName("ajax__calendar_month");

        //for (i = 0; i < targetMonths.length; i++) {
        //    targetMonths[i].style.width = "30px";
        //    targetMonths[i].style.height = "40px";
        //}

        //var targetYear = document.getElementById('cphCont_' + idCalendario + '_cetxtFecha_yearsBody').getElementsByClassName("ajax__calendar_year");

        //for (i = 0; i < targetYear.length; i++) {
        //    targetYear[i].style.width = "30px";
        //}
    }

    //$(document).ready(function () {
    //    $('.date').mask('00/00/0000');
    //});
    
</script>

