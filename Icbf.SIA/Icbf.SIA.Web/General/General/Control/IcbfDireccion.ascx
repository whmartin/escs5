﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IcbfDireccion.ascx.cs"
    Inherits="IcbfDireccion" Debug="true" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<div style="width: 100%">
    <asp:UpdatePanel ID="upZonaDireccion" runat="server">
        <ContentTemplate>
            <div style="width: 100%">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblZona" runat="server" Text="Zona *"></asp:Label>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvZonaGeo" ForeColor="Red" runat="server" CssClass="componente"
                                ValidationGroup="btnGuardar" ControlToValidate="rbtnDetalleZonaGeo" ErrorMessage=" Campo Requerido"
                                SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rbtnDetalleZonaGeo" runat="server"
                                CssClass="componente" RepeatDirection="Horizontal"
                                Width="110%" AutoPostBack="true"
                                OnSelectedIndexChanged="rbtnDetalleZonaGeo_SelectedIndexChanged">
                                <asp:ListItem Value="R">Rural</asp:ListItem>
                                <asp:ListItem Value="U">Urbana</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: left; width: 100%">
                <asp:Label ID="lblDireccion" runat="server" Text="Direcci&oacute;n *"></asp:Label>
                <asp:RequiredFieldValidator ID="rfvTxtDireccion" SetFocusOnError="true" runat="server"
                    ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar" ControlToValidate="txtDireccion"
                    Display="Dynamic" Enabled="False" ForeColor="Red"></asp:RequiredFieldValidator>

                <asp:Panel ID="pTipoDireccion" runat="server">
                    <table border="0" style="text-align: left; width: 100%">
                        <tr>
                            <td style="width: 50%">
                                <asp:Panel ID="panel" runat="server">
                                    <asp:DropDownList ID="ddlVia" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVia_SelectedIndexChanged"
                                        CssClass="comboDireccionVia" Width="40%">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="AU">Autopista</asp:ListItem>
                                        <asp:ListItem Value="AV">Avenida</asp:ListItem>
                                        <asp:ListItem Value="AC">Avenida Calle</asp:ListItem>
                                        <asp:ListItem Value="AK">Avenida Carrera</asp:ListItem>
                                        <asp:ListItem Value="BL">Bulevar</asp:ListItem>
                                        <asp:ListItem Value="CL">Calle</asp:ListItem>
                                        <asp:ListItem Value="KR">Carrera</asp:ListItem>
                                        <asp:ListItem Value="CT">Carretera</asp:ListItem>
                                        <asp:ListItem Value="CQ">Circular</asp:ListItem>
                                        <asp:ListItem Value="CC">Cuentas Corridas</asp:ListItem>
                                        <asp:ListItem Value="DG">Diagonal</asp:ListItem>
                                        <asp:ListItem Value="PJ">Pasaje</asp:ListItem>
                                        <asp:ListItem Value="PS">Paseo</asp:ListItem>
                                        <asp:ListItem Value="PT">Peatonal</asp:ListItem>
                                        <asp:ListItem Value="TV">Transversal</asp:ListItem>
                                        <asp:ListItem Value="TC">Troncal</asp:ListItem>
                                        <asp:ListItem Value="VT">Variante</asp:ListItem>
                                        <asp:ListItem Value="VI">Vía</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlManzana" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlManzana_SelectedIndexChanged"
                                        CssClass="comboDireccionManzana" Width="30%">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="MZ">Manzana</asp:ListItem>
                                        <asp:ListItem Value="IN">Interior</asp:ListItem>
                                        <asp:ListItem Value="SC">Sector</asp:ListItem>
                                        <asp:ListItem Value="ET">Etapa</asp:ListItem>
                                        <asp:ListItem Value="ED">Edificio</asp:ListItem>
                                        <asp:ListItem Value="MD">Modulo</asp:ListItem>
                                        <asp:ListItem Value="TO">Torre</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:Panel>
                            </td>
                            <td style="width: 50%"></td>
                        </tr>
                        <tr>
                            <td style="width: 50%">
                                <asp:Panel ID="pVia" runat="server" Width="100%">
                                    <asp:TextBox ID="txtNombreVia" runat="server" CssClass="cajaTextoGen" Width="27%"
                                        OnTextChanged="Obj_LostFocus" AutoPostBack="true" MaxLength="100"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteTxtNombreVia" TargetControlID="txtNombreVia"
                                        runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom"
                                        Enabled="True" ValidChars=" ">
                                    </Ajax:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="rfvTxtNombreVia" SetFocusOnError="true" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Width="2%" ControlToValidate="txtNombreVia"
                                        ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlLetra" runat="server" CssClass="comboLetras" Width="10%"
                                        OnSelectedIndexChanged="Obj_LostFocus" AutoPostBack="true">
                                        <asp:ListItem Value=" "></asp:ListItem>
                                        <asp:ListItem Value="A">A</asp:ListItem>
                                        <asp:ListItem Value="B">B</asp:ListItem>
                                        <asp:ListItem Value="C">C</asp:ListItem>
                                        <asp:ListItem Value="D">D</asp:ListItem>
                                        <asp:ListItem Value="E">E</asp:ListItem>
                                        <asp:ListItem Value="F">F</asp:ListItem>
                                        <asp:ListItem Value="G">G</asp:ListItem>
                                        <asp:ListItem Value="H">H</asp:ListItem>
                                        <asp:ListItem Value="I">I</asp:ListItem>
                                        <asp:ListItem Value="J">J</asp:ListItem>
                                        <asp:ListItem Value="K">K</asp:ListItem>
                                        <asp:ListItem Value="L">L</asp:ListItem>
                                        <asp:ListItem Value="M">M</asp:ListItem>
                                        <asp:ListItem Value="N">N</asp:ListItem>
                                        <asp:ListItem Value="O">O</asp:ListItem>
                                        <asp:ListItem Value="P">P</asp:ListItem>
                                        <asp:ListItem Value="Q">Q</asp:ListItem>
                                        <asp:ListItem Value="R">R</asp:ListItem>
                                        <asp:ListItem Value="S">S</asp:ListItem>
                                        <asp:ListItem Value="T">T</asp:ListItem>
                                        <asp:ListItem Value="U">U</asp:ListItem>
                                        <asp:ListItem Value="V">V</asp:ListItem>
                                        <asp:ListItem Value="W">W</asp:ListItem>
                                        <asp:ListItem Value="X">X</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                        <asp:ListItem Value="Z">Z</asp:ListItem>
                                        <asp:ListItem Value="AA">AA</asp:ListItem>
                                        <asp:ListItem Value="AB">AB</asp:ListItem>
                                        <asp:ListItem Value="AC">AC</asp:ListItem>
                                        <asp:ListItem Value="AD">AD</asp:ListItem>
                                        <asp:ListItem Value="AE">AE</asp:ListItem>
                                        <asp:ListItem Value="AF">AF</asp:ListItem>
                                        <asp:ListItem Value="AG">AG</asp:ListItem>
                                        <asp:ListItem Value="AH">AH</asp:ListItem>
                                        <asp:ListItem Value="AI">AI</asp:ListItem>
                                        <asp:ListItem Value="AJ">AJ</asp:ListItem>
                                        <asp:ListItem Value="AK">AK</asp:ListItem>
                                        <asp:ListItem Value="AL">AL</asp:ListItem>
                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                        <asp:ListItem Value="AN">AN</asp:ListItem>
                                        <asp:ListItem Value="AO">AO</asp:ListItem>
                                        <asp:ListItem Value="AP">AP</asp:ListItem>
                                        <asp:ListItem Value="AQ">AQ</asp:ListItem>
                                        <asp:ListItem Value="AR">AR</asp:ListItem>
                                        <asp:ListItem Value="AS">AS</asp:ListItem>
                                        <asp:ListItem Value="AT">AT</asp:ListItem>
                                        <asp:ListItem Value="AU">AU</asp:ListItem>
                                        <asp:ListItem Value="AV">AV</asp:ListItem>
                                        <asp:ListItem Value="AW">AW</asp:ListItem>
                                        <asp:ListItem Value="AX">AX</asp:ListItem>
                                        <asp:ListItem Value="AY">AY</asp:ListItem>
                                        <asp:ListItem Value="AZ">AZ</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlBis" runat="server" CssClass="comboBis" Width="10%" OnSelectedIndexChanged="DesactivarLetra2"
                                        AutoPostBack="true">
                                        <asp:ListItem Value="">N/A</asp:ListItem>
                                        <asp:ListItem Value="BIS">BIS</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlLetra2" runat="server" CssClass="comboLetras" Width="10%"
                                        Enabled="false" OnSelectedIndexChanged="Obj_LostFocus" AutoPostBack="true">
                                        <asp:ListItem Value=" "></asp:ListItem>
                                        <asp:ListItem Value="A">A</asp:ListItem>
                                        <asp:ListItem Value="B">B</asp:ListItem>
                                        <asp:ListItem Value="C">C</asp:ListItem>
                                        <asp:ListItem Value="D">D</asp:ListItem>
                                        <asp:ListItem Value="E">E</asp:ListItem>
                                        <asp:ListItem Value="F">F</asp:ListItem>
                                        <asp:ListItem Value="G">G</asp:ListItem>
                                        <asp:ListItem Value="H">H</asp:ListItem>
                                        <asp:ListItem Value="I">I</asp:ListItem>
                                        <asp:ListItem Value="J">J</asp:ListItem>
                                        <asp:ListItem Value="K">K</asp:ListItem>
                                        <asp:ListItem Value="L">L</asp:ListItem>
                                        <asp:ListItem Value="M">M</asp:ListItem>
                                        <asp:ListItem Value="N">N</asp:ListItem>
                                        <asp:ListItem Value="O">O</asp:ListItem>
                                        <asp:ListItem Value="P">P</asp:ListItem>
                                        <asp:ListItem Value="Q">Q</asp:ListItem>
                                        <asp:ListItem Value="R">R</asp:ListItem>
                                        <asp:ListItem Value="S">S</asp:ListItem>
                                        <asp:ListItem Value="T">T</asp:ListItem>
                                        <asp:ListItem Value="U">U</asp:ListItem>
                                        <asp:ListItem Value="V">V</asp:ListItem>
                                        <asp:ListItem Value="W">W</asp:ListItem>
                                        <asp:ListItem Value="X">X</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                        <asp:ListItem Value="Z">Z</asp:ListItem>
                                        <asp:ListItem Value="AA">AA</asp:ListItem>
                                        <asp:ListItem Value="AB">AB</asp:ListItem>
                                        <asp:ListItem Value="AC">AC</asp:ListItem>
                                        <asp:ListItem Value="AD">AD</asp:ListItem>
                                        <asp:ListItem Value="AE">AE</asp:ListItem>
                                        <asp:ListItem Value="AF">AF</asp:ListItem>
                                        <asp:ListItem Value="AG">AG</asp:ListItem>
                                        <asp:ListItem Value="AH">AH</asp:ListItem>
                                        <asp:ListItem Value="AI">AI</asp:ListItem>
                                        <asp:ListItem Value="AJ">AJ</asp:ListItem>
                                        <asp:ListItem Value="AK">AK</asp:ListItem>
                                        <asp:ListItem Value="AL">AL</asp:ListItem>
                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                        <asp:ListItem Value="AN">AN</asp:ListItem>
                                        <asp:ListItem Value="AO">AO</asp:ListItem>
                                        <asp:ListItem Value="AP">AP</asp:ListItem>
                                        <asp:ListItem Value="AQ">AQ</asp:ListItem>
                                        <asp:ListItem Value="AR">AR</asp:ListItem>
                                        <asp:ListItem Value="AS">AS</asp:ListItem>
                                        <asp:ListItem Value="AT">AT</asp:ListItem>
                                        <asp:ListItem Value="AU">AU</asp:ListItem>
                                        <asp:ListItem Value="AV">AV</asp:ListItem>
                                        <asp:ListItem Value="AW">AW</asp:ListItem>
                                        <asp:ListItem Value="AX">AX</asp:ListItem>
                                        <asp:ListItem Value="AY">AY</asp:ListItem>
                                        <asp:ListItem Value="AZ">AZ</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlSentido" runat="server" CssClass="comboSentido" Width="20%"
                                        OnSelectedIndexChanged="Obj_LostFocus" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtNumero" runat="server" CssClass="cajaTextoNumero" OnTextChanged="Obj_LostFocus"
                                        AutoPostBack="true" MaxLength="3" Width="10%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteTxtNumero" TargetControlID="txtNumero" runat="server"
                                        FilterType="Numbers" Enabled="True">
                                    </Ajax:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="rfvTxtNumero" SetFocusOnError="true" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Width="5%" ValidationGroup="btnGuardar" ControlToValidate="txtNumero"
                                        Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                                    <br />
                                    <asp:DropDownList ID="ddlLetra3" runat="server" CssClass="comboLetras" Width="15%"
                                        OnSelectedIndexChanged="Obj_LostFocus" AutoPostBack="true">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="A">A</asp:ListItem>
                                        <asp:ListItem Value="B">B</asp:ListItem>
                                        <asp:ListItem Value="C">C</asp:ListItem>
                                        <asp:ListItem Value="D">D</asp:ListItem>
                                        <asp:ListItem Value="E">E</asp:ListItem>
                                        <asp:ListItem Value="F">F</asp:ListItem>
                                        <asp:ListItem Value="G">G</asp:ListItem>
                                        <asp:ListItem Value="H">H</asp:ListItem>
                                        <asp:ListItem Value="I">I</asp:ListItem>
                                        <asp:ListItem Value="J">J</asp:ListItem>
                                        <asp:ListItem Value="K">K</asp:ListItem>
                                        <asp:ListItem Value="L">L</asp:ListItem>
                                        <asp:ListItem Value="M">M</asp:ListItem>
                                        <asp:ListItem Value="N">N</asp:ListItem>
                                        <asp:ListItem Value="O">O</asp:ListItem>
                                        <asp:ListItem Value="P">P</asp:ListItem>
                                        <asp:ListItem Value="Q">Q</asp:ListItem>
                                        <asp:ListItem Value="R">R</asp:ListItem>
                                        <asp:ListItem Value="S">S</asp:ListItem>
                                        <asp:ListItem Value="T">T</asp:ListItem>
                                        <asp:ListItem Value="U">U</asp:ListItem>
                                        <asp:ListItem Value="V">V</asp:ListItem>
                                        <asp:ListItem Value="W">W</asp:ListItem>
                                        <asp:ListItem Value="X">X</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                        <asp:ListItem Value="Z">Z</asp:ListItem>
                                        <asp:ListItem Value="AA">AA</asp:ListItem>
                                        <asp:ListItem Value="AB">AB</asp:ListItem>
                                        <asp:ListItem Value="AC">AC</asp:ListItem>
                                        <asp:ListItem Value="AD">AD</asp:ListItem>
                                        <asp:ListItem Value="AE">AE</asp:ListItem>
                                        <asp:ListItem Value="AF">AF</asp:ListItem>
                                        <asp:ListItem Value="AG">AG</asp:ListItem>
                                        <asp:ListItem Value="AH">AH</asp:ListItem>
                                        <asp:ListItem Value="AI">AI</asp:ListItem>
                                        <asp:ListItem Value="AJ">AJ</asp:ListItem>
                                        <asp:ListItem Value="AK">AK</asp:ListItem>
                                        <asp:ListItem Value="AL">AL</asp:ListItem>
                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                        <asp:ListItem Value="AN">AN</asp:ListItem>
                                        <asp:ListItem Value="AO">AO</asp:ListItem>
                                        <asp:ListItem Value="AP">AP</asp:ListItem>
                                        <asp:ListItem Value="AQ">AQ</asp:ListItem>
                                        <asp:ListItem Value="AR">AR</asp:ListItem>
                                        <asp:ListItem Value="AS">AS</asp:ListItem>
                                        <asp:ListItem Value="AT">AT</asp:ListItem>
                                        <asp:ListItem Value="AU">AU</asp:ListItem>
                                        <asp:ListItem Value="AV">AV</asp:ListItem>
                                        <asp:ListItem Value="AW">AW</asp:ListItem>
                                        <asp:ListItem Value="AX">AX</asp:ListItem>
                                        <asp:ListItem Value="AY">AY</asp:ListItem>
                                        <asp:ListItem Value="AZ">AZ</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlBis2" runat="server" CssClass="comboBis" Width="15%" OnSelectedIndexChanged="DesactivarLetra4"
                                        AutoPostBack="true">
                                        <asp:ListItem Value="">N/A</asp:ListItem>
                                        <asp:ListItem Value="BIS">BIS</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlLetra4" runat="server" CssClass="comboLetras" Width="15%"
                                        Enabled="false" OnSelectedIndexChanged="Obj_LostFocus" AutoPostBack="true">
                                        <asp:ListItem Value=""> </asp:ListItem>
                                        <asp:ListItem Value="A">A</asp:ListItem>
                                        <asp:ListItem Value="B">B</asp:ListItem>
                                        <asp:ListItem Value="C">C</asp:ListItem>
                                        <asp:ListItem Value="D">D</asp:ListItem>
                                        <asp:ListItem Value="E">E</asp:ListItem>
                                        <asp:ListItem Value="F">F</asp:ListItem>
                                        <asp:ListItem Value="G">G</asp:ListItem>
                                        <asp:ListItem Value="H">H</asp:ListItem>
                                        <asp:ListItem Value="I">I</asp:ListItem>
                                        <asp:ListItem Value="J">J</asp:ListItem>
                                        <asp:ListItem Value="K">K</asp:ListItem>
                                        <asp:ListItem Value="L">L</asp:ListItem>
                                        <asp:ListItem Value="M">M</asp:ListItem>
                                        <asp:ListItem Value="N">N</asp:ListItem>
                                        <asp:ListItem Value="O">O</asp:ListItem>
                                        <asp:ListItem Value="P">P</asp:ListItem>
                                        <asp:ListItem Value="Q">Q</asp:ListItem>
                                        <asp:ListItem Value="R">R</asp:ListItem>
                                        <asp:ListItem Value="S">S</asp:ListItem>
                                        <asp:ListItem Value="T">T</asp:ListItem>
                                        <asp:ListItem Value="U">U</asp:ListItem>
                                        <asp:ListItem Value="V">V</asp:ListItem>
                                        <asp:ListItem Value="W">W</asp:ListItem>
                                        <asp:ListItem Value="X">X</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                        <asp:ListItem Value="Z">Z</asp:ListItem>
                                        <asp:ListItem Value="AA">AA</asp:ListItem>
                                        <asp:ListItem Value="AB">AB</asp:ListItem>
                                        <asp:ListItem Value="AC">AC</asp:ListItem>
                                        <asp:ListItem Value="AD">AD</asp:ListItem>
                                        <asp:ListItem Value="AE">AE</asp:ListItem>
                                        <asp:ListItem Value="AF">AF</asp:ListItem>
                                        <asp:ListItem Value="AG">AG</asp:ListItem>
                                        <asp:ListItem Value="AH">AH</asp:ListItem>
                                        <asp:ListItem Value="AI">AI</asp:ListItem>
                                        <asp:ListItem Value="AJ">AJ</asp:ListItem>
                                        <asp:ListItem Value="AK">AK</asp:ListItem>
                                        <asp:ListItem Value="AL">AL</asp:ListItem>
                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                        <asp:ListItem Value="AN">AN</asp:ListItem>
                                        <asp:ListItem Value="AO">AO</asp:ListItem>
                                        <asp:ListItem Value="AP">AP</asp:ListItem>
                                        <asp:ListItem Value="AQ">AQ</asp:ListItem>
                                        <asp:ListItem Value="AR">AR</asp:ListItem>
                                        <asp:ListItem Value="AS">AS</asp:ListItem>
                                        <asp:ListItem Value="AT">AT</asp:ListItem>
                                        <asp:ListItem Value="AU">AU</asp:ListItem>
                                        <asp:ListItem Value="AV">AV</asp:ListItem>
                                        <asp:ListItem Value="AW">AW</asp:ListItem>
                                        <asp:ListItem Value="AX">AX</asp:ListItem>
                                        <asp:ListItem Value="AY">AY</asp:ListItem>
                                        <asp:ListItem Value="AZ">AZ</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtPlaca" runat="server" CssClass="cajaTextoNumerolargo" Width="20%"
                                        OnTextChanged="Obj_LostFocus" AutoPostBack="true" MaxLength="3"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteTxtPlaca" TargetControlID="txtPlaca" runat="server"
                                        FilterType="Numbers" Enabled="True">
                                    </Ajax:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="rfvTxtPlaca" SetFocusOnError="true" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Width="2%" ControlToValidate="txtPlaca" ValidationGroup="btnGuardar"
                                        Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlSentido2" runat="server" CssClass="comboSentidolargo" OnSelectedIndexChanged="Obj_LostFocus"
                                        AutoPostBack="true" Width="25%">
                                    </asp:DropDownList>
                                </asp:Panel>
                                <asp:Panel ID="pManzana" runat="server" Width="100%">
                                    <asp:TextBox ID="txtNombreManzana" runat="server" CssClass="cajaTextoGen" Width="30%"
                                        OnTextChanged="Obj_LostFocus" AutoPostBack="true" MaxLength="100"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteTxtNombreManzana" TargetControlID="txtNombreManzana"
                                        runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom"
                                        Enabled="True" ValidChars=" ">
                                    </Ajax:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="rfvTxtNombreManzana" SetFocusOnError="true" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Width="2%" ControlToValidate="txtNombreManzana"
                                        Display="Dynamic" ValidationGroup="btnGuardar" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlUnidad" runat="server" CssClass="comboDireccionVia" Width="30%"
                                        OnSelectedIndexChanged="Obj_LostFocus" AutoPostBack="true">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="AL">Altillo</asp:ListItem>
                                        <asp:ListItem Value="AP">Apartamento</asp:ListItem>
                                        <asp:ListItem Value="BG">Bodega</asp:ListItem>
                                        <asp:ListItem Value="CS">Casa</asp:ListItem>
                                        <asp:ListItem Value="CN">Consultorio</asp:ListItem>
                                        <asp:ListItem Value="DP">Depósito</asp:ListItem>
                                        <asp:ListItem Value="DS">Depósito Sótano</asp:ListItem>
                                        <asp:ListItem Value="GA">Garaje</asp:ListItem>
                                        <asp:ListItem Value="GS">Garaje Sótano</asp:ListItem>
                                        <asp:ListItem Value="LC">Local</asp:ListItem>
                                        <asp:ListItem Value="LM">Local Mezzanine</asp:ListItem>
                                        <asp:ListItem Value="LT">Lote</asp:ListItem>
                                        <asp:ListItem Value="MN">Mezzanine</asp:ListItem>
                                        <asp:ListItem Value="OF">Oficina</asp:ListItem>
                                        <asp:ListItem Value="PA">Parqueadero</asp:ListItem>
                                        <asp:ListItem Value="PN">Pent-House</asp:ListItem>
                                        <asp:ListItem Value="PL">Planta</asp:ListItem>
                                        <asp:ListItem Value="PD">Predio</asp:ListItem>
                                        <asp:ListItem Value="SS">Semisótano</asp:ListItem>
                                        <asp:ListItem Value="SO">Sótano</asp:ListItem>
                                        <asp:ListItem Value="ST">Suite</asp:ListItem>
                                        <asp:ListItem Value="TZ">Terraza</asp:ListItem>
                                        <asp:ListItem Value="UN">Unidad</asp:ListItem>
                                        <asp:ListItem Value="UL">Unidad Residencial</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvDdlUnidad" SetFocusOnError="true" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Width="2%" ControlToValidate="ddlUnidad" Display="Dynamic"
                                        ValidationGroup="btnGuardar" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtNombreUnidad" runat="server" CssClass="cajaTextoGen" Width="30%"
                                        OnTextChanged="Obj_LostFocus" AutoPostBack="true" MaxLength="100"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteTxtNombreUnidad" TargetControlID="txtNombreUnidad"
                                        runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom"
                                        Enabled="True" ValidChars=" ">
                                    </Ajax:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="rfvTxtNombreUnidad" SetFocusOnError="true" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Width="2%" ControlToValidate="txtNombreUnidad"
                                        ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator>
                                </asp:Panel>
                            </td>
                            <td style="width: 50%; vertical-align: bottom;">
                                <asp:Panel ID="Panel1" runat="server" Width="100%">
                                    <asp:TextBox ID="txtComplemento" runat="server" CssClass="cajaTextoGen" OnTextChanged="Obj_LostFocus"
                                        AutoPostBack="true" Visible="false" MaxLength="100"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteTxtComplemento" TargetControlID="txtComplemento"
                                        runat="server" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom"
                                        Enabled="True" ValidChars=" ">
                                    </Ajax:FilteredTextBoxExtender>
                                    <br />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 75%">
                            <asp:TextBox ID="txtDireccion" AutoPostBack="true" runat="server" CssClass="cajaTextoDireccion"
                                Width="95%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 25%">
                            <asp:LinkButton ID="btnLimpiarDireccion" runat="server" CssClass="vinculo" OnClick="btnLimpiarDireccion_Click">Limpiar</asp:LinkButton>
                        </td>
                        <%--                        <td style="width: 77px; height: 59px;">
                            <asp:Label ID="LblObsevaciones" runat="server" Text="Observación"
                                Visible="False"></asp:Label>
                            <asp:TextBox ID="TxtObservacionAU" runat="server" Height="55px" TextMode="MultiLine"
                                Width="405px" Visible="False"></asp:TextBox>
                        </td>--%>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
