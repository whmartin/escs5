﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Cargue_Archivos.aspx.cs" Inherits="General_General_Cargue_Archivos_Cargue_Archivos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table style="width: 100%">
        <tr class="rowA">
            <td>
                <asp:Label runat="server" ID="LblTipoArchivo" Text="Tipo Archivo"></asp:Label>
                <asp:RegularExpressionValidator runat="server" ID="revArchivo" ControlToValidate="FuArchivoUsuario"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
            <td>
                <asp:Label runat="server" ID="LblDocumento" Text="Documento"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:DropDownList runat="server" Width="500px" ID="ddlTipoArchivo" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlTipoArchivo_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:FileUpload ID="FuArchivoUsuario" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
            </td>
        </tr>
        <tr class="rowAG"  >
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvParticipante" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdParticipante" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvParticipante_PageIndexChanging" OnSelectedIndexChanged="gvParticipante_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo de Documento de Identidad" DataField="NomTipoDocumento" />
                            <asp:BoundField HeaderText="Numero de Documento de Identidad" DataField="NumeroDocumento" />
                            <asp:BoundField HeaderText="Primer Nombre Participante" DataField="PrimerNombre" />
                            <asp:BoundField HeaderText="Primer Apellido Participante" DataField="PrimerApellido" />
                            <asp:BoundField HeaderText="Sexo" DataField="Sexo" />
                            <asp:BoundField HeaderText="Fecha de Nacimiento" DataField="FechaNacimiento" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
    </table>
</asp:Content>
