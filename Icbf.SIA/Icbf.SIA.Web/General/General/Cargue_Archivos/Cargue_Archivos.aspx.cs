﻿using System;
using System.Web.UI.WebControls;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
/// <summary>
/// Página a la que se redirigen los módulos que incluyen subida de archivos en pantalla independiente
/// </summary>
public partial class General_General_Cargue_Archivos_Cargue_Archivos : GeneralWeb
{
    masterPrincipal toolBar;

    #region "Funciones y procedimientos"

    /// <summary>
    /// Inicializa variables locales y manejadores de eventos
    /// </summary>
        private void Iniciar()
        {
            try
            {
                toolBar = (masterPrincipal)this.Master;
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

                gvParticipante.PageSize = PageSize();
                gvParticipante.EmptyDataText = EmptyDataText();

                toolBar.EstablecerTitulos("Participante", SolutionPage.List.ToString());
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }

    /// <summary>
    /// Maneja el evento cuendo selecciona un registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
        private void SeleccionarRegistro(GridViewRow pRow)
        {
            try
            {
                int rowIndex = pRow.RowIndex;
                string strValue = gvParticipante.DataKeys[rowIndex].Value.ToString();
                SetSessionParameter("Participante.IdParticipante", strValue);
                NavigateTo(SolutionPage.Detail);
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }


        private void Buscar()
        {
            
        }

    #endregion

    #region "Eventos"

    /// <summary>
    /// Manejador del evento de la lista cuando cambia de valor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        protected void ddlTipoArchivo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoArchivo.SelectedValue == "1")
            {
                revArchivo.ValidationExpression = @"^.+\.((jpg)|(JPG))$";
                revArchivo.ErrorMessage = "Solo se permiten archivos (JPG)";
            }
            if (ddlTipoArchivo.SelectedValue == "2")
            {
                revArchivo.ValidationExpression = @"^.+\.((pdf)|(PDF))$";
                revArchivo.ErrorMessage = "Solo se permiten archivos (PDF)";
            }
        }

    /// <summary>
    /// Manejador del evento cuando cambia el ìndice de la pàgina en un control grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        protected void gvParticipante_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvParticipante.PageIndex = e.NewPageIndex;
            Buscar();
        }

    /// <summary>
    /// Manejador del evento para el botón buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

       /// <summary>
       /// Manejador del evento para el botón Nuevo
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            NavigateTo(SolutionPage.Add);
        }

    /// <summary>
    /// Manejador del evento para cuando el ìndice del registro cambia en una grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        protected void gvParticipante_SelectedIndexChanged(object sender, EventArgs e)
        {
            SeleccionarRegistro(gvParticipante.SelectedRow);
        }
    #endregion
}