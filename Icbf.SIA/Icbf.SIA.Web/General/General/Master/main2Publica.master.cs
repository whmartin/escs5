﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using System.Web.Security;
using Icbf.Seguridad.Service;


public delegate void ToolBarDelegatePublic(object sender, EventArgs e);

/// <summary>
/// Master page de las transacciones en SIA
/// </summary>
public partial class masterPrincipalPublica : System.Web.UI.MasterPage
{
    public enum Botones
    {
        Guardar,
        Reporte,
        Eliminar,
        Editar
    }

    public event ToolBarDelegatePublic eventoGuardar;
    ToolBarDelegatePublic guardarToolbar;

    public event ToolBarDelegatePublic eventoBuscar;
    ToolBarDelegatePublic buscarToolbar;

    public event ToolBarDelegatePublic eventoDuplicar;
    ToolBarDelegatePublic duplicarToolbar;

    public event ToolBarDelegatePublic eventoBuscarTercero;
    ToolBarDelegatePublic buscarToolbarTercero;

    public event ToolBarDelegatePublic eventoListar;
    ToolBarDelegatePublic listarToolbar;

    public event ToolBarDelegatePublic eventoNuevo;
    ToolBarDelegatePublic nuevoToolbar;

    public event ToolBarDelegatePublic eventoEliminar;
    ToolBarDelegatePublic eliminarToolbar;

    public event ToolBarDelegatePublic eventoEditar;
    ToolBarDelegatePublic editarToolbar;

    public event ToolBarDelegatePublic eventoAprobar;
    ToolBarDelegatePublic aprobarToolbar;

    public event ToolBarDelegatePublic eventoAdjuntar;
    ToolBarDelegatePublic adjuntarToolbar;

    public event ToolBarDelegatePublic eventoCambiarOpcion;
    ToolBarDelegatePublic cambiarOpcionToolbar;

    public event ToolBarDelegatePublic eventoExcel;
    ToolBarDelegatePublic excelToolbar;

    public event ToolBarDelegatePublic eventoReporte;
    ToolBarDelegatePublic ReporteToolbar;

    public event ToolBarDelegatePublic eventoRetornar;
    ToolBarDelegatePublic RetornarToolbar;

    public event ToolBarDelegatePublic eventoLimpiar;
    ToolBarDelegatePublic LimpiarToolbar;

    public event ToolBarDelegatePublic eventoBuscarModificacion;
    ToolBarDelegatePublic BuscarModificacionToolbar;

    public event ToolBarDelegatePublic eventoRechazar;
    ToolBarDelegatePublic eventoRechazarToolbar;

    #region eventos
    /// <summary>
    /// Manejador del evento PreInit de la Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {

    }

    public void VerBtnEditar(bool estado)
    {
        btnEditar.Visible = estado;
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        guardarToolbar = eventoGuardar;
        buscarToolbar = eventoBuscar;
        buscarToolbarTercero = eventoBuscarTercero;
        listarToolbar = eventoListar;
        nuevoToolbar = eventoNuevo;
        eliminarToolbar = eventoEliminar;
        editarToolbar = eventoEditar;
        aprobarToolbar = eventoAprobar;
        adjuntarToolbar = eventoAdjuntar;
        cambiarOpcionToolbar = eventoCambiarOpcion;
        excelToolbar = eventoExcel;
        ReporteToolbar = eventoReporte;
        RetornarToolbar = eventoRetornar;
        duplicarToolbar = eventoDuplicar;
        LimpiarToolbar = eventoLimpiar;
        BuscarModificacionToolbar = eventoBuscarModificacion;
        eventoRechazarToolbar = eventoRechazar;

        if (guardarToolbar == null)
            btnGuardar.Visible = false;
        if (buscarToolbar == null)
            btnBuscar.Visible = false;
        if (nuevoToolbar == null)
            btnNuevo.Visible = false;
        if (eliminarToolbar == null)
            btnEliminar.Visible = false;
        if (editarToolbar == null)
            btnEditar.Visible = false;
        if (aprobarToolbar == null)
            btnAprobar.Visible = false;
        if (cambiarOpcionToolbar == null)
            ddlExtends.Visible = false;
        if (RetornarToolbar == null)
            btnRetornar.Visible = false;
        if (eventoRechazarToolbar == null)
            btnRechazar.Visible = false;

        GetVersion();
    }

    /// <summary>
    /// Manejador del evento click para el botón Retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (RetornarToolbar != null)
            RetornarToolbar(sender, e);
    }

    /// <summary>
    /// Manejador del evento Click para el botón Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        if (nuevoToolbar != null)
            nuevoToolbar(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        if (editarToolbar != null)
            editarToolbar(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (guardarToolbar != null)
            guardarToolbar(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (eventoBuscar != null)
            eventoBuscar(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Duplicar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDuplicar_Click(object sender, EventArgs e)
    {
        if (eventoDuplicar != null)
            eventoDuplicar(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Buscar Tercero
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnBuscarTercerroClick(object sender, EventArgs e)
    {
        if (eventoBuscarTercero != null)
            eventoBuscarTercero(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (eliminarToolbar != null)
            eliminarToolbar(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        if (eventoExcel != null)
            eventoExcel(sender, e);
    }

    /// <summary>
    /// Manejador del evento click para el botón Reporte
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReporte_Click(object sender, EventArgs e)
    {
        if (eventoReporte != null)
            eventoReporte(sender, e);
    }
    /// <summary>
    /// Evento Rechazar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        if (eventoRechazarToolbar != null)
            eventoRechazar(sender, e);
    }
    /// <summary>
    /// Manejador del evento ìndice cambia para el control lista desplegable
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlExtends_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cambiarOpcionToolbar != null)
            cambiarOpcionToolbar(sender, e);
    }

    protected void btnMenu_Click(object sender, MenuEventArgs e)
    {

    }

    protected void hfValorMenu_ValueChanged(object sender, EventArgs e)
    {

    }

    #endregion

    /// <summary>
    /// Establece tìtulos e imagen en pantalla
    /// </summary>
    /// <param name="pNombrePagina"></param>
    /// <param name="Pagina"></param>
    public void EstablecerTitulos(String pNombrePagina, String Pagina)
    {
        lblTitulo.Text = pNombrePagina;

        switch (Pagina)
        {
            case "List":
                imgPrograma.ImageUrl = "~/Image/main/prog-list.png";
                break;
            case "Detail":
                imgPrograma.ImageUrl = "~/Image/main/prog-info.png";
                break;
            case "Add":
                imgPrograma.ImageUrl = "~/Image/main/prog-edit.png";
                break;
        }
    }

    /// <summary>
    /// Estable tìtulos
    /// </summary>
    /// <param name="pNombrePagina"></param>
    public void EstablecerTitulos(String pNombrePagina)
    {
        lblTitulo.Text = pNombrePagina;

        imgPrograma.ImageUrl = "~/Image/main/prog-edit.png";
    }

    /// <summary>
    /// Mostrar mensaje de error en pantalla
    /// </summary>
    /// <param name="pMensaje"></param>
    public void MostrarMensajeError(String pMensaje)
    {
        lblError.CssClass = "lbE";
        lblError.Visible = true;
        lblError.Text = pMensaje;
    }
    /// <summary>
    /// Mostrar mensaje de guardado en pantalla
    /// </summary>
    public void MostrarMensajeGuardado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "La información ha sido guardada exitosamente.";
    }
    /// <summary>
    /// Mostrar mensaje de eliminado en pantalla
    /// </summary>
    public void MostrarMensajeEliminado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "El registro ha sido eliminado correctamente.";
    }
    /// <summary>
    /// Mostrar mensaje de modificado en pantalla
    /// </summary>
    public void MostrarMensajeModificado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "La información ha sido modificada exitosamente";
    }
    /// <summary>
    /// Mostrar mensaje de guardado en pantalla
    /// </summary>
    /// <param name="pMensaje"></param>
    public void MostrarMensajeGuardado(String pMensaje)
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = pMensaje;
    }
    /// <summary>
    /// Limpiar mensaje de error
    /// </summary>
    public void LipiarMensajeError()
    {
        lblError.Visible = false;
        lblError.Text = "";
    }

    public void ConfirmarAccion(string button, string message)
    {
        //btnGuardar.OnClientClick = "return confirm('Está seguro que desea guardar la información?')";
        if (((LinkButton)(this.FindControl(button))) != null)
        {
            ((LinkButton)(this.FindControl(button))).OnClientClick = "return confirm('" + message + "')";
        }

    }

    public void SetSaveConfirmation(string pFunction)
    {
        btnGuardar.OnClientClick = pFunction;
    }
    public void RemoveSaveConfirmation()
    {
        btnGuardar.OnClientClick = "";
    }
    public void SetAprobarConfirmation(string pFunction)
    {
        btnAprobar.OnClientClick = pFunction;
    }

    public void SetValidationGroupBoton(Botones boton, string funcion)
    {
        switch (boton)
        {
            case Botones.Guardar:
                btnGuardar.ValidationGroup = funcion;
                break;
            case Botones.Eliminar:
                btnEliminar.ValidationGroup = funcion;
                break;
            case Botones.Editar:
                btnEditar.ValidationGroup = funcion;
                break;
            default:
                break;
        }
    }

    private void GetVersion()
    {
        //this.LblVersion.Text = " Versión: " + ApplicationInformation.ExecutingAssemblyVersion.ToString() + "" +
        //                       " - " + ApplicationInformation.CompileDate.ToString();
        //this.LblVersion.ToolTip = ApplicationInformation.CompileDate.ToString();
    }

    public void SetGenerarReporteConfrimation(string pFunction)
    {
    }
    public void RemoveAprobarConfirmation()
    {
        btnAprobar.OnClientClick = string.Empty;
    }
    public void MostrarBotonAprobar(bool mostrar)
    {
        btnAprobar.Visible = mostrar;   
    }
    public void MostrarBotonNuevo(bool mostrar)
    {
        btnNuevo.Visible = mostrar;
    }
    public void MostrarBotonEditar(bool mostrar)
    {
        btnEditar.Visible = mostrar;
    }
    public void MostrarBotonConsultar()
    {
        btnBuscar.Visible = true;
    }
    public void OcultarBotonGuardar(bool ocultar)
    {
        btnGuardar.Visible = ocultar;
    }
    public void OcultarBotonEliminar(bool ocultar)
    {
        btnGuardar.Visible = ocultar;
    }
    public void OcultarBotonEditar(bool ocultar)
    {
        btnGuardar.Visible = ocultar;
    }
    public void OcultarBotonBuscar(bool ocultar)
    {
        btnBuscar.Visible = ocultar;
    }
    public void OcultarBotonNuevo(bool ocultar)
    {
        btnNuevo.Visible = !ocultar;
    }
    public void MostrarBotonEliminar(bool ocultar)
    {
        btnEliminar.Visible = ocultar;
    }
    public void OcultarBotonRechazar(bool ocultar)
    {
        btnRechazar.Visible = !ocultar;
    }
    public void SetRechazarConfirmation(string pFunction)
    {
        btnRechazar.OnClientClick = pFunction;
    }

    /// <summary>
    /// Manejado de evento click para el botón Aprobar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        if (aprobarToolbar != null)
            aprobarToolbar(sender, e);
    }
    /// <summary>
    /// Manejado de evento click para el botón Adjuntar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdjuntar_Click(object sender, EventArgs e)
    {
        if (adjuntarToolbar != null)
            adjuntarToolbar(sender, e);
    }
    /// <summary>
    /// Limpiar una lista desplegable
    /// </summary>
    public void LimpiarOpcionesAdicionales()
    {
        ddlExtends.Items.Clear();
        ddlExtends.Items.Add(new ListItem("Ir a..", "-1"));
    }
    /// <summary>
    /// Agregar opción adicional a una lista desplegable
    /// </summary>
    /// <param name="pItem"></param>
    public void AgregarOpcionAdicional(ListItem pItem)
    {
        ddlExtends.Items.Add(pItem);
    }
    public void SeleccionarOpcion(String pOpcion)
    {
        ddlExtends.SelectedValue = pOpcion;
    }

    protected void btnLimpiarPantalla_Click(object sender, EventArgs e)
    {
        if (LimpiarToolbar != null)
            LimpiarToolbar(sender, e);
    }
    protected void btnBuscarSolicitud_Click(object sender, EventArgs e)
    {
        if (BuscarModificacionToolbar != null)
            BuscarModificacionToolbar(sender, e);
    }
}
