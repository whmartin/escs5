﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterPrincipal.aspx.cs"
    Inherits="General_General_Master_MasterPrincipal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../../Styles/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <link href="../../../Styles/layout-default.css" rel="stylesheet" type="text/css" />
    <link href="../../../Styles/global.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <%--this is the center frame where all the pages will be displayed--%>
    <iframe id="Iframe1" name="center" class="ui-layout-center" noresize="true" overflow="scroll"
        src="../../../Page/General/Inicio/Default.aspx" frameborder="0" marginheight="0"
        marginwidth="0" runat="server"></iframe>
    <div class="ui-layout-north">
        <table cellpadding="0" cellspacing="0" id="CabeceraPrincipal">
            <tr>
                <td id="hL">
                    <img src="../../../Image/main_2/lg.png" alt="Instituto Colombiano de Bienestar Familiar" />
                </td>
                <td>
                    <asp:Image ID="ImgLogo" runat="server" ImageUrl="~/Image/main/Logo_Cuentame.jpg">
                    </asp:Image>
                </td>
                <td id="hI">
                    <asp:Label runat="server" ID="lblNombreUsuario" CssClass="hIt"></asp:Label>
                    <asp:Label runat="server" ID="lblSucursal"></asp:Label>
                    <asp:Label runat="server" ID="lblRol"></asp:Label>
                    <asp:Label runat="server" ID="lblFecha"></asp:Label>
                </td>
                <td id="hB">
                    <asp:LinkButton runat="server" ID="btnHome" OnClick="btnHome_Click1">
                        <a href="../../../Page/General/Inicio/Default.aspx" target="center">
                            <img src="../../../Image/btn/home.png" width="23px" height="23px" alt="Inicio" title="Inicio"/>        
                        </a>
                    
                    </asp:LinkButton>
                    <asp:LinkButton runat="server" ID="btnHelp">
                                    <img src="../../../Image/btn/help.png" width="23px" height="23px" alt="Ayuda" title="Ayuda"/>
                    </asp:LinkButton>
                    <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutAction="Redirect" LoginText="Logout"
                        LogoutText="Cerrar Sesión" LogoutPageUrl="~/Default.aspx" LogoutImageUrl="~/Image/btn/logout.png"
                        Width="23px" Height="23px" ToolTip="Cerrar Sesión" OnLoggedOut="LoginStatus1_LoggedOut" />
                </td>
            </tr>
        </table>
    </div>
    <div class="ui-layout-west">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </div>
    <div class="ui-layout-south">
        <div id="piePrincipal">
            <table cellpadding="0" cellspacing="0" id="pT">
                <tr>
                    <td id="pTa">
                        <a href="javascript:void(0);OpenAudit();">
                            <asp:Label runat="server" ID="lblAuditoria"></asp:Label>
                        </a>
                    </td>
                    <td>
                        <span>Instituto Colombiano de Bienestar Familiar . &copy; Todos los derechos reservados
                            . Colombia .
                            <%: DateTime.Now.Year %>
                            -- </span>
                        <asp:Label ID="LblVersion" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
    <div id="DialogoMaster">
    </div>
    <!-- Llaves agregadas prueba popup -->
    <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="../../../Scripts/Dialog.js" type="text/javascript"></script>
    <!-- Llaves agregadas prueba popup -->
    <script src="../../../Scripts/jq.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jq_Layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            // Propiedades del panel Jquey----------------
            $('body').layout({
                south__size: 30,
                south__slidable: false,
                north__size: 75,
                west__size: 231,
                west__slidable: false,
                north__slidable: false,
                north__togglerLength_closed: "100%",
                north__spacing_closed: 10,
                north__togglerTip_open: "Ocultar",
                north__togglerTip_closed: "Mostrar",
                west__togglerTip_open: "Ocultar",
                west__togglerTip_closed: "Mostrar",
                west__togglerClass: "toggler",
                north__togglerClass: "toggler",
                west__resizerClass: "resizer",
                north__resizerClass: "resizer",
                south__resizable: false,
                south__spacing_open: 0,
                south__spacing_closed: 20,

                north__resizable: false,
                north__spacing_open: 0,
                north__spacing_closed: 20


            });
            //END: Propiedades del panel Jquey----------------   

            $('#ulMenuPrincipal > li > ul').eq(0).addClass('active').slideDown('normal');

            $('.menu1').click(function (event) {
                var elem = $(this).next();
                if (elem.is('ul')) {
                    event.preventDefault();
                    $('#ulMenuPrincipal ul:visible').not(elem).slideUp();
                    elem.slideToggle();
                }
            });

            $('#ulMenuPrincipal li ul li a').click(function (event) {
                var elem = $(this).next();
                if (elem.is('ul')) {
                    event.preventDefault();
                    var elemhijo = $(this).children();
                    elemhijo.not(elem).slideUp();
                    elem.slideToggle();
                }

            });
        });
    </script>
</body>
</html>
