﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public delegate void ToolBarDelegateLupa2(object sender, EventArgs e);
/// <summary>
/// Master Page para los documentos de módulo financiero
/// </summary>
public partial class General_General_Master_main32 : System.Web.UI.MasterPage
{
    public event ToolBarDelegateLupa2 eventoBuscar;
    ToolBarDelegateLupa2 buscarToolbar;

    /// <summary>
    /// Manejador del evento Cargar Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        buscarToolbar = eventoBuscar;
        if (buscarToolbar == null)
            btnBuscar.Visible = false;
    }

    /// <summary>
    /// Manejador de evento click para el botón Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (eventoBuscar != null)
            eventoBuscar(sender, e);
    }

    /// <summary>
    /// Establece tìtulos en pantalla
    /// </summary>
    /// <param name="pNombrePagina"></param>
    /// <param name="Pagina"></param>
    public void EstablecerTitulos(String pNombrePagina, String Pagina)
    {
        lblTitulo.Text = pNombrePagina;

        switch (Pagina)
        {
            case "List":
                imgPrograma.ImageUrl = "~/Image/main/prog-list.png";
                break;
            case "Detail":
                imgPrograma.ImageUrl = "~/Image/main/prog-info.png";
                break;
            case "Add":
                imgPrograma.ImageUrl = "~/Image/main/prog-edit.png";
                break;
        }
    }

    /// <summary>
    /// Establece tìtulos en pantalla
    /// </summary>
    /// <param name="pNombrePagina"></param>
    public void EstablecerTitulos(String pNombrePagina)
    {
        lblTitulo.Text = pNombrePagina;
        imgPrograma.ImageUrl = "~/Image/main/prog-edit.png";
    }

    /// <summary>
    /// Muestra emnsaje de error en pantalla
    /// </summary>
    /// <param name="pMensaje"></param>
    public void MostrarMensajeError(String pMensaje)
    {
        lblError.Visible = true;
        lblError.Text = pMensaje;
    }
}
