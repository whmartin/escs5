﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public delegate void ToolBarDelegateLupa(object sender, EventArgs e);

/// <summary>
/// Master Page usado en las ventanas emergentes de las lupas de búsqueda detallada de campo
/// </summary>
public partial class General_General_Master_Lupa : System.Web.UI.MasterPage
{
    public event ToolBarDelegateLupa eventoGuardar;
    ToolBarDelegateLupa guardarToolbar;
    public event ToolBarDelegateLupa eventoBuscar;
    ToolBarDelegateLupa buscarToolbar;
    public event ToolBarDelegateLupa eventoNuevo;
    ToolBarDelegateLupa nuevoToolbar;
    public event ToolBarDelegateLupa eventoEliminar;
    ToolBarDelegateLupa eliminarToolbar;
    public event ToolBarDelegateLupa eventoEditar;
    ToolBarDelegateLupa editarToolbar;
    public event ToolBarDelegateLupa eventoRetornar;
    ToolBarDelegateLupa retornarToolbar;
    public event ToolBarDelegateLupa eventoLimpiar;
    ToolBarDelegateLupa LimpiarToolbar;

    /// <summary>
    /// Manejador del evento para la Carga de la Pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        nuevoToolbar = eventoNuevo;
        guardarToolbar = eventoGuardar;
        buscarToolbar = eventoBuscar;
        eliminarToolbar = eventoEliminar;
        editarToolbar = eventoEditar;
        retornarToolbar = eventoRetornar;
        LimpiarToolbar = eventoLimpiar;
        if (nuevoToolbar == null)
            btnNuevo.Visible = false;
        if (guardarToolbar == null)
            btnGuardar.Visible = false;
        if (buscarToolbar == null)
            btnBuscar.Visible = false;
        if (editarToolbar == null)
            btnEditar.Visible = false;
        if (eliminarToolbar == null)
            btnEliminar.Visible = false;
        if (retornarToolbar == null)
            btnRetornar.Visible = false;
        if (LimpiarToolbar == null)
            btnLimpiarPantalla.Visible = false;
    }
    /// <summary>
    /// Manejador del evento click para el botón Guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (guardarToolbar != null)
            guardarToolbar(sender, e);
    }

    /// <summary>
    /// Manejados del evento del botón Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (eventoBuscar != null)
            eventoBuscar(sender, e);
    }
    /// <summary>
    /// Mostrar mensaje de guardado en pantalla
    /// </summary>
    public void MostrarMensajeGuardado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "La información ha sido registrada exitosamente.";
    }

    /// <summary>
    /// Mostrar mensaje de guardado en pantalla
    /// </summary>
    public void MostrarMensajeEliminado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "El registro ha sido eliminado correctamente.";
    }


    /// <summary>
    /// Configura la acción a llevar a cabo en el evento OnClientClick del botón
    /// buscar
    /// </summary>
    /// <param name="pFunction"></param>
    public void SetBtnBuscarOnClientClick(string pFunction)
    {
        btnBuscar.OnClientClick = pFunction;
    }
    /// <summary>
    /// Elimina la acción a llevar a cabo en el evento OnClientClick del botón
    /// buscar
    /// </summary>
    public void RemoveBtnBuscarOnClientClick()
    {
        btnBuscar.OnClientClick = string.Empty;
    }

    /// <summary>
    /// Configura la acción a llevar a cabo en el evento OnClientClick del botón
    /// buscar
    /// </summary>
    /// <param name="pFunction"></param>
    public void SetBtnGuardarOnClientClick(string pFunction)
    {
        btnGuardar.OnClientClick = pFunction;
    }
    /// <summary>
    /// Elimina la acción a llevar a cabo en el evento OnClientClick del botón
    /// buscar
    /// </summary>
    public void RemoveBtnGuardarOnClientClick()
    {
        btnGuardar.OnClientClick = string.Empty;
    }

    /// <summary>
    /// Limpiar mensaje de error
    /// </summary>
    public void LipiarMensajeError()
    {
        lblError.Visible = false;
        lblError.Text = "";
    }

    /// <summary>
    /// Establece tìtulos
    /// </summary>
    /// <param name="pNombrePagina">Nombre de la pàgina</param>
    /// <param name="Pagina">Pàgina</param>

    public void EstablecerTitulos(String pNombrePagina, String Pagina)
    {
        lblTitulo.Text = pNombrePagina;

        switch (Pagina)
        {
            case "List":
                imgPrograma.ImageUrl = "~/Image/main/prog-list.png";
                break;
            case "Detail":
                imgPrograma.ImageUrl = "~/Image/main/prog-info.png";
                break;
            case "Add":
                imgPrograma.ImageUrl = "~/Image/main/prog-edit.png";
                break;
        }
    }
    /// <summary>
    /// Establece HabilitarBotonGuardar
    /// </summary>
    /// <param name="pHabilitar">Booleano con el estado de la petición</param>
    
    public void HabilitarBotonGuardar(Boolean pHabilitar)
    {
        btnGuardar.Enabled = pHabilitar;
    }

    /// <summary>
    /// Establece OcultarBotonBuscar
    /// </summary>
    /// <param name="pHabilitar">Booleano con el estado de la petición</param>
    
    public void OcultarBotonBuscar(Boolean pHabilitar)
    {
        btnBuscar.Visible = !pHabilitar;
    }

    /// <summary>
    /// Establece OcultarBotonGuardar
    /// </summary>
    /// <param name="pHabilitar">Booleano con el estado de la petición</param>

    public void OcultarBotonGuardar(Boolean pHabilitar)
    {
        btnGuardar.Visible = !pHabilitar;
    }

    /// <summary>
    /// Establece OcultarBotonNuevo
    /// </summary>
    /// <param name="pHabilitar">Booleano con el estado de la petición</param>

    public void OcultarBotonNuevo(Boolean pHabilitar)
    {
        btnNuevo.Visible = !pHabilitar;
    }

    /// <summary>
    /// Establece t`titulos
    /// </summary>
    /// <param name="pNombrePagina"></param>
    public void EstablecerTitulos(String pNombrePagina)
    {
        lblTitulo.Text = pNombrePagina;
        imgPrograma.ImageUrl = "~/Image/main/prog-edit.png";
    }
    public void MostrarMensajeError(String pMensaje)
    {
        lblError.CssClass = "lbE";
        lblError.Visible = true;
        lblError.Text = pMensaje;
    }

    public void MostrarMensajeError(Dictionary<string, string> pMensaje)
    {
        StringBuilder texto = new StringBuilder();

        foreach (var item in pMensaje)
            texto.AppendLine(item.Key + " - " + item.Value);

        lblError.CssClass = "lbE";
        lblError.Visible = true;
        lblError.Text = texto.ToString().Replace("\n", "<br/>");
    }
    public void QuitarMensajeError()
    {
        lblError.Visible = false;
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        if (nuevoToolbar != null)
            nuevoToolbar(sender, e);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (eliminarToolbar != null)
            eliminarToolbar(sender, e);
        
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        if (editarToolbar != null)
            editarToolbar(sender, e);
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (retornarToolbar != null)
            retornarToolbar(sender, e);
    }
    protected void btnLimpiarPantalla_Click(object sender, EventArgs e)
    {
        if (LimpiarToolbar != null)
            LimpiarToolbar(sender, e);
    }

    public string GetClientIdGuardar()
    {
        return btnGuardar.ClientID;
    }
}
