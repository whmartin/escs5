﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Presentation;
using System.Web.Security;
using Icbf.SIA.Service;

/// <summary>
/// Página usada como contenedora del sitio general de SIA, carga el menú de la aplicación.
/// </summary>
public partial class General_General_Master_MasterPrincipal : System.Web.UI.Page
{
    #region "Funciones"
    private List<Programa> LoadHijos(int pIdModulo, String[] pNombreRol, int? idPadre, HtmlGenericControl programa)
    {
        SeguridadService vSeguridad = new SeguridadService();
        List<Programa> vProgramas = vSeguridad.ConsultarProgramasHijosPermitidosPermiso(pIdModulo, pNombreRol, idPadre);

        HtmlGenericControl etulsubmenu = new HtmlGenericControl("ul");
        etulsubmenu.Attributes.Add("class", "EstiloMenuUl");
        /*Recorremos los hijos par agregrcelos a los programas*/
        foreach (Programa vProgramaHijo in vProgramas)
        {
            HtmlGenericControl etlisubmenu = new HtmlGenericControl("li");
            //etulsubmenu.Attributes.Add("class", "submenu");

            HtmlGenericControl etasubmenu = new HtmlGenericControl("a");
            etasubmenu.Attributes.Add("class", "EstiloMenuUla");
            string nombreMenu = "";
            if (vProgramaHijo.NombrePrograma.Length <= 3)
            {
                nombreMenu = vProgramaHijo.NombrePrograma.ToUpper();
            }
            else
            {
                char PrimeraLetra = vProgramaHijo.NombrePrograma[0];
                nombreMenu = PrimeraLetra.ToString().ToUpper() + vProgramaHijo.NombrePrograma.Substring(1).ToLower();
            }


            etasubmenu.InnerText = nombreMenu;
            etlisubmenu.Controls.Add(etasubmenu);
            etulsubmenu.Controls.Add(etlisubmenu);

            List<Programa> vProgramas2 = LoadHijos(vProgramaHijo.IdModulo, pNombreRol, vProgramaHijo.IdPrograma, etlisubmenu);

            if (vProgramas2.Count == 0)
            {


                etasubmenu.Attributes.Add("target", "center");

                if (vProgramaHijo.EsReporte == 1)
                {
                    etasubmenu.Attributes.Add("href", ResolveUrl("~/Page/Reportes/TransversalReportes/List.aspx?oRp=" + vProgramaHijo.IdPrograma.ToString()));
                }
                else
                {
                    etasubmenu.Attributes.Add("href", ResolveUrl("~/Page/" + vProgramaHijo.CodigoPrograma + "/List.aspx"));
                }

            }
            else
            {
                etasubmenu.Attributes.Add("class", "desplegable");
            }
            programa.Controls.Add(etulsubmenu);
        }
        return vProgramas;
    }

    public void CambiarImagen()
    {
        ImgLogo.ImageUrl = "~/Image/main/Logo_Cuentame.jpg";

        string App = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
        if (App == "Denuncias")
            ImgLogo.ImageUrl = "~/Image/main/LogoDenuncias.png";
        if (App == "Proveedores")
            ImgLogo.ImageUrl = "~/Image/main/LogoProveedores.jpg";

    }

    public void SetMenu()
    {
        int vPestanaAbierta = 0;
        if (Session["MenuId"] != null)
            Int32.TryParse(Session["MenuId"].ToString(), out vPestanaAbierta);


        GeneralWeb vGeneralWeb = new GeneralWeb();
        if (vGeneralWeb.GetSessionMod() == null)
        {
            SeguridadService vSeguridad = new SeguridadService();
            Usuario vUsuario = vGeneralWeb.GetSessionUser();
            string[] vRoles = Roles.GetRolesForUser(vUsuario.NombreUsuario);
            List<Modulo> vModulos = vSeguridad.ConsultarPermisosRol(vRoles);
            vGeneralWeb.SetSessionMod(vModulos);


            HtmlGenericControl etulPrincipal = new HtmlGenericControl("ul");
            etulPrincipal.ID = "ulMenuPrincipal";
            etulPrincipal.Attributes.Add("class", "EstiloMenuUl");
            foreach (Modulo vModulo in vModulos)
            {
                HtmlGenericControl etlimenu = new HtmlGenericControl("li");
                HtmlGenericControl etamenu = new HtmlGenericControl("a");
                etamenu.Attributes.Add("class", "menu1 EstiloMenuUla");

                etamenu.InnerText = vModulo.NombreModulo.ToUpper();
                etlimenu.Controls.Add(etamenu);

                LoadHijos(vModulo.IdModulo, vRoles, null, etlimenu);

                etulPrincipal.Controls.Add(etlimenu);

            }
            PlaceHolder1.Controls.Add(etulPrincipal);
        }
        else
        {

            SeguridadService vSeguridad = new SeguridadService();
            Usuario vUsuario = vGeneralWeb.GetSessionUser();
            string[] vRoles = Roles.GetRolesForUser(vUsuario.NombreUsuario);
            List<Modulo> vModulos = vGeneralWeb.GetSessionMod();

            HtmlGenericControl etulPrincipal = new HtmlGenericControl("ul");
            etulPrincipal.Attributes.Add("class", "EstiloMenuUl");
            etulPrincipal.ID = "ulMenuPrincipal";

            foreach (Modulo vModulo in vModulos)
            {
                HtmlGenericControl etlimenu = new HtmlGenericControl("li");
                HtmlGenericControl etamenu = new HtmlGenericControl("a");
                etamenu.Attributes.Add("class", "menu1 EstiloMenuUla");
                etamenu.InnerText = vModulo.NombreModulo.ToUpper();
                etlimenu.Controls.Add(etamenu);

                LoadHijos(vModulo.IdModulo, vRoles, null, etlimenu);

                etulPrincipal.Controls.Add(etlimenu);

            }
            PlaceHolder1.Controls.Add(etulPrincipal);
        }
    }
    public void SetMenu(int pPosicion)
    {
        GeneralWeb vGeneralWeb = new GeneralWeb();
        if (vGeneralWeb.GetSessionMod() == null)
        {
            SeguridadService vSeguridad = new SeguridadService();
            Usuario vUsuario = vGeneralWeb.GetSessionUser();
            string[] vRoles = Roles.GetRolesForUser(vUsuario.NombreUsuario);
            List<Modulo> vModulos = vSeguridad.ConsultarPermisosRol(vRoles);
            vGeneralWeb.SetSessionMod(vModulos);

            HtmlGenericControl etulPrincipal = new HtmlGenericControl("ul");
            etulPrincipal.ID = "ulMenuPrincipal";
            etulPrincipal.Attributes.Add("class", "EstiloMenuUl");
            foreach (Modulo vModulo in vModulos)
            {
                HtmlGenericControl etlimenu = new HtmlGenericControl("li");
                HtmlGenericControl etamenu = new HtmlGenericControl("a");
                etamenu.Attributes.Add("class", "menu1 EstiloMenuUla");

                etamenu.InnerText = vModulo.NombreModulo;
                etlimenu.Controls.Add(etamenu);

                LoadHijos(vModulo.IdModulo, vRoles, null, etlimenu);


                etulPrincipal.Controls.Add(etlimenu);

            }
            PlaceHolder1.Controls.Add(etulPrincipal);

        }
        else
        {
            SeguridadService vSeguridad = new SeguridadService();
            Usuario vUsuario = vGeneralWeb.GetSessionUser();
            string[] vRoles = Roles.GetRolesForUser(vUsuario.NombreUsuario);
            List<Modulo> vModulos = vGeneralWeb.GetSessionMod();

            HtmlGenericControl etulPrincipal = new HtmlGenericControl("ul");
            etulPrincipal.Attributes.Add("class", "EstiloMenuUl");
            etulPrincipal.ID = "ulMenuPrincipal";

            foreach (Modulo vModulo in vModulos)
            {
                HtmlGenericControl etlimenu = new HtmlGenericControl("li");
                HtmlGenericControl etamenu = new HtmlGenericControl("a");
                etamenu.Attributes.Add("class", "menu1 EstiloMenuUla");
                etamenu.InnerText = vModulo.NombreModulo;
                etlimenu.Controls.Add(etamenu);

                LoadHijos(vModulo.IdModulo, vRoles, null, etlimenu);

                etulPrincipal.Controls.Add(etlimenu);

            }
            PlaceHolder1.Controls.Add(etulPrincipal);

        }
    }
    public void EstablecerTitulos()
    {
        lblFecha.Text = DateTime.Now.ToShortDateString();
        lblNombreUsuario.Text = new GeneralWeb().GetSessionUser().NombreUsuario;
        //lblRol.Text = new GeneralWeb().GetSessionUser().Rol;
    }

    public void EstablecerTitulos(String pNombrePagina)
    {
        lblFecha.Text = DateTime.Now.ToShortDateString();
        lblNombreUsuario.Text = new GeneralWeb().GetSessionUser().NombreUsuario;        
        //lblRol.Text = new GeneralWeb().GetSessionUser().Rol;

    }

    private void GetVersion()
    {
        this.LblVersion.Text = " Versión: " + ApplicationInformation.ExecutingAssemblyVersion.ToString() + "" +
                               " - " + ApplicationInformation.CompileDate.ToString();
        this.LblVersion.ToolTip = ApplicationInformation.CompileDate.ToString();
    }
    #endregion

    #region"Eventos"
    /// <summary>
    /// Manejador del evento Cargar para la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        CambiarImagen();
        SetMenu();
        EstablecerTitulos();
        GetVersion();
    }

    protected void btnHome_Click(object sender, EventArgs e)
    {

    }

    protected void btnHome_Click1(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Manejador del evento salir para el control login de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {

        string App = "";
        MembershipUser membershipUser = Membership.GetUser();

        if (membershipUser != null)
        {
            string[] vRoles = Roles.GetRolesForUser(membershipUser.UserName);
            App = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
        }

        Session.Clear();
        Session.Abandon();

        Response.Redirect("~/Default" + App + ".aspx");
    }
    #endregion

}