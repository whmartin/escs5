﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Master Page de la parte externa del sitio de Proveedores
/// </summary>
public partial class MasterProveedores : System.Web.UI.MasterPage
{
    /// <summary>
    /// Mostrar mensaje de error en pantalla
    /// </summary>
    /// <param name="pMensaje"></param>
    public void MostrarMensajeError(String pMensaje)
    {
        lblError.CssClass = "lbE";
        lblError.Visible = true;
        lblError.Text = pMensaje;
    }
    /// <summary>
    /// Mostrar mensaje de guardado en pantalla
    /// </summary>
    public void MostrarMensajeGuardado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "La información ha sido guardada.";
    }
    /// <summary>
    /// Mostrar mensaje de Eliminado en pantalla
    /// </summary>
    public void MostrarMensajeEliminado()
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = "La información ha sido eliminada.";
    }
    /// <summary>
    /// Mostrar mensaje de Guardado en pantalla
    /// </summary>
    /// <param name="pMensaje"></param>
    public void MostrarMensajeGuardado(String pMensaje)
    {
        lblError.CssClass = "lbEM";
        lblError.Visible = true;
        lblError.Text = pMensaje;
    }
    /// <summary>
    /// Limpia de pantalla mensaje de error
    /// </summary>
    public void LipiarMensajeError()
    {
        lblError.Visible = false;
        lblError.Text = "";
    }
}
