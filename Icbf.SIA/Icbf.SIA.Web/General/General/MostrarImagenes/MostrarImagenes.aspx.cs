﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Xml;
using System.IO;

/// <summary>
/// Página genérica para la descarga de imágenes del FTP
/// </summary>
public partial class General_General_MostrarImagenes : GeneralWeb
{
    /// <summary>
    /// Manejdaor del evento cargar la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Iniciar();
        }

    }

    /// <summary>
    /// Descarga archico especificado, en copncreto una imagen
    /// </summary>
    private void Iniciar()
    {

        if (GetSessionParameter("Imagenes") != null)
        {
            var Imagen = (Byte[])GetSessionParameter("Imagenes");
            RemoveSessionParameter("Imagenes");
            var stream = new MemoryStream(Imagen);
            this.Response.Clear();
            this.Response.ContentType = "image/jpg";
            this.Response.BinaryWrite(stream.ToArray());
            this.Response.Flush();
            this.Response.Close();
            this.Response.End();
        }


    }


}