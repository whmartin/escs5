﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Xml;
using Microsoft.Reporting.WebForms;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using System.IO;

/// <summary>
/// Página que despliega los reportes de la aplicación
/// </summary>
public partial class General_General_ReportViewer : GeneralWeb
{
    masterPrincipal toolBar;
    Report objReporte;
    string strPreviousPage;
    ContratoService vContratoService = new ContratoService();

    #region eventos
    /// <summary>
    /// Manejdaor del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["ReportName"] == null) { Iniciar(); }
    }

    /// <summary>
    /// Manejdaor del evento cargar la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["ReportName"] == null)
        {
            toolBar.LipiarMensajeError();
        }
        ValidateSession();

        if (!Page.IsPostBack)
        {
            LoadReport();
        }
    }

    /// <summary>
    /// manejador del evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        //Set null to the report session         
        RemoveSessionParameter("Report");
        NavigateTo("~/Page/" + strPreviousPage + "/List.aspx", false);
    }

    #endregion

    #region mètodos

    /// <summary>
    /// Carga el nombre del reporte
    /// </summary>
    /// <param name="ReportName"></param>
    /// <returns></returns>
    private static string LoadReportName(string ReportName)
    {
        string ReportPath = string.Empty;

        XmlDocument xDoc = new XmlDocument();

        xDoc.Load(HttpContext.Current.Server.MapPath("~/General/General/Report/Reports.xml"));

        XmlNodeList Reportes = xDoc.GetElementsByTagName("Reportes");

        XmlNodeList lista = ((XmlElement)Reportes[0]).GetElementsByTagName("reporte");

        foreach (XmlElement reporte in lista)
        {
            if (reporte.GetAttribute("nombre") == ReportName)
            {
                ReportPath = reporte.GetAttribute("ruta");
                break;
            }
        }

        return ReportPath;
    }

    /// <summary>
    /// Carga reporte para ser descargado al cliente
    /// </summary>
    private void LoadReport()
    {
        if (Request.QueryString["esFuc"] != null)
        {
            ShowReportFUC();
        }
        else if (Request.QueryString["ReportName"] != null)
        {
            ShowReport(Convert.ToString(Request.QueryString["ReportName"]));
        }
        else if (objReporte != null)
        {
            ShowReport();
        }
    }

    /// <summary>
    /// Muestra el Reporte del FUC
    /// </summary>
    private void ShowReportFUC()
    {
        try
        {
            string vReportPath = ConfigurationManager.AppSettings["ReportLocalPath"];

            //rviReportes.LocalReport.ReportPath = Server.MapPath("~/General/General/Report/ReporteFUC.rdlc");

            rviReportes.LocalReport.ReportPath = Path.Combine(vReportPath, "ReporteFUC.rdlc");

            rviReportes.LocalReport.EnableHyperlinks = true;

            rviReportes.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

            rviReportes.LocalReport.Refresh();

            rviReportes.ShowExportControls = true;

            var parametros = objReporte.GetParemeters();

            if (parametros.Count > 0)
            {
                 var vigenciaFiscal = parametros.FirstOrDefault(e => e.Name == "VigenciaFiscalInicial");    
                 int idVigenciaFiscal = int.Parse(vigenciaFiscal.Values[0]);

                 var FechaDesde = parametros.FirstOrDefault(e => e.Name == "FechaDesde");    
                 DateTime vFechaDesde = DateTime.Parse(FechaDesde.Values[0]);

                 var FechaHasta = parametros.FirstOrDefault(e => e.Name == "FechaHasta");    
                 DateTime vFechaHasta = DateTime.Parse(FechaHasta.Values[0]);

                 var Regional = parametros.FirstOrDefault(e => e.Name == "Regional");    
		         string vidRegional = Regional.Values[0].ToString();

                var ModalidadSeleccion = parametros.FirstOrDefault(e => e.Name == "ModalidadSeleccion");    
                int ? vidModalidad = null;
                if (ModalidadSeleccion != null)
		            vidModalidad = int.Parse(ModalidadSeleccion.Values[0]);

                var CategoriaContrato = parametros.FirstOrDefault(e => e.Name == "CategoriaContrato");    
                int ? vidCategoria = null;
                if (CategoriaContrato != null)
		              vidCategoria = int.Parse(CategoriaContrato.Values[0]);

                var TipoContrato = parametros.FirstOrDefault(e => e.Name == "TipoContrato");    
                int ? vidTipoContrato = null;
                if (TipoContrato != null)
		              vidTipoContrato = int.Parse(TipoContrato.Values[0]);

                var TipoPersona = parametros.FirstOrDefault(e => e.Name == "TipoPersona");    
                int ? vidTipoPersona = null;
                if (TipoPersona != null)
		              vidTipoPersona = int.Parse(TipoPersona.Values[0]);


                var EstadoContrato = parametros.FirstOrDefault(e => e.Name == "EstadoContrato");    
                int ? vidEstadoContrato = null;
                if (EstadoContrato != null)
		              vidEstadoContrato = int.Parse(EstadoContrato.Values[0]);

                List<ReporteFUC> misitems = null;

                if(Request.QueryString["esHistorico"] == "1")
                    misitems = vContratoService.ObtenerReporteFUCHistorico(vFechaDesde, vFechaHasta, idVigenciaFiscal, vidRegional, vidModalidad, vidCategoria, vidTipoContrato, vidEstadoContrato, vidTipoPersona);
                else
                    misitems = vContratoService.ObtenerReporteFUC(vFechaDesde, vFechaHasta, idVigenciaFiscal, vidRegional, vidModalidad, vidCategoria, vidTipoContrato, vidEstadoContrato, vidTipoPersona);

                ReportDataSource dsItems = new ReportDataSource();
                dsItems.Name = "DataSet1";
                dsItems.Value = misitems;

                rviReportes.LocalReport.DataSources.Add(dsItems);

                objReporte.ClearParameters();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Instancia el reporte
    /// </summary>
    private void ShowReport()
    {
        try
        {
            string vReportServerUrl = ConfigurationManager.AppSettings["ReportServerUrl"];
            string vReportPath = ConfigurationManager.AppSettings["ReportPath"];
            string vUserName = ConfigurationManager.AppSettings["UserName"];
            string vPassword = ConfigurationManager.AppSettings["Password"];
            string vDomainName = ConfigurationManager.AppSettings["DomainName"];

            string Reportpath = LoadReportName(objReporte.ReportName);


            //set the url for the reporting service web service
            rviReportes.ServerReport.ReportServerUrl = new System.Uri(vReportServerUrl);

            //gets, checks and assign the report path for the reports
            if (vReportPath != string.Empty)
                rviReportes.ServerReport.ReportPath = "/" + vReportPath + "/" + Reportpath + "/" + objReporte.ReportName;
            else
                rviReportes.ServerReport.ReportPath = "/" + Reportpath;

            rviReportes.ServerReport.ReportPath = rviReportes.ServerReport.ReportPath.Replace("//", "/");
            //set the report credentias
            rviReportes.ServerReport.ReportServerCredentials = new CustomReportCredentials(vUserName,
                                                                                           vPassword,
                                                                                           vDomainName);

            //set the Processing Mode
            rviReportes.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;


            //set the Processing Mode
            rviReportes.ServerReport.Refresh();

            //set the ShowExportControls Option
            rviReportes.ShowExportControls = objReporte.ShowExportControls;

            //If there are parameter we procced to pass them to the report viewer
            if (objReporte.GetParemeters().Count > 0)
            {
                rviReportes.ServerReport.SetParameters(objReporte.GetParemeters());
                //Clear all the parameter from the report object
                objReporte.ClearParameters();
            }
           

            if (objReporte.ExportarPDF || objReporte.ExportarWord)
            {
                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                byte[] bytes;
                   
                 if(objReporte.ExportarPDF)
                     bytes = rviReportes.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                else
                     bytes = rviReportes.ServerReport.Render("Word", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader("content-disposition", "attachment; filename= " + objReporte.NombreArchivoExp + "." + extension);
                Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file  
                Response.Flush(); // send it to the client to download  
                Response.End();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Libera el reporte al cliente
    /// </summary>
    /// <param name="strReportPath"></param>
    private void ShowReport(string strReportPath)
    {
        try
        {
            string vReportServerUrl = ConfigurationManager.AppSettings["ReportServerUrl"];
            string vReportPath = ConfigurationManager.AppSettings["ReportPath"];
            string vUserName = ConfigurationManager.AppSettings["UserName"];
            string vPassword = ConfigurationManager.AppSettings["Password"];
            string vDomainName = ConfigurationManager.AppSettings["DomainName"];

            string Reportpath = LoadReportName(strReportPath);

            //set the url for the reporting service web service
            rviReportes.ServerReport.ReportServerUrl = new System.Uri(vReportServerUrl);

            //gets, checks and assign the report path for the reports
            if (vReportPath != string.Empty)
                rviReportes.ServerReport.ReportPath = "/" + vReportPath + "/" + Reportpath;
            else
                rviReportes.ServerReport.ReportPath = "/" + Reportpath;

            //set the report credentias
            rviReportes.ServerReport.ReportServerCredentials = new CustomReportCredentials(vUserName,
                                                                                           vPassword,
                                                                                           vDomainName);

            //set the Processing Mode
            rviReportes.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

            //set the Processing Mode
            rviReportes.ServerReport.Refresh();

            //set the ShowExportControls Option
            rviReportes.ShowExportControls = true;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    
    /// <summary>
    /// Inicializa variables locales y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            objReporte = (Report)GetSessionParameter("Report");
            toolBar.EstablecerTitulos(objReporte.ReportTitle);
            strPreviousPage = objReporte.PreviousPageURL;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}