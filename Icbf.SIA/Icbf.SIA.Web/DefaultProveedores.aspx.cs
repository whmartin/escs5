﻿using System;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI.WebControls;
using Icbf.Oferente.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Seguridad.Service;

/// <summary>
/// Página de login para los usuarios externos
/// </summary>
public partial class DefaultProveedores : GeneralWeb
{
    SIAService vRUBOService = new SIAService();

    

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetVersion();  
            var vSeguridadService = new SeguridadService();
            this.lblMsg.Text = (vSeguridadService.ConsultarParametro(1) == null) ? "No se encontró el parametro" : vSeguridadService.ConsultarParametro(1).ValorParametro;

            Session.Remove("Integrado");

            //read de cocckie ----20140226 --
            HttpCookie aCookie = Request.Cookies["IdSession"];
            if (aCookie != null)
            {
                string username = Server.HtmlEncode(aCookie.Value);

                if (Session["Mysession"] != null)
                {
                    if (username == Session["Mysession"].ToString())
                    {
                        NavigateTo(@"General/General/Master/MasterPrincipal.aspx", true);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Raises the LoggedIn event after the user logs in to the Web site and has been authenticated
    /// </summary>
    /// <param name="sender"></param>
    protected void OnLoggedIn(object sender, EventArgs e)
    {
        if (!vRUBOService.ConsultarUsuario(loGeneraciones.UserName).Rol.Split(';').Contains("PROVEEDORES")) return;
        ValidateUser(loGeneraciones.UserName, loGeneraciones.Password);

        //set the cookie ------20140226
        String sessionId;
        sessionId = Session.SessionID;
        HttpCookie loginCookie1 = new HttpCookie("IdSession");
        loginCookie1.Value = sessionId;
        Response.Cookies.Add(loginCookie1);
        Session["Mysession"] = sessionId;
        //set the cookie ------20140226 --Fin

        NavigateTo(@"General/General/Master/MasterPrincipal.aspx", true);
    }

    /// <summary>
    /// Raises the LoggingIn event when a user submits login information but before the authentication takes place
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoggingIn(object sender, LoginCancelEventArgs e)
    {
        //read de cocckie ----20140226 --
        HttpCookie aCookie = Request.Cookies["IdSession"];
        if (aCookie != null)
        {
            string username = Server.HtmlEncode(aCookie.Value);

            if (Session["Mysession"] != null)
            {
                if (username == Session["Mysession"].ToString())
                {
                    e.Cancel = true;
                    Response.Redirect("Error.aspx");
                }
            }
        }
        //read de cocckie ----20140226 --fin 


        if (Session["MaxInvalidPasswordAttemptsProv"] == null && Session["AttemptsProv"] == null)
        {
            Session["MaxInvalidPasswordAttemptsProv"] = Convert.ToInt16(ConfigurationManager.AppSettings["MaxInvalidPasswordAttempts"]);
            Session["Attempts"] = 1;
        }


        if (Convert.ToInt16(Session["AttemptsProv"]) > Convert.ToInt16(Session["MaxInvalidPasswordAttemptsProv"]))
        {
            captcha.Validate();
            if (captcha.IsValid == false)
            {
                e.Cancel = true;
            }
        }


        var usuario = Membership.GetUser(loGeneraciones.UserName);
        if (usuario != null)
        {
            this.loGeneraciones.FailureText = @"La contraseña es incorrecta, inténtelo de nuevo.";
            if (usuario.IsApproved == false)
            {
                if (loGeneraciones.Password.Equals(usuario.GetPassword()))
                {
                    if (vRUBOService.ConsultarUsuario(loGeneraciones.UserName).Rol.Split(';').Contains("PROVEEDORES"))
                    {
                        Session["usuarioProveedorLog"] = usuario;
                        NavigateTo(@"Account/CodeActivationProveedores.aspx", true);
                    }
                }
            }
        }
        else
        {
            this.loGeneraciones.FailureText = @"Usuario no existe, puede crearlo por la opción Registra tu cuenta de usuario.";
        }
    }

    /// <summary>
    /// Raises the LoginError event when a login attempt fails.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoginError(object sender, EventArgs e)
    {
        if (Convert.ToInt16(Session["AttemptsProv"]) < Convert.ToInt16(Session["MaxInvalidPasswordAttemptsProv"]) + 1)
        {
            Session["AttemptsProv"] = Convert.ToInt16(Session["AttemptsProv"]) + 1;
        }

    }


    /// <summary>
    /// Regresa una constante, valor uno
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public static string RetornarContador()
    {
        //return "1";

        string view = "0";
        if (Convert.ToInt16(HttpContext.Current.Session["AttemptsProv"]) > Convert.ToInt16(HttpContext.Current.Session["MaxInvalidPasswordAttemptsProv"]))
        {
            view = "1";
        }

        return view;
    }

    /// <summary>
    /// Manejador del evento click para el botón PasswordRecovery
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PasswordRecoveryLinkButton_Click(object sender, EventArgs e)
    {
        string textoCorreo = this.loGeneraciones.UserName;

        if (!textoCorreo.Equals("") && !textoCorreo.Equals("alguien@example.com"))
        {

            Response.Redirect(@"~/Account/RecoverPasswordProveedores.aspx?correo=" + textoCorreo);
        }
        else
        {
            Response.Redirect(@"~/Account/RecoverPasswordProveedores.aspx");
        }
    }

    /// <summary>
    /// Manejador del evento click para el botón Register
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkRegister_Click(object sender, EventArgs e)
    {
        string textoCorreo = this.loGeneraciones.UserName;

        if (!textoCorreo.Equals("") && !textoCorreo.Equals("alguien@example.com"))
        {
            Response.Redirect(@"~/Account/RegisterProveedor.aspx?correo=" + textoCorreo);
        }
        else
        {
            Response.Redirect(@"~/Account/RegisterProveedor.aspx");
        }

    }

    /// <summary>
    /// Obtiene versión del sistema
    /// </summary>
    private void GetVersion()
    {
        this.LblVersion.Text = " Versión: " + ApplicationInformation.ExecutingAssemblyVersion.ToString();
        this.LblVersion.ToolTip = ApplicationInformation.CompileDate.ToString();
    }

}