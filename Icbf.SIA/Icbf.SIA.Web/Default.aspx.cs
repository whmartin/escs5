﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using System.Configuration;
using System.Web.Services;

/// <summary>
/// Página a la que redirige el sistema cuando la esión expira, redirecciona a DefaultF
/// </summary>
public partial class _Default : GeneralWeb
{

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string App = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString(); 
        Response.Write("<script>window.open('" + ResolveUrl("~/Default" + App + ".aspx") + "','_parent');</script>");
    }
}
