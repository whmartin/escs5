﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using System.Configuration;
using System.Web.Services;

/// <summary>
/// Página de login para los usuarios internos
/// </summary>
public partial class DefaultF : GeneralWeb
{

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {      
        if (!IsPostBack)
        {
            GetVersion();

            //read de cocckie ----20140226 --
            HttpCookie aCookie = Request.Cookies["IdSession"];
            if (aCookie != null)
            {
                string username = Server.HtmlEncode(aCookie.Value);

                if (Session["Mysession"] != null)
                {
                    if (username == Session["Mysession"].ToString())
                    {
                        NavigateTo(@"General/General/Master/MasterPrincipal.aspx", true);
                    }
                }
            }
            //read de cocckie ----20140226 --fin

        }          
    }

    /// <summary>
    /// Raises the LoggedIn event after the user logs in to the Web site and has been authenticated
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoggedIn(object sender, EventArgs e)
    {
        ValidateUser(loGeneraciones.UserName, loGeneraciones.Password);

        //set the cookie ------20140226
        String sessionId;
        sessionId = Session.SessionID;
        HttpCookie loginCookie1 = new HttpCookie("IdSession");
        loginCookie1.Value = sessionId;
        Response.Cookies.Add(loginCookie1);
        Session["Mysession"] = sessionId;
        //set the cookie ------20140226 --Fin


        NavigateTo(@"General/General/Master/MasterPrincipal.aspx", true);        
    }

    /// <summary>
    /// Raises the LoggingIn event when a user submits login information but before the authentication takes place
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoggingIn(object sender, LoginCancelEventArgs e)
    {
        //read de cocckie ----20140226 --
        HttpCookie aCookie = Request.Cookies["IdSession"];
        if (aCookie != null)
        {
            string username = Server.HtmlEncode(aCookie.Value);

            if (Session["Mysession"] != null)
            {
                if (username == Session["Mysession"].ToString())
                {
                    e.Cancel = true;
                    Response.Redirect("Error.aspx");
                }
            }
        }
        //read de cocckie ----20140226 --fin



        if (Session["MaxInvalidPasswordAttempts"] == null && Session["Attempts"] == null)
        {
            Session["MaxInvalidPasswordAttempts"] = Convert.ToInt16(ConfigurationManager.AppSettings["MaxInvalidPasswordAttempts"]);
            Session["Attempts"] = 1;
        }

        //Habilitar Capcha
        //if (Convert.ToInt16(Session["Attempts"]) > Convert.ToInt16(Session["MaxInvalidPasswordAttempts"]) )
        //{
        //    captcha.Validate();
        //    if (captcha.IsValid == false)
        //    {
        //        e.Cancel = true;
        //    }
        //}
        // Fin Habilitar Capcha
    }

    /// <summary>
    /// Raises the LoginError event when a login attempt fails.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnLoginError(object sender, EventArgs e)
    {
        if (Convert.ToInt16(Session["Attempts"]) < Convert.ToInt16(Session["MaxInvalidPasswordAttempts"]) +1 )
        {
            Session["Attempts"] = Convert.ToInt16(Session["Attempts"]) + 1;
        }
       
    }

    /// <summary>
    /// Obtiene versión del aplicativo
    /// </summary>
    private void GetVersion()
    {
        this.LblVersion.Text = " Versión: " + ApplicationInformation.ExecutingAssemblyVersion.ToString();
        this.LblVersion.ToolTip = ApplicationInformation.CompileDate.ToString();
    }
  
    /// <summary>
    /// Regresa valor del número máxico de intentos
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public static string RetornarContador()
    {
        string view = "0";
        if (Convert.ToInt16(HttpContext.Current.Session["Attempts"]) > Convert.ToInt16(HttpContext.Current.Session["MaxInvalidPasswordAttempts"]))
        {
            view = "1";
        }

        return view;
    }
}
