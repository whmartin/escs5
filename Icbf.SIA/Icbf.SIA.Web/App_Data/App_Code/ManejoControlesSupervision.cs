﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Icbf.Supervision.Service;
using Icbf.Supervision.Entity;
using System.Web.UI.WebControls;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Clase con métodos generales para uso de DropDownList y RadioButtonList para la supervision de contratos
/// </summary>
public class ManejoControlesSupervision
{
    private SupervisionService vSupervisionService;
    SIAService vRUBOService;
    
    public ManejoControlesSupervision()
    {
        vRUBOService = new SIAService(); 
        vSupervisionService = new SupervisionService();
    }


    /// <summary>
    /// Inicializa los valores para un control lista radio buttons
    /// </summary>
    /// <param name="rblist"></param>
    /// <param name="valorTrue"></param>
    /// <param name="valorFalse"></param>
    public static void ValoresTrueFalseRadioButtonList(RadioButtonList rblist, string valorTrue, string valorFalse, string pSelected)
    {
        rblist.Items.Insert(0, new ListItem(valorTrue, "1"));
        rblist.Items.Insert(1, new ListItem(valorFalse, "0"));
        rblist.SelectedValue = pSelected;
    }

    public static void LlenarAplicable(RadioButtonList rblist, string pSelected)
    {
        rblist.Items.Insert(0, new ListItem("EC", "1"));
        rblist.Items.Insert(1, new ListItem("UDS", "2"));
        rblist.SelectedValue = pSelected;
    }

    /// <summary>
    /// Inicializa las Direcciones
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pIdUsuario"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlenarDireccion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<SupervisionDireccion> vDireccionSupervisions = vSupervisionService.ConsultarSupervisionDireccions(null,"","",1);  //. .ConsultarRegionalsUsuario(pIdUsuario);
        foreach (SupervisionDireccion vDireccionSupervision in vDireccionSupervisions)
        { pDropDownList.Items.Insert(0, new ListItem(vDireccionSupervision.NombreDireccion, vDireccionSupervision.IdDireccionesICBF.ToString())); }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }

    }

    /// <summary>
    /// Inicializa los tipos de respuesta
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    public void LlearTipoRespuesta(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<SupervisionPreguntas> vPreguntasSupervisions = vSupervisionService.ConsultarTipoRespuesta();
        foreach (SupervisionPreguntas vPreguntasSupervision in vPreguntasSupervisions)
        {
            pDropDownList.Items.Insert(0, new ListItem(vPreguntasSupervision.Descripcion, vPreguntasSupervision.IdTipoRespuesta.ToString()));


        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    /// <summary>
    /// Inicializa el Responsable
    /// </summary>
    /// <param name="pDropDownList"></param>
    /// <param name="pPredeterminado"></param>
    /// <param name="pMostrarSeleccionar"></param>
    /// <param name="Direccion"></param>
    public void LlenarResponsable(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? Direccion)
    {
        pDropDownList.Items.Clear();
        List<SupervisionResponsables> vResponsablesSupervisions = vSupervisionService.ConsultarSupervisionResponsabless(Direccion, "", 1);
        foreach (SupervisionResponsables vResponsablesSupervision in vResponsablesSupervisions)
        {
            pDropDownList.Items.Insert(0, new ListItem(vResponsablesSupervision.Nombre, vResponsablesSupervision.IdResponsable.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }
    
    /// <summary>
    /// Inicializa los estados de los cuestionarios
    /// </summary>
    /// <param name="rblists"></param>
    /// <param name="Valor1"></param>
    /// <param name="Valor2"></param>
    /// <param name="Valor3"></param>
    public static void ValoresEstadoCuestionario(RadioButtonList rblists, string Valor1, string Valor2, string Valor3)
    {
        rblists.Items.Insert(0, new ListItem(Valor1, "1"));
        rblists.Items.Insert(1, new ListItem(Valor2, "2"));
        rblists.Items.Insert(2, new ListItem(Valor3, "3"));

    }

    public void LlenarPreguntasLB(ListBox pDropDownList, Boolean? pEstado)
    {
        pDropDownList.Items.Clear();
        List<SupervisionPreguntas> Preguntas = vSupervisionService.ConsultarSupervisionPreguntass(null, null, "", "", null, null, null, null, "");
        foreach (SupervisionPreguntas vPregunta in Preguntas)
        {
            pDropDownList.Items.Insert(0, new ListItem(vPregunta.Nombre, vPregunta.IdPregunta.ToString()));
        }
    }
    /// <summary>
    /// Inicializa la modalidad / servicio
    /// </summary>
    /// <param name="pDropDownList"></param>
    public static void LlenarModalidad(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();

        pDropDownList.Items.Insert(0, new ListItem("EV12 Centro de Emergencia Conflicto", "1"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Externado Conflicto", "2"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Internado Conflicto(CENTROS DE PROTECCIÓN ESPECIALIZADOS - INTERNADOS )", "3"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Intervenciones de Apoyo Conflicto", "4"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Semi Internado Conflicto", "5"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 CAE Desvinculados", "6"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Hogar Transitorio", "7"));
        pDropDownList.Items.Insert(0, new ListItem("EV 12 Entidad Hogar Tutor", "8"));
        pDropDownList.Items.Insert(0, new ListItem("EV 12 Unidad de Servicio Hogar Tutor", "9"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 CAE SRPA", "10"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Centro Internamiento Preventivo SRPA", "11"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Externado SRPA", "12"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Internado Abierto SRPA", "13"));
        pDropDownList.Items.Insert(0, new ListItem("EV12 Intervenciones de Apoyo - Libertad Vigilada SRPA", "14"));

        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }

    }

    public void LlenarComponente(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? IdDireccion)
    {
        pDropDownList.Items.Clear();

        List<SupervisionSubcomponentes> Subcomponentes = vSupervisionService.ConsultarSupervisionSubcomponentess(IdDireccion, "", 1);
        foreach (SupervisionSubcomponentes Subcomponente in Subcomponentes)
        {
            pDropDownList.Items.Insert(0, new ListItem(Subcomponente.Nombre, Subcomponente.IdSubcomponente.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }


    }

    public String LlenarComponenteAjax(Boolean pMostrarSeleccionar, int? IdDireccion)
    {
        System.Text.StringBuilder vOpciones = new System.Text.StringBuilder();
        List<SupervisionSubcomponentes> Subcomponentes = vSupervisionService.ConsultarSupervisionSubcomponentess(IdDireccion, "", 1);
        foreach (SupervisionSubcomponentes Subcomponente in Subcomponentes)
        {
            vOpciones.Append("<option value='" + Subcomponente.IdSubcomponente.ToString() + "'>" + Subcomponente.Nombre + "</option>");
        }

        return vOpciones.ToString();

    }

    public void LlenarPregunta(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? IdDireccion)
    {
        pDropDownList.Items.Clear();

        List<SupervisionPreguntas> Preguntas = vSupervisionService.ConsultarSupervisionPreguntass(null, IdDireccion, "", "", null, null, null, 1, "");
        foreach (SupervisionPreguntas Pregunta in Preguntas)
        {
            pDropDownList.Items.Insert(0, new ListItem(Pregunta.Nombre, Pregunta.IdPregunta.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }


    }

    public String LlenarPreguntaAjax(int? IdDireccion, int? aplicableA)
    {
        System.Text.StringBuilder vOpciones = new System.Text.StringBuilder();
        List<SupervisionPreguntas> Preguntas = vSupervisionService.ConsultarSupervisionPreguntass(null, IdDireccion, "", "", null, aplicableA, null, 1, "");
        foreach (SupervisionPreguntas Pregunta in Preguntas)
        {
            vOpciones.Append("<option value='" + Pregunta.IdPregunta.ToString() + "'>" + Pregunta.Nombre + "</option>");
        }
        return vOpciones.ToString();
    }


    public void LlenarRespuestas(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? pIdPregunta)
    {
        pDropDownList.Items.Clear();

        List<SupervisionRespuestas> Respuestas = vSupervisionService.ConsultarSupervisionRespuestass(pIdPregunta, null, null, 1);
        foreach (SupervisionRespuestas Respuesta in Respuestas)
        {
            pDropDownList.Items.Insert(0, new ListItem(Respuesta.Valor, Respuesta.IdRespuesta.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }


    }

    public String LlenarRespuestasAjax(int? pIdPregunta)
    {
        System.Text.StringBuilder vOpciones = new System.Text.StringBuilder();
        List<SupervisionRespuestas> Respuestas = vSupervisionService.ConsultarSupervisionRespuestass(pIdPregunta,null,null,1);
        foreach (SupervisionRespuestas Respuesta in Respuestas)
        {
            vOpciones.Append("<option value='" + Respuesta.IdRespuesta.ToString() + "'>" + Respuesta.Valor + "</option>");
        }
        return vOpciones.ToString();
    }
    

    //public void LlenarResponsable(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int Direccion)
    //{
    //    pDropDownList.Items.Clear();
    //    List<SupervisionResponsables> vResponsablesSupervisions = vSupervisionService.ConsultarSupervisionResponsabless(Direccion, "", 1);
    //    foreach (SupervisionResponsables vResponsablesSupervision in vResponsablesSupervisions)
    //    {
    //        pDropDownList.Items.Insert(0, new ListItem(vResponsablesSupervision.Nombre, vResponsablesSupervision.IdDireccion.ToString()));

    //    }
    //    if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
    //    if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    //}

    //public static void LLenarDireccion(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    //{

    //    pDropDownList.Items.Clear();

    //    pDropDownList.Items.Insert(0, new ListItem("Dirección de primera infancia", "1"));
    //    pDropDownList.Items.Insert(0, new ListItem("Dirección de protección", "2"));


    //    if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
    //    if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }

    //}

    public static void LLenarVigencia(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {

        pDropDownList.Items.Clear();
        pDropDownList.Items.Insert(0, new ListItem("2015", "2015"));
        pDropDownList.Items.Insert(0, new ListItem("2016", "2016"));

        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }

    }

    public static void LlenarComboLista(DropDownList combo, List<Regional> lista, string dataValue, string dataText)
    {

        combo.DataSource = lista;
        combo.DataValueField = dataValue;
        combo.DataTextField = dataText;
        combo.DataBind();
        combo.Items.Insert(0, new ListItem("Seleccione", "-1"));
        combo.SelectedValue = "-1";

    }

    public void LlenarComponente(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<SupervisionComponentes> vComponentesSupervision = vSupervisionService.ConsultarSupervisionComponentes(null, null, "", 1);
        foreach (SupervisionComponentes Componente in vComponentesSupervision)
        {
            pDropDownList.Items.Insert(0, new ListItem(Componente.NombreComponente, Componente.IdComponente.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }

    }
    public void LlenarSubComponente(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, int? pDireccion)
    {
        pDropDownList.Items.Clear();
        List<SupervisionSubcomponentes> vSubComponentesSupervision = vSupervisionService.ConsultarSupervisionSubcomponentess(pDireccion, "", 1);
        foreach (SupervisionSubcomponentes SubComponente in vSubComponentesSupervision)
        {
            pDropDownList.Items.Insert(0, new ListItem(SubComponente.Nombre, SubComponente.IdSubcomponente.ToString()));
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if (pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }

    }

    public void LlenarTipoDocumento(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
             pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString())); 
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
        if (pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

    //public string obtenercodRegional(int)
    //{ 
    

    //    return
    //}

}
