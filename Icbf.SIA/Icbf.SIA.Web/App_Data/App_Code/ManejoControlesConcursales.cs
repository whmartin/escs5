﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Icbf.Supervision.Service;
using Icbf.Supervision.Entity;
using System.Web.UI.WebControls;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Clase con métodos generales para uso de DropDownList y RadioButtonList para la supervision de contratos
/// </summary>
public class ManejoControlesConcursales
{
    SIAService vSiaService;
    
    public ManejoControlesConcursales()
    {
        vSiaService = new SIAService(); 
    }

    public void LlenarComboTipoIdentificacionNMF(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<TipoDocumento> vLTipoDoc = vSiaService.ConsultarTiposDocumentoNMF(null,null);
        foreach(TipoDocumento tD in vLTipoDoc)
        {
           pDropDownList.Items.Add(new ListItem(tD.Codigo, tD.Codigo.ToString())); 
        }

        if(pMostrarSeleccionar)
        pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void LlenarRegionalPCI(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar)
    {
        pDropDownList.Items.Clear();
        List<Regional> vRegionals = vSiaService.ConsultarRegionalPCIs(null, null);
        foreach(Regional vRegional in vRegionals)
        { pDropDownList.Items.Insert(0, new ListItem(vRegional.NombreRegional, vRegional.CodigoRegional.ToString())); }
        //pDropDownList.DataBind();
        if(pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
        if(pPredeterminado != null && pPredeterminado.Length > 0) { pDropDownList.SelectedValue = pPredeterminado.ToString(); }
    }

}
