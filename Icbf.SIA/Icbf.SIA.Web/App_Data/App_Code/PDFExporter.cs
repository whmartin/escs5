﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using iTextSharp.text;
using iTextSharp.text.pdf;
/// <summary>
/// Clase para generación y exportación de PDF
/// </summary>
public class PDFExporter
{
       private readonly DataTable dataTable;
       private readonly string fileName;
       private readonly bool timeStamp;
    
 
    /// <summary>
    /// Exporta la tabla a un formato de documento
    /// </summary>
    /// <param name="dataTable"></param>
    /// <param name="fileName"></param>
    /// <param name="timeStamp"></param>
        public PDFExporter(DataTable dataTable, string fileName, bool timeStamp)
        {
            this.dataTable = dataTable;
            this.fileName = timeStamp ? String.Format("{0}-{1}", fileName, GetTimeStamp(DateTime.Now)) : fileName;
            this.timeStamp = timeStamp;
        }
 
    /// <summary>
    /// Exporta el documento liberando el contexto de la memoria
    /// </summary>
        public void ExportPDF()
        {
            HttpResponse Response = HttpContext.Current.Response;
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
 
            // step 1: creation of a document-object
            Document document = new Document(PageSize.A4, 10, 10, 90, 10);
 
            // step 2: we create a writer that listens to the document
            PdfWriter writer = PdfWriter.GetInstance(document, Response.OutputStream);
 
            //set some header stuff
            document.AddTitle(fileName);
            document.AddSubject(String.Format("Table of {0}", fileName));
            document.AddCreator("www.yetanothercoder.com");
            document.AddAuthor("naveenj");
 
            // step 3: we open the document
            document.Open();
 
            // step 4: we add content to the document
            CreatePages(document);
 
            // step 5: we close the document
            document.Close();
        }
 
    /// <summary>
    /// Crea las pàginas del documento
    /// </summary>
    /// <param name="document"></param>
        private void CreatePages(Document document)
        {
            document.NewPage();
           // document.Add(FormatPageHeaderPhrase(dataTable.TableName));
            PdfPTable pdfTable = new PdfPTable(dataTable.Columns.Count);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 100; // percentage
            pdfTable.DefaultCell.BorderWidth = 2;
            pdfTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
 
            foreach (DataColumn column in dataTable.Columns)
            {
             //   pdfTable.AddCell(FormatHeaderPhrase(column.ColumnName));
            }
            pdfTable.HeaderRows = 1;  // this is the end of the table header
            pdfTable.DefaultCell.BorderWidth = 1;
 
            foreach (DataRow row in dataTable.Rows)
            {
                foreach (object cell in row.ItemArray)
                {
                    //assume toString produces valid output
                    pdfTable.AddCell(FormatPhrase(cell.ToString()));
                }
            }
 
            document.Add(pdfTable);
        }
 
        private Phrase FormatPhrase(string value)
        {
            return new Phrase(value, FontFactory.GetFont(FontFactory.TIMES, 8));
        }
 
    /// <summary>
    /// Da formato a una fecha completa
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
        private string GetTimeStamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
