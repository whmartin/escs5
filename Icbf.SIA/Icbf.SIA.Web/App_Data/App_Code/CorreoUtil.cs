﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

    /// <summary>
    /// Class CorreoUtil
    /// </summary>
    public class CorreoUtil
    {
        /// <summary>
        /// Envia un mail
        /// </summary>
        /// <param name="emailPara">Email destino</param>        
        /// <param name="nombreOrigen">Nombre de la "persona" que envia el correo</param>
        /// <param name="asunto">Asunto del mensaje</param>
        /// <param name="mensaje">El mensaje a enviar</param>
        /// <param name="idUsuario">El id del usuario que realiza la transacción</param>
        /// <param name="archivo">Ruta del archivo adjunto</param>
        /// <returns>Indica si se envio el correo</returns>public static bool
        public static bool EnviarCorreo(string emailPara, string nombreOrigen, string asunto, string mensaje, int idUsuario, string archivo)
        {
            if (string.IsNullOrEmpty(emailPara))
            {
                throw new Exception("El email de destino no puede ser vacio");
            }

            try
            {
                MailMessage objMensaje = new MailMessage();
                MailAddress objEnvioEmail = new MailAddress(ConfigurationManager.AppSettings["MailUser"].ToString(), nombreOrigen);

                objMensaje.From = objEnvioEmail;
                objMensaje.From.DisplayName.ToString();
                MailAddress destinoEmail = null;
                if (emailPara.Contains(","))
                {
                    if (emailPara.EndsWith(","))
                    {
                        emailPara = emailPara.Substring(0, emailPara.Length - 1);
                    }

                    string[] destinoSplit = emailPara.Split(',');

                    for (int i = 0; i < destinoSplit.Length; i++)
                    {
                        destinoEmail = new MailAddress(destinoSplit[i].ToString());
                        objMensaje.To.Add(destinoEmail);
                    }
                }
                else
                {
                    destinoEmail = new MailAddress(emailPara);
                    objMensaje.To.Add(destinoEmail);
                }

                if (!string.IsNullOrEmpty(archivo))
                {
                    Attachment data = new Attachment(archivo, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = data.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(archivo);
                    disposition.ModificationDate = File.GetLastWriteTime(archivo);
                    disposition.ReadDate = File.GetLastAccessTime(archivo);
                    objMensaje.Attachments.Add(data);
                }

                objMensaje.Subject = asunto;
                objMensaje.Body = mensaje;
                objMensaje.IsBodyHtml = true;
                objMensaje.Bcc.Add(ConfigurationManager.AppSettings["MailUser"].ToString());
                SmtpClient mailSMTP = new SmtpClient(ConfigurationManager.AppSettings["MailHost"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["MailPort"].ToString()));
                mailSMTP.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailUser"].ToString(), ConfigurationManager.AppSettings["MailPassword"].ToString());
                mailSMTP.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MailEnableSsl"].ToString());
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
               { return true; };
                mailSMTP.Send(objMensaje);
                objMensaje.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("quota exceeded"))
                {
                    throw new Exception("Se ha excedido el límite de envíos de correos diarios.");
                }

                ////ErrorLog.Set_ErrorLog("Deliverix3Library", "UtilGenerica", "EnviarCorreo", ex, idUsuario, System.Configuration.ConfigurationManager.ConnectionStrings["DXCSqlServer"].ToString());
                return false;
            }
        }
    }
