﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;
using System.Net;

/// <summary>
/// Clase que permite establecer las credenciales de conexión a Reporting Services.
/// </summary>
public class CustomReportCredentials : IReportServerCredentials
{
    private string _UserName;
    private string _PassWord;
    private string _DomainName;

    /// <summary>
    /// Constructor de la clase CustomReportCredentials
    /// </summary>
    /// <param name="UserName"></param>
    /// <param name="PassWord"></param>
    /// <param name="DomainName"></param>
    public CustomReportCredentials(string UserName, string PassWord, string DomainName)
    {
        _UserName = UserName;
        _PassWord = PassWord;
        _DomainName = DomainName;
    }

    public System.Security.Principal.WindowsIdentity ImpersonationUser
    {
        get { return null; }
    }

    public ICredentials NetworkCredentials
    {
        get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
    }

    /// <summary>
    /// Verifica credenciales
    /// </summary>
    /// <param name="authCookie"></param>
    /// <param name="user"></param>
    /// <param name="password"></param>
    /// <param name="authority"></param>
    /// <returns></returns>
    public bool GetFormsCredentials(out Cookie authCookie, out string user,
     out string password, out string authority)
    {
        authCookie = null;
        user = password = authority = null;
        return false;
    }
}