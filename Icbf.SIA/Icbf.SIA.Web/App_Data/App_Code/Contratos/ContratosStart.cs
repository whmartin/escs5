﻿using Icbf.RUBO.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de ContratosStart
/// </summary>
public static class ContratosStart
{
    private const string NOMBRE_ROL = "AdminGContratos";
    private const string NOMBRE_ROL_COORDINADORCONTRATOS = "CoordinadorContratos";
    private const string NOMBRE_ROL_ABOGADOCONTRATOS = "AbogadoContratos";
    private const string NOMBRE_ROL_GESTORCONTRATOS = "GestorContratos";

    public static void Init()
    {
        InitAdminContratos();
        InitCoordinador();
        InitGestor();
        InitAbogado();
    }

    private static void InitAdminContratos()
    {
        try
        {
            SeguridadService service = new SeguridadService();

            var rol = service.ConsultarRoles(NOMBRE_ROL, true);

            if (rol != null && rol.Count > 0)
                rol = rol.Where(e => e.NombreRol == NOMBRE_ROL).ToList();

            Icbf.Seguridad.Entity.Rol miRol = new Rol();

            if (rol == null || rol.Count == 0)
            {
                var modulos = service.ConsultarModulos("Contratos", true);
                var modulosSeguridad = service.ConsultarModulos("Seguridad", true);

                if (modulos != null && modulos.Count > 0)
                {
                    var moduloContratos = modulos.FirstOrDefault(e => e.NombreModulo == "Contratos");
                    var moduloSeguridad = modulosSeguridad.FirstOrDefault(e => e.NombreModulo == "Seguridad");

                    var programas = service.ConsultarProgramas(moduloContratos.IdModulo, string.Empty);
                    var programasSeguridad = service.ConsultarProgramas(moduloSeguridad.IdModulo, string.Empty);

                    if (programas != null && programas.Count > 0)
                    {
                        List<Permiso> permisos = new List<Permiso>();

                        foreach (var itemPrograma in programas)
                        {
                            permisos.Add(new Permiso()
                            {
                                Consultar = true,
                                Eliminar = true,
                                IdPrograma = itemPrograma.IdPrograma,
                                Insertar = true,
                                Modificar = true,
                                UsuarioCreacion = "AdminSia",
                            });
                        }

                        foreach (var itemPrograma in programasSeguridad)
                        {
                            permisos.Add(new Permiso()
                            {
                                Consultar = true,
                                Eliminar = true,
                                IdPrograma = itemPrograma.IdPrograma,
                                Insertar = true,
                                Modificar = true,
                                UsuarioCreacion = "AdminSia",
                            });
                        }

                        miRol = new Icbf.Seguridad.Entity.Rol()
                        {
                            DescripcionRol = "Rol de Administración del modúlo de contratos",
                            Estado = true,
                            UsuarioCreacion = "AdminSia",
                            NombreRol = NOMBRE_ROL,
                            FechaCreacion = DateTime.Now,
                            Permisos = permisos,
                            EsAdministrador = false
                        };

                        service.InsertarRol(miRol);
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }     
    }

    private static void InitCoordinador()
    {

    }

    private static void InitAbogado()
    {

    }

    private static void InitGestor()
    {

    }
}