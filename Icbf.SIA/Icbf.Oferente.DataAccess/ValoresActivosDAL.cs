using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ValoresActivosDAL : GeneralDAL
    {
        public ValoresActivosDAL()
        {
        }
        public int InsertarValoresActivos(ValoresActivos pValoresActivos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValoresActivos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValoresActivos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoValoresActivos", DbType.String, pValoresActivos.CodigoValoresActivos);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pValoresActivos.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pValoresActivos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValoresActivos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pValoresActivos.IdValoresActivos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValoresActivos").ToString());
                    GenerarLogAuditoria(pValoresActivos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarValoresActivos(ValoresActivos pValoresActivos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValoresActivos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValoresActivos", DbType.Int32, pValoresActivos.IdValoresActivos);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoValoresActivos", DbType.String, pValoresActivos.CodigoValoresActivos);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pValoresActivos.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pValoresActivos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pValoresActivos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValoresActivos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarValoresActivos(ValoresActivos pValoresActivos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValoresActivos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValoresActivos", DbType.Int32, pValoresActivos.IdValoresActivos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValoresActivos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public ValoresActivos ConsultarValoresActivos(int pIdValoresActivos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValoresActivos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdValoresActivos", DbType.Int32, pIdValoresActivos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValoresActivos vValoresActivos = new ValoresActivos();
                        while (vDataReaderResults.Read())
                        {
                            vValoresActivos.IdValoresActivos = vDataReaderResults["IdValoresActivos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValoresActivos"].ToString()) : vValoresActivos.IdValoresActivos;
                            vValoresActivos.CodigoValoresActivos = vDataReaderResults["CodigoValoresActivos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoValoresActivos"].ToString()) : vValoresActivos.CodigoValoresActivos;
                            vValoresActivos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vValoresActivos.Descripcion;
                            vValoresActivos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vValoresActivos.Estado;
                            vValoresActivos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValoresActivos.UsuarioCrea;
                            vValoresActivos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValoresActivos.FechaCrea;
                            vValoresActivos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValoresActivos.UsuarioModifica;
                            vValoresActivos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValoresActivos.FechaModifica;
                        }
                        return vValoresActivos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ValoresActivos> ConsultarValoresActivoss(String pCodigoValoresActivos, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValoresActivoss_Consultar"))
                {
                    if (pCodigoValoresActivos != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoValoresActivos", DbType.String, pCodigoValoresActivos);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValoresActivos> vListaValoresActivos = new List<ValoresActivos>();
                        while (vDataReaderResults.Read())
                        {
                            ValoresActivos vValoresActivos = new ValoresActivos();
                            vValoresActivos.IdValoresActivos = vDataReaderResults["IdValoresActivos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValoresActivos"].ToString()) : vValoresActivos.IdValoresActivos;
                            vValoresActivos.CodigoValoresActivos = vDataReaderResults["CodigoValoresActivos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoValoresActivos"].ToString()) : vValoresActivos.CodigoValoresActivos;
                            vValoresActivos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vValoresActivos.Descripcion;
                            vValoresActivos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vValoresActivos.Estado;
                            vValoresActivos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValoresActivos.UsuarioCrea;
                            vValoresActivos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValoresActivos.FechaCrea;
                            vValoresActivos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValoresActivos.UsuarioModifica;
                            vValoresActivos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValoresActivos.FechaModifica;
                            vListaValoresActivos.Add(vValoresActivos);
                        }
                        return vListaValoresActivos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoValoresActivos(String pCodigoValoresActivos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValoresActivos_Consultar_ValoresActivos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoValoresActivos", DbType.String, pCodigoValoresActivos);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoValoresActivos = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoValoresActivos = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoValoresActivos;
                        }
                        return vExisteCodigoValoresActivos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
