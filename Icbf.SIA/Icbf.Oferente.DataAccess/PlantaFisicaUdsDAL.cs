using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class PlantaFisicaUdsDAL : GeneralDAL
    {
        public PlantaFisicaUdsDAL()
        {
        }
        public int InsertarPlantaFisicaUds(PlantaFisicaUds pPlantaFisicaUds)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PlantaFisicaUds_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPlantaFisicaUds", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoPlantaFisicaUds", DbType.String, pPlantaFisicaUds.CodigoPlantaFisicaUds);
                    vDataBase.AddInParameter(vDbCommand, "@NombrePlantaFisicaUds", DbType.String, pPlantaFisicaUds.NombrePlantaFisicaUds);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pPlantaFisicaUds.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPlantaFisicaUds.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPlantaFisicaUds.IdPlantaFisicaUds = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPlantaFisicaUds").ToString());
                    GenerarLogAuditoria(pPlantaFisicaUds, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarPlantaFisicaUds(PlantaFisicaUds pPlantaFisicaUds)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PlantaFisicaUds_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPlantaFisicaUds", DbType.Int32, pPlantaFisicaUds.IdPlantaFisicaUds);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoPlantaFisicaUds", DbType.String, pPlantaFisicaUds.CodigoPlantaFisicaUds);
                    vDataBase.AddInParameter(vDbCommand, "@NombrePlantaFisicaUds", DbType.String, pPlantaFisicaUds.NombrePlantaFisicaUds);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pPlantaFisicaUds.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPlantaFisicaUds.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlantaFisicaUds, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarPlantaFisicaUds(PlantaFisicaUds pPlantaFisicaUds)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PlantaFisicaUds_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPlantaFisicaUds", DbType.Int32, pPlantaFisicaUds.IdPlantaFisicaUds);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPlantaFisicaUds, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public PlantaFisicaUds ConsultarPlantaFisicaUds(int pIdPlantaFisicaUds)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PlantaFisicaUds_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlantaFisicaUds", DbType.Int32, pIdPlantaFisicaUds);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        PlantaFisicaUds vPlantaFisicaUds = new PlantaFisicaUds();
                        while (vDataReaderResults.Read())
                        {
                            vPlantaFisicaUds.IdPlantaFisicaUds = vDataReaderResults["IdPlantaFisicaUds"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlantaFisicaUds"].ToString()) : vPlantaFisicaUds.IdPlantaFisicaUds;
                            vPlantaFisicaUds.CodigoPlantaFisicaUds = vDataReaderResults["CodigoPlantaFisicaUds"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPlantaFisicaUds"].ToString()) : vPlantaFisicaUds.CodigoPlantaFisicaUds;
                            vPlantaFisicaUds.NombrePlantaFisicaUds = vDataReaderResults["NombrePlantaFisicaUds"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePlantaFisicaUds"].ToString()) : vPlantaFisicaUds.NombrePlantaFisicaUds;
                            vPlantaFisicaUds.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vPlantaFisicaUds.Estado;
                            vPlantaFisicaUds.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlantaFisicaUds.UsuarioCrea;
                            vPlantaFisicaUds.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlantaFisicaUds.FechaCrea;
                            vPlantaFisicaUds.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlantaFisicaUds.UsuarioModifica;
                            vPlantaFisicaUds.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlantaFisicaUds.FechaModifica;
                        }
                        return vPlantaFisicaUds;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlantaFisicaUds> ConsultarPlantaFisicaUdss(String pCodigoPlantaFisicaUds, String pNombrePlantaFisicaUds, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PlantaFisicaUdss_Consultar"))
                {
                    if(pCodigoPlantaFisicaUds != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoPlantaFisicaUds", DbType.String, pCodigoPlantaFisicaUds);
                    if(pNombrePlantaFisicaUds != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombrePlantaFisicaUds", DbType.String, pNombrePlantaFisicaUds);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PlantaFisicaUds> vListaPlantaFisicaUds = new List<PlantaFisicaUds>();
                        while (vDataReaderResults.Read())
                        {
                                PlantaFisicaUds vPlantaFisicaUds = new PlantaFisicaUds();
                            vPlantaFisicaUds.IdPlantaFisicaUds = vDataReaderResults["IdPlantaFisicaUds"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPlantaFisicaUds"].ToString()) : vPlantaFisicaUds.IdPlantaFisicaUds;
                            vPlantaFisicaUds.CodigoPlantaFisicaUds = vDataReaderResults["CodigoPlantaFisicaUds"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPlantaFisicaUds"].ToString()) : vPlantaFisicaUds.CodigoPlantaFisicaUds;
                            vPlantaFisicaUds.NombrePlantaFisicaUds = vDataReaderResults["NombrePlantaFisicaUds"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePlantaFisicaUds"].ToString()) : vPlantaFisicaUds.NombrePlantaFisicaUds;
                            vPlantaFisicaUds.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vPlantaFisicaUds.Estado;
                            if (vPlantaFisicaUds.Estado == true)
                                vPlantaFisicaUds.DescripcionEstado = "Activo";
                            else
                                vPlantaFisicaUds.DescripcionEstado = "Inactivo";
                            vPlantaFisicaUds.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPlantaFisicaUds.UsuarioCrea;
                            vPlantaFisicaUds.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPlantaFisicaUds.FechaCrea;
                            vPlantaFisicaUds.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPlantaFisicaUds.UsuarioModifica;
                            vPlantaFisicaUds.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPlantaFisicaUds.FechaModifica;
                                vListaPlantaFisicaUds.Add(vPlantaFisicaUds);
                        }
                        return vListaPlantaFisicaUds;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
