using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase NumeroTrabajadorDAL
    /// </summary>
    public class NumeroTrabajadorDAL : GeneralDAL
    {
        public NumeroTrabajadorDAL()
        {
        }
        /// <summary>
        /// Insertar Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int InsertarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NumeroTrabajador_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdNumeroTrabajador", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNumeroTrabajador", DbType.String, pNumeroTrabajador.CodigoNumeroTrabajador);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pNumeroTrabajador.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pNumeroTrabajador.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pNumeroTrabajador.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pNumeroTrabajador.IdNumeroTrabajador = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdNumeroTrabajador").ToString());
                    GenerarLogAuditoria(pNumeroTrabajador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Modificar un Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int ModificarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NumeroTrabajador_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroTrabajador", DbType.Int32, pNumeroTrabajador.IdNumeroTrabajador);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNumeroTrabajador", DbType.String, pNumeroTrabajador.CodigoNumeroTrabajador);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pNumeroTrabajador.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pNumeroTrabajador.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pNumeroTrabajador.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNumeroTrabajador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Numero Trabajador
        /// </summary>
        /// <param name="pNumeroTrabajador"></param>
        /// <returns></returns>
        public int EliminarNumeroTrabajador(NumeroTrabajador pNumeroTrabajador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NumeroTrabajador_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroTrabajador", DbType.Int32, pNumeroTrabajador.IdNumeroTrabajador);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNumeroTrabajador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar NumeroTrabajador
        /// </summary>
        /// <param name="pIdNumeroTrabajador"></param>
        /// <returns></returns>
        public NumeroTrabajador ConsultarNumeroTrabajador(int pIdNumeroTrabajador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NumeroTrabajador_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNumeroTrabajador", DbType.Int32, pIdNumeroTrabajador);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        NumeroTrabajador vNumeroTrabajador = new NumeroTrabajador();
                        while (vDataReaderResults.Read())
                        {
                            vNumeroTrabajador.IdNumeroTrabajador = vDataReaderResults["IdNumeroTrabajador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroTrabajador"].ToString()) : vNumeroTrabajador.IdNumeroTrabajador;
                            vNumeroTrabajador.CodigoNumeroTrabajador = vDataReaderResults["CodigoNumeroTrabajador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNumeroTrabajador"].ToString()) : vNumeroTrabajador.CodigoNumeroTrabajador;
                            vNumeroTrabajador.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNumeroTrabajador.Descripcion;
                            vNumeroTrabajador.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vNumeroTrabajador.Estado;
                            vNumeroTrabajador.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNumeroTrabajador.UsuarioCrea;
                            vNumeroTrabajador.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNumeroTrabajador.FechaCrea;
                            vNumeroTrabajador.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNumeroTrabajador.UsuarioModifica;
                            vNumeroTrabajador.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNumeroTrabajador.FechaModifica;
                        }
                        return vNumeroTrabajador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de NumeroTrabajador
        /// </summary>
        /// <param name="pCodigoNumeroTrabajador"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<NumeroTrabajador> ConsultarNumeroTrabajadors(String pCodigoNumeroTrabajador, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NumeroTrabajadors_Consultar"))
                {
                    if(pCodigoNumeroTrabajador != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoNumeroTrabajador", DbType.String, pCodigoNumeroTrabajador);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NumeroTrabajador> vListaNumeroTrabajador = new List<NumeroTrabajador>();
                        while (vDataReaderResults.Read())
                        {
                                NumeroTrabajador vNumeroTrabajador = new NumeroTrabajador();
                            vNumeroTrabajador.IdNumeroTrabajador = vDataReaderResults["IdNumeroTrabajador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNumeroTrabajador"].ToString()) : vNumeroTrabajador.IdNumeroTrabajador;
                            vNumeroTrabajador.CodigoNumeroTrabajador = vDataReaderResults["CodigoNumeroTrabajador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNumeroTrabajador"].ToString()) : vNumeroTrabajador.CodigoNumeroTrabajador;
                            vNumeroTrabajador.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNumeroTrabajador.Descripcion;
                            vNumeroTrabajador.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vNumeroTrabajador.Estado;
                            vNumeroTrabajador.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNumeroTrabajador.UsuarioCrea;
                            vNumeroTrabajador.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNumeroTrabajador.FechaCrea;
                            vNumeroTrabajador.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNumeroTrabajador.UsuarioModifica;
                            vNumeroTrabajador.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNumeroTrabajador.FechaModifica;
                                vListaNumeroTrabajador.Add(vNumeroTrabajador);
                        }
                        return vListaNumeroTrabajador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar CodigoNumeroTrabajador
        /// </summary>
        /// <param name="pCodigoNumeroTrabajador"></param>
        /// <returns></returns>
        public int ConsultarCodigoNumeroTrabajador(String pCodigoNumeroTrabajador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NumeroTrabajador_Consultar_CodigoNumeroTrabajador"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNumeroTrabajador", DbType.String, pCodigoNumeroTrabajador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoNumeroTrabajador = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoNumeroTrabajador = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoNumeroTrabajador;
                        }
                        return vExisteCodigoNumeroTrabajador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
