using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class SiNoDAL : GeneralDAL
    {
        public SiNoDAL()
        {
        }
        public int InsertarSiNo(SiNo pSiNo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SiNo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSiNo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSiNo", DbType.String, pSiNo.CodigoSiNo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSiNo", DbType.String, pSiNo.NombreSiNo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pSiNo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSiNo.Usuariocrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSiNo.IdSiNo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSiNo").ToString());
                    GenerarLogAuditoria(pSiNo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSiNo(SiNo pSiNo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SiNo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSiNo", DbType.Int32, pSiNo.IdSiNo);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSiNo", DbType.String, pSiNo.CodigoSiNo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSiNo", DbType.String, pSiNo.NombreSiNo);
                    vDataBase.AddInParameter(vDbCommand, "@SiNo", DbType.Boolean, pSiNo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSiNo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSiNo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSiNo(SiNo pSiNo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SiNo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSiNo", DbType.Int32, pSiNo.IdSiNo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSiNo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public SiNo ConsultarSiNo(int pIdSiNo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SiNo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSiNo", DbType.Int32, pIdSiNo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SiNo vSiNo = new SiNo();
                        while (vDataReaderResults.Read())
                        {
                            vSiNo.IdSiNo = vDataReaderResults["IdSiNo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSiNo"].ToString()) : vSiNo.IdSiNo;
                            vSiNo.CodigoSiNo = vDataReaderResults["CodigoSiNo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSiNo"].ToString()) : vSiNo.CodigoSiNo;
                            vSiNo.NombreSiNo = vDataReaderResults["NombreSiNo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSiNo"].ToString()) : vSiNo.NombreSiNo;
                            vSiNo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vSiNo.Estado;
                            vSiNo.Usuariocrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSiNo.Usuariocrea;
                            vSiNo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSiNo.FechaCrea;
                            vSiNo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSiNo.UsuarioModifica;
                            vSiNo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSiNo.FechaModifica;

                            if (vSiNo.Estado == true)
                            {
                                vSiNo.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vSiNo.DescripcionEstado = "Inactivo";
                            }
                        }
                        return vSiNo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SiNo> ConsultarSiNos(String pCodigoSiNo, String pNombreSiNo, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SiNos_Consultar"))
                {
                    if(pCodigoSiNo != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoSiNo", DbType.String, pCodigoSiNo);
                    if(pNombreSiNo != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreSiNo", DbType.String, pNombreSiNo);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SiNo> vListaSiNo = new List<SiNo>();
                        while (vDataReaderResults.Read())
                        {
                                SiNo vSiNo = new SiNo();
                            vSiNo.IdSiNo = vDataReaderResults["IdSiNo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSiNo"].ToString()) : vSiNo.IdSiNo;
                            vSiNo.CodigoSiNo = vDataReaderResults["CodigoSiNo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSiNo"].ToString()) : vSiNo.CodigoSiNo;
                            vSiNo.NombreSiNo = vDataReaderResults["NombreSiNo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSiNo"].ToString()) : vSiNo.NombreSiNo;
                            vSiNo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vSiNo.Estado;
                            vSiNo.Usuariocrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSiNo.Usuariocrea;
                            vSiNo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSiNo.FechaCrea;
                            vSiNo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSiNo.UsuarioModifica;
                            vSiNo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSiNo.FechaModifica;

                            if (vSiNo.Estado == true)
                            {
                                vSiNo.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vSiNo.DescripcionEstado = "Inactivo";
                            }                           
                            
                            vListaSiNo.Add(vSiNo);
                        }
                        return vListaSiNo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
