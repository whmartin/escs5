﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class EstadoTerceroDAL : GeneralDAL
    {
        public EstadoTerceroDAL()
        {
        }
        

        
        public List<EstadoTercero> ConsultarEstadosTerceros(Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadosTercero_Consultar"))
                {
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoTercero> vListaEstado = new List<EstadoTercero>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoTercero vEstado = new EstadoTercero();
                            vEstado.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vEstado.IdEstadoTercero;
                            vEstado.CodigoEstadoTercero = vDataReaderResults["CodigoEstadoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoTercero"].ToString()) : vEstado.CodigoEstadoTercero;
                            vEstado.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vEstado.DescripcionEstado;
                            vEstado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstado.Estado;
                            vEstado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstado.UsuarioCrea;
                            vEstado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstado.FechaCrea;
                            vEstado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstado.UsuarioModifica;
                            vEstado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstado.FechaModifica;
                            vListaEstado.Add(vEstado);
                        }
                        return vListaEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoTercero ConsultarEstadoTercero(int id)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, id);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoTercero vEstado = new EstadoTercero();
                        while (vDataReaderResults.Read())
                        {
                            vEstado.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vEstado.IdEstadoTercero;
                            vEstado.CodigoEstadoTercero = vDataReaderResults["CodigoEstadoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoTercero"].ToString()) : vEstado.CodigoEstadoTercero;
                            vEstado.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vEstado.DescripcionEstado;
                            vEstado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstado.Estado;
                            vEstado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstado.UsuarioCrea;
                            vEstado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstado.FechaCrea;
                            vEstado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstado.UsuarioModifica;
                            vEstado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstado.FechaModifica;
                        }
                        return vEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoTercero ConsultarEstadoTercero(string codigoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_ConsultarByCodigo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@codigoTercero", DbType.String, codigoTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoTercero vEstado = new EstadoTercero();
                        while (vDataReaderResults.Read())
                        {
                            vEstado.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vEstado.IdEstadoTercero;
                            vEstado.CodigoEstadoTercero = vDataReaderResults["CodigoEstadoTercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoTercero"].ToString()) : vEstado.CodigoEstadoTercero;
                            vEstado.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vEstado.DescripcionEstado;
                            vEstado.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstado.Estado;
                            vEstado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstado.UsuarioCrea;
                            vEstado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstado.FechaCrea;
                            vEstado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstado.UsuarioModifica;
                            vEstado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstado.FechaModifica;
                        }
                        return vEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
