using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase InfraestructuraDAL
    /// </summary>
    public class InfraestructuraDAL : GeneralDAL
    {
        public InfraestructuraDAL()
        {
        }
        /// <summary>
        /// Insertar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int InsertarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Infraestructura_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInfraestructura", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pInfraestructura.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfraestructura.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdContacto", DbType.Int32, pInfraestructura.IdContacto);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pInfraestructura.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Personal", DbType.Decimal, pInfraestructura.Personal);
                    vDataBase.AddInParameter(vDbCommand, "@VincLaboral", DbType.Decimal, pInfraestructura.VincLaboral);
                    vDataBase.AddInParameter(vDbCommand, "@PrestServicios", DbType.Decimal, pInfraestructura.PrestServicios);
                    vDataBase.AddInParameter(vDbCommand, "@Voluntariado", DbType.Decimal, pInfraestructura.Voluntariado);
                    vDataBase.AddInParameter(vDbCommand, "@VoluntPermanente", DbType.Decimal, pInfraestructura.VoluntPermanente);
                    vDataBase.AddInParameter(vDbCommand, "@Asociados", DbType.Decimal, pInfraestructura.Asociados);
                    vDataBase.AddInParameter(vDbCommand, "@Mision", DbType.Decimal, pInfraestructura.Mision);
                    vDataBase.AddInParameter(vDbCommand, "@EspacioArchivo", DbType.Boolean, pInfraestructura.EspacioArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@EspacioMatOficina", DbType.Boolean, pInfraestructura.EspacioMatOficina);
                    vDataBase.AddInParameter(vDbCommand, "@EquipoComputo", DbType.Boolean, pInfraestructura.EquipoComputo);

                    if (pInfraestructura.SistOperativo != null)
                        vDataBase.AddInParameter(vDbCommand, "@SistOperativo", DbType.Int32, pInfraestructura.SistOperativo);

                    vDataBase.AddInParameter(vDbCommand, "@ConLineaFija", DbType.Boolean, pInfraestructura.ConLineaFija);
                    vDataBase.AddInParameter(vDbCommand, "@ConLineaCelular", DbType.Boolean, pInfraestructura.ConLineaCelular);
                    vDataBase.AddInParameter(vDbCommand, "@ConEmail", DbType.Boolean, pInfraestructura.ConEmail);
                    vDataBase.AddInParameter(vDbCommand, "@ConAccesoInternet", DbType.Boolean, pInfraestructura.ConAccesoInternet);
                    vDataBase.AddInParameter(vDbCommand, "@OtrasSedes", DbType.Boolean, pInfraestructura.OtrasSedes);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadOtrasSedes", DbType.Int32, pInfraestructura.CantidadOtrasSedes);
                    vDataBase.AddInParameter(vDbCommand, "@SedesPropias", DbType.Boolean, pInfraestructura.SedesPropias);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInfraestructura.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInfraestructura.IdInfraestructura = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdInfraestructura").ToString());
                    GenerarLogAuditoria(pInfraestructura, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int ModificarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Infraestructura_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInfraestructura", DbType.Int32, pInfraestructura.IdInfraestructura);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pInfraestructura.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfraestructura.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdContacto", DbType.Int32, pInfraestructura.IdContacto);
                    vDataBase.AddInParameter(vDbCommand, "@IdDireccion", DbType.Int32, pInfraestructura.IdDireccion);
                    vDataBase.AddInParameter(vDbCommand, "@Personal", DbType.Decimal, pInfraestructura.Personal);
                    vDataBase.AddInParameter(vDbCommand, "@VincLaboral", DbType.Decimal, pInfraestructura.VincLaboral);
                    vDataBase.AddInParameter(vDbCommand, "@PrestServicios", DbType.Decimal, pInfraestructura.PrestServicios);
                    vDataBase.AddInParameter(vDbCommand, "@Voluntariado", DbType.Decimal, pInfraestructura.Voluntariado);
                    vDataBase.AddInParameter(vDbCommand, "@VoluntPermanente", DbType.Decimal, pInfraestructura.VoluntPermanente);
                    vDataBase.AddInParameter(vDbCommand, "@Asociados", DbType.Decimal, pInfraestructura.Asociados);
                    vDataBase.AddInParameter(vDbCommand, "@Mision", DbType.Decimal, pInfraestructura.Mision);
                    vDataBase.AddInParameter(vDbCommand, "@EspacioArchivo", DbType.Boolean, pInfraestructura.EspacioArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@EspacioMatOficina", DbType.Boolean, pInfraestructura.EspacioMatOficina);
                    vDataBase.AddInParameter(vDbCommand, "@EquipoComputo", DbType.Boolean, pInfraestructura.EquipoComputo);

                    if (pInfraestructura.SistOperativo != null)
                        vDataBase.AddInParameter(vDbCommand, "@SistOperativo", DbType.Int32, pInfraestructura.SistOperativo);

                    vDataBase.AddInParameter(vDbCommand, "@ConLineaFija", DbType.Boolean, pInfraestructura.ConLineaFija);
                    vDataBase.AddInParameter(vDbCommand, "@ConLineaCelular", DbType.Boolean, pInfraestructura.ConLineaCelular);
                    vDataBase.AddInParameter(vDbCommand, "@ConEmail", DbType.Boolean, pInfraestructura.ConEmail);
                    vDataBase.AddInParameter(vDbCommand, "@ConAccesoInternet", DbType.Boolean, pInfraestructura.ConAccesoInternet);
                    vDataBase.AddInParameter(vDbCommand, "@OtrasSedes", DbType.Boolean, pInfraestructura.OtrasSedes);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadOtrasSedes", DbType.Int32, pInfraestructura.CantidadOtrasSedes);
                    vDataBase.AddInParameter(vDbCommand, "@SedesPropias", DbType.Boolean, pInfraestructura.SedesPropias);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInfraestructura.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfraestructura, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Eliminar Infraestructura
        /// </summary>
        /// <param name="pInfraestructura"></param>
        /// <returns></returns>
        public int EliminarInfraestructura(Infraestructura pInfraestructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Infraestructura_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInfraestructura", DbType.Int32, pInfraestructura.IdInfraestructura);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfraestructura, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Infraestructura
        /// </summary>
        /// <param name="pIdInfraestructura"></param>
        /// <returns></returns>
        public Infraestructura ConsultarInfraestructura(int pIdInfraestructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Infraestructura_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInfraestructura", DbType.Int32, pIdInfraestructura);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Infraestructura vInfraestructura = new Infraestructura();
                        while (vDataReaderResults.Read())
                        {
                            vInfraestructura.IdInfraestructura = vDataReaderResults["IdInfraestructura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfraestructura"].ToString()) : vInfraestructura.IdInfraestructura;
                            vInfraestructura.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vInfraestructura.IdVigencia;
                            vInfraestructura.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfraestructura.IdEntidad;
                            vInfraestructura.IdContacto = vDataReaderResults["IdContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContacto"].ToString()) : vInfraestructura.IdContacto;
                            vInfraestructura.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vInfraestructura.IdDireccion;
                            vInfraestructura.Personal = vDataReaderResults["Personal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Personal"].ToString()) : vInfraestructura.Personal;
                            vInfraestructura.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VincLaboral"].ToString()) : vInfraestructura.VincLaboral;
                            vInfraestructura.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PrestServicios"].ToString()) : vInfraestructura.PrestServicios;
                            vInfraestructura.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Voluntariado"].ToString()) : vInfraestructura.Voluntariado;
                            vInfraestructura.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VoluntPermanente"].ToString()) : vInfraestructura.VoluntPermanente;
                            vInfraestructura.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Asociados"].ToString()) : vInfraestructura.Asociados;
                            vInfraestructura.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Mision"].ToString()) : vInfraestructura.Mision;
                            vInfraestructura.EspacioArchivo = vDataReaderResults["EspacioArchivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EspacioArchivo"].ToString()) : vInfraestructura.EspacioArchivo;
                            vInfraestructura.EspacioMatOficina = vDataReaderResults["EspacioMatOficina"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EspacioMatOficina"].ToString()) : vInfraestructura.EspacioMatOficina;
                            vInfraestructura.EquipoComputo = vDataReaderResults["EquipoComputo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EquipoComputo"].ToString()) : vInfraestructura.EquipoComputo;
                            vInfraestructura.SistOperativo = vDataReaderResults["SistOperativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SistOperativo"].ToString()) : vInfraestructura.SistOperativo;
                            vInfraestructura.ConLineaFija = vDataReaderResults["ConLineaFija"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConLineaFija"].ToString()) : vInfraestructura.ConLineaFija;
                            vInfraestructura.ConLineaCelular = vDataReaderResults["ConLineaCelular"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConLineaCelular"].ToString()) : vInfraestructura.ConLineaCelular;
                            vInfraestructura.ConEmail = vDataReaderResults["ConEmail"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConEmail"].ToString()) : vInfraestructura.ConEmail;
                            vInfraestructura.ConAccesoInternet = vDataReaderResults["ConAccesoInternet"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConAccesoInternet"].ToString()) : vInfraestructura.ConAccesoInternet;
                            vInfraestructura.OtrasSedes = vDataReaderResults["OtrasSedes"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["OtrasSedes"].ToString()) : vInfraestructura.OtrasSedes;
                            vInfraestructura.CantidadOtrasSedes = vDataReaderResults["CantidadOtrasSedes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadOtrasSedes"].ToString()) : vInfraestructura.CantidadOtrasSedes;
                            vInfraestructura.SedesPropias = vDataReaderResults["SedesPropias"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["SedesPropias"].ToString()) : vInfraestructura.SedesPropias;
                            vInfraestructura.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfraestructura.UsuarioCrea;
                            vInfraestructura.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfraestructura.FechaCrea;
                            vInfraestructura.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfraestructura.UsuarioModifica;
                            vInfraestructura.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfraestructura.FechaModifica;
                        }
                        return vInfraestructura;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un lista de Infraestructuras
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public List<Infraestructura> ConsultarInfraestructuras(int? pIdVigencia, Decimal? pIdEntidad)
        {
            try
            {
                var vDataBase = ObtenerInstancia();
                using (var vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Infraestructuras_Consultar"))
                {
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (var vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaInfraestructura = new List<Infraestructura>();

                        while (vDataReaderResults.Read())
                        {
                            var vInfraestructura = new Infraestructura();
                            vInfraestructura.IdInfraestructura = vDataReaderResults["IdInfraestructura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfraestructura"].ToString()) : vInfraestructura.IdInfraestructura;
                            vInfraestructura.IdVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vInfraestructura.IdVigencia;
                            vInfraestructura.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfraestructura.IdEntidad;
                            vInfraestructura.IdContacto = vDataReaderResults["IdContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContacto"].ToString()) : vInfraestructura.IdContacto;
                            vInfraestructura.IdDireccion = vDataReaderResults["IdDireccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDireccion"].ToString()) : vInfraestructura.IdDireccion;
                            vInfraestructura.Personal = vDataReaderResults["Personal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Personal"].ToString()) : vInfraestructura.Personal;
                            vInfraestructura.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VincLaboral"].ToString()) : vInfraestructura.VincLaboral;
                            vInfraestructura.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PrestServicios"].ToString()) : vInfraestructura.PrestServicios;
                            vInfraestructura.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Voluntariado"].ToString()) : vInfraestructura.Voluntariado;
                            vInfraestructura.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VoluntPermanente"].ToString()) : vInfraestructura.VoluntPermanente;
                            vInfraestructura.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Asociados"].ToString()) : vInfraestructura.Asociados;
                            vInfraestructura.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Mision"].ToString()) : vInfraestructura.Mision;
                            vInfraestructura.EspacioArchivo = vDataReaderResults["EspacioArchivo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EspacioArchivo"].ToString()) : vInfraestructura.EspacioArchivo;
                            vInfraestructura.EspacioMatOficina = vDataReaderResults["EspacioMatOficina"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EspacioMatOficina"].ToString()) : vInfraestructura.EspacioMatOficina;
                            vInfraestructura.EquipoComputo = vDataReaderResults["EquipoComputo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EquipoComputo"].ToString()) : vInfraestructura.EquipoComputo;
                            vInfraestructura.SistOperativo = vDataReaderResults["SistOperativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["SistOperativo"].ToString()) : vInfraestructura.SistOperativo;
                            vInfraestructura.ConLineaFija = vDataReaderResults["ConLineaFija"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConLineaFija"].ToString()) : vInfraestructura.ConLineaFija;
                            vInfraestructura.ConLineaCelular = vDataReaderResults["ConLineaCelular"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConLineaCelular"].ToString()) : vInfraestructura.ConLineaCelular;
                            vInfraestructura.ConEmail = vDataReaderResults["ConEmail"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConEmail"].ToString()) : vInfraestructura.ConEmail;
                            vInfraestructura.ConAccesoInternet = vDataReaderResults["ConAccesoInternet"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConAccesoInternet"].ToString()) : vInfraestructura.ConAccesoInternet;
                            vInfraestructura.OtrasSedes = vDataReaderResults["OtrasSedes"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["OtrasSedes"].ToString()) : vInfraestructura.OtrasSedes;
                            vInfraestructura.CantidadOtrasSedes = vDataReaderResults["CantidadOtrasSedes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadOtrasSedes"].ToString()) : vInfraestructura.CantidadOtrasSedes;
                            vInfraestructura.SedesPropias = vDataReaderResults["SedesPropias"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["SedesPropias"].ToString()) : vInfraestructura.SedesPropias;
                            vInfraestructura.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfraestructura.UsuarioCrea;
                            vInfraestructura.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfraestructura.FechaCrea;
                            vInfraestructura.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfraestructura.UsuarioModifica;
                            vInfraestructura.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfraestructura.FechaModifica;

                            vInfraestructura.NombreContacto = vDataReaderResults["NombreContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreContacto"].ToString()) : vInfraestructura.NombreContacto;
                            vInfraestructura.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vInfraestructura.NombreDepartamento;
                            vInfraestructura.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vInfraestructura.NombreMunicipio;

                            vListaInfraestructura.Add(vInfraestructura);
                        }
                        return vListaInfraestructura;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
