using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase ComponentesDAL
    /// </summary>
    public class ComponentesDAL : GeneralDAL
    {
        public ComponentesDAL()
        {
        }
        /// <summary>
        ///  Insertar Componentes
        /// </summary>
        /// <param name="pComponentes"></param>
        /// <returns></returns>
        public int InsertarComponentes(Componentes pComponentes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CONTRATO].[ComponentesInsertar]"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdComponente", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pComponentes.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pComponentes.Estado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pComponentes.IdComponente = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdComponente").ToString());
                    GenerarLogAuditoria(pComponentes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Componentes
        /// </summary>
        /// <param name="pComponentes"></param>
        /// <returns></returns>
        public int ModificarComponentes(Componentes pComponentes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("[CONTRATO].[ComponentesActualizar]"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pComponentes.IdComponente);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pComponentes.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pComponentes.Estado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pComponentes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Componentes
        /// </summary>
        /// <param name="pComponentes"></param>
        /// <returns></returns>
        public int EliminarComponentes(Componentes pComponentes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand(""))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, pComponentes.IdComponente);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pComponentes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Componentes
        /// </summary>
        /// <param name="pIdAcuerdo"></param>
        /// <returns></returns>
        public Componentes ConsultarComponentes(int IdComponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Componentes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdComponente", DbType.Int32, IdComponente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Componentes vComponentes = new Componentes();
                        while (vDataReaderResults.Read())
                        {
                            vComponentes.IdComponente = vDataReaderResults["IdComponente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdComponente"].ToString()) : vComponentes.IdComponente;
                            vComponentes.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vComponentes.Nombre;
                            vComponentes.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Estado"].ToString()) : vComponentes.Estado;
                        }
                        return vComponentes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }       
    }
}
