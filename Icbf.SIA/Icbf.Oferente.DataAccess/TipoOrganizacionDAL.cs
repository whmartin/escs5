using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TipoOrganizacionDAL : GeneralDAL
    {
        public TipoOrganizacionDAL()
        {
        }
        public int InsertarTipoOrganizacion(TipoOrganizacion pTipoOrganizacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoOrganizacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoOrganizacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoOrganizacion", DbType.String, pTipoOrganizacion.CodigoTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoOrganizacion", DbType.String, pTipoOrganizacion.NombreTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoOrganizacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoOrganizacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoOrganizacion.IdTipoOrganizacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoOrganizacion").ToString());
                    GenerarLogAuditoria(pTipoOrganizacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoOrganizacion(TipoOrganizacion pTipoOrganizacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoOrganizacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrganizacion", DbType.Int32, pTipoOrganizacion.IdTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoOrganizacion", DbType.String, pTipoOrganizacion.CodigoTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoOrganizacion", DbType.String, pTipoOrganizacion.NombreTipoOrganizacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoOrganizacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoOrganizacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoOrganizacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoOrganizacion(TipoOrganizacion pTipoOrganizacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoOrganizacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrganizacion", DbType.Int32, pTipoOrganizacion.IdTipoOrganizacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoOrganizacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TipoOrganizacion ConsultarTipoOrganizacion(int pIdTipoOrganizacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoOrganizacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrganizacion", DbType.Int32, pIdTipoOrganizacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoOrganizacion vTipoOrganizacion = new TipoOrganizacion();
                        while (vDataReaderResults.Read())
                        {
                            vTipoOrganizacion.IdTipoOrganizacion = vDataReaderResults["IdTipoOrganizacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoOrganizacion"].ToString()) : vTipoOrganizacion.IdTipoOrganizacion;
                            vTipoOrganizacion.CodigoTipoOrganizacion = vDataReaderResults["CodigoTipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoOrganizacion"].ToString()) : vTipoOrganizacion.CodigoTipoOrganizacion;
                            vTipoOrganizacion.NombreTipoOrganizacion = vDataReaderResults["NombreTipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoOrganizacion"].ToString()) : vTipoOrganizacion.NombreTipoOrganizacion;
                            vTipoOrganizacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoOrganizacion.Estado;
                            vTipoOrganizacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoOrganizacion.UsuarioCrea;
                            vTipoOrganizacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoOrganizacion.FechaCrea;
                            vTipoOrganizacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoOrganizacion.UsuarioModifica;
                            vTipoOrganizacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoOrganizacion.FechaModifica;
                        }
                        return vTipoOrganizacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoOrganizacion> ConsultarTipoOrganizacions(String pCodigoTipoOrganizacion, String pNombreTipoOrganizacion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoOrganizacions_Consultar"))
                {
                    if(pCodigoTipoOrganizacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipoOrganizacion", DbType.String, pCodigoTipoOrganizacion);
                    if(pNombreTipoOrganizacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoOrganizacion", DbType.String, pNombreTipoOrganizacion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoOrganizacion> vListaTipoOrganizacion = new List<TipoOrganizacion>();
                        while (vDataReaderResults.Read())
                        {
                                TipoOrganizacion vTipoOrganizacion = new TipoOrganizacion();
                            vTipoOrganizacion.IdTipoOrganizacion = vDataReaderResults["IdTipoOrganizacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoOrganizacion"].ToString()) : vTipoOrganizacion.IdTipoOrganizacion;
                            vTipoOrganizacion.CodigoTipoOrganizacion = vDataReaderResults["CodigoTipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoOrganizacion"].ToString()) : vTipoOrganizacion.CodigoTipoOrganizacion;
                            vTipoOrganizacion.NombreTipoOrganizacion = vDataReaderResults["NombreTipoOrganizacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoOrganizacion"].ToString()) : vTipoOrganizacion.NombreTipoOrganizacion;
                            vTipoOrganizacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoOrganizacion.Estado;
                            if (vTipoOrganizacion.Estado == true)
                                vTipoOrganizacion.DescripcionEstado = "Activo";
                            else
                                vTipoOrganizacion.DescripcionEstado = "Inactivo";
                            vTipoOrganizacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoOrganizacion.UsuarioCrea;
                            vTipoOrganizacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoOrganizacion.FechaCrea;
                            vTipoOrganizacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoOrganizacion.UsuarioModifica;
                            vTipoOrganizacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoOrganizacion.FechaModifica;
                                vListaTipoOrganizacion.Add(vTipoOrganizacion);
                        }
                        return vListaTipoOrganizacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
