using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ZonaDAL : GeneralDAL
    {
        public ZonaDAL()
        {
        }
        public int InsertarZona(Zona pZona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Zona_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdZona", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoZona", DbType.String, pZona.CodigoZona);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZona", DbType.String, pZona.NombreZona);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pZona.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pZona.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pZona.IdZona = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdZona").ToString());
                    GenerarLogAuditoria(pZona, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarZona(Zona pZona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Zona_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pZona.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoZona", DbType.String, pZona.CodigoZona);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZona", DbType.String, pZona.NombreZona);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pZona.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pZona.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pZona, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarZona(Zona pZona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Zona_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pZona.IdZona);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pZona, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Zona ConsultarZona(int pIdZona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Zona_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pIdZona);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Zona vZona = new Zona();
                        while (vDataReaderResults.Read())
                        {
                            vZona.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vZona.IdZona;
                            vZona.CodigoZona = vDataReaderResults["CodigoZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoZona"].ToString()) : vZona.CodigoZona;
                            vZona.NombreZona = vDataReaderResults["NombreZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZona"].ToString()) : vZona.NombreZona;
                            vZona.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vZona.Estado;
                            vZona.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vZona.UsuarioCrea;
                            vZona.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vZona.FechaCrea;
                            vZona.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vZona.UsuarioModifica;
                            vZona.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vZona.FechaModifica;
                        }
                        return vZona;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Zona> ConsultarZonas(String pCodigoZona, String pNombreZona, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Zonas_Consultar"))
                {
                    if(pCodigoZona != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoZona", DbType.String, pCodigoZona);
                    if(pNombreZona != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreZona", DbType.String, pNombreZona);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Zona> vListaZona = new List<Zona>();
                        while (vDataReaderResults.Read())
                        {
                                Zona vZona = new Zona();
                            vZona.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vZona.IdZona;
                            vZona.CodigoZona = vDataReaderResults["CodigoZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoZona"].ToString()) : vZona.CodigoZona;
                            vZona.NombreZona = vDataReaderResults["NombreZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZona"].ToString()) : vZona.NombreZona;
                            vZona.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vZona.Estado;
                            if (vZona.Estado == true)
                                vZona.DescripcionEstado = "Activo";
                            else
                                vZona.DescripcionEstado = "Inactivo";
                            vZona.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vZona.UsuarioCrea;
                            vZona.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vZona.FechaCrea;
                            vZona.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vZona.UsuarioModifica;
                            vZona.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vZona.FechaModifica;
                                vListaZona.Add(vZona);
                        }
                        return vListaZona;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
