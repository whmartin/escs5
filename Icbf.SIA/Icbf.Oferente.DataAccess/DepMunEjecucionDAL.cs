using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class DepMunEjecucionDAL : GeneralDAL
    {
        public DepMunEjecucionDAL()
        {
        }
        public int InsertarDepMunEjecucion(DepMunEjecucion pDepMunEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepMunEjecucion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDepMunEjecucion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pDepMunEjecucion.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pDepMunEjecucion.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pDepMunEjecucion.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Cupos", DbType.Int32, pDepMunEjecucion.Cupos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDepMunEjecucion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDepMunEjecucion.IdDepMunEjecucion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDepMunEjecucion").ToString());
                    GenerarLogAuditoria(pDepMunEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarDepMunEjecucion(DepMunEjecucion pDepMunEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepMunEjecucion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDepMunEjecucion", DbType.Int32, pDepMunEjecucion.IdDepMunEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pDepMunEjecucion.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pDepMunEjecucion.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pDepMunEjecucion.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Cupos", DbType.Int32, pDepMunEjecucion.Cupos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDepMunEjecucion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDepMunEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarDepMunEjecucion(DepMunEjecucion pDepMunEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepMunEjecucion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDepMunEjecucion", DbType.Int32, pDepMunEjecucion.IdDepMunEjecucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDepMunEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public DepMunEjecucion ConsultarDepMunEjecucion(int pIdDepMunEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepMunEjecucion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDepMunEjecucion", DbType.Int32, pIdDepMunEjecucion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DepMunEjecucion vDepMunEjecucion = new DepMunEjecucion();
                        while (vDataReaderResults.Read())
                        {
                            vDepMunEjecucion.IdDepMunEjecucion = vDataReaderResults["IdDepMunEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepMunEjecucion"].ToString()) : vDepMunEjecucion.IdDepMunEjecucion;
                            vDepMunEjecucion.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDepMunEjecucion.IdExpEntidad;
                            vDepMunEjecucion.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDepMunEjecucion.IdDepartamento;
                            vDepMunEjecucion.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDepMunEjecucion.NombreDepartamento;
                            vDepMunEjecucion.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vDepMunEjecucion.IdMunicipio;
                            vDepMunEjecucion.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vDepMunEjecucion.NombreMunicipio;
                            vDepMunEjecucion.Cupos = vDataReaderResults["Cupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cupos"].ToString()) : vDepMunEjecucion.Cupos;
                            vDepMunEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDepMunEjecucion.UsuarioCrea;
                            vDepMunEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDepMunEjecucion.FechaCrea;
                            vDepMunEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDepMunEjecucion.UsuarioModifica;
                            vDepMunEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDepMunEjecucion.FechaModifica;
                        }
                        return vDepMunEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DepMunEjecucion> ConsultarDepMunEjecucions(int vIdExpEntidad, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepMunEjecucions_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, vIdExpEntidad);
                    if(pIdDepartamento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if(pIdMunicipio != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DepMunEjecucion> vListaDepMunEjecucion = new List<DepMunEjecucion>();
                        while (vDataReaderResults.Read())
                        {
                                DepMunEjecucion vDepMunEjecucion = new DepMunEjecucion();
                            vDepMunEjecucion.IdDepMunEjecucion = vDataReaderResults["IdDepMunEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepMunEjecucion"].ToString()) : vDepMunEjecucion.IdDepMunEjecucion;
                            vDepMunEjecucion.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDepMunEjecucion.IdExpEntidad;
                            vDepMunEjecucion.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDepMunEjecucion.IdDepartamento;
                            vDepMunEjecucion.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDepMunEjecucion.NombreDepartamento;
                            vDepMunEjecucion.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vDepMunEjecucion.IdMunicipio;
                            vDepMunEjecucion.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vDepMunEjecucion.NombreMunicipio;
                            vDepMunEjecucion.Cupos = vDataReaderResults["Cupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cupos"].ToString()) : vDepMunEjecucion.Cupos;
                            vDepMunEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDepMunEjecucion.UsuarioCrea;
                            vDepMunEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDepMunEjecucion.FechaCrea;
                            vDepMunEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDepMunEjecucion.UsuarioModifica;
                            vDepMunEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDepMunEjecucion.FechaModifica;
                                vListaDepMunEjecucion.Add(vDepMunEjecucion);
                        }
                        return vListaDepMunEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarDepMunEjecucionxDepartamento(int pIdExpEntidad, int pIdDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepMunEjecucion_EliminarxDepartamento"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pIdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIdExpEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}