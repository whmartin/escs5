﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.DataAccess
{
    public class ObservacionesConvocatoriaDAL : GeneralDAL
    {
        public ObservacionesConvocatoriaDAL() { }

        public List<ObservacionesConvocatoria> ConsultarObservacionessConvocatoria(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ObservacionessConvocatoria_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObservacionesConvocatoria> vListaObservacionesConvocatoria = new List<ObservacionesConvocatoria>();
                        while (vDataReaderResults.Read())
                        {
                            ObservacionesConvocatoria vObservacionesConvocatoria = new ObservacionesConvocatoria();
                            
                            vObservacionesConvocatoria.IdObservacionConvocatoria = vDataReaderResults["IdObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObservacionConvocatoria"].ToString()) : vObservacionesConvocatoria.IdObservacionConvocatoria;
                            vObservacionesConvocatoria.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vObservacionesConvocatoria.IdTipoObservacion;
                            vObservacionesConvocatoria.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vObservacionesConvocatoria.IdConvocatoria;
                            vObservacionesConvocatoria.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? vDataReaderResults["NombreSolicitante"].ToString() : vObservacionesConvocatoria.NombreSolicitante;
                            vObservacionesConvocatoria.CorreoSolicitante = vDataReaderResults["CorreoSolicitante"] != DBNull.Value ? vDataReaderResults["CorreoSolicitante"].ToString() : vObservacionesConvocatoria.CorreoSolicitante;
                            vObservacionesConvocatoria.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? vDataReaderResults["Observacion"].ToString() : vObservacionesConvocatoria.Observacion;
                            vObservacionesConvocatoria.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vObservacionesConvocatoria.IdUsuario;
                            vObservacionesConvocatoria.Coordinador = vDataReaderResults["Coordinador"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Coordinador"].ToString()) : vObservacionesConvocatoria.Coordinador;
                            vObservacionesConvocatoria.IdEstadoObservacion = vDataReaderResults["IdEstadoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoObservacion"].ToString()) : vObservacionesConvocatoria.IdEstadoObservacion;

                            vListaObservacionesConvocatoria.Add(vObservacionesConvocatoria);
                        }
                        return vListaObservacionesConvocatoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
