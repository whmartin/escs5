using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase EntidadProvOferenteDAL
    /// </summary>
    public class EntidadProvOferenteDAL : GeneralDAL
    {
        public EntidadProvOferenteDAL()
        {
        }
        /// <summary>
        /// Inserta una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int InsertarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@TIPOENTOFPROV", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTERCERO", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTERCEROREPLEGAL", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOCIIUPRINCIPAL", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOCIIUSECUNDARIO", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOREGTRIB", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOSECTOR", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOACTIVIDAD", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOCLASEENTIDAD", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPORAMAPUBLICA", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOENTIDAD", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPONIVELGOB", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPONIVELORGANIZACIONAL", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOORIGENCAPITAL", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOCERTIFICADORCALIDAD", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPOCERTIFICATAMANO", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPONATURALEZAJURID", DbType.Int32, 18);
                    vDataBase.AddOutParameter(vDbCommand, "@IDTIPORANGOSACTIVOS", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@FECHACIIUPRINCIPAL", DbType.DateTime, pEntidadProvOferente.FECHACIIUPRINCIPAL);
                    vDataBase.AddInParameter(vDbCommand, "@FECHACIIUSECUNDARIO", DbType.DateTime, pEntidadProvOferente.FECHACIIUSECUNDARIO);
                    vDataBase.AddInParameter(vDbCommand, "@SITIOWEB", DbType.String, pEntidadProvOferente.SITIOWEB);
                    vDataBase.AddInParameter(vDbCommand, "@FECHACONSTITUCION", DbType.DateTime, pEntidadProvOferente.FECHACONSTITUCION);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAVIGENCIA", DbType.DateTime, pEntidadProvOferente.FECHAVIGENCIA);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAMATRICULAMERC", DbType.DateTime, pEntidadProvOferente.FECHAMATRICULAMERC);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAEXPIRACION", DbType.DateTime, pEntidadProvOferente.FECHAEXPIRACION);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAEXPIRACION", DbType.DateTime, pEntidadProvOferente.FECHAEXPIRACION);
                    vDataBase.AddInParameter(vDbCommand, "@NOMBRECOMERCIAL", DbType.DateTime, pEntidadProvOferente.NOMBRECOMERCIAL);
                    vDataBase.AddInParameter(vDbCommand, "@EXENMATRICULAMER", DbType.Boolean, pEntidadProvOferente.EXENMATRICULAMER);
                    vDataBase.AddInParameter(vDbCommand, "@MATRICULAMERCANTIL", DbType.String, pEntidadProvOferente.MATRICULAMERCANTIL);
                    vDataBase.AddInParameter(vDbCommand, "@OBSERVALIDADOR", DbType.String, pEntidadProvOferente.OBSERVALIDADOR);
                    vDataBase.AddInParameter(vDbCommand, "@INSCRITORUP", DbType.Boolean, pEntidadProvOferente.INSCRITORUP);
                    vDataBase.AddInParameter(vDbCommand, "@NUMRUP", DbType.Int32, pEntidadProvOferente.NUMRUP);
                    vDataBase.AddInParameter(vDbCommand, "@NOMBREENTIDADACREDITADORA", DbType.String, pEntidadProvOferente.NOMBREENTIDADACREDITADORA);
                    vDataBase.AddInParameter(vDbCommand, "@PORCTJPRIVADO", DbType.Decimal, pEntidadProvOferente.PORCTJPRIVADO);
                    vDataBase.AddInParameter(vDbCommand, "@PORCTJPUBLICO", DbType.Decimal, pEntidadProvOferente.PORCTJPUBLICO);
                    vDataBase.AddInParameter(vDbCommand, "@NUMTOTALTRABAJADORES", DbType.Int32, pEntidadProvOferente.NUMTOTALTRABAJADORES);
                    vDataBase.AddInParameter(vDbCommand, "@ANOREGISTRO", DbType.Int32, pEntidadProvOferente.ANOREGISTRO);
                    vDataBase.AddInParameter(vDbCommand, "@SIGLA", DbType.String, pEntidadProvOferente.SIGLA);
                    vDataBase.AddInParameter(vDbCommand, "@RUPOBALANCE", DbType.Boolean, pEntidadProvOferente.RUPOBALANCE);
                    vDataBase.AddInParameter(vDbCommand, "@ESPACIOARCHIVO", DbType.Boolean, pEntidadProvOferente.ESPACIOARCHIVO);
                    vDataBase.AddInParameter(vDbCommand, "@ESPACIOMATOFICINA", DbType.Boolean, pEntidadProvOferente.ESPACIOMATOFICINA);
                    vDataBase.AddInParameter(vDbCommand, "@EQUIPOCOMPUTO", DbType.Boolean, pEntidadProvOferente.EQUIPOCOMPUTO);
                    vDataBase.AddInParameter(vDbCommand, "@SISTOPERATIVO", DbType.String, pEntidadProvOferente.SISTOPERATIVO);
                    vDataBase.AddInParameter(vDbCommand, "@CONLINEAFIJA", DbType.Boolean, pEntidadProvOferente.CONLINEAFIJA);
                    vDataBase.AddInParameter(vDbCommand, "@CONLINEACELULAR", DbType.Boolean, pEntidadProvOferente.CONLINEACELULAR);
                    vDataBase.AddInParameter(vDbCommand, "@CONEMAIL", DbType.Boolean, pEntidadProvOferente.CONEMAIL);
                    vDataBase.AddInParameter(vDbCommand, "@CONACCESOINTERNET", DbType.Boolean, pEntidadProvOferente.CONACCESOINTERNET);
                    vDataBase.AddInParameter(vDbCommand, "@NUMSEDES", DbType.Int32, pEntidadProvOferente.NUMSEDES);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEntidadProvOferente.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEntidadProvOferente.IDENTIDAD = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDENTIDAD").ToString());
                    pEntidadProvOferente.TIPOENTOFPROV = Convert.ToBoolean(vDataBase.GetParameterValue(vDbCommand, "@TIPOENTOFPROV").ToString());
                    pEntidadProvOferente.IDTERCERO = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTERCERO").ToString());
                    pEntidadProvOferente.IDTERCEROREPLEGAL = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTERCEROREPLEGAL").ToString());
                    pEntidadProvOferente.IDTIPOCIIUPRINCIPAL = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOCIIUPRINCIPAL").ToString());
                    pEntidadProvOferente.IDTIPOCIIUSECUNDARIO = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOCIIUSECUNDARIO").ToString());
                    pEntidadProvOferente.IDTIPOREGTRIB = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOREGTRIB").ToString());
                    pEntidadProvOferente.IDTIPOSECTOR = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOSECTOR").ToString());
                    pEntidadProvOferente.IDTIPOACTIVIDAD = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOACTIVIDAD").ToString());
                    pEntidadProvOferente.IDTIPOCLASEENTIDAD = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOCLASEENTIDAD").ToString());
                    pEntidadProvOferente.IDTIPORAMAPUBLICA = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPORAMAPUBLICA").ToString());
                    pEntidadProvOferente.IDTIPOENTIDAD = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOENTIDAD").ToString());
                    pEntidadProvOferente.IDTIPONIVELGOB = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPONIVELGOB").ToString());
                    pEntidadProvOferente.IDTIPONIVELORGANIZACIONAL = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPONIVELORGANIZACIONAL").ToString());
                    pEntidadProvOferente.IDTIPOORIGENCAPITAL = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOORIGENCAPITAL").ToString());
                    pEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOCERTIFICADORCALIDAD").ToString());
                    pEntidadProvOferente.IDTIPOCERTIFICATAMANO = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPOCERTIFICATAMANO").ToString());
                    pEntidadProvOferente.IDTIPONATURALEZAJURID = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPONATURALEZAJURID").ToString());
                    pEntidadProvOferente.IDTIPORANGOSACTIVOS = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDTIPORANGOSACTIVOS").ToString());
                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
       

        /// <summary>
        /// Cambiar Estado de una Entidad ProvOferente
        /// </summary>
        /// <param name="pEstadoValidacion"></param>
        /// <param name="pIdEntidadProvOferente"></param>
        /// <returns></returns>
        public int CambiarEstadoEntidadProvOferente(int pIdEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_CambiarEstado"))
                {
                    int vResultado;
                    
                    
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pIdEntidadProvOferente);
                  
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand); 

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cambiar Estado Entidad ProvOferente Validacion
        /// </summary>
        /// <param name="pEstadoValidacion"></param>
        /// <param name="pIdEntidadProvOferente"></param>
        /// <returns></returns>
        public int CambiarEstadoEntidadProvOferenteValidacion(string pEstadoValidacion, int pIdEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_CambiarEstado_Validacion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@EstadoValidacion", DbType.String, pEstadoValidacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pIdEntidadProvOferente);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Modifica una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>       
        public int ModificarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pEntidadProvOferente.IDENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@TIPOENTOFPROV", DbType.Int32, pEntidadProvOferente.TIPOENTOFPROV);
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCERO", DbType.Int32, pEntidadProvOferente.IDTERCERO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCEROREPLEGAL", DbType.Int32, pEntidadProvOferente.IDTERCEROREPLEGAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCIIUPRINCIPAL", DbType.Int32, pEntidadProvOferente.IDTIPOCIIUPRINCIPAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCIIUSECUNDARIO", DbType.Int32, pEntidadProvOferente.IDTIPOCIIUSECUNDARIO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOREGTRIB", DbType.Int32, pEntidadProvOferente.IDTIPOREGTRIB);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOSECTOR", DbType.Int32, pEntidadProvOferente.IDTIPOSECTOR);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOACTIVIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOACTIVIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCLASEENTIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOCLASEENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPORAMAPUBLICA", DbType.Int32, pEntidadProvOferente.IDTIPORAMAPUBLICA);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOENTIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONIVELGOB", DbType.Int32, pEntidadProvOferente.IDTIPONIVELGOB);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONIVELORGANIZACIONAL", DbType.Int32, pEntidadProvOferente.IDTIPONIVELORGANIZACIONAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOORIGENCAPITAL", DbType.Int32, pEntidadProvOferente.IDTIPOORIGENCAPITAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCERTIFICADORCALIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCERTIFICATAMANO", DbType.Int32, pEntidadProvOferente.IDTIPOCERTIFICATAMANO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONATURALEZAJURID", DbType.Int32, pEntidadProvOferente.IDTIPONATURALEZAJURID);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPORANGOSACTIVOS", DbType.Int32, pEntidadProvOferente.IDTIPORANGOSACTIVOS);
                    vDataBase.AddInParameter(vDbCommand, "@FECHACIIUPRINCIPAL", DbType.DateTime, pEntidadProvOferente.FECHACIIUPRINCIPAL);
                    vDataBase.AddInParameter(vDbCommand, "@FECHACIIUSECUNDARIO", DbType.DateTime, pEntidadProvOferente.FECHACIIUSECUNDARIO);
                    vDataBase.AddInParameter(vDbCommand, "@SITIOWEB", DbType.String, pEntidadProvOferente.SITIOWEB);
                    vDataBase.AddInParameter(vDbCommand, "@FECHACONSTITUCION", DbType.DateTime, pEntidadProvOferente.FECHACONSTITUCION);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAVIGENCIA", DbType.DateTime, pEntidadProvOferente.FECHAVIGENCIA);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAMATRICULAMERC", DbType.DateTime, pEntidadProvOferente.FECHAMATRICULAMERC);
                    vDataBase.AddInParameter(vDbCommand, "@FECHAEXPIRACION", DbType.DateTime, pEntidadProvOferente.FECHAEXPIRACION);
                    vDataBase.AddInParameter(vDbCommand, "@NOMBRECOMERCIAL", DbType.DateTime, pEntidadProvOferente.NOMBRECOMERCIAL);
                    vDataBase.AddInParameter(vDbCommand, "@EXENMATRICULAMER", DbType.Boolean, pEntidadProvOferente.EXENMATRICULAMER);
                    vDataBase.AddInParameter(vDbCommand, "@MATRICULAMERCANTIL", DbType.String, pEntidadProvOferente.MATRICULAMERCANTIL);
                    vDataBase.AddInParameter(vDbCommand, "@OBSERVALIDADOR", DbType.String, pEntidadProvOferente.OBSERVALIDADOR);
                    vDataBase.AddInParameter(vDbCommand, "@INSCRITORUP", DbType.Boolean, pEntidadProvOferente.INSCRITORUP);
                    vDataBase.AddInParameter(vDbCommand, "@NUMRUP", DbType.Int32, pEntidadProvOferente.NUMRUP);
                    vDataBase.AddInParameter(vDbCommand, "@NOMBREENTIDADACREDITADORA", DbType.String, pEntidadProvOferente.NOMBREENTIDADACREDITADORA);
                    vDataBase.AddInParameter(vDbCommand, "@PORCTJPRIVADO", DbType.String, pEntidadProvOferente.PORCTJPRIVADO);
                    vDataBase.AddInParameter(vDbCommand, "@PORCTJPUBLICO", DbType.String, pEntidadProvOferente.PORCTJPUBLICO);
                    vDataBase.AddInParameter(vDbCommand, "@NUMTOTALTRABAJADORES", DbType.Int32, pEntidadProvOferente.NUMTOTALTRABAJADORES);
                    vDataBase.AddInParameter(vDbCommand, "@ANOREGISTRO", DbType.Int32, pEntidadProvOferente.ANOREGISTRO);
                    vDataBase.AddInParameter(vDbCommand, "@SIGLA", DbType.String, pEntidadProvOferente.SIGLA);
                    vDataBase.AddInParameter(vDbCommand, "@RUPOBALANCE", DbType.Boolean, pEntidadProvOferente.RUPOBALANCE);
                    vDataBase.AddInParameter(vDbCommand, "@ESPACIOARCHIVO", DbType.Boolean, pEntidadProvOferente.ESPACIOARCHIVO);
                    vDataBase.AddInParameter(vDbCommand, "@ESPACIOMATOFICINA", DbType.Boolean, pEntidadProvOferente.ESPACIOMATOFICINA);
                    vDataBase.AddInParameter(vDbCommand, "@EQUIPOCOMPUTO", DbType.Boolean, pEntidadProvOferente.EQUIPOCOMPUTO);
                    vDataBase.AddInParameter(vDbCommand, "@SISTOPERATIVO", DbType.String, pEntidadProvOferente.SISTOPERATIVO);
                    vDataBase.AddInParameter(vDbCommand, "@CONLINEAFIJA", DbType.Boolean, pEntidadProvOferente.CONLINEAFIJA);
                    vDataBase.AddInParameter(vDbCommand, "@CONLINEACELULAR", DbType.Boolean, pEntidadProvOferente.CONLINEACELULAR);
                    vDataBase.AddInParameter(vDbCommand, "@CONEMAIL", DbType.Boolean, pEntidadProvOferente.CONEMAIL);
                    vDataBase.AddInParameter(vDbCommand, "@CONACCESOINTERNET", DbType.Boolean, pEntidadProvOferente.CONACCESOINTERNET);
                    vDataBase.AddInParameter(vDbCommand, "@NUMSEDES", DbType.Int32, pEntidadProvOferente.NUMSEDES);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEntidadProvOferente.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (!string.IsNullOrEmpty(vDataBase.GetParameterValue(vDbCommand, "@IDENTIDAD").ToString()))
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDENTIDAD").ToString());
                    }

                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Elimina una Entidad ProvOferente
        /// </summary>
        /// <param name="pEntidadProvOferente"></param>
        /// <returns></returns>
        public int EliminarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pEntidadProvOferente.IDENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@TIPOENTOFPROV", DbType.Int32, pEntidadProvOferente.TIPOENTOFPROV);
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCERO", DbType.Int32, pEntidadProvOferente.IDTERCERO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCEROREPLEGAL", DbType.Int32, pEntidadProvOferente.IDTERCEROREPLEGAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCIIUPRINCIPAL", DbType.Int32, pEntidadProvOferente.IDTIPOCIIUPRINCIPAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCIIUSECUNDARIO", DbType.Int32, pEntidadProvOferente.IDTIPOCIIUSECUNDARIO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOREGTRIB", DbType.Int32, pEntidadProvOferente.IDTIPOREGTRIB);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOSECTOR", DbType.Int32, pEntidadProvOferente.IDTIPOSECTOR);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOACTIVIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOACTIVIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCLASEENTIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOCLASEENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPORAMAPUBLICA", DbType.Int32, pEntidadProvOferente.IDTIPORAMAPUBLICA);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOENTIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONIVELGOB", DbType.Int32, pEntidadProvOferente.IDTIPONIVELGOB);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONIVELORGANIZACIONAL", DbType.Int32, pEntidadProvOferente.IDTIPONIVELORGANIZACIONAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOORIGENCAPITAL", DbType.Int32, pEntidadProvOferente.IDTIPOORIGENCAPITAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCERTIFICADORCALIDAD", DbType.Int32, pEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCERTIFICATAMANO", DbType.Int32, pEntidadProvOferente.IDTIPOCERTIFICATAMANO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONATURALEZAJURID", DbType.Int32, pEntidadProvOferente.IDTIPONATURALEZAJURID);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPORANGOSACTIVOS", DbType.Int32, pEntidadProvOferente.IDTIPORANGOSACTIVOS);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDENTIDAD"></param>
        /// <param name="pTIPOENTOFPROV"></param>
        /// <param name="pIDTERCERO"></param>
        /// <param name="pIDTERCEROREPLEGAL"></param>
        /// <param name="pIDTIPOCIIUPRINCIPAL"></param>
        /// <param name="pIDTIPOCIIUSECUNDARIO"></param>
        /// <param name="pIDTIPOREGTRIB"></param>
        /// <param name="pIDTIPOSECTOR"></param>
        /// <param name="pIDTIPOACTIVIDAD"></param>
        /// <param name="pIDTIPOCLASEENTIDAD"></param>
        /// <param name="pIDTIPORAMAPUBLICA"></param>
        /// <param name="pIDTIPOENTIDAD"></param>
        /// <param name="pIDTIPONIVELGOB"></param>
        /// <param name="pIDTIPONIVELORGANIZACIONAL"></param>
        /// <param name="pIDTIPOORIGENCAPITAL"></param>
        /// <param name="pIDTIPOCERTIFICADORCALIDAD"></param>
        /// <param name="pIDTIPOCERTIFICATAMANO"></param>
        /// <param name="pIDTIPONATURALEZAJURID"></param>
        /// <param name="pIDTIPORANGOSACTIVOS"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIDENTIDAD, int pTIPOENTOFPROV, int pIDTERCERO, int pIDTERCEROREPLEGAL, int pIDTIPOCIIUPRINCIPAL, int pIDTIPOCIIUSECUNDARIO, int pIDTIPOREGTRIB, int pIDTIPOSECTOR, int pIDTIPOACTIVIDAD, int pIDTIPOCLASEENTIDAD, int pIDTIPORAMAPUBLICA, int pIDTIPOENTIDAD, int pIDTIPONIVELGOB, int pIDTIPONIVELORGANIZACIONAL, int pIDTIPOORIGENCAPITAL, int pIDTIPOCERTIFICADORCALIDAD, int pIDTIPOCERTIFICATAMANO, int pIDTIPONATURALEZAJURID, int pIDTIPORANGOSACTIVOS)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pIDENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@TIPOENTOFPROV", DbType.Int32, pTIPOENTOFPROV);
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCERO", DbType.Int32, pIDTERCERO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCEROREPLEGAL", DbType.Int32, pIDTERCEROREPLEGAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCIIUPRINCIPAL", DbType.Int32, pIDTIPOCIIUPRINCIPAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCIIUSECUNDARIO", DbType.Int32, pIDTIPOCIIUSECUNDARIO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOREGTRIB", DbType.Int32, pIDTIPOREGTRIB);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOSECTOR", DbType.Int32, pIDTIPOSECTOR);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOACTIVIDAD", DbType.Int32, pIDTIPOACTIVIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCLASEENTIDAD", DbType.Int32, pIDTIPOCLASEENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPORAMAPUBLICA", DbType.Int32, pIDTIPORAMAPUBLICA);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOENTIDAD", DbType.Int32, pIDTIPOENTIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONIVELGOB", DbType.Int32, pIDTIPONIVELGOB);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONIVELORGANIZACIONAL", DbType.Int32, pIDTIPONIVELORGANIZACIONAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOORIGENCAPITAL", DbType.Int32, pIDTIPOORIGENCAPITAL);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCERTIFICADORCALIDAD", DbType.Int32, pIDTIPOCERTIFICADORCALIDAD);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPOCERTIFICATAMANO", DbType.Int32, pIDTIPOCERTIFICATAMANO);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPONATURALEZAJURID", DbType.Int32, pIDTIPONATURALEZAJURID);
                    vDataBase.AddInParameter(vDbCommand, "@IDTIPORANGOSACTIVOS", DbType.Int32, pIDTIPORANGOSACTIVOS);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
                        while (vDataReaderResults.Read())
                        {
                            vEntidadProvOferente.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vEntidadProvOferente.IDENTIDAD;
                            vEntidadProvOferente.TIPOENTOFPROV = vDataReaderResults["TIPOENTOFPROV"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TIPOENTOFPROV"].ToString()) : vEntidadProvOferente.TIPOENTOFPROV;
                            vEntidadProvOferente.IDTERCERO = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vEntidadProvOferente.IDTERCERO;
                            vEntidadProvOferente.IDTERCEROREPLEGAL = vDataReaderResults["IDTERCEROREPLEGAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCEROREPLEGAL"].ToString()) : vEntidadProvOferente.IDTERCEROREPLEGAL;
                            vEntidadProvOferente.IDTIPOCIIUPRINCIPAL = vDataReaderResults["IDTIPOCIIUPRINCIPAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.IDTIPOCIIUPRINCIPAL;
                            vEntidadProvOferente.IDTIPOCIIUSECUNDARIO = vDataReaderResults["IDTIPOCIIUSECUNDARIO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.IDTIPOCIIUSECUNDARIO;
                            vEntidadProvOferente.IDTIPOREGTRIB = vDataReaderResults["IDTIPOREGTRIB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOREGTRIB"].ToString()) : vEntidadProvOferente.IDTIPOREGTRIB;
                            vEntidadProvOferente.IDTIPOSECTOR = vDataReaderResults["IDTIPOSECTOR"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOSECTOR"].ToString()) : vEntidadProvOferente.IDTIPOSECTOR;
                            vEntidadProvOferente.IDTIPOACTIVIDAD = vDataReaderResults["IDTIPOACTIVIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOACTIVIDAD"].ToString()) : vEntidadProvOferente.IDTIPOACTIVIDAD;
                            vEntidadProvOferente.IDTIPOCLASEENTIDAD = vDataReaderResults["IDTIPOCLASEENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCLASEENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCLASEENTIDAD;
                            vEntidadProvOferente.IDTIPORAMAPUBLICA = vDataReaderResults["IDTIPORAMAPUBLICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORAMAPUBLICA"].ToString()) : vEntidadProvOferente.IDTIPORAMAPUBLICA;
                            vEntidadProvOferente.IDTIPOENTIDAD = vDataReaderResults["IDTIPOENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOENTIDAD;
                            vEntidadProvOferente.IDTIPONIVELGOB = vDataReaderResults["IDTIPONIVELGOB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELGOB"].ToString()) : vEntidadProvOferente.IDTIPONIVELGOB;
                            vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL = vDataReaderResults["IDTIPONIVELORGANIZACIONAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELORGANIZACIONAL"].ToString()) : vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL;
                            vEntidadProvOferente.IDTIPOORIGENCAPITAL = vDataReaderResults["IDTIPOORIGENCAPITAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOORIGENCAPITAL"].ToString()) : vEntidadProvOferente.IDTIPOORIGENCAPITAL;
                            vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD = vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD;
                            vEntidadProvOferente.IDTIPOCERTIFICATAMANO = vDataReaderResults["IDTIPOCERTIFICATAMANO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICATAMANO"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICATAMANO;
                            vEntidadProvOferente.IDTIPONATURALEZAJURID = vDataReaderResults["IDTIPONATURALEZAJURID"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONATURALEZAJURID"].ToString()) : vEntidadProvOferente.IDTIPONATURALEZAJURID;
                            vEntidadProvOferente.IDTIPORANGOSACTIVOS = vDataReaderResults["IDTIPORANGOSACTIVOS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORANGOSACTIVOS"].ToString()) : vEntidadProvOferente.IDTIPORANGOSACTIVOS;
                            vEntidadProvOferente.FECHACIIUPRINCIPAL = vDataReaderResults["FECHACIIUPRINCIPAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.FECHACIIUPRINCIPAL;
                            vEntidadProvOferente.FECHACIIUSECUNDARIO = vDataReaderResults["FECHACIIUSECUNDARIO"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.FECHACIIUSECUNDARIO;
                            vEntidadProvOferente.SITIOWEB = vDataReaderResults["SITIOWEB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SITIOWEB"].ToString()) : vEntidadProvOferente.SITIOWEB;
                            vEntidadProvOferente.FECHACONSTITUCION = vDataReaderResults["FECHACONSTITUCION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACONSTITUCION"].ToString()) : vEntidadProvOferente.FECHACONSTITUCION;
                            vEntidadProvOferente.FECHAVIGENCIA = vDataReaderResults["FECHAVIGENCIA"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAVIGENCIA"].ToString()) : vEntidadProvOferente.FECHAVIGENCIA;
                            vEntidadProvOferente.FECHAMATRICULAMERC = vDataReaderResults["FECHAMATRICULAMERC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAMATRICULAMERC"].ToString()) : vEntidadProvOferente.FECHAMATRICULAMERC;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.NOMBRECOMERCIAL = vDataReaderResults["NOMBRECOMERCIAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["NOMBRECOMERCIAL"].ToString()) : vEntidadProvOferente.NOMBRECOMERCIAL;
                            vEntidadProvOferente.EXENMATRICULAMER = vDataReaderResults["EXENMATRICULAMER"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EXENMATRICULAMER"].ToString()) : vEntidadProvOferente.EXENMATRICULAMER;
                            vEntidadProvOferente.MATRICULAMERCANTIL = vDataReaderResults["MATRICULAMERCANTIL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MATRICULAMERCANTIL"].ToString()) : vEntidadProvOferente.MATRICULAMERCANTIL;
                            vEntidadProvOferente.OBSERVALIDADOR = vDataReaderResults["OBSERVALIDADOR"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OBSERVALIDADOR"].ToString()) : vEntidadProvOferente.OBSERVALIDADOR;
                            vEntidadProvOferente.INSCRITORUP = vDataReaderResults["INSCRITORUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["INSCRITORUP"].ToString()) : vEntidadProvOferente.INSCRITORUP;
                            vEntidadProvOferente.NUMRUP = vDataReaderResults["NUMRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMRUP"].ToString()) : vEntidadProvOferente.NUMRUP;
                            vEntidadProvOferente.NOMBREENTIDADACREDITADORA = vDataReaderResults["NOMBREENTIDADACREDITADORA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBREENTIDADACREDITADORA"].ToString()) : vEntidadProvOferente.NOMBREENTIDADACREDITADORA;
                            vEntidadProvOferente.PORCTJPRIVADO = vDataReaderResults["PORCTJPRIVADO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPRIVADO"].ToString()) : vEntidadProvOferente.PORCTJPRIVADO;
                            vEntidadProvOferente.PORCTJPUBLICO = vDataReaderResults["PORCTJPUBLICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPUBLICO"].ToString()) : vEntidadProvOferente.PORCTJPUBLICO;
                            vEntidadProvOferente.NUMTOTALTRABAJADORES = vDataReaderResults["NUMTOTALTRABAJADORES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMTOTALTRABAJADORES"].ToString()) : vEntidadProvOferente.NUMTOTALTRABAJADORES;
                            vEntidadProvOferente.ANOREGISTRO = vDataReaderResults["ANOREGISTRO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ANOREGISTRO"].ToString()) : vEntidadProvOferente.ANOREGISTRO;
                            vEntidadProvOferente.SIGLA = vDataReaderResults["SIGLA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SIGLA"].ToString()) : vEntidadProvOferente.SIGLA;
                            vEntidadProvOferente.RUPOBALANCE = vDataReaderResults["RUPOBALANCE"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUPOBALANCE"].ToString()) : vEntidadProvOferente.RUPOBALANCE;
                            vEntidadProvOferente.ESPACIOARCHIVO = vDataReaderResults["ESPACIOARCHIVO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOARCHIVO"].ToString()) : vEntidadProvOferente.ESPACIOARCHIVO;
                            vEntidadProvOferente.ESPACIOMATOFICINA = vDataReaderResults["ESPACIOMATOFICINA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOMATOFICINA"].ToString()) : vEntidadProvOferente.ESPACIOMATOFICINA;
                            vEntidadProvOferente.EQUIPOCOMPUTO = vDataReaderResults["EQUIPOCOMPUTO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EQUIPOCOMPUTO"].ToString()) : vEntidadProvOferente.EQUIPOCOMPUTO;
                            vEntidadProvOferente.SISTOPERATIVO = vDataReaderResults["SISTOPERATIVO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SISTOPERATIVO"].ToString()) : vEntidadProvOferente.SISTOPERATIVO;
                            vEntidadProvOferente.CONLINEAFIJA = vDataReaderResults["CONLINEAFIJA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEAFIJA"].ToString()) : vEntidadProvOferente.CONLINEAFIJA;
                            vEntidadProvOferente.CONLINEACELULAR = vDataReaderResults["CONLINEACELULAR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEACELULAR"].ToString()) : vEntidadProvOferente.CONLINEACELULAR;
                            vEntidadProvOferente.CONEMAIL = vDataReaderResults["CONEMAIL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONEMAIL"].ToString()) : vEntidadProvOferente.CONEMAIL;
                            vEntidadProvOferente.CONACCESOINTERNET = vDataReaderResults["CONACCESOINTERNET"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONACCESOINTERNET"].ToString()) : vEntidadProvOferente.CONACCESOINTERNET;
                            vEntidadProvOferente.NUMSEDES = vDataReaderResults["NUMSEDES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMSEDES"].ToString()) : vEntidadProvOferente.NUMSEDES;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                        }
                        return vEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }




        /// <summary>
        ///  Consultar una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIdEntidad, bool pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_ConsultarPorIDyEstado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pIdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@ESTADOGUARDADO", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
                        while (vDataReaderResults.Read())
                        {
                            vEntidadProvOferente.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vEntidadProvOferente.IDENTIDAD;
                            vEntidadProvOferente.TIPOENTOFPROV = vDataReaderResults["TIPOENTOFPROV"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TIPOENTOFPROV"].ToString()) : vEntidadProvOferente.TIPOENTOFPROV;
                            vEntidadProvOferente.IDTERCERO = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vEntidadProvOferente.IDTERCERO;
                            vEntidadProvOferente.IDTERCEROREPLEGAL = vDataReaderResults["IDTERCEROREPLEGAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCEROREPLEGAL"].ToString()) : vEntidadProvOferente.IDTERCEROREPLEGAL;
                            vEntidadProvOferente.IDTIPOCIIUPRINCIPAL = vDataReaderResults["IDTIPOCIIUPRINCIPAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.IDTIPOCIIUPRINCIPAL;
                            vEntidadProvOferente.IDTIPOCIIUSECUNDARIO = vDataReaderResults["IDTIPOCIIUSECUNDARIO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.IDTIPOCIIUSECUNDARIO;
                            vEntidadProvOferente.IDTIPOREGTRIB = vDataReaderResults["IDTIPOREGTRIB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOREGTRIB"].ToString()) : vEntidadProvOferente.IDTIPOREGTRIB;
                            vEntidadProvOferente.IDTIPOSECTOR = vDataReaderResults["IDTIPOSECTOR"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOSECTOR"].ToString()) : vEntidadProvOferente.IDTIPOSECTOR;
                            vEntidadProvOferente.IDTIPOACTIVIDAD = vDataReaderResults["IDTIPOACTIVIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOACTIVIDAD"].ToString()) : vEntidadProvOferente.IDTIPOACTIVIDAD;
                            vEntidadProvOferente.IDTIPOCLASEENTIDAD = vDataReaderResults["IDTIPOCLASEENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCLASEENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCLASEENTIDAD;
                            vEntidadProvOferente.IDTIPORAMAPUBLICA = vDataReaderResults["IDTIPORAMAPUBLICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORAMAPUBLICA"].ToString()) : vEntidadProvOferente.IDTIPORAMAPUBLICA;
                            vEntidadProvOferente.IDTIPOENTIDAD = vDataReaderResults["IDTIPOENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOENTIDAD;
                            vEntidadProvOferente.IDTIPONIVELGOB = vDataReaderResults["IDTIPONIVELGOB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELGOB"].ToString()) : vEntidadProvOferente.IDTIPONIVELGOB;
                            vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL = vDataReaderResults["IDTIPONIVELORGANIZACIONAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELORGANIZACIONAL"].ToString()) : vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL;
                            vEntidadProvOferente.IDTIPOORIGENCAPITAL = vDataReaderResults["IDTIPOORIGENCAPITAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOORIGENCAPITAL"].ToString()) : vEntidadProvOferente.IDTIPOORIGENCAPITAL;
                            vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD = vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD;
                            vEntidadProvOferente.IDTIPOCERTIFICATAMANO = vDataReaderResults["IDTIPOCERTIFICATAMANO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICATAMANO"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICATAMANO;
                            vEntidadProvOferente.IDTIPONATURALEZAJURID = vDataReaderResults["IDTIPONATURALEZAJURID"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONATURALEZAJURID"].ToString()) : vEntidadProvOferente.IDTIPONATURALEZAJURID;
                            vEntidadProvOferente.IDTIPORANGOSACTIVOS = vDataReaderResults["IDTIPORANGOSACTIVOS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORANGOSACTIVOS"].ToString()) : vEntidadProvOferente.IDTIPORANGOSACTIVOS;
                            vEntidadProvOferente.FECHACIIUPRINCIPAL = vDataReaderResults["FECHACIIUPRINCIPAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.FECHACIIUPRINCIPAL;
                            vEntidadProvOferente.FECHACIIUSECUNDARIO = vDataReaderResults["FECHACIIUSECUNDARIO"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.FECHACIIUSECUNDARIO;
                            vEntidadProvOferente.SITIOWEB = vDataReaderResults["SITIOWEB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SITIOWEB"].ToString()) : vEntidadProvOferente.SITIOWEB;
                            vEntidadProvOferente.FECHACONSTITUCION = vDataReaderResults["FECHACONSTITUCION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACONSTITUCION"].ToString()) : vEntidadProvOferente.FECHACONSTITUCION;
                            vEntidadProvOferente.FECHAVIGENCIA = vDataReaderResults["FECHAVIGENCIA"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAVIGENCIA"].ToString()) : vEntidadProvOferente.FECHAVIGENCIA;
                            vEntidadProvOferente.FECHAMATRICULAMERC = vDataReaderResults["FECHAMATRICULAMERC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAMATRICULAMERC"].ToString()) : vEntidadProvOferente.FECHAMATRICULAMERC;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.NOMBRECOMERCIAL = vDataReaderResults["NOMBRECOMERCIAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["NOMBRECOMERCIAL"].ToString()) : vEntidadProvOferente.NOMBRECOMERCIAL;
                            vEntidadProvOferente.EXENMATRICULAMER = vDataReaderResults["EXENMATRICULAMER"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EXENMATRICULAMER"].ToString()) : vEntidadProvOferente.EXENMATRICULAMER;
                            vEntidadProvOferente.MATRICULAMERCANTIL = vDataReaderResults["MATRICULAMERCANTIL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MATRICULAMERCANTIL"].ToString()) : vEntidadProvOferente.MATRICULAMERCANTIL;
                            vEntidadProvOferente.OBSERVALIDADOR = vDataReaderResults["OBSERVALIDADOR"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OBSERVALIDADOR"].ToString()) : vEntidadProvOferente.OBSERVALIDADOR;
                            vEntidadProvOferente.INSCRITORUP = vDataReaderResults["INSCRITORUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["INSCRITORUP"].ToString()) : vEntidadProvOferente.INSCRITORUP;
                            vEntidadProvOferente.NUMRUP = vDataReaderResults["NUMRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMRUP"].ToString()) : vEntidadProvOferente.NUMRUP;
                            vEntidadProvOferente.NOMBREENTIDADACREDITADORA = vDataReaderResults["NOMBREENTIDADACREDITADORA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBREENTIDADACREDITADORA"].ToString()) : vEntidadProvOferente.NOMBREENTIDADACREDITADORA;
                            vEntidadProvOferente.PORCTJPRIVADO = vDataReaderResults["PORCTJPRIVADO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPRIVADO"].ToString()) : vEntidadProvOferente.PORCTJPRIVADO;
                            vEntidadProvOferente.PORCTJPUBLICO = vDataReaderResults["PORCTJPUBLICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPUBLICO"].ToString()) : vEntidadProvOferente.PORCTJPUBLICO;
                            vEntidadProvOferente.NUMTOTALTRABAJADORES = vDataReaderResults["NUMTOTALTRABAJADORES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMTOTALTRABAJADORES"].ToString()) : vEntidadProvOferente.NUMTOTALTRABAJADORES;
                            vEntidadProvOferente.ANOREGISTRO = vDataReaderResults["ANOREGISTRO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ANOREGISTRO"].ToString()) : vEntidadProvOferente.ANOREGISTRO;
                            vEntidadProvOferente.SIGLA = vDataReaderResults["SIGLA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SIGLA"].ToString()) : vEntidadProvOferente.SIGLA;
                            vEntidadProvOferente.RUPOBALANCE = vDataReaderResults["RUPOBALANCE"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUPOBALANCE"].ToString()) : vEntidadProvOferente.RUPOBALANCE;
                            vEntidadProvOferente.ESPACIOARCHIVO = vDataReaderResults["ESPACIOARCHIVO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOARCHIVO"].ToString()) : vEntidadProvOferente.ESPACIOARCHIVO;
                            vEntidadProvOferente.ESPACIOMATOFICINA = vDataReaderResults["ESPACIOMATOFICINA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOMATOFICINA"].ToString()) : vEntidadProvOferente.ESPACIOMATOFICINA;
                            vEntidadProvOferente.EQUIPOCOMPUTO = vDataReaderResults["EQUIPOCOMPUTO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EQUIPOCOMPUTO"].ToString()) : vEntidadProvOferente.EQUIPOCOMPUTO;
                            vEntidadProvOferente.SISTOPERATIVO = vDataReaderResults["SISTOPERATIVO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SISTOPERATIVO"].ToString()) : vEntidadProvOferente.SISTOPERATIVO;
                            vEntidadProvOferente.CONLINEAFIJA = vDataReaderResults["CONLINEAFIJA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEAFIJA"].ToString()) : vEntidadProvOferente.CONLINEAFIJA;
                            vEntidadProvOferente.CONLINEACELULAR = vDataReaderResults["CONLINEACELULAR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEACELULAR"].ToString()) : vEntidadProvOferente.CONLINEACELULAR;
                            vEntidadProvOferente.CONEMAIL = vDataReaderResults["CONEMAIL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONEMAIL"].ToString()) : vEntidadProvOferente.CONEMAIL;
                            vEntidadProvOferente.CONACCESOINTERNET = vDataReaderResults["CONACCESOINTERNET"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONACCESOINTERNET"].ToString()) : vEntidadProvOferente.CONACCESOINTERNET;
                            vEntidadProvOferente.NUMSEDES = vDataReaderResults["NUMSEDES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMSEDES"].ToString()) : vEntidadProvOferente.NUMSEDES;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                        }
                        return vEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarEstadoValidacionEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_ConsultarEstadoValidacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string estadoValidacion = "";
                        while (vDataReaderResults.Read())
                        {
                            estadoValidacion = vDataReaderResults["EstadoValidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoValidacion"].ToString()) : estadoValidacion;
                        }
                        return estadoValidacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consulta una Entidad ProvOferente
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIDTERCERO)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCERO", DbType.Int32, pIDTERCERO);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
                        while (vDataReaderResults.Read())
                        {
                            vEntidadProvOferente.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vEntidadProvOferente.IDENTIDAD;
                            vEntidadProvOferente.TIPOENTOFPROV = vDataReaderResults["TIPOENTOFPROV"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TIPOENTOFPROV"].ToString()) : vEntidadProvOferente.TIPOENTOFPROV;
                            vEntidadProvOferente.IDTERCERO = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vEntidadProvOferente.IDTERCERO;
                            vEntidadProvOferente.IDTERCEROREPLEGAL = vDataReaderResults["IDTERCEROREPLEGAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCEROREPLEGAL"].ToString()) : vEntidadProvOferente.IDTERCEROREPLEGAL;
                            vEntidadProvOferente.IDTIPOCIIUPRINCIPAL = vDataReaderResults["IDTIPOCIIUPRINCIPAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.IDTIPOCIIUPRINCIPAL;
                            vEntidadProvOferente.IDTIPOCIIUSECUNDARIO = vDataReaderResults["IDTIPOCIIUSECUNDARIO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.IDTIPOCIIUSECUNDARIO;
                            vEntidadProvOferente.IDTIPOREGTRIB = vDataReaderResults["IDTIPOREGTRIB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOREGTRIB"].ToString()) : vEntidadProvOferente.IDTIPOREGTRIB;
                            vEntidadProvOferente.IDTIPOSECTOR = vDataReaderResults["IDTIPOSECTOR"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOSECTOR"].ToString()) : vEntidadProvOferente.IDTIPOSECTOR;
                            vEntidadProvOferente.IDTIPOACTIVIDAD = vDataReaderResults["IDTIPOACTIVIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOACTIVIDAD"].ToString()) : vEntidadProvOferente.IDTIPOACTIVIDAD;
                            vEntidadProvOferente.IDTIPOCLASEENTIDAD = vDataReaderResults["IDTIPOCLASEENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCLASEENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCLASEENTIDAD;
                            vEntidadProvOferente.IDTIPORAMAPUBLICA = vDataReaderResults["IDTIPORAMAPUBLICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORAMAPUBLICA"].ToString()) : vEntidadProvOferente.IDTIPORAMAPUBLICA;
                            vEntidadProvOferente.IDTIPOENTIDAD = vDataReaderResults["IDTIPOENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOENTIDAD;
                            vEntidadProvOferente.IDTIPONIVELGOB = vDataReaderResults["IDTIPONIVELGOB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELGOB"].ToString()) : vEntidadProvOferente.IDTIPONIVELGOB;
                            vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL = vDataReaderResults["IDTIPONIVELORGANIZACIONAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELORGANIZACIONAL"].ToString()) : vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL;
                            vEntidadProvOferente.IDTIPOORIGENCAPITAL = vDataReaderResults["IDTIPOORIGENCAPITAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOORIGENCAPITAL"].ToString()) : vEntidadProvOferente.IDTIPOORIGENCAPITAL;
                            vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD = vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD;
                            vEntidadProvOferente.IDTIPOCERTIFICATAMANO = vDataReaderResults["IDTIPOCERTIFICATAMANO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICATAMANO"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICATAMANO;
                            vEntidadProvOferente.IDTIPONATURALEZAJURID = vDataReaderResults["IDTIPONATURALEZAJURID"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONATURALEZAJURID"].ToString()) : vEntidadProvOferente.IDTIPONATURALEZAJURID;
                            vEntidadProvOferente.IDTIPORANGOSACTIVOS = vDataReaderResults["IDTIPORANGOSACTIVOS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORANGOSACTIVOS"].ToString()) : vEntidadProvOferente.IDTIPORANGOSACTIVOS;
                            vEntidadProvOferente.FECHACIIUPRINCIPAL = vDataReaderResults["FECHACIIUPRINCIPAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.FECHACIIUPRINCIPAL;
                            vEntidadProvOferente.FECHACIIUSECUNDARIO = vDataReaderResults["FECHACIIUSECUNDARIO"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.FECHACIIUSECUNDARIO;
                            vEntidadProvOferente.SITIOWEB = vDataReaderResults["SITIOWEB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SITIOWEB"].ToString()) : vEntidadProvOferente.SITIOWEB;
                            vEntidadProvOferente.FECHACONSTITUCION = vDataReaderResults["FECHACONSTITUCION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACONSTITUCION"].ToString()) : vEntidadProvOferente.FECHACONSTITUCION;
                            vEntidadProvOferente.FECHAVIGENCIA = vDataReaderResults["FECHAVIGENCIA"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAVIGENCIA"].ToString()) : vEntidadProvOferente.FECHAVIGENCIA;
                            vEntidadProvOferente.FECHAMATRICULAMERC = vDataReaderResults["FECHAMATRICULAMERC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAMATRICULAMERC"].ToString()) : vEntidadProvOferente.FECHAMATRICULAMERC;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.NOMBRECOMERCIAL = vDataReaderResults["NOMBRECOMERCIAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["NOMBRECOMERCIAL"].ToString()) : vEntidadProvOferente.NOMBRECOMERCIAL;
                            vEntidadProvOferente.EXENMATRICULAMER = vDataReaderResults["EXENMATRICULAMER"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EXENMATRICULAMER"].ToString()) : vEntidadProvOferente.EXENMATRICULAMER;
                            vEntidadProvOferente.MATRICULAMERCANTIL = vDataReaderResults["MATRICULAMERCANTIL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MATRICULAMERCANTIL"].ToString()) : vEntidadProvOferente.MATRICULAMERCANTIL;
                            vEntidadProvOferente.OBSERVALIDADOR = vDataReaderResults["OBSERVALIDADOR"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OBSERVALIDADOR"].ToString()) : vEntidadProvOferente.OBSERVALIDADOR;
                            vEntidadProvOferente.INSCRITORUP = vDataReaderResults["INSCRITORUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["INSCRITORUP"].ToString()) : vEntidadProvOferente.INSCRITORUP;
                            vEntidadProvOferente.NUMRUP = vDataReaderResults["NUMRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMRUP"].ToString()) : vEntidadProvOferente.NUMRUP;
                            vEntidadProvOferente.NOMBREENTIDADACREDITADORA = vDataReaderResults["NOMBREENTIDADACREDITADORA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBREENTIDADACREDITADORA"].ToString()) : vEntidadProvOferente.NOMBREENTIDADACREDITADORA;
                            vEntidadProvOferente.PORCTJPRIVADO = vDataReaderResults["PORCTJPRIVADO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPRIVADO"].ToString()) : vEntidadProvOferente.PORCTJPRIVADO;
                            vEntidadProvOferente.PORCTJPUBLICO = vDataReaderResults["PORCTJPUBLICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPUBLICO"].ToString()) : vEntidadProvOferente.PORCTJPUBLICO;
                            vEntidadProvOferente.NUMTOTALTRABAJADORES = vDataReaderResults["NUMTOTALTRABAJADORES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMTOTALTRABAJADORES"].ToString()) : vEntidadProvOferente.NUMTOTALTRABAJADORES;
                            vEntidadProvOferente.ANOREGISTRO = vDataReaderResults["ANOREGISTRO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ANOREGISTRO"].ToString()) : vEntidadProvOferente.ANOREGISTRO;
                            vEntidadProvOferente.SIGLA = vDataReaderResults["SIGLA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SIGLA"].ToString()) : vEntidadProvOferente.SIGLA;
                            vEntidadProvOferente.RUPOBALANCE = vDataReaderResults["RUPOBALANCE"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUPOBALANCE"].ToString()) : vEntidadProvOferente.RUPOBALANCE;
                            vEntidadProvOferente.ESPACIOARCHIVO = vDataReaderResults["ESPACIOARCHIVO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOARCHIVO"].ToString()) : vEntidadProvOferente.ESPACIOARCHIVO;
                            vEntidadProvOferente.ESPACIOMATOFICINA = vDataReaderResults["ESPACIOMATOFICINA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOMATOFICINA"].ToString()) : vEntidadProvOferente.ESPACIOMATOFICINA;
                            vEntidadProvOferente.EQUIPOCOMPUTO = vDataReaderResults["EQUIPOCOMPUTO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EQUIPOCOMPUTO"].ToString()) : vEntidadProvOferente.EQUIPOCOMPUTO;
                            vEntidadProvOferente.SISTOPERATIVO = vDataReaderResults["SISTOPERATIVO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SISTOPERATIVO"].ToString()) : vEntidadProvOferente.SISTOPERATIVO;
                            vEntidadProvOferente.CONLINEAFIJA = vDataReaderResults["CONLINEAFIJA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEAFIJA"].ToString()) : vEntidadProvOferente.CONLINEAFIJA;
                            vEntidadProvOferente.CONLINEACELULAR = vDataReaderResults["CONLINEACELULAR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEACELULAR"].ToString()) : vEntidadProvOferente.CONLINEACELULAR;
                            vEntidadProvOferente.CONEMAIL = vDataReaderResults["CONEMAIL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONEMAIL"].ToString()) : vEntidadProvOferente.CONEMAIL;
                            vEntidadProvOferente.CONACCESOINTERNET = vDataReaderResults["CONACCESOINTERNET"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONACCESOINTERNET"].ToString()) : vEntidadProvOferente.CONACCESOINTERNET;
                            vEntidadProvOferente.NUMSEDES = vDataReaderResults["NUMSEDES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMSEDES"].ToString()) : vEntidadProvOferente.NUMSEDES;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                        }
                        return vEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una lista de Entidad ProvOferentes
        /// </summary>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferentes_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntidadProvOferente> vListaEntidadProvOferente = new List<EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
                            vEntidadProvOferente.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vEntidadProvOferente.IDENTIDAD;
                            vEntidadProvOferente.TIPOENTOFPROV = vDataReaderResults["TIPOENTOFPROV"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TIPOENTOFPROV"].ToString()) : vEntidadProvOferente.TIPOENTOFPROV;
                            vEntidadProvOferente.IDTERCERO = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vEntidadProvOferente.IDTERCERO;
                            vEntidadProvOferente.IDTERCEROREPLEGAL = vDataReaderResults["IDTERCEROREPLEGAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCEROREPLEGAL"].ToString()) : vEntidadProvOferente.IDTERCEROREPLEGAL;
                            vEntidadProvOferente.IDTIPOCIIUPRINCIPAL = vDataReaderResults["IDTIPOCIIUPRINCIPAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.IDTIPOCIIUPRINCIPAL;
                            vEntidadProvOferente.IDTIPOCIIUSECUNDARIO = vDataReaderResults["IDTIPOCIIUSECUNDARIO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.IDTIPOCIIUSECUNDARIO;
                            vEntidadProvOferente.IDTIPOREGTRIB = vDataReaderResults["IDTIPOREGTRIB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOREGTRIB"].ToString()) : vEntidadProvOferente.IDTIPOREGTRIB;
                            vEntidadProvOferente.IDTIPOSECTOR = vDataReaderResults["IDTIPOSECTOR"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOSECTOR"].ToString()) : vEntidadProvOferente.IDTIPOSECTOR;
                            vEntidadProvOferente.IDTIPOACTIVIDAD = vDataReaderResults["IDTIPOACTIVIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOACTIVIDAD"].ToString()) : vEntidadProvOferente.IDTIPOACTIVIDAD;
                            vEntidadProvOferente.IDTIPOCLASEENTIDAD = vDataReaderResults["IDTIPOCLASEENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCLASEENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCLASEENTIDAD;
                            vEntidadProvOferente.IDTIPORAMAPUBLICA = vDataReaderResults["IDTIPORAMAPUBLICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORAMAPUBLICA"].ToString()) : vEntidadProvOferente.IDTIPORAMAPUBLICA;
                            vEntidadProvOferente.IDTIPOENTIDAD = vDataReaderResults["IDTIPOENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOENTIDAD;
                            vEntidadProvOferente.IDTIPONIVELGOB = vDataReaderResults["IDTIPONIVELGOB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELGOB"].ToString()) : vEntidadProvOferente.IDTIPONIVELGOB;
                            vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL = vDataReaderResults["IDTIPONIVELORGANIZACIONAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELORGANIZACIONAL"].ToString()) : vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL;
                            vEntidadProvOferente.IDTIPOORIGENCAPITAL = vDataReaderResults["IDTIPOORIGENCAPITAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOORIGENCAPITAL"].ToString()) : vEntidadProvOferente.IDTIPOORIGENCAPITAL;
                            vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD = vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD;
                            vEntidadProvOferente.IDTIPOCERTIFICATAMANO = vDataReaderResults["IDTIPOCERTIFICATAMANO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICATAMANO"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICATAMANO;
                            vEntidadProvOferente.IDTIPONATURALEZAJURID = vDataReaderResults["IDTIPONATURALEZAJURID"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONATURALEZAJURID"].ToString()) : vEntidadProvOferente.IDTIPONATURALEZAJURID;
                            vEntidadProvOferente.IDTIPORANGOSACTIVOS = vDataReaderResults["IDTIPORANGOSACTIVOS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORANGOSACTIVOS"].ToString()) : vEntidadProvOferente.IDTIPORANGOSACTIVOS;
                            vEntidadProvOferente.FECHACIIUPRINCIPAL = vDataReaderResults["FECHACIIUPRINCIPAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.FECHACIIUPRINCIPAL;
                            vEntidadProvOferente.FECHACIIUSECUNDARIO = vDataReaderResults["FECHACIIUSECUNDARIO"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.FECHACIIUSECUNDARIO;
                            vEntidadProvOferente.SITIOWEB = vDataReaderResults["SITIOWEB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SITIOWEB"].ToString()) : vEntidadProvOferente.SITIOWEB;
                            vEntidadProvOferente.FECHACONSTITUCION = vDataReaderResults["FECHACONSTITUCION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACONSTITUCION"].ToString()) : vEntidadProvOferente.FECHACONSTITUCION;
                            vEntidadProvOferente.FECHAVIGENCIA = vDataReaderResults["FECHAVIGENCIA"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAVIGENCIA"].ToString()) : vEntidadProvOferente.FECHAVIGENCIA;
                            vEntidadProvOferente.FECHAMATRICULAMERC = vDataReaderResults["FECHAMATRICULAMERC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAMATRICULAMERC"].ToString()) : vEntidadProvOferente.FECHAMATRICULAMERC;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.NOMBRECOMERCIAL = vDataReaderResults["NOMBRECOMERCIAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["NOMBRECOMERCIAL"].ToString()) : vEntidadProvOferente.NOMBRECOMERCIAL;
                            vEntidadProvOferente.EXENMATRICULAMER = vDataReaderResults["EXENMATRICULAMER"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EXENMATRICULAMER"].ToString()) : vEntidadProvOferente.EXENMATRICULAMER;
                            vEntidadProvOferente.MATRICULAMERCANTIL = vDataReaderResults["MATRICULAMERCANTIL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MATRICULAMERCANTIL"].ToString()) : vEntidadProvOferente.MATRICULAMERCANTIL;
                            vEntidadProvOferente.OBSERVALIDADOR = vDataReaderResults["OBSERVALIDADOR"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OBSERVALIDADOR"].ToString()) : vEntidadProvOferente.OBSERVALIDADOR;
                            vEntidadProvOferente.INSCRITORUP = vDataReaderResults["INSCRITORUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["INSCRITORUP"].ToString()) : vEntidadProvOferente.INSCRITORUP;
                            vEntidadProvOferente.NUMRUP = vDataReaderResults["NUMRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMRUP"].ToString()) : vEntidadProvOferente.NUMRUP;
                            vEntidadProvOferente.NOMBREENTIDADACREDITADORA = vDataReaderResults["NOMBREENTIDADACREDITADORA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBREENTIDADACREDITADORA"].ToString()) : vEntidadProvOferente.NOMBREENTIDADACREDITADORA;
                            vEntidadProvOferente.PORCTJPRIVADO = vDataReaderResults["PORCTJPRIVADO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPRIVADO"].ToString()) : vEntidadProvOferente.PORCTJPRIVADO;
                            vEntidadProvOferente.PORCTJPUBLICO = vDataReaderResults["PORCTJPUBLICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPUBLICO"].ToString()) : vEntidadProvOferente.PORCTJPUBLICO;
                            vEntidadProvOferente.NUMTOTALTRABAJADORES = vDataReaderResults["NUMTOTALTRABAJADORES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMTOTALTRABAJADORES"].ToString()) : vEntidadProvOferente.NUMTOTALTRABAJADORES;
                            vEntidadProvOferente.ANOREGISTRO = vDataReaderResults["ANOREGISTRO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ANOREGISTRO"].ToString()) : vEntidadProvOferente.ANOREGISTRO;
                            vEntidadProvOferente.SIGLA = vDataReaderResults["SIGLA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SIGLA"].ToString()) : vEntidadProvOferente.SIGLA;
                            vEntidadProvOferente.RUPOBALANCE = vDataReaderResults["RUPOBALANCE"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUPOBALANCE"].ToString()) : vEntidadProvOferente.RUPOBALANCE;
                            vEntidadProvOferente.ESPACIOARCHIVO = vDataReaderResults["ESPACIOARCHIVO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOARCHIVO"].ToString()) : vEntidadProvOferente.ESPACIOARCHIVO;
                            vEntidadProvOferente.ESPACIOMATOFICINA = vDataReaderResults["ESPACIOMATOFICINA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOMATOFICINA"].ToString()) : vEntidadProvOferente.ESPACIOMATOFICINA;
                            vEntidadProvOferente.EQUIPOCOMPUTO = vDataReaderResults["EQUIPOCOMPUTO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EQUIPOCOMPUTO"].ToString()) : vEntidadProvOferente.EQUIPOCOMPUTO;
                            vEntidadProvOferente.SISTOPERATIVO = vDataReaderResults["SISTOPERATIVO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SISTOPERATIVO"].ToString()) : vEntidadProvOferente.SISTOPERATIVO;
                            vEntidadProvOferente.CONLINEAFIJA = vDataReaderResults["CONLINEAFIJA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEAFIJA"].ToString()) : vEntidadProvOferente.CONLINEAFIJA;
                            vEntidadProvOferente.CONLINEACELULAR = vDataReaderResults["CONLINEACELULAR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEACELULAR"].ToString()) : vEntidadProvOferente.CONLINEACELULAR;
                            vEntidadProvOferente.CONEMAIL = vDataReaderResults["CONEMAIL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONEMAIL"].ToString()) : vEntidadProvOferente.CONEMAIL;
                            vEntidadProvOferente.CONACCESOINTERNET = vDataReaderResults["CONACCESOINTERNET"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONACCESOINTERNET"].ToString()) : vEntidadProvOferente.CONACCESOINTERNET;
                            vEntidadProvOferente.NUMSEDES = vDataReaderResults["NUMSEDES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMSEDES"].ToString()) : vEntidadProvOferente.NUMSEDES;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un lista de Entidad ProvOferentes de acuerdo a un IDTERCERO
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes(int? pIDTERCERO)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferentes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCERO", DbType.Int32, pIDTERCERO);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntidadProvOferente> vListaEntidadProvOferente = new List<EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
                            vEntidadProvOferente.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vEntidadProvOferente.IDENTIDAD;

                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Consulta los datos de la entidad de  Oferentes  Validados
        /// </summary>
        /// <param name="pIDTERCERO"></param>
        /// <returns></returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentesValidados()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferenteValidado_Consultar"))
                {
                
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntidadProvOferente> vListaEntidadProvOferente = new List<EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
                            vEntidadProvOferente.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vEntidadProvOferente.IDENTIDAD;
                            vEntidadProvOferente.TIPOENTOFPROV = vDataReaderResults["TIPOENTOFPROV"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TIPOENTOFPROV"].ToString()) : vEntidadProvOferente.TIPOENTOFPROV;
                            vEntidadProvOferente.IDTERCERO = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vEntidadProvOferente.IDTERCERO;
                            vEntidadProvOferente.IDTERCEROREPLEGAL = vDataReaderResults["IDTERCEROREPLEGAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCEROREPLEGAL"].ToString()) : vEntidadProvOferente.IDTERCEROREPLEGAL;
                            vEntidadProvOferente.IDTIPOCIIUPRINCIPAL = vDataReaderResults["IDTIPOCIIUPRINCIPAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.IDTIPOCIIUPRINCIPAL;
                            vEntidadProvOferente.IDTIPOCIIUSECUNDARIO = vDataReaderResults["IDTIPOCIIUSECUNDARIO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.IDTIPOCIIUSECUNDARIO;
                            vEntidadProvOferente.IDTIPOREGTRIB = vDataReaderResults["IDTIPOREGTRIB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOREGTRIB"].ToString()) : vEntidadProvOferente.IDTIPOREGTRIB;
                            vEntidadProvOferente.IDTIPOSECTOR = vDataReaderResults["IDTIPOSECTOR"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOSECTOR"].ToString()) : vEntidadProvOferente.IDTIPOSECTOR;
                            vEntidadProvOferente.IDTIPOACTIVIDAD = vDataReaderResults["IDTIPOACTIVIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOACTIVIDAD"].ToString()) : vEntidadProvOferente.IDTIPOACTIVIDAD;
                            vEntidadProvOferente.IDTIPOCLASEENTIDAD = vDataReaderResults["IDTIPOCLASEENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCLASEENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCLASEENTIDAD;
                            vEntidadProvOferente.IDTIPORAMAPUBLICA = vDataReaderResults["IDTIPORAMAPUBLICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORAMAPUBLICA"].ToString()) : vEntidadProvOferente.IDTIPORAMAPUBLICA;
                            vEntidadProvOferente.IDTIPOENTIDAD = vDataReaderResults["IDTIPOENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOENTIDAD"].ToString()) : vEntidadProvOferente.IDTIPOENTIDAD;
                            vEntidadProvOferente.IDTIPONIVELGOB = vDataReaderResults["IDTIPONIVELGOB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELGOB"].ToString()) : vEntidadProvOferente.IDTIPONIVELGOB;
                            vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL = vDataReaderResults["IDTIPONIVELORGANIZACIONAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONIVELORGANIZACIONAL"].ToString()) : vEntidadProvOferente.IDTIPONIVELORGANIZACIONAL;
                            vEntidadProvOferente.IDTIPOORIGENCAPITAL = vDataReaderResults["IDTIPOORIGENCAPITAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOORIGENCAPITAL"].ToString()) : vEntidadProvOferente.IDTIPOORIGENCAPITAL;
                            vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD = vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICADORCALIDAD"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICADORCALIDAD;
                            vEntidadProvOferente.IDTIPOCERTIFICATAMANO = vDataReaderResults["IDTIPOCERTIFICATAMANO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPOCERTIFICATAMANO"].ToString()) : vEntidadProvOferente.IDTIPOCERTIFICATAMANO;
                            vEntidadProvOferente.IDTIPONATURALEZAJURID = vDataReaderResults["IDTIPONATURALEZAJURID"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPONATURALEZAJURID"].ToString()) : vEntidadProvOferente.IDTIPONATURALEZAJURID;
                            vEntidadProvOferente.IDTIPORANGOSACTIVOS = vDataReaderResults["IDTIPORANGOSACTIVOS"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPORANGOSACTIVOS"].ToString()) : vEntidadProvOferente.IDTIPORANGOSACTIVOS;
                            vEntidadProvOferente.FECHACIIUPRINCIPAL = vDataReaderResults["FECHACIIUPRINCIPAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUPRINCIPAL"].ToString()) : vEntidadProvOferente.FECHACIIUPRINCIPAL;
                            vEntidadProvOferente.FECHACIIUSECUNDARIO = vDataReaderResults["FECHACIIUSECUNDARIO"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACIIUSECUNDARIO"].ToString()) : vEntidadProvOferente.FECHACIIUSECUNDARIO;
                            vEntidadProvOferente.SITIOWEB = vDataReaderResults["SITIOWEB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SITIOWEB"].ToString()) : vEntidadProvOferente.SITIOWEB;
                            vEntidadProvOferente.FECHACONSTITUCION = vDataReaderResults["FECHACONSTITUCION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHACONSTITUCION"].ToString()) : vEntidadProvOferente.FECHACONSTITUCION;
                            vEntidadProvOferente.FECHAVIGENCIA = vDataReaderResults["FECHAVIGENCIA"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAVIGENCIA"].ToString()) : vEntidadProvOferente.FECHAVIGENCIA;
                            vEntidadProvOferente.FECHAMATRICULAMERC = vDataReaderResults["FECHAMATRICULAMERC"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAMATRICULAMERC"].ToString()) : vEntidadProvOferente.FECHAMATRICULAMERC;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.FECHAEXPIRACION = vDataReaderResults["FECHAEXPIRACION"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FECHAEXPIRACION"].ToString()) : vEntidadProvOferente.FECHAEXPIRACION;
                            vEntidadProvOferente.NOMBRECOMERCIAL = vDataReaderResults["NOMBRECOMERCIAL"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["NOMBRECOMERCIAL"].ToString()) : vEntidadProvOferente.NOMBRECOMERCIAL;
                            vEntidadProvOferente.EXENMATRICULAMER = vDataReaderResults["EXENMATRICULAMER"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EXENMATRICULAMER"].ToString()) : vEntidadProvOferente.EXENMATRICULAMER;
                            vEntidadProvOferente.MATRICULAMERCANTIL = vDataReaderResults["MATRICULAMERCANTIL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MATRICULAMERCANTIL"].ToString()) : vEntidadProvOferente.MATRICULAMERCANTIL;
                            vEntidadProvOferente.OBSERVALIDADOR = vDataReaderResults["OBSERVALIDADOR"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OBSERVALIDADOR"].ToString()) : vEntidadProvOferente.OBSERVALIDADOR;
                            vEntidadProvOferente.INSCRITORUP = vDataReaderResults["INSCRITORUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["INSCRITORUP"].ToString()) : vEntidadProvOferente.INSCRITORUP;
                            vEntidadProvOferente.NUMRUP = vDataReaderResults["NUMRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMRUP"].ToString()) : vEntidadProvOferente.NUMRUP;
                            vEntidadProvOferente.NOMBREENTIDADACREDITADORA = vDataReaderResults["NOMBREENTIDADACREDITADORA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NOMBREENTIDADACREDITADORA"].ToString()) : vEntidadProvOferente.NOMBREENTIDADACREDITADORA;
                            vEntidadProvOferente.PORCTJPRIVADO = vDataReaderResults["PORCTJPRIVADO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPRIVADO"].ToString()) : vEntidadProvOferente.PORCTJPRIVADO;
                            vEntidadProvOferente.PORCTJPUBLICO = vDataReaderResults["PORCTJPUBLICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PORCTJPUBLICO"].ToString()) : vEntidadProvOferente.PORCTJPUBLICO;
                            vEntidadProvOferente.NUMTOTALTRABAJADORES = vDataReaderResults["NUMTOTALTRABAJADORES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMTOTALTRABAJADORES"].ToString()) : vEntidadProvOferente.NUMTOTALTRABAJADORES;
                            vEntidadProvOferente.ANOREGISTRO = vDataReaderResults["ANOREGISTRO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ANOREGISTRO"].ToString()) : vEntidadProvOferente.ANOREGISTRO;
                            vEntidadProvOferente.SIGLA = vDataReaderResults["SIGLA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SIGLA"].ToString()) : vEntidadProvOferente.SIGLA;
                            vEntidadProvOferente.RUPOBALANCE = vDataReaderResults["RUPOBALANCE"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUPOBALANCE"].ToString()) : vEntidadProvOferente.RUPOBALANCE;
                            vEntidadProvOferente.ESPACIOARCHIVO = vDataReaderResults["ESPACIOARCHIVO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOARCHIVO"].ToString()) : vEntidadProvOferente.ESPACIOARCHIVO;
                            vEntidadProvOferente.ESPACIOMATOFICINA = vDataReaderResults["ESPACIOMATOFICINA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ESPACIOMATOFICINA"].ToString()) : vEntidadProvOferente.ESPACIOMATOFICINA;
                            vEntidadProvOferente.EQUIPOCOMPUTO = vDataReaderResults["EQUIPOCOMPUTO"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EQUIPOCOMPUTO"].ToString()) : vEntidadProvOferente.EQUIPOCOMPUTO;
                            vEntidadProvOferente.SISTOPERATIVO = vDataReaderResults["SISTOPERATIVO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SISTOPERATIVO"].ToString()) : vEntidadProvOferente.SISTOPERATIVO;
                            vEntidadProvOferente.CONLINEAFIJA = vDataReaderResults["CONLINEAFIJA"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEAFIJA"].ToString()) : vEntidadProvOferente.CONLINEAFIJA;
                            vEntidadProvOferente.CONLINEACELULAR = vDataReaderResults["CONLINEACELULAR"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONLINEACELULAR"].ToString()) : vEntidadProvOferente.CONLINEACELULAR;
                            vEntidadProvOferente.CONEMAIL = vDataReaderResults["CONEMAIL"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONEMAIL"].ToString()) : vEntidadProvOferente.CONEMAIL;
                            vEntidadProvOferente.CONACCESOINTERNET = vDataReaderResults["CONACCESOINTERNET"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CONACCESOINTERNET"].ToString()) : vEntidadProvOferente.CONACCESOINTERNET;
                            vEntidadProvOferente.NUMSEDES = vDataReaderResults["NUMSEDES"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NUMSEDES"].ToString()) : vEntidadProvOferente.NUMSEDES;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
