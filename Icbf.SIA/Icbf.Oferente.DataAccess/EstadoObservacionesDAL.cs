﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.DataAccess
{
    public class EstadoObservacionesDAL : GeneralDAL
    {
        public EstadoObservacionesDAL()
        {

        }

        public EstadoObservaciones ConsultarEstadoObservacionPorCodigoEstado(string pCodigoEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EstadoObservaciones_CodigoEstado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstado", DbType.String, pCodigoEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoObservaciones vEstadoObservaciones = new EstadoObservaciones();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoObservaciones.IdEstadoObservacion = vDataReaderResults["IdEstadoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoObservacion"].ToString()) : vEstadoObservaciones.IdEstadoObservacion;
                            vEstadoObservaciones.PasoAnterior = vDataReaderResults["PasoAnterior"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PasoAnterior"].ToString()) : vEstadoObservaciones.PasoAnterior;
                            vEstadoObservaciones.PasoSiguiente = vDataReaderResults["PasoSiguiente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PasoSiguiente"].ToString()) : vEstadoObservaciones.PasoSiguiente;
                            vEstadoObservaciones.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoObservaciones.Estado;
                            vEstadoObservaciones.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? vDataReaderResults["CodigoEstado"].ToString() : vEstadoObservaciones.CodigoEstado;
                            vEstadoObservaciones.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? vDataReaderResults["DescripcionEstado"].ToString() : vEstadoObservaciones.DescripcionEstado;
                        }
                        return vEstadoObservaciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
