using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase ReferenteDAL
    /// </summary>
    public class ReferenteDAL : GeneralDAL
    {
        public ReferenteDAL()
        {
        }

        /// <summary>
        /// Inserta un Referente
        /// </summary>
        /// <param name="pReferente"></param>
        /// <returns></returns>
        public int InsertarReferente(Referente pReferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                DbCommand vDbCommand;
                using (vDbCommand = vDataBase.GetStoredProcCommand("usp_Terceros_Referente_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdReferente", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pReferente.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pReferente.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pReferente.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Correo", DbType.String, pReferente.Correo);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pReferente.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pReferente.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pReferente.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pReferente.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pReferente.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdReferente").ToString());
                    GenerarLogAuditoria(pReferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica un Referente
        /// </summary>
        /// <param name="pReferente"></param>
        /// <returns></returns>
        public int ModificarReferente(Referente pReferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Terceros_Referente_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdReferente", DbType.Int32, pReferente.IdReferente);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pReferente.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pReferente.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pReferente.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Correo", DbType.String, pReferente.Correo);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pReferente.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pReferente.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pReferente.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pReferente.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pReferente.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pReferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Referente por Id
        /// </summary>
        /// <param name="pIdReferente"></param>
        /// <returns></returns>
        public List<Referente> ConsultarReferentes(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Terceros_Referentes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Referente> lstReferente = new List<Referente>();
                        while (vDataReaderResults.Read())
                        {
                            Referente vReferente = new Referente();
                            vReferente.IdReferente = vDataReaderResults["IdReferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReferente"].ToString()) : vReferente.IdReferente;
                            vReferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vReferente.IdTercero;
                            vReferente.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vReferente.IdMunicipio;
                            vReferente.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vReferente.IdDepartamento;

                            vReferente.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"]) : vReferente.Nombre;
                            vReferente.Correo = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"]) : vReferente.Correo;
                            vReferente.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"]) : vReferente.Indicativo;
                            vReferente.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"]) : vReferente.Telefono;
                            vReferente.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"]) : vReferente.Extension;
                            vReferente.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"]) : vReferente.Celular;
                            
                            vReferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vReferente.UsuarioCrea;
                            vReferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vReferente.FechaCrea;
                            vReferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vReferente.UsuarioModifica;
                            vReferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vReferente.FechaModifica;

                            lstReferente.Add(vReferente);
                        }
                        return lstReferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
