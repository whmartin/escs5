using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TipoRegimenTributarioDAL : GeneralDAL
    {
        public TipoRegimenTributarioDAL()
        {
        }
        public int InsertarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoRegimenTributario_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoRegimenTributario", DbType.String, pTipoRegimenTributario.CodigoTipoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoRegimenTributario", DbType.String, pTipoRegimenTributario.NombreTipoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoRegimenTributario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoRegimenTributario.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoRegimenTributario.IdTipoRegimenTributario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoRegimenTributario").ToString());
                    GenerarLogAuditoria(pTipoRegimenTributario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoRegimenTributario_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pTipoRegimenTributario.IdTipoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoRegimenTributario", DbType.String, pTipoRegimenTributario.CodigoTipoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoRegimenTributario", DbType.String, pTipoRegimenTributario.NombreTipoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoRegimenTributario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoRegimenTributario.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoRegimenTributario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoRegimenTributario_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pTipoRegimenTributario.IdTipoRegimenTributario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoRegimenTributario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TipoRegimenTributario ConsultarTipoRegimenTributario(int pIdTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoRegimenTributario_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pIdTipoRegimenTributario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoRegimenTributario vTipoRegimenTributario = new TipoRegimenTributario();
                        while (vDataReaderResults.Read())
                        {
                            vTipoRegimenTributario.IdTipoRegimenTributario = vDataReaderResults["IdTipoRegimenTributario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.IdTipoRegimenTributario;
                            vTipoRegimenTributario.CodigoTipoRegimenTributario = vDataReaderResults["CodigoTipoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.CodigoTipoRegimenTributario;
                            vTipoRegimenTributario.NombreTipoRegimenTributario = vDataReaderResults["NombreTipoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.NombreTipoRegimenTributario;
                            vTipoRegimenTributario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRegimenTributario.Estado;
                            vTipoRegimenTributario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoRegimenTributario.UsuarioCrea;
                            vTipoRegimenTributario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoRegimenTributario.FechaCrea;
                            vTipoRegimenTributario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoRegimenTributario.UsuarioModifica;
                            vTipoRegimenTributario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoRegimenTributario.FechaModifica;
                        }
                        return vTipoRegimenTributario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoRegimenTributario> ConsultarTipoRegimenTributarios(String pCodigoTipoRegimenTributario, String pNombreTipoRegimenTributario, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoRegimenTributarios_Consultar"))
                {
                    if(pCodigoTipoRegimenTributario != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipoRegimenTributario", DbType.String, pCodigoTipoRegimenTributario);
                    if(pNombreTipoRegimenTributario != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoRegimenTributario", DbType.String, pNombreTipoRegimenTributario);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoRegimenTributario> vListaTipoRegimenTributario = new List<TipoRegimenTributario>();
                        while (vDataReaderResults.Read())
                        {
                                TipoRegimenTributario vTipoRegimenTributario = new TipoRegimenTributario();
                            vTipoRegimenTributario.IdTipoRegimenTributario = vDataReaderResults["IdTipoRegimenTributario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.IdTipoRegimenTributario;
                            vTipoRegimenTributario.CodigoTipoRegimenTributario = vDataReaderResults["CodigoTipoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.CodigoTipoRegimenTributario;
                            vTipoRegimenTributario.NombreTipoRegimenTributario = vDataReaderResults["NombreTipoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.NombreTipoRegimenTributario;
                            vTipoRegimenTributario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRegimenTributario.Estado;
                            if (vTipoRegimenTributario.Estado == true)
                                vTipoRegimenTributario.DescripcionEstado = "Activo";
                            else
                                vTipoRegimenTributario.DescripcionEstado = "Inactivo";
                            vTipoRegimenTributario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoRegimenTributario.UsuarioCrea;
                            vTipoRegimenTributario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoRegimenTributario.FechaCrea;
                            vTipoRegimenTributario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoRegimenTributario.UsuarioModifica;
                            vTipoRegimenTributario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoRegimenTributario.FechaModifica;
                                vListaTipoRegimenTributario.Add(vTipoRegimenTributario);
                        }
                        return vListaTipoRegimenTributario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
