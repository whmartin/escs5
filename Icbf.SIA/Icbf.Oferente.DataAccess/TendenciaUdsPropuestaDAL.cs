using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TendenciaUdsPropuestaDAL : GeneralDAL
    {
        public TendenciaUdsPropuestaDAL()
        {
        }
        public int InsertarTendenciaUdsPropuesta(TendenciaUdsPropuesta pTendenciaUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TendenciaUdsPropuesta_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTendenciaUdsPropuesta", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTendenciaUdsPropuesta", DbType.String, pTendenciaUdsPropuesta.CodigoTendenciaUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTendenciaUdsPropuesta", DbType.String, pTendenciaUdsPropuesta.NombreTendenciaUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTendenciaUdsPropuesta.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTendenciaUdsPropuesta.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTendenciaUdsPropuesta.IdTendenciaUdsPropuesta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTendenciaUdsPropuesta").ToString());
                    GenerarLogAuditoria(pTendenciaUdsPropuesta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTendenciaUdsPropuesta(TendenciaUdsPropuesta pTendenciaUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TendenciaUdsPropuesta_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTendenciaUdsPropuesta", DbType.Int32, pTendenciaUdsPropuesta.IdTendenciaUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTendenciaUdsPropuesta", DbType.String, pTendenciaUdsPropuesta.CodigoTendenciaUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTendenciaUdsPropuesta", DbType.String, pTendenciaUdsPropuesta.NombreTendenciaUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTendenciaUdsPropuesta.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTendenciaUdsPropuesta.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTendenciaUdsPropuesta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTendenciaUdsPropuesta(TendenciaUdsPropuesta pTendenciaUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TendenciaUdsPropuesta_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTendenciaUdsPropuesta", DbType.Int32, pTendenciaUdsPropuesta.IdTendenciaUdsPropuesta);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTendenciaUdsPropuesta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TendenciaUdsPropuesta ConsultarTendenciaUdsPropuesta(int pIdTendenciaUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TendenciaUdsPropuesta_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTendenciaUdsPropuesta", DbType.Int32, pIdTendenciaUdsPropuesta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TendenciaUdsPropuesta vTendenciaUdsPropuesta = new TendenciaUdsPropuesta();
                        while (vDataReaderResults.Read())
                        {
                            vTendenciaUdsPropuesta.IdTendenciaUdsPropuesta = vDataReaderResults["IdTendenciaUdsPropuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTendenciaUdsPropuesta"].ToString()) : vTendenciaUdsPropuesta.IdTendenciaUdsPropuesta;
                            vTendenciaUdsPropuesta.CodigoTendenciaUdsPropuesta = vDataReaderResults["CodigoTendenciaUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTendenciaUdsPropuesta"].ToString()) : vTendenciaUdsPropuesta.CodigoTendenciaUdsPropuesta;
                            vTendenciaUdsPropuesta.NombreTendenciaUdsPropuesta = vDataReaderResults["NombreTendenciaUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTendenciaUdsPropuesta"].ToString()) : vTendenciaUdsPropuesta.NombreTendenciaUdsPropuesta;
                            vTendenciaUdsPropuesta.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTendenciaUdsPropuesta.Estado;
                            vTendenciaUdsPropuesta.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTendenciaUdsPropuesta.UsuarioCrea;
                            vTendenciaUdsPropuesta.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTendenciaUdsPropuesta.FechaCrea;
                            vTendenciaUdsPropuesta.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTendenciaUdsPropuesta.UsuarioModifica;
                            vTendenciaUdsPropuesta.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTendenciaUdsPropuesta.FechaModifica;
                        }
                        return vTendenciaUdsPropuesta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TendenciaUdsPropuesta> ConsultarTendenciaUdsPropuestas(String pCodigoTendenciaUdsPropuesta, String pNombreTendenciaUdsPropuesta, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TendenciaUdsPropuestas_Consultar"))
                {
                    if(pCodigoTendenciaUdsPropuesta != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTendenciaUdsPropuesta", DbType.String, pCodigoTendenciaUdsPropuesta);
                    if(pNombreTendenciaUdsPropuesta != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTendenciaUdsPropuesta", DbType.String, pNombreTendenciaUdsPropuesta);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TendenciaUdsPropuesta> vListaTendenciaUdsPropuesta = new List<TendenciaUdsPropuesta>();
                        while (vDataReaderResults.Read())
                        {
                                TendenciaUdsPropuesta vTendenciaUdsPropuesta = new TendenciaUdsPropuesta();
                            vTendenciaUdsPropuesta.IdTendenciaUdsPropuesta = vDataReaderResults["IdTendenciaUdsPropuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTendenciaUdsPropuesta"].ToString()) : vTendenciaUdsPropuesta.IdTendenciaUdsPropuesta;
                            vTendenciaUdsPropuesta.CodigoTendenciaUdsPropuesta = vDataReaderResults["CodigoTendenciaUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTendenciaUdsPropuesta"].ToString()) : vTendenciaUdsPropuesta.CodigoTendenciaUdsPropuesta;
                            vTendenciaUdsPropuesta.NombreTendenciaUdsPropuesta = vDataReaderResults["NombreTendenciaUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTendenciaUdsPropuesta"].ToString()) : vTendenciaUdsPropuesta.NombreTendenciaUdsPropuesta;
                            vTendenciaUdsPropuesta.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTendenciaUdsPropuesta.Estado;
                            if (vTendenciaUdsPropuesta.Estado == true)
                            {
                                vTendenciaUdsPropuesta.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vTendenciaUdsPropuesta.DescripcionEstado = "Inactivo";
                            }
                            vTendenciaUdsPropuesta.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTendenciaUdsPropuesta.UsuarioCrea;
                            vTendenciaUdsPropuesta.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTendenciaUdsPropuesta.FechaCrea;
                            vTendenciaUdsPropuesta.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTendenciaUdsPropuesta.UsuarioModifica;
                            vTendenciaUdsPropuesta.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTendenciaUdsPropuesta.FechaModifica;
                            vListaTendenciaUdsPropuesta.Add(vTendenciaUdsPropuesta);
                        }
                        return vListaTendenciaUdsPropuesta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
