﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Diagnostics;


namespace Icbf.Oferente.DataAccess
{
    public class GeneralDAL
    {
        public GeneralDAL()
        {

        }
        public Microsoft.Practices.EnterpriseLibrary.Data.Database ObtenerInstancia()
        {
            //return DatabaseFactory.CreateDatabase("DataBaseConnectionString");
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
        }
        protected void GenerarLogAuditoria(Object pObjeto, DbCommand pDbCommand)
        {
            try
            {
                Icbf.Seguridad.Entity.EntityAuditoria pDatosAuditoria = (Icbf.Seguridad.Entity.EntityAuditoria)pObjeto;
                if (pDatosAuditoria.ProgramaGeneraLog)
                {
                    pDatosAuditoria.Tabla = pDbCommand.CommandText;
                    pDatosAuditoria.ParametrosOperacion = "";
                    Type myType = pObjeto.GetType();
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    bool PrimeraPropiedad = true;
                    foreach (PropertyInfo prop in props)
                    {
                        //if (prop.Name == "Id" + myType.Name)
                        //    pDatosAuditoria.IdRegistro = int.Parse(prop.GetValue(pObjeto, null).ToString());
                        if (PrimeraPropiedad)
                        {
                            pDatosAuditoria.IdRegistro = int.Parse(prop.GetValue(pObjeto, null).ToString());
                            PrimeraPropiedad = false;
                        }
                        if (!prop.Name.Equals("FechaCrea") && !prop.Name.Equals("FechaModifica"))
                            pDatosAuditoria.ParametrosOperacion += String.Format("[{0} - {1}] ", prop.Name, prop.GetValue(pObjeto, null));

                    }

                    Database vDataBase = DatabaseFactory.CreateDatabase("AuditaConnectionString");
                    using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Aud_InsertarLogAuditoria"))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@pUsuario", DbType.String, pDatosAuditoria.Usuario);
                        vDataBase.AddInParameter(vDbCommand, "@pPrograma", DbType.String, pDatosAuditoria.Programa);
                        vDataBase.AddInParameter(vDbCommand, "@pOperacion", DbType.String, pDatosAuditoria.Operacion);
                        vDataBase.AddInParameter(vDbCommand, "@pParametrosOperacion", DbType.String, pDatosAuditoria.ParametrosOperacion);
                        vDataBase.AddInParameter(vDbCommand, "@pTabla", DbType.String, pDatosAuditoria.Tabla);
                        vDataBase.AddInParameter(vDbCommand, "@pIdRegistro", DbType.Int64, pDatosAuditoria.IdRegistro);
                        vDataBase.AddInParameter(vDbCommand, "@pDireccionIp", DbType.String, pDatosAuditoria.DireccionIP);
                        vDataBase.AddInParameter(vDbCommand, "@pNavegador", DbType.String, pDatosAuditoria.Navegador);
                        vDataBase.ExecuteNonQuery(vDbCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Source, ex.Message);
            }
        }
    }
}
