using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class RUPBalanceDAL : GeneralDAL
    {
        public RUPBalanceDAL()
        {
        }
        public int InsertarRUPBalance(RUPBalance pRUPBalance)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RUPBalance_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRUPBalance", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRUPBalance", DbType.String, pRUPBalance.CodigoRUPBalance);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRUPBalance", DbType.String, pRUPBalance.NombreRUPBalance);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRUPBalance.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRUPBalance.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRUPBalance.IdRUPBalance = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRUPBalance").ToString());
                    GenerarLogAuditoria(pRUPBalance, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarRUPBalance(RUPBalance pRUPBalance)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RUPBalance_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRUPBalance", DbType.Int32, pRUPBalance.IdRUPBalance);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRUPBalance", DbType.String, pRUPBalance.CodigoRUPBalance);
                    vDataBase.AddInParameter(vDbCommand, "@NombreRUPBalance", DbType.String, pRUPBalance.NombreRUPBalance);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRUPBalance.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRUPBalance.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRUPBalance, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarRUPBalance(RUPBalance pRUPBalance)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RUPBalance_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRUPBalance", DbType.Int32, pRUPBalance.IdRUPBalance);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRUPBalance, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public RUPBalance ConsultarRUPBalance(int pIdRUPBalance)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RUPBalance_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRUPBalance", DbType.Int32, pIdRUPBalance);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RUPBalance vRUPBalance = new RUPBalance();
                        while (vDataReaderResults.Read())
                        {
                            vRUPBalance.IdRUPBalance = vDataReaderResults["IdRUPBalance"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRUPBalance"].ToString()) : vRUPBalance.IdRUPBalance;
                            vRUPBalance.CodigoRUPBalance = vDataReaderResults["CodigoRUPBalance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRUPBalance"].ToString()) : vRUPBalance.CodigoRUPBalance;
                            vRUPBalance.NombreRUPBalance = vDataReaderResults["NombreRUPBalance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRUPBalance"].ToString()) : vRUPBalance.NombreRUPBalance;
                            vRUPBalance.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRUPBalance.Estado;
                            vRUPBalance.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRUPBalance.UsuarioCrea;
                            vRUPBalance.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRUPBalance.FechaCrea;
                            vRUPBalance.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRUPBalance.UsuarioModifica;
                            vRUPBalance.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRUPBalance.FechaModifica;

                            if (vRUPBalance.Estado == true)
                            {
                                vRUPBalance.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vRUPBalance.DescripcionEstado = "Inactivo";
                            }                       
                        }
                        return vRUPBalance;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RUPBalance> ConsultarRUPBalances(String pCodigoRUPBalance, String pNombreRUPBalance, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RUPBalances_Consultar"))
                {
                    if(pCodigoRUPBalance != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoRUPBalance", DbType.String, pCodigoRUPBalance);
                    if(pNombreRUPBalance != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreRUPBalance", DbType.String, pNombreRUPBalance);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RUPBalance> vListaRUPBalance = new List<RUPBalance>();
                        while (vDataReaderResults.Read())
                        {
                                RUPBalance vRUPBalance = new RUPBalance();
                            vRUPBalance.IdRUPBalance = vDataReaderResults["IdRUPBalance"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRUPBalance"].ToString()) : vRUPBalance.IdRUPBalance;
                            vRUPBalance.CodigoRUPBalance = vDataReaderResults["CodigoRUPBalance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRUPBalance"].ToString()) : vRUPBalance.CodigoRUPBalance;
                            vRUPBalance.NombreRUPBalance = vDataReaderResults["NombreRUPBalance"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRUPBalance"].ToString()) : vRUPBalance.NombreRUPBalance;
                            vRUPBalance.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRUPBalance.Estado;
                            vRUPBalance.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRUPBalance.UsuarioCrea;
                            vRUPBalance.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRUPBalance.FechaCrea;
                            vRUPBalance.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRUPBalance.UsuarioModifica;
                            vRUPBalance.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRUPBalance.FechaModifica;

                            vRUPBalance.DescripcionEstado = vRUPBalance.Estado == true ? "Activo" : "Inactivo";                               
                            
                            vListaRUPBalance.Add(vRUPBalance);
                        }
                        return vListaRUPBalance;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
