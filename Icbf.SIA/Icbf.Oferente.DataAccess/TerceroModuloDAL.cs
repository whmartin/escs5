using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase TerceroModuloDAL
    /// </summary>
    public class TerceroModuloDAL : GeneralDAL
    {
        public TerceroModuloDAL()
        {
        }
        /// <summary>
        /// Insertar TerceroModulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int InsertarTerceroModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroModulo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pTerceroModulo.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IDModulo", DbType.Int32, pTerceroModulo.IdModulo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTerceroModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Eliminar Tercero Entidad Modulo
        /// </summary>
        /// <param name="pTerceroModulo"></param>
        /// <returns></returns>
        public int EliminarTerceroEntidadModulo(TerceroModulo pTerceroModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroEntidadModulo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pTerceroModulo.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pTerceroModulo.IdModulo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTerceroModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero Modulos
        /// </summary>
        /// <param name="pIDTercero"></param>
        /// <param name="pIDModulo"></param>
        /// <returns></returns>
        public List<TerceroModulo> ConsultarTerceroModulos(int? pIDTercero, int? pIDModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroModulos_Consultar"))
                {
                    if (pIDTercero != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIDTercero);
                    }

                    if (pIDModulo != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IDModulo", DbType.Int32, pIDModulo);    
                    }
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TerceroModulo> vListaTerceroModulo = new List<TerceroModulo>();
                        while (vDataReaderResults.Read())
                        {
                            TerceroModulo vTerceroModulo = new TerceroModulo();
                            vTerceroModulo.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vTerceroModulo.IdEntidad;
                            vTerceroModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vTerceroModulo.IdModulo;

                            vTerceroModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vTerceroModulo.Estado;
                            vTerceroModulo.ordern = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Orden"].ToString()) : vTerceroModulo.ordern;
                            vListaTerceroModulo.Add(vTerceroModulo);
                        }
                        return vListaTerceroModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
