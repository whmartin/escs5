using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class EstadoDAL : GeneralDAL
    {
        public EstadoDAL()
        {
        }
        public int InsertarEstado(Estado pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Estado_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstado", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstado", DbType.String, pEstado.CodigoEstado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEstado", DbType.String, pEstado.NombreEstado);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado.Estado2);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstado.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstado.IdEstado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEstado").ToString());
                    GenerarLogAuditoria(pEstado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarEstado(Estado pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Estado_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pEstado.IdEstado);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstado", DbType.String, pEstado.CodigoEstado);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEstado", DbType.String, pEstado.NombreEstado);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado.Estado2);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstado.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarEstado(Estado pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Estado_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pEstado.IdEstado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstado, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Estado ConsultarEstado(int pIdEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Estado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pIdEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Estado vEstado = new Estado();
                        while (vDataReaderResults.Read())
                        {
                            vEstado.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vEstado.IdEstado;
                            vEstado.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vEstado.CodigoEstado;
                            vEstado.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vEstado.NombreEstado;
                            vEstado.Estado2 = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstado.Estado2;
                            vEstado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstado.UsuarioCrea;
                            vEstado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstado.FechaCrea;
                            vEstado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstado.UsuarioModifica;
                            vEstado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstado.FechaModifica;
                        }
                        return vEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Estado> ConsultarEstados(String pCodigoEstado, String pNombreEstado, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Estados_Consultar"))
                {
                    if(pCodigoEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoEstado", DbType.String, pCodigoEstado);
                    if(pNombreEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreEstado", DbType.String, pNombreEstado);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Estado> vListaEstado = new List<Estado>();
                        while (vDataReaderResults.Read())
                        {
                                Estado vEstado = new Estado();
                            vEstado.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vEstado.IdEstado;
                            vEstado.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vEstado.CodigoEstado;
                            vEstado.NombreEstado = vDataReaderResults["NombreEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstado"].ToString()) : vEstado.NombreEstado;
                            vEstado.Estado2 = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstado.Estado2;
                            vEstado.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstado.UsuarioCrea;
                            vEstado.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstado.FechaCrea;
                            vEstado.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstado.UsuarioModifica;
                            vEstado.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstado.FechaModifica;
                                vListaEstado.Add(vEstado);
                        }
                        return vListaEstado;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
