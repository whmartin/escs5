﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class DocumentoExperienciaDAL : GeneralDAL
    {
        public DocumentoExperienciaDAL()
        {
        }

        public int InsertarObligatorioExperiencia(DocumentoExperiencia pDocumentoExperiencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoExperiencia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentoExperiencia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoExperiencia", DbType.String, pDocumentoExperiencia.CodigoDocumentoExperiencia);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocumentoExperiencia.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pDocumentoExperiencia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentoExperiencia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDocumentoExperiencia.IdDocumentoExperiencia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentoExperiencia").ToString());
                    GenerarLogAuditoria(pDocumentoExperiencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarObligatorioExperiencia(DocumentoExperiencia pDocumentoExperiencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoExperiencia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoExperiencia", DbType.Int32, pDocumentoExperiencia.IdDocumentoExperiencia);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoExperiencia", DbType.String, pDocumentoExperiencia.CodigoDocumentoExperiencia);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocumentoExperiencia.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pDocumentoExperiencia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentoExperiencia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentoExperiencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarObligatorioExperiencia(DocumentoExperiencia pDocumentoExperiencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoExperiencia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoExperiencia", DbType.Int32, pDocumentoExperiencia.IdDocumentoExperiencia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentoExperiencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DocumentoExperiencia ConsultarObligatorioExperiencia(int pIdDocumentoExperiencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoExperiencia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoExperiencia", DbType.Int32, pIdDocumentoExperiencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocumentoExperiencia vDocumentoExperiencia = new DocumentoExperiencia();
                        while (vDataReaderResults.Read())
                        {
                            vDocumentoExperiencia.IdDocumentoExperiencia = vDataReaderResults["IdDocumentoExperiencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoExperiencia"].ToString()) : vDocumentoExperiencia.IdDocumentoExperiencia;
                            vDocumentoExperiencia.CodigoDocumentoExperiencia = vDataReaderResults["CodigoDocumentoExperiencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDocumentoExperiencia"].ToString()) : vDocumentoExperiencia.CodigoDocumentoExperiencia;
                            vDocumentoExperiencia.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocumentoExperiencia.Descripcion;
                            vDocumentoExperiencia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vDocumentoExperiencia.Estado;
                            vDocumentoExperiencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentoExperiencia.UsuarioCrea;
                            vDocumentoExperiencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentoExperiencia.FechaCrea;
                            vDocumentoExperiencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentoExperiencia.UsuarioModifica;
                            vDocumentoExperiencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentoExperiencia.FechaModifica;
                        }
                        return vDocumentoExperiencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentoExperiencia> ConsultarObligatorioExperiencias(String pCodigoDocumentoExperiencia, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoExperiencias_Consultar"))
                {
                    if (pCodigoDocumentoExperiencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoExperiencia", DbType.String, pCodigoDocumentoExperiencia);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentoExperiencia> vListaObligatorioExperiencia = new List<DocumentoExperiencia>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentoExperiencia vObligatorioExperiencia = new DocumentoExperiencia();
                            vObligatorioExperiencia.IdDocumentoExperiencia = vDataReaderResults["IdDocumentoExperiencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoExperiencia"].ToString()) : vObligatorioExperiencia.IdDocumentoExperiencia;
                            vObligatorioExperiencia.CodigoDocumentoExperiencia = vDataReaderResults["CodigoDocumentoExperiencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDocumentoExperiencia"].ToString()) : vObligatorioExperiencia.CodigoDocumentoExperiencia;
                            vObligatorioExperiencia.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vObligatorioExperiencia.Descripcion;
                            vObligatorioExperiencia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vObligatorioExperiencia.Estado;
                            vObligatorioExperiencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligatorioExperiencia.UsuarioCrea;
                            vObligatorioExperiencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligatorioExperiencia.FechaCrea;
                            vObligatorioExperiencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligatorioExperiencia.UsuarioModifica;
                            vObligatorioExperiencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligatorioExperiencia.FechaModifica;
                            vListaObligatorioExperiencia.Add(vObligatorioExperiencia);
                        }
                        return vListaObligatorioExperiencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoObligatorioExperiencia(String pCodigoDocumentoExperiencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoExperiencia_Consultar_CodigoDocumentoExperiencia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoExperiencia", DbType.String, pCodigoDocumentoExperiencia);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoObligatorioExperiencia = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoObligatorioExperiencia = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoObligatorioExperiencia;
                        }
                        return vExisteCodigoObligatorioExperiencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}