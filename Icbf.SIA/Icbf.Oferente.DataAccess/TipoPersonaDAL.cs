using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TipoPersonaDAL : GeneralDAL
    {
        public TipoPersonaDAL()
        {
        }

        /// <summary>
        ///  Consultar TipoPersona
        /// </summary>
        /// <param name="pIdTipoPersona"></param>
        /// <returns></returns>
        public TipoPersona ConsultarTipoPersona(int pIdTipoPersona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoPersona_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoPersona vTipoPersona = new TipoPersona();
                        while (vDataReaderResults.Read())
                        {
                            vTipoPersona.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTipoPersona.IdTipoPersona;
                            vTipoPersona.CodigoTipoPersona = vDataReaderResults["CodigoTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoPersona"].ToString()) : vTipoPersona.CodigoTipoPersona;
                            vTipoPersona.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTipoPersona.NombreTipoPersona;
                            vTipoPersona.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoPersona.Estado;
                            vTipoPersona.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoPersona.UsuarioCrea;
                            vTipoPersona.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoPersona.FechaCrea;
                            vTipoPersona.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoPersona.UsuarioModifica;
                            vTipoPersona.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoPersona.FechaModifica;
                        }
                        return vTipoPersona;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta Tipo Personas
        /// </summary>
        /// <param name="pCodigoTipoPersona"></param>
        /// <param name="pNombreTipoPersona"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoPersona> ConsultarTipoPersonas(String pCodigoTipoPersona, String pNombreTipoPersona, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoPersonas_Consultar"))
                {
                    if(pCodigoTipoPersona != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipoPersona", DbType.String, pCodigoTipoPersona);
                    if(pNombreTipoPersona != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoPersona", DbType.String, pNombreTipoPersona);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoPersona> vListaTipoPersona = new List<TipoPersona>();
                        while (vDataReaderResults.Read())
                        {
                                TipoPersona vTipoPersona = new TipoPersona();
                            vTipoPersona.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTipoPersona.IdTipoPersona;
                            vTipoPersona.CodigoTipoPersona = vDataReaderResults["CodigoTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoPersona"].ToString()) : vTipoPersona.CodigoTipoPersona;
                            vTipoPersona.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTipoPersona.NombreTipoPersona;
                            vTipoPersona.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoPersona.Estado;
                            if (vTipoPersona.Estado == true)
                                vTipoPersona.DescripcionEstado = "Activo";
                            else
                                vTipoPersona.DescripcionEstado = "Inactivo";
                            vTipoPersona.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoPersona.UsuarioCrea;
                            vTipoPersona.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoPersona.FechaCrea;
                            vTipoPersona.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoPersona.UsuarioModifica;
                            vTipoPersona.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoPersona.FechaModifica;
                                vListaTipoPersona.Add(vTipoPersona);
                        }
                        return vListaTipoPersona;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
