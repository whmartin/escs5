﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase RankingOferenteDAL
    /// </summary>
    public class RankingOferenteDAL : GeneralDAL
    {
        public RankingOferenteDAL()
        {
        }
        /// <summary>
        /// Inserta un Ranking Oferente
        /// </summary>
        /// <param name="pRankingOferenteDAL"></param>
        /// <returns></returns>
        public int InsertarRankingOferente(IdoneidadOferente pRankingOferenteDAL)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RankingOferente_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRankingOferente", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pRankingOferenteDAL.IdIdentidad);
                    vDataBase.AddInParameter(vDbCommand, "@Ranking", DbType.Int32, pRankingOferenteDAL.Ranking);
                    vDataBase.AddInParameter(vDbCommand, "@Idoneo", DbType.String, pRankingOferenteDAL.Idoneo);
                    vDataBase.AddInParameter(vDbCommand, "@Valorlegal", DbType.String, 0);
                    vDataBase.AddInParameter(vDbCommand, "@ValorIndicadores", DbType.String, 0);
                    vDataBase.AddInParameter(vDbCommand, "@ValorInfraestructura", DbType.String, 0);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pRankingOferenteDAL.Obervacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRankingOferenteDAL.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRankingOferenteDAL.IdRankingOferente = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRankingOferente").ToString());
                    GenerarLogAuditoria(pRankingOferenteDAL, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Modifica un Ranking Oferente
        /// </summary>
        /// <param name="pRankingOferenteDAL"></param>
        /// <returns></returns>
        public int ModificarRankingOferente(IdoneidadOferente pRankingOferenteDAL)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RankingOferente_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRankingOferente", DbType.Int32, pRankingOferenteDAL.IdRankingOferente);
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pRankingOferenteDAL.IdIdentidad);
                    vDataBase.AddInParameter(vDbCommand, "@Ranking", DbType.Int32, pRankingOferenteDAL.Ranking);
                    vDataBase.AddInParameter(vDbCommand, "@Idoneo", DbType.String, pRankingOferenteDAL.Idoneo);
                    vDataBase.AddInParameter(vDbCommand, "@Observacion", DbType.String, pRankingOferenteDAL.Obervacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRankingOferenteDAL.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRankingOferenteDAL, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Calcular Ranking Oferente
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int CalcularRankingOferente(int pIdEntidad, int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CalculoRankingOferente_Consultar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIdEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
