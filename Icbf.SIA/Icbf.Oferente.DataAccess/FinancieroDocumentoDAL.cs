using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase FinancieroDocumentoDAL
    /// </summary>
    public class FinancieroDocumentoDAL : GeneralDAL
    {
        public FinancieroDocumentoDAL()
        {
        }
        /// <summary>
        /// Insertar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int InsertarFinancieroDocumento(FinancieroDocumento pFinancieroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FinancieroDocumento_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFinancieroDocumento", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdFinanciero", DbType.Int32, pFinancieroDocumento.IdFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@IdParametroDocumentoFinanciero", DbType.Int32, pFinancieroDocumento.IdParametroDocumentoFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFinancieroDocumento.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IDArchivo", DbType.String, pFinancieroDocumento.IDArchivo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFinancieroDocumento.IdFinancieroDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFinancieroDocumento").ToString());
                    GenerarLogAuditoria(pFinancieroDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int ModificarFinancieroDocumento(FinancieroDocumento pFinancieroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FinancieroDocumento_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFinancieroDocumento", DbType.Int32, pFinancieroDocumento.IdFinancieroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdFinanciero", DbType.Int32, pFinancieroDocumento.IdFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@IdParametroDocumentoFinanciero", DbType.Int32, pFinancieroDocumento.IdParametroDocumentoFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFinancieroDocumento.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFinancieroDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Financiero Documento
        /// </summary>
        /// <param name="pFinancieroDocumento"></param>
        /// <returns></returns>
        public int EliminarFinancieroDocumento(int pFinancieroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FinancieroDocumento_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFinancieroDocumento", DbType.Int32, pFinancieroDocumento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFinancieroDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Financiero Documento
        /// </summary>
        /// <param name="pIdFinancieroDocumento"></param>
        /// <returns></returns>
        public FinancieroDocumento ConsultarFinancieroDocumento(int pIdFinancieroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FinancieroDocumento_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFinancieroDocumento", DbType.Int32, pIdFinancieroDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FinancieroDocumento vFinancieroDocumento = new FinancieroDocumento();
                        while (vDataReaderResults.Read())
                        {
                            vFinancieroDocumento.IdFinancieroDocumento = vDataReaderResults["IdFinancieroDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinancieroDocumento"].ToString()) : vFinancieroDocumento.IdFinancieroDocumento;
                            vFinancieroDocumento.IdFinanciero = vDataReaderResults["IdFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinanciero"].ToString()) : vFinancieroDocumento.IdFinanciero;
                            vFinancieroDocumento.IdParametroDocumentoFinanciero = vDataReaderResults["IdParametroDocumentoFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdParametroDocumentoFinanciero"].ToString()) : vFinancieroDocumento.IdParametroDocumentoFinanciero;
                            vFinancieroDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFinancieroDocumento.UsuarioCrea;
                            vFinancieroDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFinancieroDocumento.FechaCrea;
                            vFinancieroDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFinancieroDocumento.UsuarioModifica;
                            vFinancieroDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFinancieroDocumento.FechaModifica;
                        }
                        return vFinancieroDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Financiero Documentos
        /// </summary>
        /// <param name="pIdFinanciero"></param>
        /// <param name="pIdParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public List<FinancieroDocumento> ConsultarFinancieroDocumentos(int? pIdFinanciero, int? pIdParametroDocumentoFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FinancieroDocumentos_Consultar"))
                {
                    if(pIdFinanciero != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdFinanciero", DbType.Int32, pIdFinanciero);
                    if(pIdParametroDocumentoFinanciero != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdParametroDocumentoFinanciero", DbType.Int32, pIdParametroDocumentoFinanciero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FinancieroDocumento> vListaFinancieroDocumento = new List<FinancieroDocumento>();
                        while (vDataReaderResults.Read())
                        {
                                FinancieroDocumento vFinancieroDocumento = new FinancieroDocumento();
                            vFinancieroDocumento.IdFinancieroDocumento = vDataReaderResults["IdFinancieroDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinancieroDocumento"].ToString()) : vFinancieroDocumento.IdFinancieroDocumento;
                            vFinancieroDocumento.IdFinanciero = vDataReaderResults["IdFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinanciero"].ToString()) : vFinancieroDocumento.IdFinanciero;
                            vFinancieroDocumento.IdParametroDocumentoFinanciero = vDataReaderResults["IdParametroDocumentoFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdParametroDocumentoFinanciero"].ToString()) : vFinancieroDocumento.IdParametroDocumentoFinanciero;
                            vFinancieroDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFinancieroDocumento.UsuarioCrea;
                            vFinancieroDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFinancieroDocumento.FechaCrea;
                            vFinancieroDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFinancieroDocumento.UsuarioModifica;
                            vFinancieroDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFinancieroDocumento.FechaModifica;
                                vListaFinancieroDocumento.Add(vFinancieroDocumento);
                        }
                        return vListaFinancieroDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
