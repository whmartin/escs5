using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definici�n de clase TerceroDAL
    /// </summary>
    public class TerceroDAL : GeneralDAL
    {
        public TerceroDAL()
        {
        }
        /// <summary>
        /// Insertar Representante Legal
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_RepresentanteLegal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pTercero.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pTercero.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pTercero.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pTercero.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pTercero.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pTercero.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Sexo", DbType.String, pTercero.Sexo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTercero.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTercero.IdDListaTipoDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return pTercero.IdDListaTipoDocumento;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                DbCommand vDbCommand;
                using (vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pTercero.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTercero.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pTercero.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pTercero.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pTercero.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pTercero.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pTercero.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Sexo", DbType.String, pTercero.Sexo);
                    vDataBase.AddInParameter(vDbCommand, "@ProviderUserKey", DbType.String, pTercero.ProviderUserKey);
                    vDataBase.AddInParameter(vDbCommand, "@DIGITOVERIFICACION", DbType.Int32, pTercero.DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, pTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RAZONSOCIAL", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionId", DbType.DateTime, pTercero.FechaExpedicionId);
                    vDataBase.AddInParameter(vDbCommand, "@FechaNacimiento", DbType.DateTime, pTercero.FechaNacimiento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTercero.UsuarioCrea);

                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pTercero.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pTercero.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pTercero.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pTercero.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pTercero.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pTercero.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pTercero.Direccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarCargarTercero(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                DbCommand vDbCommand;
                using (vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargarTercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pTercero.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTercero.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pTercero.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pTercero.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pTercero.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pTercero.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pTercero.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Sexo", DbType.String, pTercero.Sexo);
                    vDataBase.AddInParameter(vDbCommand, "@DIGITOVERIFICACION", DbType.Int32, pTercero.DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, pTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RAZONSOCIAL", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pTercero.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pTercero.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pTercero.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pTercero.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pTercero.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pTercero.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pTercero.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTercero.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Inserta Tercero con IdTemporal
        /// </summary>
        /// <param name="pTercero"></param>
        /// <param name="idTemporal"></param>
        /// <returns></returns>
        public int InsertarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                DbCommand vDbCommand;
                using (vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pTercero.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTercero.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pTercero.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pTercero.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pTercero.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pTercero.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pTercero.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Sexo", DbType.String, pTercero.Sexo);
                    vDataBase.AddInParameter(vDbCommand, "@ProviderUserKey", DbType.String, pTercero.ProviderUserKey);
                    vDataBase.AddInParameter(vDbCommand, "@DIGITOVERIFICACION", DbType.Int32, pTercero.DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, pTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RAZONSOCIAL", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionId", DbType.DateTime, pTercero.FechaExpedicionId);
                    vDataBase.AddInParameter(vDbCommand, "@FechaNacimiento", DbType.DateTime, pTercero.FechaNacimiento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTercero.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, idTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@CreadoPorInterno", DbType.Boolean, pTercero.CreadoPorInterno);

                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pTercero.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pTercero.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pTercero.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pTercero.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pTercero.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pTercero.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pTercero.Direccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("CorreoUnico")) { throw new GenericException("El correo ya existe."); }

                if (ex.Message.Contains("NUMEROIDENTIFICACIONUnico")) { throw new GenericException("El n�mero de identificaci�n ya existe."); }

                throw ex;
            }
        }

        /// <summary>
        /// relaciona el  Tercero  con su Representante Legal en la Tabla Entidades
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <param name="IdRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarTerceroRepesentanteLegal(int pIdTercero, int IdRepresentanteLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroRepesentanteLegal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEntidad", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdRepresentanteLegal", DbType.Int32, IdRepresentanteLegal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pIdTercero.ToString());
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pIdTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEntidad").ToString());
                    GenerarLogAuditoria(pIdTercero, vDbCommand);
                    return vResultado;


                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Creado por Fabi�n Valencia 
        /// Modifica un Tercero
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int ModificarTercero(Tercero pTercero, string idTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pTercero.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTercero.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pTercero.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionId", DbType.DateTime, pTercero.FechaExpedicionId);
                    vDataBase.AddInParameter(vDbCommand, "@Sexo", DbType.String, pTercero.Sexo);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pTercero.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pTercero.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pTercero.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pTercero.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@FechaNacimiento", DbType.DateTime, pTercero.FechaNacimiento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTercero.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@EsFundacion", DbType.Boolean, pTercero.EsFundacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, idTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@CreadoPorInterno", DbType.Boolean, pTercero.CreadoPorInterno);

                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.String, pTercero.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.String, pTercero.Telefono);
                    vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.String, pTercero.Extension);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pTercero.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pTercero.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pTercero.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pTercero.Direccion);
                    if (!string.IsNullOrEmpty(pTercero.ProviderUserKey))
                        vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pTercero.ProviderUserKey);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTercero(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroUsuario_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTercero.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.String, pTercero.Celular);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Tercero por Id
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"]) : vTercero.ConsecutivoInterno;
                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.ProviderUserKey = vDataReaderResults["ProviderUserKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProviderUserKey"].ToString()) : vTercero.ProviderUserKey;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.CreadoPorInterno = vDataReaderResults["CreadoPorInterno"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CreadoPorInterno"]) : vTercero.CreadoPorInterno;

                            vTercero.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? vDataReaderResults["Indicativo"].ToString() : vTercero.Indicativo;
                            vTercero.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? vDataReaderResults["Telefono"].ToString() : vTercero.Telefono;
                            vTercero.Extension = vDataReaderResults["Extension"] != DBNull.Value ? vDataReaderResults["Extension"].ToString() : vTercero.Extension;
                            vTercero.Celular = vDataReaderResults["Celular"] != DBNull.Value ? vDataReaderResults["Celular"].ToString() : vTercero.Celular;
                            vTercero.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vTercero.IdDepartamento;
                            vTercero.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vTercero.IdMunicipio;
                            vTercero.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vTercero.IdZona;
                            vTercero.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? vDataReaderResults["Direccion"].ToString() : vTercero.Direccion;

                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un lista de Terceros de acuerdo a varios parametros
        /// </summary>
        /// <param name="vIdDListaTipoDocumento"></param>
        /// <param name="vIdEstadoTercero"></param>
        /// <param name="vIdTipoPersona"></param>
        /// <param name="vNumeroIdentificacion"></param>
        /// <param name="vPrimerNombre"></param>
        /// <param name="vSegundoNombre"></param>
        /// <param name="vPrimerApellido"></param>
        /// <param name="vSegundoApellido"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, int? vNumeroIdentificacion,
                                                string vPrimerNombre, string vSegundoNombre, string vPrimerApellido, string vSegundoApellido)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Terceros_Consultar"))
                {

                    if (vIdDListaTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, vIdDListaTipoDocumento);
                    if (vIdEstadoTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, vIdEstadoTercero);
                    if (vIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, vIdTipoPersona);
                    if (vNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, vNumeroIdentificacion);
                    if (vPrimerNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@PRIMERNOMBRE", DbType.String, vPrimerNombre);
                    if (vSegundoNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@SEGUNDONOMBRE", DbType.String, vSegundoNombre);
                    if (vPrimerApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@PRIMERAPELLIDO", DbType.String, vPrimerApellido);
                    if (vSegundoApellido != null)
                        vDataBase.AddInParameter(vDbCommand, "@SEGUNDOAPELLIDO", DbType.String, vSegundoApellido);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {


                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.IDENTIDAD = vDataReaderResults["IDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDENTIDAD"].ToString()) : vTercero.IDENTIDAD;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_Consultar"))
                {

                    if (vIdDListaTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, vIdDListaTipoDocumento);
                    if (vIdEstadoTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, vIdEstadoTercero);
                    if (vIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, vIdTipoPersona);
                    if (vNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, vNumeroIdentificacion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.NombreListaTipoDocumento = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTercero.NombreListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTercero.NombreTipoPersona;

                            vTercero.DigitoVerificacion = vDataReaderResults["DIGITOVERIFICACION"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DIGITOVERIFICACION"].ToString()) : vTercero.DigitoVerificacion;
                            //vTercero.NombreDigitoVerificacion = vDataReaderResults["NombreDigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDigitoVerificacion"].ToString()) : vTercero.NombreDigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.NombreEstadoTercero = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vTercero.NombreEstadoTercero;
                            //vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;


                            vTercero.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;

                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario, string vtercero, int? vClaseActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_Consultar"))
                {

                    if (vIdDListaTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, vIdDListaTipoDocumento);
                    if (vIdEstadoTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, vIdEstadoTercero);
                    if (vIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, vIdTipoPersona);
                    if (vNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, vNumeroIdentificacion);
                    if (vUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@USUARIOCREA", DbType.String, vUsuario);
                    if (vtercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@Tercero", DbType.String, vtercero);
                    if (vClaseActividad != null)
                        vDataBase.AddInParameter(vDbCommand, "@claseActividad", DbType.String, vClaseActividad);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.NombreListaTipoDocumento = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTercero.NombreListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTercero.NombreTipoPersona;

                            vTercero.DigitoVerificacion = vDataReaderResults["DIGITOVERIFICACION"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DIGITOVERIFICACION"].ToString()) : vTercero.DigitoVerificacion;
                            //vTercero.NombreDigitoVerificacion = vDataReaderResults["NombreDigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDigitoVerificacion"].ToString()) : vTercero.NombreDigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.NombreEstadoTercero = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vTercero.NombreEstadoTercero;
                            //vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;


                            vTercero.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.claseActividad = vDataReaderResults["claseActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["claseActividad"].ToString()) : vTercero.claseActividad;

                            string nombre_razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.Nombre_Razonsocial = nombre_razonsocial.Trim();
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Tercero> ConsultarTercerosNoProveedores(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, string vUsuario, string vtercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_ConsultarNoProveedores"))
                {

                    if (vIdDListaTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, vIdDListaTipoDocumento);
                    if (vIdEstadoTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, vIdEstadoTercero);
                    if (vIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, vIdTipoPersona);
                    if (vNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, vNumeroIdentificacion);
                    if (vUsuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@USUARIOCREA", DbType.String, vUsuario);
                    if (vtercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@Tercero", DbType.String, vtercero);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.NombreListaTipoDocumento = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTercero.NombreListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTercero.NombreTipoPersona;

                            vTercero.DigitoVerificacion = vDataReaderResults["DIGITOVERIFICACION"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DIGITOVERIFICACION"].ToString()) : vTercero.DigitoVerificacion;
                            //vTercero.NombreDigitoVerificacion = vDataReaderResults["NombreDigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDigitoVerificacion"].ToString()) : vTercero.NombreDigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.NombreEstadoTercero = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vTercero.NombreEstadoTercero;
                            //vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;


                            vTercero.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;

                            string nombre_razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.Nombre_Razonsocial = nombre_razonsocial.Trim();
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceros(int? vIdDListaTipoDocumento, int? vIdEstadoTercero, int? vIdTipoPersona, string vNumeroIdentificacion, DateTime vFechaRegistro, string vNombreTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_Consultar"))
                {

                    if (vIdDListaTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, vIdDListaTipoDocumento);
                    if (vIdEstadoTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, vIdEstadoTercero);
                    if (vIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, vIdTipoPersona);
                    if (vNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, vNumeroIdentificacion);
                    if (vFechaRegistro != DateTime.MinValue)
                        vDataBase.AddInParameter(vDbCommand, "@FECHACREA", DbType.Date, vFechaRegistro);
                    if (vNombreTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@TERCERO", DbType.String, vNombreTercero);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.NombreListaTipoDocumento = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTercero.NombreListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTercero.NombreTipoPersona;

                            vTercero.DigitoVerificacion = vDataReaderResults["DIGITOVERIFICACION"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DIGITOVERIFICACION"].ToString()) : vTercero.DigitoVerificacion;
                            //vTercero.NombreDigitoVerificacion = vDataReaderResults["NombreDigitoVerificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDigitoVerificacion"].ToString()) : vTercero.NombreDigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.NombreEstadoTercero = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vTercero.NombreEstadoTercero;
                            //vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;


                            vTercero.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;

                            string nombre_razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.Nombre_Razonsocial = nombre_razonsocial.Trim();
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero ProviderUserKey
        /// </summary>
        /// <param name="ProviderUserKey"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroProviderUserKey(string ProviderUserKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ProviderUserKey", DbType.String, ProviderUserKey);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {

                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;
                            vTercero.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"]) : vTercero.ConsecutivoInterno;
                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.ProviderUserKey = vDataReaderResults["ProviderUserKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProviderUserKey"].ToString()) : vTercero.ProviderUserKey;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.CreadoPorInterno = vDataReaderResults["CreadoPorInterno"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CreadoPorInterno"]) : vTercero.CreadoPorInterno;

                            vTercero.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? vDataReaderResults["Indicativo"].ToString() : vTercero.Indicativo;
                            vTercero.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? vDataReaderResults["Telefono"].ToString() : vTercero.Telefono;
                            vTercero.Extension = vDataReaderResults["Extension"] != DBNull.Value ? vDataReaderResults["Extension"].ToString() : vTercero.Extension;
                            vTercero.Celular = vDataReaderResults["Celular"] != DBNull.Value ? vDataReaderResults["Celular"].ToString() : vTercero.Celular;
                            vTercero.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vTercero.IdDepartamento;
                            vTercero.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vTercero.IdMunicipio;
                            vTercero.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vTercero.IdZona;
                            vTercero.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? vDataReaderResults["Direccion"].ToString() : vTercero.Direccion;

                            vTercero.EsFundacion = vDataReaderResults["EsFundacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFundacion"].ToString()) : vTercero.EsFundacion;
                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Tercero ConsultarTerceroPorEmail(string pEmail)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroPorEmail_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pEmail", DbType.String, pEmail);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {

                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;


                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar un Reprentante Legal Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public int ConsultarReprentanteLogalTercero(int pIdTercero)
        {
            try
            {
                int IdRepresentanteLegal = 0;

                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ReprentanteLogalTercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            IdRepresentanteLegal = vDataReaderResults["IDTERCEROREPLEGAL"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCEROREPLEGAL"].ToString()) : IdRepresentanteLegal;
                        }
                        return IdRepresentanteLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Tercero por Tipo y Numero Identificacion
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacion(int? pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Consultar_TipoNumeroIdentificacion"))
                {
                    if (pTipoidentificacion != null) vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pTipoidentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;

                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;
                            vTercero.Celular = vDataReaderResults["Celular"] != DBNull.Value ? vDataReaderResults["Celular"].ToString() : vTercero.Celular;
                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.ProviderUserKey = vDataReaderResults["ProviderUserKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProviderUserKey"].ToString()) : vTercero.ProviderUserKey;
                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Tercero por Tipo y Numero Identificacion
        /// </summary>
        /// <param name="pTipoidentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroTipoNumeroIdentificacionParaIntegrantes(int pTipoidentificacion, string pNumeroIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SIA_Tercero_Consultar_ParaIntegrantes"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacion", DbType.Int32, pTipoidentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IDTIPODOCIDENTIFICA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTIPODOCIDENTIFICA"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.IdEstadoTercero = vDataReaderResults["IDESTADOTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDESTADOTERCERO"].ToString()) : vTercero.IdEstadoTercero;

                            vTercero.DigitoVerificacion = vDataReaderResults["DigitoVerificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DigitoVerificacion"].ToString()) : vTercero.DigitoVerificacion;

                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;

                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaExpedicionId = vDataReaderResults["FechaExpedicionId"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionId"].ToString()) : vTercero.FechaExpedicionId;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.ProviderUserKey = vDataReaderResults["ProviderUserKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ProviderUserKey"].ToString()) : vTercero.ProviderUserKey;
                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tercero Proveedor
        /// </summary>
        /// <param name="pTercero"></param>
        /// <returns></returns>
        public int InsertarTerceroProveedor(Tercero pTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                DbCommand vDbCommand;
                using (vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTercero", DbType.Int32, 0);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pTercero.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTercero.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pTercero.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pTercero.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pTercero.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pTercero.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pTercero.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pTercero.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Sexo", DbType.String, pTercero.Sexo);
                    vDataBase.AddInParameter(vDbCommand, "@ProviderUserKey", DbType.String, pTercero.ProviderUserKey);
                    vDataBase.AddInParameter(vDbCommand, "@DIGITOVERIFICACION", DbType.Int32, pTercero.DigitoVerificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, pTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RAZONSOCIAL", DbType.String, pTercero.RazonSocial);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicionId", DbType.DateTime, pTercero.FechaExpedicionId);
                    vDataBase.AddInParameter(vDbCommand, "@FechaNacimiento", DbType.DateTime, pTercero.FechaNacimiento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTercero.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@CreadoPorInterno", DbType.Boolean, pTercero.CreadoPorInterno);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTercero").ToString());
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<Tercero> ConsultarTerceros_Proveedores_Validados(int TipoConsulta, int? vIdTipoPersona, int? vIdDListaTipoDocumento, string vNumeroIdentificacion, string vNombreTercero, string usuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_Consultar_Validados"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@isTercero", DbType.Int32, TipoConsulta);
                    if (vIdDListaTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, vIdDListaTipoDocumento);
                    if (vIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, vIdTipoPersona);
                    if (vNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, vNumeroIdentificacion);
                    if (vNombreTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@Tercero", DbType.String, vNombreTercero);
                    if (usuario != null)
                        vDataBase.AddInParameter(vDbCommand, "@USUARIOCREA", DbType.String, usuario);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.NombreListaTipoDocumento = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTercero.NombreListaTipoDocumento;
                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PRIMERNOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERNOMBRE"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SEGUNDONOMBRE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SEGUNDONOMBRE"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PRIMERAPELLIDO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PRIMERAPELLIDO"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.RazonSocial = vDataReaderResults["RazonSocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RazonSocial"].ToString()) : vTercero.RazonSocial;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.NombreEstadoTercero = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vTercero.NombreEstadoTercero;

                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " +
                                                          vTercero.PrimerApellido + " " + vTercero.SegundoApellido + " " +
                                                          vTercero.RazonSocial;
                            vTercero.Nombre_Razonsocial = vTercero.Nombre_Razonsocial.Trim();
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool PuedeTerceroCambiarEstado(int pIdTercero)
        {
            bool puede = false;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        vDataReaderResults.Read();
                        int retorno = Convert.ToInt32(vDataReaderResults["sePuedeCambiar"].ToString());
                        if (retorno == 1)
                            puede = true;
                    }
                }

                return puede;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Creado por Juan Carlos Valverde S�mano 
        /// Modifica un la Raz�n Social de un Tercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// /// <param name="pRazonSocial"></param>
        /// <returns></returns>
        public bool ModificarRazonSocialTercero(int pIdTercero, string pRazonSocial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_ModificarRazonSocial"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@RazonSocial", DbType.String, pRazonSocial);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tercero por correoelectronico
        /// </summary>
        /// <param name="pCorreoelectronico"></param>
        /// <returns></returns>
        public Tercero ConsultarTerceroPorCorreo(string pCorreoelectronico)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@correoElectronico", DbType.String, pCorreoelectronico);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tercero vTercero = new Tercero();
                        while (vDataReaderResults.Read())
                        {
                            vTercero.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vTercero.IdTercero;
                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.CreadoPorInterno = vDataReaderResults["CreadoPorInterno"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["CreadoPorInterno"]) : vTercero.CreadoPorInterno;
                        }
                        return vTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Tercero> ConsultarTerceroFuente(String pTipoPersona, String pTipoidentificacion, String pIdentificacion
           , String pProveedor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Tercero_Terceros_Consultar_Fuente"))
                {

                    if (!string.IsNullOrEmpty(pTipoPersona))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, Convert.ToInt32(pTipoPersona));
                    if (!string.IsNullOrEmpty(pTipoidentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.Int32, Convert.ToInt32(pTipoidentificacion));
                    if (!string.IsNullOrEmpty(pIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pIdentificacion);
                    if (!string.IsNullOrEmpty(pProveedor))
                        vDataBase.AddInParameter(vDbCommand, "@Proveedor", DbType.String, pProveedor);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaEntidadProvOferente = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            var vEntidadProvOferente = new Tercero();

                            vEntidadProvOferente.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vEntidadProvOferente.ConsecutivoInterno;
                            vEntidadProvOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.RazonSocial = vDataReaderResults["Tercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tercero"].ToString()) : vEntidadProvOferente.RazonSocial;
                            vEntidadProvOferente.NombreListaTipoDocumento = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vEntidadProvOferente.NombreListaTipoDocumento;
                            vEntidadProvOferente.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vEntidadProvOferente.NumeroIdentificacion;
                            vEntidadProvOferente.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vEntidadProvOferente.NombreTipoPersona;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
