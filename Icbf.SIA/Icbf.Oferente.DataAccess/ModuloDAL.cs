﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase ModuloDAL
    /// </summary>
    public class ModuloDAL : GeneralDAL
    {
        public ModuloDAL()
        {
        }
        /// <summary>
        /// Insertar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int InsertarModulo(Modulo pModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdModulo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pModulo.NombreModulo);
                    vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.Int32, pModulo.Orden);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pModulo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Ruta", DbType.String, pModulo.Ruta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pModulo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModulo.IdModulo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModulo").ToString());
                    GenerarLogAuditoria(pModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int ModificarModulo(Modulo pModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pModulo.IdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pModulo.NombreModulo);
                    vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.Int32, pModulo.Orden);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pModulo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Ruta", DbType.String, pModulo.Ruta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pModulo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar un Modulo
        /// </summary>
        /// <param name="pModulo"></param>
        /// <returns></returns>
        public int EliminarModulo(Modulo pModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pModulo.IdModulo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Orden Modulo
        /// </summary>
        /// <param name="IdEntidad"></param>
        /// <param name="IdModulo"></param>
        /// <returns></returns>
        public int EliminarOrdenModulo(int IdEntidad, int IdModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TerceroEntidadModulo_Eliminar_Orden"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, IdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, IdEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(IdModulo, vDbCommand);
                    GenerarLogAuditoria(IdEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Modulo
        /// </summary>
        /// <param name="pIdModulo"></param>
        /// <returns></returns>
        public Modulo ConsultarModulo(int pIdModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pIdModulo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Modulo vModulo = new Modulo();
                        while (vDataReaderResults.Read())
                        {
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Orden"].ToString()) : vModulo.Orden;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.Ruta = vDataReaderResults["Ruta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ruta"].ToString()) : vModulo.Ruta;
                            vModulo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModulo.UsuarioCrea;
                            vModulo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModulo.FechaCrea;
                            vModulo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModulo.UsuarioModifica;
                            vModulo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModulo.FechaModifica;
                        }
                        return vModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Una lista de Modulos
        /// </summary>
        /// <param name="pNombreModulo"></param>
        /// <param name="pOrden"></param>
        /// <param name="pEstado"></param>
        /// <param name="pRuta"></param>
        /// <returns></returns>
        public List<Modulo> ConsultarModulos(String pNombreModulo, int? pOrden, String pEstado, String pRuta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulos_Consultar"))
                {
                    if (pNombreModulo != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pNombreModulo);
                    if (pOrden != null)
                        vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.Int32, pOrden);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    if (pRuta != null)
                        vDataBase.AddInParameter(vDbCommand, "@Ruta", DbType.String, pRuta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Orden"].ToString()) : vModulo.Orden;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.Ruta = vDataReaderResults["Ruta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ruta"].ToString()) : vModulo.Ruta;
                            vModulo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModulo.UsuarioCrea;
                            vModulo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModulo.FechaCrea;
                            vModulo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModulo.UsuarioModifica;
                            vModulo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModulo.FechaModifica;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Modulos Orden
        /// </summary>
        /// <param name="pNombreModulo"></param>
        /// <param name="pOrden"></param>
        /// <param name="pEstado"></param>
        /// <param name="pRuta"></param>
        /// <returns></returns>
        public List<Modulo> ConsultarModulosOrden(String pNombreModulo, int? pOrden, String pEstado, String pRuta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulos_Button"))
                {
                    if (pNombreModulo != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pNombreModulo);
                    if (pOrden != null)
                        vDataBase.AddInParameter(vDbCommand, "@Orden", DbType.Int32, pOrden);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    if (pRuta != null)
                        vDataBase.AddInParameter(vDbCommand, "@Ruta", DbType.String, pRuta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Orden"].ToString()) : vModulo.Orden;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.Ruta = vDataReaderResults["Ruta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ruta"].ToString()) : vModulo.Ruta;
                            vModulo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModulo.UsuarioCrea;
                            vModulo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModulo.FechaCrea;
                            vModulo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModulo.UsuarioModifica;
                            vModulo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModulo.FechaModifica;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Modulo ConsultarModuloCodigo(int pCodigo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Modulos_Consultar_Codigo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoModulo", DbType.Int32, pCodigo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Modulo vModulo = new Modulo();
                        while (vDataReaderResults.Read())
                        {
                            
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.CodigoModulo = vDataReaderResults["CodigoModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoModulo"].ToString()) : vModulo.CodigoModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Orden = vDataReaderResults["Orden"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Orden"].ToString()) : vModulo.Orden;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.Ruta = vDataReaderResults["Ruta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ruta"].ToString()) : vModulo.Ruta;
                            vModulo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModulo.UsuarioCrea;
                            vModulo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModulo.FechaCrea;
                            vModulo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModulo.UsuarioModifica;
                            vModulo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModulo.FechaModifica;
                        }
                        return vModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}