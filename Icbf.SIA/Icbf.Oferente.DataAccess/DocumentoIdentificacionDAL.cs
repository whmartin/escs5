using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class DocumentoIdentificacionDAL : GeneralDAL
    {
        public DocumentoIdentificacionDAL()
        {
        }
        public int InsertarDocumentoIdentificacion(DocumentoIdentificacion pDocumentoIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoIdentificacion", DbType.String, pDocumentoIdentificacion.CodigoDocumentoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocumentoIdentificacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pDocumentoIdentificacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentoIdentificacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDocumentoIdentificacion.IdDocumentoIdentificacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentoIdentificacion").ToString());
                    GenerarLogAuditoria(pDocumentoIdentificacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDocumentoIdentificacion(DocumentoIdentificacion pDocumentoIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, pDocumentoIdentificacion.IdDocumentoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoIdentificacion", DbType.String, pDocumentoIdentificacion.CodigoDocumentoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocumentoIdentificacion.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pDocumentoIdentificacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentoIdentificacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentoIdentificacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDocumentoIdentificacion(DocumentoIdentificacion pDocumentoIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, pDocumentoIdentificacion.IdDocumentoIdentificacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentoIdentificacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public DocumentoIdentificacion ConsultarDocumentoIdentificacion(int pIdDocumentoIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, pIdDocumentoIdentificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocumentoIdentificacion vDocumentoIdentificacion = new DocumentoIdentificacion();
                        while (vDataReaderResults.Read())
                        {
                            vDocumentoIdentificacion.IdDocumentoIdentificacion = vDataReaderResults["IdDocumentoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoIdentificacion"].ToString()) : vDocumentoIdentificacion.IdDocumentoIdentificacion;
                            vDocumentoIdentificacion.CodigoDocumentoIdentificacion = vDataReaderResults["CodigoDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDocumentoIdentificacion"].ToString()) : vDocumentoIdentificacion.CodigoDocumentoIdentificacion;
                            vDocumentoIdentificacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocumentoIdentificacion.Descripcion;
                            vDocumentoIdentificacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vDocumentoIdentificacion.Estado;
                            vDocumentoIdentificacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentoIdentificacion.UsuarioCrea;
                            vDocumentoIdentificacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentoIdentificacion.FechaCrea;
                            vDocumentoIdentificacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentoIdentificacion.UsuarioModifica;
                            vDocumentoIdentificacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentoIdentificacion.FechaModifica;
                        }
                        return vDocumentoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentoIdentificacion> ConsultarDocumentoIdentificacions(String pCodigoDocumentoIdentificacion, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacions_Consultar"))
                {
                    if (pCodigoDocumentoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoIdentificacion", DbType.String, pCodigoDocumentoIdentificacion);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentoIdentificacion> vListaDocumentoIdentificacion = new List<DocumentoIdentificacion>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentoIdentificacion vDocumentoIdentificacion = new DocumentoIdentificacion();
                            vDocumentoIdentificacion.IdDocumentoIdentificacion = vDataReaderResults["IdDocumentoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoIdentificacion"].ToString()) : vDocumentoIdentificacion.IdDocumentoIdentificacion;
                            vDocumentoIdentificacion.CodigoDocumentoIdentificacion = vDataReaderResults["CodigoDocumentoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoDocumentoIdentificacion"].ToString()) : vDocumentoIdentificacion.CodigoDocumentoIdentificacion;
                            vDocumentoIdentificacion.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocumentoIdentificacion.Descripcion;
                            vDocumentoIdentificacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vDocumentoIdentificacion.Estado;
                            vDocumentoIdentificacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentoIdentificacion.UsuarioCrea;
                            vDocumentoIdentificacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentoIdentificacion.FechaCrea;
                            vDocumentoIdentificacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentoIdentificacion.UsuarioModifica;
                            vDocumentoIdentificacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentoIdentificacion.FechaModifica;
                            vListaDocumentoIdentificacion.Add(vDocumentoIdentificacion);
                        }
                        return vListaDocumentoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoDocumentoIdentificacion(String pCodigoDocumentoIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacion_Consultar_CodigoDocumentoIdentificacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumentoIdentificacion", DbType.String, pCodigoDocumentoIdentificacion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vCodigoDocumentoIdentificacion = 0;
                        while (vDataReaderResults.Read())
                        {
                            vCodigoDocumentoIdentificacion = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vCodigoDocumentoIdentificacion;
                        }
                        return vCodigoDocumentoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
