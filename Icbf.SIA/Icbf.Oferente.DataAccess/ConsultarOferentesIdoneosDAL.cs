using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase ConsultarOferentesIdoneosDAL
    /// </summary>
    public class ConsultarOferentesIdoneosDAL : GeneralDAL
    {
        public ConsultarOferentesIdoneosDAL()
        {
        }
        /// <summary>
        /// Consultar Consultar Oferentes Idoneos
        /// </summary>
        /// <param name="pIdentificacion"></param>
        /// <param name="pSigla"></param>
        /// <param name="pNombreEntidadOferente"></param>
        /// <param name="pDepartamentoUbicacionSede"></param>
        /// <param name="pMunicipioUbicacionSede"></param>
        /// <param name="pDepartamentoUbicacionCaracterizacion"></param>
        /// <param name="pMunicipioUbicacionCaracterizacion"></param>
        /// <param name="pFormaVisualizarReporte"></param>
        /// <returns></returns>
        public List<ConsultarOferentesIdoneos> ConsultarConsultarOferentesIdoneos(int? pIdentificacion,
                                                                                  string pSigla,
                                                                                  string pNombreEntidadOferente,
                                                                                  string pDepartamentoUbicacionSede,
                                                                                  string pMunicipioUbicacionSede,
                                                                                  string pDepartamentoUbicacionCaracterizacion,
                                                                                  string pMunicipioUbicacionCaracterizacion,
                                                                                  string pFormaVisualizarReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ConsultarOferentesIdoneoss_Consultar"))
                {
                    if (pIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pIdentificacion);
                    if (pSigla != null)
                        vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pSigla);
                    if (pNombreEntidadOferente != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreEntidadOferente", DbType.String, pNombreEntidadOferente);
                    if (pDepartamentoUbicacionSede != null)
                        vDataBase.AddInParameter(vDbCommand, "@DepartamentoUbicacionSede", DbType.String, pDepartamentoUbicacionSede);
                    if (pMunicipioUbicacionSede != null)
                        vDataBase.AddInParameter(vDbCommand, "@MunicipioUbicacionSede", DbType.String, pMunicipioUbicacionSede);
                    if (pDepartamentoUbicacionCaracterizacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@DepartamentoUbicacionCaracterizacion", DbType.String, pDepartamentoUbicacionCaracterizacion);
                    if (pMunicipioUbicacionCaracterizacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@MunicipioUbicacionCaracterizacion", DbType.String, pMunicipioUbicacionCaracterizacion);
                    if (pFormaVisualizarReporte != null)
                        vDataBase.AddInParameter(vDbCommand, "@FormaVisualizarReporte", DbType.String, pFormaVisualizarReporte);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConsultarOferentesIdoneos> vListaConsultarOferentesIdoneos = new List<ConsultarOferentesIdoneos>();
                        while (vDataReaderResults.Read())
                        {
                            ConsultarOferentesIdoneos vConsultarOferentesIdoneos = new ConsultarOferentesIdoneos();
                            vConsultarOferentesIdoneos.Identificacion = vDataReaderResults[0] != DBNull.Value ? (vDataReaderResults[0].ToString()) : vConsultarOferentesIdoneos.Identificacion;
                                
                            if (vDataReaderResults.FieldCount == 2)
                            {
                                vConsultarOferentesIdoneos.NombreEntidadOferente = vDataReaderResults[1] != DBNull.Value ? (vDataReaderResults[1].ToString()) : vConsultarOferentesIdoneos.NombreEntidadOferente;
                            }else if (vDataReaderResults.FieldCount == 3)
                            {
                                vConsultarOferentesIdoneos.NombreEntidadOferente = vDataReaderResults[1] != DBNull.Value ? (vDataReaderResults[1].ToString()) : vConsultarOferentesIdoneos.NombreEntidadOferente;
                                vConsultarOferentesIdoneos.Sigla = vDataReaderResults[2] != DBNull.Value ? (vDataReaderResults[2].ToString()) : vConsultarOferentesIdoneos.Sigla;
                            }else if (vDataReaderResults.FieldCount == 4)
                            {
                                vConsultarOferentesIdoneos.NombreEntidadOferente = vDataReaderResults[1] != DBNull.Value ? (vDataReaderResults[1].ToString()) : vConsultarOferentesIdoneos.NombreEntidadOferente;
                                vConsultarOferentesIdoneos.Sigla = vDataReaderResults[2] != DBNull.Value ? (vDataReaderResults[2].ToString()) : vConsultarOferentesIdoneos.Sigla;
                                vConsultarOferentesIdoneos.DepartamentoUbicacionCaracterizacion = vDataReaderResults[3] != DBNull.Value ? (vDataReaderResults[3].ToString()) : vConsultarOferentesIdoneos.DepartamentoUbicacionCaracterizacion;
                            }
                            else if (vDataReaderResults.FieldCount == 5)
                            {
                                vConsultarOferentesIdoneos.NombreEntidadOferente = vDataReaderResults[1] != DBNull.Value ? (vDataReaderResults[1].ToString()) : vConsultarOferentesIdoneos.NombreEntidadOferente;
                                vConsultarOferentesIdoneos.Sigla = vDataReaderResults[2] != DBNull.Value ? (vDataReaderResults[2].ToString()) : vConsultarOferentesIdoneos.Sigla;
                                vConsultarOferentesIdoneos.DepartamentoUbicacionCaracterizacion = vDataReaderResults[3] != DBNull.Value ? (vDataReaderResults[3].ToString()) : vConsultarOferentesIdoneos.DepartamentoUbicacionCaracterizacion;
                                vConsultarOferentesIdoneos.MunicipioUbicacionCaracterizacion = vDataReaderResults[4] != DBNull.Value ? (vDataReaderResults[4].ToString()) : vConsultarOferentesIdoneos.MunicipioUbicacionCaracterizacion;
                            }
                            else
                            {
                                vConsultarOferentesIdoneos.NombreEntidadOferente = vDataReaderResults[1] != DBNull.Value ? (vDataReaderResults[1].ToString()) : vConsultarOferentesIdoneos.NombreEntidadOferente;
                                vConsultarOferentesIdoneos.Sigla = vDataReaderResults[2] != DBNull.Value ? (vDataReaderResults[2].ToString()) : vConsultarOferentesIdoneos.Sigla;
                                //vConsultarOferentesIdoneos.DepartamentoUbicacionSede = vDataReaderResults["Departamento Ubicacion Sede"] != DBNull.Value ? (vDataReaderResults["Departamento Ubicacion Sede"].ToString()) : vConsultarOferentesIdoneos.DepartamentoUbicacionSede;
                                //vConsultarOferentesIdoneos.MunicipioUbicacionSede = vDataReaderResults["Municipio Ubicacion Sede"] != DBNull.Value ? (vDataReaderResults["Municipio Ubicacion Sede"].ToString()) : vConsultarOferentesIdoneos.MunicipioUbicacionSede;
                                vConsultarOferentesIdoneos.DepartamentoUbicacionCaracterizacion = vDataReaderResults[3] != DBNull.Value ? (vDataReaderResults[3].ToString()) : vConsultarOferentesIdoneos.DepartamentoUbicacionCaracterizacion;
                                vConsultarOferentesIdoneos.MunicipioUbicacionCaracterizacion = vDataReaderResults[4] != DBNull.Value ? (vDataReaderResults[4].ToString()) : vConsultarOferentesIdoneos.MunicipioUbicacionCaracterizacion;
                                vConsultarOferentesIdoneos.Ranking = vDataReaderResults[5] != DBNull.Value ? (vDataReaderResults[5].ToString()) : vConsultarOferentesIdoneos.Ranking;
                            }
                            //vConsultarOferentesIdoneos.FormaVisualizarReporte = vDataReaderResults["Forma Visualizar Reporte"] != DBNull.Value ? (vDataReaderResults["Forma Visualizar Reporte"].ToString()) : vConsultarOferentesIdoneos.FormaVisualizarReporte;
                            vListaConsultarOferentesIdoneos.Add(vConsultarOferentesIdoneos);
                        }
                        return vListaConsultarOferentesIdoneos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
