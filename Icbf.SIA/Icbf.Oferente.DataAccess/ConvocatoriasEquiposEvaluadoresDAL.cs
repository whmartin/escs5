﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.DataAccess
{
    public class ConvocatoriasEquiposEvaluadoresDAL : GeneralDAL
    {
        public ConvocatoriasEquiposEvaluadoresDAL() { }

        public int InsertarConvocatoriasEquiposEvaluadores(ConvocatoriasEquiposEvaluadores pConvocatoriasEquiposEvaluadores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ConvocatoriasEquiposEvaluadores_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdConvocatoriasEquiposEvaluadores", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, pConvocatoriasEquiposEvaluadores.IdConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pConvocatoriasEquiposEvaluadores.IdEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pConvocatoriasEquiposEvaluadores.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pConvocatoriasEquiposEvaluadores.IdConvocatoriasEquiposEvaluadores = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdConvocatoriasEquiposEvaluadores").ToString());
                    GenerarLogAuditoria(pConvocatoriasEquiposEvaluadores, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConvocatoriasEquiposEvaluadores> ConsultarConvocatoriassEquiposEvaluadores(int pIdEquipoEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ConvocatoriassEquiposEvaluadores_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pIdEquipoEvaluador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ConvocatoriasEquiposEvaluadores> vListaConvocatoriasEquiposEvaluadores = new List<ConvocatoriasEquiposEvaluadores>();
                        while (vDataReaderResults.Read())
                        {
                            ConvocatoriasEquiposEvaluadores vConvocatoriasEquiposEvaluadores = new ConvocatoriasEquiposEvaluadores();
                            vConvocatoriasEquiposEvaluadores.IdConvocatoriasEquiposEvaluadores = vDataReaderResults["IdConvocatoriasEquiposEvaluadores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoriasEquiposEvaluadores"].ToString()) : vConvocatoriasEquiposEvaluadores.IdConvocatoriasEquiposEvaluadores;
                            vConvocatoriasEquiposEvaluadores.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vConvocatoriasEquiposEvaluadores.IdConvocatoria;
                            vConvocatoriasEquiposEvaluadores.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vConvocatoriasEquiposEvaluadores.IdEquipoEvaluador;
                            vListaConvocatoriasEquiposEvaluadores.Add(vConvocatoriasEquiposEvaluadores);
                        }
                        return vListaConvocatoriasEquiposEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarConvocatoriasEquiposEvaluadores(ConvocatoriasEquiposEvaluadores pConvocatoriasEquiposEvaluadores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ConvocatoriasEquiposEvaluadores_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, pConvocatoriasEquiposEvaluadores.IdConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pConvocatoriasEquiposEvaluadores.IdEquipoEvaluador);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConvocatoriasEquiposEvaluadores, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
