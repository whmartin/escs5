﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.DataAccess
{
    public class ObservacionesPendientesDAL : GeneralDAL
    {
        public ObservacionesPendientesDAL() { }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessEvaluador(string pNumeroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ObservacionesPendientessEvaluador_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObservacionesPendientes> vListaObservacionesPendientes = new List<ObservacionesPendientes>();
                        while (vDataReaderResults.Read())
                        {
                            ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

                            vObservacionesPendientes.IdObservacionConvocatoria = vDataReaderResults["IdObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObservacionConvocatoria"].ToString()) : vObservacionesPendientes.IdObservacionConvocatoria;
                            vObservacionesPendientes.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vObservacionesPendientes.IdTipoObservacion;
                            vObservacionesPendientes.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vObservacionesPendientes.IdUsuario;
                            vObservacionesPendientes.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? vDataReaderResults["NombreSolicitante"].ToString() : vObservacionesPendientes.NombreSolicitante;
                            vObservacionesPendientes.CorreoSolicitante = vDataReaderResults["CorreoSolicitante"] != DBNull.Value ? vDataReaderResults["CorreoSolicitante"].ToString() : vObservacionesPendientes.CorreoSolicitante;
                            vObservacionesPendientes.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? vDataReaderResults["Observacion"].ToString() : vObservacionesPendientes.Observacion;
                            vObservacionesPendientes.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vObservacionesPendientes.IdConvocatoria;
                            vObservacionesPendientes.FechaObservacion = vDataReaderResults["FechaObservacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacion"].ToString()) : vObservacionesPendientes.FechaObservacion;
                            vObservacionesPendientes.TipoObservacion = vDataReaderResults["TipoObservacion"] != DBNull.Value ? vDataReaderResults["TipoObservacion"].ToString() : vObservacionesPendientes.TipoObservacion;
                            vObservacionesPendientes.EstadoObservacion = vDataReaderResults["EstadoObservacion"] != DBNull.Value ? vDataReaderResults["EstadoObservacion"].ToString() : vObservacionesPendientes.EstadoObservacion;
                            vObservacionesPendientes.FechaUltimoEstado = vDataReaderResults["FechaUltimoEstado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaUltimoEstado"].ToString()) : vObservacionesPendientes.FechaUltimoEstado;

                            vListaObservacionesPendientes.Add(vObservacionesPendientes);
                        }
                        return vListaObservacionesPendientes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ObservacionesPendientes> ConsultarObservacionesPendientessCoordinador(string pNumeroDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ObservacionesPendientessCoordinador_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObservacionesPendientes> vListaObservacionesPendientes = new List<ObservacionesPendientes>();
                        while (vDataReaderResults.Read())
                        {
                            ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

                            vObservacionesPendientes.IdObservacionConvocatoria = vDataReaderResults["IdObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObservacionConvocatoria"].ToString()) : vObservacionesPendientes.IdObservacionConvocatoria;
                            vObservacionesPendientes.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vObservacionesPendientes.IdTipoObservacion;
                            vObservacionesPendientes.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vObservacionesPendientes.IdUsuario;
                            vObservacionesPendientes.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? vDataReaderResults["NombreSolicitante"].ToString() : vObservacionesPendientes.NombreSolicitante;
                            vObservacionesPendientes.CorreoSolicitante = vDataReaderResults["CorreoSolicitante"] != DBNull.Value ? vDataReaderResults["CorreoSolicitante"].ToString() : vObservacionesPendientes.CorreoSolicitante;
                            vObservacionesPendientes.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? vDataReaderResults["Observacion"].ToString() : vObservacionesPendientes.Observacion;
                            vObservacionesPendientes.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vObservacionesPendientes.IdConvocatoria;
                            vObservacionesPendientes.FechaObservacion = vDataReaderResults["FechaObservacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacion"].ToString()) : vObservacionesPendientes.FechaObservacion;
                            vObservacionesPendientes.TipoObservacion = vDataReaderResults["TipoObservacion"] != DBNull.Value ? vDataReaderResults["TipoObservacion"].ToString() : vObservacionesPendientes.TipoObservacion;
                            vObservacionesPendientes.EstadoObservacion = vDataReaderResults["EstadoObservacion"] != DBNull.Value ? vDataReaderResults["EstadoObservacion"].ToString() : vObservacionesPendientes.EstadoObservacion;
                            vObservacionesPendientes.FechaUltimoEstado = vDataReaderResults["FechaUltimoEstado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaUltimoEstado"].ToString()) : vObservacionesPendientes.FechaUltimoEstado;

                            vListaObservacionesPendientes.Add(vObservacionesPendientes);
                        }
                        return vListaObservacionesPendientes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        public ObservacionesPendientes ConsultarGestionMasRecienteAUnaObservacion(int pIdObservacionConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_GestionObservacionConvocatoriaMasReciente_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdObservacionConvocatoria", DbType.Int32, pIdObservacionConvocatoria);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();
                        while (vDataReaderResults.Read())
                        {
                            vObservacionesPendientes.IdGestionObservacionConvocatoria = vDataReaderResults["IdGestionObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionObservacionConvocatoria"].ToString()) : vObservacionesPendientes.IdGestionObservacionConvocatoria;
                            vObservacionesPendientes.EsDevolucion = vDataReaderResults["EsDevolucion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsDevolucion"].ToString()) : vObservacionesPendientes.EsDevolucion;
                            vObservacionesPendientes.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? vDataReaderResults["Descripcion"].ToString() : vObservacionesPendientes.Descripcion;
                            vObservacionesPendientes.FechaRespuesta = vDataReaderResults["FechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuesta"].ToString()) : vObservacionesPendientes.FechaRespuesta;
                        }
                        return vObservacionesPendientes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        public int InsertarGestionObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_GestionObservacionConvocatoria_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdGestionObservacionConvocatoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdObservacionConvocatoria", DbType.Int32, pObservacionesPendientes.IdObservacionConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pObservacionesPendientes.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@EsDevolucion", DbType.Boolean, pObservacionesPendientes.EsDevolucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pObservacionesPendientes.IdUsuario);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObservacionesPendientes.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObservacionesPendientes.IdGestionObservacionConvocatoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdGestionObservacionConvocatoria").ToString());
                    GenerarLogAuditoria(pObservacionesPendientes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ObservacionConvocatoria_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObservacionConvocatoria", DbType.Int32, pObservacionesPendientes.IdObservacionConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pObservacionesPendientes.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoObservacion", DbType.Int32, pObservacionesPendientes.IdEstadoObservacion);
                    vDataBase.AddInParameter(vDbCommand, "@Coordinador", DbType.Boolean, pObservacionesPendientes.EsCoordinador);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObservacionesPendientes.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObservacionesPendientes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientess()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ObservacionesPendientess_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObservacionesPendientes> vListaObservacionesPendientes = new List<ObservacionesPendientes>();
                        while (vDataReaderResults.Read())
                        {
                            ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

                            vObservacionesPendientes.IdObservacionConvocatoria = vDataReaderResults["IdObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObservacionConvocatoria"].ToString()) : vObservacionesPendientes.IdObservacionConvocatoria;
                            vObservacionesPendientes.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vObservacionesPendientes.IdTipoObservacion;
                            vObservacionesPendientes.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vObservacionesPendientes.IdConvocatoria;
                            vObservacionesPendientes.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? vDataReaderResults["NombreSolicitante"].ToString() : vObservacionesPendientes.NombreSolicitante;
                            vObservacionesPendientes.CorreoSolicitante = vDataReaderResults["CorreoSolicitante"] != DBNull.Value ? vDataReaderResults["CorreoSolicitante"].ToString() : vObservacionesPendientes.CorreoSolicitante;
                            vObservacionesPendientes.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? vDataReaderResults["Observacion"].ToString() : vObservacionesPendientes.Observacion;
                            vObservacionesPendientes.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vObservacionesPendientes.IdUsuario;
                            vObservacionesPendientes.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vObservacionesPendientes.NumeroDocumento;
                            vObservacionesPendientes.EsCoordinador = vDataReaderResults["EsCoordinador"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsCoordinador"].ToString()) : vObservacionesPendientes.EsCoordinador;
                            vObservacionesPendientes.TipoObservacion = vDataReaderResults["TipoObservacion"] != DBNull.Value ? vDataReaderResults["TipoObservacion"].ToString() : vObservacionesPendientes.TipoObservacion;
                            vObservacionesPendientes.EstadoObservacion = vDataReaderResults["EstadoObservacion"] != DBNull.Value ? vDataReaderResults["EstadoObservacion"].ToString() : vObservacionesPendientes.EstadoObservacion;
                            
                            vListaObservacionesPendientes.Add(vObservacionesPendientes);
                        }
                        return vListaObservacionesPendientes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObservacionesPendientes> ConsultarObservacionesPendientessContratista()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_ObservacionesPendientessContratista_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObservacionesPendientes> vListaObservacionesPendientes = new List<ObservacionesPendientes>();
                        while (vDataReaderResults.Read())
                        {
                            ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

                            vObservacionesPendientes.IdObservacionConvocatoria = vDataReaderResults["IdObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObservacionConvocatoria"].ToString()) : vObservacionesPendientes.IdObservacionConvocatoria;
                            vObservacionesPendientes.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vObservacionesPendientes.IdTipoObservacion;
                            vObservacionesPendientes.NombreSolicitante = vDataReaderResults["NombreSolicitante"] != DBNull.Value ? vDataReaderResults["NombreSolicitante"].ToString() : vObservacionesPendientes.NombreSolicitante;
                            vObservacionesPendientes.CorreoSolicitante = vDataReaderResults["CorreoSolicitante"] != DBNull.Value ? vDataReaderResults["CorreoSolicitante"].ToString() : vObservacionesPendientes.CorreoSolicitante;
                            vObservacionesPendientes.Observacion = vDataReaderResults["Observacion"] != DBNull.Value ? vDataReaderResults["Observacion"].ToString() : vObservacionesPendientes.Observacion;
                            vObservacionesPendientes.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vObservacionesPendientes.IdConvocatoria;
                            vObservacionesPendientes.FechaObservacion = vDataReaderResults["FechaObservacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacion"].ToString()) : vObservacionesPendientes.FechaObservacion;
                            vObservacionesPendientes.TipoObservacion = vDataReaderResults["TipoObservacion"] != DBNull.Value ? vDataReaderResults["TipoObservacion"].ToString() : vObservacionesPendientes.TipoObservacion;
                            vObservacionesPendientes.EstadoObservacion = vDataReaderResults["EstadoObservacion"] != DBNull.Value ? vDataReaderResults["EstadoObservacion"].ToString() : vObservacionesPendientes.EstadoObservacion;
                            vObservacionesPendientes.FechaUltimoEstado = vDataReaderResults["FechaUltimoEstado"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaUltimoEstado"].ToString()) : vObservacionesPendientes.FechaUltimoEstado;

                            vListaObservacionesPendientes.Add(vObservacionesPendientes);
                        }
                        return vListaObservacionesPendientes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ObservacionesPendientes ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(int pIdObservacionConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_GestionObservacionConvocatoriaMasRecienteDelEvaluadorParaContratista_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdObservacionConvocatoria", DbType.Int32, pIdObservacionConvocatoria);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();
                        while (vDataReaderResults.Read())
                        {
                            vObservacionesPendientes.IdGestionObservacionConvocatoria = vDataReaderResults["IdGestionObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionObservacionConvocatoria"].ToString()) : vObservacionesPendientes.IdGestionObservacionConvocatoria;
                            vObservacionesPendientes.EsDevolucion = vDataReaderResults["EsDevolucion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsDevolucion"].ToString()) : vObservacionesPendientes.EsDevolucion;
                            vObservacionesPendientes.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? vDataReaderResults["Descripcion"].ToString() : vObservacionesPendientes.Descripcion;
                            vObservacionesPendientes.FechaRespuesta = vDataReaderResults["FechaRespuesta"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuesta"].ToString()) : vObservacionesPendientes.FechaRespuesta;
                            vObservacionesPendientes.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vObservacionesPendientes.IdUsuario;
                        }
                        return vObservacionesPendientes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDescripcionGestionObservacionConvocatoria(ObservacionesPendientes pObservacionesPendientes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_GestionObservacionConvocatoria_Descripcion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObservacionConvocatoria", DbType.Int32, pObservacionesPendientes.IdGestionObservacionConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pObservacionesPendientes.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObservacionesPendientes.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObservacionesPendientes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
