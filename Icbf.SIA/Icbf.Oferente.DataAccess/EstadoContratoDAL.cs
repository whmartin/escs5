using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class EstadoContratoDAL : GeneralDAL
    {
        public EstadoContratoDAL()
        {
        }
        public int InsertarEstadoContrato(EstadoContrato pEstadoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadoContrato_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoContrato", DbType.String, pEstadoContrato.CodigoEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoContrato.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstadoContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadoContrato.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadoContrato.IdEstadoContrato = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEstadoContrato").ToString());
                    GenerarLogAuditoria(pEstadoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarEstadoContrato(EstadoContrato pEstadoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadoContrato_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pEstadoContrato.IdEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoContrato", DbType.String, pEstadoContrato.CodigoEstadoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoContrato.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstadoContrato.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadoContrato.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarEstadoContrato(EstadoContrato pEstadoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadoContrato_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pEstadoContrato.IdEstadoContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoContrato, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public EstadoContrato ConsultarEstadoContrato(int pIdEstadoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadoContrato_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoContrato", DbType.Int32, pIdEstadoContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoContrato vEstadoContrato = new EstadoContrato();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vEstadoContrato.IdEstadoContrato;
                            vEstadoContrato.CodigoEstadoContrato = vDataReaderResults["CodigoEstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoContrato"].ToString()) : vEstadoContrato.CodigoEstadoContrato;
                            vEstadoContrato.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoContrato.Descripcion;
                            vEstadoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vEstadoContrato.Estado;
                            vEstadoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoContrato.UsuarioCrea;
                            vEstadoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoContrato.FechaCrea;
                            vEstadoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoContrato.UsuarioModifica;
                            vEstadoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoContrato.FechaModifica;
                        }
                        return vEstadoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EstadoContrato> ConsultarEstadoContratos(String pCodigoEstadoContrato, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadoContratos_Consultar"))
                {
                    if (pCodigoEstadoContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoContrato", DbType.String, pCodigoEstadoContrato);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoContrato> vListaEstadoContrato = new List<EstadoContrato>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoContrato vEstadoContrato = new EstadoContrato();
                            vEstadoContrato.IdEstadoContrato = vDataReaderResults["IdEstadoContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoContrato"].ToString()) : vEstadoContrato.IdEstadoContrato;
                            vEstadoContrato.CodigoEstadoContrato = vDataReaderResults["CodigoEstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoContrato"].ToString()) : vEstadoContrato.CodigoEstadoContrato;
                            vEstadoContrato.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoContrato.Descripcion;
                            vEstadoContrato.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vEstadoContrato.Estado;
                            vEstadoContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoContrato.UsuarioCrea;
                            vEstadoContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoContrato.FechaCrea;
                            vEstadoContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoContrato.UsuarioModifica;
                            vEstadoContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoContrato.FechaModifica;
                            vListaEstadoContrato.Add(vEstadoContrato);
                        }
                        return vListaEstadoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoEstadoContrato(String pCodigoEstadoContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EstadoContrato_Consultar_CodigoEstadoContrato"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoContrato", DbType.String, pCodigoEstadoContrato);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoEstadoContrato = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoEstadoContrato = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoEstadoContrato;
                        }
                        return vExisteCodigoEstadoContrato;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
