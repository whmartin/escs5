﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ConfiguracionTareasDAL : GeneralDAL
    {
        public ConfiguracionTareasDAL() {}

        /// <summary>
        /// Método de inserción para la entidad ConfiguracionTareas
        /// </summary>
        /// <param name="pConfiguracionTareas></param>

        public int InsertarConfiguracionTareas(ConfiguracionTareas pConfiguracionTareas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_BancoOferentes_ConfiguracionAsignacionTareas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@CupoMaximoPorPersona", DbType.Int32, pConfiguracionTareas.CupoMaximoPorPersona);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeMaximoAlerta", DbType.Decimal, pConfiguracionTareas.PorcentajeMaximoAlerta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pConfiguracionTareas.UsuarioCrea);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConfiguracionTareas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConfiguracionTareas
        /// </summary>
        /// <param name="pIdConfigAsignacionTareas"></param>
        public ConfiguracionTareas ConsultarConfiguracionTareas()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_BancoOferentes_ConfiguracionAsignacionTareas_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ConfiguracionTareas vConfiguracionTareas = new ConfiguracionTareas();
                        while (vDataReaderResults.Read())
                        {
                            vConfiguracionTareas.IdConfigAsignacionTareas = vDataReaderResults["IdConfiguracionAsignacionTareas"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConfiguracionAsignacionTareas"].ToString()) : vConfiguracionTareas.IdConfigAsignacionTareas;
                            vConfiguracionTareas.CupoMaximoPorPersona = vDataReaderResults["CupoMaximoPorPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CupoMaximoPorPersona"].ToString()) : vConfiguracionTareas.CupoMaximoPorPersona;
                            vConfiguracionTareas.PorcentajeMaximoAlerta = vDataReaderResults["PorcentajeMaximoAlerta"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeMaximoAlerta"].ToString()) : vConfiguracionTareas.PorcentajeMaximoAlerta;
                        }
                        return vConfiguracionTareas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de modificación para la entidad ConfiguracionTareas
        /// </summary>
        /// <param name="pConfiguracionTareas"></param>
        public int ModificarConfiguracionTareas(ConfiguracionTareas pConfiguracionTareas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_BancoOferentes_ConfiguracionAsignacionTareas_Modificar"))
                {
                    int vResultado;
                    
                    vDataBase.AddInParameter(vDbCommand, "@CupoMaximoPorPersona", DbType.Int32, pConfiguracionTareas.CupoMaximoPorPersona);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeMaximoAlerta", DbType.Decimal, pConfiguracionTareas.PorcentajeMaximoAlerta);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pConfiguracionTareas.UsuarioModifica);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pConfiguracionTareas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
    }
}
