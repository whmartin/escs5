using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase DocumentoIdentificacionTerceroDAL
    /// </summary>
    public class DocumentoIdentificacionTerceroDAL : GeneralDAL
    {
        public DocumentoIdentificacionTerceroDAL()
        {
        }
        /// <summary>
        /// Inserta un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int InsertarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacionTercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocumentoIdentificacionTercero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDocumentoIdentificacionTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, pDocumentoIdentificacionTercero.IdDocumentoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Int32, pDocumentoIdentificacionTercero.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pDocumentoIdentificacionTercero.EstadoCertificado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicion", DbType.DateTime, pDocumentoIdentificacionTercero.FechaExpedicion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpiracion", DbType.DateTime, pDocumentoIdentificacionTercero.FechaExpiracion);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocumentoIdentificacionTercero.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (!string.IsNullOrEmpty(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentoIdentificacionTercero").ToString()))
                       pDocumentoIdentificacionTercero.IdDocumentoIdentificacionTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocumentoIdentificacionTercero").ToString());
                   
                    GenerarLogAuditoria(pDocumentoIdentificacionTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Modifica un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int ModificarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacionTercero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacionTercero", DbType.Int32, pDocumentoIdentificacionTercero.IdDocumentoIdentificacionTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDocumentoIdentificacionTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, pDocumentoIdentificacionTercero.IdDocumentoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pDocumentoIdentificacionTercero.EstadoCertificado);
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Int32, pDocumentoIdentificacionTercero.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpedicion", DbType.DateTime, pDocumentoIdentificacionTercero.FechaExpedicion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpiracion", DbType.DateTime, pDocumentoIdentificacionTercero.FechaExpiracion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocumentoIdentificacionTercero.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocumentoIdentificacionTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Elimina un Documento de Identificacion de Tercero
        /// </summary>
        /// <param name="pDocumentoIdentificacionTercero"></param>
        /// <returns></returns>
        public int EliminarDocumentoIdentificacionTercero(DocumentoIdentificacionTercero pDocumentoIdentificacionTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacionTercero_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdArchivo", DbType.Int32,18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDocumentoIdentificacionTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumentoIdentificacion", DbType.Int32, pDocumentoIdentificacionTercero.IdDocumentoIdentificacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (!string.IsNullOrEmpty(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString()))
                    {
                        vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdArchivo").ToString());
                    }
                    else { vResultado = 0; }

                
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Consultar un Documento de Identificacion de Terceros
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public List<DocumentoIdentificacionTercero> ConsultarDocIdentificacionTerceros(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DocumentoIdentificacionTerceros_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocumentoIdentificacionTercero> vListaDocumentoIdentificacionTercero = new List<DocumentoIdentificacionTercero>();
                        while (vDataReaderResults.Read())
                        {
                            DocumentoIdentificacionTercero vDocumentoIdentificacionTercero = new DocumentoIdentificacionTercero();
                            vDocumentoIdentificacionTercero.IdDocumentoIdentificacionTercero = vDataReaderResults["IdDocumentoIdentificacionTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoIdentificacionTercero"].ToString()) : vDocumentoIdentificacionTercero.IdDocumentoIdentificacionTercero;
                            vDocumentoIdentificacionTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDocumentoIdentificacionTercero.IdTercero;
                            vDocumentoIdentificacionTercero.IdDocumentoIdentificacion = vDataReaderResults["IdDocumentoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumentoIdentificacion"].ToString()) : vDocumentoIdentificacionTercero.IdDocumentoIdentificacion;
                            vDocumentoIdentificacionTercero.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArchivo"].ToString()) : vDocumentoIdentificacionTercero.IdArchivo;
                            vDocumentoIdentificacionTercero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocumentoIdentificacionTercero.Descripcion;
                            vDocumentoIdentificacionTercero.Certificado = vDataReaderResults["Certificado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Certificado"].ToString()) : vDocumentoIdentificacionTercero.Certificado;
                            vDocumentoIdentificacionTercero.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vDocumentoIdentificacionTercero.Estado;
                            vDocumentoIdentificacionTercero.EstadoCertificado = vDataReaderResults["EstadoCertificado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoCertificado"].ToString()) : vDocumentoIdentificacionTercero.EstadoCertificado;
                            vDocumentoIdentificacionTercero.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vDocumentoIdentificacionTercero.NombreArchivo;
                            vDocumentoIdentificacionTercero.FechaExpedicion = vDataReaderResults["FechaExpedicion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicion"].ToString()) : vDocumentoIdentificacionTercero.FechaExpedicion;
                            vDocumentoIdentificacionTercero.FechaExpiracion = vDataReaderResults["FechaExpiracion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpiracion"].ToString()) : vDocumentoIdentificacionTercero.FechaExpiracion;
                            vDocumentoIdentificacionTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocumentoIdentificacionTercero.UsuarioCrea;
                            vDocumentoIdentificacionTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocumentoIdentificacionTercero.FechaCrea;
                            vDocumentoIdentificacionTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocumentoIdentificacionTercero.UsuarioModifica;
                            vDocumentoIdentificacionTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocumentoIdentificacionTercero.FechaModifica;
                            
                            vListaDocumentoIdentificacionTercero.Add(vDocumentoIdentificacionTercero);
                        }
                        return vListaDocumentoIdentificacionTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
