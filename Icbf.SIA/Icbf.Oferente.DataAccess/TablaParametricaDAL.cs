using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TablaParametricaDAL : GeneralDAL
    {
        public TablaParametricaDAL()
        {
        }
        public int InsertarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TablaParametrica_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTablaParametrica", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTablaParametrica", DbType.String, pTablaParametrica.CodigoTablaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTablaParametrica", DbType.String, pTablaParametrica.NombreTablaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@Url", DbType.String, pTablaParametrica.Url);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTablaParametrica.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTablaParametrica.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTablaParametrica.IdTablaParametrica = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTablaParametrica").ToString());
                    GenerarLogAuditoria(pTablaParametrica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TablaParametrica_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTablaParametrica", DbType.Int32, pTablaParametrica.IdTablaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTablaParametrica", DbType.String, pTablaParametrica.CodigoTablaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTablaParametrica", DbType.String, pTablaParametrica.NombreTablaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@Url", DbType.String, pTablaParametrica.Url);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTablaParametrica.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTablaParametrica.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTablaParametrica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TablaParametrica_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTablaParametrica", DbType.Int32, pTablaParametrica.IdTablaParametrica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTablaParametrica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TablaParametrica ConsultarTablaParametrica(int pIdTablaParametrica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TablaParametrica_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTablaParametrica", DbType.Int32, pIdTablaParametrica);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TablaParametrica vTablaParametrica = new TablaParametrica();
                        while (vDataReaderResults.Read())
                        {
                            vTablaParametrica.IdTablaParametrica = vDataReaderResults["IdTablaParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTablaParametrica"].ToString()) : vTablaParametrica.IdTablaParametrica;
                            vTablaParametrica.CodigoTablaParametrica = vDataReaderResults["CodigoTablaParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTablaParametrica"].ToString()) : vTablaParametrica.CodigoTablaParametrica;
                            vTablaParametrica.NombreTablaParametrica = vDataReaderResults["NombreTablaParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTablaParametrica"].ToString()) : vTablaParametrica.NombreTablaParametrica;
                            vTablaParametrica.Url = vDataReaderResults["Url"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Url"].ToString()) : vTablaParametrica.Url;
                            vTablaParametrica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTablaParametrica.Estado;
                            vTablaParametrica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTablaParametrica.UsuarioCrea;
                            vTablaParametrica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTablaParametrica.FechaCrea;
                            vTablaParametrica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTablaParametrica.UsuarioModifica;
                            vTablaParametrica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTablaParametrica.FechaModifica;
                        }
                        return vTablaParametrica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TablaParametrica> ConsultarTablaParametricas(String pCodigoTablaParametrica, String pNombreTablaParametrica, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TablaParametricas_Consultar"))
                {
                    if(pCodigoTablaParametrica != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTablaParametrica", DbType.String, pCodigoTablaParametrica);
                    if(pNombreTablaParametrica != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTablaParametrica", DbType.String, pNombreTablaParametrica);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TablaParametrica> vListaTablaParametrica = new List<TablaParametrica>();
                        while (vDataReaderResults.Read())
                        {
                                TablaParametrica vTablaParametrica = new TablaParametrica();
                            vTablaParametrica.IdTablaParametrica = vDataReaderResults["IdTablaParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTablaParametrica"].ToString()) : vTablaParametrica.IdTablaParametrica;
                            vTablaParametrica.CodigoTablaParametrica = vDataReaderResults["CodigoTablaParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTablaParametrica"].ToString()) : vTablaParametrica.CodigoTablaParametrica;
                            vTablaParametrica.NombreTablaParametrica = vDataReaderResults["NombreTablaParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTablaParametrica"].ToString()) : vTablaParametrica.NombreTablaParametrica;
                            vTablaParametrica.Url = vDataReaderResults["Url"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Url"].ToString()) : vTablaParametrica.Url;
                            vTablaParametrica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTablaParametrica.Estado;
                            if (vTablaParametrica.Estado == true)
                            {
                                 vTablaParametrica.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vTablaParametrica.DescripcionEstado = "Inactivo";
                            }
                            vTablaParametrica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTablaParametrica.UsuarioCrea;
                            vTablaParametrica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTablaParametrica.FechaCrea;
                            vTablaParametrica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTablaParametrica.UsuarioModifica;
                            vTablaParametrica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTablaParametrica.FechaModifica;
                                vListaTablaParametrica.Add(vTablaParametrica);
                        }
                        return vListaTablaParametrica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
