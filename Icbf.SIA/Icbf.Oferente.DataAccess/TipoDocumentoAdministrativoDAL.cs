using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase TipoDocumentoAdministrativoDAL
    /// </summary>
    public class TipoDocumentoAdministrativoDAL : GeneralDAL
    {
        public TipoDocumentoAdministrativoDAL()
        {
        }

        /// <summary>
        /// Insertar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int InsertarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoDocumentoAdministrativo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pTipoDocumentoAdministrativo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoDocumentoAdministrativo.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Personal", DbType.Int32, pTipoDocumentoAdministrativo.Personal);
                    vDataBase.AddInParameter(vDbCommand, "@Instituciones", DbType.Int32, pTipoDocumentoAdministrativo.Instituciones);
                    vDataBase.AddInParameter(vDbCommand, "@Seleccion", DbType.Int32, pTipoDocumentoAdministrativo.Seleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTipoDocumentoAdministrativo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoDocumentoAdministrativo.UsuarioCrea);
                    //
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoDocumentoAdministrativo.IdTipoDocAdministrativo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoDocAdministrativo").ToString());
                    GenerarLogAuditoria(pTipoDocumentoAdministrativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int ModificarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoDocumentoAdministrativo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pTipoDocumentoAdministrativo.IdTipoDocAdministrativo);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pTipoDocumentoAdministrativo.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoDocumentoAdministrativo.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Personal", DbType.Int32, pTipoDocumentoAdministrativo.Personal);
                    vDataBase.AddInParameter(vDbCommand, "@Instituciones", DbType.Int32, pTipoDocumentoAdministrativo.Instituciones);
                    vDataBase.AddInParameter(vDbCommand, "@Seleccion", DbType.Int32, pTipoDocumentoAdministrativo.Seleccion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTipoDocumentoAdministrativo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoDocumentoAdministrativo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocumentoAdministrativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pTipoDocumentoAdministrativo"></param>
        /// <returns></returns>
        public int EliminarTipoDocumentoAdministrativo(TipoDocumentoAdministrativo pTipoDocumentoAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoDocumentoAdministrativo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pTipoDocumentoAdministrativo.IdTipoDocAdministrativo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocumentoAdministrativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Documento Administrativo
        /// </summary>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <returns></returns>
        public TipoDocumentoAdministrativo ConsultarTipoDocumentoAdministrativo(int pIdTipoDocAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoDocumentoAdministrativo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pIdTipoDocAdministrativo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocumentoAdministrativo vTipoDocumentoAdministrativo = new TipoDocumentoAdministrativo();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumentoAdministrativo.IdTipoDocAdministrativo = vDataReaderResults["IdTipoDocAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocAdministrativo"].ToString()) : vTipoDocumentoAdministrativo.IdTipoDocAdministrativo;
                            vTipoDocumentoAdministrativo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vTipoDocumentoAdministrativo.IdFormatoArchivo;
                            vTipoDocumentoAdministrativo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocumentoAdministrativo.Descripcion;
                            vTipoDocumentoAdministrativo.Personal = vDataReaderResults["Personal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Personal"].ToString()) : vTipoDocumentoAdministrativo.Personal;
                            vTipoDocumentoAdministrativo.Instituciones = vDataReaderResults["Instituciones"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Instituciones"].ToString()) : vTipoDocumentoAdministrativo.Instituciones;
                            vTipoDocumentoAdministrativo.Seleccion = vDataReaderResults["Seleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Seleccion"].ToString()) : vTipoDocumentoAdministrativo.Seleccion;
                            vTipoDocumentoAdministrativo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoAdministrativo.Estado;
                            vTipoDocumentoAdministrativo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoAdministrativo.UsuarioCrea;
                            vTipoDocumentoAdministrativo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoAdministrativo.FechaCrea;
                            vTipoDocumentoAdministrativo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoAdministrativo.UsuarioModifica;
                            vTipoDocumentoAdministrativo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoAdministrativo.FechaModifica;
                        }
                        return vTipoDocumentoAdministrativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Documento Administrativos
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <param name="pPersonal"></param>
        /// <param name="pInstituciones"></param>
        /// <param name="pSeleccion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoDocumentoAdministrativo> ConsultarTipoDocumentoAdministrativos(int? pIdAdministrativo, int? pIdTipoDocAdministrativo, int? pPersonal, int? pInstituciones, int? pSeleccion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoDocumentoAdministrativos_Consultar"))
                {
                    if (pIdAdministrativo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pIdAdministrativo);
                    if (pIdTipoDocAdministrativo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pIdTipoDocAdministrativo);
                    if (pPersonal != null)
                        vDataBase.AddInParameter(vDbCommand, "@Personal", DbType.Int32, pPersonal);
                    if (pInstituciones != null)
                        vDataBase.AddInParameter(vDbCommand, "@Instituciones", DbType.Int32, pInstituciones);
                    if (pSeleccion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Seleccion", DbType.Int32, pSeleccion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumentoAdministrativo> vListaTipoDocumentoAdministrativo = new List<TipoDocumentoAdministrativo>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumentoAdministrativo vTipoDocumentoAdministrativo = new TipoDocumentoAdministrativo();
                            vTipoDocumentoAdministrativo.IdAdministrativoDocumento = vDataReaderResults["IdAdministrativoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativoDocumento"].ToString()) : vTipoDocumentoAdministrativo.IdAdministrativoDocumento;
                            vTipoDocumentoAdministrativo.IdTipoDocAdministrativo = vDataReaderResults["IdTipoDocAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocAdministrativo"].ToString()) : vTipoDocumentoAdministrativo.IdTipoDocAdministrativo;
                            vTipoDocumentoAdministrativo.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vTipoDocumentoAdministrativo.IdFormatoArchivo;
                            vTipoDocumentoAdministrativo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocumentoAdministrativo.Descripcion;
                            vTipoDocumentoAdministrativo.Personal = vDataReaderResults["Personal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Personal"].ToString()) : vTipoDocumentoAdministrativo.Personal;
                            vTipoDocumentoAdministrativo.Instituciones = vDataReaderResults["Instituciones"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Instituciones"].ToString()) : vTipoDocumentoAdministrativo.Instituciones;
                            vTipoDocumentoAdministrativo.Seleccion = vDataReaderResults["Seleccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Seleccion"].ToString()) : vTipoDocumentoAdministrativo.Seleccion;
                            vTipoDocumentoAdministrativo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoAdministrativo.Estado;
                            vTipoDocumentoAdministrativo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoAdministrativo.UsuarioCrea;
                            vTipoDocumentoAdministrativo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoAdministrativo.FechaCrea;
                            vTipoDocumentoAdministrativo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoAdministrativo.UsuarioModifica;
                            vTipoDocumentoAdministrativo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoAdministrativo.FechaModifica;
                            vTipoDocumentoAdministrativo.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vTipoDocumentoAdministrativo.NombreArchivo;
                            vTipoDocumentoAdministrativo.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vTipoDocumentoAdministrativo.NombreArchivoOri;
                            vTipoDocumentoAdministrativo.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArchivo"].ToString()) : vTipoDocumentoAdministrativo.IdArchivo;

                            vListaTipoDocumentoAdministrativo.Add(vTipoDocumentoAdministrativo);
                        }
                        return vListaTipoDocumentoAdministrativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
