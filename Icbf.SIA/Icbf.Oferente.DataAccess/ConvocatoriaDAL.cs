﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Icbf.Oferente.DataAccess
{
    public class ConvocatoriaDAL : GeneralDAL
    {
        public List<Convocatoria> ObtenerConvocatorias(int? numero, DateTime? fechaPublicacionD, DateTime? fechaPublicacionP, int? Estado, int? Regional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_ConsultarConvocatorias"))
                {
                    if (Estado.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, Estado);
                    if (numero.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@Numero", DbType.Int32, numero);
                    if (Regional.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, Regional);
                    if (fechaPublicacionD.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionD", DbType.DateTime, fechaPublicacionD);
                    if (fechaPublicacionP.HasValue)
                        vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionP", DbType.DateTime, fechaPublicacionP);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Convocatoria> vListaConvocatorias = new List<Convocatoria>();
                        while (vDataReaderResults.Read())
                        {
                            Convocatoria vConvocatoria = new Convocatoria();
                            vConvocatoria.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vConvocatoria.IdConvocatoria;
                            vConvocatoria.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vConvocatoria.Nombre;
                            vConvocatoria.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vConvocatoria.Estado;
                            vConvocatoria.DescripcionRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vConvocatoria.DescripcionRegional;
                            vConvocatoria.FechaPublicacionD = vDataReaderResults["FechaPublicacionD"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionD"].ToString()) : vConvocatoria.FechaPublicacionD;
                            vConvocatoria.FechaPublicacionP = vDataReaderResults["FechaPublicacionP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionP"].ToString()) : vConvocatoria.FechaPublicacionP;
                            vConvocatoria.Numero = vDataReaderResults["Numero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Numero"].ToString()) : vConvocatoria.Numero;
                            vListaConvocatorias.Add(vConvocatoria);
                        }
                        return vListaConvocatorias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public IDictionary<int, string> ConsultarTipoObservaciones()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_ConsultarTipoObservaciones"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<int, string> vListaEstados = new Dictionary<int, string>();
                        int key = -1;
                        string value = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            KeyValuePair<int, string> vTipoObservaciones = new KeyValuePair<int, string>();
                            key = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vTipoObservaciones.Key;
                            value = vDataReaderResults["DescripcionTipo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipo"].ToString()) : vTipoObservaciones.Value;

                            if (key != -1)
                                vListaEstados.Add(key, value);
                        }
                        return vListaEstados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Convocatoria ConsultarConvocatoriaPorId(int id)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_ConsultarConvocatoriaPorId"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, id);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Convocatoria vConvocatoria = null;

                        while (vDataReaderResults.Read())
                        {
                            vConvocatoria = new Convocatoria();
                            vConvocatoria.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vConvocatoria.IdConvocatoria;
                            vConvocatoria.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vConvocatoria.Nombre;
                            vConvocatoria.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vConvocatoria.Estado;
                            vConvocatoria.CodigoEstado = vDataReaderResults["CodigoEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstado"].ToString()) : vConvocatoria.CodigoEstado;
                            vConvocatoria.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Objeto"].ToString()) : vConvocatoria.Objeto;
                            vConvocatoria.DescripcionRegional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vConvocatoria.DescripcionRegional;

                            // Preliminares.
                            vConvocatoria.FechaPublicacionP = vDataReaderResults["FechaPublicacionP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionP"].ToString()) : vConvocatoria.FechaPublicacionP;
                            vConvocatoria.FechaObservacionesInicioP = vDataReaderResults["FechaObservacionesInicioP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacionesInicioP"].ToString()) : vConvocatoria.FechaObservacionesInicioP;
                            vConvocatoria.FechaObservacionesFinP = vDataReaderResults["FechaObservacionesFinP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacionesFinP"].ToString()) : vConvocatoria.FechaObservacionesFinP;
                            vConvocatoria.FechaRespuestasInicioP = vDataReaderResults["FechaRespuestasInicioP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuestasInicioP"].ToString()) : vConvocatoria.FechaRespuestasInicioP;
                            vConvocatoria.FechaRespuestasFinP = vDataReaderResults["FechaRespuestasFinP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuestasFinP"].ToString()) : vConvocatoria.FechaRespuestasFinP;
                            // Definitivas
                            vConvocatoria.FechaPublicacionD = vDataReaderResults["FechaPublicacionD"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionD"].ToString()) : vConvocatoria.FechaPublicacionD;
                            vConvocatoria.FechaObservacionesInicioD = vDataReaderResults["FechaObservacionesInicioD"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacionesInicioD"].ToString()) : vConvocatoria.FechaObservacionesInicioD;
                            vConvocatoria.FechaObservacionesFinD = vDataReaderResults["FechaObservacionesFinD"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaObservacionesFinD"].ToString()) : vConvocatoria.FechaObservacionesFinD;
                            vConvocatoria.FechaRespuestasInicioD = vDataReaderResults["FechaRespuestasInicioD"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuestasInicioD"].ToString()) : vConvocatoria.FechaRespuestasInicioD;
                            vConvocatoria.FechaRespuestasFinD = vDataReaderResults["FechaRespuestasFinD"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaRespuestasFinD"].ToString()) : vConvocatoria.FechaRespuestasFinD;
                            //Inscripciones
                            vConvocatoria.FechaInicioInscripcionOferente = vDataReaderResults["FechaInicioInscripcionOferente"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicioInscripcionOferente"].ToString()) : vConvocatoria.FechaInicioInscripcionOferente;
                            vConvocatoria.FechaFinInscripcionOferente = vDataReaderResults["FechaFinInscripcionOferente"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinInscripcionOferente"].ToString()) : vConvocatoria.FechaFinInscripcionOferente;

                            vConvocatoria.Numero = vDataReaderResults["Numero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Numero"].ToString()) : vConvocatoria.Numero;
                            vConvocatoria.EmpleadoSolicitante = vDataReaderResults["NombresSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombresSolicitante"].ToString()) : vConvocatoria.EmpleadoSolicitante;
                            vConvocatoria.CargoSolicitante = vDataReaderResults["CargoSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CargoSolicitante"].ToString()) : vConvocatoria.CargoSolicitante;
                            vConvocatoria.Dependencia = vDataReaderResults["DependenciaSolicitante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaSolicitante"].ToString()) : vConvocatoria.Dependencia;
                            vConvocatoria.IdEmpleadoSolicitante = vDataReaderResults["IdEmpleadoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoSolicitante"].ToString()) : vConvocatoria.IdEmpleadoSolicitante;

                        }

                        return vConvocatoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int CrearConvocatoria(Convocatoria item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, item.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, item.Objeto);
                    vDataBase.AddInParameter(vDbCommand, "@Numero", DbType.Int16, item.Numero);

                    // Fechas Definitivas.
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionD", DbType.DateTime, item.FechaPublicacionD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesInicialD", DbType.DateTime, item.FechaObservacionesInicioD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesFinalD", DbType.DateTime, item.FechaObservacionesFinD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasInicialD", DbType.DateTime, item.FechaRespuestasInicioD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasFinalD", DbType.DateTime, item.FechaRespuestasFinD);

                    // Fechas Preliminares
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionP", DbType.DateTime, item.FechaPublicacionP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesInicialP", DbType.DateTime, item.FechaObservacionesInicioP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesFinalP", DbType.DateTime, item.FechaObservacionesFinP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasInicialP", DbType.DateTime, item.FechaRespuestasInicioP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasFinalP", DbType.DateTime, item.FechaRespuestasFinP);

                    // Fechas Inscripción
                    vDataBase.AddInParameter(vDbCommand, "@FechaInscripcionInicial", DbType.DateTime, item.FechaInicioInscripcionOferente);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInscripcionFinal", DbType.DateTime, item.FechaFinInscripcionOferente);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, item.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    item.IdConvocatoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdConvocatoria").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ActualizarConvocatoria(Convocatoria item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, item.IdConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, item.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Objeto", DbType.String, item.Objeto);
                    vDataBase.AddInParameter(vDbCommand, "@Numero", DbType.Int16, item.Numero);

                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionD", DbType.DateTime, item.FechaPublicacionD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesInicialD", DbType.DateTime, item.FechaObservacionesInicioD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesFinalD", DbType.DateTime, item.FechaObservacionesFinD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasInicialD", DbType.DateTime, item.FechaRespuestasInicioD);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasFinalD", DbType.DateTime, item.FechaRespuestasFinD);

                    // Fechas Preliminares
                    vDataBase.AddInParameter(vDbCommand, "@FechaPublicacionP", DbType.DateTime, item.FechaPublicacionP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesInicialP", DbType.DateTime, item.FechaObservacionesInicioP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaObservacionesFinalP", DbType.DateTime, item.FechaObservacionesFinP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasInicialP", DbType.DateTime, item.FechaRespuestasInicioP);
                    vDataBase.AddInParameter(vDbCommand, "@FechaRespuestasFinalP", DbType.DateTime, item.FechaRespuestasFinP);

                    // Fechas Inscripción
                    vDataBase.AddInParameter(vDbCommand, "@FechaInscripcionInicial", DbType.DateTime, item.FechaInicioInscripcionOferente);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInscripcionFinal", DbType.DateTime, item.FechaFinInscripcionOferente);


                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, item.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int CambiarEstadoConvocatoria(int idConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_ActualizarEstado"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, idConvocatoria);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Dictionary<int, string> ConsultarEstados()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_ConsultarEstados"))
                {

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Dictionary<int, string> vListaEstados = new Dictionary<int, string>();
                        int key = -1;
                        string value = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            KeyValuePair<int, string> vEstado = new KeyValuePair<int, string>();
                            key = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vEstado.Key;
                            value = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vEstado.Value;

                            if (key != -1)
                                vListaEstados.Add(key, value);
                        }
                        return vListaEstados;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarSolicitanteConvocatoriaLupa(Convocatoria item)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_BancoOferentes_Convocatoria_InsertarSoliicitante"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdConvocatoria", DbType.Int32, item.IdConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdDependenciaSolicitante", DbType.Int32, item.IdDependenciaSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdEmpleadoSolicitante", DbType.Int32, item.IdEmpleadoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdRegionalSolicitante", DbType.Int32, item.IdRegionalSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoSolicitante", DbType.Int32, item.IdCargoSolicitante);
                    vDataBase.AddInParameter(vDbCommand, "@usuarioModifica", DbType.String, item.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<Convocatoria> ConsultarConvocatoriass(string pNombreConvocatoria,int pNumero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Convocatoriass_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombreConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@Numero", DbType.String, pNumero);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Convocatoria> vListaConvocatorias = new List<Convocatoria>();
                        while (vDataReaderResults.Read())
                        {
                            Convocatoria vConvocatoria = new Convocatoria();
                            vConvocatoria.IdConvocatoria = vDataReaderResults["IdConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdConvocatoria"].ToString()) : vConvocatoria.IdConvocatoria;
                            vConvocatoria.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? vDataReaderResults["Nombre"].ToString() : vConvocatoria.Nombre;
                            vConvocatoria.Objeto = vDataReaderResults["Objeto"] != DBNull.Value ? vDataReaderResults["Objeto"].ToString() : vConvocatoria.Objeto;
                            vConvocatoria.Numero = vDataReaderResults["Numero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Numero"].ToString()) : vConvocatoria.Numero;
                            vConvocatoria.IdRegional = vDataReaderResults["IdRegional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegional"].ToString()) : vConvocatoria.IdRegional;
                            vConvocatoria.IdDependenciaSolicitante = vDataReaderResults["IdDependenciaSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDependenciaSolicitante"].ToString()) : vConvocatoria.IdDependenciaSolicitante;
                            vConvocatoria.IdEmpleadoSolicitante = vDataReaderResults["IdEmpleadoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEmpleadoSolicitante"].ToString()) : vConvocatoria.IdEmpleadoSolicitante;
                            vConvocatoria.IdRegionalSolicitante = vDataReaderResults["IdRegionalSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalSolicitante"].ToString()) : vConvocatoria.IdRegionalSolicitante;
                            vConvocatoria.IdCargoSolicitante = vDataReaderResults["IdCargoSolicitante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoSolicitante"].ToString()) : vConvocatoria.IdCargoSolicitante;
                            vConvocatoria.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vConvocatoria.IdEstado;
                            vConvocatoria.FechaPublicacionP = vDataReaderResults["FechaPublicacionPreliminar"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionPreliminar"].ToString()) : vConvocatoria.FechaPublicacionP;
                            vConvocatoria.FechaPublicacionD = vDataReaderResults["FechaPublicacionDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaPublicacionDefinitiva"].ToString()) : vConvocatoria.FechaPublicacionD;
                            vConvocatoria.FechaObservacionesInicioP = vDataReaderResults["FechaIniObservacionesPreliminar"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIniObservacionesPreliminar"].ToString()) : vConvocatoria.FechaObservacionesInicioP;
                            vConvocatoria.FechaObservacionesFinP = vDataReaderResults["FechaFinObservacionesPremilinar"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinObservacionesPremilinar"].ToString()) : vConvocatoria.FechaObservacionesFinP;
                            vConvocatoria.FechaRespuestasInicioP = vDataReaderResults["FechaIniRespuestasPreliminar"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIniRespuestasPreliminar"].ToString()) : vConvocatoria.FechaRespuestasInicioP;
                            vConvocatoria.FechaRespuestasFinP = vDataReaderResults["FechaFinRespuestasPreliminar"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinRespuestasPreliminar"].ToString()) : vConvocatoria.FechaRespuestasFinP;
                            vConvocatoria.FechaRespuestasInicioD = vDataReaderResults["FechaIniRespuestasDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIniRespuestasDefinitiva"].ToString()) : vConvocatoria.FechaRespuestasInicioD;
                            vConvocatoria.FechaRespuestasFinD = vDataReaderResults["FechaFinRespuestasDefinitiva"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinRespuestasDefinitiva"].ToString()) : vConvocatoria.FechaRespuestasFinD;
                            vConvocatoria.FechaInicioInscripcionOferente = vDataReaderResults["FechaInscripcionInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInscripcionInicial"].ToString()) : vConvocatoria.FechaInicioInscripcionOferente;
                            vConvocatoria.FechaFinInscripcionOferente = vDataReaderResults["FechaInscripcionFinal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInscripcionFinal"].ToString()) : vConvocatoria.FechaFinInscripcionOferente;
                            vConvocatoria.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? vDataReaderResults["NombreRegional"].ToString() : vConvocatoria.NombreRegional;

                            vListaConvocatorias.Add(vConvocatoria);
                        }
                        return vListaConvocatorias;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
