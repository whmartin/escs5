using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class SistemaOperativoDAL : GeneralDAL
    {
        public SistemaOperativoDAL()
        {
        }
        public int InsertarSistemaOperativo(SistemaOperativo pSistemaOperativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SistemaOperativo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSistemaOperativo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@SistemaOperativo", DbType.String, pSistemaOperativo.CodigoSistemaOperativo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pSistemaOperativo.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pSistemaOperativo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSistemaOperativo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSistemaOperativo.IdSistemaOperativo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSistemaOperativo").ToString());
                    GenerarLogAuditoria(pSistemaOperativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSistemaOperativo(SistemaOperativo pSistemaOperativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SistemaOperativo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSistemaOperativo", DbType.Int32, pSistemaOperativo.IdSistemaOperativo);
                    vDataBase.AddInParameter(vDbCommand, "@SistemaOperativo", DbType.String, pSistemaOperativo.CodigoSistemaOperativo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pSistemaOperativo.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pSistemaOperativo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSistemaOperativo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSistemaOperativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSistemaOperativo(SistemaOperativo pSistemaOperativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SistemaOperativo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSistemaOperativo", DbType.Int32, pSistemaOperativo.IdSistemaOperativo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSistemaOperativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SistemaOperativo ConsultarSistemaOperativo(int pIdSistemaOperativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SistemaOperativo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSistemaOperativo", DbType.Int32, pIdSistemaOperativo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SistemaOperativo vSistemaOperativo = new SistemaOperativo();
                        while (vDataReaderResults.Read())
                        {
                            vSistemaOperativo.IdSistemaOperativo = vDataReaderResults["IdSistemaOperativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSistemaOperativo"].ToString()) : vSistemaOperativo.IdSistemaOperativo;
                            vSistemaOperativo.CodigoSistemaOperativo = vDataReaderResults["SistemaOperativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SistemaOperativo"].ToString()) : vSistemaOperativo.CodigoSistemaOperativo;
                            vSistemaOperativo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSistemaOperativo.Descripcion;
                            vSistemaOperativo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vSistemaOperativo.Estado;
                            vSistemaOperativo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSistemaOperativo.UsuarioCrea;
                            vSistemaOperativo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSistemaOperativo.FechaCrea;
                            vSistemaOperativo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSistemaOperativo.UsuarioModifica;
                            vSistemaOperativo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSistemaOperativo.FechaModifica;
                        }
                        return vSistemaOperativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SistemaOperativo> ConsultarSistemaOperativos(String pSistemaOperativo, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SistemaOperativos_Consultar"))
                {
                    if (pSistemaOperativo != null)
                    //{
                    //    vDataBase.AddInParameter(vDbCommand, "@SistemaOperativo", DbType.String, "1");
                    //}
                    //else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@SistemaOperativo", DbType.String, pSistemaOperativo);
                    }
                        
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<SistemaOperativo> vListaSistemaOperativo = new List<SistemaOperativo>();
                        while (vDataReaderResults.Read())
                        {
                            SistemaOperativo vSistemaOperativo = new SistemaOperativo();
                            vSistemaOperativo.IdSistemaOperativo = vDataReaderResults["IdSistemaOperativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSistemaOperativo"].ToString()) : vSistemaOperativo.IdSistemaOperativo;
                            vSistemaOperativo.CodigoSistemaOperativo = vDataReaderResults["SistemaOperativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SistemaOperativo"].ToString()) : vSistemaOperativo.CodigoSistemaOperativo;
                            vSistemaOperativo.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vSistemaOperativo.Descripcion;
                            vSistemaOperativo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vSistemaOperativo.Estado;
                            vSistemaOperativo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSistemaOperativo.UsuarioCrea;
                            vSistemaOperativo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSistemaOperativo.FechaCrea;
                            vSistemaOperativo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSistemaOperativo.UsuarioModifica;
                            vSistemaOperativo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSistemaOperativo.FechaModifica;
                            vListaSistemaOperativo.Add(vSistemaOperativo);
                        }
                        return vListaSistemaOperativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSistemaOperativo(String pSistemaoperativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_SistemaOperativo_Consultar_SistemaOperativo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@SistemaOperativo", DbType.String, pSistemaoperativo);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vSistemaOperativo = 0;
                        while (vDataReaderResults.Read())
                        {
                            vSistemaOperativo = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vSistemaOperativo;
                        }
                        return vSistemaOperativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
