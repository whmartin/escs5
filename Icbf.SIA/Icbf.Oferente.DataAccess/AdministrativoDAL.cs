using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase AdministrativoDAL
    /// </summary>
    public class AdministrativoDAL : GeneralDAL
    {
        public AdministrativoDAL()
        {
        }
        /// <summary>
        /// Insertar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int InsertarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Administrativo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Decimal, pAdministrativo.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pAdministrativo.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Organigrama", DbType.Boolean, pAdministrativo.Organigrama);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPnalAnnoPrevio", DbType.Decimal, pAdministrativo.TotalPnalAnnoPrevio);
                    vDataBase.AddInParameter(vDbCommand, "@VincLaboral", DbType.Decimal, pAdministrativo.VincLaboral);
                    vDataBase.AddInParameter(vDbCommand, "@PrestServicios", DbType.Decimal, pAdministrativo.PrestServicios);
                    vDataBase.AddInParameter(vDbCommand, "@Voluntariado", DbType.Decimal, pAdministrativo.Voluntariado);
                    vDataBase.AddInParameter(vDbCommand, "@VoluntPermanente", DbType.Decimal, pAdministrativo.VoluntPermanente);
                    vDataBase.AddInParameter(vDbCommand, "@Asociados", DbType.Decimal, pAdministrativo.Asociados);
                    vDataBase.AddInParameter(vDbCommand, "@Mision", DbType.Decimal, pAdministrativo.Mision);
                    vDataBase.AddInParameter(vDbCommand, "@PQRS", DbType.Boolean, pAdministrativo.PQRS);
                    vDataBase.AddInParameter(vDbCommand, "@GestionDocumental", DbType.Boolean, pAdministrativo.GestionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@AuditoriaInterna", DbType.Boolean, pAdministrativo.AuditoriaInterna);
                    vDataBase.AddInParameter(vDbCommand, "@ManProcedimiento", DbType.Boolean, pAdministrativo.ManProcedimiento);
                    vDataBase.AddInParameter(vDbCommand, "@ManPracticasAmbiente", DbType.Boolean, pAdministrativo.ManPracticasAmbiente);
                    vDataBase.AddInParameter(vDbCommand, "@ManComportOrg", DbType.Boolean, pAdministrativo.ManComportOrg);
                    vDataBase.AddInParameter(vDbCommand, "@ManFunciones", DbType.Boolean, pAdministrativo.ManFunciones);
                    vDataBase.AddInParameter(vDbCommand, "@ProcRegInfoContable", DbType.Boolean, pAdministrativo.ProcRegInfoContable);
                    vDataBase.AddInParameter(vDbCommand, "@PartMesasTerritoriales", DbType.Boolean, pAdministrativo.PartMesasTerritoriales);
                    vDataBase.AddInParameter(vDbCommand, "@PartAsocAgremia", DbType.Boolean, pAdministrativo.PartAsocAgremia);
                    vDataBase.AddInParameter(vDbCommand, "@PartConsejosComun", DbType.Boolean, pAdministrativo.PartConsejosComun);
                    vDataBase.AddInParameter(vDbCommand, "@ConvInterInst", DbType.Boolean, pAdministrativo.ConvInterInst);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccGral", DbType.Boolean, pAdministrativo.ProcSeleccGral);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccEtnico", DbType.Boolean, pAdministrativo.ProcSeleccEtnico);
                    vDataBase.AddInParameter(vDbCommand, "@PlanInduccCapac", DbType.Boolean, pAdministrativo.PlanInduccCapac);
                    vDataBase.AddInParameter(vDbCommand, "@EvalDesemp", DbType.Boolean, pAdministrativo.EvalDesemp);
                    vDataBase.AddInParameter(vDbCommand, "@PlanCualificacion", DbType.Boolean, pAdministrativo.PlanCualificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAdministrativo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAdministrativo.IdAdministrativo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAdministrativo").ToString());
                    GenerarLogAuditoria(pAdministrativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Administrativo
        /// </summary>
        /// <param name="pAdministrativo"></param>
        /// <returns></returns>
        public int ModificarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Administrativo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pAdministrativo.IdAdministrativo);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Decimal, pAdministrativo.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pAdministrativo.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Organigrama", DbType.Boolean, pAdministrativo.Organigrama);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPnalAnnoPrevio", DbType.Decimal, pAdministrativo.TotalPnalAnnoPrevio);
                    vDataBase.AddInParameter(vDbCommand, "@VincLaboral", DbType.Decimal, pAdministrativo.VincLaboral);
                    vDataBase.AddInParameter(vDbCommand, "@PrestServicios", DbType.Decimal, pAdministrativo.PrestServicios);
                    vDataBase.AddInParameter(vDbCommand, "@Voluntariado", DbType.Decimal, pAdministrativo.Voluntariado);
                    vDataBase.AddInParameter(vDbCommand, "@VoluntPermanente", DbType.Decimal, pAdministrativo.VoluntPermanente);
                    vDataBase.AddInParameter(vDbCommand, "@Asociados", DbType.Decimal, pAdministrativo.Asociados);
                    vDataBase.AddInParameter(vDbCommand, "@Mision", DbType.Decimal, pAdministrativo.Mision);
                    vDataBase.AddInParameter(vDbCommand, "@PQRS", DbType.Boolean, pAdministrativo.PQRS);
                    vDataBase.AddInParameter(vDbCommand, "@GestionDocumental", DbType.Boolean, pAdministrativo.GestionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@AuditoriaInterna", DbType.Boolean, pAdministrativo.AuditoriaInterna);
                    vDataBase.AddInParameter(vDbCommand, "@ManProcedimiento", DbType.Boolean, pAdministrativo.ManProcedimiento);
                    vDataBase.AddInParameter(vDbCommand, "@ManPracticasAmbiente", DbType.Boolean, pAdministrativo.ManPracticasAmbiente);
                    vDataBase.AddInParameter(vDbCommand, "@ManComportOrg", DbType.Boolean, pAdministrativo.ManComportOrg);
                    vDataBase.AddInParameter(vDbCommand, "@ManFunciones", DbType.Boolean, pAdministrativo.ManFunciones);
                    vDataBase.AddInParameter(vDbCommand, "@ProcRegInfoContable", DbType.Boolean, pAdministrativo.ProcRegInfoContable);
                    vDataBase.AddInParameter(vDbCommand, "@PartMesasTerritoriales", DbType.Boolean, pAdministrativo.PartMesasTerritoriales);
                    vDataBase.AddInParameter(vDbCommand, "@PartAsocAgremia", DbType.Boolean, pAdministrativo.PartAsocAgremia);
                    vDataBase.AddInParameter(vDbCommand, "@PartConsejosComun", DbType.Boolean, pAdministrativo.PartConsejosComun);
                    vDataBase.AddInParameter(vDbCommand, "@ConvInterInst", DbType.Boolean, pAdministrativo.ConvInterInst);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccGral", DbType.Boolean, pAdministrativo.ProcSeleccGral);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccEtnico", DbType.Boolean, pAdministrativo.ProcSeleccEtnico);
                    vDataBase.AddInParameter(vDbCommand, "@PlanInduccCapac", DbType.Boolean, pAdministrativo.PlanInduccCapac);
                    vDataBase.AddInParameter(vDbCommand, "@EvalDesemp", DbType.Boolean, pAdministrativo.EvalDesemp);
                    vDataBase.AddInParameter(vDbCommand, "@PlanCualificacion", DbType.Boolean, pAdministrativo.PlanCualificacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAdministrativo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdministrativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Eliminar Administrativo
        public int EliminarAdministrativo(Administrativo pAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Administrativo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pAdministrativo.IdAdministrativo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdministrativo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Administrativo
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <returns></returns>
        public Administrativo ConsultarAdministrativo(int pIdAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Administrativo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pIdAdministrativo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Administrativo vAdministrativo = new Administrativo();
                        while (vDataReaderResults.Read())
                        {
                            vAdministrativo.IdAdministrativo = vDataReaderResults["IdAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativo"].ToString()) : vAdministrativo.IdAdministrativo;
                            vAdministrativo.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdEntidad"].ToString()) : vAdministrativo.IdEntidad;
                            vAdministrativo.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vAdministrativo.IdVigencia;
                            vAdministrativo.Organigrama = vDataReaderResults["Organigrama"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Organigrama"].ToString()) : vAdministrativo.Organigrama;
                            vAdministrativo.TotalPnalAnnoPrevio = vDataReaderResults["TotalPnalAnnoPrevio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalPnalAnnoPrevio"].ToString()) : vAdministrativo.TotalPnalAnnoPrevio;
                            vAdministrativo.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VincLaboral"].ToString()) : vAdministrativo.VincLaboral;
                            vAdministrativo.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PrestServicios"].ToString()) : vAdministrativo.PrestServicios;
                            vAdministrativo.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Voluntariado"].ToString()) : vAdministrativo.Voluntariado;
                            vAdministrativo.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VoluntPermanente"].ToString()) : vAdministrativo.VoluntPermanente;
                            vAdministrativo.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Asociados"].ToString()) : vAdministrativo.Asociados;
                            vAdministrativo.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Mision"].ToString()) : vAdministrativo.Mision;
                            vAdministrativo.PQRS = vDataReaderResults["PQRS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PQRS"].ToString()) : vAdministrativo.PQRS;
                            vAdministrativo.GestionDocumental = vDataReaderResults["GestionDocumental"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GestionDocumental"].ToString()) : vAdministrativo.GestionDocumental;
                            vAdministrativo.AuditoriaInterna = vDataReaderResults["AuditoriaInterna"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AuditoriaInterna"].ToString()) : vAdministrativo.AuditoriaInterna;
                            vAdministrativo.ManProcedimiento = vDataReaderResults["ManProcedimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManProcedimiento"].ToString()) : vAdministrativo.ManProcedimiento;
                            vAdministrativo.ManPracticasAmbiente = vDataReaderResults["ManPracticasAmbiente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManPracticasAmbiente"].ToString()) : vAdministrativo.ManPracticasAmbiente;
                            vAdministrativo.ManComportOrg = vDataReaderResults["ManComportOrg"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManComportOrg"].ToString()) : vAdministrativo.ManComportOrg;
                            vAdministrativo.ManFunciones = vDataReaderResults["ManFunciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManFunciones"].ToString()) : vAdministrativo.ManFunciones;
                            vAdministrativo.ProcRegInfoContable = vDataReaderResults["ProcRegInfoContable"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcRegInfoContable"].ToString()) : vAdministrativo.ProcRegInfoContable;
                            vAdministrativo.PartMesasTerritoriales = vDataReaderResults["PartMesasTerritoriales"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartMesasTerritoriales"].ToString()) : vAdministrativo.PartMesasTerritoriales;
                            vAdministrativo.PartAsocAgremia = vDataReaderResults["PartAsocAgremia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartAsocAgremia"].ToString()) : vAdministrativo.PartAsocAgremia;
                            vAdministrativo.PartConsejosComun = vDataReaderResults["PartConsejosComun"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartConsejosComun"].ToString()) : vAdministrativo.PartConsejosComun;
                            vAdministrativo.ConvInterInst = vDataReaderResults["ConvInterInst"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvInterInst"].ToString()) : vAdministrativo.ConvInterInst;
                            vAdministrativo.ProcSeleccGral = vDataReaderResults["ProcSeleccGral"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccGral"].ToString()) : vAdministrativo.ProcSeleccGral;
                            vAdministrativo.ProcSeleccEtnico = vDataReaderResults["ProcSeleccEtnico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccEtnico"].ToString()) : vAdministrativo.ProcSeleccEtnico;
                            vAdministrativo.PlanInduccCapac = vDataReaderResults["PlanInduccCapac"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanInduccCapac"].ToString()) : vAdministrativo.PlanInduccCapac;
                            vAdministrativo.EvalDesemp = vDataReaderResults["EvalDesemp"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EvalDesemp"].ToString()) : vAdministrativo.EvalDesemp;
                            vAdministrativo.PlanCualificacion = vDataReaderResults["PlanCualificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanCualificacion"].ToString()) : vAdministrativo.PlanCualificacion;
                            vAdministrativo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdministrativo.UsuarioCrea;
                            vAdministrativo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdministrativo.FechaCrea;
                            vAdministrativo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdministrativo.UsuarioModifica;
                            vAdministrativo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdministrativo.FechaModifica;
                        }
                        return vAdministrativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una Lista de Administrativos
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdVigencia"></param>
        /// <returns></returns>
        public List<Administrativo> ConsultarAdministrativos(Decimal pIdEntidad, int? pIdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Administrativos_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Decimal, pIdEntidad);
                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Administrativo> vListaAdministrativo = new List<Administrativo>();
                        while (vDataReaderResults.Read())
                        {
                            Administrativo vAdministrativo = new Administrativo();
                            vAdministrativo.IdAdministrativo = vDataReaderResults["IdAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativo"].ToString()) : vAdministrativo.IdAdministrativo;
                            vAdministrativo.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdEntidad"].ToString()) : vAdministrativo.IdEntidad;
                            vAdministrativo.IdVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AcnoVigencia"].ToString()) : vAdministrativo.IdVigencia;
                            vAdministrativo.Organigrama = vDataReaderResults["Organigrama"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Organigrama"].ToString()) : vAdministrativo.Organigrama;
                            vAdministrativo.TotalPnalAnnoPrevio = vDataReaderResults["TotalPnalAnnoPrevio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalPnalAnnoPrevio"].ToString()) : vAdministrativo.TotalPnalAnnoPrevio;
                            vAdministrativo.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VincLaboral"].ToString()) : vAdministrativo.VincLaboral;
                            vAdministrativo.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PrestServicios"].ToString()) : vAdministrativo.PrestServicios;
                            vAdministrativo.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Voluntariado"].ToString()) : vAdministrativo.Voluntariado;
                            vAdministrativo.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["VoluntPermanente"].ToString()) : vAdministrativo.VoluntPermanente;
                            vAdministrativo.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Asociados"].ToString()) : vAdministrativo.Asociados;
                            vAdministrativo.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Mision"].ToString()) : vAdministrativo.Mision;
                            vAdministrativo.PQRS = vDataReaderResults["PQRS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PQRS"].ToString()) : vAdministrativo.PQRS;
                            vAdministrativo.GestionDocumental = vDataReaderResults["GestionDocumental"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GestionDocumental"].ToString()) : vAdministrativo.GestionDocumental;
                            vAdministrativo.AuditoriaInterna = vDataReaderResults["AuditoriaInterna"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AuditoriaInterna"].ToString()) : vAdministrativo.AuditoriaInterna;
                            vAdministrativo.ManProcedimiento = vDataReaderResults["ManProcedimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManProcedimiento"].ToString()) : vAdministrativo.ManProcedimiento;
                            vAdministrativo.ManPracticasAmbiente = vDataReaderResults["ManPracticasAmbiente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManPracticasAmbiente"].ToString()) : vAdministrativo.ManPracticasAmbiente;
                            vAdministrativo.ManComportOrg = vDataReaderResults["ManComportOrg"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManComportOrg"].ToString()) : vAdministrativo.ManComportOrg;
                            vAdministrativo.ManFunciones = vDataReaderResults["ManFunciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManFunciones"].ToString()) : vAdministrativo.ManFunciones;
                            vAdministrativo.ProcRegInfoContable = vDataReaderResults["ProcRegInfoContable"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcRegInfoContable"].ToString()) : vAdministrativo.ProcRegInfoContable;
                            vAdministrativo.PartMesasTerritoriales = vDataReaderResults["PartMesasTerritoriales"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartMesasTerritoriales"].ToString()) : vAdministrativo.PartMesasTerritoriales;
                            vAdministrativo.PartAsocAgremia = vDataReaderResults["PartAsocAgremia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartAsocAgremia"].ToString()) : vAdministrativo.PartAsocAgremia;
                            vAdministrativo.PartConsejosComun = vDataReaderResults["PartConsejosComun"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartConsejosComun"].ToString()) : vAdministrativo.PartConsejosComun;
                            vAdministrativo.ConvInterInst = vDataReaderResults["ConvInterInst"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvInterInst"].ToString()) : vAdministrativo.ConvInterInst;
                            vAdministrativo.ProcSeleccGral = vDataReaderResults["ProcSeleccGral"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccGral"].ToString()) : vAdministrativo.ProcSeleccGral;
                            vAdministrativo.ProcSeleccEtnico = vDataReaderResults["ProcSeleccEtnico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccEtnico"].ToString()) : vAdministrativo.ProcSeleccEtnico;
                            vAdministrativo.PlanInduccCapac = vDataReaderResults["PlanInduccCapac"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanInduccCapac"].ToString()) : vAdministrativo.PlanInduccCapac;
                            vAdministrativo.EvalDesemp = vDataReaderResults["EvalDesemp"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EvalDesemp"].ToString()) : vAdministrativo.EvalDesemp;
                            vAdministrativo.PlanCualificacion = vDataReaderResults["PlanCualificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanCualificacion"].ToString()) : vAdministrativo.PlanCualificacion;
                            vAdministrativo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdministrativo.UsuarioCrea;
                            vAdministrativo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdministrativo.FechaCrea;
                            vAdministrativo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdministrativo.UsuarioModifica;
                            vAdministrativo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdministrativo.FechaModifica;
                            vListaAdministrativo.Add(vAdministrativo);
                        }
                        return vListaAdministrativo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
