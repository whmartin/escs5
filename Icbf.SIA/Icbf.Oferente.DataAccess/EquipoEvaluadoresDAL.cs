﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace Icbf.Oferente.DataAccess
{
    public class EquipoEvaluadoresDAL : GeneralDAL
    {
        public EquipoEvaluadoresDAL()
        {
        }
        public int InsertarEquipoEvaluadores(EquipoEvaluadores pEquipoEvaluadores)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EquipoEvaluadores_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEquipoEvaluador", DbType.String, pEquipoEvaluadores.NombreEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pEquipoEvaluadores.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCoordinador", DbType.Int32, pEquipoEvaluadores.IdUsuarioCoordinador);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEquipoEvaluadores.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEquipoEvaluadores.IdEquipoEvaluador = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEquipoEvaluador").ToString());
                    GenerarLogAuditoria(pEquipoEvaluadores, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarEquiposEvaluadoress(int pIdUsuarioCoordinador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EquiposEvaluadoress_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCoordinador", DbType.Int32, pIdUsuarioCoordinador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EquipoEvaluadores> vListaEquipoEvaluadores = new List<EquipoEvaluadores>();
                        while (vDataReaderResults.Read())
                        {
                            EquipoEvaluadores vEquipoEvaluadores = new EquipoEvaluadores();

                            vEquipoEvaluadores.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEquipoEvaluadores.IdEquipoEvaluador;
                            vEquipoEvaluadores.NombreEquipoEvaluador = vDataReaderResults["NombreEquipoEvaluador"] != DBNull.Value ? vDataReaderResults["NombreEquipoEvaluador"].ToString() : vEquipoEvaluadores.NombreEquipoEvaluador;
                            vEquipoEvaluadores.IdUsuarioCoordinador = vDataReaderResults["IdUsuarioCoordinador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioCoordinador"].ToString()) : vEquipoEvaluadores.IdUsuarioCoordinador;
                            vEquipoEvaluadores.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEquipoEvaluadores.Activo;
                            vListaEquipoEvaluadores.Add(vEquipoEvaluadores);
                        }
                        return vListaEquipoEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarEquiposEvaluadoresss(string pNumeroDocumento, string pNombreCoordinador, string pNombreEquipoEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EquiposEvaluadoresss_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NumeroDocumento", DbType.String, pNumeroDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NombreCoordinador", DbType.String, pNombreCoordinador);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEquipoEvaluador", DbType.String, pNombreEquipoEvaluador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EquipoEvaluadores> vListaEquipoEvaluadores = new List<EquipoEvaluadores>();
                        while (vDataReaderResults.Read())
                        {
                            EquipoEvaluadores vEquipoEvaluadores = new EquipoEvaluadores();

                            vEquipoEvaluadores.IdUsuarioCoordinador = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vEquipoEvaluadores.IdUsuarioCoordinador;
                            vEquipoEvaluadores.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vEquipoEvaluadores.NumeroDocumento;
                            vEquipoEvaluadores.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? vDataReaderResults["NombreCompleto"].ToString() : vEquipoEvaluadores.NombreCompleto;
                            vEquipoEvaluadores.NombreEquipoEvaluador = vDataReaderResults["NombreEquipoEvaluador"] != DBNull.Value ? vDataReaderResults["NombreEquipoEvaluador"].ToString() : vEquipoEvaluadores.NombreEquipoEvaluador;
                            vEquipoEvaluadores.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEquipoEvaluadores.IdEquipoEvaluador;
                            vEquipoEvaluadores.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEquipoEvaluadores.Activo;
                            vListaEquipoEvaluadores.Add(vEquipoEvaluadores);
                        }
                        return vListaEquipoEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        public EquipoEvaluadores ConsultarEquipoEvaluador(int pIdEquipoEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EquipoEvaluador_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pIdEquipoEvaluador);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EquipoEvaluadores vEquipoEvaluadores = new EquipoEvaluadores();
                        while (vDataReaderResults.Read())
                        {
                            vEquipoEvaluadores.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEquipoEvaluadores.IdEquipoEvaluador;
                            vEquipoEvaluadores.NombreEquipoEvaluador = vDataReaderResults["NombreEquipoEvaluador"] != DBNull.Value ? vDataReaderResults["NombreEquipoEvaluador"].ToString() : vEquipoEvaluadores.NombreEquipoEvaluador;
                            vEquipoEvaluadores.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEquipoEvaluadores.Activo;
                            vEquipoEvaluadores.IdUsuarioCoordinador = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vEquipoEvaluadores.IdUsuarioCoordinador;
                            vEquipoEvaluadores.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vEquipoEvaluadores.NumeroDocumento;
                            vEquipoEvaluadores.NombreCompleto = vDataReaderResults["NombreCompleto"] != DBNull.Value ? vDataReaderResults["NombreCompleto"].ToString() : vEquipoEvaluadores.NombreCompleto;
                        }
                        return vEquipoEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarEquipoEvaluador(EquipoEvaluadores pEquipoEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EquipoEvaluadores_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pEquipoEvaluador.IdEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioCoordinador", DbType.Int32, pEquipoEvaluador.IdUsuarioCoordinador);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEquipoEvaluador", DbType.String, pEquipoEvaluador.NombreEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEquipoEvaluador.UsuarioModifica);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEquipoEvaluador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EquipoEvaluadores> ConsultarTodosEquiposEvaluadoress()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EquiposEvaluadoressTodos_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EquipoEvaluadores> vListaEquipoEvaluadores = new List<EquipoEvaluadores>();
                        while (vDataReaderResults.Read())
                        {
                            EquipoEvaluadores vEquipoEvaluadores = new EquipoEvaluadores();

                            vEquipoEvaluadores.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEquipoEvaluadores.IdEquipoEvaluador;
                            vEquipoEvaluadores.IdUsuarioCoordinador = vDataReaderResults["IdUsuarioCoordinador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioCoordinador"].ToString()) : vEquipoEvaluadores.IdUsuarioCoordinador;
                            vEquipoEvaluadores.NombreEquipoEvaluador = vDataReaderResults["NombreEquipoEvaluador"] != DBNull.Value ? vDataReaderResults["NombreEquipoEvaluador"].ToString() : vEquipoEvaluadores.NombreEquipoEvaluador;
                            vEquipoEvaluadores.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vEquipoEvaluadores.NumeroDocumento;
                            vEquipoEvaluadores.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEquipoEvaluadores.Activo;

                            vListaEquipoEvaluadores.Add(vEquipoEvaluadores);
                        }
                        return vListaEquipoEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
