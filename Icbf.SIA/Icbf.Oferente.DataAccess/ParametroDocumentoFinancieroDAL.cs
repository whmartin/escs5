using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase ParametroDocumentoFinancieroDAL
    /// </summary>
    public class ParametroDocumentoFinancieroDAL : GeneralDAL
    {
        public ParametroDocumentoFinancieroDAL()
        {
        }
        /// <summary>
        /// Insertar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int InsertarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinanciero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDParametroDocFinanciero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pParametroDocumentoFinanciero.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pParametroDocumentoFinanciero.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@Obligatorio", DbType.Boolean, pParametroDocumentoFinanciero.Obligatorio);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pParametroDocumentoFinanciero.UsuarioCrea);

                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pParametroDocumentoFinanciero.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentoRup", DbType.Boolean, pParametroDocumentoFinanciero.DocumentoRup);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pParametroDocumentoFinanciero.IDParametroDocFinanciero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDParametroDocFinanciero").ToString());
                    GenerarLogAuditoria(pParametroDocumentoFinanciero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int ModificarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinanciero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDParametroDocFinanciero", DbType.Int32, pParametroDocumentoFinanciero.IDParametroDocFinanciero);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pParametroDocumentoFinanciero.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pParametroDocumentoFinanciero.IdFormatoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@Obligatorio", DbType.Boolean, pParametroDocumentoFinanciero.Obligatorio);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pParametroDocumentoFinanciero.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pParametroDocumentoFinanciero.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentoRup", DbType.Boolean, pParametroDocumentoFinanciero.DocumentoRup);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pParametroDocumentoFinanciero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Parametro Documento Financiero
        /// </summary>
        /// <param name="pParametroDocumentoFinanciero"></param>
        /// <returns></returns>
        public int EliminarParametroDocumentoFinanciero(ParametroDocumentoFinanciero pParametroDocumentoFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinanciero_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDParametroDocFinanciero", DbType.Int32, pParametroDocumentoFinanciero.IDParametroDocFinanciero);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pParametroDocumentoFinanciero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financiero
        /// </summary>
        /// <param name="pIDParametroDocFinanciero"></param>
        /// <returns></returns>
        public ParametroDocumentoFinanciero ConsultarParametroDocumentoFinanciero(int pIDParametroDocFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinanciero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDParametroDocFinanciero", DbType.Int32, pIDParametroDocFinanciero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ParametroDocumentoFinanciero vParametroDocumentoFinanciero = new ParametroDocumentoFinanciero();
                        while (vDataReaderResults.Read())
                        {
                            vParametroDocumentoFinanciero.IDParametroDocFinanciero = vDataReaderResults["IDParametroDocFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDParametroDocFinanciero"].ToString()) : vParametroDocumentoFinanciero.IDParametroDocFinanciero;
                            vParametroDocumentoFinanciero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametroDocumentoFinanciero.Descripcion;
                            vParametroDocumentoFinanciero.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vParametroDocumentoFinanciero.IdFormatoArchivo;
                            vParametroDocumentoFinanciero.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Obligatorio"].ToString()) : vParametroDocumentoFinanciero.Obligatorio;
                            vParametroDocumentoFinanciero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vParametroDocumentoFinanciero.UsuarioCrea;
                            vParametroDocumentoFinanciero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vParametroDocumentoFinanciero.FechaCrea;
                            vParametroDocumentoFinanciero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vParametroDocumentoFinanciero.UsuarioModifica;
                            vParametroDocumentoFinanciero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vParametroDocumentoFinanciero.FechaModifica;

                            vParametroDocumentoFinanciero.DocumentoRup = vDataReaderResults["DocumentoRup"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["DocumentoRup"].ToString()) : vParametroDocumentoFinanciero.Obligatorio;
                            vParametroDocumentoFinanciero.Estado = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vParametroDocumentoFinanciero.Obligatorio;

                        }
                        return vParametroDocumentoFinanciero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Parametr oDocumento Financieros
        /// </summary>
        /// <param name="pDescripcion"></param>
        /// <param name="pIdFormatoArchivo"></param>
        /// <param name="pObligatorio"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros(String pDescripcion, int? pIdFormatoArchivo, Boolean? pObligatorio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinancieros_Consultar"))
                {
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pIdFormatoArchivo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdFormatoArchivo", DbType.Int32, pIdFormatoArchivo);
                    if(pObligatorio != null)
                         vDataBase.AddInParameter(vDbCommand, "@Obligatorio", DbType.Boolean, pObligatorio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ParametroDocumentoFinanciero> vListaParametroDocumentoFinanciero = new List<ParametroDocumentoFinanciero>();
                        while (vDataReaderResults.Read())
                        {
                                ParametroDocumentoFinanciero vParametroDocumentoFinanciero = new ParametroDocumentoFinanciero();
                            vParametroDocumentoFinanciero.IDParametroDocFinanciero = vDataReaderResults["IDParametroDocFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDParametroDocFinanciero"].ToString()) : vParametroDocumentoFinanciero.IDParametroDocFinanciero;
                            vParametroDocumentoFinanciero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametroDocumentoFinanciero.Descripcion;
                            vParametroDocumentoFinanciero.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vParametroDocumentoFinanciero.IdFormatoArchivo;
                            vParametroDocumentoFinanciero.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Obligatorio"].ToString()) : vParametroDocumentoFinanciero.Obligatorio;
                            vParametroDocumentoFinanciero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vParametroDocumentoFinanciero.UsuarioCrea;
                            vParametroDocumentoFinanciero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vParametroDocumentoFinanciero.FechaCrea;
                            vParametroDocumentoFinanciero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vParametroDocumentoFinanciero.UsuarioModifica;
                            vParametroDocumentoFinanciero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vParametroDocumentoFinanciero.FechaModifica;
                                vListaParametroDocumentoFinanciero.Add(vParametroDocumentoFinanciero);
                        }
                        return vListaParametroDocumentoFinanciero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financieros_Documento
        /// </summary>
        /// <param name="pObligatorio"></param>
        /// <param name="rup"></param>
        /// <param name="Activo"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros_Documento(Boolean pObligatorio,Boolean rup,Boolean Activo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinancieros_Documento_Consultar"))
                {
                 
                    vDataBase.AddInParameter(vDbCommand, "@Obligatorio", DbType.Boolean, pObligatorio);
                    vDataBase.AddInParameter(vDbCommand, "@DocumentoRup", DbType.Boolean, rup);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, Activo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ParametroDocumentoFinanciero> vListaParametroDocumentoFinanciero = new List<ParametroDocumentoFinanciero>();
                        while (vDataReaderResults.Read())
                        {
                            ParametroDocumentoFinanciero vParametroDocumentoFinanciero = new ParametroDocumentoFinanciero();
                            vParametroDocumentoFinanciero.IDParametroDocFinanciero = vDataReaderResults["IDParametroDocFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDParametroDocFinanciero"].ToString()) : vParametroDocumentoFinanciero.IDParametroDocFinanciero;
                            vParametroDocumentoFinanciero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametroDocumentoFinanciero.Descripcion;
                            vParametroDocumentoFinanciero.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vParametroDocumentoFinanciero.IdFormatoArchivo;
                            vParametroDocumentoFinanciero.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Obligatorio"].ToString()) : vParametroDocumentoFinanciero.Obligatorio;
                            vParametroDocumentoFinanciero.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vParametroDocumentoFinanciero.Extension;
                            //
                            //vParametroDocumentoFinanciero.IdFinanciero = vDataReaderResults["IdFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinanciero"].ToString()) : vParametroDocumentoFinanciero.IdFinanciero;
                            //vParametroDocumentoFinanciero.IdFinancieroDocumento = vDataReaderResults["IdFinancieroDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinancieroDocumento"].ToString()) : vParametroDocumentoFinanciero.IdFinancieroDocumento;
                            //vParametroDocumentoFinanciero.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vParametroDocumentoFinanciero.NombreArchivo;
                            //vParametroDocumentoFinanciero.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vParametroDocumentoFinanciero.NombreArchivoOri;
                            //vParametroDocumentoFinanciero.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArchivo"].ToString()) : vParametroDocumentoFinanciero.IdArchivo;
                            vListaParametroDocumentoFinanciero.Add(vParametroDocumentoFinanciero);
                        }
                        return vListaParametroDocumentoFinanciero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Parametro Documento Financieros_Documento_ID
        /// </summary>
        /// <param name="pObligatorio"></param>
        /// <param name="vIDFinanciero"></param>
        /// <param name="rup"></param>
        /// <returns></returns>
        public List<ParametroDocumentoFinanciero> ConsultarParametroDocumentoFinancieros_Documento_ID(Boolean pObligatorio, int vIDFinanciero, Boolean? rup)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ParametroDocumentoFinancieros_Documento_Consultar_ID"))
                {
                 
                    vDataBase.AddInParameter(vDbCommand, "@Obligatorio", DbType.Boolean, pObligatorio);
                    vDataBase.AddInParameter(vDbCommand, "@IDFinanciero", DbType.Int32, vIDFinanciero);
                    if (rup != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@DocumentoRup", DbType.Boolean, rup);
                        
                    }
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ParametroDocumentoFinanciero> vListaParametroDocumentoFinanciero = new List<ParametroDocumentoFinanciero>();
                        while (vDataReaderResults.Read())
                        {
                            ParametroDocumentoFinanciero vParametroDocumentoFinanciero = new ParametroDocumentoFinanciero();
                            vParametroDocumentoFinanciero.IDParametroDocFinanciero = vDataReaderResults["IDParametroDocFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDParametroDocFinanciero"].ToString()) : vParametroDocumentoFinanciero.IDParametroDocFinanciero;
                            vParametroDocumentoFinanciero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vParametroDocumentoFinanciero.Descripcion;
                            vParametroDocumentoFinanciero.IdFormatoArchivo = vDataReaderResults["IdFormatoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormatoArchivo"].ToString()) : vParametroDocumentoFinanciero.IdFormatoArchivo;
                            vParametroDocumentoFinanciero.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Obligatorio"].ToString()) : vParametroDocumentoFinanciero.Obligatorio;
                            vParametroDocumentoFinanciero.IdFinanciero = vDataReaderResults["IdFinanciero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinanciero"].ToString()) : vParametroDocumentoFinanciero.IdFinanciero;
                            vParametroDocumentoFinanciero.IdFinancieroDocumento = vDataReaderResults["IdFinancieroDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFinancieroDocumento"].ToString()) : vParametroDocumentoFinanciero.IdFinancieroDocumento;
                            vParametroDocumentoFinanciero.NombreArchivo = vDataReaderResults["NombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivo"].ToString()) : vParametroDocumentoFinanciero.NombreArchivo;
                            vParametroDocumentoFinanciero.NombreArchivoOri = vDataReaderResults["NombreArchivoOri"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreArchivoOri"].ToString()) : vParametroDocumentoFinanciero.NombreArchivoOri;
                            vParametroDocumentoFinanciero.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdArchivo"].ToString()) : vParametroDocumentoFinanciero.IdArchivo;
                            vParametroDocumentoFinanciero.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vParametroDocumentoFinanciero.Extension;
                            vListaParametroDocumentoFinanciero.Add(vParametroDocumentoFinanciero);
                        }
                        return vListaParametroDocumentoFinanciero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
