using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de Calse AdministrativoDocumentoDAL
    /// </summary>
    public class AdministrativoDocumentoDAL : GeneralDAL
    {
        public AdministrativoDocumentoDAL()
        {
        }
        /// <summary>
        /// Insertar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int InsertarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_AdministrativoDocumento_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAdministrativoDocumento", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pAdministrativoDocumento.IdAdministrativo);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pAdministrativoDocumento.IdTipoDocAdministrativo);
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pAdministrativoDocumento.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAdministrativoDocumento.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAdministrativoDocumento.IdAdministrativoDocumento = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAdministrativoDocumento").ToString());
                    GenerarLogAuditoria(pAdministrativoDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int ModificarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_AdministrativoDocumento_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativoDocumento", DbType.Int32, pAdministrativoDocumento.IdAdministrativoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pAdministrativoDocumento.IdAdministrativo);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pAdministrativoDocumento.IdTipoDocAdministrativo);
                    vDataBase.AddInParameter(vDbCommand, "@IdArchivo", DbType.Decimal, pAdministrativoDocumento.IdArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pAdministrativoDocumento.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdministrativoDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Eliminar Administrativo Documento
        /// </summary>
        /// <param name="pAdministrativoDocumento"></param>
        /// <returns></returns>
        public int EliminarAdministrativoDocumento(AdministrativoDocumento pAdministrativoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_AdministrativoDocumento_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativoDocumento", DbType.Int32, pAdministrativoDocumento.IdAdministrativoDocumento);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pAdministrativoDocumento, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Administrativo Documento
        /// </summary>
        /// <param name="pIdAdministrativoDocumento"></param>
        /// <returns></returns>
        public AdministrativoDocumento ConsultarAdministrativoDocumento(int pIdAdministrativoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_AdministrativoDocumento_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAdministrativoDocumento", DbType.Int32, pIdAdministrativoDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        AdministrativoDocumento vAdministrativoDocumento = new AdministrativoDocumento();
                        while (vDataReaderResults.Read())
                        {
                            vAdministrativoDocumento.IdAdministrativoDocumento = vDataReaderResults["IdAdministrativoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativoDocumento"].ToString()) : vAdministrativoDocumento.IdAdministrativoDocumento;
                            vAdministrativoDocumento.IdAdministrativo = vDataReaderResults["IdAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativo"].ToString()) : vAdministrativoDocumento.IdAdministrativo;
                            vAdministrativoDocumento.IdTipoDocAdministrativo = vDataReaderResults["IdTipoDocAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocAdministrativo"].ToString()) : vAdministrativoDocumento.IdTipoDocAdministrativo;
                            vAdministrativoDocumento.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vAdministrativoDocumento.IdArchivo;
                            vAdministrativoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdministrativoDocumento.UsuarioCrea;
                            vAdministrativoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdministrativoDocumento.FechaCrea;
                            vAdministrativoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdministrativoDocumento.UsuarioModifica;
                            vAdministrativoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdministrativoDocumento.FechaModifica;
                        }
                        return vAdministrativoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Lista de  Administrativo Documentos
        /// </summary>
        /// <param name="pIdAdministrativo"></param>
        /// <param name="pIdTipoDocAdministrativo"></param>
        /// <returns></returns>
        public List<AdministrativoDocumento> ConsultarAdministrativoDocumentos(int? pIdAdministrativo, int? pIdTipoDocAdministrativo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_AdministrativoDocumentos_Consultar"))
                {
                    if (pIdAdministrativo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdAdministrativo", DbType.Int32, pIdAdministrativo);
                    if (pIdTipoDocAdministrativo != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocAdministrativo", DbType.Int32, pIdTipoDocAdministrativo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<AdministrativoDocumento> vListaAdministrativoDocumento = new List<AdministrativoDocumento>();
                        while (vDataReaderResults.Read())
                        {
                            AdministrativoDocumento vAdministrativoDocumento = new AdministrativoDocumento();
                            vAdministrativoDocumento.IdAdministrativoDocumento = vDataReaderResults["IdAdministrativoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativoDocumento"].ToString()) : vAdministrativoDocumento.IdAdministrativoDocumento;
                            vAdministrativoDocumento.IdAdministrativo = vDataReaderResults["IdAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAdministrativo"].ToString()) : vAdministrativoDocumento.IdAdministrativo;
                            vAdministrativoDocumento.IdTipoDocAdministrativo = vDataReaderResults["IdTipoDocAdministrativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocAdministrativo"].ToString()) : vAdministrativoDocumento.IdTipoDocAdministrativo;
                            vAdministrativoDocumento.IdArchivo = vDataReaderResults["IdArchivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdArchivo"].ToString()) : vAdministrativoDocumento.IdArchivo;
                            vAdministrativoDocumento.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAdministrativoDocumento.UsuarioCrea;
                            vAdministrativoDocumento.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAdministrativoDocumento.FechaCrea;
                            vAdministrativoDocumento.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAdministrativoDocumento.UsuarioModifica;
                            vAdministrativoDocumento.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAdministrativoDocumento.FechaModifica;
                            vListaAdministrativoDocumento.Add(vAdministrativoDocumento);
                        }
                        return vListaAdministrativoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
