using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase TerceroDatoAdicionalDAL
    /// </summary>
    public class TerceroDatoAdicionalDAL : GeneralDAL
    {
        public TerceroDatoAdicionalDAL()
        {
        }

        /// <summary>
        /// Inserta un TerceroDatoAdicional
        /// </summary>
        /// <param name="pTerceroDatoAdicional"></param>
        /// <returns></returns>
        public int InsertarTerceroDatoAdicional(TerceroDatoAdicional pTerceroDatoAdicional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                DbCommand vDbCommand;
                using (vDbCommand = vDataBase.GetStoredProcCommand("usp_Terceros_TerceroDatoAdicional_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTerceroDatoAdicional.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@NivelInteres", DbType.String, pTerceroDatoAdicional.NivelInteres);
                    vDataBase.AddInParameter(vDbCommand, "@IdClaseActividad", DbType.Int32, pTerceroDatoAdicional.IdClaseActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pTerceroDatoAdicional.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTerceroDatoAdicional.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTerceroDatoAdicional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica un TerceroDatoAdicional
        /// </summary>
        /// <param name="pTerceroDatoAdicional"></param>
        /// <returns></returns>
        public int ModificarTerceroDatoAdicional(TerceroDatoAdicional pTerceroDatoAdicional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Terceros_TerceroDatoAdicional_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTerceroDatoAdicional.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@NivelInteres", DbType.String, pTerceroDatoAdicional.NivelInteres);
                    vDataBase.AddInParameter(vDbCommand, "@IdClaseActividad", DbType.Int32, pTerceroDatoAdicional.IdClaseActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pTerceroDatoAdicional.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTerceroDatoAdicional.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTerceroDatoAdicional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar TerceroDatoAdicional por Id
        /// </summary>
        /// <param name="pIdTerceroDatoAdicional"></param>
        /// <returns></returns>
        public TerceroDatoAdicional ConsultarTerceroDatoAdicional(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Terceros_TerceroDatoAdicional_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TerceroDatoAdicional vTerceroDatoAdicional = new TerceroDatoAdicional();
                        while (vDataReaderResults.Read())
                        {
                            vTerceroDatoAdicional.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTerceroDatoAdicional.IdTercero;
                            vTerceroDatoAdicional.NivelInteres = vDataReaderResults["NivelInteres"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NivelInteres"]) : vTerceroDatoAdicional.NivelInteres;
                            vTerceroDatoAdicional.IdClaseActividad = vDataReaderResults["IdClaseActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClaseActividad"].ToString()) : vTerceroDatoAdicional.IdClaseActividad;
                            vTerceroDatoAdicional.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"]) : vTerceroDatoAdicional.Observaciones;
                            
                            vTerceroDatoAdicional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTerceroDatoAdicional.UsuarioCrea;
                            vTerceroDatoAdicional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTerceroDatoAdicional.FechaCrea;
                            vTerceroDatoAdicional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTerceroDatoAdicional.UsuarioModifica;
                            vTerceroDatoAdicional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTerceroDatoAdicional.FechaModifica;

                        }
                        return vTerceroDatoAdicional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
