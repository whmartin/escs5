﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class DepEjecucionDAL : GeneralDAL
    {
        public DepEjecucionDAL()
        {
        }
        public int InsertarDepEjecucion(DepEjecucion pDepEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepEjecucion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDepEjecucion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pDepEjecucion.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pDepEjecucion.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@Cupos", DbType.Int32, pDepEjecucion.Cupos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDepEjecucion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDepEjecucion.IdDepEjecucion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDepEjecucion").ToString());
                    GenerarLogAuditoria(pDepEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDepEjecucion(DepEjecucion pDepEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepEjecucion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDepEjecucion", DbType.Int32, pDepEjecucion.IdDepEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pDepEjecucion.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pDepEjecucion.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@Cupos", DbType.Int32, pDepEjecucion.Cupos);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDepEjecucion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDepEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDepEjecucion(DepEjecucion pDepEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepEjecucion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDepEjecucion", DbType.Int32, pDepEjecucion.IdDepEjecucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDepEjecucion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DepEjecucion ConsultarDepEjecucion(int pIdDepEjecucion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepEjecucion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDepEjecucion", DbType.Int32, pIdDepEjecucion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DepEjecucion vDepEjecucion = new DepEjecucion();
                        while (vDataReaderResults.Read())
                        {
                            vDepEjecucion.IdDepEjecucion = vDataReaderResults["IdDepEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepEjecucion"].ToString()) : vDepEjecucion.IdDepEjecucion;
                            vDepEjecucion.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDepEjecucion.IdExpEntidad;
                            vDepEjecucion.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDepEjecucion.IdDepartamento;
                            vDepEjecucion.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDepEjecucion.NombreDepartamento;
                            vDepEjecucion.Cupos = vDataReaderResults["Cupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cupos"].ToString()) : vDepEjecucion.Cupos;
                            vDepEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDepEjecucion.UsuarioCrea;
                            vDepEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDepEjecucion.FechaCrea;
                            vDepEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDepEjecucion.UsuarioModifica;
                            vDepEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDepEjecucion.FechaModifica;
                        }
                        return vDepEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DepEjecucion> ConsultarDepEjecucions(int vIdExpEntidad, int? pIdDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_DepEjecucions_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, vIdExpEntidad);
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DepEjecucion> vListaDepEjecucion = new List<DepEjecucion>();
                        while (vDataReaderResults.Read())
                        {
                            DepEjecucion vDepEjecucion = new DepEjecucion();
                            vDepEjecucion.IdDepEjecucion = vDataReaderResults["IdDepEjecucion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepEjecucion"].ToString()) : vDepEjecucion.IdDepEjecucion;
                            vDepEjecucion.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDepEjecucion.IdExpEntidad;
                            vDepEjecucion.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vDepEjecucion.IdDepartamento;
                            vDepEjecucion.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vDepEjecucion.NombreDepartamento;
                            vDepEjecucion.Cupos = vDataReaderResults["Cupos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Cupos"].ToString()) : vDepEjecucion.Cupos;
                            vDepEjecucion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDepEjecucion.UsuarioCrea;
                            vDepEjecucion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDepEjecucion.FechaCrea;
                            vDepEjecucion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDepEjecucion.UsuarioModifica;
                            vDepEjecucion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDepEjecucion.FechaModifica;
                            vListaDepEjecucion.Add(vDepEjecucion);
                        }
                        return vListaDepEjecucion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}