using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de Clase FinancieroDAL
    /// </summary>
    public class FinancieroDAL : GeneralDAL
    {
        public FinancieroDAL()
        {
        }
        /// <summary>
        /// Insertar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int InsertarFinanciero(Financiero pFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Financiero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IDInfoFin", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Anio", DbType.Int32, pFinanciero.Anio);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoCorriente", DbType.Decimal, pFinanciero.ActivoCorriente);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoFijo", DbType.Decimal, pFinanciero.ActivoFijo);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoCorriente", DbType.Decimal, pFinanciero.PasivoCorriente);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoLargoPlazo", DbType.Decimal, pFinanciero.PasivoLargoPlazo);
                    vDataBase.AddInParameter(vDbCommand, "@Patrimonio", DbType.Decimal, pFinanciero.Patrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@Ingresosporservicios", DbType.Decimal, pFinanciero.Ingresosporservicios);
                    vDataBase.AddInParameter(vDbCommand, "@ContratosICBFPrimeraInfancia", DbType.Decimal, pFinanciero.ContratosICBFPrimeraInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@ContratosICBFDiferentePrimeraInfancia", DbType.Decimal, pFinanciero.ContratosICBFDiferentePrimeraInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@ContratoOtrasEntidades", DbType.Decimal, pFinanciero.ContratoOtrasEntidades);
                    vDataBase.AddInParameter(vDbCommand, "@CostosServicios", DbType.Decimal, pFinanciero.CostosServicios);
                    vDataBase.AddInParameter(vDbCommand, "@UtilidadBruta", DbType.Decimal, pFinanciero.UtilidadBruta);
                    vDataBase.AddInParameter(vDbCommand, "@GastosOperacion", DbType.Decimal, pFinanciero.GastosOperacion);
                    vDataBase.AddInParameter(vDbCommand, "@OtrosIngresos", DbType.Decimal, pFinanciero.OtrosIngresos);
                    vDataBase.AddInParameter(vDbCommand, "@GastosFinancieros", DbType.Decimal, pFinanciero.GastosFinancieros);
                    vDataBase.AddInParameter(vDbCommand, "@OtrosEgresos", DbType.Decimal, pFinanciero.OtrosEgresos);
                    vDataBase.AddInParameter(vDbCommand, "@ProvisionImpuestosRentas", DbType.Decimal, pFinanciero.ProvisionImpuestosRentas);
                    vDataBase.AddInParameter(vDbCommand, "@RUP", DbType.Boolean, pFinanciero.RUP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRUP", DbType.Int32, pFinanciero.NumeroRUP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFinanciero.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@ObligacionesFinancieras", DbType.Decimal, pFinanciero.ObligacionesFinancieras);
                    vDataBase.AddInParameter(vDbCommand, "@IDOferente", DbType.Int32, pFinanciero.IdOferente);
                    
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFinanciero.IDInfoFin = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IDInfoFin").ToString());
                    GenerarLogAuditoria(pFinanciero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int ModificarFinanciero(Financiero pFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Financiero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDInfoFin", DbType.Int32, pFinanciero.IDInfoFin);
                    vDataBase.AddInParameter(vDbCommand, "@Anio", DbType.Int32, pFinanciero.Anio);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoCorriente", DbType.Decimal, pFinanciero.ActivoCorriente);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoFijo", DbType.Decimal, pFinanciero.ActivoFijo);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoCorriente", DbType.Decimal, pFinanciero.PasivoCorriente);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoLargoPlazo", DbType.Decimal, pFinanciero.PasivoLargoPlazo);
                    vDataBase.AddInParameter(vDbCommand, "@Patrimonio", DbType.Decimal, pFinanciero.Patrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@Ingresosporservicios", DbType.Decimal, pFinanciero.Ingresosporservicios);
                    vDataBase.AddInParameter(vDbCommand, "@ContratosICBFPrimeraInfancia", DbType.Decimal, pFinanciero.ContratosICBFPrimeraInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@ContratosICBFDiferentePrimeraInfancia", DbType.Decimal, pFinanciero.ContratosICBFDiferentePrimeraInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@ContratoOtrasEntidades", DbType.Decimal, pFinanciero.ContratoOtrasEntidades);
                    vDataBase.AddInParameter(vDbCommand, "@CostosServicios", DbType.Decimal, pFinanciero.CostosServicios);
                    vDataBase.AddInParameter(vDbCommand, "@UtilidadBruta", DbType.Decimal, pFinanciero.UtilidadBruta);
                    vDataBase.AddInParameter(vDbCommand, "@GastosOperacion", DbType.Decimal, pFinanciero.GastosOperacion);
                    vDataBase.AddInParameter(vDbCommand, "@OtrosIngresos", DbType.Decimal, pFinanciero.OtrosIngresos);
                    vDataBase.AddInParameter(vDbCommand, "@GastosFinancieros", DbType.Decimal, pFinanciero.GastosFinancieros);
                    vDataBase.AddInParameter(vDbCommand, "@OtrosEgresos", DbType.Decimal, pFinanciero.OtrosEgresos);
                    vDataBase.AddInParameter(vDbCommand, "@ProvisionImpuestosRentas", DbType.Decimal, pFinanciero.ProvisionImpuestosRentas);
                    vDataBase.AddInParameter(vDbCommand, "@RUP", DbType.Boolean, pFinanciero.RUP);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroRUP", DbType.Int32, pFinanciero.NumeroRUP);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFinanciero.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@ObligacionesFinancieras", DbType.Decimal, pFinanciero.ObligacionesFinancieras);
                    vDataBase.AddInParameter(vDbCommand, "@IdOferente", DbType.Int32, pFinanciero.IdOferente);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFinanciero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Eliminar Financiero
        /// </summary>
        /// <param name="pFinanciero"></param>
        /// <returns></returns>
        public int EliminarFinanciero(Financiero pFinanciero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Financiero_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IDInfoFin", DbType.Int32, pFinanciero.IDInfoFin);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFinanciero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Financiero ConsultarFinanciero(int? pIDInfoFin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Financiero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDInfoFin", DbType.Int32, pIDInfoFin);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Financiero vFinanciero = new Financiero();
                        while (vDataReaderResults.Read())
                        {
                            vFinanciero.IDInfoFin = vDataReaderResults["IDInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDInfoFin"].ToString()) : vFinanciero.IDInfoFin;
                            vFinanciero.Anio = vDataReaderResults["Anio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anio"].ToString()) : vFinanciero.Anio;
                            vFinanciero.ActivoCorriente = vDataReaderResults["ActivoCorriente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoCorriente"].ToString()) : vFinanciero.ActivoCorriente;
                            vFinanciero.ActivoFijo = vDataReaderResults["ActivoFijo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoFijo"].ToString()) : vFinanciero.ActivoFijo;
                            vFinanciero.PasivoCorriente = vDataReaderResults["PasivoCorriente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoCorriente"].ToString()) : vFinanciero.PasivoCorriente;
                            vFinanciero.PasivoLargoPlazo = vDataReaderResults["PasivoLargoPlazo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoLargoPlazo"].ToString()) : vFinanciero.PasivoLargoPlazo;
                            vFinanciero.Patrimonio = vDataReaderResults["Patrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Patrimonio"].ToString()) : vFinanciero.Patrimonio;
                            vFinanciero.Ingresosporservicios = vDataReaderResults["Ingresosporservicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Ingresosporservicios"].ToString()) : vFinanciero.Ingresosporservicios;
                            vFinanciero.ContratosICBFPrimeraInfancia = vDataReaderResults["ContratosICBFPrimeraInfancia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ContratosICBFPrimeraInfancia"].ToString()) : vFinanciero.ContratosICBFPrimeraInfancia;
                            vFinanciero.ContratosICBFDiferentePrimeraInfancia = vDataReaderResults["ContratosICBFDiferentePrimeraInfancia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ContratosICBFDiferentePrimeraInfancia"].ToString()) : vFinanciero.ContratosICBFDiferentePrimeraInfancia;
                            vFinanciero.ContratoOtrasEntidades = vDataReaderResults["ContratoOtrasEntidades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ContratoOtrasEntidades"].ToString()) : vFinanciero.ContratoOtrasEntidades;
                            vFinanciero.CostosServicios = vDataReaderResults["CostosServicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CostosServicios"].ToString()) : vFinanciero.CostosServicios;
                            vFinanciero.UtilidadBruta = vDataReaderResults["UtilidadBruta"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadBruta"].ToString()) : vFinanciero.UtilidadBruta;
                            vFinanciero.GastosOperacion = vDataReaderResults["GastosOperacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["GastosOperacion"].ToString()) : vFinanciero.GastosOperacion;
                            vFinanciero.OtrosIngresos = vDataReaderResults["OtrosIngresos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["OtrosIngresos"].ToString()) : vFinanciero.OtrosIngresos;
                            vFinanciero.GastosFinancieros = vDataReaderResults["GastosFinancieros"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["GastosFinancieros"].ToString()) : vFinanciero.GastosFinancieros;
                            vFinanciero.OtrosEgresos = vDataReaderResults["OtrosEgresos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["OtrosEgresos"].ToString()) : vFinanciero.OtrosEgresos;
                            vFinanciero.ProvisionImpuestosRentas = vDataReaderResults["ProvisionImpuestosRentas"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ProvisionImpuestosRentas"].ToString()) : vFinanciero.ProvisionImpuestosRentas;
                            vFinanciero.RUP = vDataReaderResults["RUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUP"].ToString()) : vFinanciero.RUP;
                            vFinanciero.NumeroRUP = vDataReaderResults["NumeroRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroRUP"].ToString()) : vFinanciero.NumeroRUP;
                            vFinanciero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFinanciero.UsuarioCrea;
                            vFinanciero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFinanciero.FechaCrea;
                            vFinanciero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFinanciero.UsuarioModifica;
                            vFinanciero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFinanciero.FechaModifica;
                            vFinanciero.ObligacionesFinancieras = vDataReaderResults["ObligacionesFinancieras"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ObligacionesFinancieras"].ToString()) : vFinanciero.ObligacionesFinancieras;
                            vFinanciero.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vFinanciero.AcnoVigencia;
                        }
                        return vFinanciero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Financiero
        /// </summary>
        /// <param name="pAnio"></param>
        /// <param name="IdOferente"></param>
        /// <returns></returns>
        public List<Financiero> ConsultarFinancieros(int? pAnio,int? IdOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Financieros_Consultar"))
                {
                    if(pAnio != null)
                         vDataBase.AddInParameter(vDbCommand, "@Anio", DbType.Int32, pAnio);

                     if(IdOferente != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdOferente", DbType.Int32, IdOferente);

                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Financiero> vListaFinanciero = new List<Financiero>();
                        while (vDataReaderResults.Read())
                        {
                                Financiero vFinanciero = new Financiero();
                            vFinanciero.IDInfoFin = vDataReaderResults["IDInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDInfoFin"].ToString()) : vFinanciero.IDInfoFin;
                            vFinanciero.Anio = vDataReaderResults["Anio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anio"].ToString()) : vFinanciero.Anio;
                            vFinanciero.AcnoVigencia = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["AcnoVigencia"].ToString()) : vFinanciero.AcnoVigencia;
                            vFinanciero.ActivoCorriente = vDataReaderResults["ActivoCorriente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoCorriente"].ToString()) : vFinanciero.ActivoCorriente;
                            vFinanciero.ActivoFijo = vDataReaderResults["ActivoFijo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoFijo"].ToString()) : vFinanciero.ActivoFijo;
                            vFinanciero.PasivoCorriente = vDataReaderResults["PasivoCorriente"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoCorriente"].ToString()) : vFinanciero.PasivoCorriente;
                            vFinanciero.PasivoLargoPlazo = vDataReaderResults["PasivoLargoPlazo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoLargoPlazo"].ToString()) : vFinanciero.PasivoLargoPlazo;
                            vFinanciero.Patrimonio = vDataReaderResults["Patrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Patrimonio"].ToString()) : vFinanciero.Patrimonio;
                            vFinanciero.Ingresosporservicios = vDataReaderResults["Ingresosporservicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Ingresosporservicios"].ToString()) : vFinanciero.Ingresosporservicios;
                            vFinanciero.ContratosICBFPrimeraInfancia = vDataReaderResults["ContratosICBFPrimeraInfancia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ContratosICBFPrimeraInfancia"].ToString()) : vFinanciero.ContratosICBFPrimeraInfancia;
                            vFinanciero.ContratosICBFDiferentePrimeraInfancia = vDataReaderResults["ContratosICBFDiferentePrimeraInfancia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ContratosICBFDiferentePrimeraInfancia"].ToString()) : vFinanciero.ContratosICBFDiferentePrimeraInfancia;
                            vFinanciero.ContratoOtrasEntidades = vDataReaderResults["ContratoOtrasEntidades"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ContratoOtrasEntidades"].ToString()) : vFinanciero.ContratoOtrasEntidades;
                            vFinanciero.CostosServicios = vDataReaderResults["CostosServicios"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CostosServicios"].ToString()) : vFinanciero.CostosServicios;
                            vFinanciero.UtilidadBruta = vDataReaderResults["UtilidadBruta"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadBruta"].ToString()) : vFinanciero.UtilidadBruta;
                            vFinanciero.GastosOperacion = vDataReaderResults["GastosOperacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["GastosOperacion"].ToString()) : vFinanciero.GastosOperacion;
                            vFinanciero.OtrosIngresos = vDataReaderResults["OtrosIngresos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["OtrosIngresos"].ToString()) : vFinanciero.OtrosIngresos;
                            vFinanciero.GastosFinancieros = vDataReaderResults["GastosFinancieros"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["GastosFinancieros"].ToString()) : vFinanciero.GastosFinancieros;
                            vFinanciero.OtrosEgresos = vDataReaderResults["OtrosEgresos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["OtrosEgresos"].ToString()) : vFinanciero.OtrosEgresos;
                            vFinanciero.ProvisionImpuestosRentas = vDataReaderResults["ProvisionImpuestosRentas"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ProvisionImpuestosRentas"].ToString()) : vFinanciero.ProvisionImpuestosRentas;
                            vFinanciero.RUP = vDataReaderResults["RUP"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RUP"].ToString()) : vFinanciero.RUP;
                            vFinanciero.NumeroRUP = vDataReaderResults["NumeroRUP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroRUP"].ToString()) : vFinanciero.NumeroRUP;
                            vFinanciero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFinanciero.UsuarioCrea;
                            vFinanciero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFinanciero.FechaCrea;
                            vFinanciero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFinanciero.UsuarioModifica;
                            vFinanciero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFinanciero.FechaModifica;
                            vFinanciero.ObligacionesFinancieras = vDataReaderResults["ObligacionesFinancieras"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ObligacionesFinancieras"].ToString()) : vFinanciero.ObligacionesFinancieras;

                            vFinanciero.TotalActivo = vDataReaderResults["TotalActivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalActivo"].ToString()) : vFinanciero.TotalActivo;
                            vFinanciero.TotalPasivo = vDataReaderResults["TotalPasivo"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalPasivo"].ToString()) : vFinanciero.TotalPasivo;
                            vFinanciero.TotalPasivoPatrimonio = vDataReaderResults["TotalPasivoPatrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["TotalPasivoPatrimonio"].ToString()) : vFinanciero.TotalPasivoPatrimonio;
                            vFinanciero.UtilidadOperativa = vDataReaderResults["UtilidadOperativa"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadOperativa"].ToString()) : vFinanciero.UtilidadOperativa;
                            vFinanciero.totalOtrosIngresosyEgresos = vDataReaderResults["totalOtrosIngresosyEgresos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["totalOtrosIngresosyEgresos"].ToString()) : vFinanciero.totalOtrosIngresosyEgresos;
                            vFinanciero.UtilidadAntesDeImpuestos = vDataReaderResults["UtilidadAntesDeImpuestos"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadAntesDeImpuestos"].ToString()) : vFinanciero.UtilidadAntesDeImpuestos;
                            vFinanciero.UtilidadNeta = vDataReaderResults["UtilidadNeta"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadNeta"].ToString()) : vFinanciero.UtilidadNeta;
                            vFinanciero.UtilidadBrutaTotal = vDataReaderResults["UtilidadBrutaTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadBrutaTotal"].ToString()) : vFinanciero.UtilidadBrutaTotal;
                            
                                vListaFinanciero.Add(vFinanciero);
                        }
                        return vListaFinanciero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
