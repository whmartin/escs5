using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase TelTercerosDAL
    /// </summary>
    public class TelTercerosDAL : GeneralDAL
    {
        public TelTercerosDAL()
        {
        }
        /// <summary>
        /// Insertar Tel Terceros
        /// </summary>
        /// <param name="pTelTerceros"></param>
        /// <returns></returns>
        public int InsertarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TelTerceros_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTelTercero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTelTerceros.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IndicativoTelefono", DbType.Int32, pTelTerceros.IndicativoTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTelefono", DbType.Int32, pTelTerceros.NumeroTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@ExtensionTelefono", DbType.Int64, pTelTerceros.ExtensionTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@Movil", DbType.Int64, pTelTerceros.Movil);
                    vDataBase.AddInParameter(vDbCommand, "@IndicativoFax", DbType.Int32, pTelTerceros.IndicativoFax);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroFax", DbType.Int32, pTelTerceros.NumeroFax);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTelTerceros.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTelTerceros.IdTelTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTelTercero").ToString());
                    GenerarLogAuditoria(pTelTerceros, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TelTerceros_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTelTercero", DbType.Int32, pTelTerceros.IdTelTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pTelTerceros.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IndicativoTelefono", DbType.Int32, pTelTerceros.IndicativoTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroTelefono", DbType.Int32, pTelTerceros.NumeroTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@ExtensionTelefono", DbType.Int64, pTelTerceros.ExtensionTelefono);
                    vDataBase.AddInParameter(vDbCommand, "@Movil", DbType.Int64, pTelTerceros.Movil);
                    vDataBase.AddInParameter(vDbCommand, "@IndicativoFax", DbType.Int32, pTelTerceros.IndicativoFax);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroFax", DbType.Int32, pTelTerceros.NumeroFax);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTelTerceros.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTelTerceros, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTelTerceros(TelTerceros pTelTerceros)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TelTerceros_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTelTercero", DbType.Int32, pTelTerceros.IdTelTercero);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTelTerceros, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TelTerceros ConsultarTelTerceros(int pIdTelTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TelTerceros_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTelTercero", DbType.Int32, pIdTelTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TelTerceros vTelTerceros = new TelTerceros();
                        while (vDataReaderResults.Read())
                        {
                            vTelTerceros.IdTelTercero = vDataReaderResults["IdTelTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTelTercero"].ToString()) : vTelTerceros.IdTelTercero;
                            vTelTerceros.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTelTerceros.IdTercero;
                            vTelTerceros.IndicativoTelefono = vDataReaderResults["IndicativoTelefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IndicativoTelefono"].ToString()) : vTelTerceros.IndicativoTelefono;
                            vTelTerceros.NumeroTelefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTelefono"].ToString()) : vTelTerceros.NumeroTelefono;
                            vTelTerceros.ExtensionTelefono = vDataReaderResults["ExtensionTelefono"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["ExtensionTelefono"].ToString()) : vTelTerceros.ExtensionTelefono;
                            vTelTerceros.Movil = vDataReaderResults["Movil"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Movil"].ToString()) : vTelTerceros.Movil;
                            vTelTerceros.IndicativoFax = vDataReaderResults["IndicativoFax"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IndicativoFax"].ToString()) : vTelTerceros.IndicativoFax;
                            vTelTerceros.NumeroFax = vDataReaderResults["NumeroFax"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroFax"].ToString()) : vTelTerceros.NumeroFax;
                            vTelTerceros.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTelTerceros.UsuarioCrea;
                            vTelTerceros.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTelTerceros.FechaCrea;
                            vTelTerceros.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTelTerceros.UsuarioModifica;
                            vTelTerceros.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTelTerceros.FechaModifica;
                        }
                        return vTelTerceros;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        ///  Consultar Tel Terceros IdTercero
        /// </summary>
        /// <param name="pIdTercero"></param>
        /// <returns></returns>
        public TelTerceros ConsultarTelTercerosIdTercero(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TelTerceros vTelTerceros = new TelTerceros();
                        while (vDataReaderResults.Read())
                        {
                            vTelTerceros.IdTelTercero = vDataReaderResults["IdTelTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTelTercero"].ToString()) : vTelTerceros.IdTelTercero;
                            vTelTerceros.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTelTerceros.IdTercero;
                            vTelTerceros.IndicativoTelefono = vDataReaderResults["IndicativoTelefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IndicativoTelefono"].ToString()) : vTelTerceros.IndicativoTelefono;
                            vTelTerceros.NumeroTelefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTelefono"].ToString()) : vTelTerceros.NumeroTelefono;
                            vTelTerceros.ExtensionTelefono = vDataReaderResults["ExtensionTelefono"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["ExtensionTelefono"].ToString()) : vTelTerceros.ExtensionTelefono;
                            vTelTerceros.Movil = vDataReaderResults["Movil"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Movil"].ToString()) : vTelTerceros.Movil;
                            vTelTerceros.IndicativoFax = vDataReaderResults["IndicativoFax"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IndicativoFax"].ToString()) : vTelTerceros.IndicativoFax;
                            vTelTerceros.NumeroFax = vDataReaderResults["NumeroFax"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroFax"].ToString()) : vTelTerceros.NumeroFax;
                            vTelTerceros.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTelTerceros.UsuarioCrea;
                            vTelTerceros.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTelTerceros.FechaCrea;
                            vTelTerceros.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTelTerceros.UsuarioModifica;
                            vTelTerceros.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTelTerceros.FechaModifica;
                        }
                        return vTelTerceros;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<TelTerceros> ConsultarTelTerceross(int? pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TelTerceross_Consultar"))
                {
                    if(pIdTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TelTerceros> vListaTelTerceros = new List<TelTerceros>();
                        while (vDataReaderResults.Read())
                        {
                                TelTerceros vTelTerceros = new TelTerceros();
                            vTelTerceros.IdTelTercero = vDataReaderResults["IdTelTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTelTercero"].ToString()) : vTelTerceros.IdTelTercero;
                            vTelTerceros.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTelTerceros.IdTercero;
                            vTelTerceros.IndicativoTelefono = vDataReaderResults["IndicativoTelefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IndicativoTelefono"].ToString()) : vTelTerceros.IndicativoTelefono;
                            vTelTerceros.NumeroTelefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroTelefono"].ToString()) : vTelTerceros.NumeroTelefono;
                            vTelTerceros.ExtensionTelefono = vDataReaderResults["ExtensionTelefono"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["ExtensionTelefono"].ToString()) : vTelTerceros.ExtensionTelefono;
                            vTelTerceros.Movil = vDataReaderResults["Movil"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Movil"].ToString()) : vTelTerceros.Movil;
                            vTelTerceros.IndicativoFax = vDataReaderResults["IndicativoFax"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IndicativoFax"].ToString()) : vTelTerceros.IndicativoFax;
                            vTelTerceros.NumeroFax = vDataReaderResults["NumeroFax"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumeroFax"].ToString()) : vTelTerceros.NumeroFax;
                            vTelTerceros.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTelTerceros.UsuarioCrea;
                            vTelTerceros.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTelTerceros.FechaCrea;
                            vTelTerceros.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTelTerceros.UsuarioModifica;
                            vTelTerceros.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTelTerceros.FechaModifica;
                                vListaTelTerceros.Add(vTelTerceros);
                        }
                        return vListaTelTerceros;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
