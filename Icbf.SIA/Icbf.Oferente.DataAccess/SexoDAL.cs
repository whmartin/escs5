using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class SexoDAL : GeneralDAL
    {
        public SexoDAL()
        {
        }
        public int InsertarSexo(Sexo pSexo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Sexo_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSexo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSexo", DbType.String, pSexo.CodigoSexo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSexo", DbType.String, pSexo.NombreSexo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pSexo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSexo.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSexo.IdSexo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSexo").ToString());
                    GenerarLogAuditoria(pSexo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSexo(Sexo pSexo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Sexo_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSexo", DbType.Int32, pSexo.IdSexo);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSexo", DbType.String, pSexo.CodigoSexo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreSexo", DbType.String, pSexo.NombreSexo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pSexo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSexo.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSexo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSexo(Sexo pSexo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Sexo_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSexo", DbType.Int32, pSexo.IdSexo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSexo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Sexo ConsultarSexo(int pIdSexo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Sexo_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSexo", DbType.Int32, pIdSexo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Sexo vSexo = new Sexo();
                        while (vDataReaderResults.Read())
                        {
                            vSexo.IdSexo = vDataReaderResults["IdSexo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSexo"].ToString()) : vSexo.IdSexo;
                            vSexo.CodigoSexo = vDataReaderResults["CodigoSexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSexo"].ToString()) : vSexo.CodigoSexo;
                            vSexo.NombreSexo = vDataReaderResults["NombreSexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSexo"].ToString()) : vSexo.NombreSexo;
                            vSexo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vSexo.Estado;
                            vSexo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSexo.UsuarioCrea;
                            vSexo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSexo.FechaCrea;
                            vSexo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSexo.UsuarioModifica;
                            vSexo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSexo.FechaModifica;
                        }
                        return vSexo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Sexo> ConsultarSexos(String pCodigoSexo, String pNombreSexo, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Sexos_Consultar"))
                {
                    if(pCodigoSexo != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoSexo", DbType.String, pCodigoSexo);
                    if(pNombreSexo != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreSexo", DbType.String, pNombreSexo);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Sexo> vListaSexo = new List<Sexo>();
                        while (vDataReaderResults.Read())
                        {
                                Sexo vSexo = new Sexo();
                            vSexo.IdSexo = vDataReaderResults["IdSexo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSexo"].ToString()) : vSexo.IdSexo;
                            vSexo.CodigoSexo = vDataReaderResults["CodigoSexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSexo"].ToString()) : vSexo.CodigoSexo;
                            vSexo.NombreSexo = vDataReaderResults["NombreSexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSexo"].ToString()) : vSexo.NombreSexo;
                            vSexo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vSexo.Estado;
                            if (vSexo.Estado == true)
                                vSexo.DescripcionEstado = "Activo";
                            else
                                vSexo.DescripcionEstado = "Inactivo";
                            vSexo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSexo.UsuarioCrea;
                            vSexo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSexo.FechaCrea;
                            vSexo.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSexo.UsuarioModifica;
                            vSexo.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSexo.FechaModifica;
                                vListaSexo.Add(vSexo);
                        }
                        return vListaSexo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
