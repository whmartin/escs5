using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ZonaResidenciaDAL : GeneralDAL
    {
        public ZonaResidenciaDAL()
        {
        }
        public int InsertarZonaResidencia(ZonaResidencia pZonaResidencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ZonaResidencia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdZonaResidencia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoZonaResidencia", DbType.String, pZonaResidencia.CodigoZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZonaResidencia", DbType.String, pZonaResidencia.NombreZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pZonaResidencia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pZonaResidencia.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pZonaResidencia.IdZonaResidencia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdZonaResidencia").ToString());
                    GenerarLogAuditoria(pZonaResidencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarZonaResidencia(ZonaResidencia pZonaResidencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ZonaResidencia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdZonaResidencia", DbType.Int32, pZonaResidencia.IdZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoZonaResidencia", DbType.String, pZonaResidencia.CodigoZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZonaResidencia", DbType.String, pZonaResidencia.NombreZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pZonaResidencia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pZonaResidencia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pZonaResidencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarZonaResidencia(ZonaResidencia pZonaResidencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ZonaResidencia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdZonaResidencia", DbType.Int32, pZonaResidencia.IdZonaResidencia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pZonaResidencia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public ZonaResidencia ConsultarZonaResidencia(int pIdZonaResidencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ZonaResidencia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdZonaResidencia", DbType.Int32, pIdZonaResidencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ZonaResidencia vZonaResidencia = new ZonaResidencia();
                        while (vDataReaderResults.Read())
                        {
                            vZonaResidencia.IdZonaResidencia = vDataReaderResults["IdZonaResidencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZonaResidencia"].ToString()) : vZonaResidencia.IdZonaResidencia;
                            vZonaResidencia.CodigoZonaResidencia = vDataReaderResults["CodigoZonaResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoZonaResidencia"].ToString()) : vZonaResidencia.CodigoZonaResidencia;
                            vZonaResidencia.NombreZonaResidencia = vDataReaderResults["NombreZonaResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZonaResidencia"].ToString()) : vZonaResidencia.NombreZonaResidencia;
                            vZonaResidencia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vZonaResidencia.Estado;
                            vZonaResidencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vZonaResidencia.UsuarioCrea;
                            vZonaResidencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vZonaResidencia.FechaCrea;
                            vZonaResidencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vZonaResidencia.UsuarioModifica;
                            vZonaResidencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vZonaResidencia.FechaModifica;
                        }
                        return vZonaResidencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ZonaResidencia> ConsultarZonaResidencias(String pCodigoZonaResidencia, String pNombreZonaResidencia, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ZonaResidencias_Consultar"))
                {
                    if(pCodigoZonaResidencia != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoZonaResidencia", DbType.String, pCodigoZonaResidencia);
                    if(pNombreZonaResidencia != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreZonaResidencia", DbType.String, pNombreZonaResidencia);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ZonaResidencia> vListaZonaResidencia = new List<ZonaResidencia>();
                        while (vDataReaderResults.Read())
                        {
                                ZonaResidencia vZonaResidencia = new ZonaResidencia();
                            vZonaResidencia.IdZonaResidencia = vDataReaderResults["IdZonaResidencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZonaResidencia"].ToString()) : vZonaResidencia.IdZonaResidencia;
                            vZonaResidencia.CodigoZonaResidencia = vDataReaderResults["CodigoZonaResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoZonaResidencia"].ToString()) : vZonaResidencia.CodigoZonaResidencia;
                            vZonaResidencia.NombreZonaResidencia = vDataReaderResults["NombreZonaResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZonaResidencia"].ToString()) : vZonaResidencia.NombreZonaResidencia;
                            vZonaResidencia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vZonaResidencia.Estado;
                            if (vZonaResidencia.Estado == true)
                                vZonaResidencia.DescripcionEstado = "Activo";
                            else
                                vZonaResidencia.DescripcionEstado = "Inactivo";
                            vZonaResidencia.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vZonaResidencia.UsuarioCrea;
                            vZonaResidencia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vZonaResidencia.FechaCrea;
                            vZonaResidencia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vZonaResidencia.UsuarioModifica;
                            vZonaResidencia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vZonaResidencia.FechaModifica;
                                vListaZonaResidencia.Add(vZonaResidencia);
                        }
                        return vListaZonaResidencia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
