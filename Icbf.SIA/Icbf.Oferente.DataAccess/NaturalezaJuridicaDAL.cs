using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase NaturalezaJuridicaDAL
    /// </summary>
    public class NaturalezaJuridicaDAL : GeneralDAL
    {
        public NaturalezaJuridicaDAL()
        {
        }
    /// <summary>
        /// Consultar Naturaleza Juridicas
        /// </summary>
        /// <param name="pCodigoNaturalezaJuridica"></param>
        /// <param name="pNombreNaturalezaJuridica"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<NaturalezaJuridica> ConsultarNaturalezaJuridicas(String pCodigoNaturalezaJuridica, String pNombreNaturalezaJuridica, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_NaturalezaJuridicas_Consultar"))
                {
                    if(pCodigoNaturalezaJuridica != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoNaturalezaJuridica", DbType.String, pCodigoNaturalezaJuridica);
                    if(pNombreNaturalezaJuridica != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreNaturalezaJuridica", DbType.String, pNombreNaturalezaJuridica);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NaturalezaJuridica> vListaNaturalezaJuridica = new List<NaturalezaJuridica>();
                        while (vDataReaderResults.Read())
                        {
                                NaturalezaJuridica vNaturalezaJuridica = new NaturalezaJuridica();
                            vNaturalezaJuridica.IdNaturalezaJuridica = vDataReaderResults["IdNaturalezaJuridica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNaturalezaJuridica"].ToString()) : vNaturalezaJuridica.IdNaturalezaJuridica;
                            vNaturalezaJuridica.CodigoNaturalezaJuridica = vDataReaderResults["CodigoNaturalezaJuridica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNaturalezaJuridica"].ToString()) : vNaturalezaJuridica.CodigoNaturalezaJuridica;
                            vNaturalezaJuridica.NombreNaturalezaJuridica = vDataReaderResults["NombreNaturalezaJuridica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreNaturalezaJuridica"].ToString()) : vNaturalezaJuridica.NombreNaturalezaJuridica;
                            vNaturalezaJuridica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNaturalezaJuridica.Estado;
                            vNaturalezaJuridica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNaturalezaJuridica.UsuarioCrea;
                            vNaturalezaJuridica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNaturalezaJuridica.FechaCrea;
                            vNaturalezaJuridica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNaturalezaJuridica.UsuarioModifica;
                            vNaturalezaJuridica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNaturalezaJuridica.FechaModifica;

                            if (vNaturalezaJuridica.Estado == true)
                            {
                                vNaturalezaJuridica.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vNaturalezaJuridica.DescripcionEstado = "Inactivo";
                            }                                
                            
                            vListaNaturalezaJuridica.Add(vNaturalezaJuridica);
                        }
                        return vListaNaturalezaJuridica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
