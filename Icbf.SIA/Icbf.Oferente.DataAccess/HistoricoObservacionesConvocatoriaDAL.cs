﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.DataAccess
{
    public class HistoricoObservacionesConvocatoriaDAL : GeneralDAL
    {
        public int InsertarHistoricoObservacionesConvocatoria(HistoricoObservacionesConvocatoria pHistoricoObservacionesConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_HistoricoObservacionesConvocatoria_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObservacionConvocatoria", DbType.Int32, pHistoricoObservacionesConvocatoria.IdObservacionConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioAnterior", DbType.Int32, pHistoricoObservacionesConvocatoria.IdUsuarioAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuarioNuevo", DbType.Int32, pHistoricoObservacionesConvocatoria.IdUsuarioNuevo);
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObservacionAnterior", DbType.Int32, pHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObservacionNueva", DbType.Int32, pHistoricoObservacionesConvocatoria.IdGestionObservacionNueva);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoObservacionAnterior", DbType.Int32, pHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoObservacionNueva", DbType.Int32, pHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pHistoricoObservacionesConvocatoria.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pHistoricoObservacionesConvocatoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public HistoricoObservacionesConvocatoria ConsultarHistoricoObservacionesConvocatoriaPorGestionMasReciente(int pIdGestionObservacionNueva)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_HistoricoObservacionesConvocatoria_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdGestionObservacionNueva", DbType.Int32, pIdGestionObservacionNueva);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        HistoricoObservacionesConvocatoria vHistoricoObservacionesConvocatoria = new HistoricoObservacionesConvocatoria();
                        while (vDataReaderResults.Read())
                        {
                            vHistoricoObservacionesConvocatoria.IdHistoricoObservacionConvocatoria = vDataReaderResults["IdHistoricoObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdHistoricoObservacionConvocatoria"].ToString()) : vHistoricoObservacionesConvocatoria.IdHistoricoObservacionConvocatoria;
                            vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria = vDataReaderResults["IdObservacionConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObservacionConvocatoria"].ToString()) : vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria;
                            vHistoricoObservacionesConvocatoria.IdUsuarioAnterior = vDataReaderResults["IdUsuarioAnterior"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioAnterior"].ToString()) : vHistoricoObservacionesConvocatoria.IdUsuarioAnterior;
                            vHistoricoObservacionesConvocatoria.IdUsuarioNuevo = vDataReaderResults["IdUsuarioNuevo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuarioNuevo"].ToString()) : vHistoricoObservacionesConvocatoria.IdUsuarioNuevo;
                            vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior = vDataReaderResults["IdGestionObservacionAnterior"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionObservacionAnterior"].ToString()) : vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior;
                            vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva = vDataReaderResults["IdGestionObservacionNueva"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdGestionObservacionNueva"].ToString()) : vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva;
                            vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior = vDataReaderResults["IdEstadoObservacionAnterior"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoObservacionAnterior"].ToString()) : vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior;
                            vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva = vDataReaderResults["IdEstadoObservacionNueva"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoObservacionNueva"].ToString()) : vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva;
                        }
                        return vHistoricoObservacionesConvocatoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
