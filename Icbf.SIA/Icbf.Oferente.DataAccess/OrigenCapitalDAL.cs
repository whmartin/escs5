using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class OrigenCapitalDAL : GeneralDAL
    {
        public OrigenCapitalDAL()
        {
        }
        public int InsertarOrigenCapital(OrigenCapital pOrigenCapital)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OrigenCapital_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdOrigenCapital", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoOrigenCapital", DbType.String, pOrigenCapital.CodigoOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@NombreOrigenCapital", DbType.String, pOrigenCapital.NombreOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pOrigenCapital.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pOrigenCapital.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pOrigenCapital.IdOrigenCapital = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdOrigenCapital").ToString());
                    GenerarLogAuditoria(pOrigenCapital, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarOrigenCapital(OrigenCapital pOrigenCapital)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OrigenCapital_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdOrigenCapital", DbType.Int32, pOrigenCapital.IdOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoOrigenCapital", DbType.String, pOrigenCapital.CodigoOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@NombreOrigenCapital", DbType.String, pOrigenCapital.NombreOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pOrigenCapital.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pOrigenCapital.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pOrigenCapital, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarOrigenCapital(OrigenCapital pOrigenCapital)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OrigenCapital_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdOrigenCapital", DbType.Int32, pOrigenCapital.IdOrigenCapital);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pOrigenCapital, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public OrigenCapital ConsultarOrigenCapital(int pIdOrigenCapital)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OrigenCapital_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdOrigenCapital", DbType.Int32, pIdOrigenCapital);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        OrigenCapital vOrigenCapital = new OrigenCapital();
                        while (vDataReaderResults.Read())
                        {
                            vOrigenCapital.IdOrigenCapital = vDataReaderResults["IdOrigenCapital"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrigenCapital"].ToString()) : vOrigenCapital.IdOrigenCapital;
                            vOrigenCapital.CodigoOrigenCapital = vDataReaderResults["CodigoOrigenCapital"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoOrigenCapital"].ToString()) : vOrigenCapital.CodigoOrigenCapital;
                            vOrigenCapital.NombreOrigenCapital = vDataReaderResults["NombreOrigenCapital"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOrigenCapital"].ToString()) : vOrigenCapital.NombreOrigenCapital;
                            vOrigenCapital.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vOrigenCapital.Estado;
                            vOrigenCapital.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vOrigenCapital.UsuarioCrea;
                            vOrigenCapital.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vOrigenCapital.FechaCrea;
                            vOrigenCapital.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vOrigenCapital.UsuarioModifica;
                            vOrigenCapital.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vOrigenCapital.FechaModifica;
                        }
                        return vOrigenCapital;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<OrigenCapital> ConsultarOrigenCapitals(String pCodigoOrigenCapital, String pNombreOrigenCapital, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OrigenCapitals_Consultar"))
                {
                    if(pCodigoOrigenCapital != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoOrigenCapital", DbType.String, pCodigoOrigenCapital);
                    if(pNombreOrigenCapital != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreOrigenCapital", DbType.String, pNombreOrigenCapital);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<OrigenCapital> vListaOrigenCapital = new List<OrigenCapital>();
                        while (vDataReaderResults.Read())
                        {
                                OrigenCapital vOrigenCapital = new OrigenCapital();
                            vOrigenCapital.IdOrigenCapital = vDataReaderResults["IdOrigenCapital"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOrigenCapital"].ToString()) : vOrigenCapital.IdOrigenCapital;
                            vOrigenCapital.CodigoOrigenCapital = vDataReaderResults["CodigoOrigenCapital"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoOrigenCapital"].ToString()) : vOrigenCapital.CodigoOrigenCapital;
                            vOrigenCapital.NombreOrigenCapital = vDataReaderResults["NombreOrigenCapital"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreOrigenCapital"].ToString()) : vOrigenCapital.NombreOrigenCapital;
                            vOrigenCapital.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vOrigenCapital.Estado;
                            if (vOrigenCapital.Estado == true)
                                vOrigenCapital.DescripcionEstado = "Activo";
                            else
                                vOrigenCapital.DescripcionEstado = "Inactivo";
                            vOrigenCapital.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vOrigenCapital.UsuarioCrea;
                            vOrigenCapital.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vOrigenCapital.FechaCrea;
                            vOrigenCapital.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vOrigenCapital.UsuarioModifica;
                            vOrigenCapital.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vOrigenCapital.FechaModifica;
                                vListaOrigenCapital.Add(vOrigenCapital);
                        }
                        return vListaOrigenCapital;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
