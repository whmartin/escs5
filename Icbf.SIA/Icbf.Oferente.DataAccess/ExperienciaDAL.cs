using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Entity;

namespace Icbf.Oferente.DataAccess
{
    public class ExperienciaDAL : GeneralDAL
    {
        public ExperienciaDAL()
        {
        }
        /// <summary>
        /// Realiza una consulta del salario m�nimo espec�fico de un a�o
        /// </summary>
        /// <param name="pAno"></param>
        /// <returns></returns>
        public SalarioMinimo ConsultarSalarioMinimo(int pAno, string pdescripcion = null)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Sigepcyp_Global_DatosVigencias_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.Int32, pAno);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pdescripcion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        SalarioMinimo vSalarioMinimo = new SalarioMinimo();
                        while (vDataReaderResults.Read())
                        {

                            vSalarioMinimo.IdSalarioMinimo = vDataReaderResults["IdDatosVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDatosVigencia"].ToString()) : vSalarioMinimo.IdSalarioMinimo;
                            vSalarioMinimo.Ano = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Codigo"].ToString()) : vSalarioMinimo.Ano;
                            vSalarioMinimo.Valor = vDataReaderResults["Valor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Valor"].ToString()) : vSalarioMinimo.Valor;
                            vSalarioMinimo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSalarioMinimo.Estado;
                            vSalarioMinimo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSalarioMinimo.UsuarioCrea;
                            vSalarioMinimo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSalarioMinimo.FechaCrea;
                        }
                        return vSalarioMinimo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Experiencia Departamento
        /// </summary>
        /// <returns></returns>
        public List<ExperienciaDepartamento> ConsultarExperienciaDepartamento()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Experiencia_ConsultarDepto"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ExperienciaDepartamento> vListaExperienciaDepartamento = new List<ExperienciaDepartamento>();
                        while (vDataReaderResults.Read())
                        {
                            ExperienciaDepartamento vExperienciaDepartamento = new ExperienciaDepartamento();
                            vExperienciaDepartamento.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vExperienciaDepartamento.IdDepartamento;
                            vExperienciaDepartamento.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vExperienciaDepartamento.NombreDepartamento;
                            vListaExperienciaDepartamento.Add(vExperienciaDepartamento);
                        }
                        return vListaExperienciaDepartamento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Experiencia Municipio
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public List<ExperienciaMunicipio> ConsultarExperienciaMunicipio(int pIdDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Experiencia_ConsultarMunicipio"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.String, pIdDepartamento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ExperienciaMunicipio> vListaExperienciaMunicipio = new List<ExperienciaMunicipio>();
                        while (vDataReaderResults.Read())
                        {
                            ExperienciaMunicipio vExperienciaMunicipio = new ExperienciaMunicipio();
                            vExperienciaMunicipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vExperienciaMunicipio.IdMunicipio;
                            vExperienciaMunicipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString().Trim().ToUpper()) : vExperienciaMunicipio.NombreMunicipio;
                            vListaExperienciaMunicipio.Add(vExperienciaMunicipio);
                        }
                        return vListaExperienciaMunicipio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

  
}
}