using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class FormaVisualizarReporteDAL : GeneralDAL
    {
        public FormaVisualizarReporteDAL()
        {
        }
        public int InsertarFormaVisualizarReporte(FormaVisualizarReporte pFormaVisualizarReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FormaVisualizarReporte_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFormaVisualizarReporte", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoFormaVisualizarReporte", DbType.String, pFormaVisualizarReporte.CodigoFormaVisualizarReporte);
                    vDataBase.AddInParameter(vDbCommand, "@NombreFormaVisualizarReporte", DbType.String, pFormaVisualizarReporte.NombreFormaVisualizarReporte);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFormaVisualizarReporte.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFormaVisualizarReporte.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFormaVisualizarReporte.IdFormaVisualizarReporte = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFormaVisualizarReporte").ToString());
                    GenerarLogAuditoria(pFormaVisualizarReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarFormaVisualizarReporte(FormaVisualizarReporte pFormaVisualizarReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FormaVisualizarReporte_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaVisualizarReporte", DbType.Int32, pFormaVisualizarReporte.IdFormaVisualizarReporte);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoFormaVisualizarReporte", DbType.String, pFormaVisualizarReporte.CodigoFormaVisualizarReporte);
                    vDataBase.AddInParameter(vDbCommand, "@NombreFormaVisualizarReporte", DbType.String, pFormaVisualizarReporte.NombreFormaVisualizarReporte);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFormaVisualizarReporte.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFormaVisualizarReporte.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFormaVisualizarReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarFormaVisualizarReporte(FormaVisualizarReporte pFormaVisualizarReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FormaVisualizarReporte_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaVisualizarReporte", DbType.Int32, pFormaVisualizarReporte.IdFormaVisualizarReporte);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFormaVisualizarReporte, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public FormaVisualizarReporte ConsultarFormaVisualizarReporte(int pIdFormaVisualizarReporte)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FormaVisualizarReporte_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFormaVisualizarReporte", DbType.Int32, pIdFormaVisualizarReporte);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FormaVisualizarReporte vFormaVisualizarReporte = new FormaVisualizarReporte();
                        while (vDataReaderResults.Read())
                        {
                            vFormaVisualizarReporte.IdFormaVisualizarReporte = vDataReaderResults["IdFormaVisualizarReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaVisualizarReporte"].ToString()) : vFormaVisualizarReporte.IdFormaVisualizarReporte;
                            vFormaVisualizarReporte.CodigoFormaVisualizarReporte = vDataReaderResults["CodigoFormaVisualizarReporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoFormaVisualizarReporte"].ToString()) : vFormaVisualizarReporte.CodigoFormaVisualizarReporte;
                            vFormaVisualizarReporte.NombreFormaVisualizarReporte = vDataReaderResults["NombreFormaVisualizarReporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFormaVisualizarReporte"].ToString()) : vFormaVisualizarReporte.NombreFormaVisualizarReporte;
                            vFormaVisualizarReporte.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFormaVisualizarReporte.Estado;
                            vFormaVisualizarReporte.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFormaVisualizarReporte.UsuarioCrea;
                            vFormaVisualizarReporte.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFormaVisualizarReporte.FechaCrea;
                            vFormaVisualizarReporte.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFormaVisualizarReporte.UsuarioModifica;
                            vFormaVisualizarReporte.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFormaVisualizarReporte.FechaModifica;
                        }
                        return vFormaVisualizarReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FormaVisualizarReporte> ConsultarFormaVisualizarReportes(String pCodigoFormaVisualizarReporte, String pNombreFormaVisualizarReporte, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FormaVisualizarReportes_Consultar"))
                {
                    if(pCodigoFormaVisualizarReporte != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoFormaVisualizarReporte", DbType.String, pCodigoFormaVisualizarReporte);
                    if(pNombreFormaVisualizarReporte != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreFormaVisualizarReporte", DbType.String, pNombreFormaVisualizarReporte);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FormaVisualizarReporte> vListaFormaVisualizarReporte = new List<FormaVisualizarReporte>();
                        while (vDataReaderResults.Read())
                        {
                                FormaVisualizarReporte vFormaVisualizarReporte = new FormaVisualizarReporte();
                            vFormaVisualizarReporte.IdFormaVisualizarReporte = vDataReaderResults["IdFormaVisualizarReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormaVisualizarReporte"].ToString()) : vFormaVisualizarReporte.IdFormaVisualizarReporte;
                            vFormaVisualizarReporte.CodigoFormaVisualizarReporte = vDataReaderResults["CodigoFormaVisualizarReporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoFormaVisualizarReporte"].ToString()) : vFormaVisualizarReporte.CodigoFormaVisualizarReporte;
                            vFormaVisualizarReporte.NombreFormaVisualizarReporte = vDataReaderResults["NombreFormaVisualizarReporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFormaVisualizarReporte"].ToString()) : vFormaVisualizarReporte.NombreFormaVisualizarReporte;
                            vFormaVisualizarReporte.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFormaVisualizarReporte.Estado;
                            vFormaVisualizarReporte.DescripcionEstado = vFormaVisualizarReporte.Estado == true ? "Activo" : "Inactivo";
                            vFormaVisualizarReporte.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFormaVisualizarReporte.UsuarioCrea;
                            vFormaVisualizarReporte.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFormaVisualizarReporte.FechaCrea;
                            vFormaVisualizarReporte.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFormaVisualizarReporte.UsuarioModifica;
                            vFormaVisualizarReporte.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFormaVisualizarReporte.FechaModifica;
                                vListaFormaVisualizarReporte.Add(vFormaVisualizarReporte);
                        }
                        return vListaFormaVisualizarReporte;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
