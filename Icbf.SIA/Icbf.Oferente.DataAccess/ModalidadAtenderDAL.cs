using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ModalidadAtenderDAL : GeneralDAL
    {
        public ModalidadAtenderDAL()
        {
        }
        public int InsertarModalidadAtender(ModalidadAtender pModalidadAtender)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ModalidadAtender_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdModalidadAtender", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoModalidadAtender", DbType.String, pModalidadAtender.CodigoModalidadAtender);
                    vDataBase.AddInParameter(vDbCommand, "@NombreModalidadAtender", DbType.String, pModalidadAtender.NombreModalidadAtender);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pModalidadAtender.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pModalidadAtender.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModalidadAtender.IdModalidadAtender = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModalidadAtender").ToString());
                    GenerarLogAuditoria(pModalidadAtender, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarModalidadAtender(ModalidadAtender pModalidadAtender)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ModalidadAtender_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadAtender", DbType.Int32, pModalidadAtender.IdModalidadAtender);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoModalidadAtender", DbType.String, pModalidadAtender.CodigoModalidadAtender);
                    vDataBase.AddInParameter(vDbCommand, "@NombreModalidadAtender", DbType.String, pModalidadAtender.NombreModalidadAtender);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pModalidadAtender.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pModalidadAtender.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModalidadAtender, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarModalidadAtender(ModalidadAtender pModalidadAtender)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ModalidadAtender_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadAtender", DbType.Int32, pModalidadAtender.IdModalidadAtender);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModalidadAtender, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public ModalidadAtender ConsultarModalidadAtender(int pIdModalidadAtender)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ModalidadAtender_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadAtender", DbType.Int32, pIdModalidadAtender);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ModalidadAtender vModalidadAtender = new ModalidadAtender();
                        while (vDataReaderResults.Read())
                        {
                            vModalidadAtender.IdModalidadAtender = vDataReaderResults["IdModalidadAtender"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadAtender"].ToString()) : vModalidadAtender.IdModalidadAtender;
                            vModalidadAtender.CodigoModalidadAtender = vDataReaderResults["CodigoModalidadAtender"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoModalidadAtender"].ToString()) : vModalidadAtender.CodigoModalidadAtender;
                            vModalidadAtender.NombreModalidadAtender = vDataReaderResults["NombreModalidadAtender"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidadAtender"].ToString()) : vModalidadAtender.NombreModalidadAtender;
                            vModalidadAtender.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModalidadAtender.Estado;
                            vModalidadAtender.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadAtender.UsuarioCrea;
                            vModalidadAtender.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadAtender.FechaCrea;
                            vModalidadAtender.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadAtender.UsuarioModifica;
                            vModalidadAtender.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadAtender.FechaModifica;
                        }
                        return vModalidadAtender;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadAtender> ConsultarModalidadAtenders(String pCodigoModalidadAtender, String pNombreModalidadAtender, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ModalidadAtenders_Consultar"))
                {
                    if(pCodigoModalidadAtender != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoModalidadAtender", DbType.String, pCodigoModalidadAtender);
                    if(pNombreModalidadAtender != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreModalidadAtender", DbType.String, pNombreModalidadAtender);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ModalidadAtender> vListaModalidadAtender = new List<ModalidadAtender>();
                        while (vDataReaderResults.Read())
                        {
                                ModalidadAtender vModalidadAtender = new ModalidadAtender();
                            vModalidadAtender.IdModalidadAtender = vDataReaderResults["IdModalidadAtender"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModalidadAtender"].ToString()) : vModalidadAtender.IdModalidadAtender;
                            vModalidadAtender.CodigoModalidadAtender = vDataReaderResults["CodigoModalidadAtender"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoModalidadAtender"].ToString()) : vModalidadAtender.CodigoModalidadAtender;
                            vModalidadAtender.NombreModalidadAtender = vDataReaderResults["NombreModalidadAtender"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidadAtender"].ToString()) : vModalidadAtender.NombreModalidadAtender;
                            vModalidadAtender.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModalidadAtender.Estado;
                            if (vModalidadAtender.Estado == true)
                                vModalidadAtender.DescripcionEstado = "Activo";
                            else
                                vModalidadAtender.DescripcionEstado = "Inactivo";
                            vModalidadAtender.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vModalidadAtender.UsuarioCrea;
                            vModalidadAtender.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vModalidadAtender.FechaCrea;
                            vModalidadAtender.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vModalidadAtender.UsuarioModifica;
                            vModalidadAtender.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vModalidadAtender.FechaModifica;
                                vListaModalidadAtender.Add(vModalidadAtender);
                        }
                        return vListaModalidadAtender;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
