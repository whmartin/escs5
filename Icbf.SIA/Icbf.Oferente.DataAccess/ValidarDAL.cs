using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ValidarDAL : GeneralDAL
    {
        public ValidarDAL()
        {
        }
        public int InsertarValidar(Validar pValidar)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Validar_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pValidar.TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pValidar.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Int32, pValidar.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Oferente", DbType.String, pValidar.Oferente);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pValidar.EstadoGuardado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidar.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValidar, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarValidar(Validar pValidar)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Validar_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pValidar.TipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@TipoIdentificacion", DbType.String, pValidar.TipoIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Int32, pValidar.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@Oferente", DbType.String, pValidar.Oferente);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pValidar.EstadoGuardado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pValidar.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValidar, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarValidar(Validar pValidar)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Validar_Eliminar"))
                {
                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValidar, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Validar ConsultarValidar()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Validar_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Validar vValidar = new Validar();
                        while (vDataReaderResults.Read())
                        {
                            vValidar.TipoPersona = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vValidar.TipoPersona;
                            vValidar.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vValidar.TipoIdentificacion;
                            vValidar.TipoPersona = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipopersona"].ToString()) : vValidar.TipoPersona;
                            vValidar.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vValidar.NumeroIdentificacion;
                            vValidar.Oferente = vDataReaderResults["Oferente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Oferente"].ToString()) : vValidar.Oferente;
                            vValidar.EstadoGuardado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vValidar.EstadoGuardado;
                            vValidar.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidar.UsuarioCrea;
                            vValidar.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidar.FechaCrea;
                            vValidar.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValidar.UsuarioModifica;
                            vValidar.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValidar.FechaModifica;
                        }
                        return vValidar;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Validar> ConsultarValidars(int? pIdTipoPersona, int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pOferente, int? pIdEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Validars_Consultar"))
                {
                    if (pIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    if (pIdTipoIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoIdentificacion);
                    if (pNumeroIdentificacion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pNumeroIdentificacion);
                    if (pOferente != null)
                        vDataBase.AddInParameter(vDbCommand, "@Oferente", DbType.String, pOferente);
                    //if (pIdEstado != null)
                    //    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pIdEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Validar> vListaValidar = new List<Validar>();
                        while (vDataReaderResults.Read())
                        {
                                Validar vValidar = new Validar();
                            vValidar.IdOferente = vDataReaderResults["IdOferente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOferente"].ToString()) : vValidar.IdOferente;
                            vValidar.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vValidar.IdTercero;
                            vValidar.IdTipoIdentificacion = vDataReaderResults["IdTipoIdentificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacion"].ToString()) : vValidar.IdTipoIdentificacion;
                            vValidar.TipoIdentificacion = vDataReaderResults["TipoIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoIdentificacion"].ToString()) : vValidar.TipoIdentificacion;
                            vValidar.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vValidar.NumeroIdentificacion;
                            vValidar.Oferente = vDataReaderResults["Oferente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Oferente"].ToString()) : vValidar.Oferente;
                            vValidar.TipoPersona = vDataReaderResults["Tipopersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipopersona"].ToString()) : vValidar.TipoPersona;
                            vValidar.EstadoGuardado = vDataReaderResults["EstadoGuardado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoGuardado"].ToString()) : vValidar.EstadoGuardado;
                            vValidar.UsuarioProtege = vDataReaderResults["UsuarioProtege"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioProtege"].ToString()) : vValidar.UsuarioProtege;
                            if (vValidar.EstadoGuardado == "True")
                            {
                                vValidar.DesEstadoGuardado = "SI";
                            }
                            else
                            {
                                vValidar.DesEstadoGuardado = "NO";
                            }
                            vValidar.EstadoValidacion = vDataReaderResults["EstadoValidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoValidacion"].ToString()) : vValidar.EstadoValidacion;
                            if (vValidar.EstadoValidacion == null)
                            {
                                vValidar.DesEstadoValidacion = "Por Validar";
                            }
                            else
                            {
                                vValidar.DesEstadoValidacion = vValidar.EstadoValidacion;
                            }

                            //vValidar.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidar.UsuarioCrea;
                            //vValidar.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidar.FechaCrea;
                            //vValidar.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValidar.UsuarioModifica;
                            //vValidar.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValidar.FechaModifica;
                                vListaValidar.Add(vValidar);
                        }
                        return vListaValidar;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<TipoPersona> ConsultarTipoPersona()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Experiencia_ConsultarTipoPersona"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoPersona> vListaTipoPersona = new List<TipoPersona>();
                        while (vDataReaderResults.Read())
                        {
                            TipoPersona vTipoPersona = new TipoPersona();
                            vTipoPersona.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTipoPersona.IdTipoPersona;
                            vTipoPersona.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vTipoPersona.NombreTipoPersona;
                            vListaTipoPersona.Add(vTipoPersona);
                        }
                        return vListaTipoPersona;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoIdentificacion> ConsultarTipoIdentificacion(String pTipoPersona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoIdentificacion_ConsultarxTipoPersona"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoIdentificacion> vListaTipoIdentificacion = new List<TipoIdentificacion>();
                        while (vDataReaderResults.Read())
                        {
                            TipoIdentificacion vTipoIdentificacion = new TipoIdentificacion();
                            vTipoIdentificacion.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoIdentificacion.IdTipoDocumento;
                            vTipoIdentificacion.NombreTipoDocumento = vDataReaderResults["TipoDocumneto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumneto"].ToString()) : vTipoIdentificacion.NombreTipoDocumento;
                            vListaTipoIdentificacion.Add(vTipoIdentificacion);


                        }
                        return vListaTipoIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ValidarOferente> ConsultarValidarOferente(int pIdTercero, int pIdOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ValidarOferente_Consultar"))
                {
                    //vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Decimal, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdOferente", DbType.Decimal, pIdOferente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarOferente> vListaValidarOferente = new List<ValidarOferente>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarOferente vValidarOferente = new ValidarOferente();
                            //vValidarOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["IdTercero"].ToString()) : vValidarOferente.IdTercero;
                            vValidarOferente.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vValidarOferente.IdModulo;
                            vValidarOferente.ComponenteaValidar = vDataReaderResults["ComponenteaValidar"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ComponenteaValidar"].ToString()) : vValidarOferente.ComponenteaValidar;
                            vValidarOferente.ComponenteValidado = vDataReaderResults["ComponenteValidado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ComponenteValidado"].ToString()) : vValidarOferente.ComponenteValidado;
                            vValidarOferente.NumRevisionActual = vDataReaderResults["NumRevisionActual"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumRevisionActual"].ToString()) : vValidarOferente.NumRevisionActual;
                            vListaValidarOferente.Add(vValidarOferente);
                        }
                        return vListaValidarOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarUsuarioValidacion(int pIdOferente, string pUsuarioProtege)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_CambiarUsuario_Validacion"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioProtege", DbType.String, pUsuarioProtege);
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Int32, pIdOferente);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string ConsultarUsuarioProtegeValidar(int pIdOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_EntidadProvOferente_ConsultarUsuario_Validacion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDENTIDAD", DbType.Decimal, pIdOferente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string usuarioProtege = "";
                        while (vDataReaderResults.Read())
                        {
                            usuarioProtege = vDataReaderResults["UsuarioProtege"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioProtege"].ToString()) : usuarioProtege;
                        }
                        return usuarioProtege;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
