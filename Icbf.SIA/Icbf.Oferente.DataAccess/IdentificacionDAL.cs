using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class IdentificacionDAL : GeneralDAL
    {
        public IdentificacionDAL()
        {
        }
        public int InsertarIdentificacion(Identificacion pIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Identificacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@Identificacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Ano", DbType.String, pIdentificacion.Ano);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadOferente", DbType.String, pIdentificacion.NombreEntidadOferente);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pIdentificacion.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@Tipodeidentificación", DbType.String, pIdentificacion.Tipodeidentificación);
                    vDataBase.AddInParameter(vDbCommand, "@NúmeroIdentificación", DbType.String, pIdentificacion.NúmeroIdentificación);
                    vDataBase.AddInParameter(vDbCommand, "@DV", DbType.String, pIdentificacion.DV);
                    vDataBase.AddInParameter(vDbCommand, "@Tipopersona", DbType.String, pIdentificacion.Tipopersona);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadAcreditaExistencia", DbType.String, pIdentificacion.NombreEntidadAcreditaExistencia);
                    vDataBase.AddInParameter(vDbCommand, "@TipoRégimenTributario", DbType.String, pIdentificacion.TipoRégimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@OrigenCapital ", DbType.String, pIdentificacion.OrigenCapital );
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajePrivado ", DbType.String, pIdentificacion.PorcentajePrivado );
                    vDataBase.AddInParameter(vDbCommand, "@Porcentaje publico ", DbType.String, pIdentificacion.Porcentajepublico );
                    vDataBase.AddInParameter(vDbCommand, "@TipoOrganización ", DbType.String, pIdentificacion.TipoOrganización );
                    vDataBase.AddInParameter(vDbCommand, "@Naturalezajurídica ", DbType.String, pIdentificacion.Naturalezajurídica );
                    vDataBase.AddInParameter(vDbCommand, "@NúmeroTotalTrabajadores ", DbType.String, pIdentificacion.NúmeroTotalTrabajadores );
                    vDataBase.AddInParameter(vDbCommand, "@ValoresActivosTotales ", DbType.String, pIdentificacion.ValoresActivosTotales );
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pIdentificacion.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pIdentificacion.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pIdentificacion.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pIdentificacion.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pIdentificacion.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pIdentificacion.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pIdentificacion.Email);
                    vDataBase.AddInParameter(vDbCommand, "@DepartamentoResidencia", DbType.String, pIdentificacion.DepartamentoResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@MunicipioResidencia", DbType.String, pIdentificacion.MunicipioResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@ZonaResidencia", DbType.String, pIdentificacion.ZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZonaResto", DbType.String, pIdentificacion.NombreZonaResto);
                    vDataBase.AddInParameter(vDbCommand, "@CentroPoblado", DbType.String, pIdentificacion.CentroPoblado);
                    vDataBase.AddInParameter(vDbCommand, "@ComunaLocalidad", DbType.String, pIdentificacion.ComunaLocalidad);
                    vDataBase.AddInParameter(vDbCommand, "@Barrio", DbType.String, pIdentificacion.Barrio);
                    vDataBase.AddInParameter(vDbCommand, "@Zona", DbType.String, pIdentificacion.Zona);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pIdentificacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pIdentificacion.Identificacion_ = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@Identificacion").ToString());
                    GenerarLogAuditoria(pIdentificacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarIdentificacion(Identificacion pIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Identificacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pIdentificacion.Identificacion_);
                    vDataBase.AddInParameter(vDbCommand, "@Ano", DbType.String, pIdentificacion.Ano);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadOferente", DbType.String, pIdentificacion.NombreEntidadOferente);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pIdentificacion.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@Tipodeidentificación", DbType.String, pIdentificacion.Tipodeidentificación);
                    vDataBase.AddInParameter(vDbCommand, "@NúmeroIdentificación", DbType.String, pIdentificacion.NúmeroIdentificación);
                    vDataBase.AddInParameter(vDbCommand, "@DV", DbType.String, pIdentificacion.DV);
                    vDataBase.AddInParameter(vDbCommand, "@Tipopersona", DbType.String, pIdentificacion.Tipopersona);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadAcreditaExistencia", DbType.String, pIdentificacion.NombreEntidadAcreditaExistencia);
                    vDataBase.AddInParameter(vDbCommand, "@TipoRégimenTributario", DbType.String, pIdentificacion.TipoRégimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@OrigenCapital ", DbType.String, pIdentificacion.OrigenCapital );
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajePrivado ", DbType.String, pIdentificacion.PorcentajePrivado );
                    vDataBase.AddInParameter(vDbCommand, "@Porcentaje publico ", DbType.String, pIdentificacion.Porcentajepublico );
                    vDataBase.AddInParameter(vDbCommand, "@TipoOrganización ", DbType.String, pIdentificacion.TipoOrganización );
                    vDataBase.AddInParameter(vDbCommand, "@Naturalezajurídica ", DbType.String, pIdentificacion.Naturalezajurídica );
                    vDataBase.AddInParameter(vDbCommand, "@NúmeroTotalTrabajadores ", DbType.String, pIdentificacion.NúmeroTotalTrabajadores );
                    vDataBase.AddInParameter(vDbCommand, "@ValoresActivosTotales ", DbType.String, pIdentificacion.ValoresActivosTotales );
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaTipoDocumento", DbType.Int32, pIdentificacion.IdDListaTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pIdentificacion.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pIdentificacion.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pIdentificacion.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pIdentificacion.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pIdentificacion.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pIdentificacion.Email);
                    vDataBase.AddInParameter(vDbCommand, "@DepartamentoResidencia", DbType.String, pIdentificacion.DepartamentoResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@MunicipioResidencia", DbType.String, pIdentificacion.MunicipioResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@ZonaResidencia", DbType.String, pIdentificacion.ZonaResidencia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreZonaResto", DbType.String, pIdentificacion.NombreZonaResto);
                    vDataBase.AddInParameter(vDbCommand, "@CentroPoblado", DbType.String, pIdentificacion.CentroPoblado);
                    vDataBase.AddInParameter(vDbCommand, "@ComunaLocalidad", DbType.String, pIdentificacion.ComunaLocalidad);
                    vDataBase.AddInParameter(vDbCommand, "@Barrio", DbType.String, pIdentificacion.Barrio);
                    vDataBase.AddInParameter(vDbCommand, "@Zona", DbType.String, pIdentificacion.Zona);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pIdentificacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIdentificacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public Identificacion ConsultarIdentificacion(int pIdentificacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Identificacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.Int32, pIdentificacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Identificacion vIdentificacion = new Identificacion();
                        while (vDataReaderResults.Read())
                        {
                            vIdentificacion.Identificacion_ = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Identificacion"].ToString()) : vIdentificacion.Identificacion_;
                            vIdentificacion.Ano = vDataReaderResults["Ano"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ano"].ToString()) : vIdentificacion.Ano;
                            vIdentificacion.NombreEntidadOferente = vDataReaderResults["NombreEntidadOferente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadOferente"].ToString()) : vIdentificacion.NombreEntidadOferente;
                            vIdentificacion.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vIdentificacion.Sigla;
                            vIdentificacion.Tipodeidentificación = vDataReaderResults["Tipodeidentificación"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipodeidentificación"].ToString()) : vIdentificacion.Tipodeidentificación;
                            vIdentificacion.NúmeroIdentificación = vDataReaderResults["NúmeroIdentificación"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NúmeroIdentificación"].ToString()) : vIdentificacion.NúmeroIdentificación;
                            vIdentificacion.DV = vDataReaderResults["DV"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DV"].ToString()) : vIdentificacion.DV;
                            vIdentificacion.Tipopersona = vDataReaderResults["Tipopersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipopersona"].ToString()) : vIdentificacion.Tipopersona;
                            vIdentificacion.NombreEntidadAcreditaExistencia = vDataReaderResults["NombreEntidadAcreditaExistencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAcreditaExistencia"].ToString()) : vIdentificacion.NombreEntidadAcreditaExistencia;
                            vIdentificacion.TipoRégimenTributario = vDataReaderResults["TipoRégimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoRégimenTributario"].ToString()) : vIdentificacion.TipoRégimenTributario;
                            vIdentificacion.OrigenCapital  = vDataReaderResults["OrigenCapital "] != DBNull.Value ? Convert.ToString(vDataReaderResults["OrigenCapital "].ToString()) : vIdentificacion.OrigenCapital ;
                            vIdentificacion.PorcentajePrivado  = vDataReaderResults["PorcentajePrivado "] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajePrivado "].ToString()) : vIdentificacion.PorcentajePrivado ;
                            vIdentificacion.Porcentajepublico  = vDataReaderResults["Porcentaje publico "] != DBNull.Value ? Convert.ToString(vDataReaderResults["Porcentaje publico "].ToString()) : vIdentificacion.Porcentajepublico ;
                            vIdentificacion.TipoOrganización  = vDataReaderResults["TipoOrganización "] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoOrganización "].ToString()) : vIdentificacion.TipoOrganización ;
                            vIdentificacion.Naturalezajurídica  = vDataReaderResults["Naturalezajurídica "] != DBNull.Value ? Convert.ToString(vDataReaderResults["Naturalezajurídica "].ToString()) : vIdentificacion.Naturalezajurídica ;
                            vIdentificacion.NúmeroTotalTrabajadores  = vDataReaderResults["NúmeroTotalTrabajadores "] != DBNull.Value ? Convert.ToString(vDataReaderResults["NúmeroTotalTrabajadores "].ToString()) : vIdentificacion.NúmeroTotalTrabajadores ;
                            vIdentificacion.ValoresActivosTotales  = vDataReaderResults["ValoresActivosTotales "] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValoresActivosTotales "].ToString()) : vIdentificacion.ValoresActivosTotales ;
                            vIdentificacion.IdDListaTipoDocumento = vDataReaderResults["IdDListaTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDListaTipoDocumento"].ToString()) : vIdentificacion.IdDListaTipoDocumento;
                            vIdentificacion.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vIdentificacion.NumeroIdentificacion;
                            vIdentificacion.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vIdentificacion.PrimerNombre;
                            vIdentificacion.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vIdentificacion.SegundoNombre;
                            vIdentificacion.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vIdentificacion.PrimerApellido;
                            vIdentificacion.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vIdentificacion.SegundoApellido;
                            vIdentificacion.Email = vDataReaderResults["Email"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email"].ToString()) : vIdentificacion.Email;
                            vIdentificacion.DepartamentoResidencia = vDataReaderResults["DepartamentoResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DepartamentoResidencia"].ToString()) : vIdentificacion.DepartamentoResidencia;
                            vIdentificacion.MunicipioResidencia = vDataReaderResults["MunicipioResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MunicipioResidencia"].ToString()) : vIdentificacion.MunicipioResidencia;
                            vIdentificacion.ZonaResidencia = vDataReaderResults["ZonaResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ZonaResidencia"].ToString()) : vIdentificacion.ZonaResidencia;
                            vIdentificacion.NombreZonaResto = vDataReaderResults["NombreZonaResto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZonaResto"].ToString()) : vIdentificacion.NombreZonaResto;
                            vIdentificacion.CentroPoblado = vDataReaderResults["CentroPoblado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CentroPoblado"].ToString()) : vIdentificacion.CentroPoblado;
                            vIdentificacion.ComunaLocalidad = vDataReaderResults["ComunaLocalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ComunaLocalidad"].ToString()) : vIdentificacion.ComunaLocalidad;
                            vIdentificacion.Barrio = vDataReaderResults["Barrio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Barrio"].ToString()) : vIdentificacion.Barrio;
                            vIdentificacion.Zona = vDataReaderResults["Zona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Zona"].ToString()) : vIdentificacion.Zona;
                            vIdentificacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vIdentificacion.UsuarioCrea;
                            vIdentificacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vIdentificacion.FechaCrea;
                            vIdentificacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vIdentificacion.UsuarioModifica;
                            vIdentificacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vIdentificacion.FechaModifica;
                        }
                        return vIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Identificacion> ConsultarIdentificacions(String pAno, String pNombreEntidadOferente, String pSigla, String pTipodeidentificación, String pNúmeroIdentificación, String pDV)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Identificacions_Consultar"))
                {
                    if(pAno != null)
                         vDataBase.AddInParameter(vDbCommand, "@Ano", DbType.String, pAno);
                    if(pNombreEntidadOferente != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreEntidadOferente", DbType.String, pNombreEntidadOferente);
                    if(pSigla != null)
                         vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pSigla);
                    if(pTipodeidentificación != null)
                         vDataBase.AddInParameter(vDbCommand, "@Tipodeidentificación", DbType.String, pTipodeidentificación);
                    if(pNúmeroIdentificación != null)
                         vDataBase.AddInParameter(vDbCommand, "@NúmeroIdentificación", DbType.String, pNúmeroIdentificación);
                    if(pDV != null)
                         vDataBase.AddInParameter(vDbCommand, "@DV", DbType.String, pDV);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Identificacion> vListaIdentificacion = new List<Identificacion>();
                        while (vDataReaderResults.Read())
                        {
                                Identificacion vIdentificacion = new Identificacion();
                            vIdentificacion.Identificacion_ = vDataReaderResults["Identificacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Identificacion"].ToString()) : vIdentificacion.Identificacion_;
                            vIdentificacion.Ano = vDataReaderResults["Ano"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Ano"].ToString()) : vIdentificacion.Ano;
                            vIdentificacion.NombreEntidadOferente = vDataReaderResults["NombreEntidadOferente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadOferente"].ToString()) : vIdentificacion.NombreEntidadOferente;
                            vIdentificacion.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vIdentificacion.Sigla;
                            vIdentificacion.Tipodeidentificación = vDataReaderResults["Tipodeidentificación"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipodeidentificación"].ToString()) : vIdentificacion.Tipodeidentificación;
                            vIdentificacion.NúmeroIdentificación = vDataReaderResults["NúmeroIdentificación"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NúmeroIdentificación"].ToString()) : vIdentificacion.NúmeroIdentificación;
                            vIdentificacion.DV = vDataReaderResults["DV"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DV"].ToString()) : vIdentificacion.DV;
                            vIdentificacion.Tipopersona = vDataReaderResults["Tipopersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tipopersona"].ToString()) : vIdentificacion.Tipopersona;
                            vIdentificacion.NombreEntidadAcreditaExistencia = vDataReaderResults["NombreEntidadAcreditaExistencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAcreditaExistencia"].ToString()) : vIdentificacion.NombreEntidadAcreditaExistencia;
                            vIdentificacion.TipoRégimenTributario = vDataReaderResults["TipoRégimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoRégimenTributario"].ToString()) : vIdentificacion.TipoRégimenTributario;
                            vIdentificacion.OrigenCapital  = vDataReaderResults["OrigenCapital "] != DBNull.Value ? Convert.ToString(vDataReaderResults["OrigenCapital "].ToString()) : vIdentificacion.OrigenCapital ;
                            vIdentificacion.PorcentajePrivado  = vDataReaderResults["PorcentajePrivado "] != DBNull.Value ? Convert.ToString(vDataReaderResults["PorcentajePrivado "].ToString()) : vIdentificacion.PorcentajePrivado ;
                            vIdentificacion.Porcentajepublico  = vDataReaderResults["Porcentaje publico "] != DBNull.Value ? Convert.ToString(vDataReaderResults["Porcentaje publico "].ToString()) : vIdentificacion.Porcentajepublico ;
                            vIdentificacion.TipoOrganización  = vDataReaderResults["TipoOrganización "] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoOrganización "].ToString()) : vIdentificacion.TipoOrganización ;
                            vIdentificacion.Naturalezajurídica  = vDataReaderResults["Naturalezajurídica "] != DBNull.Value ? Convert.ToString(vDataReaderResults["Naturalezajurídica "].ToString()) : vIdentificacion.Naturalezajurídica ;
                            vIdentificacion.NúmeroTotalTrabajadores  = vDataReaderResults["NúmeroTotalTrabajadores "] != DBNull.Value ? Convert.ToString(vDataReaderResults["NúmeroTotalTrabajadores "].ToString()) : vIdentificacion.NúmeroTotalTrabajadores ;
                            vIdentificacion.ValoresActivosTotales  = vDataReaderResults["ValoresActivosTotales "] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValoresActivosTotales "].ToString()) : vIdentificacion.ValoresActivosTotales ;
                            vIdentificacion.IdDListaTipoDocumento = vDataReaderResults["IdDListaTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDListaTipoDocumento"].ToString()) : vIdentificacion.IdDListaTipoDocumento;
                            vIdentificacion.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vIdentificacion.NumeroIdentificacion;
                            vIdentificacion.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vIdentificacion.PrimerNombre;
                            vIdentificacion.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vIdentificacion.SegundoNombre;
                            vIdentificacion.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vIdentificacion.PrimerApellido;
                            vIdentificacion.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vIdentificacion.SegundoApellido;
                            vIdentificacion.Email = vDataReaderResults["Email"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email"].ToString()) : vIdentificacion.Email;
                            vIdentificacion.DepartamentoResidencia = vDataReaderResults["DepartamentoResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DepartamentoResidencia"].ToString()) : vIdentificacion.DepartamentoResidencia;
                            vIdentificacion.MunicipioResidencia = vDataReaderResults["MunicipioResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MunicipioResidencia"].ToString()) : vIdentificacion.MunicipioResidencia;
                            vIdentificacion.ZonaResidencia = vDataReaderResults["ZonaResidencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ZonaResidencia"].ToString()) : vIdentificacion.ZonaResidencia;
                            vIdentificacion.NombreZonaResto = vDataReaderResults["NombreZonaResto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreZonaResto"].ToString()) : vIdentificacion.NombreZonaResto;
                            vIdentificacion.CentroPoblado = vDataReaderResults["CentroPoblado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CentroPoblado"].ToString()) : vIdentificacion.CentroPoblado;
                            vIdentificacion.ComunaLocalidad = vDataReaderResults["ComunaLocalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ComunaLocalidad"].ToString()) : vIdentificacion.ComunaLocalidad;
                            vIdentificacion.Barrio = vDataReaderResults["Barrio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Barrio"].ToString()) : vIdentificacion.Barrio;
                            vIdentificacion.Zona = vDataReaderResults["Zona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Zona"].ToString()) : vIdentificacion.Zona;
                            vIdentificacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vIdentificacion.UsuarioCrea;
                            vIdentificacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vIdentificacion.FechaCrea;
                            vIdentificacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vIdentificacion.UsuarioModifica;
                            vIdentificacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vIdentificacion.FechaModifica;
                                vListaIdentificacion.Add(vIdentificacion);
                        }
                        return vListaIdentificacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar Entiga Generica Parametros
        /// </summary>
        /// <param name="Tabla"></param>
        /// <param name="IdCompo"></param>
        /// <param name="ValoCampo"></param>
        /// <returns></returns>
        public List<EntigaGenericaParametros> ConsultarEntigaGenericaParametros(string Tabla, string IdCompo, string ValoCampo)
        {
            try
            {
                String Sql = @"SELECT ";
                Sql += IdCompo + ",";
                Sql += ValoCampo;
                Sql += ",UsuarioCrea,FechaCrea,UsuarioModifica,FechaModifica ";
                Sql += " FROM [Oferente].[" + Tabla + "]  WHERE Estado=1";

                Database vDataBase = ObtenerInstancia();
             
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TercerosEntigaGenericaParametros_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Sql", DbType.String, Sql);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EntigaGenericaParametros> vLista = new List<EntigaGenericaParametros>();
                        while (vDataReaderResults.Read())
                        {
                            EntigaGenericaParametros vEntigaGenericaParametros = new EntigaGenericaParametros();
                            vEntigaGenericaParametros.Id = vDataReaderResults[IdCompo] != DBNull.Value ? Convert.ToInt32(vDataReaderResults[IdCompo].ToString()) : vEntigaGenericaParametros.Id;
                          //  vEntigaGenericaParametros.Codigo = vDataReaderResults[ValoCampo] != DBNull.Value ? Convert.ToString(vDataReaderResults[ValoCampo].ToString()) : vEntigaGenericaParametros.Codigo;
                            vEntigaGenericaParametros.Descripcion = vDataReaderResults[ValoCampo] != DBNull.Value ? Convert.ToString(vDataReaderResults[ValoCampo].ToString()) : vEntigaGenericaParametros.Descripcion;

                            vEntigaGenericaParametros.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntigaGenericaParametros.UsuarioCrea;
                            vEntigaGenericaParametros.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntigaGenericaParametros.FechaCrea;
                            vEntigaGenericaParametros.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntigaGenericaParametros.UsuarioModifica;
                            vEntigaGenericaParametros.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntigaGenericaParametros.FechaModifica;
                            vLista.Add(vEntigaGenericaParametros);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

     
    
    }
}
