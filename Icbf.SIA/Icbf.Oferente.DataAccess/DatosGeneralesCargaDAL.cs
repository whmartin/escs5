﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Oferente.DataAccess
{
    public class DatosGeneralesCargaDAL : GeneralDAL
    {
        /// <summary>
        /// Método para Insertar para la DatosGeneralesCarga.
        /// </summary>
        /// <param name="pDatosGeneralesCarga">Objeto tipo entidad</param>
        /// <returns>Valor Id DatosGeneralesCarga</returns> public int
        public int InsertarDatosGeneralesCarga(DatosGeneralesCarga pDatosGeneralesCarga)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Tercero_DatosGeneralesCarga_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAnalisis", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pDatosGeneralesCarga.Vigencia);
                    vDataBase.AddInParameter(vDbCommand, "@Mes", DbType.String, pDatosGeneralesCarga.Mes);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoArchivo", DbType.Int32, pDatosGeneralesCarga.IdTipoArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pDatosGeneralesCarga.NombreArchivo);
                    vDataBase.AddInParameter(vDbCommand, "@Idusuario", DbType.Int32, pDatosGeneralesCarga.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCarga", DbType.DateTime, pDatosGeneralesCarga.FechaCarga);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoAnalisis", DbType.Int32, pDatosGeneralesCarga.IdEstadoAnalisis);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pDatosGeneralesCarga.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@Version", DbType.Int32, pDatosGeneralesCarga.Version);
                    vDataBase.AddInParameter(vDbCommand, "@CantidadRegistros", DbType.Int32, pDatosGeneralesCarga.CantidadRegistrosExcel);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDatosGeneralesCarga.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDatosGeneralesCarga.IdAnalisis = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAnalisis").ToString());
                    //this.GenerarLogAuditoria(pDatosGeneralesCarga, vDbCommand);

                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar una DatosGeneralesCarga.
        /// </summary>
        /// <param name="pIdDatosGeneralesCarga">Id de la Causal</param>
        /// <returns>Entidad DatosGeneralesCarga</returns>public DatosGeneralesCarga
        public DatosGeneralesCarga ConsultarDatosGeneralesCarga(int pIdDatosGeneralesCarga)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Tercero_DatosGeneralesCarga_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAnalisis", DbType.Int32, pIdDatosGeneralesCarga);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DatosGeneralesCarga vDatosGeneralesCarga = new DatosGeneralesCarga();

                        while (vDataReaderResults.Read())
                        {
                            vDatosGeneralesCarga.IdAnalisis = vDataReaderResults["idAnalisis"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idAnalisis"].ToString()) : vDatosGeneralesCarga.IdAnalisis;
                            vDatosGeneralesCarga.Vigencia = vDataReaderResults["vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["vigencia"].ToString()) : vDatosGeneralesCarga.Vigencia;
                            vDatosGeneralesCarga.Mes = vDataReaderResults["Mes"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Mes"].ToString()) : vDatosGeneralesCarga.Mes;
                            vDatosGeneralesCarga.IdTipoArchivo = vDataReaderResults["idTipoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idTipoArchivo"].ToString()) : vDatosGeneralesCarga.IdTipoArchivo;
                            vDatosGeneralesCarga.NombreArchivo = vDataReaderResults["nombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["nombreArchivo"].ToString()) : vDatosGeneralesCarga.NombreArchivo;
                            vDatosGeneralesCarga.IdUsuario = vDataReaderResults["idusuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idusuario"].ToString()) : vDatosGeneralesCarga.IdUsuario;
                            vDatosGeneralesCarga.FechaCarga = vDataReaderResults["fechaCarga"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fechaCarga"].ToString()) : vDatosGeneralesCarga.FechaCarga;
                            vDatosGeneralesCarga.IdEstadoAnalisis = vDataReaderResults["idEstadoAnalisis"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idEstadoAnalisis"].ToString()) : vDatosGeneralesCarga.IdEstadoAnalisis;
                            vDatosGeneralesCarga.Observaciones = vDataReaderResults["observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["observaciones"].ToString()) : vDatosGeneralesCarga.Observaciones;
                            vDatosGeneralesCarga.Version = vDataReaderResults["version"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["version"].ToString()) : vDatosGeneralesCarga.Version;
                            vDatosGeneralesCarga.CantidadRegistrosExcel = vDataReaderResults["CantidadRegistrosExcel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadRegistrosExcel"].ToString()) : vDatosGeneralesCarga.CantidadRegistrosExcel;
                            vDatosGeneralesCarga.CantidadRegistrosDB = vDataReaderResults["CantidadRegistrosDB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadRegistrosDB"].ToString()) : vDatosGeneralesCarga.CantidadRegistrosDB;
                            vDatosGeneralesCarga.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDatosGeneralesCarga.UsuarioCrea;
                            vDatosGeneralesCarga.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDatosGeneralesCarga.FechaCrea;
                            vDatosGeneralesCarga.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDatosGeneralesCarga.UsuarioModifica;
                            vDatosGeneralesCarga.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDatosGeneralesCarga.FechaModifica;
                        }

                        return vDatosGeneralesCarga;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar datos Generales del cargas.
        /// </summary>
        /// <param name="pVigencia">Valor Vigencia</param>
        /// <param name="pMes">Valor Mes</param>
        /// <param name="pIdTipoArchivo">Valor IdtipoArchivo</param>
        /// <param name="pIdusuario">Valor IdArchivo</param>
        /// <param name="pIdEstadoAnalisis">Valor IdEstadoAnalisis</param>
        /// <param name="pVersion">Valor Version</param>
        /// <returns>Lista de DatosGeneralesCarga</returns>
        public List<DatosGeneralesCarga> ConsultarDatosGeneralesCargas(int? pVigencia, string pMes, int? pIdTipoArchivo, string pNombreArchivo, int? pIdusuario, int? pIdEstadoAnalisis, int? pVersion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Tercero_DatosGeneralesCargas_Consultar"))
                {
                    if (pVigencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Vigencia", DbType.Int32, pVigencia);
                    }

                    if (pMes != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Mes", DbType.String, pMes);
                    }

                    if (pIdTipoArchivo != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoArchivo", DbType.Int32, pIdTipoArchivo);
                    }

                    if (pNombreArchivo != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@NombreArchivo", DbType.String, pNombreArchivo);
                    }

                    if (pIdusuario != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Idusuario", DbType.Int32, pIdusuario);
                    }

                    if (pIdEstadoAnalisis != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdEstadoAnalisis", DbType.Int32, pIdEstadoAnalisis);
                    }

                    if (pVersion != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Version", DbType.Int32, pVersion);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DatosGeneralesCarga> vListaDatosGeneralesCarga = new List<DatosGeneralesCarga>();

                        while (vDataReaderResults.Read())
                        {
                            DatosGeneralesCarga vDatosGeneralesCarga = new DatosGeneralesCarga();
                            vDatosGeneralesCarga.IdAnalisis = vDataReaderResults["idAnalisis"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idAnalisis"].ToString()) : vDatosGeneralesCarga.IdAnalisis;
                            vDatosGeneralesCarga.Vigencia = vDataReaderResults["vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["vigencia"].ToString()) : vDatosGeneralesCarga.Vigencia;
                            vDatosGeneralesCarga.Mes = vDataReaderResults["Mes"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Mes"].ToString()) : vDatosGeneralesCarga.Mes;
                            vDatosGeneralesCarga.IdTipoArchivo = vDataReaderResults["idTipoArchivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idTipoArchivo"].ToString()) : vDatosGeneralesCarga.IdTipoArchivo;
                            vDatosGeneralesCarga.NombreArchivo = vDataReaderResults["nombreArchivo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["nombreArchivo"].ToString()) : vDatosGeneralesCarga.NombreArchivo;
                            vDatosGeneralesCarga.IdUsuario = vDataReaderResults["idusuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idusuario"].ToString()) : vDatosGeneralesCarga.IdUsuario;
                            vDatosGeneralesCarga.FechaCarga = vDataReaderResults["fechaCarga"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fechaCarga"].ToString()) : vDatosGeneralesCarga.FechaCarga;
                            vDatosGeneralesCarga.IdEstadoAnalisis = vDataReaderResults["idEstadoAnalisis"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["idEstadoAnalisis"].ToString()) : vDatosGeneralesCarga.IdEstadoAnalisis;
                            vDatosGeneralesCarga.Observaciones = vDataReaderResults["observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["observaciones"].ToString()) : vDatosGeneralesCarga.Observaciones;
                            vDatosGeneralesCarga.Version = vDataReaderResults["version"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["version"].ToString()) : vDatosGeneralesCarga.Version;
                            vDatosGeneralesCarga.CantidadRegistrosExcel = vDataReaderResults["CantidadRegistrosExcel"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadRegistrosExcel"].ToString()) : vDatosGeneralesCarga.CantidadRegistrosExcel;
                            vDatosGeneralesCarga.CantidadRegistrosDB = vDataReaderResults["CantidadRegistrosDB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CantidadRegistrosDB"].ToString()) : vDatosGeneralesCarga.CantidadRegistrosDB;
                            vDatosGeneralesCarga.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDatosGeneralesCarga.UsuarioCrea;
                            vDatosGeneralesCarga.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDatosGeneralesCarga.FechaCrea;
                            vDatosGeneralesCarga.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDatosGeneralesCarga.UsuarioModifica;
                            vDatosGeneralesCarga.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDatosGeneralesCarga.FechaModifica;
                            vListaDatosGeneralesCarga.Add(vDatosGeneralesCarga);
                        }

                        return vListaDatosGeneralesCarga;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
