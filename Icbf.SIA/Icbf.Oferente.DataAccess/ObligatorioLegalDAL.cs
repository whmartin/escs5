using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class ObligatorioLegalDAL : GeneralDAL
    {
        public ObligatorioLegalDAL()
        {
        }
        public int InsertarObligatorioLegal(ObligatorioLegal pObligatorioLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ObligatorioLegal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdObligatorioLegal", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoObligatorioLegal", DbType.String, pObligatorioLegal.CodigoObligatorioLegal);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pObligatorioLegal.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pObligatorioLegal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pObligatorioLegal.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pObligatorioLegal.IdObligatorioLegal = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdObligatorioLegal").ToString());
                    GenerarLogAuditoria(pObligatorioLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarObligatorioLegal(ObligatorioLegal pObligatorioLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ObligatorioLegal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObligatorioLegal", DbType.Int32, pObligatorioLegal.IdObligatorioLegal);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoObligatorioLegal", DbType.String, pObligatorioLegal.CodigoObligatorioLegal);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pObligatorioLegal.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pObligatorioLegal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pObligatorioLegal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObligatorioLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarObligatorioLegal(ObligatorioLegal pObligatorioLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ObligatorioLegal_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdObligatorioLegal", DbType.Int32, pObligatorioLegal.IdObligatorioLegal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pObligatorioLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public ObligatorioLegal ConsultarObligatorioLegal(int pIdObligatorioLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ObligatorioLegal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdObligatorioLegal", DbType.Int32, pIdObligatorioLegal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ObligatorioLegal vObligatorioLegal = new ObligatorioLegal();
                        while (vDataReaderResults.Read())
                        {
                            vObligatorioLegal.IdObligatorioLegal = vDataReaderResults["IdObligatorioLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObligatorioLegal"].ToString()) : vObligatorioLegal.IdObligatorioLegal;
                            vObligatorioLegal.CodigoObligatorioLegal = vDataReaderResults["CodigoObligatorioLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoObligatorioLegal"].ToString()) : vObligatorioLegal.CodigoObligatorioLegal;
                            vObligatorioLegal.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vObligatorioLegal.Descripcion;
                            vObligatorioLegal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vObligatorioLegal.Estado;
                            vObligatorioLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligatorioLegal.UsuarioCrea;
                            vObligatorioLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligatorioLegal.FechaCrea;
                            vObligatorioLegal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligatorioLegal.UsuarioModifica;
                            vObligatorioLegal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligatorioLegal.FechaModifica;
                        }
                        return vObligatorioLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObligatorioLegal> ConsultarObligatorioLegals(String pCodigoObligatorioLegal, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ObligatorioLegals_Consultar"))
                {
                    if (pCodigoObligatorioLegal != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoObligatorioLegal", DbType.String, pCodigoObligatorioLegal);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ObligatorioLegal> vListaObligatorioLegal = new List<ObligatorioLegal>();
                        while (vDataReaderResults.Read())
                        {
                            ObligatorioLegal vObligatorioLegal = new ObligatorioLegal();
                            vObligatorioLegal.IdObligatorioLegal = vDataReaderResults["IdObligatorioLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdObligatorioLegal"].ToString()) : vObligatorioLegal.IdObligatorioLegal;
                            vObligatorioLegal.CodigoObligatorioLegal = vDataReaderResults["CodigoObligatorioLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoObligatorioLegal"].ToString()) : vObligatorioLegal.CodigoObligatorioLegal;
                            vObligatorioLegal.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vObligatorioLegal.Descripcion;
                            vObligatorioLegal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vObligatorioLegal.Estado;
                            vObligatorioLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vObligatorioLegal.UsuarioCrea;
                            vObligatorioLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vObligatorioLegal.FechaCrea;
                            vObligatorioLegal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vObligatorioLegal.UsuarioModifica;
                            vObligatorioLegal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vObligatorioLegal.FechaModifica;
                            vListaObligatorioLegal.Add(vObligatorioLegal);
                        }
                        return vListaObligatorioLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoObligatorioLegal(String pCodigoObligatorioLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_ObligatorioLegal_Consultar_CodigoObligatorioLegal"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoObligatorioLegal", DbType.String, pCodigoObligatorioLegal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoObligatorioLegal = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoObligatorioLegal = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoObligatorioLegal;
                        }
                        return vExisteCodigoObligatorioLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
