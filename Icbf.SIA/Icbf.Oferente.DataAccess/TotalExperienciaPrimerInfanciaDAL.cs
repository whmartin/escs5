using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TotalExperienciaPrimerInfanciaDAL : GeneralDAL
    {
        public TotalExperienciaPrimerInfanciaDAL()
        {
        }
        public int InsertarTotalExperienciaPrimerInfancia(TotalExperienciaPrimerInfancia pTotalExperienciaPrimerInfancia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TotalExperienciaPrimerInfancia_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTotalExperienciaPrimerInfancia", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTotalExperienciaPrimerInfancia", DbType.String, pTotalExperienciaPrimerInfancia.CodigoTotalExperienciaPrimerInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTotalExperienciaPrimerInfancia", DbType.String, pTotalExperienciaPrimerInfancia.NombreTotalExperienciaPrimerInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTotalExperienciaPrimerInfancia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTotalExperienciaPrimerInfancia.Usuariocrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTotalExperienciaPrimerInfancia").ToString());
                    GenerarLogAuditoria(pTotalExperienciaPrimerInfancia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTotalExperienciaPrimerInfancia(TotalExperienciaPrimerInfancia pTotalExperienciaPrimerInfancia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TotalExperienciaPrimerInfancia_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTotalExperienciaPrimerInfancia", DbType.Int32, pTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTotalExperienciaPrimerInfancia", DbType.String, pTotalExperienciaPrimerInfancia.CodigoTotalExperienciaPrimerInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTotalExperienciaPrimerInfancia", DbType.String, pTotalExperienciaPrimerInfancia.NombreTotalExperienciaPrimerInfancia);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTotalExperienciaPrimerInfancia.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTotalExperienciaPrimerInfancia.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTotalExperienciaPrimerInfancia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTotalExperienciaPrimerInfancia(TotalExperienciaPrimerInfancia pTotalExperienciaPrimerInfancia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TotalExperienciaPrimerInfancia_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTotalExperienciaPrimerInfancia", DbType.Int32, pTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTotalExperienciaPrimerInfancia, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TotalExperienciaPrimerInfancia ConsultarTotalExperienciaPrimerInfancia(int pIdTotalExperienciaPrimerInfancia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TotalExperienciaPrimerInfancia_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTotalExperienciaPrimerInfancia", DbType.Int32, pIdTotalExperienciaPrimerInfancia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TotalExperienciaPrimerInfancia vTotalExperienciaPrimerInfancia = new TotalExperienciaPrimerInfancia();
                        while (vDataReaderResults.Read())
                        {
                            vTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia = vDataReaderResults["IdTotalExperienciaPrimerInfancia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTotalExperienciaPrimerInfancia"].ToString()) : vTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia;
                            vTotalExperienciaPrimerInfancia.CodigoTotalExperienciaPrimerInfancia = vDataReaderResults["CodigoTotalExperienciaPrimerInfancia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTotalExperienciaPrimerInfancia"].ToString()) : vTotalExperienciaPrimerInfancia.CodigoTotalExperienciaPrimerInfancia;
                            vTotalExperienciaPrimerInfancia.NombreTotalExperienciaPrimerInfancia = vDataReaderResults["NombreTotalExperienciaPrimerInfancia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTotalExperienciaPrimerInfancia"].ToString()) : vTotalExperienciaPrimerInfancia.NombreTotalExperienciaPrimerInfancia;
                            vTotalExperienciaPrimerInfancia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTotalExperienciaPrimerInfancia.Estado;
                            vTotalExperienciaPrimerInfancia.Usuariocrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTotalExperienciaPrimerInfancia.Usuariocrea;
                            vTotalExperienciaPrimerInfancia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTotalExperienciaPrimerInfancia.FechaCrea;
                            vTotalExperienciaPrimerInfancia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTotalExperienciaPrimerInfancia.UsuarioModifica;
                            vTotalExperienciaPrimerInfancia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTotalExperienciaPrimerInfancia.FechaModifica;

                            if (vTotalExperienciaPrimerInfancia.Estado == true)
                            {
                                vTotalExperienciaPrimerInfancia.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vTotalExperienciaPrimerInfancia.DescripcionEstado = "Inactivo";
                            }
                        }
                        return vTotalExperienciaPrimerInfancia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TotalExperienciaPrimerInfancia> ConsultarTotalExperienciaPrimerInfancias(String pCodigoTotalExperienciaPrimerInfancia, String pNombreTotalExperienciaPrimerInfancia, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TotalExperienciaPrimerInfancias_Consultar"))
                {
                    if(pCodigoTotalExperienciaPrimerInfancia != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTotalExperienciaPrimerInfancia", DbType.String, pCodigoTotalExperienciaPrimerInfancia);
                    if(pNombreTotalExperienciaPrimerInfancia != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTotalExperienciaPrimerInfancia", DbType.String, pNombreTotalExperienciaPrimerInfancia);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TotalExperienciaPrimerInfancia> vListaTotalExperienciaPrimerInfancia = new List<TotalExperienciaPrimerInfancia>();
                        while (vDataReaderResults.Read())
                        {
                                TotalExperienciaPrimerInfancia vTotalExperienciaPrimerInfancia = new TotalExperienciaPrimerInfancia();
                            vTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia = vDataReaderResults["IdTotalExperienciaPrimerInfancia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTotalExperienciaPrimerInfancia"].ToString()) : vTotalExperienciaPrimerInfancia.IdTotalExperienciaPrimerInfancia;
                            vTotalExperienciaPrimerInfancia.CodigoTotalExperienciaPrimerInfancia = vDataReaderResults["CodigoTotalExperienciaPrimerInfancia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTotalExperienciaPrimerInfancia"].ToString()) : vTotalExperienciaPrimerInfancia.CodigoTotalExperienciaPrimerInfancia;
                            vTotalExperienciaPrimerInfancia.NombreTotalExperienciaPrimerInfancia = vDataReaderResults["NombreTotalExperienciaPrimerInfancia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTotalExperienciaPrimerInfancia"].ToString()) : vTotalExperienciaPrimerInfancia.NombreTotalExperienciaPrimerInfancia;
                            vTotalExperienciaPrimerInfancia.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTotalExperienciaPrimerInfancia.Estado;
                            vTotalExperienciaPrimerInfancia.Usuariocrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTotalExperienciaPrimerInfancia.Usuariocrea;
                            vTotalExperienciaPrimerInfancia.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTotalExperienciaPrimerInfancia.FechaCrea;
                            vTotalExperienciaPrimerInfancia.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTotalExperienciaPrimerInfancia.UsuarioModifica;
                            vTotalExperienciaPrimerInfancia.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTotalExperienciaPrimerInfancia.FechaModifica;

                            if (vTotalExperienciaPrimerInfancia.Estado == true)
                            {
                                vTotalExperienciaPrimerInfancia.DescripcionEstado = "Activo";
                            }
                            else
                            {
                                vTotalExperienciaPrimerInfancia.DescripcionEstado = "Inactivo";
                            }
                            
                            vListaTotalExperienciaPrimerInfancia.Add(vTotalExperienciaPrimerInfancia);
                        }
                        return vListaTotalExperienciaPrimerInfancia;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
