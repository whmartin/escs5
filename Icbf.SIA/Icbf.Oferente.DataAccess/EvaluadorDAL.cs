﻿using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;
using Icbf.Oferente.Entity;
using System.Collections.Generic;

namespace Icbf.Oferente.DataAccess
{
    public class EvaluadorDAL : GeneralDAL
    {
        public EvaluadorDAL() { }

        public int InsertarEvaluador(Evaluador pEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Evaluador_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pEvaluador.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pEvaluador.IdEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObservacion", DbType.Int32, pEvaluador.IdTipoObservacion);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pEvaluador.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEvaluador.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEvaluador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Evaluador ConsultarEvaluador(int pIdUsuario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Evaluador_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pIdUsuario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Evaluador vEvaluador = new Evaluador();
                        while (vDataReaderResults.Read())
                        {
                            vEvaluador.IdEvaluador = vDataReaderResults["IdEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEvaluador"].ToString()) : vEvaluador.IdEvaluador;
                            vEvaluador.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vEvaluador.IdUsuario;
                            vEvaluador.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vEvaluador.NumeroDocumento;
                            vEvaluador.NombreEvaluador = vDataReaderResults["NombreCompleto"] != DBNull.Value ? vDataReaderResults["NombreCompleto"].ToString() : vEvaluador.NombreEvaluador;
                            vEvaluador.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEvaluador.IdEquipoEvaluador;
                            vEvaluador.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vEvaluador.IdTipoObservacion;
                            vEvaluador.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEvaluador.Activo;
                        }
                        return vEvaluador;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Evaluador> ConsultarEvaluadoress(int pIdEquipoEvaluador, bool pActivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Evaluadoress_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pIdEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pActivo);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Evaluador> vListaEvaluadores = new List<Evaluador>();
                        while (vDataReaderResults.Read())
                        {
                            Evaluador vEvaluador = new Evaluador();

                            vEvaluador.IdEvaluador = vDataReaderResults["IdEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEvaluador"].ToString()) : vEvaluador.IdEvaluador;
                            vEvaluador.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vEvaluador.IdUsuario;
                            vEvaluador.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEvaluador.IdEquipoEvaluador;
                            vEvaluador.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEvaluador.Activo;
                            vEvaluador.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vEvaluador.IdTipoObservacion;
                            vEvaluador.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vEvaluador.NumeroDocumento;
                            vEvaluador.NombreEvaluador = vDataReaderResults["NombreCompleto"] != DBNull.Value ? vDataReaderResults["NombreCompleto"].ToString() : vEvaluador.NombreEvaluador;
                            vEvaluador.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? vDataReaderResults["NombreArea"].ToString() : vEvaluador.NombreArea;
                            vEvaluador.DescTipoObservacion = vDataReaderResults["DescripcionTipo"] != DBNull.Value ? vDataReaderResults["DescripcionTipo"].ToString() : vEvaluador.DescTipoObservacion;
                            vListaEvaluadores.Add(vEvaluador);
                        }
                        return vListaEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarEvaluadoress(Evaluador pEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Evaluadores_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pEvaluador.IdEquipoEvaluador);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEvaluador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarEvaluador(Evaluador pEvaluador)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Evaluador_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEvaluador", DbType.Int32, pEvaluador.IdEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@IdUsuario", DbType.Int32, pEvaluador.IdUsuario);
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pEvaluador.IdEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObservacion", DbType.Int32, pEvaluador.IdTipoObservacion);
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pEvaluador.Activo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEvaluador.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEvaluador, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Evaluador> ConsultarEvaluadoressEstado(bool pActivo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_EvaluadoressEstado_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Activo", DbType.Boolean, pActivo);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Evaluador> vListaEvaluadores = new List<Evaluador>();
                        while (vDataReaderResults.Read())
                        {
                            Evaluador vEvaluador = new Evaluador();

                            vEvaluador.IdEvaluador = vDataReaderResults["IdEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEvaluador"].ToString()) : vEvaluador.IdEvaluador;
                            vEvaluador.IdUsuario = vDataReaderResults["IdUsuario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdUsuario"].ToString()) : vEvaluador.IdUsuario;
                            vEvaluador.IdEquipoEvaluador = vDataReaderResults["IdEquipoEvaluador"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEquipoEvaluador"].ToString()) : vEvaluador.IdEquipoEvaluador;
                            vEvaluador.Activo = vDataReaderResults["Activo"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Activo"].ToString()) : vEvaluador.Activo;
                            vEvaluador.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vEvaluador.IdTipoObservacion;
                            vEvaluador.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? vDataReaderResults["NumeroDocumento"].ToString() : vEvaluador.NumeroDocumento;
                            vEvaluador.NombreEvaluador = vDataReaderResults["NombreCompleto"] != DBNull.Value ? vDataReaderResults["NombreCompleto"].ToString() : vEvaluador.NombreEvaluador;
                            vEvaluador.NombreArea = vDataReaderResults["NombreArea"] != DBNull.Value ? vDataReaderResults["NombreArea"].ToString() : vEvaluador.NombreArea;
                            vEvaluador.DescTipoObservacion = vDataReaderResults["DescripcionTipo"] != DBNull.Value ? vDataReaderResults["DescripcionTipo"].ToString() : vEvaluador.DescTipoObservacion;
                            vListaEvaluadores.Add(vEvaluador);
                        }
                        return vListaEvaluadores;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        public Evaluador ConsultarEvaluadorDisponibleParaAsignarObservacion(int pIdEquipoEvaluador, int pIdTipoObservacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_Evaluadores_EvaluadorDisponible_Consultar"))
                {
                    vDataBase.AddOutParameter(vDbCommand, "@IdEvaluadorCandidato", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEquipoEvaluador", DbType.Int32, pIdEquipoEvaluador);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoObs", DbType.Int32, pIdTipoObservacion);

                    var vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    Evaluador vEvaluador = new Evaluador();
                    vEvaluador.IdEvaluador = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEvaluadorCandidato").ToString());
                       
                    return vEvaluador;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
