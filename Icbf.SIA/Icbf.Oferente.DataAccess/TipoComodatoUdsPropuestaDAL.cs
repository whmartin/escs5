using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TipoComodatoUdsPropuestaDAL : GeneralDAL
    {
        public TipoComodatoUdsPropuestaDAL()
        {
        }
        public int InsertarTipoComodatoUdsPropuesta(TipoComodatoUdsPropuesta pTipoComodatoUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoComodatoUdsPropuesta_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoComodatoUdsPropuesta", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoComodatoUdsPropuesta", DbType.String, pTipoComodatoUdsPropuesta.CodigoTipoComodatoUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoComodatoUdsPropuesta", DbType.String, pTipoComodatoUdsPropuesta.NombreTipoComodatoUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoComodatoUdsPropuesta.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoComodatoUdsPropuesta.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoComodatoUdsPropuesta").ToString());
                    GenerarLogAuditoria(pTipoComodatoUdsPropuesta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoComodatoUdsPropuesta(TipoComodatoUdsPropuesta pTipoComodatoUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoComodatoUdsPropuesta_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoComodatoUdsPropuesta", DbType.Int32, pTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoComodatoUdsPropuesta", DbType.String, pTipoComodatoUdsPropuesta.CodigoTipoComodatoUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoComodatoUdsPropuesta", DbType.String, pTipoComodatoUdsPropuesta.NombreTipoComodatoUdsPropuesta);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoComodatoUdsPropuesta.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoComodatoUdsPropuesta.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoComodatoUdsPropuesta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoComodatoUdsPropuesta(TipoComodatoUdsPropuesta pTipoComodatoUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoComodatoUdsPropuesta_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoComodatoUdsPropuesta", DbType.Int32, pTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoComodatoUdsPropuesta, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TipoComodatoUdsPropuesta ConsultarTipoComodatoUdsPropuesta(int pIdTipoComodatoUdsPropuesta)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoComodatoUdsPropuesta_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoComodatoUdsPropuesta", DbType.Int32, pIdTipoComodatoUdsPropuesta);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoComodatoUdsPropuesta vTipoComodatoUdsPropuesta = new TipoComodatoUdsPropuesta();
                        while (vDataReaderResults.Read())
                        {
                            vTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta = vDataReaderResults["IdTipoComodatoUdsPropuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoComodatoUdsPropuesta"].ToString()) : vTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta;
                            vTipoComodatoUdsPropuesta.CodigoTipoComodatoUdsPropuesta = vDataReaderResults["CodigoTipoComodatoUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoComodatoUdsPropuesta"].ToString()) : vTipoComodatoUdsPropuesta.CodigoTipoComodatoUdsPropuesta;
                            vTipoComodatoUdsPropuesta.NombreTipoComodatoUdsPropuesta = vDataReaderResults["NombreTipoComodatoUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoComodatoUdsPropuesta"].ToString()) : vTipoComodatoUdsPropuesta.NombreTipoComodatoUdsPropuesta;
                            vTipoComodatoUdsPropuesta.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoComodatoUdsPropuesta.Estado;
                            vTipoComodatoUdsPropuesta.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoComodatoUdsPropuesta.UsuarioCrea;
                            vTipoComodatoUdsPropuesta.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoComodatoUdsPropuesta.FechaCrea;
                            vTipoComodatoUdsPropuesta.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoComodatoUdsPropuesta.UsuarioModifica;
                            vTipoComodatoUdsPropuesta.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoComodatoUdsPropuesta.FechaModifica;
                        }
                        return vTipoComodatoUdsPropuesta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoComodatoUdsPropuesta> ConsultarTipoComodatoUdsPropuestas(String pCodigoTipoComodatoUdsPropuesta, String pNombreTipoComodatoUdsPropuesta, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoComodatoUdsPropuestas_Consultar"))
                {
                    if(pCodigoTipoComodatoUdsPropuesta != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipoComodatoUdsPropuesta", DbType.String, pCodigoTipoComodatoUdsPropuesta);
                    if(pNombreTipoComodatoUdsPropuesta != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoComodatoUdsPropuesta", DbType.String, pNombreTipoComodatoUdsPropuesta);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoComodatoUdsPropuesta> vListaTipoComodatoUdsPropuesta = new List<TipoComodatoUdsPropuesta>();
                        while (vDataReaderResults.Read())
                        {
                                TipoComodatoUdsPropuesta vTipoComodatoUdsPropuesta = new TipoComodatoUdsPropuesta();
                            vTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta = vDataReaderResults["IdTipoComodatoUdsPropuesta"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoComodatoUdsPropuesta"].ToString()) : vTipoComodatoUdsPropuesta.IdTipoComodatoUdsPropuesta;
                            vTipoComodatoUdsPropuesta.CodigoTipoComodatoUdsPropuesta = vDataReaderResults["CodigoTipoComodatoUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoComodatoUdsPropuesta"].ToString()) : vTipoComodatoUdsPropuesta.CodigoTipoComodatoUdsPropuesta;
                            vTipoComodatoUdsPropuesta.NombreTipoComodatoUdsPropuesta = vDataReaderResults["NombreTipoComodatoUdsPropuesta"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoComodatoUdsPropuesta"].ToString()) : vTipoComodatoUdsPropuesta.NombreTipoComodatoUdsPropuesta;
                            vTipoComodatoUdsPropuesta.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoComodatoUdsPropuesta.Estado;
                            if (vTipoComodatoUdsPropuesta.Estado == true)
                                vTipoComodatoUdsPropuesta.DescripcionEstado = "Activo";
                            else
                                vTipoComodatoUdsPropuesta.DescripcionEstado = "Inactivo";
                            vTipoComodatoUdsPropuesta.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoComodatoUdsPropuesta.UsuarioCrea;
                            vTipoComodatoUdsPropuesta.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoComodatoUdsPropuesta.FechaCrea;
                            vTipoComodatoUdsPropuesta.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoComodatoUdsPropuesta.UsuarioModifica;
                            vTipoComodatoUdsPropuesta.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoComodatoUdsPropuesta.FechaModifica;
                                vListaTipoComodatoUdsPropuesta.Add(vTipoComodatoUdsPropuesta);
                        }
                        return vListaTipoComodatoUdsPropuesta;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
