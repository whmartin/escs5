﻿using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.DataAccess
{
    public class TipoObservacionesDAL : GeneralDAL
    {
        public TipoObservacionesDAL() { }

        public List<TipoObservaciones> ConsultarTiposObservacioness(bool pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Oferente_TiposObservacioness_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoObservaciones> vListaTipoObservaciones = new List<TipoObservaciones>();
                        while (vDataReaderResults.Read())
                        {
                            TipoObservaciones vTipoObservaciones = new TipoObservaciones();

                            vTipoObservaciones.IdTipoObservacion = vDataReaderResults["IdTipoObservacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoObservacion"].ToString()) : vTipoObservaciones.IdTipoObservacion;
                            vTipoObservaciones.CodigoTipo = vDataReaderResults["CodigoTipo"] != DBNull.Value ? vDataReaderResults["CodigoTipo"].ToString() : vTipoObservaciones.CodigoTipo;
                            vTipoObservaciones.DescripcionTipo = vDataReaderResults["DescripcionTipo"] != DBNull.Value ? vDataReaderResults["DescripcionTipo"].ToString() : vTipoObservaciones.DescripcionTipo;

                            vListaTipoObservaciones.Add(vTipoObservaciones);
                        }
                        return vListaTipoObservaciones;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
