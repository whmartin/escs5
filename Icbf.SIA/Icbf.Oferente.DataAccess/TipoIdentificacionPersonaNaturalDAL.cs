using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class TipoIdentificacionPersonaNaturalDAL : GeneralDAL
    {
        public TipoIdentificacionPersonaNaturalDAL()
        {
        }
        public int InsertarTipoIdentificacionPersonaNatural(TipoIdentificacionPersonaNatural pTipoIdentificacionPersonaNatural)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoIdentificacionPersonaNatural_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoIdentificacionPersonaNatural", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoIdentificacionPersonaNatural", DbType.String, pTipoIdentificacionPersonaNatural.CodigoTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoIdentificacionPersonaNatural", DbType.String, pTipoIdentificacionPersonaNatural.NombreTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoIdentificacionPersonaNatural.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoIdentificacionPersonaNatural.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoIdentificacionPersonaNatural").ToString());
                    GenerarLogAuditoria(pTipoIdentificacionPersonaNatural, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoIdentificacionPersonaNatural(TipoIdentificacionPersonaNatural pTipoIdentificacionPersonaNatural)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoIdentificacionPersonaNatural_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionPersonaNatural", DbType.Int32, pTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoIdentificacionPersonaNatural", DbType.String, pTipoIdentificacionPersonaNatural.CodigoTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@NombreTipoIdentificacionPersonaNatural", DbType.String, pTipoIdentificacionPersonaNatural.NombreTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoIdentificacionPersonaNatural.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoIdentificacionPersonaNatural.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoIdentificacionPersonaNatural, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoIdentificacionPersonaNatural(TipoIdentificacionPersonaNatural pTipoIdentificacionPersonaNatural)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoIdentificacionPersonaNatural_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionPersonaNatural", DbType.Int32, pTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoIdentificacionPersonaNatural, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public TipoIdentificacionPersonaNatural ConsultarTipoIdentificacionPersonaNatural(int pIdTipoIdentificacionPersonaNatural)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoIdentificacionPersonaNatural_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionPersonaNatural", DbType.Int32, pIdTipoIdentificacionPersonaNatural);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoIdentificacionPersonaNatural vTipoIdentificacionPersonaNatural = new TipoIdentificacionPersonaNatural();
                        while (vDataReaderResults.Read())
                        {
                            vTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural = vDataReaderResults["IdTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural;
                            vTipoIdentificacionPersonaNatural.CodigoTipoIdentificacionPersonaNatural = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacionPersonaNatural.CodigoTipoIdentificacionPersonaNatural;
                            vTipoIdentificacionPersonaNatural.NombreTipoIdentificacionPersonaNatural = vDataReaderResults["NombreTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacionPersonaNatural.NombreTipoIdentificacionPersonaNatural;
                            vTipoIdentificacionPersonaNatural.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoIdentificacionPersonaNatural.Estado;
                            vTipoIdentificacionPersonaNatural.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoIdentificacionPersonaNatural.UsuarioCrea;
                            vTipoIdentificacionPersonaNatural.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoIdentificacionPersonaNatural.FechaCrea;
                            vTipoIdentificacionPersonaNatural.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoIdentificacionPersonaNatural.UsuarioModifica;
                            vTipoIdentificacionPersonaNatural.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoIdentificacionPersonaNatural.FechaModifica;
                        }
                        return vTipoIdentificacionPersonaNatural;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoIdentificacionPersonaNatural> ConsultarTipoIdentificacionPersonaNaturals(String pCodigoTipoIdentificacionPersonaNatural, String pNombreTipoIdentificacionPersonaNatural, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_TipoIdentificacionPersonaNaturals_Consultar"))
                {
                    if(pCodigoTipoIdentificacionPersonaNatural != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipoIdentificacionPersonaNatural", DbType.String, pCodigoTipoIdentificacionPersonaNatural);
                    if(pNombreTipoIdentificacionPersonaNatural != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreTipoIdentificacionPersonaNatural", DbType.String, pNombreTipoIdentificacionPersonaNatural);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoIdentificacionPersonaNatural> vListaTipoIdentificacionPersonaNatural = new List<TipoIdentificacionPersonaNatural>();
                        while (vDataReaderResults.Read())
                        {
                                TipoIdentificacionPersonaNatural vTipoIdentificacionPersonaNatural = new TipoIdentificacionPersonaNatural();
                            vTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural = vDataReaderResults["IdTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacionPersonaNatural.IdTipoIdentificacionPersonaNatural;
                            vTipoIdentificacionPersonaNatural.CodigoTipoIdentificacionPersonaNatural = vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacionPersonaNatural.CodigoTipoIdentificacionPersonaNatural;
                            vTipoIdentificacionPersonaNatural.NombreTipoIdentificacionPersonaNatural = vDataReaderResults["NombreTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoIdentificacionPersonaNatural"].ToString()) : vTipoIdentificacionPersonaNatural.NombreTipoIdentificacionPersonaNatural;
                            vTipoIdentificacionPersonaNatural.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoIdentificacionPersonaNatural.Estado;
                            if (vTipoIdentificacionPersonaNatural.Estado == true)
                            {
                                vTipoIdentificacionPersonaNatural.DescripcionEstado = "Activo";
                            }
                            else 
                            {
                                vTipoIdentificacionPersonaNatural.DescripcionEstado = "Inactivo";
                            }
                            vTipoIdentificacionPersonaNatural.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoIdentificacionPersonaNatural.UsuarioCrea;
                            vTipoIdentificacionPersonaNatural.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoIdentificacionPersonaNatural.FechaCrea;
                            vTipoIdentificacionPersonaNatural.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoIdentificacionPersonaNatural.UsuarioModifica;
                            vTipoIdentificacionPersonaNatural.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoIdentificacionPersonaNatural.FechaModifica;
                                vListaTipoIdentificacionPersonaNatural.Add(vTipoIdentificacionPersonaNatural);
                        }
                        return vListaTipoIdentificacionPersonaNatural;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
