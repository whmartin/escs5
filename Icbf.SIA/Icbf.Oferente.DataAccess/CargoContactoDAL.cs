using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase CargoContactoDAL
    /// </summary>
    public class CargoContactoDAL : GeneralDAL
    {
        public CargoContactoDAL()
        {
        }

        /// <summary>
        /// Insertar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int InsertarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargoContacto_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdCargoContacto", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCargoContacto", DbType.String, pCargoContacto.CodigoCargoContacto);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCargoContacto.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pCargoContacto.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCargoContacto.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCargoContacto.IdCargoContacto = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdCargoContacto").ToString());
                    GenerarLogAuditoria(pCargoContacto, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int ModificarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargoContacto_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoContacto", DbType.Int32, pCargoContacto.IdCargoContacto);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCargoContacto", DbType.String, pCargoContacto.CodigoCargoContacto);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pCargoContacto.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pCargoContacto.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pCargoContacto.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCargoContacto, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar Cargo Contacto
        /// </summary>
        /// <param name="pCargoContacto"></param>
        /// <returns></returns>
        public int EliminarCargoContacto(CargoContacto pCargoContacto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargoContacto_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoContacto", DbType.Int32, pCargoContacto.IdCargoContacto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCargoContacto, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Cargo Contacto
        /// </summary>
        /// <param name="pIdCargoContacto"></param>
        /// <returns></returns>
        public CargoContacto ConsultarCargoContacto(int pIdCargoContacto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargoContacto_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdCargoContacto", DbType.Int32, pIdCargoContacto);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        CargoContacto vCargoContacto = new CargoContacto();
                        while (vDataReaderResults.Read())
                        {
                            vCargoContacto.IdCargoContacto = vDataReaderResults["IdCargoContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoContacto"].ToString()) : vCargoContacto.IdCargoContacto;
                            vCargoContacto.CodigoCargoContacto = vDataReaderResults["CodigoCargoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCargoContacto"].ToString()) : vCargoContacto.CodigoCargoContacto;
                            vCargoContacto.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCargoContacto.Descripcion;
                            vCargoContacto.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vCargoContacto.Estado;
                            vCargoContacto.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCargoContacto.UsuarioCrea;
                            vCargoContacto.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCargoContacto.FechaCrea;
                            vCargoContacto.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCargoContacto.UsuarioModifica;
                            vCargoContacto.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCargoContacto.FechaModifica;
                        }
                        return vCargoContacto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista Cargo Contactos
        /// </summary>
        /// <param name="pCodigoCargoContacto"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<CargoContacto> ConsultarCargoContactos(String pCodigoCargoContacto, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargoContactos_Consultar"))
                {
                    if(pCodigoCargoContacto != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoCargoContacto", DbType.String, pCodigoCargoContacto);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CargoContacto> vListaCargoContacto = new List<CargoContacto>();
                        while (vDataReaderResults.Read())
                        {
                                CargoContacto vCargoContacto = new CargoContacto();
                            vCargoContacto.IdCargoContacto = vDataReaderResults["IdCargoContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCargoContacto"].ToString()) : vCargoContacto.IdCargoContacto;
                            vCargoContacto.CodigoCargoContacto = vDataReaderResults["CodigoCargoContacto"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCargoContacto"].ToString()) : vCargoContacto.CodigoCargoContacto;
                            vCargoContacto.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCargoContacto.Descripcion;
                            vCargoContacto.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vCargoContacto.Estado;
                            vCargoContacto.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vCargoContacto.UsuarioCrea;
                            vCargoContacto.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vCargoContacto.FechaCrea;
                            vCargoContacto.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vCargoContacto.UsuarioModifica;
                            vCargoContacto.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vCargoContacto.FechaModifica;
                                vListaCargoContacto.Add(vCargoContacto);
                        }
                        return vListaCargoContacto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Codigo Cargo Contacto
        /// </summary>
        /// <param name="pCodigoCargoContacto"></param>
        /// <returns></returns>
        public int ConsultarCodigoCargoContacto(String pCodigoCargoContacto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_CargoContacto_Consultar_CargoContacto"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCargoContacto", DbType.String, pCodigoCargoContacto);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoCargoContacto = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoCargoContacto = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoCargoContacto;
                        }
                        return vExisteCodigoCargoContacto;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
