using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class FechaConvocatoriaDAL : GeneralDAL
    {
        public FechaConvocatoriaDAL()
        {
        }
        public int InsertarFechaConvocatoria(FechaConvocatoria pFechaConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FechaConvocatoria_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFechaConvocatoria", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaDepartamento", DbType.Int32, pFechaConvocatoria.IdDListaDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncio", DbType.DateTime, pFechaConvocatoria.FechaIncio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pFechaConvocatoria.FechaTerminacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFechaConvocatoria.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFechaConvocatoria.IdFechaConvocatoria = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFechaConvocatoria").ToString());
                    GenerarLogAuditoria(pFechaConvocatoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarFechaConvocatoria(FechaConvocatoria pFechaConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FechaConvocatoria_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFechaConvocatoria", DbType.Int32, pFechaConvocatoria.IdFechaConvocatoria);
                    vDataBase.AddInParameter(vDbCommand, "@IdDListaDepartamento", DbType.Int32, pFechaConvocatoria.IdDListaDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@FechaIncio", DbType.DateTime, pFechaConvocatoria.FechaIncio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pFechaConvocatoria.FechaTerminacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFechaConvocatoria.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFechaConvocatoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarFechaConvocatoria(FechaConvocatoria pFechaConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FechaConvocatoria_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFechaConvocatoria", DbType.Int32, pFechaConvocatoria.IdFechaConvocatoria);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFechaConvocatoria, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public FechaConvocatoria ConsultarFechaConvocatoria(int pIdFechaConvocatoria)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FechaConvocatoria_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFechaConvocatoria", DbType.Int32, pIdFechaConvocatoria);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        FechaConvocatoria vFechaConvocatoria = new FechaConvocatoria();
                        while (vDataReaderResults.Read())
                        {
                            vFechaConvocatoria.IdFechaConvocatoria = vDataReaderResults["IdFechaConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFechaConvocatoria"].ToString()) : vFechaConvocatoria.IdFechaConvocatoria;
                            vFechaConvocatoria.IdDListaDepartamento = vDataReaderResults["IdDListaDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDListaDepartamento"].ToString()) : vFechaConvocatoria.IdDListaDepartamento;
                            vFechaConvocatoria.FechaIncio = vDataReaderResults["FechaIncio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncio"].ToString()) : vFechaConvocatoria.FechaIncio;
                            vFechaConvocatoria.FechaTerminacion = vDataReaderResults["FechaTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacion"].ToString()) : vFechaConvocatoria.FechaTerminacion;
                            vFechaConvocatoria.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFechaConvocatoria.UsuarioCrea;
                            vFechaConvocatoria.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFechaConvocatoria.FechaCrea;
                            vFechaConvocatoria.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFechaConvocatoria.UsuarioModifica;
                            vFechaConvocatoria.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFechaConvocatoria.FechaModifica;
                        }
                        return vFechaConvocatoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FechaConvocatoria> ConsultarFechaConvocatorias(DateTime? pFechaIncio, DateTime? pFechaTerminacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_FechaConvocatorias_Consultar"))
                {
                    if(pFechaIncio != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaIncio", DbType.DateTime, pFechaIncio);
                    if(pFechaTerminacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@FechaTerminacion", DbType.DateTime, pFechaTerminacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<FechaConvocatoria> vListaFechaConvocatoria = new List<FechaConvocatoria>();
                        while (vDataReaderResults.Read())
                        {
                                FechaConvocatoria vFechaConvocatoria = new FechaConvocatoria();
                            vFechaConvocatoria.IdFechaConvocatoria = vDataReaderResults["IdFechaConvocatoria"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFechaConvocatoria"].ToString()) : vFechaConvocatoria.IdFechaConvocatoria;
                            vFechaConvocatoria.IdDListaDepartamento = vDataReaderResults["IdDListaDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDListaDepartamento"].ToString()) : vFechaConvocatoria.IdDListaDepartamento;
                            vFechaConvocatoria.FechaIncio = vDataReaderResults["FechaIncio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIncio"].ToString()) : vFechaConvocatoria.FechaIncio;
                            vFechaConvocatoria.FechaTerminacion = vDataReaderResults["FechaTerminacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaTerminacion"].ToString()) : vFechaConvocatoria.FechaTerminacion;
                            vFechaConvocatoria.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFechaConvocatoria.UsuarioCrea;
                            vFechaConvocatoria.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFechaConvocatoria.FechaCrea;
                            vFechaConvocatoria.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFechaConvocatoria.UsuarioModifica;
                            vFechaConvocatoria.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFechaConvocatoria.FechaModifica;
                                vListaFechaConvocatoria.Add(vFechaConvocatoria);
                        }
                        return vListaFechaConvocatoria;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
