using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase PoblacionAtendidaDAL
    /// </summary>
    public class PoblacionAtendidaDAL : GeneralDAL
    {
        public PoblacionAtendidaDAL()
        {
        }
        /// <summary>
        /// Insertar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int InsertarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PoblacionAtendida_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPoblacionAtendida", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@PoblacionAtendida", DbType.String, pPoblacionAtendida.CodigoPoblacionAtendida);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pPoblacionAtendida.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pPoblacionAtendida.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pPoblacionAtendida.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPoblacionAtendida.IdPoblacionAtendida = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPoblacionAtendida").ToString());
                    GenerarLogAuditoria(pPoblacionAtendida, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modificar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int ModificarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PoblacionAtendida_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPoblacionAtendida", DbType.Int32, pPoblacionAtendida.IdPoblacionAtendida);
                    vDataBase.AddInParameter(vDbCommand, "@PoblacionAtendida", DbType.String, pPoblacionAtendida.CodigoPoblacionAtendida);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pPoblacionAtendida.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pPoblacionAtendida.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pPoblacionAtendida.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPoblacionAtendida, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        ///  Elimina rPoblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int EliminarPoblacionAtendida(PoblacionAtendida pPoblacionAtendida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PoblacionAtendida_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPoblacionAtendida", DbType.Int32, pPoblacionAtendida.IdPoblacionAtendida);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPoblacionAtendida, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Poblacion Atendida
        /// </summary>
        /// <param name="pIdPoblacionAtendida"></param>
        /// <returns></returns>
        public PoblacionAtendida ConsultarPoblacionAtendida(int pIdPoblacionAtendida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PoblacionAtendida_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPoblacionAtendida", DbType.Int32, pIdPoblacionAtendida);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        PoblacionAtendida vPoblacionAtendida = new PoblacionAtendida();
                        while (vDataReaderResults.Read())
                        {
                            vPoblacionAtendida.IdPoblacionAtendida = vDataReaderResults["IdPoblacionAtendida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPoblacionAtendida"].ToString()) : vPoblacionAtendida.IdPoblacionAtendida;
                            vPoblacionAtendida.CodigoPoblacionAtendida = vDataReaderResults["PoblacionAtendida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PoblacionAtendida"].ToString()) : vPoblacionAtendida.CodigoPoblacionAtendida;
                            vPoblacionAtendida.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vPoblacionAtendida.Descripcion;
                            vPoblacionAtendida.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vPoblacionAtendida.Estado;
                            vPoblacionAtendida.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPoblacionAtendida.UsuarioCrea;
                            vPoblacionAtendida.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPoblacionAtendida.FechaCrea;
                            vPoblacionAtendida.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPoblacionAtendida.UsuarioModifica;
                            vPoblacionAtendida.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPoblacionAtendida.FechaModifica;
                        }
                        return vPoblacionAtendida;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de Poblaciones Atendidas
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<PoblacionAtendida> ConsultarPoblacionAtendidas(String pPoblacionAtendida, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PoblacionAtendidas_Consultar"))
                {
                    if(pPoblacionAtendida != null)
                         vDataBase.AddInParameter(vDbCommand, "@PoblacionAtendida", DbType.String, pPoblacionAtendida);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<PoblacionAtendida> vListaPoblacionAtendida = new List<PoblacionAtendida>();
                        while (vDataReaderResults.Read())
                        {
                                PoblacionAtendida vPoblacionAtendida = new PoblacionAtendida();
                            vPoblacionAtendida.IdPoblacionAtendida = vDataReaderResults["IdPoblacionAtendida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPoblacionAtendida"].ToString()) : vPoblacionAtendida.IdPoblacionAtendida;
                            vPoblacionAtendida.CodigoPoblacionAtendida = vDataReaderResults["PoblacionAtendida"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PoblacionAtendida"].ToString()) : vPoblacionAtendida.CodigoPoblacionAtendida;
                            vPoblacionAtendida.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vPoblacionAtendida.Descripcion;
                            vPoblacionAtendida.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vPoblacionAtendida.Estado;
                            vPoblacionAtendida.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vPoblacionAtendida.UsuarioCrea;
                            vPoblacionAtendida.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vPoblacionAtendida.FechaCrea;
                            vPoblacionAtendida.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vPoblacionAtendida.UsuarioModifica;
                            vPoblacionAtendida.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vPoblacionAtendida.FechaModifica;
                                vListaPoblacionAtendida.Add(vPoblacionAtendida);
                        }
                        return vListaPoblacionAtendida;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Poblacion Atendida
        /// </summary>
        /// <param name="pPoblacionAtendida"></param>
        /// <returns></returns>
        public int ConsultarPoblacionAtendida(String pPoblacionAtendida)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_PoblacionAtendida_Consultar_PoblacionAtendida"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@PoblacionAtendida", DbType.String, pPoblacionAtendida);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExistePoblacionAtendida = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExistePoblacionAtendida = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExistePoblacionAtendida;
                        }
                        return vExistePoblacionAtendida;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
