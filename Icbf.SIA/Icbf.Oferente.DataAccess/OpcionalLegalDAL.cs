using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    public class OpcionalLegalDAL : GeneralDAL
    {
        public OpcionalLegalDAL()
        {
        }
        public int InsertarOpcionalLegal(OpcionalLegal pOpcionalLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OpcionalLegal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdOpcionalLegal", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoOpcionalLegal", DbType.String, pOpcionalLegal.CodigoOpcionalLegal);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pOpcionalLegal.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pOpcionalLegal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pOpcionalLegal.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pOpcionalLegal.IdOpcionalLegal = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdOpcionalLegal").ToString());
                    GenerarLogAuditoria(pOpcionalLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarOpcionalLegal(OpcionalLegal pOpcionalLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OpcionalLegal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdOpcionalLegal", DbType.Int32, pOpcionalLegal.IdOpcionalLegal);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoOpcionalLegal", DbType.String, pOpcionalLegal.CodigoOpcionalLegal);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pOpcionalLegal.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pOpcionalLegal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pOpcionalLegal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pOpcionalLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarOpcionalLegal(OpcionalLegal pOpcionalLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OpcionalLegal_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdOpcionalLegal", DbType.Int32, pOpcionalLegal.IdOpcionalLegal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pOpcionalLegal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public OpcionalLegal ConsultarOpcionalLegal(int pIdOpcionalLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OpcionalLegal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdOpcionalLegal", DbType.Int32, pIdOpcionalLegal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        OpcionalLegal vOpcionalLegal = new OpcionalLegal();
                        while (vDataReaderResults.Read())
                        {
                            vOpcionalLegal.IdOpcionalLegal = vDataReaderResults["IdOpcionalLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOpcionalLegal"].ToString()) : vOpcionalLegal.IdOpcionalLegal;
                            vOpcionalLegal.CodigoOpcionalLegal = vDataReaderResults["CodigoOpcionalLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoOpcionalLegal"].ToString()) : vOpcionalLegal.CodigoOpcionalLegal;
                            vOpcionalLegal.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vOpcionalLegal.Descripcion;
                            vOpcionalLegal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vOpcionalLegal.Estado;
                            vOpcionalLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vOpcionalLegal.UsuarioCrea;
                            vOpcionalLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vOpcionalLegal.FechaCrea;
                            vOpcionalLegal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vOpcionalLegal.UsuarioModifica;
                            vOpcionalLegal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vOpcionalLegal.FechaModifica;
                        }
                        return vOpcionalLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<OpcionalLegal> ConsultarOpcionalLegals(String pCodigoOpcionalLegal, String pDescripcion, String pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OpcionalLegals_Consultar"))
                {
                    if (pCodigoOpcionalLegal != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoOpcionalLegal", DbType.String, pCodigoOpcionalLegal);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.String, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<OpcionalLegal> vListaOpcionalLegal = new List<OpcionalLegal>();
                        while (vDataReaderResults.Read())
                        {
                            OpcionalLegal vOpcionalLegal = new OpcionalLegal();
                            vOpcionalLegal.IdOpcionalLegal = vDataReaderResults["IdOpcionalLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdOpcionalLegal"].ToString()) : vOpcionalLegal.IdOpcionalLegal;
                            vOpcionalLegal.CodigoOpcionalLegal = vDataReaderResults["CodigoOpcionalLegal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoOpcionalLegal"].ToString()) : vOpcionalLegal.CodigoOpcionalLegal;
                            vOpcionalLegal.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vOpcionalLegal.Descripcion;
                            vOpcionalLegal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vOpcionalLegal.Estado;
                            vOpcionalLegal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vOpcionalLegal.UsuarioCrea;
                            vOpcionalLegal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vOpcionalLegal.FechaCrea;
                            vOpcionalLegal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vOpcionalLegal.UsuarioModifica;
                            vOpcionalLegal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vOpcionalLegal.FechaModifica;
                            vListaOpcionalLegal.Add(vOpcionalLegal);
                        }
                        return vListaOpcionalLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarCodigoOpcionalLegal(String pCodigoOpcionalLegal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_OpcionalLegal_Consultar_CodigoOpcionalLegal"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@CodigoOpcionalLegal", DbType.String, pCodigoOpcionalLegal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        int vExisteCodigoOpcionalLegal = 0;
                        while (vDataReaderResults.Read())
                        {
                            vExisteCodigoOpcionalLegal = vDataReaderResults["Count"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Count"].ToString()) : vExisteCodigoOpcionalLegal;
                        }
                        return vExisteCodigoOpcionalLegal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
