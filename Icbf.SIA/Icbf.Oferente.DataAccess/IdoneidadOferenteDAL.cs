using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Oferente.DataAccess
{
    /// <summary>
    /// Definición de clase IdoneidadOferenteDAL
    /// </summary>
    public class IdoneidadOferenteDAL : GeneralDAL
    {
        public IdoneidadOferenteDAL()
        {
        }
       /// <summary>
        /// Consultar Idoneidad Oferente
       /// </summary>
       /// <param name="IdTercero"></param>
       /// <returns></returns>
        public List<IdoneidadOferente>  ConsultarIdoneidadOferente(int IdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                List<IdoneidadOferente> vListaIdoneidadOferente = new List<IdoneidadOferente>();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_IdoneidadOferente_Consultar"))
                {
                   

                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, IdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        while (vDataReaderResults.Read())
                        {
                            IdoneidadOferente vIdoneidadOferente = new IdoneidadOferente();
                            vIdoneidadOferente.Idtercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vIdoneidadOferente.Idtercero;
                            vIdoneidadOferente.IdIdentidad = vDataReaderResults["IDIDENTIDAD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDIDENTIDAD"].ToString()) : vIdoneidadOferente.IdIdentidad;
                            vIdoneidadOferente.TipoPersona = vDataReaderResults["TIPOPERSONA"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TIPOPERSONA"].ToString()) : vIdoneidadOferente.TipoPersona;
                            vIdoneidadOferente.TipoIdentificacion = vDataReaderResults["TIPODOCUMENTO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TIPODOCUMENTO"].ToString()) : vIdoneidadOferente.TipoIdentificacion;
                            vIdoneidadOferente.NoIdentificacion = vDataReaderResults["NoIDENTIFICACION"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["NoIDENTIFICACION"].ToString()) : vIdoneidadOferente.NoIdentificacion;
                            vIdoneidadOferente.Idoneo = vDataReaderResults["IDONEO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IDONEO"].ToString()) : vIdoneidadOferente.Idoneo;
                            vIdoneidadOferente.Obervacion = vDataReaderResults["Obervacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Obervacion"].ToString()) : vIdoneidadOferente.Obervacion;
                   
                          
                            if (vIdoneidadOferente.TipoPersona.Equals("NATURAL"))
                                vIdoneidadOferente.Oferente = vDataReaderResults["OFERENTE"] != DBNull.Value ? Convert.ToString(vDataReaderResults["OFERENTE"].ToString()) : vIdoneidadOferente.Oferente;
                            if (vIdoneidadOferente.TipoPersona.Equals("JURIDICA"))
                                vIdoneidadOferente.Oferente = vDataReaderResults["RAZONSOCIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RAZONSOCIAL"].ToString()) : vIdoneidadOferente.Oferente;
                            vIdoneidadOferente.ValidacionDocumental = vDataReaderResults["VALIDACIONDOCUMENTAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VALIDACIONDOCUMENTAL"].ToString()) : vIdoneidadOferente.ValidacionDocumental;
                            vIdoneidadOferente.ValidacionFinanciera = "NO";
                      
                            if (vIdoneidadOferente.ValidacionDocumental.Equals("NO"))
                            {
                                using (DbCommand vDbCommand1 = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_IdoneidadOferente_Liquidez_NivelEndeudamiento"))
                                {
                                    int anio = DateTime.Now.Year - 1;
                                    vDataBase.AddInParameter(vDbCommand1, "@IdTercero", DbType.Int32, IdTercero);
                                    vDataBase.AddInParameter(vDbCommand1, "@ANIO", DbType.Int32, anio);
                                    using (IDataReader vDataReaderResults1 = vDataBase.ExecuteReader(vDbCommand1))
                                    {

                                        while (vDataReaderResults1.Read())
                                        {
                                            vIdoneidadOferente.Liquidez = vDataReaderResults1["LIQUIDEZ"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults1["LIQUIDEZ"].ToString()) : vIdoneidadOferente.Liquidez;
                                            vIdoneidadOferente.NivelEndeudamiento = vDataReaderResults1["NIVELENDEUDAMIENTO"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults1["NIVELENDEUDAMIENTO"].ToString()) : vIdoneidadOferente.Liquidez;
                                            if (vIdoneidadOferente.Liquidez >= 1 && vIdoneidadOferente.NivelEndeudamiento <= 70)
                                            {
                                                vIdoneidadOferente.ValidacionFinanciera = "SI";
                                            }
                                            else { vIdoneidadOferente.ValidacionFinanciera = "NO"; }
                                        }
                                    }
                                }
                            }
                            using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_IdoneidadOferente_Experiencia"))
                            {
                                vIdoneidadOferente.ValidacionExperiencia = "NO";

                                vDataBase.AddInParameter(vDbCommand2, "@IdTercero", DbType.Int32, IdTercero);
                                using (IDataReader vDataReaderResults2 = vDataBase.ExecuteReader(vDbCommand2))
                                {
                                    while (vDataReaderResults2.Read())
                                    {
                                        vIdoneidadOferente.Experiencia = vDataReaderResults2["EXPERIENCIA"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults2["EXPERIENCIA"].ToString()) : vIdoneidadOferente.Experiencia;
                                        if (vIdoneidadOferente.Experiencia >= 24)
                                        {
                                            vIdoneidadOferente.ValidacionExperiencia = "SI";
                                        }
                                        else { vIdoneidadOferente.ValidacionExperiencia = "NO"; }
                                    }
                                }
                            }


                            vListaIdoneidadOferente.Add(vIdoneidadOferente);
                        }
                    }
                }
                return vListaIdoneidadOferente;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
