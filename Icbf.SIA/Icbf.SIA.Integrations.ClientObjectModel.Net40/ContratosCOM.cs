﻿using Icbf.Contrato.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Icbf.SIA.Integrations.ClientObjectModel.Net40
{

    public class ContratosCOM
    {
        private string vURL = System.Configuration.ConfigurationManager.AppSettings["URLWebApiSIAContratos"];

        public InfoContratoDTO GetContratoAsync(string anio, string codigoRegional, string numeroContrato)
        {
            InfoContratoDTO entidad = new InfoContratoDTO();

            try
            {
                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(vURL + "/" + anio + "/" + codigoRegional + "/" + numeroContrato);

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    string responseJson = sr.ReadToEnd();
                    var serializer = new JavaScriptSerializer();
                    entidad = serializer.Deserialize<InfoContratoDTO>(responseJson);
                }

                return entidad;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InfoContratoDTO> GetContratoAsync(string codigoRegional, string fechaDesde)
        {
            List<InfoContratoDTO> entidad = new List<InfoContratoDTO>();

            try
            {
                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(vURL + "/" + codigoRegional + "/" + fechaDesde);

                HttpWebResponse response = (HttpWebResponse)http.GetResponse();

                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    string responseJson = sr.ReadToEnd();
                    var serializer = new JavaScriptSerializer();
                    entidad = serializer.Deserialize<List<InfoContratoDTO>>(responseJson);
                }

                return entidad;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
