﻿using Icbf.ActualizacionAutomatica.Entity;
using Icbf.ActualizacionAutomatica.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.ActualizacionAutomatica.Business
{
    public class ActualizacionAutomaticaBLL
    {
        private ActualizacionAutomaticaDAL vRegistroSolicitud;
        public ActualizacionAutomaticaBLL()
        {
            vRegistroSolicitud = new ActualizacionAutomaticaDAL();
        }
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos()
        {
            try
            {
                return vRegistroSolicitud.ConsultarRegistroSolicitudEstudioSectoryCasos();
            }
            catch (Exception e)
            {
                throw;
            }
           
        }
        public int ActualizarRegistroSolicitudEstudioSectoryCasos(int vigenciapacco, decimal consecutivo, string area, string codigoarea, string estado, string FechaInicioProceso, string Id_modalidad, decimal? Id_tipo_contrato, string modalidad, string objeto_contractual, string tipo_contrato, string FechaInicioEjecucion, decimal valor_contrato)
        {
            try
            {
                return vRegistroSolicitud.ActualizarRegistroSolicitudEstudioSectoryCasos(vigenciapacco, consecutivo, area, codigoarea, estado, FechaInicioProceso, Id_modalidad, Id_tipo_contrato, modalidad, objeto_contractual, tipo_contrato, FechaInicioEjecucion, valor_contrato);
            }            
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
