﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class CierreEstudiosPorVigenciaBLL
    {
        private CierreEstudiosPorVigenciaDAL vCierreEstudiosPorVigenciaDAL;
        public CierreEstudiosPorVigenciaBLL()
        {
            vCierreEstudiosPorVigenciaDAL = new CierreEstudiosPorVigenciaDAL();
        }

        public int InsertarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.InsertarCierreEstudiosPorVigencia(pCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.ModificarCierreEstudiosPorVigencia(pCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.EliminarCierreEstudiosPorVigencia(pCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CierreEstudiosPorVigencia ConsultarCierreEstudiosPorVigencia(Int32 pIdCierreEstudiosPorVigencia)
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.ConsultarCierreEstudiosPorVigencia(pIdCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<CierreEstudiosPorVigencia> ConsultarCierreEstudiosPorVigencias(int? pAnioCierre, DateTime? pFechaEjecucion)
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.ConsultarCierreEstudiosPorVigencias(pAnioCierre, pFechaEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<int> ConsultarAnioCierre()
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.ConsultarAnioCierre();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<DateTime> ConsultarFechaEjecucion()
        {
            try
            {
                return vCierreEstudiosPorVigenciaDAL.ConsultarFechaEjecucion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
