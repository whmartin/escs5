﻿using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ActualizacionMasivaBLL
    {
        private ActualizacionMasivaDAL vRegistroSolicitudEstudioSectoryCasoDAL;
        public ActualizacionMasivaBLL()
        {
            vRegistroSolicitudEstudioSectoryCasoDAL = new ActualizacionMasivaDAL();
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos()
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ConsultarRegistroSolicitudEstudioSectoryCasos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LogActualizacionMasiva> ConsultarRegistroActualizaciones(string Usuario, string FI, string FF)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ConsultarRegistroActualizaciones(Usuario, FI, FF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CreaLogActualizacion(string userp)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.CreaLogActualizacion(userp);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarRegistroSolicitudEstudioSectoryCasos(string Userp,int vigenciapacco,decimal consecutivo,string area, string codigoarea, string estado, string FechaInicioProceso, string Id_modalidad, decimal? Id_tipo_contrato, string modalidad, string objeto_contractual, string tipo_contrato, string FechaInicioEjecucion, decimal valor_contrato)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ActualizarRegistroSolicitudEstudioSectoryCasos(Userp, vigenciapacco,consecutivo, area, codigoarea, estado, FechaInicioProceso, Id_modalidad, Id_tipo_contrato, modalidad, objeto_contractual, tipo_contrato, FechaInicioEjecucion, valor_contrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }

    
}
