using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class RegistroSolicitudEstudioSectoryCasoBLL
    {
        private RegistroSolicitudEstudioSectoryCasoDAL vRegistroSolicitudEstudioSectoryCasoDAL;
        public RegistroSolicitudEstudioSectoryCasoBLL()
        {
            vRegistroSolicitudEstudioSectoryCasoDAL = new RegistroSolicitudEstudioSectoryCasoDAL();
        }
        public int InsertarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.InsertarRegistroSolicitudEstudioSectoryCaso(pRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ModificarRegistroSolicitudEstudioSectoryCaso(pRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.EliminarRegistroSolicitudEstudioSectoryCaso(pRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroSolicitudEstudioSectoryCaso ConsultarRegistroSolicitudEstudioSectoryCaso(Int32 pIdRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ConsultarRegistroSolicitudEstudioSectoryCaso(pIdRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos(Int32? pConsecutivoEstudio, int? pAplicaPACCO, int? pConsecutivoPACCO, int? pDireccionsolicitantePACCO, String pObjetoPACCO, int? pModalidadPACCO, Decimal? pValorPresupuestalPACCO, String pVigenciaPACCO, int? pConsecutivoEstudioRelacionado, DateTime pFechaSolicitudInicial, String pActaCorreoNoRadicado, String pNombreAbreviado, String pNumeroReproceso, String pObjeto, String pCuentaVigenciasFuturasPACCO, String pAplicaProcesoSeleccion, int? pIdModalidadSeleccion, int? pIdTipoEstudio, int? pIdComplejidadInterna, int? pIdComplejidadIndicador, int? pIdResponsableES, int? pIdResponsableEC, String pOrdenadorGasto, int? pIdEstadoSolicitud, int? pIdMotivoSolicitud, int? pIdDireccionSolicitante, int? pIdAreaSolicitante, String pTipoValor, Decimal? pValorPresupuestoEstimadoSolicitante)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ConsultarRegistroSolicitudEstudioSectoryCasos(pConsecutivoEstudio, pAplicaPACCO, pConsecutivoPACCO, pDireccionsolicitantePACCO, pObjetoPACCO, pModalidadPACCO, pValorPresupuestalPACCO, pVigenciaPACCO, pConsecutivoEstudioRelacionado, pFechaSolicitudInicial, pActaCorreoNoRadicado, pNombreAbreviado, pNumeroReproceso, pObjeto, pCuentaVigenciasFuturasPACCO, pAplicaProcesoSeleccion, pIdModalidadSeleccion, pIdTipoEstudio, pIdComplejidadInterna, pIdComplejidadIndicador, pIdResponsableES, pIdResponsableEC, pOrdenadorGasto, pIdEstadoSolicitud, pIdMotivoSolicitud, pIdDireccionSolicitante, pIdAreaSolicitante, pTipoValor, pValorPresupuestoEstimadoSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultaGeneralSeguimiento(long? pConsecutivoEstudio, string pObjeto, string pNombreAbreviado, long? pConsecutivoPACCO, int? pModalidadSeleccion, string pIsEstado, int? pAnioCierre, int? pVigencia)
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ConsultaGeneralSeguimiento( pConsecutivoEstudio,  pObjeto, pNombreAbreviado,  pConsecutivoPACCO,  pModalidadSeleccion, pIsEstado,  pAnioCierre,  pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        public List<int> ConsultarFechaSolicitudInicial()
        {
            try
            {
                return vRegistroSolicitudEstudioSectoryCasoDAL.ConsultarFechaSolicitudInicial();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
