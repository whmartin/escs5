using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class BitacoraAccionesBLL
    {
        private BitacoraAccionesDAL vBitacoraAccionesDAL;
        public BitacoraAccionesBLL()
        {
            vBitacoraAccionesDAL = new BitacoraAccionesDAL();
        }

        /// <summary>
        /// M�todo para insertar una bit�cora de acci�n
        /// </summary>
        /// <param name="pBitacoraAcciones">La bit�cora de acci�n</param>
        public int InsertarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                return vBitacoraAccionesDAL.InsertarBitacoraAcciones(pBitacoraAcciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar una bit�cora de acci�n
        /// </summary>
        /// <param name="pBitacoraAcciones">La bit�cora de acci�n</param>
        public int ModificarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                return vBitacoraAccionesDAL.ModificarBitacoraAcciones(pBitacoraAcciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo eliminar una bit�cora de acci�n
        /// </summary>
        /// <param name="pBitacoraAcciones">La bit�cora de acci�n</param>
        public int EliminarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                return vBitacoraAccionesDAL.EliminarBitacoraAcciones(pBitacoraAcciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una bit�cora de acci�n por su id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El id de la bit�cora de acci�n</param>
        public List<BitacoraAcciones> ConsultarBitacoraAcciones(decimal pIdBitacoraEstudio)
        {
            try
            {
                return vBitacoraAccionesDAL.ConsultarBitacoraAcciones(pIdBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para listar la bit�cora de acciones
        /// </summary>
        /// <param name="pIdEstudio">El id del estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado</param>
        /// /// <param name="pObjeto">El objeto</param>
        /// <param name="pIdResponsableES">El id del responsable del estudio de sector</param>
        /// /// <param name="pIdResponsableEC">El id del responsable del est�dio de costos</param>
        /// <param name="pIdModalidadSeleccion">El id de la Modalidad de contrataci�n</param>
        /// /// <param name="pDireccionSolicitante">Dependencia solicitante</param>
        /// <param name="pEstado">El estado</param>
        public List<BitacoraAcciones> ConsultarBitacoraAccioness(long? pIdEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return vBitacoraAccionesDAL.ConsultarBitacoraAccioness(pIdEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar una bit�cora de est�dio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bit�cora de est�dio</param>
        public decimal InsertarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                return vBitacoraAccionesDAL.InsertarBitacoraEstudio(pBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar una bit�cora de est�dio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bit�cora de est�dio</param>
        public decimal ModificarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                return vBitacoraAccionesDAL.ModificarBitacoraEstudio(pBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar una bit�cora de est�dio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bit�cora de est�dio</param>
        public int EliminarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                return vBitacoraAccionesDAL.EliminarBitacoraEstudio(pBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una bit�cora de est�dio por su Id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El Id de la bit�cora de est�dio</param>
        public BitacoraEstudio ConsultarBitacoraEstudio(decimal pIdBitacoraEstudio)
        {
            try
            {
                return vBitacoraAccionesDAL.ConsultarBitacoraEstudio(pIdBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar si una bit�cora de est�dio existe por su Id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El Id de la bit�cora de est�dio</param>
        public bool ConsultarBitacoraEstudioExiste(decimal pIdconsecutivoEstudio)
        {
            try
            {
                return vBitacoraAccionesDAL.ConsultarBitacoraEstudioExiste(pIdconsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar la traza de una bit�cora de acci�n
        /// </summary>
        /// <param name="pIdBitacoraAccion">El Id de la bit�cora de acci�n</param>
        public List<BitacoraAcciones> ConsultarBitacoraAccionTraza(decimal pIdBitacoraAccion)
        {
            try
            {
                return vBitacoraAccionesDAL.ConsultarBitacoraAccionTraza(pIdBitacoraAccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
    }
}