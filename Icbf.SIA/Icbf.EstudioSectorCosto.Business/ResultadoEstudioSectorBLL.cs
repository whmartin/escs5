using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ResultadoEstudioSectorBLL
    {
        private ResultadoEstudioSectorDAL vResultadoEstudioSectorDAL;
        public ResultadoEstudioSectorBLL()
        {
            vResultadoEstudioSectorDAL =  new ResultadoEstudioSectorDAL();
        }

        /// <summary>
        /// M�todo para insertar resultado de estudio
        /// </summary>
        /// <param name="pResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna el id del resultado insertado</returns>
        public long InsertarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                
                return vResultadoEstudioSectorDAL.InsertarResultadoEstudioSector(pResultadoEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar resultados de estudio
        /// </summary>
        /// <param name="vResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna n�mero de filas afectadas</returns>
        public long ModificarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                return  vResultadoEstudioSectorDAL.ModificarResultadoEstudioSector(pResultadoEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar resultado estudio
        /// </summary>
        /// <param name="pResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna el n�mero de filas insertadas</returns>
        public int EliminarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                return vResultadoEstudioSectorDAL.EliminarResultadoEstudioSector(pResultadoEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region INDICADORES RESULTADO

        /// <summary>
        /// M�todo para insertar indicadores resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>Retorna el id insertado</returns>
        public long InsertarIndicadorResultado(IndicadorAdicionalResultado pIndicador)
        {
            try
            {

                return vResultadoEstudioSectorDAL.InsertarIndicadoresResultadoEstudioSector(pIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar d�as festivos
        /// </summary>
        /// <param name="pAnnio">A�o</param>
        /// <returns>Listas de d�as festivos</returns>
        public List<BaseDTO> ConsultarDiasFestivos(int pAnnio)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarDiasFestivos(pAnnio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar vigencia
        /// </summary>
        /// <returns>Retorna una lista de vigencias</returns>
        public List<BaseDTO> ConsultarVigencia()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarVigencia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar a�o cierre
        /// </summary>
        /// <returns>Lista de a�os cierre</returns>
        public List<BaseDTO> ConsultarAnioCierre()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarAnioCierre();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>retorna el n�mero de filas afectadas</returns>
        public long ModificarIndicadorResultado(IndicadorAdicionalResultado pIndicador)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarIndicadoresResultadoEstudioSector(pIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesEstudioSectorCostos">Objeto GestionesEstudioSectorCostos</param>
        /// <returns>Id del registro de gesti�n insertado</returns>
        public decimal InsertarGestionesEstudioSectorCostos(GestionesEstudioSectorCostos pGestionesEstudioSectorCostos)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarGestionesEstudioSectorCostos(pGestionesEstudioSectorCostos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesEstudioSectorCostos">Id del registro gestion estudio</param>
        /// <returns>Retorna n�mero de filas afectadas</returns>
        public decimal ModificarGestionesEstudioSectorCostos(GestionesEstudioSectorCostos pGestionesEstudioSectorCostos)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarGestionesEstudioSectorCostos(pGestionesEstudioSectorCostos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar gestiones de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado del estudio</param>
        /// <param name="pObjeto">Objeto del estudio</param>
        /// <param name="pIdResponsableES">Responsable del estudio ES</param>
        /// <param name="pIdResponsableEC">Responsable del estudio EC</param>
        /// <param name="pIdModalidadDeSeleccion">Id modalidad selecci�n</param>
        /// <param name="pDireccionSolicitante">Direcci�n del solicitante</param>
        /// <param name="pEstado">Estado del estudiio</param>
        /// <returns>Retorna una lista tipo GestionesEstudioSectorCostos</returns>
        public List<GestionesEstudioSectorCostos> ConsultarGestionEstudioSectores(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarGestionEstudioSectors(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesActividadesAdicionales">Objeto GestionesActividadesAdicionales</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int InsertarActividadesEstudioSector(GestionesActividadesAdicionales pGestionesActividadesAdicionales)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarActividadesEstudioSector(pGestionesActividadesAdicionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo modificar actividades gesti�n estudio
        /// </summary>
        /// <param name="pGestionesActividadesAdicionales">Objeto GestionesActividadesAdicionales</param>
        /// <returns>retorna el n�mero de filas afectadas</returns>
        public int ModificarActividadesEstudioSector(GestionesActividadesAdicionales pGestionesActividadesAdicionales)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarActividadesEstudioSector(pGestionesActividadesAdicionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarIndicadorResultado(IndicadorAdicionalResultado pIndicador)
        {
            try
            {
                return vResultadoEstudioSectorDAL.EliminarIndicadorResultadoEstudioSector(pIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIdResultadoEstudio">Id del registro resultado</param>
        /// <returns>Retorna lista de indicadores</returns>
        public List<IndicadorAdicionalResultado> ConsultarIndicadorResultado(long pIdResultadoEstudio)
        {

            try
            {
                return vResultadoEstudioSectorDAL.ConsultarIndicadorResultado(pIdResultadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        #endregion

        /// <summary>
        /// M�todo para consultar resultado estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id registro resultado estudio</param>
        /// <returns>Objeto ResultadoEstudioSector</returns>
        public ResultadoEstudioSector ConsultarResultadoEstudioSector(long pIdConsecutivoEstudio)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarResultadoEstudioSector(pIdConsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para listar ResultadoEstudioSectorConsulta
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado del estudio</param>
        /// <param name="pObjeto">Objeto del estudio</param>
        /// <param name="pIdResponsableES">Responsable del estudio ES</param>
        /// <param name="pIdResponsableEC">Responsable del estudio EC</param>
        /// <param name="pIdModalidadDeSeleccion">Id modalidad selecci�n</param>
        /// <param name="pDireccionSolicitante">Direcci�n del solicitante</param>
        /// <param name="pEstado">Estado del estudiio</param>
        /// <returns>Retorna una lista tipo ResultadoEstudioSectorConsulta</returns>
        public List<ResultadoEstudioSectorConsulta> ConsultarResultadoEstudioSectors(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadDeSeleccion, int?  pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarResultadoEstudioSectors(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para obtener lista responsables
        /// </summary>
        /// <returns>Retorna lista de reposnsables</returns>
        public List<BaseDTO> ConsultarResponsable()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarResponsable();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar modalidad selecci�n
        /// </summary>
        /// <returns>Lista de modalidad selecci�n</returns>
        public List<BaseDTO> ConsultarModalidadSeleccion()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarModalidadSeleccion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar Diracci�n del solicitante
        /// </summary>
        /// <returns>Lista de direcciones</returns>
        public List<BaseDTO> ConsultarDireccion()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarDireccion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para eliminar gestiones de estudio
        /// </summary>
        /// <param name="pGestionEstudioSector">Objeto GestionesEstudioSectorCostos</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int EliminarGestionEstudioSector(GestionesEstudioSectorCostos pGestionEstudioSector)
        {
            try
            {
                return vResultadoEstudioSectorDAL.EliminarGestionEstudioSector(pGestionEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consutar fechas definitivas de gestiones
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id del cosecutivo de estudio</param>
        /// <returns>Retorna objeto GestionesEstudioSectorCostos</returns>
        public GestionesEstudioSectorCostos ConsultarGestionDefinitivas(long pIdconsecutivoEstudio)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarGestionDefinitivas(pIdconsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar gesti�n de estudio
        /// </summary>
        /// <param name="pIdGestionEstudio">Id de la gesti�n de estudio</param>
        /// <returns>Retorna objeto GestionesEstudioSectorCostos</returns>
        public GestionesEstudioSectorCostos ConsultarGestionEstudioSector(decimal pIdGestionEstudio)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarGestionEstudioSector(pIdGestionEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar actividades adicionales de gestiones
        /// </summary>
        /// <param name="pIdGestionEstudio">Id de la gestion de estudio</param>
        /// <returns>Retorna lista de GestionesActividadesAdicionales</returns>
        public List<GestionesActividadesAdicionales> ConsultarGestionesActividadesAdicionales(decimal pIdGestionEstudio)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar estados 
        /// </summary>
        /// <returns>Retorna lista de estados</returns>
        public List<BaseDTO> ConsultarEstado()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarEstado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para consultar actividades
        /// </summary>
        /// <returns>Retorna lista de actividades</returns>
        public List<BaseDTO> ConsultarActividades()
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarActividades();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        #region TIEMPO ENTRE ACTIVIDADES

        /// <summary>
        /// M�todo para validar par�metros para calcular tiempos
        /// </summary>
        /// <param name="IdConsecutivoEstudio">Id del cosecutivo de estudio</param>
        /// <returns>Retorna "OK" cuando se cumplen las condiciones</returns>
        public string ValidarCalculoTiempos(decimal IdConsecutivoEstudio)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ValidarCalculoTiempos(IdConsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar Tiempos entre actividades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id tiempo entre actividades insertado</returns>
        public decimal InsertarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarTiempoEntreActividades(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para modificar tiempos entre actividades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int ModificarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarTiempoEntreActividades(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// M�todo para eliminar tiempos entre activiades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int EliminarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.EliminarTiempoEntreActividades(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        #endregion

        #region TIEMPOS

        /// <summary>
        /// M�todo para insertar tiempos SyC
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el id insertado</returns>
        public decimal InsertarTiempoEntreActividadesEstudioSyC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarTiempoEntreActividadesEstudioSyC( pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar tiempos SyC
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el n�mero de filas afectadas</returns>
        public int ModificarTiempoEntreActividadesEstudioSyC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarTiempoEntreActividadesEstudioSyC( pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        

        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesEstudioSyC(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesEstudioSyC( pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para insertar tiempos ESC
        /// </summary>
        /// <param name="pTiempoEntreActividadesFechasESC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id insertado</returns>
        public decimal InsertarTiempoEntreActividadesFechasESC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesFechasESC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarTiempoEntreActividadesFechasESC(pTiempoEntreActividadesFechasESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para modificar tiempos ESC
        /// </summary>
        /// <param name="pTiempoEntreActividadesFechasESC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id insertado</returns>
        public decimal ModificarTiempoEntreActividadesFechasESC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesFechasESC)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarTiempoEntreActividadesFechasESC(pTiempoEntreActividadesFechasESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar tiempos ESC
        /// </summary>
        /// <param name="pIdTiempoEntreActividades">Id Tiempo entre activiades</param>
        /// <returns>Retorna teimpos ESC</returns>
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesFechasESC(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesFechasESC(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesFechasESCs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesFechasESCs( pIdConsecutivoEstudio,  pNombreAbreviado,  pObjeto,  pIdResponsableES,  pIdResponsableEC,  pIdModalidadDeSeleccion,  pDireccionSolicitante,  pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int InsertarTiempoEntreActividadesFechasReales(TiempoEntreActividadesEstudioSyC pTiempoActividadesFechasReales)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarTiempoEntreActividadesFechasReales(pTiempoActividadesFechasReales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiempoEntreActividadesFechasReales(TiempoEntreActividadesEstudioSyC pTiempoActividadesFechasReales)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarTiempoEntreActividadesFechasReales(pTiempoActividadesFechasReales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesFechasReales(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesFechasReales(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesFechasRealess(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesFechasRealess(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public decimal InsertarTiempoEntreActividadesPACCO(TiemposPACCO InsertarTiempoEntreActividadesPACCO)
        {
            try
            {
                return vResultadoEstudioSectorDAL.InsertarTiempoEntreActividadesPACCO(InsertarTiempoEntreActividadesPACCO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        public int ModificarTiempoEntreActividadesPACCO(TiemposPACCO pTiempoEntreActividadesPACCO)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ModificarTiempoEntreActividadesPACCO(pTiempoEntreActividadesPACCO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        public TiemposPACCO ConsultarTiempoEntreActividadesPACCO(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesPACCO(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiemposPACCO> ConsultarTiempoEntreActividadesPACCOs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempoEntreActividadesPACCOs(pIdConsecutivoEstudio,  pNombreAbreviado, pObjeto,  pIdResponsableES, pIdResponsableEC,  pIdModalidadDeSeleccion,  pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiempoEntreActividadesEstudioSyC ConsultarTiemposESC(decimal pIdConsecutivoEstudio, string pFEstimadaFCTPreliminar)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiemposESC(pIdConsecutivoEstudio, pFEstimadaFCTPreliminar);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
    }
}
