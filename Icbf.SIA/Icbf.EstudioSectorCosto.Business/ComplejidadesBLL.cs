using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ComplejidadesBLL
    {
        private ComplejidadesDAL vComplejidadesDAL;
        public ComplejidadesBLL()
        {
            vComplejidadesDAL = new ComplejidadesDAL();
        }
        public int InsertarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                return vComplejidadesDAL.InsertarComplejidades(pComplejidades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                return vComplejidadesDAL.ModificarComplejidades(pComplejidades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                return vComplejidadesDAL.EliminarComplejidades(pComplejidades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Complejidades ConsultarComplejidades(int pIdComplejidad)
        {
            try
            {
                return vComplejidadesDAL.ConsultarComplejidades(pIdComplejidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Complejidades> ConsultarComplejidadess(String pNombre, int? pTipoComplejidad, int? pEstado)
        {
            try
            {
                return vComplejidadesDAL.ConsultarComplejidadess(pNombre, pTipoComplejidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarComplejidades(String pNombre, int pTipoComplejidad)
        {
            try
            {
                return vComplejidadesDAL.ValidarComplejidades(pNombre, pTipoComplejidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
