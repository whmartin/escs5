﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class InformacionProcesoSeleccionBLL
    {
        private InformacionProcesoSeleccionDAL vInformacionProcesoSeleccionDAL;
        public InformacionProcesoSeleccionBLL()
        {
            vInformacionProcesoSeleccionDAL = new InformacionProcesoSeleccionDAL();
        }
        public int InsertarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                return vInformacionProcesoSeleccionDAL.InsertarInformacionProcesoSeleccion(pInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                return vInformacionProcesoSeleccionDAL.ModificarInformacionProcesoSeleccion(pInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                return vInformacionProcesoSeleccionDAL.EliminarInformacionProcesoSeleccion(pInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public InformacionProcesoSeleccion ConsultarInformacionProcesoSeleccion(Int32 pIdInformacionProcesoSeleccion)
        {
            try
            {
                return vInformacionProcesoSeleccionDAL.ConsultarInformacionProcesoSeleccion(pIdInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccions(Int32? pConsecutivoEstudio, String pNombreAbreviado, String pObjeto, int? pIdModalidadSeleccion, int? pIdResponsableES, int? pIdResponsableEC, int? pIdEstadoSolicitud, int? pIdDireccionSolicitante)
        {
            try
            {
                return vInformacionProcesoSeleccionDAL.ConsultarProcesoSeleccions(pConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdModalidadSeleccion, pIdResponsableES, pIdResponsableEC, pIdEstadoSolicitud, pIdDireccionSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<InformacionProcesoSeleccion> ConsultarContratoESC(int? pConsecutivo, string pNumeroProceso, int? pVigenciaProceso, string pObjetoContrato, int? pProcesoSeleccion)
        {
            try
            {
                return vInformacionProcesoSeleccionDAL.ConsultarContratoESC(pConsecutivo, pNumeroProceso, pVigenciaProceso, pObjetoContrato, pProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
