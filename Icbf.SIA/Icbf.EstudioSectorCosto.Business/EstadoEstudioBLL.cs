using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class EstadoEstudioBLL
    {
        private EstadoEstudioDAL vEstadoEstudioDAL;
        public EstadoEstudioBLL()
        {
            vEstadoEstudioDAL = new EstadoEstudioDAL();
        }
        public int InsertarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                return vEstadoEstudioDAL.InsertarEstadoEstudio(pEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                return vEstadoEstudioDAL.ModificarEstadoEstudio(pEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                return vEstadoEstudioDAL.EliminarEstadoEstudio(pEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoEstudio ConsultarEstadoEstudio(int pIdEstadoEstudio)
        {
            try
            {
                return vEstadoEstudioDAL.ConsultarEstadoEstudio(pIdEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EstadoEstudio> ConsultarEstadoEstudios(String pNombre, String pMotivo, String pDescripcion, int? pEstado)
        {
            try
            {
                return vEstadoEstudioDAL.ConsultarEstadoEstudios(pNombre, pMotivo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarEstadoEstudio(String pNombre, String pMotivo)
        {
            try
            {
                return vEstadoEstudioDAL.ValidarEstadoEstudio(pNombre, pMotivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
