using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ActividadesBLL
    {
        private ActividadesDAL vActividadesDAL;
        public ActividadesBLL()
        {
            vActividadesDAL = new ActividadesDAL();
        }
        public int InsertarActividades(Actividades pActividades)
        {
            try
            {
                return vActividadesDAL.InsertarActividades(pActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarActividades(Actividades pActividades)
        {
            try
            {
                return vActividadesDAL.ModificarActividades(pActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarActividades(Actividades pActividades)
        {
            try
            {
                return vActividadesDAL.EliminarActividades(pActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Actividades ConsultarActividades(int pIdActividad)
        {
            try
            {
                return vActividadesDAL.ConsultarActividades(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Actividades> ConsultarActividadess(String pNombre, String pDescripcion, int? pEstado)
        {
            try
            {
                return vActividadesDAL.ConsultarActividadess(pNombre, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarActividad(String pNombreActividad)
        {
            try
            {
                return vActividadesDAL.ValidarActividad(pNombreActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
