using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class TiposEstudioBLL
    {
        private TiposEstudioDAL vTiposEstudioDAL;
        public TiposEstudioBLL()
        {
            vTiposEstudioDAL = new TiposEstudioDAL();
        }
        public int InsertarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                return vTiposEstudioDAL.InsertarTiposEstudio(pTiposEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                return vTiposEstudioDAL.ModificarTiposEstudio(pTiposEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                return vTiposEstudioDAL.EliminarTiposEstudio(pTiposEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiposEstudio ConsultarTiposEstudio(int pIdTipoEstudio)
        {
            try
            {
                return vTiposEstudioDAL.ConsultarTiposEstudio(pIdTipoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposEstudio> ConsultarTiposEstudios(String pNombre, String pDescripcion, int? pEstado)
        {
            try
            {
                return vTiposEstudioDAL.ConsultarTiposEstudios(pNombre, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarTiposEstudio(String pNombre)
        {
            try
            {
                return vTiposEstudioDAL.ValidarTiposEstudio(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
