using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class DiasEstimadosRealesPorModalidadBLL
    {
        private DiasEstimadosRealesPorModalidadDAL vDiasEstimadosRealesPorModalidadDAL;
        public DiasEstimadosRealesPorModalidadBLL()
        {
            vDiasEstimadosRealesPorModalidadDAL = new DiasEstimadosRealesPorModalidadDAL();
        }
        public int InsertarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return vDiasEstimadosRealesPorModalidadDAL.InsertarDiasEstimadosRealesPorModalidad(pDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return vDiasEstimadosRealesPorModalidadDAL.ModificarDiasEstimadosRealesPorModalidad(pDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return vDiasEstimadosRealesPorModalidadDAL.EliminarDiasEstimadosRealesPorModalidad(pDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DiasEstimadosRealesPorModalidad ConsultarDiasEstimadosRealesPorModalidad(int pIdDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return vDiasEstimadosRealesPorModalidadDAL.ConsultarDiasEstimadosRealesPorModalidad(pIdDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DiasEstimadosRealesPorModalidad> ConsultarDiasEstimadosRealesPorModalidads(int? pIdModalidadSeleccion, int? pIdComplejidadIndicador, String pOperador, int? pLimite, int? pDiasHabilesEntrePSeInicioEjecucion, int? pDiasHabilesEntreComitePS, int? pDiasHabilesEntreRadicacionContratoYComite, int? pDiasHabilesEntreESyRadicacionContratos, int? pDiasHabilesEntreFCTDefinitivaYES, int? pDiasHabilesEntreRadicacionFCTyFCTDefinitiva, int? pEstado)
        {
            try
            {
                return vDiasEstimadosRealesPorModalidadDAL.ConsultarDiasEstimadosRealesPorModalidads(pIdModalidadSeleccion, pIdComplejidadIndicador, pOperador, pLimite, pDiasHabilesEntrePSeInicioEjecucion, pDiasHabilesEntreComitePS, pDiasHabilesEntreRadicacionContratoYComite, pDiasHabilesEntreESyRadicacionContratos, pDiasHabilesEntreFCTDefinitivaYES, pDiasHabilesEntreRadicacionFCTyFCTDefinitiva, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarDiasEstimadosRealesPorModalidad(int pIdModalidadSeleccion
                                                            , int? pIdComplejidadIndicador
                                                            , String pOperador
                                                            , int? pLimite
                                                            , int? pDiasHabilesEntrePSeInicioEjecucion
                                                            , int? pDiasHabilesEntreComitePS
                                                            , int? pDiasHabilesEntreRadicacionContratoYComite
                                                            , int? pDiasHabilesEntreESyRadicacionContratos
                                                            , int? pDiasHabilesEntreFCTDefinitivaYES
                                                            , int? pDiasHabilesEntreRadicacionFCTyFCTDefinitiva)
        {
            try
            {
                return vDiasEstimadosRealesPorModalidadDAL.ValidarDiasEstimadosRealesPorModalidad(pIdModalidadSeleccion
                                                                                                , pIdComplejidadIndicador
                                                                                                , pOperador
                                                                                                , pLimite
                                                                                                , pDiasHabilesEntrePSeInicioEjecucion
                                                                                                , pDiasHabilesEntreComitePS
                                                                                                , pDiasHabilesEntreRadicacionContratoYComite
                                                                                                , pDiasHabilesEntreESyRadicacionContratos
                                                                                                , pDiasHabilesEntreFCTDefinitivaYES
                                                                                                , pDiasHabilesEntreRadicacionFCTyFCTDefinitiva);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
