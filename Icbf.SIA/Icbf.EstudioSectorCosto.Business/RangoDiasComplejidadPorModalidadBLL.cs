using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class RangoDiasComplejidadPorModalidadBLL
    {
        private RangoDiasComplejidadPorModalidadDAL vRangoDiasComplejidadPorModalidadDAL;
        public RangoDiasComplejidadPorModalidadBLL()
        {
            vRangoDiasComplejidadPorModalidadDAL = new RangoDiasComplejidadPorModalidadDAL();
        }
        public int InsertarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                return vRangoDiasComplejidadPorModalidadDAL.InsertarRangoDiasComplejidadPorModalidad(pRangoDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                return vRangoDiasComplejidadPorModalidadDAL.ModificarRangoDiasComplejidadPorModalidad(pRangoDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                return vRangoDiasComplejidadPorModalidadDAL.EliminarRangoDiasComplejidadPorModalidad(pRangoDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RangoDiasComplejidadPorModalidad ConsultarRangoDiasComplejidadPorModalidad(int pIdRangosDiasComplejidadPorModalidad)
        {
            try
            {
                return vRangoDiasComplejidadPorModalidadDAL.ConsultarRangoDiasComplejidadPorModalidad(pIdRangosDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RangoDiasComplejidadPorModalidad> ConsultarRangoDiasComplejidadPorModalidads(int? pIdModalidadSeleccion, String pOperador, int? pLimite, int? pIdComplejidadInterna, int? pIdComplejidadIndicador, int? pDiasHabilesInternos, int? pDiasHabilesCumplimientoIndicador, int? pEstado)
        {
            try
            {
                return vRangoDiasComplejidadPorModalidadDAL.ConsultarRangoDiasComplejidadPorModalidads(pIdModalidadSeleccion, pOperador, pLimite, pIdComplejidadInterna, pIdComplejidadIndicador, pDiasHabilesInternos, pDiasHabilesCumplimientoIndicador, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarRangoDiasComplejidadPorModalidad(int pIdModalidadSeleccion
                                                                , String pOperador
                                                                , int? pLimite
                                                                , int? pIdComplejidadInterna
                                                                , int? pIdComplejidadIndicador
                                                                , int? pDiasHabilesInternos
                                                                , int? pDiasHabilesCumplimientoIndicador)
        {
            try
            {
                return vRangoDiasComplejidadPorModalidadDAL.ValidarRangoDiasComplejidadPorModalidad(pIdModalidadSeleccion
                                                                                                    , pOperador
                                                                                                    , pLimite
                                                                                                    , pIdComplejidadInterna
                                                                                                    , pIdComplejidadIndicador
                                                                                                    , pDiasHabilesInternos
                                                                                                    , pDiasHabilesCumplimientoIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
