﻿using ClosedXML.Excel;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ReporteConsultaSeguimientoBLL
    {
        private ReporteConsultaSeguimientoDAL vResultadoEstudioSectorDAL;
        public ReporteConsultaSeguimientoBLL()
        {
            vResultadoEstudioSectorDAL = new ReporteConsultaSeguimientoDAL();
        }

        public List<GestionesEstudioSectorCostos> ConsultarGestion_By_IdConsecutivoEstudio(decimal pIdConsecutivoEstudio, int Nregistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarGestion_By_IdConsecutivoEstudio(pIdConsecutivoEstudio, Nregistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroSolicitudEstudioSectoryCaso ConsultarRegistroInicial_By_IdConsecutivoEstudio(decimal pIdConsecutivoEstudio, int Nregistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarRegistroInicial_By_IdConsecutivoEstudio(pIdConsecutivoEstudio, Nregistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Obtiene datos de proceso de seleccion por consecutivo estudio
        /// </summary>
        /// <param name="pConsecutivoEstudio"></param>
        /// <param name="pNombreAbreviado"></param>
        /// <param name="pObjeto"></param>
        /// <param name="pIdModalidadSeleccion"></param>
        /// <param name="pIdResponsableES"></param>
        /// <param name="pIdResponsableEC"></param>
        /// <param name="pIdEstadoSolicitud"></param>
        /// <param name="pIdDireccionSolicitante"></param>
        /// <returns></returns>
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(Int32? pConsecutivoEstudio, String pNombreAbreviado, String pObjeto, int? pIdModalidadSeleccion, int? pIdResponsableES, int? pIdResponsableEC, int? pIdEstadoSolicitud, int? pIdDireccionSolicitante)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarProcesoSeleccion_By_IdConsecutivoEstudio( pConsecutivoEstudio, pNombreAbreviado,  pObjeto,  pIdModalidadSeleccion,  pIdResponsableES, pIdResponsableEC, pIdEstadoSolicitud, pIdDireccionSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Crea el archivo excel 
        /// </summary>
        /// <param name="ModalidadyCierresAnio"></param>
        /// <param name="Usuario"></param>
        /// <param name="vCierres"></param>
        /// <param name="vBitacora"></param>
        /// <param name="vIndicadores"></param>
        /// <param name="vGAdicionales"></param>
        /// <param name="ProcesoSeleccion"></param>
        /// <param name="Estudios"></param>
        /// <param name="Gestiones"></param>
        /// <param name="vRegistro"></param>
        /// <param name="vTiemposReales"></param>
        /// <param name="vTiemposESC"></param>
        /// <param name="vTiempoPACCO"></param>
        /// <param name="vTiemposESyC"></param>
        /// <param name="IdConsecutivoEstudio"></param>
        /// <param name="pathPlantilla"></param>
        /// <returns></returns>
        public string GenerarReporteSeguimiento(List<RegistroSolicitudEstudioSectoryCaso> ModalidadyCierresAnio, string Usuario, List<CierreEstudiosPorVigencia> vCierres, List<BitacoraAcciones> vBitacora, List<IndicadorAdicionalResultado> vIndicadores, List<GestionesActividadesAdicionales> vGAdicionales, List<InformacionProcesoSeleccion> ProcesoSeleccion, ResultadoEstudioSector Estudios, List<GestionesEstudioSectorCostos> Gestiones, RegistroSolicitudEstudioSectoryCaso vRegistro, TiempoEntreActividadesEstudioSyC vTiemposReales, TiempoEntreActividadesEstudioSyC vTiemposESC, TiemposPACCO vTiempoPACCO, TiempoEntreActividadesEstudioSyC vTiemposESyC, int IdConsecutivoEstudio, string pathPlantilla)
        {
            string result = string.Empty;
            var fileResult = new XLWorkbook();
            var fileTemplate = new XLWorkbook(pathPlantilla);

            IXLWorksheet pestanaInit = null;
            pestanaInit = fileTemplate.Worksheets.First();
            var pestanaBase = fileTemplate.Worksheets.First();

            string xy = string.Empty;
            string xyBase = string.Empty;

            pestanaInit.Cell("B1").Value = Usuario.Trim();
            pestanaInit.Cell("B2").Value = DateTime.Now.ToShortDateString();

            pestanaInit.Cell("A5").Value = IdConsecutivoEstudio;
            
            if (vRegistro != null)
            {              
                pestanaInit.Cell("B5").Value = vRegistro.ConsecutivoPACCO;
                pestanaInit.Cell("C5").Value = vRegistro.NombreDireccionSolicitantePACCO;
                pestanaInit.Cell("D5").Value = vRegistro.ObjetoPACCO;
                pestanaInit.Cell("E5").Value = vRegistro.ValorPresupuestalPACCO.ToString(); 
                pestanaInit.Cell("F5").Value = vRegistro.VigenciaPACCO;
                pestanaInit.Cell("G5").Value = vRegistro.NombreModalidadPACCO;
                pestanaInit.Cell("H5").Value = vRegistro.ConsecutivoEstudioRelacionado;
                pestanaInit.Cell("I5").Value = vRegistro.FechaSolicitudInicial == null ? string.Empty : vRegistro.FechaSolicitudInicial.ToString("dd/MM/yyyy");
                pestanaInit.Cell("J5").Value = vRegistro.ActaCorreoNoRadicado;
                pestanaInit.Cell("K5").Value = vRegistro.NombreAbreviado;
                pestanaInit.Cell("L5").Value = vRegistro.NumeroReproceso;
                pestanaInit.Cell("M5").Value = vRegistro.Objeto;
                pestanaInit.Cell("N5").Value = vRegistro.CuentaVigenciasFuturasPACCO;
                pestanaInit.Cell("O5").Value = vRegistro.AplicaProcesoSeleccion;
                pestanaInit.Cell("P5").Value = vRegistro.ModalidadSeleccion;
                pestanaInit.Cell("Q5").Value = vRegistro.TipoEstudio;
                pestanaInit.Cell("R5").Value = vRegistro.ComplejidadInterna;
                pestanaInit.Cell("S5").Value = vRegistro.ComplejidadIndicador;
                pestanaInit.Cell("T5").Value = vRegistro.ResponsableES;
                pestanaInit.Cell("U5").Value = vRegistro.ResponsableEC;
                pestanaInit.Cell("V5").Value = vRegistro.OrdenadorGasto;
                pestanaInit.Cell("W5").Value = vRegistro.EstadoSolicitud;
                pestanaInit.Cell("X5").Value = vRegistro.MotivoSolicitud;
                pestanaInit.Cell("Y5").Value = vRegistro.DireccionSolicitante;
                pestanaInit.Cell("Z5").Value = vRegistro.AreaSolicitante;
                pestanaInit.Cell("AA5").Value = vRegistro.TipoValor;
                pestanaInit.Cell("AB5").Value = vRegistro.ValorPresupuestoEstimadoSolicitante.ToString(); 
            }
            if (Gestiones.Count >0)
            {

                char car = 'A';
                char car2 = 'C';
                //PREGUNTAOCOMENTARIO
                foreach (var Gestion in Gestiones)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = Gestion.FechaPreguntaOComentario == null ? string.Empty : Gestion.FechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");

                    if (Convert.ToChar(car2) == 'Z')
                    {
                        car = 'B';
                        car2 = 'A';
                    }
                    else
                    {
                        car2++;
                    }

                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = Gestion.FechaRespuesta == null ? string.Empty : Gestion.FechaRespuesta.Value.ToString("dd/MM/yyyy");

                    if (Convert.ToChar(car2) == 'Z')
                    {
                        car = 'B';
                        car2 = 'A';
                    }
                    else
                    {
                        car2++;
                    }

                    //                  
                }

                foreach(var item in Gestiones)
                {
                    pestanaInit.Cell("BC5").Value = item.UltimaFechaPreguntaOComentario == null ? string.Empty : item.UltimaFechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");
                    pestanaInit.Cell("BD5").Value = item.UltimaFechaRespuesta == null ? string.Empty : item.UltimaFechaRespuesta.Value.ToString("dd/MM/yyyy");

                    break;
                }
                ///fECHA ENTREGABLES_MC
                car = 'B';
                car2 = 'E';
                foreach (var Gestion in Gestiones)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = Gestion.FechaEntregaES_MC == null ? string.Empty : Gestion.FechaEntregaES_MC.Value.ToString("dd/MM/yyyy");
                    car2++;
                }

                ///fECHA entrega fct
                car = 'B';
                car2 = 'L';
                foreach (var Gestion in Gestiones)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = Gestion.FechaEntregaFCTORequerimiento == null ? string.Empty : Gestion.FechaEntregaFCTORequerimiento.Value.ToString("dd/MM/yyyy");
                    car2++;
                }

                ///fECHA envio sdc
                car = 'B';
                car2 = 'S';
                foreach (var Gestion in Gestiones)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = Gestion.FechaEnvioSDC == null ? string.Empty : Gestion.FechaEnvioSDC.Value.ToString("dd/MM/yyyy");
                    car2++;
                }

                ///plazo establecido
                car = 'B';
                car2 = 'Z';
                foreach (var Gestion in Gestiones)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = Gestion.PlazoEntregaCotizaciones == null ? string.Empty : Gestion.PlazoEntregaCotizaciones.Value.ToString("dd/MM/yyyy");
                    if (Convert.ToChar(car2) == 'Z')
                    {
                        car = 'C';
                        car2 = 'A';
                    }
                    else
                    {
                        car2++;
                    }
                }

            }
            if (vGAdicionales != null)
            {
                //actividades adicionales
                char car = 'C';
                char car2 = 'G';
                int cont = 0;
                foreach (var item in vGAdicionales)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.Actividad;
                    car2++;
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.Detalle;
                    car2++;
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.FechaActividadAdicional == null ? string.Empty : item.FechaActividadAdicional.Value.ToString("dd/MM/yyyy");

                    car2++;
                    if (cont == 5) break;

                    cont++;
                }

            }
            if (Estudios != null)
            {
                pestanaInit.Cell("CY5").Value = Estudios.Consultados;
                pestanaInit.Cell("CZ5").Value = Estudios.Participantes;
                pestanaInit.Cell("DA5").Value = Estudios.CotizacionesRecibidas;
                pestanaInit.Cell("DB5").Value = Estudios.CotizacionesParaPresupuesto;
                pestanaInit.Cell("DC5").Value = Estudios.RadicadoOficioEntregaESyC;
                pestanaInit.Cell("DD5").Value = Estudios.FechaDeEntregaESyC != null ? Convert.ToString(Estudios.FechaDeEntregaESyC).Substring(0, 10) : string.Empty; ;
                pestanaInit.Cell("DE5").Value = Estudios.TipoDeValor;
                pestanaInit.Cell("DF5").Value = Estudios.ValorPresupuestoESyC;
                pestanaInit.Cell("DG5").Value = Estudios.IndiceLiquidez == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DH5").Value = Estudios.IndiceLiquidez;
                pestanaInit.Cell("DI5").Value = Estudios.NivelDeEndeudamiento == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DJ5").Value = Estudios.NivelDeEndeudamiento;
                pestanaInit.Cell("DK5").Value = Estudios.RazonDeCoberturaDeIntereses == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DL5").Value = Estudios.RazonDeCoberturaDeIntereses;
                pestanaInit.Cell("DM5").Value = Estudios.CapitalDeTrabajo == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DN5").Value = Estudios.CapitalDeTrabajo;
                pestanaInit.Cell("DO5").Value = Estudios.RentabilidadDelPatrimonio == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DP5").Value = Estudios.RentabilidadDelPatrimonio;
                pestanaInit.Cell("DQ5").Value = Estudios.RentabilidadDelActivo == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DR5").Value = Estudios.RentabilidadDelActivo;
                pestanaInit.Cell("DS5").Value = Estudios.TotalPatrimonio == null ? string.Empty : Constantes.MayorOIgual; 
                pestanaInit.Cell("DT5").Value = Estudios.TotalPatrimonio;
                pestanaInit.Cell("DU5").Value = Estudios.KResidual;
                //pestanaInit.Cell("BP5").Value = Estudios.Indicador;

            }
            if (vIndicadores != null)
            {
                //indicadores
                char car = 'D';
                char car2 = 'V';
                int cont = 0;
                foreach (var item in vIndicadores)
                {
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.Indicador;
                    if (Convert.ToChar(car2) == 'Z')
                    {
                        car = 'E';
                        car2 = 'A';
                    }
                    else
                    {
                        car2++;
                    }
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.Condicion;
                    if (Convert.ToChar(car2) == 'Z')
                    {
                        car = 'E';
                        car2 = 'A';
                    }
                    else
                    {
                        car2++;
                    }
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.Valor;

                    //car2++;
                    if (Convert.ToChar(car2) == 'Z')
                    {
                        car = 'E';
                        car2 = 'A';
                    }
                    else
                    {
                        car2++;
                    }

                    if (cont == 2) break;

                    cont++;
                }
            }

            if (vTiemposESC != null)
            {
                ///
                pestanaInit.Cell("EE5").Value = Estudios.DocumentosRevisadosNAS;
                pestanaInit.Cell("EF5").Value = Estudios.Fecha != null ? Convert.ToString(Estudios.Fecha).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("EG5").Value = Estudios.RevisadoPor;
                pestanaInit.Cell("EH5").Value = Estudios.Observacion;
                ///

                pestanaInit.Cell("EI5").Value = vTiemposESyC.FentregaES_ECTiemposEquipo != null ? vTiemposESyC.FentregaES_ECTiemposEquipo.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("EJ5").Value = vTiemposESyC.FEentregaES_ECTiemposIndicador != null ? vTiemposESyC.FEentregaES_ECTiemposIndicador.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("EK5").Value = vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                pestanaInit.Cell("EL5").Value = vTiemposESyC.DiasHabilesEntreFCTFinalYSDC;
                pestanaInit.Cell("EM5").Value = vTiemposESyC.DiasHabilesEnESOCOSTEO;
                pestanaInit.Cell("EN5").Value = vTiemposESyC.DiashabilesParaDevolucion;
                //
                pestanaInit.Cell("EO5").Value = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL == null ? string.Empty : vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL.Substring(0,10);
                pestanaInit.Cell("EP5").Value = vTiempoPACCO.FEstimadaFCTPreliminar == null ? string.Empty : vTiempoPACCO.FEstimadaFCTPreliminar.Substring(0, 10);
                pestanaInit.Cell("EQ5").Value = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado != null ? Convert.ToString(vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("ER5").Value = vTiempoPACCO.FEestimadaES_EC != null ? Convert.ToString(vTiempoPACCO.FEestimadaES_EC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("ES5").Value = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos == null ? string.Empty : vTiempoPACCO.FEstimadaDocsRadicadosEnContratos.Substring(0, 10);
                pestanaInit.Cell("ET5").Value = vTiempoPACCO.FEstimadaComiteContratacion == null ? string.Empty : vTiempoPACCO.FEstimadaComiteContratacion.Substring(0, 10);
                pestanaInit.Cell("EU5").Value = vTiempoPACCO.FEstimadaInicioDelPS == null ? string.Empty : vTiempoPACCO.FEstimadaInicioDelPS.Substring(0, 10);
                pestanaInit.Cell("EV5").Value = vTiempoPACCO.FEstimadaPresentacionPropuestas == null ? string.Empty : vTiempoPACCO.FEstimadaPresentacionPropuestas.Substring(0, 10);
                pestanaInit.Cell("EW5").Value = vTiempoPACCO.FEstimadaDeInicioDeEjecucion == null ? string.Empty : vTiempoPACCO.FEstimadaDeInicioDeEjecucion.Substring(0, 10);

                //
                pestanaInit.Cell("EX5").Value = vTiemposESC.FRealFCTPreliminarESC != null ? Convert.ToString(vTiemposESC.FRealFCTPreliminarESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("EY5").Value = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC != null ? Convert.ToString(vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("EZ5").Value = vTiemposESC.FEstimadaES_EC_ESC != null ? Convert.ToString(vTiemposESC.FEstimadaES_EC_ESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FA5").Value = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC != null ? Convert.ToString(vTiemposESC.FEstimadaDocsRadicadosEnContratosESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FB5").Value = vTiemposESC.FEstimadaComitecontratacionESC != null ? Convert.ToString(vTiemposESC.FEstimadaComitecontratacionESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FC5").Value = vTiemposESC.FEstimadaInicioDelPS_ESC != null ? Convert.ToString(vTiemposESC.FEstimadaInicioDelPS_ESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FD5").Value = vTiemposESC.FEstimadaPresentacionPropuestasESC != null ? Convert.ToString(vTiemposESC.FEstimadaPresentacionPropuestasESC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FE5").Value = vTiemposESC.FEstimadaDeInicioDeEjecucionESC != null ? Convert.ToString(vTiemposESC.FEstimadaDeInicioDeEjecucionESC).Substring(0, 10) : string.Empty;

                //
                pestanaInit.Cell("FF5").Value = vTiemposReales.FRealFCTPreliminar != null ? Convert.ToString(vTiemposReales.FRealFCTPreliminar).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FG5").Value = vTiemposReales.FRealDocsRadicadosEnContratos != null ? Convert.ToString(vTiemposReales.FRealDocsRadicadosEnContratos).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FH5").Value = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado != null ? Convert.ToString(vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FI5").Value = vTiemposReales.FRealComiteContratacion != null ? Convert.ToString(vTiemposReales.FRealComiteContratacion).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FJ5").Value = vTiemposReales.FRealES_EC != null ? Convert.ToString(vTiemposReales.FRealES_EC).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FK5").Value = vTiemposReales.FRealInicioPS != null ? Convert.ToString(vTiemposReales.FRealInicioPS).Substring(0, 10) : string.Empty;
                pestanaInit.Cell("FL5").Value = vTiemposReales.FRealPresentacionPropuestas != null ? Convert.ToString(vTiemposReales.FRealPresentacionPropuestas).Substring(0, 10) : string.Empty;
            }

            if (vBitacora != null)
            {
                //indicadores
                char car = 'F';
                char car2 = 'M';
                int cont = 0;
                foreach (var item in vBitacora)
                {

                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.UltimaAccion;
                    if (Convert.ToChar(car2) == 'Z')
                    {
                        if (cont < 20)
                        {
                            car = 'G';
                            car2 = 'A';
                        }
                        else
                        {
                            car = 'H';
                            car2 = 'A';
                        }
                    }
                    else
                    {
                        car2++;
                    }
                    pestanaInit.Cell("" + Convert.ToChar(car) + "" + Convert.ToChar(car2) + "5").Value = item.ProximaAccion;

                    //car2++;
                    if (Convert.ToChar(car2) == 'Z')
                    {
                        if (cont < 20)
                        {
                            car = 'G';
                            car2 = 'A';
                        }
                        else
                        {
                            car = 'H';
                            car2 = 'A';
                        }
                    }
                    else
                    {
                        car2++;
                    }

                    if (cont == 25) break;

                    cont++;
                }
            }

            if (ProcesoSeleccion != null)
            {
                foreach (var item in ProcesoSeleccion)
                {
                    pestanaInit.Cell("HK5").Value = item.NumeroDelProceso;
                    pestanaInit.Cell("HL5").Value = item.ObjetoContrato;
                    pestanaInit.Cell("HM5").Value = item.EstadoDelProceso;
                    pestanaInit.Cell("HN5").Value = item.FechaAdjudicacionDelProceso == null ? string.Empty : item.FechaAdjudicacionDelProceso.Value.ToString("dd/MM/yyyy");
                    pestanaInit.Cell("HO5").Value = item.Contratista;
                    pestanaInit.Cell("HP5").Value = item.ValorPresupuestadoInicial.ToString(); 
                    pestanaInit.Cell("HQ5").Value = item.ValorAdjudicado.ToString();
                    pestanaInit.Cell("HR5").Value = item.PlazoDeEjecucion;
                    pestanaInit.Cell("HS5").Value = item.Observacion;
                    pestanaInit.Cell("HT5").Value = item.URLDelProceso;

                    break;
                }

            }
            if (vCierres != null)
            {
                pestanaInit.Cell("HU5").Value = ModalidadyCierresAnio[0].AnioCierre;
                foreach (var item in vCierres)
                {                    
                    pestanaInit.Cell("HV5").Value = item.FechaEjecucion == null ? string.Empty : item.FechaEjecucion.ToString("dd/MM/yyyy");
                    pestanaInit.Cell("HW5").Value = item.Descripcion;
                }
            }

            if (pestanaInit != null)
                fileResult.AddWorksheet(pestanaInit);

            string mes = DateTime.Now.Month.ToString();
            if (DateTime.Now.Month < 10)
            {
                mes = "0" + mes;
            }
            var id = "ConsultaSeguimiento_" + DateTime.Now.Day + "" + mes + "" + DateTime.Now.Year + ".xlsx";
            var pathResult = HttpContext.Current.Server.MapPath(@"~\Seguimientos");// System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"];

            if (!Directory.Exists(pathResult))
            {
                DirectoryInfo di = Directory.CreateDirectory(pathResult);
            }

            fileResult.SaveAs(Path.Combine(pathResult, id));
            result = id;
            return result;
        }


        /// <summary>
        /// Método para consultar cierres por vigencia por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de CierreEstudiosPorVigencia</returns>
        public List<CierreEstudiosPorVigencia> ConsultarCierresEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarCierresEstudio(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar información proceso selección por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de InformacionProcesoSeleccion</returns>
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccion(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarProcesoSeleccion(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de BitacoraAcciones</returns>
        public List<BitacoraAcciones> ConsultarBitacoraAccion(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarBitacoraAccion(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de TiempoEntreActividadesConsulta</returns>
        public List<TiempoEntreActividadesConsulta> ConsultarTiempos(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {


            try
            {
                return vResultadoEstudioSectorDAL.ConsultarTiempos(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de BitacoraAcciones</returns>
        public List<ResultadoEstudioSector> ConsultarResultado(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarResultado(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar gestiones por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de GestionesEstudioSectorCostos</returns>
        public List<GestionesEstudioSectorCostos> ConsultarGestionEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarGestionEstudio(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar registro inicial id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de RegistroSolicitudEstudioSectoryCaso</returns>
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroInicial(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return vResultadoEstudioSectorDAL.ConsultarRegistroInicial(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        }
}