﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ConsultaDependenciasBLL
    {
        private ConsultaDependenciasDAL vConsultaDependenciasDAL;
        public ConsultaDependenciasBLL()
        {
            vConsultaDependenciasDAL = new ConsultaDependenciasDAL();
        }

        /// <summary>
        /// Método para listar dependencias
        /// </summary>
        /// <param name="pDireccionSolicitante">Dependencia solicitante</param>
        /// <param name="pVigencia">Año de vigencia</param>
        public List<ConsultaDependencias> ConsultarConsultaDependenciass(string pDireccionSolicitante, int? pVigencia)
        {
            try
            {
                return vConsultaDependenciasDAL.ConsultarConsultaDependenciass(pDireccionSolicitante, pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    } 
}
