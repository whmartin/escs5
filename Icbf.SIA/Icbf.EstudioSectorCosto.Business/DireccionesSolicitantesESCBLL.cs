using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class DireccionesSolicitantesESCBLL
    {
        private DireccionesSolicitantesESCDAL vDireccionesSolicitantesESCDAL;
        public DireccionesSolicitantesESCBLL()
        {
            vDireccionesSolicitantesESCDAL = new DireccionesSolicitantesESCDAL();
        }
        public int InsertarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                return vDireccionesSolicitantesESCDAL.InsertarDireccionesSolicitantesESC(pDireccionesSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                return vDireccionesSolicitantesESCDAL.ModificarDireccionesSolicitantesESC(pDireccionesSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                return vDireccionesSolicitantesESCDAL.EliminarDireccionesSolicitantesESC(pDireccionesSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DireccionesSolicitantesESC ConsultarDireccionesSolicitantesESC(int pIdDireccionesSolicitantes)
        {
            try
            {
                return vDireccionesSolicitantesESCDAL.ConsultarDireccionesSolicitantesESC(pIdDireccionesSolicitantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DireccionesSolicitantesESC> ConsultarDireccionesSolicitantesESCs(String pDireccionSolicitante, String pDescripcion, int? pEstado)
        {
            try
            {
                return vDireccionesSolicitantesESCDAL.ConsultarDireccionesSolicitantesESCs(pDireccionSolicitante, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        

        public Boolean ValidarDireccionesSolicitantesESC(String pDireccionSolicitante)
        {
            try
            {
                return vDireccionesSolicitantesESCDAL.ValidarDireccionesSolicitantesESC(pDireccionSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        
    }
}
