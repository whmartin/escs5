using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ResponsablesESCBLL
    {
        private ResponsablesESCDAL vResponsablesESCDAL;
        public ResponsablesESCBLL()
        {
            vResponsablesESCDAL = new ResponsablesESCDAL();
        }
        public int InsertarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                return vResponsablesESCDAL.InsertarResponsablesESC(pResponsablesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                return vResponsablesESCDAL.ModificarResponsablesESC(pResponsablesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                return vResponsablesESCDAL.EliminarResponsablesESC(pResponsablesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ResponsablesESC ConsultarResponsablesESC(int pIdResponsable)
        {
            try
            {
                return vResponsablesESCDAL.ConsultarResponsablesESC(pIdResponsable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ResponsablesESC> ConsultarResponsablesESCs(String pResponsable, int? pEstado)
        {
            try
            {
                return vResponsablesESCDAL.ConsultarResponsablesESCs(pResponsable, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarResponsablesESC(String pResponsable)
        {
            try
            {
                return vResponsablesESCDAL.ValidarResponsablesESC(pResponsable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
