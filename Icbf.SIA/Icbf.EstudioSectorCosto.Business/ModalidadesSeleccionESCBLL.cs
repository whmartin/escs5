using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class ModalidadesSeleccionESCBLL
    {
        private ModalidadesSeleccionESCDAL vModalidadesSeleccionESCDAL;
        public ModalidadesSeleccionESCBLL()
        {
            vModalidadesSeleccionESCDAL = new ModalidadesSeleccionESCDAL();
        }
        public int InsertarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                return vModalidadesSeleccionESCDAL.InsertarModalidadesSeleccionESC(pModalidadesSeleccionESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                return vModalidadesSeleccionESCDAL.ModificarModalidadesSeleccionESC(pModalidadesSeleccionESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                return vModalidadesSeleccionESCDAL.EliminarModalidadesSeleccionESC(pModalidadesSeleccionESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ModalidadesSeleccionESC ConsultarModalidadesSeleccionESC(int pIdModalidadSeleccion)
        {
            try
            {
                return vModalidadesSeleccionESCDAL.ConsultarModalidadesSeleccionESC(pIdModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadesSeleccionESC> ConsultarModalidadesSeleccionESCs(String pModalidadSeleccion, String pDescripcion, int? pEstado,string TipoModalidad=null)
        {
            try
            {
                return vModalidadesSeleccionESCDAL.ConsultarModalidadesSeleccionESCs(pModalidadSeleccion, pDescripcion, pEstado, TipoModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarModalidadesSeleccionESC(String pModalidadSeleccion)
        {
            try
            {
                return vModalidadesSeleccionESCDAL.ValidarModalidadesSeleccionESC(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
