using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.EstudioSectorCosto.DataAccess;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Business
{
    public class AreasSolicitantesESCBLL
    {
        private AreasSolicitantesESCDAL vAreasSolicitantesESCDAL;
        public AreasSolicitantesESCBLL()
        {
            vAreasSolicitantesESCDAL = new AreasSolicitantesESCDAL();
        }
        public int InsertarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                return vAreasSolicitantesESCDAL.InsertarAreasSolicitantesESC(pAreasSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                return vAreasSolicitantesESCDAL.ModificarAreasSolicitantesESC(pAreasSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                return vAreasSolicitantesESCDAL.EliminarAreasSolicitantesESC(pAreasSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AreasSolicitantesESC ConsultarAreasSolicitantesESC(int pIdAreasSolicitantes)
        {
            try
            {
                return vAreasSolicitantesESCDAL.ConsultarAreasSolicitantesESC(pIdAreasSolicitantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AreasSolicitantesESC> ConsultarAreasSolicitantesESCs(String pAreaSolicitante, int? pIdDireccionSolicitanteESC, String pDescripcion, int? pEstado)
        {
            try
            {
                return vAreasSolicitantesESCDAL.ConsultarAreasSolicitantesESCs(pAreaSolicitante, pIdDireccionSolicitanteESC, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarAreasSolicitantesESC(String pAreaSolicitante, int pIdDireccionSolicitanteESC)
        {
            try
            {
                return vAreasSolicitantesESCDAL.ValidarAreasSolicitantesESC(pAreaSolicitante, pIdDireccionSolicitanteESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
