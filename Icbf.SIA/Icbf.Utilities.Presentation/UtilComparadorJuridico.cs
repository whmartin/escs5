﻿namespace Icbf.Utilities.Presentation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI.WebControls;

    public static class UtilComparadorJuridico
    {
        private const string Seleccione = "Seleccione...";
        /// <summary>
        /// Llena un DropDownList con el origen de datos correspondiente
        /// </summary>
        /// <param name="pComboBox">DropDownList donde se llenara la informacion</param>
        /// <param name="pDataSource">Origen de datos representado en una lista de entidad BaseDto</param>
        public static void LlenarDropDownList(DropDownList pComboBox, object pDataSource)
        {
            try
            {
                pComboBox.DataValueField = "Id";
                pComboBox.DataTextField = "Name";
                pComboBox.DataSource = pDataSource;
                pComboBox.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Util.LlenarDropDownList: " + ex.Message);
            }
        }

        /// <summary>
        /// Llena el listado de RadioButton con la informacion correspondiente
        /// </summary>
        /// <param name="pRadioButonList">RadioButtonList donde se llenara la informacion</param>
        /// <param name="pDataSource">Origen de datos representado en una lista de entidad BaseDto</param>
        public static void LlenarRadioButtonList(RadioButtonList pRadioButonList, object pDataSource)
        {
            try
            {
                pRadioButonList.DataValueField = "Id";
                pRadioButonList.DataTextField = "Name";
                pRadioButonList.DataSource = pDataSource;
                pRadioButonList.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Util.LlenarRadioButtonList: " + ex.Message);
            }
        }

        /// <summary>
        /// Convierte una lista de elementos BaseDto a una lista de elementos para un DropDownList
        /// </summary>
        /// <param name="pLista">Lista a convertir</param>
        /// <param name="pSelect">Agrega un elemento con el valor "SELECCIONE"</param>
        /// <param name="pValSelect">Valor predeterminado para el campo "VALUE" de la lista</param>
        /// <param name="pTextSelect"></param>
        /// <param name="pOrdenar">Ordena la lista por orden alfabetico</param>
        public static List<BaseDto> ToSelectedList(List<BaseDto> pLista, bool pSelect, string pValSelect = "-1", bool pOrdenar = false, string pTextSelect = null)
        {
            List<BaseDto> result;
            try
            {
                var select = Seleccione;
                result = pLista.Select(x => new BaseDto {Name = x.Name.ToUpper().Trim(), Id = x.Id}).ToList();
                if (pOrdenar) { result = result.OrderBy(x => x.Name).ToList(); }
                if (!string.IsNullOrEmpty(pTextSelect)) { select = pTextSelect; }
                if (pSelect) { result.Insert(0, new BaseDto { Name = select.ToUpper(), Id = pValSelect }); }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Util.ToUpperList: " + ex.Message);
            }
            return result;
        }

        public static string QuitarCaracteresEspeciales(string inputString)
        {
            Regex a = new Regex("[Á|à|ä|â]", RegexOptions.Compiled);
            Regex e = new Regex("[É|è|ë|ê]", RegexOptions.Compiled);
            Regex i = new Regex("[Í|ì|ï|î]", RegexOptions.Compiled);
            Regex o = new Regex("[Ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex u = new Regex("[Ú|ù|ü|û]", RegexOptions.Compiled);
            Regex n = new Regex("[ñ|Ñ]", RegexOptions.Compiled);
            inputString = a.Replace(inputString, "A");
            inputString = e.Replace(inputString, "E");
            inputString = i.Replace(inputString, "I");
            inputString = o.Replace(inputString, "O");
            inputString = u.Replace(inputString, "U");
            inputString = n.Replace(inputString, "N");

            return inputString;
        }

        /// <summary>
        /// Permite convertir Strings en números por metodo de extensión
        /// </summary>
        /// <param name="pNumero"></param>
        /// <returns></returns>
        public static int ConvertirNumeroBase32(this string pNumero)
        {
            try
            {
                return Convert.ToInt32(pNumero);
            }
            catch (Exception)
            {
                return 0;// Retorna 0 cuando genera error
            }

        }
    }
}