﻿namespace Icbf.Utilities.Presentation
{
    public class BaseDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
