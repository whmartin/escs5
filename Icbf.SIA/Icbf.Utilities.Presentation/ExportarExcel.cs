﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;

/// <summary>
/// Descripción breve de ExportarExcel
/// </summary>
public class ExportarExcel
{
    public ExportarExcel()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }
    public void ExportarGridViewExcel(GridView Grilla, Page Forma, Boolean OcultarPrimeraColumna, String NombreReporte)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Page pagina = new Page();

        HttpResponse httpRespuesta = new HttpResponse(htw);

        if (OcultarPrimeraColumna) Grilla.Columns[0].Visible = false;

        Grilla.EnableViewState = false;
        Grilla.AllowPaging = false;
        Grilla.Visible = true;

        HtmlForm vForma = new HtmlForm();

        pagina.EnableEventValidation = false;
        pagina.DesignerInitialize();
        pagina.Controls.Add(vForma);

        vForma.Controls.Add(Grilla);
        pagina.RenderControl(htw);

        Forma.Response.Clear();
        Forma.Response.Buffer = true;
        Forma.Response.ContentType = "application/octet-stream";
        Forma.Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreReporte + ".xls");
        Forma.Response.Charset = "UTF-8";
        Forma.Response.ContentEncoding = Encoding.Default;
        Forma.Response.Write(sb.ToString());
        Forma.Response.End();

        sw = null;
        htw = null;
        vForma = null;
    }
    private GridView PresentacionGrilla(GridView Tabla)
    {
        //presentación del titulo de la tabla
        Tabla.HeaderRow.Style.Add("background-color", "#FFFFFF");
        for (int i = 0; i < Tabla.Columns.Count; i++)
        {
            Tabla.HeaderRow.Cells[i].Style.Add("background-color", "green");
        }

        for (int i = 0; i < Tabla.Rows.Count; i++)
        {
            GridViewRow row = Tabla.Rows[i];
            row.BackColor = System.Drawing.Color.White;
            row.Attributes.Add("class", "textmode");
            if (i % 2 != 0)
            {
                for (int j = 0; j < Tabla.Columns.Count; j++)
                {
                    row.Cells[j].Style.Add("background-color", "#C2D69B");
                }
            }
            Tabla.FooterRow.Style.Add("background-color", "#FFFFFF");
        }
        for (int i = 0; i < Tabla.Columns.Count; i++)
        {
            Tabla.FooterRow.Cells[i].Style.Add("background-color", "green");
        }
        return Tabla;
    }

    public void ExportToExcel(DataTable dt, string nombreArchivo, string RutaDirectorio)
    {

        if (dt.Rows.Count > 0)
        {
            var rancomp = new Random();
            string complementoArch = DateTime.Now.ToString("yyyyMMdd_HHmmss_") + rancomp.Next(100);
            string filename = complementoArch + "_" + nombreArchivo + ".xls";
            using (System.IO.StringWriter tw = new System.IO.StringWriter())
            {
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.

                StreamWriter archivoSalida = null;

                string rutaArchivo = RutaDirectorio + filename;
                try
                {
                    archivoSalida = new StreamWriter(rutaArchivo, false, System.Text.Encoding.Unicode);
                    archivoSalida.Write(tw.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el archivo " + nombreArchivo, ex);
                }
                finally
                {
                    if (archivoSalida != null)
                        archivoSalida.Close();
                }

            }

        }
    }

}