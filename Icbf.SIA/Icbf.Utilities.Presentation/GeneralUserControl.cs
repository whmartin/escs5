﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Session;

namespace Icbf.Utilities.Presentation
{
    public class GeneralUserControl : System.Web.UI.UserControl
    {

        protected void SetSessionParameter(String pParametro, String pValor)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set(pParametro, pValor);
        }

        //protected void SetSessionParameter(String pParametro, Object pValor)
        //{
        //    ValidateSession();
        //    ((Parameters)Session["sessionParameters"]).Set(pParametro, pValor);
        //}


        public void ValidateSession()
        {

            if (Session["sessionParameters"] == null)
            {
                Response.Write("<script>window.open('../../../Default.aspx','_parent');</script>");
            }
        }
        private Hashtable GetSessionPermissions()
        {
            ValidateSession();
            return (Hashtable)((Parameters)Session["sessionParameters"]).Get("App.Permissions");
        }
        public Usuario GetSessionUser()
        {
            ValidateSession();
            return (Usuario)((Parameters)Session["sessionParameters"]).Get("App.User");
        }
        public List<Modulo> GetSessionMod()
        {
            ValidateSession();
            return (List<Modulo>)((Parameters)Session["sessionParameters"]).Get("App.Mod");
        }
        public List<Programa> GetSessionPro(String pModulo)
        {
            ValidateSession();
            return (List<Programa>)((Parameters)Session["sessionParameters"]).Get("App.Pro" + pModulo);
        }
        protected void SetSessionParameter(String pParametro, Object pValor)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set(pParametro, pValor);
        }
        protected Object GetSessionParameter(String pParametro)
        {
            ValidateSession();
            if (((Parameters)Session["sessionParameters"]).Get(pParametro) == null)
                return "";
            else
                return ((Parameters)Session["sessionParameters"]).Get(pParametro);
        }
        protected void RemoveSessionParameter(String pParametro)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Remove(pParametro);
        }
        protected void ClearSessionParameters()
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Clear();
        }
        protected int CountSessionParameters()
        {
            ValidateSession();
            return ((Parameters)Session["sessionParameters"]).Count();
        }
        protected Hashtable GetSessionTable()
        {
            ValidateSession();
            return ((Parameters)Session["sessionParameters"]).Tabla;
        }
        public void StartSessionParameters()
        {
            if (Session["sessionParameters"] == null)
                Session["sessionParameters"] = new Parameters();
        }
        protected void StopSessionParameters()
        {
            Session["sessionParameters"] = null;
            Session.Abandon();
        }
      
        protected void NavigateTo(String pPath)
        {
            Response.Redirect(pPath);
        }
        
        protected void NavigateTo(String pPath, bool pEndResponse)
        {
            Response.Redirect(pPath, pEndResponse);
        }
    }
}
