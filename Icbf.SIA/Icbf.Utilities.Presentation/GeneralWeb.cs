﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.IO;
using Icbf.Utilities.Session;
using System.Collections;
using Icbf.Utilities.Exceptions;
using System.Web.Security;
using System.Web.UI;
using Icbf.SIA.Entity;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Service;
using Icbf.Seguridad.Service;
using Icbf.Mostrencos.Entity;

namespace Icbf.Utilities.Presentation
{
    public class GeneralWeb : System.Web.UI.Page
    {
        protected enum SolutionPage { Add = 1, List = 2, Detail = 3, Edit = 4 };
        protected SolutionPage vSolutionPage;
        MostrencosService vMostrencosService;

        public GeneralWeb()
        {
        }
        protected void SetSessionParameter(String pParametro, String pValor)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set(pParametro, pValor);
        }

        protected void SetSessionUser(Usuario pUser)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set("App.User", pUser);
            ClearMenuSettings();
            SetSessionPermissions(pUser);
        }
        public void SetSessionMod(List<Modulo> pModulos)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set("App.Mod", pModulos);
        }
        public void ClearMenuSettings()
        {
            ((Parameters)Session["sessionParameters"]).Remove("App.Mod");
            ((Parameters)Session["sessionParameters"]).Remove("App.Pro");
        }
        public void SetSessionPro(List<Programa> pProgramas, String pModulo)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set("App.Pro" + pModulo, pProgramas);
        }
        private void SetSessionPermissions(Usuario pUser)
        {
            ValidateSession();
            Hashtable vTablaPermisos = new Hashtable();
            /* Hash para informacion de Auditoria */
            Hashtable vTablaAuditoria = new Hashtable();
            List<Programa> vListaPrograma = new List<Programa>();
            string[] vRoles = Roles.GetRolesForUser(pUser.NombreUsuario);
            //vListaPrograma = new SeguridadService().ConsultarProgramasConPermiso(vRoles[0]);
            vListaPrograma = new SeguridadService().ConsultarProgramasConPermiso(vRoles);

            foreach (Programa vPrograma in vListaPrograma)
            {
                if (vPrograma.CodigoPrograma != string.Empty)
                {
                    if (!vTablaPermisos.ContainsKey(vPrograma.CodigoPrograma.ToUpper()))
                    {
                        vTablaPermisos.Add(vPrograma.CodigoPrograma.ToUpper(), vPrograma.Permiso);
                        vTablaAuditoria.Add(vPrograma.CodigoPrograma.ToUpper(), vPrograma.GeneraLog);
                    }
                }
                else
                {
                    if (!vTablaPermisos.ContainsKey(vPrograma.IdPrograma))
                    {
                        vTablaPermisos.Add(vPrograma.IdPrograma, vPrograma.Permiso);
                        vTablaAuditoria.Add(vPrograma.IdPrograma, vPrograma.GeneraLog);
                    }
                }
            }
            ((Parameters)Session["sessionParameters"]).Set("App.Permissions", vTablaPermisos);
            ((Parameters)Session["sessionParameters"]).Set("App.Audit", vTablaAuditoria);
        }
        private Hashtable GetSessionPermissions()
        {
            ValidateSession();
            return (Hashtable)((Parameters)Session["sessionParameters"]).Get("App.Permissions");
        }
        public Usuario GetSessionUser()
        {
            ValidateSession();

            return (Usuario)((Parameters)Session["sessionParameters"]).Get("App.User");
        }
        public List<Modulo> GetSessionMod()
        {
            ValidateSession();
            return (List<Modulo>)((Parameters)Session["sessionParameters"]).Get("App.Mod");
        }
        public List<Programa> GetSessionPro(String pModulo)
        {
            ValidateSession();
            return (List<Programa>)((Parameters)Session["sessionParameters"]).Get("App.Pro" + pModulo);
        }
        protected void SetSessionParameter(String pParametro, Object pValor)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Set(pParametro, pValor);
        }
        protected Object GetSessionParameter(String pParametro)
        {
            ValidateSession();
            if (((Parameters)Session["sessionParameters"]).Get(pParametro) == null)
                return "";
            else
                return ((Parameters)Session["sessionParameters"]).Get(pParametro);
        }
        protected void RemoveSessionParameter(String pParametro)
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Remove(pParametro);
        }
        protected void ClearSessionParameters()
        {
            ValidateSession();
            ((Parameters)Session["sessionParameters"]).Clear();
        }
        protected int CountSessionParameters()
        {
            ValidateSession();
            return ((Parameters)Session["sessionParameters"]).Count();
        }
        protected Hashtable GetSessionTable()
        {
            ValidateSession();
            return ((Parameters)Session["sessionParameters"]).Tabla;
        }
        public void StartSessionParameters()
        {
            if (Session["sessionParameters"] == null)
                Session["sessionParameters"] = new Parameters();
        }
        protected void StopSessionParameters()
        {
            Session["sessionParameters"] = null;
            Session.Abandon();
        }
        protected int PageSize()
        {
            Int32 vPageSize;
            if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
            {
                throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
            }
            return vPageSize;
        }
        protected String EmptyDataText()
        {
            String vEmptyDataText;
            if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
            {
                throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
            }
            vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
            return vEmptyDataText;
        }
        protected void NavigateTo(SolutionPage pSolutionPage)
        {
            switch (pSolutionPage.ToString())
            {
                case "Add":
                    Response.Redirect("Add.aspx");
                    break;
                case "List":
                    Response.Redirect("List.aspx");
                    break;
                case "Detail":
                    Response.Redirect("Detail.aspx", true);
                    break;
                case "Edit":
                    Response.Redirect("Add.aspx?oP=E");
                    break;
            }
        }
        protected void NavigateTo(String pPath)
        {
            Response.Redirect(pPath);
        }
        protected void NavigateTo(SolutionPage pSolutionPage, bool pEndResponse)
        {
            switch (pSolutionPage.ToString())
            {
                case "Add":
                    Response.Redirect("Add.aspx", pEndResponse);
                    break;
                case "List":
                    Response.Redirect("List.aspx", pEndResponse);
                    break;
                case "Detail":
                    Response.Redirect("Detail.aspx", pEndResponse);
                    break;
                case "Edit":
                    Response.Redirect("Add.aspx?oP=E", pEndResponse);
                    break;
            }
        }
        protected void NavigateTo(String pPath, bool pEndResponse)
        {
            Response.Redirect(pPath, pEndResponse);
        }

        protected bool ValidateUser(String pUserName, String pUserPassword)
        {
            Boolean vValidate = Membership.ValidateUser(pUserName, pUserPassword);
            if (vValidate)
            {
                SetUser(pUserName);
            }
            return vValidate;
        }

        protected void SetUser(String pUserName)
        {
            StartSessionParameters();
            Usuario vUser = ConsultarUsuario(pUserName);
            if (vUser.IdUsuario == 0)
                throw new Exception("El usuario no se encuentra registrado");
            SetSessionUser(vUser);
        }
        protected String GetAuditLabel(String pUsuarioCreacion, DateTime pFechaCreacion, String pUsuarioModificacion, DateTime pFechaModificacion)
        {
            if (pUsuarioModificacion == null)
                return "Creado: " + pUsuarioCreacion + " el " + pFechaCreacion.ToString();
            else
                return "Creado: " + pUsuarioCreacion + " el " + pFechaCreacion.ToString() + " - Modificado: " + pUsuarioModificacion + " el " + pFechaModificacion.ToString();
        }
        protected Boolean ValidateAccess(MasterPage pMaster, String pPageToValidate, SolutionPage pSolutionPage)
        {
            ValidateSession();

            if (GetSessionUser() != null)
            {
                if (GetSessionPermissions()[pPageToValidate.ToUpper()] != null)
                {
                    VisualizarBotones(pMaster, GetSessionPermissions()[pPageToValidate.ToUpper()].ToString());
                    if (validatePageAccess(GetSessionPermissions()[pPageToValidate.ToUpper()].ToString(), pSolutionPage))
                    {
                        return true;
                    }
                    else
                    {
                        Response.Write("<script>window.open('../../../Default.aspx','_parent');</script>");
                        //NavigateTo("../../../Default.aspx", false);
                        //NavigateTo("../../../General/General/FinSesion.htm", false);
                        return false;
                    }
                }
                else
                {
                    Response.Write("<script>window.open('../../../Default.aspx','_parent');</script>");
                    //NavigateTo("../../../Default.aspx", false);
                    //NavigateTo("../../../General/General/FinSesion.htm", false);
                    return false;
                }
            }
            else
            {
                Response.Write("<script>window.open('../../../Default.aspx','_parent');</script>");
                //NavigateTo("../../../Default.aspx", false);
                //NavigateTo("../../../General/FinSesion.htm", false);
                return false;
            }
        }
        protected Boolean ValidateAccessDelete(MasterPage pMaster, String pPageToValidate, SolutionPage pSolutionPage)
        {
            ValidateSession();

            if (GetSessionUser() != null)
            {
                if (GetSessionPermissions()[pPageToValidate.ToUpper()] != null)
                {
                    if (validatePageDelete(GetSessionPermissions()[pPageToValidate.ToUpper()].ToString(), pSolutionPage))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        protected Boolean ValidateAccessEdit(MasterPage pMaster, String pPageToValidate, SolutionPage pSolutionPage)
        {
            ValidateSession();

            if (GetSessionUser() != null)
            {
                if (GetSessionPermissions()[pPageToValidate.ToUpper()] != null)
                {
                    if (validatePageEdit(GetSessionPermissions()[pPageToValidate.ToUpper()].ToString(), pSolutionPage))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private Boolean validatePageDelete(String pNivelAcceso, SolutionPage pSolutionPage)
        {
            ValidateSession();
            Boolean vAcceso = false;
            switch (pNivelAcceso)
            {
                case "2"://Consultar: No - Eliminar: Si - Modificar: No - Insertar: No.
                    vAcceso = true;
                    break;
                case "3"://Consultar: Si - Eliminar: Si - Modificar: No - Insertar: No.
                    vAcceso = true;
                    break;
                case "6"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: No.
                    vAcceso = true;
                    break;
                case "7"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: No.
                    vAcceso = true;
                    break;
                case "10"://Consultar: No - Eliminar: Si - Modificar: No - Insertar: Si.
                    vAcceso = true;
                    break;
                case "11"://Consultar: Si - Eliminar: Si - Modificar: No - Insertar: Si.
                    vAcceso = true;
                    break;
                case "14"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
                case "15"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
                default: break;
            }
            return vAcceso;
        }
        private Boolean validatePageAccess(String pNivelAcceso, SolutionPage pSolutionPage)
        {
            ValidateSession();
            Boolean vAcceso = true;
            switch (pNivelAcceso)
            {
                case "0"://Consultar: No - Eliminar: No - Modificar: No - Insertar: No.
                    vAcceso = false;
                    break;
                case "1"://Consultar: Si - Eliminar: No - Modificar: No - Insertar: No.
                    if (pSolutionPage != SolutionPage.List && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "2"://Consultar: No - Eliminar: Si - Modificar: No - Insertar: No.
                    if (pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "3"://Consultar: Si - Eliminar: Si - Modificar: No - Insertar: No.
                    if (pSolutionPage != SolutionPage.List && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "4"://Consultar: No - Eliminar: No - Modificar: Si - Insertar: No.
                    if (pSolutionPage != SolutionPage.Edit && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "5"://Consultar: Si - Eliminar: No - Modificar: Si - Insertar: No.
                    if (pSolutionPage != SolutionPage.List && pSolutionPage != SolutionPage.Edit && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "6"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: No.
                    if (pSolutionPage != SolutionPage.Detail && pSolutionPage != SolutionPage.Edit)
                        vAcceso = false;
                    break;
                case "7"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: No.
                    if (pSolutionPage != SolutionPage.Detail && pSolutionPage != SolutionPage.Edit && pSolutionPage != SolutionPage.List)
                        vAcceso = false;
                    break;
                case "8"://Consultar: No - Eliminar: No - Modificar: No - Insertar: Si.
                    if (pSolutionPage != SolutionPage.Add)
                        vAcceso = false;
                    break;
                case "9"://Consultar: Si - Eliminar: No - Modificar: No - Insertar: Si.
                    if (pSolutionPage != SolutionPage.List && pSolutionPage != SolutionPage.Add && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "10"://Consultar: No - Eliminar: Si - Modificar: No - Insertar: Si.
                    if (pSolutionPage != SolutionPage.Detail && pSolutionPage != SolutionPage.Add)
                        vAcceso = false;
                    break;
                case "11"://Consultar: Si - Eliminar: Si - Modificar: No - Insertar: Si.
                    if (pSolutionPage != SolutionPage.Detail && pSolutionPage != SolutionPage.Add && pSolutionPage != SolutionPage.List)
                        vAcceso = false;
                    break;
                case "12"://Consultar: No - Eliminar: No - Modificar: Si - Insertar: Si.
                    if (pSolutionPage != SolutionPage.Edit && pSolutionPage != SolutionPage.Add && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "13"://Consultar: Si - Eliminar: No - Modificar: Si - Insertar: Si.
                    if (pSolutionPage != SolutionPage.List && pSolutionPage != SolutionPage.Add && pSolutionPage != SolutionPage.Edit && pSolutionPage != SolutionPage.Detail)
                        vAcceso = false;
                    break;
                case "14"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: Si.
                    if (pSolutionPage != SolutionPage.Detail && pSolutionPage != SolutionPage.Add && pSolutionPage != SolutionPage.Edit)
                        vAcceso = false;
                    break;
                case "15"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
            }
            return vAcceso;
        }
        private Boolean validatePageEdit(String pNivelAcceso, SolutionPage pSolutionPage)
        {
            ValidateSession();
            Boolean vAcceso = false;
            switch (pNivelAcceso)
            {
                case "4"://Consultar: No - Eliminar: No - Modificar: Si - Insertar: No.
                    vAcceso = true;
                    break;
                case "5"://Consultar: Si - Eliminar: No - Modificar: Si - Insertar: No.
                    vAcceso = true;
                    break;
                case "6"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: No.
                    vAcceso = true;
                    break;
                case "7"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: No.
                    vAcceso = true;
                    break;
                case "12"://Consultar: No - Eliminar: No - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
                case "13"://Consultar: Si - Eliminar: No - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
                case "14"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
                case "15"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: Si.
                    vAcceso = true;
                    break;
                default: break;
            }
            return vAcceso;
        }
        private void VisualizarBotones(MasterPage pMaster, String pNivelAcceso)
        {
            ValidateSession();
            switch (pNivelAcceso)
            {
                case "0"://Consultar: No - Eliminar: No - Modificar: No - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnGuardar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "1"://Consultar: Si - Eliminar: No - Modificar: No - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnGuardar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "2"://Consultar: No - Eliminar: Si - Modificar: No - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "3"://Consultar: Si - Eliminar: Si - Modificar: No - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "4"://Consultar: No - Eliminar: No - Modificar: Si - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "5"://Consultar: Si - Eliminar: No - Modificar: Si - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "6"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "7"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: No.
                    pMaster.FindControl("btnNuevo").Visible = false;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "8"://Consultar: No - Eliminar: No - Modificar: No - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "9"://Consultar: Si - Eliminar: No - Modificar: No - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "10"://Consultar: No - Eliminar: Si - Modificar: No - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "11"://Consultar: Si - Eliminar: Si - Modificar: No - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = false;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "12"://Consultar: No - Eliminar: No - Modificar: Si - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "13"://Consultar: Si - Eliminar: No - Modificar: Si - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = false;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
                case "14"://Consultar: No - Eliminar: Si - Modificar: Si - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = false;
                    break;
                case "15"://Consultar: Si - Eliminar: Si - Modificar: Si - Insertar: Si.
                    pMaster.FindControl("btnNuevo").Visible = true;
                    pMaster.FindControl("btnEditar").Visible = true;
                    pMaster.FindControl("btnEliminar").Visible = true;
                    pMaster.FindControl("btnBuscar").Visible = true;
                    break;
            }
        }
        protected Usuario ConsultarUsuario(String pUserName)
        {
            ValidateSession();
            SIAService vRUBOService = new SIAService();
            return vRUBOService.ConsultarUsuario(pUserName);
        }
        public void ValidateSession()
        {

            if (Session["sessionParameters"] == null)
            {
                Response.Write("<script>window.open('" + ResolveUrl("~/Default.aspx") + "','_parent');</script>");
                //Response.Write("<script>window.open('../../../Default.aspx','_parent');</script>");
                //Response.Write("<script>window.open('~/Default.aspx','_parent');</script>");

            }
        }

        protected Usuario ConsultarUsuarioRecordarPass(String pUserName)
        {
            //ValidateSession();
            SIAService vRUBOService = new SIAService();
            return vRUBOService.ConsultarUsuario(pUserName);
        }
        private Hashtable GetSessionAudit()
        {
            ValidateSession();
            return (Hashtable)((Parameters)Session["sessionParameters"]).Get("App.Audit");
        }
        protected void InformacionAudioria(EntityAuditoria pAuditoria, String pPageToValidate, SolutionPage pSolutionPage)
        {
            try
            {

                ValidateSession();
                pAuditoria.IdRegistro = 0;
                pAuditoria.Usuario = GetSessionUser().IdUsuario.ToString();
                pAuditoria.Programa = pPageToValidate;
                pAuditoria.ParametrosOperacion = "";
                pAuditoria.Tabla = "";
                pAuditoria.DireccionIP = Request.UserHostAddress;
                pAuditoria.Navegador = String.Format("{0} - {1} - {2}", Request.Browser.Browser, Request.Browser.Version, Request.Browser.Platform);
                pAuditoria.ProgramaGeneraLog = (bool)GetSessionAudit()[pPageToValidate.ToUpper()];

                if (pSolutionPage == SolutionPage.Add)
                    pAuditoria.Operacion = "INSERT";
                else if (pSolutionPage == SolutionPage.Edit)
                    pAuditoria.Operacion = "UPDATE";
                else if (pSolutionPage == SolutionPage.Detail)
                    pAuditoria.Operacion = "DELETE";
                else
                    pAuditoria.Operacion = "UNREGISTERED";
            }
            catch
            {
            }
        }
        public void ObtenerAuditoria(String pPageName, String pPageRegister)
        {
            ClientScript.RegisterClientScriptBlock(GetType(), "Auditoria", "function OpenAudit() { myWindow = window.open('../../General/Auditoria/Detail.aspx?pI=" + pPageRegister + "&sI=" + pPageName + "', 'Auditoria', 'menubar=no,status=no,toolbar=no')}", true);//
        }

        public void SaveState(MasterPage vMaster, String pPrefijo)
        {
            ControlCollection vContolesBuscados = vMaster.FindControl("cphCont").FindControl("pnlConsulta").Controls;
            foreach (Control vContron in vContolesBuscados)
            {

                if (vContron is TextBox)
                {
                    SetSessionParameter(pPrefijo + "." + vContron.ClientID, ((TextBox)vContron).Text);
                }
                if (vContron is DropDownList)
                {
                    SetSessionParameter(pPrefijo + "." + vContron.ClientID, ((DropDownList)vContron).SelectedIndex);
                }
                if (vContron is RadioButton)
                {
                    SetSessionParameter(pPrefijo + "." + vContron.ClientID, ((RadioButton)vContron).Checked);
                }
                if (vContron is CheckBox)
                {
                    SetSessionParameter(pPrefijo + "." + vContron.ClientID, ((CheckBox)vContron).Checked);
                }
                if (vContron is RadioButtonList)
                {
                    SetSessionParameter(pPrefijo + "." + vContron.ClientID, ((RadioButtonList)vContron).SelectedValue);
                }
                if (vContron is CheckBoxList)
                {
                    SetSessionParameter(pPrefijo + "." + vContron.ClientID, ((CheckBoxList)vContron).SelectedValue);
                }

            }
        }

        public Boolean GetState(MasterPage vMaster, String pPrefijo)
        {

            Boolean vBuscar = false;

            ControlCollection vContolesBuscados = vMaster.FindControl("cphCont").FindControl("pnlConsulta").Controls;

            foreach (Control vContron in vContolesBuscados)
            {

                Object vValue = null;

                vValue = GetSessionParameter(pPrefijo + "." + vContron.ClientID);

                if (vValue != null)
                {

                    if (vValue.ToString() != "")
                    {

                        if (vContron is TextBox)
                        {

                            ((TextBox)vContron).Text = vValue.ToString();

                            vBuscar = true;

                        }

                        if (vContron is DropDownList)
                        {

                            ((DropDownList)vContron).SelectedIndex = -1;

                            ((DropDownList)vContron).SelectedIndex = Convert.ToInt32(vValue.ToString());

                            vBuscar = true;

                        }



                        if (vContron is RadioButton)
                        {

                            ((RadioButton)vContron).Checked = Convert.ToBoolean(vValue.ToString());

                            vBuscar = true;

                        }

                        if (vContron is CheckBox)
                        {

                            ((CheckBox)vContron).Checked = Convert.ToBoolean(vValue.ToString());

                            vBuscar = true;

                        }

                        if (vContron is RadioButtonList)
                        {

                            ((RadioButtonList)vContron).SelectedValue = vValue.ToString();

                            vBuscar = true;

                        }

                        if (vContron is CheckBoxList)
                        {

                            ((CheckBoxList)vContron).SelectedValue = vValue.ToString();

                            vBuscar = true;

                        }

                    }

                }

            }

            return vBuscar;

        }

        public virtual void MostrarMensajeErrorMasterPrincipal(string mensaje) { }

        public virtual void MostrarMensajeGuardadoMasterPrincipal(string mensaje) { }

        public virtual void LimpiarMensajeMasterPrincipal() { }

        public enum DocOperation { Add = 1, Update = 2, Delete = 3 };

        public void LimpiarControles(ControlCollection vListaControles)
        {
            try
            {
                foreach (Control vControl in vListaControles)
                {
                    if (vControl is TextBox)
                    {
                        ((TextBox)vControl).Text = "";
                    }
                    if (vControl is HiddenField)
                    {
                        ((HiddenField)vControl).Value = "";
                    }
                    if (vControl is DropDownList)
                    {
                        ((DropDownList)vControl).SelectedIndex = 0;
                    }
                    if (vControl is RadioButton)
                    {
                        ((RadioButton)vControl).Checked = false;
                    }
                    if (vControl is CheckBox)
                    {
                        ((CheckBox)vControl).Checked = false;
                    }
                    if (vControl is CheckBoxList)
                    {
                        foreach (ListItem vList in ((CheckBoxList)vControl).Items)
                        {
                            vList.Selected = false;
                        }
                    }
                    if (vControl is RadioButtonList)
                    {
                        foreach (ListItem vList in ((RadioButtonList)vControl).Items)
                        {
                            vList.Selected = false;
                        }
                    }
                    if (vControl is Panel)
                    {
                        LimpiarControles(vControl.Controls);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ControlCollection ObtenerContenedorMaster(MasterPage pMaster)
        {
            try
            {
                return pMaster.FindControl("cphCont").Controls;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetScriptCloseDialogCallback(string dialog, List<string> parametros = null)
        {
            string returnValues = "<script language='javascript'> ";

            if (dialog == string.Empty)
                dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');

            if (parametros != null)
            {
                returnValues += "var pObj = Array();";

                for (int i = 0; i < parametros.Count; i++)
                    returnValues += "pObj[" + (i) + "] = '" + parametros[i] + "';";

                returnValues += "parent.document.getElementById('hdLupa" + dialog + "').value = pObj;";
            }

            returnValues += " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rvCloseModalDialog", returnValues);
        }

        public void GetScriptCloseDialogScriptsCallback(string dialog, List<string> parametros = null)
        {
            string returnValues = "   <script src='../../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                               "   <script src='../../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                               "   <script src='../../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                               "   <script language='javascript'> ";

            if (dialog == string.Empty)
                dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');

            if (parametros != null)
            {
                returnValues += "var pObj = Array();";

                for (int i = 0; i < parametros.Count; i++)
                    returnValues += "pObj[" + (i) + "] = '" + parametros[i] + "';";

                returnValues += "parent.document.getElementById('hdLupa" + dialog + "').value = pObj;";
            }

            returnValues += " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rvCloseModalDialog", returnValues);
        }

        protected void SaveDocument(DocumentosSolicitadosDenunciaBien pDocumento)
        {
            try
            {
                vMostrencosService = new MostrencosService();

                if (pDocumento.IdDocumentosSolicitadosDenunciaBien == 0)
                {
                    int resultado = vMostrencosService.InsertarDocumentoSolicitadoDenunciaBien(pDocumento);

                    if (resultado == -1)
                        throw new Exception("El registro ya existe.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadDocument(int vIdDenunciaBien, GridView gvwDocumentacion)
        {
            try
            {
                vMostrencosService = new MostrencosService();

                // Se llena la grilla de documentos que se reciba
                var Data = vMostrencosService.ConsultarDocumentosXIdDenuncia(vIdDenunciaBien, 0);
                ViewState["SortedgvwDocumentacionRecibida"] = Data;
                gvwDocumentacion.DataSource = Data;
                gvwDocumentacion.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Carga el historio de denuncia
        /// </summary>
        /// <param name="vIdDenunciaBien">Id de la denuncia</param>
        /// <param name="gvwHistorico">Grilla donde se cargarian los datos</param>
        /// <out>IdHistoricoEstadoDenunciaBien</out>
        /// <out>IdDenunciaBien</out>
        /// <out>EstadoDenuncia</out>
        /// <out>UsuarioCrea</out>
        /// <out>FechaCrea</out>
        /// <out>UsuarioModifica</out>
        /// <out>FechaModifica</out>
        /// <out>NombreEstado</out>
        protected void LoadHistoricoDenuncia(int vIdDenunciaBien, GridView gvwHistorico)
        {

            try
            {
                vMostrencosService = new MostrencosService();

                List<HistoricoEstadosDenunciaBien> vHistorico = this.vMostrencosService.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(vIdDenunciaBien);
                foreach (HistoricoEstadosDenunciaBien item in vHistorico)
                {
                    Icbf.SIA.Entity.Usuario vUsuario = this.vMostrencosService.ConsultarDatosUsuario(item.IdUsuarioCrea);
                    item.NombreEstado = this.vMostrencosService.ConsultarEstadoDenunciaPorId(item.IdEstadoDenuncia).NombreEstadoDenuncia;
                    item.Responsable = vUsuario.PrimerNombre;
                    if (!string.IsNullOrEmpty(vUsuario.SegundoNombre))
                    {
                        item.Responsable += " " + vUsuario.SegundoNombre;
                    }

                    item.Responsable += " " + vUsuario.PrimerApellido;
                    if (!string.IsNullOrEmpty(vUsuario.SegundoApellido))
                    {
                        item.Responsable += " " + vUsuario.SegundoApellido;
                    }
                }
                gvwHistorico.DataSource = vHistorico;
                gvwHistorico.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected bool GuardarArchivos(int? pIdDenunciaBien, string fechaArchivo, FileUpload fulArchivoRecibido, string ruta)
        {
            try
            {
                HttpFileCollection files = Request.Files;
                string vFileName = string.Empty;

                if (files.Count > 0)
                {
                    string extFile = Path.GetExtension(fulArchivoRecibido.FileName).ToLower();
                    string extPdf = ".PDF".ToLower();
                    if (extFile != extPdf)
                    {
                        throw new Exception("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo PDF");
                    }
                    int vtamArchivo = (fulArchivoRecibido.FileBytes.Length / 1024);
                    if (vtamArchivo > 2048)
                    {
                        throw new Exception("El tamaño del archivo PDF permitido es de 2.048 KB");
                    }

                    int pPosition = 0;
                    HttpPostedFile file = files[pPosition];

                    if (file.ContentLength > 0)
                    {
                        string rutaParcial = string.Format("{0}\\{1}", pIdDenunciaBien, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + fechaArchivo + ".pdf");
                        string carpetaBase = string.Format("{0}{1}\\", Server.MapPath(ruta), pIdDenunciaBien);

                        string filePath = string.Format("{0}{1}", carpetaBase, System.IO.Path.GetFileNameWithoutExtension(file.FileName) + fechaArchivo + ".pdf");

                        bool existeCarpeta = System.IO.Directory.Exists(carpetaBase);

                        if (!existeCarpeta)
                        {
                            System.IO.Directory.CreateDirectory(carpetaBase);
                        }

                        vFileName = rutaParcial;
                        file.SaveAs(filePath);
                    }
                }

                return true;
            }
            catch (HttpRequestValidationException httpex)
            {
                throw new Exception("Se detectó un posible archivo peligroso." + httpex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
