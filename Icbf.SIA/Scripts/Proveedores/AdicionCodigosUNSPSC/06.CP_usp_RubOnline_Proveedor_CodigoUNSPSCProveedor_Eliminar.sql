USE [SIA]
GO

If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Eliminar')>0 
BEGIN
	DROP PROCEDURE usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Eliminar;
END

GO

-- =============================================
-- Author:		Luz Angela Borda
-- Create date:	14/05/2015 12:09 pm
-- Description:	Procedimiento almacenado que elimina un(a) Codigo UNSPSC de proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Eliminar]
	@CodUNSPSC INT
AS
BEGIN
	DELETE Proveedor.CodUNSPSC WHERE CodUNSPSC = @CodUNSPSC
END


