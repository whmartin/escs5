USE [SIA]
GO

If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad')>0 
BEGIN
	DROP PROCEDURE usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad;
END

GO

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) ClasedeEntidad

--Modificado por: Juan Carlos Valverde S�mano
--Fecha: 31/03/2014
--Descripci�n: Se agreg� el parametro  @IdTipoPersona, ya que el control de cambio 016
--requiere filtrar el contenido de la consulta de acuerdo al tipPersona, se agreg� tambi�n
-- el parametro @IdTipoSector para filtrar.

--Modificado por: Luz Angela Borda
--Fecha: 08/05/2015
--Descripci�n: Se agreg� el parametro  @IdRegimenTributario, ya que el control de cambio CC_PRO_BAS_15_0001
--requiere filtrar el contenido de la consulta de clase de entidad de acuerdo al regim�n tributario
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad]
	@IdTipodeActividad INT = NULL,
	@IdTipoEntidad INT = NULL, 
	@IdTipoPersona INT=NULL, 
	@IdTipoSector INT = NULL,
	@IdRegimenTributario INT = NULL
AS
BEGIN
	IF(@IdTipoPersona=1)
		BEGIN
			 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
			 FROM [Proveedor].[ClasedeEntidad] 
			 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
				   AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
				   AND Estado = 1 AND(Descripcion='PROFESIONALES LIBERALES' OR Descripcion='COMERCIANTE INDEPENDIENTE')
				   AND IdRegimenTributario <> 4
				   ORDER BY Descripcion ASC
		END
	ELSE IF(@IdTipoPersona =2)
		BEGIN
			IF(@IdTipoSector=1 AND @IdTipoEntidad=1 AND @IdTipodeActividad=1)
				BEGIN
					  SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
					  FROM [Proveedor].[ClasedeEntidad] 
					  WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
 							AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
							AND Estado = 1 AND(Descripcion!='PROFESIONALES LIBERALES' AND Descripcion!='COMERCIANTE INDEPENDIENTE')
							AND IdRegimenTributario <> 4
							ORDER BY Descripcion ASC
				END
			ELSE IF(@IdTipoSector=1 AND @IdTipoEntidad=1 AND @IdTipodeActividad=2)
				BEGIN
					IF  @IdRegimenTributario = 4 
						BEGIN
							  SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
							  FROM [Proveedor].[ClasedeEntidad] 
							  WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
 									AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
									AND Estado = 1 
									AND IdRegimenTributario <> 4
							  UNION
							  SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
							  FROM [Proveedor].[ClasedeEntidad] 
							  WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
 									AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
									AND Estado = 1 
									AND IdRegimenTributario = 4
									ORDER BY Descripcion ASC
						END
					ELSE
						BEGIN
							  SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
							  FROM [Proveedor].[ClasedeEntidad] 
							  WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
 									AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
									AND Estado = 1
									AND IdRegimenTributario <> 4 
									ORDER BY Descripcion ASC
						END				
				END
		END
	ELSE 
		BEGIN
			 SELECT IdClasedeEntidad, IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
			 FROM [Proveedor].[ClasedeEntidad] 
			 WHERE IdTipodeActividad = CASE WHEN @IdTipodeActividad IS NULL THEN IdTipodeActividad ELSE @IdTipodeActividad END 
				   AND IdTipodeEntidad = CASE WHEN @IdTipoEntidad IS NULL THEN IdTipodeEntidad ELSE @IdTipoEntidad END 
				   AND Estado = 1
				   AND IdRegimenTributario <> 4
				   ORDER BY Descripcion ASC
		END
END
