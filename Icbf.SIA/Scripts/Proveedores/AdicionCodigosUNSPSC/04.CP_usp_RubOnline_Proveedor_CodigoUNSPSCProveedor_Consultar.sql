USE [SIA]
GO

If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Consultar')>0 
BEGIN
	DROP PROCEDURE usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Consultar;
END

GO

-- =============================================
-- Author:		Luz Angela Borda B.
-- Create date: 13-mayo-2015 10:51 a.m.
-- Description:	Procedimiento almacenado que consulta los c�digos UNSPSC de un proveedor
-- =============================================
CREATE PROCEDURE dbo.usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Consultar
	@idTercero int
AS
BEGIN
 SELECT DISTINCT
		PRO.IdTipoCodUNSPSC,
		COD.Codigo,
		COD.Descripcion,
		PRO.CodUNSPSC 
  FROM	PROVEEDOR.CodUNSPSC PRO
  INNER JOIN 	PROVEEDOR.TipoCodigoUNSPSC COD	ON PRO.IdTipoCodUNSPSC = COD.IdTipoCodUNSPSC
  WHERE	PRO.IDTERCERO = @idTercero
END
