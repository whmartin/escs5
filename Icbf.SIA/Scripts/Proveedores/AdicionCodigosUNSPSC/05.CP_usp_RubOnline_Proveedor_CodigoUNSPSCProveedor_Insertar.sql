USE [SIA]
GO


If (select Count(*) from sysobjects where type='P' and name='usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Insertar')>0 
BEGIN
	DROP PROCEDURE usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Insertar;
END

GO


-- =============================================
-- Author:		Luz Angela Borda
-- Create date:	13 Mayo de 2015 09:12:19 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Codigo UNSPSC del Proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Insertar]
	@CodUNSPSC INT OUTPUT,
	@IdTercero INT, 	
	@IdTipoCodUNSPSC INT,	
	@UsuarioCrea NVARCHAR(250)
AS
BEGIN

	-------------------------------------------------------
	-- Borrar c�digo UNSPS en caso de que exista 
	-------------------------------------------------------

	DELETE 
	FROM	Proveedor.CodUNSPSC
	WHERE	IDTERCERO = @IdTercero
	AND		IdTipoCodUNSPSC = @IdTipoCodUNSPSC

	-------------------------------------------------------
	-- Insertar c�digo UNSPS  
	-------------------------------------------------------

	INSERT INTO Proveedor.CodUNSPSC
	(	
		IDTERCERO, 
		IdTipoCodUNSPSC, 
		UsuarioCrea, 
		FechaCrea
	)
	
	VALUES
	(
		@IdTercero, 
		@IdTipoCodUNSPSC, 
		@UsuarioCrea, 
		GETDATE()
	)
	
	SELECT @CodUNSPSC = SCOPE_IDENTITY() 		
END



