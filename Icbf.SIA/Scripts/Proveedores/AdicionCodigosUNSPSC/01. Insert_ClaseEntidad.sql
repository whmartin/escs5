USE SIA
GO
--------------------------------------------------------------
-- Adiciona campo a tabla [Proveedor].[ClasedeEntidad]
-- Fecha: 8 de maypo de 2015
-- Autor: Luz Angela Borda
--------------------------------------------------------------

alter table [Proveedor].[ClasedeEntidad] add IdRegimenTributario int null

go

----------------------------------------------------------------------------------------------------------------
-- Inserta registro nuevo en la tabla de [ClasedeEntidad] para INSTITUCION DE EDUCACION SUPERIOR
----------------------------------------------------------------------------------------------------------------

insert [Proveedor].[ClasedeEntidad]
values (
	1,
	2,
	28, 
	'INSTITUCION DE EDUCACION SUPERIOR',
	1,
	'Administrador',
	getdate(),
	null,
	null,
	4
	)

GO

----------------------------------------------------------------------------------------------------------------
-- aactualiza registros restantes
----------------------------------------------------------------------------------------------------------------
 update [Proveedor].[ClasedeEntidad] set IdRegimenTributario = 1 where CodigoClasedeEntidad <> 28
