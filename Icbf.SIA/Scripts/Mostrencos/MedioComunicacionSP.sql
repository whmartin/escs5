GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_ActualizarMedioComunicacion]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_ActualizarMedioComunicacion]
	@IdMedioComunicacion int, 
	@Nombre varchar(100),
	@Estado bit,
	@UsuarioModifica varchar(250)
AS
BEGIN
	DECLARE @ExisteTipoTitulo int;
	SELECT @ExisteTipoTitulo = count(db.IdMedioComunicacion) FROM BVMVH.MedioComunicacion db WHERE db.Nombre = @Nombre AND db.IdMedioComunicacion <> @IdMedioComunicacion

	IF (@ExisteTipoTitulo = 0)
	BEGIN
		Update BVMVH.MedioComunicacion SET Nombre = @Nombre, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GetDate() 
		WHERE IdMedioComunicacion = @IdMedioComunicacion
	END
	SELECT @ExisteTipoTitulo as Count
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_ConsultarMedioComunicacion]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_ConsultarMedioComunicacion] 
	@Nombre varchar(100) = NULL,
	@Estado bit = NULL
AS
BEGIN

	SELECT
		db.IdMedioComunicacion, db.Nombre, db.Estado
	FROM BVMVH.MedioComunicacion db 	
	WHERE
		(@Nombre IS NULL OR db.Nombre LIKE '%' + @Nombre + '%') AND (@Estado IS NULL OR db.Estado = @Estado)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_ConsultarMedioComunicacionPorId]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_ConsultarMedioComunicacionPorId] 
	@IdMedioComunicacion int
AS
BEGIN

	SELECT
		db.IdMedioComunicacion, db.Nombre, db.Estado
	FROM BVMVH.MedioComunicacion db 	
	WHERE
		db.IdMedioComunicacion = @IdMedioComunicacion
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_CrearMedioComunicacion]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_MedioComunicacion_CrearMedioComunicacion] 
	@Nombre varchar(100),
	@Estado bit,
	@UsuarioCrea varchar(250)
AS
BEGIN
	DECLARE @MedioComunicacion int;
	SELECT @MedioComunicacion = count(db.IdMedioComunicacion) FROM BVMVH.MedioComunicacion db WHERE db.Nombre = @Nombre 

	IF (@MedioComunicacion = 0)
	BEGIN
		INSERT INTO BVMVH.MedioComunicacion VALUES 
		(
			@Nombre,
			@Estado,
			@UsuarioCrea,
			GetDate(),
			NULL,
			NULL
		)
	END
	SELECT @@IDENTITY as Count
END