GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ActualizarTipoTitulo]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ActualizarTipoTitulo]
	@IdTipoTitulo int, 
	@Nombre varchar(100),
	@Estado bit,
	@UsuarioModifica varchar(250)
AS
BEGIN
	DECLARE @ExisteTipoTitulo int;
	SELECT @ExisteTipoTitulo = count(db.IdTipoTitulo) FROM BVMVH.TipoTitulo db WHERE db.Nombre = @Nombre AND db.IdTipoTitulo <> @IdTipoTitulo

	IF (@ExisteTipoTitulo = 0)
	BEGIN
		Update BVMVH.TipoTitulo SET Nombre = @Nombre, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GetDate() 
		WHERE IdTipoTitulo = @IdTipoTitulo
	END
	SELECT @ExisteTipoTitulo as Count
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_Consultar]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		JESUS EDUARDO CORTES SANCHEZ / INDESAP
-- Create date: 05/01/2018
-- Description:	CONSULTA UN REGISTRO EN LA TABLA TIPOTUTULO
-- =============================================
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_Consultar]
AS
BEGIN
	
	SELECT [IdTipoTitulo]
      ,[Nombre]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
  FROM [BVMVH].[TipoTitulo]
  ORDER BY [Nombre]
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ConsultarPorId]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JESUS EDUARDO CORTES SANCHEZ / INDESAP
-- Create date: 05/01/2018
-- Description:	CONSULTA UN REGISTRO EN LA TABLA TIPOTUTULO POR IDTIPOTITULO
-- =============================================
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ConsultarPorId]
@IdTipoTitulo INT
AS
BEGIN
	
	SELECT [IdTipoTitulo]
      ,[Nombre]
      ,[UsuarioCrea]
      ,[FechaCrea]
      ,[UsuarioModifica]
      ,[FechaModifica]
  FROM [BVMVH].[TipoTitulo]
  WHERE [IdTipoTitulo] = @IdTipoTitulo
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ConsultarTipoTitulo]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ConsultarTipoTitulo] 
	@Nombre varchar(100) = NULL,
	@Estado bit = NULL
AS
BEGIN

	SELECT
		db.IdTipoTitulo, db.Nombre, db.Estado
	FROM BVMVH.TipoTitulo db 	
	WHERE
		(@Nombre IS NULL OR db.Nombre LIKE '%' + @Nombre + '%') AND (@Estado IS NULL OR db.Estado = @Estado)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ConsultarTipoTituloPorId]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_ConsultarTipoTituloPorId] 
	@IdTipoTitulo int
AS
BEGIN

	SELECT
		db.IdTipoTitulo, db.Nombre, db.Estado
	FROM BVMVH.TipoTitulo db 	
	WHERE
		db.IdTipoTitulo = @IdTipoTitulo
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_CrearTipoTitulo]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_TipoTitulo_CrearTipoTitulo] 
	@Nombre varchar(100),
	@Estado bit,
	@UsuarioCrea varchar(250)
AS
BEGIN
	DECLARE @ExisteTipoTitulo int;
	SELECT @ExisteTipoTitulo = count(db.IdTipoTitulo) FROM BVMVH.TipoTitulo db WHERE db.Nombre = @Nombre 

	IF (@ExisteTipoTitulo = 0)
	BEGIN
		INSERT INTO BVMVH.TipoTitulo VALUES 
		(
			@Nombre,
			@Estado,
			@UsuarioCrea,
			GetDate(),
			NULL,
			NULL
		)
	END
	SELECT @@IDENTITY as Count
END