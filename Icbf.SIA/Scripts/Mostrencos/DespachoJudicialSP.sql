GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_ActualizarDespachoJudicial]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_ActualizarDespachoJudicial]
	@IdDespacho int, 
	@Nombre varchar(100),
	@Descripcion varchar(512),
	@Departamento int,
	@Municipio int,
	@Estado bit,
	@UsuarioModifica varchar(250)
AS
BEGIN
	DECLARE @ExisteDespacho int;
	SELECT @ExisteDespacho = count(db.IdDespachoJudicial) FROM BVMVH.DespachoJudicial db WHERE db.Nombre = @Nombre AND db.IdDespachoJudicial <> @IdDespacho

	IF (@ExisteDespacho = 0)
	BEGIN
		Update BVMVH.DespachoJudicial SET IdDepartamento = @Departamento, IdMunicipio = @Municipio, 
		Nombre = @Nombre, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GetDate() 
		WHERE IdDespachoJudicial = @IdDespacho
	END
	SELECT @ExisteDespacho as Count
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_ConsultarDespachoJudicial]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_ConsultarDespachoJudicial] 
	@Nombre varchar(100) = NULL,
	@Departamento int,
	@Municipio int,
	@Estado bit = NULL
AS
BEGIN

	SELECT
		db.IdDespachoJudicial, db.IdDepartamento, dep.NombreDepartamento, db.IdMunicipio,
		mun.NombreMunicipio, db.Nombre, db.Descripcion, db.Estado
	FROM BVMVH.DespachoJudicial db 	
		INNER JOIN DIV.Departamento dep on db.IdDepartamento = dep.IdDepartamento
		INNER JOIN DIV.Municipio mun on db.IdMunicipio = mun.IdMunicipio 
	WHERE
		(@Nombre IS NULL OR db.Nombre LIKE '%' + @Nombre + '%') AND @Departamento = db.IdDepartamento AND
		@Municipio = db.IdMunicipio AND (@Estado IS NULL OR db.Estado = @Estado)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_ConsultarDespachoJudicialPorId]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_ConsultarDespachoJudicialPorId] 
	@IdDespacho int
AS
BEGIN

	SELECT
		db.IdDespachoJudicial, db.IdDepartamento, dep.NombreDepartamento, db.IdMunicipio,
		mun.NombreMunicipio, db.Nombre, db.Descripcion, db.Estado
	FROM BVMVH.DespachoJudicial db 	
		INNER JOIN DIV.Departamento dep on db.IdDepartamento = dep.IdDepartamento
		INNER JOIN DIV.Municipio mun on db.IdMunicipio = mun.IdMunicipio 
	WHERE
		db.IdDespachoJudicial = @IdDespacho
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_CrearDespachoJudicial]    Script Date: 4/05/2018 2:46:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Mostrencos_BVMVH_DespachoJudicial_CrearDespachoJudicial] 
	@Nombre varchar(100),
	@Descripcion varchar(512),
	@Departamento int,
	@Municipio int,
	@Estado bit,
	@UsuarioCrea varchar(250)
AS
BEGIN
	DECLARE @ExisteDespacho int;
	SELECT @ExisteDespacho = count(db.IdDespachoJudicial) FROM BVMVH.DespachoJudicial db WHERE db.Nombre = @Nombre 
	IF (@ExisteDespacho = 0)
	BEGIN
		INSERT INTO BVMVH.DespachoJudicial VALUES 
		(
			@Departamento,
			@Municipio,
			@Nombre,
			@Descripcion,
			@Estado,
			@UsuarioCrea,
			GetDate(),
			NULL,
			NULL
		)
	END
	SELECT @@IDENTITY as Count
		
END

