using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.DataAccess
{
    public class PerfilDAL : GeneralDAL
    {
        public PerfilDAL()
        {
        }
        public int InsertarPerfil(Perfil pPerfil)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_InsertarPerfil"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPerfil", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombrePerfil", DbType.String, pPerfil.NombrePerfil);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pPerfil.UsuarioCreacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPerfil.IdPerfil = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPerfil").ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPerfil(Perfil pPerfil)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ModificarPerfil"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPerfil", DbType.Int32, pPerfil.IdPerfil);
                    vDataBase.AddInParameter(vDbCommand, "@NombrePerfil", DbType.String, pPerfil.NombrePerfil);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pPerfil.UsuarioModificacion);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPerfil(int pIdPerfil)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarPerfil"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPerfil", DbType.Int32, pIdPerfil);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Perfil> ConsultarTodosPerfil()
        {
            try
            {
                List<Perfil> oList = new List<Perfil>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarTodosPerfiles"))
                {
                    IDataReader oIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (oIdr != null)
                    {
                        while (oIdr.Read())
                            oList.Add(new Perfil() { IdPerfil = oIdr.GetInt32(0), NombrePerfil = oIdr.GetString(1)
                                , UsuarioCreacion = oIdr.GetString(2), FechaCreacion=oIdr.GetDateTime(3) });
                        return oList;
                    }
                    else
                        return null;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
