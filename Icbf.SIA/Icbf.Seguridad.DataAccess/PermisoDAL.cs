using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.DataAccess
{
    public class PermisoDAL : GeneralDAL
    {
        public PermisoDAL()
        {
        }
        public int InsertarPermiso(Permiso pPermiso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_InsertarPermiso"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPermiso", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pPermiso.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pPermiso.IdRol);
                    vDataBase.AddInParameter(vDbCommand, "@Insertar", DbType.Int32, pPermiso.Insertar);
                    vDataBase.AddInParameter(vDbCommand, "@Modificar", DbType.Int32, pPermiso.Modificar);
                    vDataBase.AddInParameter(vDbCommand, "@Eliminar", DbType.Int32, pPermiso.Eliminar);
                    vDataBase.AddInParameter(vDbCommand, "@Consultar", DbType.Int32, pPermiso.Consultar);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pPermiso.UsuarioCreacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPermiso.IdPermiso = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPermiso").ToString());
                    GenerarLogAuditoria(pPermiso, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPermiso(Permiso pPermiso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ModificarPermiso"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPermiso", DbType.Int32, pPermiso.IdPermiso);
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pPermiso.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pPermiso.IdRol);
                    vDataBase.AddInParameter(vDbCommand, "@Insertar", DbType.Int32, pPermiso.Insertar);
                    vDataBase.AddInParameter(vDbCommand, "@Modificar", DbType.Int32, pPermiso.Modificar);
                    vDataBase.AddInParameter(vDbCommand, "@Eliminar", DbType.Int32, pPermiso.Eliminar);
                    vDataBase.AddInParameter(vDbCommand, "@Consultar", DbType.Int32, pPermiso.Consultar);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pPermiso.UsuarioModificacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPermiso, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPermiso(int pIdPermiso)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarPermiso"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPermiso", DbType.Int32, pIdPermiso);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPermisosRol(int pIdRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarPermisosRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Permiso> ConsultarPermisosRol(int pIdRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarPermisosRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Permiso> vListaPermisos = new List<Permiso>();
                        while (vDataReaderResults.Read())
                        {
                            Permiso vPermiso = new Permiso();
                            vPermiso.IdPermiso = vDataReaderResults["IdPermiso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPermiso"].ToString()) : vPermiso.IdPermiso;
                            vPermiso.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPermiso.IdPrograma;
                            vPermiso.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vPermiso.IdRol;
                            vPermiso.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPermiso.NombrePrograma;
                            vPermiso.Insertar = vDataReaderResults["Insertar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Insertar"]) : vPermiso.Insertar;
                            vPermiso.Modificar = vDataReaderResults["Modificar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Modificar"]) : vPermiso.Modificar;
                            vPermiso.Eliminar = vDataReaderResults["Eliminar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Eliminar"]) : vPermiso.Eliminar;
                            vPermiso.Consultar = vDataReaderResults["Consultar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Consultar"]) : vPermiso.Consultar;
                            vPermiso.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPermiso.UsuarioCreacion;
                            vPermiso.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPermiso.FechaCreacion;
                            vPermiso.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPermiso.UsuarioModificacion;
                            vPermiso.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPermiso.FechaModificacion;
                            vListaPermisos.Add(vPermiso);
                        }
                        return vListaPermisos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
