﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.DataAccess
{
    public class RolDAL : GeneralDAL
    {
        public RolDAL()
        {
        }
        public int InsertarRol(Rol pRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_InsertarRol"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRol", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pRol.Providerkey);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pRol.NombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRol.DescripcionRol);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRol.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@EsAdministrador", DbType.Boolean, pRol.EsAdministrador);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pRol.UsuarioCreacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRol.IdRol = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRol").ToString());
                    GenerarLogAuditoria(pRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarProgramaRol(int pIdRol, Programa pPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_InsertarProgramaRol"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    vDataBase.AddInParameter(vDbCommand, "@pIdPrograma", DbType.Int32, pPrograma.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pPrograma.IdPrograma);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRol(Rol pRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ModificarRol"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pRol.IdRol);
                    vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pRol.Providerkey);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pRol.NombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRol.DescripcionRol);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRol.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@EsAdministrador", DbType.Boolean, pRol.EsAdministrador);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pRol.UsuarioModificacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRol(int pIdRol)
        {
            try
            {
                //Database vDataBase = ObtenerInstancia();
                //using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarRol"))
                //{
                //    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pIdRol);
                //    return vDataBase.ExecuteNonQuery(vDbCommand);
                //}
                return 0;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarProgramaRol(int pIdRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarProgramasRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    return vDataBase.ExecuteNonQuery(vDbCommand);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol ConsultarRol(string pProviderKey)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarRolPorProviderKey"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Rol vRol = new Rol();
                        while (vDataReaderResults.Read())
                        {
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vRol.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vRol.Providerkey;
                            vRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vRol.NombreRol;
                            vRol.DescripcionRol = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRol.DescripcionRol;
                            vRol.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRol.Estado;
                            vRol.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vRol.UsuarioCreacion;
                            vRol.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vRol.FechaCreacion;
                            vRol.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vRol.UsuarioModificacion;
                            vRol.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vRol.FechaModificacion;
                        }
                        return vRol;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol ConsultarRol(int pIdRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Rol vRol = new Rol();
                        while (vDataReaderResults.Read())
                        {
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vRol.Providerkey = vDataReaderResults["providerKey"] != DBNull.Value ? Convert.ToString(vDataReaderResults["providerKey"].ToString()) : vRol.Providerkey;
                            vRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vRol.NombreRol;
                            vRol.DescripcionRol = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRol.DescripcionRol;
                            vRol.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRol.Estado;
                            vRol.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? vDataReaderResults["UsuarioCreacion"].ToString() : vRol.UsuarioCreacion;
                            vRol.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vRol.FechaCreacion;
                            vRol.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? vDataReaderResults["UsuarioModificacion"].ToString() : vRol.UsuarioModificacion;
                            vRol.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vRol.FechaModificacion;
                            vRol.EsAdministrador = vDataReaderResults["EsAdministrador"] != DBNull.Value ? vDataReaderResults["EsAdministrador"].ToString() == "True" ? true:false : vRol.EsAdministrador;
                        }
                        return vRol;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Rol> ConsultarRoles(String pNombreRol, Boolean? pEstado)
        {
            try
            {
                List<Rol> oList = new List<Rol>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarRoles"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    IDataReader oIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (oIdr != null)
                    {
                        while (oIdr.Read())
                            oList.Add(new Rol()
                                {
                                    IdRol = oIdr.GetInt32(0)
                                    ,
                                    Providerkey = oIdr.GetString(1)
                                    ,
                                    NombreRol = oIdr.GetString(2)
                                    ,
                                    DescripcionRol = oIdr.GetString(3)
                                    ,
                                    Estado = oIdr.GetBoolean(4)
                                });
                        return oList;
                    }
                    else
                        return null;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarRolesNombreEsAdmin(String pNombreRol, Boolean? pEstado)
        {
            try
            {
                List<Rol> oList = new List<Rol>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarRolesNombre"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    IDataReader oIdr = vDataBase.ExecuteReader(vDbCommand);
                    if (oIdr != null)
                    {
                        while (oIdr.Read())
                        {
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                    
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasAsignados(int pIdRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarProgramasAsignadosRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasExcluidos(int pIdRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarProgramasExcluidosRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdRol", DbType.Int32, pIdRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Metodo para consultar Id del usuario por Rol.
        /// </summary>
        /// <param name="pNombreRol">Filtro de busqueda</param>
        /// <returns>Lista de IDs de usuarios</returns>
        public string ConsultarUsuarioPorRol(string pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstanciaQoS();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_SPCP_Seg_UsuariosPorRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@Rol", DbType.String, pNombreRol);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vProviderKey = string.Empty;
                        while (vDataReaderResults.Read())
                        {
                            vProviderKey = string.Concat(vProviderKey, "'", (vDataReaderResults["UserId"] != DBNull.Value ? vDataReaderResults["UserId"] : ""), "', ");
                        }
                        vProviderKey = (!string.IsNullOrEmpty(vProviderKey)) ? vProviderKey.Remove(vProviderKey.Length - 2) : vProviderKey;
                        return vProviderKey;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
