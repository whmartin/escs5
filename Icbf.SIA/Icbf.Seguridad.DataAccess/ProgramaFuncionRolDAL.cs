﻿//-----------------------------------------------------------------------
// <copyright file="ProgramaFuncionRolDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncionRolDAL.</summary>
// <author>Ingenian Software</author>
// <date>22/02/2017 0355 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Seguridad.DataAccess
{
    /// <summary>
    /// Clase para manejar la lógica de negocio de ProgramaFuncionRolDAL
    /// </summary>
    public class ProgramaFuncionRolDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProgramaFuncionRolDAL()
        {
        }

        /// <summary>
        /// Método para Insertar Programa Funcion Rol
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Id del Programa Funcion Rol insertado</returns>public int
        public int InsertarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionRol_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProgramaFuncionRol", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, pProgramaFuncionRol.IdProgramaFuncion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pProgramaFuncionRol.IdRol);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pProgramaFuncionRol.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pProgramaFuncionRol.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProgramaFuncionRol.IdProgramaFuncionRol = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProgramaFuncionRol").ToString());
                    GenerarLogAuditoria(pProgramaFuncionRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar Programa Funcion Rol
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int
        public int ModificarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionRol_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncionRol", DbType.Int32, pProgramaFuncionRol.IdProgramaFuncionRol);
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, pProgramaFuncionRol.IdProgramaFuncion);
                    vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.Int32, pProgramaFuncionRol.IdRol);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pProgramaFuncionRol.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProgramaFuncionRol.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProgramaFuncionRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Eliminar Programa Funcion Rol por Id
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int
        public int EliminarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionRol_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncionRol", DbType.Int32, pProgramaFuncionRol.IdProgramaFuncionRol);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProgramaFuncionRol, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programa Funcion Rol
        /// </summary>
        /// <param name="pIdProgramaFuncionRol">Id ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public ProgramaFuncionRol
        public ProgramaFuncionRol ConsultarProgramaFuncionRol(int pIdProgramaFuncionRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionRol_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncionRol", DbType.Int32, pIdProgramaFuncionRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProgramaFuncionRol vProgramaFuncionRol = new ProgramaFuncionRol();
                        while (vDataReaderResults.Read())
                        {
                            vProgramaFuncionRol.IdProgramaFuncionRol = vDataReaderResults["IdProgramaFuncionRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncionRol"].ToString()) : vProgramaFuncionRol.IdProgramaFuncionRol;
                            vProgramaFuncionRol.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncionRol.IdProgramaFuncion;
                            vProgramaFuncionRol.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncionRol.IdPrograma;
                            vProgramaFuncionRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vProgramaFuncionRol.IdRol;
                            vProgramaFuncionRol.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncionRol.NombrePrograma;
                            vProgramaFuncionRol.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? (vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncionRol.NombreFuncion;
                            vProgramaFuncionRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? (vDataReaderResults["Nombre"].ToString()) : vProgramaFuncionRol.NombreRol;
                            vProgramaFuncionRol.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vProgramaFuncionRol.Estado;
                            vProgramaFuncionRol.UsuarioCrea = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vProgramaFuncionRol.UsuarioCrea;
                            vProgramaFuncionRol.FechaCrea = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vProgramaFuncionRol.FechaCrea;
                            vProgramaFuncionRol.UsuarioModifica = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vProgramaFuncionRol.UsuarioModifica;
                            vProgramaFuncionRol.FechaModifica = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vProgramaFuncionRol.FechaModifica;
                        }

                        return vProgramaFuncionRol;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas Funcion Roles
        /// </summary>
        /// <param name="pIdPrograma">Id Programa a filtrar</param>
        /// <param name="pIdProgramaFuncion">Id Programa Funcion a filtrar</param>
        /// <param name="pIdRol">Id Rol a filtrar</param>
        /// <param name="pEstado">Estado a filtrar</param>
        /// <returns>Lista de Programas Funcion Roles consultado</returns>public List
        public List<ProgramaFuncionRol> ConsultarProgramaFuncionRols(int? pIdPrograma, int? pIdProgramaFuncion, int? pIdRol, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionRols_Consultar"))
                {
                    if (pIdPrograma != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pIdPrograma);

                    if (pIdProgramaFuncion != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, pIdProgramaFuncion);

                    if (pIdRol != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRol", DbType.String, pIdRol);

                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProgramaFuncionRol> vListaProgramaFuncionRol = new List<ProgramaFuncionRol>();
                        while (vDataReaderResults.Read())
                        {
                            ProgramaFuncionRol vProgramaFuncionRol = new ProgramaFuncionRol();
                            vProgramaFuncionRol.IdProgramaFuncionRol = vDataReaderResults["IdProgramaFuncionRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncionRol"].ToString()) : vProgramaFuncionRol.IdProgramaFuncionRol;
                            vProgramaFuncionRol.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncionRol.IdProgramaFuncion;
                            vProgramaFuncionRol.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncionRol.IdPrograma;
                            vProgramaFuncionRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vProgramaFuncionRol.IdRol;
                            vProgramaFuncionRol.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncionRol.NombrePrograma;
                            vProgramaFuncionRol.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? (vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncionRol.NombreFuncion;
                            vProgramaFuncionRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? (vDataReaderResults["Nombre"].ToString()) : vProgramaFuncionRol.NombreRol;
                            vProgramaFuncionRol.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vProgramaFuncionRol.Estado;
                            vProgramaFuncionRol.UsuarioCrea = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vProgramaFuncionRol.UsuarioCrea;
                            vProgramaFuncionRol.FechaCrea = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vProgramaFuncionRol.FechaCrea;
                            vProgramaFuncionRol.UsuarioModifica = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vProgramaFuncionRol.UsuarioModifica;
                            vProgramaFuncionRol.FechaModifica = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vProgramaFuncionRol.FechaModifica;
                            vListaProgramaFuncionRol.Add(vProgramaFuncionRol);
                        }

                        return vListaProgramaFuncionRol;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para ConsultarProgramas
        /// </summary>
        /// <returns>Lista de ProgramaFuncion consultada</returns>public List
        public List<Programa> ConsultarProgramas()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionsProgramas_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaProgramaFuncion = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vProgramaFuncion = new Programa();
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncion.NombrePrograma;
                            vListaProgramaFuncion.Add(vProgramaFuncion);
                        }

                        return vListaProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Roles
        /// </summary>
        /// <returns>Lista Rol consultada</returns>public List
        public List<Rol> ConsultarRoles()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionRoles_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Rol> vListaRol = new List<Rol>();
                        while (vDataReaderResults.Read())
                        {
                            Rol vRol = new Rol();
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? (vDataReaderResults["Nombre"].ToString()) : vRol.NombreRol;
                            vListaRol.Add(vRol);
                        }

                        return vListaRol;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas Funcion por Programa
        /// </summary>
        /// <param name="pIdPrograma">Id del Programa a filtrar</param>
        /// <returns>Lista de ProgramaFuncion consultada</returns>public List
        public List<ProgramaFuncion> ConsultarProgramasFuncionPorPrograma(int pIdPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionsPorPrograma_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pIdPrograma);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProgramaFuncion> vListaProgramaFuncion = new List<ProgramaFuncion>();
                        while (vDataReaderResults.Read())
                        {
                            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
                            vProgramaFuncion.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncion.IdProgramaFuncion;
                            vProgramaFuncion.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? (vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncion.NombreFuncion;
                            vListaProgramaFuncion.Add(vProgramaFuncion);
                        }

                        return vListaProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
