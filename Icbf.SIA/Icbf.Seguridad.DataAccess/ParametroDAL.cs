using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Seguridad.DataAccess
{
    public class ParametroDAL : GeneralDAL
    {
        public ParametroDAL()
        {
        }
        public int InsertarParametro(Parametro pParametro)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();                
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_Parametro_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdParametro", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreParametro", DbType.String, pParametro.NombreParametro);
                    vDataBase.AddInParameter(vDbCommand, "@ValorParametro", DbType.String, pParametro.ValorParametro);
                    vDataBase.AddInParameter(vDbCommand, "@ImagenParametro", DbType.String, pParametro.ImagenParametro);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pParametro.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Funcionalidad", DbType.String, pParametro.Funcionalidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pParametro.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pParametro.IdParametro = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdParametro").ToString());
                    GenerarLogAuditoria(pParametro, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarParametro(Parametro pParametro)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_Parametro_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdParametro", DbType.Int32, pParametro.IdParametro);
                    vDataBase.AddInParameter(vDbCommand, "@NombreParametro", DbType.String, pParametro.NombreParametro);
                    vDataBase.AddInParameter(vDbCommand, "@ValorParametro", DbType.String, pParametro.ValorParametro);
                    if (pParametro.ImagenParametro != null) vDataBase.AddInParameter(vDbCommand, "@ImagenParametro", DbType.String, pParametro.ImagenParametro);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pParametro.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@Funcionalidad", DbType.String, pParametro.Funcionalidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pParametro.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pParametro, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarParametro(Parametro pParametro)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_Parametro_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdParametro", DbType.Int32, pParametro.IdParametro);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pParametro, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        public Parametro ConsultarParametro(int pIdParametro)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_Parametro_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdParametro", DbType.Int32, pIdParametro);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Parametro vParametro = new Parametro();
                        while (vDataReaderResults.Read())
                        {
                            vParametro.IdParametro = vDataReaderResults["IdParametro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdParametro"].ToString()) : vParametro.IdParametro;
                            vParametro.NombreParametro = vDataReaderResults["NombreParametro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreParametro"].ToString()) : vParametro.NombreParametro;
                            vParametro.ValorParametro = vDataReaderResults["ValorParametro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorParametro"].ToString()) : vParametro.ValorParametro;
                            vParametro.ImagenParametro = vDataReaderResults["ImagenParametro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ImagenParametro"].ToString()) : vParametro.ImagenParametro;
                            vParametro.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vParametro.Estado;
                            vParametro.Funcionalidad = vDataReaderResults["Funcionalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Funcionalidad"].ToString()) : vParametro.Funcionalidad;
                            vParametro.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vParametro.UsuarioCrea;
                            vParametro.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vParametro.FechaCrea;
                            vParametro.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vParametro.UsuarioModifica;
                            vParametro.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vParametro.FechaModifica;
                        }
                        return vParametro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Parametro> ConsultarParametros(String pNombreParametro, String pValorParametro, Boolean? pEstado, String pFuncionalidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_SEG_Parametros_Consultar"))
                {
                    if(pNombreParametro != null)
                         vDataBase.AddInParameter(vDbCommand, "@NombreParametro", DbType.String, pNombreParametro);
                    if(pValorParametro != null)
                         vDataBase.AddInParameter(vDbCommand, "@ValorParametro", DbType.String, pValorParametro);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pFuncionalidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@Funcionalidad", DbType.String, pFuncionalidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Parametro> vListaParametro = new List<Parametro>();
                        while (vDataReaderResults.Read())
                        {
                                Parametro vParametro = new Parametro();
                            vParametro.IdParametro = vDataReaderResults["IdParametro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdParametro"].ToString()) : vParametro.IdParametro;
                            vParametro.NombreParametro = vDataReaderResults["NombreParametro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreParametro"].ToString()) : vParametro.NombreParametro;
                            vParametro.ValorParametro = vDataReaderResults["ValorParametro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ValorParametro"].ToString()) : vParametro.ValorParametro;
                            vParametro.ImagenParametro = vDataReaderResults["ImagenParametro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ImagenParametro"].ToString()) : vParametro.ImagenParametro;
                            vParametro.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vParametro.Estado;
                            vParametro.Funcionalidad = vDataReaderResults["Funcionalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Funcionalidad"].ToString()) : vParametro.Funcionalidad;
                            vParametro.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vParametro.UsuarioCrea;
                            vParametro.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vParametro.FechaCrea;
                            vParametro.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vParametro.UsuarioModifica;
                            vParametro.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vParametro.FechaModifica;
                                vListaParametro.Add(vParametro);
                        }
                        return vListaParametro;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
