﻿//-----------------------------------------------------------------------
// <copyright file="ProgramaFuncionDAL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncionDAL.</summary>
// <author>Ingenian Software</author>
// <date>22/02/2017 0355 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Icbf.Seguridad.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Icbf.Seguridad.DataAccess
{
    /// <summary>
    /// Clase para manejar la lógica de negocio de ProgramaFuncionDAL
    /// </summary>
    public class ProgramaFuncionDAL : GeneralDAL
    {
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProgramaFuncionDAL()
        {
        }

        /// <summary>
        /// Método para Insertar un ProgramaFuncion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Id del ProgramaFuncion insertado</returns>public int 
        public int InsertarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pProgramaFuncion.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@NombreFuncion", DbType.String, pProgramaFuncion.NombreFuncion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pProgramaFuncion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pProgramaFuncion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pProgramaFuncion.IdProgramaFuncion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdProgramaFuncion").ToString());
                    GenerarLogAuditoria(pProgramaFuncion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar un Programa Funcion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int 
        public int ModificarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, pProgramaFuncion.IdProgramaFuncion);
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pProgramaFuncion.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@NombreFuncion", DbType.String, pProgramaFuncion.NombreFuncion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pProgramaFuncion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pProgramaFuncion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProgramaFuncion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Eliminar Programa Funcion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int 
        public int EliminarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, pProgramaFuncion.IdProgramaFuncion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pProgramaFuncion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programa Funcion por Id
        /// </summary>
        /// <param name="pIdProgramaFuncion">Id del Programa Funcion a filtrar</param>
        /// <returns>Programa Funcion consultado</returns>public ProgramaFuncion
        public ProgramaFuncion ConsultarProgramaFuncion(int pIdProgramaFuncion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdProgramaFuncion", DbType.Int32, pIdProgramaFuncion);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
                        while (vDataReaderResults.Read())
                        {
                            vProgramaFuncion.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncion.IdProgramaFuncion;
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncion.NombrePrograma;
                            vProgramaFuncion.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? (vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncion.NombreFuncion;
                            vProgramaFuncion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vProgramaFuncion.Estado;
                            vProgramaFuncion.UsuarioCrea = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vProgramaFuncion.UsuarioCrea;
                            vProgramaFuncion.FechaCrea = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vProgramaFuncion.FechaCrea;
                            vProgramaFuncion.UsuarioModifica = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vProgramaFuncion.UsuarioModifica;
                            vProgramaFuncion.FechaModifica = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vProgramaFuncion.FechaModifica;
                        }

                        return vProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas de Funcion
        /// </summary>
        /// <param name="pIdPrograma">Id Programa a filtrar</param>
        /// <param name="pNombreFuncion">Nombre Funcion a filtrar</param>
        /// <param name="pEstado">Estado a filtrar</param>
        /// <returns>Lista de Programa Funcion</returns>public List
        public List<ProgramaFuncion> ConsultarProgramaFuncions(int? pIdPrograma, string pNombreFuncion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncions_Consultar"))
                {
                    if (pIdPrograma != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pIdPrograma);
                    if (pNombreFuncion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreFuncion", DbType.String, pNombreFuncion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProgramaFuncion> vListaProgramaFuncion = new List<ProgramaFuncion>();
                        while (vDataReaderResults.Read())
                        {
                            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
                            vProgramaFuncion.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncion.IdProgramaFuncion;
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncion.NombrePrograma;
                            vProgramaFuncion.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? (vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncion.NombreFuncion;
                            vProgramaFuncion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vProgramaFuncion.Estado;
                            vProgramaFuncion.UsuarioCrea = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vProgramaFuncion.UsuarioCrea;
                            vProgramaFuncion.FechaCrea = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vProgramaFuncion.FechaCrea;
                            vProgramaFuncion.UsuarioModifica = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vProgramaFuncion.UsuarioModifica;
                            vProgramaFuncion.FechaModifica = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vProgramaFuncion.FechaModifica;
                            vListaProgramaFuncion.Add(vProgramaFuncion);
                        }

                        return vListaProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas
        /// </summary>
        /// <returns>Lista de Programas consultados</returns>public List
        public List<Programa> ConsultarProgramas()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionsProgramas_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaProgramaFuncion = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vProgramaFuncion = new Programa();
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncion.NombrePrograma;
                            vListaProgramaFuncion.Add(vProgramaFuncion);
                        }

                        return vListaProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas del Modulo Denuncias
        /// </summary>
        /// <returns>Lista de Programas consultados</returns>public List
        public List<Programa> ConsultarProgramasDenuncias()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncionsProgramasDenuncias_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaProgramaFuncion = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vProgramaFuncion = new Programa();
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncion.NombrePrograma;
                            vListaProgramaFuncion.Add(vProgramaFuncion);
                        }

                        return vListaProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProgramaFuncion> ConsultarProgramaFuncionExiste(int pIdPrograma, string pNombreFuncion, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramaFuncions_ConsultarE"))
                {
                    if (pIdPrograma != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pIdPrograma);
                    if (pNombreFuncion != null)
                        vDataBase.AddInParameter(vDbCommand, "@NombreFuncion", DbType.String, pNombreFuncion);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProgramaFuncion> vListaProgramaFuncion = new List<ProgramaFuncion>();
                        while (vDataReaderResults.Read())
                        {
                            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
                            vProgramaFuncion.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncion.IdProgramaFuncion;
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? (vDataReaderResults["NombrePrograma"].ToString()) : vProgramaFuncion.NombrePrograma;
                            vProgramaFuncion.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? (vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncion.NombreFuncion;
                            vProgramaFuncion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vProgramaFuncion.Estado;
                            vProgramaFuncion.UsuarioCrea = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vProgramaFuncion.UsuarioCrea;
                            vProgramaFuncion.FechaCrea = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vProgramaFuncion.FechaCrea;
                            vProgramaFuncion.UsuarioModifica = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vProgramaFuncion.UsuarioModifica;
                            vProgramaFuncion.FechaModifica = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vProgramaFuncion.FechaModifica;
                            vListaProgramaFuncion.Add(vProgramaFuncion);
                        }

                        return vListaProgramaFuncion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Consulta de las funciones de una página a las que tiene acceso un rol
        /// </summary>
        /// <param name="pRolNombre">Nombre del Rol</param>
        /// <param name="pCodigoPrograma">Nombre en el code behind de la página</param>
        /// <returns>Lista de ProgramaFuncion</returns>public List
        public List<ProgramaFuncion> ConsultarUsuarioProgramaFuncionRol(string pRolNombre, string pCodigoPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Tributario_TRI_ProgramaFuncionRol_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@RolNombre", DbType.String, pRolNombre);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoPrograma", DbType.String, pCodigoPrograma);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProgramaFuncion> vListaUsuarios = new List<ProgramaFuncion>();

                        while (vDataReaderResults.Read())
                        {
                            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
                            vProgramaFuncion.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncion.IdProgramaFuncion;
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncion.NombreFuncion;
                            vProgramaFuncion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vProgramaFuncion.Estado;
                            vListaUsuarios.Add(vProgramaFuncion);
                        }

                        return vListaUsuarios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Consulta de las funciones de una página a las que tiene acceso un rol
        /// </summary>
        /// <param name="pRolNombre">Nombre del Rol</param>
        /// <param name="pCodigoPrograma">Nombre en el code behind de la página</param>
        /// <returns>Lista de ProgramaFuncion</returns>public List
        public List<ProgramaFuncion> ConsultarUsuarioProgramaFuncionRolComisiones(string pRolNombre, string pCodigoPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Sigepcyp_SPCP_ProgramaFuncionRol_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@RolNombre", DbType.String, pRolNombre);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoPrograma", DbType.String, pCodigoPrograma);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ProgramaFuncion> vListaUsuarios = new List<ProgramaFuncion>();

                        while (vDataReaderResults.Read())
                        {
                            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
                            vProgramaFuncion.IdProgramaFuncion = vDataReaderResults["IdProgramaFuncion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdProgramaFuncion"].ToString()) : vProgramaFuncion.IdProgramaFuncion;
                            vProgramaFuncion.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vProgramaFuncion.IdPrograma;
                            vProgramaFuncion.NombreFuncion = vDataReaderResults["NombreFusion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFusion"].ToString()) : vProgramaFuncion.NombreFuncion;
                            vProgramaFuncion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vProgramaFuncion.Estado;
                            vListaUsuarios.Add(vProgramaFuncion);
                        }

                        return vListaUsuarios;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
