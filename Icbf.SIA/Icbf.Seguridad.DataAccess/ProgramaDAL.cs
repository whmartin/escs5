using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Xml.Linq;

namespace Icbf.Seguridad.DataAccess
{
    public class ProgramaDAL : GeneralDAL
    {
        public ProgramaDAL()
        {
        }
        public int InsertarPrograma(Programa pPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_InsertarPrograma"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdPrograma", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pPrograma.IdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@NombrePrograma", DbType.String, pPrograma.NombrePrograma);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoPrograma", DbType.String, pPrograma.CodigoPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@Posicion", DbType.Int32, pPrograma.Posicion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pPrograma.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@VisibleMenu", DbType.Boolean, pPrograma.VisibleMenu);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pPrograma.UsuarioCreacion);
                    vDataBase.AddInParameter(vDbCommand, "@GeneraLog", DbType.Boolean, pPrograma.GeneraLog);
                    //Implementacion reportes dinamicos
                    vDataBase.AddInParameter(vDbCommand, "@EsReporte", DbType.Int32, pPrograma.EsReporte);
                    vDataBase.AddInParameter(vDbCommand, "@IdReporte", DbType.Int32, pPrograma.IdReporte);


                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pPrograma.IdPrograma = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdPrograma").ToString());
                    GenerarLogAuditoria(pPrograma, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPrograma(Programa pPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ModificarPrograma"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pPrograma.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pPrograma.IdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@NombrePrograma", DbType.String, pPrograma.NombrePrograma);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoPrograma", DbType.String, pPrograma.CodigoPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@Posicion", DbType.Int32, pPrograma.Posicion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pPrograma.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@VisibleMenu", DbType.Boolean, pPrograma.VisibleMenu);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pPrograma.UsuarioModificacion);
                    vDataBase.AddInParameter(vDbCommand, "@GeneraLog", DbType.Boolean, pPrograma.GeneraLog);
                    //implementacion reportes dinamicos
                    vDataBase.AddInParameter(vDbCommand, "@EsReporte", DbType.Int32, pPrograma.EsReporte);
                    vDataBase.AddInParameter(vDbCommand, "@IdReporte", DbType.Int32, pPrograma.IdReporte);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPrograma, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPrograma(Programa pPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarPrograma"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pPrograma.IdPrograma);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pPrograma, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Programa ConsultarPrograma(int pIdPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarPrograma"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pIdPrograma);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Programa vPrograma = new Programa();
                        while (vDataReaderResults.Read())
                        {
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.GeneraLog = vDataReaderResults["generaLog"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["generaLog"]) : vPrograma.GeneraLog;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                        }
                        return vPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramas(int pIdModulo, String pNombreModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarProgramas"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pIdModulo", DbType.Int32, pIdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@pNombreModulo", DbType.String, pNombreModulo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasPermitidos(int pIdModulo, String pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ProgramasModuloRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pIdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@nombreRol", DbType.String, pNombreRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Programa> ConsultarProgramasHijoPermitidosPermiso(int pIdModulo, String pNombreRol, int? idPadre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarHijoPermisoModuloRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pIdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@nombreRol", DbType.String, pNombreRol);
                    vDataBase.AddInParameter(vDbCommand, "@ID", DbType.Int32, idPadre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Programa> ConsultarProgramasHijoPermitidosPermiso(int pIdModulo, String[] pNombreRol, int? idPadre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarHijoPermisoModuloRol_ListRol"))
                {
                    var xml = new XElement("DataRolesUsuario",
                                            pNombreRol.Select(i => new XElement("RolUsuario",
                                            new XAttribute("NombreRol", i))));

                    vDataBase.AddInParameter(vDbCommand, "@XMLnombreRol", DbType.String, xml.ToString());
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pIdModulo);                    
                    vDataBase.AddInParameter(vDbCommand, "@ID", DbType.Int32, idPadre);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasPermitidosPermiso(int pIdModulo, String pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarPermisoModuloRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pIdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@nombreRol", DbType.String, pNombreRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasConPermiso(String pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarProgramasRolPermiso"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@pNombreRol", DbType.String, pNombreRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            //vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.Permiso = vDataReaderResults["Permiso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Permiso"]) : vPrograma.Permiso;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.GeneraLog = vDataReaderResults["generaLog"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["generaLog"]) : vPrograma.GeneraLog;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<Programa> ConsultarProgramasConPermiso(String[] pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarProgramasRolPermiso_list"))
                {
                    var xml = new XElement("DataRolesUsuario",
                                            pNombreRol.Select(i => new XElement("RolUsuario",
                                            new XAttribute("NombreRol", i))));

                    vDataBase.AddInParameter(vDbCommand, "@XMLnombreRol", DbType.String, xml.ToString());
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            //vPrograma.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vPrograma.NombreModulo;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.Permiso = vDataReaderResults["Permiso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Permiso"]) : vPrograma.Permiso;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vPrograma.GeneraLog = vDataReaderResults["generaLog"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["generaLog"]) : vPrograma.GeneraLog;
                            vPrograma.EsReporte = vDataReaderResults["EsReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EsReporte"].ToString()) : vPrograma.EsReporte;
                            vPrograma.IdReporte = vDataReaderResults["IdReporte"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdReporte"].ToString()) : vPrograma.IdReporte;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
