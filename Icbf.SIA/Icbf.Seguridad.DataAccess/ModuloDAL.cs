using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Xml.Linq;

namespace Icbf.Seguridad.DataAccess
{
    public class ModuloDAL : GeneralDAL
    {
        public ModuloDAL()
        {
        }
        public int InsertarModulo(Modulo pModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_InsertarModulo"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdModulo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pModulo.NombreModulo);
                    vDataBase.AddInParameter(vDbCommand, "@Posicion", DbType.Int32, pModulo.Posicion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pModulo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCreacion", DbType.String, pModulo.UsuarioCreacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pModulo.IdModulo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdModulo").ToString());
                    GenerarLogAuditoria(pModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModulo(Modulo pModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ModificarModulo"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pModulo.IdModulo);
                    vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pModulo.NombreModulo);
                    vDataBase.AddInParameter(vDbCommand, "@Posicion", DbType.Int32, pModulo.Posicion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pModulo.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModificacion", DbType.String, pModulo.UsuarioModificacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarModulo(int pIdmodulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarModulo"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pIdmodulo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Modulo vModulo = new Modulo();
                        while (vDataReaderResults.Read())
                        {
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vModulo.UsuarioCreacion;
                            vModulo.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vModulo.FechaCreacion;
                            vModulo.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vModulo.UsuarioModificacion;
                            vModulo.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vModulo.FechaModificacion;
                        }
                        return vModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModuloPorNombre(string pNombreModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarModuloPorNombre"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NombreModulo", DbType.String, pNombreModulo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModulosRol(string pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarModulosRol"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@nombreRol", DbType.String, pNombreRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vModulo.UsuarioCreacion;
                            vModulo.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vModulo.FechaCreacion;
                            vModulo.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vModulo.UsuarioModificacion;
                            vModulo.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vModulo.FechaModificacion;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarPermisosRol(string pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarPermisosRolSeguridad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@nombreRol", DbType.String, pNombreRol);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vModulo.UsuarioCreacion;
                            vModulo.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vModulo.FechaCreacion;
                            vModulo.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vModulo.UsuarioModificacion;
                            vModulo.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vModulo.FechaModificacion;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarPermisosRol(string[] pNombreRol)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarPermisosRolSeguridad_ListRol"))
                {
                    var xml = new XElement("DataRolesUsuario",
                                             pNombreRol.Select(i => new XElement("RolUsuario",
                                             new XAttribute("NombreRol", i))));

                    vDataBase.AddInParameter(vDbCommand, "@XMLnombreRol", DbType.String, xml.ToString());
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {                        
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vModulo.UsuarioCreacion;
                            vModulo.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vModulo.FechaCreacion;
                            vModulo.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vModulo.UsuarioModificacion;
                            vModulo.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vModulo.FechaModificacion;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModulos(string pNombreModulo, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarModulos"))
                {
                    if (pNombreModulo != null)
                        vDataBase.AddInParameter(vDbCommand, "@pNombreModulo", DbType.String, pNombreModulo);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@pEstado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Modulo> vListaModulo = new List<Modulo>();
                        while (vDataReaderResults.Read())
                        {
                            Modulo vModulo = new Modulo();
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vModulo.UsuarioCreacion;
                            vModulo.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vModulo.FechaCreacion;
                            vModulo.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vModulo.UsuarioModificacion;
                            vModulo.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vModulo.FechaModificacion;
                            vListaModulo.Add(vModulo);
                        }
                        return vListaModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarTodosModulos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_ConsultarModulo"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Modulo vModulo = new Modulo();
                        while (vDataReaderResults.Read())
                        {
                            vModulo.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vModulo.IdModulo;
                            vModulo.NombreModulo = vDataReaderResults["NombreModulo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModulo"].ToString()) : vModulo.NombreModulo;
                            vModulo.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vModulo.Posicion;
                            vModulo.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vModulo.Estado;
                            vModulo.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vModulo.UsuarioCreacion;
                            vModulo.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vModulo.FechaCreacion;
                            vModulo.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vModulo.UsuarioModificacion;
                            vModulo.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vModulo.FechaModificacion;
                        }
                        return vModulo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModulo(Modulo pModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_Seg_EliminarModulo"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdModulo", DbType.Int32, pModulo.IdModulo);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pModulo, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
