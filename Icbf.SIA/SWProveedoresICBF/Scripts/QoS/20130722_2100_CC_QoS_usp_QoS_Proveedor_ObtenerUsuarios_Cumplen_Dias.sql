USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]    Script Date: 07/22/2013 20:28:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]    Script Date: 07/22/2013 20:28:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- ==========================================================================================
-- Author:		 Carlos Felipe Cubillos
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Obtiene los usuarios creados después de cierto parámetro
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias]


@ApplicationName NVARCHAR(256), --- Nombre de la aplicacion en el membership
@RoleName NVARCHAR(256), ---Rol del usuario
@MesesEliminacion INT,  ---Numero de meses para eliminar usuario inactivo
@DiasMail INT --Numero de dias antes de eliminación de usuario(Envio Email)

AS
BEGIN

DECLARE @ApplicationId		uniqueidentifier

SELECT @ApplicationId = [ApplicationId]
	FROM [QoS].[dbo].[aspnet_Applications]
	WHERE [ApplicationName] = @ApplicationName

SELECT [QOS].[DBO].aspnet_Membership.UserId, IsApproved
	FROM [QoS].[dbo].aspnet_UsersInRoles
		INNER JOIN [QoS].[dbo].aspnet_Roles on [QoS].[dbo].aspnet_UsersInRoles.RoleId = [QoS].[dbo].aspnet_Roles.RoleId
		INNER JOIN [QoS].[dbo].aspnet_Users on [QoS].[dbo].aspnet_UsersInRoles.UserId = [QoS].[dbo].aspnet_Users.UserId
											AND [QoS].[dbo].aspnet_Roles.ApplicationId = [QoS].[dbo].aspnet_Roles.ApplicationId
		INNER JOIN [QoS].[dbo].aspnet_Membership on [QoS].[dbo].aspnet_Users.UserId = [QoS].[dbo].aspnet_Membership.UserId
	WHERE [QOS].[DBO].aspnet_Membership.[ApplicationId] = @ApplicationId
		AND aspnet_Roles.RoleName = @RoleName
		AND (GETDATE() >= DATEADD(MONTH, @MesesEliminacion , (DATEADD(DAY, - @DiasMail, CreateDate)))
			OR GETDATE() >= DATEADD(MONTH, @MesesEliminacion, CreateDate))
	
END


GO


