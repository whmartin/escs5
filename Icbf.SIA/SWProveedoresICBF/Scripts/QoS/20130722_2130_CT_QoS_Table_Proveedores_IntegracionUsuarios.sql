USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[Table_Proveedores_IntegracionUsuarios]    Script Date: 07/22/2013 20:44:10 ******/
IF EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'Table_Proveedores_IntegracionUsuarios')
DROP type [dbo].[Table_Proveedores_IntegracionUsuarios]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[Table_Proveedores_IntegracionUsuarios]    Script Date: 07/22/2013 20:44:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ==========================================================================================
-- Author:		 Carlos Cubillos
-- Create date:  22/07/2013
-- Description:	 Tipo para uso de procedimientos de integración
-- ==========================================================================================
CREATE TYPE [dbo].[Table_Proveedores_IntegracionUsuarios] AS TABLE(
      [UserId] [nvarchar](125) NOT NULL,
      [PrimerNombre] [nvarchar](150) NULL,
      [SegundoNombre] [nvarchar](150) NULL,
      [PrimerApellido] [nvarchar](150) NULL,
      [SegundoApellido] [nvarchar](150) NULL,
      [RazonSocial] [nvarchar](150) NULL,
      [ExisteTercero] [Bit] NULL,
      [IsApproved] [Bit] NULL
)




GO


