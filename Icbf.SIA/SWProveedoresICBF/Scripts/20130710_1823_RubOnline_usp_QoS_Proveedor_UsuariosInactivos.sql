USE [RubOnlineV2G_Produccion]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/10/2013 18:22:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QoS_Proveedor_UsuariosInactivos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]
GO

USE [RubOnlineV2G_Produccion]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/10/2013 18:22:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ==========================================================================================
-- Author:		 Jos� Ignacio De Los Reyes
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Se Crea para enivar correo de suspension de usuario si su estado es inactivo
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]

@UserId NVARCHAR(256)

AS
BEGIN

	DELETE [SEG].[Usuario]
	WHERE [providerKey] = @UserId
	
END--FIN PP




GO


