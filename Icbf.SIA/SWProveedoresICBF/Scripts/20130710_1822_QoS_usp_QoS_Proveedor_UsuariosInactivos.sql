USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/10/2013 18:21:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QoS_Proveedor_UsuariosInactivos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]
GO

USE [QoS]
GO

/****** Object:  StoredProcedure [dbo].[usp_QoS_Proveedor_UsuariosInactivos]    Script Date: 07/10/2013 18:21:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- ==========================================================================================
-- Author:		 Jos� Ignacio De Los Reyes
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Se Crea para enivar correo de suspension de usuario si su estado es inactivo
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]

@ApplicationName NVARCHAR(256), --- Nombre de la aplicacion en el membership
@RoleName NVARCHAR(256), ---Rol del usuario
@MesesEliminacion INT,  ---Numero de meses para eliminar usuario inactivo
@DiasMail INT --Numero de dias antes de eliminaci�n de usuario(Envio Email)

AS
BEGIN

	DECLARE @ApplicationId		uniqueidentifier,
			@UserId				uniqueidentifier,
			@IdUsuario			INT,
			@ProviderKey		VARCHAR(125),
			@Index				INT,
			@CantRegistros		INT,
			@Email_Body			NVARCHAR(MAX),
			@Email_Destino		NVARCHAR(512),
			@Usuario			NVARCHAR(300),
			@TextoEmail			NVARCHAR(MAX),
			@FechaEliminacion	DATETIME

	SELECT @ApplicationId = [ApplicationId]
	FROM [QoS].[dbo].[aspnet_Applications]
	WHERE [ApplicationName] = @ApplicationName
	
	SELECT IDENTITY(INT,1,1) ID, [QOS].[DBO].aspnet_Membership.*
	INTO #TMP_UsuariosInactivos
	FROM [QoS].[dbo].aspnet_UsersInRoles
		INNER JOIN [QoS].[dbo].aspnet_Roles on [QoS].[dbo].aspnet_UsersInRoles.RoleId = [QoS].[dbo].aspnet_Roles.RoleId
		INNER JOIN [QoS].[dbo].aspnet_Users on [QoS].[dbo].aspnet_UsersInRoles.UserId = [QoS].[dbo].aspnet_Users.UserId
											AND [QoS].[dbo].aspnet_Roles.ApplicationId = [QoS].[dbo].aspnet_Roles.ApplicationId
		INNER JOIN [QoS].[dbo].aspnet_Membership on [QoS].[dbo].aspnet_Users.UserId = [QoS].[dbo].aspnet_Membership.UserId
	WHERE [QOS].[DBO].aspnet_Membership.[ApplicationId] = @ApplicationId
		AND IsApproved = 0
		AND EnvioCorreo = 0
		AND aspnet_Roles.RoleName = @RoleName
		AND GETDATE() >= DATEADD(MONTH, @MesesEliminacion , (DATEADD(DAY, - @DiasMail, CreateDate)))
	
	SELECT @CantRegistros = COUNT(ID) FROM #TMP_UsuariosInactivos
	SET @Index = 1
	
	WHILE(@CantRegistros >= @Index)
	BEGIN
		SELECT @Email_Destino = ''
		
		SELECT @Email_Destino = #TMP_UsuariosInactivos.Email,
			   @UserId = #TMP_UsuariosInactivos.UserId
		FROM #TMP_UsuariosInactivos
		WHERE ID = @Index
		
		SET @TextoEmail = 'Si su cuenta no se activa o no se registra como tercero o proveedor, su cuenta de usuario ser� eliminada; si no es posible la activaci�n en este t�rmino, puede hacer nuevamente el registro del usuario.'
		
		SELECT @Usuario = 'Apreciado proveedor'
				
		SELECT @Email_Body = ''
		SELECT @Email_Body = @Email_Body + '<table width=100% height=412 border=0 cellpadding=0 cellspacing=0>
												<tr>
													<td colspan=3 bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td width=10% bgcolor=#81BA3D>&nbsp;</td>
													<td>
														<table width=100% border=0 align=center>
															<tr>
																<td width=5%>&nbsp;</td>
																<td align=center><strong> '
        SELECT @Email_Body = @Email_Body + '						Sistema de Informaci�n Terceros y Gesti�n de Proveedores del Instituto Colombiano de Bienestar Familiar</strong></td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td align=center><strong> '
        SELECT @Email_Body = @Email_Body + '						�Eliminaci�n de Registro!</strong></td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td><strong>Apreciado(a):</strong>&nbsp;' + @Usuario + ',</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>' + @TextoEmail + ':
																</td>
																<td>&nbsp;</td>
															</tr>'  
        SELECT @Email_Body = @Email_Body + '				<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td><strong>Activa tu cuenta</strong> '
        SELECT @Email_Body = @Email_Body + '						para empezar a utilizar el sistema, es muy <strong>f�cil y r�pido</strong>
																</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>
																	<strong>1.- </strong>Ingresa a la p�gina del Icbf.
																</td><td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>2.- </strong>Ingresa por la opci�n de ingreso al sistema.</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>3.- </strong>Ingresa tus credenciales de acceso.</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>4.- </strong>Ingresa los dem�s datos solicitados por el sistema.</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr> '
		SELECT @Email_Body = @Email_Body + '					<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
														</table>
													</td>
													<td width=10% bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>Este correo electr�nico fue enviado por un sistema autom�tico, favor de no responderlo. </td>
												</tr>
												<tr> '
        SELECT @Email_Body = @Email_Body + '		<td colspan=3 align=center bgcolor=#81BA3D>Si tienes alguna duda, puedes dirigirte a nuestra secci�n de  '
        SELECT @Email_Body = @Email_Body + '		<a href=http://www.icbf.gov.co/ target=_blank>Asistencia y Soporte.</a>
													</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>&nbsp;</td>
												</tr>
											</table>'
											
		EXEC msdb.dbo.sp_send_dbmail 
			@profile_name = 'GonetMail', --Colocar perfil que se tenga configurado
			@recipients = @Email_Destino, --Destinatario
			@subject = 'Proceso de Eliminar Usuarios',--Asunto
			@body = @Email_Body, --Cuerpo de correo
			@body_format = 'HTML' ; --Formato de correo
						
		
		UPDATE [QOS].[DBO].aspnet_Membership
		SET EnvioCorreo = 1
		WHERE [QOS].[DBO].aspnet_Membership.UserId = @UserId
		
		SET @Index = (@Index + 1)
	END--FIN WHILE(@CantRegistros >= @Index)
	
	SELECT IDENTITY(INT,1,1) ID, [QOS].[DBO].aspnet_Membership.*
	INTO #TMP_UsuariosInactivosAEliminar
	FROM [QoS].[dbo].aspnet_UsersInRoles
		INNER JOIN [QoS].[dbo].aspnet_Roles on [QoS].[dbo].aspnet_UsersInRoles.RoleId = [QoS].[dbo].aspnet_Roles.RoleId
		INNER JOIN [QoS].[dbo].aspnet_Users on [QoS].[dbo].aspnet_UsersInRoles.UserId = [QoS].[dbo].aspnet_Users.UserId
			AND [QoS].[dbo].aspnet_Roles.ApplicationId = [QoS].[dbo].aspnet_Roles.ApplicationId
		INNER JOIN [QoS].[dbo].aspnet_Membership on [QoS].[dbo].aspnet_Users.UserId = [QoS].[dbo].aspnet_Membership.UserId
	WHERE [QOS].[DBO].ASPNET_MEMBERSHIP.[ApplicationId] = @ApplicationId
		AND IsApproved = 0
		AND EnvioCorreo = 1
		AND aspnet_Roles.RoleName = @RoleName
		AND  GETDATE() >= DATEADD(MONTH, @MesesEliminacion, CreateDate)
		
	SELECT @CantRegistros = COUNT(ID) FROM #TMP_UsuariosInactivosAEliminar
	SET @Index = 1
	
	WHILE(@CantRegistros >= @Index)
	BEGIN
		SELECT @Email_Destino = ''
		
		SELECT @Email_Destino = #TMP_UsuariosInactivosAEliminar.Email,
			   @UserId = #TMP_UsuariosInactivosAEliminar.UserId
		FROM #TMP_UsuariosInactivosAEliminar
		WHERE ID = @Index
		
		--BORRA DE USERIN ROLE
		DELETE [QoS].[dbo].[aspnet_UsersInRoles]
		WHERE [UserId] = @UserId

		--BORRA DE MEMBERSHIP
		DELETE [QoS].[dbo].[aspnet_Membership]
		WHERE [UserId] = @UserId


		--BORRA DE USERS
		DELETE [QoS].[dbo].[aspnet_Users]
		WHERE [UserId] = @UserId
		
		--BORRA DE LA BASE LOCAL
		--DELETE [SEG].[Usuario]
		--WHERE [providerKey] = @UserId

		SET @Index = (@Index + 1)
	END--FIN WHILE(@CantRegistros >= @Index)
	
	
	SELECT * FROM #TMP_UsuariosInactivosAEliminar
	
								
	IF OBJECT_ID('#TMP_UsuariosInactivosAEliminar') IS NOT NULL
	BEGIN
		DROP TABLE #TMP_UsuariosInactivosAEliminar
	END
										
	IF OBJECT_ID('#TMP_UsuariosInactivos') IS NOT NULL
	BEGIN
		DROP TABLE #TMP_UsuariosInactivos
	END
	
	
	
	
END--FIN PP




GO


