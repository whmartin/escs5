﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

namespace DataLayer.interfaces
{
    interface IDataLayer
    {

        #region Public Methods

                bool getdata();

                bool ExcecuteData();

                bool ExcecuteStored();

                void GetParameter(string strNombre, 
                                  DbType enuDbType, 
                                  Object objValue,
                                  ParameterDirection enuParameterDirection);

                void GetParameter(string strNombre,
                                  SqlDbType enuDbType,
                                  Object objValue,
                                  ParameterDirection enuParameterDirection);

                string FormatDatabaseMoney(decimal decDecimal);

                string FormatDateByProvider(DateTime datdate,
                                            bool blnIncluyeHora);

                object GetData(string strQueryString,
                               DataLayer.Entities.DataLayer._menuDataResult enuDataResult);

                object GetData(string strQueryString,
                               DataLayer.Entities.DataLayer._menuDataResult enuDataResult,
                               string strStringConnection);

                void AddGenericParameter(string strParameterName,
                                         DbType objDbType,
                                         ParameterDirection objParameterDirection,
                                         Object ObjValue);

                void AddGenericParameter(string strParameterName,
                                         Object ObjValue);

                object ExecuteStoredProcedure(string strStoredName,
                                              DataLayer.Entities.DataLayer._menuDataResult enuDataResult);

                Int32 ExecuteQueryString(string strQueryString);

                void ExecuteQueryString(string strQueryString ,
                                        string strStringConnection);

                void ExecuteArrayQueriesStringInTransaction(ArrayList ctlstrQueryString);

                void ExecuteArrayQueriesStringInTransaction(string[] astrQueryString);

                bool VerifyQueriesString(string strSql);

                DataSet GetDataset(Int32 intIndex,
                                   string[,] astrQuerystring);

                object Table(string strQueryString);

                bool Row(string strQueryString,
                         ref DataRow objDataRow);

                object Ejecutar();

                object EjecutarDataset();

                object DataSet(string strQueryString);

        #endregion

    }
}
