﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data.Common;
using System.Configuration;
using System.Security.Cryptography;
using System.Security;
using System.IO;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;

namespace DataLayer.Entities
{
    public class DataLayer: interfaces.IDataLayer
    {
        #region Enumerators

        /// <summary>
        /// Enumerator that allows us to identify what type of object to return.
        /// </summary>
        /// <remarks>
        /// <list>Created: Oct. 21/2012 - Jlopez</list>
        /// </remarks>
        public enum _menuDataResult
        {
            Dataset = 0,
            DataTable = 1,
            DataRow = 2,
            Escalar = 3,
            Xml = 4,
            Verificacion = 5,
            DataNothing = 6,
            DataReader = 7
        };

        /// <summary>
        /// Enumerator that allows us to identify the type of encryption is handled in the Connections string
        /// </summary>
        /// <remarks>
        /// <list>Created: Oct. 21/2012 - Jlopez</list>
        /// </remarks>
        public enum HashAlgorithm
        {
            SHA1 = 1,
            MD5 = 2
        };

        #endregion

        #region Variables or constants

                /// <summary>
                /// constants of type <see cref="string"></see> that allows us to store el PassPhrase.
                /// </summary>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                const string _sPassPhrase = "paysett";

                /// <summary>
                /// constants of type <see cref="string)"></see> that allows us to store el SaltValue.
                /// </summary>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                const string _sSaltValue  = "PAYsetT";

                /// <summary>
                /// constants of type <see cref="string"></see> that allows us to store vector for Encryption
                /// </summary>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                const string _sInitVector = "1234567890123456";

                /// <summary>
                /// constants of type <see cref="Int32"></see> that allows us to store size for encryption
                /// </summary>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                const Int32 _iKeySize = 256;

                /// <summary>
                /// constate de Tipo <see cref="string"></see> that allows us to store the Implement Name 
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                const string  _conNameClass = "DataLayer Entities";

                /// <summary>
                /// constate de Tipo <see cref="string"></see> that allows us to store the Project Name
                /// </summary>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                const string _conNameProject = "DataLayer";

                /// <summary>
                /// Variable of type <see cref="DbConnection"></see> that allows us to store the Connection Data
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                private DbConnection _mobjConnection;

                /// <summary>
                /// Variable of type <see cref="DbCommand"></see> that allows us to store the Command Data of the Connection
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                private DbCommand _mobjCommand;

                /// <summary>
                /// Variable of type <see cref="DbTransaction"></see> that allows us to store the Transaction data of the Connection
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                private DbTransaction _mobjTransaction;

                /// <summary>
                /// Variable of type <see cref="List<DbParameter>"></see>Variable of type string that allows us to store the Parameter list 
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                private List<DbParameter> _mlstParameter = new List<DbParameter>();

                /// <summary>
                /// Variable of type <see cref="DbDataAdapter"></see> Variable of type string that allows us to store data connection adapter.
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 jlopez</list>
                /// </remarks>
                private DbDataAdapter _mobjAdapter;

                /// <summary>
                /// Variable of type  <see cref="Int32"></see> that allows us to store Rows affected in the process
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public Int32 _mintRowsAffected = 0;

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store query strings, insert, update, and delete data
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrQuerystring = string.Empty;

                /// <summary>
                /// Variable of type  <see cref="List<string>"></see> that allows us to store query strings, insert, update, and delete data
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public List<string> _mstrQuerysstring = new List<string>();

                /// <summary>
                /// Variable of type  <see cref="_menuDataResult"></see> that allows us to store the list of types of objects to return
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public _menuDataResult _mDataResult  = _menuDataResult.DataNothing;

                /// <summary>
                /// Variable of type  <see cref="bool"></see> that allows us to store transaction if in the process of update or insert transaction handle
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public bool _mblnManejaTransaccion = false;

                /// <summary>
                /// Variable of type  <see cref="char"></see> that allows us to store prefix names stored
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public char _mchrcharacterParameterStored = Convert.ToChar("@");

                /// <summary>
                /// Variable of type  <see cref="char"></see> that allows us to identify the character handling dates Database
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public char _mchrcharacterDate = Convert.ToChar("'");

                /// <summary>
                /// Variable of type  <see cref="Int32"></see> that allows us to store the timeout of the Connection
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public Int32 _mintTimerConnection = 0;

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store the Connection string
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrstringConnection = string.Empty;

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store the Server Name
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrServerName = string.Empty;

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store the User Password  
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrPassword = string.Empty;

                /// <summary>
                /// Variable of type  <see cref="List<string>"></see> that allows us to store names query strings, insert, update, and delete data
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public List<string> _mstrNameQuerysstring = new List<string>();

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store the User Code
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrUser = string.Empty;

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store the Data Base Name
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrDataBaseName = string.Empty;

                /// <summary>
                /// Variable of type  <see cref="string"></see> that allows us to store the Provider name of the Connections 
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrNameProvider = string.Empty;

                /// <summary>
                /// Variable of type <see cref="string"></see>that allows us to store the error message 
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string _mstrErrorMessage = string.Empty;

                /// <summary>
                /// Variable of type <see cref="object"></see>that allows us to store the object of Process Result 
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public object _mobjResult = null;

        #endregion

        #region Properties

                /// <summary>
                /// Property of Type <see cref="string"></see> that allows us store  the Stored Procedure Name
                /// </summary>
                /// <remarks>
                /// <list>Creado: Oct. 08/2012 - jlopez</list>
                /// </remarks>
                public string Nombre 
                {
                    get
                    {
                        return _mstrQuerystring;
                    }
                    set
                    {
                        _mstrQuerystring = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="string"></see> that allows us to store  the query string, insert, update and delete
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string Querystring
                {
                    get
                    {
                        return _mstrQuerystring;
                    }
                    set
                    {
                        _mstrQuerystring = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="List<string>"></see> that allows us to store  the list of the query string, insert, update and delete
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public List<string> Querysstring
                {
                    get
                    {
                        return _mstrQuerysstring;
                    }
                    set
                    {
                        _mstrQuerysstring = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="List<string>"></see> that allows us to store  the list of names  of the query string, insert, update and delete
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public List<string> NameQuerysstring 
                {
                    get
                    {
                        return _mstrNameQuerysstring;
                    }
                    set
                    {
                        _mstrNameQuerysstring = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="List<string>"></see> that allows us to store the Object Type that return the Operation
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public _menuDataResult DataResult
                {
                    get
                    {
                        return _mDataResult;
                    }
                    set
                    {
                        _mDataResult = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="bool"></see> that allows us to store if the process of Insert or Update Handles Transaction
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public bool ManejaTransaccion
                {
                    get
                    {
                        return _mblnManejaTransaccion;
                    }
                    set
                    {
                        _mblnManejaTransaccion = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="char"></see> that allows us to store name prefix of the parameters in the Database
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public char CharacterParameterStored
                {
                    get
                    {
                        return _mchrcharacterParameterStored;
                    }
                    set
                    {
                        _mchrcharacterParameterStored = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="char"></see> that allows us to store the character data type identification date Database
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public char CharacterDate
                {
                    get
                    {
                        return _mchrcharacterDate;
                    }
                    set
                    {
                        _mchrcharacterDate = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="Int32"></see> that allows us to store the Timeout of the Connection
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public Int32 TimerConnection
                {
                    get
                    {
                        return _mintTimerConnection;
                    }
                    set
                    {
                        _mintTimerConnection = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="string"></see> that allows us to store the Connection string
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string stringConnection
                {
                    get
                    {
                        return _mstrstringConnection;
                    }
                    set
                    {
                        _mstrstringConnection = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="string"></see> that allows us to store the Server Name
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string ServerName
                {
                    get
                    {
                        return _mstrServerName;
                    }
                    set
                    {
                        _mstrServerName = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="string"></see> that allows us to store the Provider NAme
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string NameProvider
                {
                    get
                    {
                        return _mstrNameProvider;
                    }
                    set
                    {
                        _mstrNameProvider = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="string"></see> that allows us to store the Error Message
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string ErrorMessage
                {
                    get
                    {
                        return _mstrErrorMessage;
                    }
                    set
                    {
                        _mstrErrorMessage = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="Int32"></see> that allows us to store the Rows Affected in the Process
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public Int32 RowsAffected
                {
                    get
                    {
                        return _mintRowsAffected;
                    }
                    set
                    {
                        _mintRowsAffected = value;
                    }
                }

                /// <summary>
                /// Property of Type <see cref="object"></see> that allows us to store the Object that result of the process
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public object objResult
                {
                    get
                    {
                        return _mobjResult;
                    }
                    set
                    {
                        _mobjResult = value;
                    }
                }

                /// <summary>
                /// Property of Type  <see cref="string"></see> that allows us to store the User Password 
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string Password
                {
                    get
                    {
                        return _mstrPassword;
                    }
                    set
                    {
                        _mstrPassword = value;
                    }
                }

                /// <summary>
                /// Property of Type  <see cref="string"></see> that allows us to store ethe User Code
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string User
                {
                    get
                    {
                        return _mstrUser;
                    }
                    set
                    {
                        _mstrUser = value;
                    }
                }

                /// <summary>
                /// Property of Type  <see cref="string"></see> that allows us to store the Data Base Name
                /// </summary>
                /// <remarks>
                /// <list>Created: Oct. 20/2012 - jlopez</list>
                /// </remarks>
                public string DataBaseName
                {
                    get
                    {
                        return _mstrDataBaseName;
                    }
                    set
                    {
                        _mstrDataBaseName = value;
                    }
                }

        #endregion

        #region Constructors

        /// <summary>
        /// Procedure to load the initial variables
        /// </summary>
        /// <remarks>
        /// <list>Created: Oct. 23/2012 - jlopez</list>
        /// </remarks>
        public DataLayer()
        {
            //Description: string variable that allows us to store the separator character
            //Created: Oct. 23/2012 - jlopez
            string strSeparator = ";";
            //Description: Int32 variable allows us to store the byte encryption management
            //Created: Oct. 23/2012 - jlopez
            Int32 intByteValidation;

            intByteValidation = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ByteValidation"));

            this.CharacterParameterStored = Convert.ToChar(ConfigurationSettings.AppSettings.Get("CharacterParameterStored"));
            this.CharacterDate = Convert.ToChar(ConfigurationSettings.AppSettings.Get("CharacterDate"));
            this.TimerConnection = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("TimerConnection"));

            if (intByteValidation == 1)
            {
                this.stringConnection = Decrypt(ConfigurationManager.ConnectionStrings[Convert.ToString(ConfigurationSettings.AppSettings.Get("ConnectionName")).Trim()].ConnectionString, HashAlgorithm.SHA1);
            }
            else
            {
                this.stringConnection = ConfigurationManager.ConnectionStrings[Convert.ToString(ConfigurationSettings.AppSettings.Get("ConnectionName")).Trim()].ConnectionString;
            }

            foreach(string strPartstringConnection in stringConnection.Split(strSeparator.ToCharArray()))
            {
                if(strPartstringConnection.IndexOf("Data Source=") >= 0 )
                {
                    this.ServerName = strPartstringConnection.Replace("Data Source=", "");
                }    

                if(strPartstringConnection.IndexOf("Password=") >= 0 )
                {
                    this.Password = strPartstringConnection.Replace("Password=", "");
                }   

                if(strPartstringConnection.IndexOf("User ID=") >= 0 )
                {
                    this.User = strPartstringConnection.Replace("User ID=", "");
                }   

                if(strPartstringConnection.IndexOf("Initial Catalog=") >= 0 )
                {
                    this.DataBaseName = strPartstringConnection.Replace("Initial Catalog=", "");
                }  
            }

            this.NameProvider = ConfigurationManager.ConnectionStrings[Convert.ToString(ConfigurationSettings.AppSettings.Get("ConnectionName")).Trim()].ProviderName;
        }

        /// <summary>
        /// Procedure to load the initial variables 
        /// </summary> 
        /// <remarks>
        /// <param name="strConnectionName"> parameter of type <see cref="string"></see> that allows us to choose a different connection string to the default</param>
        /// <list>Created: Oct. 23/2012 - jlopez</list>
        /// </remarks>
        public DataLayer(string strConnectionName)
        {
            //Description: string variable that allows us to store the separator character
            //Created: Oct. 23/2012 - jlopez
            string strSeparator = ";";
            //Description: Int32 variable allows us to store the byte encryption management
            //Created: Oct. 23/2012 - jlopez
            Int32 intByteValidation;


            intByteValidation = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ByteValidation"));

            this.CharacterParameterStored = Convert.ToChar(ConfigurationSettings.AppSettings.Get("CharacterParameterStored"));
            this.CharacterDate = Convert.ToChar(ConfigurationSettings.AppSettings.Get("CharacterDate"));
            this.TimerConnection = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("TimerConnection"));

            if (intByteValidation == 1)
            {
                this.stringConnection = Decrypt(ConfigurationManager.ConnectionStrings[strConnectionName].ConnectionString, HashAlgorithm.SHA1);
            }
            else
            {
                this.stringConnection = ConfigurationManager.ConnectionStrings[strConnectionName].ConnectionString;
            }

            foreach(string strPartstringConnection in stringConnection.Split(strSeparator.ToCharArray()))
            {
                if(strPartstringConnection.IndexOf("Data Source=") >= 0 )
                {
                    this.ServerName = strPartstringConnection.Replace("Data Source=", "");
                }    

                if(strPartstringConnection.IndexOf("Password=") >= 0 )
                {
                    this.Password = strPartstringConnection.Replace("Password=", "");
                }   

                if(strPartstringConnection.IndexOf("User ID=") >= 0 )
                {
                    this.User = strPartstringConnection.Replace("User ID=", "");
                }   

                if(strPartstringConnection.IndexOf("Initial Catalog=") >= 0 )
                {
                    this.DataBaseName = strPartstringConnection.Replace("Initial Catalog=", "");
                }  
            }

            this.NameProvider = ConfigurationManager.ConnectionStrings[strConnectionName].ProviderName;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Función que nos permite retornar una cadena encriptada basada en una cadena enviada en el parametro.
        /// </summary>
        /// <param name="strEncryptstring">Parametro de tipo <see cref="string"></see> que nos permite almacenar la cadena que se va a encriptar</param>
        /// <param name="enuHashalgorithm">Parametro de tipo HashAlgorithm que nos permite almacenar los tipos de encripción</param> 
        /// <returns>Variable de tipo <see cref="string"></see> que nos permite retornar la cadena encriptada</returns>
        /// <remarks><list>Created: oct. 23/2012 - jlopez</list>
        /// </remarks>
        public string Encrypt(string strEncryptstring, HashAlgorithm enuHashalgorithm)
        {
            //Descripcion: Variable de tipo string que nos permite almacenar el resultado del proceso
            //Created: Oct. 23/2012 - jlopez
            string strResult = string.Empty;
            //Descripcion: Variable de tipo Byte() que nos permite almacenar el Vector de Encriptar
            //Created: Oct. 23/2012 - jlopez
            byte[] aintVectorBytes  = Encoding.ASCII.GetBytes(_sInitVector);
            //Descripcion: Variable de tipo Byte() que nos permite almacenar los valores base de la encripción
            //Created: Oct. 23/2012 - jlopez
            byte[] asaltValueBytes = Encoding.ASCII.GetBytes(_sSaltValue);
            //Descripcion: Variable de tipo Byte() que nos permite almacenar los valores a encriptar
            //Created: Oct. 23/2012 - jlopez
            byte[] sB = Encoding.ASCII.GetBytes(strEncryptstring);
            //Descripcion: Variable de tipo PasswordDeriveBytes que nos permite almacenar los parametros de encripción
            //Created: Oct. 23/2012 - jlopez
            PasswordDeriveBytes password = new PasswordDeriveBytes(_sPassPhrase,
                                                                   asaltValueBytes,
                                                                   enuHashalgorithm.ToString(),
                                                                   2);

            //Descripcion: Variable de tipo Byte() que nos permite almacenar los Byte de los parametros PasswordDeriveBytes de encripción
            //Created: Oct. 23/2012 - jlopez
            byte[] keyBytes = password.GetBytes(Convert.ToInt32(_iKeySize / 8));
            //Descripcion: Variable de tipo RijndaelManaged que nos permite almacenar los parametros RijndaelManaged de encripción
            //Created: Oct. 23/2012 - jlopez
            RijndaelManaged symmetricKey = new RijndaelManaged();

            symmetricKey.Mode = CipherMode.CBC;

            //Descripcion: Variable de tipo ICryptoTransform que nos permite almacenar los parametros ICryptoTransform de encripción
            //Created: Oct. 23/2012 - jlopez
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, aintVectorBytes);
            //Descripcion: Variable de tipo MemoryStream que nos permite almacenar los datos encriptados
            //Created: Oct. 23/2012 - jlopez
            MemoryStream MemoryStream = new MemoryStream();
            //Descripcion: Variable de tipo CryptoStream que nos permite procesar la encripción y almacenar en la variable MemoryStream
            //Created: Oct. 23/2012 - jlopez
            CryptoStream CryptoStream = new CryptoStream(MemoryStream, encryptor, CryptoStreamMode.Write);

            CryptoStream.Write(sB, 0, sB.Length);
            CryptoStream.FlushFinalBlock();

            return strResult;
        }

        /// <summary>
        /// Función que nos permite retornar una cadena desencriptada basada en una cadena encriptada enviada en el parametro.
        /// </summary>
        /// <param name="strstring">Parametro de tipo <see cref="string"></see> que nos permite almacenar la cadena que se va a desencriptar</param>
        /// <param name="enuHashalgorithm">Parametro de tipo HashAlgorithm que nos permite almacenar los tipos de encripción</param> 
        /// <returns>Variable de tipo <see cref="string"></see> que nos permite retornar la cadena desencriptada</returns>
        /// <remarks><list>Created: oct. 23/2012 - jlopez</list>
        /// </remarks>
        public string Decrypt(string strstring, HashAlgorithm enuHashalgorithm)
        {
            //Descripcion: Variable de tipo string que nos permite almacenar el resultado del proceso
            //Created: Oct. 23/2012 - jlopez
            string strResult = string.Empty;
            //Descripcion: Variable de tipo Byte() que nos permite almacenar el vector de Desencriptado
            //Created: Oct. 23/2012 - jlopez
            byte[] ainitVectorBytes = Encoding.ASCII.GetBytes(_sInitVector);
            //Descripcion: Variable de tipo Byte() que nos permite almacenar los valores de Desencriptado
            //Created: Oct. 23/2012 - jlopez
            byte[] asaltValueBytes  = Encoding.ASCII.GetBytes(_sSaltValue);
            //Descripcion: Variable de tipo Byte() que nos permite almacenar los valores Base64 de Desencriptado
            //Created: Oct. 23/2012 - jlopez
            byte[] cipherTextBytes  = Convert.FromBase64String(strstring);
            //Descripcion: Variable de tipo PasswordDeriveBytes que nos permite almacenar los parametros de encripción
            //Created: Oct. 23/2012 - jlopez
            PasswordDeriveBytes password = new PasswordDeriveBytes(_sPassPhrase,
                                                                          asaltValueBytes,
                                                                          enuHashalgorithm.ToString(),
                                                                          2);

            //Descripcion: Variable de tipo Byte() que nos permite almacenar los Byte de los parametros PasswordDeriveBytes de desencripción
            //Created: Oct. 23/2012 - jlopez
            byte[] keyBytes = password.GetBytes(Convert.ToInt32(_iKeySize / 8));
            //Descripcion: Variable de tipo RijndaelManaged que nos permite almacenar los parametros RijndaelManaged de encripción
            //Created: Oct. 23/2012 - jlopez
            RijndaelManaged symmetricKey = new RijndaelManaged();

            symmetricKey.Mode = CipherMode.CBC;

            //Descripcion: Variable de tipo ICryptoTransform que nos permite almacenar los parametros ICryptoTransform de desencripción
            //Created: Oct. 23/2012 - jlopez
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, ainitVectorBytes);


            //Descripcion: Variable de tipo MemoryStream que nos permite almacenar los datos desencriptados
            //Created: Oct. 23/2012 - jlopez
            MemoryStream MemoryStream = new MemoryStream(cipherTextBytes);
            //Descripcion: Variable de tipo CryptoStream que nos permite procesar la desencripción y almacenar en la variable MemoryStream
            //Created: Oct. 23/2012 - jlopez
            CryptoStream CryptoStream = new CryptoStream(MemoryStream, decryptor, CryptoStreamMode.Read);

            //Descripcion: Variable de tipo Byte() que nos permite almacenar el plano de texto byte Desencriptados
            //Created: Oct. 23/2012 - jlopez
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            //Descripcion: Variable de tipo Int32 que nos permite almacenar el tamaño de la cadena desencriptada
            //Created: Oct. 23/2012 - jlopez
            Int32 decryptedByteCount = CryptoStream.Read(cipherTextBytes, 0, cipherTextBytes.Length);

            MemoryStream.Close();
            CryptoStream.Close();

            strResult = Encoding.UTF8.GetString(cipherTextBytes,
                                                0,
                                                decryptedByteCount);

            return strResult;
        }

        /// <summary>
        /// Funcion que nos permite retornar una cadena Encriptada con la logica indicada en el parametro
        /// </summary>
        /// <param name="strToken">Parametro de tipo <see cref="string"></see> que nos permite almacenar la cadena a encriptar</param>
        /// <param name="enuTypeHashAlgorithm">Parametro de tipo HashAlgorithm que nos permite almacenar el tipo de encripción</param>
        /// <returns>Variable de tipo <see cref="string"></see> que nos permite retornar la cadena encriptada</returns>
        /// <remarks>Creado: dic. 02/2011 - jlopez</remarks>
        public string GetAHashUsing(string strToken, HashAlgorithm enuTypeHashAlgorithm)
        {
            //Descripcion: Variable de tipo Byte() que nos permite almacenar los Byte que se desean encriptar
            //Created: Oct. 23/2012 - jlopez
            byte[] objbyte = new byte[strToken.Length];


            //Descripcion: Variable de tipo System.Security.Cryptography.MD5 que nos permite procesar el medoto MD5 de Encripción
            //Created: Oct. 23/2012 - jlopez
            System.Security.Cryptography.MD5 MD5;
            //Descripcion: Variable de tipo System.Security.Cryptography.SHA1  que nos permite procesar el medoto SHA1 de Encripción
            //Created: Oct. 23/2012 - jlopez
            System.Security.Cryptography.SHA1 SHA1;

            //Descripcion: Variable de tipo Byte() que nos permite almacenar los Byte del resultado de la encripción
            //Created: Oct. 23/2012 - jlopez
            byte[] abytResult = null;

            if( enuTypeHashAlgorithm ==  DataLayer.HashAlgorithm.MD5)
            {
                    MD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    abytResult = MD5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(strToken));
            }
            else if(enuTypeHashAlgorithm ==  DataLayer.HashAlgorithm.SHA1)
            {
                    SHA1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                    abytResult = SHA1.ComputeHash(System.Text.Encoding.ASCII.GetBytes(strToken));
            }

            return System.Text.Encoding.ASCII.GetString(abytResult);
        }

        /// <summary>
        /// Función que nos permite crear los mensajes de error personalizados.
        /// </summary>
        /// <param name="strMensajeException">Parametro de tipo <see cref="string"></see> que nos permite almacenar el mensaje de la exception</param>
        /// <param name="strNombreprocedimiento">Parametro de tipo <see cref="string"></see> que nos permite almacenar el Nombre del procedimiento o función</param>
        /// <returns>Variable de tipo <see cref="string"></see> que nos retorna el mensaje personalizado</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        string MakeMessage(string strMensajeException , 
                           string strNombreprocedimiento)
        {
            if (strMensajeException.Trim() == "")
            {
                strMensajeException = "Error no identificado.";
            }

            return "Error: " + "\n\r" + 
                   "Proyecto: " + _conNameProject + "\n\r" + 
                   "Implementación: " + _conNameClass + "\n\r" + 
                   "Función ó Procedimiento: " + strNombreprocedimiento + "\n\r" + 
                   ".Net: " + strMensajeException.Trim();
        }

        /// <summary>
        /// Función que nos permite abrir una conexión
        /// </summary>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        bool openConnection()
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider = null;

            try 
            {
                objDbProvider = DbProviderFactories.GetFactory(_mstrNameProvider);

                _mobjConnection = objDbProvider.CreateConnection();

                _mobjConnection.ConnectionString = _mstrstringConnection;
                _mobjConnection.Open();
            }
            catch (Exception objException)
            {
                _mstrErrorMessage = MakeMessage(objException.Message, "openConnection");

                return false;
            }

            return true;
        }

        /// <summary>
        /// Procedimiento que nos permite cerrar la conexión
        /// </summary>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        void CloseConnection()
        {
            if (_mDataResult != _menuDataResult.DataReader)
            {
                if (_mobjConnection.State != System.Data.ConnectionState.Closed)
                {
                    _mobjConnection.Close();
                    _mobjConnection.Dispose();
                }
            }
        }

        /// <summary>
        /// Función que nos permite crear los objetos Command
        /// </summary>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        bool CreateCommand(CommandType enuCommandType, 
                           string strQuerystring)
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider;

            try 
            {
                objDbProvider = DbProviderFactories.GetFactory(_mstrNameProvider);

                if (_mstrstringConnection.Trim() == "")
                {
                    _mstrErrorMessage = MakeMessage("no existe Cadena de Conexión", "CreateCommand");
                    return false;
                }

                _mobjCommand = objDbProvider.CreateCommand();

                _mobjCommand.CommandTimeout = _mintTimerConnection;
                _mobjCommand.CommandType = enuCommandType;
                _mobjCommand.CommandText = strQuerystring;
                _mobjCommand.Connection = _mobjConnection;

                if (this._mblnManejaTransaccion == true)
                {
                    _mobjCommand.Transaction = _mobjTransaction;
                }

                if (_mlstParameter.Count > 0)
                {
                    foreach (DbParameter _mobjParameter in _mlstParameter)
                    {
                        _mobjCommand.Parameters.Add(_mobjParameter);
                    }
                }
            }
            catch (Exception objException) 
            {
                _mstrErrorMessage = MakeMessage(objException.Message, "CreateCommand");

                return false;
            }

            return true;
        }

        /// <summary>
        /// Función que nos permite almacenar en un objeto los datos generados de la operación
        /// </summary>
        /// <param name="strNameTabla">Parametro de tipo <see cref="string"></see> que nos permite almacenar el nombre de la tabla que se desea asignar</param>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        bool returnsData(string strNameTabla, 
                         DataSet objDsResult)
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider;

            try 
            {
                objDbProvider = DbProviderFactories.GetFactory(_mstrNameProvider);

                _mobjAdapter = objDbProvider.CreateDataAdapter();

                _mobjAdapter.SelectCommand = _mobjCommand;

                _mobjResult = null;

                if(_mDataResult == _menuDataResult.Dataset)
                {
                        if (strNameTabla.Trim() != "")
                        {
                            _mobjAdapter.Fill(objDsResult, strNameTabla);
                        }
                        else
                        {
                            _mobjAdapter.Fill(objDsResult);
                        }
                }

                this._mobjResult = objDsResult;
            }
            catch (Exception objexception)
            {
                _mobjAdapter.Dispose();
                _mobjCommand.Dispose();

                CloseConnection();

                _mstrErrorMessage = MakeMessage(objexception.Message, "returnsData");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Función que nos permite almacenar en un objeto los datos generados de la operación
        /// </summary>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        bool returnsData()
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider;
            //Descripcion: Variable de tipo DataSet que nos permite almacenar el dataset resultado de la operación
            //Creado: dic. 06/2011 - jlopez
            DataSet objDsResult = new DataSet(); 

            try 
            {
                objDbProvider = DbProviderFactories.GetFactory(_mstrNameProvider);

                _mobjAdapter = objDbProvider.CreateDataAdapter();

                _mobjAdapter.SelectCommand = _mobjCommand;

                switch (_mDataResult)
                {
                    case _menuDataResult.Dataset:
                        _mobjAdapter.Fill(objDsResult);

                        _mobjResult = objDsResult.Copy();

                        break;
                    case _menuDataResult.DataTable:
                        _mobjAdapter.Fill(objDsResult);

                        if (objDsResult != null)
                        {
                            if (objDsResult.Tables.Count > 0)
                            {
                                _mobjResult = objDsResult.Tables[0].Copy();
                            }
                        }

                        break;
                    case _menuDataResult.DataRow:
                        _mobjAdapter.Fill(objDsResult);

                        if (objDsResult != null)
                        {
                            if (objDsResult.Tables.Count > 0)
                            {
                                if (objDsResult.Tables[0].Rows.Count > 0)
                                {
                                    _mobjResult = objDsResult.Tables[0].Rows[0];
                                }
                            }
                        }

                        break;
                    case _menuDataResult.Escalar:
                        _mobjAdapter.Fill(objDsResult);

                        if (objDsResult != null)
                        {
                            if (objDsResult.Tables.Count > 0)
                            {
                                if(objDsResult.Tables[0].Rows.Count > 0)
                                {
                                    _mobjResult = objDsResult.Tables[0].Rows[0][0];
                                }
                            }
                        }

                        break;
                    case _menuDataResult.Verificacion:
                        _mobjAdapter.Fill(objDsResult);

                        if (objDsResult != null)
                        {
                            if (objDsResult.Tables.Count > 0)
                            {
                                if (objDsResult.Tables[0].Rows.Count > 0)
                                {
                                    _mobjResult = true;
                                }
                                else
                                {
                                    _mobjResult = false;
                                }
                            }
                        } 

                        break;
                    case _menuDataResult.Xml:
                        XmlDataDocument objXMLDataDocument;

                        _mobjAdapter.Fill(objDsResult);

                        if (objDsResult != null)
                        {
                            objXMLDataDocument = new XmlDataDocument(objDsResult);

                            _mobjResult = objXMLDataDocument;
                        }

                        break;
                    case _menuDataResult.DataNothing:
                        _mobjCommand.ExecuteNonQuery();

                        break;
                    case _menuDataResult.DataReader:
                        _mobjResult = _mobjCommand.ExecuteReader(CommandBehavior.CloseConnection);

                        break;
                }
            }
            catch (Exception onjException)
            {
                _mobjAdapter.Dispose();
                _mobjCommand.Dispose();

                CloseConnection();

                _mstrErrorMessage = MakeMessage(onjException.Message, "returnsData");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Función que nos permite generar el nombre del parametro segun la Base de Datos
        /// </summary>
        /// <param name="strParameterName">Parametro de tipo <see cref="string"></see> que permite almacenar el nombre del stored procedure</param>
        /// <returns>Variable de tipo <see cref="string"></see> que nos permite retornar el nombre del stored procedure formateado</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 jlopez</list>
        /// </remarks>
        string FormatParameter(string strParameterName)
        {
            //Descripcion: Variable de tipo Char que nos permite almacenar el caracter inicial del nombre del parametro
            //Creado: dic. 06/2011 - jlopez
            char chrCharacterInitial;

            chrCharacterInitial = Convert.ToChar(strParameterName.Substring(0, 1));

            if (chrCharacterInitial == _mchrcharacterParameterStored)
            {
                return strParameterName;
            }
            else
            {
                return _mchrcharacterParameterStored + strParameterName;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Función que nos permite  generar objetos cargados de datos indicados en las cadenas de querys
        /// </summary>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
        public bool getdata()
        {
            //Descripcion: Variable de tipo DataSet que nos permite almacenar el resultado de la consulta
            //Creado: dic. 06/2011 - jlopez
            DataSet objDsResult = new DataSet();
            //Descripcion: Variable de tipo Int32 que nos permite almacenar el indicador de las tablas en el dataset
            //Creado: dic. 06/2011 - jlopez
            Int32 intIndexTabla = 0;
            //Descripcion: Variable de tipo string que nos permite almacenar el nombre de la tabla del dataset
            //Creado: dic. 06/2011 - jlopez
            string strNombreTabla  = string.Empty;

            if (openConnection() == false)
            {
                return false;
            }

            if (this.Querystring != "")
            {
                if (CreateCommand(CommandType.Text,
                                    this.Querystring) == false)
                {
                    return false;
                }

                if (returnsData() == false)
                {
                    return false;
                }
            }
            else
            {
                foreach (string strQuerystringTemp in this.Querysstring)
                {
                    this.Querystring = strQuerystringTemp;

                    if (CreateCommand(CommandType.Text, 
                                        strQuerystringTemp) == false)
                    {
                        return false;
                    }

                    if (this.NameQuerysstring != null)
                    {
                        if (this.NameQuerysstring.Count > intIndexTabla)
                        {
                            strNombreTabla = NameQuerysstring[intIndexTabla];
                        }
                        else
                        {
                            strNombreTabla = "TablaResultado" + Convert.ToString(intIndexTabla);
                        }
                    }
                    else
                    {
                        strNombreTabla = "TablaResultado" + Convert.ToString(intIndexTabla);
                    }


                    if (returnsData(strNombreTabla, 
                                    objDsResult) == false)
                    {
                        return false;
                    }

                    intIndexTabla = intIndexTabla + 1;
                }
            }

            _mobjAdapter.Dispose();
            _mobjCommand.Dispose();

            CloseConnection();

            return true;
        }

                                                                /// <summary>
        /// Función que nos permite ejecutar una inserción, una actualización o una borrado, manejando o no transacciones
        /// </summary>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
        public bool ExcecuteData()
        {
            //Descripcion: Variable de tipo DbTransaction que nos permite manejar transacción en la operación
            //Creado: dic. 06/2011 - jlopez
            DbTransaction objDbTransaction = null;


            if (openConnection() == false) 
            {
                return false;
            }

            if (this.ManejaTransaccion = true) 
            {
                _mobjTransaction = _mobjConnection.BeginTransaction();
            }

            if (this.Querystring != "") 
            {
                if (CreateCommand(CommandType.Text, 
                                    this.Querystring) == false) 
                {
                    if (this.ManejaTransaccion = true) 
                    {
                        _mobjTransaction.Rollback();
                    }

                    return false;
                }

                try 
                {
                    this.RowsAffected = _mobjCommand.ExecuteNonQuery();
                }
                catch (Exception objException)
                {
                    _mstrErrorMessage = MakeMessage(objException.Message, "ExcecuteData");

                    if (this.ManejaTransaccion == true) 
                    {
                        _mobjTransaction.Rollback();
                    }

                    return false;
                }
            }
            else
            {
                foreach (string strQuerystringTemp in this.Querysstring)
                {
                    if (CreateCommand(CommandType.Text, 
                                        strQuerystringTemp) == false) 
                    {
                        if (this.ManejaTransaccion == true) 
                        {
                            _mobjTransaction.Rollback();
                        }

                        return false;
                    }

                    try 
                    {
                        this.RowsAffected = this.RowsAffected + _mobjCommand.ExecuteNonQuery();
                    }
                    catch (Exception objException) 
                    {
                        this.ErrorMessage = MakeMessage(objException.Message, "ExcecuteData");

                        if (this.ManejaTransaccion == true) 
                        {
                            _mobjTransaction.Rollback();
                        }

                        return false;
                    }
                }
            }

            if (_mblnManejaTransaccion = true)
            {
                _mobjTransaction.Commit();
            }

            return true;
        }

                                                                /// <summary>
        /// Función que nos permite generar objetos cargados de datos o generaración de transac mediante un Stored Procedure
        /// </summary>
        /// <returns>Variable de tipo <see cref="bool"></see> que nos permite la confirmación de la operación</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
                                                                                                                                                                                                                public bool ExcecuteStored()
        {
            if (openConnection() == false) 
            {
                return false;
            }

            if (this.Querystring.Trim() == "") 
            {
                this.ErrorMessage = MakeMessage("No existe nombre del Stored procedure", "ExcecuteStored");

                return false;
            }

            if (CreateCommand(CommandType.StoredProcedure, 
                                this.Querystring.Trim()) == false)
            {
                return false;
            }

            if (returnsData() == false) 
            {
                return false;
            }

            _mobjAdapter.Dispose();
            _mobjCommand.Dispose();

            CloseConnection();

            return true;
        }

                                                                                        /// <summary>
        /// Procedimiento que nos permite cargar parametros para ejecución de stored procedure
        /// </summary>
        /// <param name="strNombre">Parametro de tipo <see cref="string"></see> que nos permite almacenar el nombre del parametro del stored</param>
        /// <param name="enuDbType">Parametro de tipo <see cref="DbType"></see> que nos permite almacenar el tipo de dato del parametro del stored</param>
        /// <param name="objValue">Parametro de tipo <see cref="Object"></see> que nos permite almacenar el valor del parametro del stored</param>
        /// <param name="enuParameterDirection">Parametro de tipo <see cref="ParameterDirection"></see> que nos permite identificar la orientación del parametro Entrada o Salida</param>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
        public void GetParameter(string strNombre,
                                DbType enuDbType,
                                Object objValue,
                                ParameterDirection enuParameterDirection)
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider;
            //Descripcion: Variable de tipo DbParameter que nos permite almacenar el parametro del stored procedure
            //Creado: dic. 06/2011 - jlopez
            DbParameter _objParameter;

            objDbProvider = DbProviderFactories.GetFactory(this.NameProvider);

            _objParameter = objDbProvider.CreateParameter();

            _objParameter.DbType = enuDbType;
            _objParameter.ParameterName = FormatParameter(strNombre);
            _objParameter.Value = objValue;
            _objParameter.Direction = enuParameterDirection;

            this._mlstParameter.Add(_objParameter);
        }

        /// <summary>
        /// Procedimiento que nos permite cargar parametros para ejecución de stored procedure
        /// </summary>
        /// <param name="strNombre">Parametro de tipo <see cref="string"></see> que nos permite almacenar el nombre del parametro del stored</param>
        /// <param name="enuDbType">Parametro de tipo <see cref="SqlDbType"></see> que nos permite almacenar el tipo de dato SQL del parametro del stored</param>
        /// <param name="objValue">Parametro de tipo <see cref="Object"></see> que nos permite almacenar el valor del parametro del stored</param>
        /// <param name="enuParameterDirection">Parametro de tipo <see cref="ParameterDirection"></see> que nos permite identificar la orientación del parametro Entrada o Salida</param>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
        public void GetParameter(string strNombre,
                                SqlDbType enuDbType,
                                Object objValue,
                                ParameterDirection enuParameterDirection)
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider;
            //Descripcion: Variable de tipo SqlParameter que nos permite almacenar el parametro del stored procedure
            //Creado: dic. 06/2011 - jlopez
            System.Data.SqlClient.SqlParameter _objParameter;

            objDbProvider = DbProviderFactories.GetFactory(this.NameProvider);

            _objParameter = (System.Data.SqlClient.SqlParameter)objDbProvider.CreateParameter();

            _objParameter.SqlDbType = enuDbType;
            _objParameter.ParameterName = FormatParameter(strNombre);
            _objParameter.Value = objValue;
            _objParameter.Direction = enuParameterDirection;

            this._mlstParameter.Add(_objParameter);
        }

        /// <summary>
        /// Función que nos permite dar un formato adecuado al número de tipo decimal
        /// </summary>
        /// <param name="decDecimal">Parametro de tipo <see cref="Decimal"></see> que nos permite almacenar el número a formatear</param>
        /// <returns>Variable de tipo <see cref="string"></see> que nos permite retornar el número con el formato establecido</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
        public string FormatDatabaseMoney(Decimal decDecimal)
        {
            //Descripcion: Variable de tipo NumberFormatInfo que nos permite almacenar el formato del numero decimal
            //Creado: dic. 06/2011 - jlopez
            NumberFormatInfo objNumberFormatInfo;

            objNumberFormatInfo = new CultureInfo("", false).NumberFormat;

            objNumberFormatInfo.NumberDecimalSeparator = ".";
            objNumberFormatInfo.NumberGroupSeparator = "";
            objNumberFormatInfo.NumberDecimalDigits = 4;

            return decDecimal.ToString("N", objNumberFormatInfo).Trim();
        }

        /// <summary>
        /// Función que nos permite dar un formato valido por la Base de datos a una fecha
        /// </summary>
        /// <param name="datdate">Parametro de tipo <see cref="DateTime"></see> que nos permite almacenar la fecha a formatear</param>
        /// <param name="blnIncluyeHora">Parametro de tipo <see cref="bool"></see> que nos permite identificar si la fecha maneja hora</param>
        /// <returns>Variable de tipo <see cref="string"></see> que nos permite retornar la fecha con el formato establecido</returns>
        /// <remarks>
        /// <list>Creado: dic. 06/2011 - jlopez</list>
        /// </remarks>
        public string FormatDateByProvider(DateTime datdate,
                                           bool blnIncluyeHora)
        {
            //Descripcion: Variable de tipo string que nos permite almacenar la fecha formateada
            //Creado: dic. 06/2011 - jlopez
            string strResult = string.Empty;

            if (this.CharacterDate.ToString() == "")
            {
                this.ErrorMessage = MakeMessage("Es necesario definir el caracter de identificador de fechas", "FormatDateByProvider");
            }

            if (blnIncluyeHora == false) 
            {
                strResult = this.CharacterDate.ToString() + datdate.ToString("yyyyMMdd") + this.CharacterDate.ToString();
            }
            else
            {
                strResult = this.CharacterDate.ToString() + datdate.ToString("yyyyMMdd HH:mm") + this.CharacterDate.ToString();
            }

            return strResult;
        }

        public Object GetData(string strQuerystring,
                              DataLayer._menuDataResult  enuDataResult)
        {
            Querystring = strQuerystring;
            DataResult = enuDataResult;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return objResult;
        }

        public Object GetData(string strQuerystring,
                              DataLayer._menuDataResult enuDataResult,
                              string strstringConnection)
        {
            object objResult; 

            stringConnection = strstringConnection;
            Querystring = strQuerystring;
            DataResult = enuDataResult;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            objResult = this.objResult;

            return objResult;
        }

        public void AddGenericParameter(string strParameterName,
                                        DbType objDbType,
                                        ParameterDirection objParameterDirection,
                                        Object ObjValue)
        {
            GetParameter(strParameterName, objDbType, ObjValue, objParameterDirection);
        }

        public void AddGenericParameter(string strParameterName,
                                        Object ObjValue)
        {
            //Descripcion: Variable de tipo DbProviderFactory que nos permite almacenar el factory 
            //Creado: dic. 06/2011 - jlopez
            DbProviderFactory objDbProvider;
            //Descripcion: Variable de tipo DbParameter que nos permite almacenar el parametro del stored procedure
            //Creado: dic. 06/2011 - jlopez
            DbParameter _objParameter;

            objDbProvider = DbProviderFactories.GetFactory(this.NameProvider);

            _objParameter = objDbProvider.CreateParameter();

            _objParameter.ParameterName = FormatParameter(strParameterName);
            _objParameter.Value = ObjValue;

            this._mlstParameter.Add(_objParameter);
        }

        public object ExecuteStoredProcedure(string strStoredName,
                                             DataLayer._menuDataResult enuDataResult)
        {
            Querystring = strStoredName;
            DataResult = enuDataResult;

            if (ExcecuteStored() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return objResult;
        }

        public Int32 ExecuteQueryString(string strQuerystring)
        {
            Querystring = strQuerystring;

            DataResult = DataLayer._menuDataResult.DataNothing;

            if (ExcecuteData() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return RowsAffected;
        }

        public void ExecuteQueryString(string strQuerystring,
                                       string strstringConnection)
        {
            stringConnection = strstringConnection;
            Querystring = strQuerystring;
            DataResult = DataLayer._menuDataResult.DataNothing;

            if (ExcecuteData() == false) 
            {
                throw new Exception(ErrorMessage);
            }
        }

        public void ExecuteArrayQueriesStringInTransaction(ArrayList ctlstrQuerystring)
        {
            List<string> lstQuerystring = new List<string>();

            foreach (string strQuerystring in ctlstrQuerystring)
            {
                lstQuerystring.Add(strQuerystring);
            }

            Querysstring = lstQuerystring;
            ManejaTransaccion = true;
            DataResult = DataLayer._menuDataResult.DataNothing;

            if (ExcecuteData() == false) 
            {
                throw new Exception(ErrorMessage);
            }
        }

        public void ExecuteArrayQueriesStringInTransaction(string[] astrQuerystring)
        {
            List<string> lstQuerystring = new List<string>();

            foreach (string strQuerystring in astrQuerystring)
            {
                if (strQuerystring != null)
                {
                    if (strQuerystring.Trim() != "") 
                    {
                        lstQuerystring.Add(strQuerystring);
                    }
                }
            }

            Querysstring = lstQuerystring;
            ManejaTransaccion = true;
            DataResult = DataLayer._menuDataResult.DataNothing;

            if (ExcecuteData() == false) 
            {
                throw new Exception(ErrorMessage);
            }
        }

        public bool VerifyQueriesString(string strSql)
        {
            Querystring = strSql;
            DataResult = DataLayer._menuDataResult.Verificacion;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return (bool)this.objResult;
        }

        public DataSet GetDataset(Int32 intIndex,
                                  string[,] astrQuerystring)
        {
            List<string> lstQuerystring = new List<string>();
            List<string> lstNameQuerystring = new List<string>();

            for (Int32 intIndexArray = 1;intIndexArray <= intIndex;intIndexArray = intIndexArray + 1)
            {
                lstQuerystring.Add(astrQuerystring[intIndexArray, 1]);
                lstNameQuerystring.Add(astrQuerystring[intIndexArray, 2]);
            }

            Querysstring = lstQuerystring;
            NameQuerysstring = lstNameQuerystring;
            DataResult = DataLayer._menuDataResult.Dataset;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return (DataSet)objResult;
        }

        public object Table(string strQuerystring) 
        {
            Querystring = strQuerystring;

            DataResult = DataLayer._menuDataResult.DataTable;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return objResult;
        }

        public bool Row(string strQuerystring, 
                        ref DataRow objDataRow)
        {
            Querystring = strQuerystring;

            DataResult = DataLayer._menuDataResult.DataRow;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            objDataRow = (DataRow)objResult;

            return true;
        }

        public object Ejecutar()
        {
            DataResult = _menuDataResult.DataTable;

            if (ExcecuteStored() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return objResult;
        }

        public object EjecutarDataset()
        {

            DataResult = _menuDataResult.Dataset;

            if (ExcecuteStored() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return objResult;
        }

        public object DataSet(string strQuerystring)
        {
            Querystring = strQuerystring;

            DataResult = DataLayer._menuDataResult.Dataset;

            if (getdata() == false) 
            {
                throw new Exception(ErrorMessage);
            }

            return objResult;
        }

        #endregion

    }
}
