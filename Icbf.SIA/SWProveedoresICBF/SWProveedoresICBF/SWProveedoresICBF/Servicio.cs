﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.IO;
using SWProveedoresICBF.Business;

using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using System.Collections;

namespace SWProveedoresICBF
{
    public partial class Servicio : ServiceBase
    {
        #region Variables and Constant

        private Timer _t = null;
        readonly System.Configuration.AppSettingsReader _configurationAppSetting = new System.Configuration.AppSettingsReader();
        public static string LogDirectory = string.Empty;
        public static string LogFileName = string.Empty;
        public static string ApplicationName = string.Empty;
        public static string RoleName = string.Empty;
        public static string MesesEliminacion = string.Empty;
        public static string MesesEliminacionNoSubsanable = string.Empty;
        public static string DiasMail = string.Empty;
        public static string TextoEmail = string.Empty;
        public static string TituloEmail = string.Empty;
        public static string Paso1 = string.Empty;
        public static string Paso2 = string.Empty;
        public static string Paso3 = string.Empty;
        public static string Paso4 = string.Empty;
        int _interval = 10000;

        #endregion

        public Servicio()
        {
            InitializeComponent();
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            //Clase.PruebaTabla();


            //This is the timer interval of the windows service timer, it will provide the time in milliseconds betwen events
            _interval = (int) _configurationAppSetting.GetValue("TimerInterval", typeof (int));

            //Directory where we are going to save the log file
            LogDirectory = (string) _configurationAppSetting.GetValue("LogDirectory", typeof (string));

            //Name of the log file .txt
            LogFileName = (string) _configurationAppSetting.GetValue("LogFileName", typeof (string));
            

            //Name ASP.net membership aplication
            ApplicationName = (string) _configurationAppSetting.GetValue("ApplicationName", typeof (string));


            //Nombre del role que deben tener los usuarios a los que se le aplicará la validacion
            RoleName = (string) _configurationAppSetting.GetValue("RoleName", typeof (string));


            string strError = "";
            //Numero de meses a los cuales se debe eliminar el usuario inactivo
            //Recuperar parámetro de meseseliminacion
            //MesesEliminacion = (string) _configurationAppSetting.GetValue("MesesEliminacion", typeof (string));
            MesesEliminacion = Clase.ConsultarParametro(11, ref strError);
            MesesEliminacionNoSubsanable = Clase.ConsultarParametro(32, ref strError);
            //Recuperar parámetro de DiasMail
            //Numero de dias antes de la elimincaión del usuario, en donde se envia el mensaje de notificacion de eliminación
            //DiasMail = (string) _configurationAppSetting.GetValue("DiasMail", typeof (string));
            DiasMail = Clase.ConsultarParametro(25, ref strError);

            TextoEmail = Clase.ConsultarParametro(24, ref strError);
            TituloEmail = Clase.ConsultarParametro(26, ref strError);
            Paso1 = Clase.ConsultarParametro(17, ref strError);
            Paso2 = Clase.ConsultarParametro(18, ref strError);
            Paso3 = Clase.ConsultarParametro(19, ref strError);
            Paso4 = Clase.ConsultarParametro(20, ref strError);

            //If the directory does not exist, we have to create the directory
            if (System.IO.Directory.Exists(LogDirectory) == false)
            {
                System.IO.Directory.CreateDirectory(LogDirectory);
            }

            
            //Proceso();
            //ProcesoEstadoPorEliminar();

            //passing the parameter to the timer
            _t = new Timer(_interval);

            //create a new event handler for the timer.Elapsed event
            _t.Elapsed += new ElapsedEventHandler(t_Elapsed);
        }

        #region Protected Methos


        protected override void OnStart(string[] args)
        {
            _t.Start(); //Iniciamos el timer
        }


        protected override void OnStop()
        {
            _t.Stop(); //Paramos el timer
        }


        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            // stop the timer so we can work we the files
            _t.Stop();

            Proceso();

            //after we finish with the file, we start the time again..
            _t.Start();
        }


        #endregion

        private static void Proceso()
        {
            try
            {
                string strError = string.Empty;

                DataTable dtUsuarios = Clase.ConsultarUsuariosAplicacion(ApplicationName, RoleName,
                                                                         Convert.ToInt32(MesesEliminacion),
                                                                         Convert.ToInt32(DiasMail), ref strError);

                dtUsuarios = Clase.ObtenerDataIntegracion(dtUsuarios);




                var dtData = (DataTable)Clase.UsuariosInactivos(ApplicationName, RoleName, Convert.ToInt32(MesesEliminacion),Convert.ToInt32(DiasMail),
                   TextoEmail, TituloEmail, Paso1, Paso2, Paso3, Paso4, dtUsuarios);

                foreach (DataRow  dr in dtData.Rows)
                {
                    string usuario = Convert.ToString(dr["UserId"]);
                    if (Clase.UsuariosInactivosSia(usuario, ref strError) == false)
                    {
                        TextWriter tw = new StreamWriter(LogDirectory + LogFileName, true);
                        tw.WriteLine("Error al eliminar el usuario: " + usuario + " :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + ", Descripción: " + strError);
                        tw.Close();
                    }
                    else
                    {
                        TextWriter tw = new StreamWriter(LogDirectory + LogFileName, true);
                        tw.WriteLine("Usuario eliminado: " + usuario + " :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                        tw.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                TextWriter tw = new StreamWriter(LogDirectory + LogFileName, true);
                tw.WriteLine("Error : " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + ", Error en t_Elapsed : " + ex.Message.ToString(CultureInfo.InvariantCulture));
                tw.Close();
            }
        }

        //Proceso que elimina la informacion del Usuario cuando se encuentran en estado por Eliminar
        private static void ProcesoEstadoPorEliminar()
        {
            try
            {
                string strError = string.Empty;

                //Obtiene los usuarios de Proveedores en SIA que esten en estado en eliminacion y cumplan las condiciones de Eliminación.
                DataTable dtUsuarios = Clase.ObtenerDataUsuariosPorEliminar(Convert.ToInt32(MesesEliminacionNoSubsanable), Convert.ToInt32(DiasMail));

                //Realizo el proceso de envio de correo y de eliminacion en la QoS
                var dtData = (DataTable)Clase.UsuariosPorEliminarQos(ApplicationName, RoleName,
                    TextoEmail, TituloEmail, Paso1, Paso2, Paso3, Paso4, dtUsuarios);

                foreach (DataRow dr in dtData.Rows)
                {
                    string usuario = Convert.ToString(dr["UserId"]);

                    //Realizo la Eliminacion en las instancias de SIA
                    if (Clase.UsuariosPorEliminarSia(usuario, ref strError) == false)
                    {
                        TextWriter tw = new StreamWriter(LogDirectory + LogFileName, true);
                        tw.WriteLine("Error al eliminar el usuario: " + usuario + " :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + ", Descripción: " + strError);
                        tw.Close();
                    }
                    else
                    {
                        TextWriter tw = new StreamWriter(LogDirectory + LogFileName, true);
                        tw.WriteLine("Usuario eliminado: " + usuario + " :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                        tw.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                TextWriter tw = new StreamWriter(LogDirectory + LogFileName, true);
                tw.WriteLine("Error : " + DateTime.Now.ToString(CultureInfo.InvariantCulture) + ", Error en t_Elapsed : " + ex.Message.ToString(CultureInfo.InvariantCulture));
                tw.Close();
            }
        }
    }
}
