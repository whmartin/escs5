﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SWProveedoresICBF.Business
{
    public class Clase
    {
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public static bool UsuariosInactivosSia(string UserId, ref string strError)
        {
            var objDataLayer = new DataLayer.Entities.DataLayer
                {
                    DataResult = DataLayer.Entities.DataLayer._menuDataResult.DataNothing,
                    Querystring = "usp_QoS_Proveedor_UsuariosInactivos",
                    stringConnection =
                        ConfigurationManager.ConnectionStrings[
                            Convert.ToString(ConfigurationManager.AppSettings.Get("SIAConnectionName")).Trim()]
                            .ConnectionString
                };

            objDataLayer.GetParameter("@UserId", System.Data.DbType.String, UserId, System.Data.ParameterDirection.Input);

            if (objDataLayer.ExcecuteStored() == false)
            {
                strError = objDataLayer.ErrorMessage;
                return false;
            }

            return true;
        }
              
        //public static DataTable UsuariosInactivos(string ApplicationName, string RoleName, int MesesEliminacion, int DiasMail, ref string strError)
        //{
        //    var objDataLayer = new DataLayer.Entities.DataLayer
        //        {
        //            DataResult = DataLayer.Entities.DataLayer._menuDataResult.DataTable,
        //            Querystring = "usp_QoS_Proveedor_UsuariosInactivos",
        //            stringConnection =
        //                ConfigurationManager.ConnectionStrings[
        //                    Convert.ToString(ConfigurationManager.AppSettings.Get("MembershipConnectionName")).Trim()]
        //                    .ConnectionString
        //        };

        //    objDataLayer.GetParameter("@ApplicationName", System.Data.DbType.String, ApplicationName, System.Data.ParameterDirection.Input);
        //    objDataLayer.GetParameter("@RoleName", System.Data.DbType.String, RoleName, System.Data.ParameterDirection.Input);
        //    objDataLayer.GetParameter("@MesesEliminacion", System.Data.DbType.Int32, MesesEliminacion, System.Data.ParameterDirection.Input);
        //    objDataLayer.GetParameter("@DiasMail", System.Data.DbType.Int32, DiasMail, System.Data.ParameterDirection.Input);

        //    if (objDataLayer.ExcecuteStored() == false)
        //    {
        //        strError = objDataLayer.ErrorMessage;
        //        return null;
        //    }

        //    var dtData = (DataTable)objDataLayer.objResult;

        //    return dtData;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idParametro"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public static string ConsultarParametro(int idParametro, ref string strError)
        {
            var objDataLayer = new DataLayer.Entities.DataLayer
            {
                DataResult = DataLayer.Entities.DataLayer._menuDataResult.DataTable,
                Querystring = "usp_RubOnline_SEG_Parametro_Consultar",
                stringConnection =
                    ConfigurationManager.ConnectionStrings[
                        Convert.ToString(ConfigurationManager.AppSettings.Get("SIAConnectionName")).Trim()]
                        .ConnectionString
            };

            objDataLayer.GetParameter("@IdParametro", System.Data.DbType.Int32, idParametro, System.Data.ParameterDirection.Input);

            if (objDataLayer.ExcecuteStored() == false)
            {
                strError = objDataLayer.ErrorMessage;
                return null;
            }

            DataTable dtData = (DataTable)objDataLayer.objResult;

            if (dtData.Rows.Count > 0)
            {
                return dtData.Rows[0]["ValorParametro"].ToString();
            }
            else
            {
                return null;
            }
        }

        public static DataSet PruebaTabla()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ApplicationId");
            DataRow dr1 = dt.NewRow();
            dr1[0] = "04C1A44A-68D4-4D14-A5B2-000D3CF7BF37";
            dt.Rows.Add(dr1);
            DataRow dr2 = dt.NewRow();
            dr2[0] = "839BB92A-497F-4A65-9F96-000E25B176EE";
            dt.Rows.Add(dr2);
            DataRow dr3 = dt.NewRow();
            dr3[0] = "80241F7D-D78A-4D35-ACBC-0021FD5FDBB3";
            dt.Rows.Add(dr3);

            DataSet ds = new DataSet();

            SqlConnection objSqlConnection = null;
            SqlCommand objSQLComman=null;
            try
            {
                objSQLComman = new SqlCommand("usp_QoS_Proveedor_Prueba_");
                objSQLComman.CommandType = CommandType.StoredProcedure;
                objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["QoS"].ConnectionString);
                objSQLComman.Connection = objSqlConnection;    

                SqlParameter parData = new SqlParameter("@pData", SqlDbType.Structured);
                parData.TypeName = "dbo.pTableDataTest";
                parData.Value = dt;
                objSQLComman.Parameters.Add(parData);               

                objSqlConnection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = objSQLComman;
                
                adapter.Fill(ds);
                return ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (objSQLComman != null)
                {
                    objSQLComman.Dispose();
                }
                if (objSqlConnection .State == ConnectionState.Open)
                {
                    objSqlConnection.Close();
                }
                objSqlConnection.Close();
                
            }

        
        }

        public static DataTable ConsultarUsuariosAplicacion(string ApplicationName, string RoleName, int MesesEliminacion, int DiasMail, ref string strError)
        {
            var objDataLayer = new DataLayer.Entities.DataLayer
            {
                DataResult = DataLayer.Entities.DataLayer._menuDataResult.DataTable,
                Querystring = "usp_QoS_Proveedor_ObtenerUsuarios_Cumplen_Dias",
                stringConnection =
                    ConfigurationManager.ConnectionStrings[
                        Convert.ToString(ConfigurationManager.AppSettings.Get("MembershipConnectionName")).Trim()]
                        .ConnectionString
            };

            objDataLayer.GetParameter("@ApplicationName", System.Data.DbType.String, ApplicationName, System.Data.ParameterDirection.Input);
            objDataLayer.GetParameter("@RoleName", System.Data.DbType.String, RoleName, System.Data.ParameterDirection.Input);
            objDataLayer.GetParameter("@MesesEliminacion", System.Data.DbType.Int32, MesesEliminacion, System.Data.ParameterDirection.Input);
            objDataLayer.GetParameter("@DiasMail", System.Data.DbType.Int32, DiasMail, System.Data.ParameterDirection.Input);

            if (objDataLayer.ExcecuteStored() == false)
            {
                strError = objDataLayer.ErrorMessage;
                return null;
            }

            DataTable dtDataProc = (DataTable)objDataLayer.objResult;
            DataTable dtData = new DataTable();
            dtData.Columns.Add("UserId");
            dtData.Columns.Add("PrimerNombre");
            dtData.Columns.Add("SegundoNombre");
            dtData.Columns.Add("PrimerApellido");
            dtData.Columns.Add("SegundoApellido");
            dtData.Columns.Add("RazonSocial");
            dtData.Columns.Add("ExisteTercero");
            dtData.Columns.Add("IsApproved");

            foreach (DataRow dr in dtDataProc.Rows)
            {
                DataRow drNew = dtData.NewRow();
                drNew["UserId"] = dr["UserId"];
                drNew["IsApproved"] = dr["IsApproved"];
                dtData.Rows.Add(drNew);
            }

            return dtData;
        }

        public static DataTable ObtenerDataIntegracion(DataTable dtInt)
        {
            DataSet ds=new DataSet();
            SqlConnection objSqlConnection = null;
            SqlCommand objSQLComman = null;
            try
            {
                objSQLComman = new SqlCommand("usp_RubOnline_Proveedor_IntegrarUsuarios");
                objSQLComman.CommandType = CommandType.StoredProcedure;
                objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SIA"].ConnectionString);
                objSQLComman.Connection = objSqlConnection;

                SqlParameter parData = new SqlParameter("@pData", SqlDbType.Structured);
                parData.TypeName = "dbo.Table_Proveedores_IntegracionUsuarios";
                parData.Value = dtInt;
                objSQLComman.Parameters.Add(parData);

                objSqlConnection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = objSQLComman;

                adapter.Fill(ds);

                DataTable dtDataProc = ds.Tables[0];
                DataTable dtData = new DataTable();
                dtData.Columns.Add("UserId");
                dtData.Columns.Add("PrimerNombre");
                dtData.Columns.Add("SegundoNombre");
                dtData.Columns.Add("PrimerApellido");
                dtData.Columns.Add("SegundoApellido");
                dtData.Columns.Add("RazonSocial");
                dtData.Columns.Add("ExisteTercero");
                dtData.Columns.Add("IsApproved");

                foreach (DataRow dr in dtDataProc.Rows)
                {
                    DataRow drNew = dtData.NewRow();
                    drNew["UserId"] = dr["UserId"];
                    drNew["PrimerNombre"] = dr["PrimerNombre"];
                    drNew["SegundoNombre"] = dr["SegundoNombre"];
                    drNew["PrimerApellido"] = dr["PrimerApellido"];
                    drNew["SegundoApellido"] = dr["SegundoApellido"];
                    drNew["RazonSocial"] = dr["RazonSocial"];
                    drNew["ExisteTercero"] = dr["ExisteTercero"];
                    drNew["IsApproved"] = dr["IsApproved"];
                    dtData.Rows.Add(drNew);
                }

                return dtData;

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (objSQLComman != null)
                {
                    objSQLComman.Dispose();
                }
                if (objSqlConnection.State == ConnectionState.Open)
                {
                    objSqlConnection.Close();
                }
                objSqlConnection.Close();
            }
        }

        public static DataTable UsuariosInactivos(string ApplicationName, string RoleName, int MesesEliminacion, int DiasMail, string textoEmail, string tituloEmail,
            string paso1, string paso2, string paso3, string paso4, DataTable dtInt)
        {
            DataSet ds = new DataSet();
            SqlConnection objSqlConnection = null;
            SqlCommand objSQLComman = null;
            try
            {
                objSQLComman = new SqlCommand("usp_QoS_Proveedor_UsuariosInactivos");
                objSQLComman.CommandType = CommandType.StoredProcedure;
                objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["QoS"].ConnectionString);
                objSQLComman.Connection = objSqlConnection;


                objSQLComman.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                objSQLComman.Parameters.AddWithValue("@RoleName", RoleName);
                objSQLComman.Parameters.AddWithValue("@MesesEliminacion", MesesEliminacion);
                objSQLComman.Parameters.AddWithValue("@DiasMail", DiasMail);
                objSQLComman.Parameters.AddWithValue("@TextoEmail", textoEmail);
                objSQLComman.Parameters.AddWithValue("@TituloEmail", tituloEmail);
                objSQLComman.Parameters.AddWithValue("@Paso1", paso1);
                objSQLComman.Parameters.AddWithValue("@Paso2", paso2);
                objSQLComman.Parameters.AddWithValue("@Paso3", paso3);
                objSQLComman.Parameters.AddWithValue("@Paso4", paso4);


                SqlParameter parData = new SqlParameter("@pData", SqlDbType.Structured);
                parData.TypeName = "dbo.Table_Proveedores_IntegracionUsuarios";
                parData.Value = dtInt;
                objSQLComman.Parameters.Add(parData);

                objSqlConnection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = objSQLComman;

                adapter.Fill(ds);

                 return ds.Tables[0];           

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (objSQLComman != null)
                {
                    objSQLComman.Dispose();
                }
                if (objSqlConnection.State == ConnectionState.Open)
                {
                    objSqlConnection.Close();
                }
                objSqlConnection.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public static bool UsuariosPorEliminarSia(string UserId, ref string strError)
        {
            var objDataLayer = new DataLayer.Entities.DataLayer
            {
                DataResult = DataLayer.Entities.DataLayer._menuDataResult.DataNothing,
                Querystring = "usp_RubOnline_Proveedor_UsuariosPorEliminar",
                stringConnection =
                    ConfigurationManager.ConnectionStrings[
                        Convert.ToString(ConfigurationManager.AppSettings.Get("SIAConnectionName")).Trim()]
                        .ConnectionString
            };

            objDataLayer.GetParameter("@UserId", System.Data.DbType.String, UserId, System.Data.ParameterDirection.Input);

            if (objDataLayer.ExcecuteStored() == false)
            {
                strError = objDataLayer.ErrorMessage;
                return false;
            }

            return true;
        }

        public static DataTable ObtenerDataUsuariosPorEliminar(int MesesEliminacion, int DiasMail)
        {
            DataSet ds = new DataSet();
            SqlConnection objSqlConnection = null;
            SqlCommand objSQLComman = null;
            try
            {
                objSQLComman = new SqlCommand("usp_RubOnline_Proveedor_IntegrarUsuariosPorEliminar");
                objSQLComman.CommandType = CommandType.StoredProcedure;
                objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SIA"].ConnectionString);
                objSQLComman.Connection = objSqlConnection;

                objSQLComman.Parameters.AddWithValue("@MesesEliminacion", MesesEliminacion);
                objSQLComman.Parameters.AddWithValue("@DiasMail", DiasMail);

                objSqlConnection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = objSQLComman;

                adapter.Fill(ds);

                DataTable dtDataProc = ds.Tables[0];
                DataTable dtData = new DataTable();
                dtData.Columns.Add("UserId");
                dtData.Columns.Add("PrimerNombre");
                dtData.Columns.Add("SegundoNombre");
                dtData.Columns.Add("PrimerApellido");
                dtData.Columns.Add("SegundoApellido");
                dtData.Columns.Add("RazonSocial");
                dtData.Columns.Add("ExisteTercero");
                dtData.Columns.Add("IsApproved");
                dtData.Columns.Add("Eliminar");

                foreach (DataRow dr in dtDataProc.Rows)
                {
                    DataRow drNew = dtData.NewRow();
                    drNew["UserId"] = dr["UserId"];
                    drNew["PrimerNombre"] = dr["PrimerNombre"];
                    drNew["SegundoNombre"] = dr["SegundoNombre"];
                    drNew["PrimerApellido"] = dr["PrimerApellido"];
                    drNew["SegundoApellido"] = dr["SegundoApellido"];
                    drNew["RazonSocial"] = dr["RazonSocial"];
                    drNew["ExisteTercero"] = dr["ExisteTercero"];
                    drNew["IsApproved"] = dr["IsApproved"];
                    drNew["Eliminar"] = dr["Eliminar"];
                    dtData.Rows.Add(drNew);
                }

                return dtData;

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (objSQLComman != null)
                {
                    objSQLComman.Dispose();
                }
                if (objSqlConnection.State == ConnectionState.Open)
                {
                    objSqlConnection.Close();
                }
                objSqlConnection.Close();
            }
        }

        public static DataTable UsuariosPorEliminarQos(string ApplicationName, string RoleName, string textoEmail, string tituloEmail,
            string paso1, string paso2, string paso3, string paso4, DataTable dtInt)
        {
            DataSet ds = new DataSet();
            SqlConnection objSqlConnection = null;
            SqlCommand objSQLComman = null;
            try
            {
                objSQLComman = new SqlCommand("usp_QoS_Proveedor_UsuariosPorEliminar");
                objSQLComman.CommandType = CommandType.StoredProcedure;
                objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["QoS"].ConnectionString);
                objSQLComman.Connection = objSqlConnection;

                objSQLComman.Parameters.AddWithValue("@ApplicationName", ApplicationName);
                objSQLComman.Parameters.AddWithValue("@RoleName", RoleName);
                objSQLComman.Parameters.AddWithValue("@TextoEmail", textoEmail);
                objSQLComman.Parameters.AddWithValue("@TituloEmail", tituloEmail);
                objSQLComman.Parameters.AddWithValue("@Paso1", paso1);
                objSQLComman.Parameters.AddWithValue("@Paso2", paso2);
                objSQLComman.Parameters.AddWithValue("@Paso3", paso3);
                objSQLComman.Parameters.AddWithValue("@Paso4", paso4);

                SqlParameter parData = new SqlParameter("@pData", SqlDbType.Structured);
                parData.TypeName = "dbo.Table_Proveedores_UsuariosPorEliminar";
                parData.Value = dtInt;
                objSQLComman.Parameters.Add(parData);

                objSqlConnection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = objSQLComman;

                adapter.Fill(ds);

                return ds.Tables[0];

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            finally
            {
                if (objSQLComman != null)
                {
                    objSQLComman.Dispose();
                }
                if (objSqlConnection.State == ConnectionState.Open)
                {
                    objSqlConnection.Close();
                }
                objSqlConnection.Close();
            }
        }

    }
}
