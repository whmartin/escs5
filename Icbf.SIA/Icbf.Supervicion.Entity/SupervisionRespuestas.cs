using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionRespuestas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdRespuesta
        {
            get;
            set;
        }
        public int IdPregunta
        {
            get;
            set;
        }
        public String Valor
        {
            get;
            set;
        }
        public Boolean VulneraDerecho
        {
            get;
            set;
        }
        public String VulneraDerechoStr
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String EstadoStr
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SupervisionRespuestas()
        {
        }
    }
}
