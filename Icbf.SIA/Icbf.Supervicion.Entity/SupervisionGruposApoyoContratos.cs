using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionGruposApoyoContratos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int? IdContratosGrupo
        {
            get;
            set;
        }
        public int IdGrupo
        {
            get;
            set;
        }
        public int IdDireccion
        {
            get;
            set;
        }
        public String NombreDireccionICBF
        {
            get;
            set;
        }
        public Int64? IdContrato
        {
            get;
            set;
        }
        public String NumeroContrato
        {
            get;
            set;
        }
        public int IdRegional
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }

        public int Vigencia
        {
            get;
            set;
        }


        public String NumeroDocumentoEntidad
        {
            get;
            set;
        }
        
         public String NombreEntidadContratista
        {
            get;
            set;
        }
        
         public String DireccionEntidadContratista
        {
            get;
            set;
        }

         public String NumeroDocumentoRepLegal
        {
            get;
            set;
        }

         public String NombreRepLegal
        {
            get;
            set;
        }

         public String NumeroDocumentoSupervisor
        {
            get;
            set;
        }
        
        public String NombreSupervisor
        {
            get;
            set;
        }

        public int Estado
        {
            get;
            set;
        }
        public String NombreEstado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaInicioContrato
        {
            get;
            set;
        }
        public DateTime FechaFinContrato
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public String MensajeError
        {
            get;
            set;
        }

        public SupervisionGruposApoyoContratos()
        {
        }
    }
}
