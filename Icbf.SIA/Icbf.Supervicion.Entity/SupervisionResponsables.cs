using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionResponsables : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdResponsable
        {
            get;
            set;
        }
        public int IdDireccion
        {
            get;
            set;
        }
        public string Direccion
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public string EstadoStr
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        
        public SupervisionResponsables()
        {
        }

        
    }
}
