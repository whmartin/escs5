using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionPreguntas : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdPregunta
        {
            get;
            set;
        }
        public int IdDireccion
        {
            get;
            set;
        }
        public String Direccion
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public String Descripcion
        {
            get;
            set;
        }
        public int IdTipoRespuesta
        {
            get;
            set;
        }
        public String NombreTipoRespuesta
        {
            get;
            set;
        }
        public int Aplicable
        {
            get;
            set;
        }
        public String AplicableStr
        {
            get;
            set;
        }
        public int Responsable
        {
            get;
            set;
        }
        public string NombreResponsable 
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String EstadoStr
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SupervisionPreguntas()
        {
        }
    }
}
