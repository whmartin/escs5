using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionGruposApoyoIntegrantes : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdIntegranteGrupo
        {
            get;
            set;
        }
        public int IdGrupo
        {
            get;
            set;
        }
        public String Grupo
        {
            get;
            set;
        }
        public int IdDireccion
        {
            get;
            set;
        }
        public int Identificacion
        {
            get;
            set;
        }
        public String Nombres
        {
            get;
            set;
        }
        public String Apellidos
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SupervisionGruposApoyoIntegrantes()
        {
        }
    }
}
