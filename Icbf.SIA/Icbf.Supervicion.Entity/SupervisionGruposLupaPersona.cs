using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionGruposLupaPersona : Icbf.Seguridad.Entity.EntityAuditoria
    {

        public String TipoPersona
        {
            get;
            set;
        }

        public int IdTipoIdentificacion
        {
            get;
            set;
        }
        public String TipoIdentificacion
        {
            get;
            set;
        }

        public Int64 Identificacion
        {
            get;
            set;
        }
        public String NombreTercero
        {
            get;
            set;
        }

        public String Nombre1
        {
            get;
            set;
        }
        public String Nombre2
        {
            get;
            set;
        }

        public String Apellido1
        {
            get;
            set;
        }
        public String Apellido2
        {
            get;
            set;
        }

        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SupervisionGruposLupaPersona()
        {
        }
    }
}
