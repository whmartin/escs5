using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionSubcomponentes : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdSubcomponente
        {
            get;
            set;
        }
        public int IdDireccion
        {
            get;
            set;
        }
        public String Direccion
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String EstadoStr
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SupervisionSubcomponentes()
        {
        }
    }
}
