﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
  public class SupervisionReglasCriterios
    {

       public int IdCriterioRespuesta
        {
            get;
            set;
        }
        public int IdRespuesta
        {
            get;
            set;
        }
        public int IdCriterio
        {
            get;
            set;
        }
         public int TipoConfiguracion
        {
            get;
            set;
        }

         public int ValorMinimo
        {
            get;
            set;
        }
         public int ValorMaximo
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        } 
        
        public DateTime FechaCrea
        {
            get;
            set;
        }

        public String UsuarioModifica
        {
            get;
            set;
        }
       
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public String MensajeError
        {
            get;
            set;
        }

        public SupervisionReglasCriterios()
        {
        }
 
    }
}
