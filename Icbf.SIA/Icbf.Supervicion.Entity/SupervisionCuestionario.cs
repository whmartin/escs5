using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Icbf.Supervision.Entity
{
    public class SupervisionCuestionario : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdCuestionario
        {
            get;
            set;
        }
        public String Nombre
        {
            get;
            set;
        }
        public int IdDireccion
        {
            get;
            set;
        }
        public String Direccion
        {
            get;
            set;
        }

        public int IdVigenciaServicio
        {
            get;
            set;
        }
        public int IdModalidadServicio
        {
            get;
            set;
        }
        public int IdSubComponente
        {
            get;
            set;
        }
        public int IdComponente
        {
            get;
            set;
        }
        public int EstadoPregunta
        {
            get;
            set;
        }
        public String EstadoPreguntaStr
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public int IdCuestionarioPregunta
        {
            get;
            set;
        }
        //public List<SupervisionPreguntas> IdPregunta
        //{
        //    get;
        //    set;
        //}

        public int IdPregunta
        {
            get;
            set;
        }
        public DataTable Preguntas
        {
            get;
            set;
        }
        public string NombrePregunta 
        { 
            get; 
            set; 
        }
        public int EstadoCuestionarioPregunta
        {
            get;
            set;
        }

        public int Componente
        {
            get;
            set;
        }
        public SupervisionCuestionario()
        {
        }
    }
}
