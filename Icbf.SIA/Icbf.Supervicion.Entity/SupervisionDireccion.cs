using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Supervision.Entity
{
    public class SupervisionDireccion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdDireccionesICBF
        {
            get;
            set;
        }
        public String CodigoDireccion
        {
            get;
            set;
        }
        public String NombreDireccion
        {
            get;
            set;
        }
        public int Estado
        {
            get;
            set;
        }
        public String EstadoStr
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public SupervisionDireccion()
        {
        }
    }
}
