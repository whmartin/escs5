﻿using Icbf.Contrato.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.ServerObejctModel.Contratos
{
    [ServiceContract]
    public interface IServiceContratos
    {
        [OperationContract]
        [WebGet(UriTemplate = "InfoContratoPacco/{IdPlanCompras}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        InfoContratoDTO GetInfoContratoPacco(string IdPlanCompras);

        [OperationContract]
        [WebGet(UriTemplate = "InfoContratoGeneral/{anioContrato}/{codigoRegional}/{numeroContrato}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        InfoContratoDTO GetInfoContratoGeneral(string anioContrato, string codigoRegional, string numeroContrato);

        [OperationContract]
        [WebGet(UriTemplate = "InfoContratosGeneralPorPeriodo/{codigoRegional}/{fechaInicio}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<InfoContratoDTO> GetInfoContratoGeneralPorPeriodo(string codigoRegional, string fechaInicio);

        [OperationContract]
        [WebGet(UriTemplate = "InfoContratosGeneralPorRubro/{codigoRegional}/{vigencia}/{codigoRubro}/{*numeroContrato}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<InfoContratoDTO> GetInfoContratoGeneralPorRubro(string codigoRegional, string vigencia, string codigoRubro,string numeroContrato);
    }
}
