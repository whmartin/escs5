﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using System.ServiceModel.Web;
using System.Net;
using System.ServiceModel.Activation;

namespace Icbf.SIA.Integrations.ServerObejctModel.Contratos
{
    [AspNetCompatibilityRequirements( RequirementsMode=AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceContratos : IServiceContratos
    {
        private IntegrationService _IntegrationService;

        public ServiceContratos()
        {
            _IntegrationService = new IntegrationService();
        }

        public InfoContratoDTO GetInfoContratoPacco(string IdPlanCompras)
        {
            InfoContratoDTO resultList = null;
            try
            {
                int idPlan = 0;

                if (int.TryParse(IdPlanCompras, out idPlan))
                {
                    resultList = _IntegrationService.ObtenerInfoContratosPaccoPorId(idPlan);    
                }
                else
                {
                    throw new WebFaultException(HttpStatusCode.BadRequest); 
                }
            }
            catch (Exception ex)
            {

           }

            return resultList;
        }

        public InfoContratoDTO GetInfoContratoGeneral(string anioContrato, string codigoRegional, string numeroContrato)
        {
            InfoContratoDTO result = null;
            try
            {
                int año = 0, numero = 0;

                if (int.TryParse(anioContrato, out año) && int.TryParse(numeroContrato, out numero))
                    result = _IntegrationService.ObtenerInfoContratosGeneral(año, numero, codigoRegional);
                else
                    throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(HttpStatusCode.InternalServerError);
            }

            return result;
        }

        public List<InfoContratoDTO> GetInfoContratoGeneralPorPeriodo(string codigoRegional, string fechaContrato)
        {
            List<InfoContratoDTO> result = null;
            try
            {
                DateTime fechaDesde;
                int anio;

                if (DateTime.TryParse(fechaContrato, out fechaDesde))
                {
                    DateTime fechaHasta = fechaDesde.AddDays(7);
                    result = _IntegrationService.ObtenerInfoContratoGeneralPorPeriodo(codigoRegional, fechaDesde, fechaHasta);
                }
                else
                    throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(HttpStatusCode.InternalServerError);
            }

            return result;
        }

        public List<InfoContratoDTO> GetInfoContratoGeneralPorRubro(string codigoRegional, string vigencia, string codigoRubro, string numeroContrato)
        {
            List<InfoContratoDTO> result = null;
            try
            {
                int numeroContratoOut = 0;
                int vigenciaOut;

                bool isValidNumeroContrato = true;

                if (!string.IsNullOrEmpty(numeroContrato) && !int.TryParse(numeroContrato, out numeroContratoOut))
                    isValidNumeroContrato = false;

                if ( vigencia.Length == 4 && int.TryParse(vigencia, out vigenciaOut) && isValidNumeroContrato)
                {
                    result = _IntegrationService.GetInfoContratoGeneralPorRubro(codigoRegional, vigenciaOut, codigoRubro,numeroContratoOut);
                }
                else
                    throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(HttpStatusCode.InternalServerError);
            }

            return result;
        }
    }
}
