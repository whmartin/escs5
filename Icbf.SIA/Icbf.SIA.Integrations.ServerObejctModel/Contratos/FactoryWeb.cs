﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.ServerObejctModel.Contratos
{

    public class MyFactoryContratos : ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            ServiceHost host = new ServiceHost(serviceType, baseAddresses);
            WebHttpBehavior httpBehaivor = new WebHttpBehavior();
            httpBehaivor.HelpEnabled = true;

            WebHttpBinding httpBinding;

            if(baseAddresses.Any(e => e.Scheme == "https"))
                httpBinding = new WebHttpBinding(WebHttpSecurityMode.Transport);
            else
                httpBinding = new WebHttpBinding(WebHttpSecurityMode.None);

            host.AddServiceEndpoint(typeof(IServiceContratos),httpBinding,"").Behaviors.Add(httpBehaivor);

            ServiceMetadataBehavior smb = host.Description.Behaviors.Find<ServiceMetadataBehavior>();

            if(smb != null)
            {
                if(baseAddresses.Any(e => e.Scheme == "http"))
                smb.HttpGetEnabled = true;
                else
                smb.HttpGetEnabled = false;

                if(baseAddresses.Any(e => e.Scheme == "https"))
                smb.HttpsGetEnabled = true;
                else
                smb.HttpsGetEnabled = false;
            }

            ServiceDebugBehavior sdb = host.Description.Behaviors.Find<ServiceDebugBehavior>();
            sdb.IncludeExceptionDetailInFaults = true;
            return host;
        }
    }
}
