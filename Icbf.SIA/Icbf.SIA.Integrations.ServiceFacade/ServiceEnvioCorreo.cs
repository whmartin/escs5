﻿using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Integrations.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace Icbf.SIA.Integrations.ServiceFacade
{
    public class ServiceEnvioCorreo
    {
        public RolDAL vRolDAL = new RolDAL();
        public FeriadosDAL vFeriadosDAL = new FeriadosDAL();

        public List<EnvioCorreo> ConsultarListaCorreos()
        {
            try
            {
                return new EnvioCorreoDAL().ConsultarListaCorreos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EnvioCorreo ConsultarListaCorreosPorId(int IdDenunciaBien)
        {
            try
            {
                return new EnvioCorreoDAL().ConsultarListaCorreosPorId(IdDenunciaBien);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Consulta los roles por el nombre de programa y el nombre de la función
        /// </summary>
        /// <param name="pNombrePrograma">Nombre del programa</param>
        /// <param name="pNombreFuncion">Nombre de la función</param>
        /// <returns>lista de roles</returns>
        public List<Rol> ConsultarRolesPorProgramaFuncion(string pNombrePrograma, string pNombreFuncion)
        {
            try
            {
                return vRolDAL.ConsultarRolesPorProgramaFuncion(pNombrePrograma, pNombreFuncion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Método para consultar el correo del enlace de área.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoCoordinadorJuridico(int? pIdRegional, string pProviderKey)
        {
            try
            {
                return vRolDAL.ConsultarCorreoCoordinadorJuridico(pIdRegional, pProviderKey);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// obtiene el correo del cordinador juridico
        /// </summary>
        /// <param name="pIdRegionalDestino">id de la regional</param>
        /// <param name="pkeys">keys de busqueda</param>
        /// <returns>correo del coordinador juridico</returns>
        public string GetCorreoCoordinadorJuridico(int pIdRegional, bool pEsOrigen)
        {
            string vCorreoCoordinadorJuridicoRegional = string.Empty;
            try
            {
                string vkeys = string.Empty;
                List<Icbf.Seguridad.Entity.Rol> vListaRoles = this.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", "COORDINADOR JURIDICO");
                List<string> vUsuarioRol = new List<string>();
                foreach (Icbf.Seguridad.Entity.Rol item in vListaRoles)
                {
                    vUsuarioRol.AddRange(Roles.GetUsersInRole(item.NombreRol).ToList());
                }

                foreach (string item in vUsuarioRol)
                {
                    vkeys = string.Concat(vkeys, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? string.Empty : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
                }

                vkeys = vkeys.Trim().TrimEnd(',');
                vCorreoCoordinadorJuridicoRegional = this.ConsultarCorreoCoordinadorJuridico(pIdRegional, vkeys);

                if (vCorreoCoordinadorJuridicoRegional.Split(';').Count() > 1)
                {
                    string vMensaje = pEsOrigen ? "Existe  más de un coordinador para la regional origen" : "Existe  más de un coordinador para la regional destino";
                    vCorreoCoordinadorJuridicoRegional = null;
                    throw new Exception(vMensaje);
                }

                return vCorreoCoordinadorJuridicoRegional;
            }
            catch (Exception e)
            {
                return vCorreoCoordinadorJuridicoRegional;
            }
        }

        #region Feriados
        public List<Feriados> ObtenerFeriados()
        {
            try
            {
                return vFeriadosDAL.ObtenerFeriados();
            }

            catch (Exception ex)
            {
                throw(ex);
            }
        }
        #endregion

    }
}
