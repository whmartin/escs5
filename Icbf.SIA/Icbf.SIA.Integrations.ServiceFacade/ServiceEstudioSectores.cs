﻿using Icbf.EstudioSectorCosto.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icbf.SIA.Integrations.DataAccess;

namespace Icbf.SIA.Integrations.ServiceFacade
{
    public class ServiceEstudioSectores
    {
        /// <summary>
        /// Método para consultar los cálculos de las fechas PACCO estudio sector
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id del consecutivo de estudio</param>
        /// <param name="pFechaEstimadaInicioEjecucion">Fecha que proviene de ws PACCO</param>
        /// <param name="pIdModalidadSeleccionPACCO">Id Modalidad que proviene de ws PACCO</param>
        /// <param name="pModalidadSeleccionPACCO">Modalidad que proviene de ws PACCO</param>
        /// <param name="pValorContrato">Valor contrato que proviene de ws PACCO</param>
        /// <returns>Tiempos con fechas PACCO</returns>
        public TiemposPACCO ConsultarFehasPACCO(decimal pIdConsecutivoEstudio, DateTime pFechaEstimadaInicioEjecucion, decimal pIdModalidadSeleccionPACCO, string pModalidadSeleccionPACCO, decimal pValorContrato)
        {
            try
            {
                return new IntegrationEstudioSectoresDAL().ConsultarFehasPACCO(pIdConsecutivoEstudio, pFechaEstimadaInicioEjecucion, pIdModalidadSeleccionPACCO, pModalidadSeleccionPACCO, pValorContrato);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}