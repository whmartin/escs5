﻿
using Icbf.Contrato.Entity;
using Icbf.SIA.Integrations.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.ServiceFacade
{
    public class ServiceContratos
    {
        public List<InfoContratoPaccoDTO> ObtenerInfoContratosPaccoPorId(int idPlanCompras)
        {
            try
            {
                return new IntegrationContratosDAL().ObtenerInfoContratosPaccoPorId(idPlanCompras);
            }
            catch (Exception ex)
            {   
                throw ex;
            }
        }
    }
}
