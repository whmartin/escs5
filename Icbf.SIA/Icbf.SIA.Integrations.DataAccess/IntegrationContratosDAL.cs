﻿using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.DataAccess
{
    public class IntegrationContratosDAL : GeneralDAL
    {
        public IntegrationContratosDAL(){}

        public List<InfoContratoPaccoDTO> ObtenerInfoContratosPaccoPorId(int idPlanCompras)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();

                Database vDataBase = factory.CreateDefault();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_Integration_ConsultarContratoPacco"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdPlanCompras", DbType.Int32, idPlanCompras);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InfoContratoPaccoDTO> vListaCodigos = new List<InfoContratoPaccoDTO>();

                        int idContrato = 0;

                        while (vDataReaderResults.Read())
                        {
                            InfoContratoPaccoDTO vInfoContratoPaccoCDP = new InfoContratoPaccoDTO();
                            idContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Consecutivo"].ToString()) : 0;
                            vInfoContratoPaccoCDP.IdContrato = idContrato;
                            vInfoContratoPaccoCDP.Consecutivo = vDataReaderResults["Consecutivo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Consecutivo"].ToString()) : vInfoContratoPaccoCDP.Consecutivo;
                            vInfoContratoPaccoCDP.Vigencia = vDataReaderResults["Vigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Vigencia"].ToString()) : vInfoContratoPaccoCDP.Vigencia;
                            vInfoContratoPaccoCDP.CodRegional = vDataReaderResults["CodRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodRegional"].ToString()) : vInfoContratoPaccoCDP.CodRegional;
                            vInfoContratoPaccoCDP.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vInfoContratoPaccoCDP.NombreRegional;
                            vInfoContratoPaccoCDP.EstadoContrato = vDataReaderResults["EstadoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoContrato"].ToString()) : vInfoContratoPaccoCDP.EstadoContrato;
                            vInfoContratoPaccoCDP.NumeroDocumento = vDataReaderResults["NumeroDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroDocumento"].ToString()) : vInfoContratoPaccoCDP.NumeroDocumento;
                            vInfoContratoPaccoCDP.TipoDocumento = vDataReaderResults["TipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumento"].ToString()) : vInfoContratoPaccoCDP.NumeroDocumento;
                            vInfoContratoPaccoCDP.FechaContrato = vDataReaderResults["FechaContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaContrato"].ToString()) : vInfoContratoPaccoCDP.FechaContrato;
                            vInfoContratoPaccoCDP.ValorContrato = vDataReaderResults["ValorContrato"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorContrato"].ToString()) : vInfoContratoPaccoCDP.ValorContrato;
                            vInfoContratoPaccoCDP.IdModalidad = vDataReaderResults["IdModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdModalidad"].ToString()) : vInfoContratoPaccoCDP.IdModalidad;
                            vInfoContratoPaccoCDP.NombreModalidad = vDataReaderResults["NombreModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreModalidad"].ToString()) : vInfoContratoPaccoCDP.NombreModalidad;
                            vInfoContratoPaccoCDP.TipoPersona = vDataReaderResults["TipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoPersona"].ToString()) : vInfoContratoPaccoCDP.TipoPersona;
                            vInfoContratoPaccoCDP.Tercero = vDataReaderResults["Tercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Tercero"].ToString()) : vInfoContratoPaccoCDP.Tercero;
                            vInfoContratoPaccoCDP.TerceroDescripcion = vDataReaderResults["TerceroDescripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TerceroDescripcion"].ToString()) : vInfoContratoPaccoCDP.TerceroDescripcion;
                            vInfoContratoPaccoCDP.TipoDocumento = vDataReaderResults["TipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumento"].ToString()) : vInfoContratoPaccoCDP.TipoDocumento;
                            vInfoContratoPaccoCDP.TipoContrato = vDataReaderResults["TipoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoContrato"].ToString()) : vInfoContratoPaccoCDP.TipoContrato;
                            vInfoContratoPaccoCDP.FechaFinalizacionInicialContrato = vDataReaderResults["FechaFinalizacionInicialContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalizacionInicialContrato"].ToString()) : vInfoContratoPaccoCDP.FechaFinalizacionInicialContrato;
                            vInfoContratoPaccoCDP.FechaFinalTerminacionContrato = vDataReaderResults["FechaFinalTerminacionContrato"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFinalTerminacionContrato"].ToString()) : vInfoContratoPaccoCDP.FechaFinalTerminacionContrato;
                            

                            var misCDPS = ConsultarContratosCDP(idContrato);

                            if (misCDPS.Count > 0)
                            {
                                vInfoContratoPaccoCDP.CDP = new List<InfoContratoCDPDTO>();

                                foreach (var item in misCDPS)
                                    vInfoContratoPaccoCDP.CDP.Add(new InfoContratoCDPDTO() { CDP = item.NumeroCDP, Fecha = item.FechaCDP });
                                
                            }

                            var misRPS = ConsultarRPContratosAsociados(idContrato, null);

                            if (misRPS.Count > 0)
                            {
                                vInfoContratoPaccoCDP.RP = new List<InfoContratoRPDTO>();

                                foreach (var item in misRPS)
                                    vInfoContratoPaccoCDP.RP.Add(new InfoContratoRPDTO() { RP = item.NumeroRP, Fecha = item.FechaExpedicionRP });
                            }

                            vListaCodigos.Add(vInfoContratoPaccoCDP);
                        }

                        return vListaCodigos;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private List<ContratosCDPDTO> ConsultarContratosCDP(int pIdContrato)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();

                Database vDataBase = factory.CreateDefault();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_ContratosCDP_Obtener"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContratosCDPDTO> vListaContratosCDP = new List<ContratosCDPDTO>();
                        while (vDataReaderResults.Read())
                        {
                            ContratosCDPDTO vContratosCDP = new ContratosCDPDTO();
                            vContratosCDP.IdContratosCDP = vDataReaderResults["IdContratosCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContratosCDP"].ToString()) : vContratosCDP.IdContratosCDP;
                            vContratosCDP.IdCDP = vDataReaderResults["IdCDP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdCDP"].ToString()) : vContratosCDP.IdCDP;
                            vContratosCDP.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vContratosCDP.IdContrato;
                            vContratosCDP.Regional = vDataReaderResults["Regional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Regional"].ToString()) : vContratosCDP.Regional;
                            vContratosCDP.Area = vDataReaderResults["Area"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Area"].ToString()) : vContratosCDP.Area;
                            vContratosCDP.NumeroCDP = vDataReaderResults["NumeroCDP"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCDP"].ToString()) : vContratosCDP.NumeroCDP;
                            vContratosCDP.FechaCDP = vDataReaderResults["FechaCDP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCDP"].ToString()) : vContratosCDP.FechaCDP;
                            vContratosCDP.ValorCDP = vDataReaderResults["ValorCDP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorCDP"].ToString()) : vContratosCDP.ValorCDP;
                            vContratosCDP.RubroPresupuestal = vDataReaderResults["RubroPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RubroPresupuestal"].ToString()) : vContratosCDP.RubroPresupuestal;
                            vContratosCDP.TipoFuenteFinanciamiento = vDataReaderResults["TipoFuenteFinanciamiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoFuenteFinanciamiento"].ToString()) : vContratosCDP.TipoFuenteFinanciamiento;
                            vContratosCDP.RecursoPresupuestal = vDataReaderResults["RecursoPresupuestal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RecursoPresupuestal"].ToString()) : vContratosCDP.RecursoPresupuestal;
                            vContratosCDP.DependenciaAfectacionGastos = vDataReaderResults["DependenciaAfectacionGastos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DependenciaAfectacionGastos"].ToString()) : vContratosCDP.DependenciaAfectacionGastos;
                            vContratosCDP.TipoDocumentoSoporte = vDataReaderResults["TipoDocumentoSoporte"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoDocumentoSoporte"].ToString()) : vContratosCDP.TipoDocumentoSoporte;
                            vContratosCDP.TipoSituacionFondos = vDataReaderResults["TipoSituacionFondos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipoSituacionFondos"].ToString()) : vContratosCDP.TipoSituacionFondos;
                            vContratosCDP.ConsecutivoPlanCompras = vDataReaderResults["ConsecutivoPlanCompras"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPlanCompras"].ToString()) : vContratosCDP.ConsecutivoPlanCompras;
                            vContratosCDP.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContratosCDP.UsuarioCrea;
                            vContratosCDP.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContratosCDP.FechaCrea;
                            vContratosCDP.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContratosCDP.UsuarioModifica;
                            vContratosCDP.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContratosCDP.FechaModifica;
                            vContratosCDP.CodigoRubro = vDataReaderResults["CodigoRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRubro"].ToString()) : vContratosCDP.CodigoRubro;
                            vContratosCDP.DescripcionRubro = vDataReaderResults["DescripcionRubro"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionRubro"].ToString()) : vContratosCDP.DescripcionRubro;
                            vContratosCDP.ValorRubro = vDataReaderResults["ValorRubro"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRubro"].ToString()) : vContratosCDP.ValorRubro;
                            vListaContratosCDP.Add(vContratosCDP);
                        }
                        return vListaContratosCDP;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<RPContratoDTO> ConsultarRPContratosAsociados(int? pIdContrato, int? pIdRP)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();

                Database vDataBase = factory.CreateDefault();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_CONTRATO_RPContratoss_Consultar"))
                {
                    if (pIdContrato != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, pIdContrato);
                    if (pIdRP != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRP", DbType.Int32, pIdRP);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RPContratoDTO> vListaAsociarRPContrato = new List<RPContratoDTO>();
                        while (vDataReaderResults.Read())
                        {
                            RPContratoDTO vAsociarRPContrato = new RPContratoDTO();
                            vAsociarRPContrato.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vAsociarRPContrato.IdContrato;
                            vAsociarRPContrato.IdRP = vDataReaderResults["IdRP"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRP"].ToString()) : vAsociarRPContrato.IdRP;
                            vAsociarRPContrato.ValorRP = vDataReaderResults["ValorRP"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ValorRP"].ToString()) : vAsociarRPContrato.ValorRP;
                            vAsociarRPContrato.CodigoRegional = vDataReaderResults["CodigoRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegional"].ToString()) : vAsociarRPContrato.CodigoRegional;
                            vAsociarRPContrato.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreRegional"].ToString()) : vAsociarRPContrato.NombreRegional;
                            vAsociarRPContrato.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAsociarRPContrato.UsuarioCrea;
                            vAsociarRPContrato.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAsociarRPContrato.FechaCrea;
                            vAsociarRPContrato.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAsociarRPContrato.UsuarioModifica;
                            vAsociarRPContrato.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAsociarRPContrato.FechaModifica;
                            vAsociarRPContrato.FechaExpedicionRP = vDataReaderResults["FechaExpedicionRP"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpedicionRP"].ToString()) : vAsociarRPContrato.FechaExpedicionRP;
                            vAsociarRPContrato.NumeroRP = vDataReaderResults["NumeroCompromiso"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroCompromiso"].ToString()) : vAsociarRPContrato.NumeroRP;

                            vListaAsociarRPContrato.Add(vAsociarRPContrato);
                        }
                        return vListaAsociarRPContrato;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
