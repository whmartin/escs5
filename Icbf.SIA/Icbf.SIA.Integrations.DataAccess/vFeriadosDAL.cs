﻿using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.DataAccess
{
    public class FeriadosDAL : GeneralDAL
    {
        /// <summary>
        /// Consulta ObtenerFeriados
        /// </summary>
        /// <returns>List tipo Feriados con la información consultada</returns>
        public List<Feriados> ObtenerFeriados()
        {
            try
            {
                //Database vDataBase = ObtenerInstancia();
                EnvioCorreo vEnvioCorreo = new EnvioCorreo();
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database vDataBase = factory.CreateDefault();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_BVMVH_Mostrencos_ConsultarFeriados"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Feriados> vFeriadosList = new List<Feriados>();
                        while (vDataReaderResults.Read())
                        {
                            Feriados vFeriados = new Feriados();
                            vFeriados.fedID = vDataReaderResults["fedID"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["fedID"].ToString()) : vFeriados.fedID;
                            vFeriados.fed_descripcion = vDataReaderResults["fed_descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["fed_descripcion"].ToString()) : vFeriados.fed_descripcion;
                            vFeriadosList.Add(vFeriados);
                        }
                        return vFeriadosList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
