﻿using Icbf.Mostrencos.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.DataAccess
{
    public class EnvioCorreoDAL : GeneralDAL
    {
        public EnvioCorreoDAL() { }

        //Consultar Datos Informe Denunciante
        public List<EnvioCorreo> ConsultarListaCorreos()
        {
            try
            {
                List<EnvioCorreo> vlstEnvioCorreo = new List<EnvioCorreo>();
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database vDataBase = factory.CreateDefault();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Consultar_Lista_Correos"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            EnvioCorreo vEnvioCorreo = new EnvioCorreo();
                            vEnvioCorreo.IdDenunciaBien = vDataReaderResults["IdDenunciaBien"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDenunciaBien"].ToString()) : vEnvioCorreo.IdDenunciaBien;
                            vEnvioCorreo.IdRegionalUbicacion = vDataReaderResults["IdRegionalUbicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalUbicacion"].ToString()) : vEnvioCorreo.IdRegionalUbicacion;
                            vEnvioCorreo.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? vDataReaderResults["NombreRegional"].ToString() : vEnvioCorreo.NombreRegional;
                            vEnvioCorreo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEnvioCorreo.FechaCrea;
                            vEnvioCorreo.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vEnvioCorreo.RadicadoDenuncia;
                            vEnvioCorreo.CorreoAbogado = vDataReaderResults["CorreoAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoAbogado"].ToString()) : vEnvioCorreo.CorreoAbogado;
                            vEnvioCorreo.CorreoDenunciante = vDataReaderResults["CorreoDenunciante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoDenunciante"].ToString()) : vEnvioCorreo.CorreoDenunciante;
                            vlstEnvioCorreo.Add(vEnvioCorreo);
                        }
                        return vlstEnvioCorreo;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EnvioCorreo ConsultarListaCorreosPorId(int IdDenunciaBien)
        {
            try
            {
                EnvioCorreo vEnvioCorreo = new EnvioCorreo();
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database vDataBase = factory.CreateDefault();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Consultar_Lista_Correos_Por_IdDenuncia"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDenunciaBien", DbType.Int32, IdDenunciaBien);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vEnvioCorreo.NombreEstadoDenuncia = vDataReaderResults["NombreEstadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstadoDenuncia"].ToString()) : vEnvioCorreo.NombreEstadoDenuncia;
                            vEnvioCorreo.IdRegionalUbicacion = vDataReaderResults["IdRegionalUbicacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegionalUbicacion"].ToString()) : vEnvioCorreo.IdRegionalUbicacion;
                            vEnvioCorreo.NombreRegional = vDataReaderResults["NombreRegional"] != DBNull.Value ? vDataReaderResults["NombreRegional"].ToString() : vEnvioCorreo.NombreRegional;
                            vEnvioCorreo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEnvioCorreo.FechaCrea;
                            vEnvioCorreo.FechaInforme = vDataReaderResults["FechaInforme"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInforme"].ToString()) : vEnvioCorreo.FechaInforme;
                            vEnvioCorreo.RadicadoDenuncia = vDataReaderResults["RadicadoDenuncia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RadicadoDenuncia"].ToString()) : vEnvioCorreo.RadicadoDenuncia;
                            vEnvioCorreo.CorreoAbogado = vDataReaderResults["CorreoAbogado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoAbogado"].ToString()) : vEnvioCorreo.CorreoAbogado;
                            vEnvioCorreo.CorreoDenunciante = vDataReaderResults["CorreoDenunciante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CorreoDenunciante"].ToString()) : vEnvioCorreo.CorreoDenunciante;
                        }
                        return vEnvioCorreo;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
