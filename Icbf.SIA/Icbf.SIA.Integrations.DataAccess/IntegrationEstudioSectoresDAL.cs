﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace Icbf.SIA.Integrations.DataAccess
{
    public class IntegrationEstudioSectoresDAL:GeneralDAL
    {
        /// <summary>
        /// Método para consultar los cálculos de las fechas PACCO estudio sector
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id del consecutivo de estudio</param>
        /// <param name="pFechaEstimadaInicioEjecucion">Fecha que proviene de ws PACCO</param>
        /// <param name="pIdModalidadSeleccionPACCO">Id Modalidad que proviene de ws PACCO</param>
        /// <param name="pModalidadSeleccionPACCO">Modalidad que proviene de ws PACCO</param>
        /// <param name="pValorContrato">Valor contrato que proviene de ws PACCO</param>
        /// <returns>Tiempos con fechas PACCO</returns>
        public TiemposPACCO ConsultarFehasPACCO(decimal pIdConsecutivoEstudio,DateTime pFechaEstimadaInicioEjecucion,decimal pIdModalidadSeleccionPACCO,string pModalidadSeleccionPACCO, decimal pValorContrato)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();

                Database vDataBase = factory.CreateDefault();
                
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_CalcularFechas_PACCO"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Decimal, pIdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaEstimadaInicioEjecucion", DbType.DateTime, pFechaEstimadaInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@IdModalidadSeleccionPACCO", DbType.Decimal, pIdModalidadSeleccionPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ModalidadSeleccionPACCO", DbType.String, pModalidadSeleccionPACCO);
                    vDataBase.AddInParameter(vDbCommand, "@ValorContrato", DbType.Decimal, pValorContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TiemposPACCO vTiempoEntreActividadesEstudioSyCPACCO = new TiemposPACCO();
                        while (vDataReaderResults.Read())
                        {
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaInicioDelPS = vDataReaderResults["FEstimadaInicioDelPS"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FEstimadaInicioDelPS"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaInicioDelPS;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaPresentacionPropuestas = vDataReaderResults["FechaEstimadaPresentacionPropuestas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FechaEstimadaPresentacionPropuestas"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaPresentacionPropuestas;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaComiteContratacion = vDataReaderResults["FEstimadaComiteContratacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FEstimadaComiteContratacion"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaComiteContratacion;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaDocsRadicadosEnContratos = vDataReaderResults["FestimadadocsRadicadosEnContratos"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FestimadadocsRadicadosEnContratos"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaDocsRadicadosEnContratos;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEestimadaES_EC = vDataReaderResults["FEstimadaES_EC"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FEstimadaES_EC"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEestimadaES_EC;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisado"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaFCTPreliminar = vDataReaderResults["FestimadaFCTPreliminar"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FestimadaFCTPreliminar"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaFCTPreliminar;
                            vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaFCTPreliminarREGINICIAL = vDataReaderResults["FestimadaFCTPreliminarREGINICIAL"] != DBNull.Value ? Convert.ToString(vDataReaderResults["FestimadaFCTPreliminarREGINICIAL"].ToString()) : vTiempoEntreActividadesEstudioSyCPACCO.FEstimadaFCTPreliminarREGINICIAL;                           
                                                        
                        }
                        return vTiempoEntreActividadesEstudioSyCPACCO;
                    }
                }
            }
            
            catch (Exception ex)
            {
                throw ex;
            }

        }
       
    }
}
