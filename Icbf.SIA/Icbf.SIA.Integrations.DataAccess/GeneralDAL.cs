﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.DataAccess
{
    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class GeneralDAL
    {
        public GeneralDAL(){}

        public Database ObtenerInstancia()
        {
            return DatabaseFactory.CreateDatabase();
        }
    }
}
