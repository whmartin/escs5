﻿using Icbf.Seguridad.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.SIA.Integrations.DataAccess
{
    /// <summary>
    /// Clase que contiene todos los métodos para interactuar con la base de datos a través de EnterpriseLibrary
    /// </summary>
    public class RolDAL : GeneralDAL
    {
        public RolDAL() { }

        /// <summary>
        /// Consulta los roles por el nombre de programa y el nombre de la función
        /// </summary>
        /// <param name="pNombrePrograma">Nombre del programa</param>
        /// <param name="pNombreFuncion">Nombre de la función</param>
        /// <returns>lista de roles</returns>
        public List<Rol> ConsultarRolesPorProgramaFuncion(string pNombrePrograma, string pNombreFuncion)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database vDataBase = factory.CreateDefault();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_Usuario_ConsultarPorProgramaFuncion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@NombrePrograma", DbType.String, pNombrePrograma);
                    vDataBase.AddInParameter(vDbCommand, "@NombreFuncion", DbType.String, pNombreFuncion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Rol> vListaRoles = new List<Rol>();
                        while (vDataReaderResults.Read())
                        {
                            Rol vRol = new Rol();
                            vRol.IdRol = vDataReaderResults["IdRol"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRol"].ToString()) : vRol.IdRol;
                            vRol.NombreRol = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vRol.NombreRol;
                            vListaRoles.Add(vRol);
                            vRol = null;
                        }

                        return vListaRoles;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ConsultarCorreoCoordinadorJuridico(int? pIdRegional, string pProviderKey)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database vDataBase = factory.CreateDefault();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Mostrencos_BVMVH_RegistroDenuncia_ConsultarCorreosCoordinadorJuridico"))
                {
                    if (pIdRegional != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdRegional", DbType.Int32, pIdRegional);
                    }

                    if (pProviderKey != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ProviderKey", DbType.String, pProviderKey);
                    }

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        string vCorreoCoordinadorJuridico = string.Empty;

                        while (vDataReaderResults.Read())
                        {
                            vCorreoCoordinadorJuridico = string.Concat(vCorreoCoordinadorJuridico, (vDataReaderResults["CorreoElectronico"] != DBNull.Value ? vDataReaderResults["CorreoElectronico"] : vCorreoCoordinadorJuridico), "; ");
                        }

                        vCorreoCoordinadorJuridico = (!string.IsNullOrEmpty(vCorreoCoordinadorJuridico)) ? vCorreoCoordinadorJuridico.Remove(vCorreoCoordinadorJuridico.Length - 2) : vCorreoCoordinadorJuridico;

                        return vCorreoCoordinadorJuridico;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
