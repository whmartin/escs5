using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionPreguntasBLL
    {
        private SupervisionPreguntasDAL vSupervisionPreguntasDAL;
        public SupervisionPreguntasBLL()
        {
            vSupervisionPreguntasDAL = new SupervisionPreguntasDAL();
        }
        public int InsertarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                return vSupervisionPreguntasDAL.InsertarSupervisionPreguntas(pSupervisionPreguntas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                return vSupervisionPreguntasDAL.ModificarSupervisionPreguntas(pSupervisionPreguntas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                return vSupervisionPreguntasDAL.EliminarSupervisionPreguntas(pSupervisionPreguntas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionPreguntas ConsultarSupervisionPreguntas(int pIdPregunta)
        {
            try
            {
                return vSupervisionPreguntasDAL.ConsultarSupervisionPreguntas(pIdPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int ConsultarSupervisionPreguntas(string NombrePregunta, int? Aplicable)
        {
            try
            {
                return vSupervisionPreguntasDAL.ConsultarSupervisionPreguntas(NombrePregunta, Aplicable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionPreguntas> ConsultarSupervisionPreguntass(int? pIdPregunta,int? pIdDireccion, String pNombre, String pDescripcion, int? pIdTipoRespuesta, int? pAplicable, int? pResponsable, int? pEstado, String pUsuarioCrea)
        {
            try
            {
                return vSupervisionPreguntasDAL.ConsultarSupervisionPreguntass(pIdPregunta,pIdDireccion, pNombre, pDescripcion, pIdTipoRespuesta, pAplicable, pResponsable, pEstado,pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionPreguntas> ConsultarTipoRespuesta()
        {
            try
            {
                return vSupervisionPreguntasDAL.ConsultarTipoRespuesta();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
    }
}
