using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionGruposApoyoContratosBLL
    {
        private SupervisionGruposApoyoContratosDAL vSupervisionGruposApoyoContratosDAL;
        public SupervisionGruposApoyoContratosBLL()
        {
            vSupervisionGruposApoyoContratosDAL = new SupervisionGruposApoyoContratosDAL();
        }
        public int InsertarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos, System.Data.DataTable pDtbContratos)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.InsertarSupervisionGruposApoyoContratos(pSupervisionGruposApoyoContratos, pDtbContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.ModificarSupervisionGruposApoyoContratos(pSupervisionGruposApoyoContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.EliminarSupervisionGruposApoyoContratos(pSupervisionGruposApoyoContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyoContratos ConsultarSupervisionGruposApoyoContratos(int pIdContratosGrupo, int pIdGrupo, int pIdContrato)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.ConsultarSupervisionGruposApoyoContratos(pIdContratosGrupo, pIdGrupo, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionGruposApoyoContratos(String pNombre)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.ConsultarSupervisionGruposApoyoContratos(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionGruposApoyoContratos> ConsultarSupervisionGruposApoyoContratoss(int? pIdContratosGrupo, int? pIdGrupo, int? pIdDireccion, int? pIdRegional, int? pVigencia, int? pEstado)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.ConsultarSupervisionGruposApoyoContratoss(pIdContratosGrupo, pIdGrupo, pIdDireccion, pIdRegional, pVigencia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionGruposApoyoContratos> ConsultarContratosCuentame(Int32? pVigencia, string pIdRegional, Int16 IdDireccion)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.ConsultarContratosCuentame(pVigencia, pIdRegional, IdDireccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }


        public List<SupervisionGruposApoyoContratos> ConsultarContratosSim(int? pVigencia, string pIdRegional)
        {
            try
            {
                return vSupervisionGruposApoyoContratosDAL.ConsultarContratosSim(pVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

    }
}
