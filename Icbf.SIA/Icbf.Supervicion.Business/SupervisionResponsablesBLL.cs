using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionResponsablesBLL
    {
        private SupervisionResponsablesDAL vSupervisionResponsablesDAL;
        public SupervisionResponsablesBLL()
        {
            vSupervisionResponsablesDAL = new SupervisionResponsablesDAL();
        }
        public int InsertarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                return vSupervisionResponsablesDAL.InsertarSupervisionResponsables(pSupervisionResponsables);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                return vSupervisionResponsablesDAL.ModificarSupervisionResponsables(pSupervisionResponsables);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                return vSupervisionResponsablesDAL.EliminarSupervisionResponsables(pSupervisionResponsables);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionResponsables ConsultarSupervisionResponsables(int pIdResponsable)
        {
            try
            {
                return vSupervisionResponsablesDAL.ConsultarSupervisionResponsables(pIdResponsable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionResponsables(string Nombre)
        {
            try
            {
                return vSupervisionResponsablesDAL.ConsultarSupervisionResponsables(Nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionResponsables> ConsultarSupervisionResponsabless(int? pIdDireccion, String pNombre, int? pEstado)
        {
            try
            {
                return vSupervisionResponsablesDAL.ConsultarSupervisionResponsabless(pIdDireccion, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
