using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionDireccionBLL
    {
        private SupervisionDireccionDAL vSupervisionDireccionDAL;
        public SupervisionDireccionBLL()
        {
            vSupervisionDireccionDAL = new SupervisionDireccionDAL();
        }

        public List<SupervisionDireccion> ConsultarSupervisionDireccions(int? pIdDireccionesICBF, String pCodigoDireccion, String pNombreDireccion, int? pEstado)
        {
            try
            {
                return vSupervisionDireccionDAL.ConsultarSupervisionDireccions(pIdDireccionesICBF, pCodigoDireccion, pNombreDireccion,pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
