using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionGruposApoyoBLL
    {
        private SupervisionGruposApoyoDAL vSupervisionGruposApoyoDAL;
        public SupervisionGruposApoyoBLL()
        {
            vSupervisionGruposApoyoDAL = new SupervisionGruposApoyoDAL();
        }
        public int InsertarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                return vSupervisionGruposApoyoDAL.InsertarSupervisionGruposApoyo(pSupervisionGruposApoyo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                return vSupervisionGruposApoyoDAL.ModificarSupervisionGruposApoyo(pSupervisionGruposApoyo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                return vSupervisionGruposApoyoDAL.EliminarSupervisionGruposApoyo(pSupervisionGruposApoyo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyo ConsultarSupervisionGruposApoyo(int pIdGrupo)
        {
            try
            {
                return vSupervisionGruposApoyoDAL.ConsultarSupervisionGruposApoyo(pIdGrupo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
       public int ConsultarSupervisionGruposApoyo(String Nombre)
        {
            try
            {
                return vSupervisionGruposApoyoDAL.ConsultarSupervisionGruposApoyo(Nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<SupervisionGruposApoyo> ConsultarSupervisionGruposApoyos(int? pIdDireccion, int? pIdRegional, String pNombre, int? pEstado)
        {
            try
            {
                return vSupervisionGruposApoyoDAL.ConsultarSupervisionGruposApoyos(pIdDireccion, pIdRegional, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

 
    }
}
