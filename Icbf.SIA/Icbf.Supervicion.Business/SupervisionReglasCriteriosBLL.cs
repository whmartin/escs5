﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionReglasCriteriosBLL
    {
        private SupervisionReglasCriteriosDAL vSupervisionReglasCriteriosDAL;

        public SupervisionReglasCriteriosBLL()
        {
            vSupervisionReglasCriteriosDAL = new SupervisionReglasCriteriosDAL();
        }

        public List<SupervisionReglasCriterios> ConsultarSupervisionReglasCriterios(int? pIdCriterioRespuesta, int? pIdRespuesta, int? pTipoconf, int? pEstado, int? pTipoConsulta)
        {
            try
            {
                return vSupervisionReglasCriteriosDAL.ConsultarSupervisionCriterios(pIdCriterioRespuesta, pIdRespuesta, pTipoconf, pEstado, pTipoConsulta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionReglasCriterios InsertarSupervisionReglasCriterios(SupervisionReglasCriterios pObjSupReglasCriterios, System.Data.DataTable dtbCriterios)
        {
            try
            {
                return vSupervisionReglasCriteriosDAL.InsertarSupervisionReglasCriterios(pObjSupReglasCriterios, dtbCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionPreguntas ConsultarDatosFiltrosSupervisionCriterios(int? pIdRespuesta)
        {
            try
            {
                return vSupervisionReglasCriteriosDAL.ConsultarDatosFiltrosSupervisionCriterios(pIdRespuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
