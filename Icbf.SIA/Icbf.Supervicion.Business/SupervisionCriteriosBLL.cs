using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionCriteriosBLL
    {
        private SupervisionCriteriosDAL vSupervisionCriteriosDAL;
        public SupervisionCriteriosBLL()
        {
            vSupervisionCriteriosDAL = new SupervisionCriteriosDAL();
        }
        public int InsertarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                return vSupervisionCriteriosDAL.InsertarSupervisionCriterios(pSupervisionCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                return vSupervisionCriteriosDAL.ModificarSupervisionCriterios(pSupervisionCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                return vSupervisionCriteriosDAL.EliminarSupervisionCriterios(pSupervisionCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionCriterios ConsultarSupervisionCriterios(int pIdCriterio)
        {
            try
            {
                return vSupervisionCriteriosDAL.ConsultarSupervisionCriterios(pIdCriterio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionCriterios(string Valor)
        {
            try
            {
                return vSupervisionCriteriosDAL.ConsultarSupervisionCriterios(Valor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCriterios> ConsultarSupervisionCriterioss(int? pIdPregunta, String pValor, int? pEstado)
        {
            try
            {
                return vSupervisionCriteriosDAL.ConsultarSupervisionCriterioss(pIdPregunta, pValor, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
