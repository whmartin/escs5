using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionSubcomponentesBLL
    {
        private SupervisionSubcomponentesDAL vSupervisionSubcomponentesDAL;
        public SupervisionSubcomponentesBLL()
        {
            vSupervisionSubcomponentesDAL = new SupervisionSubcomponentesDAL();
        }
        public int InsertarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.InsertarSupervisionSubcomponentes(pSupervisionSubcomponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.ModificarSupervisionSubcomponentes(pSupervisionSubcomponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.EliminarSupervisionSubcomponentes(pSupervisionSubcomponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionSubcomponentes ConsultarSupervisionSubcomponentes(int pIdSubcomponente)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.ConsultarSupervisionSubcomponentes(pIdSubcomponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionSubcomponentes(string Nombre)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.ConsultarSupervisionSubcomponentes(Nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionSubcomponentes> ConsultarSupervisionSubcomponentess(int? pIdDireccion, String pNombre, int? pEstado)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.ConsultarSupervisionSubcomponentess(pIdDireccion, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionComponentes> ConsultarSupervisionComponentes(int? pIdComponente, int? pCodComponente, String pNombreComponente, int? pEstado)
        {
            try
            {
                return vSupervisionSubcomponentesDAL.ConsultarSupervisionComponentes(pIdComponente, pCodComponente, pNombreComponente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
    }
}
