using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionGruposApoyoIntegrantesBLL
    {
        private SupervisionGruposApoyoIntegrantesDAL vSupervisionGruposApoyoIntegrantesDAL;
        public SupervisionGruposApoyoIntegrantesBLL()
        {
            vSupervisionGruposApoyoIntegrantesDAL = new SupervisionGruposApoyoIntegrantesDAL();
        }
        public int InsertarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.InsertarSupervisionGruposApoyoIntegrantes(pSupervisionGruposApoyoIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.ModificarSupervisionGruposApoyoIntegrantes(pSupervisionGruposApoyoIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.EliminarSupervisionGruposApoyoIntegrantes(pSupervisionGruposApoyoIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyoIntegrantes ConsultarSupervisionGruposApoyoIntegrantes(int pIdIntegranteGrupo, int pIdGrupo)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.ConsultarSupervisionGruposApoyoIntegrantes(pIdIntegranteGrupo, pIdGrupo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ConsultarSupervisionGruposApoyoIntegrantes(String pNombre)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.ConsultarSupervisionGruposApoyoIntegrantes(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyoIntegrantes> ConsultarSupervisionGruposApoyoIntegrantess(int? pIdIntegranteGrupo, int? pIdGrupo, int? pIdentificacion, String pNombres, String pApellidos, int? pEstado)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.ConsultarSupervisionGruposApoyoIntegrantess(pIdIntegranteGrupo, pIdGrupo, pIdentificacion, pNombres, pApellidos, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposLupaPersona> ConsultarPersonaLupa(String pIdTipoDocumento, Int64? pNumeroIdentificacion, String pNombre1, String pNombre2, String pApellido1, String pApellido2)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesDAL.ConsultarPersonaLupa(pIdTipoDocumento,pNumeroIdentificacion,pNombre1,pNombre2,pApellido1,pApellido2);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        
    }
}
