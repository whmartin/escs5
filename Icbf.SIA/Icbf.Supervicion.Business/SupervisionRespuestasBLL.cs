using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionRespuestasBLL
    {
        private SupervisionRespuestasDAL vSupervisionRespuestasDAL;
        public SupervisionRespuestasBLL()
        {
            vSupervisionRespuestasDAL = new SupervisionRespuestasDAL();
        }
        public int InsertarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                return vSupervisionRespuestasDAL.InsertarSupervisionRespuestas(pSupervisionRespuestas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                return vSupervisionRespuestasDAL.ModificarSupervisionRespuestas(pSupervisionRespuestas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                return vSupervisionRespuestasDAL.EliminarSupervisionRespuestas(pSupervisionRespuestas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionRespuestas ConsultarSupervisionRespuestas(int pIdRespuesta)
        {
            try
            {
                return vSupervisionRespuestasDAL.ConsultarSupervisionRespuestas(pIdRespuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionRespuestas(string Valor)
        {
            try
            {
                return vSupervisionRespuestasDAL.ConsultarSupervisionRespuestas(Valor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionRespuestas> ConsultarSupervisionRespuestass(int? pIdPregunta, String pValor, Boolean? pVulneraDerecho, int? pEstado)
        {
            try
            {
                return vSupervisionRespuestasDAL.ConsultarSupervisionRespuestass(pIdPregunta, pValor, pVulneraDerecho, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
