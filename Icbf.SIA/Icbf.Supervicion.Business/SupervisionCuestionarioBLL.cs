using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.DataAccess;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Business
{
    public class SupervisionCuestionarioBLL
    {
        private SupervisionCuestionarioDAL vSupervisionCuestionarioDAL;
        public SupervisionCuestionarioBLL()
        {
            vSupervisionCuestionarioDAL = new SupervisionCuestionarioDAL();
        }
        public int InsertarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDAL.InsertarSupervisionCuestionario(pSupervisionCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDAL.ModificarSupervisionCuestionario(pSupervisionCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDAL.EliminarSupervisionCuestionario(pSupervisionCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SupervisionCuestionario ConsultarSupervisionCuestionario(int pIdCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDAL.ConsultarSupervisionCuestionario(pIdCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCuestionario> ConsultarSupervisionCuestionarios(String pNombre, int? pIdDireccion, int? pIdVigenciaServicio, int? pIdModalidadServicio, int? pIdSubComponente, int? pIdComponente, int? pEstado)
        {
            try
            {
                return vSupervisionCuestionarioDAL.ConsultarSupervisionCuestionarios(pNombre, pIdDireccion, pIdVigenciaServicio, pIdModalidadServicio, pIdSubComponente,pIdComponente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCuestionario> ConsultarSupervisionCuestionarioPreguntas(int? IdPregunta)
        {
            try
            {
                return vSupervisionCuestionarioDAL.ConsultarSupervisionCuestionarioPreguntas(IdPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
