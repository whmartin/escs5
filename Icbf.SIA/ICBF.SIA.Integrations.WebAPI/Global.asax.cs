﻿//-----------------------------------------------------------------------
// <copyright file="Global.asx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase Global.asx.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>15/11/2016</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using ICBF.Adopciones.Integrations.WebAPI.App_Start;

namespace ICBF.SIA.Integrations.WebAPI
{
    /// <summary>
    /// Clase para el manejo de WebApiApplication.
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Método para Iniciar la aplicación.
        /// </summary>
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}