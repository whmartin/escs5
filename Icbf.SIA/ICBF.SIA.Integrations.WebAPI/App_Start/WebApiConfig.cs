﻿//-----------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase WebApiConfig.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>15/11/2016</date>
//-----------------------------------------------------------------------

using System.Web.Http;
using System.Linq;
using System.Collections.Generic;

namespace ICBF.Adopciones.Integrations.WebAPI.App_Start
{
    /// <summary>Provides the WebApiConfig.</summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Integrates config Unity when the application Register.
        /// </summary>
        /// <param name="config">Config required</param>
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}