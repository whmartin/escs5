//-----------------------------------------------------------------------
// <copyright file="UnityConfig.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase UnityConfig.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>15/11/2016</date>
//-----------------------------------------------------------------------

using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace ICBF.Adopciones.Integrations.WebAPI.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container

        /// <summary>
        /// M�todo Lazy.
        /// </summary>
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        /// <returns>Container requerido</returns>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //container.LoadConfiguration();
            //container.RegisterInstance<UnitOfWork>(new UnitOfWork());
        }

        /// <summary>
        /// M�todo est�tico T.
        /// </summary>
        /// <typeparam name="T">Parametro a filtrar</typeparam>
        /// <returns>Container requerido</returns>
        public static T Resolve<T>()
        {
            return container.Value.Resolve<T>();
        }

        /// <summary>
        /// M�todo est�ctico Resolve.
        /// </summary>
        /// <param name="type">Type a filtrar</param>
        /// <returns>Container requerido</returns>
        public static object Resolve(Type type)
        {
            return container.Value.Resolve(type);
        }
    }
}