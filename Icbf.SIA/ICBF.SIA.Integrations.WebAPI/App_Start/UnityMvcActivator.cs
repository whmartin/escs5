//-----------------------------------------------------------------------
// <copyright file="UnityWebActivator.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase UnityWebActivator.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>18/08/2016</date>
//-----------------------------------------------------------------------

using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Practices.Unity.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ICBF.Adopciones.Integrations.WebAPI.App_Start.UnityWebActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(ICBF.Adopciones.Integrations.WebAPI.App_Start.UnityWebActivator), "Shutdown")]

namespace ICBF.Adopciones.Integrations.WebAPI.App_Start
{
    /// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
    public static class UnityWebActivator
    {
        /// <summary>Integrates Unity when the application starts.</summary>
        public static void Start()
        {
            var container = UnityConfig.GetConfiguredContainer();

            //// FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            //// FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

            //// Configures container for ASP.NET MVC
            //// DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            //// Configures container for WebAPI
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            //// TODO: Uncomment if you want to use PerRequestLifetimeManager
            //// Microsoft.Web.Infrastructure.DynamicModuleHelper.DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
        }

        /// <summary>Disposes the Unity container when the application is shut down.</summary>
        public static void Shutdown()
        {
            var container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }
    }
}