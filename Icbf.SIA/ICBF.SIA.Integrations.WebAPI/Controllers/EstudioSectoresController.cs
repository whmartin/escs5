﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Icbf.SIA.Integrations.ServiceFacade;
using System.Web.Http.Cors;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using System.Globalization;
using System.Threading;

namespace ICBF.SIA.Integrations.WebAPI.Controllers
{
    public class EstudioSectoresController : ApiController
    {
        private ServiceEstudioSectores vServiceEstudioSectores;
        public EstudioSectoresController()
        {
            vServiceEstudioSectores = new ServiceEstudioSectores();
        }

        /// <summary>
        /// Método que para obtener el resultado del calculo fechas PACCO de estudio de sectores
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id del consecutivo de estudio</param>
        /// <param name="pFechaEstimadaInicioEjecucion">Fecha que proviene de ws PACCO</param>
        /// <param name="pIdModalidadSeleccionPACCO">Id Modalidad que proviene de ws PACCO</param>
        /// <param name="pModalidadSeleccionPACCO">Modalidad que proviene de ws PACCO</param>
        /// <param name="pValorContrato">Valor contrato que proviene de ws PACCO</param>
        /// <returns>Tiempos con fechas PACCO</returns>
        [HttpPost]

        public TiemposPACCO ConsultaFechasPACCO(dynamic request)
        {

            TiemposPACCO vTiempoEntreActividadesEstudioSyCPACCO = null;
            try
            {
                
                Thread.CurrentThread.CurrentCulture = new CultureInfo("es-CO");
                decimal vIdConsecutivoEstudio = decimal.Parse(Convert.ToString(request.pIdConsecutivoEstudio));
                DateTime vFechaEstimadaInicioEjecucion = Convert.ToDateTime(Convert.ToString(request.pFechaEstimadaInicioEjecucion));
                decimal vIdModalidadSeleccionPACCO = decimal.Parse(Convert.ToString(request.pIdModalidadSeleccionPACCO));
                string vModalidadSeleccionPACCO = (Convert.ToString(request.pModalidadSeleccionPACCO));
                decimal vValorContrato = decimal.Parse(Convert.ToString(request.pValorContrato));
                vTiempoEntreActividadesEstudioSyCPACCO = vServiceEstudioSectores.ConsultarFehasPACCO(vIdConsecutivoEstudio, vFechaEstimadaInicioEjecucion, vIdModalidadSeleccionPACCO, vModalidadSeleccionPACCO, vValorContrato);

                return vTiempoEntreActividadesEstudioSyCPACCO;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


        }
    }
}
