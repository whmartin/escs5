﻿using Icbf.Contrato.Entity;
using Icbf.SIA.Integrations.ServiceFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ICBF.SIA.Integrations.WebAPI.Controllers
{
    public class ContratosController : ApiController
    {
        private ServiceContratos _IntegrationService;

        public ContratosController()
        {
            _IntegrationService = new ServiceContratos();
        }

        /// <summary>
        /// Método para obtener un listado de Aplicacion por parametros.
        /// </summary>
        /// <param name="model">AplicacionDTO model</param>
        /// <returns>Lista Aplicacion</returns>
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult Pacco(string id)
        {
            List<InfoContratoPaccoDTO> resultList = null;
            try
            {
                int idPlan = 0;

                if (int.TryParse(id, out idPlan))
                {
                    resultList = _IntegrationService.ObtenerInfoContratosPaccoPorId(idPlan);
                }
                else
                    return BadRequest("Id Incorrecto");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(resultList);
        }
    }
}
