﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
using Icbf.Mostrencos.Entity;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Integrations.ServiceFacade;

namespace ICBF.SIA.Integrations.WebAPI.Controllers
{
    public class EnvioCorreoController : ApiController
    {
        private ServiceEnvioCorreo vServiceEnvioCorreo;

        public EnvioCorreoController()
        {
            vServiceEnvioCorreo = new ServiceEnvioCorreo();
        }



        /// <summary>
        /// Método para obtener un listado de Aplicacion por parametros.
        /// </summary>
        /// <param name="model">AplicacionDTO model</param>
        /// <returns>Lista Aplicacion</returns>
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult EnviarCorreos()
        {
            try
            {
                this.CargarListaCorreos();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        /// <summary>
        /// Enviar correo a los destinatarios.
        /// </summary>
        /// <param name="pDe"></param>
        /// <param name="pPara"></param>
        /// <param name="pAsunto"></param>
        private void CargarListaCorreos()
        {
            List<EnvioCorreo> vListEnvioCorreo = new List<EnvioCorreo>();
            vListEnvioCorreo = this.vServiceEnvioCorreo.ConsultarListaCorreos();
            if (vListEnvioCorreo.Count > 0)
            {
                //DateTime FechaInformeContrato;
                EnvioCorreo CorreoTempo;
                DateTime FechaCreaMonitoreo;
                foreach (var correo in vListEnvioCorreo)
                {
                    CorreoTempo = new EnvioCorreo();
                    //FechaInformeContrato = correo.FechaInforme;
                    CorreoTempo = this.vServiceEnvioCorreo.ConsultarListaCorreosPorId(correo.IdDenunciaBien);
                    if (CorreoTempo.FechaCrea == new DateTime())
                    {

                        FechaCreaMonitoreo = this.CalculoDiaHabil(correo.FechaCrea.AddDays(90));

                        if (FechaCreaMonitoreo <= DateTime.Now)
                        {
                            /// this.ConstruccionEnvioCorreos(ConfigurationManager.AppSettings["MailFrom"], "Alerta Informe Resumen Ejecutivo del Denunciante Pendiente por Registrar", CorreoTempo);
                            this.ConstruccionEnvioCorreos(ConfigurationManager.AppSettings["MailFrom"], "Alerta Informe Resumen Ejecutivo del Denunciante Pendiente por Registrar", correo);
                        }

                    }
                    else if (CorreoTempo.FechaInforme != new DateTime())
                    {
                        //Cuando no hay datos de informe de denunciante para este Id.
                        if (CorreoTempo.FechaInforme <= DateTime.Now)
                        {
                            //Envia correo
                            //this.ConstruccionEnvioCorreos(ConfigurationManager.AppSettings["MailFrom"], "Alerta Informe Resumen Ejecutivo del Denunciante Pendiente por Registrar", correo);
                            this.ConstruccionEnvioCorreos(ConfigurationManager.AppSettings["MailFrom"], "Alerta Informe Resumen Ejecutivo del Denunciante Pendiente por Registrar", CorreoTempo);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calcula dias habiles
        /// </summary>
        /// <param name="fechaInicial"></param>
        /// <returns></returns>
        private DateTime CalculoDiaHabil(DateTime fechaInicial)
        {
            //MostrencosService vMostrencosService = new MostrencosService();
            DateTime tempDate = fechaInicial;
            List<Feriados> feriados = new List<Feriados>();
            feriados = this.vServiceEnvioCorreo.ObtenerFeriados();
            if (feriados.Count > 0)
            {
                while (tempDate.DayOfWeek == DayOfWeek.Saturday || tempDate.DayOfWeek == DayOfWeek.Sunday
                    || feriados.Exists(t => t.fedID == tempDate))
                {
                    tempDate = tempDate.AddDays(1);
                }
            }
            return tempDate;
        }

        /// <summary>
        /// Método que construye y envía los correos correspondientes.
        /// </summary>
        public Task ConstruccionEnvioCorreos(string pDe, string pAsunto, EnvioCorreo correo)
        {
            correo.CorreoCoordinador = this.vServiceEnvioCorreo.GetCorreoCoordinadorJuridico(correo.IdRegionalUbicacion, true);
            MailMessage MyMailMessage = new MailMessage();
            MyMailMessage.From = new MailAddress(pDe);
            if (correo.CorreoDenunciante != null)
            {
                MyMailMessage.To.Add(new MailAddress(correo.CorreoDenunciante));
            }
            if (correo.CorreoAbogado != null)
            {
                MyMailMessage.To.Add(new MailAddress(correo.CorreoAbogado));
            }

            if ((correo.CorreoCoordinador != null) && (correo.CorreoCoordinador != string.Empty))
            {
                MyMailMessage.CC.Add(new MailAddress(correo.CorreoCoordinador));
            }

            MyMailMessage.Subject = pAsunto;
            MyMailMessage.IsBodyHtml = true;
            StringBuilder strbulMnj = new StringBuilder();
            strbulMnj.AppendLine("<html><body>");
            strbulMnj.AppendLine("  <table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'>");
            strbulMnj.AppendLine("      <tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr>");
            strbulMnj.AppendLine("      <tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr>");
            strbulMnj.AppendLine("      <tr><td width='10%' bgcolor='#81BA3D'>&nbsp;</td><td>");
            strbulMnj.AppendLine("          <table width='100%'border='0'align='center'>");
            strbulMnj.AppendLine("              <tr><td width='5%'>&nbsp;</td><td align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td><strong>Señor(a): </strong>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>Me permito notificar que a la fecha no se ha realizado el registro del informe resumen ejecutivo del denunciante con número de radicado " + correo.RadicadoDenuncia + ".");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>Cordialmente,</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>Vocaciones Heraditarias Bienes Vacantes Mostrencos " + "</br>" + correo.NombreRegional + "</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            strbulMnj.AppendLine("          </table></td><td width='10%' bgcolor='#81BA3D'>&nbsp;</td></tr>");
            strbulMnj.AppendLine("      <tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr>");
            strbulMnj.AppendLine("      <tr><td colspan='3' align='center' bgcolor='#81BA3D'>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo.</td></tr>");
            strbulMnj.AppendLine("      <tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr>");
            strbulMnj.AppendLine("  </table>");
            strbulMnj.AppendLine("</body></html>");

            MyMailMessage.Body = strbulMnj.ToString();

            SmtpClient MySmtpClient = new SmtpClient();
            MySmtpClient.Host = ConfigurationManager.AppSettings["MailHost"].ToString();
            MySmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MailPort"]);
            MySmtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MailEnableSsl"]);
            MySmtpClient.UseDefaultCredentials = false;
            MySmtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"]);
            object userState = MyMailMessage;
            return Task.Run(() =>
            {
                try
                {
                    MySmtpClient.Send(MyMailMessage);
                }
                catch (Exception e)
                {

                }
            });
        }

        /// <summary>
        /// obtiene el correo del cordinador juridico
        /// </summary>
        /// <param name="pIdRegionalDestino">id de la regional</param>
        /// <param name="pkeys">keys de busqueda</param>
        /// <returns>correo del coordinador juridico</returns>
        public string GetCorreoCoordinadorJuridico(int pIdRegional, bool pEsOrigen)
        {
            string vCorreoCoordinadorJuridicoRegional = string.Empty;
            try
            {
                string vkeys = string.Empty;
                List<Rol> vListaRoles = this.vServiceEnvioCorreo.ConsultarRolesPorProgramaFuncion("TRASLADAR DENUNCIA", "COORDINADOR JURIDICO");
                List<string> vUsuarioRol = new List<string>();
                foreach (Rol item in vListaRoles)
                {
                    vUsuarioRol.AddRange(Roles.GetUsersInRole(item.NombreRol).ToList());
                }

                foreach (string item in vUsuarioRol)
                {
                    vkeys = string.Concat(vkeys, "'", (string.IsNullOrEmpty(Membership.GetUser(item).ProviderUserKey.ToString()) ? string.Empty : Membership.GetUser(item).ProviderUserKey.ToString()), "', ");
                }

                vkeys = vkeys.Trim().TrimEnd(',');
                vCorreoCoordinadorJuridicoRegional = this.vServiceEnvioCorreo.ConsultarCorreoCoordinadorJuridico(pIdRegional, vkeys);

                if (vCorreoCoordinadorJuridicoRegional.Split(';').Count() > 1)
                {
                    string vMensaje = pEsOrigen ? "Existe  más de un coordinador para la regional origen" : "Existe  más de un coordinador para la regional destino";
                    vCorreoCoordinadorJuridicoRegional = null;
                    throw new Exception(vMensaje);
                }

                return vCorreoCoordinadorJuridicoRegional;
            }
            catch (Exception)
            {
                return vCorreoCoordinadorJuridicoRegional;
            }
        }
    }
}