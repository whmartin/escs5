﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad TipoUsuario
    /// </summary>
    public class TipoUsuarioBLL
    {
        private TipoUsuarioDAL vTipoUsuarioDAL;
        public TipoUsuarioBLL()
        {
            vTipoUsuarioDAL = new TipoUsuarioDAL();
        }
        /// <summary>
        /// Gonet 
        /// Permite insertar un tipo de usuario
        ///     -Genera auditoria
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pTipoUsuario">Instancia con la información del tipo a insertar</param>
        /// <returns>Identificador de base de datos de la información insertada</returns>
        public int InsertarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.InsertarTipoUsuario(pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite modificar un tipo de usuario
        ///     -Genera auditoria
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pTipoUsuario">Instancia con la información a modificar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int ModificarTipoUsuario(TipoUsuario pTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.ModificarTipoUsuario(pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite eliminar un tipo de usuario
        ///     -Genera auditoria
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pIdTipoUsuario">Instancia con la información a eliminar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int EliminarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.EliminarTipoUsuario(pIdTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Consulta y recupera la información de tipo de usuario a partir del identificador de base de datos
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pIdTipoUsuario">Entero con el identificador del tipo</param>
        /// <returns>Instancia que contiene la información recuperada de la base de datos</returns>
        public TipoUsuario ConsultarTipoUsuario(int pIdTipoUsuario)
        {
            try
            {
                return vTipoUsuarioDAL.ConsultarTipoUsuario(pIdTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consulta y recupera la información de los tipos de usuario a partir de los filtros dados
        /// 18-Feb-2014
        /// </summary>
        /// <param name="pCodigoTipoUsuario">Cadena de texto con el codigo de tipo de usuario</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con las instancias que contiene la información, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<TipoUsuario> ConsultarTipoUsuarios(String pCodigoTipoUsuario, String pNombreTipoUsuario, String pEstado)
        {
            try
            {
                return vTipoUsuarioDAL.ConsultarTipoUsuarios(pCodigoTipoUsuario, pNombreTipoUsuario, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
