﻿//-----------------------------------------------------------------------
// <copyright file="ITramiteBLL.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ITramiteBLL.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Icbf.SIA.Entity;

namespace Icbf.SIA.Business.IBusiness
{
    /// <summary>
    /// Clase para manejar la interfaz de la capade negocio de los Tramites.
    /// </summary>
    public interface ITramiteBLL
    {
        /// <summary>
        /// Método para obtener objeto Tramite por id.
        /// </summary>
        /// <param name="pId">Id de Tramite a consultar</param>
        /// <returns>Tramite consultado</returns>
        List<Tramite> GetTramite(int pId);
    }
}
