﻿using Icbf.SIA.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Business.IBusiness
{
    public interface IConsultaNotariasBLL
    {
        /// <summary>
        /// Método para obtener objeto Tramite por id.
        /// </summary>
        /// <param name="pId">Id de Tramite a consultar</param>
        /// <returns>Tramite consultado</returns>
        List<Notaria> GetNotarias(string pId);
    }
}
