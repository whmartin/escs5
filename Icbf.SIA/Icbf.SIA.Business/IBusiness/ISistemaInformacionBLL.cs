﻿//-----------------------------------------------------------------------
// <copyright file="ISistemaInformacionBLL.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ISistemaInformacionBLL.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Icbf.SIA.Entity;

namespace Icbf.SIA.Business.IBusiness
{
    /// <summary>
    /// Clase que maneja la interfaz de la capa de negocio de los sistemas de información.
    /// </summary>
    public interface ISistemaInformacionBLL
    {
        /// <summary>
        /// Método para obtener objeto Tramite por id.
        /// </summary>
        /// <param name="pId">Id de Tramite a consultar</param>
        /// <returns>Tramite consultado</returns>
        List<SistemaInformacion> GetSistemaInformacion(int pId);
    }
}
