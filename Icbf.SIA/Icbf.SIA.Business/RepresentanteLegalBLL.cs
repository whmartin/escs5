using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class RepresentanteLegalBLL
    {
        /// <summary>
        /// Clase de Datos para la entidad RepresentanteLegal
        /// </summary>
        private RepresentanteLegalDAL vRepresentanteLegalDAL;
        public RepresentanteLegalBLL()
        {
            vRepresentanteLegalDAL = new RepresentanteLegalDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int InsertarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalDAL.InsertarRepresentanteLegal(pRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int ModificarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalDAL.ModificarRepresentanteLegal(pRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pRepresentanteLegal"></param>
        /// <returns></returns>
        public int EliminarRepresentanteLegal(RepresentanteLegal pRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalDAL.EliminarRepresentanteLegal(pRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pIdRepresentanteLegal"></param>
        /// <returns></returns>
        public RepresentanteLegal ConsultarRepresentanteLegal(int pIdRepresentanteLegal)
        {
            try
            {
                return vRepresentanteLegalDAL.ConsultarRepresentanteLegal(pIdRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RepresentanteLegal
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdMunicipio"></param>
        /// <param name="pIdentificacion"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pSegundoNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pSegundoApellido"></param>
        /// <returns></returns>
        public List<RepresentanteLegal> ConsultarRepresentanteLegals(int? pIdTipoDocumento, int? pIdDepartamento, int? pIdMunicipio, String pIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                return vRepresentanteLegalDAL.ConsultarRepresentanteLegals(pIdTipoDocumento, pIdDepartamento, pIdMunicipio, pIdentificacion, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
