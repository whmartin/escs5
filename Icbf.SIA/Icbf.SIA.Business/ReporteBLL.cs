using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class ReporteBLL
    {
        private ReporteDAL vReporteDAL;
        public ReporteBLL()
        {
            vReporteDAL = new ReporteDAL();
        }
        public int InsertarReporte(Reporte pReporte)
        {
            try
            {
                return vReporteDAL.InsertarReporte(pReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarReporte(Reporte pReporte)
        {
            try
            {
                return vReporteDAL.ModificarReporte(pReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarReporte(Reporte pReporte)
        {
            try
            {
                return vReporteDAL.EliminarReporte(pReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public Reporte ConsultarReporte(int pIdReporte)
        {
            try
            {
                return vReporteDAL.ConsultarReporte(pIdReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Reporte> ConsultarReportes(String pNombreReporte, String pServidor)
        {
            try
            {
                return vReporteDAL.ConsultarReportes(pNombreReporte, pServidor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
