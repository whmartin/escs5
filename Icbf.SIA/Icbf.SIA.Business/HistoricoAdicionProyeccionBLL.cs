using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  HistoricoAdicionProyeccion
    /// </summary>
    public class HistoricoAdicionProyeccionBLL
    {
        private HistoricoAdicionProyeccionDAL vHistoricoAdicionProyeccionDAL;
        public HistoricoAdicionProyeccionBLL()
        {
            vHistoricoAdicionProyeccionDAL = new HistoricoAdicionProyeccionDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int InsertarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionDAL.InsertarHistoricoAdicionProyeccion(pHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int ModificarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionDAL.ModificarHistoricoAdicionProyeccion(pHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pHistoricoAdicionProyeccion"></param>
        public int EliminarHistoricoAdicionProyeccion(HistoricoAdicionProyeccion pHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionDAL.EliminarHistoricoAdicionProyeccion(pHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pIdHistoricoAdicionProyeccion"></param>
        public HistoricoAdicionProyeccion ConsultarHistoricoAdicionProyeccion(int pIdHistoricoAdicionProyeccion)
        {
            try
            {
                return vHistoricoAdicionProyeccionDAL.ConsultarHistoricoAdicionProyeccion(pIdHistoricoAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoAdicionProyeccion
        /// </summary>
        /// <param name="pIdAdicionProyeccion"></param>
        /// <param name="pRazonRechazo"></param>
        /// <param name="pRechazado"></param>
        public List<HistoricoAdicionProyeccion> ConsultarHistoricoAdicionProyeccions(int? pIdAdicionProyeccion, String pRazonRechazo, Boolean? pRechazado)
        {
            try
            {
                return vHistoricoAdicionProyeccionDAL.ConsultarHistoricoAdicionProyeccions(pIdAdicionProyeccion, pRazonRechazo, pRechazado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
