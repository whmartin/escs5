using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  RubrosCupos
    /// </summary>
    public class RubrosCuposBLL
    {
        private RubrosCuposDAL vRubrosCuposDAL;
        public RubrosCuposBLL()
        {
            vRubrosCuposDAL = new RubrosCuposDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int InsertarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                return vRubrosCuposDAL.InsertarRubrosCupos(pRubrosCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int ModificarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                return vRubrosCuposDAL.ModificarRubrosCupos(pRubrosCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RubrosCupos
        /// </summary>
        /// <param name="pRubrosCupos"></param>
        public int EliminarRubrosCupos(RubrosCupos pRubrosCupos)
        {
            try
            {
                return vRubrosCuposDAL.EliminarRubrosCupos(pRubrosCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad RubrosCupos
        /// </summary>
        /// <param name="pIdRubroCupos"></param>
        public RubrosCupos ConsultarRubrosCupos(int pIdRubroCupos)
        {
            try
            {
                return vRubrosCuposDAL.ConsultarRubrosCupos(pIdRubroCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RubrosCupos
        /// </summary>
        /// <param name="pCodigoRubro"></param>
        /// <param name="pDescripcionRubro"></param>
        /// <param name="pEstado"></param>
        public List<RubrosCupos> ConsultarRubrosCuposs(String pCodigoRubro, String pDescripcionRubro, Boolean? pEstado)
        {
            try
            {
                return vRubrosCuposDAL.ConsultarRubrosCuposs(pCodigoRubro, pDescripcionRubro, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
