using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  HistoricoAdicionCandidato
    /// </summary>
    public class HistoricoAdicionCandidatoBLL
    {
        private HistoricoAdicionCandidatoDAL vHistoricoAdicionCandidatoDAL;
        public HistoricoAdicionCandidatoBLL()
        {
            vHistoricoAdicionCandidatoDAL = new HistoricoAdicionCandidatoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int InsertarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoDAL.InsertarHistoricoAdicionCandidato(pHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int ModificarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoDAL.ModificarHistoricoAdicionCandidato(pHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pHistoricoAdicionCandidato"></param>
        public int EliminarHistoricoAdicionCandidato(HistoricoAdicionCandidato pHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoDAL.EliminarHistoricoAdicionCandidato(pHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pIdHistoricoAdicionCandidato"></param>
        public HistoricoAdicionCandidato ConsultarHistoricoAdicionCandidato(int pIdHistoricoAdicionCandidato)
        {
            try
            {
                return vHistoricoAdicionCandidatoDAL.ConsultarHistoricoAdicionCandidato(pIdHistoricoAdicionCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoAdicionCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pValorAdicion"></param>
        /// <param name="pValorAdicionAnterior"></param>
        public List<HistoricoAdicionCandidato> ConsultarHistoricoAdicionCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, Decimal? pValorAdicion, Decimal? pValorAdicionAnterior)
        {
            try
            {
                return vHistoricoAdicionCandidatoDAL.ConsultarHistoricoAdicionCandidatos(pIdCupoArea, pIdProyeccionPresuspuesto, pValorAdicion, pValorAdicionAnterior);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
