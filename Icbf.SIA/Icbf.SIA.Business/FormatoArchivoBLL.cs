using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class FormatoArchivoBLL
    {
        private FormatoArchivoDAL vFormatoArchivoDAL;
        public FormatoArchivoBLL()
        {
            vFormatoArchivoDAL = new FormatoArchivoDAL();
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad FormatoArchivo
        /// </summary>
        /// <param name="pTablaTemporal"></param>
        /// <param name="pExt"></param>
        /// <returns></returns>
        public List<FormatoArchivo> ConsultarFormatoAchivoByTemporarExt(string pTablaTemporal, string pExt)
        {
            try
            {
                return vFormatoArchivoDAL.ConsultarFormatoArchivosTemporalExt(pTablaTemporal, pExt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
