﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Icbf.SIA.Business;
using Icbf.SIA.DataAccess.Concursales;
using Icbf.SIA.Entity.Concursales;
using Icbf.Utilities.Exceptions;


namespace Icbf.SIA.Business.Concursales
{
    public class TipoProcesoNaturalezaBLL
    {
        private TipoProcesoNaturalezaDAL vNaturalezaTipoProcesoDAL;
        public TipoProcesoNaturalezaBLL()
        {
            vNaturalezaTipoProcesoDAL = new TipoProcesoNaturalezaDAL();
        }


       public List<TipoProcesoNaturaleza> ConsultarTiposdeProcesosporNaturaleza(string vTipoProceso, string vNaturaleza, Boolean? vEstado)
        {
            try
            {
                return vNaturalezaTipoProcesoDAL.ConsultarTiposdeProcesosporNaturaleza(vTipoProceso, vNaturaleza,vEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

       public TipoProcesoNaturaleza ConsultarTipoProcesoNaturaleza(int vIdValor)
        {
            try
            {
                return vNaturalezaTipoProcesoDAL.ConsultarTipoProcesoNaturaleza(vIdValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTipoProcesoNaturaleza(TipoProcesoNaturaleza vValor)
        {
            try
            {
                return vNaturalezaTipoProcesoDAL.ModificarTipoProcesoNaturaleza(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarTipoProcesoNaturaleza(TipoProcesoNaturaleza vValor)
        {
            try
            {
                return vNaturalezaTipoProcesoDAL.InsertarTipoProcesoNaturaleza(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTipoProcesoNaturaleza(TipoProcesoNaturaleza vValor)
        {
            try
            {
                return vNaturalezaTipoProcesoDAL.EliminarTipoProcesoNaturaleza(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}

