﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Icbf.SIA.Business;
using Icbf.SIA.DataAccess.Concursales;
using Icbf.SIA.Entity.Concursales;
using Icbf.Utilities.Exceptions;


namespace Icbf.SIA.Business.Concursales
{
    public class ParametricasBLL
    {
        private ParametricasDAL vConcursalDAL;
        public ParametricasBLL()
        {
            vConcursalDAL = new ParametricasDAL();
        }


        public List<Clasificador> ConsultarParameticasConcursales(Boolean? vEstado, string vNombre)
        {
            try
            {
                return vConcursalDAL.ConsultarParameticasConcursales(vEstado, vNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParameticasConcursalesDetalle(Boolean? vEstado, string vNombre, int vIdClasificador)
        {
            try
            {
                return vConcursalDAL.ConsultarParameticasConcursalesDetalle(vEstado, vNombre, vIdClasificador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParameticasNaturalezaTipoProceso(Boolean? vEstado, string vNombre)
        {
            try
            {
                return vConcursalDAL.ConsultarParameticasNaturalezaTipoProceso(vEstado, vNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParametricaPorPadre(int vIdPadre)
        {
            try
            {
                return vConcursalDAL.ConsultarParametricaPorPadre(vIdPadre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Valor> ConsultarParametricaPorCodigo(string vCodigo)
        {
            try
            {
                return vConcursalDAL.ConsultarParametricaPorCodigo(vCodigo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Valor ConsultarParametricaPorCodigo(string vCodigo, string vNombre)
        {
            try
            {
                return vConcursalDAL.ConsultarParametricaPorCodigo(vCodigo,vNombre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Clasificador ConsultarClasificador(int vIdClasificador)
        {
            try
            {
                return vConcursalDAL.ConsultarClasificador(vIdClasificador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Valor ConsultarValorParametrica(int vIdValor)
        {
            try
            {
                return vConcursalDAL.ConsultarValorParametrica(vIdValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarValorParametrica(Valor vValor)
        {
            try
            {
                return vConcursalDAL.ModificarValorParametrica(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarValorParametrica(Valor vValor)
        {
            try
            {
                return vConcursalDAL.InsertarValorParametrica(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarValorParametrica(Valor vValor)
        {
            try
            {
                return vConcursalDAL.EliminarValorParametrica(vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
