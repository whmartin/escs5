﻿using Icbf.SIA.DataAccess.Concursales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity.Concursales;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business.Concursales
{
    
    public class ProcesoConcursalBLL
    {
        private ProcesoConcursalDAL _procesoConcursalDAL;

        public ProcesoConcursalBLL()
        {
            _procesoConcursalDAL = new ProcesoConcursalDAL();
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursales(int idRegional, string numeroIdentificacion, int? idEstado, string usuarioAsignado, string consecutivo)
        {
            try
            {
                return _procesoConcursalDAL.ObtenerProcesosConcursales(idRegional, numeroIdentificacion, idEstado, usuarioAsignado, consecutivo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesMultipleEstado(int idRegional, string numeroIdentificacion, string usuarioAsignado, string[] estados, string consecutivo)
        {
            try
            {
                StringBuilder misEstados = new StringBuilder();

                foreach(var item in estados)
                    misEstados.AppendFormat("{0},", item);

                return _procesoConcursalDAL.ObtenerProcesosConcursalesMultipleEstado(idRegional, numeroIdentificacion, usuarioAsignado, misEstados.ToString(), consecutivo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesOmitirEstados(int idRegional, string numeroIdentificacion, string usuarioAsignado, string[] estadosOmitir, string consecutivo, int? vIdEstado = null)
        {
            try
            {
                StringBuilder misEstados = new StringBuilder();

                foreach(var item in estadosOmitir)
                    misEstados.AppendFormat("{0},", item);

                return _procesoConcursalDAL.ObtenerProcesosConcursalesOmitirEstados(idRegional, numeroIdentificacion, usuarioAsignado, misEstados.ToString(), consecutivo, vIdEstado);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ProcesoConcursal ObtenerProcesosConcursalPorId(int idProceso)
        {
            try
            {
                return _procesoConcursalDAL.ObtenerProcesosConcursalPorId(idProceso);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalDocumento> ObtenerDocumentosPorIdProceso(int idProceso)
        {
            try
            {
                return _procesoConcursalDAL.ObtenerDocumentosPorIdProceso(idProceso);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AsignarUsuarioProceso(int id, string usuario, string usuarioModifica)
        {
            try
            {
                return _procesoConcursalDAL.AsignarUsuarioProceso(id,usuario,usuarioModifica);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarNovedad(ProcesoConcursalNovedad item)
        {
            try
            {
                return _procesoConcursalDAL.IngresarNovedad(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int IngresarGestionNovedad(ProcesoConcursalGestionNovedad item)
        {
            try
            {
                return _procesoConcursalDAL.IngresarGestionNovedad(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursal> ObtenerProcesosConcursalesGestionNovedades(int idRegional, string numeroIdentificacion, int? idEstado, string usuarioAsignado, string consecutivo)
        {
            try
            {
                return _procesoConcursalDAL.ObtenerProcesosConcursalesGestionNovedades(idRegional,numeroIdentificacion,idEstado,usuarioAsignado,consecutivo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalGestion> ObtenerGestionProcesoPorIdProceso(int idProceso)
        {
            try
            {
                return _procesoConcursalDAL.ObtenerGestionProcesoPorIdProceso(idProceso);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
