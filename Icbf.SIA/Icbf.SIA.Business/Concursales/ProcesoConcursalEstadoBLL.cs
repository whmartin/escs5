﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.DataAccess.Concursales;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business.Concursales
{
    public class ProcesoConcursalEstadoBLL
    {
        private ProcesoConcursalEstadoDAL VConcursalEstadoDAL;
        public ProcesoConcursalEstadoBLL()
        {
            VConcursalEstadoDAL = new ProcesoConcursalEstadoDAL();
        }

        public ProcesoConcursalEstado ConsultarEstadoPorCodigo(string codigo)
        {
            try
            {
                return VConcursalEstadoDAL.ConsultarParametricaPorCodigo(codigo);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesoConcursalEstado> ConsultarEstadosOmitir(string[] codigosEstadosOmitidos)
        {
            try
            {
                StringBuilder codigos = new StringBuilder();

                foreach(var item in codigosEstadosOmitidos)
                    codigos.AppendFormat("{0},",item);

                return VConcursalEstadoDAL.ConsultarEstadosOmitir(codigos.ToString());
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
