﻿using System;
using System.Collections.Generic;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.DataAccess.Concursales;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business.Concursales
{
    public class SolicitudesBLL
    {
        private SolicitudesDAL vSolicitudesDAL;

        public SolicitudesBLL()
        {
            vSolicitudesDAL = new SolicitudesDAL();
        }

        public List<CarteraActiva> ConsultarCarteraActiva(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            try
            {
                return vSolicitudesDAL.ConsultarCarteraActiva(vIdentificacion,vIdTipoDocumento,vIDRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CarteraAgrupada> ConsultarCarteraActivaAgrupada(string vIdentificacion, int? vIdTipoDocumento, string vIDRegional)
        {
            try
            {
                return vSolicitudesDAL.ConsultarCarteraActivaAgrupada(vIdentificacion, vIdTipoDocumento, vIDRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CarteraActiva ConsultarCarteraActivaPorId(int vId)
        {
            try
            {
                return vSolicitudesDAL.ConsultarCarteraActivaPorId(vId);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ExisteCarteraActivaAsociada(int vId, String resolucion )
        {
            try
            {
                return vSolicitudesDAL.ExisteCarteraActivaAsociada(vId,resolucion);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string IniciarProcesoConcursal(CarteraActiva item)
        {
            try
            {
                return vSolicitudesDAL.IniciarProcesoConcursal(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public KeyValuePair<int, bool> ValidarCarteraTercero(string identificacion)
        {
            try
            {
                return vSolicitudesDAL.ValidarCarteraTercero(identificacion);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosProcesoConcursal(ProcesoConcursalDocumento item)
        {
            try
            {
                return vSolicitudesDAL.InsertarDocumentosProcesoConcursal(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarDocumentoProcesoConcursal(int idDocumento)
        {
            try
            {
                return vSolicitudesDAL.EliminarDocumentoProcesoConcursal(idDocumento);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
