using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  AdicionProrrogaCandidato
    /// </summary>
    public class AdicionProrrogaCandidatoBLL
    {
        private AdicionProrrogaCandidatoDAL vAdicionProrrogaCandidatoDAL;
        public AdicionProrrogaCandidatoBLL()
        {
            vAdicionProrrogaCandidatoDAL = new AdicionProrrogaCandidatoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int InsertarAdicionProrrogaCandidato(AdicionProrrogaCandidato pAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoDAL.InsertarAdicionProrrogaCandidato(pAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int ModificarAdicionProrrogaCandidato(AdicionProrrogaCandidato pAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoDAL.ModificarAdicionProrrogaCandidato(pAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pAdicionProrrogaCandidato"></param>
        public int EliminarAdicionProrrogaCandidato(int pAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoDAL.EliminarAdicionProrrogaCandidato(pAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pIdAdicionProrrogaCandidato"></param>
        public AdicionProrrogaCandidato ConsultarAdicionProrrogaCandidato(int pIdAdicionProrrogaCandidato)
        {
            try
            {
                return vAdicionProrrogaCandidatoDAL.ConsultarAdicionProrrogaCandidato(pIdAdicionProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AdicionProrrogaCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pFechaIncioProyeccion"></param>
        /// <param name="pFechaFinProyeccion"></param>
        /// <param name="pValorAdicionado"></param>
        public List<AdicionProrrogaCandidato> ConsultarAdicionProrrogaCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, DateTime? pFechaIncioProyeccion, DateTime? pFechaFinProyeccion, Decimal? pValorAdicionado)
        {
            try
            {
                return vAdicionProrrogaCandidatoDAL.ConsultarAdicionProrrogaCandidatos(pIdCupoArea, pIdProyeccionPresuspuesto, pFechaIncioProyeccion, pFechaFinProyeccion, pValorAdicionado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
