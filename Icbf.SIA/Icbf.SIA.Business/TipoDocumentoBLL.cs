using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.SIA.Business
{
    public class TipoDocumentoBLL
    {
        private TipoDocumentoDAL vTipoDocumentoDAL;
        public TipoDocumentoBLL()
        {
            vTipoDocumentoDAL = new TipoDocumentoDAL();
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoDocumento
        /// </summary>
        /// <param name="pIdTipoDocumento"></param>
        /// <returns></returns>
        public TipoDocumento ConsultarTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.ConsultarTipoDocumento(pIdTipoDocumento);  
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por codigo y nombre para la entidad TipoDocumento
        /// </summary>
        /// <returns></returns>
        public List<TipoDocumento> ConsultarTiposDocumento(String pCodigoDocumento, String pNombreDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.ConsultarTiposDocumento(pCodigoDocumento, pNombreDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoDocumento> ConsultarTiposDocumentoNMF(String pCodigoDocumento, String pNombreDocumento)
        {
            try
            {
                return vTipoDocumentoDAL.ConsultarTiposDocumento(pCodigoDocumento, pNombreDocumento);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoDocumento
        /// </summary>
        /// <returns></returns>
        public List<TipoDocumento> ConsultarTodosTipoDocumento()
        {
            try
            {
                return vTipoDocumentoDAL.ConsultarTodosTipoDocumento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
