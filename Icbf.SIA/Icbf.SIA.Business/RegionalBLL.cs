using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class RegionalBLL
    {
        private RegionalDAL vRegionalDAL;
        public RegionalBLL()
        {
            vRegionalDAL = new RegionalDAL();
        }
        
        public List<Regional> ConsultarRegionals(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalDAL.ConsultarRegionals(pCodigoRegional, pNombreRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Regional> ConsultarRegionalsNMF(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalDAL.ConsultarRegionalsNMF(pCodigoRegional, pNombreRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Regional> ConsultarRegionalPCIs(String pCodigoRegional, String pNombreRegional)
        {
            try
            {
                return vRegionalDAL.ConsultarRegionalPCIs(pCodigoRegional, pNombreRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Emilio Calapi�a
        /// Obtiene la informaci�n de la regional cuyo identificador �nico de tabla coincida
        /// con la entrada dada
        /// </summary>
        /// <param name="pIdRegional">Entero con el identificador de la regional</param>
        /// <returns>Entidad con la informaci�n de la regional</returns>
        public Regional ConsultarRegional(int? pIdRegional)
        {
            try
            {
                return vRegionalDAL.ConsultarRegional(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Regional> ConsultarRegionalsUsuario(int pIdUsuario)
        {
            try
            {
                return vRegionalDAL.ConsultarRegionalsUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
