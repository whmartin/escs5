using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class VigenciaBLL
    {
        private VigenciaDAL vVigenciaDAL;
        public VigenciaBLL()
        {
            vVigenciaDAL = new VigenciaDAL();
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Vigencia
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <returns></returns>
        public Vigencia ConsultarVigencia(int pIdVigencia)
        {
            try
            {
                return vVigenciaDAL.ConsultarVigencia(pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Vigencia
        /// </summary>
        /// <param name="pActivo"></param>
        /// <returns></returns>
        public List<Vigencia> ConsultarVigenciasSinAnnoActual(String pActivo)
        {
            try
            {
                return vVigenciaDAL.ConsultarVigenciasSinAnnoActual(pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Vigencia
        /// </summary>
        /// <param name="pActivo">Valor booleano que representa el estado del registro activo o inactivo</param>
        /// <returns>Lista de la instancia Vigencia</returns>
        public List<Vigencia> ConsultarVigencias(bool pActivo)
        {
            try
            {
                return vVigenciaDAL.ConsultarVigencias(pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarVigenciasExistenteAnio(int pIdContrato, int pAnio)
        {
            try
            {
                return vVigenciaDAL.ConsultarVigenciasExistenteAnio(pIdContrato,pAnio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
