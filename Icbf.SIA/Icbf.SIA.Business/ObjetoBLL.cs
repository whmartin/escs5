using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Objeto
    /// </summary>
    public class ObjetoBLL
    {
        private ObjetoDAL vObjetoDAL;
        public ObjetoBLL()
        {
            vObjetoDAL = new ObjetoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int InsertarObjeto(Objeto pObjeto)
        {
            try
            {
                return vObjetoDAL.InsertarObjeto(pObjeto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int ModificarObjeto(Objeto pObjeto)
        {
            try
            {
                return vObjetoDAL.ModificarObjeto(pObjeto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Objeto
        /// </summary>
        /// <param name="pObjeto"></param>
        public int EliminarObjeto(Objeto pObjeto)
        {
            try
            {
                return vObjetoDAL.EliminarObjeto(pObjeto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Objeto
        /// </summary>
        /// <param name="pIdObjetoCupos"></param>
        public Objeto ConsultarObjeto(int pIdObjetoCupos)
        {
            try
            {
                return vObjetoDAL.ConsultarObjeto(pIdObjetoCupos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Objeto
        /// </summary>
        /// <param name="pTitulo"></param>
        /// <param name="pDescripcionObjeto"></param>
        /// <param name="pEstado"></param>
        public List<Objeto> ConsultarObjetos(String pTitulo, String pDescripcionObjeto, Boolean? pEstado)
        {
            try
            {
                return vObjetoDAL.ConsultarObjetos(pTitulo, pDescripcionObjeto, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
