﻿//-----------------------------------------------------------------------
// <copyright file="SistemaInformacionBLL.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase SistemaInformacionBLL.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Icbf.SIA.Business.IBusiness;
using Icbf.SIA.Entity;
using Icbf.SIA.Integration.ServiceConsumer;
using Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase que maneja la logica de los sistemas de información.
    /// </summary>
    public class SistemaInformacionBLL : ISistemaInformacionBLL
    {
        /// <summary>
        /// Metodo para retornar el parametro
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>Lista de tipo controladores del sistema</returns>
        public List<SistemaInformacion> GetSistemaInformacion(int pId)
        {
            ISistemaInformacionSC vISistemaInformacionSC = new SistemaInformacionSC();
            int vIdSistemaInformacion = 1;
            return vISistemaInformacionSC.GetSistemaInformacion(vIdSistemaInformacion);
        }
    }
}
