﻿using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.SIA.Business
{
    public class CargaArchivoBLL
    {
        private CargaArchivoDAL vCargaArchivoDAL;

        public CargaArchivoBLL()
        {
            vCargaArchivoDAL = new CargaArchivoDAL();
        }

        public List<DescripcionErrorCarga> CargarDatosArchivo(String xml)
        {
            try
            {
                return vCargaArchivoDAL.CargarDatosArchivo(xml);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
