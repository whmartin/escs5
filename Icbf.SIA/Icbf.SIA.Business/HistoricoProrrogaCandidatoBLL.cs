using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  HistoricoProrrogaCandidato
    /// </summary>
    public class HistoricoProrrogaCandidatoBLL
    {
        private HistoricoProrrogaCandidatoDAL vHistoricoProrrogaCandidatoDAL;
        public HistoricoProrrogaCandidatoBLL()
        {
            vHistoricoProrrogaCandidatoDAL = new HistoricoProrrogaCandidatoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int InsertarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoDAL.InsertarHistoricoProrrogaCandidato(pHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int ModificarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoDAL.ModificarHistoricoProrrogaCandidato(pHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pHistoricoProrrogaCandidato"></param>
        public int EliminarHistoricoProrrogaCandidato(HistoricoProrrogaCandidato pHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoDAL.EliminarHistoricoProrrogaCandidato(pHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pIdHistoricoProrrogaCandidato"></param>
        public HistoricoProrrogaCandidato ConsultarHistoricoProrrogaCandidato(int pIdHistoricoProrrogaCandidato)
        {
            try
            {
                return vHistoricoProrrogaCandidatoDAL.ConsultarHistoricoProrrogaCandidato(pIdHistoricoProrrogaCandidato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad HistoricoProrrogaCandidato
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        /// <param name="pIdProyeccionPresuspuesto"></param>
        /// <param name="pFechaIncioAnterior"></param>
        /// <param name="pFechaIncioNueva"></param>
        /// <param name="pFechaFinAnterior"></param>
        /// <param name="pFechaFinNueva"></param>
        public List<HistoricoProrrogaCandidato> ConsultarHistoricoProrrogaCandidatos(int? pIdCupoArea, int? pIdProyeccionPresuspuesto, DateTime? pFechaIncioAnterior, DateTime? pFechaIncioNueva, DateTime? pFechaFinAnterior, DateTime? pFechaFinNueva)
        {
            try
            {
                return vHistoricoProrrogaCandidatoDAL.ConsultarHistoricoProrrogaCandidatos(pIdCupoArea, pIdProyeccionPresuspuesto, pFechaIncioAnterior, pFechaIncioNueva, pFechaFinAnterior, pFechaFinNueva);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
