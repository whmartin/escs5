using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  AdicionProyeccion
    /// </summary>
    public class AdicionProyeccionBLL
    {
        private AdicionProyeccionDAL vAdicionProyeccionDAL;
        public AdicionProyeccionBLL()
        {
            vAdicionProyeccionDAL = new AdicionProyeccionDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int InsertarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionDAL.InsertarAdicionProyeccion(pAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int ModificarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionDAL.ModificarAdicionProyeccion(pAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pAdicionProyeccion"></param>
        public int EliminarAdicionProyeccion(AdicionProyeccion pAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionDAL.EliminarAdicionProyeccion(pAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pIdAdicionProyeccion"></param>
        public AdicionProyeccion ConsultarAdicionProyeccion(int pIdAdicionProyeccion)
        {
            try
            {
                return vAdicionProyeccionDAL.ConsultarAdicionProyeccion(pIdAdicionProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AdicionProyeccion
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        /// <param name="pValorAdicionado"></param>
        /// <param name="pAprobado"></param>
        public AdicionProyeccion ConsultarAdicionProyeccions(int? pIdProyeccionPresupuestos, Decimal? pValorAdicionado, Boolean? pAprobado)
        {
            try
            {
                return vAdicionProyeccionDAL.ConsultarAdicionProyeccions(pIdProyeccionPresupuestos, pValorAdicionado, pAprobado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<ProyeccionAdiciones> ConsultarProyeccionAdiciones(int? pIdProyeccionPresupuestos, int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vAdicionProyeccionDAL.ConsultarProyeccionAdiciones(pIdProyeccionPresupuestos, pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProyeccionAdiciones> ConsultarProyeccionAdicionesIndividual(int? pIdProyeccionPresupuestos, int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
           , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vAdicionProyeccionDAL.ConsultarProyeccionAdicionesIndividual(pIdProyeccionPresupuestos, pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
