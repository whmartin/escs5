using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ProyeccionPresupuestos
    /// </summary>
    public class ProyeccionPresupuestosBLL
    {
        private ProyeccionPresupuestosDAL vProyeccionPresupuestosDAL;
        public ProyeccionPresupuestosBLL()
        {
            vProyeccionPresupuestosDAL = new ProyeccionPresupuestosDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int InsertarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.InsertarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int ModificarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ModificarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pProyeccionPresupuestos"></param>
        public int EliminarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.EliminarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdProyeccionPresupuestos"></param>
        public ProyeccionPresupuestos ConsultarProyeccionPresupuestos(int pIdProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ConsultarProyeccionPresupuestos(pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdArea"></param>
        /// <param name="pValorCupo"></param>
        /// <param name="pTotalCupos"></param>      
        /// <param name="pActivo"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestoss(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ConsultarProyeccionPresupuestoss(pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        
        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionPresupuestos
        /// </summary>
        /// <param name="pIdVigencia"></param>
        /// <param name="pIdArea"></param>
        /// <param name="pValorCupo"></param>
        /// <param name="pTotalCupos"></param>      
        /// <param name="pActivo"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestoCuposAre(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
            , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ConsultarProyeccionPresupuestoCuposAre(pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<ProyeccionPresupuestos> ConsultarProyeccionPresupuestosAdiciones(int? pIdVigencia, int? pIdArea, String pUsuarioAprobo
           , DateTime? pFechaAprobacion, int? pIdRegional, int? pIdRubro, string CodEstado)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ConsultarProyeccionPresupuestosAdiciones(pIdVigencia, pIdArea,
                    pUsuarioAprobo, pFechaAprobacion, pIdRegional, pIdRubro, CodEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionPresupuestos(ProyeccionPresupuestos pProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.AprobarProyeccionPresupuestos(pProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int AprobarProyeccionPresupuestos(List<ProyeccionPresupuestos> pProyeccionPresupuestos, string pUsuarioModifica)
        {
            try
            {
                return vProyeccionPresupuestosDAL.AprobarProyeccionPresupuestos(pProyeccionPresupuestos, pUsuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<HistoricoAproPresupuestos> ConsultarHistoricoProyeccionPresupuestos(int pIdProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ConsultarHistoricoProyeccionPresupuestos(pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoAproPresupuestos> ConsultarHistoricoProyeccionRechazados(int pIdProyeccionPresupuestos)
        {
            try
            {
                return vProyeccionPresupuestosDAL.ConsultarHistoricoProyeccionRechazados(pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
