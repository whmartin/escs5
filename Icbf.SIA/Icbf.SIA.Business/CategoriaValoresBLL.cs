using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  CategoriaValores
    /// </summary>
    public class CategoriaValoresBLL
    {
        private CategoriaValoresDAL vCategoriaValoresDAL;
        public CategoriaValoresBLL()
        {
            vCategoriaValoresDAL = new CategoriaValoresDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int InsertarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                return vCategoriaValoresDAL.InsertarCategoriaValores(pCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int ModificarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                return vCategoriaValoresDAL.ModificarCategoriaValores(pCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaValores
        /// </summary>
        /// <param name="pCategoriaValores"></param>
        public int EliminarCategoriaValores(CategoriaValores pCategoriaValores)
        {
            try
            {
                return vCategoriaValoresDAL.EliminarCategoriaValores(pCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CategoriaValores
        /// </summary>
        /// <param name="pIdCategoriaValor"></param>
        public CategoriaValores ConsultarCategoriaValores(int pIdCategoriaValor)
        {
            try
            {
                return vCategoriaValoresDAL.ConsultarCategoriaValores(pIdCategoriaValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaValores
        /// </summary>
        /// <param name="pNivel"></param>
        /// <param name="pIdVigencia"></param>
        /// <param name="pActivo"></param>
        public List<CategoriaValores> ConsultarCategoriaValoress(int? pNivel, int? pIdVigencia, Boolean? pActivo, int? IdCategoriaEmpleados)
        {
            try
            {
                return vCategoriaValoresDAL.ConsultarCategoriaValoress(pNivel, pIdVigencia, pActivo, IdCategoriaEmpleados);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CategoriaValores> ConsultarCategoriaValoresAsignada(int? pIdProyeccionPresupuestos, int? pIdCategoriaEmpleado)
        {
            try
            {
                return vCategoriaValoresDAL.ConsultarCategoriaValoresAsignada(pIdProyeccionPresupuestos, pIdCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
