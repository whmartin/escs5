using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  CupoAreas
    /// </summary>
    public class CupoAreasBLL
    {
        private CupoAreasDAL vCupoAreasDAL;
        public CupoAreasBLL()
        {
            vCupoAreasDAL = new CupoAreasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int InsertarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasDAL.InsertarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoCupoAreas(List<CupoAreas> pCupoAreasEstado, string pUsuarioModifica, int pIdProyeccionPresupuestos)
        {
            try
            {
                return vCupoAreasDAL.CambiarEstadoCupoAreas(pCupoAreasEstado, pUsuarioModifica, pIdProyeccionPresupuestos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int ModificarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasDAL.ModificarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarAdicionYProrrogas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasDAL.ModificarAdicionYProrrogas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CupoAreas
        /// </summary>
        /// <param name="pCupoAreas"></param>
        public int EliminarCupoAreas(CupoAreas pCupoAreas)
        {
            try
            {
                return vCupoAreasDAL.EliminarCupoAreas(pCupoAreas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CupoAreas
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        public CupoAreas ConsultarCupoAreas(int pIdCupoArea)
        {
            try
            {
                return vCupoAreasDAL.ConsultarCupoAreas(pIdCupoArea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad CupoAreas
        /// </summary>
        /// <param name="pIdCupoArea"></param>
        public List<AdicionesYProrrogas> ConsultarAdicionYProrrogas(CupoAreas pIdCupoArea)
        {
            try
            {
                return vCupoAreasDAL.ConsultarAdicionYProrrogas(pIdCupoArea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad CupoAreas
        /// </summary>
        /// <param name="pConsecutivoInterno"></param>
        /// <param name="pIdProveedor"></param>
        /// <param name="pFechaIngresoICBF"></param>
        /// <param name="pIdCategoriaValores"></param>
        /// <param name="pHonorarioBase"></param>
        /// <param name="pIvaHonorario"></param>
        /// <param name="pPorcentajeIVA"></param>
        /// <param name="pTiempoProyectadoMeses"></param>
        /// <param name="pTiempoProyectadoDias"></param>
        /// <param name="pAprobado"></param>
        /// <param name="pUsuarioAprobo"></param>
        /// <param name="pFechaAprobacion"></param>
        public List<CupoAreas> ConsultarCupoAreass(String pConsecutivoInterno, int? pIdProveedor, DateTime? pFechaIngresoICBF
            , int? pIdCategoriaValores, int? pIdProyeccionPresupuestos, String pIdEstadoProceso, string pNombreProveedor)
        {
            try
            {
                return vCupoAreasDAL.ConsultarCupoAreass(pConsecutivoInterno, pIdProveedor, pFechaIngresoICBF
            , pIdCategoriaValores, pIdProyeccionPresupuestos, pIdEstadoProceso, pNombreProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public CupoAreas ConsultarCupoAreasCategoria(int pIdProyeccionPresupuestos, int pIdCategoriaValores)
        {
            try
            {
                return vCupoAreasDAL.ConsultarCupoAreasCategoria(pIdProyeccionPresupuestos, pIdCategoriaValores);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
