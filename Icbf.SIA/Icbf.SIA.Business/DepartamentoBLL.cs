using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class DepartamentoBLL
    {
        private DepartamentoDAL vDepartamentoDAL;
        public DepartamentoBLL()
        {
            vDepartamentoDAL = new DepartamentoDAL();
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Departamento
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public Departamento ConsultarDepartamento(int pIdDepartamento)
        {
            try
            {
                return vDepartamentoDAL.ConsultarDepartamento(pIdDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Departamento
        /// </summary>
        /// <param name="pIdPais"></param>
        /// <param name="pCodigoDepartamento"></param>
        /// <param name="pNombreDepartamento"></param>
        /// <returns></returns>
        public List<Departamento> ConsultarDepartamentos(int? pIdPais, String pCodigoDepartamento, String pNombreDepartamento)
        {
            try
            {
                return vDepartamentoDAL.ConsultarDepartamentos(pIdPais, pCodigoDepartamento, pNombreDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
