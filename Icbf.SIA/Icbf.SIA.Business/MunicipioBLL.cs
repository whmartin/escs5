using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    public class MunicipioBLL
    {
        private MunicipioDAL vMunicipioDAL;
        public MunicipioBLL()
        {
            vMunicipioDAL = new MunicipioDAL();
        }

        public Municipio ConsultarMunicipio(int pIdMunicipio)
        {
            try
            {
                return vMunicipioDAL.ConsultarMunicipio(pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Municipio> ConsultarMunicipios(int? pIdDepartamento, String pCodigoMunicipio, String pNombreMunicipio)
        {
            try
            {
                return vMunicipioDAL.ConsultarMunicipios(pIdDepartamento, pCodigoMunicipio, pNombreMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Municipio> ConsultarMunicipiosPorIdsDep(string pIdsDepartamento)
        {
            try
            {
                return vMunicipioDAL.ConsultarMunicipiosPorIdsDep(pIdsDepartamento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
