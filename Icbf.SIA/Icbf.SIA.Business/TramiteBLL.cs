﻿//-----------------------------------------------------------------------
// <copyright file="TramiteBLL.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteBLL.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Icbf.SIA.Business.IBusiness;
using Icbf.SIA.Entity;
using Icbf.SIA.Integration.ServiceConsumer;
using Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase para manejar la logica del WebApi de Tramite.
    /// </summary>
    public class TramiteBLL:ITramiteBLL
    {
        /// <summary>
        /// Metodo para retornar el parametro
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>Lista de tramites</returns>
        public List<Tramite> GetTramite(int pId)
        {
            ITramiteSC vITramiteSC = new TramitesSC();
            int vIdTramite = 1;
            return vITramiteSC.GetTramite(vIdTramite);
        }
    }
}
