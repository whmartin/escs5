﻿
using Icbf.SIA.Business.IBusiness;
using Icbf.SIA.Entity;
using Icbf.SIA.Integration.ServiceConsumer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Business
{
    public class ConsultaNotariasBLL : IConsultaNotariasBLL
    {
        /// <summary>
        /// Metodo para retornar el parametro
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>Lista de tramites</returns>
        public List<Notaria> GetNotarias(string pId)
        {
            Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer.IConsultaNotariasSC vIConsultaNotariasSC = new ConsultaNotariasSC();
            //int vIdTramite = 1;
            return vIConsultaNotariasSC.GetNotarias("NOTARIAS");
        }
    }
}
