using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  CategoriaEmpleado
    /// </summary>
    public class CategoriaEmpleadoBLL
    {
        private CategoriaEmpleadoDAL vCategoriaEmpleadoDAL;
        public CategoriaEmpleadoBLL()
        {
            vCategoriaEmpleadoDAL = new CategoriaEmpleadoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int InsertarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                return vCategoriaEmpleadoDAL.InsertarCategoriaEmpleado(pCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int ModificarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                return vCategoriaEmpleadoDAL.ModificarCategoriaEmpleado(pCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCategoriaEmpleado"></param>
        public int EliminarCategoriaEmpleado(CategoriaEmpleado pCategoriaEmpleado)
        {
            try
            {
                return vCategoriaEmpleadoDAL.EliminarCategoriaEmpleado(pCategoriaEmpleado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pIdCategoria"></param>
        public CategoriaEmpleado ConsultarCategoriaEmpleado(int pIdCategoria)
        {
            try
            {
                return vCategoriaEmpleadoDAL.ConsultarCategoriaEmpleado(pIdCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CategoriaEmpleado
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pActivo"></param>
        public List<CategoriaEmpleado> ConsultarCategoriaEmpleados(String pCodigo, Boolean? pActivo, String pDescripcion, String pCodigoProducto)
        {
            try
            {
                return vCategoriaEmpleadoDAL.ConsultarCategoriaEmpleados(pCodigo, pActivo, pDescripcion, pCodigoProducto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CategoriaEmpleado> ConsultarCategoriaEmpleadoAsignada(int? pIdProyeccionPresupuestos, String pCodEstadoProceso)
        {
            try
            {
                return vCategoriaEmpleadoDAL.ConsultarCategoriaEmpleadoAsignada(pIdProyeccionPresupuestos, pCodEstadoProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
