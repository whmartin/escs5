﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Utilities.Exceptions;
using System.IO;
using System.Net;
using System.Collections;
using System.Web;
using System.Data;
//using System.Net;
using System.Net.Security;

using Icbf.SIA.Entity;


namespace Icbf.SIA.Business
{
    public class UtilidadesArchivoBLL
    {
        /// <summary>
        /// Método que Certifica un Archivo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certification"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        /// <summary>
        /// Método que descarga un archivo del FTP
        /// </summary>
        /// <param name="NombreFTP"></param>
        /// <param name="NombreArchivo"></param>
        /// <returns></returns>
        public MemoryStream DescargarArchivoFtp(string NombreFTP, string NombreArchivo)
        {
            MemoryStream archivo = null;
            int vIntentos = 0;
            try
            {
                while (archivo == null && vIntentos < 10) 
                {
                    FtpWebRequest vFtpWebRequest;
                    //configuracion para conectarte al ftp
                    vFtpWebRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(NombreFTP + NombreArchivo));
                    //vFtpWebRequest.PreAuthenticate = true;
                    vFtpWebRequest.UseBinary = true;
                    vFtpWebRequest.Proxy = null;
                    vFtpWebRequest.KeepAlive = true;
                    vFtpWebRequest.Method = WebRequestMethods.Ftp.DownloadFile;

                    Boolean vEnableSsl;
                    Boolean vFtpUsePassive;
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpEnableSsl"], out vEnableSsl))
                    {
                        throw new UserInterfaceException("El parametro FtpEnableSsl tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                    }
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpUsePassive"], out vFtpUsePassive))
                    {
                        throw new UserInterfaceException("El parametro FtpUsePassive tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                    }
                    vFtpWebRequest.EnableSsl = vEnableSsl;
                    vFtpWebRequest.UsePassive = vFtpUsePassive;

                    Boolean vFtpA;
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpA"], out vFtpA))
                    {
                        throw new UserInterfaceException("El parametro FtpA tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                    }


                    if (vFtpA)
                    {
                        vFtpWebRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FtpUser"], System.Configuration.ConfigurationManager.AppSettings["FtpPass"]);
                        Boolean vFtps;
                        if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["Ftps"], out vFtps))
                        {
                            throw new UserInterfaceException("El parametro Ftps tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                        }

                        if (vFtps)
                            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                    }
                    else
                    {
                        vFtpWebRequest.Credentials = new NetworkCredential("", "");
                    }

                    try
                    {
                        MemoryStream outputStream = new MemoryStream();
                        FtpWebResponse response = (FtpWebResponse)vFtpWebRequest.GetResponse();
                        Stream ftpStream = response.GetResponseStream();

                        long cl = response.ContentLength;
                        int bufferSize = 2048;
                        int readCount;
                        byte[] buffer = new byte[bufferSize];
                        readCount = ftpStream.Read(buffer, 0, bufferSize);

                        while (readCount > 0)
                        {
                            outputStream.Write(buffer, 0, readCount);
                            readCount = ftpStream.Read(buffer, 0, bufferSize);
                        }
                        ftpStream.Close();
                        outputStream.Close();
                        response.Close();

                        archivo = outputStream;

                        //using (FtpWebResponse response = (FtpWebResponse)vFtpWebRequest.GetResponse()) 
                        //{
                        //    archivo = response.GetResponseStream();
                        //    vIntentos++;
                        //}
                    }
                    catch (Exception ex)
                    {
                        vIntentos++;
                    }

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
            return archivo;
        }
        /// <summary>
        /// Método que Sube un Archivo al FTP
        /// </summary>
        /// <param name="pFileStream"></param>
        /// <param name="NombreFTP"></param>
        /// <param name="Nombre"></param>
        /// <returns></returns>
        public Boolean SubirArchivoFtp(Stream pFileStream, String NombreFTP, String Nombre)
        {
            try
            {
                Boolean vResultado = false;
                int vIntentos = 0;
                while (!vResultado && vIntentos < 10)
                {
                    FtpWebRequest vFtpWebRequest;
                    vFtpWebRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(NombreFTP+Nombre));
                    vFtpWebRequest.UseBinary = true;
                    vFtpWebRequest.Proxy = null;
                    vFtpWebRequest.KeepAlive = true;
                    vFtpWebRequest.Method = WebRequestMethods.Ftp.UploadFile;

                    Boolean vEnableSsl;
                    Boolean vFtpUsePassive;
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpEnableSsl"], out vEnableSsl))
                    {
                        throw new UserInterfaceException("El parametro FtpEnableSsl tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                    }
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpUsePassive"], out vFtpUsePassive))
                    {
                        throw new UserInterfaceException("El parametro FtpUsePassive tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                    }
                    vFtpWebRequest.EnableSsl = vEnableSsl;
                    vFtpWebRequest.UsePassive = vFtpUsePassive;

                    Boolean vFtpA;
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpA"], out vFtpA))
                    {
                        throw new UserInterfaceException("El parametro FtpA tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                    }


                    if (vFtpA)
                    {
                        vFtpWebRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FtpUser"], System.Configuration.ConfigurationManager.AppSettings["FtpPass"]);
                        Boolean vFtps;
                        if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["Ftps"], out vFtps))
                        {
                            throw new UserInterfaceException("El parametro Ftps tiene un valor incorrecto, por favor cambielo en el Web.Config.");
                        }

                        if (vFtps)
                            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                    }
                    else
                    {
                        vFtpWebRequest.Credentials = new NetworkCredential("", "");
                    }

                    byte[] vByte = new byte[pFileStream.Length];
                    pFileStream.Read(vByte, 0, (int)pFileStream.Length);
                    try
                    {
                        using (Stream vWriter = vFtpWebRequest.GetRequestStream())
                        {
                            vWriter.Write(vByte, 0, vByte.Length);
                            vIntentos++;
                            vResultado = true;
                        }
                    }
                    catch(Exception ex)
                    {
                        vIntentos++;
                    }
                }
                return vResultado;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public  bool CrearDirectorioFTP(string directorio)
        {

            try
            {
                FtpWebRequest vFtpWebRequest;
                vFtpWebRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(directorio));
                vFtpWebRequest.UseBinary = true;
                vFtpWebRequest.Proxy = null;
                vFtpWebRequest.KeepAlive = true;
                vFtpWebRequest.Method = WebRequestMethods.Ftp.MakeDirectory;

                Boolean vEnableSsl;
                Boolean vFtpUsePassive;
                if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpEnableSsl"], out vEnableSsl))
                    throw new UserInterfaceException("El parametro FtpEnableSsl tiene un valor incorrecto, por favor cambielo en el Web.Config.");

                if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpUsePassive"], out vFtpUsePassive))
                    throw new UserInterfaceException("El parametro FtpUsePassive tiene un valor incorrecto, por favor cambielo en el Web.Config.");

                vFtpWebRequest.EnableSsl = vEnableSsl;
                vFtpWebRequest.UsePassive = vFtpUsePassive;

                Boolean vFtpA;
                if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["FtpA"], out vFtpA))
                    throw new UserInterfaceException("El parametro FtpA tiene un valor incorrecto, por favor cambielo en el Web.Config.");

                if (vFtpA)
                {
                    vFtpWebRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FtpUser"], System.Configuration.ConfigurationManager.AppSettings["FtpPass"]);
                    Boolean vFtps;
                    if (!Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["Ftps"], out vFtps))
                        throw new UserInterfaceException("El parametro Ftps tiene un valor incorrecto, por favor cambielo en el Web.Config.");

                    if (vFtps)
                        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                }
                else
                    vFtpWebRequest.Credentials = new NetworkCredential("", "");

                FtpWebResponse response = (FtpWebResponse)vFtpWebRequest.GetResponse();
                Stream ftpStream = response.GetResponseStream();

                ftpStream.Close();
                response.Close();

                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                    return true;
                }
                else
                {
                    response.Close();
                    return false;
                }
            }
        }
    }
}
