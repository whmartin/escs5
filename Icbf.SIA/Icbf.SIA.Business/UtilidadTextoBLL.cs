﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Business
{
    public class UtilidadTextoBLL
    {
        /// <summary>
        /// Método que quita espacios de un Texto
        /// </summary>
        /// <param name="Texto"></param>
        /// <returns></returns>
        public string QuitarEspacios(string Texto)
        {
            Texto = Texto.Replace(" ", "");
            return Texto;
        }


    }
}
