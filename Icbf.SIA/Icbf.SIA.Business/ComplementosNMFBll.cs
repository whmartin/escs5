﻿using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Business
{
    public class ComplementosNMFBll
    {
        private ComplementosNMFDAL vComplementosNMFDAL;
        public ComplementosNMFBll()
        {
            vComplementosNMFDAL = new ComplementosNMFDAL();
        }

        public List<TipoRecursoFinPptal> ConsultarTipoRecursoFinPptal()
        {
            try
            {
                return vComplementosNMFDAL.ConsultarTipoRecursoFinPptal();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TipoRecursoFinPptal ConsultarRecurso(int? pIdRecurso)
        {
            try
            {
                return vComplementosNMFDAL.ConsultarRecurso(pIdRecurso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CatGralRubrosPptalGasto> ConsultarCatGralRubrosPptalGasto(int? pIdAreasInt)
        {
            try
            {
                return vComplementosNMFDAL.ConsultarCatGralRubrosPptalGasto(pIdAreasInt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AreasInternasNMF> ConsultarAreas(int? pIdAreasInt)
        {
            try
            {
                return vComplementosNMFDAL.ConsultarAreas(pIdAreasInt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
