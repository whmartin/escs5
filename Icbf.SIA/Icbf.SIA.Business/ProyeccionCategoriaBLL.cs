using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ProyeccionCategoria
    /// </summary>
    public class ProyeccionCategoriaBLL
    {
        private ProyeccionCategoriaDAL vProyeccionCategoriaDAL;
        public ProyeccionCategoriaBLL()
        {
            vProyeccionCategoriaDAL = new ProyeccionCategoriaDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int InsertarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaDAL.InsertarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int ModificarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaDAL.ModificarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pProyeccionCategoria"></param>
        public int EliminarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaDAL.EliminarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pIDProyeccionCategoria"></param>
        public ProyeccionCategoria ConsultarProyeccionCategoria(int? pIDProyeccionCategoria, int? pIdProyeccion)
        {
            try
            {
                return vProyeccionCategoriaDAL.ConsultarProyeccionCategoria(pIDProyeccionCategoria, pIdProyeccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ProyeccionCategoria
        /// </summary>
        /// <param name="pCantidad"></param>
        public List<ProyeccionCategoria> ConsultarProyeccionCategorias(int? pIdProyeccionNecesidad, String pCodEstadoProceso)
        {
            try
            {
                return vProyeccionCategoriaDAL.ConsultarProyeccionCategorias(pIdProyeccionNecesidad, pCodEstadoProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionCategorias(List<ProyeccionCategoria> pProyeccionCategoria, string pUsuarioModifica)
        {
            try
            {
                return vProyeccionCategoriaDAL.AprobarProyeccionCategorias(pProyeccionCategoria, pUsuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AprobarProyeccionCategoria(ProyeccionCategoria pProyeccionCategoria)
        {
            try
            {
                return vProyeccionCategoriaDAL.AprobarProyeccionCategoria(pProyeccionCategoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
