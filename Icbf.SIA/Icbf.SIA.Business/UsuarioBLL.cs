using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Net.Mail;

namespace Icbf.SIA.Business
{
    /// <summary>
    /// Clase Business para la entidad Usuario
    /// </summary>
    public class UsuarioBLL
    {
        private UsuarioDAL vUsuarioDAL;
        public UsuarioBLL()
        {
            vUsuarioDAL = new UsuarioDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int InsertarUsuario(Usuario pUsuario)
        {
            try
            {

                MembershipUserCollection oMuColection = Membership.FindUsersByEmail(pUsuario.CorreoElectronico);
                if (oMuColection.Count > 0)
                {
                    foreach (var VARIABLE in oMuColection)
                    {
                        MembershipUser xx = (MembershipUser)VARIABLE;

                        if (xx.IsApproved)
                        {
                            throw new UserInterfaceException("Correo electr�nico ya existe y su cuenta esta activa");
                        }
                        else
                        {
                            throw new UserInterfaceException("Usuario existente, tiene pendiente activar la cuenta");
                        }

                    }
                }

                MembershipUser oMu = Membership.GetUser(pUsuario.NombreUsuario);
                if (oMu != null)
                {
                    return -1;
                }
                else
                {
                    string[] RolUsuario = pUsuario.Rol.Split(';');
                    RolUsuario = RolUsuario.Where(val => val != "").ToArray();

                    foreach (string itemRol in RolUsuario)
                    {
                        if (!Roles.RoleExists(itemRol))
                        {
                            throw new UserInterfaceException("EL rol seleccionado no existe, verifique por favor.");
                        }
                    }
                    oMu = Membership.CreateUser(pUsuario.NombreUsuario, pUsuario.Contrasena, pUsuario.CorreoElectronico);


                    foreach (string itemRol in RolUsuario)
                    {
                        Roles.AddUserToRole(pUsuario.NombreUsuario, itemRol);
                    }

                    pUsuario.Providerkey = oMu.ProviderUserKey.ToString();


                    if (pUsuario.Rol.Split(';').Contains("OFERENTEWEB") || pUsuario.Rol.Split(';').Contains("PROVEEDORES") || pUsuario.Rol.Split(';').Contains("OACWEB"))
                    {
                        oMu.Comment = pUsuario.CodigoValidacion;
                        oMu.IsApproved = false;
                        Membership.UpdateUser(oMu);
                    }


                    return vUsuarioDAL.InsertarUsuario(pUsuario);

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                string[] RolUsuario = pUsuario.Rol.Split(';');
                RolUsuario = RolUsuario.Where(val => val != "").ToArray();

                foreach (string itemRol in RolUsuario)
                {
                    if (Roles.RoleExists(itemRol))
                        if (Roles.FindUsersInRole(itemRol, pUsuario.NombreUsuario).Length > 0)
                            Roles.RemoveUserFromRole(pUsuario.NombreUsuario, itemRol);
                }


                Membership.DeleteUser(pUsuario.NombreUsuario);
                if (pUsuario.IdUsuario != 0)
                    vUsuarioDAL.EliminarUsuario(pUsuario);
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de inserci�n para la entidad Usuario desde la pantalla tercero
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int InsertarUsuarioInterno(Usuario pUsuario)
        {
            try
            {

                MembershipUserCollection oMuColection = Membership.FindUsersByEmail(pUsuario.CorreoElectronico);
                if (oMuColection.Count > 0)
                {
                    foreach (var VARIABLE in oMuColection)
                    {
                        MembershipUser xx = (MembershipUser)VARIABLE;

                        if (xx.IsApproved)
                        {
                            throw new UserInterfaceException("Correo electr�nico ya existe y su cuenta esta activa");
                        }
                        else
                        {
                            throw new UserInterfaceException("Usuario existente, tiene pendiente activar la cuenta");
                        }

                    }
                }

                MembershipUser oMu = Membership.GetUser(pUsuario.NombreUsuario);
                if (oMu != null)
                {
                    return -1;
                }
                else
                {
                    string[] RolUsuario = pUsuario.Rol.Split(';');
                    RolUsuario = RolUsuario.Where(val => val != "").ToArray();

                    foreach (string itemRol in RolUsuario)
                    {
                        if (!Roles.RoleExists(itemRol))
                        {
                            throw new UserInterfaceException("EL rol seleccionado no existe, verifique por favor.");
                        }
                    }
                    oMu = Membership.CreateUser(pUsuario.NombreUsuario, pUsuario.Contrasena, pUsuario.CorreoElectronico);


                    foreach (string itemRol in RolUsuario)
                    {
                        Roles.AddUserToRole(pUsuario.NombreUsuario, itemRol);
                    }

                    pUsuario.Providerkey = oMu.ProviderUserKey.ToString();

                    return vUsuarioDAL.InsertarUsuario(pUsuario);

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                string[] RolUsuario = pUsuario.Rol.Split(';');
                RolUsuario = RolUsuario.Where(val => val != "").ToArray();

                foreach (string itemRol in RolUsuario)
                {
                    if (Roles.RoleExists(itemRol))
                        if (Roles.FindUsersInRole(itemRol, pUsuario.NombreUsuario).Length > 0)
                            Roles.RemoveUserFromRole(pUsuario.NombreUsuario, itemRol);
                }


                Membership.DeleteUser(pUsuario.NombreUsuario);
                if (pUsuario.IdUsuario != 0)
                    vUsuarioDAL.EliminarUsuario(pUsuario);
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo de modificaci�n para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int ModificarUsuario(Usuario pUsuario)
        {
            try
            {
                MembershipUser oMu = Membership.GetUser(pUsuario.NombreUsuario);
                if (oMu == null)
                {
                    return -1;
                }
                else
                {
                    if (pUsuario.Contrasena.Trim() != "")
                    {
                        oMu.ChangePassword(oMu.GetPassword(), pUsuario.Contrasena);
                    }

                    bool vExisteRol = false;

                    string[] RolUsuario = pUsuario.Rol.Split(';');
                    RolUsuario = RolUsuario.Where(val => val != "").ToArray();

                    foreach (string itemRol in RolUsuario)
                    {
                        if (!Roles.RoleExists(itemRol))
                        {
                            throw new UserInterfaceException("EL rol seleccionado no existe, verifique por favor.");
                        }
                    }

                    //foreach (string ItemRolRem in Roles.GetRolesForUser(oMu.UserName))
                    //{
                    //    if (Roles.RoleExists(pUsuario.Rol))
                    //    {
                    //        throw new UserInterfaceException("Uno De Los Roles seleccionado no existe, verifique por favor.");
                    //    }
                    //}


                    oMu.Email = pUsuario.CorreoElectronico;
                    oMu.IsApproved = pUsuario.Estado;
                    Membership.UpdateUser(oMu);
                    //if (Roles.GetRolesForUser(oMu.UserName).Length > 0)
                    foreach (string ItemRolRem in Roles.GetRolesForUser(oMu.UserName))
                    {
                        Roles.RemoveUserFromRole(pUsuario.NombreUsuario, ItemRolRem);
                    }

                    foreach (string itemRol in RolUsuario)
                    {
                        if (!string.IsNullOrEmpty(itemRol))
                            Roles.AddUserToRole(pUsuario.NombreUsuario, itemRol);
                    }
                    return vUsuarioDAL.ModificarUsuario(pUsuario);

                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <sumary>        
        /// M�todo para modificar el nombre usuario que es el correo de acceso
        /// <param name="pIdUsuario"></param>
        /// <param name="pcorreoErrado"></param>
        /// <param name="pcorreoNuevo"></param>
        /// <returns></returns>
        public int modificarCorreoUsuario(Usuario pUsuario)
        {
            try
            {
                vUsuarioDAL.modificarCorreoUsuario(pUsuario);
                return vUsuarioDAL.modificarCorreoUsuarioQoS(pUsuario, Membership.ApplicationName);
            }
            catch (Exception)
            {

                throw;
            }
        }




        /// <summary>
        /// M�todo de eliminaci�n para la entidad Usuario
        /// </summary>
        /// <param name="pUsuario"></param>
        /// <returns></returns>
        public int EliminarUsuario(Usuario pUsuario)
        {
            try
            {
                return vUsuarioDAL.EliminarUsuario(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por Nombre de Usuario para la entidad Usuario
        /// </summary>
        /// <param name="pNombreUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(string pNombreUsuario)
        {
            try
            {
                Usuario oUsuario;
                MembershipUser oMu = Membership.GetUser(pNombreUsuario);
                if (oMu != null)
                {
                    oUsuario = vUsuarioDAL.ConsultarUsuario(oMu.ProviderUserKey.ToString());

                    oUsuario.NombreUsuario = oMu.UserName;
                    oUsuario.CorreoElectronico = oMu.Email;
                    //oUsuario.Contrasena = oMu.GetPassword();
                    oUsuario.Rol = string.Join(";", Roles.GetRolesForUser(oMu.UserName));
                    return oUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarDatosUsuario(int pIdUsuario)
        {
            try
            {
                return vUsuarioDAL.ConsultarUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id y por numero de documento para la entidad Usuario
        /// </summary>
        /// <param name="pTipoDocumento"></param>
        /// <param name="pNumeroDocumento"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(int pTipoDocumento, string pNumeroDocumento)
        {
            try
            {
                Usuario oUsuario;
                oUsuario = vUsuarioDAL.ConsultarUsuario(pTipoDocumento, pNumeroDocumento);
                if (oUsuario.IdUsuario != 0)
                {
                    return oUsuario;
                }
                else if (oUsuario.NombreUsuario != null)
                {
                    MembershipUser oMu = Membership.GetUser(oUsuario.NombreUsuario);
                    if (oMu != null)
                    {
                        oUsuario.NombreUsuario = oMu.UserName;
                        oUsuario.CorreoElectronico = oMu.Email;
                        oUsuario.Contrasena = oMu.GetPassword();
                        oUsuario.Rol = Roles.GetRolesForUser(pNumeroDocumento)[0];
                    }
                    return oUsuario;
                }
                return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Usuario
        /// </summary>
        /// <param name="pNumeroDocumento"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pPerfil"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Usuario> ConsultarUsuarios(String pNumeroDocumento, String pPrimerNombre, String pPrimerApellido, int? pPerfil, Boolean? pEstado)
        {
            try
            {
                List<Usuario> vListaUsuarios = vUsuarioDAL.ConsultarUsuarios(pNumeroDocumento, pPrimerNombre, pPrimerApellido, pPerfil, pEstado);
                //foreach (Usuario vUsuario in vListaUsuarios)
                //{
                //    MembershipUser oMu = Membership.GetUser(new Guid(vUsuario.Providerkey));
                //    if (oMu != null)
                //    {
                //        vUsuario.NombreUsuario = oMu.UserName;
                //        vUsuario.CorreoElectronico = oMu.Email;
                //        vUsuario.Contrasena = oMu.GetPassword();
                //        //vUsuario.Rol = Roles.GetRolesForUser(oMu.UserName)[0];
                //    }
                //}
                return vListaUsuarios;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Usuario
        /// </summary>
        /// <param name="pIdUsuario"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuario(int pIdUsuario)
        {
            try
            {
                Usuario oUsuario = vUsuarioDAL.ConsultarUsuario(pIdUsuario);
                MembershipUser oMu = Membership.GetUser(new Guid(oUsuario.Providerkey));
                if (oMu != null)
                {
                    oUsuario.NombreUsuario = oMu.UserName;
                    oUsuario.CorreoElectronico = oMu.Email;
                    oUsuario.Contrasena = oMu.GetPassword();
                    oUsuario.Rol = String.Join(";", Roles.GetRolesForUser(oMu.UserName));//[0];
                    oUsuario.CodigoValidacion = oMu.Comment;
                    return oUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo de consulta para la entidad Usuario
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ConsultarTodosUsuarios()
        {
            try
            {
                List<Usuario> oListUsuario = new List<Usuario>();
                MembershipUserCollection oMuC = Membership.GetAllUsers();
                if (oMuC != null)
                {
                    foreach (MembershipUser oMu in oMuC)
                    {
                        Usuario oUsuario = vUsuarioDAL.ConsultarUsuario(oMu.ProviderUserKey.ToString());
                        oUsuario.NombreUsuario = oMu.UserName;
                        oUsuario.CorreoElectronico = oMu.Email;
                        oUsuario.Contrasena = oMu.GetPassword();
                        oUsuario.Rol = String.Join(";", Roles.GetRolesForUser(oMu.UserName)); //Roles.GetRolesForUser(oMu.UserName)[0];
                        oListUsuario.Add(oUsuario);
                    }
                    return oListUsuario;
                }
                else
                    return null;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo de modificaci�n para la Razon Social de un Usuario
        /// </summary>
        /// <param name="pRazonSocial"></param>
        /// <param name="pproviderKey"></param>
        /// <returns></returns>
        public bool ModificarRazonSocialDeUsuario(string pRazonSocial, string pproviderKey)
        {
            try
            {
                return vUsuarioDAL.ModificarRazonSocialDeUsuario(pRazonSocial, pproviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// M�todo de consulta por ProviderKey para la entidad Usuario
        /// </summary>
        /// <param name="pProviderKey"></param>
        /// <returns></returns>
        public Usuario ConsultarUsuarioPorproviderKey(string pProviderKey)
        {
            try
            {
                return vUsuarioDAL.ConsultarUsuario(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por NumeroIdentificacion para la entidad Usuario
        /// </summary>
        /// <param name="pNumeroDocumento"></param>
        /// /// <param name="pNombre"></param>
        public List<Usuario> ConsultarUsuarioss(string pNumeroDocumento, string pNombre)
        {
            try
            {
                return vUsuarioDAL.ConsultarUsuarioss(pNumeroDocumento,pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public Usuario ConsultarUsuarioPorId(int pIdUsuario)
        {
            try
            {
                return vUsuarioDAL.ConsultarUsuarioPorId(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
