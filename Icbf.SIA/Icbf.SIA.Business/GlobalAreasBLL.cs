﻿using Icbf.SIA.DataAccess;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.SIA.Business
{
    public class GlobalAreasBLL
    {
        private GlobalAreasDAL vGlobalAreasDAL;

        public GlobalAreasBLL()
        {
            vGlobalAreasDAL = new GlobalAreasDAL();
        }
        

        public List<GlobalAreas> ConsultarGlobalArea(int? pIdAreasInt, int? pIdRegionalInt)
        {
            try
            {
                return vGlobalAreasDAL.ConsultarGlobalArea(pIdAreasInt, pIdRegionalInt);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GlobalAreas> ConsultarAreasRegionales(int? pIdAreasInt, String pIdRegionalesStr)
        {
            try
            {
                return vGlobalAreasDAL.ConsultarAreasRegionales(pIdAreasInt, pIdRegionalesStr);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
