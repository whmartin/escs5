﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.TaskScheduler;
using Icbf.Utilities.Exceptions;
using System.Windows.Forms;
using System.IO;


namespace ICBF.ActualizaInfoPaccoContratos.Scheduler
{
    public class prueba
    {
        static void Main(string[] args)
        {
            try
            {
                using (TaskService ts = new TaskService())
                {                    
                    string Ubicacion = Directory.GetCurrentDirectory() + @"\ICBF.ActualizaInfoPaccoContratos.exe";
                    string UserName = Environment.UserName;
                    // Create a new task definition and assign properties
                    TaskDefinition td = ts.NewTask();
                    td.RegistrationInfo.Description = "Actualización estudios del sector con información de PACCO y contratos";
                    // Create a trigger that will fire the task at this time every other day
                    td.Triggers.Add(new DailyTrigger { DaysInterval = 1, StartBoundary = DateTime.Today.AddDays(1).AddHours(23) });
                    // Create an action that will launch Notepad whenever the trigger fires
                    td.Actions.Add(new ExecAction(@Ubicacion, null, null));
                    // Register the task in the root folder
                     ts.RootFolder.RegisterTaskDefinition(@"Actualiza InfoPacco Contratos", td, TaskCreation.CreateOrUpdate, UserName, null,TaskLogonType.InteractiveTokenOrPassword,"");
                }

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
            
        }

    }
}
