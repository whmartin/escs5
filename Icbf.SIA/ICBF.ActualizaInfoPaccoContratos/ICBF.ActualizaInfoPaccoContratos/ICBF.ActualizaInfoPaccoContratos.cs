﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Runtime.InteropServices;
using System.CodeDom.Compiler;
using System.ServiceModel;

namespace ICBF.ActualizaInfoPaccoContratos
{
     public class Program
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SystemParametersInfo(int uAction,
        int uParam, string lpvParam, int fuWinIni);
        static void Main(string[] args)
        {
            System.Console.WriteLine("Realizando actualización...");
            try
            {
                Actualizar();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        static void Actualizar()
        {
            List<RegistroSolicitudEstudioSectoryCaso> vRegistroSolicitudEstudioSectoryCaso = new List<RegistroSolicitudEstudioSectoryCaso>();
            vRegistroSolicitudEstudioSectoryCaso = ConsultarSolicitudEstudioSectoryCasos();
            foreach (var vRegistroEstudioSectoryCaso in vRegistroSolicitudEstudioSectoryCaso)
            {
                ICBF.ActualizaInfoPaccoContratos.WSContratosPACCO.WSContratosPACCOSoapClient wsContratosPacco = new ICBF.ActualizaInfoPaccoContratos.WSContratosPACCO.WSContratosPACCOSoapClient();
                var myGridResults = wsContratosPacco.GetListaConsecutivosEncabezadosPACCOXModalidad(Convert.ToInt32(vRegistroEstudioSectoryCaso.VigenciaPACCO),
                                                                                                      null,
                                                                                                      0,
                                                                                                      0,
                                                                                                      vRegistroEstudioSectoryCaso.ConsecutivoPACCO,
                                                                                                      null,
                                                                                                      0,
                                                                                                      0,
                                                                                                      null,
                                                                                                      0);
                
                if (myGridResults.Length > 0)
                {
                    ActualizarRegistroSolicitudEstudioSectoryCasos(vRegistroEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso, Convert.ToInt32(myGridResults[0].codigoarea), Convert.ToInt32(myGridResults[0].Id_modalidad), myGridResults[0].objeto_contractual, myGridResults[0].valor_contrato, vRegistroEstudioSectoryCaso.VigenciaPACCO);
                    if (vRegistroEstudioSectoryCaso.IdContrato != null)
                    {
                        ActualizarInformacionProcesoSeleccion(vRegistroEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso, Convert.ToInt32(vRegistroEstudioSectoryCaso.IdContrato));
                    }
                }

            }
        }

        static List<RegistroSolicitudEstudioSectoryCaso> ConsultarSolicitudEstudioSectoryCasos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudesPACCO_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCaso = new List<RegistroSolicitudEstudioSectoryCaso>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
                            vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso;
                            vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO = vDataReaderResults["ConsecutivoPACCO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ConsecutivoPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO;
                            vRegistroSolicitudEstudioSectoryCaso.IdContrato = vDataReaderResults["IdContrato"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContrato"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdContrato;
                            vListaRegistroSolicitudEstudioSectoryCaso.Add(vRegistroSolicitudEstudioSectoryCaso);
                        }
                        return vListaRegistroSolicitudEstudioSectoryCaso;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        static int ActualizarRegistroSolicitudEstudioSectoryCasos(int consecutivo, int codigoarea, int Id_modalidad, string objeto_contractual, decimal? valor_contrato, string VigenciaPACCO)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudesPACCO_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@consecutivo", DbType.Decimal, consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@codigoarea", DbType.String, codigoarea);
                    vDataBase.AddInParameter(vDbCommand, "@Id_modalidad", DbType.Int32, Id_modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@objeto_contractual", DbType.String, objeto_contractual);
                    vDataBase.AddInParameter(vDbCommand, "@valor_contrato", DbType.Decimal, valor_contrato);
                    vDataBase.AddInParameter(vDbCommand, "@VigenciaPACCO", DbType.String, VigenciaPACCO);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        static int ActualizarInformacionProcesoSeleccion(int IdConsecutivoEstudio, int IdContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_InformacionProcesoSeleccion_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdConsecutivoEstudio", DbType.Int32, IdConsecutivoEstudio);
                    vDataBase.AddInParameter(vDbCommand, "@IdContrato", DbType.Int32, IdContrato);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        static Microsoft.Practices.EnterpriseLibrary.Data.Database ObtenerInstancia()
        {
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
        }

        public class RegistroSolicitudEstudioSectoryCaso
        {
            public int IdRegistroSolicitudEstudioSectoryCaso;
            public string VigenciaPACCO;
            public int ConsecutivoPACCO;
            public int? IdContrato;
        }

    }
}
