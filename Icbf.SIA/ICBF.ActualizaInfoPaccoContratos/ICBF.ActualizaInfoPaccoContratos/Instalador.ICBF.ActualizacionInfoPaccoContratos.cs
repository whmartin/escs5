﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32.TaskScheduler;
using ICBF.ActualizaInfoPaccoContratos;
using System.ServiceProcess;
using System.Diagnostics;


namespace ICBF.ActualizaInfoPaccoContratos
{
    [RunInstaller(true)]
    public partial class Instalador : System.Configuration.Install.Installer
    {
        public Instalador()
        {
            InitializeComponent();            
        }


        protected override void OnAfterInstall(IDictionary savedState)
        {
            try
            {
                string Audita = Context.Parameters["Audita"];
                string QoS = Context.Parameters["QoS"];
                string DataBaseConnec = Context.Parameters["DataBaseConnec"];
                string DataBaseCENTRAL = Context.Parameters["DataBaseCENTRAL"];
                string Actualizador = null;

                ExeConfigurationFileMap map = new ExeConfigurationFileMap();
                //Getting the path location 
                string configFile = string.Concat(Assembly.GetExecutingAssembly().Location, ".config");
                Actualizador = Assembly.GetExecutingAssembly().Location;
                map.ExeConfigFilename = configFile;
                var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                ConnectionStringSettings connectionstring = new ConnectionStringSettings();
                config.ConnectionStrings.ConnectionStrings["AuditaConnectionString"].ConnectionString = Audita;
                config.Save();
                config.ConnectionStrings.ConnectionStrings["QoS"].ConnectionString = QoS;
                config.Save();

                config.ConnectionStrings.ConnectionStrings["DataBaseConnectionString"].ConnectionString = DataBaseConnec;
                config.Save();

                config.ConnectionStrings.ConnectionStrings["DataBaseCENTRAL"].ConnectionString = DataBaseCENTRAL;
                config.Save();
            }
            catch (Exception ex)
            {
                throw new InstallException("A forced exception", ex);
            }
        }


        protected override void OnCommitting(IDictionary savedState)
        {
            base.OnCommitting(savedState);
            ProcessStartInfo info = new ProcessStartInfo();
            info.UseShellExecute = true;
            info.FileName = "ICBF.ActualizaInfoPaccoContratos.Scheduler.exe";
            info.WorkingDirectory = System.IO.Path.GetDirectoryName(this.Context.Parameters["AssemblyPath"]);
            Process.Start(info);

        }

    }
}
