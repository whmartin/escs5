﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  FormaPago
    /// </summary>
    public class TipoFormaPagoBLL
    {
        private TipoFormaPagoDAL vTipoFormaPagoDAL;
        public TipoFormaPagoBLL()
        {
            vTipoFormaPagoDAL = new TipoFormaPagoDAL();
        }
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Metodo que permite consultar la entidad TipoFormaPago filtrando los resultados por los filtros
        /// codigoTipoFormaPago, nombreTipoFormaPago, descripcion, estado
        /// </summary>
        /// <param name="pCodigoTipoFormaPago">Cadena de texto con el codigo del tipo forma pago</param>
        /// <param name="pNombreTipoFormaPago">Cadena de texto con el nombre del tipo forma pago</param>
        /// <param name="pDescripcion">Cadena de texto con la descripción del tipo forma pago</param>
        /// <param name="pEstado">Booleano con el estado del tipo forma de pago</param>
        /// <returns>Listado de entidades TipoFormaPago con la información encontrada en la base de datos</returns>
        public List<TipoFormaPago> ConsultarTipoMedioPagos(String pCodigoTipoFormaPago, String pNombreTipoFormaPago, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoFormaPagoDAL.ConsultarTipoMedioPagos(pCodigoTipoFormaPago, pNombreTipoFormaPago, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int InsertarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoDAL.InsertarTipoFormaPago(pTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Tipo

        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int ModificarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoDAL.ModificarTipoFormaPago(pTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pTipoFormaPago"></param>
        /// <returns></returns>
        public int EliminarTipoFormaPago(TipoFormaPago pTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoDAL.EliminarTipoFormaPago(pTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pIdTipoFormaPago"></param>
        /// <returns></returns>
        public TipoFormaPago ConsultarTipoFormaPago(int pIdTipoFormaPago)
        {
            try
            {
                return vTipoFormaPagoDAL.ConsultarTipoFormaPago(pIdTipoFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TipoFormaPago
        /// </summary>
        /// <param name="pNombreTipoFormaPago"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoFormaPago> ConsultarTipoFormaPagos(String pNombreTipoFormaPago, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoFormaPagoDAL.ConsultarTipoFormaPagos(pNombreTipoFormaPago, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
