﻿using ClosedXML.Excel;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class CargueArchivosBLL
    {
        private const int PageSize = 117;

        private const string FinArchivo = "999999";

        private ArchivoDAL _ArchivosDAL;

        public CargueArchivosBLL()
        {
            _ArchivosDAL = new ArchivoDAL();
        }

        public CargueContratoMasivo ValidarArchivo(Stream fileContent, string usuario)
        {
            int fila = 2;
            int columna = 1;

            CargueContratoMasivo result = new CargueContratoMasivo();
            List<itemCargueArchivo> RegistrosValidadosList = new List<itemCargueArchivo>();
            List<itemCargueError> RegistrosErradosList = new List<itemCargueError>();

            using (var fileTemplate = new XLWorkbook(fileContent))
            {
                IXLWorksheet pestanaInit = fileTemplate.Worksheets.First();

                string xy = string.Empty;

                bool bandera = true;

                while (bandera)
                {
                    string valor = pestanaInit.Cell(fila, columna).Value.ToString();

                    if (columna == 1 && valor == FinArchivo)
                        bandera = false;

                    if (bandera)
                    {
                        var itemResult = ValidarRegistro(pestanaInit, fila);

                        if (!string.IsNullOrEmpty(itemResult.Key))
                        {
                            result.RegistrosErrados++;
                            itemCargueError miRegistroErrado = ObtenerRegistroErrado(fila, pestanaInit, itemResult.Key);
                            RegistrosErradosList.Add(miRegistroErrado);
                        }
                        else
                            RegistrosValidadosList.Add(itemResult.Value);
                    }

                    fila++;
                }

                Dictionary<int, string> registrosNoCargadosDB = _ArchivosDAL.CargarRegistros(RegistrosValidadosList, usuario);
                foreach (var itemValidaciones in registrosNoCargadosDB)
                {
                    result.RegistrosErrados++;
                    itemCargueError miRegistroErrado = ObtenerRegistroErrado(itemValidaciones.Key, pestanaInit, itemValidaciones.Value);
                    RegistrosErradosList.Add(miRegistroErrado);
                }

                result.RegistrosCargados = RegistrosValidadosList.Count - registrosNoCargadosDB.Count;
                result.TotalRegistros = fila - 3;
                result.ListaRegistroErrados = RegistrosErradosList;
            }

            return result;
        }

        private itemCargueError ObtenerRegistroErrado(int fila, IXLWorksheet celdasFila, string itemResult)
        {
            itemCargueError result = new itemCargueError();
            result.CodigoRegional = celdasFila.Cell("B"+fila).Value.ToString();
            result.Regional = celdasFila.Cell("A"+fila).Value.ToString();
            result.NumeroContrato = celdasFila.Cell("C"+fila).Value.ToString();
            result.Observaciones = itemResult;
            result.Fila = fila;
            return result;
        }

        private KeyValuePair<string,itemCargueArchivo> ValidarRegistro(IXLWorksheet pestana, int fila)
        {
            StringBuilder result = new StringBuilder();

            itemCargueArchivo itemValidar = new itemCargueArchivo();
            itemValidar.Fila = fila;
            itemValidar.CodigoRegional = pestana.Cell("B" + fila).Value.ToString();

            DateTime fechaSuscripcion;
            if (DateTime.TryParse(pestana.Cell("D" + fila).Value.ToString(), out fechaSuscripcion))
                itemValidar.FechaSuscripcion = fechaSuscripcion; 
            else
                result.AppendLine("FechaSuscripcion - Formato de Fecha de Suscripción Invalido.");

            if (fechaSuscripcion != null)
            {
                int numeroContrato;

                if (int.TryParse(pestana.Cell("C" + fila).Value.ToString(),out numeroContrato))
                {
                    itemValidar.ConsecutivoContrato = itemValidar.CodigoRegional.Trim() + "" +
                                                      numeroContrato.ToString().PadLeft(5, '0') + "" + 
                                                      fechaSuscripcion.Year.ToString().Trim();
                }
                else
                    result.AppendLine("No Cto. - Formato ConsecutivoContrato Invalido.");
            }
            else
                result.AppendLine("No Cto. -Falta de Vigencia ConsecutivoContrato Invalido.");

            itemValidar.CategoriaContrato  = pestana.Cell("E" + fila).Value.ToString();
            itemValidar.ModalidadSeleccion = pestana.Cell("F" + fila).Value.ToString();

            itemValidar.NumeroProceso = pestana.Cell("G" + fila).Value.ToString();

            string valorFechaAdjudicacionProceso = pestana.Cell("H" + fila).Value.ToString();

            if (! string.IsNullOrEmpty(valorFechaAdjudicacionProceso))
	        {
                DateTime fechaAdjudicacionProceso;
                if (DateTime.TryParse(valorFechaAdjudicacionProceso, out fechaAdjudicacionProceso))
                    itemValidar.FechaAdjudicacionProceso = fechaSuscripcion;
                else
                    result.AppendLine("FechaAdjudicacionProceso - Formato de Fecha de Adjudicacion Proceso Invalido.");
            }

            itemValidar.TipoContrato = pestana.Cell("I" + fila).Value.ToString();
            itemValidar.AreaSolicitante = pestana.Cell("J" + fila).Value.ToString();
            itemValidar.CedulaSolicitante = pestana.Cell("L" + fila).Value.ToString();
            itemValidar.TipoIdentificacionContratista = pestana.Cell("N" + fila).Value.ToString();
            itemValidar.NumeroIdentificacionContratista = pestana.Cell("O" + fila).Value.ToString();
            itemValidar.ModalidadAcademica = pestana.Cell("R" + fila).Value.ToString();
            itemValidar.Profesion = pestana.Cell("S" + fila).Value.ToString();
            itemValidar.AfectacionRecurso = pestana.Cell("AS" + fila).Value.ToString();
            itemValidar.CodigoSecop = pestana.Cell("AT" + fila).Value.ToString();
            itemValidar.ObjetoContrato = pestana.Cell("AU" + fila).Value.ToString();
            itemValidar.LugarEjeccucion = pestana.Cell("AV" + fila).Value.ToString();

            DateTime fechaInicioEjecucion;
            if (DateTime.TryParse(pestana.Cell("AW" + fila).Value.ToString(), out fechaInicioEjecucion))
                itemValidar.FechaInicioEjecucion = fechaInicioEjecucion;
            else
                result.AppendLine("fechaInicioEjecucion - Formato de Fecha de Inicio de Ejecución Invalido.");

            DateTime fechaFinEjecucion;
            if (DateTime.TryParse(pestana.Cell("AX" + fila).Value.ToString(), out fechaFinEjecucion))
                itemValidar.FechaFinEjecuccion = fechaFinEjecucion;
            else
                result.AppendLine("FechaFinEjecuccion - Formato de Fecha de Fin de Ejecución Invalido.");

            int consecutivoPlanCOmpras;
            if (int.TryParse(pestana.Cell("AZ" + fila).Value.ToString(), out consecutivoPlanCOmpras))
                itemValidar.ConsecutivoPlanCompras = consecutivoPlanCOmpras;
            else
                result.AppendLine("Consecutivo PACCO - Formato Consecutivo PACCO Invalido.");

            int nroCDP;
            if (int.TryParse(pestana.Cell("BA" + fila).Value.ToString(), out nroCDP))
                itemValidar.CDP = nroCDP;
            else
                result.AppendLine("CDP - Formato Consecutivo CDP Invalido.");

            int nroRP;
            if (int.TryParse(pestana.Cell("BD" + fila).Value.ToString(), out nroRP))
                itemValidar.RP = nroRP;
            else
                result.AppendLine("RP - Formato Consecutivo RP Invalido.");

            itemValidar.TipoPago = pestana.Cell("BJ" + fila).Value.ToString();

            itemValidar.Anticipo = pestana.Cell("BK" + fila).Value.ToString();

            string valorAnticipo = pestana.Cell("BL" + fila).Value.ToString();

            if (! string.IsNullOrEmpty(valorAnticipo))
            {
                decimal ValorAnticipoOut;
                if (decimal.TryParse(valorAnticipo, out ValorAnticipoOut))
                    itemValidar.ValorAnticipo = ValorAnticipoOut;
                else
                    result.AppendLine("ValorAnticipo - Formato Valor Anticipo de Contrato Invalido.");                
            }

            itemValidar.cofinaciacionEntidad1 = pestana.Cell("BM" + fila).Value.ToString();
            string cofinaciacionEntidad1Valor = pestana.Cell("BN" + fila).Value.ToString();
            
            if (! string.IsNullOrEmpty(cofinaciacionEntidad1Valor))
            {
                decimal ValorCofinanciacion1;
                if (decimal.TryParse(cofinaciacionEntidad1Valor, out ValorCofinanciacion1))
                    itemValidar.ValorCofinanciacion1 = ValorCofinanciacion1;
                else
                    result.AppendLine("ValorCofinanciacion1 - Formato Valor Incial de Contrato Invalido.");
            }


            itemValidar.cofinaciacionEntidad2 = pestana.Cell("BO" + fila).Value.ToString();
            string cofinaciacionEntidad2Valor = pestana.Cell("BP" + fila).Value.ToString();

            if (!string.IsNullOrEmpty(cofinaciacionEntidad2Valor))
            {
                decimal ValorCofinanciacion2;
                if (decimal.TryParse(cofinaciacionEntidad2Valor, out ValorCofinanciacion2))
                    itemValidar.ValorCofinanciacion2 = ValorCofinanciacion2;
                else
                    result.AppendLine("ValorCofinanciacion2 - Formato Valor Cofinanciación 2 Invalido.");
            }

            itemValidar.VigenciaFutura = pestana.Cell("BR" + fila).Value.ToString();
            string VigenciaFuturaValor = pestana.Cell("BS" + fila).Value.ToString();


            if (!string.IsNullOrEmpty(VigenciaFuturaValor))
            {
                decimal ValorVigenciaFutura;
                if (decimal.TryParse(VigenciaFuturaValor, out ValorVigenciaFutura))
                    itemValidar.ValorVigenciaFutura = ValorVigenciaFutura;
                else
                    result.AppendLine("ValorVigenciaFutura - Formato Valor Vigencia Futura Invalido.");
            }


            decimal ValorIncialContratoICBF;
            if (decimal.TryParse(pestana.Cell("BT" + fila).Value.ToString(), out ValorIncialContratoICBF))
                itemValidar.ValorIncialContrato = ValorIncialContratoICBF;
            else
                result.AppendLine("ValorIncialContratoICBF - Formato Valor Incial de Contrato ICBF Invalido.");

            DateTime fechaGarantia;
            if (DateTime.TryParse(pestana.Cell("BV" + fila).Value.ToString(), out fechaGarantia))
                itemValidar.FechaGarantia = fechaGarantia;
            else
                result.AppendLine("FechaGarantia - Formato de Fecha de Inicio de Ejecución Invalido.");

            itemValidar.TipoGarantia = pestana.Cell("BW" + fila).Value.ToString();
            itemValidar.Aseguradora = pestana.Cell("BX" + fila).Value.ToString();
            itemValidar.RiesgosGarantias = pestana.Cell("BY" + fila).Value.ToString();
            itemValidar.PorcentajeGarantia = pestana.Cell("BZ" + fila).Value.ToString().Replace("%",string.Empty);

            decimal ValorAseguradoGarantia;
            if (decimal.TryParse(pestana.Cell("CA" + fila).Value.ToString(), out ValorAseguradoGarantia))
                itemValidar.ValorAseguradoGarantia = ValorAseguradoGarantia;
            else
                result.AppendLine("ValorAseguradoGarantia - Formato Valor Asegurado de Garantía Invalido.");

            itemValidar.AreaSupervisor = pestana.Cell("CB" + fila).Value.ToString().Trim();
            itemValidar.CargoSupervisor = pestana.Cell("CC" + fila).Value.ToString().Trim();
            itemValidar.IdentificacionSupervisor = pestana.Cell("CE" + fila).Value.ToString().Trim();

            itemValidar.CargoOrdenador = pestana.Cell("CI" + fila).Value.ToString().Trim();
            itemValidar.IdentificacionOrdenador = pestana.Cell("CJ" + fila).Value.ToString().Trim();

            string FechaProrroga1 = pestana.Cell("CK" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaProrroga1))
            {
                DateTime FechaProrroga1Out;
                if (DateTime.TryParse(FechaProrroga1, out FechaProrroga1Out))
                    itemValidar.FechaProrroga1 = FechaProrroga1Out;
                else
                    result.AppendLine("FechaProrroga1 - Formato de Fecha de Prorroga Proceso Invalido.");
            }

            string FechaProrroga2 = pestana.Cell("CM" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaProrroga2))
            {
                DateTime FechaProrroga2Out;
                if (DateTime.TryParse(FechaProrroga2, out FechaProrroga2Out))
                    itemValidar.FechaProrroga2 = FechaProrroga2Out;
                else
                    result.AppendLine("FechaProrroga2 - Formato de Fecha de Prorroga Proceso Invalido.");
            }

            string FechaProrroga3 = pestana.Cell("CO" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaProrroga3))
            {
                DateTime FechaProrroga3Out;
                if (DateTime.TryParse(FechaProrroga3, out FechaProrroga3Out))
                    itemValidar.FechaProrroga3 = FechaProrroga3Out;
                else
                    result.AppendLine("FechaProrroga3 - Formato de Fecha de Prorroga Proceso Invalido.");
            }

            string diasTerminacionAnticipada = pestana.Cell("CQ" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(diasTerminacionAnticipada))
            {
                int diasTerminacionOut;
                if (int.TryParse(diasTerminacionAnticipada, out diasTerminacionOut))
                    itemValidar.DiasTerminacionAnticipada = diasTerminacionOut;
                else
                    result.AppendLine("diasTerminacionAnticipada - Días de terminacion anticipada Proceso Invalido.");
            }

            DateTime fechaTerminacionFinal;
            if (DateTime.TryParse(pestana.Cell("CT" + fila).Value.ToString(), out fechaTerminacionFinal))
                itemValidar.fechaTerminacionFinal = fechaTerminacionFinal;
            else
                result.AppendLine("fechaTerminacionFinal - Formato de Fecha de Terminación Final  Invalido.");

            string FechaAdicion1 = pestana.Cell("CU" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaAdicion1))
            {
                DateTime FechaAdicion1Out;
                if (DateTime.TryParse(FechaAdicion1, out FechaAdicion1Out))
                    itemValidar.FechaAdicion1 = FechaAdicion1Out;
                else
                    result.AppendLine("FechaAdicion1 - Formato de Fecha de Adicion Invalido.");
            }
            string RPAdicion1 = pestana.Cell("CV" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(RPAdicion1))
            {
                int RPAdicion1Out;
                if (int.TryParse(RPAdicion1, out RPAdicion1Out))
                    itemValidar.RPAdicion1 = RPAdicion1Out;
                else
                    result.AppendLine("RPAdicion1 - Formato de RP de Adicion Invalido.");
            }
            string ValorAdicion1 = pestana.Cell("CW" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(ValorAdicion1))
            {
                decimal ValorAdicion1Out;
                if (decimal.TryParse(ValorAdicion1, out ValorAdicion1Out))
                    itemValidar.ValorAdicion1 = ValorAdicion1Out;
                else
                    result.AppendLine("ValorAdicion1 - Formato del Valor de Adicion Invalido.");
            }

            string FechaAdicion2 = pestana.Cell("CX" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaAdicion2))
            {
                DateTime FechaAdicion2Out;
                if (DateTime.TryParse(FechaAdicion2, out FechaAdicion2Out))
                    itemValidar.FechaAdicion2 = FechaAdicion2Out;
                else
                    result.AppendLine("FechaAdicion2 - Formato de Fecha de Adicion Invalido.");
            }
            string RPAdicion2 = pestana.Cell("CY" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(RPAdicion2))
            {
                int RPAdicion2Out;
                if (int.TryParse(RPAdicion2, out RPAdicion2Out))
                    itemValidar.RPAdicion2 = RPAdicion2Out;
                else
                    result.AppendLine("RPAdicion2 - Formato de RP de Adicion Invalido.");
            }
            string ValorAdicion2 = pestana.Cell("CZ" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(ValorAdicion2))
            {
                decimal ValorAdicion2Out;
                if (decimal.TryParse(ValorAdicion2, out ValorAdicion2Out))
                    itemValidar.ValorAdicion2 = ValorAdicion2Out;
                else
                    result.AppendLine("ValorAdicion2 - Formato del Valor de Adicion Invalido.");
            }


            string FechaAdicion3 = pestana.Cell("DA" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaAdicion3))
            {
                DateTime FechaAdicion3Out;
                if (DateTime.TryParse(FechaAdicion3, out FechaAdicion3Out))
                    itemValidar.FechaAdicion3 = FechaAdicion3Out;
                else
                    result.AppendLine("FechaAdicion3 - Formato de Fecha de Adicion Invalido.");
            }
            string RPAdicion3 = pestana.Cell("DB" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(RPAdicion3))
            {
                int RPAdicion3Out;
                if (int.TryParse(RPAdicion3, out RPAdicion3Out))
                    itemValidar.RPAdicion3 = RPAdicion3Out;
                else
                    result.AppendLine("RPAdicion3 - Formato de RP de Adicion Invalido.");
            }
            string ValorAdicion3 = pestana.Cell("DC" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(ValorAdicion3))
            {
                decimal ValorAdicion3Out;
                if (decimal.TryParse(ValorAdicion3, out ValorAdicion3Out))
                    itemValidar.ValorAdicion3 = ValorAdicion3Out;
                else
                    result.AppendLine("ValorAdicion3 - Formato del Valor de Adicion Invalido.");
            }

            string FechaAdicion4 = pestana.Cell("DD" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaAdicion4))
            {
                DateTime FechaAdicion4Out;
                if (DateTime.TryParse(FechaAdicion4, out FechaAdicion4Out))
                    itemValidar.FechaAdicion4 = FechaAdicion4Out;
                else
                    result.AppendLine("FechaAdicion4 - Formato de Fecha de Adicion Invalido.");
            }
            string RPAdicion4 = pestana.Cell("DE" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(RPAdicion4))
            {
                int RPAdicion4Out;
                if (int.TryParse(RPAdicion4, out RPAdicion4Out))
                    itemValidar.RPAdicion4 = RPAdicion4Out;
                else
                    result.AppendLine("RPAdicion4 - Formato de RP de Adicion Invalido.");
            }
            string ValorAdicion4 = pestana.Cell("DF" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(ValorAdicion4))
            {
                decimal ValorAdicion4Out;
                if (decimal.TryParse(ValorAdicion4, out ValorAdicion4Out))
                    itemValidar.ValorAdicion4 = ValorAdicion4Out;
                else
                    result.AppendLine("ValorAdicion4 - Formato del Valor de Adicion Invalido.");
            }


            string ValorDisminucion = pestana.Cell("DH" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(ValorDisminucion))
            {
                decimal ValorDisminucionOut;
                if (decimal.TryParse(ValorDisminucion, out ValorDisminucionOut))
                    itemValidar.ValorDisminucion = ValorDisminucionOut;
                else
                    result.AppendLine("ValorDisminucion - Formato del Valor de Adicion Invalido.");
            }

            decimal ValorFinalContrato;
            if (decimal.TryParse(pestana.Cell("DJ" + fila).Value.ToString(), out ValorFinalContrato))
                itemValidar.ValorFinalContrato = ValorFinalContrato;
            else
                result.AppendLine("ValorFinalContrato - Formato del Valor Final del Contrato Invalido.");

            string FechaLiquidacion = pestana.Cell("DK" + fila).Value.ToString();
            if (!string.IsNullOrEmpty(FechaLiquidacion))
            {
                DateTime FechaLiquidacionOut;
                if (DateTime.TryParse(FechaLiquidacion, out FechaLiquidacionOut))
                    itemValidar.FechaLiquidacion = FechaLiquidacionOut;
                else
                    result.AppendLine("FechaLiquidacion - Formato de Fecha de Liquidacion Invalido.");
            }

            string estado = pestana.Cell("DL" + fila).Value.ToString();

            switch (estado)
            {
                case "LIQUIDADO":
                    estado = "LIQB";
                    break;
                case "VIGENTE":
                    estado = "EJE";
                    break;
                case "TERMINADO":
                    estado = "TER";
                    break;
                default:
                    result.AppendLine("EstadoContrato - El formato del estado del cotnrato es invalido.");
                    break;
            }

            itemValidar.Estado = estado;

            return new KeyValuePair<string,itemCargueArchivo>(result.ToString(),itemValidar);
        }

        public string GenerarArchivoValidaciones(List<itemCargueError> RegistrosErradosList, string pathPlantilla)
        {
            string result = string.Empty;

            var fileResult = new XLWorkbook();

            var fileTemplate = new XLWorkbook(pathPlantilla);

            IXLWorksheet varF1 = GenerarValidaciones(RegistrosErradosList, fileTemplate);

            fileResult.AddWorksheet(varF1);

            var id = Guid.NewGuid().ToString() + ".xlsx";
            var pathResult = System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"];
            fileResult.SaveAs(Path.Combine(pathResult, id));
            result = id;

            return result;
        }

        private IXLWorksheet GenerarValidaciones(List<itemCargueError> RegistrosErradosList, XLWorkbook fileTemplate)
        {
            IXLWorksheet pestanaInit = null;

            pestanaInit = fileTemplate.Worksheets.First();

            if (RegistrosErradosList != null && RegistrosErradosList.Count > 0)
            {
                int x = 2;
                string xy = string.Empty;

                foreach (var itemFila in RegistrosErradosList)
                {
                    xy = "A" + x;
                    pestanaInit.Cell(xy).Value = itemFila.Fila;                    
                    xy = "B" + x;
                    pestanaInit.Cell(xy).Value = itemFila.Regional;
                    xy = "C" + x;
                    pestanaInit.Cell(xy).Value = itemFila.NumeroContrato;
                    xy = "D" + x;
                    pestanaInit.Cell(xy).Value = itemFila.Observaciones;
                       
                    x++;
                }
            }

            return pestanaInit;

        
        }

    }
}
