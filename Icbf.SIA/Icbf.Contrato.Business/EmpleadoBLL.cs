﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Empleado
    /// </summary>
    public class EmpleadoBLL
    {
        private EmpleadoDAL vEmpleadoDAL;
        public EmpleadoBLL()
        {
            vEmpleadoDAL = new EmpleadoDAL();
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad Empleado
        /// </summary>
        /// <param name="pIdTipoIdentificacion">Cadena de texto con el identificador del tipo de identificación</param>
        /// <param name="pNumeroIdentificacion">Cadena de texto con el numero de identificación</param>
        /// <param name="pIdTipoVinculacionContractual">Cadena de texto con la descripcion del tipo de vinculación que es el mismo id</param>
        /// <param name="pIdRegional">Cadena de texto con el codigo de la regional</param>
        /// <param name="pPrimerNombre">Cadena de texto con el primer nombre del empleado</param>
        /// <param name="pSegundoNombre">Cadena de texto con el segundo nombre del empleado</param>
        /// <param name="pPrimerApellido">Cadena de texto con el primer apellido del empleado</param>
        /// <param name="pSegundoApellido">Cadena de texto con el segundo apellido del empleado</param>
        /// <returns>Lista con los empleados que coinciden con los filtros</returns>
        public List<Empleado> ConsultarEmpleados(String pIdTipoIdentificacion, String pNumeroIdentificacion, String pIdTipoVinculacionContractual,
            String pIdRegional, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                return vEmpleadoDAL.ConsultarEmpleados(pIdTipoIdentificacion, pNumeroIdentificacion, pIdTipoVinculacionContractual,
                    pIdRegional, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros de la tabla [KACTUS].[KPRODII].[dbo].[nm_tnomi]
        /// que contiene los tipos de vinculación contractual
        /// </summary>
        /// <param name="pIdTipoVinCont">Cadena de texto con el identificador numérico del tipo de vinculación</param>
        /// <param name="pTipoVincCont">Cadena de texto con la descripción del tipo de vinculación</param>
        /// <returns>Lista con los tipos de vinculación contractual encapsulados en entidades tipo empleado</returns>
        public List<Empleado> ConsultarTiposVinculacionContractual(String pIdTipoVinCont, String pTipoVincCont)
        {
            try
            {
                return vEmpleadoDAL.ConsultarTiposVinculacionContractual(pIdTipoVinCont, pTipoVincCont);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Consulta la información del empleado en Kactus a partir de los filtros dados
        /// </summary>
        /// <param name="pNumeroIdentificacion">Cadena de texto con el numero de identificacion</param>
        /// <param name="pRegional">Cadena de texto con el identificador de la regional</param>
        /// <param name="pDependencia">Cadena de texto con el identificador de la dependencia</param>
        /// <param name="pCargo">Cadena de texto con el identificador del cargo</param>
        /// <returns>Entidad Empleado con la información obtenida de la base de datos</returns>
        public Empleado ConsultarEmpleado(String pNumeroIdentificacion, String pRegional, String pDependencia, String pCargo)
        {
            try
            {
                return vEmpleadoDAL.ConsultarEmpleado(pNumeroIdentificacion, pRegional, pDependencia, pCargo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<string, string> ConsultarDependencias(string codigo, int cantidad)
        {
            try
            {
                return vEmpleadoDAL.ConsultarDependencias(codigo, cantidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
