using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  NumeroProcesos
    /// </summary>  
    public class NumeroProcesosBLL
    {
        private NumeroProcesosDAL vNumeroProcesosDAL;
        public NumeroProcesosBLL()
        {
            vNumeroProcesosDAL = new NumeroProcesosDAL();
        }
        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int InsertarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                return vNumeroProcesosDAL.InsertarNumeroProcesos(pNumeroProcesos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de modificación para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int ModificarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                return vNumeroProcesosDAL.ModificarNumeroProcesos(pNumeroProcesos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de eliminación para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProcesos">instancia que contiene la información del numero de procesos</param>
        public int EliminarNumeroProcesos(NumeroProcesos pNumeroProcesos)
        {
            try
            {
                return vNumeroProcesosDAL.EliminarNumeroProcesos(pNumeroProcesos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por id para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pIdNumeroProceso">Valor entero con el Id del numero del proceso</param>
        public NumeroProcesos ConsultarNumeroProcesos(int pIdNumeroProceso)
        {
            try
            {
                return vNumeroProcesosDAL.ConsultarNumeroProcesos(pIdNumeroProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por filtros para la entidad NumeroProcesos
        /// Fecha: 08/05/2014
        /// </summary>
        /// <param name="pNumeroProceso">Valor entero con el número del proceso</param>
        /// <param name="pInactivo">Valor Booleano del estado del registro</param>
        /// <param name="pNumeroProcesoGenerado">Cadena de texto con el numero de proceso generado</param>
        /// <param name="pIdModalidadSeleccion">Valor entero con el Id de la modalidad de selección</param>
        /// <param name="pIdRegional">Valor entero con el Id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el Id de la vigencia</param>
        public List<NumeroProcesos> ConsultarNumeroProcesoss(int? pIdNumeroProceso, int? pNumeroProceso, Boolean? pInactivo, String pNumeroProcesoGenerado, int? pIdModalidadSeleccion, int? pIdRegional, int? pIdVigencia)
        {
            try
            {
                return vNumeroProcesosDAL.ConsultarNumeroProcesoss(pIdNumeroProceso,pNumeroProceso, pInactivo, pNumeroProcesoGenerado, pIdModalidadSeleccion, pIdRegional, pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Método de consulta por id NumeroProcesos opr cada contrato
        /// Fecha: 21/05/2014
        /// </summary>
        /// <param name="pIdNumeroProceso">Valor entero con el Id del numero del proceso</param>
        public bool ConsultarNumeroProcesosPorContrato(int pIdNumeroProceso)
        {
            try
            {
                return vNumeroProcesosDAL.ConsultarNumeroProcesosPorContrato(pIdNumeroProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
