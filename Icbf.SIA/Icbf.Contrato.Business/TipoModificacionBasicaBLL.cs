using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class TipoModificacionBasicaBLL
    {
        private TipoModificacionBasicaDAL vTipoModificacionBasicaDAL;
        public TipoModificacionBasicaBLL()
        {
            vTipoModificacionBasicaDAL = new TipoModificacionBasicaDAL();
        }
        public int InsertarTipoModificacionBasica(TipoModificacionBasica pTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionBasicaDAL.InsertarTipoModificacionBasica(pTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarTipoModificacionBasica(TipoModificacionBasica pTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionBasicaDAL.ModificarTipoModificacionBasica(pTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarTipoModificacionBasica(TipoModificacionBasica pTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionBasicaDAL.EliminarTipoModificacionBasica(pTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoModificacionBasica ConsultarTipoModificacionBasica(int pIdTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionBasicaDAL.ConsultarTipoModificacionBasica(pIdTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacionBasica> ConsultarTipoModificacionBasicas(int? pIdTipoModificacionBasica, String pDescripcion, Boolean? pRequiereModificacion, Boolean? pEstado)
        {
            try
            {
                return vTipoModificacionBasicaDAL.ConsultarTipoModificacionBasicas(pIdTipoModificacionBasica, pDescripcion, pRequiereModificacion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
