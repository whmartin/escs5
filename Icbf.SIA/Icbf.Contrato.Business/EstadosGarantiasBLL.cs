using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  EstadosGarantias
    /// </summary>
    public class EstadosGarantiasBLL
    {
        private EstadosGarantiasDAL vEstadosGarantiasDAL;
        public EstadosGarantiasBLL()
        {
            vEstadosGarantiasDAL = new EstadosGarantiasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int InsertarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasDAL.InsertarEstadosGarantias(pEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int ModificarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasDAL.ModificarEstadosGarantias(pEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pEstadosGarantias"></param>
        public int EliminarEstadosGarantias(EstadosGarantias pEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasDAL.EliminarEstadosGarantias(pEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pIDEstadosGarantias"></param>
        public EstadosGarantias ConsultarEstadosGarantias(int pIDEstadosGarantias)
        {
            try
            {
                return vEstadosGarantiasDAL.ConsultarEstadosGarantias(pIDEstadosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadosGarantias
        /// </summary>
        /// <param name="pCodigoEstadoGarantia"></param>
        /// <param name="pDescripcionEstadoGarantia"></param>
        public List<EstadosGarantias> ConsultarEstadosGarantiass(String pCodigoEstadoGarantia, String pDescripcionEstadoGarantia)
        {
            try
            {
                return vEstadosGarantiasDAL.ConsultarEstadosGarantiass(pCodigoEstadoGarantia, pDescripcionEstadoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void EnviarAprobacionGarantia(int idGarantia, string estado)
        {
            try
            {
                vEstadosGarantiasDAL.EnviarAprobacionGarantia(idGarantia, estado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <returns></returns>
        public List<string> ValidarAprobacionGarantia(int idGarantia)
        {
            try
            {
                return vEstadosGarantiasDAL.ValidarAprobacionGarantia(idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
