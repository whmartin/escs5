using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ContratistaGarantias
    /// </summary>
    public class ContratistaGarantiasBLL
    {
        private ContratistaGarantiasDAL vContratistaGarantiasDAL;
        public ContratistaGarantiasBLL()
        {
            vContratistaGarantiasDAL = new ContratistaGarantiasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int InsertarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                return vContratistaGarantiasDAL.InsertarContratistaGarantias(pContratistaGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int ModificarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                return vContratistaGarantiasDAL.ModificarContratistaGarantias(pContratistaGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pContratistaGarantias"></param>
        public int EliminarContratistaGarantias(ContratistaGarantias pContratistaGarantias)
        {
            try
            {
                return vContratistaGarantiasDAL.EliminarContratistaGarantias(pContratistaGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int EliminarContratistaGarantias(int idGarantia)
        {
            try
            {
                return vContratistaGarantiasDAL.EliminarContratistaGarantias(idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pIDContratista_Garantias"></param>
        public ContratistaGarantias ConsultarContratistaGarantias(int pIDContratista_Garantias)
        {
            try
            {
                return vContratistaGarantiasDAL.ConsultarContratistaGarantias(pIDContratista_Garantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ContratistaGarantias
        /// </summary>
        /// <param name="pIDGarantia"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        public List<ContratistaGarantias> ConsultarContratistaGarantiass(int? pIDGarantia, int? pIDEntidadProvOferente)
        {
            try
            {
                return vContratistaGarantiasDAL.ConsultarContratistaGarantiass(pIDGarantia, pIDEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
