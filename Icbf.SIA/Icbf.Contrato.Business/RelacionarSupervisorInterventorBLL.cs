using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad SupervisorInterventor
    /// </summary>
    public class RelacionarSupervisorInterventorBLL
    {
        private RelacionarSupervisorInterventorDAL vRelacionarSupervisorInterventorDAL;
        public RelacionarSupervisorInterventorBLL()
        {
            vRelacionarSupervisorInterventorDAL = new RelacionarSupervisorInterventorDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int InsertarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorDAL.InsertarRelacionarSupervisorInterventor(pRelacionarSupervisorInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int ModificarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorDAL.ModificarRelacionarSupervisorInterventor(pRelacionarSupervisorInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pRelacionarSupervisorInterventor"></param>
        /// <returns></returns>
        public int EliminarRelacionarSupervisorInterventor(RelacionarSupervisorInterventor pRelacionarSupervisorInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorDAL.EliminarRelacionarSupervisorInterventor(pRelacionarSupervisorInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pIDSupervisorInterv"></param>
        /// <returns></returns>
        public RelacionarSupervisorInterventor ConsultarRelacionarSupervisorInterventor(int pIDSupervisorInterv)
        {
            try
            {
                return vRelacionarSupervisorInterventorDAL.ConsultarRelacionarSupervisorInterventor(pIDSupervisorInterv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RelacionarSupervisorInterventor
        /// </summary>
        /// <param name="pOrigenTipoSupervisor"></param>
        /// <param name="pIDTipoSupvInterventor"></param>
        /// <param name="pIDTerceroExterno"></param>
        /// <param name="pTipoVinculacion"></param>
        /// <param name="pFechaInicia"></param>
        /// <param name="pFechaFinaliza"></param>
        /// <param name="pEstado"></param>
        /// <param name="pFechaModificaInterventor"></param>
        /// <returns></returns>
        public List<RelacionarSupervisorInterventor> ConsultarRelacionarSupervisorInterventors(bool pOrigenTipoSupervisor, int? pIDTipoSupvInterventor, int? pIDTerceroExterno, bool pTipoVinculacion, DateTime? pFechaInicia, DateTime? pFechaFinaliza, bool pEstado, DateTime? pFechaModificaInterventor)
        {
            try
            {
                return vRelacionarSupervisorInterventorDAL.ConsultarRelacionarSupervisorInterventors(pOrigenTipoSupervisor, pIDTipoSupvInterventor, pIDTerceroExterno, pTipoVinculacion, pFechaInicia, pFechaFinaliza, pEstado, pFechaModificaInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
