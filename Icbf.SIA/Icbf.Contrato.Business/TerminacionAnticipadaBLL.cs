﻿using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class TerminacionAnticipadaBLL
    {
        private TerminacionAnticipadaDAL _terminacionAnticiapadaDAL;

        public TerminacionAnticipadaBLL()
        {
            _terminacionAnticiapadaDAL = new TerminacionAnticipadaDAL();
        }

        public int InsertarTerminacionAnticipada(TerminacionAnticipada pTerminacionAnticipada)
        {
            try
            {
                return _terminacionAnticiapadaDAL.InsertarTerminacionAnticipada(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTerminacionAnticipada(TerminacionAnticipada pTerminacionAnticipada)
        {
            try
            {
               return _terminacionAnticiapadaDAL.ModificarTerminacionAnticipada(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaContrato(int IdContrato)
        {
            try
            {
                return _terminacionAnticiapadaDAL.ConsultarTerminacionAnticipadaContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaId(int IdTerminacionAnticipada)
        {
            try
            {
                return _terminacionAnticiapadaDAL.ConsultarTerminacionAnticipadaId(IdTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TerminacionAnticipada> ConsultarTerminacionAnticipadaIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                return _terminacionAnticiapadaDAL.ConsultarTerminacionAnticipadaIdDetalle(IdDetConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
