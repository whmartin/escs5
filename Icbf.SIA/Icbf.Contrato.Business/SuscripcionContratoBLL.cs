﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class SuscripcionContratoBLL
    {
        private SuscripcionContratoDAL vSuscripcionContratoDAL;
        public SuscripcionContratoBLL()
        {
            vSuscripcionContratoDAL = new SuscripcionContratoDAL();
        }

        /// <summary>
        /// Método de inserción para la entidad ArchivoContrato
        /// </summary>
        /// <param name="pArchivoContrato"></param>
        /// <returns></returns>
        public int InsertarArchivoContrato(ArchivoContrato pArchivoContrato)
        {
            try
            {
                return vSuscripcionContratoDAL.InsertarArchivoContrato(pArchivoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Archivo sólo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdContrato">Valor entero Id Contrato</param>
        /// <returns>Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivo(int pIdContrato)
        {
            try
            {
                return vSuscripcionContratoDAL.ConsultarArchivo(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Permite eliminar un documento Archivo Contrato
        ///     -Genera auditoria
        /// 26-Jun-2013
        /// </summary>
        /// <param name="pArchivoContrato">Instancia que contiene la información del documento  
        /// a eliminar</param>
        /// <returns>Entero con el resultado de la operación de actualización, en la base de datos</returns>
        public int EliminarArchivoContrato(ArchivoContrato pArchivoContrato)
        {
            try
            {
                return vSuscripcionContratoDAL.EliminarArchivoContrato(pArchivoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="vFechaSuscripcion"></param>
        /// <param name="vIdEstadoContrato"></param>
        public int SuscribirContrato(int pIdContrato, string pUsuarioModifica, DateTime vFechaSuscripcion, int vIdEstadoContrato, 
                                     string consecutivoContrato, DateTime ? fechaFinalizacion, string vinculoSECOP)
        {
            try
            {
                return vSuscripcionContratoDAL.SuscribirContrato(pIdContrato, pUsuarioModifica, vFechaSuscripcion, vIdEstadoContrato, consecutivoContrato, fechaFinalizacion, vinculoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para actualizar el numero de contrato de la entidad contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="pNumerContrato"></param>
        public int ActualizarNumeroContrato(int pIdContrato, string pUsuarioModifica, string pNumerContrato, string pConsecutivoSuscrito)
        {
            try
            {
                return vSuscripcionContratoDAL.ActualizarNumeroContrato(pIdContrato, pUsuarioModifica, pNumerContrato, pConsecutivoSuscrito);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar Archivo sólo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdVigencia">Valor entero Id Contrato</param>
        /// <param name="pIdRegional">Valor entero Id Regional</param>
        /// <returns>Entity Archivo</returns>
        public List<ConsecutivoContratoRegionales> ConsultarConsecutivoContratoRegionales(int pIdVigencia,
                                                                                          int pIdRegional)
        {
            try
            {
                return vSuscripcionContratoDAL.ConsultarConsecutivoContratoRegionales(pIdVigencia,pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
