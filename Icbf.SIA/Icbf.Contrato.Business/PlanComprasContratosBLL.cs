using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  PlanComprasContratos
    /// </summary>
    public class PlanComprasContratosBll
    {
        private readonly PlanComprasContratosDAL _vPlanComprasContratosDal;
        public PlanComprasContratosBll()
        {
            _vPlanComprasContratosDal = new PlanComprasContratosDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int InsertarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return _vPlanComprasContratosDal.InsertarPlanComprasContratos(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int ModificarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return _vPlanComprasContratosDal.ModificarPlanComprasContratos(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int EliminarPlanComprasContratos(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return _vPlanComprasContratosDal.EliminarPlanComprasContratos(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pPlanComprasContratos"></param>
        public int EliminarPlanComprasPrecontractual(PlanComprasContratos pPlanComprasContratos)
        {
            try
            {
                return _vPlanComprasContratosDal.EliminarPlanComprasPreContractual(pPlanComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasContratos ConsultarPlanComprasContratos(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                return _vPlanComprasContratosDal.ConsultarPlanComprasContratos(pIdProductoPlanCompraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pVigencia"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdPlanDeComprasContratos"></param>
        /// <param name="pCodigoRegional"></param>
        public List<PlanComprasContratos> ConsultarPlanComprasContratoss(int? pVigencia,
                                                                         int? pIdContrato,
                                                                         int? pIdPlanDeComprasContratos,
                                                                         int? pIdUsuario)
        {
            try
            {
                return _vPlanComprasContratosDal.ConsultarPlanComprasContratoss(pVigencia, 
                                                                                pIdContrato, 
                                                                                pIdPlanDeComprasContratos,
                                                                                pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasContratos
        /// </summary>
        /// <param name="pIdValoresContrato"></param>
        public List<PlanComprasContratos> ConsultarValoresPlanComprasContratos(int? pIdValoresContrato)
        {
            try
            {
                return _vPlanComprasContratosDal.ConsultarValoresPlanComprasContratos(pIdValoresContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vigencia"></param>
        /// <param name="codigoRegional"></param>
        /// <param name="trimestre"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int ActualizarInformacionEjecucionContrato(int vigencia, string codigoRegional, ReporteContraloraTrimestre trimestre, string usuario)
        {
            try
            {
                return _vPlanComprasContratosDal.ActualizarInformacionEjecucionContrato(vigencia, codigoRegional, trimestre, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

    }
}
