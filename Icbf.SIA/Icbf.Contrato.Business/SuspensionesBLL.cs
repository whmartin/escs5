using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class SuspensionesBLL
    {
        private SuspensionesDAL vSuspensionesDAL;
        public SuspensionesBLL()
        {
            vSuspensionesDAL = new SuspensionesDAL();
        }
        public int InsertarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                return vSuspensionesDAL.InsertarSuspensiones(pSuspensiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                return vSuspensionesDAL.ModificarSuspensiones(pSuspensiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int AplicarSuspensiones(int pIdSuspension)
        {
            try
            {
                return vSuspensionesDAL.AplicarSuspensiones(pIdSuspension);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSuspensiones(Suspensiones pSuspensiones)
        {
            try
            {
                return vSuspensionesDAL.EliminarSuspensiones(pSuspensiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public Suspensiones Suspensiones_DetalleConsModContractual_Consultar(int pIdSuspension)
        {
            try
            {
                return vSuspensionesDAL.Suspensiones_DetalleConsModContractual_Consultar(pIdSuspension);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Suspensiones ConsultarSuspensiones(int pIdSuspension)
        {
            try
            {
                return vSuspensionesDAL.ConsultarSuspensiones(pIdSuspension);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones ConsultarCalculosSuspensiones(int pIdContrato, DateTime pFechaInicio, DateTime pFechaFin, int pValorConmutado)
        {
            try
            {
                return vSuspensionesDAL.ConsultarCalculosSuspensiones(pIdContrato, pFechaInicio,pFechaFin, pValorConmutado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Suspensiones> ConsultarSuspensioness(DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                return vSuspensionesDAL.ConsultarSuspensioness(pFechaInicio, pFechaFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Suspensiones> ConsultarSuspensionesPorDetalleModificacion(int? pDetalleConModificacion)
        {
            try
            {
                return vSuspensionesDAL.ConsultarSuspensionesPorDetalleModificacion(pDetalleConModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Suspensiones ConsultarSuspensionesPorDetalle(int? pDetalleConModificacion)
        {
            try
            {
                return vSuspensionesDAL.ConsultarSuspensionesPorDetalle(pDetalleConModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
