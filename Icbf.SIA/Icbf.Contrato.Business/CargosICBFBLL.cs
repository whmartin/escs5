using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de la capa de negocio que contiene los métodos para Insertar, Modificar, Eliminar y Consultar registros de la entidad  CargosICBFDAL
    /// </summary>
    public class CargosICBFBLL
    {
        private CargosICBFDAL vCargosICBFDAL;
        public CargosICBFBLL()
        {
            vCargosICBFDAL = new CargosICBFDAL();
        }
        /// <summary>
        /// Gonet
        /// Insertar Cargos ICBF 
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCargosICBF">Instancia que contiene la información del Cargo ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int InsertarCargosICBF(CargosICBF pCargosICBF)
        {
            try
            {
                return vCargosICBFDAL.InsertarCargosICBF(pCargosICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar Cargos ICBF 
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCargosICBF">Instancia que contiene la información del Cargo ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int ModificarCargosICBF(CargosICBF pCargosICBF)
        {
            try
            {
                return vCargosICBFDAL.ModificarCargosICBF(pCargosICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar Cargos ICBF 
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCargosICBF">Instancia que contiene la información del Cargo ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int EliminarCargosICBF(CargosICBF pCargosICBF)
        {
            try
            {
                return vCargosICBFDAL.EliminarCargosICBF(pCargosICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Cargos ICBF por Id del cargo
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pIdCargo">Valor entero con el Id del cargo</param>
        /// <returns>Instancia que contiene la información del archivo recuperada de la base de datos con el cargo ICBF</returns>
        public CargosICBF ConsultarCargosICBF(int pIdCargo)
        {
            try
            {
                return vCargosICBFDAL.ConsultarCargosICBF(pIdCargo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Cargos ICBF
        /// Fecha: 13/01/2014
        /// </summary>
        /// <param name="pCodCargo">Cadena de texto con el código del cargo</param>
        /// <param name="pNombre">Cadena de texto con el Nombre del cargo</param>
        /// <param name="pEstado">Booleano con el estado del cargo</param>
        /// <returns>Lista de la Instancia que contiene la información del archivo recuperada de la base de datos con los cargos ICBF</returns>
        public List<CargosICBF> ConsultarCargosICBFs(String pCodCargo, String pNombre, Boolean? pEstado)
        {
            try
            {
                return vCargosICBFDAL.ConsultarCargosICBFs(pCodCargo, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
