using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class PreguntaBLL
    {
        private PreguntaDAL vPreguntaDAL;
        public PreguntaBLL()
        {
            vPreguntaDAL = new PreguntaDAL();
        }
        public int InsertarPregunta(Pregunta pPregunta)
        {
            try
            {
                return vPreguntaDAL.InsertarPregunta(pPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarPregunta(Pregunta pPregunta)
        {
            try
            {
                return vPreguntaDAL.ModificarPregunta(pPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarPregunta(Pregunta pPregunta)
        {
            try
            {
                return vPreguntaDAL.EliminarPregunta(pPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Pregunta ConsultarPregunta(int pIdPregunta)
        {
            try
            {
                return vPreguntaDAL.ConsultarPregunta(pIdPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Pregunta> ConsultarPreguntas(String pDescripcionPregunta, int? pIdTipoContrato, int? pIdComponente, int? pIdSubComponente, int? pIdCategoriaContrato, int? pRequiereDocumento, int? pEstado)
        {
            try
            {
                return vPreguntaDAL.ConsultarPreguntas(pDescripcionPregunta, pIdTipoContrato, pIdComponente, pIdSubComponente, pIdCategoriaContrato, pRequiereDocumento, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
