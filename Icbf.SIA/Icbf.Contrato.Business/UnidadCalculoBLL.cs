using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  UnidadCalculo
    /// </summary>
    public class UnidadCalculoBLL
    {
        private UnidadCalculoDAL vUnidadCalculoDAL;
        public UnidadCalculoBLL()
        {
            vUnidadCalculoDAL = new UnidadCalculoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int InsertarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoDAL.InsertarUnidadCalculo(pUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int ModificarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoDAL.ModificarUnidadCalculo(pUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pUnidadCalculo"></param>
        public int EliminarUnidadCalculo(UnidadCalculo pUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoDAL.EliminarUnidadCalculo(pUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pIDUnidadCalculo"></param>
        public UnidadCalculo ConsultarUnidadCalculo(int pIDUnidadCalculo)
        {
            try
            {
                return vUnidadCalculoDAL.ConsultarUnidadCalculo(pIDUnidadCalculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad UnidadCalculo
        /// </summary>
        /// <param name="pCodUnidadCalculo"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pInactivo"></param>
        public List<UnidadCalculo> ConsultarUnidadCalculos(String pCodUnidadCalculo, String pDescripcion, Boolean? pInactivo)
        {
            try
            {
                return vUnidadCalculoDAL.ConsultarUnidadCalculos(pCodUnidadCalculo, pDescripcion, pInactivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
