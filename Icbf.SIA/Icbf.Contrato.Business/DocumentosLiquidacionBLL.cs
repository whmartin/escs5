﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class DocumentosLiquidacionBLL
    {
        private DocumentosLiquidacionDAL vDocumentosLiquidacionDAL;
        public DocumentosLiquidacionBLL()
        {
            vDocumentosLiquidacionDAL = new DocumentosLiquidacionDAL();
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar en la tabla Documento Liquidacion
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del documento </param>
        public int InsertarDocumentosLiquidacion(DocumentosLiquidacion pDocumentoLiquidacion)
        {
            try
            {
                return vDocumentosLiquidacionDAL.InsertarDocumentoLiquidacion(pDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar por id para la entidad NumeroProcesos
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int ModificarDocumentosLiquidacion(DocumentosLiquidacion pDocumentoLiquidacion)
        {
            try
            {
                return vDocumentosLiquidacionDAL.ModificarDocumentoLiquidacion(pDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar por id para la entidad NumeroProcesos
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int EliminarDocumentosLiquidacion(DocumentosLiquidacion pDocumentoLiquidacion)
        {
            try
            {
                return vDocumentosLiquidacionDAL.EliminarDocumentoLiquidacion(pDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de consulta por id para la entidad NumeroProcesos
        /// Fecha: 12/05/2016
        /// </summary>
        /// <param name="pIdDocumentoLiquidacion">Valor entero con el Id del Documento de Liquidacion</param>
        public DocumentosLiquidacion ConsultarDocumentosLiquidacion(int pIdDocumentoLiquidacion)
        {
            try
            {
                return vDocumentosLiquidacionDAL.ConsultarDocumentoLiquidacion(pIdDocumentoLiquidacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosLiquidacion> ConsultarVariosDocumentosLiquidacion(Boolean? dEstado, string dNombreDocumento)
        {
            try
            {
                return vDocumentosLiquidacionDAL.ConsultarVariosDocumentosLiquidacion(dEstado, dNombreDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
