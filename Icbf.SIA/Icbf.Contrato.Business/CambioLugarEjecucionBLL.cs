﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  LugarEjecucionContrato
    /// </summary>
    public class CambioLugarEjecucionBLL
    {
        private CambioLugarEjecucionDAL vLugarEjecucionContratoDAL;
        public CambioLugarEjecucionBLL()
        {
            vLugarEjecucionContratoDAL = new CambioLugarEjecucionDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int InsertarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.InsertarCambioLugarEjecucion(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int ModificarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ModificarCambioLugarEjecucion(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int EliminarCambioLugarEjecucion(CambioLugarEjecucion pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.EliminarCambioLugarEjecucion(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdLugarEjecucionContratos"></param>
        public int InsertarLugaresEjecucionActuales(int pIdContrato, int pIdDetalleConsModContractual , string pUsuarioCrea)
        {
            try
            {
                return vLugarEjecucionContratoDAL.InsertarLugaresEjecucionActuales(pIdContrato, pIdDetalleConsModContractual, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pNivelNacional"></param>
        //public List<LugarEjecucionContrato> ConsultarLugarEjecucionContratos(int? pIdContrato, int? pIdDepartamento, int? pIdRegional, Boolean? pNivelNacional)
        //{
        //    try
        //    {
        //        return vLugarEjecucionContratoDAL.ConsultarLugarEjecucionContratos(pIdContrato, pIdDepartamento, pIdRegional, pNivelNacional);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los lugares de ejecución asociados a un contrato
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado con los lugares obtenidos de la base de datos</returns>
        public List<CambioLugarEjecucion> ConsultarCambiosLugaresEjecucion(int pIdDetalleConsModContractual)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ConsultarCambiosdeLugaresEjecucion(pIdDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
