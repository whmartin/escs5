﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Contrato Asociado
    /// </summary>
    public class TipoContratoAsociadoBLL
    {
        private TipoContratoAsociadoDAL vTipoContratoAsociadoDAL;
        public TipoContratoAsociadoBLL()
        {
            vTipoContratoAsociadoDAL = new TipoContratoAsociadoDAL();
        }

        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo para obtener la información de la entidad Contrato Asociado a partir del identificador de tabla
        /// o el codigo del contrato asociado
        /// </summary>
        /// <param name="pContratoAsociado">Entidad con la información de identificador o código según la necesidad</param>
        /// <returns>Entidad con la información recuperada de la base de datos</returns>
        public TipoContratoAsociado IdentificadorCodigoTipoContratoAsociado(TipoContratoAsociado pTipoContratoAsociado)
        {
            try
            {
                return vTipoContratoAsociadoDAL.IdentificadorCodigoTipoContratoAsociado(pTipoContratoAsociado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
