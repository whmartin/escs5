﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Proveedores_Contratos
    /// </summary>
    public class ProveedoresContratosBLL
    {
        private ProveedoresContratosDAL vProveedoresContratosDAL;
        public ProveedoresContratosBLL()
        {
            vProveedoresContratosDAL = new ProveedoresContratosDAL();
        }

        /// <summary>
        /// Método de inserción para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedoresContratos"></param>
        public int InsertarProveedoresContratos(ProveedoresContratos pProveedoresContratos)
        {
            try
            {
                return vProveedoresContratosDAL.InsertarProveedoresContratos(pProveedoresContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<ProveedoresContratos> ConsultarProveedoresContratoss(int? pIdProveedores, int? pIdContrato)
        {
            try
            {
                return vProveedoresContratosDAL.ConsultarProveedoresContratoss(pIdProveedores, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos por forma de pago y id proveedor
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdFormapago"></param>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Boolean ConsultarProveedoresContratosFormaPago(int? pIdProveedores, int? pIdFormapago, int? pIdContrato)
        {
            try
            {
                return vProveedoresContratosDAL.ConsultarProveedoresContratosFormaPago(pIdProveedores, pIdFormapago, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
