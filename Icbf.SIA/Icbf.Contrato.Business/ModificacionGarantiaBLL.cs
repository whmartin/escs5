using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ModificacionGarantiaBLL
    {
        private ModificacionGarantiaDAL vModificacionGarantiaDAL;
        public ModificacionGarantiaBLL()
        {
            vModificacionGarantiaDAL = new ModificacionGarantiaDAL();
        }
        public int InsertarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaDAL.InsertarModificacionGarantia(pModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaDAL.ModificarModificacionGarantia(pModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarModificacionGarantia(ModificacionGarantia pModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaDAL.EliminarModificacionGarantia(pModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ModificacionGarantia ConsultarModificacionGarantia(int pIdModificacionGarantia)
        {
            try
            {
                return vModificacionGarantiaDAL.ConsultarModificacionGarantia(pIdModificacionGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModificacionGarantia> ConsultarModificacionGarantias(int? pIdContrato, int? pNumeroDocumento, DateTime? pFechaRegistro, String pTipoModificacion, String pNumeroGarantia, int? pIdAmparo, String pTipoAmparo, DateTime? pFechaVigenciaDesde, DateTime? pFechaVigenciaHasta, Decimal? pCalculoValorAsegurado, String pTipoCalculo, Decimal? pValorAdicion, Decimal? pValorTotalReduccion, Decimal? pValorAsegurado, String pObservacionesModificacion)
        {
            try
            {
                return vModificacionGarantiaDAL.ConsultarModificacionGarantias(pIdContrato, pNumeroDocumento, pFechaRegistro, pTipoModificacion, pNumeroGarantia, pIdAmparo, pTipoAmparo, pFechaVigenciaDesde, pFechaVigenciaHasta, pCalculoValorAsegurado, pTipoCalculo, pValorAdicion, pValorTotalReduccion, pValorAsegurado, pObservacionesModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
