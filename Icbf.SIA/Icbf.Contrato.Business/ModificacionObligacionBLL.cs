using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ModificacionObligacionBLL
    {
        private ModificacionObligacionDAL vModificacionObligacionDAL;
        public ModificacionObligacionBLL()
        {
            vModificacionObligacionDAL = new ModificacionObligacionDAL();
        }
        public int InsertarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                return vModificacionObligacionDAL.InsertarModificacionObligacion(pModificacionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                return vModificacionObligacionDAL.ModificarModificacionObligacion(pModificacionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarModificacionObligacion(ModificacionObligacion pModificacionObligacion)
        {
            try
            {
                return vModificacionObligacionDAL.EliminarModificacionObligacion(pModificacionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ModificacionObligacion ConsultarModificacionObligacion(int pIdModObligacion)
        {
            try
            {
                return vModificacionObligacionDAL.ConsultarModificacionObligacion(pIdModObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModificacionObligacion> ConsultarModificacionObligacions()
        {
            try
            {
                return vModificacionObligacionDAL.ConsultarModificacionObligacions();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<ModificacionObligacion> ConsultarModificacionObligacionXIDDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                return vModificacionObligacionDAL.ConsultarModificacionObligacionXIDDetalleConsModContractual (pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
