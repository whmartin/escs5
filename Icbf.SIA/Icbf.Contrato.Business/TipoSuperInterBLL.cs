﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  TipoSuperInter
    /// </summary>
    public class TipoSuperInterBLL
    {
        private TipoSuperInterDAL vTipoSuperInterDAL;
        public TipoSuperInterBLL()
        {
            vTipoSuperInterDAL = new TipoSuperInterDAL();
        }
        

        /// <summary>
        /// Método de consulta por filtros para la entidad TipoSuperInter
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public List<TipoSuperInter> ConsultarTipoSuperInters(String pCodigo, String pDescripcion)
        {
            try
            {
                return vTipoSuperInterDAL.ConsultarTipoSuperInters(pCodigo, pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

