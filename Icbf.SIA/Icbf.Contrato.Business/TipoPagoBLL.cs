﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{

    public class TipoPagoBLL
    {
        private TipoPagoDAL vTipoPagoDAL;
        public TipoPagoBLL()
        {
            vTipoPagoDAL = new TipoPagoDAL();
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar en la tabla TipoPago
        /// Fecha: 24/08/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago </param>
        public int InsertarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                return vTipoPagoDAL.InsertarTipoPago(pTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar por id el codigo en la tabla TipoPago
        /// Fecha: 24/08/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago </param>
        public int ModificarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                return vTipoPagoDAL.ModificarTipoPago(pTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar por id para la entidad TipoPago
        /// Fecha: 24/08/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago </param>
        public int EliminarTipoPago(TipoPago pTipoPago)
        {
            try
            {
                return vTipoPagoDAL.EliminarTipoPago(pTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de consulta por id para la entidad TipoPago
        /// Fecha: 24/08/2016
        /// </summary>
        /// <param name="pTipoPago">instancia que contiene la información del Tipo de Pago </param>
        public TipoPago ConsultarTipoPago(int pIdTipoPago)
        {
            try
            {
                return vTipoPagoDAL.ConsultarTipoPago(pIdTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoPago> ConsultarVariosTipoPago(Boolean? cEstado, string cNombreTipoPago)
        {
            try
            {
                return vTipoPagoDAL.ConsultarVariosTipoPago(cEstado, cNombreTipoPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
