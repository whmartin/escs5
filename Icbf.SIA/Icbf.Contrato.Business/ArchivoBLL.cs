using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.SIA.Business;
using Icbf.SIA.Entity;
using Icbf.Utilities.Exceptions;


namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para el registro de Archivos
    /// </summary>
    public class ArchivoBLL
    {
        private ArchivoDAL vArchivoDAL;
        public ArchivoBLL()
        {
            vArchivoDAL = new ArchivoDAL();
        }
        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int InsertarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.InsertarArchivo(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pArchivo"></param>
        /// <returns></returns>
        public int InsertarArchivoContrato(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.InsertarArchivoContrato(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarArchivoAnexo(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.InsertarArchivoAnexo(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo GCB
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int InsertarArchivoGCB(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.InsertarArchivoGCB(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Insertar Archivo nuevo retornando el Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Id Archivo</returns>
        public decimal InsertarArchivoReturID(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.InsertarArchivoReturID(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar Archivo 
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns>
        public int ModificarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.ModificarArchivo(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar Archivo 
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns> 
        public int EliminarDocumentoAnexo(decimal pIdArchivo)
        {
            try
            {
                return vArchivoDAL.EliminarDocumentoAnexo(pIdArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdArchivo"></param>
        /// <returns></returns>
        public int EliminarDocumentoAnexoContrato(decimal pIdArchivo)
        {
            try
            {
                return vArchivoDAL.EliminarDocumentoAnexoContrato(pIdArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar Archivo 
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <returns>int Resultado</returns> EliminarDocumentoAnexo
        public int EliminarArchivo(ArchivoContrato pArchivo)
        {
            try
            {
                return vArchivoDAL.EliminarArchivo(pArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivo s�lo por Id
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdArchivo">Valor Decimal Id Archivo</param>
        /// <returns>Entity Archivo</returns>
        public ArchivoContrato ConsultarArchivo(decimal pIdArchivo)
        {
            try
            {
                return vArchivoDAL.ConsultarArchivo(pIdArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivo por Id tabla y por Nombre tabla
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="idTabla">Cadena String con el Id tabla</param>
        /// <param name="nombreTabla">Cadena String con el nombre de la tabla</param>
        /// <returns>Lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivoByNombreIDTabla(string idTabla, string nombreTabla)
        {
            try
            {
                return vArchivoDAL.ConsultarArchivoByNombreIDTabla(idTabla, nombreTabla);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Tommy Puccini - Grupo Apoyo
        /// Consultar Archivo s�lo por IdContrato y el nombre del TipoEstructura
        /// Fecha: 23/11/2015
        /// </summary>
        /// <returns>List Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivoTipoEstructurayContrato(int pIdcontrato, String pTipoEstructura, int idTipoEstructura)
        {
            try
            {
                List<ArchivoContrato> vListaArchivos = vArchivoDAL.ConsultarArchivoTipoEstructurayContrato(pIdcontrato, pTipoEstructura, idTipoEstructura);
                
                return vListaArchivos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivos listar GCB informe
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <param name="pIdInformeMensual">int Id Informe mensual</param>
        /// <returns>Lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivosListGCBInforme(ArchivoContrato pArchivo, int pIdInformeMensual)
        {
            try
            {
                List<ArchivoContrato> vListaArchivos = vArchivoDAL.ConsultarArchivosListGCBInforme(pArchivo, pIdInformeMensual);
                //foreach (Archivo vArchivo in vListaArchivos)
                //{
                //    UsuarioBLL vUsuarioBLL = new UsuarioBLL();
                //    Usuario vUsuario = vUsuarioBLL.ConsultarUsuario(Convert.ToInt32(vArchivo.IdUsuario));
                //    MembershipUser oMu = Membership.GetUser(new Guid(vUsuario.Providerkey));
                //    if (oMu != null)
                //        vArchivo.NombreUsuario = oMu.UserName;
                //}
                return vListaArchivos;

                //return vArchivoDAL.ConsultarArchivosList(pArchivo);
                //return vListaArchivos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivos listar GCB
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pArchivo">Entity Archivo</param>
        /// <param name="pIdLlave">int Id llave</param>
        /// <param name="pSigla">Cadena string Sigla</param>
        /// <param name="pIdFormato">int Id formato</param>
        /// <returns>Lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivosListGCB(ArchivoContrato pArchivo, int pIdLlave, string pSigla, int pIdFormato)
        {
            try
            {
                List<ArchivoContrato> vListaArchivos = vArchivoDAL.ConsultarArchivosListGCB(pArchivo, pIdLlave, pSigla, pIdFormato);
                return vListaArchivos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Archivos por Id usuario � por formato del archivo
        /// Fecha: 19/12/2013
        /// </summary>
        /// <param name="pIdUsuario">int con el Id usuario</param>
        /// <param name="pIdFormatoArchivo">int Id formato archivo</param>
        /// <returns>lista Entity Archivo</returns>
        public List<ArchivoContrato> ConsultarArchivos(int? pIdUsuario, int? pIdFormatoArchivo)
        {
            try
            {
                return vArchivoDAL.ConsultarArchivos(pIdUsuario, pIdFormatoArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public List<ArchivoContrato> ConsultarArchivosPorContrato(int idContrato)
        {
            try
            {
                return vArchivoDAL.ConsultarArchivosPorContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idRegional"></param>
        /// <returns></returns>
        public List<CargueContratoMasivo> ConsultarCargueContratosArchivos(int? idRegional)
        {
            try
            {
                return vArchivoDAL.ConsultarCargueContratosArchivos(idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
