﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  SupervisorInterContrato
    /// </summary>
    public class SupervisorInterContratoBLL
    {
        private SupervisorInterContratoDAL vSupervisorInterContratoDAL;
        public SupervisorInterContratoBLL()
        {
            vSupervisorInterContratoDAL = new SupervisorInterContratoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int InsertarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.InsertarSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de inserción para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        /// <param name="pDirectorInterventoria"></param>
        /// <returns></returns>
        public int InsertarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato, DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                return vSupervisorInterContratoDAL.InsertarSupervisorInterContrato(pSupervisorInterContrato, pDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int ModificarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ModificarSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int EliminarSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.EliminarSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        public SupervisorInterContrato ConsultarSupervisorInterContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ConsultarSupervisorInterContrato(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pFechaInicio"></param>
        /// <param name="pInactivo"></param>
        /// <param name="pIdentificacion"></param>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pIDTipoSuperInter"></param>
        /// <param name="pIdNumeroContratoInterventoria"></param>
        /// <param name="pIDProveedoresInterventor"></param>
        /// <param name="pIDEmpleadosSupervisor"></param>
        /// <param name="pIdContrato"></param>
        public List<SupervisorInterContrato> ConsultarSupervisorInterContratos(DateTime? pFechaInicio, Boolean? pInactivo, String pIdentificacion, String pTipoIdentificacion, int? pIDTipoSuperInter, int? pIdNumeroContratoInterventoria, int? pIDProveedoresInterventor, int? pIDEmpleadosSupervisor, int? pIdContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ConsultarSupervisorInterContratos(pFechaInicio, pInactivo, pIdentificacion, pTipoIdentificacion, pIDTipoSuperInter, pIdNumeroContratoInterventoria, pIDProveedoresInterventor, pIDEmpleadosSupervisor, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresInterventoresContrato(int pIdContrato, bool? pDirectoresInterventoria)
        {
            try
            {
                return vSupervisorInterContratoDAL.ObtenerSupervisoresInterventoresContrato(pIdContrato, pDirectoresInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisorInterContrato> ObtenerSupervisoresInterventoresContratoMigrado(int pIdContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ObtenerSupervisoresInterventoresContratoMigrado(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de modificación del campo FechaInicio para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato">Entidad con la información a actualizar</param>
        /// <returns>Resultado de la operación</returns>
        public int ActualizarFechaInicioSupervisorInterContrato(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ActualizarFechaInicioSupervisorInterContrato(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Efrain Diaz
        /// Obtiene la información relacionada con los supervisores/interventores 
        /// dado
        /// </summary>
        /// <param name="pNumeroIdentidicacion">Entero con el identificador Supervisor/Inerventor</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public SupervisorInterContrato ObtenerSupervisorInterventoresContratoDetalle(int pNumeroIdentidicacion,
                                                                                     bool? pDirectoresInterventoria)
        {
            try
            {
                return vSupervisorInterContratoDAL.ObtenerSupervisorInterventoresContratoDetalle( pNumeroIdentidicacion,
                                                                                     pDirectoresInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSupervisorTemporal(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.InsertarSupervisorTemporal(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad SupervisorInterContrato
        /// </summary>
        /// <param name="pSupervisorInterContrato"></param>
        public int EliminarSupervisorTemporal(SupervisorInterContrato pSupervisorInterContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.EliminarSupervisorTemporal(pSupervisorInterContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void GenerarSupervisoresTemporales(int idContrato)
        {
            try
            {
                vSupervisorInterContratoDAL.GenerarSupervisoresTemporales(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        /// <returns></returns>
        public SupervisorInterContrato ConsultarSupervisorTemporalContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ConsultarSupervisorTemporalContrato(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información relacionada con los supervisores/interventores asociados a un contrato
        /// dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado de Supervisores/Interventores obtenidos de la base de datos</returns>
        public List<SupervisorInterContrato> ObtenerSupervisoresTemporalesContrato(int pIdContrato, int ?idsupervisorAnterior)
        {
            try
            {
                return vSupervisorInterContratoDAL.ObtenerSupervisoresTemporalesContrato(pIdContrato, idsupervisorAnterior);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisorInterContrato> ObtenerSupervisoresHistoricoContrato(int pIdContrato)
        {
            try
            {
                return vSupervisorInterContratoDAL.ObtenerSupervisoresHistoricoContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
