using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class AdicionesBLL
    {
        private AdicionesDAL vAdicionesDAL;
        public AdicionesBLL()
        {
            vAdicionesDAL = new AdicionesDAL();
        }
        public int InsertarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                return vAdicionesDAL.InsertarAdiciones(pAdiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                return vAdicionesDAL.ModificarAdiciones(pAdiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarAdiciones(int IdPlanComprasContrato, string IdProducto, int idAdiccion, int idProductoPlanCompras, string tipoProducto, decimal ValorActualAdiccion)
        {
            try
            {
                return vAdicionesDAL.ModificarAdiciones(IdPlanComprasContrato, IdProducto, idAdiccion, idProductoPlanCompras, tipoProducto, ValorActualAdiccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarAdiciones(Adiciones pAdiciones)
        {
            try
            {
                return vAdicionesDAL.EliminarAdiciones(pAdiciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Adiciones ConsultarAdiciones(int pIdAdicion)
        {
            try
            {
                return vAdicionesDAL.ConsultarAdiciones(pIdAdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Adiciones> ConsultarAdicioness(int? pValorAdicion, String pJustificacionAdicion, int? pEstado, DateTime? pFechaAdicion, int? pIDDetalleConsModContractual)
        {
            try
            {
                return vAdicionesDAL.ConsultarAdicioness(pValorAdicion, pJustificacionAdicion, pEstado, pFechaAdicion, pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Adiciones> ConsultarAdicionesAprobadasContrato(int IdContrato)
        {
            try
            {
                return vAdicionesDAL.ConsultarAdicionesAprobadasContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorTotalAdicionado(int IdContrato)
        {
            decimal result = 0;

            try
            {
                result = vAdicionesDAL.ConsultarValorTotalAdicionado(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public void ActualizarAdicionRP(int idAdicion, int idContrato)
        {
            try
            {
                vAdicionesDAL.ActualizarAdicionRP(idAdicion, idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void ActualizarCesionRP(int idCesion)
        {
            try
            {
                vAdicionesDAL.ActualizarCesionRP(idCesion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
