using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class FUCBLL
    {
        private FUCDAL vFUCDAL;
        public FUCBLL()
        {
            vFUCDAL = new FUCDAL();
        }
        public int InsertarFUC(FUC pFUC)
        {
            try
            {
                return vFUCDAL.InsertarFUC(pFUC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarFUC(FUC pFUC)
        {
            try
            {
                return vFUCDAL.ModificarFUC(pFUC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarFUC(FUC pFUC)
        {
            try
            {
                return vFUCDAL.EliminarFUC(pFUC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public FUC ConsultarFUC(Int64 pId)
        {
            try
            {
                return vFUCDAL.ConsultarFUC(pId);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<FUC> ConsultarFUCs(String pRegional, int? pNumeroContrato, DateTime? pFechaSuscripcion, int? pIdAreaSolicita, String pContratistaTipoIdentificacion, String pContratistaNombre, String pRPRubro1, String pSupervisor)
        {
            try
            {
                return vFUCDAL.ConsultarFUCs(pRegional, pNumeroContrato, pFechaSuscripcion, pIdAreaSolicita, pContratistaTipoIdentificacion, pContratistaNombre, pRPRubro1, pSupervisor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
