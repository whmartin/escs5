using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  AsociarRPContrato
    /// </summary>
    public class RPContratoBLL
    {
        private RPContratoDAL vRPContratoDAL;
        public RPContratoBLL()
        {
            vRPContratoDAL = new RPContratoDAL();
        }

        /// <summary>
        /// Gonet
        /// Método de inserción para la entidad RPContrato
        /// Fecha: 11/07/2014
        /// </summary>
        /// <param name="pRPContrato">instancia que contiene la información de RPContratos</param>
        public int InsertarRPContratos(RPContrato pRPContrato)
        {
            try
            {
                return vRPContratoDAL.InsertarRPContratos( pRPContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosAsociados(int? pIdContrato, int? pIdRP)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratosAsociados(pIdContrato, pIdRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        public List<RPContrato> ConsultarRPContratosAsociados(int pIdContrato)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratosAsociados(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RPContrato> ConsultarRPContratosAsociadosVigencias(int? pIdContrato, bool? pesVigenciaFutura,int? pAnioVigencia)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratosAsociadosVigencias(pIdContrato, pesVigenciaFutura, pAnioVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        /// <param name="pIdRegional">Valor entero con el id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el id de la vigencia</param>
        /// <param name="pValorTotalDesde">Valor decimal con el Valor Total Desde</param>
        /// <param name="pValorTotalHasta">Valor decimal con el Valor Total hasta</param>
        /// <param name="pFechaRpDesde">Valor Fecha Rp Desde</param>
        /// <param name="pFechaRpHasta">Valor Fecha Rp Hasta</param>
        public List<RPContrato> ConsultarRPContratossActualizado(int? pIdRP, int? pIdRegional, int? pIdVigencia, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaRpDesde, DateTime? pFechaRpHasta)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratossActualizado(pIdRP, pIdRegional, pIdVigencia, pValorTotalDesde,
                                                            pValorTotalHasta, pFechaRpDesde, pFechaRpHasta);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RPContratos
        /// </summary>
        /// <param name="pIdRP">Valor entero con el id del RP</param>
        /// <param name="pIdRegional">Valor entero con el id de la regional</param>
        /// <param name="pIdVigencia">Valor entero con el id de la vigencia</param>
        /// <param name="pValorTotalDesde">Valor decimal con el Valor Total Desde</param>
        /// <param name="pValorTotalHasta">Valor decimal con el Valor Total hasta</param>
        /// <param name="pFechaRpDesde">Valor Fecha Rp Desde</param>
        /// <param name="pFechaRpHasta">Valor Fecha Rp Hasta</param>
        public List<RPContrato> ConsultarRPContratoss(int? pIdRP, int? pIdRegional, int? pIdVigencia, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaRpDesde, DateTime? pFechaRpHasta)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratoss(pIdRP, pIdRegional, pIdVigencia, pValorTotalDesde,
                                                            pValorTotalHasta, pFechaRpDesde, pFechaRpHasta);
                
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de actualización de un contrato suscrito a contrato RP
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pUsuarioModifica"></param>
        /// <param name="vFechaSuscripcion"></param>
        /// <param name="vIdEstadoContrato"></param>
        public int ActualizarContratoRP(int pIdContrato, string pUsuarioModifica, int vIdEstadoContrato)
        {
            try
            {
                return vRPContratoDAL.ActualizarContratoRP(pIdContrato, pUsuarioModifica, vIdEstadoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="usuario"></param>
        /// <param name="idRP"></param>
        /// <returns></returns>
        public int ActualizarAdicionRP(int idAdicion, string usuario, int idRP, decimal valorRP, int idRegional)
        {
            try
            {
                return vRPContratoDAL.ActualizarAdicionRP(idAdicion, usuario, idRP, valorRP, idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="usuario"></param>
        /// <param name="idRP"></param>
        /// <returns></returns>
        public int ActualizarCesionRP(int idCesion, string usuario, int idRP, decimal valorRP, int idRegional)
        {
            try
            {
                return vRPContratoDAL.ActualizarCesionRP(idCesion, usuario, idRP, valorRP, idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdAdicion"></param>
        /// <returns></returns>
        public RPContrato ConsultarRPAdicionAsociados(int pIdContrato, int pIdAdicion, bool esAdicion)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratosAsociados(pIdContrato, pIdAdicion, esAdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="pIdRP"></param>
        /// <returns></returns>
        public List<RPContrato> ConsultarRPContratosExiste(int pIdRP)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratosExiste(pIdRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="pIdRP"></param>
        /// <returns></returns>
        public List<RPContrato> ConsultarRPContratosExiste(int pNumeroContrato, int idRegional)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPContratosExiste(pNumeroContrato, idRegional);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarRPContratos(RPContrato pRPContrato, int pIdVigenciaFutura)
        {
            try
            {
                return vRPContratoDAL.EliminarRPContratos(pRPContrato, pIdVigenciaFutura);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RPContrato ConsultarRPPorId(int pIdRP)
        {
            try
            {
                return vRPContratoDAL.ConsultarRPPorId(pIdRP);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
