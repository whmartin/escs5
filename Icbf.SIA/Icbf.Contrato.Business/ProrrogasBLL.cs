using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ProrrogasBLL
    {
        private ProrrogasDAL vProrrogasDAL;
        
        public ProrrogasBLL()
        {
            vProrrogasDAL = new ProrrogasDAL();
        }

        public int InsertarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                return vProrrogasDAL.InsertarProrrogas(pProrrogas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
        public int ModificarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                return vProrrogasDAL.ModificarProrrogas(pProrrogas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
        public int EliminarProrrogas(Prorrogas pProrrogas)
        {
            try
            {
                return vProrrogasDAL.EliminarProrrogas(pProrrogas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Prorrogas ConsultarProrrogas(int pIdProrroga)
        {
            try
            {
                return vProrrogasDAL.ConsultarProrrogas(pIdProrroga);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Prorrogas> ConsultarProrrogass(DateTime? pFechaInicio, DateTime? pFechaFin, int? pDias, int? pMeses, int? pAños, String pJustificacion, int? pIdDetalleConsModContractual)
        {
            try
            {
                return vProrrogasDAL.ConsultarProrrogass(pFechaInicio, pFechaFin, pDias, pMeses, pAños, pJustificacion, pIdDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Prorrogas> ConsultarProrrogasContrato(int idContrato)
        {
            try
            {
                return vProrrogasDAL.ConsultarProrrogasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<Prorrogas> ConsultarProrrogasAprobadasContrato(int idContrato)
        {
            try
            {
                return vProrrogasDAL.ConsultarProrrogasAprobadasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }        

        public Tiempo ConsultarProrrogasAprobadasContratoDetalle(int idContrato)
        {
            try
            {
                return vProrrogasDAL.ConsultarProrrogasAprobadasContratoDetalle(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
