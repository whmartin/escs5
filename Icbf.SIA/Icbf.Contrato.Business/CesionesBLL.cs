using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class CesionesBLL
    {
        private CesionesDAL vCesionesDAL;
        
        public CesionesBLL()
        {
            vCesionesDAL = new CesionesDAL();
        }
        
        public int InsertarCesiones(Cesiones pCesiones)
        {
            try
            {
                return vCesionesDAL.InsertarCesiones(pCesiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
        public int ModificarCesiones(Cesiones pCesiones)
        {
            try
            {
                return vCesionesDAL.ModificarCesiones(pCesiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int EliminarCesiones(Cesiones pCesiones)
        {
            try
            {
                return vCesionesDAL.EliminarCesiones(pCesiones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Cesiones ConsultarCesiones(int pIdCesion)
        {
            try
            {
                return vCesionesDAL.ConsultarCesiones(pIdCesion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Cesiones ConsultarCesionesContrato(int pIdContrato)
        {
            try
            {
                return vCesionesDAL.ConsultarCesionesContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarInfoCesionesSuscritas(int pIdContrato)
        {
            try
            {
                return vCesionesDAL.ConsultarInfoCesionesSuscritas(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarInfoCesiones(int pIdContrato)
        {
            try
            {
                return vCesionesDAL.ConsultarInfoCesiones(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarCesioness(String pFechaCesion, String pJustificacion, int? pEstado, int? pIDDetalleConsModContractual)
        {
            try
            {
                return vCesionesDAL.ConsultarCesioness(pFechaCesion, pJustificacion, pEstado, pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarCesionessContrato(int pContrato, int? pIntegrantesUnionTemporal, int? pIdCesion)
        {
            try
            {
                return vCesionesDAL.ConsultarCesionessContrato(pContrato, pIntegrantesUnionTemporal, pIdCesion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarContratisasActuales(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vCesionesDAL.ConsultarContratisasActuales(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarContratisasCesion(int pContrato, int idcesion, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vCesionesDAL.ConsultarContratisasCesion(pContrato,idcesion, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Cesiones> ConsultarExisteCesionario(int pContrato, int idcesion, int idProveedorePadre)
        {
            try
            {
                return vCesionesDAL.ConsultarExisteCesionario(pContrato, idcesion, idProveedorePadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
