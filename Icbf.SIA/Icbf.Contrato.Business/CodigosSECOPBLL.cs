﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{

    public class CodigosSECOPBLL
    {
        private CodigosSECOPDAL vCodigoSECOPDAL;
        public CodigosSECOPBLL()
        {
            vCodigoSECOPDAL = new CodigosSECOPDAL();
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar en la tabla CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del documento </param>
        public int InsertarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                return vCodigoSECOPDAL.InsertarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar por id el codigo en la tabla CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int ModificarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                return vCodigoSECOPDAL.ModificarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar por id para la entidad CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pDocumentoLiquidacion">instancia que contiene la información del numero de procesos</param>
        public int EliminarCodigoSECOP(CodigosSECOP cCodigoSECOP)
        {
            try
            {
                return vCodigoSECOPDAL.EliminarCodigoSECOP(cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de consulta por id para la entidad CodigoSECOP
        /// Fecha: 27/06/2016
        /// </summary>
        /// <param name="pIdDocumentoLiquidacion">Valor entero con el Id del Documento de Liquidacion</param>
        public CodigosSECOP ConsultarCodigoSECOP(int cIdCodigoSECOP)
        {
            try
            {
                return vCodigoSECOPDAL.ConsultarCodigoSECOP(cIdCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<CodigosSECOP> ConsultarVariosCodigosSECOP(Boolean? cEstado, string cCodigoSECOP, int ? cantidad)
        {
            try
            {
                return vCodigoSECOPDAL.ConsultarVariosCodigoSECOP(cEstado, cCodigoSECOP,cantidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int AsociarCodigoSECOPContrato(int idContrato, int idCodigoSecop)
        {
            try
            {
                return vCodigoSECOPDAL.AsociarCodigoSECOPContrato(idContrato, idCodigoSecop);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ObtenerCodigosSECOPContrato(int idContrato)
        {
            try
            {
                return vCodigoSECOPDAL.ObtenerCodigosSECOPContrato(idContrato);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarCodigoSECOPContrato(int idContrato, int idCodigoSecop)
        {
            try
            {
                return vCodigoSECOPDAL.EliminarCodigoSECOPContrato(idContrato, idCodigoSecop);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarVinculoSECOP(int idContrato, string cCodigoSECOP)
        {
            try
            {
                return vCodigoSECOPDAL.ActualizarVinculoSECOP(idContrato,cCodigoSECOP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
