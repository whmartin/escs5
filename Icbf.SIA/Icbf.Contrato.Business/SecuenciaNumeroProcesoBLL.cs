using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  secuencia n�mero proceso
    /// </summary>
    public class SecuenciaNumeroProcesoBLL
    {
        private SecuenciaNumeroProcesoDAL vSecuenciaNumeroProcesoDAL;
        public SecuenciaNumeroProcesoBLL()
        {
            vSecuenciaNumeroProcesoDAL = new SecuenciaNumeroProcesoDAL();
        }
        /// <summary>
        /// M�todo que Genera el Numero de Proceso
        /// </summary>
        /// <param name="pSecuenciaNumeroProceso"></param>
        /// <returns></returns>
        public string GenerarNumeroProceso(SecuenciaNumeroProceso pSecuenciaNumeroProceso)
        {
            try
            {
                return vSecuenciaNumeroProcesoDAL.GenerarNumeroProceso(pSecuenciaNumeroProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
