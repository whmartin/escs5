using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para el m�dulo de gesti�n de supervisores e interventores
    /// </summary>
    public class ConsultarSupervisorInterventorBLL
    {
        private ConsultarSupervisorInterventorDAL vConsultarSupervisorInterventorDAL;
        public ConsultarSupervisorInterventorBLL()
        {
            vConsultarSupervisorInterventorDAL = new ConsultarSupervisorInterventorDAL();
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ConsultarSupervisorInterventor
        /// </summary>
        /// <param name="pTipoSupervisorInterventor"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pNombreRazonSocialSupervisorInterventor"></param>
        /// <param name="pNumeroIdentificacionDirectorInterventoria"></param>
        /// <param name="pNombreRazonSocialDirectorInterventoria"></param>
        /// <returns></returns>
        public DataTable ConsultarConsultarSupervisorInterventors(int? pTipoSupervisorInterventor, String pNumeroContrato, String pNumeroIdentificacion, String pNombreRazonSocialSupervisorInterventor, String pNumeroIdentificacionDirectorInterventoria, String pNombreRazonSocialDirectorInterventoria)
        {
            try
            {
                return vConsultarSupervisorInterventorDAL.ConsultarConsultarSupervisorInterventors(pTipoSupervisorInterventor, pNumeroContrato, pNumeroIdentificacion, pNombreRazonSocialSupervisorInterventor, pNumeroIdentificacionDirectorInterventoria, pNombreRazonSocialDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
