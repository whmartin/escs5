﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{

    public class RolesBLL
    {
        private RolesDAL vRolesDAL;
        public RolesBLL()
        {
            vRolesDAL = new RolesDAL();
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de insertar en la tabla Roles
        /// Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRoles">instancia que contiene la información de los Roles</param>
        public int InsertarRoles(RolesContrato pRol)
        {
            try
            {
                return vRolesDAL.InsertarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Modificar por id el codigo en la tabla Roles
        /// Fecha: Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public int ModificarRoles(RolesContrato pRol)
        {
            try
            {
                return vRolesDAL.ModificarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de Eliminar por id para la entidad Roles
        /// Fecha: Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public int EliminarRoles(RolesContrato pRol)
        {
            try
            {
                return vRolesDAL.EliminarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Cesar Augusto Lopez
        /// Método de consulta por id para la entidad Roles
        /// Fecha: Fecha: 14/09/2016
        /// </summary>
        /// <param name="pRol">instancia que contiene la información de los Roles</param>
        public RolesContrato ConsultarRoles(int pRol)
        {
            try
            {
                return vRolesDAL.ConsultarRoles(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RolesContrato> ConsultarVariosRoles(Boolean? cEstado, string cNombreRol)
        {
            try
            {
                return vRolesDAL.ConsultarVariosRoles(cEstado, cNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

