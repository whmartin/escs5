﻿using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class LiquidacionContratoBLL
    {
        private LiquidacionContratoDAL _liquidacionContratoDAL;

        public LiquidacionContratoBLL()
        {
            _liquidacionContratoDAL = new LiquidacionContratoDAL();
        }

        public int InsertarLiquidacionContrato(LiquidacionContrato pTerminacionAnticipada)
        {
            try
            {
                return _liquidacionContratoDAL.InsertarLiquidacionContrato(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarLiquidacionContrato(LiquidacionContrato pTerminacionAnticipada)
        {
            try
            {
                return _liquidacionContratoDAL.ModificarLiquidacionContrato(pTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionContrato(int IdContrato)
        {
            try
            {
                return _liquidacionContratoDAL.ConsultarLiquidacionContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionId(int IdTerminacionAnticipada)
        {
            try
            {
                return _liquidacionContratoDAL.ConsultarLiquidacionContratoId(IdTerminacionAnticipada);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LiquidacionContrato> ConsultarLiquidacionIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                return _liquidacionContratoDAL.ConsultarLiquidacionContratoIdDetalle(IdDetConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
