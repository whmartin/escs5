using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  AporteContrato
    /// </summary>
    public class AporteContratoBLL
    {
        private AporteContratoDAL vAporteContratoDAL;
        public AporteContratoBLL()
        {
            vAporteContratoDAL = new AporteContratoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int InsertarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                return vAporteContratoDAL.InsertarAporteContrato(pAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        
        public int EliminarAporteContratoValor(int pIdContrato)
        {
            try
            {
                return vAporteContratoDAL.EliminarAporteContratoValor(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarAporteContratoValor(int pIdContrato, decimal pValorAporte, string usuarioModifica)
        {
            try
            {
                return vAporteContratoDAL.ModificarAporteContratoValor(pIdContrato, pValorAporte,usuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int ModificarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                return vAporteContratoDAL.ModificarAporteContrato(pAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AporteContrato
        /// </summary>
        /// <param name="pAporteContrato"></param>
        public int EliminarAporteContrato(AporteContrato pAporteContrato)
        {
            try
            {
                return vAporteContratoDAL.EliminarAporteContrato(pAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarAportesCofinanciacionContrato(int idContrato)
        {
            try
            {
                return vAporteContratoDAL.EliminarAportesCofinanciacionContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarAportesEspecieICBFContrato(int idContrato)
        {
            try
            {
                return vAporteContratoDAL.EliminarAportesEspecieICBFContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AporteContrato
        /// </summary>
        /// <param name="pIdAporteContrato"></param>
        public AporteContrato ConsultarAporteContrato(int pIdAporteContrato)
        {
            try
            {
                return vAporteContratoDAL.ConsultarAporteContrato(pIdAporteContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AporteContrato
        /// </summary>
        /// <param name="pAportanteICBF"></param>
        /// <param name="pNumeroIdentificacionICBF"></param>
        /// <param name="pValorAporte"></param>
        /// <param name="pDescripcionAporte"></param>
        /// <param name="pAporteEnDinero"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        /// <param name="pFechaRP"></param>
        /// <param name="pNumeroRP"></param>
        /// <param name="pEstado"></param>
        public List<AporteContrato> ConsultarAporteContratos(Boolean? pAportanteICBF, String pNumeroIdentificacionICBF, Decimal? pValorAporte, String pDescripcionAporte, Boolean? pAporteEnDinero, int? pIdContrato, int? pIDEntidadProvOferente, DateTime? pFechaRP, String pNumeroRP, Boolean? pEstado)
        {
            try
            {
                return vAporteContratoDAL.ConsultarAporteContratos(pAportanteICBF, pNumeroIdentificacionICBF, pValorAporte, pDescripcionAporte, pAporteEnDinero, pIdContrato, pIDEntidadProvOferente, pFechaRP, pNumeroRP, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un contratista asociado a un tipo y valor de aporte
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteContratoAporte(int pIdContrato, Boolean pAporteEnDinero, Decimal pValorAporte, bool pAportanteICBF)
        {
            try
            {
                return vAporteContratoDAL.ExisteContratoAporte(pIdContrato,pAporteEnDinero,pValorAporte, pAportanteICBF)
                ;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la información de los aportes asociados a un contrato seleccionados a partir de los filtros de entrada
        /// (Página Registro Contratos)
        /// </summary>
        /// <param name="pAportanteICBF">Booleano que indica si el aporte es hecho por el ICBF o no</param>
        /// <param name="pIdContrato">Entero con el identificador del contrato al que se asocia el aporte</param>
        /// <param name="pIDEntidadProvOferente">Entero con el identificador del contratista que realiza el aporte</param>
        /// <param name="pEstado">Booleano que indica el estado del aporte</param>
        /// <returns>Listado de aportes obtenidos de la base de datos que coinciden con los filtros</returns>
        public List<AporteContrato> ObtenerAportesContrato(Boolean? pAportanteICBF, int? pIdContrato, int? pIDEntidadProvOferente, Boolean? pEstado)
        {
            try
            {
                return vAporteContratoDAL.ObtenerAportesContrato(pAportanteICBF, pIdContrato, pIDEntidadProvOferente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// ICBF/SSII
        /// Obtiene los aportes que se le realizan a un contrato.
        /// </summary>
        /// <param name="pIdContrato">
        ///  Id del contrato aosciado
        /// </param>
        /// <returns>
        /// Listado de aportes asociados al contrato.
        /// </returns>
        public List<AporteContrato> ObtenerAportesContrato(int pIdContrato, bool adiciones)
        {
            try
            {
                return vAporteContratoDAL.ObtenerAportesContrato(pIdContrato, adiciones);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<AporteContrato> ObtenerAportesContratoActuales(int pIdContrato)
        {
            try
            {
                return vAporteContratoDAL.ObtenerAportesContratoActuales(pIdContrato);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pidAdicion"></param>
        /// <returns></returns>
        public List<AporteContrato> ObtenerAportesContratoAdicionReduccion(int pidModifcacion, bool esAdicion)
        {
            try
            {
                return vAporteContratoDAL.ObtenerAportesContratoAdicionReduccion(pidModifcacion,esAdicion);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iddetCons"></param>
        /// <param name="idAdicion"></param>
        /// <param name="idAportePadre"></param>
        /// <param name="valorAporte"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int CrearAportesAdicion(int iddetCons, int idAdicion, int idAportePadre, decimal valorAporte, string usuario)
        {
            try
            {
                return vAporteContratoDAL.CrearAportesAdicion(iddetCons, idAdicion, idAportePadre, valorAporte, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="valorAporte"></param>
        /// <param name="idAportePadre"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int actualizarAportesAdicion(int idAdicion, decimal valorAporte, int idAportePadre, string usuario)
        {
            try
            {
                return vAporteContratoDAL.actualizarAportesAdicion(idAdicion, valorAporte, idAportePadre, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idDetCons"></param>
        /// <param name="valorAporte"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public KeyValuePair<int, int> CrearAporteDineroAdicion(int idDetCons, decimal valorAporte, string usuario, bool esAdicion, int idAporte, int idPlanCompras)
        {
            try
            {
                return vAporteContratoDAL.CrearAporteDineroAdicion(idDetCons, valorAporte, usuario, esAdicion, idAporte, idPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idAdicion"></param>
        /// <param name="valorAporte"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int ActualizarAporteDineroAdicion(int idModificacion, decimal valorAporte, string usuario, bool esAdicion, int idPlanCompras, int idAporte)
        {
            try
            {
                return vAporteContratoDAL.ActualizarAporteDineroAdicion(idModificacion, valorAporte, usuario, esAdicion, idPlanCompras, idAporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="esAdicion"></param>
        /// <param name="idModificacion"></param>
        /// <param name="idPlanCompras"></param>
        /// <returns></returns>
        public AporteContrato ObtenerAporteModificacion(bool esAdicion, int idModificacion, int idPlanCompras)
        {
            try
            {
                return vAporteContratoDAL.ObtenerAporteModificacion(esAdicion, idModificacion, idPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPlanComprasContrato"></param>
        /// <returns></returns>
        public decimal ObtenerValorTotalPlanComprasContrato(int idPlanComprasContrato)
        {
            try
            {
                return vAporteContratoDAL.ObtenerValorTotalPlanComprasContrato(idPlanComprasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idReduccion"></param>
        /// <param name="valorAporte"></param>
        /// <param name="idAportePadre"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int ActualizarAportesReduccion(int idReduccion, decimal valorAporte, int idAportePadre, string usuario)
        {
            try
            {
                return vAporteContratoDAL.ActualizarAportesReduccion(idReduccion, valorAporte, idAportePadre, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iddetCons"></param>
        /// <param name="idReduccion"></param>
        /// <param name="idAportePadre"></param>
        /// <param name="valorAporte"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public int CrearAportesReduccion(int iddetCons, int idReduccion, int idAportePadre, decimal valorAporte, string usuario)
        {
            try
            {
                return vAporteContratoDAL.CrearAportesReduccion(iddetCons, idReduccion, idAportePadre, valorAporte, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
