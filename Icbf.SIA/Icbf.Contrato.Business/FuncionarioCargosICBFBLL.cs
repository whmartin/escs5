using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de la capa de negocio que contiene los métodos para Insertar, Modificar, Eliminar y Consultar registros de los cargos de los funcionarios del ICBF
    /// </summary>
    public class FuncionarioCargosICBFBLL
    {
        private FuncionarioCargosICBFDAL vFuncionarioCargosICBFDAL;
        public FuncionarioCargosICBFBLL()
        {
            vFuncionarioCargosICBFDAL = new FuncionarioCargosICBFDAL();
        }
        /// <summary>
        /// Gonet
        /// Insertar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioCargosICBF">Instancia que contiene la información de los cargos funcionarios del ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int InsertarFuncionarioCargosICBF(FuncionarioCargosICBF pFuncionarioCargosICBF)
        {
            try
            {
                return vFuncionarioCargosICBFDAL.InsertarFuncionarioCargosICBF(pFuncionarioCargosICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioCargosICBF">Instancia que contiene la información de los cargos funcionarios del ICBF</param>
        /// <returns>Identificador de la base de datos del registro modificado</returns>
        public int ModificarFuncionarioCargosICBF(FuncionarioCargosICBF pFuncionarioCargosICBF)
        {
            try
            {
                return vFuncionarioCargosICBFDAL.ModificarFuncionarioCargosICBF(pFuncionarioCargosICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioCargosICBF">Instancia que contiene la información de los cargos funcionarios del ICBF</param>
        /// <returns>Identificador de la base de datos del registro eliminado</returns>
        public int EliminarFuncionarioCargosICBF(FuncionarioCargosICBF pFuncionarioCargosICBF)
        {
            try
            {
                return vFuncionarioCargosICBFDAL.EliminarFuncionarioCargosICBF(pFuncionarioCargosICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar cargo funcionario del ICBF por Id
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdFuncCargo">Valor entero con el Id cargo del funcionario</param>
        /// <returns>Instancia que contiene la información de los cargos funcionarios del ICBF</returns>
        public FuncionarioCargosICBF ConsultarFuncionarioCargosICBF(int pIdFuncCargo)
        {
            try
            {
                return vFuncionarioCargosICBFDAL.ConsultarFuncionarioCargosICBF(pIdFuncCargo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdCargo">Valor entero con el Id del cargo, permite valores nulos</param>
        /// <param name="pIdFuncionario">Valor entero con el Id del funcionario, permite valores nulos</param>
        /// <param name="pResolucionNombramiento">Cadena de texto con la resolución del nombramiento</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con la Instancia que contiene la información los cargos funcionarios del ICBF</returns>
        public List<FuncionarioCargosICBF> ConsultarFuncionarioCargosICBFs(int? pIdCargo, int? pIdFuncionario, String pResolucionNombramiento, Boolean? pEstado)
        {
            try
            {
                return vFuncionarioCargosICBFDAL.ConsultarFuncionarioCargosICBFs(pIdCargo, pIdFuncionario, pResolucionNombramiento, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Consultar cargo funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdCargo">Valor entero con el Id del cargo, permite valores nulos</param>
        /// <param name="pIdFuncionario">Valor entero con el Id del funcionario, permite valores nulos</param>
        /// <param name="pResolucionNombramiento">Cadena de texto con la resolución del nombramiento</param>
        /// <param name="pEstado">Cadena de texto con el estado</param>
        /// <returns>Lista con la Instancia que contiene la información los cargos funcionarios del ICBF</returns>
        public List<FuncionarioCargosICBF> ConsultarFuncionarioCargosICBFs(int? pIdCargo, int? pIdFuncionario, String pResolucionNombramiento, Boolean? pEstado, Boolean? pEstadoFunc)
        {
            try
            {
                return vFuncionarioCargosICBFDAL.ConsultarFuncionarioCargosICBFs(pIdCargo, pIdFuncionario, pResolucionNombramiento, pEstado, pEstadoFunc);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
