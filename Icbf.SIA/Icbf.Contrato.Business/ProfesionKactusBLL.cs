﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Contrato.DataAccess;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Modalidad Academica Kactus
    /// </summary>
    public class ProfesionKactusBLL
    {
        private ProfesionKactusDAL vProfesionKactusDAL;
        public ProfesionKactusBLL()
        {
            vProfesionKactusDAL = new ProfesionKactusDAL();
        }

        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo de consulta por filtros para la entidad ProfesionKactus
        /// </summary>
        /// <param name="pModalidadAcademica">Cadena de texto con la modalidad academica asociada a la profesión</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcion de la profesión</param>
        /// <param name="pCodigo">Cadena de texto con el codigo de la profesión</param>
        /// <param name="pEstado">Booleano con el estado de la profesión</param>
        /// <returns>Lista con las profesiones que coinciden con los filtros dados</returns>
        public List<ProfesionKactus> ConsultarProfesionesKactus(String pModalidadAcademica, String pDescripcion, String pCodigo, Boolean? pEstado)
        {
            try
            {
                return vProfesionKactusDAL.ConsultarProfesionesKactus(pModalidadAcademica, pDescripcion, pCodigo, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
