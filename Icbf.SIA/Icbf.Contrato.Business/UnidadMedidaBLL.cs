using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Unidad Medida
    /// </summary>
    public class UnidadMedidaBLL
    {

        private UnidadMedidaDAL vUnidadMedidaDAL;
        public UnidadMedidaBLL()
        {
            vUnidadMedidaDAL = new UnidadMedidaDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int InsertarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                return vUnidadMedidaDAL.InsertarUnidadMedida(pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int ModificarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                return vUnidadMedidaDAL.ModificarUnidadMedida(pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad UnidadMedida
        /// </summary>
        /// <param name="pUnidadMedida"></param>
        /// <returns></returns>
        public int EliminarUnidadMedida(UnidadMedida pUnidadMedida)
        {
            try
            {
                return vUnidadMedidaDAL.EliminarUnidadMedida(pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad UnidadMedida
        /// </summary>
        /// <param name="pIdNumeroContrato"></param>
        /// <returns></returns>
        public UnidadMedida ConsultarUnidadMedida(int pIdNumeroContrato)
        {
            try
            {
                return vUnidadMedidaDAL.ConsultarUnidadMedida(pIdNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad UnidadMedida
        /// </summary>
        /// <param name="pIdNumeroContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pFechaInicioEjecuciónContrato"></param>
        /// <param name="pFechaTerminacionInicialContrato"></param>
        /// <param name="pFechaFinalTerminacionContrato"></param>
        /// <returns></returns>
        public List<UnidadMedida> ConsultarUnidadMedidas(int? pIdNumeroContrato, String pNumeroContrato, DateTime? pFechaInicioEjecuciónContrato, DateTime? pFechaTerminacionInicialContrato, DateTime? pFechaFinalTerminacionContrato)
        {
            try
            {
                return vUnidadMedidaDAL.ConsultarUnidadMedidas(pIdNumeroContrato, pNumeroContrato, pFechaInicioEjecuciónContrato, pFechaTerminacionInicialContrato, pFechaFinalTerminacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
