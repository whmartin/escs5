using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  VigenciaFuturas
    /// </summary>
    public class VigenciaFuturasBLL
    {
        private VigenciaFuturasDAL vVigenciaFuturasDAL;
        public VigenciaFuturasBLL()
        {
            vVigenciaFuturasDAL = new VigenciaFuturasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int InsertarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasDAL.InsertarVigenciaFuturas(pVigenciaFuturas);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        /// <returns></returns>
        public int InsertarVigenciaFuturasModificacionContrato(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasDAL.InsertarVigenciaFuturasModificacionContrato(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int ModificarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasDAL.ModificarVigenciaFuturas(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pVigenciaFuturas"></param>
        public int EliminarVigenciaFuturas(VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasDAL.EliminarVigenciaFuturas(pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        public int EliminarVigenciaFuturasContrato(int idContrato)
        {
            try
            {
                return vVigenciaFuturasDAL.EliminarVigenciaFuturasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pIDVigenciaFuturas"></param>
        public VigenciaFuturas ConsultarVigenciaFuturas(int pIDVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasDAL.ConsultarVigenciaFuturas(pIDVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad VigenciaFuturas
        /// </summary>
        /// <param name="pNumeroRadicado"></param>
        /// <param name="pFechaExpedicion"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pValorVigenciaFutura"></param>
        public List<VigenciaFuturas> ConsultarVigenciaFuturass(String pNumeroRadicado, DateTime? pFechaExpedicion, int? pIdContrato, Decimal? pValorVigenciaFutura)
        {
            try
            {
                return vVigenciaFuturasDAL.ConsultarVigenciaFuturass(pNumeroRadicado, pFechaExpedicion, pIdContrato, pValorVigenciaFutura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public VigenciaFuturas ConsultarVigenciaFuturasporContrato(int pIdContrato)
        {
            try
            {
                return vVigenciaFuturasDAL.ConsultarVigenciaFuturasporContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Entity.Contrato> ConsultarContratosVigenciasFuturas(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, string pNumeroContratoConvenio, string pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vVigenciaFuturasDAL.ConsultarContratosVigenciasFuturas(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarVigenciaFuturasModificacionContrato(int IDVigenciaFuturas)
        {
            try
            {
                return vVigenciaFuturasDAL.EliminarVigenciaFuturasModificacionContrato(IDVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarVigenciaFuturasModificacionContratoTodas(int idContrato)
        {
            try
            {
                return vVigenciaFuturasDAL.EliminarVigenciaFuturasModificacionContratoTodas(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
