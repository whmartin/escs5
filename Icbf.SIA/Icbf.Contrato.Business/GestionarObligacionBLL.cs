using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de gesti�n de obligaciones
    /// </summary>
    public class GestionarObligacionBLL
    {
        private GestionarObligacionDAL vGestionarObligacionDAL;
        public GestionarObligacionBLL()
        {
            vGestionarObligacionDAL = new GestionarObligacionDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int InsertarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                return vGestionarObligacionDAL.InsertarGestionarObligacion(pGestionarObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int ModificarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                return vGestionarObligacionDAL.ModificarGestionarObligacion(pGestionarObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pGestionarObligacion"></param>
        /// <returns></returns>
        public int EliminarGestionarObligacion(GestionarObligacion pGestionarObligacion)
        {
            try
            {
                return vGestionarObligacionDAL.EliminarGestionarObligacion(pGestionarObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pIdGestionObligacion"></param>
        /// <returns></returns>
        public GestionarObligacion ConsultarGestionarObligacion(int pIdGestionObligacion)
        {
            try
            {
                return vGestionarObligacionDAL.ConsultarGestionarObligacion(pIdGestionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad GestionarObligacion
        /// </summary>
        /// <param name="pIdGestionObligacion"></param>
        /// <param name="pIdGestionClausula"></param>
        /// <param name="pNombreObligacion"></param>
        /// <param name="pIdTipoObligacion"></param>
        /// <param name="pOrden"></param>
        /// <param name="pDescripcionObligacion"></param>
        /// <returns></returns>
        public List<GestionarObligacion> ConsultarGestionarObligacions(int? pIdGestionObligacion, int? pIdGestionClausula, String pNombreObligacion, int? pIdTipoObligacion, String pOrden, String pDescripcionObligacion)
        {
            try
            {
                return vGestionarObligacionDAL.ConsultarGestionarObligacions(pIdGestionObligacion, pIdGestionClausula, pNombreObligacion, pIdTipoObligacion, pOrden, pDescripcionObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
