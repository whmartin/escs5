using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ComponentesBLL
    {
        private ComponentesDAL vComponentesDAL;
        public ComponentesBLL()
        {
            vComponentesDAL = new ComponentesDAL();
        }
        public int InsertarComponentes(Componentes pComponentes)
        {
            try
            {
                return vComponentesDAL.InsertarComponentes(pComponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarComponentes(Componentes pComponentes)
        {
            try
            {
                return vComponentesDAL.ModificarComponentes(pComponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarComponentes(Componentes pComponentes)
        {
            try
            {
                return vComponentesDAL.EliminarComponentes(pComponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public List<Componentes> ConsultarComponentess(string Nombre, int Estado)
        {
            try
            {
                return vComponentesDAL.ConsultarComponentes(Nombre,Estado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Componentes ConsultarComponentess(int IdComponente)
        {
            try
            {
                return vComponentesDAL.ConsultarComponentes(IdComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
       
    }
}
