﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase temporal de Emilio Calapina/Gonet
    /// </summary>
    public partial class ContratoModificacionBLL
    {
        private ContratoModificacionDAL vContratoDAL;

        public ContratoModificacionBLL()
        {
            vContratoDAL = new ContratoModificacionDAL();
        }

        public List<Entity.Contrato> ConsultarContratosModificacion(DateTime? vFechaRegistroSistemaDesde, DateTime? vFechaRegistroSistemaHasta, int? vIdContrato, int? vVigenciaFiscalinicial, int? vIDRegional, int? vIDModalidadSeleccion, int? vIDCategoriaContrato, int? vIDTipoContrato, int? vIDEstadoContrato, string vNumeroContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosModificacion(vFechaRegistroSistemaDesde, vFechaRegistroSistemaHasta, vIdContrato, vVigenciaFiscalinicial, vIDRegional, vIDModalidadSeleccion, vIDCategoriaContrato, vIDTipoContrato, vIDEstadoContrato, vNumeroContrato);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Entity.ContratoModificacionDetalle ConsultarPorId(int idContrato)
        {
            try
            {
                return vContratoDAL.ConsultarPorId(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int Modificar(Entity.ContratoModificacionEditar item)
        {
            try
            {
                return vContratoDAL.Modificar(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
    }
}
