﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;

namespace Icbf.Contrato.Business
{
    public class IntegrationContratosBLL
    {
        private IntegrationContratosDAL _IntegrationDAL;

        public IntegrationContratosBLL()
        {
            _IntegrationDAL = new IntegrationContratosDAL();
        }

        public InfoContratoDTO ObtenerInfoContratosPaccoPorId(int idPlanCompras)
        {
            try
            {
                return _IntegrationDAL.ObtenerInfoContratosPaccoPorId(idPlanCompras);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InfoContratoDTO ObtenerInfoContratosGeneral(int ano, int numero, string codigoRegional)
        {
            try
            {
                string vNumeroContrato = numero.ToString().PadLeft(5, '0');

                string consecutvo = codigoRegional.Trim()+""+vNumeroContrato+""+ano.ToString();

                return _IntegrationDAL.ObtenerInfoContratosGeneral(consecutvo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InfoContratoDTO> ObtenerInfoContratoGeneralPorPeriodo(string codigoRegional, DateTime fechaDesde, DateTime fechaHasta)
        {
            try
            {
                return _IntegrationDAL.ObtenerInfoContratoGeneralPorPeriodo(codigoRegional, fechaDesde, fechaHasta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InfoContratoDTO> GetInfoContratoGeneralPorRubro(string codigoRegional, int vigencia, string codigoRubro, int numeroContrato)
        {
            
                try
                {
                    string vNumeroContrato = numeroContrato.ToString().PadLeft(5, '0');

                    string consecutivo = string.Empty;

                if(numeroContrato != 0)
                    consecutivo = codigoRegional.Trim() + "" + vNumeroContrato + "" + vigencia.ToString(); //.Substring(2, 2).ToString();

                    return _IntegrationDAL.GetInfoContratoGeneralPorRubro(codigoRegional, vigencia, codigoRubro, consecutivo);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            
        }
    }
}
