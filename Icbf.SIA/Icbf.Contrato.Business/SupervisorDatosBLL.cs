﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class SupervisorDatosBLL
    {
        private SupervisorDatosDAL vSupervisorDatosDAL;

        public SupervisorDatosBLL()
        {
            vSupervisorDatosDAL = new SupervisorDatosDAL();
        }

        public List<SupervisorTipoIdentificacion> ConsultarSupervisorTipoIdentificacions()
        {
            try
            {
                return vSupervisorDatosDAL.ConsultarSupervisorTipoIdentificacions();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorTipoVinculacion
        /// </summary>
        public List<SupervisorTipoVinculacion> ConsultarSupervisorTipoVinculacions()
        {
            try
            {
                return vSupervisorDatosDAL.ConsultarSupervisorTipoVinculacions();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorRegional
        /// </summary>
        public List<SupervisorRegional> ConsultarSupervisorRegionals()
        {
            try
            {
                return vSupervisorDatosDAL.ConsultarSupervisorRegionals();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
