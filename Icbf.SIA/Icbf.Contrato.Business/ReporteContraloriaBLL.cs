﻿using ClosedXML.Excel;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class ReporteContraloriaBLL
    {
        private const string NIT_ICBF = "899999239";
        private const string DEFAULT_AMPARO_VALOR = "99999998 NO SE DILIGENCIA INFORMACIÓN PARA ESTE FORMULARIO EN ESTE PERÍODO DE REPORTE";
        private const string DEFAULT_GARANTIA_VALOR = "6 NO CONSTITUYÓ GARANTÍAS";
        private const string DEFAULT_FECHA = "1900/01/01";
        private const string DEFAULT_TIPO_SEGUIMIENTO = "4 NO SE DILIGENCIA INFORMACIÓN PARA ESTE FORMULARIO EN ESTE PERÍODO DE REPORTE";
        private const string DEFAULT_TIPO_IDENTIFICACION = "5 NO SE DILIGENCIA INFORMACIÓN PARA ESTE FORMULARIO EN ESTE PERÍODO DE REPORTE";
        private const string DEFAULT_DIGITO_VERIFICACION = "11 NO SE DILIGENCIA INFORMACIÓN PARA ESTE FORMULARIO EN ESTE PERÍODO DE REPORTE ";
        private const string DEFAULT_CLASE_CONVENIO1 = "1 CONTRATO / CONVENIO INTERADMINISTRATIVO";
        private const string DEFAULT_CLASE_CONVENIO2 = "2 CONVENIO DE COOPERACIÓN (NACIONAL / INTERNACIONAL)";

        private String [] unidades = 
        {"cero", "uno", "dos" ,"tres" ,"cuatro" ,"cinco" ,
            "seis" ,"siete" ,"ocho" ,"nueve","diez"};
        private String [] especiales = 
        {"once", "doce","trece","catorce", "quince", 
            "diezciseis", "diecisiete", "dieciocho", "diecinueve"};
        private String [] decenas = 
        {"veinte", "treinta","cuarenta","cincuenta", "sesenta",
            "setenta", "ochenta", "noventa"};

        private string[] tiposContratoConvenio = { "CONVENIO DE ASOCIACIÓN", "CONVENIO DE COOPERACION INTERNACIONAL" };

        private static string[] Celdas = 
            {
              "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
              "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV", "AW","AX","AY","AZ"
            };

        private ReporteContraloriaDAL _ReporteContraloriaDAL;
        private GarantiaDAL _GarantiaDAL;
        private SupervisorInterContratoDAL _SupervisorInterContrato;
        private AmparosGarantiasDAL _AmparosDAL;

        public ReporteContraloriaBLL()
        {
            _ReporteContraloriaDAL = new ReporteContraloriaDAL();
            _GarantiaDAL = new GarantiaDAL();
            _SupervisorInterContrato = new SupervisorInterContratoDAL();
            _AmparosDAL = new AmparosGarantiasDAL();
        }

        public string GenerarReporteContraloria(int vigencia, int periodo, string idRegional, string pathPlantilla)
        {
            string result = string.Empty;

           KeyValuePair<DateTime, DateTime> valoresRangosReporte = ObtenerRangosFechaReporte(vigencia, periodo);

           var fileResult = new XLWorkbook();

           var fileTemplate = new XLWorkbook(pathPlantilla);

           IXLWorksheet varF1 = GenerarF1(pathPlantilla,valoresRangosReporte.Key, valoresRangosReporte.Value,periodo,idRegional,fileTemplate);
           IXLWorksheet varF2 = GenerarF2(pathPlantilla, valoresRangosReporte.Key, valoresRangosReporte.Value, periodo, idRegional, fileTemplate);
           IXLWorksheet varF3 = GenerarF3(pathPlantilla, valoresRangosReporte.Key, valoresRangosReporte.Value, periodo, idRegional, fileTemplate);
           IXLWorksheet varF4 = GenerarF4(pathPlantilla, valoresRangosReporte.Key, valoresRangosReporte.Value, periodo, idRegional, fileTemplate);
           IXLWorksheet varF5 = GenerarF5(pathPlantilla, valoresRangosReporte.Key, valoresRangosReporte.Value, periodo, idRegional, fileTemplate);

            if(varF1 != null)
               fileResult.AddWorksheet(varF1);
            else
               fileResult.AddWorksheet(fileTemplate.Worksheet(1));

            if (varF2 != null)
                fileResult.AddWorksheet(varF2);
            else
                fileResult.AddWorksheet(fileTemplate.Worksheet(2));

            if (varF3 != null)
                fileResult.AddWorksheet(varF3);
            else
                fileResult.AddWorksheet(fileTemplate.Worksheet(3));

            if(varF4 != null)
                 fileResult.AddWorksheet(varF4);
            else
                fileResult.AddWorksheet(fileTemplate.Worksheet(4));
           
           if(varF5 != null)
               fileResult.AddWorksheet(varF5);
           else
               fileResult.AddWorksheet(fileTemplate.Worksheet(5));
          
            var id = Guid.NewGuid().ToString() + ".xlsx";
            var pathResult = System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"];
            fileResult.SaveAs(Path.Combine(pathResult,id));
            result = id;
           
           return result;
        }

        #region Generación F1

        private IXLWorksheet GenerarF1(string pathPlantilla, DateTime fechaInicio, DateTime fechaFinal, int periodo, string idRegional, XLWorkbook fileTemplate)
        {

            IXLWorksheet pestanaInit = null;

            pestanaInit = fileTemplate.Worksheets.First();
            pestanaInit.Cell("C5").Value = fechaFinal.ToShortDateString();


            var listaContratos = _ReporteContraloriaDAL.ObtenerInformacionBasica(fechaInicio, fechaFinal, idRegional, periodo);

            if (listaContratos != null && listaContratos.Count > 0)
            {
                int x = 11;

                var fileTemplateBase = new XLWorkbook(pathPlantilla);

                var pestanaBase = fileTemplateBase.Worksheets.First();

                string xy = string.Empty;
                string xyBase = string.Empty;

                pestanaInit.Row(12).Clear(XLClearOptions.ContentsAndFormats);
                pestanaInit.Row(13).Clear(XLClearOptions.ContentsAndFormats);

                foreach (var itemFila in listaContratos)
                {
                    if(x == 11)
                    {
                        string rrr = string.Empty;
                    }


                    for (int y = 0; y < Celdas.Length; y++)
                    {
                        xy = Celdas[y] + x;
                        xyBase = Celdas[y] + 11;

                        try
                        {
                            var resultxy = ObtenerValorCeldaF1(Celdas[y], itemFila);

                            if (Celdas[y] == "A")
                            {
                                pestanaInit.Cell(xy).Style = pestanaBase.Cell("A11").Style;
                                pestanaInit.Cell(xy).Value = (x - 11) + 1;
                            }
                            else if (Celdas[y] == "B")
                            {
                                pestanaInit.Cell(xy).Value = "FILA_" + ((x - 11) + 1);
                            }
                            else if (Celdas[y] == "E")
                            {
                                pestanaInit.Cell(xy).SetDataType( XLCellValues.Text);
                                pestanaInit.Cell(xy).SetValue<string>(resultxy[0]);
                            }
                            else if (Celdas[y] == "M")
                            {
                                pestanaInit.Cell(xy).Style.NumberFormat.Format = "$ #,##0.00";
                                pestanaInit.Cell(xy).SetValue<decimal>(decimal.Parse(resultxy[0]));                                
                            }
                            else if (Celdas[y] == "X")
                            {
                                pestanaInit.Cell(xy).Value = resultxy[0];
                                pestanaInit.Cell(Celdas[y + 2] + x).Style.DateFormat.SetFormat("YYYY/MM/DD");
                                pestanaInit.Cell(Celdas[y + 2] + x).Value = resultxy[1];
                                pestanaInit.Cell(Celdas[y + 1] + x).Value = resultxy[2];
                            }
                            else if (Celdas[y] == "AA")
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaInit.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaInit.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];
                                pestanaInit.Cell(Celdas[y + 1] + x).Value = resultxy[1];
                                pestanaInit.Cell(Celdas[y + 2] + x).Value = resultxy[2];
                                pestanaInit.Cell(Celdas[y + 3] + x).Value = resultxy[3];
                                pestanaInit.Cell(Celdas[y + 4] + x).Value = resultxy[4];
                                pestanaInit.Cell(Celdas[y + 5] + x).Value = resultxy[5];
                                pestanaInit.Cell(Celdas[y + 6] + x).Value = resultxy[6];
                                pestanaInit.Cell(Celdas[y + 7] + x).Value = resultxy[7];
                                pestanaInit.Cell(Celdas[y + 8] + x).Value = resultxy[8];
                                pestanaInit.Cell(Celdas[y + 9] + x).Value = resultxy[9];
                                pestanaInit.Cell(Celdas[y + 10] + x).Value = resultxy[10];
                                pestanaInit.Cell(Celdas[y + 11] + x).Value = resultxy[11];
                                pestanaInit.Cell(Celdas[y + 12] + x).Value = resultxy[12];
                            }
                            else if (Celdas[y] == "AW")
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaInit.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaInit.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];
                                pestanaInit.Cell(Celdas[y + 1] + x).Value = resultxy[1];
                                pestanaInit.Cell(Celdas[y + 2] + x).Value = resultxy[2];
                                pestanaInit.Cell(Celdas[y + 3] + x).Value = resultxy[3];
                            }
                            else if (resultxy.Count == 1)
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaBase.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaBase.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];
                            }
                        }
                        catch (Exception ex)
                        {
                            string resultEx = string.Format("Error F1, Fila:{0} - Msj:{1}", x.ToString(), ex.Message);
                            throw new Exception(resultEx);
                        }
                    }
                    x++;
                }

                var x1 = x + 1;

                for (int i = 0; i < Celdas.Length; i++)
                {
                    string xyEnd = Celdas[i] + x;
                    pestanaInit.Cell(xyEnd).Value = pestanaBase.Row(12).Cell(i + 1).Value;
                    pestanaInit.Cell(xyEnd).Style = pestanaBase.Row(12).Cell(i + 1).Style;

                    string xyEnd1 = Celdas[i] + x1;
                    pestanaInit.Cell(xyEnd1).Value = pestanaBase.Row(13).Cell(i + 1).Value;
                    pestanaInit.Cell(xyEnd1).Style = pestanaBase.Row(13).Cell(i + 1).Style;
                }
            }

            return pestanaInit;

        
        }

        private List<string> ObtenerValorCeldaF1(string xy, ReporteContraloria itemFila)
        {
            List<string> result = new List<string>();


            switch (xy)
            {
                case "C":
                    result.Add("1 SI");
                    break;
                case "D":
                    string valor = string.Format("{0}-{1}", itemFila.CodigoRegional, itemFila.NombreRegional);
                    result.Add(valor);
                    break;
                case "E":
                    result.Add(itemFila.NumeroContrato);
                    break;
                case "F":
                    result.Add(FormatDate(itemFila.FechaSuscripcion));
                    break;
                case "G":
                    result.Add(ObtenerCantidadVeces(itemFila.CantidadVeces));
                    break;
                case "H":
                    result.Add(itemFila.ObjetoContrato);
                    break;
                case "I":
                    result.Add(itemFila.ModalidadSeleccion);
                    break;
                case "J":
                    result.Add(itemFila.ClaseContrato);
                    break;
                case "K":
                    result.Add(itemFila.ClaseContratoOriginal);
                    break;
                case "L":
                    result.Add(itemFila.CodigoSecop);
                    break;
                case "M":
                    result.Add(itemFila.ValorInicial.ToString());
                    break;
                case "N":
                    result.Add(itemFila.EsContrato);
                    break;
                case "O":
                    result.Add(NIT_ICBF);
                    break;
                case "P":
                    result.Add("3 DV 2");
                    break;
                case "Q":
                    result.Add(itemFila.Naturaleza);
                    break;
                case "R":
                    if (itemFila.Naturaleza == "1 PERSONA NATURAL")
                    {
                        if (_ReporteContraloriaDAL.EsRUTPersonaNatural(itemFila.IdContrato))
                            result.Add("2 RUT - REGISTRO ÚNICO TRIBUTARIO");
                        else
                            result.Add(itemFila.TipoIdentificacion);
                    }
                    else
                        result.Add(itemFila.TipoIdentificacion);
                    break;
                case "S":
                    if (itemFila.Naturaleza == "1 PERSONA NATURAL" && itemFila.TipoIdentificacion != "4 CÉDULA DE EXTRANJERÍA")
                        result.Add(itemFila.NoIdentificacionContratista);
                    else
                        result.Add(string.Empty);
                    break;
                case "T":
                    if (itemFila.Naturaleza != "1 PERSONA NATURAL")
                        result.Add(itemFila.NoIdentificacionContratista);
                    else
                        result.Add(string.Empty);
                    break;
                case "U":
                    if (itemFila.Naturaleza != "1 PERSONA NATURAL")
                        result.Add(ObtenerDigitoVerificacion(itemFila.NoIdentificacionContratista));
                    else
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                    break;
                case "V":
                    if (itemFila.Naturaleza == "1 PERSONA NATURAL" && itemFila.TipoIdentificacion == "4 CÉDULA DE EXTRANJERÍA")
                        result.Add(itemFila.NoIdentificacionContratista);
                    else
                        result.Add(string.Empty);
                    break;
                case "W":
                    result.Add(itemFila.NombresRazonSocialContratista);
                    break;
                case "X":
                    result = ObtenerValoresGarantias(itemFila.IdContrato);
                    break;
                case "AA":
                    result = ObtenerValoresSupervisorInterventor(itemFila);
                    break;
                case "AN":
                    result.Add(itemFila.DiasTotales.ToString());
                    break;
                case "AO":
                    if (!string.IsNullOrEmpty(itemFila.FormaPago))
                        result.Add(itemFila.FormaPago);
                    else
                        result.Add("3 NO PACTADOS");
                    break;
                case "AP":
                    if (!string.IsNullOrEmpty(itemFila.FormaPago) && itemFila.FormaPago != "3 NO PACTADOS")
                    {
                        decimal valorAnticipado = itemFila.ValorInicial * itemFila.PorcentajeAnticipo / 100;
                        result.Add(valorAnticipado.ToString());
                    }
                    else
                        result.Add("0");
                    break;
                case "AQ":
                    result.Add(_ReporteContraloriaDAL.ExistenProrrogasAdiciones(itemFila.IdContrato));
                    break;
                case "AR":
                    result.Add(itemFila.ValorAdicionado.ToString());
                    break;
                case "AS":
                    result.Add(itemFila.DiasProrrogados.ToString());
                    break;
                case "AT":
                    result.Add(FormatDate(itemFila.FechaInicioContrato));
                    break;
                case "AU":
                    result.Add(FormatDate(itemFila.FechaFinalizacion));
                    break;
                case "AV":
                    result.Add(FormatDate(itemFila.FechaLiquidacion));
                    break;
                case "AW":
                    result = ObtenerValoresAvances(itemFila);
                    break;
                case "AY":
                    result = ObtenerValoresAvancesProgramado(itemFila);
                    break;
                case "AZ":
                    result = ObtenerValoresAvancesReal(itemFila);
                    break;
                default:
                    break;
            }

            return result;


        }

        #endregion

        #region Generación F2

        private IXLWorksheet GenerarF3(string pathPlantilla, DateTime fechaInicial, DateTime fechaFinal, int periodo, string idRegional, XLWorkbook fileTemplate)
        {
            IXLWorksheet pestanaInit = null;

            pestanaInit = fileTemplate.Worksheet(3);

            pestanaInit.Cell("C5").Value = fechaFinal.ToShortDateString();

            return pestanaInit;
        }

        #endregion

        #region Generación F3

        private IXLWorksheet GenerarF2(string pathPlantilla, DateTime fechaInicial, DateTime fechaFinal, int periodo, string idRegional, XLWorkbook fileTemplate)
        {
            IXLWorksheet pestanaInit = null;

            pestanaInit = fileTemplate.Worksheet(2);

            pestanaInit.Cell("C5").Value = fechaFinal.ToShortDateString();

            return pestanaInit;
        }

        #endregion

        #region Generación F4

        private IXLWorksheet GenerarF4(string pathPlantilla, DateTime fechaInicio, DateTime fechaFinal, int periodo, string idRegional, XLWorkbook fileTemplate)
        {
            IXLWorksheet pestanaInit = null;

            pestanaInit = fileTemplate.Worksheet(4);

            pestanaInit.Cell("C5").Value = fechaFinal.ToShortDateString();

            var listaContratos = _ReporteContraloriaDAL.ObtenerInformacionConvenios(fechaInicio, fechaFinal, idRegional,periodo);

            if (listaContratos != null && listaContratos.Count > 0)
            {
                int x = 11;

                var fileTemplateBase = new XLWorkbook(pathPlantilla);

                var pestanaBase = fileTemplateBase.Worksheet(4);

                string xy = string.Empty;
                string xyBase = string.Empty;

                pestanaInit.Row(12).Clear(XLClearOptions.ContentsAndFormats);
                pestanaInit.Row(13).Clear(XLClearOptions.ContentsAndFormats);

                foreach (var itemFila in listaContratos)
                {
                    for (int y = 0; y < 39; y++)
                    {
                        xy = Celdas[y] + x;
                        xyBase = Celdas[y] + 11;

                        try
                        {
                            var resultxy = ObtenerValorCeldaF4(Celdas[y], itemFila);

                            if (Celdas[y] == "A")
                            {
                                pestanaInit.Cell(xy).Style = pestanaBase.Cell("A11").Style;
                                pestanaInit.Cell(xy).Value = (x - 11) + 1;
                            }
                            else if (Celdas[y] == "B")
                            {
                                pestanaInit.Cell(xy).Value = "FILA_" + ((x - 11) + 1);
                            }
                            else if (Celdas[y] == "J")
                            {
                                pestanaInit.Cell(xy).Style.NumberFormat.Format = "$ #,##0.00";
                                pestanaInit.Cell(xy).SetValue<decimal>(decimal.Parse(resultxy[0]));   
                            }
                            else if (Celdas[y] == "G")
                            {
                                pestanaInit.Cell(xy).SetDataType(XLCellValues.Text);
                                pestanaInit.Cell(xy).SetValue<string>(resultxy[0]);
                            }                                
                            else if (Celdas[y] == "O")
                            {
                                pestanaInit.Cell(xy).Value = resultxy[0];
                                pestanaInit.Cell(Celdas[y + 1] + x).Value = resultxy[2];
                            }
                            else if (Celdas[y] == "Q")
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaInit.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaInit.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];

                                pestanaInit.Cell(Celdas[y + 1] + x).Value = resultxy[1];
                                pestanaInit.Cell(Celdas[y + 2] + x).Value = resultxy[2];
                                pestanaInit.Cell(Celdas[y + 3] + x).Value = resultxy[3];
                                pestanaInit.Cell(Celdas[y + 4] + x).Value = resultxy[4];
                                pestanaInit.Cell(Celdas[y + 5] + x).Value = resultxy[5];
                                pestanaInit.Cell(Celdas[y + 6] + x).Value = resultxy[6];
                                
                                pestanaInit.Cell(Celdas[y + 7] + x).Value = resultxy[7];
                                pestanaInit.Cell(Celdas[y + 8] + x).Value = resultxy[8];
                                pestanaInit.Cell(Celdas[y + 9] + x).Value = resultxy[9];
                                pestanaInit.Cell(Celdas[y + 10] + x).Value = resultxy[10];
                                pestanaInit.Cell(Celdas[y + 11] + x).Value = resultxy[11];
                            }
                            else if (Celdas[y] == "AJ")
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaInit.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaInit.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];
                                pestanaInit.Cell(Celdas[y + 1] + x).Value = resultxy[1];
                                pestanaInit.Cell(Celdas[y + 2] + x).Value = resultxy[2];
                                pestanaInit.Cell(Celdas[y + 3] + x).Value = resultxy[3];
                            }
                            else if (resultxy.Count == 1)
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaBase.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaBase.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];
                            }
                        }
                        catch (Exception ex)
                        {
                            string resultEx = string.Format("Error F4, Fila:{0} - Msj:{1}", x.ToString(), ex.Message);
                            throw new Exception(resultEx);
                        }
                    }
                    x++;
                }

                var x1 = x + 1;

                for (int i = 0; i < Celdas.Length; i++)
                {
                    string xyEnd = Celdas[i] + x;
                    pestanaInit.Cell(xyEnd).Value = pestanaBase.Row(12).Cell(i + 1).Value;
                    pestanaInit.Cell(xyEnd).Style = pestanaBase.Row(12).Cell(i + 1).Style;

                    string xyEnd1 = Celdas[i] + x1;
                    pestanaInit.Cell(xyEnd1).Value = pestanaBase.Row(13).Cell(i + 1).Value;
                    pestanaInit.Cell(xyEnd1).Style = pestanaBase.Row(13).Cell(i + 1).Style;
                }
            }

            return pestanaInit;
        }

        private List<string> ObtenerValorCeldaF4(string xy, ReporteContraloria itemFila)
        {
            List<string> result = new List<string>();


            switch (xy)
            {
                case "C":
                    result.Add("1 SI");
                    break;
                case "D":
                    string valor = string.Format("{0}-{1}", itemFila.CodigoRegional, itemFila.NombreRegional);
                    result.Add(valor);
                    break;
                case "E":
                    result.Add(itemFila.ClaseContrato);
                    break;
                case "F":
                    result.Add(itemFila.NumeroContrato);
                    break;
                case "G":
                    result.Add(FormatDate(itemFila.FechaSuscripcion));
                    break;
                case "H":
                    result.Add(ObtenerCantidadVeces(itemFila.CantidadVeces));
                    break;
                case "I":
                    result.Add(itemFila.ObjetoContrato);
                    break;
                case "J":
                    result.Add(itemFila.ValorInicial.ToString());
                    break;
                case "K":
                    result.Add(itemFila.NoIdentificacionContratista);
                    break;
                case "L":
                    result.Add(ObtenerDigitoVerificacion(itemFila.NoIdentificacionContratista));
                    break;
                case "M":
                    result.Add(itemFila.NombresRazonSocialContratista);
                    break;
                case "N":
                    result.Add(itemFila.DiasTotales.ToString());
                    break;
                case "O":
                    result = ObtenerValoresGarantias(itemFila.IdContrato);
                    break;
                case "Q":
                    result = ObtenerValoresSupervisorInterventorF4(itemFila);
                    break;
                case "AC":
                    result.Add(itemFila.DiasTotales.ToString());
                    break;
                case "AD":
                    result.Add(_ReporteContraloriaDAL.ExistenProrrogasAdiciones(itemFila.IdContrato));
                    break;
                case "AE":
                    result.Add(itemFila.ValorAdicionado.ToString());
                    break;
                case "AF":
                    result.Add(itemFila.DiasProrrogados.ToString());
                    break;
                case "AG":
                    result.Add(FormatDate(itemFila.FechaInicioContrato));
                    break;
                case "AH":
                    result.Add(FormatDate(itemFila.FechaFinalizacion));
                    break;
                case "AI":
                    result.Add(FormatDate(itemFila.FechaLiquidacion));
                    break;
                case "AJ":
                    result = ObtenerValoresAvances(itemFila);
                    break;
                case "AL":
                    result = ObtenerValoresAvancesProgramado(itemFila);
                    break;
                case "AM":
                    result = ObtenerValoresAvancesReal(itemFila);
                    break;
                default:
                    break;
            }

            return result;
        }

        #endregion

        #region Generación F5

        private IXLWorksheet GenerarF5(string pathPlantilla, DateTime fechaInicio, DateTime fechaFinal, int periodo, string idRegional, XLWorkbook fileTemplate)
        {
            IXLWorksheet pestanaInit = null;

            pestanaInit = fileTemplate.Worksheet(5);

            pestanaInit.Cell("C5").Value = fechaFinal.ToShortDateString();

            var listaContratos = _ReporteContraloriaDAL.ObtenerInformacionConsorciosUnionesTemporales(fechaInicio, fechaFinal, idRegional);

            if (listaContratos != null && listaContratos.Count > 0)
            {
                int x = 11;

                var fileTemplateBase = new XLWorkbook(pathPlantilla);


                var pestanaBase = fileTemplateBase.Worksheet(5);

                string xy = string.Empty;
                string xyBase = string.Empty;

                pestanaInit.Row(12).Clear(XLClearOptions.ContentsAndFormats);
                pestanaInit.Row(13).Clear(XLClearOptions.ContentsAndFormats);

                foreach (var itemFila in listaContratos)
                {
                    for (int y = 0; y < Celdas.Length; y++)
                    {
                        xy = Celdas[y] + x;
                        xyBase = Celdas[y] + 11;

                        try
                        {
                            var resultxy = ObtenerValorCeldaF5(Celdas[y], itemFila);

                            if (Celdas[y] == "A")
                            {
                                pestanaInit.Cell(xy).Style = pestanaBase.Cell("A11").Style;
                                pestanaInit.Cell(xy).Value = (x - 11) + 1;
                            }
                            else if (Celdas[y] == "B")
                            {
                                pestanaInit.Cell(xy).Value = "FILA_" + ((x - 11) + 1);
                            }
                            else if (resultxy.Count == 1)
                            {
                                pestanaInit.Cell(xy).SetDataType(pestanaBase.Cell(xyBase).DataType);
                                pestanaInit.Cell(xy).Style.DateFormat.SetFormat(pestanaBase.Cell(xyBase).Style.DateFormat.Format);
                                pestanaInit.Cell(xy).Value = resultxy[0];
                            }
                        }
                        catch (Exception ex)
                        {
                            string resultEx = string.Format("Error F5, Fila:{0} - Msj:{1}", x.ToString(), ex.Message);
                            throw new Exception(resultEx);
                        }
                    }
                    x++;
                }

                var x1 = x + 1;

                for (int i = 0; i < Celdas.Length; i++)
                {
                    string xyEnd = Celdas[i] + x;
                    pestanaInit.Cell(xyEnd).Value = pestanaBase.Row(12).Cell(i + 1).Value;
                    pestanaInit.Cell(xyEnd).Style = pestanaBase.Row(12).Cell(i + 1).Style;

                    string xyEnd1 = Celdas[i] + x1;
                    pestanaInit.Cell(xyEnd1).Value = pestanaBase.Row(13).Cell(i + 1).Value;
                    pestanaInit.Cell(xyEnd1).Style = pestanaBase.Row(13).Cell(i + 1).Style;
                }
            }

            return pestanaInit;
        }

        private List<string> ObtenerValorCeldaF5(string xy, ReporteContraloriaUnionesConsorcios itemFila)
        {
            List<string> result = new List<string>();


            switch (xy)
            {
                case "C":
                    result.Add("1 SI");
                    break;
                case "D":
                    string valor = string.Format("{0}-{1}", itemFila.CodigoRegional, itemFila.NombreRegional);
                    result.Add(valor);
                    break;
                case "E":
                    result.Add(itemFila.NumeroContrato);
                    break;
                case "F":
                    result.Add(FormatDate(itemFila.FechaSuscripcion));
                    break;
                case "G":
                    if (itemFila.CodigoNaturaleza == "003")
                        result.Add("1 CONSORCIO");
                    else if (itemFila.CodigoNaturaleza == "004")
                        result.Add("2 UNIÓN TEMPORAL");
                    else
                        result.Add("99999998 NO SE DILIGENCIA INFORMACIÓN PARA ESTE FORMULARIO EN ESTE PERÍODO DE REPORTE");
                    break;
                case "H":
                    result.Add(itemFila.NoIdentificacionContratista);
                    break;
                case "I":
                    result.Add(ObtenerDigitoVerificacion(itemFila.NoIdentificacionContratista));
                    break;
                case "J":
                    result.Add(itemFila.NombresRazonSocialContratista);
                    break;
                case "K":
                    result.Add(itemFila.NaturalezaIntegrante);
                    break;
                case "L":
                    result.Add(itemFila.TipoIdentificacionIntegrante);
                    break;
                case "M":
                    if (itemFila.CodigoIdentificacionIntegrante == "CC" || itemFila.CodigoIdentificacionIntegrante == "RUT")
                        result.Add(itemFila.NoIdentificacionIntegrante);
                    else
                        result.Add(string.Empty);
                    break;
                case "N":
                    if (itemFila.CodigoIdentificacionIntegrante == "NIT")
                        result.Add(itemFila.NoIdentificacionIntegrante);
                    else
                        result.Add(string.Empty);
                    break;
                case "O":
                    if (itemFila.CodigoIdentificacionIntegrante == "NIT" || itemFila.CodigoIdentificacionIntegrante == "RUT")
                        result.Add(ObtenerDigitoVerificacion(itemFila.NoIdentificacionIntegrante));
                    else
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                    break;
                case "P":
                    if (itemFila.CodigoIdentificacionIntegrante == "CE")
                        result.Add(itemFila.NoIdentificacionIntegrante);
                    else
                        result.Add(string.Empty);
                    break;
                case "Q":
                    result.Add(itemFila.NombresRazonSocialIntegrante);
                    break;
                default:
                    break;
            }

            return result;
        }

        #endregion

        #region Metodos Axulirares Generación Reporte

        private string ObtenerCantidadVeces(int cantidad)
        {
            string result = string.Empty;

            if (cantidad == 0)
                result = "51 NO SE DILIGENCIA INFORMACIÓN PARA ESTE FORMULARIO EN ESTE PERÍODO DE REPORTE";
            else if (cantidad == 1)
                result = "1 PRIMER VEZ";
            else
                result = string.Format("{0} {1} VECES", cantidad, NumerosLetras(cantidad));
            
            return result;
        }

        private string NumerosLetras(int num)
        {
            string result = string.Empty;

             if(num>=0 && num<11)
              result = unidades[num];         
             else if(num<20)
              result = especiales[num-11];        
             else if(num<100)
             {
                int unid = num % 10;
                int dec = num/10;
                if(unid == 0)
                 result = decenas[dec-2];                
                else
                  result = decenas[dec-2] + " y " + unidades[unid];
             }

             return result.ToUpper();
        }

        private List<string> ObtenerValoresGarantias(int idContrato)
        {
            List<string> result = new List<string>();

            var garantias = _GarantiaDAL.ConsultarInfoGarantias(idContrato);

            if (garantias.Count > 0)
            {
                var garantia = garantias.First();

                if (!string.IsNullOrEmpty(garantia.ValorSIRECI))
                    result.Add(garantia.ValorSIRECI);
                else
                    result.Add(DEFAULT_GARANTIA_VALOR);

                if (garantias.First().FechaExpedicionGarantia.HasValue)
                    result.Add(FormatDate(garantias.First().FechaExpedicionGarantia.Value));
                else
                    result.Add(DEFAULT_FECHA);

                var amparos = _AmparosDAL.ConsultarAmparosGarantiass(null, null, null, null, garantia.IDGarantia);

                if (amparos.Count > 0)
                {
                    if (amparos.Count == 1 && ! string.IsNullOrEmpty(amparos.First().valorSIRECI))
                        result.Add(amparos.First().valorSIRECI);
                    else if (amparos.Count == 2)
                    {
                        if (amparos.Any(ame => ame.IdTipoAmparo == 2) && amparos.Any(ame => ame.IdTipoAmparo == 8))
                            result.Add("45 CUMPLIM+ CALIDAD DL SERVICIO");
                        else if (amparos.Any(ame => ame.IdTipoAmparo == 2) && amparos.Any(ame => ame.IdTipoAmparo == 4))
                            result.Add("41 CUMPLIM+ PAGO D SALARIOS_PRESTAC SOC LEGALES");
                        else if (amparos.Any(ame => ame.IdTipoAmparo == 28) && amparos.Any(ame => ame.IdTipoAmparo == 4))
                            result.Add("46 CUMPLIM+ ESTABIL_CALIDAD D OBRA+ PAGO D SALARIOS_PRESTAC SOC LEGALES");
                        else if (amparos.Any(ame => ame.IdTipoAmparo == 4) && amparos.Any(ame => ame.IdTipoAmparo == 5))
                            result.Add("76 PAGO D SALARIOS_PRESTAC SOC LEG + RESPONSAB EXTRACONTRACTUAL");
                        else
                            result.Add(DEFAULT_AMPARO_VALOR);
                    }
                    else if (amparos.Count == 3)
                    {
                        if (amparos.Any(ame => ame.IdTipoAmparo == 2) && amparos.Any(ame => ame.IdTipoAmparo == 3) && amparos.Any(ame => ame.IdTipoAmparo == 4))
                            result.Add("46 CUMPLIM+ ESTABIL_CALIDAD D OBRA+ PAGO D SALARIOS_PRESTAC SOC LEGALES");
                        else
                            result.Add(DEFAULT_AMPARO_VALOR);
                    }
                    else if (amparos.Count == 4)
                    {
                        if (amparos.Any(ame => ame.IdTipoAmparo == 2) && amparos.Any(ame => ame.IdTipoAmparo == 4) && amparos.Any(ame => ame.IdTipoAmparo == 28) && amparos.Any(ame => ame.IdTipoAmparo == 5))
                            result.Add("47 CUMPLIM+ ESTABIL_CALIDAD D OBRA+ RESPONSAB EXTRACONTRACTUAL");
                        else
                            result.Add(DEFAULT_AMPARO_VALOR);                        
                    }
                    else
                        result.Add(DEFAULT_AMPARO_VALOR);
                }
                else
                result.Add(DEFAULT_AMPARO_VALOR);
            }
            else
            {
                result.Add(DEFAULT_GARANTIA_VALOR);
                result.Add(DEFAULT_FECHA);
                result.Add(DEFAULT_AMPARO_VALOR);
            }
            return result;
        }

        private List<string> ObtenerValoresSupervisorInterventor(int idContrato)
        {
            List<string> result = new List<string>();

            var supervisoresInter = _SupervisorInterContrato.ObtenerSupervisoresInterventoresContrato(idContrato, false);

            if (supervisoresInter != null && supervisoresInter.Count > 0)
            {
                SupervisorInterContrato supervisor = null;
                SupervisorInterContrato interventor = null;

                if (supervisoresInter.Any(e1 => e1.IDTipoSuperInter == 1) && supervisoresInter.Any(e1 => e1.IDTipoSuperInter == 2))
                {
                    supervisor = supervisoresInter.First(e1 => e1.IDTipoSuperInter == 1);
                    interventor = supervisoresInter.First(e1 => e1.IDTipoSuperInter == 2);
                    result.Add("3 INTERVENTOR y SUPERVISOR");
                }
                else if (supervisoresInter.Any(e1 => e1.IDTipoSuperInter == 1))
                {
                    supervisor = supervisoresInter.First(e1 => e1.IDTipoSuperInter == 1);
                    result.Add("2 SUPERVISOR");
                }
                else if (supervisoresInter.Any(e1 => e1.IDTipoSuperInter == 2))
                {
                    interventor = supervisoresInter.First(e1 => e1.IDTipoSuperInter == 2);
                    result.Add("1 INTERVENTOR");
                }

                if (interventor != null)
                {
                    if (interventor.TipoIdentificacion == "CEDULA DE CIUDADANIA")
                    {
                        result.Add("3 CÉDULA DE CIUDADANÍA");
                        result.Add(interventor.Identificacion);
                        result.Add(string.Empty);
                        result.Add(ObtenerDigitoVerificacion(interventor.Identificacion));
                        result.Add(string.Empty);
                        result.Add(interventor.SupervisorInterventor.NombreCompleto);
                    }
                    else if (interventor.TipoIdentificacion == "NUMERO DE IDENTIFICACION TRIBUTARIA")
                    {
                        result.Add("1 NIT");
                        result.Add(string.Empty);
                        result.Add(interventor.Identificacion);
                        result.Add(ObtenerDigitoVerificacion(interventor.Identificacion));
                        result.Add(string.Empty);
                        result.Add(interventor.SupervisorInterventor.NombreCompleto);
                    }
                    else if (interventor.TipoIdentificacion == "CEDULA DE EXTRANJERIA")
                    {
                        result.Add("4 CÉDULA DE EXTRANJERÍA");
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(interventor.Identificacion);
                        result.Add(interventor.SupervisorInterventor.NombreCompleto);
                    }
                }
                else
                {
                    result.Add(DEFAULT_TIPO_IDENTIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                    result.Add(DEFAULT_DIGITO_VERIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                }

                if (supervisor != null)
                {
                    if (supervisor.TipoIdentificacion == "CÉDULA CIUDADANIA")
                    {
                        result.Add("3 CÉDULA DE CIUDADANÍA");
                        result.Add(supervisor.Identificacion);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(string.Empty);
                        result.Add(supervisor.SupervisorInterventor.NombreCompleto);
                    }
                    else if (interventor.TipoIdentificacion == "CÉDULA EXTRANJERIA")
                    {
                        result.Add("4 CÉDULA DE EXTRANJERÍA");
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(supervisor.Identificacion);
                        result.Add(supervisor.SupervisorInterventor.NombreCompleto);
                    }
                }
                else
                {
                    result.Add(DEFAULT_TIPO_IDENTIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                    result.Add(DEFAULT_DIGITO_VERIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                }
            }
            else
            {
                result.Add(DEFAULT_TIPO_SEGUIMIENTO);
                result.Add(DEFAULT_TIPO_IDENTIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_DIGITO_VERIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_TIPO_IDENTIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_DIGITO_VERIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
            }

            return result;
        }

        private List<string> ObtenerValoresSupervisorInterventor(ReporteContraloria item)
        {
            List<string> result = new List<string>();

            if (!string.IsNullOrEmpty(item.IdentificacionSupervisor) ||  !string.IsNullOrEmpty(item.IdentificacionInterventor) )
            {
                if (!string.IsNullOrEmpty(item.IdentificacionSupervisor) && !string.IsNullOrEmpty(item.IdentificacionInterventor))
                    result.Add("3 INTERVENTOR y SUPERVISOR");
                else if (!string.IsNullOrEmpty(item.IdentificacionSupervisor))
                    result.Add("2 SUPERVISOR");
                else if (!string.IsNullOrEmpty(item.IdentificacionInterventor))
                    result.Add("1 INTERVENTOR");

                if (!string.IsNullOrEmpty(item.IdentificacionInterventor))
                {
                    if (item.TipoIdentificacionInterventor.ToUpper() == "CEDULA DE CIUDADANIA")
                    {
                        result.Add("3 CÉDULA DE CIUDADANÍA");
                        result.Add(item.IdentificacionInterventor);
                        result.Add(string.Empty);
                        result.Add(ObtenerDigitoVerificacion(item.IdentificacionInterventor));
                        result.Add(string.Empty);
                        result.Add(item.NombreInterventor);
                    }
                    else if (item.TipoIdentificacionInterventor.ToUpper() == "NUMERO DE IDENTIFICACION TRIBUTARIA")
                    {
                        result.Add("1 NIT");
                        result.Add(string.Empty);
                        result.Add(item.IdentificacionInterventor);
                        result.Add(ObtenerDigitoVerificacion(item.IdentificacionInterventor));
                        result.Add(string.Empty);
                        result.Add(item.NombreInterventor);
                    }
                    else if (item.TipoIdentificacionInterventor.ToUpper() == "CEDULA DE EXTRANJERIA")
                    {
                        result.Add("4 CÉDULA DE EXTRANJERÍA");
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(item.IdentificacionInterventor);
                        result.Add(item.NombreInterventor);
                    }
                }
                else
                {
                    result.Add(DEFAULT_TIPO_IDENTIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                    result.Add(DEFAULT_DIGITO_VERIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                }

                if (!string.IsNullOrEmpty(item.IdentificacionSupervisor))
                {
                    if (item.TipoIdentificacionSupervisor.ToUpper() == "CÉDULA CIUDADANIA")
                    {
                        result.Add("3 CÉDULA DE CIUDADANÍA");
                        result.Add(item.IdentificacionSupervisor);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(string.Empty);
                        result.Add(item.NombresSupervisor);
                    }
                    else if (item.TipoIdentificacionSupervisor.ToUpper() == "CÉDULA EXTRANJERIA")
                    {
                        result.Add("4 CÉDULA DE EXTRANJERÍA");
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(item.IdentificacionSupervisor);
                        result.Add(item.NombresSupervisor);
                    }
                    else
                    {
                        result.Add(DEFAULT_TIPO_IDENTIFICACION);
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                    }
                }
                else
                {
                    result.Add(DEFAULT_TIPO_IDENTIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                    result.Add(DEFAULT_DIGITO_VERIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                }
            }
            else
            {
                result.Add(DEFAULT_TIPO_SEGUIMIENTO);
                result.Add(DEFAULT_TIPO_IDENTIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_DIGITO_VERIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_TIPO_IDENTIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_DIGITO_VERIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
            }

            return result;
        }

        private List<string> ObtenerValoresSupervisorInterventorF4(ReporteContraloria item)
        {
            List<string> result = new List<string>();

            if (!string.IsNullOrEmpty(item.IdentificacionSupervisor) || !string.IsNullOrEmpty(item.IdentificacionInterventor))
            {
                if (!string.IsNullOrEmpty(item.IdentificacionSupervisor) && !string.IsNullOrEmpty(item.IdentificacionInterventor))
                    result.Add("3 INTERVENTOR y SUPERVISOR");
                else if (!string.IsNullOrEmpty(item.IdentificacionSupervisor))
                    result.Add("2 SUPERVISOR");
                else if (!string.IsNullOrEmpty(item.IdentificacionInterventor))
                    result.Add("1 INTERVENTOR");

                if (!string.IsNullOrEmpty(item.IdentificacionInterventor))
                {
                    if (item.TipoIdentificacionInterventor.ToUpper() == "CEDULA DE CIUDADANIA")
                    {
                        result.Add("3 CÉDULA DE CIUDADANÍA");
                        result.Add(item.IdentificacionInterventor);
                        result.Add(string.Empty);
                        result.Add(ObtenerDigitoVerificacion(item.IdentificacionInterventor));
                        result.Add(string.Empty);
                        result.Add(item.NombreInterventor);
                    }
                    else if (item.TipoIdentificacionInterventor.ToUpper() == "NUMERO DE IDENTIFICACION TRIBUTARIA")
                    {
                        result.Add("1 NIT");
                        result.Add(string.Empty);
                        result.Add(item.IdentificacionInterventor);
                        result.Add(ObtenerDigitoVerificacion(item.IdentificacionInterventor));
                        result.Add(string.Empty);
                        result.Add(item.NombreInterventor);
                    }
                    else if (item.TipoIdentificacionInterventor.ToUpper() == "CEDULA DE EXTRANJERIA")
                    {
                        result.Add("4 CÉDULA DE EXTRANJERÍA");
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(item.IdentificacionInterventor);
                        result.Add(item.NombreInterventor);
                    }
                }
                else
                {
                    result.Add(DEFAULT_TIPO_IDENTIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                    result.Add(DEFAULT_DIGITO_VERIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                }

                if (!string.IsNullOrEmpty(item.IdentificacionSupervisor))
                {
                    if (item.TipoIdentificacionSupervisor.ToUpper() == "CÉDULA CIUDADANIA" )
                    {
                        result.Add("3 CÉDULA DE CIUDADANÍA");
                        result.Add(item.IdentificacionSupervisor);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(item.NombresSupervisor);
                    }
                    else if (item.TipoIdentificacionSupervisor.ToUpper() == "CÉDULA EXTRANJERIA")
                    {
                        result.Add("4 CÉDULA DE EXTRANJERÍA");
                        result.Add(item.IdentificacionSupervisor);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(item.NombresSupervisor);
                    }
                    else
                    {
                        result.Add(DEFAULT_TIPO_IDENTIFICACION);
                        result.Add(string.Empty);
                        result.Add(string.Empty);
                        result.Add(DEFAULT_DIGITO_VERIFICACION);
                        result.Add(string.Empty);
                    }
                }
                else
                {
                    result.Add(DEFAULT_TIPO_IDENTIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                    result.Add(DEFAULT_DIGITO_VERIFICACION);
                    result.Add(string.Empty);
                    result.Add(string.Empty);
                }
            }
            else
            {
                result.Add(DEFAULT_TIPO_SEGUIMIENTO);
                
                result.Add(DEFAULT_TIPO_IDENTIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_DIGITO_VERIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);

                result.Add(DEFAULT_TIPO_IDENTIFICACION);
                result.Add(string.Empty);
                result.Add(string.Empty);
                result.Add(DEFAULT_DIGITO_VERIFICACION);
                result.Add(string.Empty);
            }

            return result;
        }


        private string ObtenerDigitoVerificacion(string nit)
        {
            string result = string.Empty;

            int resultD = 0;

            if (!string.IsNullOrEmpty(nit))
            {
                char[] a = nit.ToArray();
                int[] valores = {3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 61, 67, 71 };

                for (int j = 0; j < nit.Length; j++)
                {
                    resultD += int.Parse(nit[j].ToString()) * valores[nit.Length - j];
                }

                resultD = resultD % 11;

                if (resultD > 1)
                    resultD = 11 - resultD;
            }

            switch (resultD)
            {
                case 0:
                    result = "1 DV 0";
                    break;
                case 1:
                    result = "2 DV 1";
                    break;
                case 2:
                    result = "3 DV 2";
                    break;
                case 3:
                    result = "4 DV 3";
                    break;
                case 4:
                    result = "5 DV 4";
                    break;
                case 5:
                    result = "6 DV 5";
                    break;
                case 6:
                    result = "7 DV 6";
                    break;
                case 7:
                    result = "8 DV 7";
                    break;
                case 8:
                    result = "9 DV 8";
                    break;
                case 9:
                    result = "10 DV 9";
                    break;
                default:
                    result = DEFAULT_DIGITO_VERIFICACION;
                    break;
            }

            return result;
        }

        private KeyValuePair<DateTime, DateTime> ObtenerRangosFechaReporte(int vigencia, int periodo)
        {
            KeyValuePair<DateTime,DateTime> result;

            DateTime fechaInicio = DateTime.MinValue, fechaFin = DateTime.MinValue;

            switch (periodo)
            {
                case 1:
                    fechaInicio = new DateTime(vigencia, 1, 1);
                    fechaFin = new DateTime(vigencia, 3, 31);
                    break;
                case 2:
                    fechaInicio = new DateTime(vigencia, 4, 1);
                    fechaFin = new DateTime(vigencia, 6, 30);
                    break;
                case 3:
                    fechaInicio = new DateTime(vigencia, 7, 1);
                    fechaFin = new DateTime(vigencia, 9, 30);
                    break;
                case 4:
                    fechaInicio = new DateTime(vigencia, 10, 1);
                    fechaFin = new DateTime(vigencia, 12, 31);
                    break;
                default:
                    break;
            }

            result = new KeyValuePair<DateTime, DateTime>(fechaInicio, fechaFin);

            return result;
        }

        private string FormatDate( DateTime fecha)
        {
            if (fecha != null && fecha != DateTime.MinValue)
                return string.Format("{0}/{1}/{2}", fecha.Year, fecha.Month < 10 ? "0"+fecha.Month.ToString(): fecha.Month.ToString(), fecha.Day < 10 ? "0"+fecha.Day.ToString(): fecha.Day.ToString());
            else
                return string.Empty;
        }

        private List<string> ObtenerValoresAvances(ReporteContraloria itemFila)
        {
            List<string> resultList = new List<string>();

            decimal diasActual = ObtenerdiasEntreFechas360(itemFila.FechaInicioContrato, DateTime.Now.Date);

            decimal diasContrato = ObtenerdiasEntreFechas360(itemFila.FechaInicioContrato, itemFila.FechaFinalizacion);

            decimal valorDiferenciaDias = Math.Round((diasActual / diasContrato) * 100);

            if (valorDiferenciaDias > 100)
                valorDiferenciaDias = 100;

            resultList.Add(valorDiferenciaDias+"%");

            if (itemFila.FechaTerminacionAnticipada.HasValue)
            {
                var diasTerminacionAnticiapada = ObtenerdiasEntreFechas360(itemFila.FechaInicioContrato, itemFila.FechaTerminacionAnticipada.Value);
                var valorDiferenciaDiasTerminacion = Math.Round((diasTerminacionAnticiapada / diasContrato) * 100); 

                if (valorDiferenciaDiasTerminacion > 100)
                    valorDiferenciaDiasTerminacion = 100;

                resultList.Add(valorDiferenciaDiasTerminacion + "%");
            }
            else
                resultList.Add(valorDiferenciaDias + "%");

            resultList.Add(string.Empty);
            resultList.Add(string.Empty);

            return resultList;
        }

        private List<string> ObtenerValoresAvancesReal(ReporteContraloria itemFila)
        {
            List<string> resultList = new List<string>();
            decimal Porcentaje = 0;
            
            if (itemFila.ValorFinal > 0)
            {
                Porcentaje = (itemFila.ValorProgramado / itemFila.ValorFinal) * 100;

                if (Porcentaje > 100)
                    Porcentaje = 100;

                Porcentaje = Math.Round(Porcentaje, 2);
            }

            resultList.Add(Porcentaje + "%");
            return resultList;
        }

        private List<string> ObtenerValoresAvancesProgramado(ReporteContraloria itemFila)
        {
            List<string> resultList = new List<string>();
            decimal Porcentaje = 0;
            
            if (itemFila.ValorInicial > 0)
            {
                Porcentaje = (itemFila.ValorProgramado / itemFila.ValorInicial) * 100;

                if (Porcentaje > 100)
                    Porcentaje = 100;

                Porcentaje = Math.Round(Porcentaje, 2);
            }

            resultList.Add(Porcentaje + "%");
            return resultList;
        }

        private static int ObtenerdiasEntreFechas360(DateTime fechaInicial, DateTime fechafinal)
        {
            fechafinal = fechafinal.AddDays(1);

            int result = 0;
            int ai, mi, di;
            int af, mf, df;

            ai = fechaInicial.Year;
            mi = fechaInicial.Month;
            di = fechaInicial.Day;

            af = fechafinal.Year;
            mf = fechafinal.Month;
            df = fechafinal.Day;


            if (di == 31 || (mi == 2 && di > 27))
                di = 30;
            if (df > 27 && mf == 2)
                df = 30;
            if (df == 31 && di < 30)
            {
                mf++;
                df = 1;
            }
            else if (df == 31)
                df = 30;
            else
            {
                if (di == 31 || (mi == 2 && di > 27))
                    di = 30;
                if (df == 31 || (mf == 2 && df > 27))
                    df = 30;
            }

            if (Math.Abs(af - ai) == 0)
                result = (mf - mi) * 30 + df - di;
            else
                result = Math.Abs(af - ai - 1) * 360 + 360 - mi * 30 + 30 - di + 30 * (mf - 1) + df;


            return result;
        }

        #endregion
    }
}
