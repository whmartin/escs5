using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class SubComponenteBLL
    {
        private SubComponenteDAL vSubComponenteDAL;
        public SubComponenteBLL()
        {
            vSubComponenteDAL = new SubComponenteDAL();
        }
        public int InsertarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                return vSubComponenteDAL.InsertarSubComponente(pSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                return vSubComponenteDAL.ModificarSubComponente(pSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarSubComponente(SubComponente pSubComponente)
        {
            try
            {
                return vSubComponenteDAL.EliminarSubComponente(pSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SubComponente ConsultarSubComponente(int pIdSubComponente)
        {
            try
            {
                return vSubComponenteDAL.ConsultarSubComponente(pIdSubComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SubComponente> ConsultarSubComponentes(String pNombreSubComponente, int? pIdComponente, int? pEstado)
        {
            try
            {
                return vSubComponenteDAL.ConsultarSubComponentes(pNombreSubComponente, pIdComponente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
