using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Garantia
    /// </summary>
    public class GarantiaBLL
    {
        private GarantiaDAL vGarantiaDAL;
        public GarantiaBLL()
        {
            vGarantiaDAL = new GarantiaDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int InsertarGarantia(Garantia pGarantia)
        {
            try
            {
                return vGarantiaDAL.InsertarGarantia(pGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int ModificarGarantia(Garantia pGarantia)
        {
            try
            {
                return vGarantiaDAL.ModificarGarantia(pGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Garantia
        /// </summary>
        /// <param name="pGarantia"></param>
        public int EliminarGarantia(Garantia pGarantia)
        {
            try
            {
                return vGarantiaDAL.EliminarGarantia(pGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Garantia
        /// </summary>
        /// <param name="pIDGarantia"></param>
        public Garantia ConsultarGarantia(int pIDGarantia)
        {
            try
            {
                return vGarantiaDAL.ConsultarGarantia(pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Garantia
        /// </summary>
        /// <param name="pIDGarantia"></param>
        public Garantia ConsultarMaximaGarantia(int pIDGarantia)
        {
            try
            {
                return vGarantiaDAL.ConsultarMaximaGarantia(pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Garantia
        /// </summary>
        /// <param name="pIDTipoGarantia"></param>
        /// <param name="pNumeroGarantia"></param>
        /// <param name="pAprobada"></param>
        /// <param name="pDevuelta"></param>
        /// <param name="pFechaAprobacionGarantia"></param>
        /// <param name="pFechaCertificacionGarantia"></param>
        /// <param name="pFechaDevolucion"></param>
        /// <param name="pMotivoDevolucion"></param>
        /// <param name="pIDUsuarioAprobacion"></param>
        /// <param name="pIDUsuarioDevolucion"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pBeneficiarioICBF"></param>
        /// <param name="pDescripcionBeneficiarios"></param>
        /// <param name="pFechaInicioGarantia"></param>
        /// <param name="pFechaExpedicionGarantia"></param>
        /// <param name="pFechaVencimientoInicialGarantia"></param>
        /// <param name="pFechaVencimientoFinalGarantia"></param>
        /// <param name="pFechaReciboGarantia"></param>
        /// <param name="pValorGarantia"></param>
        /// <param name="pAnexos"></param>
        /// <param name="pObservacionesAnexos"></param>
        /// <param name="pEstado"></param>
        /// <param name="pEntidadProvOferenteAseguradora"></param>
        /// <param name="pIDSucursalAseguradoraContrato"></param>
        /// <param name="pIDEstadosGarantias"></param>
        public List<Garantia> ConsultarGarantias(int? pIDTipoGarantia, String pNumeroGarantia, DateTime? pFechaAprobacionGarantia, DateTime? pFechaCertificacionGarantia, DateTime? pFechaDevolucion, String pMotivoDevolucion, int? pIDUsuarioAprobacion, int? pIDUsuarioDevolucion, int? pIdContrato, Boolean? pBeneficiarioICBF, Boolean? pBeneficiarioOTROS, String pDescripcionBeneficiarios, DateTime? pFechaInicioGarantia, DateTime? pFechaExpedicionGarantia,
            DateTime? pFechaVencimientoInicialGarantia, DateTime? pFechaVencimientoFinalGarantia, DateTime? pFechaReciboGarantia, String pValorGarantia, Boolean? pAnexos,
            String pObservacionesAnexos, int? pEntidadProvOferenteAseguradora, Boolean? pEstado, int? pIDSucursalAseguradoraContrato, int? pIDEstadosGarantias, String pCodEstadoContrato, String pCodEstadoGarantia)
        {
            try
            {
                return vGarantiaDAL.ConsultarGarantias(pIDTipoGarantia, pNumeroGarantia, pFechaAprobacionGarantia, pFechaCertificacionGarantia, pFechaDevolucion, pMotivoDevolucion, pIDUsuarioAprobacion, pIDUsuarioDevolucion, pIdContrato, pBeneficiarioICBF, pBeneficiarioOTROS, pDescripcionBeneficiarios, pFechaInicioGarantia, pFechaExpedicionGarantia, pFechaVencimientoInicialGarantia,
                    pFechaVencimientoFinalGarantia, pFechaReciboGarantia, pValorGarantia, pAnexos, pObservacionesAnexos, pEntidadProvOferenteAseguradora, pEstado, pIDSucursalAseguradoraContrato, pIDEstadosGarantias, pCodEstadoContrato, pCodEstadoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica la existencia de un numero de garantia existente
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public bool ExisteNumeroGarantia(String pNumerogarantia)
        {
            try
            {
                return vGarantiaDAL.ExisteNumeroGarantia(pNumerogarantia)
                ;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int existeGarantiaContrato(String pNumerogarantia)
        {
            try
            {
                return vGarantiaDAL.existeGarantiaContrato(pNumerogarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por idcontrato para la entidad Garantia
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el Id del contrato</param>
        public List<Garantia> ConsultarInfoGarantias(int? pIdContrato)
        {
            try
            {
                return vGarantiaDAL.ConsultarInfoGarantias(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoMigrado> ConsultarInfoGarantiasMigracion(int? pIdContrato)
        {
            try
            {
                return vGarantiaDAL.ConsultarInfoGarantiasMigracion(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGarantia"></param>
        /// <param name="aprobada"></param>
        /// <returns></returns>
        public int CambiarEstadoGarantia(int idGarantia, bool aprobada, bool esregistro, DateTime fecha, string observaciones, DateTime? fechaFinalizacion)
        {
            try
            {
                return vGarantiaDAL.CambiarEstadoGarantia(idGarantia, aprobada, esregistro, fecha, observaciones, fechaFinalizacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="idTipoModificacionContractual"></param>
        /// <returns></returns>
        public ConsModContractual ConsultarModificacionContractualGarantia(int idContrato, int idGarantia)
        {
            try
            {
                return vGarantiaDAL.ConsultarModificacionContractualGarantia(idContrato, idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarGarantia(Garantia item)
        {
            try
            {
                return vGarantiaDAL.ActualizarGarantia(item);
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
