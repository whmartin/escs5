using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  SucursalAseguradoraContrato
    /// </summary>
    public class SucursalAseguradoraContratoBLL
    {
        private SucursalAseguradoraContratoDAL vSucursalAseguradoraContratoDAL;
        public SucursalAseguradoraContratoBLL()
        {
            vSucursalAseguradoraContratoDAL = new SucursalAseguradoraContratoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int InsertarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoDAL.InsertarSucursalAseguradoraContrato(pSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int ModificarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoDAL.ModificarSucursalAseguradoraContrato(pSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pSucursalAseguradoraContrato"></param>
        public int EliminarSucursalAseguradoraContrato(SucursalAseguradoraContrato pSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoDAL.EliminarSucursalAseguradoraContrato(pSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pIDSucursalAseguradoraContrato"></param>
        public SucursalAseguradoraContrato ConsultarSucursalAseguradoraContrato(int pIDSucursalAseguradoraContrato)
        {
            try
            {
                return vSucursalAseguradoraContratoDAL.ConsultarSucursalAseguradoraContrato(pIDSucursalAseguradoraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SucursalAseguradoraContrato
        /// </summary>
        /// <param name="pIDDepartamento"></param>
        /// <param name="pIDMunicipio"></param>
        /// <param name="pNombre"></param>
        /// <param name="pIDZona"></param>
        /// <param name="pDireccionNotificacion"></param>
        /// <param name="pCorreoElectronico"></param>
        /// <param name="pIndicativo"></param>
        /// <param name="pTelefono"></param>
        /// <param name="pExtension"></param>
        /// <param name="pCelular"></param>
        /// <param name="pIDEntidadProvOferente"></param>
        /// <param name="pCodigoSucursal"></param>
        public List<SucursalAseguradoraContrato> ConsultarSucursalAseguradoraContratos(int? pIDDepartamento, int? pIDMunicipio, String pNombre, int? pIDZona, String pDireccionNotificacion, String pCorreoElectronico, String pIndicativo, String pTelefono, String pExtension, String pCelular, int? pIDEntidadProvOferente, int? pCodigoSucursal)
        {
            try
            {
                return vSucursalAseguradoraContratoDAL.ConsultarSucursalAseguradoraContratos(pIDDepartamento, pIDMunicipio, pNombre, pIDZona, pDireccionNotificacion, pCorreoElectronico, pIndicativo, pTelefono, pExtension, pCelular, pIDEntidadProvOferente, pCodigoSucursal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
