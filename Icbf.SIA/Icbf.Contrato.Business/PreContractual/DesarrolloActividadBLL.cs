﻿using Icbf.Contrato.DataAccess.PreContractual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business.PreContractual
{
    public class DesarrolloActividadBLL
    {
        private DesarrolloActividadDAL _Dal;

        public DesarrolloActividadBLL()
        {
            _Dal = new DesarrolloActividadDAL();
        }

        public List<SolicitudContratoActividadesDesarrollo> ConsultarDesarrolloActividadPorId(int idSolicitud)
        {
            try
            {
                return _Dal.ConsultarDesarrolloActividadPorId(idSolicitud);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividadesDesarrollo> ConsultarDesarrolloActividadPorIdActividad(int idSolicitud, int idActividad)
        {
            try
            {
                return _Dal.ConsultarDesarrolloActividadPorIdActividad(idSolicitud, idActividad);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContratoActividadesDesarrolloDetalle ConsultarDesarrolloActividadPorId(int idSolicitudContrato, int idActividad, int? idActividadPadre)
        {
            try
            {
                return _Dal.ConsultarDesarrolloActividadPorId(idSolicitudContrato, idActividad, idActividadPadre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDesarrolloActividad(SolicitudContratoActividadesDesarrolloDetalle item)
        {
            try
            {
                return _Dal.InsertarDesarrolloActividad(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDesarrolloActividad(SolicitudContratoActividadesDesarrolloDetalle item)
        {
            try
            {
                return _Dal.ModificarDesarrolloActividad(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int FinalizarGestionActividad(int idDesarrolloActividad)
        {
            try
            {
                return _Dal.FinalizarGestionActividad(idDesarrolloActividad);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDesarrolloActividadDocumentos(SolicitudContratoActividadesDesarrolloDocumentos item)
        {
            try
            {
                return _Dal.InsertarDesarrolloActividadDocumentos(item);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSDesarrolloActividadDocumentos(int idDesarrolloActividadDocumento)
        {
            try
            {
                return _Dal.EliminarSDesarrolloActividadDocumentos(idDesarrolloActividadDocumento);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividadesDesarrolloDocumentos> ConsultarDocumentosPorIdDesarrolloActividad(int idDesarrolloActividad)
        {
            try
            {
                return _Dal.ConsultarDocumentosPorIdDesarrolloActividad(idDesarrolloActividad);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
