﻿using Icbf.Contrato.DataAccess.PreContractual;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Bussines.PreContractual
{
    public class TiposDocumentosBLL
    {
        private TiposDocumentosDAL vTiposDocumentosDAL;

        public TiposDocumentosBLL()
        {
            vTiposDocumentosDAL = new TiposDocumentosDAL();
        } 

        public int InsertarTipoDocumento(TiposDocumento pTipoDocumento)
        {
            try
            {
                return vTiposDocumentosDAL.InsertarTipoDocumento(pTipoDocumento);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarTipoDocumento(TiposDocumento pTipoDocumento)
        {
            try
            {
                return vTiposDocumentosDAL.ModificarTipoDocumento(pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiposDocumento ConsultarTipoDocumentoPorId(int pIdTipoDocumento)
        {
            try
            {
                return vTiposDocumentosDAL.ConsultarTipoDocumentoPorId(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTiposDocumentos(int? pIdModalidadSeleccion, string pNombre, bool ? estado)
        {
            try
            {
                return vTiposDocumentosDAL.ConsultarTiposDocumentos(pIdModalidadSeleccion, pNombre, estado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTipoDocumentoPorActividad(int idActividad)
        {
            try
            {
                return vTiposDocumentosDAL.ConsultarTipoDocumentoPorActividad(idActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposDocumento> ConsultarTipoDocumentoPosiblesPorActividad(int idActividad)
        {
            try
            {
                return vTiposDocumentosDAL.ConsultarTipoDocumentoPosiblesPorActividad(idActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarModalidadSeleccionPorIdTipoDocumento(int pIdTipoDocumento)
        {
            try
            {
                return vTiposDocumentosDAL.ConsultarModalidadSeleccionPorIdTipoDocumento(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
