﻿using Icbf.Contrato.DataAccess.PreContractual;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Bussines.PreContractual
{
    public class SolicitudContratoBLL 
    {
        private SolicitudContratoDAL vSolicitudContratoDAL;

        public SolicitudContratoBLL()
        {
            vSolicitudContratoDAL = new SolicitudContratoDAL();
        }

        public int InsertarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                return vSolicitudContratoDAL.InsertarSolicitudContrato(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                return vSolicitudContratoDAL.ModificarSolicitudContrato(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSolicitudContrato(SolicitudContrato item)
        {
            try
            {
                return vSolicitudContratoDAL.EliminarSolicitudContrato(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContrato ConsultarPorId(int idSolicitudContrato)
        {
            try
            {
                return vSolicitudContratoDAL.ConsultarPorId(idSolicitudContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContrato> ConsultarSolicitudContrato(DateTime? vFechaRegistroSistemaDesde, DateTime? vFechaRegistroSistemaHasta, int? vIdSolicitudContrato, int? vVigenciaFiscalinicial, int? vIDRegional, int? vIDModalidadSeleccion, int? vIDCategoriaContrato, int? vIDTipoContrato, int? vIDEstadoSolicitud, string usuarioArea, string usuarioRevision)
        {
            try
            {
                return vSolicitudContratoDAL.ConsultarSolicitudContrato(vFechaRegistroSistemaDesde, vFechaRegistroSistemaHasta, vIdSolicitudContrato, vVigenciaFiscalinicial, vIDRegional, vIDModalidadSeleccion, vIDCategoriaContrato, vIDTipoContrato, vIDEstadoSolicitud,usuarioArea, usuarioRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

         public List<SolicitudContratoGestion> ConsultarGestionSolicitud(int idSolicitudContrato)
        {
            try
            {
                return vSolicitudContratoDAL.ConsultarGestionSolicitud(idSolicitudContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoEstado> ConsultarEstados()
        {
            try
            {
                return vSolicitudContratoDAL.ConsultarEstados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<string,string> ValidarSolicitudContrato(int idSolicitud)
        {
            try
            {
                return  vSolicitudContratoDAL.ValidarSolicitudContrato(idSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoSolicitud(SolicitudContratoCambioEstado solCambioEstado)
        {
            try
            {
                return vSolicitudContratoDAL.CambiarEstadoSolicitud(solCambioEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoDocumentos> ConsultarDocumentosPorIdSolicitud(int idSolicitud)
        {
            try
            {
                return vSolicitudContratoDAL.ConsultarDocumentosPorIdSolicitud(idSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarSolicitudContratoDocumentos(int idSolicitud)
        {
            try
            {
                return vSolicitudContratoDAL.EliminarSolicitudContratoDocumentos(idSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarSolicitudContratoDocumentos(SolicitudContratoDocumentos item)
        {
            try
            {
                return vSolicitudContratoDAL.InsertarSolicitudContratoDocumentos(item);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int, string> ConsultarDocumentosFaltantesPorCrear(int idSolicitud, int idModalidadSeleccion)
        {
            try
            {
                return vSolicitudContratoDAL.ConsultarDocumentosFaltantesPorCrear(idSolicitud, idModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
