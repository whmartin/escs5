﻿using Icbf.Contrato.DataAccess.PreContractual;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business.PreContractual
{
    public class ActividadesBLL 
    {
        private ActividadesDAL vActividadesDAL;

        public ActividadesBLL()
        {
            vActividadesDAL = new ActividadesDAL();
        } 

        public int InsertarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                return vActividadesDAL.InsertarActividad(pActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                return vActividadesDAL.ModificarActividad(pActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividadSubActividadDocumentos(int idActividad, string tiposDocumentos, string subActividades)
        {
            try
            {
                return vActividadesDAL.ModificarActividadSubActividadDocumentos(idActividad, tiposDocumentos, subActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarActividad(SolicitudContratoActividad pActividad)
        {
            try
            {
                return vActividadesDAL.EliminarActividad(pActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolicitudContratoActividad ConsultarActividadPorId(int pIdSolicitudContratoActividades)
        {
            try
            {
                return vActividadesDAL.ConsultarActividadPorId(pIdSolicitudContratoActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarActividades(int? pIdModalidadSeleccion, String pNombre, int? pTipoDocumento, bool? pEstado, bool esPadre)
        {
            try
            {
                return vActividadesDAL.ConsultarActividades(pIdModalidadSeleccion, pNombre, pTipoDocumento, pEstado, esPadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Dictionary<int,string> ConsultarModalidadSeleccionPorIdActividad(int pIdActividad)
        {
            try
            {
                return vActividadesDAL.ConsultarModalidadSeleccionPorIdActividad(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarActividadDocumentos(int idActividad, string tiposDocumentos)
        {
            try
            {
                return vActividadesDAL.ModificarActividadDocumentos(idActividad, tiposDocumentos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarSubActividadesPorActividad(int pIdActividad)
        {
            try
            {
                return vActividadesDAL.ConsultarSubActividadesPorActividad(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolicitudContratoActividad> ConsultarSubActividadesPosiblesPorActividad(int pIdActividad)
        {
            try
            {
                return vActividadesDAL.ConsultarSubActividadesPosiblesPorActividad(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
