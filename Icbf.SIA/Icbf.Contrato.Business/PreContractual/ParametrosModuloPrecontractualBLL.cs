﻿using Icbf.Contrato.DataAccess.PreContractual;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business.PreContractual
{
    public class ParametrosModuloPrecontractualBLL
    {
        private ParametrosModuloPrecontractualDAL _Dal;
        public ParametrosModuloPrecontractualBLL()
        {
            _Dal = new ParametrosModuloPrecontractualDAL();
        }

        public ParametrosModuloPrecontractual ConsultarParametro(string Nombre)
        {
            try
            {
                return _Dal.ConsultarParametro(Nombre);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
