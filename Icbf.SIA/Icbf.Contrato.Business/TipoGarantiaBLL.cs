using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Tipo Garant�a
    /// </summary>
    public class TipoGarantiaBLL
    {
        private TipoGarantiaDAL vTipoGarantiaDAL;
        public TipoGarantiaBLL()
        {
            vTipoGarantiaDAL = new TipoGarantiaDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int InsertarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                return vTipoGarantiaDAL.InsertarTipoGarantia(pTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int ModificarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                return vTipoGarantiaDAL.ModificarTipoGarantia(pTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoGarantia
        /// </summary>
        /// <param name="pTipoGarantia"></param>
        /// <returns></returns>
        public int EliminarTipoGarantia(TipoGarantia pTipoGarantia)
        {
            try
            {
                return vTipoGarantiaDAL.EliminarTipoGarantia(pTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoGarantia
        /// </summary>
        /// <param name="pIdTipoGarantia"></param>
        /// <returns></returns>
        public TipoGarantia ConsultarTipoGarantia(int pIdTipoGarantia)
        {
            try
            {
                return vTipoGarantiaDAL.ConsultarTipoGarantia(pIdTipoGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoGarantia
        /// </summary>
        /// <param name="pNombreTipoGarantia"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoGarantia> ConsultarTipoGarantias(String pNombreTipoGarantia, Boolean? pEstado)
        {
            try
            {
                return vTipoGarantiaDAL.ConsultarTipoGarantias(pNombreTipoGarantia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
