using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ObligacionesContratoReporte
    /// </summary>
    public class ObligacionesContratoReporteBLL
    {
        private ObligacionesContratoReporteDAL vObligacionesContratoReporteDAL;
        public ObligacionesContratoReporteBLL()
        {
            vObligacionesContratoReporteDAL = new ObligacionesContratoReporteDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int InsertarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                return vObligacionesContratoReporteDAL.InsertarObligacionesContratoReporte(pObligacionesContratoReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int ModificarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                return vObligacionesContratoReporteDAL.ModificarObligacionesContratoReporte(pObligacionesContratoReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pObligacionesContratoReporte"></param>
        public int EliminarObligacionesContratoReporte(ObligacionesContratoReporte pObligacionesContratoReporte)
        {
            try
            {
                return vObligacionesContratoReporteDAL.EliminarObligacionesContratoReporte(pObligacionesContratoReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pIDObligacionContrato"></param>
        public ObligacionesContratoReporte ConsultarObligacionesContratoReporte(int pIDObligacionContrato)
        {
            try
            {
                return vObligacionesContratoReporteDAL.ConsultarObligacionesContratoReporte(pIDObligacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ObligacionesContratoReporte
        /// </summary>
        /// <param name="pIDContrato"></param>
        /// <param name="pDescripcionObligacion"></param>
        public List<ObligacionesContratoReporte> ConsultarObligacionesContratoReportes(int? pIDContrato)
        {
            try
            {
                return vObligacionesContratoReporteDAL.ConsultarObligacionesContratoReportes(pIDContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ObligacionesContratoReporte> ConsultarObligacionesSigepcyp(int NumCDP, int NumDocIdentificacion, int AnioVigencia)
        {
            try
            {
                return vObligacionesContratoReporteDAL.ConsultarObligacionesSigepcyp(NumCDP,NumDocIdentificacion, AnioVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
