using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Informaci�n Presupuestal RP
    /// </summary>
    public class InformacionPresupuestalRPBLL
    {
        private InformacionPresupuestalRPDAL vInformacionPresupuestalRPDAL;
        public InformacionPresupuestalRPBLL()
        {
            vInformacionPresupuestalRPDAL = new InformacionPresupuestalRPDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int InsertarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRPDAL.InsertarInformacionPresupuestalRP(pInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int ModificarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRPDAL.ModificarInformacionPresupuestalRP(pInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public int EliminarInformacionPresupuestalRP(InformacionPresupuestalRP pInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRPDAL.EliminarInformacionPresupuestalRP(pInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pIdInformacionPresupuestalRP"></param>
        /// <returns></returns>
        public InformacionPresupuestalRP ConsultarInformacionPresupuestalRP(int pIdInformacionPresupuestalRP)
        {
            try
            {
                return vInformacionPresupuestalRPDAL.ConsultarInformacionPresupuestalRP(pIdInformacionPresupuestalRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad InformacionPresupuestalRP
        /// </summary>
        /// <param name="pFechaSolicitudRP"></param>
        /// <param name="pNumeroRP"></param>
        /// <returns></returns>
        public List<InformacionPresupuestalRP> ConsultarInformacionPresupuestalRPs(DateTime? pFechaSolicitudRP, String pNumeroRP)
        {
            try
            {
                return vInformacionPresupuestalRPDAL.ConsultarInformacionPresupuestalRPs(pFechaSolicitudRP, pNumeroRP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
