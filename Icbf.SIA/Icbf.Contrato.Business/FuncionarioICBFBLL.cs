using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de la capa de negocio que contiene los métodos para Insertar, Modificar, Eliminar y Consultar registros de los funcionarios del ICBF
    /// </summary>
    public class FuncionarioICBFBLL
    {
        private FuncionarioICBFDAL vFuncionarioICBFDAL;
        public FuncionarioICBFBLL()
        {
            vFuncionarioICBFDAL = new FuncionarioICBFDAL();
        }
        /// <summary>
        /// Gonet
        /// Insertar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioICBF">Instancia que contiene la información del funcionario del ICBF</param>
        /// <returns>Identificador de la base de datos del registro insertado</returns>
        public int InsertarFuncionarioICBF(FuncionarioICBF pFuncionarioICBF)
        {
            try
            {
                return vFuncionarioICBFDAL.InsertarFuncionarioICBF(pFuncionarioICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Modificar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioICBF">Instancia que contiene la información del funcionario del ICBF</param>
        /// <returns>Identificador de la base de datos del registro modificado</returns>
        public int ModificarFuncionarioICBF(FuncionarioICBF pFuncionarioICBF)
        {
            try
            {
                return vFuncionarioICBFDAL.ModificarFuncionarioICBF(pFuncionarioICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Eliminar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pFuncionarioICBF">Instancia que contiene la información del funcionario del ICBF</param>
        /// <returns>Identificador de la base de datos del registro eliminado</returns>
        public int EliminarFuncionarioICBF(FuncionarioICBF pFuncionarioICBF)
        {
            try
            {
                return vFuncionarioICBFDAL.EliminarFuncionarioICBF(pFuncionarioICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar funcionario del ICBF por Id
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdFuncionario">Valor entero con el Id del funcionario</param>
        /// <returns>Instancia que contiene la información del funcionario del ICBF</returns>
        public FuncionarioICBF ConsultarFuncionarioICBF(int pIdFuncionario)
        {
            try
            {
                return vFuncionarioICBFDAL.ConsultarFuncionarioICBF(pIdFuncionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar funcionario del ICBF
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pIdRegionalPCI">Valor entero con el Id de la Regional , permite valores nulos</param>
        /// <param name="pIdUsuario">Valor entero con el Id del Usuario, permite valores nulos</param>
        /// <param name="pEstado">Valor booleano</param>
        /// <returns>Lista de la Instancia que contiene la información del funcionario del ICBF</returns>
        public List<FuncionarioICBF> ConsultarFuncionarioICBFs(int? pIdRegionalPCI, int? pIdUsuario, Boolean? pEstado)
        {
            try
            {
                return vFuncionarioICBFDAL.ConsultarFuncionarioICBFs(pIdRegionalPCI,pIdUsuario,pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
