using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Obligaci�n
    /// </summary>
    public class ObligacionBLL
    {
        private ObligacionDAL vObligacionDAL;
        public ObligacionBLL()
        {
            vObligacionDAL = new ObligacionDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int InsertarObligacion(Obligacion pObligacion)
        {
            try
            {
                return vObligacionDAL.InsertarObligacion(pObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int ModificarObligacion(Obligacion pObligacion)
        {
            try
            {
                return vObligacionDAL.ModificarObligacion(pObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad Obligacion
        /// </summary>
        /// <param name="pObligacion"></param>
        /// <returns></returns>
        public int EliminarObligacion(Obligacion pObligacion)
        {
            try
            {
                return vObligacionDAL.EliminarObligacion(pObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Obligacion
        /// </summary>
        /// <param name="pIdObligacion"></param>
        /// <returns></returns>
        public Obligacion ConsultarObligacion(int pIdObligacion)
        {
            try
            {
                return vObligacionDAL.ConsultarObligacion(pIdObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Obligacion
        /// </summary>
        /// <param name="pDescripcion"></param>
        /// <param name="pIdTipoObligacion"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<Obligacion> ConsultarObligacions(String pDescripcion, int? pIdTipoObligacion, int? pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                return vObligacionDAL.ConsultarObligacions(pDescripcion, pIdTipoObligacion, pIdTipoContrato, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
