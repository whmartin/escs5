using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  PlanComprasProductos
    /// </summary>
    public class PlanComprasProductosBLL
    {
        private PlanComprasProductosDAL vPlanComprasProductosDAL;

        public PlanComprasProductosBLL()
        {
            vPlanComprasProductosDAL = new PlanComprasProductosDAL();
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarPlanComprasProductos(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int InsertarProductoPlanComprasContrato(ProductoPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarProductoPlanComprasContrato(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarProductoPlanComprasContrato(ProductoPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosDAL.ModificarProductoPlanComprasContrato(pPlanComprasProductos);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
       
        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubrosTotal(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarDetallePlanComprasProductosRubrosTotal(pPlanComprasProductos,esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <param name="esEdicion"></param>
        /// <returns></returns>
        public int InsertarDetallePlanComprasProductosRubrosConTotalPrecontractual(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarDetallePlanComprasProductosRubrosConTotalPrecontractual(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubrosConTotalAdicion(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarDetallePlanComprasProductosRubrosConTotalAdicion(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int InsertarDetallePlanComprasProductosRubrosTotalVigencias(PlanComprasProductos pPlanComprasProductos, bool esEdicion)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarDetallePlanComprasProductosRubrosConTotalVigencias(pPlanComprasProductos, esEdicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPlanCompras"></param>
        /// <param name="valorPlanCompras"></param>
        /// <returns></returns>
        public int ActualizarPlanComprasTotal(int idPlanCompras, decimal valorPlanCompras)
        {
            try
            {
                return vPlanComprasProductosDAL.ActualizarPlanComprasTotal(idPlanCompras,valorPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int InsertarDetallePlanComprasProductosRubros(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarDetallePlanComprasProductosRubros(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de modificación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int ModificarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosDAL.ModificarPlanComprasProductos(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de eliminación para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        public int EliminarPlanComprasProductos(PlanComprasProductos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasProductosDAL.EliminarPlanComprasProductos(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasProductos ConsultarPlanComprasProductos(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                return vPlanComprasProductosDAL.ConsultarPlanComprasProductos(pIdProductoPlanCompraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasProductos
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pCodigoProducto"></param>
        /// <param name="pNombreProducto"></param>
        /// <param name="pTipoProducto"></param>
        /// <param name="pCantidadCupos"></param>
        /// <param name="pValorUnitario"></param>
        /// <param name="pTiempo"></param>
        /// <param name="pValorTotal"></param>
        /// <param name="pUnidadTiempo"></param>
        /// <param name="pUnidadMedida"></param>
        public List<PlanComprasProductos> ConsultarPlanComprasProductoss(int? pNumeroConsecutivoPlanCompras, String pCodigoProducto, String pNombreProducto, String pTipoProducto, Decimal? pCantidadCupos, Decimal? pValorUnitario, Decimal? pTiempo, Decimal? pValorTotal, String pUnidadTiempo, String pUnidadMedida)
        {
            try
            {
                return vPlanComprasProductosDAL.ConsultarPlanComprasProductoss(pNumeroConsecutivoPlanCompras, pCodigoProducto, pNombreProducto, pTipoProducto, pCantidadCupos, pValorUnitario, pTiempo, pValorTotal, pUnidadTiempo, pUnidadMedida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los productos asociados a un plan de compras para ser utilizado en la pagina de registro
        /// de contratos
        /// </summary>
        /// <param name="pIdPlanDeComprasContratos">Entero con el identificador del plan de compras</param>
        /// <returns>Listado de productos asociados al plan de compras obtenidos de la base de datos</returns>
        public List<PlanComprasProductos> ObtenerProductosPlanCompras(int pIdPlanDeComprasContratos)
        {
            try
            {
                return vPlanComprasProductosDAL.ObtenerProductosPlanCompras(pIdPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdProductoPlanComprasContrato"></param>
        /// <returns></returns>
        public ValidacionProductoOriginal ValidarModifcacionProducto(int pIdProductoPlanComprasContrato)
        {
            try
            {
                return vPlanComprasProductosDAL.ValidarModifcacionProducto(pIdProductoPlanComprasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ValidacionRubroOriginal ValidarModifcacionRubro(int pIdRubroPlanComprasContrato)
        {
            try
            {
                return vPlanComprasProductosDAL.ValidarModifcacionRubro(pIdRubroPlanComprasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PlanComprasProductos ConsultarPlanComprasProductoActual(int idPlanComprasContrato, string codigoProducto)
        {
            try
            {
                return vPlanComprasProductosDAL.ConsultarPlanComprasProductoActual(idPlanComprasContrato, codigoProducto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int ModificarProductoPlanComprasContratoAdiciones(ProductoPlanComprasContratos pPlanComprasProductos, int idAporte)
        {
            try
            {
                return vPlanComprasProductosDAL.ModificarProductoPlanComprasContratoAdiciones(pPlanComprasProductos, idAporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="pPlanComprasProductos"></param>
        /// <returns></returns>
        public int InsertarProductoPlanComprasContratoAdiciones(ProductoPlanComprasContratos pPlanComprasProductos, int idAporte)
        {
            try
            {
                return vPlanComprasProductosDAL.InsertarProductoPlanComprasContratoAdiciones(pPlanComprasProductos, idAporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
