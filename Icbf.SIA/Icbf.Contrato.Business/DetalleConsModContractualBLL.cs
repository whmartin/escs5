using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class DetalleConsModContractualBLL
    {
        private DetalleConsModContractualDAL vDetalleConsModContractualDAL;
        public DetalleConsModContractualBLL()
        {
            vDetalleConsModContractualDAL = new DetalleConsModContractualDAL();
        }
        public int InsertarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                return vDetalleConsModContractualDAL.InsertarDetalleConsModContractual(pDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                return vDetalleConsModContractualDAL.ModificarDetalleConsModContractual(pDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarDetalleConsModContractual(DetalleConsModContractual pDetalleConsModContractual)
        {
            try
            {
                return vDetalleConsModContractualDAL.EliminarDetalleConsModContractual(pDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public DetalleConsModContractual ConsultarDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                return vDetalleConsModContractualDAL.ConsultarDetalleConsModContractual(pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DetalleConsModContractual> ConsultarDetalleConsModContractuals(int? pIDTipoModificacionContractual, int? pIDCosModContractual)
        {
            try
            {
                return vDetalleConsModContractualDAL.ConsultarDetalleConsModContractuals(pIDTipoModificacionContractual, pIDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
