﻿using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class ReportesBLL
    {
        private ReportesDAL _ReportesDAL;

        public ReportesBLL()
        {
            _ReportesDAL = new ReportesDAL();
        }

     public List<ReporteFUC> ObtenerReporteFUC
    (
     DateTime fechaDesde,
     DateTime fechaHasta,
     int vigencia,
     string idRegional,
     int? idModalidad,
     int? idCategoria,
     int? idTipoContrato,
     int? idEstadoContrato,
     int? idTipoPersona
    )
        {
            try
            {
                return _ReportesDAL.ObtenerReporteFUC(fechaDesde, fechaHasta, vigencia, idRegional, idModalidad, idCategoria, idTipoContrato, idEstadoContrato, idTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


        }


    public List<ReporteFUC> ObtenerReporteFUCHistorico
    (
    DateTime fechaDesde,
    DateTime fechaHasta,
    int vigencia,
    string idRegional,
    int? idModalidad,
    int? idCategoria,
    int? idTipoContrato,
    int? idEstadoContrato,
    int? idTipoPersona
    )
    {
        try
        {
            return _ReportesDAL.ObtenerReporteFUCHistorico(fechaDesde, fechaHasta, vigencia, idRegional, idModalidad, idCategoria, idTipoContrato, idEstadoContrato, idTipoPersona);
        }
        catch (UserInterfaceException)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }


    }



    }
}
