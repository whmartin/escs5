using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Modalidad Selecci�n
    /// </summary>
    public class ModalidadSeleccionBLL
    {
        private ModalidadSeleccionDAL vModalidadSeleccionDAL;
        public ModalidadSeleccionBLL()
        {
            vModalidadSeleccionDAL = new ModalidadSeleccionDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int InsertarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionDAL.InsertarModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int ModificarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionDAL.ModificarModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pModalidadSeleccion"></param>
        /// <returns></returns>
        public int EliminarModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionDAL.EliminarModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pIdModalidad"></param>
        /// <returns></returns>
        public ModalidadSeleccion ConsultarModalidadSeleccion(int pIdModalidad)
        {
            try
            {
                return vModalidadSeleccionDAL.ConsultarModalidadSeleccion(pIdModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ModalidadSeleccion
        /// </summary>
        /// <param name="pNombre"></param>
        /// <param name="pSigla"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ModalidadSeleccion> ConsultarModalidadSeleccions(String pNombre, String pSigla, Boolean? pEstado)
        {
            try
            {
                return vModalidadSeleccionDAL.ConsultarModalidadSeleccions(pNombre, pSigla, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadSeleccion> ConsultarModalidadSeleccionTipoContrato(int pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                return vModalidadSeleccionDAL.ConsultarModalidadSeleccionTipoContrato(pIdTipoContrato,pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos), de la modalidad de seleccion, la 
        /// informaci�n del mismo
        /// </summary>
        /// <param name="pModalidadSeleccion">Entidad con la informaci�n del filtro</param>
        /// <returns>Entidad con la informaci�n de la modalidad de seleccion recuperada</returns>
        public ModalidadSeleccion IdentificadorCodigoModalidadSeleccion(ModalidadSeleccion pModalidadSeleccion)
        {
            try
            {
                return vModalidadSeleccionDAL.IdentificadorCodigoModalidadSeleccion(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
