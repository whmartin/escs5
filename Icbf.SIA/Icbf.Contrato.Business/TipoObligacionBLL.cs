using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo Obligaci�n
    /// </summary>
    public class TipoObligacionBLL
    {
        private TipoObligacionDAL vTipoObligacionDAL;
        public TipoObligacionBLL()
        {
            vTipoObligacionDAL = new TipoObligacionDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int InsertarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                return vTipoObligacionDAL.InsertarTipoObligacion(pTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int ModificarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                return vTipoObligacionDAL.ModificarTipoObligacion(pTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoObligacion
        /// </summary>
        /// <param name="pTipoObligacion"></param>
        /// <returns></returns>
        public int EliminarTipoObligacion(TipoObligacion pTipoObligacion)
        {
            try
            {
                return vTipoObligacionDAL.EliminarTipoObligacion(pTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoObligacion
        /// </summary>
        /// <param name="pIdTipoObligacion"></param>
        /// <returns></returns>
        public TipoObligacion ConsultarTipoObligacion(int pIdTipoObligacion)
        {
            try
            {
                return vTipoObligacionDAL.ConsultarTipoObligacion(pIdTipoObligacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoObligacion
        /// </summary>
        /// <param name="pNombreTipoObligacion"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoObligacion> ConsultarTipoObligacions(String pNombreTipoObligacion,String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoObligacionDAL.ConsultarTipoObligacions(pNombreTipoObligacion,pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
