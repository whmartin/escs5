using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad secuencia n�mero contrato
    /// </summary>
    public class SecuenciaNumeroContratoBLL
    {
        private SecuenciaNumeroContratoDAL vSecuenciaNumeroContratoDAL;
        public SecuenciaNumeroContratoBLL()
        {
            vSecuenciaNumeroContratoDAL = new SecuenciaNumeroContratoDAL();
        }
        /// <summary>
        /// M�todo que Genera el Numero de Contrato
        /// </summary>
        /// <param name="pSecuenciaNumeroContrato"></param>
        /// <returns></returns>
        public string GenerarNumeroContrato(SecuenciaNumeroContrato pSecuenciaNumeroContrato)
        {
            try
            {
                return vSecuenciaNumeroContratoDAL.GenerarNumeroContrato(pSecuenciaNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        
    }
}
