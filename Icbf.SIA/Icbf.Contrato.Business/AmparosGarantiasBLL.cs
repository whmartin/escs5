using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  AmparosGarantias
    /// </summary>
    public class AmparosGarantiasBLL
    {
        private AmparosGarantiasDAL vAmparosGarantiasDAL;
        public AmparosGarantiasBLL()
        {
            vAmparosGarantiasDAL = new AmparosGarantiasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int InsertarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasDAL.InsertarAmparosGarantias(pAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int ModificarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasDAL.ModificarAmparosGarantias(pAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pAmparosGarantias"></param>
        public int EliminarAmparosGarantias(AmparosGarantias pAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasDAL.EliminarAmparosGarantias(pAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pIDAmparosGarantias"></param>
        public AmparosGarantias ConsultarAmparosGarantias(int pIDAmparosGarantias)
        {
            try
            {
                return vAmparosGarantiasDAL.ConsultarAmparosGarantias(pIDAmparosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad AmparosGarantias
        /// </summary>
        /// <param name="pFechaVigenciaDesde"></param>
        /// <param name="pFechaVigenciaHasta"></param>
        /// <param name="pValorCalculoAsegurado"></param>
        /// <param name="pValorAsegurado"></param>
        public List<AmparosGarantias> ConsultarAmparosGarantiass(DateTime? pFechaVigenciaDesde, DateTime? pFechaVigenciaHasta, Decimal? pValorCalculoAsegurado, Decimal? pValorAsegurado, int? pIDGarantia)
        {
            try
            {
                return vAmparosGarantiasDAL.ConsultarAmparosGarantiass(pFechaVigenciaDesde, pFechaVigenciaHasta, pValorCalculoAsegurado, pValorAsegurado,pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarValoresContrato(int? pIdContrato)
        {
            try
            {
                return vAmparosGarantiasDAL.ConsultarValoresContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AmparosGarantias ConsultarFechaVigenciaAmparoRelacionado(int? IdGarantia)
        {
            try
            {
                return vAmparosGarantiasDAL.ConsultarFechaVigenciaAmparoRelacionado(IdGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
