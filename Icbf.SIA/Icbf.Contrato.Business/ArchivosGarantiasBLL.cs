using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ArchivosGarantias
    /// </summary>
    public class ArchivosGarantiasBLL
    {
        private ArchivosGarantiasDAL vArchivosGarantiasDAL;
        public ArchivosGarantiasBLL()
        {
            vArchivosGarantiasDAL = new ArchivosGarantiasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int InsertarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasDAL.InsertarArchivosGarantias(pArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int ModificarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasDAL.ModificarArchivosGarantias(pArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pArchivosGarantias"></param>
        public int EliminarArchivosGarantias(ArchivosGarantias pArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasDAL.EliminarArchivosGarantias(pArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pIDArchivosGarantias"></param>
        public ArchivosGarantias ConsultarArchivosGarantias(int pIDArchivosGarantias)
        {
            try
            {
                return vArchivosGarantiasDAL.ConsultarArchivosGarantias(pIDArchivosGarantias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ArchivosGarantias
        /// </summary>
        /// <param name="pIDArchivo"></param>
        /// <param name="pIDGarantia"></param>
        public List<ArchivosGarantias> ConsultarArchivosGarantiass(int? pIDArchivo, int? pIDGarantia)
        {
            try
            {
                return vArchivosGarantiasDAL.ConsultarArchivosGarantiass(pIDArchivo, pIDGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet
        /// Quita los acentos de una cadena de texto eliminando también espacios
        /// en blanco existentes en cualquier ubicación dentro de la cadena de
        /// texto dada
        /// 26-Dic-2013
        /// </summary>
        /// <param name="Texto">Cadena de texto a la que se van a quitar los acentos</param>
        /// <returns>Cadena texto sin acentos</returns>
        public string QuitarAcentos(string Texto)
        {

            Texto = Texto.Replace(" ", "");

            //quitar acentos 
            var textoNormalizado = Texto.Normalize(NormalizationForm.FormD);
            var reg = new Regex("[^a-zA-Z0-9 ]");
            string textoFinal = reg.Replace(textoNormalizado, "");

            //Verificamos la longitud del archivo si soporta lo permite 
            int longiCad = 0;
            longiCad = textoFinal.Length;
            if (textoFinal.Length > 50)
            {
                longiCad = 50;
            }

            return textoFinal.Substring(0, longiCad);
        }
    }
}
