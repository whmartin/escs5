﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Relación contrato
    /// </summary>
    public class RelacionContratoBLL
    {
        private RelacionContratoDAL vRelacionContratoDAL;
        public RelacionContratoBLL()
        {
            vRelacionContratoDAL = new RelacionContratoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad RelacionContrato
        /// </summary>
        /// <param name="pRelacionContrato"></param>
        /// <returns></returns>
        public int InsertarRelacionContrato(RelacionContrato pRelacionContrato)
        {
            try
            {
                return vRelacionContratoDAL.InsertarRelacionContrato(pRelacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RelacionContrato
        /// </summary>
        /// <param name="pRelacionContrato"></param>
        /// <returns></returns>
        public int EliminarRelacionContrato(RelacionContrato pRelacionContrato)
        {
            try
            {
                return vRelacionContratoDAL.EliminarRelacionContrato(pRelacionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por id para la entidad RelacionContrato
        /// </summary>
        /// <param name="idContratoMaestro"></param>
        /// <returns></returns>
        public List<RelacionContrato> ConsultarRelacionContrato(int idContratoMaestro)
        {
            try
            {
                return vRelacionContratoDAL.ConsultarRelacionContrato(idContratoMaestro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por NumeroContrato para la entidad RelacionContrato
        /// </summary>
        /// <param name="pNumeroContrato"></param>
        /// <returns></returns>
        public List<RelacionContrato> ConsultarRelacionsContrato(string pNumeroContrato)
        {
            try
            {
                return vRelacionContratoDAL.ConsultarRelacionsContrato(pNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad RelacionContrato
        /// </summary>
        /// <param name="idContratoAsociado"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContratoMaestro(int idContratoAsociado)
        {
            try
            {
                return vRelacionContratoDAL.ConsultarContratoMaestro(idContratoAsociado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
