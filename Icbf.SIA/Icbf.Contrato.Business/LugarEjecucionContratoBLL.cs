using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  LugarEjecucionContrato
    /// </summary>
    public class LugarEjecucionContratoBLL
    {
        private LugarEjecucionContratoDAL vLugarEjecucionContratoDAL;
        public LugarEjecucionContratoBLL()
        {
            vLugarEjecucionContratoDAL = new LugarEjecucionContratoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int InsertarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.InsertarLugarEjecucionContrato(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int ModificarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ModificarLugarEjecucionContrato(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pLugarEjecucionContrato"></param>
        public int EliminarLugarEjecucionContrato(LugarEjecucionContrato pLugarEjecucionContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.EliminarLugarEjecucionContrato(pLugarEjecucionContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdLugarEjecucionContratos"></param>
        public LugarEjecucionContrato ConsultarLugarEjecucionContrato(int pIdLugarEjecucionContratos)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ConsultarLugarEjecucionContrato(pIdLugarEjecucionContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad LugarEjecucionContrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIdDepartamento"></param>
        /// <param name="pIdRegional"></param>
        /// <param name="pNivelNacional"></param>
        public List<LugarEjecucionContrato> ConsultarLugarEjecucionContratos(int? pIdContrato, int? pIdDepartamento, int? pIdRegional, Boolean? pNivelNacional)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ConsultarLugarEjecucionContratos(pIdContrato, pIdDepartamento, pIdRegional, pNivelNacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Obtiene los lugares de ejecuci�n asociados a un contrato
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato</param>
        /// <returns>Listado con los lugares obtenidos de la base de datos</returns>
        public List<LugarEjecucionContrato> ConsultarLugaresEjecucionContrato(int pIdContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ConsultarLugaresEjecucionContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<LugarEjecucionContrato> ConsultarLugaresEjecucionContratoHistorico(int pIdContrato)
        {
            try
            {
                return vLugarEjecucionContratoDAL.ConsultarLugaresEjecucionContratoHistorico(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
