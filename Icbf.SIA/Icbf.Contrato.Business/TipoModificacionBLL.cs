using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class TipoModificacionBLL
    {
        private TipoModificacionDAL vTipoModificacionDAL;
        
        public TipoModificacionBLL()
        {
            vTipoModificacionDAL = new TipoModificacionDAL();
        }
        
        public int InsertarTipoModificacion(TipoModificacion pTipoModificacionBasica, string idsAsociados)
        {
            try
            {
                return vTipoModificacionDAL.InsertarTipoModificacion(pTipoModificacionBasica, idsAsociados);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarTipoModificacion(TipoModificacion pTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionDAL.ModificarTipoModificacion(pTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public TipoModificacion ConsultarTipoModificacion(int pIdTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionDAL.ConsultarTipoModificacion(pIdTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public  bool ConsultarExistenciaCombinacion(string idsAsociados,int idTipoModificacion)
        {
            try
            {
                return vTipoModificacionDAL.ConsultarExistenciaCombinacion(idsAsociados, idTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        
        }

        public List<TipoModificacion> ConsultarTipoModificaciones(int? pIdTipoModificacionBasica, String pDescripcion, Boolean? pRequiereModificacion, Boolean? pEstado, string codigo)
        {
            try
            {
                return vTipoModificacionDAL.ConsultarTipoModificacion(pIdTipoModificacionBasica, pDescripcion, pRequiereModificacion, pEstado, codigo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacion> ConsultarTipoModificacionPorDefecto(bool todos)
        {
            try
            {
                return vTipoModificacionDAL.ConsultarTipoModificacionPorDefecto(todos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacion> ConsultarTipoModificacionHijos(int pIdTipoModificacionBasica)
        {
            try
            {
                return vTipoModificacionDAL.ConsultarTipoModificacionHijos(pIdTipoModificacionBasica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarPuedeEliminar(int pIdTipoModificacion)
        {
            bool result = false;

            try
            {
                result = vTipoModificacionDAL.ConsultarPuedeEliminar(pIdTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }

        public int EliminarModificacion(int pIdTipoModificacion)
        {
            int result = 0;

            try
            {
                result = vTipoModificacionDAL.EliminarModificacion(pIdTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }
    }
}
