using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad contratos
    /// </summary>
    public class ContratoBLL
    {
        private ContratoDAL vContratoDAL;
        public ContratoBLL()
        {
            vContratoDAL = new ContratoDAL();
        }
        
        /// <summary>
        /// M�todo de inserci�n para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int InsertarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.InsertarContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int ModificarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.ModificarContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad Contrato
        /// </summary>
        /// <param name="pContrato"></param>
        /// <returns></returns>
        public int EliminarContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.EliminarContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idContrato"></param>
        /// <param name="fechaAnulacion"></param>
        /// <param name="usuarioModifica"></param>
        /// <returns></returns>
        public int AnularContrato(int idContrato, DateTime fechaAnulacion, string usuarioModifica)
        {
            try
            {
                return vContratoDAL.AnularContrato(idContrato, fechaAnulacion, usuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public Contrato.Entity.Contrato ConsultarContrato(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarContratoModificacion(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratoModificacion(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarContratoAdiciones(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratoAdiciones(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratos(DateTime? pFechaRegistro, String pNumeroProceso, Decimal? pNumeroContrato, int? pIdModalidad, int? pIdCategoriaContrato, int? pIdTipoContrato, string pClaseContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratos(pFechaRegistro, pNumeroProceso, pNumeroContrato, pIdModalidad, pIdCategoriaContrato, pIdTipoContrato, pClaseContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratoss(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso,string pNombreContratistas,int? pIDTipoIdentificacion,int? pNumeroIdentificacion, string pUsuarioCreacion)
        {
            try
            {
                return vContratoDAL.ConsultarContratoss(pFechaRegistroSistemaDesde, pFechaRegistroSistemaHasta, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContrato, pIdDependenciaSolicitante, pManejaRecurso, pNombreContratistas, pIDTipoIdentificacion, pNumeroIdentificacion, pUsuarioCreacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosHistorico(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, Boolean? pManejaRecurso, string pNombreContratistas, int? pIDTipoIdentificacion, int? pNumeroIdentificacion)
        {
            try
            {
                return vContratoDAL.ConsultarContratosHistorico(pFechaRegistroSistemaDesde, pFechaRegistroSistemaHasta, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContrato, pIdDependenciaSolicitante, pManejaRecurso, pNombreContratistas, pIDTipoIdentificacion, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contratos
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pFechaRegistroSistemaHasta"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        public List<Contrato.Entity.Contrato> ConsultarContratosRegistrados(DateTime? pFechaRegistroSistemaDesde, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContraro, string pUsuarioCrea, DateTime? pFechaInicioEjecucion, DateTime? pFechaFinalizaInicioContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosRegistrados(pFechaRegistroSistemaDesde, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContraro, pUsuarioCrea, pFechaInicioEjecucion, pFechaFinalizaInicioContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pFechaRegistroSistemaDesde"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pVigenciaFiscalinicial"></param>
        /// <param name="pIDRegional"></param>
        /// <param name="pIDModalidadSeleccion"></param>
        /// <param name="pIDCategoriaContrato"></param>
        /// <param name="pIDTipoContrato"></param>
        /// <param name="pIDEstadoContraro"></param>
        /// <param name="pUsuarioCrea"></param>
        /// <param name="pFechaInicioEjecucion"></param>
        /// <param name="pFechaFinalizaInicioContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosAnulados(DateTime? pFechaRegistroSistemaDesde, int? pIdContrato, String pNumeroContrato, String pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContraro, string pUsuarioCrea, DateTime? pFechaInicioEjecucion, DateTime? pFechaFinalizaInicioContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosAnulacion(pFechaRegistroSistemaDesde, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContraro, pUsuarioCrea, pFechaInicioEjecucion, pFechaFinalizaInicioContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// M�todo de consulta para los contratos suscritos
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosSuscritos(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, String pNumeroContratoConvenio, String pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vContratoDAL.ConsultarContratosSuscritos(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato,pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad Contrato para una Ventana Emergente
        /// </summary>
        /// <param name="pFechaRegistro"></param>
        /// <param name="pNumeroProceso"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pIdModalidad"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pClaseContrato"></param>
        /// <returns></returns>
        public DataTable ConsultarContratosLupa(DateTime? pFechaRegistro, String pNumeroProceso, string pNumeroContrato, int? pIdModalidad, int? pIdCategoriaContrato, int? pIdTipoContrato, string pClaseContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosLupa(pFechaRegistro, pNumeroProceso, pNumeroContrato, pIdModalidad, pIdCategoriaContrato, pIdTipoContrato, pClaseContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta contratos por el filtro
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroContrato"></param>
        /// <returns></returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosLupa(int? pIdContrato, String pNumeroContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosLupa(pIdContrato, pNumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por filtros para la entidad EstadoContrato
        /// </summary>
        /// <param name="pIdEstadoContrato">Valor entero con el id del Estado del contrato</param>
        /// <param name="pCodigoEstadoContrato">Cadena de texto con el c�digo del estado del contrato</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcipon del estado del contrato</param>
        /// <param name="pInactivo">Valor Booleano con el estado del registro</param>
        /// <returns>Lista de la instancia EstadoContrato</returns>
        public List<Contrato.Entity.EstadoContrato> ConsultarEstadoContrato(int? pIdEstadoContrato, string pCodigoEstadoContrato,
                                                            string pDescripcion, bool pInactivo)
        {
            try
            {
                return vContratoDAL.ConsultarEstadoContrato(pIdEstadoContrato, pCodigoEstadoContrato,
                                                             pDescripcion, pInactivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region LupaEmpleado
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Actualiza la informacion de solicitante a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int LupaEmpleadoInsertarSolicitanteContrato(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.LupaEmpleadoInsertarSolicitanteContrato(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Actualiza la informacion de solicitante a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int LupaEmpleadoInsertarSolicitanteContratoPrecontractual(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.LupaEmpleadoInsertarSolicitanteContratoPrecontractual(pContrato);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Actualiza la informacion de ordenador gasto a un contrato especifico
        /// </summary>
        /// <param name="pContrato">Entidad con la informacion del solicitante y contrato a asociar</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int LupaEmpleadoInsertarOrdenadoGasto(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.LupaEmpleadoInsertarOrdenadoGasto(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int LupaEmpleadoInsertarOrdenadoGastoPreContractual(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.LupaEmpleadoInsertarOrdenadoGastoPrecontractual(pContrato);
            }
            catch(UserInterfaceException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region MetodosPaginaRegistroContratos
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Registra un contrato vacio sobre el que se va a trabajar, devolviendo su identificador para el funcionamiento
        /// de las lupas
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n m�nima para la creaci�n del contrato, en ella se
        /// almacena el identificador del contrato creado</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int ContratoRegistroInicial(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.ContratoRegistroInicial(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Realiza la actualizacion de la informaci�n contenida dentro de la entidad de entrada de tipo contrato
        /// </summary>
        /// <param name="pContrato">Entidad con la informaci�n a actualizar</param>
        /// <returns>Entero con el resultado de la operaci�n</returns>
        public int ContratoActualizacion(Contrato.Entity.Contrato pContrato)
        {
            try
            {
                return vContratoDAL.ContratoActualizacion(pContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ActualizarValorInicialContrato(int vIdContraro, decimal vValor)
        {
            try
            {
               return vContratoDAL.ActualizarValorInicialContrato(vIdContraro, vValor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorsinAportes(int vIdContraro)
        {
            try
            {
                return vContratoDAL.ConsultarValorsinAportes(vIdContraro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Obtiene el contrato asociado con el identificador dado
        /// </summary>
        /// <param name="pIdContrato">Entero con el identificador del contrato a obtener.</param>
        /// <returns>Entidad contrato con la informaci�n obtenida de la base de datos</returns>
        public Contrato.Entity.Contrato ContratoObtener(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ContratoObtener(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet/Emilio Calapina
        /// Valida que el valor final del contrato sea mayor igual a la sumatoria de los valores finales
        /// de los contratos asociados a dicho contrato + el valor inical dado
        /// </summary>
        /// <param name="pIdContratoConvMarco"></param>
        /// <param name="pValorInicialContConv"></param>
        /// <returns></returns>
        public bool ValidacionValorInicialSuperaValorFinal(int pIdContratoConvMarco, decimal pValorInicialContConv, int pIdContrato)
        {
            try
            {
                return vContratoDAL.ValidacionValorInicialSuperaValorFinal(pIdContratoConvMarco, pValorInicialContConv, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Valida si la regional del contrato convenio coincide con la regional de
        /// los registros de plan de compras
        /// </summary>
        /// <param name="pIdContratoConvMarco">Entero con el identificador del contrato</param>
        /// <param name="pCodigoRegionalContConv">Cadena de texto con el codigo de la regional</param>
        /// <returns>Retorna true si la validacion permite paso</returns>
        public bool ValidacionCoincideRegionalesContratoPlanComprasContrato(int pIdContratoConvMarco, string pCodigoRegionalContConv)
        {
            try
            {
                return vContratoDAL.ValidacionCoincideRegionalesContratoPlanComprasContrato(pIdContratoConvMarco, pCodigoRegionalContConv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region ContratosAdheridos

        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato de Contratos Relacionados Convenio Marco
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el IdContrato</param>
        /// <returns>Lista de la instancia Contrato</returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosRelacionadosConvMarco(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosRelacionadosConvMarco(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo de consulta por id para la entidad Contrato de Contratos Adheridos
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el IdContrato</param>
        /// <returns>Lista de la instancia Contrato</returns>
        public List<Contrato.Entity.Contrato> ConsultarContratosAdheridos(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosAdheridos(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pVigenciaFuturas"></param>
        /// <returns></returns>
        public bool ValidaValorInicialVsCDPplusVigenciasFuturas(int pIdContrato, VigenciaFuturas pVigenciaFuturas)
        {
            try
            {
                return vContratoDAL.ValidaValorInicialVsCDPplusVigenciasFuturas(pIdContrato, pVigenciaFuturas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ConsultarContratoRPGarantiasAprobadas(int pIdContrato)
        {
            bool isValid = false;

            try
            {
                isValid = vContratoDAL.ConsultarContratoRPGarantiasAprobadas(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return isValid;

        }

        public List<Entity.Contrato> ConsultarContratosAprobacionGarantia(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, string pNumeroContratoConvenio, string pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vContratoDAL.ConsultarContratosAprobacionGarantia(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Entity.Contrato> ConsultarContratosConRP(DateTime? pFechaRegistroDesde, DateTime? pFechaRegistroHasta, string pNumeroContratoConvenio, string pVigenciaFiscalInicial, int? pRegionalContrato, int? pCategoriaContrato, int? pTipoContrato, int? pUsuario)
        {
            try
            {
                return vContratoDAL.ConsultarContratosConRP(pFechaRegistroDesde, pFechaRegistroHasta,
                    pNumeroContratoConvenio, pVigenciaFiscalInicial, pRegionalContrato, pCategoriaContrato,
                    pTipoContrato, pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarIdentificacionOrdenadorG(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarIdentificacionOrdenadorG(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ContratistaMigrados ConsultarContratistaMigracion(int pIdEntidad)
        {
            try
            {
                return vContratoDAL.ConsultarContratistaMigracion(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Contrato.Entity.Contrato ConsultarNumeroCDPObligacion(int pIdContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContrato(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Entity.Contrato> ConsultarContratosSimple(DateTime? pFechaRegistroSistemaDesde, DateTime? pFechaRegistroSistemaHasta, int? pIdContrato, string pNumeroContrato, string pNumeroProceso, int? pVigenciaFiscalinicial, int? pIDRegional, int? pIDModalidadSeleccion, int? pIDCategoriaContrato, int? pIDTipoContrato, int? pIDEstadoContrato, int? pIdDependenciaSolicitante, bool? pManejaRecurso)
        {
            try
            {

                return vContratoDAL.ConsultarContratosSimple(pFechaRegistroSistemaDesde, pFechaRegistroSistemaHasta, pIdContrato, pNumeroContrato, pNumeroProceso, pVigenciaFiscalinicial, pIDRegional, pIDModalidadSeleccion, pIDCategoriaContrato, pIDTipoContrato, pIDEstadoContrato, pIdDependenciaSolicitante, pManejaRecurso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet ConsultarContratosSuscritosObligacionesPresupuestales(string ids, string usuario, string rol)
        {
            try
            {
                return vContratoDAL.ConsultarContratosSuscritosObligacionesPresupuestales(ids,usuario, rol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarContratosSuscritosObligacionesPresupuestales(string ids, string usuario)
        {
            try
            {
                return vContratoDAL.ActualizarContratosSuscritosObligacionesPresupuestales(ids, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosGenerarObligaciones(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosGenerarObligaciones(pFechaSuscripcion, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosInformeObligaciones(DateTime? pFechaInicioDesde, DateTime? pFechaInicioHasta, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato)
        {
            try
            {
                return vContratoDAL.ConsultarContratosInformeObligaciones(pFechaInicioDesde, pFechaInicioHasta, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Contrato.Entity.Contrato> ConsultarContratosGenerarAreas(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato, string idDependencia)
        {
            try
            {
                return vContratoDAL.ConsultarContratosGenerarAreas(pFechaSuscripcion, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato, idDependencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet ConsultarContratosGenerarAreasDataSet(DateTime? pFechaSuscripcion, String pNumeroContrato, int pVigenciaFiscalinicial, int pIDRegional, int? pIDCategoriaContrato, int? pIDTipoContrato, string idDependencia)
        {
            try
            {
                return vContratoDAL.ConsultarContratosGenerarAreasDataSet(pFechaSuscripcion, pNumeroContrato, pVigenciaFiscalinicial, pIDRegional, pIDCategoriaContrato, pIDTipoContrato, idDependencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
