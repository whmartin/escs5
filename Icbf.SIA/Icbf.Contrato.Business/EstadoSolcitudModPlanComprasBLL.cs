﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  EstadoSolcitudModPlanCompras
    /// </summary>
    public class EstadoSolcitudModPlanComprasBLL
    {
        private EstadoSolcitudModPlanComprasDAL vEstadoSolcitudModPlanComprasDAL;
        public EstadoSolcitudModPlanComprasBLL()
        {
            vEstadoSolcitudModPlanComprasDAL = new EstadoSolcitudModPlanComprasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int InsertarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasDAL.InsertarEstadoSolcitudModPlanCompras(pEstadoSolcitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int ModificarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasDAL.ModificarEstadoSolcitudModPlanCompras(pEstadoSolcitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pEstadoSolcitudModPlanCompras"></param>
        public int EliminarEstadoSolcitudModPlanCompras(EstadoSolcitudModPlanCompras pEstadoSolcitudModPlanCompras)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasDAL.EliminarEstadoSolcitudModPlanCompras(pEstadoSolcitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pIdEstadoSolicitud"></param>
        public EstadoSolcitudModPlanCompras ConsultarEstadoSolcitudModPlanCompras(String pIdEstadoSolicitud)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasDAL.ConsultarEstadoSolcitudModPlanCompras(pIdEstadoSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad EstadoSolcitudModPlanCompras
        /// </summary>
        /// <param name="pCodEstado"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pActivo"></param>
        public List<EstadoSolcitudModPlanCompras> ConsultarEstadoSolcitudModPlanComprass(String pCodEstado, String pDescripcion, Boolean? pActivo)
        {
            try
            {
                return vEstadoSolcitudModPlanComprasDAL.ConsultarEstadoSolcitudModPlanComprass(pCodEstado, pDescripcion, pActivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

