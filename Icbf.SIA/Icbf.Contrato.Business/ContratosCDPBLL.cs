﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System.Data;


namespace Icbf.Contrato.Business
{
    public class ContratosCDPBLL
    {
         private ContratosCDPDAL vContratoCDPDAL;
         public ContratosCDPBLL()
        {
            vContratoCDPDAL = new ContratosCDPDAL();
        }

        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP">Instancoa con el registro de contratosCDP</param>
        public int InsertarContratosCDP(ContratosCDP pContratosCDP, DataTable vDTRubros)
        {
            try
            {
                return vContratoCDPDAL.InsertarContratosCDP(pContratosCDP, vDTRubros);
            
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP">Instancoa con el registro de contratosCDP</param>
        public int InsertarContratosCDP(ContratosCDP pContratosCDP)
        {
            try
            {
                return vContratoCDPDAL.InsertarContratosCDP(pContratosCDP);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminar para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pContratosCDP"></param>
        /// <returns></returns>
        public int EliminarContratosCDP(ContratosCDP pContratosCDP)
        {
            try
            {
                return vContratoCDPDAL.EliminarContratosCDP(pContratosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarContratosCDPVigencias(ContratosCDP pContratosCDP, int pIdVigenciaFutura)
        {
            try
            {
                return vContratoCDPDAL.EliminarContratosCDPVigencias(pContratosCDP, pIdVigenciaFutura);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Método de consulta CDPs asociados a un contrato
        /// 01-07-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <returns>Lista de entidades ContratosCDP con la información obtenida de la base de datos</returns>
        public List<ContratosCDP> ConsultarContratosCDP(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDP(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPUnificado(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPUnificado(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPSimple(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPSimple(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroSimple(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPRubroSimple(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroUnificado(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPRubroUnificado(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPVigencias(int pIdContrato, bool pEsVigenciaFutura, int ? pAnioVigencia)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPVigencias(pIdContrato, pEsVigenciaFutura, pAnioVigencia);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ObligacionesCDP ConsultarDatosContratoSigepcyp(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarDatosContratoSigepcyp(pIdContrato);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPLinea(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPLinea(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroLinea(int pIdContrato)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPRubroLinea(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPLineaVF(int pIdContrato, bool EsVigenciaFutura, int? aniovigencia)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPLineaVF(pIdContrato,EsVigenciaFutura,aniovigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratosCDP> ConsultarContratosCDPRubroLineaVF(int pIdContrato, bool EsVigenciaFutura, int? aniovigencia)
        {
            try
            {
                return vContratoCDPDAL.ConsultarContratosCDPRubroLineaVF(pIdContrato,EsVigenciaFutura,aniovigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
