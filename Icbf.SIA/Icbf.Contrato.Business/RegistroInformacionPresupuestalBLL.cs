using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using ICBF.MasterData.Entity;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  RegistroInformacionPresupuestal
    /// </summary>
    public class RegistroInformacionPresupuestalBLL
    {
        private RegistroInformacionPresupuestalDAL vRegistroInformacionPresupuestalDAL;
        public RegistroInformacionPresupuestalBLL()
        {
            vRegistroInformacionPresupuestalDAL = new RegistroInformacionPresupuestalDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pRegistroInformacionPresupuestal"></param>
        public int InsertarRegistroInformacionPresupuestal(RegistroInformacionPresupuestal pRegistroInformacionPresupuestal)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.InsertarRegistroInformacionPresupuestal(pRegistroInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pRegistroInformacionPresupuestal"></param>
        public int ModificarRegistroInformacionPresupuestal(RegistroInformacionPresupuestal pRegistroInformacionPresupuestal)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ModificarRegistroInformacionPresupuestal(pRegistroInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pRegistroInformacionPresupuestal"></param>
        public int EliminarRegistroInformacionPresupuestal(RegistroInformacionPresupuestal pRegistroInformacionPresupuestal)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.EliminarRegistroInformacionPresupuestal(pRegistroInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name=""></param>
        public RegistroInformacionPresupuestal ConsultarRegistroInformacionPresupuestal()
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestal();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad RegistroInformacionPresupuestal
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestals(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, Decimal? pValorTotalDesde, Decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestals(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        /// <param name="pNumeroRegistros"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalSimple(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestalSimple(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalUnificado(int? pVigenciaFiscal, int? pRegionalICBF, string pNumeroCDP, int? pArea, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestalUnificado(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalSimpleRubro(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestalSimpleRubro(pVigenciaFiscal, pRegionalICBF, pNumeroCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestalUnificadoRubro(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestalUnificadoRubro(pVigenciaFiscal, pRegionalICBF, pNumeroCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet 
        /// Método de consulta por filtros de areas internas
        /// 4-Feb-2014
        /// </summary>
        /// <param name="pRegionalICBF">vlor entero con el codigo codigo de la regional</param>
        /// <returns>Lista con las instancias que contiene las areas, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<AreasInternas> ConsultarAreasInternas(int? pRegionalICBF)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarAreasInternas(pRegionalICBF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet 
        /// Método de consulta los contaros relacionados a un número de CDP
        /// 26-05-2014
        /// </summary>
        /// <param name="pIdContrato">Valor entero con el id del contrato</param>
        /// <param name="pIdCDP">Valor entero con el id del CDP</param>
        /// <returns></returns>
        public bool ConsultarContratosCDP(int pIdContrato, int pIdCDP)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarContratosCDP(pIdContrato, pIdCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pIdContrato"></param>
        /// <returns></returns>
        public List<RegistroInformacionPresupuestal> ConsultarRegistroInformacionPresupuestals(int numeroCDP, int pIdEtlCdp)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestals(numeroCDP, pIdEtlCdp);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pVigenciaFiscal"></param>
        /// <param name="pRegionalICBF"></param>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pArea"></param>
        /// <param name="pValorTotalDesde"></param>
        /// <param name="pValorTotalHasta"></param>
        /// <param name="pFechaCDPDesde"></param>
        /// <param name="pFechaCDPHasta"></param>
        /// <param name="pNumeroRegistros"></param>
        /// <returns></returns>
        public KeyValuePair<int,ConsultaCDPMaestro> ConsultarCDPConRubros(int pVigenciaFiscal, int pRegionalICBF, string pNumeroCDP, int? pArea, decimal? pValorTotalDesde, decimal? pValorTotalHasta, DateTime? pFechaCDPDesde, DateTime? pFechaCDPHasta, int pNumeroRegistros)
        {
            ConsultaCDPMaestro result = new ConsultaCDPMaestro();
            int idContratosCDP = 0;
            try
            {
                var itemCDP =  vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestalSimple(pVigenciaFiscal, pRegionalICBF, pNumeroCDP, pArea, pValorTotalDesde, pValorTotalHasta, pFechaCDPDesde, pFechaCDPHasta, pNumeroRegistros);

                if (itemCDP != null && itemCDP.Count() > 0)
                {
                    var itemCDPFirst = itemCDP.First();
                    result.FechaCDP = itemCDPFirst.FechaCDP.ToShortDateString();
                    result.NumeroCDP = itemCDPFirst.NumeroCDP;
                    result.ValorCDP = itemCDPFirst.ValorCDP.ToString();
                    idContratosCDP = itemCDPFirst.IdEtlCDP;
                    var itemsCDPDetalle = vRegistroInformacionPresupuestalDAL.ConsultarRegistroInformacionPresupuestalSimpleRubro(pVigenciaFiscal, pRegionalICBF, pNumeroCDP);

                    if (itemsCDPDetalle != null && itemsCDPDetalle.Count () > 0)
                    {
                        List<ConsultaCDPDetalle> resultDetalle = new List<ConsultaCDPDetalle>();

                        ConsultaCDPDetalle resultDetalleItem;

                        foreach (var itemCDPDetalle in itemsCDPDetalle)
                        {
                            resultDetalleItem = new ConsultaCDPDetalle();
                            resultDetalleItem.DescripcionDependenciaAfectacionGasto = itemCDPDetalle.DependenciaAfectacionGastos;
                            resultDetalleItem.RubroPresupuestal = itemCDPDetalle.RubroPresupuestal;
                            resultDetalleItem.DescripcionTipoFuenteFinanciamiento = itemCDPDetalle.TipoFuenteFinanciamiento;
                            resultDetalleItem.DescripcionTipoRecursoPresupuestal = itemCDPDetalle.RecursoPresupuestal;
                            resultDetalleItem.DescripcionTipoSituacionFondos = itemCDPDetalle.TipoSituacionFondos;
                            resultDetalleItem.ValorDetalle = itemCDPDetalle.ValorCDP.ToString();
                            resultDetalleItem.CodigoRuproPresupuestal = itemCDPDetalle.CodigoRubro;
                            resultDetalle.Add(resultDetalleItem);
                        }

                        result.detalle = resultDetalle;
                    }
                }
            }
            catch (Exception ex)
            { 
                throw ex;
            }

            return new KeyValuePair<int,ConsultaCDPMaestro>(idContratosCDP,result);
        }

        public int ConsultarExisteCPDAsociadoContrato(int numeroCDP, DateTime fecha, int idRegional)
        {
            try
            {
                return vRegistroInformacionPresupuestalDAL.ConsultarExisteCPDAsociadoContrato(numeroCDP, fecha, idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
