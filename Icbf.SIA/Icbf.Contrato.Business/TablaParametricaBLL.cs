﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.DataAccess; 

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Tabla Paramétrica
    /// </summary>
    public class TablaParametricaBLL
    {
        private TablaParametricaDAL vTablaParametricaDAL;
        public TablaParametricaBLL()
        {
            vTablaParametricaDAL = new TablaParametricaDAL();
        }
        /// <summary>
        /// Método de consulta por filtros para la entidad TablaParametrica
        /// </summary>
        /// <param name="pCodigoTablaParametrica"></param>
        /// <param name="pNombreTablaParametrica"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TablaParametrica> ConsultarTablaParametricas(String pCodigoTablaParametrica, String pNombreTablaParametrica, Boolean? pEstado, bool pEsPrecontractual)
        {
            try
            {
                return vTablaParametricaDAL.ConsultarTablaParametricas(pCodigoTablaParametrica, pNombreTablaParametrica, pEstado, pEsPrecontractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
