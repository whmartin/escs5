﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ConsultarSuperInterContrato
    /// </summary>
    public class ConsultarSuperInterContratoBLL
    {
        private ConsultarSuperInterContratoDAL vConsultarSuperInterContratoDAL;
        public ConsultarSuperInterContratoBLL()
        {
            vConsultarSuperInterContratoDAL = new ConsultarSuperInterContratoDAL();
        }
        ///// <summary>
        ///// Método de inserción para la entidad ConsultarSuperInterContrato
        ///// </summary>
        ///// <param name="pConsultarSuperInterContrato"></param>
        //public int InsertarConsultarSuperInterContrato(ConsultarSuperInterContrato pConsultarSuperInterContrato)
        //{
        //    try
        //    {
        //        return vConsultarSuperInterContratoDAL.InsertarConsultarSuperInterContrato(pConsultarSuperInterContrato);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de modificación para la entidad ConsultarSuperInterContrato
        ///// </summary>
        ///// <param name="pConsultarSuperInterContrato"></param>
        //public int ModificarConsultarSuperInterContrato(ConsultarSuperInterContrato pConsultarSuperInterContrato)
        //{
        //    try
        //    {
        //        return vConsultarSuperInterContratoDAL.ModificarConsultarSuperInterContrato(pConsultarSuperInterContrato);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de eliminación para la entidad ConsultarSuperInterContrato
        ///// </summary>
        ///// <param name="pConsultarSuperInterContrato"></param>
        //public int EliminarConsultarSuperInterContrato(ConsultarSuperInterContrato pConsultarSuperInterContrato)
        //{
        //    try
        //    {
        //        return vConsultarSuperInterContratoDAL.EliminarConsultarSuperInterContrato(pConsultarSuperInterContrato);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Método de consulta por id para la entidad ConsultarSuperInterContrato
        /// </summary>
        /// <param name="pIDSupervisorIntervContrato"></param>
        public ConsultarSuperInterContrato ConsultarConsultarSuperInterContrato(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vConsultarSuperInterContratoDAL.ConsultarConsultarSuperInterContrato(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ConsultarSuperInterContrato
        /// </summary>
        /// <param name="pIDSupervisorInterventor"></param>
        /// <param name="pNumeroContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pTipoIdentificacion"></param>
        /// <param name="pRazonSocial"></param>
        /// <param name="pNombreSuperInterv"></param>
        /// <param name="pNumIdentifDirInterventoria"></param>
        /// <param name="pNombreDirInterventoria"></param>
        /// <param name="pTipoContrato"></param>
        /// <param name="pRegionalContrato"></param>
        /// <param name="pFechaSuscripcion"></param>
        /// <param name="pFechaTerminacion"></param>
        /// <param name="pFechaInicioSuperInterv"></param>
        /// <param name="pFechaFinalSuperInterv"></param>
        /// <param name="pFechaModSuperInterv"></param>
        /// <param name="pNumeroContratoInterventoria"></param>
        /// <param name="pTelefonoInterventoria"></param>
        /// <param name="pTelCelularInterventoria"></param>
        public List<ConsultarSuperInterContrato> ConsultarConsultarSuperInterContratos(String pCodSupervisorInterventor, String pNumeroContrato, String pNumeroIdentificacion, String pRazonSocial, String pNombreSuperInterv, String pNumIdentifDirInterventoria, String pNombreDirInterventoria)
        {
            try
            {
                return vConsultarSuperInterContratoDAL.ConsultarConsultarSuperInterContratos(pCodSupervisorInterventor, pNumeroContrato, pNumeroIdentificacion, pRazonSocial, pNombreSuperInterv, pNumIdentifDirInterventoria, pNombreDirInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}


