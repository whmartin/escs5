﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class SolModContractualBLL 
    { 
        private SolModContractualDAL vSolModContractualDAL;

        public SolModContractualBLL()
        {
            vSolModContractualDAL = new SolModContractualDAL();
        }

        public int InsertarSolModContractual(ConsModContractual psModContractual)
        {
            try
            {
                return vSolModContractualDAL.InsertarSolModContractual(psModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarSolModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vSolModContractualDAL.ModificarSolModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public List<SolModContractualContrato> ConsultarContratos(string numeroContrato,
                                                                  int? vigenciaFiscal,
                                                                  int? regional,
                                                                  int? idCategoriaContrato,
                                                                  int? idTipoContrato,
                                                                  string numeroIdentificacion,
                                                                  int? valorContratoDesde,
                                                                  int? valorContratoHasta)
        {
            try
            {
                return vSolModContractualDAL.ConsultarContratos(numeroContrato, vigenciaFiscal, regional, idCategoriaContrato, idTipoContrato,numeroIdentificacion, valorContratoDesde, valorContratoHasta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public SolModContractualContrato ConsultarContrato(int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ConsultarContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }       

        public List<SolModContractual> ConsultarSolicitudesPorContrato(int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ConsultarSolicitudesPorContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarSolicitudesRechazadasPorContrato(int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ConsultarSolicitudesRechazadasPorContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractual ConsultarSolitud(int IDCosModContractual)
        {
            try
            {
                return vSolModContractualDAL.ConsultarSolitud(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ConsModContractual ConsultarUltimaSolitud(int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ConsultarUltimaSolicitud(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractualDetalle> ConsultarSolicitudesDetalle(int IDCosModContractual)
        {
            try
            {
                return vSolModContractualDAL.ConsultarSolicitudesDetalle(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public bool ConsultarExisteSolicitudModificacion(int idContrato, int idTipoSolicitud)
        {
            bool existe = false;

            try
            {
                return vSolModContractualDAL.ConsultarExisteSolicitudModificacion(idContrato, idTipoSolicitud);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public bool ConsultarPuedeModificarTipoSolicitud(int idCosModContractual)
        {
            bool existe = false;

            try
            {
                return vSolModContractualDAL.ConsultarPuedeModificarTipoSolicitud(idCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public bool ConsultarSiesContratoSV(int idContrato)
        {
            bool existe = false;

            try
            {
                return vSolModContractualDAL.ConsultarSiesContratoSV(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return existe;
        }

        public List<SolModContractualValidacion> ValidarCambiodeEstadoaEnviada(int IDConsModContractual)
        {
            try
            {
                return vSolModContractualDAL.ValidarCambiodeEstadoaEnviada(IDConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CambiarEstadoRegistroaEnviada(int IDConsModContractual)
        {
            try
            {
                return vSolModContractualDAL.CambiarEstadoRegistroaEnviada(IDConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionEstadoContractualContrato(int IDConsModContractual, int Estado, String JustificacionRechazo, String JustificacionDevolucion)
        {
            try
            {
                return vSolModContractualDAL.ModificacionEstadoContractualContrato(IDConsModContractual, Estado, JustificacionRechazo, JustificacionDevolucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionContractualContrato(int IDCosModContractual, string usuario)
        {
            try
            {
                return vSolModContractualDAL.ModificacionContractualContrato(IDCosModContractual, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificacionContractualContratoRechazoDevolucion(int IDCosModContractual, bool esRechazo, string usuario, string justificacion)
        {
            try
            {
                return vSolModContractualDAL.ModificacionContractualContratoRechazoDevolucion(IDCosModContractual, esRechazo, usuario, justificacion);   
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public IDictionary<string,int> ConsultarConsolidadoModificaciones(int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ConsultarConsolidadoModificaciones(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoSolicitudesContrato> ConsultarModificacionesHistorico(int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ConsultarModificacionesHistorico(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public  List<string> PuedeCrearSolicitud(int idContrato, int idTipoModificacion, int idConsModcontractual)
         {
            try
            {
                return vSolModContractualDAL.PuedeCrearSolicitud(idContrato, idTipoModificacion, idConsModcontractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
         }

        public List<SolModContractualHistorico> ConsultarSolicitudesHistorico(int IDCosModContractual)
        {
            try
            {
                return vSolModContractualDAL.ConsultarSolicitudesHistorico(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int SubscribirModificacionContractualContrato(int idConsmodContractual, string usuario, DateTime fechaAprobacion )
        {
            try
            {
                return vSolModContractualDAL.SubscribirModificacionContractualContrato(idConsmodContractual,  usuario, fechaAprobacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SolModContractual ObtenerModificacionSinModificacionGarantiaPorTipo(int idTipoModificacion, int idContrato)
        {
            try
            {
                return vSolModContractualDAL.ObtenerModificacionSinModificacionGarantiaPorTipo(idTipoModificacion, idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
            
        }
    }
}
