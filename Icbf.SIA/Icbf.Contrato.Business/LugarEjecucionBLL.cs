using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Lugar de ejecuci�n
    /// </summary>
    public class LugarEjecucionBLL
    {
        private LugarEjecucionDAL vLugarEjecucionDAL;
        public LugarEjecucionBLL()
        {
            vLugarEjecucionDAL = new LugarEjecucionDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad LugarEjecucion
        /// </summary>
        /// <param name="pLugarEjecucion"></param>
        /// <returns></returns>
        public int InsertarLugarEjecucion(LugarEjecucion pLugarEjecucion)
        {
            try
            {
                return vLugarEjecucionDAL.InsertarLugarEjecucion(pLugarEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad LugarEjecucion
        /// </summary>
        /// <param name="pLugarEjecucion"></param>
        /// <returns></returns>
        public int EliminarLugarEjecucion(LugarEjecucion pLugarEjecucion)
        {
            try
            {
                return vLugarEjecucionDAL.EliminarLugaresEjecucion(pLugarEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad LugarEjecucion
        /// </summary>
        /// <param name="IdContratoLugarEjecucion"></param>
        /// <returns></returns>
        public List<LugarEjecucion> ConsultarLugarEjecucions(int IdContratoLugarEjecucion)
        {
            try
            {
                return vLugarEjecucionDAL.ConsultarLugarEjecucions(IdContratoLugarEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
