using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Informaci�n Presupuestal
    /// </summary>
    public class InformacionPresupuestalBLL
    {
        private InformacionPresupuestalDAL vInformacionPresupuestalDAL;
        public InformacionPresupuestalBLL()
        {
            vInformacionPresupuestalDAL = new InformacionPresupuestalDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int InsertarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalDAL.InsertarInformacionPresupuestal(pInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int ModificarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalDAL.ModificarInformacionPresupuestal(pInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pInformacionPresupuestal"></param>
        /// <returns></returns>
        public int EliminarInformacionPresupuestal(InformacionPresupuestal pInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalDAL.EliminarInformacionPresupuestal(pInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pIdInformacionPresupuestal"></param>
        /// <returns></returns>
        public InformacionPresupuestal ConsultarInformacionPresupuestal(int pIdInformacionPresupuestal)
        {
            try
            {
                return vInformacionPresupuestalDAL.ConsultarInformacionPresupuestal(pIdInformacionPresupuestal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad InformacionPresupuestal
        /// </summary>
        /// <param name="pNumeroCDP"></param>
        /// <param name="pFechaExpedicionCDP"></param>
        /// <returns></returns>
        public List<InformacionPresupuestal> ConsultarInformacionPresupuestals(String pNumeroCDP, DateTime? pFechaExpedicionCDP)
        {
            try
            {
                return vInformacionPresupuestalDAL.ConsultarInformacionPresupuestals(pNumeroCDP, pFechaExpedicionCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
