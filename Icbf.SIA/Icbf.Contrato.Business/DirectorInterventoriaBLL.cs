using System;
using System.Collections.Generic;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  DirectorInterventoria
    /// </summary>
    public class DirectorInterventoriaBLL
    {
        private DirectorInterventoriaDAL vDirectorInterventoriaDAL;
        public DirectorInterventoriaBLL()
        {
            vDirectorInterventoriaDAL = new DirectorInterventoriaDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pDirectorInterventoria"></param>
        public int InsertarDirectorInterventoria(DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                return vDirectorInterventoriaDAL.InsertarDirectorInterventoria(pDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pDirectorInterventoria"></param>
        public int ModificarDirectorInterventoria(DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                return vDirectorInterventoriaDAL.ModificarDirectorInterventoria(pDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pDirectorInterventoria"></param>
        public int EliminarDirectorInterventoria(DirectorInterventoria pDirectorInterventoria)
        {
            try
            {
                return vDirectorInterventoriaDAL.EliminarDirectorInterventoria(pDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pIDDirectorInterventoria"></param>
        public DirectorInterventoria ConsultarDirectorInterventoria(int pIDDirectorInterventoria)
        {
            try
            {
                return vDirectorInterventoriaDAL.ConsultarDirectorInterventoria(pIDDirectorInterventoria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad DirectorInterventoria
        /// </summary>
        /// <param name="pIdTipoIdentificacion"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pPrimerNombre"></param>
        /// <param name="pSegundoNombre"></param>
        /// <param name="pPrimerApellido"></param>
        /// <param name="pSegundoApellido"></param>
        /// <param name="pCelular"></param>
        /// <param name="pTelefono"></param>
        /// <param name="pCorreoElectronico"></param>
        public List<DirectorInterventoria> ConsultarDirectorInterventorias(int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido, String pCelular, String pTelefono, String pCorreoElectronico)
        {
            try
            {
                return vDirectorInterventoriaDAL.ConsultarDirectorInterventorias(pIdTipoIdentificacion, pNumeroIdentificacion, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido, pCelular, pTelefono, pCorreoElectronico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
