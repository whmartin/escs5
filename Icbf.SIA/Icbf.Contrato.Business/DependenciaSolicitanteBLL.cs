﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class DependenciaSolicitanteBLL
    {
        private DependenciaSolicitanteDAL vDependenciaSolicitanteDAL;
        public DependenciaSolicitanteBLL()
        {
            vDependenciaSolicitanteDAL = new DependenciaSolicitanteDAL();
        }


        /// <summary>
        /// Método de consulta por filtros para la entidad DependenciaSolicitante
        /// </summary>
        /// <param name="pIdDependenciaSolicitante"></param>
        /// <param name="pNombreDependenciaSolicitante"></param>
        /// <returns></returns>
        public List<DependenciaSolicitante> ConsultarDependenciaSolicitante(int? pIdDependenciaSolicitante
            , String pNombreDependenciaSolicitante,string pCodigoRegional)
        {
            try
            {
                return vDependenciaSolicitanteDAL.ConsultarDependenciaSolicitante(pIdDependenciaSolicitante,  pNombreDependenciaSolicitante, pCodigoRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
