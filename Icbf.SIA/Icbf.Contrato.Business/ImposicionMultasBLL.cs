﻿using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class ImposicionMultasBLL
    {
        private ImposicionMultaDAL _ImposicionMultaDAL;

        public ImposicionMultasBLL()
        {
            _ImposicionMultaDAL = new ImposicionMultaDAL();
        }

        public int InsertarImposicionMulta(ImposicionMulta pImposicion)
        {
            try
            {
                return _ImposicionMultaDAL.InsertarImposicionMulta(pImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarImposicionMulta(ImposicionMulta pImposicion)
        {
            try
            {
                return _ImposicionMultaDAL.ModificarImposicionMulta(pImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMulta(int IdContrato)
        {
            try
            {
                return _ImposicionMultaDAL.ConsultarImposicionMulta(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMultaId(int IdImposicion)
        {
            try
            {
                return _ImposicionMultaDAL.ConsultarImposicionMultaId(IdImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMulta> ConsultarImposicionMultaIdDetalle(int IdDetConsModContractual)
        {
            try
            {
                return _ImposicionMultaDAL.ConsultarImposicionMultaIdDetalle(IdDetConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ImposicionMultaHistorico> ConsultarHistoricoProcesoImpMultas(int idImposicion)
        {
            try
            {
                return _ImposicionMultaDAL.ConsultarHistoricoProcesoImpMultas(idImposicion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
