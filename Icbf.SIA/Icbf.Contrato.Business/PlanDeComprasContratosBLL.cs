﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  PlanDeComprasContratos
    /// </summary>
    public class PlanDeComprasContratosBLL
    {
        private PlanDeComprasContratosDAL vPlanDeComprasContratosDAL;
        public PlanDeComprasContratosBLL()
        {
            vPlanDeComprasContratosDAL = new PlanDeComprasContratosDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int InsertarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosDAL.InsertarPlanDeComprasContratos(pPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int ModificarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosDAL.ModificarPlanDeComprasContratos(pPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pPlanDeComprasContratos"></param>
        public int EliminarPlanDeComprasContratos(PlanDeComprasContratos pPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosDAL.EliminarPlanDeComprasContratos(pPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;

            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pIDPlanDeComprasContratos"></param>
        public PlanDeComprasContratos ConsultarPlanDeComprasContratos(int pIDPlanDeComprasContratos)
        {
            try
            {
                return vPlanDeComprasContratosDAL.ConsultarPlanDeComprasContratos(pIDPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanDeComprasContratos
        /// </summary>
        /// <param name="pIdContrato"></param>
        /// <param name="pIDPlanDeCompras"></param>
        public List<PlanDeComprasContratos> ConsultarPlanDeComprasContratoss(int? pIdContrato, int? pIDPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasContratosDAL.ConsultarPlanDeComprasContratoss(pIdContrato, pIDPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanDeComprasContratos> ConsultarPlanDeComprasContratosVigencias(int? pIdContrato, int? pIDPlanDeCompras, bool? pesVigenciaFutura, int? pAnioVigencia)
        {
            try
            {
                return vPlanDeComprasContratosDAL.ConsultarPlanDeComprasContratosVigencias(pIdContrato, pIDPlanDeCompras, pesVigenciaFutura, pAnioVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public bool VerificarExistenciaModificacionProducto(int IDPlanDeComprasContratos, string IdProducto, bool esAdicion, int idModificacion)
        {
            bool isValid = false;

            try
            {
                isValid = vPlanDeComprasContratosDAL.VerificarExistenciaModificacionProducto(IDPlanDeComprasContratos, IdProducto, esAdicion, idModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return isValid;
        }



    }
}

