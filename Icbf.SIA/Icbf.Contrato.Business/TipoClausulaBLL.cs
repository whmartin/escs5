using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo Cl�lusula
    /// </summary>
    public class TipoClausulaBLL
    {
        private TipoClausulaDAL vTipoClausulaDAL;
        public TipoClausulaBLL()
        {
            vTipoClausulaDAL = new TipoClausulaDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int InsertarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                return vTipoClausulaDAL.InsertarTipoClausula(pTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int ModificarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                return vTipoClausulaDAL.ModificarTipoClausula(pTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoClausula
        /// </summary>
        /// <param name="pTipoClausula"></param>
        /// <returns></returns>
        public int EliminarTipoClausula(TipoClausula pTipoClausula)
        {
            try
            {
                return vTipoClausulaDAL.EliminarTipoClausula(pTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoClausula
        /// </summary>
        /// <param name="pIdTipoClausula"></param>
        /// <returns></returns>
        public TipoClausula ConsultarTipoClausula(int pIdTipoClausula)
        {
            try
            {
                return vTipoClausulaDAL.ConsultarTipoClausula(pIdTipoClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoClausula
        /// </summary>
        /// <param name="pNombreTipoClausula"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoClausula> ConsultarTipoClausulas(String pNombreTipoClausula,String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoClausulaDAL.ConsultarTipoClausulas(pNombreTipoClausula,pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
