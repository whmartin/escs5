using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  FormaPago
    /// </summary>
    public class FormaPagoBLL
    {
        private FormaPagoDAL vFormaPagoDAL;
        public FormaPagoBLL()
        {
            vFormaPagoDAL = new FormaPagoDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la información de la entidad Forma de pago</param>
        public int InsertarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                return vFormaPagoDAL.InsertarFormaPago(pFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la información de la entidad Forma de pago</param>
        public int ModificarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                return vFormaPagoDAL.ModificarFormaPago(pFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad FormaPago
        /// </summary>
        /// <param name="pFormaPago">Instancia con la información de la entidad Forma de pago</param>
        public int EliminarFormaPago(FormaPago pFormaPago)
        {
            try
            {
                return vFormaPagoDAL.EliminarFormaPago(pFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad FormaPago
        /// </summary>
        /// <param name="pIdFormaPago">Valor entero con el Id de la forma de pago</param>
        public FormaPago ConsultarFormaPago(int pIdFormaPago)
        {
            try
            {
                return vFormaPagoDAL.ConsultarFormaPago(pIdFormaPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad FormaPago
        /// </summary>
        /// <param name="pIdMedioPago">Valor entero con el Id del medio de pago</param>
        /// <param name="pIdEntidadFinanciera">Valor entero con el id de entidad financiera</param>
        /// <param name="pTipoCuentaBancaria">Valor entero con el Id del tipo de cuenta bancaria</param>
        /// <param name="pNumeroCuentaBancaria">Cadena de texto con el número de la cuenta bancaria</param>
        public List<FormaPago> ConsultarFormaPagos(int? pIdProveedores, int? pIdMedioPago, int? pIdEntidadFinanciera, int? pTipoCuentaBancaria, String pNumeroCuentaBancaria)
        {
            try
            {
                return vFormaPagoDAL.ConsultarFormaPagos(pIdProveedores, pIdMedioPago, pIdEntidadFinanciera, pTipoCuentaBancaria, pNumeroCuentaBancaria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consultar Entidad Financiera 
        /// Fecha: 17/01/2014
        /// </summary>
        /// <param name="pCodigoEntFin">Cadena de texto con el código de la entidad financiera</param>
        /// <param name="pNombreEntFin">Cadena de texto con el nombre de la entidad financiera</param>
        /// <param name="pEstado">Valor Booleano con el estado de la entidad financiera</param>
        /// <param name="pEsPagoPorOrdenDePago">Valor Booleano con el estado pago por orden de pago</param>
        /// <param name="pEsPagoPorOrdenBancaria">Valor Booleano con el estado pago por orden bancaria</param>
        /// <returns>Lista con la Instancia que contiene la información de la entidad financiera</returns>
        public List<EntidadFinanciera> ConsultarEntidadFinancieras(String pCodigoEntFin, String pNombreEntFin, Boolean? pEstado, Boolean? pEsPagoPorOrdenDePago, Boolean? pEsPagoPorOrdenBancaria)
        {
            try
            {
                return vFormaPagoDAL.ConsultarEntidadFinancieras(pCodigoEntFin, pNombreEntFin, pEstado, pEsPagoPorOrdenDePago, pEsPagoPorOrdenBancaria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Consulta los tipos de cuenta de entidad financiera segun los filtros dados
        /// 30-01-2014
        /// </summary>
        /// <param name="pCodTipoCta">Cadena de texto con el codigo del tipo de cuenta</param>
        /// <param name="pDescripcion">Cadena de texto con la descripcion del tipo de cuenta</param>
        /// <param name="pEstado">Booleano que indica el estado</param>
        /// <returns>Lista de instancias que contiene los tipo código de retención</returns>
        public List<TipoCuentaEntFin> ConsultarTipoCuentaEntFins(String pCodTipoCta, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vFormaPagoDAL.ConsultarTipoCuentaEntFins(pCodTipoCta, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet 
        /// Permite consultar el tipo de medio de pago a partir de los 
        /// filtros dados
        /// 7-Feb-2014
        /// </summary>
        /// <param name="pCodMedioPago">Cadena de texto con el codigo medio de pago</param>
        /// <param name="pDescTipoMedioPago">Cadena de texto con la descripcion del tipo medio de pago</param>
        /// <param name="pEstado">Booleano que indica el estado</param>
        /// <returns>Lista con las instancias que contiene los tipos de medio de pago, que coinciden con los filtros,  
        /// obtenidos de la base de datos</returns>
        public List<TipoMedioPago> ConsultarTipoMedioPagos(String pCodMedioPago, String pDescTipoMedioPago, Boolean? pEstado)
        {
            try
            {
                return vFormaPagoDAL.ConsultarTipoMedioPagos(pCodMedioPago, pDescTipoMedioPago, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros si el usuario que creó el registro sea el mismo que el que ingresa al sistema
        /// </summary>
        /// <param name="pIdMedioPago">Valor entero con el Id del medio de pago</param>
        /// <param name="pIdEntidadFinanciera">Valor entero con el id de entidad financiera</param>
        /// <param name="pTipoCuentaBancaria">Valor entero con el Id del tipo de cuenta bancaria</param>
        /// <param name="pNumeroCuentaBancaria">Cadena de texto con el número de la cuenta bancaria</param>
        /// <param name="pUsuarioCrea">Cadena de texto con el nombre del usuario creación</param>
        public bool ConsultarFormaPagosUsuarioCrea(int? pIdProveedores, int? pIdMedioPago, int? pIdEntidadFinanciera,
                                                   int? pTipoCuentaBancaria, String pNumeroCuentaBancaria,
                                                   string pUsuarioCrea)
        {
            try
            {
                return vFormaPagoDAL.ConsultarFormaPagosUsuarioCrea(pIdProveedores, pIdMedioPago, pIdEntidadFinanciera, pTipoCuentaBancaria, pNumeroCuentaBancaria, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros si el usuario que creó el registro sea el mismo que el que ingresa al sistema
        /// </summary>
        /// <param name="pUsuarioCrea">Cadena de texto con el nombre del usuario creación</param>
        public bool ConsultarContratoUsuarioCrea(string pUsuarioCrea, int pidContrato)
        {
            try
            {
                return vFormaPagoDAL.ConsultarContratoUsuarioCrea(pUsuarioCrea, pidContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Gonet
        /// Método de consulta por id para la Tercero
        /// 7-Feb-2014
        /// </summary>
        /// <param name="pIdTercero">Entero con el identificador del tercero</param>
        /// <returns>Instancia que contiene la información del tercero recuperado 
        /// de la base de datos</returns>
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return vFormaPagoDAL.ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de modificación para la entidad FormaPago por contrato
        /// </summary>
        /// /// <param name="pIdContrato">Instancia con la información del id del contrato</param>
        /// <param name="pIdFormaPago">Instancia con la información del id de la forma de pago</param>
        public int ModificarFormaPagoContrato(int pIdProveedoresContratos, int pIdFormaPago, string pUsuarioModifica)
        {
            try
            {
                return vFormaPagoDAL.ModificarFormaPagoContrato(pIdProveedoresContratos, pIdFormaPago, pUsuarioModifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta un(a) Contratos por el id de la forma de pago retorna verdadero si la forma de pago esta asociada a un contrato
        /// </summary>
        /// <param name="pIdFormapago">Valor Entero con el Id de la forma de pago</param>
        /// /// <param name="pIdProveedoresContratos">Valor Entero con el Id ProveedoresContratos</param>
        public bool ConsultarFormaPagoContrato(int pIdFormapago, int pIdProveedoresContratos)
        {
        
            try
            {
                return vFormaPagoDAL.ConsultarFormaPagoContrato(pIdFormapago, pIdProveedoresContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
