using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de gesti�n de cl�usulas
    /// </summary>
    public class GestionarClausulasContratoBLL
    {
        private GestionarClausulasContratoDAL vGestionarClausulasContratoDAL;
        public GestionarClausulasContratoBLL()
        {
            vGestionarClausulasContratoDAL = new GestionarClausulasContratoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int InsertarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                return vGestionarClausulasContratoDAL.InsertarGestionarClausulasContrato(pGestionarClausulasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int ModificarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                return vGestionarClausulasContratoDAL.ModificarGestionarClausulasContrato(pGestionarClausulasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pGestionarClausulasContrato"></param>
        /// <returns></returns>
        public int EliminarGestionarClausulasContrato(GestionarClausulasContrato pGestionarClausulasContrato)
        {
            try
            {
                return vGestionarClausulasContratoDAL.EliminarGestionarClausulasContrato(pGestionarClausulasContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pIdGestionClausula"></param>
        /// <returns></returns>
        public GestionarClausulasContrato ConsultarGestionarClausulasContrato(int pIdGestionClausula)
        {
            try
            {
                return vGestionarClausulasContratoDAL.ConsultarGestionarClausulasContrato(pIdGestionClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad GestionarClausulasContrato
        /// </summary>
        /// <param name="pIdGestionClausula"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNombreClausula"></param>
        /// <param name="pTipoClausula"></param>
        /// <param name="pOrden"></param>
        /// <param name="pOrdenNumero"></param>
        /// <param name="pDescripcionClausula"></param>
        /// <returns></returns>
        public List<GestionarClausulasContrato> ConsultarGestionarClausulasContratos(int? pIdGestionClausula, String pIdContrato, String pNombreClausula, int? pTipoClausula, String pOrden, int? pOrdenNumero, String pDescripcionClausula)
        {
            try
            {
                return vGestionarClausulasContratoDAL.ConsultarGestionarClausulasContratos(pIdGestionClausula, pIdContrato, pNombreClausula, pTipoClausula, pOrden, pOrdenNumero, pDescripcionClausula);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
