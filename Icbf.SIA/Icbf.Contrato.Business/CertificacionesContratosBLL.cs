using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  CertificacionesContratos
    /// </summary>
    public class CertificacionesContratosBLL
    {
        private CertificacionesContratosDAL vCertificacionesContratosDAL;
        public CertificacionesContratosBLL()
        {
            vCertificacionesContratosDAL = new CertificacionesContratosDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int InsertarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return vCertificacionesContratosDAL.InsertarCertificacionesContratos(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int ModificarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return vCertificacionesContratosDAL.ModificarCertificacionesContratos(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int AdicionarArchivoACertificacion(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return vCertificacionesContratosDAL.AdicionarArchivoACertificacion(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pCertificacionesContratos"></param>
        public int EliminarCertificacionesContratos(CertificacionesContratos pCertificacionesContratos)
        {
            try
            {
                return vCertificacionesContratosDAL.EliminarCertificacionesContratos(pCertificacionesContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pIDCertificacion"></param>
        public CertificacionesContratos ConsultarCertificacionesContratos(int pIDCertificacion)
        {
            try
            {
                return vCertificacionesContratosDAL.ConsultarCertificacionesContratos(pIDCertificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad CertificacionesContratos
        /// </summary>
        /// <param name="pIDContrato"></param>
        /// <param name="pFechaCertificacion"></param>
        /// <param name="pEstado"></param>
        public List<CertificacionesContratos> ConsultarCertificacionesContratoss(int? vVigencia, int? vIDRegional
                    , string vNumeroContrato, DateTime? vFechaInicial, DateTime? vFechaFinal)
        {
            try
            {
                return vCertificacionesContratosDAL.ConsultarCertificacionesContratoss(vVigencia,vIDRegional
                    , vNumeroContrato, vFechaInicial, vFechaFinal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<CertificacionesContratos> ConsultarCertificacionesPorContratos(int IDContrato)
        {
            try
            {
                return vCertificacionesContratosDAL.ConsultarCertificacionesPorContratos(IDContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
