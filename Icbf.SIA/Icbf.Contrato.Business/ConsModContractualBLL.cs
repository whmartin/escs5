using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ConsModContractualBLL
    {
        private ConsModContractualDAL vConsModContractualDAL;
        public ConsModContractualBLL()
        {
            vConsModContractualDAL = new ConsModContractualDAL();
        }
        public int InsertarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vConsModContractualDAL.InsertarConsModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vConsModContractualDAL.ModificarConsModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int EliminarConsModContractual(ConsModContractual pConsModContractual)
        {
            try
            {
                return vConsModContractualDAL.EliminarConsModContractual(pConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public ConsModContractual ConsultarConsModContractual(int pIDCosModContractual)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractual(pIDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public ConsModContractual ConsModContractualPorDetalleConsModContractual(int pIDDetalleConsModContractual)
        {
            try
            {
                return vConsModContractualDAL.ConsModContractualPorDetalleConsModContractual(pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConsModContractual> ConsultarConsModContractuals(String pNumeroDoc, int? pIDContrato, String pConsecutivoSuscrito, int? pSuscrito, string pUsuario)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractuals(pNumeroDoc, pIDContrato, pConsecutivoSuscrito, pSuscrito,pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitud(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractualPorSolicitud(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudAdiciones(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractualPorSolicitudAdiciones(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudCesion(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractualPorSolicitudCesion(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualPorSolicitudReparto(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractualPorSolicitudReparto(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SolModContractual> ConsultarConsModContractualImpMultas(string pnumeroContrato, int? pvigenciaFiscal, int? pregional, int? pidCategoriaContrato, int? pidTipoContrato)
        {
            try
            {
                return vConsModContractualDAL.ConsultarConsModContractualImpMultas(pnumeroContrato, pvigenciaFiscal, pregional, pidCategoriaContrato, pidTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
