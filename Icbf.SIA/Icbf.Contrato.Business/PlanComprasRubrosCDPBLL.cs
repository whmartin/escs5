using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  PlanComprasRubrosCDP
    /// </summary>
    public class PlanComprasRubrosCDPBLL
    {
        private PlanComprasRubrosCDPDAL vPlanComprasRubrosCDPDAL;

        public PlanComprasRubrosCDPBLL()
        {
            vPlanComprasRubrosCDPDAL = new PlanComprasRubrosCDPDAL();
        }

        /// <summary>
        /// Método de inserción para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int InsertarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.InsertarPlanComprasRubrosCDP(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int ModificarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.ModificarPlanComprasRubrosCDP(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarRubroPlanComprasContratoReduccionAdicion(RubroPlanComprasContratos pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.ModificarRubroPlanComprasContratoReduccionAdicion(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarRubroPlanComprasContrato(RubroPlanComprasContratos pPlanComprasProductos)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.InsertarRubroPlanComprasContrato(pPlanComprasProductos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool VerificarExistenciaModificacionRubro(int IDPlanDeComprasContratos, string IdRubro, bool esAdicion, int idModificacion)
        {
            bool isValid = false;

            try
            {
                isValid = vPlanComprasRubrosCDPDAL.VerificarExistenciaModificacionRubro(IDPlanDeComprasContratos, IdRubro, esAdicion, idModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }


            return isValid;
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pPlanComprasRubrosCDP"></param>
        public int EliminarPlanComprasRubrosCDP(PlanComprasRubrosCDP pPlanComprasRubrosCDP)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.EliminarPlanComprasRubrosCDP(pPlanComprasRubrosCDP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pIdProductoPlanCompraContrato"></param>
        public PlanComprasRubrosCDP ConsultarPlanComprasRubrosCDP(int? pIdProductoPlanCompraContrato)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.ConsultarPlanComprasRubrosCDP(pIdProductoPlanCompraContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanComprasRubrosCDP
        /// </summary>
        /// <param name="pNumeroConsecutivoPlanCompras"></param>
        /// <param name="pCodigoRubro"></param>
        /// <param name="pDescripcionRubro"></param>
        /// <param name="pValorRubro"></param>
        public List<PlanComprasRubrosCDP> ConsultarPlanComprasRubrosCDPs(int? pNumeroConsecutivoPlanCompras, String pCodigoRubro, String pDescripcionRubro, Decimal? pValorRubro)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.ConsultarPlanComprasRubrosCDPs(pNumeroConsecutivoPlanCompras, pCodigoRubro, pDescripcionRubro, pValorRubro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene los rubros asociados a un plan de compras para ser utilizado en la pagina de registro
        /// de contratos
        /// </summary>
        /// <param name="pIdPlanDeComprasContratos">Entero con el identificador del plan de compras</param>
        /// <returns>Listado de rubros asociados al plan de compras obtenidos de la base de datos</returns>
        public List<PlanComprasRubrosCDP> ObtenerRubrosPlanCompras(int pIdPlanDeComprasContratos)
        {
            try
            {
                return vPlanComprasRubrosCDPDAL.ObtenerRubrosPlanCompras(pIdPlanDeComprasContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    
    }
}
