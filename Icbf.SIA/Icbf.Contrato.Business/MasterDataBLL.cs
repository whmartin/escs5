﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.Entity;
using ICBF.MasterData.ClientObjectModel40;
using ICBF.MasterData.Entity;
using Icbf.SIA.Business;
using Icbf.SIA.Entity;

namespace Icbf.Contrato.Business
{
    public class MasterDataBLL
    {
        private RegionalBLL _vregionalBLL;

        public MasterDataBLL()
        {
            _vregionalBLL = new RegionalBLL();
        }

        public string NitIcbf 
        { 
            get
            {
                string result = string.Empty;

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Icbf.Nit"]))
                    result = System.Configuration.ConfigurationManager.AppSettings["Icbf.Nit"];

                return result;
            }
        }

        public string NitRazonSocial
        {
            get
            {
                string result = string.Empty;

                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Icbf.RazonSocial"]))
                    result = System.Configuration.ConfigurationManager.AppSettings["Icbf.RazonSocial"];

                return result;
            }
        }

        public ConsultaCDPMaestro ObtenerCDP(string codigoCDP, int regional)
        {
            ConsultaCDPMaestro result = new ConsultaCDPMaestro();

            try
            {
                long codigoCDPOut;

                if (long.TryParse(codigoCDP, out codigoCDPOut))
                {
                    Regional itemRegional = _vregionalBLL.ConsultarRegional(regional);

                    var itemRegionalPCI = _vregionalBLL.ConsultarRegionalPCIs(itemRegional.CodigoRegional, null);

                    MasterDataCOM40 clienteServicio = new MasterDataCOM40();
                    result = clienteServicio.ConsultarCDPListaMinisterioApi(codigoCDPOut, itemRegionalPCI[0].CodigoRegional, NitRazonSocial, NitIcbf);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public List<RPContrato> ObtenerRP(int vIdRP, int vRegionalICBF,int tipoCompromiso,int tipoVigencia)
        {
            List<RPContrato> result = new List<RPContrato>();

            try
            {
                    Regional itemRegional = _vregionalBLL.ConsultarRegional(vRegionalICBF);

                    var itemRegionalPCI = _vregionalBLL.ConsultarRegionalPCIs(itemRegional.CodigoRegional, null);

                    MasterDataCOM40 clienteServicio = new MasterDataCOM40();

                    var serviceResult = clienteServicio.ConsultarCompromisoMinisterioApi(vIdRP, itemRegionalPCI[0].CodigoRegional, tipoCompromiso, tipoVigencia);

                result = MasterDataBLL.AdaptarRP(serviceResult, vRegionalICBF);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private static List<RPContrato> AdaptarRP(ConsultaCompromisos item, int idRegional)
        {
            List<RPContrato> resultList = new List<RPContrato>();

            RPContrato itemResult = new RPContrato();

            if(item != null)
            {
                itemResult.NombreRegional = item.NomPCI;
                itemResult.IdRP = 0;
                itemResult.IdRegional = idRegional;
                itemResult.ValorInicialRP = item.valorActualPesos.Value;
                itemResult.ValorActualRP = item.valorActualPesos.Value;
                itemResult.NumeroRP = item.CodCompromiso.ToString();
                itemResult.FechaRP = item.FechaCompromiso;
                itemResult.EsEnLinea = true;
                resultList.Add(itemResult);
            }

            return resultList;
        }
    }
}
