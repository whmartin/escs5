using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Cl�usula Contraro
    /// </summary>
    public class ClausulaContratoBLL
    {
        private ClausulaContratoDAL vClausulaContratoDAL;
        public ClausulaContratoBLL()
        {
            vClausulaContratoDAL = new ClausulaContratoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int InsertarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                return vClausulaContratoDAL.InsertarClausulaContrato(pClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int ModificarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                return vClausulaContratoDAL.ModificarClausulaContrato(pClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pClausulaContrato"></param>
        /// <returns></returns>
        public int EliminarClausulaContrato(ClausulaContrato pClausulaContrato)
        {
            try
            {
                return vClausulaContratoDAL.EliminarClausulaContrato(pClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pIdClausulaContrato"></param>
        /// <returns></returns>
        public ClausulaContrato ConsultarClausulaContrato(int pIdClausulaContrato)
        {
            try
            {
                return vClausulaContratoDAL.ConsultarClausulaContrato(pIdClausulaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ClausulaContrato
        /// </summary>
        /// <param name="pNombreClausulaContrato"></param>
        /// <param name="pContenido"></param>
        /// <param name="pIdTipoClausula"></param>
        /// <param name="pIdTipoContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ClausulaContrato> ConsultarClausulaContratos(String pNombreClausulaContrato,String pContenido ,int? pIdTipoClausula, int? pIdTipoContrato, Boolean? pEstado)
        {
            try
            {
                return vClausulaContratoDAL.ConsultarClausulaContratos(pNombreClausulaContrato, pContenido, pIdTipoClausula, pIdTipoContrato, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
