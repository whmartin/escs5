﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  ConsecutivoContratoRegionales
    /// </summary>
    public class ConsecutivoContratoRegionalesBLL
    {
        private ConsecutivoContratoRegionalesDAL vConsecutivoContratoRegionalesDAL;
        public ConsecutivoContratoRegionalesBLL()
        {
            vConsecutivoContratoRegionalesDAL = new ConsecutivoContratoRegionalesDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int InsertarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.InsertarConsecutivoContratoRegionales(pConsecutivoContratoRegionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int ModificarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.ModificarConsecutivoContratoRegionales(pConsecutivoContratoRegionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pConsecutivoContratoRegionales"></param>
        public int EliminarConsecutivoContratoRegionales(ConsecutivoContratoRegionales pConsecutivoContratoRegionales)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.EliminarConsecutivoContratoRegionales(pConsecutivoContratoRegionales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public ConsecutivoContratoRegionales ConsultarConsecutivoContratoRegionales(int pIDConsecutivoContratoRegional)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.ConsultarConsecutivoContratoRegionales(pIDConsecutivoContratoRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIdRegional"></param>
        /// <param name="pIdVigencia"></param>
        /// <param name="pConsecutivo"></param>
        public List<ConsecutivoContratoRegionales> ConsultarConsecutivoContratoRegionaless(int? pIdRegional, int? pIdVigencia, Decimal? pConsecutivo)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.ConsultarConsecutivoContratoRegionaless(pIdRegional, pIdVigencia, pConsecutivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public bool EsMayorConsecutivoContratoRegionales(Decimal pConsecutivo, int pIdVigencia, int pIdRegional)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.EsMayorConsecutivoContratoRegionales(pConsecutivo, pIdVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad ConsecutivoContratoRegionales
        /// </summary>
        /// <param name="pIDConsecutivoContratoRegional"></param>
        public bool ExisteConsecutivoContratoRegionales(Decimal pConsecutivo, int pIdVigencia, int pIdRegional)
        {
            try
            {
                return vConsecutivoContratoRegionalesDAL.ExisteConsecutivoContratoRegionales(pConsecutivo, pIdVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

