﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  SolicitudModPlanCompras
    /// </summary>
    public class SolicitudModPlanComprasBLL
    {
        private SolicitudModPlanComprasDAL vSolicitudModPlanComprasDAL;
        public SolicitudModPlanComprasBLL()
        {
            vSolicitudModPlanComprasDAL = new SolicitudModPlanComprasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int InsertarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                return vSolicitudModPlanComprasDAL.InsertarSolicitudModPlanCompras(pSolicitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int ModificarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                return vSolicitudModPlanComprasDAL.ModificarSolicitudModPlanCompras(pSolicitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pSolicitudModPlanCompras"></param>
        public int EliminarSolicitudModPlanCompras(SolicitudModPlanCompras pSolicitudModPlanCompras)
        {
            try
            {
                return vSolicitudModPlanComprasDAL.EliminarSolicitudModPlanCompras(pSolicitudModPlanCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pIdSolicitudModPlanCompra"></param>
        public SolicitudModPlanCompras ConsultarSolicitudModPlanCompras(int pIdSolicitudModPlanCompra)
        {
            try
            {
                return vSolicitudModPlanComprasDAL.ConsultarSolicitudModPlanCompras(pIdSolicitudModPlanCompra);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SolicitudModPlanCompras
        /// </summary>
        /// <param name="pJustificacion"></param>
        /// <param name="pNumeroSolicitud"></param>
        /// <param name="pFechaSolicitud"></param>
        /// <param name="pIdEstadoSolicitud"></param>
        /// <param name="pIdUsuarioSolicitud"></param>
        /// <param name="pIdPlanDeCompras"></param>
        /// <param name="pIdVigencia"></param>
        public List<SolicitudModPlanCompras> ConsultarSolicitudModPlanComprass(int? pNumeroConsecutivoPlanCompras, int? pIdContrato, int? pNumeroSolicitud, string pVigencia)
        {
            try
            {
                return vSolicitudModPlanComprasDAL.ConsultarSolicitudModPlanComprass(pNumeroConsecutivoPlanCompras, pIdContrato, pNumeroSolicitud, pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool ExisteEnviadoSolicitudModPlanCompras(string pCodEstado, int NumeroConsecutivoPlanCompras, string pVigenciapc)
        {
            try
            {
                return vSolicitudModPlanComprasDAL.ExisteEnviadoSolicitudModPlanCompras(pCodEstado, NumeroConsecutivoPlanCompras, pVigenciapc);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

