﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  SupervisorInterventor
    /// </summary>
    public class SupervisorInterventorBLL
    {
        private SupervisorInterventorDAL vSupervisorInterventorDAL;
        public SupervisorInterventorBLL()
        {
            vSupervisorInterventorDAL = new SupervisorInterventorDAL();
        }
        
 /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public TipoSupervisorInterventor ConsultarSupervisorInterventorsID(int pIDTipoSuperInter)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarSupervisorInterventorsID(pIDTipoSuperInter);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad SupervisorInterventor
        /// </summary>
        /// <param name="pCodigo"></param>
        /// <param name="pDescripcion"></param>
        public List<TipoSupervisorInterventor> ConsultarSupervisorInterventors(String pCodigo, String pDescripcion)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarSupervisorInterventors(pCodigo, pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Interventor> ConsultarInterventors(int? pIdTipoPersona, int? pIdTipoIdentificacion, String pNumeroIdentificacion, String pNombreRazonSocial)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarInterventors(pIdTipoPersona, pIdTipoIdentificacion, pNumeroIdentificacion, pNombreRazonSocial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Interventor ConsultarInterventor(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarInterventor(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Supervisor ConsultarSupervisor(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarSupervisor(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Supervisor ConsultarSupervisorTemporal(int pIDSupervisorIntervContrato)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarSupervisorTemporal(pIDSupervisorIntervContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Supervisor> ConsultarSupervisors(String pIdTipoidentificacion, String pNumeroIdentificacion, String pIdTipoVinculacion, String pIdRegional,
                                                    String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido)
        {
            try
            {
                return vSupervisorInterventorDAL.ConsultarSupervisors(pIdTipoidentificacion, pNumeroIdentificacion, pIdTipoVinculacion, pIdRegional, pPrimerNombre, 
                                                                        pSegundoNombre, pPrimerApellido, pSegundoApellido);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarRolSuerpvisor(int idSupervisorInter, int idRol)
        {
            try
            {
                return vSupervisorInterventorDAL.ActualizarRolSuerpvisor(idSupervisorInter, idRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

