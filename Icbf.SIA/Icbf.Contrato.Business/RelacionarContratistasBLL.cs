using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Relacionar Contratistas
    /// </summary>
    public class RelacionarContratistasBLL
    {
        private RelacionarContratistasDAL vRelacionarContratistasDAL;
        public RelacionarContratistasBLL()
        {
            vRelacionarContratistasDAL = new RelacionarContratistasDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int InsertarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                return vRelacionarContratistasDAL.InsertarRelacionarContratistas(pRelacionarContratistas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int ModificarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                return vRelacionarContratistasDAL.ModificarRelacionarContratistas(pRelacionarContratistas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pRelacionarContratistas"></param>
        /// <returns></returns>
        public int EliminarRelacionarContratistas(RelacionarContratistas pRelacionarContratistas)
        {
            try
            {
                return vRelacionarContratistasDAL.EliminarRelacionarContratistas(pRelacionarContratistas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pIdContratistaContrato"></param>
        /// <returns></returns>
        public RelacionarContratistas ConsultarRelacionarContratistas(int pIdContratistaContrato)
        {
            try
            {
                return vRelacionarContratistasDAL.ConsultarRelacionarContratistas(pIdContratistaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RelacionarContratistas
        /// </summary>
        /// <param name="pIdContratistaContrato"></param>
        /// <param name="pIdContrato"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pClaseEntidad"></param>
        /// <param name="pPorcentajeParticipacion"></param>
        /// <param name="pEstadoIntegrante"></param>
        /// <param name="pNumeroIdentificacionRepresentanteLegal"></param>
        /// <returns></returns>
        public List<RelacionarContratistas> ConsultarRelacionarContratistass(int? pIdContratistaContrato, int? pIdContrato,  long pNumeroIdentificacion, String pClaseEntidad, int? pPorcentajeParticipacion, Boolean pEstadoIntegrante, long pNumeroIdentificacionRepresentanteLegal)
        {
            try
            {
                return vRelacionarContratistasDAL.ConsultarRelacionarContratistass(pIdContratistaContrato, pIdContrato, pNumeroIdentificacion, pClaseEntidad, pPorcentajeParticipacion, pEstadoIntegrante, pNumeroIdentificacionRepresentanteLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
