﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Proveedores_Contratos
    /// </summary>
    public class Proveedores_ContratosBLL
    {
        private Proveedores_ContratosDAL vProveedores_ContratosDAL;
        public Proveedores_ContratosBLL()
        {
            vProveedores_ContratosDAL = new Proveedores_ContratosDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int InsertarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosDAL.InsertarProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int ModificarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosDAL.ModificarProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        public int EliminarProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosDAL.EliminarProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedoresContratos"></param>
        public Proveedores_Contratos ConsultarProveedores_Contratos(int pIdProveedoresContratos)
        {
            try
            {
                return vProveedores_ContratosDAL.ConsultarProveedores_Contratos(pIdProveedoresContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratos
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<Proveedores_Contratos> ConsultarProveedores_Contratoss(int? pIdProveedores, int? pIdContrato)
        {
            try
            {
                return vProveedores_ContratosDAL.ConsultarProveedores_Contratoss(pIdProveedores, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta de la lista de proveedores asociados a un contrato
        /// </summary>
        /// <param name="pIdProveedores"></param>
        /// <param name="pIdContrato"></param>
        public List<Proveedores_Contratos> ConsultarContratistasContratos(int? pcontrato)
        {
            try
            {
                return vProveedores_ContratosDAL.ConsultarContratistasContratos(pcontrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica existencia de contrato ya asociado a un proveedor
        /// </summary>
        /// <param name="pProveedores_Contratos"></param>
        /// <returns></returns>
        public KeyValuePair<int,string> ExisteProveedores_Contratos(Proveedores_Contratos pProveedores_Contratos)
        {
            try
            {
                return vProveedores_ContratosDAL.ExisteProveedores_Contratos(pProveedores_Contratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapiña
        /// Obtiene la informacion de contratistas relacionados con un contrato
        /// </summary>
        /// <param name="pContrato">Entero con el identificador del contrato</param>
        /// <param name="pIntegrantesUnionTemporal">Booleano que indica si se obtiene la información de contratistas o la de los integrantes 
        /// del consorcio o union temporal asociados al contrato</param>
        /// <returns>Listado de contratistas obtenidos de la base de datos</returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContrato(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosDAL.ObtenerProveedoresContrato(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContrato"></param>
        /// <param name="pIntegrantesUnionTemporal"></param>
        /// <returns></returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContratoActuales(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosDAL.ObtenerProveedoresContratoActuales(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Proveedores_Contratos> ObtenerProveedoresContratoMigrados(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosDAL.ObtenerProveedoresContratoMigrados(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContrato"></param>
        /// <param name="pIntegrantesUnionTemporal"></param>
        /// <returns></returns>
        public List<Proveedores_Contratos> ObtenerProveedoresContratoHistorico(int pContrato, int? pIntegrantesUnionTemporal)
        {
            try
            {
                return vProveedores_ContratosDAL.ObtenerProveedoresContratoHistorico(pContrato, pIntegrantesUnionTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
