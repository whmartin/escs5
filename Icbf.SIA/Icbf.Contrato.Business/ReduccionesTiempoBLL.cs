﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ReduccionesTiempoBLL
    {
        private ReduccionesTiempoDAL vReduccionesTiempoDAL;

        public ReduccionesTiempoBLL()
        {
            vReduccionesTiempoDAL = new ReduccionesTiempoDAL();
        }

        public int InsertarReduccionesTiempo(ReduccionesTiempo vReduccionesTiempo)
        {
            try
            {
                return vReduccionesTiempoDAL.InsertarReduccionesTiempo(vReduccionesTiempo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarReduccionesTiempo(ReduccionesTiempo vReduccionesTiempo)
        {
            try
            {
                return vReduccionesTiempoDAL.ModificarReduccionesTiempo(vReduccionesTiempo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarReduccionesTiempo(ReduccionesTiempo vReduccionesTiempo)
        {
            try
            {
                return vReduccionesTiempoDAL.EliminarReduccionesTiempo(vReduccionesTiempo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReduccionesTiempo ConsultarReduccionTiempo(int vIdReduccionTiempo)
        {
            try
            {
                return vReduccionesTiempoDAL.ConsultarReduccionTiempo(vIdReduccionTiempo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempo(DateTime? pFechaReduccion,  int? pIdDetalleConsModContractual)
        {
            try
            {
                return vReduccionesTiempoDAL.ConsultarReduccionesTiempo(pFechaReduccion, pIdDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempoContrato(int idContrato)
        {
            try
            {
                return vReduccionesTiempoDAL.ConsultarReduccionesTiempoContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<ReduccionesTiempo> ConsultarReduccionesTiempoAprobadasContrato(int idContrato)
        {
            try
            {
                return vReduccionesTiempoDAL.ConsultarReduccionesTiempoAprobadasContrato(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

    }
}
