using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo Contrato
    /// </summary>
    public class TipoContratoBLL
    {
        private TipoContratoDAL vTipoContratoDAL;
        public TipoContratoBLL()
        {
            vTipoContratoDAL = new TipoContratoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int InsertarTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoDAL.InsertarTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int ModificarTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoDAL.ModificarTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoContrato
        /// </summary>
        /// <param name="pTipoContrato"></param>
        /// <returns></returns>
        public int EliminarTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoDAL.EliminarTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoContrato
        /// </summary>
        /// <param name="pIdTipoContrato"></param>
        /// <returns></returns>
        public TipoContrato ConsultarTipoContrato(int pIdTipoContrato)
        {
            try
            {
                return vTipoContratoDAL.ConsultarTipoContrato(pIdTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoContrato
        /// </summary>
        /// <param name="pNombreTipoContrato"></param>
        /// <param name="pIdCategoriaContrato"></param>
        /// <param name="pEstado"></param>
        /// <param name="pActaInicio"></param>
        /// <param name="pAporteCofinanciacion"></param>
        /// <param name="pRecursosFinancieros"></param>
        /// <param name="pRegimenContrato"></param>
        /// <param name="pDescripcionTipoContrato"></param>
        /// <returns></returns>
        public List<TipoContrato> ConsultarTipoContratos(String pNombreTipoContrato, int? pIdCategoriaContrato, Boolean? pEstado, Boolean? pActaInicio, Boolean? pAporteCofinanciacion, Boolean? pRecursosFinancieros, int? pRegimenContrato, String pDescripcionTipoContrato)
        {
            try
            {
                return vTipoContratoDAL.ConsultarTipoContratos(pNombreTipoContrato, pIdCategoriaContrato, pEstado, pActaInicio, pAporteCofinanciacion, pRecursosFinancieros, pRegimenContrato, pDescripcionTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) del tipo de contrato la 
        /// informaci�n del mismo
        /// </summary>
        /// <param name="pTipoContrato">Entidad con la informaci�n del filtro</param>
        /// <returns>Entidad con la informaci�n de tipo de contrato recuperado</returns>
        public TipoContrato IdentificadorCodigoTipoContrato(TipoContrato pTipoContrato)
        {
            try
            {
                return vTipoContratoDAL.IdentificadorCodigoTipoContrato(pTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
