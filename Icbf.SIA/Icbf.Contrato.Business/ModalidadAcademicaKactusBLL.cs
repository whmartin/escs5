﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Icbf.Contrato.DataAccess;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Modalidad Academica Kactus
    /// </summary>
    public class ModalidadAcademicaKactusBLL
    {
        private ModalidadAcademicaKactusDAL vModalidadAcademicaKactusDAL;
        public ModalidadAcademicaKactusBLL()
        {
            vModalidadAcademicaKactusDAL = new ModalidadAcademicaKactusDAL();
        }
        /// <summary>
        /// Gonet/Emilio Calapina
        /// Metodo de consulta por filtros para la entidad ModalidadAcademicaKactus
        /// </summary>
        /// <param name="pNombre">Cadena de texto con el nombre</param>
        /// <param name="pId">Cadena de texto con el identificador</param>
        /// <param name="pEstado">Booleano con el estado</param>
        /// <returns>Lista con las modalidades que coinciden con los filtros dados</returns>
        public List<ModalidadAcademicaKactus> ConsultarModalidadesAcademicasKactus(String pNombre, String pId, Boolean? pEstado)
        {
            try
            {
                return vModalidadAcademicaKactusDAL.ConsultarModalidadesAcademicasKactus(pNombre, pId, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
