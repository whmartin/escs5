using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contratos.Entity
{
    /// <summary>
    /// Guarda la información relacionada con el número de proceso
    /// </summary>
    public class NumeroProcesos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdNumeroProceso
        {
            get;
            set;
        }
        public int NumeroProceso
        {
            get;
            set;
        }
        public Boolean Inactivo
        {
            get;
            set;
        }
        public String NumeroProcesoGenerado
        {
            get;
            set;
        }
        public int IdModalidadSeleccion
        {
            get;
            set;
        }
        public int IdRegional
        {
            get;
            set;
        }
        public int IdVigencia
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public NumeroProcesos()
        {
        }
    }
}
