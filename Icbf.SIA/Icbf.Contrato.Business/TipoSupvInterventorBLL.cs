using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Tipo supervisor interventor
    /// </summary>
    public class TipoSupvInterventorBLL
    {
        private TipoSupvInterventorDAL vTipoSupvInterventorDAL;
        public TipoSupvInterventorBLL()
        {
            vTipoSupvInterventorDAL = new TipoSupvInterventorDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int InsertarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorDAL.InsertarTipoSupvInterventor(pTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int ModificarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorDAL.ModificarTipoSupvInterventor(pTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pTipoSupvInterventor"></param>
        /// <returns></returns>
        public int EliminarTipoSupvInterventor(TipoSupvInterventor pTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorDAL.EliminarTipoSupvInterventor(pTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pIdTipoSupvInterventor"></param>
        /// <returns></returns>
        public TipoSupvInterventor ConsultarTipoSupvInterventor(int pIdTipoSupvInterventor)
        {
            try
            {
                return vTipoSupvInterventorDAL.ConsultarTipoSupvInterventor(pIdTipoSupvInterventor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoSupvInterventor
        /// </summary>
        /// <param name="pNombre"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoSupvInterventor> ConsultarTipoSupvInterventors(String pNombre, Boolean? pEstado)
        {
            try
            {
                return vTipoSupvInterventorDAL.ConsultarTipoSupvInterventors(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
