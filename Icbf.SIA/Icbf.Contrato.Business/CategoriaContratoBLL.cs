using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Categor�a Contrato
    /// </summary>
    public class CategoriaContratoBLL
    {
        private CategoriaContratoDAL vCategoriaContratoDAL; 
        public CategoriaContratoBLL()
        {
            vCategoriaContratoDAL = new CategoriaContratoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int InsertarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoDAL.InsertarCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int ModificarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoDAL.ModificarCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pCategoriaContrato"></param>
        /// <returns></returns>
        public int EliminarCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoDAL.EliminarCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pIdCategoriaContrato"></param>
        /// <returns></returns>
        public CategoriaContrato ConsultarCategoriaContrato(int pIdCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoDAL.ConsultarCategoriaContrato(pIdCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad CategoriaContrato
        /// </summary>
        /// <param name="pNombreCategoriaContrato"></param>
        /// <param name="pDescripcionCategoriaContrato"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<CategoriaContrato> ConsultarCategoriaContratos(String pNombreCategoriaContrato, String pDescripcionCategoriaContrato, Boolean? pEstado)
        {
            try
            {
                return vCategoriaContratoDAL.ConsultarCategoriaContratos(pNombreCategoriaContrato, pDescripcionCategoriaContrato, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Gonet/Emilio Calapi�a
        /// Permite obtener a partir del identificador de tabla o el c�digo(Uno de los dos) de la categoria de contrato la 
        /// informaci�n del mismo
        /// </summary>
        /// <param name="pCategoriaContrato">Entidad con la informaci�n del filtro</param>
        /// <returns>Entidad con la informaci�n de tipo de contrato recuperado</returns>
        public CategoriaContrato IdentificadorCodigoCategoriaContrato(CategoriaContrato pCategoriaContrato)
        {
            try
            {
                return vCategoriaContratoDAL.IdentificadorCodigoCategoriaContrato(pCategoriaContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
