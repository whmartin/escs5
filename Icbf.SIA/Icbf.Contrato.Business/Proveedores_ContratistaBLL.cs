﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Proveedores_Contratista
    /// </summary>
    public class Proveedores_ContratistaBLL
    {
        private Proveedores_ContratistaDAL vProveedores_ContratistaDAL;
        public Proveedores_ContratistaBLL()
        {
            vProveedores_ContratistaDAL = new Proveedores_ContratistaDAL();
        }
        ///// <summary>
        ///// Método de inserción para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pProveedores_Contratista"></param>
        //public int InsertarProveedores_Contratista(Proveedores_Contratista pProveedores_Contratista)
        //{
        //    try
        //    {
        //        return vProveedores_ContratistaDAL.InsertarProveedores_Contratista(pProveedores_Contratista);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de modificación para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pProveedores_Contratista"></param>
        //public int ModificarProveedores_Contratista(Proveedores_Contratista pProveedores_Contratista)
        //{
        //    try
        //    {
        //        return vProveedores_ContratistaDAL.ModificarProveedores_Contratista(pProveedores_Contratista);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        ///// Método de eliminación para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pProveedores_Contratista"></param>
        //public int EliminarProveedores_Contratista(Proveedores_Contratista pProveedores_Contratista)
        //{
        //    try
        //    {
        //        return vProveedores_ContratistaDAL.EliminarProveedores_Contratista(pProveedores_Contratista);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        ///// <summary>
        ///// Método de consulta por id para la entidad Proveedores_Contratista
        ///// </summary>
        ///// <param name="pIdProveedoresContratista"></param>
        //public Proveedores_Contratista ConsultarProveedores_Contratista(int pIdProveedoresContratista)
        //{
        //    try
        //    {
        //        return vProveedores_ContratistaDAL.ConsultarProveedores_Contratista(pIdProveedoresContratista);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// Método de consulta por filtros para la entidad Proveedores_Contratista
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pIdTercero"></param>
        /// <param name="pNombreTipoPersona"></param>
        /// <param name="pCodDocumento"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pRazonsocial"></param>
        /// <param name="pNumeroidentificacionRepLegal"></param>
        /// <param name="pRazonsocialRepLegal"></param>
        public List<Proveedores_Contratista> ConsultarProveedores_Contratistas(int? pIdEntidad, int? pIdTercero, String pNombreTipoPersona, String pCodDocumento, String pNumeroIdentificacion, String pRazonsocial, String pNumeroidentificacionRepLegal, String pRazonsocialRepLegal)
        {
            try
            {
                return vProveedores_ContratistaDAL.ConsultarProveedores_Contratistas(pIdEntidad, pIdTercero, pNombreTipoPersona, pCodDocumento, pNumeroIdentificacion, pRazonsocial, pNumeroidentificacionRepLegal, pRazonsocialRepLegal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
