﻿using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class GarantiaModificacionBLL
    {
        private GarantiaModificacionDAL GarantiaModificacionDAL;

        public GarantiaModificacionBLL()
        {
            GarantiaModificacionDAL = new GarantiaModificacionDAL();
        }

        public List<GarantiaModificacionContrato> ConsultarContratos(string numeroContrato,
                                                  int? vigenciaFiscal,
                                                  int? regional,
                                                  int? idCategoriaContrato,
                                                  int? idTipoContrato)
        {
            try
            {
                return GarantiaModificacionDAL.ConsultarContratos(numeroContrato, vigenciaFiscal, regional, idCategoriaContrato, idTipoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public void GenerarModificacionGarantia(int idGarantia, string usuariocreacion, DateTime fechaAprobacion)
        {
            try
            {
                GarantiaModificacionDAL.GenerarModificacionGarantia(idGarantia, usuariocreacion, fechaAprobacion); 
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool EsGarantiaEditada(int idTipoModificacion1, int idContrato)
        {
            try
            {
                return GarantiaModificacionDAL.EsGarantiaEditada(idTipoModificacion1, idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public bool PuedeCrearGarantiasNuevasModificacion(int idTipoModificacion)
        {
            try
            {
                return GarantiaModificacionDAL.PuedeCrearGarantiasNuevasModificacion(idTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TipoModificacion> ObtenerTiposModificacionModGarantia(int idContrato)
        {
            try
            {
                return GarantiaModificacionDAL.ObtenerTiposModificacionModGarantia(idContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Garantias Historicos

        public List<GarantiaHistorico> ConsultarInfoGarantiasHistorico(int? pIdGarantia)
        {
            try
            {
                return GarantiaModificacionDAL.ConsultarInfoGarantiasHistorico(pIdGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GarantiaHistorico> ConsultarInfoGarantiasHistoricoPorContrato(int vIdContrato)
        {
            try
            {
                return GarantiaModificacionDAL.ConsultarInfoGarantiasHistoricoPorContrato(vIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Garantias Temporales

        public GarantiaHistorico ConsultarGarantiaTemporal(int pIDGarantiaTemporal)
        {
            try
            {
                return GarantiaModificacionDAL.ConsultarGarantiaTemporal(pIDGarantiaTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int GenerarGarantiaTemporal(int idGarantia, int idTipoModificacion)
        {
            try
            {
                return GarantiaModificacionDAL.GenerarGarantiaTemporal(idGarantia, idTipoModificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public int ModificarGarantiaTemporal(GarantiaHistorico pGarantiaTemporal)
        {
            try
            {
                return GarantiaModificacionDAL.ModificarGarantiaTemporal(pGarantiaTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public string TipoModificacionGarantiaTemporal(int idGarantia)
        {
            try
            {
                return GarantiaModificacionDAL.TipoModificacionGarantiaTemporal(idGarantia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Amparos Garantias Temporales

        public List<AmparosGarantias> ObtenerAmparosGarantiaTemporal(int pidGarantiaTemporal)
        {
            try
            {
                return GarantiaModificacionDAL.ObtenerAmparosGarantiaTemporal(pidGarantiaTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion



    }
}
