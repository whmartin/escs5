﻿using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Contrato.Business
{
    public class SolicitudCDPBLL
    {
        SolicitudCDPDAL DAL;

        public SolicitudCDPBLL()
        {
            DAL = new SolicitudCDPDAL();
        }

        public int InsertarLoteSolicitudesCDP(SolicitudCDP item , string nombreArchivo)
        {
            try
            {
                return DAL.InsertarLoteSolicitudesCDP(item,nombreArchivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TrazabilidadSolicitudCDP> ObtenerTrazabilidad()
        {
            try
            {
                return DAL.ObtenerTrazabilidad();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
