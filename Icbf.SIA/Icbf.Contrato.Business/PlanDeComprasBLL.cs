﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  PlanDeCompras
    /// </summary>
    public class PlanDeComprasBLL
    {
        private PlanDeComprasDAL vPlanDeComprasDAL;
        public PlanDeComprasBLL()
        {
            vPlanDeComprasDAL = new PlanDeComprasDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int InsertarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasDAL.InsertarPlanDeCompras(pPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int ModificarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasDAL.ModificarPlanDeCompras(pPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pPlanDeCompras"></param>
        public int EliminarPlanDeCompras(PlanDeCompras pPlanDeCompras)
        {
            try
            {
                return vPlanDeComprasDAL.EliminarPlanDeCompras(pPlanDeCompras);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pIdPlanDeCompras"></param>
        public PlanDeCompras ConsultarPlanDeCompras(int pIdPlanDeCompras, string pVigencia)
        {
            try
            {
                return vPlanDeComprasDAL.ConsultarPlanDeCompras(pIdPlanDeCompras,pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad PlanDeCompras
        /// </summary>
        /// <param name="pNumeroConsecutivo"></param>
        public List<PlanDeCompras> ConsultarPlanDeComprass(int? pNumeroConsecutivo)
        {
            try
            {
                return vPlanDeComprasDAL.ConsultarPlanDeComprass(pNumeroConsecutivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
