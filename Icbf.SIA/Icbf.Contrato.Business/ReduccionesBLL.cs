using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class ReduccionesBLL
    {
        private ReduccionesDAL vReduccionesDAL;
        public ReduccionesBLL()
        {
            vReduccionesDAL = new ReduccionesDAL();
        }
        public int InsertarReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesDAL.InsertarReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        public int ModificarReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesDAL.ModificarReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public int ModificarValorReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesDAL.ModificarValorReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarReducciones(Reducciones pReducciones)
        {
            try
            {
                return vReduccionesDAL.EliminarReducciones(pReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public Reducciones ConsultarReducciones(int pIdReducciones)
        {
            try
            {
                return vReduccionesDAL.ConsultarReduccion(pIdReducciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reducciones> ConsultarReduccioness(int? pValorReduccion, DateTime? pFechaReduccion, int? pIDDetalleConsModContractual)
        {
            try
            {
                return vReduccionesDAL.ConsultarReduccioness(pValorReduccion, pFechaReduccion, pIDDetalleConsModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReduccionContrato ConsultarContratoReduccion(int pIdContrato)
        {
            try
            {
                return vReduccionesDAL.ConsultarContratoReduccion(pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Reducciones> ConsultarReduccionesAprobadasContrato(int IdContrato)
        {
            try
            {
               return vReduccionesDAL.ConsultarReduccionesAprobadasContrato(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public decimal ConsultarValorTotalReducido(int IdContrato)
        {
            decimal result = 0;

            try
            {
                return vReduccionesDAL.ConsultarValorTotalReducido(IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

            return result;
        }
    }
}
