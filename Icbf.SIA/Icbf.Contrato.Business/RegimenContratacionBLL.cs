using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad R�gimen Contrataci�n
    /// </summary>
    public class RegimenContratacionBLL
    {
        private RegimenContratacionDAL vRegimenContratacionDAL;
        public RegimenContratacionBLL()
        {
            vRegimenContratacionDAL = new RegimenContratacionDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int InsertarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionDAL.InsertarRegimenContratacion(pRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int ModificarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionDAL.ModificarRegimenContratacion(pRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pRegimenContratacion"></param>
        /// <returns></returns>
        public int EliminarRegimenContratacion(RegimenContratacion pRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionDAL.EliminarRegimenContratacion(pRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pIdRegimenContratacion"></param>
        /// <returns></returns>
        public RegimenContratacion ConsultarRegimenContratacion(int pIdRegimenContratacion)
        {
            try
            {
                return vRegimenContratacionDAL.ConsultarRegimenContratacion(pIdRegimenContratacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad RegimenContratacion
        /// </summary>
        /// <param name="pNombreRegimenContratacion"></param>
        /// <param name="pDescripcion"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<RegimenContratacion> ConsultarRegimenContratacions(String pNombreRegimenContratacion, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vRegimenContratacionDAL.ConsultarRegimenContratacions(pNombreRegimenContratacion,pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
