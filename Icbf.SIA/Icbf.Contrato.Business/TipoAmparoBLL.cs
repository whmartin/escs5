using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo Amparo
    /// </summary>
    public class TipoAmparoBLL
    {
        private TipoAmparoDAL vTipoAmparoDAL;
        public TipoAmparoBLL()
        {
            vTipoAmparoDAL = new TipoAmparoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int InsertarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                return vTipoAmparoDAL.InsertarTipoAmparo(pTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int ModificarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                return vTipoAmparoDAL.ModificarTipoAmparo(pTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad TipoAmparo
        /// </summary>
        /// <param name="pTipoAmparo"></param>
        /// <returns></returns>
        public int EliminarTipoAmparo(TipoAmparo pTipoAmparo)
        {
            try
            {
                return vTipoAmparoDAL.EliminarTipoAmparo(pTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad TipoAmparo
        /// </summary>
        /// <param name="pIdTipoAmparo"></param>
        /// <returns></returns>
        public TipoAmparo ConsultarTipoAmparo(int pIdTipoAmparo)
        {
            try
            {
                return vTipoAmparoDAL.ConsultarTipoAmparo(pIdTipoAmparo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad TipoAmparo
        /// </summary>
        /// <param name="pNombreTipoAmparo"></param>
        /// <param name="pIdTipoGarantia"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<TipoAmparo> ConsultarTipoAmparos(String pNombreTipoAmparo, int? pIdTipoGarantia, Boolean? pEstado)
        {
            try
            {
                return vTipoAmparoDAL.ConsultarTipoAmparos(pNombreTipoAmparo, pIdTipoGarantia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
