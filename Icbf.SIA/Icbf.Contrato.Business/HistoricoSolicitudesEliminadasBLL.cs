﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    public class HistoricoSolicitudesEliminadasBLL
    {
        private HistoricoSolicitudesEliminadasDAL vHistoricoSolicitudesEliminadasDAL;

        public HistoricoSolicitudesEliminadasBLL()
        {
            vHistoricoSolicitudesEliminadasDAL = new HistoricoSolicitudesEliminadasDAL();
        }

        public int InsertarSolModContractualHistorico(HistoricoSolicitudesEliminadas psModContractual)
        {
            try
            {
                return vHistoricoSolicitudesEliminadasDAL.InsertarSolModContractualHistorico(psModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public HistoricoSolicitudesEliminadas ConsultarSolicitudesEliminacion(int IDCosModContractual)
        {
            try
            {
                return vHistoricoSolicitudesEliminadasDAL.ConsultarSolicitudesEliminacion(IDCosModContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<HistoricoSolicitudesEliminadas> ConsultarSolicitudesElimindasHistorico()
        {
            try
            {
                return vHistoricoSolicitudesEliminadasDAL.ConsultarSolicitudesElimindasHistorico();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}

