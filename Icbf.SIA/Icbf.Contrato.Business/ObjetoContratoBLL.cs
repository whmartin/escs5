using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Contrato.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Objeto Contrato
    /// </summary>
    public class ObjetoContratoBLL
    {
        private ObjetoContratoDAL vObjetoContratoDAL;
        public ObjetoContratoBLL()
        {
            vObjetoContratoDAL = new ObjetoContratoDAL();
        }
        /// <summary>
        /// M�todo de inserci�n para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int InsertarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                return vObjetoContratoDAL.InsertarObjetoContrato(pObjetoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de modificaci�n para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int ModificarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                return vObjetoContratoDAL.ModificarObjetoContrato(pObjetoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de eliminaci�n para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pObjetoContrato"></param>
        /// <returns></returns>
        public int EliminarObjetoContrato(ObjetoContrato pObjetoContrato)
        {
            try
            {
                return vObjetoContratoDAL.EliminarObjetoContrato(pObjetoContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por id para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pIdObjetoContratoContractual"></param>
        /// <returns></returns>
        public ObjetoContrato ConsultarObjetoContrato(int pIdObjetoContratoContractual)
        {
            try
            {
                return vObjetoContratoDAL.ConsultarObjetoContrato(pIdObjetoContratoContractual);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// M�todo de consulta por filtros para la entidad ObjetoContrato
        /// </summary>
        /// <param name="pIdObjetoContratoContractual"></param>
        /// <param name="pObjetoContractual"></param>
        /// <param name="pEstado"></param>
        /// <returns></returns>
        public List<ObjetoContrato> ConsultarObjetoContratos(int? pIdObjetoContratoContractual, String pObjetoContractual, Boolean? pEstado)
        {
            try
            {
                return vObjetoContratoDAL.ConsultarObjetoContratos(pIdObjetoContratoContractual, pObjetoContractual, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
