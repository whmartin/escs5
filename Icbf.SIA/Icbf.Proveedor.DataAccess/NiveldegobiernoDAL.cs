using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad nivel de gobierno
    /// </summary>
    public class NiveldegobiernoDAL : GeneralDAL
    {
        public NiveldegobiernoDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Niveldegobierno_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdNiveldegobierno", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNiveldegobierno", DbType.String, pNiveldegobierno.CodigoNiveldegobierno);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pNiveldegobierno.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pNiveldegobierno.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pNiveldegobierno.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pNiveldegobierno.IdNiveldegobierno = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdNiveldegobierno").ToString());
                    GenerarLogAuditoria(pNiveldegobierno, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Niveldegobierno_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNiveldegobierno", DbType.Int32, pNiveldegobierno.IdNiveldegobierno);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNiveldegobierno", DbType.String, pNiveldegobierno.CodigoNiveldegobierno);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pNiveldegobierno.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pNiveldegobierno.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pNiveldegobierno.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNiveldegobierno, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Niveldegobierno_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNiveldegobierno", DbType.Int32, pNiveldegobierno.IdNiveldegobierno);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNiveldegobierno, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pIdNiveldegobierno">Identificador de una entidad Nivel de gobierno</param>
        /// <returns>Entidad Nivel de gobierno</returns>
        public Niveldegobierno ConsultarNiveldegobierno(int pIdNiveldegobierno)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Niveldegobierno_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNiveldegobierno", DbType.Int32, pIdNiveldegobierno);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Niveldegobierno vNiveldegobierno = new Niveldegobierno();
                        while (vDataReaderResults.Read())
                        {
                            vNiveldegobierno.IdNiveldegobierno = vDataReaderResults["IdNiveldegobierno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNiveldegobierno"].ToString()) : vNiveldegobierno.IdNiveldegobierno;
                            vNiveldegobierno.CodigoNiveldegobierno = vDataReaderResults["CodigoNiveldegobierno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNiveldegobierno"].ToString()) : vNiveldegobierno.CodigoNiveldegobierno;
                            vNiveldegobierno.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNiveldegobierno.Descripcion;
                            vNiveldegobierno.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNiveldegobierno.Estado;
                            vNiveldegobierno.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNiveldegobierno.UsuarioCrea;
                            vNiveldegobierno.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNiveldegobierno.FechaCrea;
                            vNiveldegobierno.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNiveldegobierno.UsuarioModifica;
                            vNiveldegobierno.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNiveldegobierno.FechaModifica;
                        }
                        return vNiveldegobierno;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <param name="pCodigoNiveldegobierno">C�digo en una entidad Nivel de gobierno</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Nivel de gobierno</param>
        /// <param name="pEstado">Estado en una entidad Nivel de gobierno</param>
        /// <returns>Lista de entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernos(String pCodigoNiveldegobierno, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Niveldegobiernos_Consultar"))
                {
                    if(pCodigoNiveldegobierno != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoNiveldegobierno", DbType.String, pCodigoNiveldegobierno);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Niveldegobierno> vListaNiveldegobierno = new List<Niveldegobierno>();
                        while (vDataReaderResults.Read())
                        {
                                Niveldegobierno vNiveldegobierno = new Niveldegobierno();
                            vNiveldegobierno.IdNiveldegobierno = vDataReaderResults["IdNiveldegobierno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNiveldegobierno"].ToString()) : vNiveldegobierno.IdNiveldegobierno;
                            vNiveldegobierno.CodigoNiveldegobierno = vDataReaderResults["CodigoNiveldegobierno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNiveldegobierno"].ToString()) : vNiveldegobierno.CodigoNiveldegobierno;
                            vNiveldegobierno.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNiveldegobierno.Descripcion;
                            vNiveldegobierno.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNiveldegobierno.Estado;
                            vNiveldegobierno.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNiveldegobierno.UsuarioCrea;
                            vNiveldegobierno.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNiveldegobierno.FechaCrea;
                            vNiveldegobierno.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNiveldegobierno.UsuarioModifica;
                            vNiveldegobierno.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNiveldegobierno.FechaModifica;
                                vListaNiveldegobierno.Add(vNiveldegobierno);
                        }
                        return vListaNiveldegobierno;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <returns>Lista de Entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernosAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All"))
                {
                      using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Niveldegobierno> vListaNiveldegobierno = new List<Niveldegobierno>();
                        while (vDataReaderResults.Read())
                        {
                            Niveldegobierno vNiveldegobierno = new Niveldegobierno();
                            vNiveldegobierno.IdNiveldegobierno = vDataReaderResults["IdNiveldegobierno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNiveldegobierno"].ToString()) : vNiveldegobierno.IdNiveldegobierno;
                            vNiveldegobierno.CodigoNiveldegobierno = vDataReaderResults["CodigoNiveldegobierno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNiveldegobierno"].ToString()) : vNiveldegobierno.CodigoNiveldegobierno;
                            vNiveldegobierno.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNiveldegobierno.Descripcion;
                            vNiveldegobierno.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNiveldegobierno.Estado;
                            vNiveldegobierno.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNiveldegobierno.UsuarioCrea;
                            vNiveldegobierno.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNiveldegobierno.FechaCrea;
                            vNiveldegobierno.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNiveldegobierno.UsuarioModifica;
                            vNiveldegobierno.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNiveldegobierno.FechaModifica;
                            vListaNiveldegobierno.Add(vNiveldegobierno);
                        }
                        return vListaNiveldegobierno;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <param name="pIdRamaEstructura">Identificador de la rama estructura en una entidad Nivel de gobierno</param>
        /// <returns>Lista de entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernosRamaoEstructura(int pIdRamaEstructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelGobierno"))
                {
                    if (pIdRamaEstructura >0)
                        vDataBase.AddInParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, pIdRamaEstructura);
                   
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Niveldegobierno> vListaNiveldegobierno = new List<Niveldegobierno>();
                        while (vDataReaderResults.Read())
                        {
                            Niveldegobierno vNiveldegobierno = new Niveldegobierno();
                            vNiveldegobierno.IdNiveldegobierno = vDataReaderResults["IdNivelGobierno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNivelGobierno"].ToString()) : vNiveldegobierno.IdNiveldegobierno;
                            vNiveldegobierno.CodigoNiveldegobierno = vDataReaderResults["CodigoNivelGobierno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNivelGobierno"].ToString()) : vNiveldegobierno.CodigoNiveldegobierno;
                            vNiveldegobierno.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNiveldegobierno.Descripcion;
                            vListaNiveldegobierno.Add(vNiveldegobierno);
                        }
                        return vListaNiveldegobierno;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
