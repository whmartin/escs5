using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad de validaci�n de terceros
    /// </summary>
    public class ValidarTerceroDAL : GeneralDAL
    {
        public ValidarTerceroDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad validaci�n de terceros
        /// </summary>
        /// <param name="pValidarTercero">Entidad validaci�n de terceros</param>
        /// <param name="idEstadoTercero">Identificador del estado</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValidarTercero(ValidarTercero pValidarTercero, int idEstadoTercero)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarTercero_Insertar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValidarTercero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pValidarTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarTercero.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarTercero.ConfirmaYAprueba);
                    // JCHE
                    // Implementacion de nueva variable tipoIncidente 
                    vDataBase.AddInParameter(vDbCommand, "@TipoIncidente", DbType.Int32, pValidarTercero.TipoIncidente);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarTercero.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@FechaCrea", DbType.DateTime, pValidarTercero.FechaCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pValidarTercero.IdValidarTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValidarTercero").ToString());
                    GenerarLogAuditoria(pValidarTercero, vDbCommand);
                    //return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdTercero", DbType.String, pValidarTercero.IdTercero);
                        
                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoTercero", DbType.Int16, idEstadoTercero );
                                                    
                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarTercero.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);
                        
                        GenerarLogAuditoria(pValidarTercero, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }
     
        /// <summary>
        /// Consulta una entidad validaci�n de terceros
        /// </summary>
        /// <param name="pIdValidarTercero">Identificador en una entidad validaci�n de terceros</param>
        /// <returns>Entidad validaci�n de terceros</returns>
        public ValidarTercero ConsultarValidarTercero(int pIdValidarTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarTercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarTercero", DbType.Int32, pIdValidarTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarTercero vValidarTercero = new ValidarTercero();
                        while (vDataReaderResults.Read())
                        {
                            vValidarTercero.IdValidarTercero = vDataReaderResults["IdValidarTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarTercero"].ToString()) : vValidarTercero.IdValidarTercero;
                            vValidarTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vValidarTercero.IdTercero;
                            vValidarTercero.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarTercero.Observaciones;
                            vValidarTercero.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarTercero.ConfirmaYAprueba;
                            vValidarTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarTercero.UsuarioCrea;
                            vValidarTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarTercero.FechaCrea;
                        }
                        return vValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de terceros
        /// </summary>
        /// <param name="pIdTercero">Identificador de una entidad validaci�n de terceros</param>
        /// <returns>Entidad validaci�n de terceros</returns>
        public ValidarTercero ConsultarValidarTercero_Tercero(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero );
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarTercero vValidarTercero = new ValidarTercero();
                        while (vDataReaderResults.Read())
                        {
                            vValidarTercero.IdValidarTercero = vDataReaderResults["IdValidarTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarTercero"].ToString()) : vValidarTercero.IdValidarTercero;
                            vValidarTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vValidarTercero.IdTercero;
                            vValidarTercero.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarTercero.Observaciones;
                            vValidarTercero.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarTercero.ConfirmaYAprueba;
                            vValidarTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarTercero.UsuarioCrea;
                            vValidarTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarTercero.FechaCrea;
                        }
                        return vValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validaci�n de terceros
        /// </summary>
        /// <param name="pIdTercero">Identificador del tercero</param>
        /// <param name="pObservaciones">Observaciones en una entidad validaci�n de terceros</param>
        /// <param name="pConfirmaYAprueba">Es Confirma ya aprueba valor en una entidad validaci�n de terceros</param>
        /// <returns>Lista de entidades validaci�n de terceros</returns>
        public List<ValidarTercero> ConsultarValidarTerceros(Int32 pIdTercero, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarTerceros_Consultar"))
                {
                    if(pIdTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if(pObservaciones != null)
                         vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if(pConfirmaYAprueba != null)
                         vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarTercero> vListaValidarTercero = new List<ValidarTercero>();
                        while (vDataReaderResults.Read())
                        {
                                ValidarTercero vValidarTercero = new ValidarTercero();
                            vValidarTercero.IdValidarTercero = vDataReaderResults["IdValidarTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarTercero"].ToString()) : vValidarTercero.IdValidarTercero;
                            vValidarTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vValidarTercero.IdTercero;
                            vValidarTercero.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarTercero.Observaciones;
                            vValidarTercero.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarTercero.ConfirmaYAprueba;
                            vValidarTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarTercero.UsuarioCrea;
                            vValidarTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarTercero.FechaCrea;
                            // JCHE
                            // Implementacion de nueva variable tipoIncidente 
                            vValidarTercero.TipoIncidente = vDataReaderResults["TipoIncidente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TipoIncidente"].ToString()) : vValidarTercero.TipoIncidente;
                                vListaValidarTercero.Add(vValidarTercero);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar un tipo de transacci�n
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciiu</param>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoTransaction(Icbf.Proveedor.Entity.TipoCiiu pTipoCiiu, Tipoentidad pTipoentidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiiu_Insertar"))
                {

                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoCiiu", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCiiu", DbType.String, pTipoCiiu.CodigoCiiu);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCiiu.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCiiu.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoCiiu.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pTipoCiiu.IdTipoCiiu = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoCiiu").ToString());

                    GenerarLogAuditoria(pTipoCiiu, vDbCommand);

                    //vDbTransaction.Commit();
                    //vDBConnection.Close();

                    // return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidad_Insertar"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddOutParameter(vDbCommand2, "@IdTipoentidad", DbType.Int32, 18);
                        vDataBase.AddInParameter(vDbCommand2, "@CodigoTipoentidad", DbType.String, pTipoentidad.CodigoTipoentidad);
                        vDataBase.AddInParameter(vDbCommand2, "@Descripcion", DbType.String, pTipoentidad.Descripcion);
                        vDataBase.AddInParameter(vDbCommand2, "@Estado", DbType.Boolean, pTipoentidad.Estado);
                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioCrea", DbType.String, pTipoentidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);
                        pTipoentidad.IdTipoentidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand2, "@IdTipoentidad").ToString());
                        GenerarLogAuditoria(pTipoentidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw ex;
            }
        }

        /// <summary>
        /// Modifica el estado de una entidad Tercero
        /// </summary>
        /// <param name="pTercero">Entidad Tercero</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTercero_EstadoTercero(Oferente.Entity.Tercero pTercero)
        {
            try
            {
                int vResultado;
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Terceros_Modificar_Estado"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IDTERCERO", DbType.Int32, pTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IDESTADOTERCERO", DbType.Int32, pTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@USUARIOMODIFICA", DbType.String , pTercero.UsuarioModifica);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
