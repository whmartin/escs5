﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad de validación de módulo Integrantes
    /// </summary>
    public class ValidacionIntegrantesEntidadDAL : GeneralDAL
    {
        public ValidacionIntegrantesEntidadDAL() { }



        /// <summary>
        /// Modifica una validacion del modulo integrantes para cambiar su estado de validación.
        /// </summary>
        /// <param name="pValidacionIntegrantesEntidad">ValidacionIntegrantesEntidad</param>
        /// <returns>Identificador de ValidacionIntegrantesEntidad en tabla</returns>
        public int ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(Icbf.Proveedor.Entity.ValidacionIntegrantesEntidad pValidacionIntegrantesEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pValidacionIntegrantesEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoIntegrantes", DbType.Int32, pValidacionIntegrantesEntidad.IdEstadoValidacionIntegrantes);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pValidacionIntegrantesEntidad.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pValidacionIntegrantesEntidad.Finalizado);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValidacionIntegrantesEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// Consulta un ValidacionIntegrantesEntidad de una Entidad
        /// </summary>
        /// <param name="pIdEntidad">IdEntidad</param>
        /// <returns>ValidacionIntegrantesEntidad</returns>
        public ValidacionIntegrantesEntidad Consultar_ValidacionIntegrantesEntidad(int pIdEntidad)
        {
         try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Icbf.Proveedor.Entity.ValidacionIntegrantesEntidad vValidacionIntegrantesEntidad = new Icbf.Proveedor.Entity.ValidacionIntegrantesEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidacionIntegrantesEntidad.IdValidacionIntegrantesEntidad = vDataReaderResults["IdValidacionIntegrantesEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidacionIntegrantesEntidad"].ToString()) : vValidacionIntegrantesEntidad.IdValidacionIntegrantesEntidad;
                            vValidacionIntegrantesEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidacionIntegrantesEntidad.IdEntidad;
                            vValidacionIntegrantesEntidad.IdEstadoValidacionIntegrantes = vDataReaderResults["IdEstadoValidacionIntegrantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoValidacionIntegrantes"].ToString()) : vValidacionIntegrantesEntidad.IdEstadoValidacionIntegrantes;
                            vValidacionIntegrantesEntidad.EstadoValidacionIntegrantes = vDataReaderResults["EstadoValidacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoValidacion"].ToString()) : vValidacionIntegrantesEntidad.EstadoValidacionIntegrantes;
                            vValidacionIntegrantesEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidacionIntegrantesEntidad.NroRevision;
                            vValidacionIntegrantesEntidad.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vValidacionIntegrantesEntidad.Finalizado;
                            vValidacionIntegrantesEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidacionIntegrantesEntidad.UsuarioCrea;
                            vValidacionIntegrantesEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidacionIntegrantesEntidad.FechaCrea;
                            vValidacionIntegrantesEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValidacionIntegrantesEntidad.UsuarioModifica;
                            vValidacionIntegrantesEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValidacionIntegrantesEntidad.FechaModifica;
                        }
                        return vValidacionIntegrantesEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Finaliza ValidacionIntegrantes
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public int FinalizaValidacionIntegrantesEntidad(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
  

    }
}
