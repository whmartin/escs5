using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Entity;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Documento programa
    /// </summary>
    public class TipoDocumentoProgramaDAL : GeneralDAL
    {
        public TipoDocumentoProgramaDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoDocumentoPrograma", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumentoPrograma.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pTipoDocumentoPrograma.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTipoDocumentoPrograma.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@MaxPermitidoKB", DbType.Int32, pTipoDocumentoPrograma.MaxPermitidoKB);
                    vDataBase.AddInParameter(vDbCommand, "@ExtensionesPermitidas", DbType.String, pTipoDocumentoPrograma.ExtensionesPermitidas);
                    vDataBase.AddInParameter(vDbCommand, "@ObligRupNoRenovado", DbType.Int32, pTipoDocumentoPrograma.ObligRupNoRenovado);
                    vDataBase.AddInParameter(vDbCommand, "@ObligRupRenovado", DbType.Int32, pTipoDocumentoPrograma.ObligRupRenovado);
                    vDataBase.AddInParameter(vDbCommand, "@ObligPersonaJuridica", DbType.Int32, pTipoDocumentoPrograma.ObligPersonaJuridica);
                    vDataBase.AddInParameter(vDbCommand, "@ObligPersonaNatural", DbType.Int32, pTipoDocumentoPrograma.ObligPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@ObligSectorPublico", DbType.Int32, pTipoDocumentoPrograma.ObligSectorPublico);
                    vDataBase.AddInParameter(vDbCommand, "@ObligSectorPrivado", DbType.Int32, pTipoDocumentoPrograma.ObligSectorPrivado);

                    vDataBase.AddInParameter(vDbCommand, "@ObligEntidadNacioanl", DbType.Boolean, pTipoDocumentoPrograma.ObligENtidadNAcional);
                    vDataBase.AddInParameter(vDbCommand, "@ObligEntidadExtrajera", DbType.Boolean, pTipoDocumentoPrograma.ObligENtidadExtrajera);
                    vDataBase.AddInParameter(vDbCommand, "@ObligConsorcio", DbType.Boolean, pTipoDocumentoPrograma.ObligConsorcio);
                    vDataBase.AddInParameter(vDbCommand, "@ObligUnionTemporal", DbType.Boolean, pTipoDocumentoPrograma.ObligUnionTemporal);
                    
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoDocumentoPrograma.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoDocumentoPrograma.IdTipoDocumentoPrograma = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoDocumentoPrograma").ToString());
                    GenerarLogAuditoria(pTipoDocumentoPrograma, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoPrograma", DbType.Int32, pTipoDocumentoPrograma.IdTipoDocumentoPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pTipoDocumentoPrograma.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pTipoDocumentoPrograma.IdPrograma);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pTipoDocumentoPrograma.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@MaxPermitidoKB", DbType.Int32, pTipoDocumentoPrograma.MaxPermitidoKB);
                    vDataBase.AddInParameter(vDbCommand, "@ExtensionesPermitidas", DbType.String, pTipoDocumentoPrograma.ExtensionesPermitidas);
                    vDataBase.AddInParameter(vDbCommand, "@ObligRupNoRenovado", DbType.Int32, pTipoDocumentoPrograma.ObligRupNoRenovado);
                    vDataBase.AddInParameter(vDbCommand, "@ObligRupRenovado", DbType.Int32, pTipoDocumentoPrograma.ObligRupRenovado);
                    vDataBase.AddInParameter(vDbCommand, "@ObligPersonaJuridica", DbType.Int32, pTipoDocumentoPrograma.ObligPersonaJuridica);
                    vDataBase.AddInParameter(vDbCommand, "@ObligPersonaNatural", DbType.Int32, pTipoDocumentoPrograma.ObligPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@ObligSectorPublico", DbType.Int32, pTipoDocumentoPrograma.ObligSectorPublico );
                    vDataBase.AddInParameter(vDbCommand, "@ObligSectorPrivado", DbType.Int32, pTipoDocumentoPrograma.ObligSectorPrivado );

                    vDataBase.AddInParameter(vDbCommand, "@ObligEntidadNacioanl", DbType.Boolean, pTipoDocumentoPrograma.ObligENtidadNAcional);
                    vDataBase.AddInParameter(vDbCommand, "@ObligEntidadExtrajera", DbType.Boolean, pTipoDocumentoPrograma.ObligENtidadExtrajera);
                    vDataBase.AddInParameter(vDbCommand, "@ObligConsorcio", DbType.Boolean, pTipoDocumentoPrograma.ObligConsorcio);
                    vDataBase.AddInParameter(vDbCommand, "@ObligUnionTemporal", DbType.Boolean, pTipoDocumentoPrograma.ObligUnionTemporal);

                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoDocumentoPrograma.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocumentoPrograma, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoPrograma", DbType.Int32, pTipoDocumentoPrograma.IdTipoDocumentoPrograma);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocumentoPrograma, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumentoPrograma">Identificador en una entidad Tipo Documento programa</param>
        /// <returns>Entidad Tipo Documento programa</returns>
        public TipoDocumentoPrograma ConsultarTipoDocumentoPrograma(int pIdTipoDocumentoPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoPrograma", DbType.Int32, pIdTipoDocumentoPrograma);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocumentoPrograma vTipoDocumentoPrograma = new TipoDocumentoPrograma();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocumentoPrograma.IdTipoDocumentoPrograma = vDataReaderResults["IdTipoDocumentoPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoPrograma"].ToString()) : vTipoDocumentoPrograma.IdTipoDocumentoPrograma;
                            vTipoDocumentoPrograma.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumentoPrograma.IdTipoDocumento;
                            vTipoDocumentoPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vTipoDocumentoPrograma.IdPrograma;
                            vTipoDocumentoPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoPrograma.Estado;
                            vTipoDocumentoPrograma.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["MaxPermitidoKB"].ToString()) : vTipoDocumentoPrograma.MaxPermitidoKB;
                            vTipoDocumentoPrograma.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vTipoDocumentoPrograma.ExtensionesPermitidas;
                            vTipoDocumentoPrograma.ObligRupNoRenovado = vDataReaderResults["ObligRupNoRenovado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligRupNoRenovado"].ToString()) : vTipoDocumentoPrograma.ObligRupNoRenovado;
                            vTipoDocumentoPrograma.ObligRupRenovado = vDataReaderResults["ObligRupRenovado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligRupRenovado"].ToString()) : vTipoDocumentoPrograma.ObligRupRenovado;
                            vTipoDocumentoPrograma.ObligPersonaJuridica = vDataReaderResults["ObligPersonaJuridica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligPersonaJuridica"].ToString()) : vTipoDocumentoPrograma.ObligPersonaJuridica;
                            vTipoDocumentoPrograma.ObligPersonaNatural = vDataReaderResults["ObligPersonaNatural"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligPersonaNatural"].ToString()) : vTipoDocumentoPrograma.ObligPersonaNatural;

                            vTipoDocumentoPrograma.ObligSectorPublico = vDataReaderResults["ObligSectorPublico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligSectorPublico"].ToString()) : vTipoDocumentoPrograma.ObligSectorPublico;
                            vTipoDocumentoPrograma.ObligSectorPrivado = vDataReaderResults["ObligSectorPrivado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligSectorPrivado"].ToString()) : vTipoDocumentoPrograma.ObligSectorPrivado;
                            vTipoDocumentoPrograma.ObligENtidadNAcional = vDataReaderResults["ObligEntNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligEntNacional"].ToString()) : vTipoDocumentoPrograma.ObligENtidadNAcional;
                            vTipoDocumentoPrograma.ObligENtidadExtrajera = vDataReaderResults["ObligEntExtranjera"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligEntExtranjera"].ToString()) : vTipoDocumentoPrograma.ObligENtidadExtrajera;
                            vTipoDocumentoPrograma.ObligConsorcio = vDataReaderResults["ObligConsorcio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligConsorcio"].ToString()) : vTipoDocumentoPrograma.ObligConsorcio;
                            vTipoDocumentoPrograma.ObligUnionTemporal = vDataReaderResults["ObligUnionTemporal"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligUnionTemporal"].ToString()) : vTipoDocumentoPrograma.ObligUnionTemporal;

                            vTipoDocumentoPrograma.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoPrograma.UsuarioCrea;
                            vTipoDocumentoPrograma.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoPrograma.FechaCrea;
                            vTipoDocumentoPrograma.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoPrograma.UsuarioModifica;
                            vTipoDocumentoPrograma.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoPrograma.FechaModifica;
                        }
                        return vTipoDocumentoPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumentoPrograma">Identificador del tipo de documento de programa en una entidad Tipo Documento programa</param>
        /// <param name="pIdTipoDocumento">Identificador del tipo de documento</param>
        /// <param name="pIdPrograma">Identificador del programa</param>
        /// <returns>Lista de entidades Tipo Documento programa</returns>
        public List<TipoDocumentoPrograma> ConsultarTipoDocumentoProgramas(int? pIdTipoDocumentoPrograma, int? pIdTipoDocumento, int? pIdPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumentoProgramas_Consultar"))
                {
                    if(pIdTipoDocumentoPrograma != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumentoPrograma", DbType.Int32, pIdTipoDocumentoPrograma);
                    if(pIdTipoDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento);
                    if(pIdPrograma != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdPrograma", DbType.Int32, pIdPrograma);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumentoPrograma> vListaTipoDocumentoPrograma = new List<TipoDocumentoPrograma>();
                        while (vDataReaderResults.Read())
                        {
                                TipoDocumentoPrograma vTipoDocumentoPrograma = new TipoDocumentoPrograma();
                            vTipoDocumentoPrograma.IdTipoDocumentoPrograma = vDataReaderResults["IdTipoDocumentoPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoPrograma"].ToString()) : vTipoDocumentoPrograma.IdTipoDocumentoPrograma;
                            vTipoDocumentoPrograma.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumentoPrograma.IdTipoDocumento;
                            vTipoDocumentoPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vTipoDocumentoPrograma.IdPrograma;
                            vTipoDocumentoPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vTipoDocumentoPrograma.CodigoPrograma;
                            vTipoDocumentoPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vTipoDocumentoPrograma.NombrePrograma;
                            vTipoDocumentoPrograma.CodigoTipoDocumento = vDataReaderResults["CodigoTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoDocumento"].ToString()) : vTipoDocumentoPrograma.CodigoTipoDocumento;
                            vTipoDocumentoPrograma.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocumentoPrograma.Descripcion;
                            vTipoDocumentoPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoPrograma.Estado;
                            vTipoDocumentoPrograma.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["MaxPermitidoKB"].ToString()) : vTipoDocumentoPrograma.MaxPermitidoKB;
                            vTipoDocumentoPrograma.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vTipoDocumentoPrograma.ExtensionesPermitidas;
                            vTipoDocumentoPrograma.ObligRupNoRenovado = vDataReaderResults["ObligRupNoRenovado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligRupNoRenovado"].ToString()) : vTipoDocumentoPrograma.ObligRupNoRenovado;
                            vTipoDocumentoPrograma.ObligRupRenovado = vDataReaderResults["ObligRupRenovado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligRupRenovado"].ToString()) : vTipoDocumentoPrograma.ObligRupRenovado;
                            vTipoDocumentoPrograma.ObligPersonaJuridica = vDataReaderResults["ObligPersonaJuridica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligPersonaJuridica"].ToString()) : vTipoDocumentoPrograma.ObligPersonaJuridica;
                            vTipoDocumentoPrograma.ObligPersonaNatural = vDataReaderResults["ObligPersonaNatural"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligPersonaNatural"].ToString()) : vTipoDocumentoPrograma.ObligPersonaNatural;

                            vTipoDocumentoPrograma.ObligSectorPublico = vDataReaderResults["ObligSectorPublico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligSectorPublico"].ToString()) : vTipoDocumentoPrograma.ObligSectorPublico;
                            vTipoDocumentoPrograma.ObligSectorPrivado = vDataReaderResults["ObligSectorPrivado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["ObligSectorPrivado"].ToString()) : vTipoDocumentoPrograma.ObligSectorPrivado;

                            vTipoDocumentoPrograma.ObligENtidadNAcional = vDataReaderResults["ObligEntNacional"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligEntNacional"].ToString()) : vTipoDocumentoPrograma.ObligENtidadNAcional;
                            vTipoDocumentoPrograma.ObligENtidadExtrajera = vDataReaderResults["ObligEntExtranjera"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligEntExtranjera"].ToString()) : vTipoDocumentoPrograma.ObligENtidadExtrajera;
                            vTipoDocumentoPrograma.ObligConsorcio = vDataReaderResults["ObligConsorcio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligConsorcio"].ToString()) : vTipoDocumentoPrograma.ObligConsorcio;
                            vTipoDocumentoPrograma.ObligUnionTemporal = vDataReaderResults["ObligUnionTemporal"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ObligUnionTemporal"].ToString()) : vTipoDocumentoPrograma.ObligUnionTemporal;
                     
                            vTipoDocumentoPrograma.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoPrograma.UsuarioCrea;
                            vTipoDocumentoPrograma.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoPrograma.FechaCrea;
                            vTipoDocumentoPrograma.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoPrograma.UsuarioModifica;
                            vTipoDocumentoPrograma.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoPrograma.FechaModifica;
                                vListaTipoDocumentoPrograma.Add(vTipoDocumentoPrograma);
                        }
                        return vListaTipoDocumentoPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumento">Identificador del tipo del documento en una entidad Tipo Documento programa</param>
        /// <returns>Lista de entidades Tipo Documento programa</returns>
        public List<TipoDocumentoProveedor> ConsultarTipoDocumentos(int? pIdTipoDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumento_Consultar"))
                {
                    if (pIdTipoDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pIdTipoDocumento);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocumentoProveedor> vListaTipoDocumento = new List<TipoDocumentoProveedor>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocumentoProveedor vTipoDocumentoPrograma = new TipoDocumentoProveedor();
                            vTipoDocumentoPrograma.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocumentoPrograma.IdTipoDocumento;
                            vTipoDocumentoPrograma.CodigoTipoDocumento = vDataReaderResults["CodigoTipoDocumento"] != DBNull.Value ? Convert.ToString (vDataReaderResults["CodigoTipoDocumento"].ToString()) : vTipoDocumentoPrograma.CodigoTipoDocumento;
                            vTipoDocumentoPrograma.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocumentoPrograma.Descripcion;
                            vTipoDocumentoPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoDocumentoPrograma.Estado;
                            vTipoDocumentoPrograma.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocumentoPrograma.UsuarioCrea;
                            vTipoDocumentoPrograma.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocumentoPrograma.FechaCrea;
                            vTipoDocumentoPrograma.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocumentoPrograma.UsuarioModifica;
                            vTipoDocumentoPrograma.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocumentoPrograma.FechaModifica;
                            vListaTipoDocumento.Add(vTipoDocumentoPrograma);
                        }
                        return vListaTipoDocumento;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar programas
        /// </summary>
        /// <param name="pIdModulo">Identificador del m�dulo</param>
        /// <returns>Entidad Programa</returns>
        public List<Programa> ConsultarProgramas(int pIdModulo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@idModulo", DbType.Int32, pIdModulo);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Programa> vListaPrograma = new List<Programa>();
                        while (vDataReaderResults.Read())
                        {
                            Programa vPrograma = new Programa();
                            vPrograma.IdPrograma = vDataReaderResults["IdPrograma"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdPrograma"].ToString()) : vPrograma.IdPrograma;
                            vPrograma.IdModulo = vDataReaderResults["IdModulo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdModulo"].ToString()) : vPrograma.IdModulo;
                            vPrograma.NombrePrograma = vDataReaderResults["NombrePrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombrePrograma"].ToString()) : vPrograma.NombrePrograma;
                            vPrograma.CodigoPrograma = vDataReaderResults["CodigoPrograma"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoPrograma"].ToString()) : vPrograma.CodigoPrograma;
                            vPrograma.Posicion = vDataReaderResults["Posicion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Posicion"].ToString()) : vPrograma.Posicion;
                            vPrograma.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"]) : vPrograma.Estado;
                            vPrograma.VisibleMenu = vDataReaderResults["VisibleMenu"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["VisibleMenu"]) : vPrograma.VisibleMenu;
                            vPrograma.UsuarioCreacion = vDataReaderResults["UsuarioCreacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCreacion"].ToString()) : vPrograma.UsuarioCreacion;
                            vPrograma.FechaCreacion = vDataReaderResults["FechaCreacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCreacion"].ToString()) : vPrograma.FechaCreacion;
                            vPrograma.UsuarioModificacion = vDataReaderResults["UsuarioModificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModificacion"].ToString()) : vPrograma.UsuarioModificacion;
                            vPrograma.FechaModificacion = vDataReaderResults["FechaModificacion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModificacion"].ToString()) : vPrograma.FechaModificacion;
                            vListaPrograma.Add(vPrograma);
                        }
                        return vListaPrograma;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
