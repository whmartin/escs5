using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad sucursal
    /// </summary>
    public class SucursalDAL : GeneralDAL
    {
        public SucursalDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarSucursal(Sucursal pSucursal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Sucursal_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdSucursal", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pSucursal.IdEntidad);
                    if (pSucursal.Indicativo != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.Int32, pSucursal.Indicativo);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSucursal.Nombre);
                    if (pSucursal.Telefono != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.Int32, pSucursal.Telefono);
                    }
                    if (pSucursal.Extension != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.Int64, pSucursal.Extension);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.Int64, pSucursal.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@Correo", DbType.String, pSucursal.Correo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSucursal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pSucursal.IdZona );
                    vDataBase.AddInParameter(vDbCommand, "@Departamento", DbType.Int32, pSucursal.Departamento);
                    vDataBase.AddInParameter(vDbCommand, "@Municipio", DbType.Int32, pSucursal.Municipio);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pSucursal.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pSucursal.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Editable", DbType.Int32, pSucursal.Editable);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pSucursal.IdSucursal = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdSucursal").ToString());
                    GenerarLogAuditoria(pSucursal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarSucursal(Sucursal pSucursal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Sucursal_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSucursal", DbType.Int32, pSucursal.IdSucursal);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pSucursal.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSucursal.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.Int32, pSucursal.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.Int32, pSucursal.Telefono);
                    if (pSucursal.Extension!=null)
                        vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.Int64, pSucursal.Extension.Value);                    
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.Int64, pSucursal.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@Correo", DbType.String, pSucursal.Correo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSucursal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pSucursal.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Departamento", DbType.Int32, pSucursal.Departamento);
                    vDataBase.AddInParameter(vDbCommand, "@Municipio", DbType.Int32, pSucursal.Municipio);                    
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pSucursal.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSucursal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSucursal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica Sucursal Default
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarSucursalDefault(Sucursal pSucursal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Sucursal_ModificarDefault"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pSucursal.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pSucursal.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@Indicativo", DbType.Int32, pSucursal.Indicativo);
                    vDataBase.AddInParameter(vDbCommand, "@Telefono", DbType.Int32, pSucursal.Telefono);
                    if (pSucursal.Extension != null)
                        vDataBase.AddInParameter(vDbCommand, "@Extension", DbType.Int64, pSucursal.Extension.Value);
                    vDataBase.AddInParameter(vDbCommand, "@Celular", DbType.Int64, pSucursal.Celular);
                    vDataBase.AddInParameter(vDbCommand, "@Correo", DbType.String, pSucursal.Correo);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pSucursal.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pSucursal.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Departamento", DbType.Int32, pSucursal.Departamento);
                    vDataBase.AddInParameter(vDbCommand, "@Municipio", DbType.Int32, pSucursal.Municipio);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pSucursal.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pSucursal.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSucursal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarSucursal(Sucursal pSucursal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Sucursal_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdSucursal", DbType.Int32, pSucursal.IdSucursal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pSucursal, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Sucursal
        /// </summary>
        /// <param name="pIdSucursal">Identificador de una entidad Sucursal</param>
        /// <returns>Entidad Sucursal</returns>
        public Sucursal ConsultarSucursal(int pIdSucursal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Sucursal_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdSucursal", DbType.Int32, pIdSucursal);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Sucursal vSucursal = new Sucursal();
                        while (vDataReaderResults.Read())
                        {
                            vSucursal.IdSucursal = vDataReaderResults["IdSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursal"].ToString()) : vSucursal.IdSucursal;
                            vSucursal.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vSucursal.IdEntidad;
                            vSucursal.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSucursal.Nombre;
                            vSucursal.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Indicativo"].ToString()) : vSucursal.Indicativo;
                            vSucursal.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Telefono"].ToString()) : vSucursal.Telefono;
                            vSucursal.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Extension"].ToString()) : vSucursal.Extension;
                            vSucursal.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Int64.Parse(vDataReaderResults["Celular"].ToString()) : vSucursal.Celular;
                            vSucursal.Correo = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"].ToString()) : vSucursal.Correo;
                            vSucursal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSucursal.Estado;
                            vSucursal.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vSucursal.IdZona;
                            vSucursal.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Departamento"].ToString()) : vSucursal.Departamento;
                            vSucursal.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Municipio"].ToString()) : vSucursal.Municipio;
                            vSucursal.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vSucursal.Direccion;
                            vSucursal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSucursal.UsuarioCrea;
                            vSucursal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSucursal.FechaCrea;
                            vSucursal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSucursal.UsuarioModifica;
                            vSucursal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSucursal.FechaModifica;
                            vSucursal.Editable = vDataReaderResults["Editable"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Editable"].ToString()) : vSucursal.Editable;
                        }
                        return vSucursal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Sucursal
        /// </summary>
        /// <param name="pIdEntidad">Identificador general de una entidad Sucursal</param>
        /// <param name="pEstado">estado de una entidad Sucursal</param>
        /// <returns>Lista de entidades Sucursal</returns>
        public List<Sucursal> ConsultarSucursals(int? pIdEntidad, int? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Sucursals_Consultar"))
                {
                    if(pIdEntidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Sucursal> vListaSucursal = new List<Sucursal>();
                        while (vDataReaderResults.Read())
                        {
                                Sucursal vSucursal = new Sucursal();
                            vSucursal.IdSucursal = vDataReaderResults["IdSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursal"].ToString()) : vSucursal.IdSucursal;
                            vSucursal.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vSucursal.IdEntidad;
                            vSucursal.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vSucursal.Nombre;
                            vSucursal.Indicativo = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Indicativo"].ToString()) : vSucursal.Indicativo;
                            vSucursal.Telefono = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Telefono"].ToString()) : vSucursal.Telefono;
                            vSucursal.Extension = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Extension"].ToString()) : vSucursal.Extension;
                            vSucursal.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToInt64(vDataReaderResults["Celular"].ToString()) : vSucursal.Celular;
                            vSucursal.Correo = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"].ToString()) : vSucursal.Correo;
                            vSucursal.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vSucursal.Estado;
                            vSucursal.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vSucursal.IdZona;
                            vSucursal.Departamento = vDataReaderResults["Departamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Departamento"].ToString()) : vSucursal.Departamento;
                            vSucursal.Municipio = vDataReaderResults["Municipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Municipio"].ToString()) : vSucursal.Municipio;
                            vSucursal.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vSucursal.Direccion;
                            vSucursal.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vSucursal.UsuarioCrea;
                            vSucursal.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vSucursal.FechaCrea;
                            vSucursal.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vSucursal.UsuarioModifica;
                            vSucursal.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vSucursal.FechaModifica;
                                vListaSucursal.Add(vSucursal);
                        }
                        return vListaSucursal;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
