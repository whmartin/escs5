using System;
using System.Collections.Generic;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad de Proveedores Oferente
    /// </summary>
    public class EntidadProvOferenteDal : GeneralDAL
    {

        public EntidadProvOferenteDal()
        {
        }

        /// <summary>
        /// Modifica una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        /// 
        public int ModificarEntidadProvOferente(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            int vResultado;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Modificar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    string usuario = "";

                    #region Datos Telefono Proveedor

                    string spProveedoTelTercero = "";
                    Int32 idTelTercero = 0;
                    if (pEntidadProvOferente.TelTerceroProveedor.IdTercero == 0)
                    {
                        spProveedoTelTercero = "usp_RubOnline_Oferente_TelTerceros_Insertar";
                        usuario = "@UsuarioCrea";
                    }
                    else
                    {
                        spProveedoTelTercero = "usp_RubOnline_Oferente_TelTerceros_Modificar";
                        idTelTercero = pEntidadProvOferente.TelTerceroProveedor.IdTelTercero;
                        usuario = "@UsuarioModifica";
                    }
                    using (DbCommand vDbCommand4 = vDataBase.GetStoredProcCommand(spProveedoTelTercero))
                    {
                        if (idTelTercero == 0)
                        {
                            vDataBase.AddOutParameter(vDbCommand4, "@IdTelTercero", DbType.Int32, idTelTercero);
                        }
                        else
                        {
                            vDataBase.AddInParameter(vDbCommand4, "@IdTelTercero", DbType.Int32, idTelTercero);
                        }

                        vDataBase.AddInParameter(vDbCommand4, "@IdTercero", DbType.Int32,
                                                 pEntidadProvOferente.TerceroProveedor.IdTercero);
                        vDataBase.AddInParameter(vDbCommand4, "@IndicativoTelefono", DbType.Int32,
                                                 pEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono);
                        vDataBase.AddInParameter(vDbCommand4, "@NumeroTelefono", DbType.Int32,
                                                 pEntidadProvOferente.TelTerceroProveedor.NumeroTelefono);
                        vDataBase.AddInParameter(vDbCommand4, "@ExtensionTelefono", DbType.Int64,
                                                 pEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono);
                        vDataBase.AddInParameter(vDbCommand4, "@Movil", DbType.Int64,
                                                 pEntidadProvOferente.TelTerceroProveedor.Movil);
                        if (pEntidadProvOferente.TelTerceroProveedor.IndicativoFax != null)
                        {
                            vDataBase.AddInParameter(vDbCommand4, "@IndicativoFax", DbType.Int32,
                                                     pEntidadProvOferente.TelTerceroProveedor.IndicativoFax);
                        }
                        if (pEntidadProvOferente.TelTerceroProveedor.NumeroFax != null)
                        {
                            vDataBase.AddInParameter(vDbCommand4, "@NumeroFax", DbType.Int32,
                                                     pEntidadProvOferente.TelTerceroProveedor.NumeroFax);
                        }
                        vDataBase.AddInParameter(vDbCommand4, usuario, DbType.String,
                                                 pEntidadProvOferente.TelTerceroProveedor.UsuarioCrea);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand4, vDbTransaction);
                        if (idTelTercero == 0)
                        {
                            pEntidadProvOferente.TelTerceroProveedor.IdTelTercero =
                                Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand4, "@IdTelTercero").ToString());
                        }
                        GenerarLogAuditoria(pEntidadProvOferente.TelTerceroProveedor, vDbCommand4);

                    }

                    #endregion


                    #region Adiciona o actualiza Representante Legal

                    if (pEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 || pEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || pEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                    {
                        string spRepTercero = "";
                        Int32 idReplegaTercero = 0;
                        if (pEntidadProvOferente.RepresentanteLegal.IdTercero == 0)
                        {
                            spRepTercero = "usp_RubOnline_Oferente_RepresentanteLegal_Insertar";
                            usuario = "@UsuarioCrea";
                        }
                        else
                        {
                            spRepTercero = "usp_RubOnline_Oferente_RepresentanteLegal_Modificar";
                            idReplegaTercero = pEntidadProvOferente.RepresentanteLegal.IdTercero;
                            usuario = "@UsuarioModifica";
                        }

                        using (DbCommand vDbCommand5 = vDataBase.GetStoredProcCommand(spRepTercero))
                        {
                            vDataBase.AddInParameter(vDbCommand5, "@IdEntidad", DbType.Int32, pEntidadProvOferente.IdEntidad);

                            if (idReplegaTercero == 0)
                            {
                                vDataBase.AddOutParameter(vDbCommand5, "@IdTercero", DbType.Int32, idReplegaTercero);
                            }
                            else
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IdTercero", DbType.Int32, idReplegaTercero);
                            }
                            if (pEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento != -1 &&
                                pEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento != 0)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IdDListaTipoDocumento", DbType.Int32,
                                                         pEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@NumeroIdentificacion", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.PrimerNombre))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@PrimerNombre", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.PrimerNombre.Trim());
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.SegundoNombre))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@SegundoNombre", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.SegundoNombre.Trim());
                            }
                            else
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@SegundoNombre", DbType.String, string.Empty);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.PrimerApellido))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@PrimerApellido", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.PrimerApellido.Trim());
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.SegundoApellido))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@SegundoApellido", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.SegundoApellido.Trim());
                            }
                            else
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@SegundoApellido", DbType.String, string.Empty);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.Email))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@Email", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.Email.Trim());
                            }
                            if (pEntidadProvOferente.RepresentanteLegal.IdTipoPersona != null &&
                                pEntidadProvOferente.RepresentanteLegal.IdTipoPersona != null)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IdTipoPersona", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.IdTipoPersona);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.Sexo))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@Sexo", DbType.String,
                                                         pEntidadProvOferente.RepresentanteLegal.Sexo);
                            }
                            if (pEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@FechaNacimiento", DbType.DateTime, pEntidadProvOferente.RepresentanteLegal.FechaNacimiento);
                            }
                            vDataBase.AddInParameter(vDbCommand5, usuario, DbType.String,
                                                     pEntidadProvOferente.RepresentanteLegal.UsuarioCrea);
                            vDataBase.AddInParameter(vDbCommand5, "@CreadoPorInterno", DbType.Boolean, pEntidadProvOferente.TerceroProveedor.CreadoPorInterno);
                            vDataBase.ExecuteNonQuery(vDbCommand5, vDbTransaction);
                            if (idReplegaTercero == 0)
                            {
                                pEntidadProvOferente.RepresentanteLegal.IdTercero =
                                    Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand5, "@IdTercero").ToString());
                            }

                            GenerarLogAuditoria(pEntidadProvOferente.RepresentanteLegal, vDbCommand5);

                        }
                        string spRepreTelTercero = "";
                        Int32 idTelRepreTercero = 0;
                        if (pEntidadProvOferente.TelRepresentanteLegal.IdTelTercero == 0)
                        {
                            spRepreTelTercero = "usp_RubOnline_Oferente_TelTerceros_Insertar";
                            usuario = "@UsuarioCrea";
                        }
                        else
                        {
                            spRepreTelTercero = "usp_RubOnline_Oferente_TelTerceros_Modificar";
                            idTelRepreTercero = pEntidadProvOferente.TelRepresentanteLegal.IdTelTercero;
                            usuario = "@UsuarioModifica";
                        }
                        using (DbCommand vDbCommand6 = vDataBase.GetStoredProcCommand(spRepreTelTercero))
                        {
                            if (idTelRepreTercero == 0)
                            {
                                vDataBase.AddOutParameter(vDbCommand6, "@IdTelTercero", DbType.Int32, idTelRepreTercero);
                            }
                            else
                            {
                                vDataBase.AddInParameter(vDbCommand6, "@IdTelTercero", DbType.Int32, idTelRepreTercero);
                            }
                            vDataBase.AddInParameter(vDbCommand6, "@IdTercero", DbType.Int32,
                                                     pEntidadProvOferente.RepresentanteLegal.IdTercero);
                            vDataBase.AddInParameter(vDbCommand6, "@IndicativoTelefono", DbType.Int32,
                                                     pEntidadProvOferente.TelRepresentanteLegal.IndicativoTelefono);
                            vDataBase.AddInParameter(vDbCommand6, "@NumeroTelefono", DbType.Int32,
                                                     pEntidadProvOferente.TelRepresentanteLegal.NumeroTelefono);
                            vDataBase.AddInParameter(vDbCommand6, "@ExtensionTelefono", DbType.Int64,
                                                     pEntidadProvOferente.TelRepresentanteLegal.ExtensionTelefono);
                            vDataBase.AddInParameter(vDbCommand6, "@Movil", DbType.Int64,
                                                     pEntidadProvOferente.TelRepresentanteLegal.Movil);
                            if (pEntidadProvOferente.TelRepresentanteLegal.IndicativoFax != null)
                            {
                                vDataBase.AddInParameter(vDbCommand6, "@IndicativoFax", DbType.Int32,
                                                         pEntidadProvOferente.TelRepresentanteLegal.IndicativoFax);
                            }
                            if (pEntidadProvOferente.TelRepresentanteLegal.NumeroFax != null)
                            {
                                vDataBase.AddInParameter(vDbCommand6, "@NumeroFax", DbType.Int32,
                                                         pEntidadProvOferente.TelRepresentanteLegal.NumeroFax);
                            }
                            vDataBase.AddInParameter(vDbCommand6, usuario, DbType.String,
                                                     pEntidadProvOferente.TelRepresentanteLegal.UsuarioCrea);
                            vResultado = vDataBase.ExecuteNonQuery(vDbCommand6, vDbTransaction);
                            if (idTelRepreTercero == 0)
                            {
                                pEntidadProvOferente.TelRepresentanteLegal.IdTelTercero =
                                    Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand6, "@IdTelTercero").ToString());
                            }
                            GenerarLogAuditoria(pEntidadProvOferente.TelRepresentanteLegal, vDbCommand6);

                        }
                    }

                    #endregion




                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pEntidadProvOferente.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@TipoEntOfProv", DbType.Boolean,
                                             pEntidadProvOferente.TipoEntOfProv);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32,
                                             pEntidadProvOferente.TerceroProveedor.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiuPrincipal", DbType.Int32,
                                             pEntidadProvOferente.IdTipoCiiuPrincipal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiuSecundario", DbType.Int32,
                                             pEntidadProvOferente.IdTipoCiiuSecundario);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32,
                                             pEntidadProvOferente.IdTipoSector);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoClaseEntidad", DbType.Int32,
                                             pEntidadProvOferente.IdTipoClaseEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRamaPublica", DbType.Int32,
                                             pEntidadProvOferente.IdTipoRamaPublica);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoNivelGob", DbType.Int32,
                                             pEntidadProvOferente.IdTipoNivelGob);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoNivelOrganizacional", DbType.Int32,
                                             pEntidadProvOferente.IdTipoNivelOrganizacional);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCertificadorCalidad", DbType.Int32,
                                             pEntidadProvOferente.IdTipoCertificadorCalidad);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean,
                      pEntidadProvOferente.Finalizado);
                    if (pEntidadProvOferente.FechaCiiuPrincipal.ToString() != "01/01/1900 12:00:00 a.m.")
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaCiiuPrincipal", DbType.DateTime,
                                                 pEntidadProvOferente.FechaCiiuPrincipal);
                    }
                    if (pEntidadProvOferente.FechaCiiuSecundario.ToString() != "01/01/1900 12:00:00 a.m.")
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaCiiuSecundario", DbType.DateTime,
                                                 pEntidadProvOferente.FechaCiiuSecundario);
                    }
                    else
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaCiiuSecundario", DbType.DateTime,
                                                 null);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@FechaConstitucion", DbType.DateTime,
                                             pEntidadProvOferente.FechaConstitucion);
                    vDataBase.AddInParameter(vDbCommand, "@FechaVigencia", DbType.DateTime,
                                             pEntidadProvOferente.FechaVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@FechaMatriculaMerc", DbType.DateTime,
                                             pEntidadProvOferente.FechaMatriculaMerc);
                    vDataBase.AddInParameter(vDbCommand, "@FechaExpiracion", DbType.DateTime,
                                             pEntidadProvOferente.FechaExpiracion);
                    vDataBase.AddInParameter(vDbCommand, "@TipoVigencia", DbType.Boolean,
                                             pEntidadProvOferente.TipoVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@ExenMatriculaMer", DbType.Boolean,
                                             pEntidadProvOferente.ExenMatriculaMer);
                    vDataBase.AddInParameter(vDbCommand, "@MatriculaMercantil", DbType.String,
                                             pEntidadProvOferente.MatriculaMercantil);
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.ObserValidador))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObserValidador", DbType.String,
                                                 pEntidadProvOferente.ObserValidador.Trim());
                    }
                    //
                    // Se agrega esta variable para el manejo de documentos Mauricio Martinez.
                    //
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.IdTemporal))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pEntidadProvOferente.IdTemporal);
                    }

                    if (pEntidadProvOferente.NumIntegrantes != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@NumIntegrantes", DbType.Int32, pEntidadProvOferente.NumIntegrantes);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@AnoRegistro", DbType.Int32, pEntidadProvOferente.AnoRegistro);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pEntidadProvOferente.IdEstado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String,
                                             pEntidadProvOferente.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@fechaICBF", DbType.DateTime, pEntidadProvOferente.FechaIngresoICBF);
                    //Agregar Correo Electronico... para actualizar desde el procedimiento en la tabla de Terceros.
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pEntidadProvOferente.TerceroProveedor.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pEntidadProvOferente.TerceroProveedor.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pEntidadProvOferente.TerceroProveedor.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pEntidadProvOferente.TerceroProveedor.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pEntidadProvOferente.TerceroProveedor.Email);
                    vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar"))
                    {

                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdInfoAdmin != 0)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdInfoAdmin", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdInfoAdmin);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdVigencia != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdVigencia", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdVigencia);
                        }
                        vDataBase.AddInParameter(vDbCommand2, "@IdEntidad", DbType.Int32, pEntidadProvOferente.IdEntidad);
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoRegTrib", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoOrigenCapital != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoOrigenCapital", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoOrigenCapital);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoActividad", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad);
                        }

                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoEntidad", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoNaturalezaJurid != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoNaturalezaJurid", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoNaturalezaJurid);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosTrabajadores != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoRangosTrabajadores", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosTrabajadores);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosActivos != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoRangosActivos", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosActivos);
                        }

                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdRepLegal", DbType.Int32, pEntidadProvOferente.RepresentanteLegal.IdTercero);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoCertificaTamano != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoCertificaTamano", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoCertificaTamano);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoEntidadPublica", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdDepartamentoConstituida", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdMunicipioConstituida", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdDepartamentoDirComercial", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdMunicipioDirComercial", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@DireccionComercial", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.IdZona))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdZona", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdZona);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.NombreComercial))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NombreComercial", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NombreComercial.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NombreEstablecimiento", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.Sigla))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Sigla", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Sigla.Trim());
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PorctjPrivado != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PorctjPrivado", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PorctjPrivado);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PorctjPublico != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PorctjPublico", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PorctjPublico);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.SitioWeb))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@SitioWeb", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.SitioWeb.Trim());
                        }
                        else
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@SitioWeb", DbType.String, string.Empty);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.NombreEntidadAcreditadora))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NombreEntidadAcreditadora", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NombreEntidadAcreditadora.Trim());
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Organigrama != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Organigrama", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Organigrama);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.TotalPnalAnnoPrevio != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@TotalPnalAnnoPrevio", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.TotalPnalAnnoPrevio);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.VincLaboral != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@VincLaboral", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.VincLaboral);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PrestServicios != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PrestServicios", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PrestServicios);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Voluntariado != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Voluntariado", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Voluntariado);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.VoluntPermanente != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@VoluntPermanente", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.VoluntPermanente);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Asociados != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Asociados", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Asociados);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Mision != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Mision", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Mision);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PQRS != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PQRS", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PQRS);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdInfoAdmin != 0)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@GestionDocumental", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.GestionDocumental);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.AuditoriaInterna != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@AuditoriaInterna", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.AuditoriaInterna);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManProcedimiento != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManProcedimiento", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManProcedimiento);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManPracticasAmbiente != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManPracticasAmbiente", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManPracticasAmbiente);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManComportOrg != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManComportOrg", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManComportOrg);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManFunciones != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManFunciones", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManFunciones);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ProcRegInfoContable != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ProcRegInfoContable", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ProcRegInfoContable);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PartMesasTerritoriales != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PartMesasTerritoriales", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PartMesasTerritoriales);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PartAsocAgremia != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PartAsocAgremia", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PartAsocAgremia);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PartConsejosComun != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PartConsejosComun", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PartConsejosComun);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ConvInterInst != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ConvInterInst", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ConvInterInst);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccGral != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ProcSeleccGral", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccGral);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccEtnico != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ProcSeleccEtnico", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccEtnico);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PlanInduccCapac != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PlanInduccCapac", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PlanInduccCapac);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.EvalDesemp != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@EvalDesemp", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.EvalDesemp);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PlanCualificacion != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PlanCualificacion", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PlanCualificacion);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.NumSedes != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NumSedes", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NumSedes);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.SedesPropias != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@SedesPropias", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.SedesPropias);
                        }
                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String,
                                            pEntidadProvOferente.InfoAdminEntidadProv.UsuarioModifica);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);
                        GenerarLogAuditoria(pEntidadProvOferente.InfoAdminEntidadProv, vDbCommand2);
                        vDbTransaction.Commit();
                        vDBConnection.Close();
                    }
                    return vResultado;
                }


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEntidadProvOferente(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pEntidadProvOferente.IdEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>Entidad Proveedores Oferente</returns>
        public Icbf.Proveedor.Entity.EntidadProvOferente ConsultarEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
                        vEntidadProvOferente.InfoAdminEntidadProv = new Entity.InfoAdminEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vEntidadProvOferente.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vEntidadProvOferente.IdEntidad;
                            vEntidadProvOferente.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vEntidadProvOferente.ConsecutivoInterno;
                            vEntidadProvOferente.TipoEntOfProv = vDataReaderResults["TipoEntOfProv"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoEntOfProv"].ToString()) : vEntidadProvOferente.TipoEntOfProv;
                            vEntidadProvOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.IdTipoCiiuPrincipal = vDataReaderResults["IdTipoCiiuPrincipal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCiiuPrincipal"].ToString()) : vEntidadProvOferente.IdTipoCiiuPrincipal;
                            vEntidadProvOferente.IdTipoCiiuSecundario = vDataReaderResults["IdTipoCiiuSecundario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCiiuSecundario"].ToString()) : vEntidadProvOferente.IdTipoCiiuSecundario;
                            vEntidadProvOferente.IdTipoSector = vDataReaderResults["IdTipoSector"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSector"].ToString()) : vEntidadProvOferente.IdTipoSector;
                            vEntidadProvOferente.IdTipoClaseEntidad = vDataReaderResults["IdTipoClaseEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoClaseEntidad"].ToString()) : vEntidadProvOferente.IdTipoClaseEntidad;
                            vEntidadProvOferente.IdTipoRamaPublica = vDataReaderResults["IdTipoRamaPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRamaPublica"].ToString()) : vEntidadProvOferente.IdTipoRamaPublica;
                            vEntidadProvOferente.IdTipoNivelGob = vDataReaderResults["IdTipoNivelGob"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNivelGob"].ToString()) : vEntidadProvOferente.IdTipoNivelGob;
                            vEntidadProvOferente.IdTipoNivelOrganizacional = vDataReaderResults["IdTipoNivelOrganizacional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNivelOrganizacional"].ToString()) : vEntidadProvOferente.IdTipoNivelOrganizacional;
                            vEntidadProvOferente.IdTipoCertificadorCalidad = vDataReaderResults["IdTipoCertificadorCalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCertificadorCalidad"].ToString()) : vEntidadProvOferente.IdTipoCertificadorCalidad;
                            vEntidadProvOferente.FechaCiiuPrincipal = vDataReaderResults["FechaCiiuPrincipal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCiiuPrincipal"].ToString()) : vEntidadProvOferente.FechaCiiuPrincipal;
                            vEntidadProvOferente.FechaCiiuSecundario = vDataReaderResults["FechaCiiuSecundario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCiiuSecundario"].ToString()) : vEntidadProvOferente.FechaCiiuSecundario;
                            vEntidadProvOferente.FechaConstitucion = vDataReaderResults["FechaConstitucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaConstitucion"].ToString()) : vEntidadProvOferente.FechaConstitucion;
                            vEntidadProvOferente.FechaVigencia = vDataReaderResults["FechaVigencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigencia"].ToString()) : vEntidadProvOferente.FechaVigencia;
                            vEntidadProvOferente.FechaMatriculaMerc = vDataReaderResults["FechaMatriculaMerc"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaMatriculaMerc"].ToString()) : vEntidadProvOferente.FechaMatriculaMerc;
                            vEntidadProvOferente.FechaExpiracion = vDataReaderResults["FechaExpiracion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpiracion"].ToString()) : vEntidadProvOferente.FechaExpiracion;
                            vEntidadProvOferente.TipoVigencia = vDataReaderResults["TipoVigencia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoVigencia"].ToString()) : vEntidadProvOferente.TipoVigencia;
                            vEntidadProvOferente.ExenMatriculaMer = vDataReaderResults["ExenMatriculaMer"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ExenMatriculaMer"].ToString()) : vEntidadProvOferente.ExenMatriculaMer;
                            vEntidadProvOferente.MatriculaMercantil = vDataReaderResults["MatriculaMercantil"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaMercantil"].ToString()) : vEntidadProvOferente.MatriculaMercantil;
                            vEntidadProvOferente.ObserValidador = vDataReaderResults["ObserValidador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObserValidador"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.AnoRegistro = vDataReaderResults["AnoRegistro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnoRegistro"].ToString()) : vEntidadProvOferente.AnoRegistro;
                            vEntidadProvOferente.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vEntidadProvOferente.IdEstado;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                            vEntidadProvOferente.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vEntidadProvOferente.Finalizado;
                            vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad;
                            vEntidadProvOferente.Estado = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEntidadProvOferente.Descripcion;
                            vEntidadProvOferente.IdEstadoProveedor = vDataReaderResults["IdEstadoProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProveedor"].ToString()) : vEntidadProvOferente.IdEstadoProveedor;
                            vEntidadProvOferente.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vEntidadProvOferente.IdTipoPersona;
                            vEntidadProvOferente.DescEstadoProv = vDataReaderResults["DescEstadoProv"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescEstadoProv"].ToString()) : vEntidadProvOferente.DescEstadoProv;

                            vEntidadProvOferente.FechaIngresoICBF = vDataReaderResults["FechaIngresoICBF"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaIngresoICBF"].ToString()) : vEntidadProvOferente.FechaIngresoICBF;


                            //
                            vEntidadProvOferente.NumIntegrantes = vDataReaderResults["NumIntegrantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumIntegrantes"].ToString()) : vEntidadProvOferente.NumIntegrantes;
                            //DescEstadoProv
                            vEntidadProvOferente.OferenteMigrado = vDataReaderResults["OferentesMigrados"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["OferentesMigrados"].ToString()) : vEntidadProvOferente.OferenteMigrado;
                        }
                        return vEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEntidadProvOferentes()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferentes_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Icbf.Proveedor.Entity.EntidadProvOferente> vListaEntidadProvOferente = new List<Icbf.Proveedor.Entity.EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
                            vEntidadProvOferente.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vEntidadProvOferente.IdEntidad;
                            vEntidadProvOferente.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vEntidadProvOferente.ConsecutivoInterno;
                            vEntidadProvOferente.TipoEntOfProv = vDataReaderResults["TipoEntOfProv"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoEntOfProv"].ToString()) : vEntidadProvOferente.TipoEntOfProv;
                            vEntidadProvOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.IdTipoCiiuPrincipal = vDataReaderResults["IdTipoCiiuPrincipal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCiiuPrincipal"].ToString()) : vEntidadProvOferente.IdTipoCiiuPrincipal;
                            vEntidadProvOferente.IdTipoCiiuSecundario = vDataReaderResults["IdTipoCiiuSecundario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCiiuSecundario"].ToString()) : vEntidadProvOferente.IdTipoCiiuSecundario;
                            vEntidadProvOferente.IdTipoSector = vDataReaderResults["IdTipoSector"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSector"].ToString()) : vEntidadProvOferente.IdTipoSector;
                            vEntidadProvOferente.IdTipoClaseEntidad = vDataReaderResults["IdTipoClaseEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoClaseEntidad"].ToString()) : vEntidadProvOferente.IdTipoClaseEntidad;
                            vEntidadProvOferente.IdTipoRamaPublica = vDataReaderResults["IdTipoRamaPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRamaPublica"].ToString()) : vEntidadProvOferente.IdTipoRamaPublica;
                            vEntidadProvOferente.IdTipoNivelGob = vDataReaderResults["IdTipoNivelGob"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNivelGob"].ToString()) : vEntidadProvOferente.IdTipoNivelGob;
                            vEntidadProvOferente.IdTipoNivelOrganizacional = vDataReaderResults["IdTipoNivelOrganizacional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNivelOrganizacional"].ToString()) : vEntidadProvOferente.IdTipoNivelOrganizacional;
                            vEntidadProvOferente.IdTipoCertificadorCalidad = vDataReaderResults["IdTipoCertificadorCalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCertificadorCalidad"].ToString()) : vEntidadProvOferente.IdTipoCertificadorCalidad;
                            vEntidadProvOferente.FechaCiiuPrincipal = vDataReaderResults["FechaCiiuPrincipal"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCiiuPrincipal"].ToString()) : vEntidadProvOferente.FechaCiiuPrincipal;
                            vEntidadProvOferente.FechaCiiuSecundario = vDataReaderResults["FechaCiiuSecundario"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCiiuSecundario"].ToString()) : vEntidadProvOferente.FechaCiiuSecundario;
                            vEntidadProvOferente.FechaConstitucion = vDataReaderResults["FechaConstitucion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaConstitucion"].ToString()) : vEntidadProvOferente.FechaConstitucion;
                            vEntidadProvOferente.FechaVigencia = vDataReaderResults["FechaVigencia"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaVigencia"].ToString()) : vEntidadProvOferente.FechaVigencia;
                            vEntidadProvOferente.FechaMatriculaMerc = vDataReaderResults["FechaMatriculaMerc"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaMatriculaMerc"].ToString()) : vEntidadProvOferente.FechaMatriculaMerc;
                            vEntidadProvOferente.FechaExpiracion = vDataReaderResults["FechaExpiracion"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaExpiracion"].ToString()) : vEntidadProvOferente.FechaExpiracion;
                            vEntidadProvOferente.TipoVigencia = vDataReaderResults["TipoVigencia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoVigencia"].ToString()) : vEntidadProvOferente.TipoVigencia;
                            vEntidadProvOferente.ExenMatriculaMer = vDataReaderResults["ExenMatriculaMer"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ExenMatriculaMer"].ToString()) : vEntidadProvOferente.ExenMatriculaMer;
                            vEntidadProvOferente.MatriculaMercantil = vDataReaderResults["MatriculaMercantil"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MatriculaMercantil"].ToString()) : vEntidadProvOferente.MatriculaMercantil;
                            vEntidadProvOferente.ObserValidador = vDataReaderResults["ObserValidador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObserValidador"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.AnoRegistro = vDataReaderResults["AnoRegistro"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["AnoRegistro"].ToString()) : vEntidadProvOferente.AnoRegistro;
                            vEntidadProvOferente.IdEstado = vDataReaderResults["IdEstado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstado"].ToString()) : vEntidadProvOferente.IdEstado;
                            vEntidadProvOferente.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEntidadProvOferente.UsuarioCrea;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEntidadProvOferente.UsuarioModifica;
                            vEntidadProvOferente.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEntidadProvOferente.FechaModifica;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pTipoidentificacion">Tipo de identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pIdentificacion">Identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor">Proveedor en una entidad Proveedores Oferente</param>
        /// <param name="pEstado">Estado en una entidad Proveedores Oferente</param>
        /// <param name="pUsuarioCrea">Usuario que creaci�n para una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEntidadProvOferentes(String pTipoPersona, String pTipoidentificacion, String pIdentificacion, String pProveedor, String pEstado, String pUsuarioCrea)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Consultar"))
                {

                    if (!string.IsNullOrEmpty(pTipoPersona))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, Convert.ToInt32(pTipoPersona));
                    if (!string.IsNullOrEmpty(pTipoidentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.Int32, Convert.ToInt32(pTipoidentificacion));
                    if (!string.IsNullOrEmpty(pIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pIdentificacion);
                    if (!string.IsNullOrEmpty(pProveedor))
                        vDataBase.AddInParameter(vDbCommand, "@Proveedor", DbType.String, pProveedor);
                    if (!string.IsNullOrEmpty(pEstado))
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, Convert.ToInt32(pEstado));
                    if (!string.IsNullOrEmpty(pUsuarioCrea))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaEntidadProvOferente = new List<Icbf.Proveedor.Entity.EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            var vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();

                            vEntidadProvOferente.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vEntidadProvOferente.IdEntidad;
                            vEntidadProvOferente.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vEntidadProvOferente.ConsecutivoInterno;
                            vEntidadProvOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.ObserValidador = vDataReaderResults["ObserValidador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObserValidador"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.Proveedor = vDataReaderResults["Razonsocila"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocila"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.TipoPersonaNombre = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.EstadoProveedor = vDataReaderResults["EstadoProveedor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoProveedor"].ToString()) : vEntidadProvOferente.EstadoProveedor;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pIdTercero">Id Tercero</param>
        /// <returns>Lista de sucursa�es asociadas a los Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarSucursales_EntidadProvOferentes(int? pIdTercero, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales"))
                {
                    if (pIdTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaEntidadProvOferente = new List<Icbf.Proveedor.Entity.EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            var vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();

                            vEntidadProvOferente.IdSucursal = vDataReaderResults["IdSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursal"].ToString()) : vEntidadProvOferente.IdSucursal;
                            vEntidadProvOferente.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vEntidadProvOferente.IdDepartamento;
                            vEntidadProvOferente.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vEntidadProvOferente.NombreDepartamento;
                            vEntidadProvOferente.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vEntidadProvOferente.IdMunicipio;
                            vEntidadProvOferente.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vEntidadProvOferente.NombreMunicipio;
                            vEntidadProvOferente.NombreSucursal = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vEntidadProvOferente.NombreSucursal;
                            vEntidadProvOferente.DireccionSucursal = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vEntidadProvOferente.DireccionSucursal;
                            vEntidadProvOferente.CorreoSucursal = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"].ToString()) : vEntidadProvOferente.CorreoSucursal;
                            vEntidadProvOferente.IndicativoSucursal = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vEntidadProvOferente.IndicativoSucursal;
                            vEntidadProvOferente.TelefonoSucursal = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vEntidadProvOferente.TelefonoSucursal;
                            vEntidadProvOferente.ExtensionSucursal = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vEntidadProvOferente.ExtensionSucursal;
                            vEntidadProvOferente.CelularSucursal = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vEntidadProvOferente.CelularSucursal;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente Municipio
        /// </summary>
        /// <param name="pIdTercero">Id Tercero</param>
        /// <returns>Lista de sucursa�es asociadas a los Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarSucursales_EntidadProvOferentes_Municipio(int? pIdTercero, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_EntidadProvOferente_ConsultarSucursales_Municipio"))
                {
                    if (pIdTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if (pIdDepartamento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaEntidadProvOferente = new List<Icbf.Proveedor.Entity.EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            var vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();

                            //vEntidadProvOferente.IdSucursal = vDataReaderResults["IdSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursal"].ToString()) : vEntidadProvOferente.IdSucursal;
                            //vEntidadProvOferente.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vEntidadProvOferente.IdDepartamento;
                            //vEntidadProvOferente.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vEntidadProvOferente.NombreDepartamento;
                            vEntidadProvOferente.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vEntidadProvOferente.IdMunicipio;
                            vEntidadProvOferente.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vEntidadProvOferente.NombreMunicipio;
                            //vEntidadProvOferente.NombreSucursal = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vEntidadProvOferente.NombreSucursal;
                            //vEntidadProvOferente.DireccionSucursal = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vEntidadProvOferente.DireccionSucursal;
                            //vEntidadProvOferente.CorreoSucursal = vDataReaderResults["Correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Correo"].ToString()) : vEntidadProvOferente.CorreoSucursal;
                            //vEntidadProvOferente.IndicativoSucursal = vDataReaderResults["Indicativo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Indicativo"].ToString()) : vEntidadProvOferente.IndicativoSucursal;
                            //vEntidadProvOferente.TelefonoSucursal = vDataReaderResults["Telefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Telefono"].ToString()) : vEntidadProvOferente.TelefonoSucursal;
                            //vEntidadProvOferente.ExtensionSucursal = vDataReaderResults["Extension"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Extension"].ToString()) : vEntidadProvOferente.ExtensionSucursal;
                            //vEntidadProvOferente.CelularSucursal = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vEntidadProvOferente.CelularSucursal;
                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pIdEstado">Identificador del Estado en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pIDTIPODOCIDENTIFICA">Identificador del tipo de documento de identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pNUMEROIDENTIFICACION">N�mero de identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoCiiuPrincipal">Identificador del tipo de Ciiu principal en una entidad Proveedores Oferente</param>
        /// <param name="pIdMunicipioDirComercial">Identificador del municipio en una entidad Proveedores Oferente</param>
        /// <param name="pIdDepartamentoDirComercial">Identificador del departamento en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoSector">Identificador del tiepo de sector en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoRegimenTributario">Identificador del tipo de r�gimen tributario en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor"> en una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEntidadProvOferentes_Validar(String pIdEstado, String pIdTipoPersona,
            String pIDTIPODOCIDENTIFICA, String pNUMEROIDENTIFICACION, String pIdTipoCiiuPrincipal, String pIdMunicipioDirComercial,
            String pIdDepartamentoDirComercial, String pIdTipoSector, String pIdTipoRegimenTributario, String pProveedor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar"))
                {
                    if (!string.IsNullOrEmpty(pIdEstado))
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pIdEstado);
                    if (!string.IsNullOrEmpty(pIdTipoPersona))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    if (!string.IsNullOrEmpty(pIDTIPODOCIDENTIFICA))
                        vDataBase.AddInParameter(vDbCommand, "@IDTIPODOCIDENTIFICA", DbType.Int32, pIDTIPODOCIDENTIFICA);
                    if (!string.IsNullOrEmpty(pNUMEROIDENTIFICACION))
                        vDataBase.AddInParameter(vDbCommand, "@NUMEROIDENTIFICACION", DbType.String, pNUMEROIDENTIFICACION);
                    if (!string.IsNullOrEmpty(pIdTipoCiiuPrincipal))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiuPrincipal", DbType.Int32, pIdTipoCiiuPrincipal);
                    if (!string.IsNullOrEmpty(pIdMunicipioDirComercial))
                        vDataBase.AddInParameter(vDbCommand, "@IdMunicipioDirComercial", DbType.Int32, pIdMunicipioDirComercial);
                    if (!string.IsNullOrEmpty(pIdDepartamentoDirComercial))
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamentoDirComercial", DbType.Int32, pIdDepartamentoDirComercial);
                    if (!string.IsNullOrEmpty(pIdTipoSector))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, pIdTipoSector);
                    if (!string.IsNullOrEmpty(pIdTipoRegimenTributario))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pIdTipoRegimenTributario);
                    if (!string.IsNullOrEmpty(pProveedor))
                        vDataBase.AddInParameter(vDbCommand, "@Proveedor", DbType.String, pProveedor);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        var vListaEntidadProvOferente = new List<Icbf.Proveedor.Entity.EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            var vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();

                            vEntidadProvOferente.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vEntidadProvOferente.IdEntidad;
                            //vEntidadProvOferente.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vEntidadProvOferente.ConsecutivoInterno;
                            vEntidadProvOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.Proveedor = vDataReaderResults["Razonsocial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocial"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.TipoPersonaNombre = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vEntidadProvOferente.ObserValidador;

                            vEntidadProvOferente.ActividadCiiuPrincipal = vDataReaderResults["ActividadCiiuPrincipal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ActividadCiiuPrincipal"].ToString()) : vEntidadProvOferente.ActividadCiiuPrincipal;

                            vEntidadProvOferente.SectorEntidad = vDataReaderResults["SectorEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SectorEntidad"].ToString()) : vEntidadProvOferente.SectorEntidad;
                            vEntidadProvOferente.RegimenTributario = vDataReaderResults["RegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["RegimenTributario"].ToString()) : vEntidadProvOferente.RegimenTributario;

                            vEntidadProvOferente.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vEntidadProvOferente.NombreMunicipio;
                            vEntidadProvOferente.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vEntidadProvOferente.NombreDepartamento;
                            vEntidadProvOferente.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEntidadProvOferente.FechaCrea;
                            vEntidadProvOferente.EstadoProveedor = vDataReaderResults["EstadoProveedor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoProveedor"].ToString()) : vEntidadProvOferente.EstadoProveedor;

                            vListaEntidadProvOferente.Add(vEntidadProvOferente);
                        }
                        return vListaEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta una nueva entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Identificador de la entidad Proveedores Oferente en tabla</returns>
        public int InsertarEntidadProvOferente(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDbConnection = null;
            int vResultado;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Insertar"))
                {
                    vDbConnection = vDataBase.CreateConnection();
                    vDbConnection.Open();
                    vDbTransaction = vDbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    string usuario = "";

                    #region Datos Telefono Proveedor

                    string spProveedoTelTercero = "";
                    Int32 idTelTercero = 0;
                    //if (pEntidadProvOferente.TelTerceroProveedor.IdTercero == 0)   modificado por Mauricio Martinez 2013/07/10 07:00 AM
                    if (pEntidadProvOferente.TelTerceroProveedor.IdTelTercero == 0)
                    {
                        spProveedoTelTercero = "usp_RubOnline_Oferente_TelTerceros_Insertar";
                        usuario = "@UsuarioCrea";
                    }
                    else
                    {
                        spProveedoTelTercero = "usp_RubOnline_Oferente_TelTerceros_Modificar";
                        idTelTercero = pEntidadProvOferente.TelTerceroProveedor.IdTelTercero;
                        usuario = "@UsuarioModifica";
                    }
                    using (DbCommand vDbCommand4 = vDataBase.GetStoredProcCommand(spProveedoTelTercero))
                    {
                        if (idTelTercero == 0)
                        {
                            vDataBase.AddOutParameter(vDbCommand4, "@IdTelTercero", DbType.Int32, idTelTercero);
                        }
                        else
                        {
                            vDataBase.AddInParameter(vDbCommand4, "@IdTelTercero", DbType.Int32, idTelTercero);
                        }

                        vDataBase.AddInParameter(vDbCommand4, "@IdTercero", DbType.Int32, pEntidadProvOferente.TerceroProveedor.IdTercero);
                        vDataBase.AddInParameter(vDbCommand4, "@IndicativoTelefono", DbType.Int32, pEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono);
                        vDataBase.AddInParameter(vDbCommand4, "@NumeroTelefono", DbType.Int32, pEntidadProvOferente.TelTerceroProveedor.NumeroTelefono);
                        vDataBase.AddInParameter(vDbCommand4, "@ExtensionTelefono", DbType.Int64, pEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono);
                        vDataBase.AddInParameter(vDbCommand4, "@Movil", DbType.Int64, pEntidadProvOferente.TelTerceroProveedor.Movil);
                        if (pEntidadProvOferente.TelTerceroProveedor.IndicativoFax != null)
                        {
                            vDataBase.AddInParameter(vDbCommand4, "@IndicativoFax", DbType.Int32, pEntidadProvOferente.TelTerceroProveedor.IndicativoFax);
                        }
                        if (pEntidadProvOferente.TelTerceroProveedor.NumeroFax != null)
                        {
                            vDataBase.AddInParameter(vDbCommand4, "@NumeroFax", DbType.Int32, pEntidadProvOferente.TelTerceroProveedor.NumeroFax);
                        }
                        vDataBase.AddInParameter(vDbCommand4, usuario, DbType.String, pEntidadProvOferente.TelTerceroProveedor.UsuarioCrea);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand4, vDbTransaction);
                        if (idTelTercero == 0)
                        {
                            pEntidadProvOferente.TelTerceroProveedor.IdTelTercero =
                                Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand4, "@IdTelTercero").ToString());
                        }
                        GenerarLogAuditoria(pEntidadProvOferente.TelTerceroProveedor, vDbCommand4);

                    }
                    #endregion

                    #region Adiciona o actualiza Representante Legal
                    if (pEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 || pEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || pEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                    {
                        string spRepTercero = "";
                        Int32 idReplegaTercero = 0;
                        if (pEntidadProvOferente.RepresentanteLegal.IdTercero == 0)
                        {
                            spRepTercero = "usp_RubOnline_Oferente_RepresentanteLegal_Insertar";
                            usuario = "@UsuarioCrea";

                           

                        }
                        else
                        {
                            spRepTercero = "usp_RubOnline_Oferente_Tercero_Modificar";
                            idReplegaTercero = pEntidadProvOferente.RepresentanteLegal.IdTercero;
                            usuario = "@UsuarioModifica";
                        }

                        // Se ordenan los parametros de acuerdo al orden del SP y se agregan: identidad,razonsocial,digitoverificacion,FechaExpedicionId
                        // ticket 12338 05/10/2016 Zulma.Barrantes

                        using (DbCommand vDbCommand5 = vDataBase.GetStoredProcCommand(spRepTercero))
                        {
                            if (idReplegaTercero == 0)
                            {
                                vDataBase.AddOutParameter(vDbCommand5, "@IdTercero", DbType.Int32, idReplegaTercero);
                            }
                            else
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IdTercero", DbType.Int32, idReplegaTercero);
                            }

                            //identidad
                            if (pEntidadProvOferente.RepresentanteLegal.IDENTIDAD != -1 && usuario == "@UsuarioCrea")
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IDENTIDAD", DbType.Int32, pEntidadProvOferente.RepresentanteLegal.IDENTIDAD);
                            }
                            //

                            if (pEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento != -1)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IdDListaTipoDocumento", DbType.Int32, pEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@NumeroIdentificacion", DbType.String, pEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.PrimerNombre))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@PrimerNombre", DbType.String, pEntidadProvOferente.RepresentanteLegal.PrimerNombre.Trim());
                            }
                            if (pEntidadProvOferente.RepresentanteLegal.IdTipoPersona != null && pEntidadProvOferente.RepresentanteLegal.IdTipoPersona != -1)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@IdTipoPersona", DbType.String, pEntidadProvOferente.RepresentanteLegal.IdTipoPersona);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.SegundoNombre))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@SegundoNombre", DbType.String, pEntidadProvOferente.RepresentanteLegal.SegundoNombre.Trim());
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.PrimerApellido))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@PrimerApellido", DbType.String, pEntidadProvOferente.RepresentanteLegal.PrimerApellido.Trim());
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.SegundoApellido))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@SegundoApellido", DbType.String, pEntidadProvOferente.RepresentanteLegal.SegundoApellido.Trim());
                            }
                            //razon social
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.RazonSocial))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@RazonSocial", DbType.String, pEntidadProvOferente.RepresentanteLegal.RazonSocial.Trim());
                            }
                            //digitoverificacion
                            if (pEntidadProvOferente.RepresentanteLegal.DigitoVerificacion != -1)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@DigitoVerificacion", DbType.Int32, pEntidadProvOferente.RepresentanteLegal.DigitoVerificacion);
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.Email))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@Email", DbType.String, pEntidadProvOferente.RepresentanteLegal.Email.Trim());
                            }
                            if (!string.IsNullOrEmpty(pEntidadProvOferente.RepresentanteLegal.Sexo))
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@Sexo", DbType.String, pEntidadProvOferente.RepresentanteLegal.Sexo);
                            }
                            //fechaexpedicionid
                            if (pEntidadProvOferente.RepresentanteLegal.FechaExpedicionId != null)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@FechaExpedicionId", DbType.DateTime, pEntidadProvOferente.RepresentanteLegal.FechaExpedicionId);
                            }
                            if (pEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                            {
                                vDataBase.AddInParameter(vDbCommand5, "@FechaNacimiento", DbType.DateTime, pEntidadProvOferente.RepresentanteLegal.FechaNacimiento);
                            }
                            vDataBase.AddInParameter(vDbCommand5, "@CreadoPorInterno", DbType.Boolean, pEntidadProvOferente.TerceroProveedor.CreadoPorInterno);

                            vDataBase.AddInParameter(vDbCommand5, usuario, DbType.String, pEntidadProvOferente.RepresentanteLegal.UsuarioCrea);
                            vDataBase.ExecuteNonQuery(vDbCommand5, vDbTransaction);
                            if (idReplegaTercero == 0)
                            {
                                pEntidadProvOferente.RepresentanteLegal.IdTercero =
                                    Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand5, "@IdTercero").ToString());
                            }
                            GenerarLogAuditoria(pEntidadProvOferente.RepresentanteLegal, vDbCommand5);

                        }
                        string spRepreTelTercero = "";
                        Int32 idTelRepreTercero = 0;
                        if (pEntidadProvOferente.TelRepresentanteLegal.IdTelTercero == 0)
                        {
                            spRepreTelTercero = "usp_RubOnline_Oferente_TelTerceros_Insertar";
                            usuario = "@UsuarioCrea";
                        }
                        else
                        {
                            spRepreTelTercero = "usp_RubOnline_Oferente_TelTerceros_Modificar";
                            idTelRepreTercero = pEntidadProvOferente.TelRepresentanteLegal.IdTelTercero;
                            usuario = "@UsuarioModifica";
                        }
                        using (DbCommand vDbCommand6 = vDataBase.GetStoredProcCommand(spRepreTelTercero))
                        {
                            if (idTelRepreTercero == 0)
                            {
                                vDataBase.AddOutParameter(vDbCommand6, "@IdTelTercero", DbType.Int32, idTelRepreTercero);
                            }
                            else
                            {
                                vDataBase.AddInParameter(vDbCommand6, "@IdTelTercero", DbType.Int32, idTelRepreTercero);
                            }
                            vDataBase.AddInParameter(vDbCommand6, "@IdTercero", DbType.Int32, pEntidadProvOferente.RepresentanteLegal.IdTercero);
                            vDataBase.AddInParameter(vDbCommand6, "@IndicativoTelefono", DbType.Int32, pEntidadProvOferente.TelRepresentanteLegal.IndicativoTelefono);
                            vDataBase.AddInParameter(vDbCommand6, "@NumeroTelefono", DbType.Int32, pEntidadProvOferente.TelRepresentanteLegal.NumeroTelefono);
                            vDataBase.AddInParameter(vDbCommand6, "@ExtensionTelefono", DbType.Int64, pEntidadProvOferente.TelRepresentanteLegal.ExtensionTelefono);
                            vDataBase.AddInParameter(vDbCommand6, "@Movil", DbType.Int64, pEntidadProvOferente.TelRepresentanteLegal.Movil);
                            if (pEntidadProvOferente.TelRepresentanteLegal.IndicativoFax != null && pEntidadProvOferente.TelRepresentanteLegal.IndicativoFax != null)
                            {
                                vDataBase.AddInParameter(vDbCommand6, "@IndicativoFax", DbType.Int32, pEntidadProvOferente.TelRepresentanteLegal.IndicativoFax);
                            }
                            if (pEntidadProvOferente.TelRepresentanteLegal.NumeroFax != null && pEntidadProvOferente.TelRepresentanteLegal.NumeroFax != null)
                            {
                                vDataBase.AddInParameter(vDbCommand6, "@NumeroFax", DbType.Int32, pEntidadProvOferente.TelRepresentanteLegal.NumeroFax);
                            }
                            vDataBase.AddInParameter(vDbCommand6, usuario, DbType.String, pEntidadProvOferente.TelRepresentanteLegal.UsuarioCrea);
                            vResultado = vDataBase.ExecuteNonQuery(vDbCommand6, vDbTransaction);
                            if (idTelRepreTercero == 0)
                            {
                                pEntidadProvOferente.TelRepresentanteLegal.IdTelTercero =
                                    Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand6, "@IdTelTercero").ToString());
                            }
                            GenerarLogAuditoria(pEntidadProvOferente.TelRepresentanteLegal, vDbCommand6);

                        }
                    }
                    #endregion

                    #region Adiciona EntidadProvOferente


                    vDataBase.AddOutParameter(vDbCommand, "@IdEntidad", DbType.Int32, 18);
                    if (pEntidadProvOferente.TipoEntOfProv != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@TipoEntOfProv", DbType.Boolean, pEntidadProvOferente.TipoEntOfProv);
                    }
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.ConsecutivoInterno))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ConsecutivoInterno", DbType.String, pEntidadProvOferente.ConsecutivoInterno);
                    }
                    if (pEntidadProvOferente.TerceroProveedor.IdTercero != 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pEntidadProvOferente.TerceroProveedor.IdTercero);
                    }
                    if (pEntidadProvOferente.IdTipoCiiuPrincipal != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiuPrincipal", DbType.Int32, pEntidadProvOferente.IdTipoCiiuPrincipal);
                    }
                    if (pEntidadProvOferente.IdTipoCiiuSecundario != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiuSecundario", DbType.Int32, pEntidadProvOferente.IdTipoCiiuSecundario);
                    }
                    if (pEntidadProvOferente.IdTipoSector != null)
                    {
                        if (pEntidadProvOferente.IdTipoSector == -1)
                            vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, null);
                        else
                            vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, pEntidadProvOferente.IdTipoSector);
                    }
                    if (pEntidadProvOferente.IdTipoClaseEntidad != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoClaseEntidad", DbType.Int32, pEntidadProvOferente.IdTipoClaseEntidad);
                    }
                    if (pEntidadProvOferente.IdTipoRamaPublica != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoRamaPublica", DbType.Int32, pEntidadProvOferente.IdTipoRamaPublica);
                    }
                    if (pEntidadProvOferente.IdTipoNivelGob != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoNivelGob", DbType.Int32, pEntidadProvOferente.IdTipoNivelGob);
                    }
                    if (pEntidadProvOferente.IdTipoNivelOrganizacional != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoNivelOrganizacional", DbType.Int32, pEntidadProvOferente.IdTipoNivelOrganizacional);
                    }
                    if (pEntidadProvOferente.IdTipoCertificadorCalidad != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoCertificadorCalidad", DbType.Int32, pEntidadProvOferente.IdTipoCertificadorCalidad);
                    }
                    if (pEntidadProvOferente.FechaCiiuPrincipal != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaCiiuPrincipal", DbType.DateTime, pEntidadProvOferente.FechaCiiuPrincipal);
                    }
                    if (pEntidadProvOferente.FechaCiiuSecundario != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaCiiuSecundario", DbType.DateTime, pEntidadProvOferente.FechaCiiuSecundario);
                    }
                    if (pEntidadProvOferente.FechaConstitucion != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaConstitucion", DbType.DateTime, pEntidadProvOferente.FechaConstitucion);
                    }
                    if (pEntidadProvOferente.FechaVigencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaVigencia", DbType.DateTime, pEntidadProvOferente.FechaVigencia);
                    }
                    if (pEntidadProvOferente.FechaMatriculaMerc != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaMatriculaMerc", DbType.DateTime, pEntidadProvOferente.FechaMatriculaMerc);
                    }
                    if (pEntidadProvOferente.FechaExpiracion != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@FechaExpiracion", DbType.DateTime, pEntidadProvOferente.FechaExpiracion);
                    }
                    if (pEntidadProvOferente.TipoVigencia != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@TipoVigencia", DbType.Boolean, pEntidadProvOferente.TipoVigencia);
                    }
                    if (pEntidadProvOferente.ExenMatriculaMer != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ExenMatriculaMer", DbType.Boolean, pEntidadProvOferente.ExenMatriculaMer);
                    }
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.MatriculaMercantil))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@MatriculaMercantil", DbType.String, pEntidadProvOferente.MatriculaMercantil.Trim());
                    }
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.ObserValidador))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ObserValidador", DbType.String, pEntidadProvOferente.ObserValidador.Trim());
                    }
                    if (pEntidadProvOferente.AnoRegistro != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@AnoRegistro", DbType.Int32, pEntidadProvOferente.AnoRegistro);
                    }
                    if (pEntidadProvOferente.IdEstado != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pEntidadProvOferente.IdEstado);
                    }
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.UsuarioCrea))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEntidadProvOferente.UsuarioCrea);
                    }

                    //
                    // Se agrega esta variable para el manejo de documentos Mauricio Martinez.
                    //
                    if (!string.IsNullOrEmpty(pEntidadProvOferente.IdTemporal))
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pEntidadProvOferente.IdTemporal);
                    }

                    if (pEntidadProvOferente.NumIntegrantes != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@NumIntegrantes", DbType.Int32, pEntidadProvOferente.NumIntegrantes);
                    }

                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pEntidadProvOferente.TerceroProveedor.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pEntidadProvOferente.TerceroProveedor.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pEntidadProvOferente.TerceroProveedor.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pEntidadProvOferente.TerceroProveedor.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@CorreoElectronico", DbType.String, pEntidadProvOferente.TerceroProveedor.Email);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pEntidadProvOferente.IdEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEntidad").ToString());
                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);

                    #endregion

                    #region Adiciona InfoAdminEntidad

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar"))
                    {

                        vDataBase.AddOutParameter(vDbCommand2, "@IdInfoAdmin", DbType.Int32, pEntidadProvOferente.InfoAdminEntidadProv.IdInfoAdmin);

                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdVigencia != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdVigencia", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdVigencia);
                        }
                        vDataBase.AddInParameter(vDbCommand2, "@IdEntidad", DbType.Int32, pEntidadProvOferente.IdEntidad);
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoRegTrib", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoOrigenCapital != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoOrigenCapital", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoOrigenCapital);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoActividad", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad);
                        }

                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoEntidad", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoNaturalezaJurid != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoNaturalezaJurid", DbType.Int32,
                                                         pEntidadProvOferente.InfoAdminEntidadProv.IdTipoNaturalezaJurid);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosTrabajadores != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoRangosTrabajadores", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosTrabajadores);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosActivos != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoRangosActivos", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoRangosActivos);
                        }

                        if (pEntidadProvOferente.RepresentanteLegal.IdTercero != 0 && pEntidadProvOferente.RepresentanteLegal.IdTercero != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdRepLegal", DbType.Int32, pEntidadProvOferente.RepresentanteLegal.IdTercero);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoCertificaTamano != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoCertificaTamano", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoCertificaTamano);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdTipoEntidadPublica", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdDepartamentoConstituida", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdMunicipioConstituida", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdDepartamentoDirComercial", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdMunicipioDirComercial", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@DireccionComercial", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.IdZona))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@IdZona", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.IdZona);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.NombreComercial))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NombreComercial", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NombreComercial.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NombreEstablecimiento", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.Sigla))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Sigla", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Sigla.Trim());
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PorctjPrivado != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PorctjPrivado", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PorctjPrivado);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PorctjPublico != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PorctjPublico", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PorctjPublico);
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.SitioWeb))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@SitioWeb", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.SitioWeb.Trim());
                        }
                        if (!string.IsNullOrEmpty(pEntidadProvOferente.InfoAdminEntidadProv.NombreEntidadAcreditadora))
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NombreEntidadAcreditadora", DbType.String,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NombreEntidadAcreditadora.Trim());
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Organigrama != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Organigrama", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Organigrama);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.TotalPnalAnnoPrevio != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@TotalPnalAnnoPrevio", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.TotalPnalAnnoPrevio);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.VincLaboral != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@VincLaboral", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.VincLaboral);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PrestServicios != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PrestServicios", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PrestServicios);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Voluntariado != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Voluntariado", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Voluntariado);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.VoluntPermanente != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@VoluntPermanente", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.VoluntPermanente);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Asociados != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Asociados", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Asociados);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.Mision != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@Mision", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.Mision);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PQRS != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PQRS", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PQRS);
                        }
                        vDataBase.AddInParameter(vDbCommand2, "@GestionDocumental", DbType.Boolean,
                                                 pEntidadProvOferente.InfoAdminEntidadProv.GestionDocumental);
                        if (pEntidadProvOferente.InfoAdminEntidadProv.AuditoriaInterna != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@AuditoriaInterna", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.AuditoriaInterna);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManProcedimiento != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManProcedimiento", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManProcedimiento);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManPracticasAmbiente != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManPracticasAmbiente", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManPracticasAmbiente);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManComportOrg != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManComportOrg", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManComportOrg);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ManFunciones != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ManFunciones", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ManFunciones);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ProcRegInfoContable != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ProcRegInfoContable", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ProcRegInfoContable);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PartMesasTerritoriales != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PartMesasTerritoriales", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PartMesasTerritoriales);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PartAsocAgremia != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PartAsocAgremia", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PartAsocAgremia);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PartConsejosComun != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PartConsejosComun", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PartConsejosComun);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ConvInterInst != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ConvInterInst", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ConvInterInst);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccGral != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ProcSeleccGral", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccGral);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccEtnico != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@ProcSeleccEtnico", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.ProcSeleccEtnico);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PlanInduccCapac != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PlanInduccCapac", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PlanInduccCapac);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.EvalDesemp != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@EvalDesemp", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.EvalDesemp);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.PlanCualificacion != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@PlanCualificacion", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.PlanCualificacion);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.NumSedes != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@NumSedes", DbType.Int32,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.NumSedes);
                        }
                        if (pEntidadProvOferente.InfoAdminEntidadProv.SedesPropias != null)
                        {
                            vDataBase.AddInParameter(vDbCommand2, "@SedesPropias", DbType.Boolean,
                                                     pEntidadProvOferente.InfoAdminEntidadProv.SedesPropias);
                        }
                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioCrea", DbType.String, pEntidadProvOferente.InfoAdminEntidadProv.UsuarioCrea);
                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);
                        GenerarLogAuditoria(pEntidadProvOferente.InfoAdminEntidadProv, vDbCommand2);

                        #endregion
                        vDbTransaction.Commit();
                        vDbConnection.Close();
                        return 1;
                    }

                }


            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDbConnection.Close();
                //return 0;
                if (ex.Message.Contains("CorreoUnico")) throw new Exception("El correo del representante legal ya existe en terceros");
                if (ex.Message.Contains("NUMEROIDENTIFICACIONUnico")) throw new Exception("El n�mero de identificaci�n del representante legal ya existe en terceros");
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Identificador de la entidad Proveedores Oferente en tabla</returns>
        public int ModificarEntidadProvOfernte_EstadoDocumental(Icbf.Proveedor.Entity.EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Modificar_EstadoDocumental"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pEntidadProvOferente.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, pEntidadProvOferente.IdEstado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEntidadProvOferente.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pEntidadProvOferente.Finalizado);
                    // vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pEntidadProvOferente.IdTemporal);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEntidadProvOferente, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Env�a correo electr�nico por inconsistencia en una entidad Proveedores Oferente
        /// </summary>
        /// <param name="idEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EnviarMailsInconsistenciasProveedor(Int32 idEntidad, string pAsunto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@idEntidad", DbType.Int32, idEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Asunto", DbType.String, pAsunto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Estructura el correo electr�nico a enviar por inconsistencia en una entidad Proveedores Oferente
        /// </summary>
        /// <param name="idEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>String Mail Body HTML</returns>
        //public string[] EstructurarMailsInconsistenciasProveedor(Int32 idEntidad)
        //{
        //    try
        //    {
        //        Database vDataBase = ObtenerInstancia();
        //        using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstructurarCorreo_InconsistenciadelaInformacionIngresada"))
        //        {
        //            string mailBody = "";
        //            string emailDestino = "";
        //            string[] valoresCorreo = new string[2];
        //            vDataBase.AddInParameter(vDbCommand, "@idEntidad", DbType.Int32, idEntidad);

        //            using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
        //            {
        //                vDataReaderResults.Read();
        //                mailBody = vDataReaderResults["MailBody"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MailBody"].ToString()) : mailBody;
        //                emailDestino = vDataReaderResults["Email_Destino"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email_Destino"].ToString()) : emailDestino;
        //            }
        //            valoresCorreo[0] = mailBody;
        //            valoresCorreo[1] = emailDestino;
        //            return valoresCorreo;
        //        }
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        public int marcarCorreosEnviados_ComunicadoValidarProveedor(Int32 IdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EnviarCorreo_InconsistenciadelaInformacionIngresada"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@idEntidad", DbType.Int32, IdEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Proveedor
        /// </summary>
        /// <param name="pEstado">Estado en una entidad Estado Proveedor</param>
        /// <returns>Lista de entidades Estado Proveedor</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEstadoProveedor(Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_EstadoProveedor_Consultar"))
                {
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Icbf.Proveedor.Entity.EntidadProvOferente> vListaEstadoProveedor = new List<Icbf.Proveedor.Entity.EntidadProvOferente>();
                        while (vDataReaderResults.Read())
                        {
                            Icbf.Proveedor.Entity.EntidadProvOferente vEstadoProveedor = new Icbf.Proveedor.Entity.EntidadProvOferente();
                            vEstadoProveedor.IdEstadoProveedor = vDataReaderResults["IdEstadoProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProveedor"].ToString()) : vEstadoProveedor.IdEstadoProveedor;
                            vEstadoProveedor.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoProveedor.Descripcion;

                            vListaEstadoProveedor.Add(vEstadoProveedor);
                        }
                        return vListaEstadoProveedor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza EntidadProvOferente
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public int FinalizaEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_EntidadProvOferente_Finalizar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene la Fecha de Migraci�n de un Registro proveniente de Oferentes.
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public DateTime GetFechaMigracion(int pIdEntidad)
        {
            try
            {
                DateTime fechaMigracion = new DateTime();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_Fecha_Migracion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            fechaMigracion = Convert.ToDateTime(vDataReaderResults["Fecha"].ToString());

                        }
                    }
                }
                return fechaMigracion;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene la auditoria de acciones para una EntidadProvOferente
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public List<Icbf.Proveedor.Entity.AuditoriaAccionesEntidad> GetAuditoriaAccionesEntidad(int pIdEntidad)
        {
            try
            {
                List<Icbf.Proveedor.Entity.AuditoriaAccionesEntidad> listaAuditoria = new List<Entity.AuditoriaAccionesEntidad>();
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            Icbf.Proveedor.Entity.AuditoriaAccionesEntidad objAuditoria = new Entity.AuditoriaAccionesEntidad();
                            objAuditoria.IdAccion = vDataReaderResults["IdAccion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAccion"].ToString()) : objAuditoria.IdAccion;
                            objAuditoria.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : objAuditoria.IdEntidad;
                            objAuditoria.Fecha = vDataReaderResults["Fecha"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["Fecha"].ToString()) : objAuditoria.Fecha;
                            objAuditoria.Usuario = vDataReaderResults["Usuario"] != DBNull.Value ? vDataReaderResults["Usuario"].ToString() : objAuditoria.Usuario;
                            objAuditoria.IdSistema = vDataReaderResults["IdSistema"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSistema"].ToString()) : objAuditoria.IdSistema;
                            objAuditoria.Sistema = vDataReaderResults["Sistema"] != DBNull.Value ? vDataReaderResults["Sistema"].ToString() : objAuditoria.Sistema;
                            objAuditoria.Accion = vDataReaderResults["Accion"] != DBNull.Value ? vDataReaderResults["Accion"].ToString() : objAuditoria.Accion;

                            listaAuditoria.Add(objAuditoria);
                        }
                    }
                }
                return listaAuditoria;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idproveedorContrato"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarHistoricoRepresentanteLegal(int idproveedorContrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Oferente_Terceros_ConsultarHistoricoRepresentanteLegal"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdProveedoresContratos", DbType.Int32, idproveedorContrato);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {

                        List<Tercero> vListTercero = new List<Tercero>();
                        while (vDataReaderResults.Read())
                        {
                            Tercero vTercero = new Tercero();
                            vTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vTercero.IdTercero;
                            vTercero.IdDListaTipoDocumento = vDataReaderResults["IdTipoDocumentoIdentifcacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumentoIdentifcacion"].ToString()) : vTercero.IdDListaTipoDocumento;
                            vTercero.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTercero.IdTipoPersona;
                            vTercero.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vTercero.NumeroIdentificacion;
                            vTercero.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vTercero.PrimerNombre;
                            vTercero.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vTercero.SegundoNombre;
                            vTercero.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vTercero.PrimerApellido;
                            vTercero.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vTercero.SegundoApellido;
                            vTercero.Email = vDataReaderResults["CORREOELECTRONICO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CORREOELECTRONICO"].ToString()) : vTercero.Email;
                            vTercero.Sexo = vDataReaderResults["Sexo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sexo"].ToString()) : vTercero.Sexo;
                            vTercero.FechaNacimiento = vDataReaderResults["FechaNacimiento"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaNacimiento"].ToString()) : vTercero.FechaNacimiento;
                            vTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTercero.UsuarioCrea;
                            vTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTercero.FechaCrea;
                            vTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTercero.UsuarioModifica;
                            vTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTercero.FechaModifica;
                            vTercero.Nombre_Razonsocial = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " + vTercero.PrimerApellido + " " + vTercero.SegundoApellido;
                            vListTercero.Add(vTercero);
                        }
                        return vListTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
