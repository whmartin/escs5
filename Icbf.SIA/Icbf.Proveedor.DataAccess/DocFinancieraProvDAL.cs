using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Documentos adjuntos en la informaci�n financiera del proveedor
    /// </summary>
    public class DocFinancieraProvDAL : GeneralDAL
    {
        public DocFinancieraProvDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProv_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocFinancieraProv.IdDocAdjunto);
                    if (pDocFinancieraProv.IdInfoFin != 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pDocFinancieraProv.IdInfoFin);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pDocFinancieraProv.NombreDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocFinancieraProv.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pDocFinancieraProv.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocFinancieraProv.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pDocFinancieraProv.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pDocFinancieraProv.IdTemporal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDocFinancieraProv.IdDocAdjunto = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocAdjunto").ToString());
                    GenerarLogAuditoria(pDocFinancieraProv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProv_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocFinancieraProv.IdDocAdjunto);

                    if (pDocFinancieraProv.IdInfoFin != 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pDocFinancieraProv.IdInfoFin);
                    }

                    
                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pDocFinancieraProv.NombreDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocFinancieraProv.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pDocFinancieraProv.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocFinancieraProv.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocFinancieraProv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocFinancieraProv.IdDocAdjunto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocFinancieraProv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Entidad Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public DocFinancieraProv ConsultarDocFinancieraProv(int pIdDocAdjunto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProv_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pIdDocAdjunto);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocFinancieraProv vDocFinancieraProv = new DocFinancieraProv();
                        while (vDataReaderResults.Read())
                        {
                            vDocFinancieraProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocFinancieraProv.IdDocAdjunto;
                            vDocFinancieraProv.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vDocFinancieraProv.IdInfoFin;
                            vDocFinancieraProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocFinancieraProv.NombreDocumento;
                            vDocFinancieraProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocFinancieraProv.LinkDocumento;
                            vDocFinancieraProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocFinancieraProv.Observaciones;
                            vDocFinancieraProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocFinancieraProv.UsuarioCrea;
                            vDocFinancieraProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocFinancieraProv.FechaCrea;
                            vDocFinancieraProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocFinancieraProv.UsuarioModifica;
                            vDocFinancieraProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocFinancieraProv.FechaModifica;
                            vDocFinancieraProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocFinancieraProv.IdTemporal;
                        }
                        return vDocFinancieraProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la informaci�n financiera en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProvs(int pIdInfoFin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar"))
                {
                    if (pIdInfoFin != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int16, pIdInfoFin);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocFinancieraProv> vListaDocFinancieraProv = new List<DocFinancieraProv>();
                        while (vDataReaderResults.Read())
                        {
                            DocFinancieraProv vDocFinancieraProv = new DocFinancieraProv();
                            vDocFinancieraProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocFinancieraProv.IdDocAdjunto;
                            vDocFinancieraProv.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vDocFinancieraProv.IdInfoFin;
                            vDocFinancieraProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocFinancieraProv.NombreDocumento;
                            vDocFinancieraProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocFinancieraProv.LinkDocumento;
                            vDocFinancieraProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocFinancieraProv.Observaciones;
                            vDocFinancieraProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocFinancieraProv.UsuarioCrea;
                            vDocFinancieraProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocFinancieraProv.FechaCrea;
                            vDocFinancieraProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocFinancieraProv.UsuarioModifica;
                            vDocFinancieraProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocFinancieraProv.FechaModifica;
                            vDocFinancieraProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocFinancieraProv.IdTemporal;
                            vListaDocFinancieraProv.Add(vDocFinancieraProv);
                        }
                        return vListaDocFinancieraProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la informaci�n financiera en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pRupRenovado">Rup renovado en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProv_IdInfoFin_Rup(int pIdInfoFin, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdInfoFin_Rup"))
                {
                    if(pIdInfoFin != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int16 , pIdInfoFin );
                    if (pRupRenovado != null)
                        vDataBase.AddInParameter(vDbCommand, "@RupRenovado", DbType.String, pRupRenovado);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);

                    if (pTipoSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoSector", DbType.String, pTipoSector);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocFinancieraProv> vListaDocFinancieraProv = new List<DocFinancieraProv>();
                        while (vDataReaderResults.Read())
                        {
                                DocFinancieraProv vDocFinancieraProv = new DocFinancieraProv();
                            vDocFinancieraProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocFinancieraProv.IdDocAdjunto;
                            vDocFinancieraProv.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vDocFinancieraProv.IdInfoFin;
                            vDocFinancieraProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocFinancieraProv.NombreDocumento;
                            vDocFinancieraProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocFinancieraProv.LinkDocumento;
                            vDocFinancieraProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocFinancieraProv.Observaciones;
                            vDocFinancieraProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocFinancieraProv.UsuarioCrea;
                            vDocFinancieraProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocFinancieraProv.FechaCrea;
                            vDocFinancieraProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocFinancieraProv.UsuarioModifica;
                            vDocFinancieraProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocFinancieraProv.FechaModifica;
                            vDocFinancieraProv.Obligatorio  = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDocFinancieraProv.Obligatorio;
                            
                            vDocFinancieraProv.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocFinancieraProv.IdTipoDocumento;
                            vDocFinancieraProv.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocFinancieraProv.MaxPermitidoKB;
                            vDocFinancieraProv.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocFinancieraProv.ExtensionesPermitidas;
                            vDocFinancieraProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocFinancieraProv.IdTemporal;
                            vListaDocFinancieraProv.Add(vDocFinancieraProv);
                        }
                        return vListaDocFinancieraProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pRupRenovado">Rup renovado en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProv_IdTemporal_Rup(string pIdTemporal, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocFinancieraProv_Consultar_IdTemporal_Rup"))
                {
                    if (pIdTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pIdTemporal );
                    if (pRupRenovado != null)
                        vDataBase.AddInParameter(vDbCommand, "@RupRenovado", DbType.String, pRupRenovado);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);
                    if (pTipoSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoSector", DbType.String, pTipoSector);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocFinancieraProv> vListaDocFinancieraProv = new List<DocFinancieraProv>();
                        while (vDataReaderResults.Read())
                        {
                            DocFinancieraProv vDocFinancieraProv = new DocFinancieraProv();
                            vDocFinancieraProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocFinancieraProv.IdDocAdjunto;
                            vDocFinancieraProv.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vDocFinancieraProv.IdInfoFin;
                            vDocFinancieraProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocFinancieraProv.NombreDocumento;
                            vDocFinancieraProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocFinancieraProv.LinkDocumento;
                            vDocFinancieraProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocFinancieraProv.Observaciones;
                            vDocFinancieraProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocFinancieraProv.UsuarioCrea;
                            vDocFinancieraProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocFinancieraProv.FechaCrea;
                            vDocFinancieraProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocFinancieraProv.UsuarioModifica;
                            vDocFinancieraProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocFinancieraProv.FechaModifica;
                            vDocFinancieraProv.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDocFinancieraProv.Obligatorio;

                            vDocFinancieraProv.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocFinancieraProv.IdTipoDocumento;
                            vDocFinancieraProv.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocFinancieraProv.MaxPermitidoKB;
                            vDocFinancieraProv.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocFinancieraProv.ExtensionesPermitidas;
                            vDocFinancieraProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocFinancieraProv.IdTemporal;
                            vListaDocFinancieraProv.Add(vDocFinancieraProv);
                        }
                        return vListaDocFinancieraProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
