using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Documentos asociados a notificaci�n judicial
    /// </summary>
    public class DocNotificacionJudicialDAL : GeneralDAL
    {
        public DocNotificacionJudicialDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocNotJudicial", DbType.Int32, pDocNotificacionJudicial.IdDocNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pDocNotificacionJudicial.IdNotJudicial );
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pDocNotificacionJudicial.IdDocumento );
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocNotificacionJudicial.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocNotificacionJudicial.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Anno", DbType.Int32, pDocNotificacionJudicial.Anno);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocNotificacionJudicial.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocNotJudicial").ToString());
                    GenerarLogAuditoria(pDocNotificacionJudicial, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocNotJudicial", DbType.Int32, pDocNotificacionJudicial.IdDocNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pDocNotificacionJudicial.IdNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pDocNotificacionJudicial.IdDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocNotificacionJudicial.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocNotificacionJudicial.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Anno", DbType.Int32, pDocNotificacionJudicial.Anno);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocNotificacionJudicial.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocNotificacionJudicial, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocNotJudicial", DbType.Int32, pDocNotificacionJudicial.IdDocNotJudicial);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocNotificacionJudicial, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pIdDocNotJudicial">Identificador en entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="pIdEntidad">Identificador en entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="pIdDocumento">Identificador de documento en entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>Entidad Documentos asociados a notificaci�n judicial</returns>
        public DocNotificacionJudicial ConsultarDocNotificacionJudicial(int pIdDocNotJudicial, int pIdEntidad, int pIdDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocNotificacionJudicial_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocNotJudicial", DbType.Int32, pIdDocNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pIdDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocNotificacionJudicial vDocNotificacionJudicial = new DocNotificacionJudicial();
                        while (vDataReaderResults.Read())
                        {
                            vDocNotificacionJudicial.IdDocNotJudicial = vDataReaderResults["IdDocNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocNotJudicial"].ToString()) : vDocNotificacionJudicial.IdDocNotJudicial;
                            vDocNotificacionJudicial.IdNotJudicial = vDataReaderResults["IdNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotJudicial"].ToString()) : vDocNotificacionJudicial.IdNotJudicial;
                            vDocNotificacionJudicial.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocNotificacionJudicial.IdDocumento;
                            vDocNotificacionJudicial.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocNotificacionJudicial.Descripcion;
                            vDocNotificacionJudicial.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocNotificacionJudicial.LinkDocumento;
                            vDocNotificacionJudicial.Anno = vDataReaderResults["Anno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anno"].ToString()) : vDocNotificacionJudicial.Anno;
                            vDocNotificacionJudicial.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocNotificacionJudicial.UsuarioCrea;
                            vDocNotificacionJudicial.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocNotificacionJudicial.FechaCrea;
                            vDocNotificacionJudicial.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocNotificacionJudicial.UsuarioModifica;
                            vDocNotificacionJudicial.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocNotificacionJudicial.FechaModifica;
                        }
                        return vDocNotificacionJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pIdDocNotJudicial">Identifidor de una entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="IdNotJudicial">Identificador de una notificaci�n en una entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="pIdDocumento">Identificador de un documento en una entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>Lista de entidades Documentos asociados a notificaci�n judicial</returns>
        public List<DocNotificacionJudicial> ConsultarDocNotificacionJudicials(int? pIdDocNotJudicial, int? IdNotJudicial, int? pIdDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar"))
                {
                    if(pIdDocNotJudicial != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDocNotJudicial", DbType.Int32, pIdDocNotJudicial);
                    if (IdNotJudicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, IdNotJudicial);
                    if(pIdDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pIdDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocNotificacionJudicial> vListaDocNotificacionJudicial = new List<DocNotificacionJudicial>();
                        while (vDataReaderResults.Read())
                        {
                                DocNotificacionJudicial vDocNotificacionJudicial = new DocNotificacionJudicial();
                            vDocNotificacionJudicial.IdDocNotJudicial = vDataReaderResults["IdDocNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocNotJudicial"].ToString()) : vDocNotificacionJudicial.IdDocNotJudicial;
                            vDocNotificacionJudicial.IdNotJudicial = vDataReaderResults["IdNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotJudicial"].ToString()) : vDocNotificacionJudicial.IdNotJudicial;
                            vDocNotificacionJudicial.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocNotificacionJudicial.IdDocumento;
                            vDocNotificacionJudicial.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocNotificacionJudicial.NombreTipoDocumento;
                            vDocNotificacionJudicial.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocNotificacionJudicial.Descripcion;
                            vDocNotificacionJudicial.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocNotificacionJudicial.LinkDocumento;
                            vDocNotificacionJudicial.Anno = vDataReaderResults["Anno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anno"].ToString()) : vDocNotificacionJudicial.Anno;
                            vDocNotificacionJudicial.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocNotificacionJudicial.UsuarioCrea;
                            vDocNotificacionJudicial.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocNotificacionJudicial.FechaCrea;
                            vDocNotificacionJudicial.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocNotificacionJudicial.UsuarioModifica;
                            vDocNotificacionJudicial.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocNotificacionJudicial.FechaModifica;
                                vListaDocNotificacionJudicial.Add(vDocNotificacionJudicial);
                        }
                        return vListaDocNotificacionJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="IdNotJudicial">Identificador de una notificaci�n en una entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>Lista de entidades Documentos asociados a notificaci�n judicial</returns>
        public List<DocNotificacionJudicial> ConsultarDocNotificacionJudicials_Consultar_IdNotJudicial(int? IdNotJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial"))
                {
                    if (IdNotJudicial != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, IdNotJudicial);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocNotificacionJudicial> vListaDocNotificacionJudicial = new List<DocNotificacionJudicial>();
                        while (vDataReaderResults.Read())
                        {
                            DocNotificacionJudicial vDocNotificacionJudicial = new DocNotificacionJudicial();
                            vDocNotificacionJudicial.IdDocNotJudicial = vDataReaderResults["IdDocNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocNotJudicial"].ToString()) : vDocNotificacionJudicial.IdDocNotJudicial;
                            vDocNotificacionJudicial.IdNotJudicial = vDataReaderResults["IdNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotJudicial"].ToString()) : vDocNotificacionJudicial.IdNotJudicial;
                            vDocNotificacionJudicial.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocNotificacionJudicial.IdDocumento;
                            vDocNotificacionJudicial.NombreTipoDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocNotificacionJudicial.NombreTipoDocumento;
                            vDocNotificacionJudicial.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocNotificacionJudicial.LinkDocumento;
                            vDocNotificacionJudicial.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocNotificacionJudicial.UsuarioCrea;
                            vDocNotificacionJudicial.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocNotificacionJudicial.Descripcion;
                            vDocNotificacionJudicial.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocNotificacionJudicial.FechaCrea;
                            vDocNotificacionJudicial.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocNotificacionJudicial.UsuarioModifica;
                            vDocNotificacionJudicial.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocNotificacionJudicial.FechaModifica;
                            vDocNotificacionJudicial.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocNotificacionJudicial.MaxPermitidoKB;
                            vDocNotificacionJudicial.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocNotificacionJudicial.ExtensionesPermitidas;
                            
                            vListaDocNotificacionJudicial.Add(vDocNotificacionJudicial);
                        }
                        return vListaDocNotificacionJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
