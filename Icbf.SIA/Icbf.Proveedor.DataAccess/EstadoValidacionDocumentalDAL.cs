using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Estado validaci�n Documental
    /// </summary>
    public class EstadoValidacionDocumentalDAL : GeneralDAL
    {
        public EstadoValidacionDocumentalDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validaci�n Documental</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoValidacionDocumental", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoValidacionDocumental", DbType.String, pEstadoValidacionDocumental.CodigoEstadoValidacionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoValidacionDocumental.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstadoValidacionDocumental.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadoValidacionDocumental.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadoValidacionDocumental.IdEstadoValidacionDocumental = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEstadoValidacionDocumental").ToString());
                    GenerarLogAuditoria(pEstadoValidacionDocumental, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validaci�n Documental</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoValidacionDocumental", DbType.Int32, pEstadoValidacionDocumental.IdEstadoValidacionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoValidacionDocumental", DbType.String, pEstadoValidacionDocumental.CodigoEstadoValidacionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoValidacionDocumental.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstadoValidacionDocumental.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadoValidacionDocumental.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoValidacionDocumental, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validaci�n Documental</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoValidacionDocumental", DbType.Int32, pEstadoValidacionDocumental.IdEstadoValidacionDocumental);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoValidacionDocumental, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pIdEstadoValidacionDocumental">Identificador de la entidad Estado validaci�n Documental</param>
        /// <returns>Entidad Estado validaci�n Documental</returns>
        public EstadoValidacionDocumental ConsultarEstadoValidacionDocumental(int pIdEstadoValidacionDocumental)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoValidacionDocumental", DbType.Int32, pIdEstadoValidacionDocumental);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoValidacionDocumental vEstadoValidacionDocumental = new EstadoValidacionDocumental();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoValidacionDocumental.IdEstadoValidacionDocumental = vDataReaderResults["IdEstadoValidacionDocumental"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoValidacionDocumental"].ToString()) : vEstadoValidacionDocumental.IdEstadoValidacionDocumental;
                            vEstadoValidacionDocumental.CodigoEstadoValidacionDocumental = vDataReaderResults["CodigoEstadoValidacionDocumental"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoValidacionDocumental"].ToString()) : vEstadoValidacionDocumental.CodigoEstadoValidacionDocumental;
                            vEstadoValidacionDocumental.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoValidacionDocumental.Descripcion;
                            vEstadoValidacionDocumental.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoValidacionDocumental.Estado;
                            vEstadoValidacionDocumental.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoValidacionDocumental.UsuarioCrea;
                            vEstadoValidacionDocumental.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoValidacionDocumental.FechaCrea;
                            vEstadoValidacionDocumental.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoValidacionDocumental.UsuarioModifica;
                            vEstadoValidacionDocumental.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoValidacionDocumental.FechaModifica;
                        }
                        return vEstadoValidacionDocumental;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado validaci�n Documental
        /// </summary>
        /// <param name="pCodigoEstadoValidacionDocumental">C�digo de estado de validaci�n en una entidad Estado validaci�n Documental</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Estado validaci�n Documental</param>
        /// <param name="pEstado">Estado de una entidad Estado validaci�n Documental</param>
        /// <returns>Lista de entidades Estado validaci�n Documental</returns>
        public List<EstadoValidacionDocumental> ConsultarEstadoValidacionDocumentals(String pCodigoEstadoValidacionDocumental, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar"))
                {
                    if(pCodigoEstadoValidacionDocumental != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoEstadoValidacionDocumental", DbType.String, pCodigoEstadoValidacionDocumental);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoValidacionDocumental> vListaEstadoValidacionDocumental = new List<EstadoValidacionDocumental>();
                        while (vDataReaderResults.Read())
                        {
                                EstadoValidacionDocumental vEstadoValidacionDocumental = new EstadoValidacionDocumental();
                            vEstadoValidacionDocumental.IdEstadoValidacionDocumental = vDataReaderResults["IdEstadoValidacionDocumental"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoValidacionDocumental"].ToString()) : vEstadoValidacionDocumental.IdEstadoValidacionDocumental;
                            vEstadoValidacionDocumental.CodigoEstadoValidacionDocumental = vDataReaderResults["CodigoEstadoValidacionDocumental"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadoValidacionDocumental"].ToString()) : vEstadoValidacionDocumental.CodigoEstadoValidacionDocumental;
                            vEstadoValidacionDocumental.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoValidacionDocumental.Descripcion;
                            vEstadoValidacionDocumental.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoValidacionDocumental.Estado;
                            vEstadoValidacionDocumental.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoValidacionDocumental.UsuarioCrea;
                            vEstadoValidacionDocumental.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoValidacionDocumental.FechaCrea;
                            vEstadoValidacionDocumental.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoValidacionDocumental.UsuarioModifica;
                            vEstadoValidacionDocumental.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoValidacionDocumental.FechaModifica;
                                vListaEstadoValidacionDocumental.Add(vEstadoValidacionDocumental);
                        }
                        return vListaEstadoValidacionDocumental;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
