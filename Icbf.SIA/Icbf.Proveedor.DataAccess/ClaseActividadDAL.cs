using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Clase de Actividad
    /// </summary>
    public class ClaseActividadDAL : GeneralDAL
    {
        public ClaseActividadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Clase de Actividad
        /// </summary>
        /// <param name="pClaseActividad">Entidad Clase de Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarClaseActividad(ClaseActividad pClaseActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Proveedor_ClaseActividad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdClaseActividad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pClaseActividad.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pClaseActividad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pClaseActividad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pClaseActividad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pClaseActividad.IdClaseActividad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdClaseActividad").ToString());
                    GenerarLogAuditoria(pClaseActividad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Clase de Actividad
        /// </summary>
        /// <param name="pClaseActividad">Entidad Clase de Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarClaseActividad(ClaseActividad pClaseActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Proveedor_ClaseActividad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdClaseActividad", DbType.Int32, pClaseActividad.IdClaseActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pClaseActividad.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pClaseActividad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pClaseActividad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pClaseActividad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pClaseActividad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Clase de Actividad
        /// </summary>
        /// <param name="pIdClaseActividad">Identificador en una entidad Clase de Actividad</param>
        /// <returns>Entidad Clase de Actividad</returns>
        public ClaseActividad ConsultarClaseActividad(int pIdClaseActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Proveedor_ClaseActividad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdClaseActividad", DbType.Int32, pIdClaseActividad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ClaseActividad vClaseActividad = new ClaseActividad();
                        while (vDataReaderResults.Read())
                        {
                            vClaseActividad.IdClaseActividad = vDataReaderResults["IdClaseActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClaseActividad"].ToString()) : vClaseActividad.IdClaseActividad;
                            vClaseActividad.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vClaseActividad.Codigo;
                            vClaseActividad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vClaseActividad.Descripcion;
                            vClaseActividad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClaseActividad.Estado;
                            vClaseActividad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClaseActividad.UsuarioCrea;
                            vClaseActividad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClaseActividad.FechaCrea;
                            vClaseActividad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClaseActividad.UsuarioModifica;
                            vClaseActividad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClaseActividad.FechaModifica;
                        }
                        return vClaseActividad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de clases de actividades
        /// </summary>
        /// <param name="pCodigo">C�digo de la clase de actividad</param>
        /// <param name="pDescripcion">Descripci�n de la clase de actividad</param>
        /// <param name="pEstado">Estado de la clase de actividad</param>
        /// <returns>Lista de clases de actividades</returns>
        public List<ClaseActividad> ConsultarClaseActividads(String pCodigo, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_Proveedor_ClaseActividads_Consultar"))
                {
                    if (pCodigo != null)
                        vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCodigo);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClaseActividad> vListaClaseActividad = new List<ClaseActividad>();
                        while (vDataReaderResults.Read())
                        {
                                ClaseActividad vClaseActividad = new ClaseActividad();
                            vClaseActividad.IdClaseActividad = vDataReaderResults["IdClaseActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClaseActividad"].ToString()) : vClaseActividad.IdClaseActividad;
                            vClaseActividad.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vClaseActividad.Codigo;
                            vClaseActividad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vClaseActividad.Descripcion;
                            vClaseActividad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClaseActividad.Estado;
                            vClaseActividad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClaseActividad.UsuarioCrea;
                            vClaseActividad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClaseActividad.FechaCrea;
                            vClaseActividad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClaseActividad.UsuarioModifica;
                            vClaseActividad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClaseActividad.FechaModifica;
                                vListaClaseActividad.Add(vClaseActividad);
                        }
                        return vListaClaseActividad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
