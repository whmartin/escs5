using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad de validaci�n de experiencia
    /// </summary>
    public class ValidarInfoExperienciaEntidadDAL : GeneralDAL
    {
        public ValidarInfoExperienciaEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pValidarInfoExperienciaEntidad">Entidad Validaci�n de experiencia</param>
        /// <param name="idEstadoInfoExperienciaEntidad">Identificador del estado</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValidarInfoExperienciaEntidad(ValidarInfoExperienciaEntidad pValidarInfoExperienciaEntidad, int idEstadoInfoExperienciaEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Insertar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValidarInfoExperienciaEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoExperienciaEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pValidarInfoExperienciaEntidad.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoExperienciaEntidad.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoExperienciaEntidad.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoExperienciaEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValidarInfoExperienciaEntidad").ToString());
                    GenerarLogAuditoria(pValidarInfoExperienciaEntidad, vDbCommand);
                    //return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdExpEntidad", DbType.String, pValidarInfoExperienciaEntidad.IdExpEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoExperienciaEntidad", DbType.Int16, idEstadoInfoExperienciaEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoExperienciaEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        GenerarLogAuditoria(pValidarInfoExperienciaEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pValidarInfoExperienciaEntidad">Entidad Validaci�n de experiencia</param>
        /// <param name="idEstadoInfoExperienciaEntidad">Identificador del estado</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarValidarInfoExperienciaEntidad(ValidarInfoExperienciaEntidad pValidarInfoExperienciaEntidad, int idEstadoInfoExperienciaEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Modificar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoExperienciaEntidad", DbType.Int32, pValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoExperienciaEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pValidarInfoExperienciaEntidad.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoExperienciaEntidad.Observaciones);
                    if (pValidarInfoExperienciaEntidad.ConfirmaYAprueba != null)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoExperienciaEntidad.ConfirmaYAprueba);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoExperienciaEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);

                    GenerarLogAuditoria(pValidarInfoExperienciaEntidad, vDbCommand);

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado"))
                    {

                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdExpEntidad", DbType.String, pValidarInfoExperienciaEntidad.IdExpEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoExperienciaEntidad", DbType.Int16, idEstadoInfoExperienciaEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoExperienciaEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        GenerarLogAuditoria(pValidarInfoExperienciaEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdValidarInfoExperienciaEntidad">Identificador de una entidad Validaci�n de experiencia</param>
        /// <returns>Entidad Validaci�n de experiencia</returns>
        public ValidarInfoExperienciaEntidad ConsultarValidarInfoExperienciaEntidad(int pIdValidarInfoExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoExperienciaEntidad", DbType.Int32, pIdValidarInfoExperienciaEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoExperienciaEntidad vValidarInfoExperienciaEntidad = new ValidarInfoExperienciaEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad = vDataReaderResults["IdValidarInfoExperienciaEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoExperienciaEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad;
                            vValidarInfoExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdExpEntidad;
                            vValidarInfoExperienciaEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoExperienciaEntidad.NroRevision;
                            vValidarInfoExperienciaEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoExperienciaEntidad.Observaciones;
                            vValidarInfoExperienciaEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoExperienciaEntidad.ConfirmaYAprueba;
                            vValidarInfoExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoExperienciaEntidad.UsuarioCrea;
                            vValidarInfoExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoExperienciaEntidad.FechaCrea;
                        }
                        return vValidarInfoExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en una entidad Validaci�n de experiencia</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad Validaci�n de experiencia</param>
        /// <returns>Lista de entidades Validaci�n de experiencia</returns>
        public List<ValidarInfoExperienciaEntidad> ConsultarValidarInfoExperienciaEntidads(Int32 pIdExpEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_Consultar"))
                {
                    if (pIdExpEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pIdExpEntidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoExperienciaEntidad> vListaValidarTercero = new List<ValidarInfoExperienciaEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoExperienciaEntidad vValidarInfoExperienciaEntidad = new ValidarInfoExperienciaEntidad();
                   
                            vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad = vDataReaderResults["IdValidarInfoExperienciaEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoExperienciaEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad;
                            vValidarInfoExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdExpEntidad;
                            vValidarInfoExperienciaEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoExperienciaEntidad.NroRevision;
                            vValidarInfoExperienciaEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoExperienciaEntidad.Observaciones;
                            vValidarInfoExperienciaEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoExperienciaEntidad.ConfirmaYAprueba;
                            vValidarInfoExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoExperienciaEntidad.UsuarioCrea;
                            vValidarInfoExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoExperienciaEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoExperienciaEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en la entidad Validaci�n de experiencia</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad Validaci�n de experiencia</param>
        /// <returns>Lista de entidades Validaci�n de experiencia</returns>
        public List<ValidarInfoExperienciaEntidad> ConsultarValidarInfoExperienciaEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pObservaciones != null)
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if (pConfirmaYAprueba != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoExperienciaEntidad> vListaValidarTercero = new List<ValidarInfoExperienciaEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoExperienciaEntidad vValidarInfoExperienciaEntidad = new ValidarInfoExperienciaEntidad();

                            vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad = vDataReaderResults["IdValidarInfoExperienciaEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoExperienciaEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad;
                            vValidarInfoExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdExpEntidad;
                            vValidarInfoExperienciaEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoExperienciaEntidad.NroRevision;
                            vValidarInfoExperienciaEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoExperienciaEntidad.Observaciones;
                            vValidarInfoExperienciaEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoExperienciaEntidad.ConfirmaYAprueba;
                            vValidarInfoExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoExperienciaEntidad.UsuarioCrea;
                            vValidarInfoExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoExperienciaEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoExperienciaEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pIdExpEntidad">Identificador de la experiencia en la entidad Validaci�n de experiencia</param>
        /// <returns>Entidad Validaci�n de experiencia</returns>
        public ValidarInfoExperienciaEntidad ConsultarValidarInfoExperienciaEntidad_Ultima(Int32 pIdEntidad, Int32 pIdExpEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_Ultima"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int16, pIdExpEntidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoExperienciaEntidad vValidarInfoExperienciaEntidad = new ValidarInfoExperienciaEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad = vDataReaderResults["IdValidarInfoExperienciaEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoExperienciaEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdValidarInfoExperienciaEntidad;
                            vValidarInfoExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vValidarInfoExperienciaEntidad.IdExpEntidad;
                            vValidarInfoExperienciaEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoExperienciaEntidad.NroRevision;
                            vValidarInfoExperienciaEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoExperienciaEntidad.Observaciones;
                            vValidarInfoExperienciaEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoExperienciaEntidad.ConfirmaYAprueba;
                            vValidarInfoExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoExperienciaEntidad.UsuarioCrea;
                            vValidarInfoExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoExperienciaEntidad.FechaCrea;
                        }
                        return vValidarInfoExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
