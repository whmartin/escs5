using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Informaci�n m�dulo financero
    /// </summary>
    public class InfoFinancieraEntidadDAL : GeneralDAL
    {
        public InfoFinancieraEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInfoFin", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoFinancieraEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pInfoFinancieraEntidad.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoCte", DbType.Decimal, pInfoFinancieraEntidad.ActivoCte);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoTotal", DbType.Decimal, pInfoFinancieraEntidad.ActivoTotal);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoCte", DbType.Decimal, pInfoFinancieraEntidad.PasivoCte);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoTotal", DbType.Decimal, pInfoFinancieraEntidad.PasivoTotal);
                    vDataBase.AddInParameter(vDbCommand, "@Patrimonio", DbType.Decimal, pInfoFinancieraEntidad.Patrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@GastosInteresFinancieros", DbType.Decimal, pInfoFinancieraEntidad.GastosInteresFinancieros);
                    vDataBase.AddInParameter(vDbCommand, "@UtilidadOperacional", DbType.Decimal, pInfoFinancieraEntidad.UtilidadOperacional);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaIndicadoresFinancieros", DbType.Boolean, pInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros);
                    vDataBase.AddInParameter(vDbCommand, "@RupRenovado", DbType.Boolean, pInfoFinancieraEntidad.RupRenovado);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoValidacion", DbType.Int32, pInfoFinancieraEntidad.EstadoValidacion);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesInformacionFinanciera", DbType.String, pInfoFinancieraEntidad.ObservacionesInformacionFinanciera);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesValidadorICBF", DbType.String, pInfoFinancieraEntidad.ObservacionesValidadorICBF);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInfoFinancieraEntidad.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pInfoFinancieraEntidad.IdTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pInfoFinancieraEntidad.Finalizado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInfoFinancieraEntidad.IdInfoFin = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdInfoFin").ToString());
                    GenerarLogAuditoria(pInfoFinancieraEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Informaci�n m�dulo financero</param>
        /// <returns>Identificador de la entidad Informaci�n m�dulo financero en tabla</returns>
        public int ModificarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pInfoFinancieraEntidad.IdInfoFin);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoFinancieraEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pInfoFinancieraEntidad.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoCte", DbType.Decimal, pInfoFinancieraEntidad.ActivoCte);
                    vDataBase.AddInParameter(vDbCommand, "@ActivoTotal", DbType.Decimal, pInfoFinancieraEntidad.ActivoTotal);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoCte", DbType.Decimal, pInfoFinancieraEntidad.PasivoCte);
                    vDataBase.AddInParameter(vDbCommand, "@PasivoTotal", DbType.Decimal, pInfoFinancieraEntidad.PasivoTotal);
                    vDataBase.AddInParameter(vDbCommand, "@Patrimonio", DbType.Decimal, pInfoFinancieraEntidad.Patrimonio);
                    vDataBase.AddInParameter(vDbCommand, "@GastosInteresFinancieros", DbType.Decimal, pInfoFinancieraEntidad.GastosInteresFinancieros);
                    vDataBase.AddInParameter(vDbCommand, "@UtilidadOperacional", DbType.Decimal, pInfoFinancieraEntidad.UtilidadOperacional);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaIndicadoresFinancieros", DbType.Boolean, pInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros);
                    vDataBase.AddInParameter(vDbCommand, "@RupRenovado", DbType.Boolean, pInfoFinancieraEntidad.RupRenovado);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoValidacion", DbType.Int32, pInfoFinancieraEntidad.EstadoValidacion);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesInformacionFinanciera", DbType.String, pInfoFinancieraEntidad.ObservacionesInformacionFinanciera);
                    vDataBase.AddInParameter(vDbCommand, "@ObservacionesValidadorICBF", DbType.String, pInfoFinancieraEntidad.ObservacionesValidadorICBF);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInfoFinancieraEntidad.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pInfoFinancieraEntidad.Finalizado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoFinancieraEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar una entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Identificador de una entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pInfoFinancieraEntidad.IdInfoFin);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoFinancieraEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pIdInfoFin">Identificador de una entidad Informaci�n m�dulo financero</param>
        /// <returns>Entidad Informaci�n m�dulo financero</returns>
        public InfoFinancieraEntidad ConsultarInfoFinancieraEntidad(int pIdInfoFin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pIdInfoFin);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vInfoFinancieraEntidad.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vInfoFinancieraEntidad.IdInfoFin;
                            vInfoFinancieraEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoFinancieraEntidad.IdEntidad;
                            vInfoFinancieraEntidad.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vInfoFinancieraEntidad.IdVigencia;
                            vInfoFinancieraEntidad.ActivoCte = vDataReaderResults["ActivoCte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoCte"].ToString()) : vInfoFinancieraEntidad.ActivoCte;
                            vInfoFinancieraEntidad.ActivoTotal = vDataReaderResults["ActivoTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoTotal"].ToString()) : vInfoFinancieraEntidad.ActivoTotal;
                            vInfoFinancieraEntidad.PasivoCte = vDataReaderResults["PasivoCte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoCte"].ToString()) : vInfoFinancieraEntidad.PasivoCte;
                            vInfoFinancieraEntidad.PasivoTotal = vDataReaderResults["PasivoTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoTotal"].ToString()) : vInfoFinancieraEntidad.PasivoTotal;
                            vInfoFinancieraEntidad.Patrimonio = vDataReaderResults["Patrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Patrimonio"].ToString()) : vInfoFinancieraEntidad.Patrimonio;
                            vInfoFinancieraEntidad.GastosInteresFinancieros = vDataReaderResults["GastosInteresFinancieros"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["GastosInteresFinancieros"].ToString()) : vInfoFinancieraEntidad.GastosInteresFinancieros;
                            vInfoFinancieraEntidad.UtilidadOperacional = vDataReaderResults["UtilidadOperacional"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadOperacional"].ToString()) : vInfoFinancieraEntidad.UtilidadOperacional;
                            vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros = vDataReaderResults["ConfirmaIndicadoresFinancieros"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaIndicadoresFinancieros"].ToString()) : vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros;
                            vInfoFinancieraEntidad.RupRenovado = vDataReaderResults["RupRenovado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RupRenovado"].ToString()) : vInfoFinancieraEntidad.RupRenovado;
                            vInfoFinancieraEntidad.EstadoValidacion = vDataReaderResults["EstadoValidacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoValidacion"].ToString()) : vInfoFinancieraEntidad.EstadoValidacion;
                            vInfoFinancieraEntidad.ObservacionesInformacionFinanciera = vDataReaderResults["ObservacionesInformacionFinanciera"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesInformacionFinanciera"].ToString()) : vInfoFinancieraEntidad.ObservacionesInformacionFinanciera;
                            vInfoFinancieraEntidad.ObservacionesValidadorICBF = vDataReaderResults["ObservacionesValidadorICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesValidadorICBF"].ToString()) : vInfoFinancieraEntidad.ObservacionesValidadorICBF;
                            vInfoFinancieraEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoFinancieraEntidad.UsuarioCrea;
                            vInfoFinancieraEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoFinancieraEntidad.FechaCrea;
                            vInfoFinancieraEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoFinancieraEntidad.UsuarioModifica;
                            vInfoFinancieraEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoFinancieraEntidad.FechaModifica;
                            vInfoFinancieraEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["NroRevision"].ToString()) : vInfoFinancieraEntidad.NroRevision;
                            vInfoFinancieraEntidad.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vInfoFinancieraEntidad.Finalizado;
                        }
                        return vInfoFinancieraEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pIdEntidad">Identificador general en una entidad Informaci�n m�dulo financero</param>
        /// <param name="pIdVigencia">Identificador de vigencia en una entidad Informaci�n m�dulo financero</param>
        /// <returns>Lista de entidades Informaci�n m�dulo financero</returns>
        public List<InfoFinancieraEntidad> ConsultarInfoFinancieraEntidads(int? pIdEntidad, int? pIdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    if (pIdVigencia != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pIdVigencia);
         
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InfoFinancieraEntidad> vListaInfoFinancieraEntidad = new List<InfoFinancieraEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();
                            vInfoFinancieraEntidad.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vInfoFinancieraEntidad.IdInfoFin;
                            vInfoFinancieraEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoFinancieraEntidad.IdEntidad;
                            vInfoFinancieraEntidad.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vInfoFinancieraEntidad.IdVigencia;
                            vInfoFinancieraEntidad.ActivoCte = vDataReaderResults["ActivoCte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoCte"].ToString()) : vInfoFinancieraEntidad.ActivoCte;
                            vInfoFinancieraEntidad.ActivoTotal = vDataReaderResults["ActivoTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ActivoTotal"].ToString()) : vInfoFinancieraEntidad.ActivoTotal;
                            vInfoFinancieraEntidad.PasivoCte = vDataReaderResults["PasivoCte"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoCte"].ToString()) : vInfoFinancieraEntidad.PasivoCte;
                            vInfoFinancieraEntidad.PasivoTotal = vDataReaderResults["PasivoTotal"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PasivoTotal"].ToString()) : vInfoFinancieraEntidad.PasivoTotal;
                            vInfoFinancieraEntidad.Patrimonio = vDataReaderResults["Patrimonio"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Patrimonio"].ToString()) : vInfoFinancieraEntidad.Patrimonio;
                            vInfoFinancieraEntidad.GastosInteresFinancieros = vDataReaderResults["GastosInteresFinancieros"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["GastosInteresFinancieros"].ToString()) : vInfoFinancieraEntidad.GastosInteresFinancieros;
                            vInfoFinancieraEntidad.UtilidadOperacional = vDataReaderResults["UtilidadOperacional"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["UtilidadOperacional"].ToString()) : vInfoFinancieraEntidad.UtilidadOperacional;
                            vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros = vDataReaderResults["ConfirmaIndicadoresFinancieros"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaIndicadoresFinancieros"].ToString()) : vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros;
                            vInfoFinancieraEntidad.RupRenovado = vDataReaderResults["RupRenovado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["RupRenovado"].ToString()) : vInfoFinancieraEntidad.RupRenovado;
                            vInfoFinancieraEntidad.EstadoValidacion = vDataReaderResults["EstadoValidacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoValidacion"].ToString()) : vInfoFinancieraEntidad.EstadoValidacion;
                            vInfoFinancieraEntidad.ObservacionesInformacionFinanciera = vDataReaderResults["ObservacionesInformacionFinanciera"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesInformacionFinanciera"].ToString()) : vInfoFinancieraEntidad.ObservacionesInformacionFinanciera;
                            vInfoFinancieraEntidad.ObservacionesValidadorICBF = vDataReaderResults["ObservacionesValidadorICBF"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObservacionesValidadorICBF"].ToString()) : vInfoFinancieraEntidad.ObservacionesValidadorICBF;
                            vInfoFinancieraEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoFinancieraEntidad.UsuarioCrea;
                            vInfoFinancieraEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoFinancieraEntidad.FechaCrea;
                            vInfoFinancieraEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoFinancieraEntidad.UsuarioModifica;
                            vInfoFinancieraEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoFinancieraEntidad.FechaModifica;
                            vInfoFinancieraEntidad.Aprobada = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConfirmaYAprueba"])  : vInfoFinancieraEntidad.Aprobada;
                            //
                            //Calcula el Patrimonio SMLV segun el salario minimo de la vigencia
                            //
                            vInfoFinancieraEntidad.PatrimonioSMLV = vInfoFinancieraEntidad.Patrimonio / SalarioMinimo_Consultar(vInfoFinancieraEntidad.IdVigencia);
                            
                            vListaInfoFinancieraEntidad.Add(vInfoFinancieraEntidad);
                        }
                        return vListaInfoFinancieraEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta salario m�nimo
        /// </summary>
        /// <param name="IdVigencia">Identificador de vigencia en una entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero que expresa el Salario m�nimo</returns>
        public int SalarioMinimo_Consultar(int IdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, IdVigencia);
                    vResultado = (int)vDataBase.ExecuteScalar(vDbCommand);
                    //GenerarLogAuditoria(pInfoFinancieraEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n m�dulo financero por liberar
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarInfoFinancieraEntidad_Liberar(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoFinancieraEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoValidacion", DbType.Int32, pInfoFinancieraEntidad.EstadoValidacion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInfoFinancieraEntidad.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pInfoFinancieraEntidad.Finalizado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoFinancieraEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza InfoFinancieraEntidad
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public int FinalizaInfoFinancieraEntidad(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_InfoFinancieraEntidad_Finalizar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
