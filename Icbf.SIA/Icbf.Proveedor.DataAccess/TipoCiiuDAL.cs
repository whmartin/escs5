using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad tipo Ciuu
    /// </summary>
    public class TipoCiiuDAL : GeneralDAL
    {
        public TipoCiiuDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiiu_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoCiiu", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCiiu", DbType.String, pTipoCiiu.CodigoCiiu);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCiiu.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCiiu.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoCiiu.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoCiiu.IdTipoCiiu = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoCiiu").ToString());
                    GenerarLogAuditoria(pTipoCiiu, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiiu_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiu", DbType.Int32, pTipoCiiu.IdTipoCiiu);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCiiu", DbType.String, pTipoCiiu.CodigoCiiu);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCiiu.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCiiu.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoCiiu.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoCiiu, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiiu_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiu", DbType.Int32, pTipoCiiu.IdTipoCiiu);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoCiiu, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pIdTipoCiiu">Identificador de una entidad Tipo Ciuu</param>
        /// <returns>Entidad Tipo Ciuu</returns>
        public TipoCiiu ConsultarTipoCiiu(int pIdTipoCiiu)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiiu_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCiiu", DbType.Int32, pIdTipoCiiu);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoCiiu vTipoCiiu = new TipoCiiu();
                        while (vDataReaderResults.Read())
                        {
                            vTipoCiiu.IdTipoCiiu = vDataReaderResults["IdTipoCiiu"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCiiu"].ToString()) : vTipoCiiu.IdTipoCiiu;
                            vTipoCiiu.CodigoCiiu = vDataReaderResults["CodigoCiiu"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCiiu"].ToString()) : vTipoCiiu.CodigoCiiu;
                            vTipoCiiu.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCiiu.Descripcion;
                            vTipoCiiu.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCiiu.Estado;
                            vTipoCiiu.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCiiu.UsuarioCrea;
                            vTipoCiiu.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCiiu.FechaCrea;
                            vTipoCiiu.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCiiu.UsuarioModifica;
                            vTipoCiiu.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCiiu.FechaModifica;
                        }
                        return vTipoCiiu;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Ciuu
        /// </summary>
        /// <param name="pCodigoCiiu">C�digo en una entidad Tipo Ciuu</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Ciuu</param>
        /// <param name="pEstado">Estado en una entidad Tipo Ciuu</param>
        /// <returns>Lista de entidades Tipo Ciuu</returns>
        public List<TipoCiiu> ConsultarTipoCiius(String pCodigoCiiu, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiius_Consultar"))
                {
                    if(pCodigoCiiu != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoCiiu", DbType.String, pCodigoCiiu);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCiiu> vListaTipoCiiu = new List<TipoCiiu>();
                        while (vDataReaderResults.Read())
                        {
                                TipoCiiu vTipoCiiu = new TipoCiiu();
                            vTipoCiiu.IdTipoCiiu = vDataReaderResults["IdTipoCiiu"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCiiu"].ToString()) : vTipoCiiu.IdTipoCiiu;
                            vTipoCiiu.CodigoCiiu = vDataReaderResults["CodigoCiiu"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoCiiu"].ToString()) : vTipoCiiu.CodigoCiiu;
                            vTipoCiiu.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCiiu.Descripcion;
                            vTipoCiiu.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCiiu.Estado;
                            vTipoCiiu.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCiiu.UsuarioCrea;
                            vTipoCiiu.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCiiu.FechaCrea;
                            vTipoCiiu.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCiiu.UsuarioModifica;
                            vTipoCiiu.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCiiu.FechaModifica;
                                vListaTipoCiiu.Add(vTipoCiiu);
                        }
                        return vListaTipoCiiu;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar una transacci�n para tipo Ciiu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciiu</param>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoTransaction(Icbf.Proveedor.Entity.TipoCiiu pTipoCiiu, Tipoentidad  pTipoentidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();

                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCiiu_Insertar"))
                {

                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoCiiu", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoCiiu", DbType.String, pTipoCiiu.CodigoCiiu);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCiiu.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCiiu.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoCiiu.UsuarioCrea);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand,vDbTransaction);
                    pTipoCiiu.IdTipoCiiu = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoCiiu").ToString());
                    
                    GenerarLogAuditoria(pTipoCiiu, vDbCommand);

                    //vDbTransaction.Commit();
                    //vDBConnection.Close();

                    // return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidad_Insertar"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;
                     
                        vDataBase.AddOutParameter(vDbCommand2, "@IdTipoentidad", DbType.Int32, 18);
                        vDataBase.AddInParameter(vDbCommand2, "@CodigoTipoentidad", DbType.String, pTipoentidad.CodigoTipoentidad);
                        vDataBase.AddInParameter(vDbCommand2, "@Descripcion", DbType.String, pTipoentidad.Descripcion);
                        vDataBase.AddInParameter(vDbCommand2, "@Estado", DbType.Boolean, pTipoentidad.Estado);
                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioCrea", DbType.String, pTipoentidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);                        
                        pTipoentidad.IdTipoentidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand2, "@IdTipoentidad").ToString());
                        GenerarLogAuditoria(pTipoentidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw ex;
            }
        }

    }
}
