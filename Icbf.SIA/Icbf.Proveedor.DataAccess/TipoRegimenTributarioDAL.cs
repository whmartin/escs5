using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Tipo R�gimen Tributario
    /// </summary>
    public class TipoRegimenTributarioDAL : GeneralDAL
    {
        public TipoRegimenTributarioDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo R�gimen Tributario</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegimenTributario", DbType.String, pTipoRegimenTributario.CodigoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoRegimenTributario.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTipoRegimenTributario.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoRegimenTributario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoRegimenTributario.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoRegimenTributario.IdTipoRegimenTributario = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoRegimenTributario").ToString());
                    GenerarLogAuditoria(pTipoRegimenTributario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo R�gimen Tributario</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoRegimenTributario_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pTipoRegimenTributario.IdTipoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRegimenTributario", DbType.String, pTipoRegimenTributario.CodigoRegimenTributario);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoRegimenTributario.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pTipoRegimenTributario.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoRegimenTributario.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoRegimenTributario.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoRegimenTributario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo R�gimen Tributario</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoRegimenTributario_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pTipoRegimenTributario.IdTipoRegimenTributario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoRegimenTributario, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pIdTipoRegimenTributario">Identificador de una entidad Tipo R�gimen Tributario</param>
        /// <returns>Entidad Tipo R�gimen Tributario</returns>
        public Icbf.Proveedor.Entity.TipoRegimenTributario ConsultarTipoRegimenTributario(int pIdTipoRegimenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegimenTributario", DbType.Int32, pIdTipoRegimenTributario);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoRegimenTributario vTipoRegimenTributario = new TipoRegimenTributario();
                        while (vDataReaderResults.Read())
                        {
                            vTipoRegimenTributario.IdTipoRegimenTributario = vDataReaderResults["IdTipoRegimenTributario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.IdTipoRegimenTributario;
                            vTipoRegimenTributario.CodigoRegimenTributario = vDataReaderResults["CodigoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegimenTributario"].ToString()) : vTipoRegimenTributario.CodigoRegimenTributario;
                            vTipoRegimenTributario.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoRegimenTributario.Descripcion;
                            vTipoRegimenTributario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTipoRegimenTributario.IdTipoPersona;
                            vTipoRegimenTributario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRegimenTributario.Estado;
                            vTipoRegimenTributario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoRegimenTributario.UsuarioCrea;
                            vTipoRegimenTributario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoRegimenTributario.FechaCrea;
                            vTipoRegimenTributario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoRegimenTributario.UsuarioModifica;
                            vTipoRegimenTributario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoRegimenTributario.FechaModifica;
                            vTipoRegimenTributario.DescripcionTipoPersona = vDataReaderResults["DescripcionTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoPersona"].ToString()) : vTipoRegimenTributario.DescripcionTipoPersona;
                        }
                        return vTipoRegimenTributario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pCodigoRegimenTributario">C�digo en una entidad Tipo R�gimen Tributario</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo R�gimen Tributario</param>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Tipo R�gimen Tributario</param>
        /// <param name="pEstado">Estado en una entidad Tipo R�gimen Tributario</param>
        /// <returns>Lista de entidades Tipo R�gimen Tributario</returns>
        public List<Icbf.Proveedor.Entity.TipoRegimenTributario> ConsultarTipoRegimenTributarios(String pCodigoRegimenTributario, String pDescripcion, int? pIdTipoPersona, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar"))
                {
                    if (pCodigoRegimenTributario != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoRegimenTributario", DbType.String, pCodigoRegimenTributario);
                    if (pDescripcion != null)
                        vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if (pIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoRegimenTributario> vListaTipoRegimenTributario = new List<TipoRegimenTributario>();
                        while (vDataReaderResults.Read())
                        {
                            TipoRegimenTributario vTipoRegimenTributario = new TipoRegimenTributario();
                            vTipoRegimenTributario.IdTipoRegimenTributario = vDataReaderResults["IdTipoRegimenTributario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.IdTipoRegimenTributario;
                            vTipoRegimenTributario.CodigoRegimenTributario = vDataReaderResults["CodigoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegimenTributario"].ToString()) : vTipoRegimenTributario.CodigoRegimenTributario;
                            vTipoRegimenTributario.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoRegimenTributario.Descripcion;
                            vTipoRegimenTributario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTipoRegimenTributario.IdTipoPersona;
                            vTipoRegimenTributario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRegimenTributario.Estado;
                            vTipoRegimenTributario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoRegimenTributario.UsuarioCrea;
                            vTipoRegimenTributario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoRegimenTributario.FechaCrea;
                            vTipoRegimenTributario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoRegimenTributario.UsuarioModifica;
                            vTipoRegimenTributario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoRegimenTributario.FechaModifica;
                            vTipoRegimenTributario.DescripcionTipoPersona = vDataReaderResults["DescripcionTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoPersona"].ToString()) : vTipoRegimenTributario.DescripcionTipoPersona;
                            vListaTipoRegimenTributario.Add(vTipoRegimenTributario);
                        }
                        return vListaTipoRegimenTributario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    
        /// <summary>
        /// Consultar una lista de entidades Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Tipo R�gimen Tributario</param>
        /// <returns>Lista de entidades Tipo R�gimen Tributario</returns>
        public List<TipoRegimenTributario> ConsultarTipoRegimenTributarioTipoPersona( int? pIdTipoPersona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona"))
                {
                      if (pIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                     using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoRegimenTributario> vListaTipoRegimenTributario = new List<TipoRegimenTributario>();
                        while (vDataReaderResults.Read())
                        {
                            TipoRegimenTributario vTipoRegimenTributario = new TipoRegimenTributario();
                            vTipoRegimenTributario.IdTipoRegimenTributario = vDataReaderResults["IdTipoRegimenTributario"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegimenTributario"].ToString()) : vTipoRegimenTributario.IdTipoRegimenTributario;
                            vTipoRegimenTributario.CodigoRegimenTributario = vDataReaderResults["CodigoRegimenTributario"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRegimenTributario"].ToString()) : vTipoRegimenTributario.CodigoRegimenTributario;
                            vTipoRegimenTributario.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoRegimenTributario.Descripcion;
                            vTipoRegimenTributario.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vTipoRegimenTributario.IdTipoPersona;
                            vTipoRegimenTributario.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoRegimenTributario.Estado;
                            vTipoRegimenTributario.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoRegimenTributario.UsuarioCrea;
                            vTipoRegimenTributario.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoRegimenTributario.FechaCrea;
                            vTipoRegimenTributario.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoRegimenTributario.UsuarioModifica;
                            vTipoRegimenTributario.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoRegimenTributario.FechaModifica;
                            //vTipoRegimenTributario.DescripcionTipoPersona = vDataReaderResults["DescripcionTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionTipoPersona"].ToString()) : vTipoRegimenTributario.DescripcionTipoPersona;
                            vListaTipoRegimenTributario.Add(vTipoRegimenTributario);
                        }
                        return vListaTipoRegimenTributario;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
