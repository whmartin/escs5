using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Acuerdos
    /// </summary>
    public class AcuerdosDAL : GeneralDAL
    {
        public AcuerdosDAL()
        {
        }
        /// <summary>
        /// Inserta un nuevo Acuerdo
        /// </summary>
        /// <param name="pAcuerdos">Entidad Acuerdos</param>
        /// <returns>Identificador del Acuerdo en tabla</returns>
        public int InsertarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Acuerdos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdAcuerdo", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pAcuerdos.Nombre);
                    vDataBase.AddInParameter(vDbCommand, "@ContenidoHtml", DbType.String, pAcuerdos.ContenidoHtml);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pAcuerdos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pAcuerdos.IdAcuerdo = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdAcuerdo").ToString());
                    GenerarLogAuditoria(pAcuerdos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Acuerdo
        /// </summary>
        /// <param name="pIdAcuerdo">Identificador del Acuerdo</param>
        /// <returns>Entidad Acuerdos</returns>
        public Acuerdos ConsultarAcuerdos(int pIdAcuerdo)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Acuerdos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdAcuerdo", DbType.Int32, pIdAcuerdo);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Acuerdos vAcuerdos = new Acuerdos();
                        while (vDataReaderResults.Read())
                        {
                            vAcuerdos.IdAcuerdo = vDataReaderResults["IdAcuerdo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAcuerdo"].ToString()) : vAcuerdos.IdAcuerdo;
                            vAcuerdos.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vAcuerdos.Nombre;
                            vAcuerdos.ContenidoHtml = vDataReaderResults["ContenidoHtml"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContenidoHtml"].ToString()) : vAcuerdos.ContenidoHtml;
                            vAcuerdos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAcuerdos.UsuarioCrea;
                            vAcuerdos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAcuerdos.FechaCrea;
                            vAcuerdos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAcuerdos.UsuarioModifica;
                            vAcuerdos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAcuerdos.FechaModifica;
                        }
                        return vAcuerdos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Acuerdo Vigente
        /// </summary>
        /// <param></param>
        /// <returns>Entidad Acuerdos</returns>
        public Acuerdos ConsultarAcuerdoVigente()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Acuerdos vAcuerdos = new Acuerdos();
                        while (vDataReaderResults.Read())
                        {
                            vAcuerdos.IdAcuerdo = vDataReaderResults["IdAcuerdo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAcuerdo"].ToString()) : vAcuerdos.IdAcuerdo;
                            vAcuerdos.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vAcuerdos.Nombre;
                            vAcuerdos.ContenidoHtml = vDataReaderResults["ContenidoHtml"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContenidoHtml"].ToString()) : vAcuerdos.ContenidoHtml;
                            vAcuerdos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAcuerdos.UsuarioCrea;
                            vAcuerdos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAcuerdos.FechaCrea;
                            vAcuerdos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAcuerdos.UsuarioModifica;
                            vAcuerdos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAcuerdos.FechaModifica;
                        }
                        return vAcuerdos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Acuerdos
        /// </summary>
        /// <param name="pNombre">Nombre del Acuerdo</param>
        /// <returns>Lista de Entidades Acuerdos</returns>
        public List<Acuerdos> ConsultarAcuerdoss(String pNombre)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Acuerdoss_Consultar"))
                {
                    if (pNombre != null)
                        vDataBase.AddInParameter(vDbCommand, "@Nombre", DbType.String, pNombre);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Acuerdos> vListaAcuerdos = new List<Acuerdos>();
                        while (vDataReaderResults.Read())
                        {
                            Acuerdos vAcuerdos = new Acuerdos();
                            vAcuerdos.IdAcuerdo = vDataReaderResults["IdAcuerdo"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdAcuerdo"].ToString()) : vAcuerdos.IdAcuerdo;
                            vAcuerdos.Nombre = vDataReaderResults["Nombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Nombre"].ToString()) : vAcuerdos.Nombre;
                            vAcuerdos.ContenidoHtml = vDataReaderResults["ContenidoHtml"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ContenidoHtml"].ToString()) : vAcuerdos.ContenidoHtml;
                            vAcuerdos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vAcuerdos.UsuarioCrea;
                            vAcuerdos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vAcuerdos.FechaCrea;
                            vAcuerdos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vAcuerdos.UsuarioModifica;
                            vAcuerdos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vAcuerdos.FechaModifica;
                            vListaAcuerdos.Add(vAcuerdos);
                        }
                        return vListaAcuerdos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
