using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Documentos asociados al proveedor
    /// </summary>
    public class DocDatosBasicoProvDAL : GeneralDAL
    {
        public DocDatosBasicoProvDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, 18);
                    if (pDocDatosBasicoProv.IdEntidad>0)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pDocDatosBasicoProv.IdEntidad);

                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pDocDatosBasicoProv.NombreDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocDatosBasicoProv.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pDocDatosBasicoProv.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocDatosBasicoProv.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pDocDatosBasicoProv.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pDocDatosBasicoProv.IdTemporal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDocDatosBasicoProv.IdDocAdjunto = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocAdjunto").ToString());
                    GenerarLogAuditoria(pDocDatosBasicoProv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocDatosBasicoProv.IdDocAdjunto);

                    if (pDocDatosBasicoProv.IdEntidad > 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pDocDatosBasicoProv.IdEntidad);

                    
                    vDataBase.AddInParameter(vDbCommand, "@NombreDocumento", DbType.String, pDocDatosBasicoProv.NombreDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocDatosBasicoProv.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pDocDatosBasicoProv.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocDatosBasicoProv.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocDatosBasicoProv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocDatosBasicoProv.IdDocAdjunto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocDatosBasicoProv, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la entidad Documentos asociados al proveedor</param>
        /// <returns>Entidad Documentos asociados al proveedor</returns>
        public DocDatosBasicoProv ConsultarDocDatosBasicoProv(int pIdDocAdjunto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pIdDocAdjunto);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocDatosBasicoProv vDocDatosBasicoProv = new DocDatosBasicoProv();
                        while (vDataReaderResults.Read())
                        {
                            vDocDatosBasicoProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocDatosBasicoProv.IdDocAdjunto;
                            vDocDatosBasicoProv.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vDocDatosBasicoProv.IdEntidad;
                            vDocDatosBasicoProv.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocDatosBasicoProv.IdTipoDocumento;
                            vDocDatosBasicoProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocDatosBasicoProv.NombreDocumento;
                            vDocDatosBasicoProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocDatosBasicoProv.LinkDocumento; 
                            vDocDatosBasicoProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocDatosBasicoProv.Observaciones;
                            vDocDatosBasicoProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocDatosBasicoProv.UsuarioCrea;
                            vDocDatosBasicoProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocDatosBasicoProv.FechaCrea;
                            vDocDatosBasicoProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocDatosBasicoProv.UsuarioModifica;
                            vDocDatosBasicoProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocDatosBasicoProv.FechaModifica;
                            vDocDatosBasicoProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocDatosBasicoProv.IdTemporal;
                        }
                        return vDocDatosBasicoProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la entidad Documentos asociados al proveedor</param>
        /// <param name="pIdEntidad">Identificador del tipo de la entidad Documentos asociados al proveedor</param>
        /// <param name="pIdDocAdjunto">Nombre del documento en la entidad Documentos asociados al proveedor</param>
        /// <param name="pIdDocAdjunto">Observaciones en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicoProvs(int? pIdDocAdjunto, int? pIdEntidad, String pNombreDocumento, String pObservaciones)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar"))
                {
                     if(pIdEntidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                     using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocDatosBasicoProv> vListaDocDatosBasicoProv = new List<DocDatosBasicoProv>();
                        while (vDataReaderResults.Read())
                        {
                                DocDatosBasicoProv vDocDatosBasicoProv = new DocDatosBasicoProv();
                            vDocDatosBasicoProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocDatosBasicoProv.IdDocAdjunto;
                            vDocDatosBasicoProv.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vDocDatosBasicoProv.IdEntidad;
                            vDocDatosBasicoProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocDatosBasicoProv.NombreDocumento;
                            vDocDatosBasicoProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocDatosBasicoProv.LinkDocumento;
                            vDocDatosBasicoProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocDatosBasicoProv.Observaciones;
                            vDocDatosBasicoProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocDatosBasicoProv.UsuarioCrea;
                            vDocDatosBasicoProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocDatosBasicoProv.FechaCrea;
                            vDocDatosBasicoProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocDatosBasicoProv.UsuarioModifica;
                            vDocDatosBasicoProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocDatosBasicoProv.FechaModifica;
                            vDocDatosBasicoProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocDatosBasicoProv.IdTemporal;
                                vListaDocDatosBasicoProv.Add(vDocDatosBasicoProv);
                        }
                        return vListaDocDatosBasicoProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador del tipo en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoSector">Tipo de Sector en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicoProv_IdEntidad_TipoPersona(int pIdEntidad, string pTipoPersona, string pTipoSector, string pIdTemporal, string pTipoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdEntidad_TipoPersona_TipoSector"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);

                    if (pTipoSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoSector", DbType.String, pTipoSector);

                    if (pIdTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pIdTemporal);

                    if (pTipoEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoEntidad", DbType.String, pTipoEntidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocDatosBasicoProv> vListaDocFinancieraProv = new List<DocDatosBasicoProv>();
                        while (vDataReaderResults.Read())
                        {
                            DocDatosBasicoProv vDocDatosBasicoProv = new DocDatosBasicoProv();
                            vDocDatosBasicoProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocDatosBasicoProv.IdDocAdjunto;
                            vDocDatosBasicoProv.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vDocDatosBasicoProv.IdEntidad;
                            vDocDatosBasicoProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocDatosBasicoProv.NombreDocumento;
                            vDocDatosBasicoProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocDatosBasicoProv.LinkDocumento;
                            vDocDatosBasicoProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocDatosBasicoProv.Observaciones;
                            vDocDatosBasicoProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocDatosBasicoProv.UsuarioCrea;
                            vDocDatosBasicoProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocDatosBasicoProv.FechaCrea;
                            vDocDatosBasicoProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocDatosBasicoProv.UsuarioModifica;
                            vDocDatosBasicoProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocDatosBasicoProv.FechaModifica;
                            vDocDatosBasicoProv.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDocDatosBasicoProv.Obligatorio;
                            vDocDatosBasicoProv.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocDatosBasicoProv.IdTipoDocumento;
                            vDocDatosBasicoProv.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocDatosBasicoProv.MaxPermitidoKB;
                            vDocDatosBasicoProv.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocDatosBasicoProv.ExtensionesPermitidas;
                            vDocDatosBasicoProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocDatosBasicoProv.IdTemporal;

                            vListaDocFinancieraProv.Add(vDocDatosBasicoProv);
                        }
                        return vListaDocFinancieraProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoSector">Tipo de Sector en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicoProv_IdTemporal_TipoPersona(string pIdTemporal, string pTipoPersona, string pTipoSector, string pTipoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar_IdTemporal_TipoPersona_TipoSector"))
                {
                    if (pIdTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pIdTemporal );
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);

                    if (pTipoSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoSector", DbType.String, pTipoSector);
                    if(pTipoEntidad!=null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoEntidad", DbType.String, pTipoEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocDatosBasicoProv> vListaDocFinancieraProv = new List<DocDatosBasicoProv>();
                        while (vDataReaderResults.Read())
                        {
                            DocDatosBasicoProv vDocDatosBasicoProv = new DocDatosBasicoProv();
                            vDocDatosBasicoProv.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocDatosBasicoProv.IdDocAdjunto;
                            vDocDatosBasicoProv.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vDocDatosBasicoProv.IdEntidad;
                            vDocDatosBasicoProv.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocDatosBasicoProv.NombreDocumento;
                            vDocDatosBasicoProv.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocDatosBasicoProv.LinkDocumento;
                            vDocDatosBasicoProv.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDocDatosBasicoProv.Observaciones;
                            vDocDatosBasicoProv.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocDatosBasicoProv.UsuarioCrea;
                            vDocDatosBasicoProv.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocDatosBasicoProv.FechaCrea;
                            vDocDatosBasicoProv.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocDatosBasicoProv.UsuarioModifica;
                            vDocDatosBasicoProv.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocDatosBasicoProv.FechaModifica;
                            vDocDatosBasicoProv.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDocDatosBasicoProv.Obligatorio;
                            vDocDatosBasicoProv.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocDatosBasicoProv.IdTipoDocumento;
                            vDocDatosBasicoProv.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocDatosBasicoProv.MaxPermitidoKB;
                            vDocDatosBasicoProv.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocDatosBasicoProv.ExtensionesPermitidas;
                            vDocDatosBasicoProv.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocDatosBasicoProv.IdTemporal;

                            vListaDocFinancieraProv.Add(vDocDatosBasicoProv);
                        }
                        return vListaDocFinancieraProv;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
