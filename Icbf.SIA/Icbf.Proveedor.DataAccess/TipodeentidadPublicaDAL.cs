using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Entidad P�blica
    /// </summary>
    public class TipodeentidadPublicaDAL : GeneralDAL
    {
        public TipodeentidadPublicaDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad P�blica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipodeentidadPublica", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipodeentidadPublica", DbType.String, pTipodeentidadPublica.CodigoTipodeentidadPublica);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipodeentidadPublica.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipodeentidadPublica.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipodeentidadPublica.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipodeentidadPublica.IdTipodeentidadPublica = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipodeentidadPublica").ToString());
                    GenerarLogAuditoria(pTipodeentidadPublica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("NombreUnico"))
                {
                    throw new Exception("La descripci�n ingresada ya existe");
                }
                else if (ex.Message.Contains("CodigoUnico"))
                {
                    throw new Exception("El c�digo ingresado ya existe");
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad P�blica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeentidadPublica_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeentidadPublica", DbType.Int32, pTipodeentidadPublica.IdTipodeentidadPublica);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipodeentidadPublica", DbType.String, pTipodeentidadPublica.CodigoTipodeentidadPublica);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipodeentidadPublica.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipodeentidadPublica.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipodeentidadPublica.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipodeentidadPublica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("NombreUnico"))
                {
                    throw new Exception("La descripci�n ingresada ya existe");
                }
                else if (ex.Message.Contains("CodigoUnico"))
                {
                    throw new Exception("El c�digo ingresado ya existe");
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad P�blica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeentidadPublica", DbType.Int32, pTipodeentidadPublica.IdTipodeentidadPublica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipodeentidadPublica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pIdTipodeentidadPublica">Identificador en una entidad Tipo Entidad P�blica</param>
        /// <returns>Entidad Tipo Entidad P�blica</returns>
        public TipodeentidadPublica ConsultarTipodeentidadPublica(int pIdTipodeentidadPublica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeentidadPublica", DbType.Int32, pIdTipodeentidadPublica);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipodeentidadPublica vTipodeentidadPublica = new TipodeentidadPublica();
                        while (vDataReaderResults.Read())
                        {
                            vTipodeentidadPublica.IdTipodeentidadPublica = vDataReaderResults["IdTipodeentidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.IdTipodeentidadPublica;
                            vTipodeentidadPublica.CodigoTipodeentidadPublica = vDataReaderResults["CodigoTipodeentidadPublica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.CodigoTipodeentidadPublica;
                            vTipodeentidadPublica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeentidadPublica.Descripcion;
                            vTipodeentidadPublica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipodeentidadPublica.Estado;
                            vTipodeentidadPublica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipodeentidadPublica.UsuarioCrea;
                            vTipodeentidadPublica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipodeentidadPublica.FechaCrea;
                            vTipodeentidadPublica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipodeentidadPublica.UsuarioModifica;
                            vTipodeentidadPublica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipodeentidadPublica.FechaModifica;
                        }
                        return vTipodeentidadPublica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad P�blica
        /// </summary>
        /// <param name="pCodigoTipodeentidadPublica">C�digo en una entidad Tipo Entidad P�blica</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Entidad P�blica</param>
        /// <param name="pEstado">Estado en una entidad Tipo Entidad P�blica</param>
        /// <returns>Lista de entidades Tipo Entidad P�blica</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicas(String pCodigoTipodeentidadPublica, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar"))
                {
                    if(pCodigoTipodeentidadPublica != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipodeentidadPublica", DbType.String, pCodigoTipodeentidadPublica);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipodeentidadPublica> vListaTipodeentidadPublica = new List<TipodeentidadPublica>();
                        while (vDataReaderResults.Read())
                        {
                                TipodeentidadPublica vTipodeentidadPublica = new TipodeentidadPublica();
                            vTipodeentidadPublica.IdTipodeentidadPublica = vDataReaderResults["IdTipodeentidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.IdTipodeentidadPublica;
                            vTipodeentidadPublica.CodigoTipodeentidadPublica = vDataReaderResults["CodigoTipodeentidadPublica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.CodigoTipodeentidadPublica;
                            vTipodeentidadPublica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeentidadPublica.Descripcion;
                            vTipodeentidadPublica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipodeentidadPublica.Estado;
                            vTipodeentidadPublica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipodeentidadPublica.UsuarioCrea;
                            vTipodeentidadPublica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipodeentidadPublica.FechaCrea;
                            vTipodeentidadPublica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipodeentidadPublica.UsuarioModifica;
                            vTipodeentidadPublica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipodeentidadPublica.FechaModifica;
                                vListaTipodeentidadPublica.Add(vTipodeentidadPublica);
                        }
                        return vListaTipodeentidadPublica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad P�blica
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad P�blica</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicasAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeentidadPublicas_Consultar"))
                {
                        using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipodeentidadPublica> vListaTipodeentidadPublica = new List<TipodeentidadPublica>();
                        while (vDataReaderResults.Read())
                        {
                            TipodeentidadPublica vTipodeentidadPublica = new TipodeentidadPublica();
                            vTipodeentidadPublica.IdTipodeentidadPublica = vDataReaderResults["IdTipodeentidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.IdTipodeentidadPublica;
                            vTipodeentidadPublica.CodigoTipodeentidadPublica = vDataReaderResults["CodigoTipodeentidadPublica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.CodigoTipodeentidadPublica;
                            vTipodeentidadPublica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeentidadPublica.Descripcion;
                            vTipodeentidadPublica.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipodeentidadPublica.Estado;
                            vTipodeentidadPublica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipodeentidadPublica.UsuarioCrea;
                            vTipodeentidadPublica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipodeentidadPublica.FechaCrea;
                            vTipodeentidadPublica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipodeentidadPublica.UsuarioModifica;
                            vTipodeentidadPublica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipodeentidadPublica.FechaModifica;
                            vListaTipodeentidadPublica.Add(vTipodeentidadPublica);
                        }
                        return vListaTipodeentidadPublica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad P�blica por IdRama. IdNivelGob, IdNivelOrganizacional
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad P�blica</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicas_PorRamaNivelGobYNivelOrg(int idRama, int idNivelGob, int idNivelOrg)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarEntPublica"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, idRama);
                    vDataBase.AddInParameter(vDbCommand, "@IdNivelGobierno", DbType.Int32, idNivelGob);
                    vDataBase.AddInParameter(vDbCommand, "@IdNivelOrganizacional", DbType.Int32, idNivelOrg);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipodeentidadPublica> vListaTipodeentidadPublica = new List<TipodeentidadPublica>();
                        while (vDataReaderResults.Read())
                        {
                            TipodeentidadPublica vTipodeentidadPublica = new TipodeentidadPublica();
                            vTipodeentidadPublica.IdTipodeentidadPublica = vDataReaderResults["IdTipodeentidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.IdTipodeentidadPublica;
                            vTipodeentidadPublica.CodigoTipodeentidadPublica = vDataReaderResults["CodigoTipodeentidadPublica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeentidadPublica"].ToString()) : vTipodeentidadPublica.CodigoTipodeentidadPublica;
                            vTipodeentidadPublica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeentidadPublica.Descripcion;
                            vListaTipodeentidadPublica.Add(vTipodeentidadPublica);
                        }
                        return vListaTipodeentidadPublica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
