using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Formacion
    /// </summary>
    public class FormacionDAL : GeneralDAL
    {
        public FormacionDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int InsertarFormacion(Formacion pFormacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Formacion_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdFormacion", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pFormacion.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoModalidad", DbType.String, pFormacion.CodigoModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, pFormacion.IdProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@EsFormacionPrincipal", DbType.Boolean, pFormacion.EsFormacionPrincipal);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFormacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pFormacion.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pFormacion.IdFormacion = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdFormacion").ToString());
                    GenerarLogAuditoria(pFormacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int ModificarFormacion(Formacion pFormacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Formacion_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFormacion", DbType.Int32, pFormacion.IdFormacion);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pFormacion.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoModalidad", DbType.String, pFormacion.CodigoModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, pFormacion.IdProfesion);
                    vDataBase.AddInParameter(vDbCommand, "@EsFormacionPrincipal", DbType.Boolean, pFormacion.EsFormacionPrincipal);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pFormacion.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pFormacion.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFormacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int EliminarFormacion(Formacion pFormacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Formacion_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdFormacion", DbType.Int32, pFormacion.IdFormacion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pFormacion, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad Formacion
        /// </summary>
        /// <param name="pIdFormacion"></param>
        public Formacion ConsultarFormacion(int pIdFormacion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Formacion_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdFormacion", DbType.Int32, pIdFormacion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Formacion vFormacion = new Formacion();
                        while (vDataReaderResults.Read())
                        {
                            vFormacion.IdFormacion = vDataReaderResults["IdFormacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormacion"].ToString()) : vFormacion.IdFormacion;
                            vFormacion.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vFormacion.IdEntidad;
                            vFormacion.CodigoModalidad = vDataReaderResults["CodigoModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoModalidad"].ToString()) : vFormacion.CodigoModalidad;
                            vFormacion.IdProfesion = vDataReaderResults["IdProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdProfesion"].ToString()) : vFormacion.IdProfesion;
                            vFormacion.EsFormacionPrincipal = vDataReaderResults["EsFormacionPrincipal"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFormacionPrincipal"].ToString()) : vFormacion.EsFormacionPrincipal;
                            vFormacion.tipoFormacion = vDataReaderResults["formacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["formacion"].ToString()) : vFormacion.tipoFormacion;
                            vFormacion.nombreModalidadAcademica = vDataReaderResults["ModalidadAcademica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadAcademica"].ToString()) : vFormacion.nombreModalidadAcademica;
                            vFormacion.nombreProfesion = vDataReaderResults["Profesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Profesion"].ToString()) : vFormacion.nombreProfesion;
                            vFormacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFormacion.Estado;
                            vFormacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFormacion.UsuarioCrea;
                            vFormacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFormacion.FechaCrea;
                            vFormacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFormacion.UsuarioModifica;
                            vFormacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFormacion.FechaModifica;
                        }
                        return vFormacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Formacion
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pCodigoModalidad"></param>
        /// <param name="pIdProfesion"></param>
        /// <param name="pEsFormacionPrincipal"></param>
        /// <param name="pEstado"></param>
        public List<Formacion> ConsultarFormacions(int? pIdEntidad, String pCodigoModalidad, String pIdProfesion, Boolean? pEsFormacionPrincipal, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Formacions_Consultar"))
                {
                    if(pIdEntidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if(pCodigoModalidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoModalidad", DbType.String, pCodigoModalidad);
                    if(pIdProfesion != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdProfesion", DbType.String, pIdProfesion);
                    if(pEsFormacionPrincipal != null)
                         vDataBase.AddInParameter(vDbCommand, "@EsFormacionPrincipal", DbType.Boolean, pEsFormacionPrincipal);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Formacion> vListaFormacion = new List<Formacion>();
                        while (vDataReaderResults.Read())
                        {
                                Formacion vFormacion = new Formacion();
                            vFormacion.IdFormacion = vDataReaderResults["IdFormacion"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdFormacion"].ToString()) : vFormacion.IdFormacion;
                            vFormacion.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vFormacion.IdEntidad;
                            vFormacion.CodigoModalidad = vDataReaderResults["CodigoModalidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoModalidad"].ToString()) : vFormacion.CodigoModalidad;
                            vFormacion.IdProfesion = vDataReaderResults["IdProfesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdProfesion"].ToString()) : vFormacion.IdProfesion;
                            vFormacion.EsFormacionPrincipal = vDataReaderResults["EsFormacionPrincipal"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EsFormacionPrincipal"].ToString()) : vFormacion.EsFormacionPrincipal;
                            vFormacion.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vFormacion.Estado;
                            vFormacion.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vFormacion.UsuarioCrea;
                            vFormacion.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vFormacion.FechaCrea;
                            vFormacion.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vFormacion.UsuarioModifica;
                            vFormacion.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vFormacion.FechaModifica;

                            vFormacion.tipoFormacion = vDataReaderResults["formacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["formacion"].ToString()) : vFormacion.tipoFormacion;
                            vFormacion.ModalidadAcademica = vDataReaderResults["ModalidadAcademica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ModalidadAcademica"].ToString()) : vFormacion.ModalidadAcademica;
                            vFormacion.Profesion = vDataReaderResults["Profesion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Profesion"].ToString()) : vFormacion.Profesion;

                            vListaFormacion.Add(vFormacion);
                        }
                        return vListaFormacion;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
