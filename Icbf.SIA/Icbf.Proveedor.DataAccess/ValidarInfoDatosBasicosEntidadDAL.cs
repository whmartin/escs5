using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad de validaci�n de datos b�sicos del proveedor
    /// </summary>
    public class ValidarInfoDatosBasicosEntidadDAL : GeneralDAL
    {
        public ValidarInfoDatosBasicosEntidadDAL()
        {
        }

        /// <summary>
        /// Insertar una entidad Validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValidarInfoDatosBasicosEntidad(ValidarInfoDatosBasicosEntidad pValidarInfoDatosBasicosEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Insertar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValidarInfoDatosBasicosEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoDatosBasicosEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pValidarInfoDatosBasicosEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoDatosBasicosEntidad.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoDatosBasicosEntidad.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoDatosBasicosEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValidarInfoDatosBasicosEntidad").ToString());
                    //GenerarLogAuditoria(pValidarInfoDatosBasicosEntidad, vDbCommand);
                    //return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdEntidad", DbType.String, pValidarInfoDatosBasicosEntidad.IdEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoDatosBasicosEntidad", DbType.Int16, idEstadoInfoDatosBasicosEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoDatosBasicosEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        // GenerarLogAuditoria(pValidarInfoDatosBasicosEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarValidarInfoDatosBasicosEntidad(ValidarInfoDatosBasicosEntidad pValidarInfoDatosBasicosEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Modificar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoDatosBasicosEntidad", DbType.Int32, pValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoDatosBasicosEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pValidarInfoDatosBasicosEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoDatosBasicosEntidad.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoDatosBasicosEntidad.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoDatosBasicosEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);

                    GenerarLogAuditoria(pValidarInfoDatosBasicosEntidad, vDbCommand);

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdEntidad", DbType.String, pValidarInfoDatosBasicosEntidad.IdEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoDatosBasicosEntidad", DbType.Int16, idEstadoInfoDatosBasicosEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoDatosBasicosEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        GenerarLogAuditoria(pValidarInfoDatosBasicosEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdValidarInfoDatosBasicosEntidad">Identificador en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Entidad validaci�n de datos b�sicos del proveedor</returns>
        public ValidarInfoDatosBasicosEntidad ConsultarValidarInfoDatosBasicosEntidad(int pIdValidarInfoDatosBasicosEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoDatosBasicosEntidad", DbType.Int32, pIdValidarInfoDatosBasicosEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad = vDataReaderResults["IdValidarInfoDatosBasicosEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoDatosBasicosEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad;
                            vValidarInfoDatosBasicosEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdEntidad;
                            vValidarInfoDatosBasicosEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoDatosBasicosEntidad.NroRevision;
                            vValidarInfoDatosBasicosEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoDatosBasicosEntidad.Observaciones;
                            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba;
                            vValidarInfoDatosBasicosEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.UsuarioCrea;
                            vValidarInfoDatosBasicosEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.FechaCrea;
                        }
                        return vValidarInfoDatosBasicosEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Lista de entidades validaci�n de datos b�sicos del proveedor</returns>
        public List<ValidarInfoDatosBasicosEntidad> ConsultarValidarInfoDatosBasicosEntidads(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pObservaciones != null)
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if (pConfirmaYAprueba != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoDatosBasicosEntidad> vListaValidarTercero = new List<ValidarInfoDatosBasicosEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();

                            vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad = vDataReaderResults["IdValidarInfoDatosBasicosEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoDatosBasicosEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad;
                            vValidarInfoDatosBasicosEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdEntidad;
                            vValidarInfoDatosBasicosEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoDatosBasicosEntidad.NroRevision;
                            vValidarInfoDatosBasicosEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoDatosBasicosEntidad.Observaciones;
                            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba;
                            vValidarInfoDatosBasicosEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.UsuarioCrea;
                            vValidarInfoDatosBasicosEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoDatosBasicosEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Lista de entidades validaci�n de datos b�sicos del proveedor</returns>
        public List<ValidarInfoDatosBasicosEntidad> ConsultarValidarInfoDatosBasicosEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pObservaciones != null)
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if (pConfirmaYAprueba != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoDatosBasicosEntidad> vListaValidarTercero = new List<ValidarInfoDatosBasicosEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();

                            vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad = vDataReaderResults["IdValidarInfoDatosBasicosEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoDatosBasicosEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad;
                            vValidarInfoDatosBasicosEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdEntidad;
                            vValidarInfoDatosBasicosEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoDatosBasicosEntidad.NroRevision;
                            vValidarInfoDatosBasicosEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoDatosBasicosEntidad.Observaciones;
                            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba;
                            vValidarInfoDatosBasicosEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.UsuarioCrea;
                            vValidarInfoDatosBasicosEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoDatosBasicosEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Entidad validaci�n de datos b�sicos del proveedor</returns>
        public ValidarInfoDatosBasicosEntidad ConsultarValidarInfoDatosBasicosEntidad_Ultima(Int32 pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad = vDataReaderResults["IdValidarInfoDatosBasicosEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoDatosBasicosEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdValidarInfoDatosBasicosEntidad;
                            vValidarInfoDatosBasicosEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoDatosBasicosEntidad.IdEntidad;
                            vValidarInfoDatosBasicosEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoDatosBasicosEntidad.NroRevision;
                            vValidarInfoDatosBasicosEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoDatosBasicosEntidad.Observaciones;
                            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba;
                            vValidarInfoDatosBasicosEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.UsuarioCrea;
                            vValidarInfoDatosBasicosEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoDatosBasicosEntidad.FechaCrea;
                        }
                        return vValidarInfoDatosBasicosEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public bool ModificarConfirmaYApreba_InfoDatosBasicosEntidad(int idEntidad)
        {
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ModificarInfoDatosBasicosEntidad_ConfirmaYAprueba"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, idEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    if (vResultado > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (UserInterfaceException)
            {
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }
    }
}
