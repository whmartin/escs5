﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para Consultar entidad  Estado Proveedor
    /// </summary>
    public class EstadoProveedorDAL : GeneralDAL
    {
        public EstadoProveedorDAL()
        {
        }

        /// <summary>
        /// Consulta una entidad Estado Proveedor
        /// </summary>
        /// <param name="pDescripcion">Descripción del EstadoProveedor a Consultar</param>
        /// <returns>Entidad Estado Proveedor</returns>
        public EstadoProveedor ConsultarEstadoProveedor(string pDescripcion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@descripcionEstado", DbType.String, pDescripcion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoProveedor vEstadoProveedor = new EstadoProveedor();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoProveedor.IdEstadoProveedor = vDataReaderResults["IdEstadoProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProveedor"].ToString()) : vEstadoProveedor.IdEstadoProveedor;
                            vEstadoProveedor.DescripcionEstado = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoProveedor.DescripcionEstado;
                            vEstadoProveedor.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoProveedor.Estado;
                        }
                        return vEstadoProveedor;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }

}
