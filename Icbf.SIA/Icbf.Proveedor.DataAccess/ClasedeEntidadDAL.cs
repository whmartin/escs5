using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Clase Entidad
    /// </summary>
    public class ClasedeEntidadDAL : GeneralDAL

    {
        public ClasedeEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdClasedeEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pClasedeEntidad.IdTipodeActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pClasedeEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pClasedeEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pClasedeEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pClasedeEntidad.IdClasedeEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdClasedeEntidad").ToString());
                    GenerarLogAuditoria(pClasedeEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdClasedeEntidad", DbType.Int32, pClasedeEntidad.IdClasedeEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pClasedeEntidad.IdTipodeActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pClasedeEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pClasedeEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pClasedeEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pClasedeEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdClasedeEntidad", DbType.Int32, pClasedeEntidad.IdClasedeEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pClasedeEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una Clase Entidad
        /// </summary>
        /// <param name="pIdClasedeEntidad">Identificador de una Clase Entidad</param>
        /// <returns>Entidad Clase Entidad</returns>
        public ClasedeEntidad ConsultarClasedeEntidad(int pIdClasedeEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdClasedeEntidad", DbType.Int32, pIdClasedeEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ClasedeEntidad vClasedeEntidad = new ClasedeEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vClasedeEntidad.IdClasedeEntidad = vDataReaderResults["IdClasedeEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasedeEntidad"].ToString()) : vClasedeEntidad.IdClasedeEntidad;
                            vClasedeEntidad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vClasedeEntidad.IdTipodeActividad;
                            vClasedeEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vClasedeEntidad.Descripcion;
                            vClasedeEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClasedeEntidad.Estado;
                            vClasedeEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClasedeEntidad.UsuarioCrea;
                            vClasedeEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClasedeEntidad.FechaCrea;
                            vClasedeEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClasedeEntidad.UsuarioModifica;
                            vClasedeEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClasedeEntidad.FechaModifica;
                        }
                        return vClasedeEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pDescripcion">Descripci�n de una Clase Entidad</param>
        /// <param name="pEstado">Estado de la Clase Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidads(int? pIdTipodeActividad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidads_Consultar"))
                {
                    if(pIdTipodeActividad != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pIdTipodeActividad);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClasedeEntidad> vListaClasedeEntidad = new List<ClasedeEntidad>();
                        while (vDataReaderResults.Read())
                        {
                                ClasedeEntidad vClasedeEntidad = new ClasedeEntidad();
                            vClasedeEntidad.IdClasedeEntidad = vDataReaderResults["IdClasedeEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasedeEntidad"].ToString()) : vClasedeEntidad.IdClasedeEntidad;
                            vClasedeEntidad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vClasedeEntidad.IdTipodeActividad;
                            vClasedeEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vClasedeEntidad.Descripcion;
                            vClasedeEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClasedeEntidad.Estado;
                            vClasedeEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClasedeEntidad.UsuarioCrea;
                            vClasedeEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClasedeEntidad.FechaCrea;
                            vClasedeEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClasedeEntidad.UsuarioModifica;
                            vClasedeEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClasedeEntidad.FechaModifica;
                            vClasedeEntidad.TipodeActividad = vDataReaderResults["TipodeActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["TipodeActividad"].ToString()) : vClasedeEntidad.Descripcion;
                            vListaClasedeEntidad.Add(vClasedeEntidad);
                        }
                        return vListaClasedeEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidadAll(int? pIdTipodeActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarALL"))
                {
                    if (pIdTipodeActividad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pIdTipodeActividad);
                      using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClasedeEntidad> vListaClasedeEntidad = new List<ClasedeEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ClasedeEntidad vClasedeEntidad = new ClasedeEntidad();
                            vClasedeEntidad.IdClasedeEntidad = vDataReaderResults["IdClasedeEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasedeEntidad"].ToString()) : vClasedeEntidad.IdClasedeEntidad;
                            vClasedeEntidad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vClasedeEntidad.IdTipodeActividad;
                            vClasedeEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vClasedeEntidad.Descripcion;
                            vClasedeEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClasedeEntidad.Estado;
                            vClasedeEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClasedeEntidad.UsuarioCrea;
                            vClasedeEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClasedeEntidad.FechaCrea;
                            vClasedeEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClasedeEntidad.UsuarioModifica;
                            vClasedeEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClasedeEntidad.FechaModifica;
                            vListaClasedeEntidad.Add(vClasedeEntidad);
                        }
                        return vListaClasedeEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pIdTipoEntidad">Identificador del Tipo de Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        /// 
        public List<ClasedeEntidad> ConsultarClasedeEntidadTipodeActividadTipoEntidad(int? pIdTipodeActividad, int? pIdTipoEntidad, int? pIdTipoPersona, int? pIdSector, int? pIdRegmenTributario)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ClasedeEntidads_ConsultarTipodeActividadTipoEntidad"))
                {
                    if (pIdTipodeActividad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pIdTipodeActividad);
                    if (pIdTipoEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidad", DbType.Int32, pIdTipoEntidad);
                    if(pIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    if (pIdSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, pIdSector);
                    if (pIdRegmenTributario != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRegimenTributario ", DbType.Int32, pIdRegmenTributario);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ClasedeEntidad> vListaClasedeEntidad = new List<ClasedeEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ClasedeEntidad vClasedeEntidad = new ClasedeEntidad();
                            vClasedeEntidad.IdClasedeEntidad = vDataReaderResults["IdClasedeEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdClasedeEntidad"].ToString()) : vClasedeEntidad.IdClasedeEntidad;
                            vClasedeEntidad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vClasedeEntidad.IdTipodeActividad;
                            vClasedeEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vClasedeEntidad.Descripcion;
                            vClasedeEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vClasedeEntidad.Estado;
                            vClasedeEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vClasedeEntidad.UsuarioCrea;
                            vClasedeEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vClasedeEntidad.FechaCrea;
                            vClasedeEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vClasedeEntidad.UsuarioModifica;
                            vClasedeEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vClasedeEntidad.FechaModifica;
                            vListaClasedeEntidad.Add(vClasedeEntidad);
                        }
                        return vListaClasedeEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
