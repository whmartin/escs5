﻿using Icbf.Oferente.Entity;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.DataAccess
{
    public class ReporteTercerosImportadosDAL : GeneralDAL
    {
        /// <summary>
        /// Consultar Municipios X departamento(s)
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public List<ExperienciaMunicipio> ConsultarMunicipiosXDepto(string pIdDepartamento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ReoprteTercerosMunicipio_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.String, pIdDepartamento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ExperienciaMunicipio> vListaExperienciaMunicipio = new List<ExperienciaMunicipio>();
                        while (vDataReaderResults.Read())
                        {
                            ExperienciaMunicipio vExperienciaMunicipio = new ExperienciaMunicipio();
                            vExperienciaMunicipio.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vExperienciaMunicipio.IdMunicipio;
                            vExperienciaMunicipio.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString().Trim().ToUpper()) : vExperienciaMunicipio.NombreMunicipio;
                            vListaExperienciaMunicipio.Add(vExperienciaMunicipio);
                        }
                        return vListaExperienciaMunicipio;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar datos de terceros importados y retorna un tru si hay datos y false si no.
        /// </summary>
        /// <param name="vReporte"></param>
        /// <returns></returns>
        public bool ConsultarDatosReporte(ReporteTercerosImportados vReporte)
        {
            bool vResult = false;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ReoprteTercerosImportados_Consultar"))
                {
                    if (!string.IsNullOrEmpty(vReporte.ClaseActividad))
                        vDataBase.AddInParameter(vDbCommand, "@IdClaseActividad", DbType.String, vReporte.ClaseActividad);
                    if (!string.IsNullOrEmpty(vReporte.NivelInteres))
                        vDataBase.AddInParameter(vDbCommand, "@NivelInteres", DbType.String, vReporte.NivelInteres);
                    if (!string.IsNullOrEmpty(vReporte.Departamento))
                        vDataBase.AddInParameter(vDbCommand, "@IdDepartamentos", DbType.String, vReporte.Departamento);
                    if (!string.IsNullOrEmpty(vReporte.Municipio))
                        vDataBase.AddInParameter(vDbCommand, "@IdMunicipios", DbType.String, vReporte.Municipio);
                    if (!string.IsNullOrEmpty(vReporte.NumeroIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, vReporte.NumeroIdentificacion);
                    if (!string.IsNullOrEmpty(vReporte.NombreRazonSocial))
                        vDataBase.AddInParameter(vDbCommand, "@NombrerazonSocial", DbType.String, vReporte.NombreRazonSocial);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            int valor = vDataReaderResults["NUMEROIDENTIFICACION"] != DBNull.Value ? 1 : 0;
                            vResult = valor == 1 ? true : false;
                            break;
                        }
                        return vResult;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
