using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Estado Tercero
    /// </summary>
    public class EstadoTerceroDAL : GeneralDAL
    {
        public EstadoTerceroDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero">Entidad Estado Tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarEstadoTercero(EstadoTercero pEstadoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoTercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoTercero", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadotercero", DbType.String, pEstadoTercero.CodigoEstadotercero);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionEstado", DbType.String, pEstadoTercero.DescripcionEstado);
                    vDataBase.AddOutParameter(vDbCommand, "@Estado", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadoTercero.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadoTercero.IdEstadoTercero = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEstadoTercero").ToString());
                    pEstadoTercero.Estado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@Estado").ToString());
                    GenerarLogAuditoria(pEstadoTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero">Entidad Estado Tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEstadoTercero(EstadoTercero pEstadoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoTercero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTercero", DbType.Int32, pEstadoTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoEstadotercero", DbType.String, pEstadoTercero.CodigoEstadotercero);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionEstado", DbType.String, pEstadoTercero.DescripcionEstado);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstadoTercero.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadoTercero.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero">Entidad Estado Tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEstadoTercero(EstadoTercero pEstadoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoTercero_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTercero", DbType.Int32, pEstadoTercero.IdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstadoTercero.Estado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado Tercero
        /// </summary>
        /// <param name="pIdEstadoTercero">Identificador de una entidad Estado Tercero</param>
        /// <param name="pEstado">Estado de una entidad Estado Tercero</param>
        /// <returns>Entidad Estado Tercero</returns>
        public EstadoTercero ConsultarEstadoTercero(int pIdEstadoTercero, int pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoTercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoTercero", DbType.Int32, pIdEstadoTercero);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Int32, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoTercero vEstadoTercero = new EstadoTercero();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoTercero.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vEstadoTercero.IdEstadoTercero;
                            vEstadoTercero.CodigoEstadotercero = vDataReaderResults["CodigoEstadotercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadotercero"].ToString()) : vEstadoTercero.CodigoEstadotercero;
                            vEstadoTercero.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vEstadoTercero.DescripcionEstado;
                            vEstadoTercero.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstadoTercero.Estado;
                            vEstadoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoTercero.UsuarioCrea;
                            vEstadoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoTercero.FechaCrea;
                            vEstadoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoTercero.UsuarioModifica;
                            vEstadoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoTercero.FechaModifica;
                        }
                        return vEstadoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Tercero
        /// </summary>
        /// <param name="pIdEstadoTercero">Identificador de una entidad Estado Tercero</param>
        /// <returns>Lista de entidades Estado Tercero</returns>
        public List<EstadoTercero> ConsultarEstadoTerceros(int? pIdEstadoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoTerceros_Consultar"))
                {
                    if(pIdEstadoTercero != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdEstadoTercero", DbType.Int32, pIdEstadoTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoTercero> vListaEstadoTercero = new List<EstadoTercero>();
                        while (vDataReaderResults.Read())
                        {
                                EstadoTercero vEstadoTercero = new EstadoTercero();
                            vEstadoTercero.IdEstadoTercero = vDataReaderResults["IdEstadoTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoTercero"].ToString()) : vEstadoTercero.IdEstadoTercero;
                            vEstadoTercero.CodigoEstadotercero = vDataReaderResults["CodigoEstadotercero"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoEstadotercero"].ToString()) : vEstadoTercero.CodigoEstadotercero;
                            vEstadoTercero.DescripcionEstado = vDataReaderResults["DescripcionEstado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionEstado"].ToString()) : vEstadoTercero.DescripcionEstado;
                            vEstadoTercero.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Estado"].ToString()) : vEstadoTercero.Estado;
                            vEstadoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoTercero.UsuarioCrea;
                            vEstadoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoTercero.FechaCrea;
                            vEstadoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoTercero.UsuarioModifica;
                            vEstadoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoTercero.FechaModifica;
                                vListaEstadoTercero.Add(vEstadoTercero);
                        }
                        return vListaEstadoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un motivo de cambio de estado de Proveedor
        /// </summary>
        /// <param name="idTercero">Identificador del Tercero</param>
        /// <param name="datosBasicos">Si tiene Datos b�sicos</param>
        /// <param name="financiera">Si tiene informaci�n financiera</param>
        /// <param name="experiencia">Si tiene experiencia</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <param name="motivo">Motivo del cambio</param>
        /// <param name="usuario">Usuario que crea la entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarMotivoCambioEstado(int? idTercero,bool datosBasicos,bool financiera, bool experiencia, bool integrantes, string IdTemporal, string motivo,string usuario)
        {
            try
            {
                Database vDataBase =  ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar"))
                {
                    int vResultado=0;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, idTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, IdTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@DatosBasicos", DbType.Boolean, datosBasicos);
                    vDataBase.AddInParameter(vDbCommand, "@Financiera", DbType.Boolean, financiera);
                    vDataBase.AddInParameter(vDbCommand, "@Experiencia", DbType.Boolean, experiencia);
                    vDataBase.AddInParameter(vDbCommand, "@Integrantes", DbType.Boolean, integrantes);
                    vDataBase.AddInParameter(vDbCommand, "@Motivo", DbType.String, motivo );
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = int.Parse(idTercero.ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un motivo de cambio de estado de Tercero
        /// </summary>
        /// <param name="idTercero">Identificador del Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <param name="motivo">Motivo del cambio</param>
        /// <param name="usuario">Usuario que crea la entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarMotivoCambioEstadoTercero(int? idTercero, string IdTemporal, string motivo, string usuario)
        {//gfnfx
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, idTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, IdTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@Motivo", DbType.String, motivo);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, usuario);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = int.Parse(idTercero.ToString());
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Env�a correos por cambio de estado
        /// </summary>
        /// <param name="Tercero">Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <param name="correos">correos</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EnviarMailsCambioEstadoTerceroProveedor(string Tercero, string IdTemporal,string correos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EnviarCorreo_CambioEstadoTercero"))
                {
                    int vResultado = 0;
                    vDataBase.AddInParameter(vDbCommand, "@Tercero", DbType.String, Tercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, IdTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@correos", DbType.String, correos);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el cuerpo de correo por cambio de estado
        /// </summary>
        /// <param name="Tercero">Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <returns>string de Cuero de Correo</returns>
        public string ObtenerCuerpoDeCorreo_CambioEstadoTerceroProveedor(string Tercero, string IdTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstructurarCorreo_CambioEstadoTercero"))
                {
                    string  vResultado = "";
                    vDataBase.AddInParameter(vDbCommand, "@Tercero", DbType.String, Tercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, IdTemporal);


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vResultado = vDataReaderResults["Body"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Body"].ToString()) : "Nothing";
                        }
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Obtiene correos 
        /// </summary>
        /// <param name="ApplicationName">Nombre del ApplicationName</param>
        /// <param name="RoleName">Nombre del RoleName</param>
        /// <returns>Correo en proveedores</returns>
        public string ObtenerMailsGestorProveedor(string ApplicationName, string RoleName)
        {
            try
            {
                Database vDataBase = ObtenerInstanciaQoS();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_ObtenerMailsGestorProveedores"))
                {
                    string vResultado = "";
                    vDataBase.AddInParameter(vDbCommand, "@ApplicationName", DbType.String, ApplicationName);
                    vDataBase.AddInParameter(vDbCommand, "@RoleName", DbType.String, RoleName);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        while (vDataReaderResults.Read())
                        {
                            vResultado = vDataReaderResults["correo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["correo"].ToString()) : "Nothing";
                        }
                    }
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
