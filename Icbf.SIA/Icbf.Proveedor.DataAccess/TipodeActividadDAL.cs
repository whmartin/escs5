using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Actividad
    /// </summary>
    public class TipodeActividadDAL : GeneralDAL
    {
        public TipodeActividadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeActividad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipodeActividad", DbType.String, pTipodeActividad.CodigoTipodeActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipodeActividad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipodeActividad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipodeActividad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipodeActividad.IdTipodeActividad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipodeActividad").ToString());
                    GenerarLogAuditoria(pTipodeActividad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeActividad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pTipodeActividad.IdTipodeActividad);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipodeActividad", DbType.String, pTipodeActividad.CodigoTipodeActividad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipodeActividad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipodeActividad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipodeActividad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipodeActividad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeActividad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pTipodeActividad.IdTipodeActividad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipodeActividad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Actividad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador en una entidad Tipo Actividad</param>
        /// <returns>Entidad Tipo Actividad</returns>
        public TipodeActividad ConsultarTipodeActividad(int pIdTipodeActividad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeActividad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipodeActividad", DbType.Int32, pIdTipodeActividad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipodeActividad vTipodeActividad = new TipodeActividad();
                        while (vDataReaderResults.Read())
                        {
                            vTipodeActividad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vTipodeActividad.IdTipodeActividad;
                            vTipodeActividad.CodigoTipodeActividad = vDataReaderResults["CodigoTipodeActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeActividad"].ToString()) : vTipodeActividad.CodigoTipodeActividad;
                            vTipodeActividad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeActividad.Descripcion;
                            vTipodeActividad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipodeActividad.Estado;
                            vTipodeActividad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipodeActividad.UsuarioCrea;
                            vTipodeActividad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipodeActividad.FechaCrea;
                            vTipodeActividad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipodeActividad.UsuarioModifica;
                            vTipodeActividad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipodeActividad.FechaModifica;
                        }
                        return vTipodeActividad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Actividad
        /// </summary>
        /// <param name="pCodigoTipodeActividad">C�digo en una entidad Tipo Actividad</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Actividad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Actividad</param>
        /// <returns>Lista de entidades Tipo Actividad</returns>
        public List<TipodeActividad> ConsultarTipodeActividads(String pCodigoTipodeActividad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeActividads_Consultar"))
                {
                    if(pCodigoTipodeActividad != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipodeActividad", DbType.String, pCodigoTipodeActividad);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipodeActividad> vListaTipodeActividad = new List<TipodeActividad>();
                        while (vDataReaderResults.Read())
                        {
                                TipodeActividad vTipodeActividad = new TipodeActividad();
                            vTipodeActividad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vTipodeActividad.IdTipodeActividad;
                            vTipodeActividad.CodigoTipodeActividad = vDataReaderResults["CodigoTipodeActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeActividad"].ToString()) : vTipodeActividad.CodigoTipodeActividad;
                            vTipodeActividad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeActividad.Descripcion;
                            vTipodeActividad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipodeActividad.Estado;
                            vTipodeActividad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipodeActividad.UsuarioCrea;
                            vTipodeActividad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipodeActividad.FechaCrea;
                            vTipodeActividad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipodeActividad.UsuarioModifica;
                            vTipodeActividad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipodeActividad.FechaModifica;
                                vListaTipodeActividad.Add(vTipodeActividad);
                        }
                        return vListaTipodeActividad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Actividad
        /// </summary>
        /// <returns>Lista de entidades Tipo Actividad</returns>
        public List<TipodeActividad> ConsultarTipodeActividadAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipodeActividad_ConsultarAll"))
                {
                          using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipodeActividad> vListaTipodeActividad = new List<TipodeActividad>();
                        while (vDataReaderResults.Read())
                        {
                            TipodeActividad vTipodeActividad = new TipodeActividad();
                            vTipodeActividad.IdTipodeActividad = vDataReaderResults["IdTipodeActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipodeActividad"].ToString()) : vTipodeActividad.IdTipodeActividad;
                            vTipodeActividad.CodigoTipodeActividad = vDataReaderResults["CodigoTipodeActividad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipodeActividad"].ToString()) : vTipodeActividad.CodigoTipodeActividad;
                            vTipodeActividad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipodeActividad.Descripcion;
                            vTipodeActividad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipodeActividad.Estado;
                            vTipodeActividad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipodeActividad.UsuarioCrea;
                            vTipodeActividad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipodeActividad.FechaCrea;
                            vTipodeActividad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipodeActividad.UsuarioModifica;
                            vTipodeActividad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipodeActividad.FechaModifica;
                            vListaTipodeActividad.Add(vTipodeActividad);
                        }
                        return vListaTipodeActividad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
