using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad de validaci�n del proveedor
    /// </summary>
    public class ValidarProveedorDAL : GeneralDAL
    {
        public ValidarProveedorDAL()
        {
        }
        
        /// <summary>
        /// Consulta una lista de entidades Validaci�n de Informaci�n
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <returns>Lista de entidades Validaci�n de Informaci�n</returns>
        public List<ValidarInfo> ConsultarValidarModulosResumen(Int32 pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarModulosResumen_Consultar"))
                {
                    if (pIdEntidad != 0)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfo> vListaValidarInfo = new List<ValidarInfo>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfo vValidarInfo = new ValidarInfo();
                            vValidarInfo.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfo.NroRevision;
                            vValidarInfo.Componente = vDataReaderResults["Componente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Componente"].ToString()) : vValidarInfo.Componente;
                            //vValidarInfo.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfo.Observaciones;
                            vValidarInfo.iConfirmaYAprueba = vDataReaderResults["iConfirmaYAprueba"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["iConfirmaYAprueba"].ToString()) : vValidarInfo.iConfirmaYAprueba;
                            //vValidarInfo.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfo.UsuarioCrea;
                            //vValidarInfo.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfo.FechaCrea;
                            vValidarInfo.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vValidarInfo.Finalizado;
                            vValidarInfo.Liberar = vDataReaderResults["Liberar"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Liberar"]) : vValidarInfo.Liberar;
                            vValidarInfo.CorreoEnviado = vDataReaderResults["iCorreoEnviado"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["iCorreoEnviado"].ToString()) : vValidarInfo.CorreoEnviado;
                            vListaValidarInfo.Add(vValidarInfo);
                        }
                        return vListaValidarInfo;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica M�dulo resumen
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ValidarModulosResumen_FinalizarRevision(Int32 pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarModulosResumen_FinalizarRevision"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    EntidadProvOferenteDal objEntidadProvOferenteDal= new EntidadProvOferenteDal();
                    EntidadProvOferente entidadValidada = objEntidadProvOferenteDal.ConsultarEntidadProvOferente(pIdEntidad);
                    GenerarLogAuditoria(entidadValidada, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        
    }
}
