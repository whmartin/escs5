using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad de validación de información financiera del proveedor
    /// </summary>
    public class ValidarInfoFinancieraEntidadDAL : GeneralDAL
    {
        public ValidarInfoFinancieraEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pValidarInfoFinancieraEntidad">Entidad Validación de información financiera del proveedor</param>
        /// <param name="idEstadoInfoFinancieraEntidad">Identificador del estado en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Nůmero de registros afectados</returns>
        public int InsertarValidarInfoFinancieraEntidad(ValidarInfoFinancieraEntidad pValidarInfoFinancieraEntidad, int idEstadoInfoFinancieraEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Insertar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValidarInfoFinancieraEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoFinancieraEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pValidarInfoFinancieraEntidad.IdInfoFin);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoFinancieraEntidad.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoFinancieraEntidad.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoFinancieraEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValidarInfoFinancieraEntidad").ToString());
                    GenerarLogAuditoria(pValidarInfoFinancieraEntidad, vDbCommand);
                    //return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdInfoFin", DbType.String, pValidarInfoFinancieraEntidad.IdInfoFin);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoFinancieraEntidad", DbType.Int16, idEstadoInfoFinancieraEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoFinancieraEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        GenerarLogAuditoria(pValidarInfoFinancieraEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pValidarInfoFinancieraEntidad">Entidad Validación de información financiera del proveedor</param>
        /// <param name="idEstadoInfoFinancieraEntidad">Identificador del estado en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Nůmero de registros afectados</returns>
        public int ModificarValidarInfoFinancieraEntidad(ValidarInfoFinancieraEntidad pValidarInfoFinancieraEntidad, int idEstadoInfoFinancieraEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoFinancieraEntidad", DbType.Int32, pValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad );
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoFinancieraEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pValidarInfoFinancieraEntidad.IdInfoFin);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoFinancieraEntidad.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoFinancieraEntidad.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoFinancieraEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    
                    GenerarLogAuditoria(pValidarInfoFinancieraEntidad, vDbCommand);
                    
                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado"))
                    {

                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdInfoFin", DbType.String, pValidarInfoFinancieraEntidad.IdInfoFin);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoFinancieraEntidad", DbType.Int16, idEstadoInfoFinancieraEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoFinancieraEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        GenerarLogAuditoria(pValidarInfoFinancieraEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdValidarInfoFinancieraEntidad">Identificador en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Entidad Validación de información financiera del proveedor</returns>
        public ValidarInfoFinancieraEntidad ConsultarValidarInfoFinancieraEntidad(int pIdValidarInfoFinancieraEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoFinancieraEntidad", DbType.Int32, pIdValidarInfoFinancieraEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoFinancieraEntidad vValidarInfoFinancieraEntidad = new ValidarInfoFinancieraEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad = vDataReaderResults["IdValidarInfoFinancieraEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoFinancieraEntidad"].ToString()) : vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad;
                            vValidarInfoFinancieraEntidad.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vValidarInfoFinancieraEntidad.IdInfoFin;
                            vValidarInfoFinancieraEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoFinancieraEntidad.NroRevision;
                            vValidarInfoFinancieraEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoFinancieraEntidad.Observaciones;
                            vValidarInfoFinancieraEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoFinancieraEntidad.ConfirmaYAprueba;
                            vValidarInfoFinancieraEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoFinancieraEntidad.UsuarioCrea;
                            vValidarInfoFinancieraEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoFinancieraEntidad.FechaCrea;
                        }
                        return vValidarInfoFinancieraEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdInfoFin">Identificador de informaciňn financiera en una entidad Validación de información financiera del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad Validación de información financiera del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Lista  de entidades Validación de información financiera del proveedor</returns>
        public List<ValidarInfoFinancieraEntidad> ConsultarValidarInfoFinancieraEntidads(Int32 pIdInfoFin, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar"))
                {
                    if (pIdInfoFin != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdInfoFin", DbType.Int32, pIdInfoFin);
                    if(pObservaciones != null)
                         vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if(pConfirmaYAprueba != null)
                         vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoFinancieraEntidad> vListaValidarTercero = new List<ValidarInfoFinancieraEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoFinancieraEntidad vValidarInfoFinancieraEntidad = new ValidarInfoFinancieraEntidad();
                   
                            vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad = vDataReaderResults["IdValidarInfoFinancieraEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoFinancieraEntidad"].ToString()) : vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad;
                            vValidarInfoFinancieraEntidad.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vValidarInfoFinancieraEntidad.IdInfoFin;
                            vValidarInfoFinancieraEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoFinancieraEntidad.NroRevision;
                            vValidarInfoFinancieraEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoFinancieraEntidad.Observaciones;
                            vValidarInfoFinancieraEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoFinancieraEntidad.ConfirmaYAprueba;
                            vValidarInfoFinancieraEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoFinancieraEntidad.UsuarioCrea;
                            vValidarInfoFinancieraEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoFinancieraEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoFinancieraEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en la entidad Validación de información financiera del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es Confirma y aprueba valor en la entidad Validación de información financiera del proveedor</param>
        /// <returns>Lista de entidades Validación de información financiera del proveedor</returns>
        public List<ValidarInfoFinancieraEntidad> ConsultarValidarInfoFinancieraEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_ConsultarResumen"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pObservaciones != null)
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if (pConfirmaYAprueba != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoFinancieraEntidad> vListaValidarTercero = new List<ValidarInfoFinancieraEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoFinancieraEntidad vValidarInfoFinancieraEntidad = new ValidarInfoFinancieraEntidad();

                            vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad = vDataReaderResults["IdValidarInfoFinancieraEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoFinancieraEntidad"].ToString()) : vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad;
                            vValidarInfoFinancieraEntidad.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vValidarInfoFinancieraEntidad.IdInfoFin;
                            vValidarInfoFinancieraEntidad.Anno = vDataReaderResults["AcnoVigencia"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["AcnoVigencia"].ToString()) : vValidarInfoFinancieraEntidad.Anno;
                            vValidarInfoFinancieraEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoFinancieraEntidad.NroRevision;
                            vValidarInfoFinancieraEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoFinancieraEntidad.Observaciones;
                            vValidarInfoFinancieraEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"]) : vValidarInfoFinancieraEntidad.ConfirmaYAprueba;
                            vValidarInfoFinancieraEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoFinancieraEntidad.UsuarioCrea;
                            vValidarInfoFinancieraEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoFinancieraEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoFinancieraEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pIdVigencia">Identificador de la vigencia en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Entidad Validación de información financiera del proveedor</returns>
        public ValidarInfoFinancieraEntidad ConsultarValidarInfoFinancieraEntidad_Ultima(Int32 pIdEntidad, int pIdVigencia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_Ultima"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int16, pIdVigencia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoFinancieraEntidad vValidarInfoFinancieraEntidad = new ValidarInfoFinancieraEntidad();
                        while (vDataReaderResults.Read())                        
                        {
                            vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad = vDataReaderResults["IdValidarInfoFinancieraEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoFinancieraEntidad"].ToString()) : vValidarInfoFinancieraEntidad.IdValidarInfoFinancieraEntidad;
                            vValidarInfoFinancieraEntidad.IdInfoFin = vDataReaderResults["IdInfoFin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoFin"].ToString()) : vValidarInfoFinancieraEntidad.IdInfoFin;
                            vValidarInfoFinancieraEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoFinancieraEntidad.NroRevision;
                            vValidarInfoFinancieraEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoFinancieraEntidad.Observaciones;
                            vValidarInfoFinancieraEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoFinancieraEntidad.ConfirmaYAprueba;
                            vValidarInfoFinancieraEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoFinancieraEntidad.UsuarioCrea;
                            vValidarInfoFinancieraEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoFinancieraEntidad.FechaCrea;
                   
                        }
                        return vValidarInfoFinancieraEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
