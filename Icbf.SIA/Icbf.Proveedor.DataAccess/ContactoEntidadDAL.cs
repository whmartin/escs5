using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Contacto entidad
    /// </summary>
    public class ContactoEntidadDAL : GeneralDAL
    {
        public ContactoEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta un nuevo Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ContactoEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdContacto", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pContactoEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdSucursal", DbType.Int32, pContactoEntidad.IdSucursal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTelContacto", DbType.Int32, pContactoEntidad.IdTelContacto);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, pContactoEntidad.IdTipoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCargoEntidad", DbType.Int32, pContactoEntidad.IdTipoCargoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Decimal, pContactoEntidad.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pContactoEntidad.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pContactoEntidad.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pContactoEntidad.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pContactoEntidad.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Dependencia", DbType.String, pContactoEntidad.Dependencia);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pContactoEntidad.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pContactoEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pContactoEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pContactoEntidad.IdContacto = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdContacto").ToString());
                    GenerarLogAuditoria(pContactoEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica un Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ContactoEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContacto", DbType.Int32, pContactoEntidad.IdContacto);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pContactoEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdSucursal", DbType.Int32, pContactoEntidad.IdSucursal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTelContacto", DbType.Int32, pContactoEntidad.IdTelContacto);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, pContactoEntidad.IdTipoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCargoEntidad", DbType.Int32, pContactoEntidad.IdTipoCargoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Decimal, pContactoEntidad.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pContactoEntidad.PrimerNombre);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pContactoEntidad.SegundoNombre);
                    vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pContactoEntidad.PrimerApellido);
                    vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pContactoEntidad.SegundoApellido);
                    vDataBase.AddInParameter(vDbCommand, "@Dependencia", DbType.String, pContactoEntidad.Dependencia);
                    vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pContactoEntidad.Email);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pContactoEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pContactoEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContactoEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ContactoEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdContacto", DbType.Int32, pContactoEntidad.IdContacto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pContactoEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Contacto Entidad
        /// </summary>
        /// <param name="pIdContacto">Identificador de un Contacto Entidad</param>
        /// <returns>Entidad Contacto Entidad</returns>
        public ContactoEntidad ConsultarContactoEntidad(int pIdContacto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ContactoEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdContacto", DbType.Int32, pIdContacto);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ContactoEntidad vContactoEntidad = new ContactoEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vContactoEntidad.IdContacto = vDataReaderResults["IdContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContacto"].ToString()) : vContactoEntidad.IdContacto;
                            vContactoEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vContactoEntidad.IdEntidad;
                            vContactoEntidad.IdSucursal = vDataReaderResults["IdSucursal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdSucursal"].ToString()) : vContactoEntidad.IdSucursal;
                            vContactoEntidad.IdTelContacto = vDataReaderResults["IdTelContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTelContacto"].ToString()) : vContactoEntidad.IdTelContacto;
                            vContactoEntidad.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vContactoEntidad.IdTipoDocIdentifica;
                            vContactoEntidad.IdTipoCargoEntidad = vDataReaderResults["IdTipoCargoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCargoEntidad"].ToString()) : vContactoEntidad.IdTipoCargoEntidad;
                            vContactoEntidad.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroIdentificacion"].ToString()) : vContactoEntidad.NumeroIdentificacion;
                            vContactoEntidad.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vContactoEntidad.PrimerNombre;
                            vContactoEntidad.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vContactoEntidad.SegundoNombre;
                            vContactoEntidad.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vContactoEntidad.PrimerApellido;
                            vContactoEntidad.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vContactoEntidad.SegundoApellido;
                            vContactoEntidad.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vContactoEntidad.Dependencia;
                            vContactoEntidad.Email = vDataReaderResults["Email"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email"].ToString()) : vContactoEntidad.Email;
                            vContactoEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vContactoEntidad.Estado;
                            vContactoEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContactoEntidad.UsuarioCrea;
                            vContactoEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContactoEntidad.FechaCrea;
                            vContactoEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContactoEntidad.UsuarioModifica;
                            vContactoEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContactoEntidad.FechaModifica;
                            vContactoEntidad.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vContactoEntidad.Celular;
                            vContactoEntidad.IndicativoTelefono = vDataReaderResults["IndicativoTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IndicativoTelefono"].ToString()) : vContactoEntidad.IndicativoTelefono;
                            vContactoEntidad.NumeroTelefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTelefono"].ToString()) : vContactoEntidad.NumeroTelefono;
                            vContactoEntidad.ExtensionTelefono = vDataReaderResults["ExtensionTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionTelefono"].ToString()) : vContactoEntidad.ExtensionTelefono;
                            vContactoEntidad.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vContactoEntidad.Cargo;
                        }
                        return vContactoEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Contacto Entidad
        /// </summary>
        /// <param name="pIdEntidad">Identificador del Contacto Entidad</param>
        /// <param name="pNumeroIdentificacion">N�mero de identificaci�n del Contacto Entidad</param>
        /// <param name="pPrimerNombre">Primer nombre del Contacto Entidad</param>
        /// <param name="pSegundoNombre">Segundo nombre del Contacto Entidad</param>
        /// <param name="pPrimerApellido">Primer apellido del Contacto Entidad</param>
        /// <param name="pSegundoApellido">Segundo apellido del Contacto Entidad</param>
        /// <param name="pDependencia">Dependencia del Contacto Entidad</param>
        /// <param name="pEmail">Correo electr�nico del Contacto Entidad</param>
        /// <param name="pEstado">Estado del Contacto Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ContactoEntidad> ConsultarContactoEntidads(int ?pIdEntidad, Decimal ?pNumeroIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido, String pDependencia, String pEmail, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ContactoEntidads_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if(pNumeroIdentificacion != null)
                         vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.Decimal, pNumeroIdentificacion);
                    if(pPrimerNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@PrimerNombre", DbType.String, pPrimerNombre);
                    if(pSegundoNombre != null)
                         vDataBase.AddInParameter(vDbCommand, "@SegundoNombre", DbType.String, pSegundoNombre);
                    if(pPrimerApellido != null)
                         vDataBase.AddInParameter(vDbCommand, "@PrimerApellido", DbType.String, pPrimerApellido);
                    if(pSegundoApellido != null)
                         vDataBase.AddInParameter(vDbCommand, "@SegundoApellido", DbType.String, pSegundoApellido);
                    if(pDependencia != null)
                         vDataBase.AddInParameter(vDbCommand, "@Dependencia", DbType.String, pDependencia);
                    if(pEmail != null)
                         vDataBase.AddInParameter(vDbCommand, "@Email", DbType.String, pEmail);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ContactoEntidad> vListaContactoEntidad = new List<ContactoEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ContactoEntidad vContactoEntidad = new ContactoEntidad();
                            vContactoEntidad.IdContacto = vDataReaderResults["IdContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdContacto"].ToString()) : vContactoEntidad.IdContacto;
                            vContactoEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vContactoEntidad.IdEntidad;
                            vContactoEntidad.IdTelContacto = vDataReaderResults["IdTelContacto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTelContacto"].ToString()) : vContactoEntidad.IdTelContacto;
                            vContactoEntidad.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vContactoEntidad.IdTipoDocIdentifica;
                            vContactoEntidad.IdTipoCargoEntidad = vDataReaderResults["IdTipoCargoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCargoEntidad"].ToString()) : vContactoEntidad.IdTipoCargoEntidad;
                            vContactoEntidad.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["NumeroIdentificacion"].ToString()) : vContactoEntidad.NumeroIdentificacion;
                            vContactoEntidad.PrimerNombre = vDataReaderResults["PrimerNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerNombre"].ToString()) : vContactoEntidad.PrimerNombre;
                            vContactoEntidad.SegundoNombre = vDataReaderResults["SegundoNombre"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoNombre"].ToString()) : vContactoEntidad.SegundoNombre;
                            vContactoEntidad.PrimerApellido = vDataReaderResults["PrimerApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["PrimerApellido"].ToString()) : vContactoEntidad.PrimerApellido;
                            vContactoEntidad.SegundoApellido = vDataReaderResults["SegundoApellido"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SegundoApellido"].ToString()) : vContactoEntidad.SegundoApellido;
                            vContactoEntidad.Dependencia = vDataReaderResults["Dependencia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Dependencia"].ToString()) : vContactoEntidad.Dependencia;
                            vContactoEntidad.Email = vDataReaderResults["Email"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Email"].ToString()) : vContactoEntidad.Email;
                            vContactoEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vContactoEntidad.Estado;
                            vContactoEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vContactoEntidad.UsuarioCrea;
                            vContactoEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vContactoEntidad.FechaCrea;
                            vContactoEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vContactoEntidad.UsuarioModifica;
                            vContactoEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vContactoEntidad.FechaModifica;
                            vContactoEntidad.Celular = vDataReaderResults["Celular"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Celular"].ToString()) : vContactoEntidad.Celular;
                            vContactoEntidad.IndicativoTelefono = vDataReaderResults["IndicativoTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IndicativoTelefono"].ToString()) : vContactoEntidad.IndicativoTelefono;
                            vContactoEntidad.NumeroTelefono = vDataReaderResults["NumeroTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroTelefono"].ToString()) : vContactoEntidad.NumeroTelefono;
                            vContactoEntidad.ExtensionTelefono = vDataReaderResults["ExtensionTelefono"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionTelefono"].ToString()) : vContactoEntidad.ExtensionTelefono;
                            vContactoEntidad.Cargo = vDataReaderResults["Cargo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Cargo"].ToString()) : vContactoEntidad.Cargo;
                            vContactoEntidad.Sucursal = vDataReaderResults["Sucursal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sucursal"].ToString()) : vContactoEntidad.Sucursal;
                            vListaContactoEntidad.Add(vContactoEntidad);
                        }
                        return vListaContactoEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
