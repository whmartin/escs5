using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad notificaci�n judicial
    /// </summary>
    public class NotificacionJudicialDAL : GeneralDAL
    {
        public NotificacionJudicialDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pNotificacionJudicial">Entidad Notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NotificacionJudicial_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pNotificacionJudicial.IdNotJudicial );
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pNotificacionJudicial.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pNotificacionJudicial.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pNotificacionJudicial.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pNotificacionJudicial.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pNotificacionJudicial.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pNotificacionJudicial.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    vResultado = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdNotJudicial").ToString());
                    GenerarLogAuditoria(pNotificacionJudicial, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pNotificacionJudicial">Entidad Notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NotificacionJudicial_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pNotificacionJudicial.IdNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pNotificacionJudicial.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pNotificacionJudicial.IdDepartamento);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pNotificacionJudicial.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pNotificacionJudicial.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.String, pNotificacionJudicial.Direccion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pNotificacionJudicial.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNotificacionJudicial, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pNotificacionJudicial">Entidad Notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NotificacionJudicial_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pNotificacionJudicial.IdNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pNotificacionJudicial.IdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pNotificacionJudicial.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.Int32, pNotificacionJudicial.Direccion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNotificacionJudicial, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pIdNotJudicial">Identificador de una entidad Notificaci�n judicial</param>
        /// <param name="pIdMunicipio">Identificador de un municipio en una entidad Notificaci�n judicial</param>
        /// <param name="pIdZona">Identificador de una zona en una entidad Notificaci�n judicial</param>
        /// <param name="pDireccion">Identificador de una direcci�n en una entidad Notificaci�n judicial</param>
        /// <returns>Lista de entidades Notificaci�n judicial</returns>
        public NotificacionJudicial ConsultarNotificacionJudicial(int pIdNotJudicial, int pIdMunicipio, int pIdZona, int pDireccion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NotificacionJudicial_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pIdNotJudicial);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.Int32, pIdZona);
                    vDataBase.AddInParameter(vDbCommand, "@Direccion", DbType.Int32, pDireccion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        NotificacionJudicial vNotificacionJudicial = new NotificacionJudicial();
                        while (vDataReaderResults.Read())
                        {
                            vNotificacionJudicial.IdNotJudicial = vDataReaderResults["IdNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotJudicial"].ToString()) : vNotificacionJudicial.IdNotJudicial;
                            vNotificacionJudicial.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vNotificacionJudicial.IdEntidad;
                            vNotificacionJudicial.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vNotificacionJudicial.IdDepartamento;
                            vNotificacionJudicial.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vNotificacionJudicial.IdMunicipio;
                            vNotificacionJudicial.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vNotificacionJudicial.IdZona;
                            vNotificacionJudicial.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vNotificacionJudicial.Direccion;
                            vNotificacionJudicial.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNotificacionJudicial.UsuarioCrea;
                            vNotificacionJudicial.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNotificacionJudicial.FechaCrea;
                            vNotificacionJudicial.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNotificacionJudicial.UsuarioModifica;
                            vNotificacionJudicial.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNotificacionJudicial.FechaModifica;
                        }
                        return vNotificacionJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Notificaci�n judicial
        /// </summary>
        /// <param name="pIdNotJudicial">Identificador de una entidad Notificaci�n judicial</param>
        /// <param name="pIdEntidad">Identificador de una entidad general en una entidad Notificaci�n judicial</param>
        /// <param name="pIdDepartamento">Identificador de una departamento en una entidad Notificaci�n judicial</param>
        /// <param name="pIdMunicipio">Identificador de un Municipio en una entidad Notificaci�n judicial</param>
        /// <returns>Lista de entidades Notificaci�n judicial</returns>
        public List<NotificacionJudicial> ConsultarNotificacionJudicials(int? pIdNotJudicial, int? pIdEntidad, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NotificacionJudicials_Consultar"))
                {
                    if(pIdNotJudicial != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdNotJudicial", DbType.Int32, pIdNotJudicial);
                    if(pIdEntidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if(pIdDepartamento != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdDepartamento", DbType.Int32, pIdDepartamento);
                    if(pIdMunicipio != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdMunicipio", DbType.Int32, pIdMunicipio);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NotificacionJudicial> vListaNotificacionJudicial = new List<NotificacionJudicial>();
                        while (vDataReaderResults.Read())
                        {
                                NotificacionJudicial vNotificacionJudicial = new NotificacionJudicial();
                            vNotificacionJudicial.IdNotJudicial = vDataReaderResults["IdNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotJudicial"].ToString()) : vNotificacionJudicial.IdNotJudicial;
                            vNotificacionJudicial.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vNotificacionJudicial.IdEntidad;
                            vNotificacionJudicial.IdDepartamento = vDataReaderResults["IdDepartamento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamento"].ToString()) : vNotificacionJudicial.IdDepartamento;
                            vNotificacionJudicial.IdMunicipio = vDataReaderResults["IdMunicipio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipio"].ToString()) : vNotificacionJudicial.IdMunicipio;
                            vNotificacionJudicial.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdZona"].ToString()) : vNotificacionJudicial.IdZona;
                            vNotificacionJudicial.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vNotificacionJudicial.Direccion;
                            vNotificacionJudicial.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNotificacionJudicial.UsuarioCrea;
                            vNotificacionJudicial.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNotificacionJudicial.FechaCrea;
                            vNotificacionJudicial.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNotificacionJudicial.UsuarioModifica;
                            vNotificacionJudicial.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNotificacionJudicial.FechaModifica;
                                vListaNotificacionJudicial.Add(vNotificacionJudicial);
                        }
                        return vListaNotificacionJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Notificaci�n judicial
        /// </summary>
        /// <param name="pIdEntidad">Identificador general en una entidad Notificaci�n judicial</param>
        /// <returns>Lista de entidades Notificaci�n judicial</returns>
        public List<NotificacionJudicial> ConsultarNotificacionJudicials(int? pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NotificacionJudiciales_By_Param"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NotificacionJudicial> vListaNotificacionJudicial = new List<NotificacionJudicial>();
                        while (vDataReaderResults.Read())
                        {
                            NotificacionJudicial vNotificacionJudicial = new NotificacionJudicial();
                            vNotificacionJudicial.IdNotJudicial = vDataReaderResults["IdNotJudicial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNotJudicial"].ToString()) : vNotificacionJudicial.IdNotJudicial;
                            vNotificacionJudicial.NombreDepartamento = vDataReaderResults["NombreDepartamento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDepartamento"].ToString()) : vNotificacionJudicial.NombreDepartamento;
                            vNotificacionJudicial.NombreMunicipio = vDataReaderResults["NombreMunicipio"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreMunicipio"].ToString()) : vNotificacionJudicial.NombreMunicipio;
                            vNotificacionJudicial.Direccion = vDataReaderResults["Direccion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Direccion"].ToString()) : vNotificacionJudicial.Direccion;
                            vNotificacionJudicial.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNotificacionJudicial.UsuarioCrea;
                            vListaNotificacionJudicial.Add(vNotificacionJudicial);
                        }
                        return vListaNotificacionJudicial;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
