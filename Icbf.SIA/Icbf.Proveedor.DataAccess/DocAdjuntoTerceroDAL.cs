using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad  Documentos adjuntos al tercero
    /// </summary>
    public class DocAdjuntoTerceroDAL : GeneralDAL
    {
        public DocAdjuntoTerceroDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Entidad Documentos adjuntos al tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTercero_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pDocAdjuntoTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pDocAdjuntoTercero.IdDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocAdjuntoTercero.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocAdjuntoTercero.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Anno", DbType.Int32, pDocAdjuntoTercero.Anno);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocAdjuntoTercero.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pDocAdjuntoTercero.IdTemporal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Entidad Documentos adjuntos al tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTercero_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocAdjuntoTercero.IdDocAdjunto);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.String, pDocAdjuntoTercero.IdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocAdjuntoTercero.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocAdjuntoTercero.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocAdjuntoTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Entidad Documentos adjuntos al tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocAdjuntoTercero.IdDocAdjunto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocAdjuntoTercero, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Identificador de entidad Documentos adjuntos al tercero</param>
        /// <returns>Entidad Documentos adjuntos al tercero</returns>
        public DocAdjuntoTercero ConsultarDocAdjuntoTercero(int pIdDocAdjunto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pIdDocAdjunto);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocAdjuntoTercero vDocAdjuntoTercero = new DocAdjuntoTercero();
                        while (vDataReaderResults.Read())
                        {
                            vDocAdjuntoTercero.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocAdjuntoTercero.IdDocAdjunto;
                            vDocAdjuntoTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDocAdjuntoTercero.IdTercero;
                            vDocAdjuntoTercero.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocAdjuntoTercero.IdDocumento;
                            vDocAdjuntoTercero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocAdjuntoTercero.Descripcion;
                            vDocAdjuntoTercero.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocAdjuntoTercero.LinkDocumento;
                            vDocAdjuntoTercero.Anno = vDataReaderResults["Anno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anno"].ToString()) : vDocAdjuntoTercero.Anno;
                            vDocAdjuntoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocAdjuntoTercero.UsuarioCrea;
                            vDocAdjuntoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocAdjuntoTercero.FechaCrea;
                            vDocAdjuntoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocAdjuntoTercero.UsuarioModifica;
                            vDocAdjuntoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocAdjuntoTercero.FechaModifica;
                        }
                        return vDocAdjuntoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Identificador de entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdTercero">Identificador de Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdDocumento">Identificador del Documento en entidad Documentos adjuntos al tercero</param>
        /// <returns>Entidad Documentos adjuntos al tercero</returns>
        public DocAdjuntoTercero ConsultarDocAdjuntoTercero(int pIdDocAdjunto, int pIdTercero, int pIdDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pIdDocAdjunto);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pIdDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocAdjuntoTercero vDocAdjuntoTercero = new DocAdjuntoTercero();
                        while (vDataReaderResults.Read())
                        {
                            vDocAdjuntoTercero.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocAdjuntoTercero.IdDocAdjunto;
                            vDocAdjuntoTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDocAdjuntoTercero.IdTercero;
                            vDocAdjuntoTercero.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocAdjuntoTercero.IdDocumento;
                            vDocAdjuntoTercero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocAdjuntoTercero.Descripcion;
                            vDocAdjuntoTercero.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocAdjuntoTercero.LinkDocumento;
                            vDocAdjuntoTercero.Anno = vDataReaderResults["Anno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anno"].ToString()) : vDocAdjuntoTercero.Anno;
                            vDocAdjuntoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocAdjuntoTercero.UsuarioCrea;
                            vDocAdjuntoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocAdjuntoTercero.FechaCrea;
                            vDocAdjuntoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocAdjuntoTercero.UsuarioModifica;
                            vDocAdjuntoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocAdjuntoTercero.FechaModifica;
                        }
                        return vDocAdjuntoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdTercero">Identificador del Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pcodigoDocumento">C�digo del documento en entidad Documentos adjuntos al tercero</param>
        /// <param name="programa">Programa en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTercero(int? pIdTercero, string pcodigoDocumento, string programa)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTerceros_ConsultarByPrograma"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocumento", DbType.String, pcodigoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Programa", DbType.String , programa);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocAdjuntoTercero> vListaDocAdjuntoTercero = new List<DocAdjuntoTercero>();
                        while (vDataReaderResults.Read())
                        {
                            DocAdjuntoTercero vDocAdjuntoTercero = new DocAdjuntoTercero();
                            vDocAdjuntoTercero.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocAdjuntoTercero.IdDocAdjunto;
                            vDocAdjuntoTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDocAdjuntoTercero.IdTercero;
                            vDocAdjuntoTercero.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocAdjuntoTercero.IdDocumento;
                            vDocAdjuntoTercero.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocAdjuntoTercero.NombreTipoDocumento;
                            vDocAdjuntoTercero.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocAdjuntoTercero.MaxPermitidoKB;
                            vDocAdjuntoTercero.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocAdjuntoTercero.ExtensionesPermitidas ;
                            vDocAdjuntoTercero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocAdjuntoTercero.Descripcion;
                            vDocAdjuntoTercero.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocAdjuntoTercero.LinkDocumento;
                            vDocAdjuntoTercero.Anno = vDataReaderResults["Anno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anno"].ToString()) : vDocAdjuntoTercero.Anno;
                            vDocAdjuntoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocAdjuntoTercero.UsuarioCrea;
                            vDocAdjuntoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocAdjuntoTercero.FechaCrea;
                            vDocAdjuntoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocAdjuntoTercero.UsuarioModifica;
                            vDocAdjuntoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocAdjuntoTercero.FechaModifica;
                            vListaDocAdjuntoTercero.Add(vDocAdjuntoTercero);
                        }
                        return vListaDocAdjuntoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdTercero">Identificador del Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdDocumento">Identificador del documento en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros(int? pIdDocAdjunto, int? pIdTercero, int? pIdDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar"))
                {
                    if (pIdDocAdjunto != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pIdDocAdjunto);
                    if (pIdTercero != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if (pIdDocumento != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pIdDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocAdjuntoTercero> vListaDocAdjuntoTercero = new List<DocAdjuntoTercero>();
                        while (vDataReaderResults.Read())
                        {
                            DocAdjuntoTercero vDocAdjuntoTercero = new DocAdjuntoTercero();
                            vDocAdjuntoTercero.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocAdjuntoTercero.IdDocAdjunto;
                            vDocAdjuntoTercero.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vDocAdjuntoTercero.IdTercero;
                            vDocAdjuntoTercero.IdDocumento = vDataReaderResults["IdDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocumento"].ToString()) : vDocAdjuntoTercero.IdDocumento;
                            vDocAdjuntoTercero.NombreTipoDocumento = vDataReaderResults["NombreTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoDocumento"].ToString()) : vDocAdjuntoTercero.NombreTipoDocumento;
                            vDocAdjuntoTercero.CodigoTipoDocumento = vDataReaderResults["CodigoTipoDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoDocumento"].ToString()) : vDocAdjuntoTercero.CodigoTipoDocumento;
                            vDocAdjuntoTercero.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocAdjuntoTercero.Descripcion;
                            vDocAdjuntoTercero.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocAdjuntoTercero.LinkDocumento;
                            vDocAdjuntoTercero.Anno = vDataReaderResults["Anno"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Anno"].ToString()) : vDocAdjuntoTercero.Anno;
                            vDocAdjuntoTercero.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocAdjuntoTercero.UsuarioCrea;
                            vDocAdjuntoTercero.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocAdjuntoTercero.FechaCrea;
                            vDocAdjuntoTercero.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocAdjuntoTercero.UsuarioModifica;
                            vDocAdjuntoTercero.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocAdjuntoTercero.FechaModifica;
                            vListaDocAdjuntoTercero.Add(vDocAdjuntoTercero);
                        }
                        return vListaDocAdjuntoTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdTercero">Identificador del Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pTipoPersona">Tipo de persona en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>  //varIdTemporal =
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTercero_IdEntidad_TipoPersona(int pIdTercero, string pTipoPersona, string pIdTemporal)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTercero_TipoPersona"))
                {
                    if (pIdTercero  != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pIdTercero);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);
                    if (pIdTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pIdTemporal);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocAdjuntoTercero> vLista = new List<DocAdjuntoTercero>();
                        while (vDataReaderResults.Read())
                        {
                            DocAdjuntoTercero vDoc = new DocAdjuntoTercero();
                            vDoc.IdDocAdjunto = vDataReaderResults["IDDOCADJUNTO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDOCADJUNTO"].ToString()) : vDoc.IdDocAdjunto;
                            vDoc.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vDoc.IdTercero;
                            vDoc.NombreTipoDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDoc.NombreTipoDocumento;
                            vDoc.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDoc.LinkDocumento;
                            //vDoc.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDoc.Observaciones;
                            vDoc.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDoc.UsuarioCrea;
                            vDoc.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDoc.FechaCrea;
                            vDoc.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDoc.UsuarioModifica;
                            vDoc.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDoc.FechaModifica;
                            vDoc.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDoc.Obligatorio;
                            vDoc.IdDocumento = vDataReaderResults["IDDOCUMENTO"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IDDOCUMENTO"].ToString()) : vDoc.IdDocumento;
                            vDoc.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDoc.MaxPermitidoKB;
                            vDoc.ExtensionesPermitidas  = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDoc.ExtensionesPermitidas;
                            vDoc.IdTemporal  = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDoc.IdTemporal;

                            vLista.Add(vDoc);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en entidad Documentos adjuntos al tercero</param>
        /// <param name="pTipoPersona">Tipo de persona en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTercero_IdTemporal_TipoPersona(string pIdTemporal, string pTipoPersona)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona"))
                {
                    if (pIdTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pIdTemporal);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocAdjuntoTercero> vLista = new List<DocAdjuntoTercero>();
                        while (vDataReaderResults.Read())
                        {
                            DocAdjuntoTercero vDoc = new DocAdjuntoTercero();
                            vDoc.IdDocAdjunto = vDataReaderResults["IDDOCADJUNTO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDDOCADJUNTO"].ToString()) : vDoc.IdDocAdjunto;
                         //   vDoc.IdTercero = vDataReaderResults["IDTERCERO"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IDTERCERO"].ToString()) : vDoc.IdTercero;
                            vDoc.NombreTipoDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDoc.NombreTipoDocumento;
                            vDoc.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDoc.LinkDocumento;
                            //vDoc.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vDoc.Observaciones;
                            vDoc.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDoc.UsuarioCrea;
                            vDoc.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDoc.FechaCrea;
                            vDoc.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDoc.UsuarioModifica;
                            vDoc.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDoc.FechaModifica;
                            vDoc.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDoc.Obligatorio;
                            vDoc.IdDocumento = vDataReaderResults["IDDOCUMENTO"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["IDDOCUMENTO"].ToString()) : vDoc.IdDocumento;
                            vDoc.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDoc.MaxPermitidoKB;
                            vDoc.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDoc.ExtensionesPermitidas;
                            vDoc.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDoc.IdTemporal;

                            vLista.Add(vDoc);
                        }
                        return vLista;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
