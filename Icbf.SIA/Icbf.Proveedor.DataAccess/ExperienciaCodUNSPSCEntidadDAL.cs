using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad c�digos UNSPSC de experiencia
    /// </summary>
    public class ExperienciaCodUNSPSCEntidadDAL : GeneralDAL
    {
        public ExperienciaCodUNSPSCEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad C�digos UNSPSC de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdExpCOD", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pExperienciaCodUNSPSCEntidad.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pExperienciaCodUNSPSCEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pExperienciaCodUNSPSCEntidad.IdExpCOD = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdExpCOD").ToString());
                    GenerarLogAuditoria(pExperienciaCodUNSPSCEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad C�digos UNSPSC de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdExpCOD", DbType.Int32, pExperienciaCodUNSPSCEntidad.IdExpCOD);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC);
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pExperienciaCodUNSPSCEntidad.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pExperienciaCodUNSPSCEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pExperienciaCodUNSPSCEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad C�digos UNSPSC de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdExpCOD", DbType.Int32, pExperienciaCodUNSPSCEntidad.IdExpCOD);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pExperienciaCodUNSPSCEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pIdExpCOD">Identificador de una entidad C�digos UNSPSC de experiencia</param>
        /// <returns>Entidad C�digos UNSPSC de experiencia</returns>
        public ExperienciaCodUNSPSCEntidad ConsultarExperienciaCodUNSPSCEntidad(int pIdExpCOD)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdExpCOD", DbType.Int32, pIdExpCOD);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ExperienciaCodUNSPSCEntidad vExperienciaCodUNSPSCEntidad = new ExperienciaCodUNSPSCEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vExperienciaCodUNSPSCEntidad.IdExpCOD = vDataReaderResults["IdExpCOD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpCOD"].ToString()) : vExperienciaCodUNSPSCEntidad.IdExpCOD;
                            vExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC;
                            vExperienciaCodUNSPSCEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vExperienciaCodUNSPSCEntidad.IdExpEntidad;
                            vExperienciaCodUNSPSCEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vExperienciaCodUNSPSCEntidad.UsuarioCrea;
                            vExperienciaCodUNSPSCEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vExperienciaCodUNSPSCEntidad.FechaCrea;
                            vExperienciaCodUNSPSCEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vExperienciaCodUNSPSCEntidad.UsuarioModifica;
                            vExperienciaCodUNSPSCEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vExperienciaCodUNSPSCEntidad.FechaModifica;
                        }
                        return vExperienciaCodUNSPSCEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de una entidad C�digos UNSPSC de experiencia</param>
        /// <returns>Lista de entidades C�digos UNSPSC de experiencia</returns>
        public List<ExperienciaCodUNSPSCEntidad> ConsultarExperienciaCodUNSPSCEntidads(int? pIdExpEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar"))
                {
                    if (pIdExpEntidad!=null) 
                        vDataBase.AddInParameter(vDbCommand, "@idExpEntidad", DbType.Int32, pIdExpEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ExperienciaCodUNSPSCEntidad> vListaExperienciaCodUNSPSCEntidad = new List<ExperienciaCodUNSPSCEntidad>();
                        while (vDataReaderResults.Read())
                        {
                                ExperienciaCodUNSPSCEntidad vExperienciaCodUNSPSCEntidad = new ExperienciaCodUNSPSCEntidad();
                            vExperienciaCodUNSPSCEntidad.IdExpCOD = vDataReaderResults["IdExpCOD"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpCOD"].ToString()) : vExperienciaCodUNSPSCEntidad.IdExpCOD;
                            vExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC;
                            vExperienciaCodUNSPSCEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vExperienciaCodUNSPSCEntidad.IdExpEntidad;
                            vExperienciaCodUNSPSCEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vExperienciaCodUNSPSCEntidad.UsuarioCrea;
                            vExperienciaCodUNSPSCEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vExperienciaCodUNSPSCEntidad.FechaCrea;
                            vExperienciaCodUNSPSCEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vExperienciaCodUNSPSCEntidad.UsuarioModifica;
                            vExperienciaCodUNSPSCEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vExperienciaCodUNSPSCEntidad.FechaModifica;
                            vExperienciaCodUNSPSCEntidad.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vExperienciaCodUNSPSCEntidad.Codigo;
                            vExperienciaCodUNSPSCEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vExperienciaCodUNSPSCEntidad.Descripcion;
                                vListaExperienciaCodUNSPSCEntidad.Add(vExperienciaCodUNSPSCEntidad);
                        }
                        return vListaExperienciaCodUNSPSCEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
