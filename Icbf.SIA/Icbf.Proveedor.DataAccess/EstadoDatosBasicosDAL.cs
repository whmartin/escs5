using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Estado Datos B�sicos
    /// </summary>
    public class EstadoDatosBasicosDAL : GeneralDAL
    {
        public EstadoDatosBasicosDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos B�sicos</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdEstadoDatosBasicos", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoDatosBasicos.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstadoDatosBasicos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pEstadoDatosBasicos.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pEstadoDatosBasicos.IdEstadoDatosBasicos = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdEstadoDatosBasicos").ToString());
                    GenerarLogAuditoria(pEstadoDatosBasicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos B�sicos</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDatosBasicos", DbType.Int32, pEstadoDatosBasicos.IdEstadoDatosBasicos);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pEstadoDatosBasicos.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstadoDatosBasicos.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pEstadoDatosBasicos.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoDatosBasicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos B�sicos</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDatosBasicos", DbType.Int32, pEstadoDatosBasicos.IdEstadoDatosBasicos);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pEstadoDatosBasicos, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pIdEstadoDatosBasicos">Identificador de una entidad Estado Datos B�sicos</param>
        /// <returns>Entidad Estado Datos B�sicos</returns>
        public EstadoDatosBasicos ConsultarEstadoDatosBasicos(int pIdEstadoDatosBasicos)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEstadoDatosBasicos", DbType.Int32, pIdEstadoDatosBasicos);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        EstadoDatosBasicos vEstadoDatosBasicos = new EstadoDatosBasicos();
                        while (vDataReaderResults.Read())
                        {
                            vEstadoDatosBasicos.IdEstadoDatosBasicos = vDataReaderResults["IdEstadoDatosBasicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDatosBasicos"].ToString()) : vEstadoDatosBasicos.IdEstadoDatosBasicos;
                            vEstadoDatosBasicos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoDatosBasicos.Descripcion;
                            vEstadoDatosBasicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoDatosBasicos.Estado;
                            vEstadoDatosBasicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoDatosBasicos.UsuarioCrea;
                            vEstadoDatosBasicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoDatosBasicos.FechaCrea;
                            vEstadoDatosBasicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoDatosBasicos.UsuarioModifica;
                            vEstadoDatosBasicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoDatosBasicos.FechaModifica;
                        }
                        return vEstadoDatosBasicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Datos B�sicos
        /// </summary>
        /// <param name="pDescripcion">Descripci�n en una entidad Estado Datos B�sicos</param>
        /// <param name="pEstado">Estado en una entidad Estado Datos B�sicos</param>
        /// <returns>Lista de entidades Estado Datos B�sicos</returns>
        public List<EstadoDatosBasicos> ConsultarEstadoDatosBasicoss(String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar"))
                {
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoDatosBasicos> vListaEstadoDatosBasicos = new List<EstadoDatosBasicos>();
                        while (vDataReaderResults.Read())
                        {
                                EstadoDatosBasicos vEstadoDatosBasicos = new EstadoDatosBasicos();
                            vEstadoDatosBasicos.IdEstadoDatosBasicos = vDataReaderResults["IdEstadoDatosBasicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDatosBasicos"].ToString()) : vEstadoDatosBasicos.IdEstadoDatosBasicos;
                            vEstadoDatosBasicos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoDatosBasicos.Descripcion;
                            vEstadoDatosBasicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoDatosBasicos.Estado;
                            vEstadoDatosBasicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoDatosBasicos.UsuarioCrea;
                            vEstadoDatosBasicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoDatosBasicos.FechaCrea;
                            vEstadoDatosBasicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoDatosBasicos.UsuarioModifica;
                            vEstadoDatosBasicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoDatosBasicos.FechaModifica;
                                vListaEstadoDatosBasicos.Add(vEstadoDatosBasicos);
                        }
                        return vListaEstadoDatosBasicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstado">Estado en una entidad Estado Datos B�sicos</param>
        /// <returns>Lista de entidades Estado Datos B�sicos</returns>
        public List<EstadoDatosBasicos> ConsultarEstadoDatosBasicossAll( Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EstadoDatosBasicoss_Consultar"))
                {
                     if (pEstado != null)
                        vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<EstadoDatosBasicos> vListaEstadoDatosBasicos = new List<EstadoDatosBasicos>();
                        while (vDataReaderResults.Read())
                        {
                            EstadoDatosBasicos vEstadoDatosBasicos = new EstadoDatosBasicos();
                            vEstadoDatosBasicos.IdEstadoDatosBasicos = vDataReaderResults["IdEstadoDatosBasicos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoDatosBasicos"].ToString()) : vEstadoDatosBasicos.IdEstadoDatosBasicos;
                            vEstadoDatosBasicos.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vEstadoDatosBasicos.Descripcion;
                            vEstadoDatosBasicos.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vEstadoDatosBasicos.Estado;
                            vEstadoDatosBasicos.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vEstadoDatosBasicos.UsuarioCrea;
                            vEstadoDatosBasicos.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vEstadoDatosBasicos.FechaCrea;
                            vEstadoDatosBasicos.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vEstadoDatosBasicos.UsuarioModifica;
                            vEstadoDatosBasicos.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vEstadoDatosBasicos.FechaModifica;
                            vListaEstadoDatosBasicos.Add(vEstadoDatosBasicos);
                        }
                        return vListaEstadoDatosBasicos;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
