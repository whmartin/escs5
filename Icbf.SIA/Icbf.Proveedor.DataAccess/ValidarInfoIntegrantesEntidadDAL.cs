﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad de validación de datos básicos del proveedor
    /// </summary>
    public class ValidarInfoIntegrantesEntidadDAL : GeneralDAL
    {
        public ValidarInfoIntegrantesEntidadDAL()
        { }



        /// <summary>
        /// Consulta una lista de entidades validación de datos de Integrantes de una Entidad
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>

        /// <returns>Lista de entidades validación de integrntes de un proveedor</returns>
        public List<ValidarInfoIntegrantesEntidad> ConsultarValidarInfoIntegrantesEntidadsResumen(Int32 pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoIntegrantesEntidad> vListaValidarTercero = new List<ValidarInfoIntegrantesEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoIntegrantesEntidad vValidarInfoIntegrantesEntidad = new ValidarInfoIntegrantesEntidad();

                            vValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad = vDataReaderResults["IdValidarInfoIntegrantesEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoIntegrantesEntidad"].ToString()) : vValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad;
                            vValidarInfoIntegrantesEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoIntegrantesEntidad.IdEntidad;
                            vValidarInfoIntegrantesEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoIntegrantesEntidad.NroRevision;
                            vValidarInfoIntegrantesEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoIntegrantesEntidad.Observaciones;
                            vValidarInfoIntegrantesEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoIntegrantesEntidad.ConfirmaYAprueba;
                            vValidarInfoIntegrantesEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoIntegrantesEntidad.UsuarioCrea;
                            vValidarInfoIntegrantesEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoIntegrantesEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoIntegrantesEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validación de  Info Integrantes  del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad validación de Integrantes del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validación de Integrantes del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad validación de Integrantess del proveedor</param>
        /// <returns>Lista de entidades validación de datos básicos del proveedor</returns>
        public List<ValidarInfoIntegrantesEntidad> ConsultarValidarInfoIntegrantesEntidad(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    if (pObservaciones != null)
                        vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pObservaciones);
                    if (pConfirmaYAprueba != null)
                        vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pConfirmaYAprueba);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValidarInfoIntegrantesEntidad> vListaValidarTercero = new List<ValidarInfoIntegrantesEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            ValidarInfoIntegrantesEntidad vValidarInfoIntegrantesEntidad = new ValidarInfoIntegrantesEntidad();

                            vValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad = vDataReaderResults["IdValidarInfoIntegrantesEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoIntegrantesEntidad"].ToString()) : vValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad;
                            vValidarInfoIntegrantesEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoIntegrantesEntidad.IdEntidad;
                            vValidarInfoIntegrantesEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoIntegrantesEntidad.NroRevision;
                            vValidarInfoIntegrantesEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoIntegrantesEntidad.Observaciones;
                            vValidarInfoIntegrantesEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoIntegrantesEntidad.ConfirmaYAprueba;
                            vValidarInfoIntegrantesEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoIntegrantesEntidad.UsuarioCrea;
                            vValidarInfoIntegrantesEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoIntegrantesEntidad.FechaCrea;

                            vListaValidarTercero.Add(vValidarInfoIntegrantesEntidad);
                        }
                        return vListaValidarTercero;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una entidad validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Entidad validación de datos básicos del proveedor</returns>
        public ValidarInfoIntegrantesEntidad ConsultarValidarInfoIntegrantesEntidad_Ultima(Int32 pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValidarInfoIntegrantesEntidad vValidarInfoIntegrantesEntidad = new ValidarInfoIntegrantesEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad = vDataReaderResults["IdValidarInfoIntegrantesEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValidarInfoIntegrantesEntidad"].ToString()) : vValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad;
                            vValidarInfoIntegrantesEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vValidarInfoIntegrantesEntidad.IdEntidad;
                            vValidarInfoIntegrantesEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NroRevision"].ToString()) : vValidarInfoIntegrantesEntidad.NroRevision;
                            vValidarInfoIntegrantesEntidad.Observaciones = vDataReaderResults["Observaciones"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Observaciones"].ToString()) : vValidarInfoIntegrantesEntidad.Observaciones;
                            vValidarInfoIntegrantesEntidad.ConfirmaYAprueba = vDataReaderResults["ConfirmaYAprueba"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConfirmaYAprueba"].ToString()) : vValidarInfoIntegrantesEntidad.ConfirmaYAprueba;
                            vValidarInfoIntegrantesEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValidarInfoIntegrantesEntidad.UsuarioCrea;
                            vValidarInfoIntegrantesEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValidarInfoIntegrantesEntidad.FechaCrea;
                        }
                        return vValidarInfoIntegrantesEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosIntegrantes">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarInfoIntegrantesEntidad(ValidarInfoIntegrantesEntidad pValidarInfoDatosIntegrantes, int idEstadoInfoDatosBasicosEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InsertarValidarInfoIntegrantesEntidad"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValidarInfoIntegrantesEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoDatosIntegrantes.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pValidarInfoDatosIntegrantes.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoDatosIntegrantes.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoDatosIntegrantes.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoDatosIntegrantes.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);
                    pValidarInfoDatosIntegrantes.IdValidarInfoIntegrantesEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValidarInfoIntegrantesEntidad").ToString());
                    //GenerarLogAuditoria(pValidarInfoDatosIntegrantes, vDbCommand);
                    //return vResultado;

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdEntidad", DbType.String, pValidarInfoDatosIntegrantes.IdEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoDatosIntegrantes", DbType.Int16, idEstadoInfoDatosBasicosEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoDatosIntegrantes.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        // GenerarLogAuditoria(pValidarInfoDatosIntegrantes, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoIntegrantesEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarValidarInfoIntegrantesEntidad(ValidarInfoIntegrantesEntidad pValidarInfoIntegrantesEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            DbTransaction vDbTransaction = null;
            DbConnection vDBConnection = null;
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Modificar"))
                {
                    vDBConnection = vDataBase.CreateConnection();
                    vDBConnection.Open();
                    vDbTransaction = vDBConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                    vDbCommand.Transaction = vDbTransaction;
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValidarInfoIntegrantesEntidad", DbType.Int32, pValidarInfoIntegrantesEntidad.IdValidarInfoIntegrantesEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int32, pValidarInfoIntegrantesEntidad.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pValidarInfoIntegrantesEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Observaciones", DbType.String, pValidarInfoIntegrantesEntidad.Observaciones);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaYAprueba", DbType.Boolean, pValidarInfoIntegrantesEntidad.ConfirmaYAprueba);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValidarInfoIntegrantesEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand, vDbTransaction);

                    GenerarLogAuditoria(pValidarInfoIntegrantesEntidad, vDbCommand);

                    using (DbCommand vDbCommand2 = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ModificarEstado"))
                    {

                        //vDBConnection = vDataBase.CreateConnection();
                        //vDbTransaction = vDBConnection.BeginTransaction();
                        vDbCommand2.Transaction = vDbTransaction;

                        vDataBase.AddInParameter(vDbCommand2, "@IdEntidad", DbType.String, pValidarInfoIntegrantesEntidad.IdEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@IdEstadoInfoDatosIntegrantes", DbType.Int16, idEstadoInfoDatosBasicosEntidad);

                        vDataBase.AddInParameter(vDbCommand2, "@UsuarioModifica", DbType.String, pValidarInfoIntegrantesEntidad.UsuarioCrea);

                        vResultado = vDataBase.ExecuteNonQuery(vDbCommand2, vDbTransaction);

                        GenerarLogAuditoria(pValidarInfoIntegrantesEntidad, vDbCommand2);

                        vDbTransaction.Commit();
                        vDBConnection.Close();

                        return vResultado;

                    }
                }
            }
            catch (UserInterfaceException)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw;
            }
            catch (Exception ex)
            {
                vDbTransaction.Rollback();
                vDBConnection.Close();
                throw new GenericException(ex);
            }
        }

    }
}
