using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Identificaci�n
    /// </summary>
    public class TipoDocIdentificaDAL : GeneralDAL
    {
        public TipoDocIdentificaDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificaci�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocIdentifica", DbType.Decimal, pTipoDocIdentifica.CodigoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoDocIdentifica.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoDocIdentifica.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoDocIdentifica.IdTipoDocIdentifica = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoDocIdentifica").ToString());
                    GenerarLogAuditoria(pTipoDocIdentifica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificaci�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentifica_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, pTipoDocIdentifica.IdTipoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoDocIdentifica", DbType.Decimal, pTipoDocIdentifica.CodigoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoDocIdentifica.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoDocIdentifica.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocIdentifica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificaci�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, pTipoDocIdentifica.IdTipoDocIdentifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoDocIdentifica, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Identificador en una entidad Tipo Identificaci�n</param>
        /// <returns>Entidad Tipo Identificaci�n</returns>
        public TipoDocIdentifica ConsultarTipoDocIdentifica(int pIdTipoDocIdentifica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentifica_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocIdentifica", DbType.Int32, pIdTipoDocIdentifica);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocIdentifica vTipoDocIdentifica = new TipoDocIdentifica();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocIdentifica.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vTipoDocIdentifica.IdTipoDocIdentifica;
                            vTipoDocIdentifica.CodigoDocIdentifica = vDataReaderResults["CodigoDocIdentifica"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CodigoDocIdentifica"].ToString()) : vTipoDocIdentifica.CodigoDocIdentifica;
                            vTipoDocIdentifica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocIdentifica.Descripcion;
                            vTipoDocIdentifica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocIdentifica.UsuarioCrea;
                            vTipoDocIdentifica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocIdentifica.FechaCrea;
                            vTipoDocIdentifica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocIdentifica.UsuarioModifica;
                            vTipoDocIdentifica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocIdentifica.FechaModifica;
                        }
                        return vTipoDocIdentifica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Identificaci�n
        /// </summary>
        /// <param name="pCodigoDocIdentifica">C�digo en una entidad Tipo Identificaci�n</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Identificaci�n</param>
        /// <returns>Lista de entidades Tipo Identificaci�n</returns>
        public List<TipoDocIdentifica> ConsultarTipoDocIdentificas(Decimal? pCodigoDocIdentifica, String pDescripcion)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar"))
                {
                    if(pCodigoDocIdentifica != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoDocIdentifica", DbType.Decimal, pCodigoDocIdentifica);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocIdentifica> vListaTipoDocIdentifica = new List<TipoDocIdentifica>();
                        while (vDataReaderResults.Read())
                        {
                                TipoDocIdentifica vTipoDocIdentifica = new TipoDocIdentifica();
                            vTipoDocIdentifica.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vTipoDocIdentifica.IdTipoDocIdentifica;
                            vTipoDocIdentifica.CodigoDocIdentifica = vDataReaderResults["CodigoDocIdentifica"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CodigoDocIdentifica"].ToString()) : vTipoDocIdentifica.CodigoDocIdentifica;
                            vTipoDocIdentifica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocIdentifica.Descripcion;
                            vTipoDocIdentifica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocIdentifica.UsuarioCrea;
                            vTipoDocIdentifica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocIdentifica.FechaCrea;
                            vTipoDocIdentifica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocIdentifica.UsuarioModifica;
                            vTipoDocIdentifica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocIdentifica.FechaModifica;
                                vListaTipoDocIdentifica.Add(vTipoDocIdentifica);
                        }
                        return vListaTipoDocIdentifica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Identificaci�n
        /// </summary>
        /// <returns>Lista de entidades Tipo Identificaci�n</returns>
        public List<TipoDocIdentifica> ConsultarTipoDocIdentificaAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentificas_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoDocIdentifica> vListaTipoDocIdentifica = new List<TipoDocIdentifica>();
                        while (vDataReaderResults.Read())
                        {
                            TipoDocIdentifica vTipoDocIdentifica = new TipoDocIdentifica();
                            vTipoDocIdentifica.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocIdentifica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocIdentifica"].ToString()) : vTipoDocIdentifica.IdTipoDocIdentifica;
                            vTipoDocIdentifica.CodigoDocIdentifica = vDataReaderResults["CodigoDocIdentifica"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["CodigoDocIdentifica"].ToString()) : vTipoDocIdentifica.CodigoDocIdentifica;
                            vTipoDocIdentifica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocIdentifica.Descripcion;
                            vTipoDocIdentifica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocIdentifica.UsuarioCrea;
                            vTipoDocIdentifica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocIdentifica.FechaCrea;
                            vTipoDocIdentifica.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoDocIdentifica.UsuarioModifica;
                            vTipoDocIdentifica.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoDocIdentifica.FechaModifica;
                            vListaTipoDocIdentifica.Add(vTipoDocIdentifica);
                        }
                        return vListaTipoDocIdentifica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene los tipos de documentos a trav�s del IDdel Documento y el Id del programa
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Identificador en una entidad Tipo Identificaci�n</param>
        /// <param name="idPrograma">Identificador del programa</param>
        /// <returns>Entidad Tipo Identificaci�n</returns>
        public TipoDocIdentifica ConsultarTipoDocIdentificaByIdPrograma(int pIdTipoDocIdentifica, string idPrograma)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocumento", DbType.Int32, pIdTipoDocIdentifica);
                    vDataBase.AddInParameter(vDbCommand, "@Programa", DbType.String, idPrograma);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoDocIdentifica vTipoDocIdentifica = new TipoDocIdentifica();
                        while (vDataReaderResults.Read())
                        {
                            vTipoDocIdentifica.IdTipoDocIdentifica = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vTipoDocIdentifica.IdTipoDocIdentifica;
                            vTipoDocIdentifica.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToString(vDataReaderResults["MaxPermitidoKB"].ToString()) : vTipoDocIdentifica.MaxPermitidoKB;
                            vTipoDocIdentifica.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vTipoDocIdentifica.ExtensionesPermitidas;
                            vTipoDocIdentifica.CodigoDocIdentifica = vDataReaderResults["CodigoTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodigoTipoDocumento"].ToString()) : vTipoDocIdentifica.CodigoDocIdentifica;
                            vTipoDocIdentifica.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoDocIdentifica.Descripcion;
                            vTipoDocIdentifica.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoDocIdentifica.FechaCrea;
                            vTipoDocIdentifica.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoDocIdentifica.UsuarioCrea;
                            
                        }
                        return vTipoDocIdentifica;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
