using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos para obtener, insertar, actualizar registros, entre otros para la entidad Integrantes
    /// </summary>
    public class IntegrantesDAL : GeneralDAL
    {
        public IntegrantesDAL()
        {
        }
        /// <summary>
        /// Método de inserción para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int InsertarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Integrantes_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdIntegrante", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIntegrantes.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIntegrantes.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionPersonaNatural", DbType.Int32, pIntegrantes.IdTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pIntegrantes.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeParticipacion", DbType.Decimal, pIntegrantes.PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaCertificado", DbType.String, pIntegrantes.ConfirmaCertificado);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaPersona", DbType.String, pIntegrantes.ConfirmaPersona);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pIntegrantes.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pIntegrantes.IdIntegrante = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdIntegrante").ToString());

                    GenerarLogAuditoria(pIntegrantes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int ModificarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Integrantes_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdIntegrante", DbType.Int32, pIntegrantes.IdIntegrante);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIntegrantes.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIntegrantes.IdTipoPersona);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoIdentificacionPersonaNatural", DbType.Int32, pIntegrantes.IdTipoIdentificacionPersonaNatural);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroIdentificacion", DbType.String, pIntegrantes.NumeroIdentificacion);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentajeParticipacion", DbType.Decimal, pIntegrantes.PorcentajeParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaCertificado", DbType.String, pIntegrantes.ConfirmaCertificado);
                    vDataBase.AddInParameter(vDbCommand, "@ConfirmaPersona", DbType.String, pIntegrantes.ConfirmaPersona);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pIntegrantes.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIntegrantes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int EliminarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Integrantes_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdIntegrante", DbType.Int32, pIntegrantes.IdIntegrante);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pIntegrantes, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método de consulta por id para la entidad Integrantes
        /// </summary>
        /// <param name="pIdIntegrante"></param>
        public Integrantes ConsultarIntegrantes(int pIdIntegrante)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Integrantes_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdIntegrante", DbType.Int32, pIdIntegrante);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Integrantes vIntegrantes = new Integrantes();
                        while (vDataReaderResults.Read())
                        {
                            vIntegrantes.IdIntegrante = vDataReaderResults["IdIntegrante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdIntegrante"].ToString()) : vIntegrantes.IdIntegrante;
                            vIntegrantes.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vIntegrantes.IdEntidad;
                            vIntegrantes.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vIntegrantes.IdTipoPersona;
                            vIntegrantes.IdTipoIdentificacionPersonaNatural = vDataReaderResults["IdTipoIdentificacionPersonaNatural"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoIdentificacionPersonaNatural"].ToString()) : vIntegrantes.IdTipoIdentificacionPersonaNatural;
                            vIntegrantes.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vIntegrantes.NumeroIdentificacion;
                            vIntegrantes.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vIntegrantes.PorcentajeParticipacion;
                            vIntegrantes.ConfirmaCertificado = vDataReaderResults["ConfirmaCertificado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConfirmaCertificado"].ToString()) : vIntegrantes.ConfirmaCertificado;
                            vIntegrantes.ConfirmaPersona = vDataReaderResults["ConfirmaPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConfirmaPersona"].ToString()) : vIntegrantes.ConfirmaPersona;
                            vIntegrantes.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vIntegrantes.UsuarioCrea;
                            vIntegrantes.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vIntegrantes.FechaCrea;
                            vIntegrantes.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vIntegrantes.UsuarioModifica;
                            vIntegrantes.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vIntegrantes.FechaModifica;
                        }
                        return vIntegrantes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Integrantes
        /// </summary>
        /// <param name="pIdTipoPersona"></param>
        /// <param name="pIdTipoIdentificacionPersonaNatural"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pPorcentajeParticipacion"></param>
        /// <param name="pConfirmaCertificado"></param>
        /// <param name="pConfirmaPersona"></param>
        public List<Integrantes> ConsultarIntegrantess(int? pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Integrantess_Consultar"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Integrantes> vListaIntegrantes = new List<Integrantes>();
                        while (vDataReaderResults.Read())
                        {
                            Integrantes vIntegrantes = new Integrantes();
                            vIntegrantes.IdIntegrante = vDataReaderResults["IdIntegrante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdIntegrante"].ToString()) : vIntegrantes.IdIntegrante;
                            vIntegrantes.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vIntegrantes.IdTipoPersona;
                            vIntegrantes.Integrante = vDataReaderResults["Integrante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Integrante"].ToString()) : vIntegrantes.Integrante;
                            vIntegrantes.NombreTipoPersona = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vIntegrantes.NombreTipoPersona;
                            vIntegrantes.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vIntegrantes.PorcentajeParticipacion;
                            vIntegrantes.RepresentanteLegal = vDataReaderResults["Representante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Representante"].ToString()) : vIntegrantes.RepresentanteLegal;
                            vIntegrantes.LinkDocumento = vDataReaderResults["LinkDoc"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDoc"].ToString()) : vIntegrantes.LinkDocumento;
                            vListaIntegrantes.Add(vIntegrantes);
                        }
                        return vListaIntegrantes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Icbf.Proveedor.Entity.EntidadProvOferente ConsultarEntidadProvOferentes_SectorPrivado(String pTipoPersona, String pTipoidentificacion, String pIdentificacion, String pProveedor, String pEstado, String pUsuarioCrea)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_EntidadProvOferente_Consultar_ConTipoSector"))
                {

                    if (!string.IsNullOrEmpty(pTipoPersona))
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, Convert.ToInt32(pTipoPersona));
                    if (!string.IsNullOrEmpty(pTipoidentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@Tipoidentificacion", DbType.Int32, Convert.ToInt32(pTipoidentificacion));
                    if (!string.IsNullOrEmpty(pIdentificacion))
                        vDataBase.AddInParameter(vDbCommand, "@Identificacion", DbType.String, pIdentificacion);
                    if (!string.IsNullOrEmpty(pProveedor))
                        vDataBase.AddInParameter(vDbCommand, "@Proveedor", DbType.String, pProveedor);
                    if (!string.IsNullOrEmpty(pEstado))
                        vDataBase.AddInParameter(vDbCommand, "@IdEstado", DbType.Int32, Convert.ToInt32(pEstado));
                    if (!string.IsNullOrEmpty(pUsuarioCrea))
                        vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pUsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, 1);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
                        while (vDataReaderResults.Read())
                        {


                            vEntidadProvOferente.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vEntidadProvOferente.IdEntidad;
                            vEntidadProvOferente.ConsecutivoInterno = vDataReaderResults["ConsecutivoInterno"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ConsecutivoInterno"].ToString()) : vEntidadProvOferente.ConsecutivoInterno;
                            vEntidadProvOferente.IdTercero = vDataReaderResults["IdTercero"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTercero"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.ObserValidador = vDataReaderResults["ObserValidador"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObserValidador"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.Proveedor = vDataReaderResults["Razonsocila"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Razonsocila"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.TipoIdentificacion = vDataReaderResults["CodDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodDocumento"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.NumeroIdentificacion = vDataReaderResults["NumeroIdentificacion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroIdentificacion"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vEntidadProvOferente.IdTercero;
                            vEntidadProvOferente.TipoPersonaNombre = vDataReaderResults["NombreTipoPersona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreTipoPersona"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Estado"].ToString()) : vEntidadProvOferente.ObserValidador;
                            vEntidadProvOferente.EstadoProveedor = vDataReaderResults["EstadoProveedor"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoProveedor"].ToString()) : vEntidadProvOferente.EstadoProveedor;
                            vEntidadProvOferente.IdEstadoProveedor = vDataReaderResults["IdEstadoProveedor"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEstadoProveedor"].ToString()) : vEntidadProvOferente.IdEstadoProveedor;
                            vEntidadProvOferente.IdTipoSector = vDataReaderResults["IdTipoSector"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSector"].ToString()) : vEntidadProvOferente.IdTipoSector;
                            vEntidadProvOferente.TipoEntOfProv = vDataReaderResults["TipoEntOfProv"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["TipoEntOfProv"].ToString()) : vEntidadProvOferente.TipoEntOfProv;
                            vEntidadProvOferente.DV = vDataReaderResults["DIGITOVERIFICACION"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["DIGITOVERIFICACION"].ToString()) : vEntidadProvOferente.DV;

                        }
                        return vEntidadProvOferente;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método que valida el numero de integrantes registrados contra el numero de integrantes definido en datos básicos por el proveedor
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public Integrantes ValidaNumIntegrantes(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDOR_Integrantes_ValidaIntegrantes"))
                {

                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.String, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Integrantes vIntegrantes = new Integrantes();
                        while (vDataReaderResults.Read())
                        {
                            vIntegrantes.IntegrantesReg = vDataReaderResults["Integrantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Integrantes"].ToString()) : vIntegrantes.IntegrantesReg;
                            vIntegrantes.NumIntegrantes = vDataReaderResults["NumIntegrantes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumIntegrantes"].ToString()) : vIntegrantes.NumIntegrantes;
                            vIntegrantes.IdTipoPersona = vDataReaderResults["IdTipoPersona"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPersona"].ToString()) : vIntegrantes.IdTipoPersona;
                            vIntegrantes.PorcentajeParticipacion = vDataReaderResults["PorcentajeParticipacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentajeParticipacion"].ToString()) : vIntegrantes.PorcentajeParticipacion;
                        }
                        return vIntegrantes;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
