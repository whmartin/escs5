﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;


namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los métodos  insertar y eliminar para la entidad códigos UNSPSC de proveedor
    /// </summary>

    public class CodigoUNSPSCProveedorDAL : GeneralDAL
    {
        public CodigoUNSPSCProveedorDAL()
        {
        }
        /// <summary>
        /// Inserta una nueva entidad Códigos UNSPSC de proveedor
        /// </summary>
        /// <param name="pCodigoUNSPSCProveedorEntidad">Entidad Códigos UNSPSC de proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarCodigoUNSPSCProveedorEntidad(CodigoUNSPSCProveedor pCodigoUNSPSCProveedorEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_CodigoUNSPSCGestionProveedor_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@CodUNSPSC", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdTercero", DbType.Int32, pCodigoUNSPSCProveedorEntidad.IDTERCERO);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pCodigoUNSPSCProveedorEntidad.IdTipoCodUNSPSC);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pCodigoUNSPSCProveedorEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pCodigoUNSPSCProveedorEntidad.CodUNSPSC = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@CodUNSPSC").ToString());
                    GenerarLogAuditoria(pCodigoUNSPSCProveedorEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Códigos UNSPSC de Proveedor
        /// </summary>
        /// <param name="pCodigoUNSPSCProveedor">Entidad Códigos UNSPSC de Proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarCodigoUNSPSCProveedor(CodigoUNSPSCProveedor pCodigoUNSPSCProveedor)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_CodigoUNSPSCGestionProveedor_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@CodUNSPSC", DbType.Int32, pCodigoUNSPSCProveedor.CodUNSPSC);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pCodigoUNSPSCProveedor, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consultar  Códigos UNSPSC de un proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad</param>
        /// <returns>Entidad Código UNSPSC</returns>
        public List<CodigoUNSPSCProveedor> ConsultarCodigoUNSPSCProveedor(int pIdTercero)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_CodigoUNSPSCGestionProveedor_Consultar"))

                {
                    vDataBase.AddInParameter(vDbCommand, "@idTercero", DbType.Int32, pIdTercero);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<CodigoUNSPSCProveedor> vListaCodigoUNSPSC = new List<CodigoUNSPSCProveedor>();

                        while (vDataReaderResults.Read())
                        {
                            CodigoUNSPSCProveedor vCodigoUNSPSC = new CodigoUNSPSCProveedor();

                            vCodigoUNSPSC.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vCodigoUNSPSC.IdTipoCodUNSPSC;
                            vCodigoUNSPSC.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vCodigoUNSPSC.Codigo;
                            vCodigoUNSPSC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vCodigoUNSPSC.Descripcion;
                            vCodigoUNSPSC.CodUNSPSC = vDataReaderResults["CodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["CodUNSPSC"].ToString()) : vCodigoUNSPSC.CodUNSPSC;
                            
                            vListaCodigoUNSPSC.Add(vCodigoUNSPSC);
                        }
                        return vListaCodigoUNSPSC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}


