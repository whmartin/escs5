using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad documentos adjuntos a informaci�n de experiencia
    /// </summary>
    public class DocExperienciaEntidadDAL : GeneralDAL
    {
        public DocExperienciaEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, 18);
                    if (pDocExperienciaEntidad.IdExpEntidad != 0)
                    {
                        vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pDocExperienciaEntidad.IdExpEntidad);
                    }
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pDocExperienciaEntidad.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocExperienciaEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocExperienciaEntidad.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pDocExperienciaEntidad.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pDocExperienciaEntidad.IdTemporal);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pDocExperienciaEntidad.IdDocAdjunto = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdDocAdjunto").ToString());
                    GenerarLogAuditoria(pDocExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// modifica una entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocExperienciaEntidad.IdDocAdjunto);
                    if (pDocExperienciaEntidad.IdExpEntidad > 0) { vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pDocExperienciaEntidad.IdExpEntidad); }
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoDocumento", DbType.Int32, pDocExperienciaEntidad.IdTipoDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDocExperienciaEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pDocExperienciaEntidad.LinkDocumento);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pDocExperienciaEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pDocExperienciaEntidad.IdDocAdjunto);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pDocExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Entidad Documentos adjuntos a informaci�n de experiencia</returns>
        public DocExperienciaEntidad ConsultarDocExperienciaEntidad(int pIdDocAdjunto)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdDocAdjunto", DbType.Int32, pIdDocAdjunto);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        DocExperienciaEntidad vDocExperienciaEntidad = new DocExperienciaEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vDocExperienciaEntidad.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocExperienciaEntidad.IdDocAdjunto;
                            vDocExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDocExperienciaEntidad.IdExpEntidad;
                            vDocExperienciaEntidad.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocExperienciaEntidad.IdTipoDocumento;
                            vDocExperienciaEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocExperienciaEntidad.Descripcion;
                            vDocExperienciaEntidad.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocExperienciaEntidad.LinkDocumento;
                            vDocExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocExperienciaEntidad.UsuarioCrea;
                            vDocExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocExperienciaEntidad.FechaCrea;
                            vDocExperienciaEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocExperienciaEntidad.UsuarioModifica;
                            vDocExperienciaEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocExperienciaEntidad.FechaModifica;
                        }
                        return vDocExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de experiencia en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pLinkDocumento">Liga del documento en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a informaci�n de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidads(int ?pIdExpEntidad, String pDescripcion, String pLinkDocumento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar"))
                {
                    if (pIdExpEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pIdExpEntidad);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pLinkDocumento != null)
                         vDataBase.AddInParameter(vDbCommand, "@LinkDocumento", DbType.String, pLinkDocumento);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocExperienciaEntidad> vListaDocExperienciaEntidad = new List<DocExperienciaEntidad>();
                        while (vDataReaderResults.Read())
                        {
                                DocExperienciaEntidad vDocExperienciaEntidad = new DocExperienciaEntidad();
                            vDocExperienciaEntidad.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocExperienciaEntidad.IdDocAdjunto;
                            vDocExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDocExperienciaEntidad.IdExpEntidad;
                            vDocExperienciaEntidad.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocExperienciaEntidad.IdTipoDocumento;
                            vDocExperienciaEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocExperienciaEntidad.Descripcion;
                            vDocExperienciaEntidad.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocExperienciaEntidad.LinkDocumento;
                            vDocExperienciaEntidad.DescripcionDocumento = vDataReaderResults["DescripcionDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionDocumento"].ToString()) : vDocExperienciaEntidad.DescripcionDocumento;
                            vDocExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocExperienciaEntidad.UsuarioCrea;
                            vDocExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocExperienciaEntidad.FechaCrea;
                            vDocExperienciaEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocExperienciaEntidad.UsuarioModifica;
                            vDocExperienciaEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocExperienciaEntidad.FechaModifica;
                                vListaDocExperienciaEntidad.Add(vDocExperienciaEntidad);
                        }
                        return vListaDocExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de experiencia en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pRupRenovado">Rup Renovado en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a informaci�n de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(int pIdExpEntidad, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdExpEntidad_Rup"))
                {
                    if (pIdExpEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pIdExpEntidad);
                    if (pRupRenovado != null)
                        vDataBase.AddInParameter(vDbCommand, "@RupRenovado", DbType.String, pRupRenovado);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);
                    if (pTipoSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoSector", DbType.String, pTipoSector);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocExperienciaEntidad> vListaDocExperienciaEntidad = new List<DocExperienciaEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            DocExperienciaEntidad vDocExperienciaEntidad = new DocExperienciaEntidad();
                            vDocExperienciaEntidad.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocExperienciaEntidad.IdDocAdjunto;
                            vDocExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDocExperienciaEntidad.IdExpEntidad;
                            vDocExperienciaEntidad.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocExperienciaEntidad.IdTipoDocumento;
                            vDocExperienciaEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocExperienciaEntidad.Descripcion;
                            vDocExperienciaEntidad.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocExperienciaEntidad.LinkDocumento;
                            vDocExperienciaEntidad.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocExperienciaEntidad.NombreDocumento;
                            vDocExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocExperienciaEntidad.UsuarioCrea;
                            vDocExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocExperienciaEntidad.FechaCrea;
                            vDocExperienciaEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocExperienciaEntidad.UsuarioModifica;
                            vDocExperienciaEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocExperienciaEntidad.FechaModifica;
                            vDocExperienciaEntidad.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDocExperienciaEntidad.Obligatorio;
                            vDocExperienciaEntidad.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocExperienciaEntidad.MaxPermitidoKB;
                            vDocExperienciaEntidad.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocExperienciaEntidad.ExtensionesPermitidas;
                            vDocExperienciaEntidad.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocExperienciaEntidad.IdTemporal;
                            vListaDocExperienciaEntidad.Add(vDocExperienciaEntidad);
                        }
                        return vListaDocExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdTemporal">Identificador del temporal en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pRupRenovado">Rup Renovado en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a informaci�n de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidad_IdTemporal_Rup(string pIdTemporal, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_DocExperienciaEntidad_Consultar_IdTemporal_Rup"))
                {
                    if (pIdTemporal != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pIdTemporal);
                    if (pRupRenovado != null)
                        vDataBase.AddInParameter(vDbCommand, "@RupRenovado", DbType.String, pRupRenovado);
                    if (pTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoPersona", DbType.String, pTipoPersona);
                    if (pTipoSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@TipoSector", DbType.String, pTipoSector);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<DocExperienciaEntidad> vListaDocExperienciaEntidad = new List<DocExperienciaEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            DocExperienciaEntidad vDocExperienciaEntidad = new DocExperienciaEntidad();
                            vDocExperienciaEntidad.IdDocAdjunto = vDataReaderResults["IdDocAdjunto"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDocAdjunto"].ToString()) : vDocExperienciaEntidad.IdDocAdjunto;
                            vDocExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vDocExperienciaEntidad.IdExpEntidad;
                            vDocExperienciaEntidad.IdTipoDocumento = vDataReaderResults["IdTipoDocumento"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoDocumento"].ToString()) : vDocExperienciaEntidad.IdTipoDocumento;
                            vDocExperienciaEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vDocExperienciaEntidad.Descripcion;
                            vDocExperienciaEntidad.LinkDocumento = vDataReaderResults["LinkDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["LinkDocumento"].ToString()) : vDocExperienciaEntidad.LinkDocumento;
                            vDocExperienciaEntidad.NombreDocumento = vDataReaderResults["NombreDocumento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreDocumento"].ToString()) : vDocExperienciaEntidad.NombreDocumento;
                            vDocExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vDocExperienciaEntidad.UsuarioCrea;
                            vDocExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vDocExperienciaEntidad.FechaCrea;
                            vDocExperienciaEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vDocExperienciaEntidad.UsuarioModifica;
                            vDocExperienciaEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vDocExperienciaEntidad.FechaModifica;
                            vDocExperienciaEntidad.Obligatorio = vDataReaderResults["Obligatorio"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["Obligatorio"].ToString()) : vDocExperienciaEntidad.Obligatorio;
                            vDocExperienciaEntidad.MaxPermitidoKB = vDataReaderResults["MaxPermitidoKB"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["MaxPermitidoKB"].ToString()) : vDocExperienciaEntidad.MaxPermitidoKB;
                            vDocExperienciaEntidad.ExtensionesPermitidas = vDataReaderResults["ExtensionesPermitidas"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ExtensionesPermitidas"].ToString()) : vDocExperienciaEntidad.ExtensionesPermitidas;
                            vDocExperienciaEntidad.IdTemporal = vDataReaderResults["IdTemporal"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdTemporal"].ToString()) : vDocExperienciaEntidad.IdTemporal;
                            vListaDocExperienciaEntidad.Add(vDocExperienciaEntidad);
                        }
                        return vListaDocExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
