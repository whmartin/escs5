using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad informaci�n Administrativa
    /// </summary>
    public class InfoAdminEntidadDAL : GeneralDAL
    {
        public InfoAdminEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Informaci�n Administrativa</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdInfoAdmin", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pInfoAdminEntidad.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoAdminEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegTrib", DbType.Int32, pInfoAdminEntidad.IdTipoRegTrib);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrigenCapital", DbType.Int32, pInfoAdminEntidad.IdTipoOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoActividad", DbType.Int32, pInfoAdminEntidad.IdTipoActividad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidad", DbType.Int32, pInfoAdminEntidad.IdTipoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoNaturalezaJurid", DbType.Int32, pInfoAdminEntidad.IdTipoNaturalezaJurid);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRangosTrabajadores", DbType.Int32, pInfoAdminEntidad.IdTipoRangosTrabajadores);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRangosActivos", DbType.Int32, pInfoAdminEntidad.IdTipoRangosActivos);
                    vDataBase.AddInParameter(vDbCommand, "@IdRepLegal", DbType.Int32, pInfoAdminEntidad.IdRepLegal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCertificaTamano", DbType.Int32, pInfoAdminEntidad.IdTipoCertificaTamano);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidadPublica", DbType.Int32, pInfoAdminEntidad.IdTipoEntidadPublica);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamentoConstituida", DbType.Int32, pInfoAdminEntidad.IdDepartamentoConstituida);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipioConstituida", DbType.Int32, pInfoAdminEntidad.IdMunicipioConstituida);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamentoDirComercial", DbType.Int32, pInfoAdminEntidad.IdDepartamentoDirComercial);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipioDirComercial", DbType.Int32, pInfoAdminEntidad.IdMunicipioDirComercial);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionComercial", DbType.String, pInfoAdminEntidad.DireccionComercial);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.String, pInfoAdminEntidad.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@NombreComercial", DbType.String, pInfoAdminEntidad.NombreComercial);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEstablecimiento", DbType.String, pInfoAdminEntidad.NombreEstablecimiento);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pInfoAdminEntidad.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@PorctjPrivado", DbType.Int32, pInfoAdminEntidad.PorctjPrivado);
                    vDataBase.AddInParameter(vDbCommand, "@PorctjPublico", DbType.Int32, pInfoAdminEntidad.PorctjPublico);
                    vDataBase.AddInParameter(vDbCommand, "@SitioWeb", DbType.String, pInfoAdminEntidad.SitioWeb);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadAcreditadora", DbType.String, pInfoAdminEntidad.NombreEntidadAcreditadora);
                    vDataBase.AddInParameter(vDbCommand, "@Organigrama", DbType.Boolean, pInfoAdminEntidad.Organigrama);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPnalAnnoPrevio", DbType.Int32, pInfoAdminEntidad.TotalPnalAnnoPrevio);
                    vDataBase.AddInParameter(vDbCommand, "@VincLaboral", DbType.Int32, pInfoAdminEntidad.VincLaboral);
                    vDataBase.AddInParameter(vDbCommand, "@PrestServicios", DbType.Int32, pInfoAdminEntidad.PrestServicios);
                    vDataBase.AddInParameter(vDbCommand, "@Voluntariado", DbType.Int32, pInfoAdminEntidad.Voluntariado);
                    vDataBase.AddInParameter(vDbCommand, "@VoluntPermanente", DbType.Int32, pInfoAdminEntidad.VoluntPermanente);
                    vDataBase.AddInParameter(vDbCommand, "@Asociados", DbType.Int32, pInfoAdminEntidad.Asociados);
                    vDataBase.AddInParameter(vDbCommand, "@Mision", DbType.Int32, pInfoAdminEntidad.Mision);
                    vDataBase.AddInParameter(vDbCommand, "@PQRS", DbType.Boolean, pInfoAdminEntidad.PQRS);
                    vDataBase.AddInParameter(vDbCommand, "@GestionDocumental", DbType.Boolean, pInfoAdminEntidad.GestionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@AuditoriaInterna", DbType.Boolean, pInfoAdminEntidad.AuditoriaInterna);
                    vDataBase.AddInParameter(vDbCommand, "@ManProcedimiento", DbType.Boolean, pInfoAdminEntidad.ManProcedimiento);
                    vDataBase.AddInParameter(vDbCommand, "@ManPracticasAmbiente", DbType.Boolean, pInfoAdminEntidad.ManPracticasAmbiente);
                    vDataBase.AddInParameter(vDbCommand, "@ManComportOrg", DbType.Boolean, pInfoAdminEntidad.ManComportOrg);
                    vDataBase.AddInParameter(vDbCommand, "@ManFunciones", DbType.Boolean, pInfoAdminEntidad.ManFunciones);
                    vDataBase.AddInParameter(vDbCommand, "@ProcRegInfoContable", DbType.Boolean, pInfoAdminEntidad.ProcRegInfoContable);
                    vDataBase.AddInParameter(vDbCommand, "@PartMesasTerritoriales", DbType.Boolean, pInfoAdminEntidad.PartMesasTerritoriales);
                    vDataBase.AddInParameter(vDbCommand, "@PartAsocAgremia", DbType.Boolean, pInfoAdminEntidad.PartAsocAgremia);
                    vDataBase.AddInParameter(vDbCommand, "@PartConsejosComun", DbType.Boolean, pInfoAdminEntidad.PartConsejosComun);
                    vDataBase.AddInParameter(vDbCommand, "@ConvInterInst", DbType.Boolean, pInfoAdminEntidad.ConvInterInst);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccGral", DbType.Boolean, pInfoAdminEntidad.ProcSeleccGral);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccEtnico", DbType.Boolean, pInfoAdminEntidad.ProcSeleccEtnico);
                    vDataBase.AddInParameter(vDbCommand, "@PlanInduccCapac", DbType.Boolean, pInfoAdminEntidad.PlanInduccCapac);
                    vDataBase.AddInParameter(vDbCommand, "@EvalDesemp", DbType.Boolean, pInfoAdminEntidad.EvalDesemp);
                    vDataBase.AddInParameter(vDbCommand, "@PlanCualificacion", DbType.Boolean, pInfoAdminEntidad.PlanCualificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumSedes", DbType.Int32, pInfoAdminEntidad.NumSedes);
                    vDataBase.AddInParameter(vDbCommand, "@SedesPropias", DbType.Boolean, pInfoAdminEntidad.SedesPropias);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInfoAdminEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInfoAdminEntidad.IdInfoAdmin = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdInfoAdmin").ToString());
                    GenerarLogAuditoria(pInfoAdminEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Informaci�n Administrativa</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoAdmin", DbType.Int32, pInfoAdminEntidad.IdInfoAdmin);
                    vDataBase.AddInParameter(vDbCommand, "@IdVigencia", DbType.Int32, pInfoAdminEntidad.IdVigencia);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoAdminEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRegTrib", DbType.Int32, pInfoAdminEntidad.IdTipoRegTrib);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoOrigenCapital", DbType.Int32, pInfoAdminEntidad.IdTipoOrigenCapital);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoActividad", DbType.Int32, pInfoAdminEntidad.IdTipoActividad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidad", DbType.Int32, pInfoAdminEntidad.IdTipoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoNaturalezaJurid", DbType.Int32, pInfoAdminEntidad.IdTipoNaturalezaJurid);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRangosTrabajadores", DbType.Int32, pInfoAdminEntidad.IdTipoRangosTrabajadores);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRangosActivos", DbType.Int32, pInfoAdminEntidad.IdTipoRangosActivos);
                    vDataBase.AddInParameter(vDbCommand, "@IdRepLegal", DbType.Int32, pInfoAdminEntidad.IdRepLegal);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCertificaTamano", DbType.Int32, pInfoAdminEntidad.IdTipoCertificaTamano);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidadPublica", DbType.Int32, pInfoAdminEntidad.IdTipoEntidadPublica);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamentoConstituida", DbType.Int32, pInfoAdminEntidad.IdDepartamentoConstituida);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipioConstituida", DbType.Int32, pInfoAdminEntidad.IdMunicipioConstituida);
                    vDataBase.AddInParameter(vDbCommand, "@IdDepartamentoDirComercial", DbType.Int32, pInfoAdminEntidad.IdDepartamentoDirComercial);
                    vDataBase.AddInParameter(vDbCommand, "@IdMunicipioDirComercial", DbType.Int32, pInfoAdminEntidad.IdMunicipioDirComercial);
                    vDataBase.AddInParameter(vDbCommand, "@DireccionComercial", DbType.String, pInfoAdminEntidad.DireccionComercial);
                    vDataBase.AddInParameter(vDbCommand, "@IdZona", DbType.String, pInfoAdminEntidad.IdZona);
                    vDataBase.AddInParameter(vDbCommand, "@NombreComercial", DbType.String, pInfoAdminEntidad.NombreComercial);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEstablecimiento", DbType.String, pInfoAdminEntidad.NombreEstablecimiento);
                    vDataBase.AddInParameter(vDbCommand, "@Sigla", DbType.String, pInfoAdminEntidad.Sigla);
                    vDataBase.AddInParameter(vDbCommand, "@PorctjPrivado", DbType.Int32, pInfoAdminEntidad.PorctjPrivado);
                    vDataBase.AddInParameter(vDbCommand, "@PorctjPublico", DbType.Int32, pInfoAdminEntidad.PorctjPublico);
                    vDataBase.AddInParameter(vDbCommand, "@SitioWeb", DbType.String, pInfoAdminEntidad.SitioWeb);
                    vDataBase.AddInParameter(vDbCommand, "@NombreEntidadAcreditadora", DbType.String, pInfoAdminEntidad.NombreEntidadAcreditadora);
                    vDataBase.AddInParameter(vDbCommand, "@Organigrama", DbType.Boolean, pInfoAdminEntidad.Organigrama);
                    vDataBase.AddInParameter(vDbCommand, "@TotalPnalAnnoPrevio", DbType.Int32, pInfoAdminEntidad.TotalPnalAnnoPrevio);
                    vDataBase.AddInParameter(vDbCommand, "@VincLaboral", DbType.Int32, pInfoAdminEntidad.VincLaboral);
                    vDataBase.AddInParameter(vDbCommand, "@PrestServicios", DbType.Int32, pInfoAdminEntidad.PrestServicios);
                    vDataBase.AddInParameter(vDbCommand, "@Voluntariado", DbType.Int32, pInfoAdminEntidad.Voluntariado);
                    vDataBase.AddInParameter(vDbCommand, "@VoluntPermanente", DbType.Int32, pInfoAdminEntidad.VoluntPermanente);
                    vDataBase.AddInParameter(vDbCommand, "@Asociados", DbType.Int32, pInfoAdminEntidad.Asociados);
                    vDataBase.AddInParameter(vDbCommand, "@Mision", DbType.Int32, pInfoAdminEntidad.Mision);
                    vDataBase.AddInParameter(vDbCommand, "@PQRS", DbType.Boolean, pInfoAdminEntidad.PQRS);
                    vDataBase.AddInParameter(vDbCommand, "@GestionDocumental", DbType.Boolean, pInfoAdminEntidad.GestionDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@AuditoriaInterna", DbType.Boolean, pInfoAdminEntidad.AuditoriaInterna);
                    vDataBase.AddInParameter(vDbCommand, "@ManProcedimiento", DbType.Boolean, pInfoAdminEntidad.ManProcedimiento);
                    vDataBase.AddInParameter(vDbCommand, "@ManPracticasAmbiente", DbType.Boolean, pInfoAdminEntidad.ManPracticasAmbiente);
                    vDataBase.AddInParameter(vDbCommand, "@ManComportOrg", DbType.Boolean, pInfoAdminEntidad.ManComportOrg);
                    vDataBase.AddInParameter(vDbCommand, "@ManFunciones", DbType.Boolean, pInfoAdminEntidad.ManFunciones);
                    vDataBase.AddInParameter(vDbCommand, "@ProcRegInfoContable", DbType.Boolean, pInfoAdminEntidad.ProcRegInfoContable);
                    vDataBase.AddInParameter(vDbCommand, "@PartMesasTerritoriales", DbType.Boolean, pInfoAdminEntidad.PartMesasTerritoriales);
                    vDataBase.AddInParameter(vDbCommand, "@PartAsocAgremia", DbType.Boolean, pInfoAdminEntidad.PartAsocAgremia);
                    vDataBase.AddInParameter(vDbCommand, "@PartConsejosComun", DbType.Boolean, pInfoAdminEntidad.PartConsejosComun);
                    vDataBase.AddInParameter(vDbCommand, "@ConvInterInst", DbType.Boolean, pInfoAdminEntidad.ConvInterInst);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccGral", DbType.Boolean, pInfoAdminEntidad.ProcSeleccGral);
                    vDataBase.AddInParameter(vDbCommand, "@ProcSeleccEtnico", DbType.Boolean, pInfoAdminEntidad.ProcSeleccEtnico);
                    vDataBase.AddInParameter(vDbCommand, "@PlanInduccCapac", DbType.Boolean, pInfoAdminEntidad.PlanInduccCapac);
                    vDataBase.AddInParameter(vDbCommand, "@EvalDesemp", DbType.Boolean, pInfoAdminEntidad.EvalDesemp);
                    vDataBase.AddInParameter(vDbCommand, "@PlanCualificacion", DbType.Boolean, pInfoAdminEntidad.PlanCualificacion);
                    vDataBase.AddInParameter(vDbCommand, "@NumSedes", DbType.Int32, pInfoAdminEntidad.NumSedes);
                    vDataBase.AddInParameter(vDbCommand, "@SedesPropias", DbType.Boolean, pInfoAdminEntidad.SedesPropias);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInfoAdminEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoAdminEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Informaci�n Administrativa</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoAdmin", DbType.Int32, pInfoAdminEntidad.IdInfoAdmin);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoAdminEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pIdInfoAdmin">Identificador en una entidad Informaci�n Administrativa</param>
        /// <returns>Entidad Informaci�n Administrativa</returns>
        public InfoAdminEntidad ConsultarInfoAdminEntidad(int pIdInfoAdmin)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdInfoAdmin", DbType.Int32, pIdInfoAdmin);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InfoAdminEntidad vInfoAdminEntidad = new InfoAdminEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vInfoAdminEntidad.IdInfoAdmin = vDataReaderResults["IdInfoAdmin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoAdmin"].ToString()) : vInfoAdminEntidad.IdInfoAdmin;
                            vInfoAdminEntidad.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vInfoAdminEntidad.IdVigencia;
                            vInfoAdminEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoAdminEntidad.IdEntidad;
                            vInfoAdminEntidad.IdTipoRegTrib = vDataReaderResults["IdTipoRegTrib"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegTrib"].ToString()) : vInfoAdminEntidad.IdTipoRegTrib;
                            vInfoAdminEntidad.IdTipoOrigenCapital = vDataReaderResults["IdTipoOrigenCapital"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoOrigenCapital"].ToString()) : vInfoAdminEntidad.IdTipoOrigenCapital;
                            vInfoAdminEntidad.IdTipoActividad = vDataReaderResults["IdTipoActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoActividad"].ToString()) : vInfoAdminEntidad.IdTipoActividad;
                            vInfoAdminEntidad.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vInfoAdminEntidad.IdTipoEntidad;
                            vInfoAdminEntidad.IdTipoNaturalezaJurid = vDataReaderResults["IdTipoNaturalezaJurid"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNaturalezaJurid"].ToString()) : vInfoAdminEntidad.IdTipoNaturalezaJurid;
                            vInfoAdminEntidad.IdTipoRangosTrabajadores = vDataReaderResults["IdTipoRangosTrabajadores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangosTrabajadores"].ToString()) : vInfoAdminEntidad.IdTipoRangosTrabajadores;
                            vInfoAdminEntidad.IdTipoRangosActivos = vDataReaderResults["IdTipoRangosActivos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangosActivos"].ToString()) : vInfoAdminEntidad.IdTipoRangosActivos;
                            vInfoAdminEntidad.IdRepLegal = vDataReaderResults["IdRepLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRepLegal"].ToString()) : vInfoAdminEntidad.IdRepLegal;
                            vInfoAdminEntidad.IdTipoCertificaTamano = vDataReaderResults["IdTipoCertificaTamano"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCertificaTamano"].ToString()) : vInfoAdminEntidad.IdTipoCertificaTamano;
                            vInfoAdminEntidad.IdTipoEntidadPublica = vDataReaderResults["IdTipoEntidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidadPublica"].ToString()) : vInfoAdminEntidad.IdTipoEntidadPublica;
                            vInfoAdminEntidad.IdDepartamentoConstituida = vDataReaderResults["IdDepartamentoConstituida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoConstituida"].ToString()) : vInfoAdminEntidad.IdDepartamentoConstituida;
                            vInfoAdminEntidad.IdMunicipioConstituida = vDataReaderResults["IdMunicipioConstituida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipioConstituida"].ToString()) : vInfoAdminEntidad.IdMunicipioConstituida;
                            vInfoAdminEntidad.IdDepartamentoDirComercial = vDataReaderResults["IdDepartamentoDirComercial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoDirComercial"].ToString()) : vInfoAdminEntidad.IdDepartamentoDirComercial;
                            vInfoAdminEntidad.IdMunicipioDirComercial = vDataReaderResults["IdMunicipioDirComercial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipioDirComercial"].ToString()) : vInfoAdminEntidad.IdMunicipioDirComercial;
                            vInfoAdminEntidad.DireccionComercial = vDataReaderResults["DireccionComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionComercial"].ToString()) : vInfoAdminEntidad.DireccionComercial;
                            vInfoAdminEntidad.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdZona"].ToString()) : vInfoAdminEntidad.IdZona;
                            vInfoAdminEntidad.NombreComercial = vDataReaderResults["NombreComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComercial"].ToString()) : vInfoAdminEntidad.NombreComercial;
                            vInfoAdminEntidad.NombreEstablecimiento = vDataReaderResults["NombreEstablecimiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstablecimiento"].ToString()) : vInfoAdminEntidad.NombreEstablecimiento;
                            vInfoAdminEntidad.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vInfoAdminEntidad.Sigla;
                            vInfoAdminEntidad.PorctjPrivado = vDataReaderResults["PorctjPrivado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorctjPrivado"].ToString()) : vInfoAdminEntidad.PorctjPrivado;
                            vInfoAdminEntidad.PorctjPublico = vDataReaderResults["PorctjPublico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorctjPublico"].ToString()) : vInfoAdminEntidad.PorctjPublico;
                            vInfoAdminEntidad.SitioWeb = vDataReaderResults["SitioWeb"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SitioWeb"].ToString()) : vInfoAdminEntidad.SitioWeb;
                            vInfoAdminEntidad.NombreEntidadAcreditadora = vDataReaderResults["NombreEntidadAcreditadora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAcreditadora"].ToString()) : vInfoAdminEntidad.NombreEntidadAcreditadora;
                            vInfoAdminEntidad.Organigrama = vDataReaderResults["Organigrama"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Organigrama"].ToString()) : vInfoAdminEntidad.Organigrama;
                            vInfoAdminEntidad.TotalPnalAnnoPrevio = vDataReaderResults["TotalPnalAnnoPrevio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalPnalAnnoPrevio"].ToString()) : vInfoAdminEntidad.TotalPnalAnnoPrevio;
                            vInfoAdminEntidad.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VincLaboral"].ToString()) : vInfoAdminEntidad.VincLaboral;
                            vInfoAdminEntidad.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PrestServicios"].ToString()) : vInfoAdminEntidad.PrestServicios;
                            vInfoAdminEntidad.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Voluntariado"].ToString()) : vInfoAdminEntidad.Voluntariado;
                            vInfoAdminEntidad.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VoluntPermanente"].ToString()) : vInfoAdminEntidad.VoluntPermanente;
                            vInfoAdminEntidad.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Asociados"].ToString()) : vInfoAdminEntidad.Asociados;
                            vInfoAdminEntidad.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Mision"].ToString()) : vInfoAdminEntidad.Mision;
                            vInfoAdminEntidad.PQRS = vDataReaderResults["PQRS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PQRS"].ToString()) : vInfoAdminEntidad.PQRS;
                            vInfoAdminEntidad.GestionDocumental = vDataReaderResults["GestionDocumental"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GestionDocumental"].ToString()) : vInfoAdminEntidad.GestionDocumental;
                            vInfoAdminEntidad.AuditoriaInterna = vDataReaderResults["AuditoriaInterna"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AuditoriaInterna"].ToString()) : vInfoAdminEntidad.AuditoriaInterna;
                            vInfoAdminEntidad.ManProcedimiento = vDataReaderResults["ManProcedimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManProcedimiento"].ToString()) : vInfoAdminEntidad.ManProcedimiento;
                            vInfoAdminEntidad.ManPracticasAmbiente = vDataReaderResults["ManPracticasAmbiente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManPracticasAmbiente"].ToString()) : vInfoAdminEntidad.ManPracticasAmbiente;
                            vInfoAdminEntidad.ManComportOrg = vDataReaderResults["ManComportOrg"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManComportOrg"].ToString()) : vInfoAdminEntidad.ManComportOrg;
                            vInfoAdminEntidad.ManFunciones = vDataReaderResults["ManFunciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManFunciones"].ToString()) : vInfoAdminEntidad.ManFunciones;
                            vInfoAdminEntidad.ProcRegInfoContable = vDataReaderResults["ProcRegInfoContable"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcRegInfoContable"].ToString()) : vInfoAdminEntidad.ProcRegInfoContable;
                            vInfoAdminEntidad.PartMesasTerritoriales = vDataReaderResults["PartMesasTerritoriales"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartMesasTerritoriales"].ToString()) : vInfoAdminEntidad.PartMesasTerritoriales;
                            vInfoAdminEntidad.PartAsocAgremia = vDataReaderResults["PartAsocAgremia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartAsocAgremia"].ToString()) : vInfoAdminEntidad.PartAsocAgremia;
                            vInfoAdminEntidad.PartConsejosComun = vDataReaderResults["PartConsejosComun"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartConsejosComun"].ToString()) : vInfoAdminEntidad.PartConsejosComun;
                            vInfoAdminEntidad.ConvInterInst = vDataReaderResults["ConvInterInst"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvInterInst"].ToString()) : vInfoAdminEntidad.ConvInterInst;
                            vInfoAdminEntidad.ProcSeleccGral = vDataReaderResults["ProcSeleccGral"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccGral"].ToString()) : vInfoAdminEntidad.ProcSeleccGral;
                            vInfoAdminEntidad.ProcSeleccEtnico = vDataReaderResults["ProcSeleccEtnico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccEtnico"].ToString()) : vInfoAdminEntidad.ProcSeleccEtnico;
                            vInfoAdminEntidad.PlanInduccCapac = vDataReaderResults["PlanInduccCapac"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanInduccCapac"].ToString()) : vInfoAdminEntidad.PlanInduccCapac;
                            vInfoAdminEntidad.EvalDesemp = vDataReaderResults["EvalDesemp"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EvalDesemp"].ToString()) : vInfoAdminEntidad.EvalDesemp;
                            vInfoAdminEntidad.PlanCualificacion = vDataReaderResults["PlanCualificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanCualificacion"].ToString()) : vInfoAdminEntidad.PlanCualificacion;
                            vInfoAdminEntidad.NumSedes = vDataReaderResults["NumSedes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumSedes"].ToString()) : vInfoAdminEntidad.NumSedes;
                            vInfoAdminEntidad.SedesPropias = vDataReaderResults["SedesPropias"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["SedesPropias"].ToString()) : vInfoAdminEntidad.SedesPropias;
                            vInfoAdminEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoAdminEntidad.UsuarioCrea;
                            vInfoAdminEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoAdminEntidad.FechaCrea;
                            vInfoAdminEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoAdminEntidad.UsuarioModifica;
                            vInfoAdminEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoAdminEntidad.FechaModifica;
                        }
                        return vInfoAdminEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n Administrativa
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad Informaci�n Administrativa</param>
        /// <returns>Lista de entidades Informaci�n Administrativa</returns>
        public List<InfoAdminEntidad> ConsultarInfoAdminEntidads(int? pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidads_Consultar"))
                {
                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InfoAdminEntidad> vListaInfoAdminEntidad = new List<InfoAdminEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            InfoAdminEntidad vInfoAdminEntidad = new InfoAdminEntidad();
                            vInfoAdminEntidad.IdInfoAdmin = vDataReaderResults["IdInfoAdmin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoAdmin"].ToString()) : vInfoAdminEntidad.IdInfoAdmin;
                            vInfoAdminEntidad.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vInfoAdminEntidad.IdVigencia;
                            vInfoAdminEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoAdminEntidad.IdEntidad;
                            vInfoAdminEntidad.IdTipoRegTrib = vDataReaderResults["IdTipoRegTrib"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegTrib"].ToString()) : vInfoAdminEntidad.IdTipoRegTrib;
                            vInfoAdminEntidad.IdTipoOrigenCapital = vDataReaderResults["IdTipoOrigenCapital"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoOrigenCapital"].ToString()) : vInfoAdminEntidad.IdTipoOrigenCapital;
                            vInfoAdminEntidad.IdTipoActividad = vDataReaderResults["IdTipoActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoActividad"].ToString()) : vInfoAdminEntidad.IdTipoActividad;
                            vInfoAdminEntidad.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vInfoAdminEntidad.IdTipoEntidad;
                            vInfoAdminEntidad.IdTipoNaturalezaJurid = vDataReaderResults["IdTipoNaturalezaJurid"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNaturalezaJurid"].ToString()) : vInfoAdminEntidad.IdTipoNaturalezaJurid;
                            vInfoAdminEntidad.IdTipoRangosTrabajadores = vDataReaderResults["IdTipoRangosTrabajadores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangosTrabajadores"].ToString()) : vInfoAdminEntidad.IdTipoRangosTrabajadores;
                            vInfoAdminEntidad.IdTipoRangosActivos = vDataReaderResults["IdTipoRangosActivos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangosActivos"].ToString()) : vInfoAdminEntidad.IdTipoRangosActivos;
                            vInfoAdminEntidad.IdRepLegal = vDataReaderResults["IdRepLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRepLegal"].ToString()) : vInfoAdminEntidad.IdRepLegal;
                            vInfoAdminEntidad.IdTipoCertificaTamano = vDataReaderResults["IdTipoCertificaTamano"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCertificaTamano"].ToString()) : vInfoAdminEntidad.IdTipoCertificaTamano;
                            vInfoAdminEntidad.IdTipoEntidadPublica = vDataReaderResults["IdTipoEntidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidadPublica"].ToString()) : vInfoAdminEntidad.IdTipoEntidadPublica;
                            vInfoAdminEntidad.IdDepartamentoConstituida = vDataReaderResults["IdDepartamentoConstituida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoConstituida"].ToString()) : vInfoAdminEntidad.IdDepartamentoConstituida;
                            vInfoAdminEntidad.IdMunicipioConstituida = vDataReaderResults["IdMunicipioConstituida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipioConstituida"].ToString()) : vInfoAdminEntidad.IdMunicipioConstituida;
                            vInfoAdminEntidad.IdDepartamentoDirComercial = vDataReaderResults["IdDepartamentoDirComercial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoDirComercial"].ToString()) : vInfoAdminEntidad.IdDepartamentoDirComercial;
                            vInfoAdminEntidad.IdMunicipioDirComercial = vDataReaderResults["IdMunicipioDirComercial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipioDirComercial"].ToString()) : vInfoAdminEntidad.IdMunicipioDirComercial;
                            vInfoAdminEntidad.DireccionComercial = vDataReaderResults["DireccionComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionComercial"].ToString()) : vInfoAdminEntidad.DireccionComercial;
                            vInfoAdminEntidad.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdZona"].ToString()) : vInfoAdminEntidad.IdZona;
                            vInfoAdminEntidad.NombreComercial = vDataReaderResults["NombreComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComercial"].ToString()) : vInfoAdminEntidad.NombreComercial;
                            vInfoAdminEntidad.NombreEstablecimiento = vDataReaderResults["NombreEstablecimiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstablecimiento"].ToString()) : vInfoAdminEntidad.NombreEstablecimiento;
                            vInfoAdminEntidad.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vInfoAdminEntidad.Sigla;
                            vInfoAdminEntidad.PorctjPrivado = vDataReaderResults["PorctjPrivado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorctjPrivado"].ToString()) : vInfoAdminEntidad.PorctjPrivado;
                            vInfoAdminEntidad.PorctjPublico = vDataReaderResults["PorctjPublico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorctjPublico"].ToString()) : vInfoAdminEntidad.PorctjPublico;
                            vInfoAdminEntidad.SitioWeb = vDataReaderResults["SitioWeb"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SitioWeb"].ToString()) : vInfoAdminEntidad.SitioWeb;
                            vInfoAdminEntidad.NombreEntidadAcreditadora = vDataReaderResults["NombreEntidadAcreditadora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAcreditadora"].ToString()) : vInfoAdminEntidad.NombreEntidadAcreditadora;
                            vInfoAdminEntidad.Organigrama = vDataReaderResults["Organigrama"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Organigrama"].ToString()) : vInfoAdminEntidad.Organigrama;
                            vInfoAdminEntidad.TotalPnalAnnoPrevio = vDataReaderResults["TotalPnalAnnoPrevio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalPnalAnnoPrevio"].ToString()) : vInfoAdminEntidad.TotalPnalAnnoPrevio;
                            vInfoAdminEntidad.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VincLaboral"].ToString()) : vInfoAdminEntidad.VincLaboral;
                            vInfoAdminEntidad.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PrestServicios"].ToString()) : vInfoAdminEntidad.PrestServicios;
                            vInfoAdminEntidad.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Voluntariado"].ToString()) : vInfoAdminEntidad.Voluntariado;
                            vInfoAdminEntidad.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VoluntPermanente"].ToString()) : vInfoAdminEntidad.VoluntPermanente;
                            vInfoAdminEntidad.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Asociados"].ToString()) : vInfoAdminEntidad.Asociados;
                            vInfoAdminEntidad.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Mision"].ToString()) : vInfoAdminEntidad.Mision;
                            vInfoAdminEntidad.PQRS = vDataReaderResults["PQRS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PQRS"].ToString()) : vInfoAdminEntidad.PQRS;
                            vInfoAdminEntidad.GestionDocumental = vDataReaderResults["GestionDocumental"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GestionDocumental"].ToString()) : vInfoAdminEntidad.GestionDocumental;
                            vInfoAdminEntidad.AuditoriaInterna = vDataReaderResults["AuditoriaInterna"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AuditoriaInterna"].ToString()) : vInfoAdminEntidad.AuditoriaInterna;
                            vInfoAdminEntidad.ManProcedimiento = vDataReaderResults["ManProcedimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManProcedimiento"].ToString()) : vInfoAdminEntidad.ManProcedimiento;
                            vInfoAdminEntidad.ManPracticasAmbiente = vDataReaderResults["ManPracticasAmbiente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManPracticasAmbiente"].ToString()) : vInfoAdminEntidad.ManPracticasAmbiente;
                            vInfoAdminEntidad.ManComportOrg = vDataReaderResults["ManComportOrg"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManComportOrg"].ToString()) : vInfoAdminEntidad.ManComportOrg;
                            vInfoAdminEntidad.ManFunciones = vDataReaderResults["ManFunciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManFunciones"].ToString()) : vInfoAdminEntidad.ManFunciones;
                            vInfoAdminEntidad.ProcRegInfoContable = vDataReaderResults["ProcRegInfoContable"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcRegInfoContable"].ToString()) : vInfoAdminEntidad.ProcRegInfoContable;
                            vInfoAdminEntidad.PartMesasTerritoriales = vDataReaderResults["PartMesasTerritoriales"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartMesasTerritoriales"].ToString()) : vInfoAdminEntidad.PartMesasTerritoriales;
                            vInfoAdminEntidad.PartAsocAgremia = vDataReaderResults["PartAsocAgremia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartAsocAgremia"].ToString()) : vInfoAdminEntidad.PartAsocAgremia;
                            vInfoAdminEntidad.PartConsejosComun = vDataReaderResults["PartConsejosComun"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartConsejosComun"].ToString()) : vInfoAdminEntidad.PartConsejosComun;
                            vInfoAdminEntidad.ConvInterInst = vDataReaderResults["ConvInterInst"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvInterInst"].ToString()) : vInfoAdminEntidad.ConvInterInst;
                            vInfoAdminEntidad.ProcSeleccGral = vDataReaderResults["ProcSeleccGral"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccGral"].ToString()) : vInfoAdminEntidad.ProcSeleccGral;
                            vInfoAdminEntidad.ProcSeleccEtnico = vDataReaderResults["ProcSeleccEtnico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccEtnico"].ToString()) : vInfoAdminEntidad.ProcSeleccEtnico;
                            vInfoAdminEntidad.PlanInduccCapac = vDataReaderResults["PlanInduccCapac"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanInduccCapac"].ToString()) : vInfoAdminEntidad.PlanInduccCapac;
                            vInfoAdminEntidad.EvalDesemp = vDataReaderResults["EvalDesemp"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EvalDesemp"].ToString()) : vInfoAdminEntidad.EvalDesemp;
                            vInfoAdminEntidad.PlanCualificacion = vDataReaderResults["PlanCualificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanCualificacion"].ToString()) : vInfoAdminEntidad.PlanCualificacion;
                            vInfoAdminEntidad.NumSedes = vDataReaderResults["NumSedes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumSedes"].ToString()) : vInfoAdminEntidad.NumSedes;
                            vInfoAdminEntidad.SedesPropias = vDataReaderResults["SedesPropias"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["SedesPropias"].ToString()) : vInfoAdminEntidad.SedesPropias;
                            vInfoAdminEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoAdminEntidad.UsuarioCrea;
                            vInfoAdminEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoAdminEntidad.FechaCrea;
                            vInfoAdminEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoAdminEntidad.UsuarioModifica;
                            vInfoAdminEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoAdminEntidad.FechaModifica;
                            vListaInfoAdminEntidad.Add(vInfoAdminEntidad);
                        }
                        return vListaInfoAdminEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad Informaci�n Administrativa</param>
        /// <returns>Entidad Informaci�n Administrativa</returns>
        public InfoAdminEntidad ConsultarInfoAdminEntidadIdEntidad(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoAdminEntidad_ConsultarIdEntidad"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InfoAdminEntidad vInfoAdminEntidad = new InfoAdminEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vInfoAdminEntidad.IdInfoAdmin = vDataReaderResults["IdInfoAdmin"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdInfoAdmin"].ToString()) : vInfoAdminEntidad.IdInfoAdmin;
                            vInfoAdminEntidad.IdVigencia = vDataReaderResults["IdVigencia"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdVigencia"].ToString()) : vInfoAdminEntidad.IdVigencia;
                            vInfoAdminEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoAdminEntidad.IdEntidad;
                            vInfoAdminEntidad.IdTipoRegTrib = vDataReaderResults["IdTipoRegTrib"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRegTrib"].ToString()) : vInfoAdminEntidad.IdTipoRegTrib;
                            vInfoAdminEntidad.IdTipoOrigenCapital = vDataReaderResults["IdTipoOrigenCapital"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoOrigenCapital"].ToString()) : vInfoAdminEntidad.IdTipoOrigenCapital;
                            vInfoAdminEntidad.IdTipoActividad = vDataReaderResults["IdTipoActividad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoActividad"].ToString()) : vInfoAdminEntidad.IdTipoActividad;
                            vInfoAdminEntidad.IdTipoEntidad = vDataReaderResults["IdTipoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidad"].ToString()) : vInfoAdminEntidad.IdTipoEntidad;
                            vInfoAdminEntidad.IdTipoNaturalezaJurid = vDataReaderResults["IdTipoNaturalezaJurid"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoNaturalezaJurid"].ToString()) : vInfoAdminEntidad.IdTipoNaturalezaJurid;
                            vInfoAdminEntidad.IdTipoRangosTrabajadores = vDataReaderResults["IdTipoRangosTrabajadores"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangosTrabajadores"].ToString()) : vInfoAdminEntidad.IdTipoRangosTrabajadores;
                            vInfoAdminEntidad.IdTipoRangosActivos = vDataReaderResults["IdTipoRangosActivos"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangosActivos"].ToString()) : vInfoAdminEntidad.IdTipoRangosActivos;
                            vInfoAdminEntidad.IdRepLegal = vDataReaderResults["IdRepLegal"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRepLegal"].ToString()) : vInfoAdminEntidad.IdRepLegal;
                            vInfoAdminEntidad.IdTipoCertificaTamano = vDataReaderResults["IdTipoCertificaTamano"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCertificaTamano"].ToString()) : vInfoAdminEntidad.IdTipoCertificaTamano;
                            vInfoAdminEntidad.IdTipoEntidadPublica = vDataReaderResults["IdTipoEntidadPublica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidadPublica"].ToString()) : vInfoAdminEntidad.IdTipoEntidadPublica;
                            vInfoAdminEntidad.IdDepartamentoConstituida = vDataReaderResults["IdDepartamentoConstituida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoConstituida"].ToString()) : vInfoAdminEntidad.IdDepartamentoConstituida;
                            vInfoAdminEntidad.IdMunicipioConstituida = vDataReaderResults["IdMunicipioConstituida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipioConstituida"].ToString()) : vInfoAdminEntidad.IdMunicipioConstituida;
                            vInfoAdminEntidad.IdDepartamentoDirComercial = vDataReaderResults["IdDepartamentoDirComercial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdDepartamentoDirComercial"].ToString()) : vInfoAdminEntidad.IdDepartamentoDirComercial;
                            vInfoAdminEntidad.IdMunicipioDirComercial = vDataReaderResults["IdMunicipioDirComercial"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdMunicipioDirComercial"].ToString()) : vInfoAdminEntidad.IdMunicipioDirComercial;
                            vInfoAdminEntidad.DireccionComercial = vDataReaderResults["DireccionComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DireccionComercial"].ToString()) : vInfoAdminEntidad.DireccionComercial;
                            vInfoAdminEntidad.IdZona = vDataReaderResults["IdZona"] != DBNull.Value ? Convert.ToString(vDataReaderResults["IdZona"].ToString()) : vInfoAdminEntidad.IdZona;
                            vInfoAdminEntidad.NombreComercial = vDataReaderResults["NombreComercial"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreComercial"].ToString()) : vInfoAdminEntidad.NombreComercial;
                            vInfoAdminEntidad.NombreEstablecimiento = vDataReaderResults["NombreEstablecimiento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEstablecimiento"].ToString()) : vInfoAdminEntidad.NombreEstablecimiento;
                            vInfoAdminEntidad.Sigla = vDataReaderResults["Sigla"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Sigla"].ToString()) : vInfoAdminEntidad.Sigla;
                            vInfoAdminEntidad.PorctjPrivado = vDataReaderResults["PorctjPrivado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorctjPrivado"].ToString()) : vInfoAdminEntidad.PorctjPrivado;
                            vInfoAdminEntidad.PorctjPublico = vDataReaderResults["PorctjPublico"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PorctjPublico"].ToString()) : vInfoAdminEntidad.PorctjPublico;
                            vInfoAdminEntidad.SitioWeb = vDataReaderResults["SitioWeb"] != DBNull.Value ? Convert.ToString(vDataReaderResults["SitioWeb"].ToString()) : vInfoAdminEntidad.SitioWeb;
                            vInfoAdminEntidad.NombreEntidadAcreditadora = vDataReaderResults["NombreEntidadAcreditadora"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreEntidadAcreditadora"].ToString()) : vInfoAdminEntidad.NombreEntidadAcreditadora;
                            vInfoAdminEntidad.Organigrama = vDataReaderResults["Organigrama"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Organigrama"].ToString()) : vInfoAdminEntidad.Organigrama;
                            vInfoAdminEntidad.TotalPnalAnnoPrevio = vDataReaderResults["TotalPnalAnnoPrevio"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["TotalPnalAnnoPrevio"].ToString()) : vInfoAdminEntidad.TotalPnalAnnoPrevio;
                            vInfoAdminEntidad.VincLaboral = vDataReaderResults["VincLaboral"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VincLaboral"].ToString()) : vInfoAdminEntidad.VincLaboral;
                            vInfoAdminEntidad.PrestServicios = vDataReaderResults["PrestServicios"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["PrestServicios"].ToString()) : vInfoAdminEntidad.PrestServicios;
                            vInfoAdminEntidad.Voluntariado = vDataReaderResults["Voluntariado"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Voluntariado"].ToString()) : vInfoAdminEntidad.Voluntariado;
                            vInfoAdminEntidad.VoluntPermanente = vDataReaderResults["VoluntPermanente"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["VoluntPermanente"].ToString()) : vInfoAdminEntidad.VoluntPermanente;
                            vInfoAdminEntidad.Asociados = vDataReaderResults["Asociados"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Asociados"].ToString()) : vInfoAdminEntidad.Asociados;
                            vInfoAdminEntidad.Mision = vDataReaderResults["Mision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["Mision"].ToString()) : vInfoAdminEntidad.Mision;
                            vInfoAdminEntidad.PQRS = vDataReaderResults["PQRS"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PQRS"].ToString()) : vInfoAdminEntidad.PQRS;
                            vInfoAdminEntidad.GestionDocumental = vDataReaderResults["GestionDocumental"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["GestionDocumental"].ToString()) : vInfoAdminEntidad.GestionDocumental;
                            vInfoAdminEntidad.AuditoriaInterna = vDataReaderResults["AuditoriaInterna"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AuditoriaInterna"].ToString()) : vInfoAdminEntidad.AuditoriaInterna;
                            vInfoAdminEntidad.ManProcedimiento = vDataReaderResults["ManProcedimiento"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManProcedimiento"].ToString()) : vInfoAdminEntidad.ManProcedimiento;
                            vInfoAdminEntidad.ManPracticasAmbiente = vDataReaderResults["ManPracticasAmbiente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManPracticasAmbiente"].ToString()) : vInfoAdminEntidad.ManPracticasAmbiente;
                            vInfoAdminEntidad.ManComportOrg = vDataReaderResults["ManComportOrg"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManComportOrg"].ToString()) : vInfoAdminEntidad.ManComportOrg;
                            vInfoAdminEntidad.ManFunciones = vDataReaderResults["ManFunciones"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ManFunciones"].ToString()) : vInfoAdminEntidad.ManFunciones;
                            vInfoAdminEntidad.ProcRegInfoContable = vDataReaderResults["ProcRegInfoContable"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcRegInfoContable"].ToString()) : vInfoAdminEntidad.ProcRegInfoContable;
                            vInfoAdminEntidad.PartMesasTerritoriales = vDataReaderResults["PartMesasTerritoriales"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartMesasTerritoriales"].ToString()) : vInfoAdminEntidad.PartMesasTerritoriales;
                            vInfoAdminEntidad.PartAsocAgremia = vDataReaderResults["PartAsocAgremia"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartAsocAgremia"].ToString()) : vInfoAdminEntidad.PartAsocAgremia;
                            vInfoAdminEntidad.PartConsejosComun = vDataReaderResults["PartConsejosComun"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PartConsejosComun"].ToString()) : vInfoAdminEntidad.PartConsejosComun;
                            vInfoAdminEntidad.ConvInterInst = vDataReaderResults["ConvInterInst"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ConvInterInst"].ToString()) : vInfoAdminEntidad.ConvInterInst;
                            vInfoAdminEntidad.ProcSeleccGral = vDataReaderResults["ProcSeleccGral"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccGral"].ToString()) : vInfoAdminEntidad.ProcSeleccGral;
                            vInfoAdminEntidad.ProcSeleccEtnico = vDataReaderResults["ProcSeleccEtnico"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ProcSeleccEtnico"].ToString()) : vInfoAdminEntidad.ProcSeleccEtnico;
                            vInfoAdminEntidad.PlanInduccCapac = vDataReaderResults["PlanInduccCapac"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanInduccCapac"].ToString()) : vInfoAdminEntidad.PlanInduccCapac;
                            vInfoAdminEntidad.EvalDesemp = vDataReaderResults["EvalDesemp"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["EvalDesemp"].ToString()) : vInfoAdminEntidad.EvalDesemp;
                            vInfoAdminEntidad.PlanCualificacion = vDataReaderResults["PlanCualificacion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["PlanCualificacion"].ToString()) : vInfoAdminEntidad.PlanCualificacion;
                            vInfoAdminEntidad.NumSedes = vDataReaderResults["NumSedes"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["NumSedes"].ToString()) : vInfoAdminEntidad.NumSedes;
                            vInfoAdminEntidad.SedesPropias = vDataReaderResults["SedesPropias"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["SedesPropias"].ToString()) : vInfoAdminEntidad.SedesPropias;
                            vInfoAdminEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoAdminEntidad.UsuarioCrea;
                            vInfoAdminEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoAdminEntidad.FechaCrea;
                            vInfoAdminEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoAdminEntidad.UsuarioModifica;
                            vInfoAdminEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoAdminEntidad.FechaModifica;
                        }
                        return vInfoAdminEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
