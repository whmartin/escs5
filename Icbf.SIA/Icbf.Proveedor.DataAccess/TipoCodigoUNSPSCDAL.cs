using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad c�digo UNSPSC
    /// </summary>
    public class TipoCodigoUNSPSCDAL : GeneralDAL
    {
        public TipoCodigoUNSPSCDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad �digo UNSPSC</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pTipoCodigoUNSPSC.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCodigoUNSPSC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCodigoUNSPSC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoCodigoUNSPSC.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoCodigoUNSPSC.IdTipoCodUNSPSC = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoCodUNSPSC").ToString());
                    GenerarLogAuditoria(pTipoCodigoUNSPSC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad �digo UNSPSC</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pTipoCodigoUNSPSC.IdTipoCodUNSPSC);
                    vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pTipoCodigoUNSPSC.Codigo);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCodigoUNSPSC.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCodigoUNSPSC.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoCodigoUNSPSC.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoCodigoUNSPSC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad �digo UNSPSC</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pTipoCodigoUNSPSC.IdTipoCodUNSPSC);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoCodigoUNSPSC, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pIdTipoCodUNSPSC">Identificador de una entidad C�digo UNSPSC</param>
        /// <returns>Entidad C�digo UNSPSC</returns>
        public TipoCodigoUNSPSC ConsultarTipoCodigoUNSPSC(int pIdTipoCodUNSPSC)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pIdTipoCodUNSPSC);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
                        while (vDataReaderResults.Read())
                        {
                            vTipoCodigoUNSPSC.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vTipoCodigoUNSPSC.IdTipoCodUNSPSC;
                            vTipoCodigoUNSPSC.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vTipoCodigoUNSPSC.Codigo;
                            vTipoCodigoUNSPSC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCodigoUNSPSC.Descripcion;
                            vTipoCodigoUNSPSC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCodigoUNSPSC.Estado;
                            vTipoCodigoUNSPSC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCodigoUNSPSC.UsuarioCrea;
                            vTipoCodigoUNSPSC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCodigoUNSPSC.FechaCrea;
                            vTipoCodigoUNSPSC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCodigoUNSPSC.UsuarioModifica;
                            vTipoCodigoUNSPSC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCodigoUNSPSC.FechaModifica;
                        }
                        return vTipoCodigoUNSPSC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades C�digo UNSPSC
        /// </summary>
        /// <param name="pCodigo">C�digo en una entidad C�digo UNSPSC</param>
        /// <param name="pDescripcion">Descripci�n en una entidad C�digo UNSPSC</param>
        /// <param name="pEstado">Estado en una entidad C�digo UNSPSC</param>
        /// <param name="pLongitudUNSPSC">Longitud en una entidad C�digo UNSPSC</param>
        /// <returns>Lista de entidades C�digo UNSPSC</returns>
        public List<TipoCodigoUNSPSC> ConsultarTipoCodigoUNSPSCs(String pCodigo, String pDescripcion, Boolean? pEstado, int? pLongitudUNSPSC, String pCodigoSegmento, String pCodigoFamilia, String pCodigoClase)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCodigoUNSPSCs_Consultar"))
                {
                    if(pCodigo != null)
                         vDataBase.AddInParameter(vDbCommand, "@Codigo", DbType.String, pCodigo);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    if (pLongitudUNSPSC != null)
                        vDataBase.AddInParameter(vDbCommand, "@LongitudUNSPSC", DbType.Int32, pLongitudUNSPSC);
                    if (pCodigoSegmento != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoSegmento", DbType.String, pCodigoSegmento);
                    if (pCodigoFamilia != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoFamilia", DbType.String, pCodigoFamilia);
                    if (pCodigoClase != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoClase", DbType.String, pCodigoClase);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCodigoUNSPSC> vListaTipoCodigoUNSPSC = new List<TipoCodigoUNSPSC>();
                        while (vDataReaderResults.Read())
                        {
                                TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
                            vTipoCodigoUNSPSC.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vTipoCodigoUNSPSC.IdTipoCodUNSPSC;
                            vTipoCodigoUNSPSC.Codigo = vDataReaderResults["Codigo"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Codigo"].ToString()) : vTipoCodigoUNSPSC.Codigo;
                            vTipoCodigoUNSPSC.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCodigoUNSPSC.Descripcion;
                            vTipoCodigoUNSPSC.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCodigoUNSPSC.Estado;
                            vTipoCodigoUNSPSC.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCodigoUNSPSC.UsuarioCrea;
                            vTipoCodigoUNSPSC.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCodigoUNSPSC.FechaCrea;
                            vTipoCodigoUNSPSC.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCodigoUNSPSC.UsuarioModifica;
                            vTipoCodigoUNSPSC.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCodigoUNSPSC.FechaModifica;
                            
                                vListaTipoCodigoUNSPSC.Add(vTipoCodigoUNSPSC);
                        }
                        return vListaTipoCodigoUNSPSC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta los Segmentos para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarSegmento()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Segmento"))
                {
                   using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCodigoUNSPSC> vListaTipoCodigoUNSPSC = new List<TipoCodigoUNSPSC>();
                        while (vDataReaderResults.Read())
                        {
                            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
                            vTipoCodigoUNSPSC.CodigoSegmento = vDataReaderResults["CodigoSegmento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSegmento"].ToString()) : vTipoCodigoUNSPSC.CodigoSegmento;
                            vTipoCodigoUNSPSC.NombreSegmento = vDataReaderResults["NombreSegmento"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreSegmento"].ToString()) : vTipoCodigoUNSPSC.NombreSegmento;
                            vListaTipoCodigoUNSPSC.Add(vTipoCodigoUNSPSC);
                        }
                        return vListaTipoCodigoUNSPSC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta las Familias por Segmento para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <param name="pCodigoSegmento">C�digo del Segmento</param>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarFamilia(String pCodigoSegmento)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Familia"))
                {
                    if (pCodigoSegmento != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoSegmento", DbType.String, pCodigoSegmento);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCodigoUNSPSC> vListaTipoCodigoUNSPSC = new List<TipoCodigoUNSPSC>();
                        while (vDataReaderResults.Read())
                        {
                            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
                            vTipoCodigoUNSPSC.CodigoFamilia = vDataReaderResults["CodigoFamilia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoFamilia"].ToString()) : vTipoCodigoUNSPSC.CodigoFamilia;
                            vTipoCodigoUNSPSC.NombreFamilia = vDataReaderResults["NombreFamilia"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreFamilia"].ToString()) : vTipoCodigoUNSPSC.NombreFamilia;
                            vListaTipoCodigoUNSPSC.Add(vTipoCodigoUNSPSC);
                        }
                        return vListaTipoCodigoUNSPSC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta las Clases por Familias para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <param name="pCodigoFamilia">C�digo de la Familia</param>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarClase(String pCodigoFamilia)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Clase"))
                {
                    if (pCodigoFamilia != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoFamilia", DbType.String, pCodigoFamilia);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCodigoUNSPSC> vListaTipoCodigoUNSPSC = new List<TipoCodigoUNSPSC>();
                        while (vDataReaderResults.Read())
                        {
                            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
                            vTipoCodigoUNSPSC.CodigoClase = vDataReaderResults["CodigoClase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoClase"].ToString()) : vTipoCodigoUNSPSC.CodigoClase;
                            vTipoCodigoUNSPSC.NombreClase = vDataReaderResults["NombreClase"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NombreClase"].ToString()) : vTipoCodigoUNSPSC.NombreClase;
                            vListaTipoCodigoUNSPSC.Add(vTipoCodigoUNSPSC);
                        }
                        return vListaTipoCodigoUNSPSC;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
