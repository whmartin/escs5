using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Informaci�n M�dulo Experiencia
    /// </summary>
    public class InfoExperienciaEntidadDAL : GeneralDAL
    {
        public InfoExperienciaEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoExperienciaEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoExperienciaEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, pInfoExperienciaEntidad.IdTipoSector);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstadoExp", DbType.Int32, pInfoExperienciaEntidad.IdTipoEstadoExp);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModalidadExp", DbType.Int32, pInfoExperienciaEntidad.IdTipoModalidadExp);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModalidad", DbType.Int32, pInfoExperienciaEntidad.IdTipoModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPoblacionAtendida", DbType.Int32, pInfoExperienciaEntidad.IdTipoPoblacionAtendida);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRangoExpAcum", DbType.Int32, pInfoExperienciaEntidad.IdTipoRangoExpAcum);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pInfoExperienciaEntidad.IdTipoCodUNSPSC);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidadContratante", DbType.Int32, pInfoExperienciaEntidad.IdTipoEntidadContratante);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadContratante", DbType.String, pInfoExperienciaEntidad.EntidadContratante);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pInfoExperienciaEntidad.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pInfoExperienciaEntidad.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pInfoExperienciaEntidad.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pInfoExperienciaEntidad.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Vigente", DbType.Boolean, pInfoExperienciaEntidad.Vigente);
                    vDataBase.AddInParameter(vDbCommand, "@Cuantia", DbType.Decimal, pInfoExperienciaEntidad.Cuantia);
                    vDataBase.AddInParameter(vDbCommand, "@ExperienciaMeses", DbType.Decimal, pInfoExperienciaEntidad.ExperienciaMeses);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoDocumental", DbType.Int32, pInfoExperienciaEntidad.EstadoDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@UnionTempConsorcio", DbType.Boolean, pInfoExperienciaEntidad.UnionTempConsorcio);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentParticipacion", DbType.Decimal, pInfoExperienciaEntidad.PorcentParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@AtencionDeptos", DbType.Boolean, pInfoExperienciaEntidad.AtencionDeptos);
                    vDataBase.AddInParameter(vDbCommand, "@JardinOPreJardin", DbType.Boolean, pInfoExperienciaEntidad.JardinOPreJardin);
                    vDataBase.AddInParameter(vDbCommand, "@IdTemporal", DbType.String, pInfoExperienciaEntidad.IdTemporal);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pInfoExperienciaEntidad.UsuarioCrea);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pInfoExperienciaEntidad.Finalizado);
                    vDataBase.AddInParameter(vDbCommand, "@ContratoEjecucion", DbType.Boolean, pInfoExperienciaEntidad.ContratoEjecucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pInfoExperienciaEntidad.IdExpEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdExpEntidad").ToString());
                    GenerarLogAuditoria(pInfoExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pInfoExperienciaEntidad.IdExpEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoExperienciaEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSector", DbType.Int32, pInfoExperienciaEntidad.IdTipoSector);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEstadoExp", DbType.Int32, pInfoExperienciaEntidad.IdTipoEstadoExp);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModalidadExp", DbType.Int32, pInfoExperienciaEntidad.IdTipoModalidadExp);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoModalidad", DbType.Int32, pInfoExperienciaEntidad.IdTipoModalidad);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoPoblacionAtendida", DbType.Int32, pInfoExperienciaEntidad.IdTipoPoblacionAtendida);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoRangoExpAcum", DbType.Int32, pInfoExperienciaEntidad.IdTipoRangoExpAcum);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCodUNSPSC", DbType.Int32, pInfoExperienciaEntidad.IdTipoCodUNSPSC);
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoEntidadContratante", DbType.Int32, pInfoExperienciaEntidad.IdTipoEntidadContratante);
                    vDataBase.AddInParameter(vDbCommand, "@EntidadContratante", DbType.String, pInfoExperienciaEntidad.EntidadContratante);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicio", DbType.DateTime, pInfoExperienciaEntidad.FechaInicio);
                    vDataBase.AddInParameter(vDbCommand, "@FechaFin", DbType.DateTime, pInfoExperienciaEntidad.FechaFin);
                    vDataBase.AddInParameter(vDbCommand, "@NumeroContrato", DbType.String, pInfoExperienciaEntidad.NumeroContrato);
                    vDataBase.AddInParameter(vDbCommand, "@ObjetoContrato", DbType.String, pInfoExperienciaEntidad.ObjetoContrato);
                    vDataBase.AddInParameter(vDbCommand, "@Vigente", DbType.Boolean, pInfoExperienciaEntidad.Vigente);
                    vDataBase.AddInParameter(vDbCommand, "@Cuantia", DbType.Decimal, pInfoExperienciaEntidad.Cuantia);
                    vDataBase.AddInParameter(vDbCommand, "@ExperienciaMeses", DbType.Decimal, pInfoExperienciaEntidad.ExperienciaMeses);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoDocumental", DbType.Int32, pInfoExperienciaEntidad.EstadoDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@UnionTempConsorcio", DbType.Boolean, pInfoExperienciaEntidad.UnionTempConsorcio);
                    vDataBase.AddInParameter(vDbCommand, "@PorcentParticipacion", DbType.Decimal, pInfoExperienciaEntidad.PorcentParticipacion);
                    vDataBase.AddInParameter(vDbCommand, "@AtencionDeptos", DbType.Boolean, pInfoExperienciaEntidad.AtencionDeptos);
                    vDataBase.AddInParameter(vDbCommand, "@JardinOPreJardin", DbType.Boolean, pInfoExperienciaEntidad.JardinOPreJardin);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInfoExperienciaEntidad.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pInfoExperienciaEntidad.Finalizado);
                    vDataBase.AddInParameter(vDbCommand, "@ContratoEjecucion", DbType.Boolean, pInfoExperienciaEntidad.ContratoEjecucion);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pInfoExperienciaEntidad.IdExpEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de una entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>Entidad Informaci�n M�dulo Experiencia</returns>
        public InfoExperienciaEntidad ConsultarInfoExperienciaEntidad(int pIdExpEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoExperienciaEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdExpEntidad", DbType.Int32, pIdExpEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vInfoExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vInfoExperienciaEntidad.IdExpEntidad;
                            vInfoExperienciaEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoExperienciaEntidad.IdEntidad;
                            vInfoExperienciaEntidad.IdTipoSector = vDataReaderResults["IdTipoSector"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSector"].ToString()) : vInfoExperienciaEntidad.IdTipoSector;
                            vInfoExperienciaEntidad.IdTipoEstadoExp = vDataReaderResults["IdTipoEstadoExp"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstadoExp"].ToString()) : vInfoExperienciaEntidad.IdTipoEstadoExp;
                            vInfoExperienciaEntidad.IdTipoModalidadExp = vDataReaderResults["IdTipoModalidadExp"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModalidadExp"].ToString()) : vInfoExperienciaEntidad.IdTipoModalidadExp;
                            vInfoExperienciaEntidad.IdTipoModalidad = vDataReaderResults["IdTipoModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModalidad"].ToString()) : vInfoExperienciaEntidad.IdTipoModalidad;
                            vInfoExperienciaEntidad.IdTipoPoblacionAtendida = vDataReaderResults["IdTipoPoblacionAtendida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPoblacionAtendida"].ToString()) : vInfoExperienciaEntidad.IdTipoPoblacionAtendida;
                            vInfoExperienciaEntidad.IdTipoRangoExpAcum = vDataReaderResults["IdTipoRangoExpAcum"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangoExpAcum"].ToString()) : vInfoExperienciaEntidad.IdTipoRangoExpAcum;
                            vInfoExperienciaEntidad.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vInfoExperienciaEntidad.IdTipoCodUNSPSC;
                            vInfoExperienciaEntidad.IdTipoEntidadContratante = vDataReaderResults["IdTipoEntidadContratante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidadContratante"].ToString()) : vInfoExperienciaEntidad.IdTipoEntidadContratante;
                            vInfoExperienciaEntidad.EntidadContratante = vDataReaderResults["EntidadContratante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadContratante"].ToString()) : vInfoExperienciaEntidad.EntidadContratante;
                            vInfoExperienciaEntidad.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vInfoExperienciaEntidad.FechaInicio;
                            vInfoExperienciaEntidad.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vInfoExperienciaEntidad.FechaFin;
                            vInfoExperienciaEntidad.ExperienciaMeses = vDataReaderResults["ExperienciaMeses"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ExperienciaMeses"].ToString()) : vInfoExperienciaEntidad.ExperienciaMeses;
                            vInfoExperienciaEntidad.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vInfoExperienciaEntidad.NumeroContrato;
                            vInfoExperienciaEntidad.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vInfoExperienciaEntidad.ObjetoContrato;
                            vInfoExperienciaEntidad.Vigente = vDataReaderResults["Vigente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Vigente"].ToString()) : vInfoExperienciaEntidad.Vigente;
                            vInfoExperienciaEntidad.Cuantia = vDataReaderResults["Cuantia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Cuantia"].ToString()) : vInfoExperienciaEntidad.Cuantia;
                            vInfoExperienciaEntidad.EstadoDocumental = vDataReaderResults["EstadoDocumental"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoDocumental"].ToString()) : vInfoExperienciaEntidad.EstadoDocumental;
                            vInfoExperienciaEntidad.UnionTempConsorcio = vDataReaderResults["UnionTempConsorcio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["UnionTempConsorcio"].ToString()) : vInfoExperienciaEntidad.UnionTempConsorcio;
                            vInfoExperienciaEntidad.PorcentParticipacion = vDataReaderResults["PorcentParticipacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentParticipacion"].ToString()) : vInfoExperienciaEntidad.PorcentParticipacion;
                            vInfoExperienciaEntidad.AtencionDeptos = vDataReaderResults["AtencionDeptos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AtencionDeptos"].ToString()) : vInfoExperienciaEntidad.AtencionDeptos;
                            vInfoExperienciaEntidad.JardinOPreJardin = vDataReaderResults["JardinOPreJardin"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["JardinOPreJardin"].ToString()) : vInfoExperienciaEntidad.JardinOPreJardin;
                            vInfoExperienciaEntidad.EstadoDocumental = vDataReaderResults["EstadoDocumental"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoDocumental"].ToString()) : vInfoExperienciaEntidad.EstadoDocumental;
                            vInfoExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoExperienciaEntidad.UsuarioCrea;
                            vInfoExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoExperienciaEntidad.FechaCrea;
                            vInfoExperienciaEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoExperienciaEntidad.UsuarioModifica;
                            vInfoExperienciaEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoExperienciaEntidad.FechaModifica;
                            vInfoExperienciaEntidad.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["NroRevision"].ToString()) : vInfoExperienciaEntidad.NroRevision;
                            vInfoExperienciaEntidad.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vInfoExperienciaEntidad.Finalizado;
                            vInfoExperienciaEntidad.ContratoEjecucion = vDataReaderResults["ContratoEjecucion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ContratoEjecucion"].ToString()) : vInfoExperienciaEntidad.ContratoEjecucion;
                            vInfoExperienciaEntidad.EstadoDocDescripcion = vDataReaderResults["EstadoDocDescripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDocDescripcion"].ToString()) : vInfoExperienciaEntidad.EstadoDocDescripcion;
                            
                        }
                        return vInfoExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador general de una entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>Lista de entidades Informaci�n M�dulo Experiencia</returns>
        public List<InfoExperienciaEntidad> ConsultarInfoExperienciaEntidads(int ?pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoExperienciaEntidads_Consultar"))
                {
                    if (pIdEntidad != null) { vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad); };
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<InfoExperienciaEntidad> vListaInfoExperienciaEntidad = new List<InfoExperienciaEntidad>();
                        while (vDataReaderResults.Read())
                        {
                                InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();
                            vInfoExperienciaEntidad.IdExpEntidad = vDataReaderResults["IdExpEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdExpEntidad"].ToString()) : vInfoExperienciaEntidad.IdExpEntidad;
                            vInfoExperienciaEntidad.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vInfoExperienciaEntidad.IdEntidad;
                            vInfoExperienciaEntidad.IdTipoSector = vDataReaderResults["IdTipoSector"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSector"].ToString()) : vInfoExperienciaEntidad.IdTipoSector;
                            vInfoExperienciaEntidad.TipoSector = vDataReaderResults["TipoSector"] != DBNull.Value ? vDataReaderResults["TipoSector"].ToString() : vInfoExperienciaEntidad.TipoSector;
                            vInfoExperienciaEntidad.IdTipoEstadoExp = vDataReaderResults["IdTipoEstadoExp"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEstadoExp"].ToString()) : vInfoExperienciaEntidad.IdTipoEstadoExp;
                            vInfoExperienciaEntidad.IdTipoModalidadExp = vDataReaderResults["IdTipoModalidadExp"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModalidadExp"].ToString()) : vInfoExperienciaEntidad.IdTipoModalidadExp;
                            vInfoExperienciaEntidad.IdTipoModalidad = vDataReaderResults["IdTipoModalidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoModalidad"].ToString()) : vInfoExperienciaEntidad.IdTipoModalidad;
                            vInfoExperienciaEntidad.IdTipoPoblacionAtendida = vDataReaderResults["IdTipoPoblacionAtendida"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoPoblacionAtendida"].ToString()) : vInfoExperienciaEntidad.IdTipoPoblacionAtendida;
                            vInfoExperienciaEntidad.IdTipoRangoExpAcum = vDataReaderResults["IdTipoRangoExpAcum"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoRangoExpAcum"].ToString()) : vInfoExperienciaEntidad.IdTipoRangoExpAcum;
                            vInfoExperienciaEntidad.IdTipoCodUNSPSC = vDataReaderResults["IdTipoCodUNSPSC"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCodUNSPSC"].ToString()) : vInfoExperienciaEntidad.IdTipoCodUNSPSC;
                            vInfoExperienciaEntidad.IdTipoEntidadContratante = vDataReaderResults["IdTipoEntidadContratante"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoEntidadContratante"].ToString()) : vInfoExperienciaEntidad.IdTipoEntidadContratante;
                            vInfoExperienciaEntidad.EntidadContratante = vDataReaderResults["EntidadContratante"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EntidadContratante"].ToString()) : vInfoExperienciaEntidad.EntidadContratante;
                            vInfoExperienciaEntidad.FechaInicio = vDataReaderResults["FechaInicio"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaInicio"].ToString()) : vInfoExperienciaEntidad.FechaInicio;
                            vInfoExperienciaEntidad.FechaFin = vDataReaderResults["FechaFin"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaFin"].ToString()) : vInfoExperienciaEntidad.FechaFin;
                            vInfoExperienciaEntidad.ExperienciaMeses = vDataReaderResults["ExperienciaMeses"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["ExperienciaMeses"].ToString()) : vInfoExperienciaEntidad.ExperienciaMeses;
                            vInfoExperienciaEntidad.NumeroContrato = vDataReaderResults["NumeroContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["NumeroContrato"].ToString()) : vInfoExperienciaEntidad.NumeroContrato;
                            vInfoExperienciaEntidad.ObjetoContrato = vDataReaderResults["ObjetoContrato"] != DBNull.Value ? Convert.ToString(vDataReaderResults["ObjetoContrato"].ToString()) : vInfoExperienciaEntidad.ObjetoContrato;
                            vInfoExperienciaEntidad.Vigente = vDataReaderResults["Vigente"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Vigente"].ToString()) : vInfoExperienciaEntidad.Vigente;
                            vInfoExperienciaEntidad.Cuantia = vDataReaderResults["Cuantia"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["Cuantia"].ToString()) : vInfoExperienciaEntidad.Cuantia;
                            vInfoExperienciaEntidad.EstadoDocumental = vDataReaderResults["EstadoDocumental"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["EstadoDocumental"].ToString()) : vInfoExperienciaEntidad.EstadoDocumental;
                            vInfoExperienciaEntidad.Aprobado = vDataReaderResults["Aprobado"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Aprobado"].ToString()) : vInfoExperienciaEntidad.Aprobado;
                            vInfoExperienciaEntidad.EstadoDocDescripcion = vDataReaderResults["EstadoDocDescripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["EstadoDocDescripcion"].ToString()) : vInfoExperienciaEntidad.EstadoDocDescripcion;
                            vInfoExperienciaEntidad.UnionTempConsorcio = vDataReaderResults["UnionTempConsorcio"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["UnionTempConsorcio"].ToString()) : vInfoExperienciaEntidad.UnionTempConsorcio;
                            vInfoExperienciaEntidad.PorcentParticipacion = vDataReaderResults["PorcentParticipacion"] != DBNull.Value ? Convert.ToDecimal(vDataReaderResults["PorcentParticipacion"].ToString()) : vInfoExperienciaEntidad.PorcentParticipacion;
                            vInfoExperienciaEntidad.AtencionDeptos = vDataReaderResults["AtencionDeptos"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["AtencionDeptos"].ToString()) : vInfoExperienciaEntidad.AtencionDeptos;
                            vInfoExperienciaEntidad.JardinOPreJardin = vDataReaderResults["JardinOPreJardin"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["JardinOPreJardin"].ToString()) : vInfoExperienciaEntidad.JardinOPreJardin;
                            vInfoExperienciaEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vInfoExperienciaEntidad.UsuarioCrea;
                            vInfoExperienciaEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vInfoExperienciaEntidad.FechaCrea;
                            vInfoExperienciaEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vInfoExperienciaEntidad.UsuarioModifica;
                            vInfoExperienciaEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vInfoExperienciaEntidad.FechaModifica;
                            vInfoExperienciaEntidad.ContratoEjecucion = vDataReaderResults["ContratoEjecucion"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["ContratoEjecucion"].ToString()) : vInfoExperienciaEntidad.ContratoEjecucion;
                                vListaInfoExperienciaEntidad.Add(vInfoExperienciaEntidad);
                        }
                        return vListaInfoExperienciaEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n M�dulo Experiencia para liberar
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarInfoExperienciaEntidad_Liberar(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pInfoExperienciaEntidad.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@EstadoDocumental", DbType.Int32, pInfoExperienciaEntidad.EstadoDocumental);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pInfoExperienciaEntidad.UsuarioModifica);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pInfoExperienciaEntidad.Finalizado);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pInfoExperienciaEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza InfoExperienciaEntidad
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public int FinalizaInfoExperienciaEntidad(int pIdEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedor_InfoExperienciaEntidad_Finalizar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);

                    int vResultado;
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
