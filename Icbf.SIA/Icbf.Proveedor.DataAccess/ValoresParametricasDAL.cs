using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad de valores asociados a las tablas param�tricas
    /// </summary>
    public class ValoresParametricasDAL : GeneralDAL
    {
        public ValoresParametricasDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pValoresParametricas">Entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValoresParametricas(ValoresParametricas pValoresParametricas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValoresParametricas_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdValorParametrica", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdListaParametrica", DbType.Int32, pValoresParametricas.IdListaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoValorParametrica", DbType.String, pValoresParametricas.CodigoValorParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionValorParametrica", DbType.String, pValoresParametricas.DescripcionValorParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pValoresParametricas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pValoresParametricas.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pValoresParametricas.IdValorParametrica = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdValorParametrica").ToString());
                    GenerarLogAuditoria(pValoresParametricas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pValoresParametricas">Entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarValoresParametricas(ValoresParametricas pValoresParametricas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValoresParametricas_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValorParametrica", DbType.Int32, pValoresParametricas.IdValorParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@IdListaParametrica", DbType.Int32, pValoresParametricas.IdListaParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoValorParametrica", DbType.String, pValoresParametricas.CodigoValorParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@DescripcionValorParametrica", DbType.String, pValoresParametricas.DescripcionValorParametrica);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pValoresParametricas.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pValoresParametricas.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValoresParametricas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pValoresParametricas">Entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarValoresParametricas(ValoresParametricas pValoresParametricas)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValoresParametricas_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdValorParametrica", DbType.Int32, pValoresParametricas.IdValorParametrica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pValoresParametricas, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pIdValorParametrica">Identificador en una entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>Entidad Valores asociados a las tablas param�tricas</returns>
        public ValoresParametricas ConsultarValoresParametricas(int pIdValorParametrica)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValoresParametricas_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdValorParametrica", DbType.Int32, pIdValorParametrica);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        ValoresParametricas vValoresParametricas = new ValoresParametricas();
                        while (vDataReaderResults.Read())
                        {
                            vValoresParametricas.IdValorParametrica = vDataReaderResults["IdValorParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValorParametrica"].ToString()) : vValoresParametricas.IdValorParametrica;
                            vValoresParametricas.IdListaParametrica = vDataReaderResults["IdListaParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdListaParametrica"].ToString()) : vValoresParametricas.IdListaParametrica;
                            vValoresParametricas.CodigoValorParametrica = vDataReaderResults["CodigoValorParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoValorParametrica"].ToString()) : vValoresParametricas.CodigoValorParametrica;
                            vValoresParametricas.DescripcionValorParametrica = vDataReaderResults["DescripcionValorParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionValorParametrica"].ToString()) : vValoresParametricas.DescripcionValorParametrica;
                            vValoresParametricas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vValoresParametricas.Estado;
                            vValoresParametricas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValoresParametricas.UsuarioCrea;
                            vValoresParametricas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValoresParametricas.FechaCrea;
                            vValoresParametricas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValoresParametricas.UsuarioModifica;
                            vValoresParametricas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValoresParametricas.FechaModifica;
                        }
                        return vValoresParametricas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pIdListaParametrica">Identificador de la lista param�trica</param>
        /// <param name="pCodigoValorParametrica">C�digo en una lista param�trica</param>
        /// <param name="pEstado">Estado de la entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>Lista de entidades Valores asociados a las tablas param�tricas</returns>
        public List<ValoresParametricas> ConsultarValoresParametricass(int? pIdListaParametrica, String pCodigoValorParametrica, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_ValoresParametricass_Consultar"))
                {
                    if(pIdListaParametrica != null)
                         vDataBase.AddInParameter(vDbCommand, "@IdListaParametrica", DbType.Int32, pIdListaParametrica);
                    if(pCodigoValorParametrica != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoValorParametrica", DbType.String, pCodigoValorParametrica);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<ValoresParametricas> vListaValoresParametricas = new List<ValoresParametricas>();
                        while (vDataReaderResults.Read())
                        {
                                ValoresParametricas vValoresParametricas = new ValoresParametricas();
                            vValoresParametricas.IdValorParametrica = vDataReaderResults["IdValorParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdValorParametrica"].ToString()) : vValoresParametricas.IdValorParametrica;
                            vValoresParametricas.IdListaParametrica = vDataReaderResults["IdListaParametrica"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdListaParametrica"].ToString()) : vValoresParametricas.IdListaParametrica;
                            vValoresParametricas.CodigoValorParametrica = vDataReaderResults["CodigoValorParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoValorParametrica"].ToString()) : vValoresParametricas.CodigoValorParametrica;
                            vValoresParametricas.DescripcionValorParametrica = vDataReaderResults["DescripcionValorParametrica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["DescripcionValorParametrica"].ToString()) : vValoresParametricas.DescripcionValorParametrica;
                            vValoresParametricas.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vValoresParametricas.Estado;
                            vValoresParametricas.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vValoresParametricas.UsuarioCrea;
                            vValoresParametricas.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vValoresParametricas.FechaCrea;
                            vValoresParametricas.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vValoresParametricas.UsuarioModifica;
                            vValoresParametricas.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vValoresParametricas.FechaModifica;
                                vListaValoresParametricas.Add(vValoresParametricas);
                        }
                        return vListaValoresParametricas;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
