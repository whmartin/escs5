using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Sector Entidad
    /// </summary>
    public class TipoSectorEntidadDAL : GeneralDAL
    {
        public TipoSectorEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoSectorEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSectorEntidad", DbType.String, pTipoSectorEntidad.CodigoSectorEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoSectorEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoSectorEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoSectorEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoSectorEntidad.IdTipoSectorEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoSectorEntidad").ToString());
                    GenerarLogAuditoria(pTipoSectorEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSectorEntidad", DbType.Int32, pTipoSectorEntidad.IdTipoSectorEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoSectorEntidad", DbType.String, pTipoSectorEntidad.CodigoSectorEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoSectorEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoSectorEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoSectorEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoSectorEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSectorEntidad", DbType.Int32, pTipoSectorEntidad.IdTipoSectorEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoSectorEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pIdTipoSectorEntidad">Identificador en una entidad Tipo Sector Entidad</param>
        /// <returns>Entidad Tipo Sector Entidad</returns>
        public TipoSectorEntidad ConsultarTipoSectorEntidad(int pIdTipoSectorEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoSectorEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoSectorEntidad", DbType.Int32, pIdTipoSectorEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoSectorEntidad vTipoSectorEntidad = new TipoSectorEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vTipoSectorEntidad.IdTipoSectorEntidad = vDataReaderResults["IdTipoSectorEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSectorEntidad"].ToString()) : vTipoSectorEntidad.IdTipoSectorEntidad;
                            vTipoSectorEntidad.CodigoSectorEntidad = vDataReaderResults["CodigoSectorEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSectorEntidad"].ToString()) : vTipoSectorEntidad.CodigoSectorEntidad;
                            vTipoSectorEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoSectorEntidad.Descripcion;
                            vTipoSectorEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoSectorEntidad.Estado;
                            vTipoSectorEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoSectorEntidad.UsuarioCrea;
                            vTipoSectorEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoSectorEntidad.FechaCrea;
                            vTipoSectorEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoSectorEntidad.UsuarioModifica;
                            vTipoSectorEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoSectorEntidad.FechaModifica;
                        }
                        return vTipoSectorEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Sector Entidad
        /// </summary>
        /// <returns>Lista de entidades Tipo Sector Entidad</returns>
        public  List<TipoSectorEntidad> ConsultarTipoSectorEntidadAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoSectorEntidad> vListaTipoSectorEntidad = new List<TipoSectorEntidad>();
                      while (vDataReaderResults.Read())
                        {
                            TipoSectorEntidad vTipoSectorEntidad = new TipoSectorEntidad();
                            vTipoSectorEntidad.IdTipoSectorEntidad = vDataReaderResults["IdTipoSectorEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSectorEntidad"].ToString()) : vTipoSectorEntidad.IdTipoSectorEntidad;
                            vTipoSectorEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoSectorEntidad.Descripcion;
                            vListaTipoSectorEntidad.Add(vTipoSectorEntidad);
                        }
                      return vListaTipoSectorEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Sector Entidad
        /// </summary>
        /// <param name="pCodigoSectorEntidad">C�digo en una entidad Tipo Sector Entidad</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Sector Entidad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Sector Entidad</param>
        /// <returns>Lista de entidades Tipo Sector Entidad</returns>
       public List<TipoSectorEntidad> ConsultarTipoSectorEntidads(String pCodigoSectorEntidad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar"))
                {
                    if(pCodigoSectorEntidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoSectorEntidad", DbType.String, pCodigoSectorEntidad);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoSectorEntidad> vListaTipoSectorEntidad = new List<TipoSectorEntidad>();
                        while (vDataReaderResults.Read())
                        {
                                TipoSectorEntidad vTipoSectorEntidad = new TipoSectorEntidad();
                            vTipoSectorEntidad.IdTipoSectorEntidad = vDataReaderResults["IdTipoSectorEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoSectorEntidad"].ToString()) : vTipoSectorEntidad.IdTipoSectorEntidad;
                            vTipoSectorEntidad.CodigoSectorEntidad = vDataReaderResults["CodigoSectorEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoSectorEntidad"].ToString()) : vTipoSectorEntidad.CodigoSectorEntidad;
                            vTipoSectorEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoSectorEntidad.Descripcion;
                            vTipoSectorEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoSectorEntidad.Estado;
                            vTipoSectorEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoSectorEntidad.UsuarioCrea;
                            vTipoSectorEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoSectorEntidad.FechaCrea;
                            vTipoSectorEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoSectorEntidad.UsuarioModifica;
                            vTipoSectorEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoSectorEntidad.FechaModifica;
                                vListaTipoSectorEntidad.Add(vTipoSectorEntidad);
                        }
                        return vListaTipoSectorEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
