using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Data.SqlClient;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad tipo cargo
    /// </summary>
    public class TipoCargoEntidadDAL : GeneralDAL
    {
        public TipoCargoEntidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoCargoEntidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoCargoEntidad", DbType.String, pTipoCargoEntidad.CodigoTipoCargoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCargoEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCargoEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoCargoEntidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoCargoEntidad.IdTipoCargoEntidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoCargoEntidad").ToString());
                    GenerarLogAuditoria(pTipoCargoEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("NombreUnico"))
                {
                    throw new Exception("La descripci�n ingresada ya existe");
                }
                else if (ex.Message.Contains("CodigoUnico"))
                {
                    throw new Exception("El c�digo ingresado ya existe");
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCargoEntidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCargoEntidad", DbType.Int32, pTipoCargoEntidad.IdTipoCargoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoCargoEntidad", DbType.String, pTipoCargoEntidad.CodigoTipoCargoEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoCargoEntidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoCargoEntidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoCargoEntidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoCargoEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("NombreUnico"))
                {
                    throw new Exception("La descripci�n ingresada ya existe");
                }
                else if (ex.Message.Contains("CodigoUnico"))
                {
                    throw new Exception("El c�digo ingresado ya existe");
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
        }

        /// <summary>
        /// Eliminar una entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCargoEntidad", DbType.Int32, pTipoCargoEntidad.IdTipoCargoEntidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoCargoEntidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo cargo
        /// </summary>
        /// <param name="pIdTipoCargoEntidad">Identificador de una entidad Tipo cargo</param>
        /// <returns>Entidad Tipo cargo</returns>
        public TipoCargoEntidad ConsultarTipoCargoEntidad(int pIdTipoCargoEntidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCargoEntidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoCargoEntidad", DbType.Int32, pIdTipoCargoEntidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        TipoCargoEntidad vTipoCargoEntidad = new TipoCargoEntidad();
                        while (vDataReaderResults.Read())
                        {
                            vTipoCargoEntidad.IdTipoCargoEntidad = vDataReaderResults["IdTipoCargoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCargoEntidad"].ToString()) : vTipoCargoEntidad.IdTipoCargoEntidad;
                            vTipoCargoEntidad.CodigoTipoCargoEntidad = vDataReaderResults["CodigoTipoCargoEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoCargoEntidad"].ToString()) : vTipoCargoEntidad.CodigoTipoCargoEntidad;
                            vTipoCargoEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCargoEntidad.Descripcion;
                            vTipoCargoEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCargoEntidad.Estado;
                            vTipoCargoEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCargoEntidad.UsuarioCrea;
                            vTipoCargoEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCargoEntidad.FechaCrea;
                            vTipoCargoEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCargoEntidad.UsuarioModifica;
                            vTipoCargoEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCargoEntidad.FechaModifica;
                        }
                        return vTipoCargoEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo cargo
        /// </summary>
        /// <param name="pCodigo">C�digo de una Tipo cargo</param>
        /// <param name="pDescripcion">Descripci�n de una entidad Tipo cargo</param>
        /// <param name="pEstado">Estado de una entidad Tipo cargo</param>
        /// <returns>Lista de entidades Tipo cargo</returns>
        public List<TipoCargoEntidad> ConsultarTipoCargoEntidads(String pCodigo, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar"))
                {
                    if (pCodigo  != null)
                        vDataBase.AddInParameter(vDbCommand, "@CodigoTipoCargoEntidad", DbType.String, pCodigo);
                    
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCargoEntidad> vListaTipoCargoEntidad = new List<TipoCargoEntidad>();
                        while (vDataReaderResults.Read())
                        {
                                TipoCargoEntidad vTipoCargoEntidad = new TipoCargoEntidad();
                            vTipoCargoEntidad.IdTipoCargoEntidad = vDataReaderResults["IdTipoCargoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCargoEntidad"].ToString()) : vTipoCargoEntidad.IdTipoCargoEntidad;
                            vTipoCargoEntidad.CodigoTipoCargoEntidad = vDataReaderResults["CodigoTipoCargoEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoCargoEntidad"].ToString()) : vTipoCargoEntidad.CodigoTipoCargoEntidad;
                            vTipoCargoEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCargoEntidad.Descripcion;
                            vTipoCargoEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCargoEntidad.Estado;
                            vTipoCargoEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCargoEntidad.UsuarioCrea;
                            vTipoCargoEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCargoEntidad.FechaCrea;
                            vTipoCargoEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCargoEntidad.UsuarioModifica;
                            vTipoCargoEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCargoEntidad.FechaModifica;
                                vListaTipoCargoEntidad.Add(vTipoCargoEntidad);
                        }
                        return vListaTipoCargoEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo cargo
        /// </summary>
        /// <returns>Lista de entidades Tipo cargo</returns>
        public List<TipoCargoEntidad> ConsultarTipoCargoEntidadAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar"))
                {
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<TipoCargoEntidad> vListaTipoCargoEntidad = new List<TipoCargoEntidad>();
                        while (vDataReaderResults.Read())
                        {
                            TipoCargoEntidad vTipoCargoEntidad = new TipoCargoEntidad();
                            vTipoCargoEntidad.IdTipoCargoEntidad = vDataReaderResults["IdTipoCargoEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoCargoEntidad"].ToString()) : vTipoCargoEntidad.IdTipoCargoEntidad;
                            vTipoCargoEntidad.CodigoTipoCargoEntidad = vDataReaderResults["CodigoTipoCargoEntidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoCargoEntidad"].ToString()) : vTipoCargoEntidad.CodigoTipoCargoEntidad;
                            vTipoCargoEntidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoCargoEntidad.Descripcion;
                            vTipoCargoEntidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoCargoEntidad.Estado;
                            vTipoCargoEntidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoCargoEntidad.UsuarioCrea;
                            vTipoCargoEntidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoCargoEntidad.FechaCrea;
                            vTipoCargoEntidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoCargoEntidad.UsuarioModifica;
                            vTipoCargoEntidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoCargoEntidad.FechaModifica;
                            vListaTipoCargoEntidad.Add(vTipoCargoEntidad);
                        }
                        return vListaTipoCargoEntidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
