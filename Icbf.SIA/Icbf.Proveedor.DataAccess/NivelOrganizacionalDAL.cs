using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad nivel organizacional
    /// </summary>
    public class NivelOrganizacionalDAL : GeneralDAL
    {
        public NivelOrganizacionalDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NivelOrganizacional_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdNivelOrganizacional", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNivelOrganizacional", DbType.String, pNivelOrganizacional.CodigoNivelOrganizacional);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pNivelOrganizacional.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pNivelOrganizacional.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pNivelOrganizacional.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pNivelOrganizacional.IdNivelOrganizacional = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdNivelOrganizacional").ToString());
                    GenerarLogAuditoria(pNivelOrganizacional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NivelOrganizacional_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNivelOrganizacional", DbType.Int32, pNivelOrganizacional.IdNivelOrganizacional);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoNivelOrganizacional", DbType.String, pNivelOrganizacional.CodigoNivelOrganizacional);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pNivelOrganizacional.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pNivelOrganizacional.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pNivelOrganizacional.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNivelOrganizacional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdNivelOrganizacional", DbType.Int32, pNivelOrganizacional.IdNivelOrganizacional);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pNivelOrganizacional, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Identificador de una entidad Nivel organizacional</param>
        /// <returns>Entidad Nivel organizacional</returns>
        public NivelOrganizacional ConsultarNivelOrganizacional(int pIdNivelOrganizacional)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NivelOrganizacional_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdNivelOrganizacional", DbType.Int32, pIdNivelOrganizacional);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        NivelOrganizacional vNivelOrganizacional = new NivelOrganizacional();
                        while (vDataReaderResults.Read())
                        {
                            vNivelOrganizacional.IdNivelOrganizacional = vDataReaderResults["IdNivelOrganizacional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNivelOrganizacional"].ToString()) : vNivelOrganizacional.IdNivelOrganizacional;
                            vNivelOrganizacional.CodigoNivelOrganizacional = vDataReaderResults["CodigoNivelOrganizacional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNivelOrganizacional"].ToString()) : vNivelOrganizacional.CodigoNivelOrganizacional;
                            vNivelOrganizacional.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNivelOrganizacional.Descripcion;
                            vNivelOrganizacional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNivelOrganizacional.Estado;
                            vNivelOrganizacional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNivelOrganizacional.UsuarioCrea;
                            vNivelOrganizacional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNivelOrganizacional.FechaCrea;
                            vNivelOrganizacional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNivelOrganizacional.UsuarioModifica;
                            vNivelOrganizacional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNivelOrganizacional.FechaModifica;
                        }
                        return vNivelOrganizacional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional
        /// </summary>
        /// <param name="pCodigoNivelOrganizacional">C�digo en una entidad Nivel organizacional</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Nivel organizacional</param>
        /// <param name="pEstado">Estado en una entidad Nivel organizacional</param>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionals(String pCodigoNivelOrganizacional, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar"))
                {
                    if(pCodigoNivelOrganizacional != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoNivelOrganizacional", DbType.String, pCodigoNivelOrganizacional);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NivelOrganizacional> vListaNivelOrganizacional = new List<NivelOrganizacional>();
                        while (vDataReaderResults.Read())
                        {
                                NivelOrganizacional vNivelOrganizacional = new NivelOrganizacional();
                            vNivelOrganizacional.IdNivelOrganizacional = vDataReaderResults["IdNivelOrganizacional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNivelOrganizacional"].ToString()) : vNivelOrganizacional.IdNivelOrganizacional;
                            vNivelOrganizacional.CodigoNivelOrganizacional = vDataReaderResults["CodigoNivelOrganizacional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNivelOrganizacional"].ToString()) : vNivelOrganizacional.CodigoNivelOrganizacional;
                            vNivelOrganizacional.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNivelOrganizacional.Descripcion;
                            vNivelOrganizacional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNivelOrganizacional.Estado;
                            vNivelOrganizacional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNivelOrganizacional.UsuarioCrea;
                            vNivelOrganizacional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNivelOrganizacional.FechaCrea;
                            vNivelOrganizacional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNivelOrganizacional.UsuarioModifica;
                            vNivelOrganizacional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNivelOrganizacional.FechaModifica;
                                vListaNivelOrganizacional.Add(vNivelOrganizacional);
                        }
                        return vListaNivelOrganizacional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional
        /// </summary>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionalAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_NivelOrganizacionals_Consultar_All"))
                {
                       using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NivelOrganizacional> vListaNivelOrganizacional = new List<NivelOrganizacional>();
                        while (vDataReaderResults.Read())
                        {
                            NivelOrganizacional vNivelOrganizacional = new NivelOrganizacional();
                            vNivelOrganizacional.IdNivelOrganizacional = vDataReaderResults["IdNivelOrganizacional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNivelOrganizacional"].ToString()) : vNivelOrganizacional.IdNivelOrganizacional;
                            vNivelOrganizacional.CodigoNivelOrganizacional = vDataReaderResults["CodigoNivelOrganizacional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNivelOrganizacional"].ToString()) : vNivelOrganizacional.CodigoNivelOrganizacional;
                            vNivelOrganizacional.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNivelOrganizacional.Descripcion;
                            vNivelOrganizacional.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vNivelOrganizacional.Estado;
                            vNivelOrganizacional.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vNivelOrganizacional.UsuarioCrea;
                            vNivelOrganizacional.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vNivelOrganizacional.FechaCrea;
                            vNivelOrganizacional.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vNivelOrganizacional.UsuarioModifica;
                            vNivelOrganizacional.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vNivelOrganizacional.FechaModifica;
                            vListaNivelOrganizacional.Add(vNivelOrganizacional);
                        }
                        return vListaNivelOrganizacional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional de acuerdo a unaRama o Estructura y Nivel Gobierno
        /// </summary>
        /// <param name="IdNivelGobierno"></param>
        /// <param name="IdRama"></param>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionals_PorRamaYNivelGobierno(int IdRama,int IdNivelGobierno)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelOrg"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, IdRama);
                    vDataBase.AddInParameter(vDbCommand, "@IdNivelGobierno", DbType.Int32, IdNivelGobierno);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<NivelOrganizacional> vListaNivelOrganizacional = new List<NivelOrganizacional>();
                        while (vDataReaderResults.Read())
                        {
                            NivelOrganizacional vNivelOrganizacional = new NivelOrganizacional();
                            vNivelOrganizacional.IdNivelOrganizacional = vDataReaderResults["IdNivelOrganizacional"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdNivelOrganizacional"].ToString()) : vNivelOrganizacional.IdNivelOrganizacional;
                            vNivelOrganizacional.CodigoNivelOrganizacional = vDataReaderResults["CodigoNivelOrganizacional"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoNivelOrganizacional"].ToString()) : vNivelOrganizacional.CodigoNivelOrganizacional;
                            vNivelOrganizacional.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vNivelOrganizacional.Descripcion;
                            vListaNivelOrganizacional.Add(vNivelOrganizacional);
                        }
                        return vListaNivelOrganizacional;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

   
    }
}
