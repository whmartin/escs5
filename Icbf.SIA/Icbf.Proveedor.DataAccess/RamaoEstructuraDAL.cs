using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Rama Estructura
    /// </summary>
    public class RamaoEstructuraDAL : GeneralDAL
    {
        public RamaoEstructuraDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_RamaoEstructura_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRamaEstructura", DbType.String, pRamaoEstructura.CodigoRamaEstructura);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRamaoEstructura.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRamaoEstructura.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRamaoEstructura.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRamaoEstructura.IdRamaEstructura = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRamaEstructura").ToString());
                    GenerarLogAuditoria(pRamaoEstructura, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_RamaoEstructura_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, pRamaoEstructura.IdRamaEstructura);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoRamaEstructura", DbType.String, pRamaoEstructura.CodigoRamaEstructura);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pRamaoEstructura.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pRamaoEstructura.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRamaoEstructura.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRamaoEstructura, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_RamaoEstructura_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, pRamaoEstructura.IdRamaEstructura);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRamaoEstructura, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Identificador de una entidad Rama Estructura</param>
        /// <returns>Entidad Rama Estructura</returns>
        public RamaoEstructura ConsultarRamaoEstructura(int pIdRamaEstructura)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_RamaoEstructura_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRamaEstructura", DbType.Int32, pIdRamaEstructura);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        RamaoEstructura vRamaoEstructura = new RamaoEstructura();
                        while (vDataReaderResults.Read())
                        {
                            vRamaoEstructura.IdRamaEstructura = vDataReaderResults["IdRamaEstructura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRamaEstructura"].ToString()) : vRamaoEstructura.IdRamaEstructura;
                            vRamaoEstructura.CodigoRamaEstructura = vDataReaderResults["CodigoRamaEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRamaEstructura"].ToString()) : vRamaoEstructura.CodigoRamaEstructura;
                            vRamaoEstructura.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRamaoEstructura.Descripcion;
                            vRamaoEstructura.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRamaoEstructura.Estado;
                            vRamaoEstructura.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRamaoEstructura.UsuarioCrea;
                            vRamaoEstructura.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRamaoEstructura.FechaCrea;
                            vRamaoEstructura.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRamaoEstructura.UsuarioModifica;
                            vRamaoEstructura.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRamaoEstructura.FechaModifica;
                        }
                        return vRamaoEstructura;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Rama Estructura
        /// </summary>
        /// <param name="pCodigoRamaEstructura">C�digo de una entidad Rama Estructura</param>
        /// <param name="pDescripcion">Descripci�n de una entidad Rama Estructura</param>
        /// <param name="pEstado">Estado de una entidad Rama Estructura</param>
        /// <returns>Lista de entidades Rama Estructura</returns>
        public List<RamaoEstructura> ConsultarRamaoEstructuras(String pCodigoRamaEstructura, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_RamaoEstructuras_Consultar"))
                {
                    if(pCodigoRamaEstructura != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoRamaEstructura", DbType.String, pCodigoRamaEstructura);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RamaoEstructura> vListaRamaoEstructura = new List<RamaoEstructura>();
                        while (vDataReaderResults.Read())
                        {
                                RamaoEstructura vRamaoEstructura = new RamaoEstructura();
                            vRamaoEstructura.IdRamaEstructura = vDataReaderResults["IdRamaEstructura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRamaEstructura"].ToString()) : vRamaoEstructura.IdRamaEstructura;
                            vRamaoEstructura.CodigoRamaEstructura = vDataReaderResults["CodigoRamaEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRamaEstructura"].ToString()) : vRamaoEstructura.CodigoRamaEstructura;
                            vRamaoEstructura.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRamaoEstructura.Descripcion;
                            vRamaoEstructura.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRamaoEstructura.Estado;
                            vRamaoEstructura.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRamaoEstructura.UsuarioCrea;
                            vRamaoEstructura.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRamaoEstructura.FechaCrea;
                            vRamaoEstructura.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRamaoEstructura.UsuarioModifica;
                            vRamaoEstructura.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRamaoEstructura.FechaModifica;
                                vListaRamaoEstructura.Add(vRamaoEstructura);
                        }
                        return vListaRamaoEstructura;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Rama Estructura
        /// </summary>
        /// <returns>Lista de entidades Rama Estructura</returns>
        public List<RamaoEstructura> ConsultarRamaoEstructurasAll()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All"))
                {
                        using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RamaoEstructura> vListaRamaoEstructura = new List<RamaoEstructura>();
                        while (vDataReaderResults.Read())
                        {
                            RamaoEstructura vRamaoEstructura = new RamaoEstructura();
                            vRamaoEstructura.IdRamaEstructura = vDataReaderResults["IdRamaEstructura"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRamaEstructura"].ToString()) : vRamaoEstructura.IdRamaEstructura;
                            vRamaoEstructura.CodigoRamaEstructura = vDataReaderResults["CodigoRamaEstructura"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoRamaEstructura"].ToString()) : vRamaoEstructura.CodigoRamaEstructura;
                            vRamaoEstructura.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vRamaoEstructura.Descripcion;
                            vRamaoEstructura.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vRamaoEstructura.Estado;
                            vRamaoEstructura.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRamaoEstructura.UsuarioCrea;
                            vRamaoEstructura.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRamaoEstructura.FechaCrea;
                            vRamaoEstructura.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRamaoEstructura.UsuarioModifica;
                            vRamaoEstructura.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRamaoEstructura.FechaModifica;
                            vListaRamaoEstructura.Add(vRamaoEstructura);
                        }
                        return vListaRamaoEstructura;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
