using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Revisi�n
    /// </summary>
    public class RevisionDAL : GeneralDAL
    {
        public RevisionDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Revisi�n
        /// </summary>
        /// <param name="pRevision">Entidad Revisi�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarRevision(Revision pRevision)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Revision_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdRevision", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pRevision.IdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Componente", DbType.String, pRevision.Componente);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int16 , pRevision.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pRevision.Finalizado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pRevision.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pRevision.IdRevision = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdRevision").ToString());
                    GenerarLogAuditoria(pRevision, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Revisi�n
        /// </summary>
        /// <param name="pRevision">Entidad Revisi�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarRevision(Revision pRevision)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Revision_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdRevision", DbType.Int32, 18);
                    //vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pRevision.IdEntidad);
                    //vDataBase.AddInParameter(vDbCommand, "@Componente", DbType.String, pRevision.Componente);
                    vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.Int16, pRevision.NroRevision);
                    vDataBase.AddInParameter(vDbCommand, "@Finalizado", DbType.Boolean, pRevision.Finalizado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pRevision.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pRevision, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Revisi�n
        /// </summary>
        /// <param name="pRevision">Identificador de una entidad Revisi�n</param>
        /// <returns>Entidad Revisi�n</returns>
        public Revision ConsultarRevision(int pIdRevision)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Revision_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdRevision", DbType.Int32, pIdRevision);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Revision vRevision = new Revision();
                        while (vDataReaderResults.Read())
                        {
                            vRevision.IdRevision = vDataReaderResults["IdRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRevision"].ToString()) : vRevision.IdRevision;
                            vRevision.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vRevision.IdEntidad;
                            vRevision.Componente = vDataReaderResults["Componente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Componente"].ToString()) : vRevision.Componente;
                            vRevision.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["NroRevision"].ToString()) : vRevision.NroRevision;
                            vRevision.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vRevision.Finalizado;
                            vRevision.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRevision.UsuarioCrea;
                            vRevision.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRevision.FechaCrea;
                            vRevision.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRevision.UsuarioModifica;
                            vRevision.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRevision.FechaModifica;
                        }
                        return vRevision;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Revisi�n
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad Revisi�n</param>
        /// <param name="pComponente">Componente en una entidad Revisi�n</param>
        /// <returns>Entidad Revisi�n</returns>
        public Revision ConsultarUltimaRevision(Int32 pIdEntidad, String pComponente)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Revision_Consultar_Ultima"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad);
                    vDataBase.AddInParameter(vDbCommand, "@Componente", DbType.Int32, pComponente);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Revision vRevision = new Revision();
                        while (vDataReaderResults.Read())
                        {
                            vRevision.IdRevision = vDataReaderResults["IdRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRevision"].ToString()) : vRevision.IdRevision;
                            vRevision.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vRevision.IdEntidad;
                            vRevision.Componente = vDataReaderResults["Componente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Componente"].ToString()) : vRevision.Componente;
                            vRevision.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["NroRevision"].ToString()) : vRevision.NroRevision;
                            vRevision.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vRevision.Finalizado;
                            vRevision.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRevision.UsuarioCrea;
                            vRevision.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRevision.FechaCrea;
                            vRevision.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRevision.UsuarioModifica;
                            vRevision.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRevision.FechaModifica;
                        }
                        return vRevision;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Revisi�n
        /// </summary>
        /// <param name="pIdRevision">Identificador de Revisi�n en una entidad Revisi�n</param>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad Revisi�n</param>
        /// <param name="pComponente">Componente en una entidad Revisi�n</param>
        /// <param name="pNroRevision">N�mero en una entidad Revisi�n</param>
        /// <returns>Lista de entidades Revisi�n</returns>
        public List<Revision> ConsultarRevisions(Int32? pIdRevision, Int32? pIdEntidad, String pComponente, Int16? pNroRevision)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Revisions_Consultar"))
                {

                    if (pIdRevision!= null)
                        vDataBase.AddInParameter(vDbCommand, "@IdRevision", DbType.Int32, pIdRevision.Value);

                    if (pIdEntidad != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdEntidad", DbType.Int32, pIdEntidad.Value);

                    if (pComponente != null)
                        vDataBase.AddInParameter(vDbCommand, "@Componente", DbType.String, pComponente);

                    if (pNroRevision != null)
                        vDataBase.AddInParameter(vDbCommand, "@NroRevision", DbType.String, pNroRevision.Value);

                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Revision> vListaRevision = new List<Revision>();
                        while (vDataReaderResults.Read())
                        {
                                Revision vRevision = new Revision();
                            vRevision.IdRevision = vDataReaderResults["IdRevision"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRevision"].ToString()) : vRevision.IdRevision;
                            vRevision.IdEntidad = vDataReaderResults["IdEntidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdEntidad"].ToString()) : vRevision.IdEntidad;
                            vRevision.Componente= vDataReaderResults["Componente"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Componente"].ToString()) : vRevision.Componente;
                            vRevision.NroRevision = vDataReaderResults["NroRevision"] != DBNull.Value ? Convert.ToInt16(vDataReaderResults["NroRevision"].ToString()) : vRevision.NroRevision;
                            vRevision.Finalizado = vDataReaderResults["Finalizado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Finalizado"].ToString()) : vRevision.Finalizado;
                            vRevision.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vRevision.UsuarioCrea;
                            vRevision.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vRevision.FechaCrea;
                            vRevision.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vRevision.UsuarioModifica;
                            vRevision.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vRevision.FechaModifica;
                            vListaRevision.Add(vRevision);
                        }
                        return vListaRevision;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    
    }
}
