using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.DataAccess
{
    /// <summary>
    /// Clase de capa de acceso que contiene los m�todos para obtener, insertar, actualizar registros, entre otros para la entidad Tipo Entidad
    /// </summary>
    public class TipoentidadDAL : GeneralDAL
    {
        public TipoentidadDAL()
        {
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidad_Insertar"))
                {
                    int vResultado;
                    vDataBase.AddOutParameter(vDbCommand, "@IdTipoentidad", DbType.Int32, 18);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoentidad", DbType.String, pTipoentidad.CodigoTipoentidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoentidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoentidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioCrea", DbType.String, pTipoentidad.UsuarioCrea);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    pTipoentidad.IdTipoentidad = Convert.ToInt32(vDataBase.GetParameterValue(vDbCommand, "@IdTipoentidad").ToString());
                    GenerarLogAuditoria(pTipoentidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("NombreUnico"))
                {
                    throw new Exception("La descripci�n ingresada ya existe");
                }
                else if (ex.Message.Contains("CodigoUnico"))
                {
                    throw new Exception("El c�digo ingresado ya existe");
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidad_Modificar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoentidad", DbType.Int32, pTipoentidad.IdTipoentidad);
                    vDataBase.AddInParameter(vDbCommand, "@CodigoTipoentidad", DbType.String, pTipoentidad.CodigoTipoentidad);
                    vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pTipoentidad.Descripcion);
                    vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pTipoentidad.Estado);
                    vDataBase.AddInParameter(vDbCommand, "@UsuarioModifica", DbType.String, pTipoentidad.UsuarioModifica);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoentidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("NombreUnico"))
                {
                    throw new Exception("La descripci�n ingresada ya existe");
                }
                else if (ex.Message.Contains("CodigoUnico"))
                {
                    throw new Exception("El c�digo ingresado ya existe");
                }
                else
                {
                    throw new GenericException(ex);
                }
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidad_Eliminar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoentidad", DbType.Int32, pTipoentidad.IdTipoentidad);
                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);
                    GenerarLogAuditoria(pTipoentidad, vDbCommand);
                    return vResultado;
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Entidad
        /// </summary>
        /// <param name="pIdTipoentidad">Identificador en una entidad Tipo Entidad</param>
        /// <returns>Entidad Tipo Entidad</returns>
        public Tipoentidad ConsultarTipoentidad(int pIdTipoentidad)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidad_Consultar"))
                {
                    vDataBase.AddInParameter(vDbCommand, "@IdTipoentidad", DbType.Int32, pIdTipoentidad);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        Tipoentidad vTipoentidad = new Tipoentidad();
                        while (vDataReaderResults.Read())
                        {
                            vTipoentidad.IdTipoentidad = vDataReaderResults["IdTipoentidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoentidad"].ToString()) : vTipoentidad.IdTipoentidad;
                            vTipoentidad.CodigoTipoentidad = vDataReaderResults["CodigoTipoentidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoentidad"].ToString()) : vTipoentidad.CodigoTipoentidad;
                            vTipoentidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoentidad.Descripcion;
                            vTipoentidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoentidad.Estado;
                            vTipoentidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoentidad.UsuarioCrea;
                            vTipoentidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoentidad.FechaCrea;
                            vTipoentidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoentidad.UsuarioModifica;
                            vTipoentidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoentidad.FechaModifica;
                        }
                        return vTipoentidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad
        /// </summary>
        /// <param name="pCodigoTipoentidad">C�digo en una entidad Tipo Entidad</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Entidad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Entidad</param>
        /// <returns>Lista de entidades Tipo Entidad</returns>
        public List<Tipoentidad> ConsultarTipoentidads(String pCodigoTipoentidad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidads_Consultar"))
                {
                    if(pCodigoTipoentidad != null)
                         vDataBase.AddInParameter(vDbCommand, "@CodigoTipoentidad", DbType.String, pCodigoTipoentidad);
                    if(pDescripcion != null)
                         vDataBase.AddInParameter(vDbCommand, "@Descripcion", DbType.String, pDescripcion);
                    if(pEstado != null)
                         vDataBase.AddInParameter(vDbCommand, "@Estado", DbType.Boolean, pEstado);
                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Tipoentidad> vListaTipoentidad = new List<Tipoentidad>();
                        while (vDataReaderResults.Read())
                        {
                                Tipoentidad vTipoentidad = new Tipoentidad();
                            vTipoentidad.IdTipoentidad = vDataReaderResults["IdTipoentidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoentidad"].ToString()) : vTipoentidad.IdTipoentidad;
                            vTipoentidad.CodigoTipoentidad = vDataReaderResults["CodigoTipoentidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoentidad"].ToString()) : vTipoentidad.CodigoTipoentidad;
                            vTipoentidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoentidad.Descripcion;
                            vTipoentidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoentidad.Estado;
                            vTipoentidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoentidad.UsuarioCrea;
                            vTipoentidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoentidad.FechaCrea;
                            vTipoentidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoentidad.UsuarioModifica;
                            vTipoentidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoentidad.FechaModifica;
                                vListaTipoentidad.Add(vTipoentidad);
                        }
                        return vListaTipoentidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad</returns>
        public List<Tipoentidad> ConsultarTodosTipoentidadAll(int? pIdTipoPersona, int? pIdSector)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_RubOnline_Proveedor_Tipoentidads_Consultar_All"))
                {
                    if (pIdTipoPersona != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdTipoPersona", DbType.Int32, pIdTipoPersona);
                    if (pIdSector != null)
                        vDataBase.AddInParameter(vDbCommand, "@IdSector", DbType.Int32, pIdSector);

                         using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<Tipoentidad> vListaTipoentidad = new List<Tipoentidad>();
                        while (vDataReaderResults.Read())
                        {
                            Tipoentidad vTipoentidad = new Tipoentidad();
                            vTipoentidad.IdTipoentidad = vDataReaderResults["IdTipoentidad"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdTipoentidad"].ToString()) : vTipoentidad.IdTipoentidad;
                            vTipoentidad.CodigoTipoentidad = vDataReaderResults["CodigoTipoentidad"] != DBNull.Value ? Convert.ToString(vDataReaderResults["CodigoTipoentidad"].ToString()) : vTipoentidad.CodigoTipoentidad;
                            vTipoentidad.Descripcion = vDataReaderResults["Descripcion"] != DBNull.Value ? Convert.ToString(vDataReaderResults["Descripcion"].ToString()) : vTipoentidad.Descripcion;
                            vTipoentidad.Estado = vDataReaderResults["Estado"] != DBNull.Value ? Convert.ToBoolean(vDataReaderResults["Estado"].ToString()) : vTipoentidad.Estado;
                            vTipoentidad.UsuarioCrea = vDataReaderResults["UsuarioCrea"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioCrea"].ToString()) : vTipoentidad.UsuarioCrea;
                            vTipoentidad.FechaCrea = vDataReaderResults["FechaCrea"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaCrea"].ToString()) : vTipoentidad.FechaCrea;
                            vTipoentidad.UsuarioModifica = vDataReaderResults["UsuarioModifica"] != DBNull.Value ? Convert.ToString(vDataReaderResults["UsuarioModifica"].ToString()) : vTipoentidad.UsuarioModifica;
                            vTipoentidad.FechaModifica = vDataReaderResults["FechaModifica"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaModifica"].ToString()) : vTipoentidad.FechaModifica;
                            vListaTipoentidad.Add(vTipoentidad);
                        }
                        return vListaTipoentidad;
                    }
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
