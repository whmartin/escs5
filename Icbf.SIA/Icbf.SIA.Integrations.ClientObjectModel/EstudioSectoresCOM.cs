﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Icbf.SIA.Integrations.ClientObjectModel
{
    public class EstudioSectoresCOM
    {
        public T ConsultaFechasPACCO<T>(string pIdConsecutivoEstudio, string pFechaEstimadaInicioEjecucion, string pIdModalidadSeleccionPACCO, string pModalidadSeleccionPACCO, decimal pValorContrato)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["urlApiTiemposPACCO"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var request = new
                {
                    pIdConsecutivoEstudio = pIdConsecutivoEstudio,
                    pFechaEstimadaInicioEjecucion = pFechaEstimadaInicioEjecucion,
                    pIdModalidadSeleccionPACCO= pIdModalidadSeleccionPACCO,
                    pModalidadSeleccionPACCO= pModalidadSeleccionPACCO,
                    pValorContrato= pValorContrato

                };
                var serializer = new JavaScriptSerializer();
                TiemposPACCO vTiemposPACCO = new TiemposPACCO();
                HttpResponseMessage response = client.PostAsync("/api/estudiosectores/ConsultaFechasPACCO", new StringContent( serializer.Serialize(request).ToString(),
                                Encoding.UTF8, "application/json")).Result;

                string responseBody = string.Empty;

                if (response.IsSuccessStatusCode)
                {

                    Task<string> resp = response.Content.ReadAsStringAsync();
                    string texto = resp.Result;
                    return JsonConvert.DeserializeObject<T>(texto);
                }
                else
                {
                    return default(T);
                }
               
                
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
