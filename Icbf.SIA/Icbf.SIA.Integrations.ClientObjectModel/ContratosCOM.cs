﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Icbf.Contrato.Entity;
using System.Web.Script.Serialization;

namespace Icbf.SIA.Integrations.ClientObjectModel
{
    public class ContratosCOM
    {
        HttpClient client;

        private string vURL = ConfigurationManager.AppSettings["URLWebApiSIAContratos"];

        public ContratosCOM()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(vURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<InfoContratoPaccoDTO>> GetContratoAsync(int idPlanCompras)
        {
            HttpResponseMessage response = client.GetAsync(vURL + "/" + idPlanCompras).Result;
            string responseBody = string.Empty;
            List<InfoContratoPaccoDTO> entidades = new List<InfoContratoPaccoDTO>();

            if (response.IsSuccessStatusCode)
            {
                var serializer = new JavaScriptSerializer();
                entidades = serializer.Deserialize<List<InfoContratoPaccoDTO>>(await response.Content.ReadAsStringAsync());
            }

            return entidades;
        }

        public async Task<InfoContratoDTO> GetContratoAsync(string anio, string numeroContrato, string codigoRegional)
        {
            HttpResponseMessage response = client.GetAsync(vURL + "/" + anio + "/" + codigoRegional + "/" + numeroContrato).Result;
           
            string responseBody = string.Empty;
            
            InfoContratoDTO entidad = new InfoContratoDTO();

            if (response.IsSuccessStatusCode)
            {
                var serializer = new JavaScriptSerializer();
                entidad = serializer.Deserialize<InfoContratoDTO>(await response.Content.ReadAsStringAsync());
            }

            return entidad;
        }

        public List<InfoContratoPaccoDTO> GetContrato(int idPlanCompras)
        {
            HttpResponseMessage response = client.GetAsync(vURL + "/" + idPlanCompras).Result;
            string responseBody = string.Empty;
            List<InfoContratoPaccoDTO> entidades = new List<InfoContratoPaccoDTO>();

            if (response.IsSuccessStatusCode)
            {
                var serializer = new JavaScriptSerializer();
                Task<string> resp = response.Content.ReadAsStringAsync();
                entidades = serializer.Deserialize<List<InfoContratoPaccoDTO>>(resp.Result);
            }

            return entidades;
        }

        public InfoContratoDTO GetContrato(string anio, string numeroContrato, string codigoRegional)
        {
            HttpResponseMessage response = client.GetAsync(vURL + "/" + anio + "/" + codigoRegional + "/" + numeroContrato).Result;

            string responseBody = string.Empty;

            InfoContratoDTO entidades = new InfoContratoDTO();

            if (response.IsSuccessStatusCode)
            {
                var serializer = new JavaScriptSerializer();
                Task<string> resp = response.Content.ReadAsStringAsync();
                entidades = serializer.Deserialize<InfoContratoDTO>(resp.Result);
            }

            return entidades;
        }
    }
}
