using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Clase de actividad
    /// </summary>
    public class ClaseActividadBLL
    {
        private ClaseActividadDAL vClaseActividadDAL;
        public ClaseActividadBLL()
        {
            vClaseActividadDAL = new ClaseActividadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Clase de Actividad
        /// </summary>
        /// <param name="pClaseActividad">Entidad Clase de Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarClaseActividad(ClaseActividad pClaseActividad)
        {
            try
            {
                return vClaseActividadDAL.InsertarClaseActividad(pClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Clase de Actividad
        /// </summary>
        /// <param name="pClaseActividad">Entidad Clase de Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarClaseActividad(ClaseActividad pClaseActividad)
        {
            try
            {
                return vClaseActividadDAL.ModificarClaseActividad(pClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Clase de Actividad
        /// </summary>
        /// <param name="pIdClaseActividad">Identificador en una entidad Clase de Actividad</param>
        /// <returns>Entidad Clase de Actividad</returns>
        public ClaseActividad ConsultarClaseActividad(int pIdClaseActividad)
        {
            try
            {
                return vClaseActividadDAL.ConsultarClaseActividad(pIdClaseActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de clases de actividades
        /// </summary>
        /// <param name="pCodigo">C�digo de la clase de actividad</param>
        /// <param name="pDescripcion">Descripci�n de la clase de actividad</param>
        /// <param name="pEstado">Estado de la clase de actividad</param>
        /// <returns>Lista de clases de actividades</returns>
        public List<ClaseActividad> ConsultarClaseActividads(String pCodigo, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vClaseActividadDAL.ConsultarClaseActividads(pCodigo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
