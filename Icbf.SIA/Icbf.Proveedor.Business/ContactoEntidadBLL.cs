using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    ///  Clase de capa de negocio para la entidad Contacto Entidad
    /// </summary>
    public class ContactoEntidadBLL
    {
        private ContactoEntidadDAL vContactoEntidadDAL;
        public ContactoEntidadBLL()
        {
            vContactoEntidadDAL = new ContactoEntidadDAL();
        }

        /// <summary>
        /// Inserta un nuevo Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                return vContactoEntidadDAL.InsertarContactoEntidad(pContactoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica un Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                return vContactoEntidadDAL.ModificarContactoEntidad(pContactoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un Contacto Entidad
        /// </summary>
        /// <param name="pContactoEntidad">Entidad Contacto Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarContactoEntidad(ContactoEntidad pContactoEntidad)
        {
            try
            {
                return vContactoEntidadDAL.EliminarContactoEntidad(pContactoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Contacto Entidad
        /// </summary>
        /// <param name="pIdContacto">Identificador de un Contacto Entidad</param>
        /// <returns>Entidad Contacto Entidad</returns>
        public ContactoEntidad ConsultarContactoEntidad(int pIdContacto)
        {
            try
            {
                return vContactoEntidadDAL.ConsultarContactoEntidad(pIdContacto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Contacto Entidad
        /// </summary>
        /// <param name="pIdEntidad">Identificador del Contacto Entidad</param>
        /// <param name="pNumeroIdentificacion">N�mero de identificaci�n del Contacto Entidad</param>
        /// <param name="pPrimerNombre">Primer nombre del Contacto Entidad</param>
        /// <param name="pSegundoNombre">Segundo nombre del Contacto Entidad</param>
        /// <param name="pPrimerApellido">Primer apellido del Contacto Entidad</param>
        /// <param name="pSegundoApellido">Segundo apellido del Contacto Entidad</param>
        /// <param name="pDependencia">Dependencia del Contacto Entidad</param>
        /// <param name="pEmail">Correo electr�nico del Contacto Entidad</param>
        /// <param name="pEstado">Estado del Contacto Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ContactoEntidad> ConsultarContactoEntidads(int ?pIdEntidad, Decimal ?pNumeroIdentificacion, String pPrimerNombre, String pSegundoNombre, String pPrimerApellido, String pSegundoApellido, String pDependencia, String pEmail, Boolean? pEstado)
        {
            try
            {
                return vContactoEntidadDAL.ConsultarContactoEntidads(pIdEntidad, pNumeroIdentificacion, pPrimerNombre, pSegundoNombre, pPrimerApellido, pSegundoApellido, pDependencia, pEmail, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
