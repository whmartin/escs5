using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  de validaci�n de experiencia
    /// </summary>
    public class ValidarInfoExperienciaEntidadBLL
    {
        private ValidarInfoExperienciaEntidadDAL vValidarInfoExperienciaEntidadDAL;
        
        public ValidarInfoExperienciaEntidadBLL()
        {
            vValidarInfoExperienciaEntidadDAL = new ValidarInfoExperienciaEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pValidarInfoExperienciaEntidad">Entidad Validaci�n de experiencia</param>
        /// <param name="idEstadoInfoExperienciaEntidad">Identificador del estado</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValidarInfoExperienciaEntidad(ValidarInfoExperienciaEntidad pValidarInfoExperienciaEntidad, int idEstadoInfoExperienciaEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadDAL.InsertarValidarInfoExperienciaEntidad(pValidarInfoExperienciaEntidad, idEstadoInfoExperienciaEntidad );               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pValidarInfoExperienciaEntidad">Entidad Validaci�n de experiencia</param>
        /// <param name="idEstadoInfoExperienciaEntidad">Identificador del estado</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarValidarInfoExperienciaEntidad(ValidarInfoExperienciaEntidad pValidarInfoExperienciaEntidad, int idEstadoInfoExperienciaEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadDAL.ModificarValidarInfoExperienciaEntidad(pValidarInfoExperienciaEntidad, idEstadoInfoExperienciaEntidad);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdValidarInfoExperienciaEntidad">Identificador de una entidad Validaci�n de experiencia</param>
        /// <returns>Entidad Validaci�n de experiencia</returns>
        public ValidarInfoExperienciaEntidad ConsultarValidarInfoExperienciaEntidad(int pIdValidarInfoExperienciaEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadDAL.ConsultarValidarInfoExperienciaEntidad(pIdValidarInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en una entidad Validaci�n de experiencia</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad Validaci�n de experiencia</param>
        /// <returns>Lista de entidades Validaci�n de experiencia</returns>
        public List<ValidarInfoExperienciaEntidad> ConsultarValidarInfoExperienciaEntidads(Int32 pIdExpEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadDAL.ConsultarValidarInfoExperienciaEntidads(pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en la entidad Validaci�n de experiencia</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad Validaci�n de experiencia</param>
        /// <returns>Lista de entidades Validaci�n de experiencia</returns>
        public List<ValidarInfoExperienciaEntidad> ConsultarValidarInfoExperienciaEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoExperienciaEntidadDAL.ConsultarValidarInfoExperienciaEntidadsResumen(pIdEntidad , pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validaci�n de experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pIdExpEntidad">Identificador de la experiencia en la entidad Validaci�n de experiencia</param>
        /// <returns>Entidad Validaci�n de experiencia</returns>
        public ValidarInfoExperienciaEntidad ConsultarValidarInfoExperienciaEntidad_Ultima(Int32 pIdEntidad, Int32 pIdExpEntidad)
        {
            try
            {
                return vValidarInfoExperienciaEntidadDAL.ConsultarValidarInfoExperienciaEntidad_Ultima(pIdEntidad, pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
