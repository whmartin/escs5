using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Clase Entidad
    /// </summary>
    public class ClasedeEntidadBLL
    {
        private ClasedeEntidadDAL vClasedeEntidadDAL;
        public ClasedeEntidadBLL()
        {
            vClasedeEntidadDAL = new ClasedeEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadDAL.InsertarClasedeEntidad(pClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadDAL.ModificarClasedeEntidad(pClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una Clase Entidad
        /// </summary>
        /// <param name="pClasedeEntidad">Entidad Clase Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarClasedeEntidad(ClasedeEntidad pClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadDAL.EliminarClasedeEntidad(pClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una Clase Entidad
        /// </summary>
        /// <param name="pIdClasedeEntidad">Identificador de una Clase Entidad</param>
        /// <returns>Entidad Clase Entidad</returns>
        public ClasedeEntidad ConsultarClasedeEntidad(int pIdClasedeEntidad)
        {
            try
            {
                return vClasedeEntidadDAL.ConsultarClasedeEntidad(pIdClasedeEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pDescripcion">Descripci�n de una Clase Entidad</param>
        /// <param name="pEstado">Estado de la Clase Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidads(int? pIdTipodeActividad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vClasedeEntidadDAL.ConsultarClasedeEntidads(pIdTipodeActividad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
        public List<ClasedeEntidad> ConsultarClasedeEntidadAll(int? pIdTipodeActividad)
        {
            try
            {
                return vClasedeEntidadDAL.ConsultarClasedeEntidadAll(pIdTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Clase Entidad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador del Tipo de Actividad</param>
        /// <param name="pIdTipoEntidad">Identificador del Tipo de Entidad</param>
        /// <returns>Lista de entidades Clase Entidad</returns>
       
        public List<ClasedeEntidad> ConsultarClasedeEntidadTipodeActividadTipoEntidad(int? pIdTipodeActividad, int? pIdTipoEntidad, int? pIdTipoPersona, int? pIdSector, int? pIdRegmenTributario)
        {
            try
            {
                return vClasedeEntidadDAL.ConsultarClasedeEntidadTipodeActividadTipoEntidad(pIdTipodeActividad, pIdTipoEntidad, pIdTipoPersona, pIdSector, pIdRegmenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
