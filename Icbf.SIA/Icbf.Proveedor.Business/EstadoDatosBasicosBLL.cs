using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Estado Datos B�sicos
    /// </summary>
    public class EstadoDatosBasicosBLL
    {
        private EstadoDatosBasicosDAL vEstadoDatosBasicosDAL;
        public EstadoDatosBasicosBLL()
        {
            vEstadoDatosBasicosDAL = new EstadoDatosBasicosDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos B�sicos</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosDAL.InsertarEstadoDatosBasicos(pEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos B�sicos</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosDAL.ModificarEstadoDatosBasicos(pEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstadoDatosBasicos">Entidad Estado Datos B�sicos</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEstadoDatosBasicos(EstadoDatosBasicos pEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosDAL.EliminarEstadoDatosBasicos(pEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado Datos B�sicos
        /// </summary>
        /// <param name="pIdEstadoDatosBasicos">Identificador de una entidad Estado Datos B�sicos</param>
        /// <returns>Entidad Estado Datos B�sicos</returns>
        public EstadoDatosBasicos ConsultarEstadoDatosBasicos(int pIdEstadoDatosBasicos)
        {
            try
            {
                return vEstadoDatosBasicosDAL.ConsultarEstadoDatosBasicos(pIdEstadoDatosBasicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Datos B�sicos
        /// </summary>
        /// <param name="pDescripcion">Descripci�n en una entidad Estado Datos B�sicos</param>
        /// <param name="pEstado">Estado en una entidad Estado Datos B�sicos</param>
        /// <returns>Lista de entidades Estado Datos B�sicos</returns>
        public List<EstadoDatosBasicos> ConsultarEstadoDatosBasicoss(String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vEstadoDatosBasicosDAL.ConsultarEstadoDatosBasicoss(pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Datos B�sicos
        /// </summary>
        /// <param name="pEstado">Estado en una entidad Estado Datos B�sicos</param>
        /// <returns>Lista de entidades Estado Datos B�sicos</returns>
        public List<EstadoDatosBasicos> ConsultarEstadoDatosBasicossAll( Boolean? pEstado)
        {
            try
            {
                return vEstadoDatosBasicosDAL.ConsultarEstadoDatosBasicossAll(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
