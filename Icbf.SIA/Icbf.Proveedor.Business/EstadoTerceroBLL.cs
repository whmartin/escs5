using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Estado Tercero
    /// </summary>
    public class EstadoTerceroBLL
    {
        private EstadoTerceroDAL vEstadoTerceroDAL;
        public EstadoTerceroBLL()
        {
            vEstadoTerceroDAL = new EstadoTerceroDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero">Entidad Estado Tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarEstadoTercero(EstadoTercero pEstadoTercero)
        {
            try
            {
                return vEstadoTerceroDAL.InsertarEstadoTercero(pEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero">Entidad Estado Tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEstadoTercero(EstadoTercero pEstadoTercero)
        {
            try
            {
                return vEstadoTerceroDAL.ModificarEstadoTercero(pEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado Tercero
        /// </summary>
        /// <param name="pEstadoTercero">Entidad Estado Tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEstadoTercero(EstadoTercero pEstadoTercero)
        {
            try
            {
                return vEstadoTerceroDAL.EliminarEstadoTercero(pEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado Tercero
        /// </summary>
        /// <param name="pIdEstadoTercero">Identificador de una entidad Estado Tercero</param>
        /// <param name="pEstado">Estado de una entidad Estado Tercero</param>
        /// <returns>Entidad Estado Tercero</returns>
        public EstadoTercero ConsultarEstadoTercero(int pIdEstadoTercero, int pEstado)
        {
            try
            {
                return vEstadoTerceroDAL.ConsultarEstadoTercero(pIdEstadoTercero, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado Tercero
        /// </summary>
        /// <param name="pIdEstadoTercero">Identificador de una entidad Estado Tercero</param>
        /// <returns>Lista de entidades Estado Tercero</returns>
        public List<EstadoTercero> ConsultarEstadoTerceros(int? pIdEstadoTercero)
        {
            try
            {
                return vEstadoTerceroDAL.ConsultarEstadoTerceros(pIdEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un motivo de cambio de estado de Proveedor
        /// </summary>
        /// <param name="vTercero">Identificador del Tercero</param>
        /// <param name="datosBasicos">Si tiene Datos b�sicos</param>
        /// <param name="financiera">Si tiene informaci�n financiera</param>
        /// <param name="experiencia">Si tiene experiencia</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <param name="motivo">Motivo del cambio</param>
        /// <param name="usuario">Usuario que crea la entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarMotivoCambioEstado(int? vTercero,bool datosBasicos,bool financiera, bool experiencia, bool integrantes, string IdTemporal, string motivo, string usuario)
        {
            try
            {
                return vEstadoTerceroDAL.InsertarMotivoCambioEstado(vTercero, datosBasicos, financiera, experiencia, integrantes, IdTemporal, motivo, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Inserta un motivo de cambio de estado de Tercero
        /// </summary>
        /// <param name="vTercero">Identificador del Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <param name="motivo">Motivo del cambio</param>
        /// <param name="usuario">Usuario que crea la entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarMotivoCambioEstadoTercero(int? vTercero,  string IdTemporal, string motivo, string usuario)
        {
            try
            {
                return vEstadoTerceroDAL.InsertarMotivoCambioEstadoTercero(vTercero, IdTemporal, motivo, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Env�a correos por cambio de estado
        /// </summary>
        /// <param name="Tercero">Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <param name="correos">correos</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EnviarMailsCambioEstadoTerceroProveedor(string Tercero, string IdTemporal, string correos)
        {
            try
            {
                return vEstadoTerceroDAL.EnviarMailsCambioEstadoTerceroProveedor(Tercero, IdTemporal,correos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el cuerpo de correo por cambio de estado
        /// </summary>
        /// <param name="Tercero">Tercero</param>
        /// <param name="IdTemporal">Identificador del temporal</param>
        /// <returns>string de Cuero de Correo</returns>
        public string ObtenerCuerpoDeCorreo_CambioEstadoTerceroProveedor(string Tercero, string IdTemporal)
        {
            try
            {
                return vEstadoTerceroDAL.ObtenerCuerpoDeCorreo_CambioEstadoTerceroProveedor(Tercero, IdTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Obtiene correos 
        /// </summary>
        /// <param name="ApplicationName">Nombre del ApplicationName</param>
        /// <param name="RoleName">Nombre del RoleName</param>
        /// <returns>Correo en proveedores</returns>
        public string ObtenerMailsGestorProveedor(string ApplicationName, string RoleName)
        {
            try
            {
                return vEstadoTerceroDAL.ObtenerMailsGestorProveedor(ApplicationName, RoleName);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        
    }
}
