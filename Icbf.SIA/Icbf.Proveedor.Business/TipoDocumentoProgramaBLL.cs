using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Entity;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo Documento Programa
    /// </summary>
    public class TipoDocumentoProgramaBLL
    {
        private TipoDocumentoProgramaDAL vTipoDocumentoProgramaDAL;
        public TipoDocumentoProgramaBLL()
        {
            vTipoDocumentoProgramaDAL = new TipoDocumentoProgramaDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.InsertarTipoDocumentoPrograma(pTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.ModificarTipoDocumentoPrograma(pTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pTipoDocumentoPrograma">Entidad Tipo Documento programa</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoDocumentoPrograma(TipoDocumentoPrograma pTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.EliminarTipoDocumentoPrograma(pTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumentoPrograma">Identificador en una entidad Tipo Documento programa</param>
        /// <returns>Entidad Tipo Documento programa</returns>
        public TipoDocumentoPrograma ConsultarTipoDocumentoPrograma(int pIdTipoDocumentoPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.ConsultarTipoDocumentoPrograma(pIdTipoDocumentoPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumentoPrograma">Identificador del tipo de documento de programa en una entidad Tipo Documento programa</param>
        /// <param name="pIdTipoDocumento">Identificador del tipo de documento</param>
        /// <param name="pIdPrograma">Identificador del programa</param>
        /// <returns>Lista de entidades Tipo Documento programa</returns>
        public List<TipoDocumentoPrograma> ConsultarTipoDocumentoProgramas(int? pIdTipoDocumentoPrograma, int? pIdTipoDocumento, int? pIdPrograma)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.ConsultarTipoDocumentoProgramas(pIdTipoDocumentoPrograma, pIdTipoDocumento, pIdPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Documento programa
        /// </summary>
        /// <param name="pIdTipoDocumento">Identificador del tipo del documento en una entidad Tipo Documento programa</param>
        /// <returns>Lista de entidades Tipo Documento programa</returns>
        public List<TipoDocumentoProveedor> ConsultarTipoDocumentos(int? pIdTipoDocumento)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.ConsultarTipoDocumentos(pIdTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar programas
        /// </summary>
        /// <param name="pIdModulo">Identificador del m�dulo</param>
        public List<Programa> ConsultarProgramas(int pIdModulo)
        {
            try
            {
                return vTipoDocumentoProgramaDAL.ConsultarProgramas(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }

}
