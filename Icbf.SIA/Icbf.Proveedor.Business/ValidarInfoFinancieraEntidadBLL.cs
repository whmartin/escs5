using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  de validación de información de financiera
    /// </summary>
    public class ValidarInfoFinancieraEntidadBLL
    {
        private ValidarInfoFinancieraEntidadDAL vValidarInfoFinancieraEntidadDAL;
        
        public ValidarInfoFinancieraEntidadBLL()
        {
            vValidarInfoFinancieraEntidadDAL = new ValidarInfoFinancieraEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pValidarInfoFinancieraEntidad">Entidad Validación de información financiera del proveedor</param>
        /// <param name="idEstadoInfoFinancieraEntidad">Identificador del estado en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Nůmero de registros afectados</returns>
        public int InsertarValidarInfoFinancieraEntidad(ValidarInfoFinancieraEntidad pValidarInfoFinancieraEntidad, int idEstadoInfoFinancieraEntidad)
        {
            try
            {
                return vValidarInfoFinancieraEntidadDAL.InsertarValidarInfoFinancieraEntidad(pValidarInfoFinancieraEntidad, idEstadoInfoFinancieraEntidad );               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pValidarInfoFinancieraEntidad">Entidad Validación de información financiera del proveedor</param>
        /// <param name="idEstadoInfoFinancieraEntidad">Identificador del estado en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Nůmero de registros afectados</returns>
        public int ModificarValidarInfoFinancieraEntidad(ValidarInfoFinancieraEntidad pValidarInfoFinancieraEntidad, int idEstadoInfoFinancieraEntidad)
        {
            try
            {
                return vValidarInfoFinancieraEntidadDAL.ModificarValidarInfoFinancieraEntidad(pValidarInfoFinancieraEntidad, idEstadoInfoFinancieraEntidad);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdValidarInfoFinancieraEntidad">Identificador en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Entidad Validación de información financiera del proveedor</returns>
        public ValidarInfoFinancieraEntidad ConsultarValidarInfoFinancieraEntidad(int pIdValidarInfoFinancieraEntidad)
        {
            try
            {
                return vValidarInfoFinancieraEntidadDAL.ConsultarValidarInfoFinancieraEntidad(pIdValidarInfoFinancieraEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdInfoFin">Identificador de informaciňn financiera en una entidad Validación de información financiera del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad Validación de información financiera del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Lista  de entidades Validación de información financiera del proveedor</returns>
        public ValidarInfoFinancieraEntidad ConsultarValidarInfoFinancieraEntidad_Ultima(Int32 pIdEntidad, int pIdVigencia)
        {
            try
            {
                return vValidarInfoFinancieraEntidadDAL.ConsultarValidarInfoFinancieraEntidad_Ultima(pIdEntidad, pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pObservaciones">Observaciones en la entidad Validación de información financiera del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es Confirma y aprueba valor en la entidad Validación de información financiera del proveedor</param>
        /// <returns>Lista de entidades Validación de información financiera del proveedor</returns>
        public List<ValidarInfoFinancieraEntidad> ConsultarValidarInfoFinancieraEntidads(Int32 pIdInfoFinancieraEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoFinancieraEntidadDAL.ConsultarValidarInfoFinancieraEntidads(pIdInfoFinancieraEntidad , pObservaciones , pConfirmaYAprueba );
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Validación de información financiera del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <param name="pIdVigencia">Identificador de la vigencia en una entidad Validación de información financiera del proveedor</param>
        /// <returns>Entidad Validación de información financiera del proveedor</returns>
        public List<ValidarInfoFinancieraEntidad> ConsultarValidarInfoFinancieraEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoFinancieraEntidadDAL.ConsultarValidarInfoFinancieraEntidadsResumen(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
