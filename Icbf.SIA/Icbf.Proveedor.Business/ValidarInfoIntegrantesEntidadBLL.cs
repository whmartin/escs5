﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de validación de datos de integrantes de un proveedor
    /// </summary>
    public class ValidarInfoIntegrantesEntidadBLL
    {

        private ValidarInfoIntegrantesEntidadDAL vValidarInfoIntegrantesEntidadDAL;

        public ValidarInfoIntegrantesEntidadBLL()
        {
            vValidarInfoIntegrantesEntidadDAL = new ValidarInfoIntegrantesEntidadDAL();
        }


        /// <summary>
        /// Consulta una lista de entidades validación de datos de integrantes del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de Integrantes del proveedor</param>
        /// <returns>Lista de entidades validación de datos básicos del proveedor</returns>
        public List<ValidarInfoIntegrantesEntidad> ConsultarValidarInfoIntegrantesEntidadsResumen(Int32 pIdEntidad)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadDAL.ConsultarValidarInfoIntegrantesEntidadsResumen(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Consulta una lista de entidades validación de Integrantes del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad validación de Integrantes del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validación de Integrantes  del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad validación de Integrantes del proveedor</param>
        /// <returns>Lista de entidades validación de datos básicos del proveedor</returns>
        public List<ValidarInfoIntegrantesEntidad> ConsultarValidarInfoIntegrantesEntidad(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadDAL.ConsultarValidarInfoIntegrantesEntidad(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Entidad validación de datos básicos del proveedor</returns>
        public ValidarInfoIntegrantesEntidad ConsultarValidarInfoIntegrantesEntidad_Ultima(Int32 pIdEntidad)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadDAL.ConsultarValidarInfoIntegrantesEntidad_Ultima(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados</returns>
        public int InsertarValidarInfoIntegrantesEntidad(ValidarInfoIntegrantesEntidad pValidarInfoIntegrantesEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadDAL.InsertarValidarInfoIntegrantesEntidad(pValidarInfoIntegrantesEntidad, idEstadoInfoDatosBasicosEntidad);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Modifica una entidad Validación de datos básicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoIntegrantesEntidad">Entidad validación de datos básicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validación de datos básicos del proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int ModificarValidarInfoIntegrantesEntidad(ValidarInfoIntegrantesEntidad pValidarInfoIntegrantesEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return vValidarInfoIntegrantesEntidadDAL.ModificarValidarInfoIntegrantesEntidad(pValidarInfoIntegrantesEntidad, idEstadoInfoDatosBasicosEntidad);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}