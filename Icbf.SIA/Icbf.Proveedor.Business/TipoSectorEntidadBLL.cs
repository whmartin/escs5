using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo Sector
    /// </summary>
    public class TipoSectorEntidadBLL
    {
        private TipoSectorEntidadDAL vTipoSectorEntidadDAL;
        public TipoSectorEntidadBLL()
        {
            vTipoSectorEntidadDAL = new TipoSectorEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadDAL.InsertarTipoSectorEntidad(pTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadDAL.ModificarTipoSectorEntidad(pTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pTipoSectorEntidad">Entidad Tipo Sector Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoSectorEntidad(TipoSectorEntidad pTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadDAL.EliminarTipoSectorEntidad(pTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Sector Entidad
        /// </summary>
        /// <param name="pIdTipoSectorEntidad">Identificador en una entidad Tipo Sector Entidad</param>
        /// <returns>Entidad Tipo Sector Entidad</returns>
        public TipoSectorEntidad ConsultarTipoSectorEntidad(int pIdTipoSectorEntidad)
        {
            try
            {
                return vTipoSectorEntidadDAL.ConsultarTipoSectorEntidad(pIdTipoSectorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Sector Entidad
        /// </summary>
        /// <returns>Lista de entidades Tipo Sector Entidad</returns>
        public List<TipoSectorEntidad> ConsultarTipoSectorEntidadAll()
        {
            try
            {
                return vTipoSectorEntidadDAL.ConsultarTipoSectorEntidadAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Sector Entidad
        /// </summary>
        /// <param name="pCodigoSectorEntidad">C�digo en una entidad Tipo Sector Entidad</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Sector Entidad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Sector Entidad</param>
        /// <returns>Lista de entidades Tipo Sector Entidad</returns>
        public List<TipoSectorEntidad> ConsultarTipoSectorEntidads(String pCodigoSectorEntidad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoSectorEntidadDAL.ConsultarTipoSectorEntidads(pCodigoSectorEntidad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
