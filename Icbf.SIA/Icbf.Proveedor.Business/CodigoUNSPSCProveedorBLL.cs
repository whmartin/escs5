﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    
    /// <summary>
    /// Clase de capa de negocio para la entidad Código UNSPSC de proveedor
    /// </summary>
    public class CodigoUNSPSCProveedorBLL
    {

        private CodigoUNSPSCProveedorDAL vCodigoUNSPSCProveedorDAL;
        public CodigoUNSPSCProveedorBLL()
        {
            vCodigoUNSPSCProveedorDAL = new CodigoUNSPSCProveedorDAL();
        }


        // <summary>
        /// Inserta una nueva entidad Códigos UNSPSC de proveedor
        /// </summary>
        /// <param name="pCodigoUNSPSCProveedorEntidad">Entidad Códigos UNSPSC de Proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int InsertarCodUNSPSProveedorCEntidad(CodigoUNSPSCProveedor pCodigoUNSPSCProveedorEntidad)
        {
            try
            {
                return vCodigoUNSPSCProveedorDAL.InsertarCodigoUNSPSCProveedorEntidad(pCodigoUNSPSCProveedorEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un Código UNSPSC de Proveedor
        /// </summary>
        /// <param name="pCodigoUNSPSCProveedor">Entidad Códigos UNSPSC de proveedor</param>
        /// <returns>Nùmero de registros afectados en tabla</returns>
        public int EliminarCodUNSPSCProveedor(CodigoUNSPSCProveedor pCodigoUNSPSCProveedor)
        {
            try
            {
                return vCodigoUNSPSCProveedorDAL.EliminarCodigoUNSPSCProveedor(pCodigoUNSPSCProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Código UNSPSC de un proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad </param>
        /// <returns>Entidad Código UNSPSC</returns>
        public List<CodigoUNSPSCProveedor> ConsultarCodigoUNSPSCProveedor(int pIdTercero)
        {
            try
            {
                return vCodigoUNSPSCProveedorDAL.ConsultarCodigoUNSPSCProveedor(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}