using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo R�gimen Tributario
    /// </summary>
    public class TipoRegimenTributarioBLL
    {
        private TipoRegimenTributarioDAL vTipoRegimenTributarioDAL;
        public TipoRegimenTributarioBLL()
        {
            vTipoRegimenTributarioDAL = new TipoRegimenTributarioDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo R�gimen Tributario</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.InsertarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo R�gimen Tributario</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ModificarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pTipoRegimenTributario">Entidad Tipo R�gimen Tributario</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoRegimenTributario(TipoRegimenTributario pTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.EliminarTipoRegimenTributario(pTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pIdTipoRegimenTributario">Identificador de una entidad Tipo R�gimen Tributario</param>
        /// <returns>Entidad Tipo R�gimen Tributario</returns>
        public Icbf.Proveedor.Entity.TipoRegimenTributario ConsultarTipoRegimenTributario(int pIdTipoRegimenTributario)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ConsultarTipoRegimenTributario(pIdTipoRegimenTributario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pCodigoRegimenTributario">C�digo en una entidad Tipo R�gimen Tributario</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo R�gimen Tributario</param>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Tipo R�gimen Tributario</param>
        /// <param name="pEstado">Estado en una entidad Tipo R�gimen Tributario</param>
        /// <returns>Lista de entidades Tipo R�gimen Tributario</returns>
        public List<TipoRegimenTributario> ConsultarTipoRegimenTributarios(String pCodigoRegimenTributario, String pDescripcion, int? pIdTipoPersona, Boolean? pEstado)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ConsultarTipoRegimenTributarios(pCodigoRegimenTributario, pDescripcion, pIdTipoPersona, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo R�gimen Tributario
        /// </summary>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Tipo R�gimen Tributario</param>
        /// <returns>Lista de entidades Tipo R�gimen Tributario</returns>
        public List<Icbf.Proveedor.Entity.TipoRegimenTributario> ConsultarTipoRegimenTributarioTipoPersona(int? pIdTipoPersona)
        {
            try
            {
                return vTipoRegimenTributarioDAL.ConsultarTipoRegimenTributarioTipoPersona(pIdTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
