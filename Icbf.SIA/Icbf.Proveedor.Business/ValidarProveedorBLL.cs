using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de validaci�n de proveedores
    /// </summary>
    public class ValidarProveedorBLL
    {
        private ValidarProveedorDAL vValidarProveedorDAL;
        public ValidarProveedorBLL()
        {
            vValidarProveedorDAL = new ValidarProveedorDAL();
        }

        /// <summary>
        /// Consulta una lista de entidades Validaci�n de Informaci�n
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <returns>Lista de entidades Validaci�n de Informaci�n</returns>
        public List<ValidarInfo> ConsultarValidarModulosResumen(Int32 pIdEntidad)
        {
            try
            {
                return vValidarProveedorDAL.ConsultarValidarModulosResumen(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Verifica M�dulo resumen
        /// </summary>
        /// <param name="pIdEntidad">Identificador de la entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ValidarModulosResumen_FinalizarRevision(Int32 pIdEntidad)
        { 
            try
            {
                return vValidarProveedorDAL.ValidarModulosResumen_FinalizarRevision(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
