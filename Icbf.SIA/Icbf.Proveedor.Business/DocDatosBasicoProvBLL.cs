using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de documentos adjuntos para m�dulo datos b�sicos de proveedores
    /// </summary>
    public class DocDatosBasicoProvBLL
    {
        private DocDatosBasicoProvDAL vDocDatosBasicoProvDAL;
        public DocDatosBasicoProvBLL()
        {
            vDocDatosBasicoProvDAL = new DocDatosBasicoProvDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                return vDocDatosBasicoProvDAL.InsertarDocDatosBasicoProv(pDocDatosBasicoProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                return vDocDatosBasicoProvDAL.ModificarDocDatosBasicoProv(pDocDatosBasicoProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pDocDatosBasicoProv">Entidad Documentos asociados al proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocDatosBasicoProv(DocDatosBasicoProv pDocDatosBasicoProv)
        {
            try
            {
                return vDocDatosBasicoProvDAL.EliminarDocDatosBasicoProv(pDocDatosBasicoProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una entidad Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la entidad Documentos asociados al proveedor</param>
        /// <returns>Entidad Documentos asociados al proveedor</returns>
        public DocDatosBasicoProv ConsultarDocDatosBasicoProv(int pIdDocAdjunto)
        {
            try
            {
                return vDocDatosBasicoProvDAL.ConsultarDocDatosBasicoProv(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador del tipo en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoSector">Tipo de Sector en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicoProv_IdEntidad_TipoPersona(int pIdEntidad, string pTipoPersona, string pTipoSector, string pIdTemporal, string pTipoEntidad)
        {
            try
            {
                return vDocDatosBasicoProvDAL.ConsultarDocDatosBasicoProv_IdEntidad_TipoPersona(pIdEntidad, pTipoPersona, pTipoSector, pIdTemporal, pTipoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta una lista de entidades Documentos asociados al proveedor
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en la entidad Documentos asociados al proveedor</param>
        /// <param name="pTipoSector">Tipo de Sector en la entidad Documentos asociados al proveedor</param>
        /// <returns>Lista de entidades Documentos asociados al proveedor</returns>
        public List<DocDatosBasicoProv> ConsultarDocDatosBasicoProv_IdTemporal_TipoPersona(string pdIdTemporal, string pTipoPersona, string pTipoSector, string pTipoEntidad)
        {
            try
            {
                return vDocDatosBasicoProvDAL.ConsultarDocDatosBasicoProv_IdTemporal_TipoPersona(pdIdTemporal, pTipoPersona, pTipoSector, pTipoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
