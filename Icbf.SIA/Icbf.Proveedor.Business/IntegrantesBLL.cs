using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Integrantes
    /// </summary>
    public class IntegrantesBLL
    {
        private IntegrantesDAL vIntegrantesDAL;
        public IntegrantesBLL()
        {
            vIntegrantesDAL = new IntegrantesDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int InsertarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                return vIntegrantesDAL.InsertarIntegrantes(pIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int ModificarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                return vIntegrantesDAL.ModificarIntegrantes(pIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Integrantes
        /// </summary>
        /// <param name="pIntegrantes"></param>
        public int EliminarIntegrantes(Integrantes pIntegrantes)
        {
            try
            {
                return vIntegrantesDAL.EliminarIntegrantes(pIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Integrantes
        /// </summary>
        /// <param name="pIdIntegrante"></param>
        public Integrantes ConsultarIntegrantes(int pIdIntegrante)
        {
            try
            {
                return vIntegrantesDAL.ConsultarIntegrantes(pIdIntegrante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Integrantes
        /// </summary>
        /// <param name="pIdTipoPersona"></param>
        /// <param name="pIdTipoIdentificacionPersonaNatural"></param>
        /// <param name="pNumeroIdentificacion"></param>
        /// <param name="pPorcentajeParticipacion"></param>
        /// <param name="pConfirmaCertificado"></param>
        /// <param name="pConfirmaPersona"></param>
        public List<Integrantes> ConsultarIntegrantess(int? pIdEntidad)
        {
            try
            {
                return vIntegrantesDAL.ConsultarIntegrantess(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Icbf.Proveedor.Entity.EntidadProvOferente ConsultarEntidadProvOferentes_SectorPrivado(String pTipoPersona, String pTipoidentificacion, String pIdentificacion, String pProveedor, String pEstado, String pUsuarioCrea)
        {
            try
            {
                return vIntegrantesDAL.ConsultarEntidadProvOferentes_SectorPrivado(pTipoPersona, pTipoidentificacion, pIdentificacion, pProveedor, pEstado, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método que valida el numero de integrantes registrados contra el numero de integrantes definido en datos básicos por el proveedor
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public Integrantes ValidaNumIntegrantes(int pIdEntidad)
        {
            try
            {
                return vIntegrantesDAL.ValidaNumIntegrantes(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
    }
}
