﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    public class ValidacionIntegrantesEntidadBLL
    {
        private ValidacionIntegrantesEntidadDAL vValidacionIntegrantesEntidadDAL;
        public ValidacionIntegrantesEntidadBLL()
        {
            vValidacionIntegrantesEntidadDAL = new ValidacionIntegrantesEntidadDAL();
        }

        /// <summary>
        /// Modifica una ValidacionIntegrantesEntidad
        /// </summary>
        /// <param name="pEntidadProvOferente">ValidacionIntegrantesEntidad</param>
        /// <returns>Identificador de ValidacionIntegrantesEntidad en tabla</returns>
        public int ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(ValidacionIntegrantesEntidad pValidacionIntegrantesEntidad)
        {
            try
            {
                return vValidacionIntegrantesEntidadDAL.ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(pValidacionIntegrantesEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// Consulta un ValidacionIntegrantesEntidad de una Entidad
        /// </summary>
        /// <param name="pIdEntidad">IdEntidad</param>
        /// <returns>ValidacionIntegrantesEntidad</returns>
        public ValidacionIntegrantesEntidad Consultar_ValidacionIntegrantesEntidad(int pIdEntidad)
        {
            try
            {
                return vValidacionIntegrantesEntidadDAL.Consultar_ValidacionIntegrantesEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// Finaliza un ValidacionIntegrantesEntidad de una Entidad
        /// </summary>
        /// <param name="pIdEntidad">IdEntidad</param>
        /// <returns>int</returns>
        public int FinalizaValidacionIntegrantesEntidad(int pIdEntidad)
        {
            try
            {
                return vValidacionIntegrantesEntidadDAL.FinalizaValidacionIntegrantesEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
