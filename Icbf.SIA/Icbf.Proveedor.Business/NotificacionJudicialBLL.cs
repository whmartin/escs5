using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Notificaci�n judicial
    /// </summary>
    public class NotificacionJudicialBLL
    {
        private NotificacionJudicialDAL vNotificacionJudicialDAL;
        public NotificacionJudicialBLL()
        {
            vNotificacionJudicialDAL = new NotificacionJudicialDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pNotificacionJudicial">Entidad Notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                return vNotificacionJudicialDAL.InsertarNotificacionJudicial(pNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pNotificacionJudicial">Entidad Notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                return vNotificacionJudicialDAL.ModificarNotificacionJudicial(pNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pNotificacionJudicial">Entidad Notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarNotificacionJudicial(NotificacionJudicial pNotificacionJudicial)
        {
            try
            {
                return vNotificacionJudicialDAL.EliminarNotificacionJudicial(pNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Notificaci�n judicial
        /// </summary>
        /// <param name="pIdNotJudicial">Identificador de una entidad Notificaci�n judicial</param>
        /// <param name="pIdMunicipio">Identificador de un municipio en una entidad Notificaci�n judicial</param>
        /// <param name="pIdZona">Identificador de una zona en una entidad Notificaci�n judicial</param>
        /// <param name="pDireccion">Identificador de una direcci�n en una entidad Notificaci�n judicial</param>
        /// <returns>Lista de entidades Notificaci�n judicial</returns>
        public NotificacionJudicial ConsultarNotificacionJudicial(int pIdNotJudicial, int pIdMunicipio, int pIdZona, int pDireccion)
        {
            try
            {
                return vNotificacionJudicialDAL.ConsultarNotificacionJudicial(pIdNotJudicial, pIdMunicipio, pIdZona, pDireccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Notificaci�n judicial
        /// </summary>
        /// <param name="pIdNotJudicial">Identificador de una entidad Notificaci�n judicial</param>
        /// <param name="pIdEntidad">Identificador de una entidad general en una entidad Notificaci�n judicial</param>
        /// <param name="pIdDepartamento">Identificador de una departamento en una entidad Notificaci�n judicial</param>
        /// <param name="pIdMunicipio">Identificador de un Municipio en una entidad Notificaci�n judicial</param>
        /// <returns>Lista de entidades Notificaci�n judicial</returns>
        public List<NotificacionJudicial> ConsultarNotificacionJudicials(int? pIdNotJudicial, int? pIdEntidad, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vNotificacionJudicialDAL.ConsultarNotificacionJudicials(pIdNotJudicial, pIdEntidad, pIdDepartamento, pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Notificaci�n judicial
        /// </summary>
        /// <param name="pIdEntidad">Identificador general en una entidad Notificaci�n judicial</param>
        /// <returns>Lista de entidades Notificaci�n judicial</returns>
        public List<NotificacionJudicial> ConsultarNotificacionJudicials(int? pIdEntidad)
        {
            try
            {
                return vNotificacionJudicialDAL.ConsultarNotificacionJudicials(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

       
    }
}
