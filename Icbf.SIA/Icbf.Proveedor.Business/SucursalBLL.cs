using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad sucursal
    /// </summary>
    public class SucursalBLL
    {
        private SucursalDAL vSucursalDAL;
        public SucursalBLL()
        {
            vSucursalDAL = new SucursalDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarSucursal(Sucursal pSucursal)
        {
            try
            {
                Validaciones(pSucursal);
                return vSucursalDAL.InsertarSucursal(pSucursal);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                throw ex;// GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarSucursal(Sucursal pSucursal)
        {
            try
            {
                Validaciones(pSucursal);
                return vSucursalDAL.ModificarSucursal(pSucursal);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                throw ex; // new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que Modifica la Sucursal Default
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns></returns>
        public int ModificarSucursalDefault(Sucursal pSucursal)
        {
            try
            {
                return vSucursalDAL.ModificarSucursalDefault(pSucursal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Elimina una entidad Sucursal
        /// </summary>
        /// <param name="pSucursal">Entidad Sucursal</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarSucursal(Sucursal pSucursal)
        {
            try
            {
                return vSucursalDAL.EliminarSucursal(pSucursal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Sucursal
        /// </summary>
        /// <param name="pIdSucursal">Identificador de una entidad Sucursal</param>
        /// <returns>Entidad Sucursal</returns>
        public Sucursal ConsultarSucursal(int pIdSucursal)
        {
            try
            {
                return vSucursalDAL.ConsultarSucursal(pIdSucursal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Sucursal
        /// </summary>
        /// <param name="pIdEntidad">Identificador general de una entidad Sucursal</param>
        /// <param name="pEstado">estado de una entidad Sucursal</param>
        /// <returns>Lista de entidades Sucursal</returns>
        public List<Sucursal> ConsultarSucursals(int? pIdEntidad, int? pEstado)
        {
            try
            {
                return vSucursalDAL.ConsultarSucursals(pIdEntidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Validaci�n del nombre de una sucursal por sus nombre
        /// </summary>
        /// <param name="sucursal"></param>
        private void Validaciones(Sucursal sucursal)
        {

            if (string.IsNullOrEmpty(sucursal.Nombre))
            {
                throw new Exception("Registre una sucursal");
            }

            if (!string.IsNullOrEmpty(sucursal.Extension.ToString()) && string.IsNullOrEmpty(sucursal.Indicativo.ToString()))
            {
                throw new Exception("Debe Ingresar un Indicativo");
            }

            if (!string.IsNullOrEmpty(sucursal.Indicativo.ToString()) && string.IsNullOrEmpty(sucursal.Telefono.ToString()))
            {
                throw new Exception("Registre un n�mero de tel�fono");
            }

            if (!string.IsNullOrEmpty(sucursal.Indicativo.ToString()))
            {
                if (sucursal.Indicativo == 0)
                {
                    throw new Exception("Indicativo debe tener un valor diferente de cero");
                }
            }

            if (!string.IsNullOrEmpty(sucursal.Telefono.ToString()))
            {
               if ((sucursal.Telefono.ToString().Trim().Length != 7))
                {
                    throw new Exception("Registre un n�mero de tel�fono v�lido");
                }
               if (string.IsNullOrEmpty(sucursal.Indicativo.ToString()))
               {
                   throw new Exception("Debe Ingresar un Indicativo");
               }
            }

            if (sucursal.Celular.ToString().Trim().Length == 0)
            {              
                throw new Exception("Registre un n�mero de celular");
            }
            if (sucursal.Celular.ToString().Length != 10)
            {
                throw new Exception("N�mero de celular no v�lido");
            }
            if (sucursal.Celular.ToString().Trim().Length==0 )
            {
                throw new Exception("Registre un correo electr�nico v�lido");
            }
            
            
        }
    }
}
