using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Documentos adjuntos de terceros
    /// </summary>
    public class DocAdjuntoTerceroBLL
    {
        private DocAdjuntoTerceroDAL vDocAdjuntoTerceroDAL;
        public DocAdjuntoTerceroBLL()
        {
            vDocAdjuntoTerceroDAL = new DocAdjuntoTerceroDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Entidad Documentos adjuntos al tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.InsertarDocAdjuntoTercero(pDocAdjuntoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Entidad Documentos adjuntos al tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ModificarDocAdjuntoTercero(pDocAdjuntoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Entidad Documentos adjuntos al tercero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocAdjuntoTercero(DocAdjuntoTercero pDocAdjuntoTercero)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.EliminarDocAdjuntoTercero(pDocAdjuntoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Identificador de entidad Documentos adjuntos al tercero</param>
        /// <returns>Entidad Documentos adjuntos al tercero</returns>
        public DocAdjuntoTercero ConsultarDocAdjuntoTercero(int pIdDocAdjunto)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ConsultarDocAdjuntoTercero(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pDocAdjuntoTercero">Identificador de entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdTercero">Identificador de Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdDocumento">Identificador del Documento en entidad Documentos adjuntos al tercero</param>
        /// <returns>Entidad Documentos adjuntos al tercero</returns>
        public DocAdjuntoTercero ConsultarDocAdjuntoTercero(int pIdDocAdjunto, int pIdTercero, int pIdDocumento)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ConsultarDocAdjuntoTercero(pIdDocAdjunto, pIdTercero, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdTercero">Identificador del Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pcodigoDocumento">C�digo del documento en entidad Documentos adjuntos al tercero</param>
        /// <param name="programa">Programa en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTercero(int? pIdTercero, string pcodigoDocumento, string programa)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ConsultarDocAdjuntoTercero(pIdTercero, pcodigoDocumento, programa);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdTercero">Identificador del Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pIdDocumento">Identificador del documento en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros(int? pIdDocAdjunto, int? pIdTercero, int? pIdDocumento)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ConsultarDocAdjuntoTerceros(pIdDocAdjunto, pIdTercero, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdTercero">Identificador del Tercero en entidad Documentos adjuntos al tercero</param>
        /// <param name="pTipoPersona">Tipo de persona en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(int pIdTercero, string pTipoPersona, string idTemporal)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ConsultarDocAdjuntoTercero_IdEntidad_TipoPersona(pIdTercero, pTipoPersona, idTemporal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades de Documentos adjuntos al tercero
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en entidad Documentos adjuntos al tercero</param>
        /// <param name="pTipoPersona">Tipo de persona en entidad Documentos adjuntos al tercero</param>
        /// <returns>Lista de entidades Documentos adjuntos al tercero</returns>
        public List<DocAdjuntoTercero> ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(string pIdTemporal, string pTipoPersona)
        {
            try
            {
                return vDocAdjuntoTerceroDAL.ConsultarDocAdjuntoTercero_IdTemporal_TipoPersona(pIdTemporal, pTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
