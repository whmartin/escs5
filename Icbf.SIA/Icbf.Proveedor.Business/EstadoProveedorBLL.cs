﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
//    class EstadoProveedorBLL
    /// <summary>
    /// Clase de capa de negocio para la entidad Estado Proveedor
    /// </summary>
    public class EstadoProveedorBLL
    {
        private EstadoProveedorDAL vEstadoProveedorDAL;
        public EstadoProveedorBLL()
        {
            vEstadoProveedorDAL = new EstadoProveedorDAL();
        }


        /// <summary>
        /// Consulta una entidad Estado Proveedor
        /// </summary>
        /// <param name="pDescripcion">Descripción del EstadoProveedor a Consultar</param>
        /// <returns>Entidad Estado Proveedor</returns>
        public EstadoProveedor ConsultarEstadoProveedor(string pDescripcion)
        {
            try
            {
                return vEstadoProveedorDAL.ConsultarEstadoProveedor(pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }

}
