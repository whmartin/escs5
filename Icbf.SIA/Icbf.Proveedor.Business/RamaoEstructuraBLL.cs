using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad rama o estructura
    /// </summary>
    public class RamaoEstructuraBLL
    {
        private RamaoEstructuraDAL vRamaoEstructuraDAL;
        public RamaoEstructuraBLL()
        {
            vRamaoEstructuraDAL = new RamaoEstructuraDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                return vRamaoEstructuraDAL.InsertarRamaoEstructura(pRamaoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                return vRamaoEstructuraDAL.ModificarRamaoEstructura(pRamaoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Entidad Rama Estructura</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarRamaoEstructura(RamaoEstructura pRamaoEstructura)
        {
            try
            {
                return vRamaoEstructuraDAL.EliminarRamaoEstructura(pRamaoEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Rama Estructura
        /// </summary>
        /// <param name="pRamaoEstructura">Identificador de una entidad Rama Estructura</param>
        /// <returns>Entidad Rama Estructura</returns>
        public RamaoEstructura ConsultarRamaoEstructura(int pIdRamaEstructura)
        {
            try
            {
                return vRamaoEstructuraDAL.ConsultarRamaoEstructura(pIdRamaEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Rama Estructura
        /// </summary>
        /// <param name="pCodigoRamaEstructura">C�digo de una entidad Rama Estructura</param>
        /// <param name="pDescripcion">Descripci�n de una entidad Rama Estructura</param>
        /// <param name="pEstado">Estado de una entidad Rama Estructura</param>
        /// <returns>Lista de entidades Rama Estructura</returns>
        public List<RamaoEstructura> ConsultarRamaoEstructuras(String pCodigoRamaEstructura, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vRamaoEstructuraDAL.ConsultarRamaoEstructuras(pCodigoRamaEstructura, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Rama Estructura
        /// </summary>
        /// <returns>Lista de entidades Rama Estructura</returns>
        public List<RamaoEstructura> ConsultarRamaoEstructurasAll()
        {
            try
            {
                return vRamaoEstructuraDAL.ConsultarRamaoEstructurasAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
