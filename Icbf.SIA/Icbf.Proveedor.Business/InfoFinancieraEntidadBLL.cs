using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Informaci�n financiera
    /// </summary>
    public class InfoFinancieraEntidadBLL
    {
        private InfoFinancieraEntidadDAL vInfoFinancieraEntidadDAL;
        
        public InfoFinancieraEntidadBLL()
        {
            vInfoFinancieraEntidadDAL = new InfoFinancieraEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad, string pTipoPersona, string pTipoSector)
        {
            try
            {
                Validaciones(pInfoFinancieraEntidad);

                ValidarDocumentosObligatorios(pInfoFinancieraEntidad, pTipoPersona, pTipoSector);
                return vInfoFinancieraEntidadDAL.InsertarInfoFinancieraEntidad(pInfoFinancieraEntidad);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                 //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modificar una entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Informaci�n m�dulo financero</param>
        /// <returns>Identificador de la entidad Informaci�n m�dulo financero en tabla</returns>
        public int ModificarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad, string pTipopersona, string pTipoSector)
        {
            try
            {
                Validaciones(pInfoFinancieraEntidad);
                //ValidarDocumentosObligatorios(pInfoFinancieraEntidad, pTipopersona,pTipoSector);
                return vInfoFinancieraEntidadDAL.ModificarInfoFinancieraEntidad(pInfoFinancieraEntidad);
            }
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                 ////throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Eliminar una entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Identificador de una entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarInfoFinancieraEntidad(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            try
            {
                return vInfoFinancieraEntidadDAL.EliminarInfoFinancieraEntidad(pInfoFinancieraEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pIdInfoFin">Identificador de una entidad Informaci�n m�dulo financero</param>
        /// <returns>Entidad Informaci�n m�dulo financero</returns>
        private bool ConsultarVigencia(InfoFinancieraEntidad pInfoFinancieraEntidad)
        {
            List<InfoFinancieraEntidad> lista = vInfoFinancieraEntidadDAL.ConsultarInfoFinancieraEntidads(pInfoFinancieraEntidad.IdEntidad,null);
            foreach (InfoFinancieraEntidad info in lista)
            {
                if (pInfoFinancieraEntidad.IdInfoFin != info.IdInfoFin  &&  info.IdVigencia == pInfoFinancieraEntidad.IdVigencia)
                {
                    return true; //Existe y no es posible crearse
                }
            }

            return false;
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n m�dulo financero
        /// </summary>
        /// <param name="pIdEntidad">Identificador general en una entidad Informaci�n m�dulo financero</param>
        /// <param name="pIdVigencia">Identificador de vigencia en una entidad Informaci�n m�dulo financero</param>
        /// <returns>Lista de entidades Informaci�n m�dulo financero</returns>
        public bool ValidaVigencia(int IdEntidad, int IdVigencia)
        {
            List<InfoFinancieraEntidad> lista = vInfoFinancieraEntidadDAL.ConsultarInfoFinancieraEntidads(IdEntidad,IdVigencia);
            if (lista.Count == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Valida Informaci�n financiera
        /// </summary>
        /// <param name="pInfoFinancieraEntidad"></param>
        private void Validaciones(InfoFinancieraEntidad pInfoFinancieraEntidad)
        { 
             //try
            //{
            
                if (pInfoFinancieraEntidad.IdVigencia==-1)
                {
                    throw new Exception("Debe seleccionar una vigencia ");
                }

                if (ConsultarVigencia(pInfoFinancieraEntidad))
                {
                   throw new Exception("Vigencia ya ha sido registrada para este proveedor");
                }

                if (pInfoFinancieraEntidad.ActivoTotal==null || pInfoFinancieraEntidad.ActivoTotal == 0)
                {
                    throw new Exception("Debe registrar el activo total a esta vigencia");
                }                

                if (pInfoFinancieraEntidad.PasivoTotal  == null)
                {
                    throw new Exception("Debe registrar el pasivo total a esta vigencia");
                }

                if (pInfoFinancieraEntidad.ActivoCte == null ||  pInfoFinancieraEntidad.ActivoCte == 0)
                {
                    throw new Exception("Debe registrar el Activo corriente a esta vigencia");
                }
                 
                if (pInfoFinancieraEntidad.ActivoCte > pInfoFinancieraEntidad.ActivoTotal)
                {
                    throw new Exception("El activo corriente debe ser inferior o igual que el activo total.");
                }

                if (pInfoFinancieraEntidad.PasivoCte > pInfoFinancieraEntidad.PasivoTotal)
                {
                    throw new Exception("El pasivo corriente debe ser inferior o igual que el pasivo total.");
                }

                //if (pInfoFinancieraEntidad.PasivoCte == null || pInfoFinancieraEntidad.PasivoCte == 0)
                //{
                //    throw new Exception("Debe registrar el Pasivo corriente a esta vigencia");
                //}

                if (pInfoFinancieraEntidad.Patrimonio == null || pInfoFinancieraEntidad.Patrimonio == 0)
                {
                    throw new Exception("Debe registrar el patrimonio a esta vigencia");
                }

                if (pInfoFinancieraEntidad.ActivoTotal != pInfoFinancieraEntidad.Patrimonio + pInfoFinancieraEntidad.PasivoTotal)
                {
                    throw new Exception("Ecuaci�n contable no v�lida, verifique el valor de las cuentas. Tenga en cuenta la ecuaci�n: [Activo total = Pasivo total + Patrimonio]");
                }

                //if (pInfoFinancieraEntidad.UtilidadOperacional == null || pInfoFinancieraEntidad.UtilidadOperacional == 0)
                //{
                //    throw new Exception("Debe registrar la utilidad operacional");

                //}

                //if (pInfoFinancieraEntidad.GastosInteresFinancieros == null || pInfoFinancieraEntidad.GastosInteresFinancieros == 0)
                //{
                //    throw new Exception("Debe ingresar los gastos de intereses");
                //}

                //if (pInfoFinancieraEntidad.GastosInteresFinancieros >= pInfoFinancieraEntidad.UtilidadOperacional)
                //{
                //    throw new Exception("Los gastos de intereses debe ser menor que la utilidad operacional");
                //}

                if (pInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros == null || !pInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros)
                {
                    throw new Exception("Debe confirmar los indicadores financieros");
                }

               
            //}
            //catch (UserInterfaceException)
            //{
            //    throw;
            //}
            //catch (Exception ex)
            //{
            //     throw new GenericException(ex);
            //}
            
        }

        /// <summary>
        /// Valida documento oblogatorio
        /// </summary>
        /// <param name="pInfoFinancieraEntidad"></param>
        /// <param name="pTipoPersona"></param>
        /// <param name="pTipoSector"></param>
        public void ValidarDocumentosObligatorios(InfoFinancieraEntidad pInfoFinancieraEntidad, string pTipoPersona, string pTipoSector)
        {
            DocFinancieraProvBLL vDocFinancieraProvBLL = new DocFinancieraProvBLL();
            List<DocFinancieraProv> documentosFinancieros = new List<DocFinancieraProv>();

            if (pInfoFinancieraEntidad.IdInfoFin>0)
                documentosFinancieros = vDocFinancieraProvBLL.ConsultarDocFinancieraProv_IdInfoFin_Rup(pInfoFinancieraEntidad.IdInfoFin, pInfoFinancieraEntidad.RupRenovado ? "1" : "0", pTipoPersona, pTipoSector);
            else
                documentosFinancieros = vDocFinancieraProvBLL.ConsultarDocFinancieraProv_IdTemporal_Rup(pInfoFinancieraEntidad.IdTemporal, pInfoFinancieraEntidad.RupRenovado ? "1" : "0", pTipoPersona, pTipoSector);
            
            string mensaje = string.Empty;
            foreach (DocFinancieraProv doc in documentosFinancieros)
            {
                if (doc.Obligatorio == 1 && string.IsNullOrEmpty(doc.LinkDocumento))
                {
                    mensaje += (string.Format("Falta adjuntar el documento {0}. ", doc.NombreDocumento));
                }
            }

            if (!string.IsNullOrEmpty(mensaje))
            {
                throw new Exception(mensaje);
            }
        }

        /// <summary>
        /// Consulta informaci�n de la Entidad Informaci�n financiera
        /// </summary>
        /// <param name="pIdInfoFin"></param>
        /// <returns></returns>
        public InfoFinancieraEntidad ConsultarInfoFinancieraEntidad(int pIdInfoFin)
        {
            try
            {
                return vInfoFinancieraEntidadDAL.ConsultarInfoFinancieraEntidad(pIdInfoFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n financiera
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <returns></returns>
        public List<InfoFinancieraEntidad> ConsultarInfoFinancieraEntidads(int? pIdEntidad)
        {
            try
            {
                return vInfoFinancieraEntidadDAL.ConsultarInfoFinancieraEntidads(pIdEntidad, null);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n m�dulo financero por liberar
        /// </summary>
        /// <param name="pInfoFinancieraEntidad">Entidad Informaci�n m�dulo financero</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarInfoFinancieraEntidad_Liberar(InfoFinancieraEntidad vInfoFinanciera)
        {
            try
            {
                return vInfoFinancieraEntidadDAL.ModificarInfoFinancieraEntidad_Liberar(vInfoFinanciera);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza InfoFinancieraEntidad
        /// </summary>
        /// <param name="pIdEntidad"> Id Entidad Proveedores Oferente</param>
        public int FinalizaInfoFinancieraEntidad(int pIdEntidad)
        {
            try
            {
                return vInfoFinancieraEntidadDAL.FinalizaInfoFinancieraEntidad(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
