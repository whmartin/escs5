using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    ///  Clase de capa de negocio para la entidad nivel organizacional
    /// </summary>
    public class NivelOrganizacionalBLL
    {
        private NivelOrganizacionalDAL vNivelOrganizacionalDAL;
        public NivelOrganizacionalBLL()
        {
            vNivelOrganizacionalDAL = new NivelOrganizacionalDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalDAL.InsertarNivelOrganizacional(pNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalDAL.ModificarNivelOrganizacional(pNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Entidad Nivel organizacional</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarNivelOrganizacional(NivelOrganizacional pNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalDAL.EliminarNivelOrganizacional(pNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Nivel organizacional
        /// </summary>
        /// <param name="pNivelOrganizacional">Identificador de una entidad Nivel organizacional</param>
        /// <returns>Entidad Nivel organizacional</returns>
        public NivelOrganizacional ConsultarNivelOrganizacional(int pIdNivelOrganizacional)
        {
            try
            {
                return vNivelOrganizacionalDAL.ConsultarNivelOrganizacional(pIdNivelOrganizacional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional
        /// </summary>
        /// <param name="pCodigoNivelOrganizacional">C�digo en una entidad Nivel organizacional</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Nivel organizacional</param>
        /// <param name="pEstado">Estado en una entidad Nivel organizacional</param>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionals(String pCodigoNivelOrganizacional, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vNivelOrganizacionalDAL.ConsultarNivelOrganizacionals(pCodigoNivelOrganizacional, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional
        /// </summary>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacionalAll()
        {
            try
            {
                return vNivelOrganizacionalDAL.ConsultarNivelOrganizacionalAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel organizacional de acuero a IdRama y IdNivelGob
        /// </summary>
        /// <returns>Lista de entidades Nivel organizacional</returns>
        public List<NivelOrganizacional> ConsultarNivelOrganizacional_PorRamaYNivelGobierno(int idRama, int idNivelGob)
        {
            try
            {
                return vNivelOrganizacionalDAL.ConsultarNivelOrganizacionals_PorRamaYNivelGobierno(idRama,idNivelGob);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
