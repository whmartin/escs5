using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Estado validaci�n Documental
    /// </summary>
    public class EstadoValidacionDocumentalBLL
    {
        private EstadoValidacionDocumentalDAL vEstadoValidacionDocumentalDAL;
        public EstadoValidacionDocumentalBLL()
        {
            vEstadoValidacionDocumentalDAL = new EstadoValidacionDocumentalDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validaci�n Documental</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalDAL.InsertarEstadoValidacionDocumental(pEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validaci�n Documental</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalDAL.ModificarEstadoValidacionDocumental(pEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pEstadoValidacionDocumental">Entidad Estado validaci�n Documental</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEstadoValidacionDocumental(EstadoValidacionDocumental pEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalDAL.EliminarEstadoValidacionDocumental(pEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Estado validaci�n Documental
        /// </summary>
        /// <param name="pIdEstadoValidacionDocumental">Identificador de la entidad Estado validaci�n Documental</param>
        /// <returns>Entidad Estado validaci�n Documental</returns>
        public EstadoValidacionDocumental ConsultarEstadoValidacionDocumental(int pIdEstadoValidacionDocumental)
        {
            try
            {
                return vEstadoValidacionDocumentalDAL.ConsultarEstadoValidacionDocumental(pIdEstadoValidacionDocumental);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Estado validaci�n Documental
        /// </summary>
        /// <param name="pCodigoEstadoValidacionDocumental">C�digo de estado de validaci�n en una entidad Estado validaci�n Documental</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Estado validaci�n Documental</param>
        /// <param name="pEstado">Estado de una entidad Estado validaci�n Documental</param>
        /// <returns>Lista de entidades Estado validaci�n Documental</returns>
        public List<EstadoValidacionDocumental> ConsultarEstadoValidacionDocumentals(String pCodigoEstadoValidacionDocumental, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vEstadoValidacionDocumentalDAL.ConsultarEstadoValidacionDocumentals(pCodigoEstadoValidacionDocumental, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
