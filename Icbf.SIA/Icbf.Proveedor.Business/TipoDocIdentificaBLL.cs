using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo documento identificaci�n
    /// </summary>
    public class TipoDocIdentificaBLL
    {
        private TipoDocIdentificaDAL vTipoDocIdentificaDAL;
        public TipoDocIdentificaBLL()
        {
            vTipoDocIdentificaDAL = new TipoDocIdentificaDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificaci�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaDAL.InsertarTipoDocIdentifica(pTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificaci�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaDAL.ModificarTipoDocIdentifica(pTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pTipoDocIdentifica">Entidad Tipo Identificaci�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoDocIdentifica(TipoDocIdentifica pTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaDAL.EliminarTipoDocIdentifica(pTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Identificaci�n
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Identificador en una entidad Tipo Identificaci�n</param>
        /// <returns>Entidad Tipo Identificaci�n</returns>
        public TipoDocIdentifica ConsultarTipoDocIdentifica(int pIdTipoDocIdentifica)
        {
            try
            {
                return vTipoDocIdentificaDAL.ConsultarTipoDocIdentifica(pIdTipoDocIdentifica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Identificaci�n
        /// </summary>
        /// <param name="pCodigoDocIdentifica">C�digo en una entidad Tipo Identificaci�n</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Identificaci�n</param>
        /// <returns>Lista de entidades Tipo Identificaci�n</returns>
        public List<TipoDocIdentifica> ConsultarTipoDocIdentificas(Decimal? pCodigoDocIdentifica, String pDescripcion)
        {
            try
            {
                return vTipoDocIdentificaDAL.ConsultarTipoDocIdentificas(pCodigoDocIdentifica, pDescripcion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Identificaci�n
        /// </summary>
        /// <returns>Lista de entidades Tipo Identificaci�n</returns>
        public List<TipoDocIdentifica> ConsultarTipoDocIdentificaAll()
        {
            try
            {
                return vTipoDocIdentificaDAL.ConsultarTipoDocIdentificaAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene los tipos de documentos a trav�s del IDdel Documento y el Id del programa
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Identificador en una entidad Tipo Identificaci�n</param>
        /// <param name="idPrograma">Identificador del programa</param>
        /// <returns>Entidad Tipo Identificaci�n</returns>
        public TipoDocIdentifica ConsultarTipoDocIdentificaByIdPrograma(int pIdTipoDocIdentifica, string idPrograma)
        {
            try
            {
                return vTipoDocIdentificaDAL.ConsultarTipoDocIdentificaByIdPrograma(pIdTipoDocIdentifica, idPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
    }
}
