using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{

    /// <summary>
    /// Clase de capa de negocio para la entidad de proveedores
    /// </summary>
    public class EntidadProvOferenteBLL
    {
        private EntidadProvOferenteDal vEntidadProvOferenteDAL;
        public EntidadProvOferenteBLL()
        {
            vEntidadProvOferenteDAL = new EntidadProvOferenteDal();
        }

        /// <summary>
        /// Inserta una nueva entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Identificador de la entidad Proveedores Oferente en tabla</returns>
        public int InsertarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.InsertarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.ModificarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>Identificador de la entidad Proveedores Oferente en tabla</returns>
        public int ModificarEntidadProvOferente_EstadoDocumental(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.ModificarEntidadProvOfernte_EstadoDocumental(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// Consulta una lista de entidades Proveedores Oferente Municipio
        /// </summary>
        /// <param name="pIdTercero">Id Tercero</param>
        /// <returns>Lista de sucursa�es asociadas a los Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarSucursales_EntidadProvOferentes_Municipio(int? pIdTercero, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarSucursales_EntidadProvOferentes_Municipio(pIdTercero, pIdDepartamento, pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pEntidadProvOferente">Entidad Proveedores Oferente</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarEntidadProvOferente(EntidadProvOferente pEntidadProvOferente)
        {
            try
            {
                return vEntidadProvOferenteDAL.EliminarEntidadProvOferente(pEntidadProvOferente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>Entidad Proveedores Oferente</returns>
        public EntidadProvOferente ConsultarEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferente(pIdEntidad); 
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes()
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferentes();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pTipoidentificacion">Tipo de identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pIdentificacion">Identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor">Proveedor en una entidad Proveedores Oferente</param>
        /// <param name="pEstado">Estado en una entidad Proveedores Oferente</param>
        /// <param name="pUsuarioCrea">Usuario que creaci�n para una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes(String pTipoPersona, String pTipoidentificacion, String pIdentificacion, String pProveedor, String pEstado, String pUsuarioCrea)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferentes(pTipoPersona, pTipoidentificacion, pIdentificacion, pProveedor, pEstado, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pIdTercero">Id Tercero</param>
        /// <returns>Lista de sucursa�es asociadas a los Proveedores Oferente</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarSucursales_EntidadProvOferentes(int? pIdTercero, int? pIdDepartamento, int? pIdMunicipio)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarSucursales_EntidadProvOferentes(pIdTercero, pIdDepartamento,pIdMunicipio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Proveedores Oferente
        /// </summary>
        /// <param name="pIdEstado">Identificador del Estado en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoPersona">Identificador del tipo de persona en una entidad Proveedores Oferente</param>
        /// <param name="pIDTIPODOCIDENTIFICA">Identificador del tipo de documento de identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pNUMEROIDENTIFICACION">N�mero de identificaci�n en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoCiiuPrincipal">Identificador del tipo de Ciiu principal en una entidad Proveedores Oferente</param>
        /// <param name="pIdMunicipioDirComercial">Identificador del municipio en una entidad Proveedores Oferente</param>
        /// <param name="pIdDepartamentoDirComercial">Identificador del departamento en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoSector">Identificador del tiepo de sector en una entidad Proveedores Oferente</param>
        /// <param name="pIdTipoRegimenTributario">Identificador del tipo de r�gimen tributario en una entidad Proveedores Oferente</param>
        /// <param name="pProveedor"> en una entidad Proveedores Oferente</param>
        /// <returns>Lista de entidades Proveedores Oferente</returns>
        public List<EntidadProvOferente> ConsultarEntidadProvOferentes_Validar(String pIdEstado, String pIdTipoPersona,
            String pIDTIPODOCIDENTIFICA, String pNUMEROIDENTIFICACION, String pIdTipoCiiuPrincipal, String pIdMunicipioDirComercial,
            String pIdDepartamentoDirComercial, String pIdTipoSector, String pIdTipoRegimenTributario, String pProveedor)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEntidadProvOferentes_Validar(pIdEstado, pIdTipoPersona,
            pIDTIPODOCIDENTIFICA, pNUMEROIDENTIFICACION, pIdTipoCiiuPrincipal, pIdMunicipioDirComercial,
            pIdDepartamentoDirComercial, pIdTipoSector, pIdTipoRegimenTributario, pProveedor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Env�a correo electr�nico por inconsistencia en una entidad Proveedores Oferente
        /// </summary>
        /// <param name="idEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EnviarMailsInconsistenciasProveedor(Int32 idEntidad, string pAsunto)
        {
            try
            {
                return vEntidadProvOferenteDAL.EnviarMailsInconsistenciasProveedor(idEntidad, pAsunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Estructura el correo electr�nico a enviar por inconsistencia en una entidad Proveedores Oferente
        /// </summary>
        /// <param name="idEntidad">Identificador de una entidad Proveedores Oferente</param>
        /// <returns>String Mail Body HTML</returns>
        //public string[] EstructurarMailsInconsistenciasProveedor(Int32 idEntidad)
        //{
        //    try
        //    {
        //        return vEntidadProvOferenteDAL.EstructurarMailsInconsistenciasProveedor(idEntidad);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}
        ///// <summary>
        /// Consulta una lista de entidades Estado Proveedor
        /// </summary>
        /// <param name="pEstado">Estado en una entidad Estado Proveedor</param>
        /// <returns>Lista de entidades Estado Proveedor</returns>
        public List<Icbf.Proveedor.Entity.EntidadProvOferente> ConsultarEstadoProveedor(Boolean? pEstado)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarEstadoProveedor(pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza una entidad Proveedores Oferente
        /// </summary>
        /// <param name="pIdEntidad"> Id Entidad Proveedores Oferente</param>
        public int FinalizaEntidadProvOferente(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteDAL.FinalizaEntidadProvOferente(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Consultar Fecha de Migraci�n
        /// </summary>
        /// <param name="IdEntidad">ID Entidad Proveedores Oferente</param>
        /// <returns>Fecha de MIgracion</returns>
        public DateTime GetFechaMigracion(int IdEntidad)
        {
            try
            {
                return vEntidadProvOferenteDAL.GetFechaMigracion(IdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Obtiene la auditoria de acciones para una EntidadProvOferente
        /// </summary>
        /// <param name="IdEntidad">IdEntidad</param>
        public List<AuditoriaAccionesEntidad> GetAuditoriaAccionesEntidad(int pIdEntidad)
        {
            try
            {
                return vEntidadProvOferenteDAL.GetAuditoriaAccionesEntidad(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idproveedorContrato"></param>
        /// <returns></returns>
        public List<Oferente.Entity.Tercero> ConsultarHistoricoRepresentanteLegal(int idproveedorContrato)
        {
            try
            {
                return vEntidadProvOferenteDAL.ConsultarHistoricoRepresentanteLegal(idproveedorContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
