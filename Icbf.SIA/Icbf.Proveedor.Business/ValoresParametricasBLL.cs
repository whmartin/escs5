using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  de valores asociados a las tablas param�tricas
    /// </summary>
    public class ValoresParametricasBLL
    {
        private ValoresParametricasDAL vValoresParametricasDAL;
        public ValoresParametricasBLL()
        {
            vValoresParametricasDAL = new ValoresParametricasDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pValoresParametricas">Entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValoresParametricas(ValoresParametricas pValoresParametricas)
        {
            try
            {
                return vValoresParametricasDAL.InsertarValoresParametricas(pValoresParametricas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pValoresParametricas">Entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarValoresParametricas(ValoresParametricas pValoresParametricas)
        {
            try
            {
                return vValoresParametricasDAL.ModificarValoresParametricas(pValoresParametricas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pValoresParametricas">Entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarValoresParametricas(ValoresParametricas pValoresParametricas)
        {
            try
            {
                return vValoresParametricasDAL.EliminarValoresParametricas(pValoresParametricas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pIdValorParametrica">Identificador en una entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>Entidad Valores asociados a las tablas param�tricas</returns>
        public ValoresParametricas ConsultarValoresParametricas(int pIdValorParametrica)
        {
            try
            {
                return vValoresParametricasDAL.ConsultarValoresParametricas(pIdValorParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Valores asociados a las tablas param�tricas
        /// </summary>
        /// <param name="pIdListaParametrica">Identificador de la lista param�trica</param>
        /// <param name="pCodigoValorParametrica">C�digo en una lista param�trica</param>
        /// <param name="pEstado">Estado de la entidad Valores asociados a las tablas param�tricas</param>
        /// <returns>Lista de entidades Valores asociados a las tablas param�tricas</returns>
        public List<ValoresParametricas> ConsultarValoresParametricass(int? pIdListaParametrica, String pCodigoValorParametrica, Boolean? pEstado)
        {
            try
            {
                return vValoresParametricasDAL.ConsultarValoresParametricass(pIdListaParametrica, pCodigoValorParametrica, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
