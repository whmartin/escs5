using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Tipo entidad
    /// </summary>
    public class TipoentidadBLL
    {
        private TipoentidadDAL vTipoentidadDAL;
        public TipoentidadBLL()
        {
            vTipoentidadDAL = new TipoentidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                return vTipoentidadDAL.InsertarTipoentidad(pTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                return vTipoentidadDAL.ModificarTipoentidad(pTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                // throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Entidad
        /// </summary>
        /// <param name="pTipoentidad">Entidad Tipo Entidad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoentidad(Tipoentidad pTipoentidad)
        {
            try
            {
                return vTipoentidadDAL.EliminarTipoentidad(pTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Entidad
        /// </summary>
        /// <param name="pIdTipoentidad">Identificador en una entidad Tipo Entidad</param>
        /// <returns>Entidad Tipo Entidad</returns>
        public Tipoentidad ConsultarTipoentidad(int pIdTipoentidad)
        {
            try
            {
                return vTipoentidadDAL.ConsultarTipoentidad(pIdTipoentidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad
        /// </summary>
        /// <param name="pCodigoTipoentidad">C�digo en una entidad Tipo Entidad</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Entidad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Entidad</param>
        /// <returns>Lista de entidades Tipo Entidad</returns>
        public List<Tipoentidad> ConsultarTipoentidads(String pCodigoTipoentidad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoentidadDAL.ConsultarTipoentidads(pCodigoTipoentidad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad</returns>
        public List<Tipoentidad> ConsultarTipoentidadsAll(int? pIdTipoPersona, int? pIdSector)
        {
            try
            {
                return vTipoentidadDAL.ConsultarTodosTipoentidadAll(pIdTipoPersona, pIdSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
     
    }
}
