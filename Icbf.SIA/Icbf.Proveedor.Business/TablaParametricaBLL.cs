using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  tabla param�trica
    /// </summary>
    public class TablaParametricaBLL
    {
        private TablaParametricaDAL vTablaParametricaDAL;
        public TablaParametricaBLL()
        {
            vTablaParametricaDAL = new TablaParametricaDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tabla param�trica
        /// </summary>
        /// <param name="pTablaParametrica">Entidad Tabla param�trica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.InsertarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tabla param�trica
        /// </summary>
        /// <param name="pTablaParametrica">Entidad Tabla param�trica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.ModificarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tabla param�trica
        /// </summary>
        /// <param name="pTablaParametrica">Entidad Tabla param�trica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTablaParametrica(TablaParametrica pTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.EliminarTablaParametrica(pTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tabla param�trica
        /// </summary>
        /// <param name="pIdTablaParametrica">Identificador de una entidad Tabla param�trica</param>
        /// <returns>Entidad Tabla param�trica</returns>
        public TablaParametrica ConsultarTablaParametrica(int pIdTablaParametrica)
        {
            try
            {
                return vTablaParametricaDAL.ConsultarTablaParametrica(pIdTablaParametrica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tabla param�trica
        /// </summary>
        /// <param name="pCodigoTablaParametrica">C�digo de una entidad Tabla param�trica</param>
        /// <param name="pNombreTablaParametrica">Nombre de una entidad Tabla param�trica</param>
        /// <param name="pEstado">Estado de una entidad Tabla param�trica</param>
        /// <returns>Lista de entidades Tabla param�trica</returns>
        public List<TablaParametrica> ConsultarTablaParametricas(String pCodigoTablaParametrica, String pNombreTablaParametrica, Boolean? pEstado)
        {
            try
            {
                return vTablaParametricaDAL.ConsultarTablaParametricas(pCodigoTablaParametrica, pNombreTablaParametrica, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
