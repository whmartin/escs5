using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  de documentos adjuntos a m�dulo notificaci�n judicial
    /// </summary>
    public class DocNotificacionJudicialBLL
    {
        private DocNotificacionJudicialDAL vDocNotificacionJudicialDAL;
        public DocNotificacionJudicialBLL()
        {
            vDocNotificacionJudicialDAL = new DocNotificacionJudicialDAL();
        }
        /// <summary>
        /// Inserta una nueva entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                return vDocNotificacionJudicialDAL.InsertarDocNotificacionJudicial(pDocNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                return vDocNotificacionJudicialDAL.ModificarDocNotificacionJudicial(pDocNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pDocNotificacionJudicial">Entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocNotificacionJudicial(DocNotificacionJudicial pDocNotificacionJudicial)
        {
            try
            {
                return vDocNotificacionJudicialDAL.EliminarDocNotificacionJudicial(pDocNotificacionJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pIdDocNotJudicial">Identificador en entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="pIdEntidad">Identificador en entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="pIdDocumento">Identificador de documento en entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>Entidad Documentos asociados a notificaci�n judicial</returns>
        public DocNotificacionJudicial ConsultarDocNotificacionJudicial(int pIdDocNotJudicial, int pIdEntidad, int pIdDocumento)
        {
            try
            {
                return vDocNotificacionJudicialDAL.ConsultarDocNotificacionJudicial(pIdDocNotJudicial, pIdEntidad, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="pIdDocNotJudicial">Identifidor de una entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="IdNotJudicial">Identificador de una notificaci�n en una entidad Documentos asociados a notificaci�n judicial</param>
        /// <param name="pIdDocumento">Identificador de un documento en una entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>Lista de entidades Documentos asociados a notificaci�n judicial</returns>
        public List<DocNotificacionJudicial> ConsultarDocNotificacionJudicials(int? pIdDocNotJudicial, int? IdNotJudicial, int? pIdDocumento)
        {
            try
            {
                return vDocNotificacionJudicialDAL.ConsultarDocNotificacionJudicials(pIdDocNotJudicial, IdNotJudicial, pIdDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos asociados a notificaci�n judicial
        /// </summary>
        /// <param name="IdNotJudicial">Identificador de una notificaci�n en una entidad Documentos asociados a notificaci�n judicial</param>
        /// <returns>Lista de entidades Documentos asociados a notificaci�n judicial</returns>
        public List<DocNotificacionJudicial> ConsultarDocNotificacionJudicials_Consultar_IdNotJudicial(int? IdNotJudicial)
        {
            try
            {
                return vDocNotificacionJudicialDAL.ConsultarDocNotificacionJudicials_Consultar_IdNotJudicial( IdNotJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
