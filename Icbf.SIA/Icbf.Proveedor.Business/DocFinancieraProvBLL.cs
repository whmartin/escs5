using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    ///Clase de capa de negocio para la entidad de documentos adjuntos de m�dulo financiero
    /// </summary>
    public class DocFinancieraProvBLL
    {
        private DocFinancieraProvDAL vDocFinancieraProvDAL;
        public DocFinancieraProvBLL()
        {
            vDocFinancieraProvDAL = new DocFinancieraProvDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                return vDocFinancieraProvDAL.InsertarDocFinancieraProv(pDocFinancieraProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                return vDocFinancieraProvDAL.ModificarDocFinancieraProv(pDocFinancieraProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pDocFinancieraProv">Entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocFinancieraProv(DocFinancieraProv pDocFinancieraProv)
        {
            try
            {
                return vDocFinancieraProvDAL.EliminarDocFinancieraProv(pDocFinancieraProv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Entidad Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public DocFinancieraProv ConsultarDocFinancieraProv(int pIdDocAdjunto)
        {
            try
            {
                return vDocFinancieraProvDAL.ConsultarDocFinancieraProv(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de la informaci�n financiera en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pRupRenovado">Rup renovado en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProv_IdInfoFin_Rup(int pIdInfoFin, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocFinancieraProvDAL.ConsultarDocFinancieraProv_IdInfoFin_Rup(pIdInfoFin, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor
        /// </summary>
        /// <param name="pIdTemporal">Identificador del Temporal en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pRupRenovado">Rup renovado en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos en la informaci�n financiera del proveedor</param>
        /// <returns>Lista de entidades Documentos adjuntos en la informaci�n financiera del proveedor</returns>
        public List<DocFinancieraProv> ConsultarDocFinancieraProv_IdTemporal_Rup(string pIdTemporal, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocFinancieraProvDAL.ConsultarDocFinancieraProv_IdTemporal_Rup(pIdTemporal, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
