using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  de validaci�n de terceros
    /// </summary>
    public class ValidarTerceroBLL
    {
        private ValidarTerceroDAL vValidarTerceroDAL;
        public ValidarTerceroBLL()
        {
            vValidarTerceroDAL = new ValidarTerceroDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad validaci�n de terceros
        /// </summary>
        /// <param name="pValidarTercero">Entidad validaci�n de terceros</param>
        /// <param name="idEstadoTercero">Identificador del estado</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValidarTercero(ValidarTercero pValidarTercero, int idEstadoTercero)
        {
            try
            {
                return vValidarTerceroDAL.InsertarValidarTercero(pValidarTercero, idEstadoTercero );               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de terceros
        /// </summary>
        /// <param name="pIdValidarTercero">Identificador en una entidad validaci�n de terceros</param>
        /// <returns>Entidad validaci�n de terceros</returns>
        public ValidarTercero ConsultarValidarTercero(int pIdValidarTercero)
        {
            try
            {
                return vValidarTerceroDAL.ConsultarValidarTercero(pIdValidarTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de terceros
        /// </summary>
        /// <param name="pIdTercero">Identificador de una entidad validaci�n de terceros</param>
        /// <returns>Entidad validaci�n de terceros</returns>
        public ValidarTercero ConsultarValidarTercero_Tercero(int pIdTercero)
        {
            try
            {
                return vValidarTerceroDAL.ConsultarValidarTercero_Tercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validaci�n de terceros
        /// </summary>
        /// <param name="pIdTercero">Identificador del tercero</param>
        /// <param name="pObservaciones">Observaciones en una entidad validaci�n de terceros</param>
        /// <param name="pConfirmaYAprueba">Es Confirma ya aprueba valor en una entidad validaci�n de terceros</param>
        /// <returns>Lista de entidades validaci�n de terceros</returns>
        public List<ValidarTercero> ConsultarValidarTerceros(Int32 pIdTercero, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarTerceroDAL.ConsultarValidarTerceros(pIdTercero , pObservaciones , pConfirmaYAprueba );
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica el estado de una entidad Tercero
        /// </summary>
        /// <param name="pTercero">Entidad Tercero</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTercero_EstadoTercero(Oferente.Entity.Tercero pTercero)
        {
            try
            {
                return vValidarTerceroDAL.ModificarTercero_EstadoTercero(pTercero);               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }       
        
    }
}
