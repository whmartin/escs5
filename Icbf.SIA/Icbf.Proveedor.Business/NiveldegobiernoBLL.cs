using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  nivel de gobierno
    /// </summary>
    public class NiveldegobiernoBLL
    {
        private NiveldegobiernoDAL vNiveldegobiernoDAL;
        public NiveldegobiernoBLL()
        {
            vNiveldegobiernoDAL = new NiveldegobiernoDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoDAL.InsertarNiveldegobierno(pNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoDAL.ModificarNiveldegobierno(pNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pNiveldegobierno">Entidad Nivel de gobierno</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarNiveldegobierno(Niveldegobierno pNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoDAL.EliminarNiveldegobierno(pNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Nivel de gobierno
        /// </summary>
        /// <param name="pIdNiveldegobierno">Identificador de una entidad Nivel de gobierno</param>
        /// <returns>Entidad Nivel de gobierno</returns>
        public Niveldegobierno ConsultarNiveldegobierno(int pIdNiveldegobierno)
        {
            try
            {
                return vNiveldegobiernoDAL.ConsultarNiveldegobierno(pIdNiveldegobierno);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <param name="pCodigoNiveldegobierno">C�digo en una entidad Nivel de gobierno</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Nivel de gobierno</param>
        /// <param name="pEstado">Estado en una entidad Nivel de gobierno</param>
        /// <returns>Lista de entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernos(String pCodigoNiveldegobierno, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vNiveldegobiernoDAL.ConsultarNiveldegobiernos(pCodigoNiveldegobierno, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <param name="pIdRamaEstructura">Identificador de la rama estructura en una entidad Nivel de gobierno</param>
        /// <returns>Lista de entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernosRamaoEstructura(int pIdRamaEstructura)
        {
            try
            {
                return vNiveldegobiernoDAL.ConsultarNiveldegobiernosRamaoEstructura( pIdRamaEstructura);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Nivel de gobierno
        /// </summary>
        /// <returns>Lista de Entidades Nivel de gobierno</returns>
        public List<Niveldegobierno> ConsultarNiveldegobiernosAll()
        {
            try
            {
                return vNiveldegobiernoDAL.ConsultarNiveldegobiernosAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
