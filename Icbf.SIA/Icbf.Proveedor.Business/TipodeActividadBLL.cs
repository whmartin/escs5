using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo de actividad
    /// </summary>
    public class TipodeActividadBLL
    {
        private TipodeActividadDAL vTipodeActividadDAL;
        public TipodeActividadBLL()
        {
            vTipodeActividadDAL = new TipodeActividadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                return vTipodeActividadDAL.InsertarTipodeActividad(pTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                return vTipodeActividadDAL.ModificarTipodeActividad(pTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Actividad
        /// </summary>
        /// <param name="pTipodeActividad">Entidad Tipo Actividad</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipodeActividad(TipodeActividad pTipodeActividad)
        {
            try
            {
                return vTipodeActividadDAL.EliminarTipodeActividad(pTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Actividad
        /// </summary>
        /// <param name="pIdTipodeActividad">Identificador en una entidad Tipo Actividad</param>
        /// <returns>Entidad Tipo Actividad</returns>
        public TipodeActividad ConsultarTipodeActividad(int pIdTipodeActividad)
        {
            try
            {
                return vTipodeActividadDAL.ConsultarTipodeActividad(pIdTipodeActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Actividad
        /// </summary>
        /// <param name="pCodigoTipodeActividad">C�digo en una entidad Tipo Actividad</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Actividad</param>
        /// <param name="pEstado">Estado en una entidad Tipo Actividad</param>
        /// <returns>Lista de entidades Tipo Actividad</returns>
        public List<TipodeActividad> ConsultarTipodeActividads(String pCodigoTipodeActividad, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipodeActividadDAL.ConsultarTipodeActividads(pCodigoTipodeActividad, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Actividad
        /// </summary>
        /// <returns>Lista de entidades Tipo Actividad</returns>
        public List<TipodeActividad> ConsultarTipodeActividadAll()
        {
            try
            {
                return vTipodeActividadDAL.ConsultarTipodeActividadAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
