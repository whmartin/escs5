using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad revisi�n del proveedor
    /// </summary>
    public class RevisionBLL
    {
        private RevisionDAL vRevisionDAL;
        public RevisionBLL()
        {
            vRevisionDAL = new RevisionDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Revisi�n
        /// </summary>
        /// <param name="pRevision">Entidad Revisi�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarRevision(Revision pRevision)
        {
            try
            {
                return vRevisionDAL.InsertarRevision(pRevision);               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Revisi�n
        /// </summary>
        /// <param name="pRevision">Entidad Revisi�n</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarRevision(Revision pRevision)
        {
            try
            {
                return vRevisionDAL.ModificarRevision(pRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Revisi�n
        /// </summary>
        /// <param name="pRevision">Identificador de una entidad Revisi�n</param>
        /// <returns>Entidad Revisi�n</returns>
        public Revision ConsultarRevision(int pIdRevision)
        {
            try
            {
                return vRevisionDAL.ConsultarRevision(pIdRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Revisi�n
        /// </summary>
        /// <param name="pIdEntidad">Identificador de una entidad Revisi�n</param>
        /// <param name="pComponente">Componente en una entidad Revisi�n</param>
        /// <returns>Entidad Revisi�n</returns>
        public Revision ConsultarUltimaRevision(Int32 pIdEntidad, String pComponente)
        {
            try
            {
                return vRevisionDAL.ConsultarUltimaRevision(pIdEntidad,pComponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Revisi�n
        /// </summary>
        /// <param name="pIdRevision">Identificador de Revisi�n en una entidad Revisi�n</param>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad Revisi�n</param>
        /// <param name="pComponente">Componente en una entidad Revisi�n</param>
        /// <param name="pNroRevision">N�mero en una entidad Revisi�n</param>
        /// <returns>Lista de entidades Revisi�n</returns>
        public List<Revision> ConsultarRevisions(Int32? pIdRevision, Int32? pIdEntidad, String pComponente, Int16? pNroRevision)
        {
            try
            {
                return vRevisionDAL.ConsultarRevisions(pIdRevision, pIdEntidad, pComponente, pNroRevision);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
