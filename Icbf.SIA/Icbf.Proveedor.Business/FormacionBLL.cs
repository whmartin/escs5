using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Formacion
    /// </summary>
    public class FormacionBLL
    {
        private FormacionDAL vFormacionDAL;
        public FormacionBLL()
        {
            vFormacionDAL = new FormacionDAL();
        }
        /// <summary>
        /// Método de inserción para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int InsertarFormacion(Formacion pFormacion)
        {
            try
            {
                return vFormacionDAL.InsertarFormacion(pFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de modificación para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int ModificarFormacion(Formacion pFormacion)
        {
            try
            {
                return vFormacionDAL.ModificarFormacion(pFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método de eliminación para la entidad Formacion
        /// </summary>
        /// <param name="pFormacion"></param>
        public int EliminarFormacion(Formacion pFormacion)
        {
            try
            {
                return vFormacionDAL.EliminarFormacion(pFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por id para la entidad Formacion
        /// </summary>
        /// <param name="pIdFormacion"></param>
        public Formacion ConsultarFormacion(int pIdFormacion)
        {
            try
            {
                return vFormacionDAL.ConsultarFormacion(pIdFormacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método de consulta por filtros para la entidad Formacion
        /// </summary>
        /// <param name="pIdEntidad"></param>
        /// <param name="pCodigoModalidad"></param>
        /// <param name="pIdProfesion"></param>
        /// <param name="pEsFormacionPrincipal"></param>
        /// <param name="pEstado"></param>
        public List<Formacion> ConsultarFormacions(int? pIdEntidad, String pCodigoModalidad, String pIdProfesion, Boolean? pEsFormacionPrincipal, Boolean? pEstado)
        {
            try
            {
                return vFormacionDAL.ConsultarFormacions(pIdEntidad, pCodigoModalidad, pIdProfesion, pEsFormacionPrincipal, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
