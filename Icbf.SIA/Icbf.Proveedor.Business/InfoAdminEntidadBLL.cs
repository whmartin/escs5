using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Informaci�n Administrativa
    /// </summary>
    public class InfoAdminEntidadBLL
    {
        private InfoAdminEntidadDAL vInfoAdminEntidadDAL;
        public InfoAdminEntidadBLL()
        {
            vInfoAdminEntidadDAL = new InfoAdminEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Informaci�n Administrativa</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                return vInfoAdminEntidadDAL.InsertarInfoAdminEntidad(pInfoAdminEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Informaci�n Administrativa</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                return vInfoAdminEntidadDAL.ModificarInfoAdminEntidad(pInfoAdminEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pInfoAdminEntidad">Entidad Informaci�n Administrativa</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarInfoAdminEntidad(InfoAdminEntidad pInfoAdminEntidad)
        {
            try
            {
                return vInfoAdminEntidadDAL.EliminarInfoAdminEntidad(pInfoAdminEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pIdInfoAdmin">Identificador en una entidad Informaci�n Administrativa</param>
        /// <returns>Entidad Informaci�n Administrativa</returns>
        public InfoAdminEntidad ConsultarInfoAdminEntidad(int pIdInfoAdmin)
        {
            try
            {
                return vInfoAdminEntidadDAL.ConsultarInfoAdminEntidad(pIdInfoAdmin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n Administrativa
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad Informaci�n Administrativa</param>
        /// <returns>Lista de entidades Informaci�n Administrativa</returns>
        public List<InfoAdminEntidad> ConsultarInfoAdminEntidads(int? pIdEntidad)
        {
            try
            {
                return vInfoAdminEntidadDAL.ConsultarInfoAdminEntidads(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n Administrativa
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad Informaci�n Administrativa</param>
        /// <returns>Entidad Informaci�n Administrativa</returns>
        public InfoAdminEntidad ConsultarInfoAdminEntidadIdEntidad(int pIdEntidad)
        {
            try
            {
                return vInfoAdminEntidadDAL.ConsultarInfoAdminEntidadIdEntidad(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
