using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad C�digo UNSPSC en experiencia
    /// </summary>
    public class ExperienciaCodUNSPSCEntidadBLL
    {
        private ExperienciaCodUNSPSCEntidadDAL vExperienciaCodUNSPSCEntidadDAL;
        public ExperienciaCodUNSPSCEntidadBLL()
        {
            vExperienciaCodUNSPSCEntidadDAL = new ExperienciaCodUNSPSCEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad C�digos UNSPSC de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadDAL.InsertarExperienciaCodUNSPSCEntidad(pExperienciaCodUNSPSCEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad C�digos UNSPSC de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadDAL.ModificarExperienciaCodUNSPSCEntidad(pExperienciaCodUNSPSCEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pExperienciaCodUNSPSCEntidad">Entidad C�digos UNSPSC de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarExperienciaCodUNSPSCEntidad(ExperienciaCodUNSPSCEntidad pExperienciaCodUNSPSCEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadDAL.EliminarExperienciaCodUNSPSCEntidad(pExperienciaCodUNSPSCEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pIdExpCOD">Identificador de una entidad C�digos UNSPSC de experiencia</param>
        /// <returns>Entidad C�digos UNSPSC de experiencia</returns>
        public ExperienciaCodUNSPSCEntidad ConsultarExperienciaCodUNSPSCEntidad(int pIdExpCOD)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadDAL.ConsultarExperienciaCodUNSPSCEntidad(pIdExpCOD);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades C�digos UNSPSC de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de una entidad C�digos UNSPSC de experiencia</param>
        /// <returns>Lista de entidades C�digos UNSPSC de experiencia</returns>
        public List<ExperienciaCodUNSPSCEntidad> ConsultarExperienciaCodUNSPSCEntidads(int? pIdExpEntidad)
        {
            try
            {
                return vExperienciaCodUNSPSCEntidadDAL.ConsultarExperienciaCodUNSPSCEntidads(pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
