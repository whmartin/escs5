using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de validaci�n de datos b�sicos
    /// </summary>
    public class ValidarInfoDatosBasicosEntidadBLL
    {
        private ValidarInfoDatosBasicosEntidadDAL vValidarInfoDatosBasicosEntidadDAL;
        
        public ValidarInfoDatosBasicosEntidadBLL()
        {
            vValidarInfoDatosBasicosEntidadDAL = new ValidarInfoDatosBasicosEntidadDAL();
        }

        /// <summary>
        /// Insertar una entidad Validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarValidarInfoDatosBasicosEntidad(ValidarInfoDatosBasicosEntidad pValidarInfoDatosBasicosEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.InsertarValidarInfoDatosBasicosEntidad(pValidarInfoDatosBasicosEntidad, idEstadoInfoDatosBasicosEntidad );               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pValidarInfoDatosBasicosEntidad">Entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="idEstadoInfoDatosBasicosEntidad">Identificador de estado en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarValidarInfoDatosBasicosEntidad(ValidarInfoDatosBasicosEntidad pValidarInfoDatosBasicosEntidad, int idEstadoInfoDatosBasicosEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.ModificarValidarInfoDatosBasicosEntidad(pValidarInfoDatosBasicosEntidad, idEstadoInfoDatosBasicosEntidad);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdValidarInfoDatosBasicosEntidad">Identificador en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Entidad validaci�n de datos b�sicos del proveedor</returns>
        public ValidarInfoDatosBasicosEntidad ConsultarValidarInfoDatosBasicosEntidad(int pIdValidarInfoDatosBasicosEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.ConsultarValidarInfoDatosBasicosEntidad(pIdValidarInfoDatosBasicosEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador de entidad en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en la entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Lista de entidades validaci�n de datos b�sicos del proveedor</returns>
        public List<ValidarInfoDatosBasicosEntidad> ConsultarValidarInfoDatosBasicosEntidads(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.ConsultarValidarInfoDatosBasicosEntidads(pIdEntidad , pObservaciones , pConfirmaYAprueba );
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pObservaciones">Observaciones en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <param name="pConfirmaYAprueba">Es confirma y aprueba valor en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Lista de entidades validaci�n de datos b�sicos del proveedor</returns>
        public List<ValidarInfoDatosBasicosEntidad> ConsultarValidarInfoDatosBasicosEntidadsResumen(Int32 pIdEntidad, String pObservaciones, Boolean? pConfirmaYAprueba)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.ConsultarValidarInfoDatosBasicosEntidadsResumen(pIdEntidad, pObservaciones, pConfirmaYAprueba);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad validaci�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pIdEntidad">Identificador en una entidad validaci�n de datos b�sicos del proveedor</param>
        /// <returns>Entidad validaci�n de datos b�sicos del proveedor</returns>
        public ValidarInfoDatosBasicosEntidad ConsultarValidarInfoDatosBasicosEntidad_Ultima(Int32 pIdEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.ConsultarValidarInfoDatosBasicosEntidad_Ultima(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



        /// <summary>
        /// Modifica el ConfirmaYAprueba de la ultima revisi�n de datos b�sicos del proveedor
        /// </summary>
        /// <param name="pidEntidad">Id de la entidad Proveedor Oferente que se modifica</param>
        /// <returns>bool true si se ha ejecutado correctamente.</returns>
        public bool ModificarConfirmaYApreba_InfoDatosBasicosEntidad(int pidEntidad)
        {
            try
            {
                return vValidarInfoDatosBasicosEntidadDAL.ModificarConfirmaYApreba_InfoDatosBasicosEntidad(pidEntidad);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

       
    }
}
