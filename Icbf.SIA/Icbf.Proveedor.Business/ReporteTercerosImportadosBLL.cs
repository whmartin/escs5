﻿using Icbf.Oferente.Entity;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Business
{
    public class ReporteTercerosImportadosBLL
    {
        ReporteTercerosImportadosDAL vReporteImportado;

        public ReporteTercerosImportadosBLL()
        {
            vReporteImportado = new ReporteTercerosImportadosDAL();
        }

        /// <summary>
        /// Consultar Municipios X departamento(s)
        /// </summary>
        /// <param name="pIdDepartamento"></param>
        /// <returns></returns>
        public List<ExperienciaMunicipio> ConsultarMunicipiosXDepto(string pIdDepto)
        {
            try
            {
                return vReporteImportado.ConsultarMunicipiosXDepto(pIdDepto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar datos de terceros importados y retorna un tru si hay datos y false si no.
        /// </summary>
        /// <param name="vReporte"></param>
        /// <returns></returns>
        public bool ConsultarDatosReporte(ReporteTercerosImportados vReporte)
        {
            try
            {
                return vReporteImportado.ConsultarDatosReporte(vReporte);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
