using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Tipo c�digo UNSPSC
    /// </summary>
    public class TipoCodigoUNSPSCBLL
    {
        private TipoCodigoUNSPSCDAL vTipoCodigoUNSPSCDAL;
        public TipoCodigoUNSPSCBLL()
        {
            vTipoCodigoUNSPSCDAL = new TipoCodigoUNSPSCDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad �digo UNSPSC</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.InsertarTipoCodigoUNSPSC(pTipoCodigoUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad �digo UNSPSC</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.ModificarTipoCodigoUNSPSC(pTipoCodigoUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pTipoCodigoUNSPSC">Entidad �digo UNSPSC</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoCodigoUNSPSC(TipoCodigoUNSPSC pTipoCodigoUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.EliminarTipoCodigoUNSPSC(pTipoCodigoUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad C�digo UNSPSC
        /// </summary>
        /// <param name="pIdTipoCodUNSPSC">Identificador de una entidad C�digo UNSPSC</param>
        /// <returns>Entidad C�digo UNSPSC</returns>
        public TipoCodigoUNSPSC ConsultarTipoCodigoUNSPSC(int pIdTipoCodUNSPSC)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.ConsultarTipoCodigoUNSPSC(pIdTipoCodUNSPSC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades C�digo UNSPSC
        /// </summary>
        /// <param name="pCodigo">C�digo en una entidad C�digo UNSPSC</param>
        /// <param name="pDescripcion">Descripci�n en una entidad C�digo UNSPSC</param>
        /// <param name="pEstado">Estado en una entidad C�digo UNSPSC</param>
        /// <param name="pLongitudUNSPSC">Longitud en una entidad C�digo UNSPSC</param>
        /// <returns>Lista de entidades C�digo UNSPSC</returns>
        public List<TipoCodigoUNSPSC> ConsultarTipoCodigoUNSPSCs(String pCodigo, String pDescripcion, Boolean? pEstado, int? pLongitudUNSPSC, String pCodigoSegmento, String pCodigoFamilia, String pCodigoClase)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.ConsultarTipoCodigoUNSPSCs(pCodigo, pDescripcion, pEstado, pLongitudUNSPSC, pCodigoSegmento,pCodigoFamilia, pCodigoClase);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta los Segmentos para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarSegmento()
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.ConsultarSegmento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta las Familias por Segmento para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <param name="pCodigoSegmento">C�digo del Segmento</param>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarFamilia(String pCodigoSegmento)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.ConsultarFamilia(pCodigoSegmento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Metodo que Consulta las Clases por Familias para la entidad TipoCodigoUNSPSC
        /// </summary>
        /// <param name="pCodigoFamilia">C�digo de la Familia</param>
        /// <returns></returns>
        public List<TipoCodigoUNSPSC> ConsultarClase(String pCodigoFamilia)
        {
            try
            {
                return vTipoCodigoUNSPSCDAL.ConsultarClase(pCodigoFamilia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
