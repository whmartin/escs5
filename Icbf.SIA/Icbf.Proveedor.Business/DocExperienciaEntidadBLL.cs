using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad de documentos adjuntos a m�dulo experiencia
    /// </summary>
    public class DocExperienciaEntidadBLL
    {
        private DocExperienciaEntidadDAL vDocExperienciaEntidadDAL;
        public DocExperienciaEntidadBLL()
        {
            vDocExperienciaEntidadDAL = new DocExperienciaEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                return vDocExperienciaEntidadDAL.InsertarDocExperienciaEntidad(pDocExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// modifica una entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                return vDocExperienciaEntidadDAL.ModificarDocExperienciaEntidad(pDocExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pDocExperienciaEntidad">Entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarDocExperienciaEntidad(DocExperienciaEntidad pDocExperienciaEntidad)
        {
            try
            {
                return vDocExperienciaEntidadDAL.EliminarDocExperienciaEntidad(pDocExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdDocAdjunto">Identificador de una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Entidad Documentos adjuntos a informaci�n de experiencia</returns>
        public DocExperienciaEntidad ConsultarDocExperienciaEntidad(int pIdDocAdjunto)
        {
            try
            {
                return vDocExperienciaEntidadDAL.ConsultarDocExperienciaEntidad(pIdDocAdjunto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de experiencia en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pLinkDocumento">Liga del documento en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a informaci�n de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidads(int? pIdExpEntidad, String pDescripcion, String pLinkDocumento)
        {
            try
            {
                return vDocExperienciaEntidadDAL.ConsultarDocExperienciaEntidads(pIdExpEntidad, pDescripcion, pLinkDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de experiencia en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pRupRenovado">Rup Renovado en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a informaci�n de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(int pIdExpEntidad, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocExperienciaEntidadDAL.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(pIdExpEntidad, pRupRenovado, pTipoPersona, pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Documentos adjuntos a informaci�n de experiencia
        /// </summary>
        /// <param name="pIdTemporal">Identificador del temporal en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pRupRenovado">Rup Renovado en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoPersona">Tipo de persona en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <param name="pTipoSector">Tipo de sector en una entidad Documentos adjuntos a informaci�n de experiencia</param>
        /// <returns>Lista de entidades Documentos adjuntos a informaci�n de experiencia</returns>
        public List<DocExperienciaEntidad> ConsultarDocExperienciaEntidad_IdTemporal_Rup(string pIdTemporal, string pRupRenovado, string pTipoPersona, string pTipoSector)
        {
            try
            {
                return vDocExperienciaEntidadDAL.ConsultarDocExperienciaEntidad_IdTemporal_Rup(pIdTemporal, pRupRenovado, pTipoPersona,pTipoSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
