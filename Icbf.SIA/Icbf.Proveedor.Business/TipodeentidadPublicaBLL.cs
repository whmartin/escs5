using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Tipo de entidad p�bica
    /// </summary>
    public class TipodeentidadPublicaBLL
    {
        private TipodeentidadPublicaDAL vTipodeentidadPublicaDAL;
        public TipodeentidadPublicaBLL()
        {
            vTipodeentidadPublicaDAL = new TipodeentidadPublicaDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad P�blica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaDAL.InsertarTipodeentidadPublica(pTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad P�blica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaDAL.ModificarTipodeentidadPublica(pTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pTipodeentidadPublica">Entidad Tipo Entidad P�blica</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipodeentidadPublica(TipodeentidadPublica pTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaDAL.EliminarTipodeentidadPublica(pTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Tipo Entidad P�blica
        /// </summary>
        /// <param name="pIdTipodeentidadPublica">Identificador en una entidad Tipo Entidad P�blica</param>
        /// <returns>Entidad Tipo Entidad P�blica</returns>
        public TipodeentidadPublica ConsultarTipodeentidadPublica(int pIdTipodeentidadPublica)
        {
            try
            {
                return vTipodeentidadPublicaDAL.ConsultarTipodeentidadPublica(pIdTipodeentidadPublica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad P�blica
        /// </summary>
        /// <param name="pCodigoTipodeentidadPublica">C�digo en una entidad Tipo Entidad P�blica</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Entidad P�blica</param>
        /// <param name="pEstado">Estado en una entidad Tipo Entidad P�blica</param>
        /// <returns>Lista de entidades Tipo Entidad P�blica</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicas(String pCodigoTipodeentidadPublica, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipodeentidadPublicaDAL.ConsultarTipodeentidadPublicas(pCodigoTipodeentidadPublica, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad P�blica
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad P�blica</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicasAll()
        {
            try
            {
                return vTipodeentidadPublicaDAL.ConsultarTipodeentidadPublicasAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        
        /// <summary>
        /// Consulta una lista de entidades Tipo Entidad P�blica por IdRama. IdNivelGob, IdNivelOrganizacional
        /// </summary>
        /// <returns>Lista de entidades Tipo Entidad P�blica</returns>
        public List<TipodeentidadPublica> ConsultarTipodeentidadPublicas_PorRamaNivelGobYNivelOrg(int idRama, int idNivelGob, int idNivelOrg)
        {
            try
            {
                return vTipodeentidadPublicaDAL.ConsultarTipodeentidadPublicas_PorRamaNivelGobYNivelOrg(idRama,idNivelGob,idNivelOrg);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
