using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Informaci�n de Experiencia
    /// </summary>
    public class InfoExperienciaEntidadBLL
    {
        private InfoExperienciaEntidadDAL vInfoExperienciaEntidadDAL;
        public InfoExperienciaEntidadBLL()
        {
            vInfoExperienciaEntidadDAL = new InfoExperienciaEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int InsertarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.InsertarInfoExperienciaEntidad(pInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.ModificarInfoExperienciaEntidad(pInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina una entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int EliminarInfoExperienciaEntidad(InfoExperienciaEntidad pInfoExperienciaEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.EliminarInfoExperienciaEntidad(pInfoExperienciaEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una entidad Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pIdExpEntidad">Identificador de una entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>Entidad Informaci�n M�dulo Experiencia</returns>
        public InfoExperienciaEntidad ConsultarInfoExperienciaEntidad(int pIdExpEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.ConsultarInfoExperienciaEntidad(pIdExpEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de entidades Informaci�n M�dulo Experiencia
        /// </summary>
        /// <param name="pIdEntidad">Identificador general de una entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>Lista de entidades Informaci�n M�dulo Experiencia</returns>
        public List<InfoExperienciaEntidad> ConsultarInfoExperienciaEntidads(int? pIdEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.ConsultarInfoExperienciaEntidads(pIdEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modifica una entidad Informaci�n M�dulo Experiencia para liberar
        /// </summary>
        /// <param name="pInfoExperienciaEntidad">Entidad Informaci�n M�dulo Experiencia</param>
        /// <returns>N�mero de registros afectados en tabla</returns>
        public int ModificarInfoExperienciaEntidad_Liberar(InfoExperienciaEntidad vInfoExperiencia)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.ModificarInfoExperienciaEntidad_Liberar(vInfoExperiencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Finaliza InfoFinancieraEntidad
        /// </summary>
        /// <param name="pIdEntidad"> Id Entidad Proveedores Oferente</param>
        public int FinalizaInfoExperienciaEntidad(int pIdEntidad)
        {
            try
            {
                return vInfoExperienciaEntidadDAL.FinalizaInfoExperienciaEntidad(pIdEntidad);
            }
            catch (UserInterfaceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
