using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad  Tipo Cargo de la entidad
    /// </summary>
    public class TipoCargoEntidadBLL
    {
        private TipoCargoEntidadDAL vTipoCargoEntidadDAL;
        public TipoCargoEntidadBLL()
        {
            vTipoCargoEntidadDAL = new TipoCargoEntidadDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadDAL.InsertarTipoCargoEntidad(pTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Modifica una entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadDAL.ModificarTipoCargoEntidad(pTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 //throw new GenericException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Eliminar una entidad Tipo cargo
        /// </summary>
        /// <param name="pTipoCargoEntidad">Entidad Tipo cargo</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoCargoEntidad(TipoCargoEntidad pTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadDAL.EliminarTipoCargoEntidad(pTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo cargo
        /// </summary>
        /// <param name="pIdTipoCargoEntidad">Identificador de una entidad Tipo cargo</param>
        /// <returns>Entidad Tipo cargo</returns>
        public TipoCargoEntidad ConsultarTipoCargoEntidad(int pIdTipoCargoEntidad)
        {
            try
            {
                return vTipoCargoEntidadDAL.ConsultarTipoCargoEntidad(pIdTipoCargoEntidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo cargo
        /// </summary>
        /// <param name="pCodigo">C�digo de una Tipo cargo</param>
        /// <param name="pDescripcion">Descripci�n de una entidad Tipo cargo</param>
        /// <param name="pEstado">Estado de una entidad Tipo cargo</param>
        /// <returns>Lista de entidades Tipo cargo</returns>
        public List<TipoCargoEntidad> ConsultarTipoCargoEntidads(String pCodigo, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoCargoEntidadDAL.ConsultarTipoCargoEntidads(pCodigo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo cargo
        /// </summary>
        /// <returns>Lista de entidades Tipo cargo</returns>
        public List<TipoCargoEntidad> ConsultarTipoCargoEntidadAll()
        {
            try
            {
                return vTipoCargoEntidadDAL.ConsultarTipoCargoEntidadAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
