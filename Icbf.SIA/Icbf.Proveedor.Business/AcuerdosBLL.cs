using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    /// Clase de capa de negocio para la entidad Acuerdos
    /// </summary>
    public class AcuerdosBLL
    {
        private AcuerdosDAL vAcuerdosDAL;
        public AcuerdosBLL()
        {
            vAcuerdosDAL = new AcuerdosDAL();
        }

        /// <summary>
        /// Inserta un nuevo Acuerdo
        /// </summary>
        /// <param name="pAcuerdos">Entidad Acuerdos</param>
        /// <returns>Identificador del Acuerdo en tabla</returns>
        public int InsertarAcuerdos(Acuerdos pAcuerdos)
        {
            try
            {
                return vAcuerdosDAL.InsertarAcuerdos(pAcuerdos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Acuerdo
        /// </summary>
        /// <param name="pIdAcuerdo">Identificador del Acuerdo</param>
        /// <returns>Entidad Acuerdos</returns>
        public Acuerdos ConsultarAcuerdos(int pIdAcuerdo)
        {
            try
            {
                return vAcuerdosDAL.ConsultarAcuerdos(pIdAcuerdo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un Acuerdo Vigente
        /// </summary>
        /// <param></param>
        /// <returns>Entidad Acuerdos</returns>
        public Acuerdos ConsultarAcuerdoVigente()
        {
            try
            {
                return vAcuerdosDAL.ConsultarAcuerdoVigente();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta una lista de Acuerdos
        /// </summary>
        /// <param name="pNombre">Nombre del Acuerdo</param>
        /// <returns>Lista de Entidades Acuerdos</returns>
        public List<Acuerdos> ConsultarAcuerdoss(String pNombre)
        {
            try
            {
                return vAcuerdosDAL.ConsultarAcuerdoss(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
