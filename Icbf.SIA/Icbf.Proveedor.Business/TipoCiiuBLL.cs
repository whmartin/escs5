using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Proveedor.DataAccess;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Proveedor.Business
{
    /// <summary>
    ///  Clase de capa de negocio para la entidad Tipo Ciiu
    /// </summary>
    public class TipoCiiuBLL
    {
        private TipoCiiuDAL vTipoCiiuDAL;
        public TipoCiiuBLL()
        {
            vTipoCiiuDAL = new TipoCiiuDAL();
        }

        /// <summary>
        /// Inserta una nueva entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>N�mero de registros afectados</returns>
        public int InsertarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                return vTipoCiiuDAL.InsertarTipoCiiu(pTipoCiiu);               

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Modificar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>N�mero de registros afectados</returns>
        public int ModificarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                return vTipoCiiuDAL.ModificarTipoCiiu(pTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Eliminar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pTipoCiiu">Entidad Tipo Ciuu</param>
        /// <returns>N�mero de registros afectados</returns>
        public int EliminarTipoCiiu(TipoCiiu pTipoCiiu)
        {
            try
            {
                return vTipoCiiuDAL.EliminarTipoCiiu(pTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una entidad Tipo Ciuu
        /// </summary>
        /// <param name="pIdTipoCiiu">Identificador de una entidad Tipo Ciuu</param>
        /// <returns>Entidad Tipo Ciuu</returns>
        public TipoCiiu ConsultarTipoCiiu(int pIdTipoCiiu)
        {
            try
            {
                return vTipoCiiuDAL.ConsultarTipoCiiu(pIdTipoCiiu);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar una lista de entidades Tipo Ciuu
        /// </summary>
        /// <param name="pCodigoCiiu">C�digo en una entidad Tipo Ciuu</param>
        /// <param name="pDescripcion">Descripci�n en una entidad Tipo Ciuu</param>
        /// <param name="pEstado">Estado en una entidad Tipo Ciuu</param>
        /// <returns>Lista de entidades Tipo Ciuu</returns>
        public List<TipoCiiu> ConsultarTipoCiius(String pCodigoCiiu, String pDescripcion, Boolean? pEstado)
        {
            try
            {
                return vTipoCiiuDAL.ConsultarTipoCiius(pCodigoCiiu, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
