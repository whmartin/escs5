using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Permite administrar la formacion de los proveedores
    /// </summary>
    public class Formacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdFormacion
        {
            get;
            set;
        }
        public int IdEntidad
        {
            get;
            set;
        }
        public String CodigoModalidad
        {
            get;
            set;
        }
        public String ModalidadAcademica
        {
            get;
            set;
        }
        public String IdProfesion
        {
            get;
            set;
        }
        public String Profesion
        {
            get;
            set;
        }
        public Boolean EsFormacionPrincipal
        {
            get;
            set;
        }
        public Boolean Estado
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }

        public string tipoFormacion 
        {
            get;
            set;
        }
        public string nombreModalidadAcademica 
        { 
            get; 
            set; 
        }
        public string nombreProfesion 
        { 
            get; 
            set; 
        }

        public Formacion()
        {
        }
    }
}
