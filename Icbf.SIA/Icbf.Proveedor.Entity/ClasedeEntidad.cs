using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para Clase entidad
    /// </summary>
    public class ClasedeEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// IdClasedeEntidad
        /// </summary>
        public int IdClasedeEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// IdTipodeActividad
        /// </summary>
        public int IdTipodeActividad
        {
            get;
            set;
        }
        /// <summary>
        /// Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// TipodeActividad
        /// </summary>
        public String TipodeActividad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ClasedeEntidad()
        {
        }
    }
}
