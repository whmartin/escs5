using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para  Tipo C�digo UNSPSC
    /// </summary>
    public class TipoCodigoUNSPSC : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdTipoCodUNSPSC
        /// </summary>
        public int IdTipoCodUNSPSC
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Codigo
        /// </summary>
        public String Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado 
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoSegmento
        /// </summary>
        public String CodigoSegmento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreSegmento
        /// </summary>
        public String NombreSegmento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoFamilia
        /// </summary>
        public String CodigoFamilia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreFamilia
        /// </summary>
        public String NombreFamilia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoClase
        /// </summary>
        public String CodigoClase
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreClase
        /// </summary>
        public String NombreClase
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor 
        /// </summary>
        public TipoCodigoUNSPSC()
        {
        }
    }
}
