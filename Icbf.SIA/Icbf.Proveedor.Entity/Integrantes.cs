using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// 
    /// </summary>
    public class Integrantes : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdIntegrante
        {
            get;
            set;
        }
        public int IdEntidad
        {
            get;
            set;
        }
        public int IdTipoPersona
        {
            get;
            set;
        }
        public int IdTipoIdentificacionPersonaNatural
        {
            get;
            set;
        }
        public string NumeroIdentificacion
        {
            get;
            set;
        }
        public decimal PorcentajeParticipacion
        {
            get;
            set;
        }
        public String ConfirmaCertificado
        {
            get;
            set;
        }
        public String ConfirmaPersona
        {
            get;
            set;
        }
        public String UsuarioCrea
        {
            get;
            set;
        }
        public String UsuarioModifica
        {
            get;
            set;
        }
        public DateTime FechaCrea
        {
            get;
            set;
        }
        public DateTime FechaModifica
        {
            get;
            set;
        }
        public int IntegrantesReg
        {
            get;
            set;
        }
        public int NumIntegrantes
        {
            get;
            set;
        }
        public int DV
        {
            get;
            set;
        }
        public String Proveedor
        {
            get;
            set;
        }
        public String CorreoElectronico
        {
            get;
            set;
        }
        public String NombreTipoPersona
        {
            get;
            set;
        }
        public String Integrante
        {
            get;
            set;
        }

        public String RepresentanteLegal
        {
            get;
            set;
        }
        public String LinkDocumento
        {
            get;
            set;
        }
        public Integrantes()
        {
        }
    }
}
