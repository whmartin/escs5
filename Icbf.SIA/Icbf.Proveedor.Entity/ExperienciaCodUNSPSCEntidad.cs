using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para los c�digos UNSPSC de experiencia
    /// </summary>
    public class ExperienciaCodUNSPSCEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdExpCOD
        /// </summary>
        public int IdExpCOD
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoCodUNSPSC
        /// </summary>
        public int IdTipoCodUNSPSC
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdExpEntidad
        /// </summary>
        public int IdExpEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Codigo
        /// </summary>
        public String Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ExperienciaCodUNSPSCEntidad()
        {
        }
    }
}
