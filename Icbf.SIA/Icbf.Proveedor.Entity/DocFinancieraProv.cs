using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para los documentos adjuntos en la información financiera del proveedor
    /// </summary>
    [Serializable]
    public class DocFinancieraProv : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdDocAdjunto
        /// </summary>
        public int IdDocAdjunto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdInfoFin
        /// </summary>
        public int IdInfoFin
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDocumento
        /// </summary>
        public String NombreDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad LinkDocumento
        /// </summary>
        public String LinkDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Observaciones
        /// </summary>
        public String Observaciones
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Obligatorio
        /// </summary>
        public int Obligatorio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MaxPermitidoKB
        /// </summary>
        public int MaxPermitidoKB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ExtensionesPermitidas
        /// </summary>
        public string ExtensionesPermitidas
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdTemporal
        /// </summary>
        public string IdTemporal
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        public DocFinancieraProv()
        {
        }
    }
}
