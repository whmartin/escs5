using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para  validaci�n de m�dulo de experiencia del proveedor
    /// </summary>
    [Serializable]
    public class ValidarInfoExperienciaEntidad : ValidarInfo
    {

        /// <summary>
        /// Propiedad IdValidarInfoExperienciaEntidad
        /// </summary>
        public Int32  IdValidarInfoExperienciaEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdExpEntidad
        /// </summary>
        public Int32 IdExpEntidad
        {
            get;
            set;
        }       
        /// <summary>
        /// Constructor
        /// </summary>
        public ValidarInfoExperienciaEntidad()
        {
        }

        //-------------------------------Implements
        public int IdRegistro
        {
            get;
            set;
        }
        public string Usuario
        {
            get;
            set;
        }
        public string Programa
        {
            get;
            set;
        }
        public string Operacion
        {
            get;
            set;
        }
        public string ParametrosOperacion
        {
            get;
            set;
        }
        public string Tabla
        {
            get;
            set;
        }
        public string DireccionIP
        {
            get;
            set;
        }
        public string Navegador
        {
            get;
            set;
        }
        public bool ProgramaGeneraLog
        {
            get;
            set;
        }

    }
}
