using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para la información financiera del proveedor
    /// </summary>
    public class InfoFinancieraEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdInfoFin
        /// </summary>
        public int IdInfoFin
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdVigencia
        /// </summary>
        public int IdVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ActivoCte
        /// </summary>
        public Decimal ActivoCte
        {
            get;
            set;
        }
       
        /// <summary>
        /// Propiedad PasivoCte
        /// </summary>
        public Decimal PasivoCte
        {
            get;
            set;
        }
      
        /// <summary>
        /// Propiedad Patrimonio
        /// </summary>
        public Decimal Patrimonio
        {
            get;
            set;
        }
       
       
        /// <summary>
        /// Propiedad GastosInteresFinancieros
        /// </summary>
        public Decimal GastosInteresFinancieros
        {
            get;
            set;
        }
       
        /// <summary>
        /// Propiedad EstadoValidacion
        /// </summary>
        public int EstadoValidacion
        {
            get;
            set;
        }
       
        /// <summary>
        /// Propiedad ActivoTotal
        /// </summary>
        public Decimal ActivoTotal
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad PasivoTotal
        /// </summary>
        public Decimal PasivoTotal
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad UtilidadOperacional
        /// </summary>
        public Decimal UtilidadOperacional
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad ConfirmaIndicadoresFinancieros
        /// </summary>
        public Boolean ConfirmaIndicadoresFinancieros
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad RupRenovado
        /// </summary>
        public Boolean RupRenovado
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad ObservacionesInformacionFinanciera
        /// </summary>
        public String ObservacionesInformacionFinanciera
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad ObservacionesValidadorICBF
        /// </summary>
        public String ObservacionesValidadorICBF
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad Liquidez
        /// </summary>
        public Decimal Liquidez
        {
            get;
            set;
         
        }
        
        /// <summary>
        /// Propiedad Endeudamiento
        /// </summary>
        public Decimal Endeudamiento
        {
            get
            {
                try
                {
                    return PasivoTotal / ActivoTotal;
                }
                catch (DivideByZeroException ex1)
                {
                    throw ex1;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        
        /// <summary>
        /// Propiedad RazonCoberturaInteres
        /// </summary>
        public Decimal RazonCoberturaInteres
        {
            get;
            set;
           
        }

        
        
        /// <summary>
        /// Propiedad PatrimonioSMLV
        /// </summary>
        public Decimal PatrimonioSMLV
        {
            get;
            
            set;
        }
        
        /// <summary>
        /// Propiedad IdTemporal
        /// </summary>
        public String IdTemporal
        {
            get;
            set;
        }

        
        /// <summary>
        /// Propiedad Finalizado
        /// </summary>
        public bool Finalizado
        {
            get;
            set;
        }
        
        /// <summary>
        /// Propiedad NroRevision
        /// </summary>
        public Int16 NroRevision
        {
            get;
            set;
        }

         /// <summary>
         /// Propiedad Aprobada
         /// </summary>
        public string Aprobada
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public InfoFinancieraEntidad()
        {
        }
    }
}
