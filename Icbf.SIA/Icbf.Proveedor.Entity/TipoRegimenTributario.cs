using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para Tipo R�gimen Tributario
    /// </summary>
    public class TipoRegimenTributario : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdTipoRegimenTributario
        /// </summary>
        public int IdTipoRegimenTributario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoRegimenTributario
        /// </summary>
        public String CodigoRegimenTributario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoPersona
        /// </summary>
        public int IdTipoPersona
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad DescripcionTipoPersona
        /// </summary>
        public string DescripcionTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public TipoRegimenTributario()
        {
        }
    }
}
