using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para los valores de las tablas paramétricas
    /// </summary>
    public class ValoresParametricas : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdValorParametrica
        /// </summary>
        public int IdValorParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdListaParametrica
        /// </summary>
        public int IdListaParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoValorParametrica
        /// </summary>
        public String CodigoValorParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionValorParametrica
        /// </summary>
        public String DescripcionValorParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ValoresParametricas()
        {
        }
    }
}
