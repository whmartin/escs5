using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para notificación judicial
    /// </summary>
    public class NotificacionJudicial : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdNotJudicial
        /// </summary>
        public int IdNotJudicial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDepartamento
        /// </summary>
        public string NombreDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdMunicipio
        /// </summary>
        public int IdMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreMunicipio
        /// </summary>
        public string NombreMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdZona
        /// </summary>
        public int IdZona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Direccion
        /// </summary>
        public string  Direccion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public NotificacionJudicial()
        {
        }
    }
}
