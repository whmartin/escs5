﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{

    /// <summary>
    /// Clase entidad para los códigos UNSPSC de Proveedor
    /// </summary>
    public class CodigoUNSPSCProveedor : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdExpCOD
        {
            get;
            set;
        }


        /// <summary>
        /// Propiedad CodUNSPSC
        /// </summary>
        public int CodUNSPSC
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDTERCERO
        /// </summary>
        public int IDTERCERO
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoCodUNSPSC
        /// </summary>
        public int IdTipoCodUNSPSC
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Codigo Clase
        /// </summary>
        public String Codigo
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }

       /// <summary>
        /// Constructor
        /// </summary>
        public CodigoUNSPSCProveedor()
        {
        }
       
    }
}
