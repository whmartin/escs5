using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para el estado del tercero
    /// </summary>
    public class EstadoTercero : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdEstadoTercero
        /// </summary>
        public int IdEstadoTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoEstadotercero
        /// </summary>
        public String CodigoEstadotercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public int Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public EstadoTercero()
        {
        }
    }
}
