using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para sucursal
    /// </summary>
    public class Sucursal : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdSucursal
        /// </summary>
        public int IdSucursal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Nombre
        /// </summary>
        public String Nombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Indicativo
        /// </summary>
        public int? Indicativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Telefono
        /// </summary>
        public int? Telefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Extension
        /// </summary>
        public Int64? Extension
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Celular
        /// </summary>
        public long Celular
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Correo
        /// </summary>
        public String Correo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public int Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdZona
        /// </summary>
        public int IdZona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Departamento
        /// </summary>
        public int Departamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Municipio
        /// </summary>
        public int Municipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Direccion
        /// </summary>
        public String Direccion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Editable
        /// </summary>
        public int Editable
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public Sucursal()
        {
        }
    }
}
