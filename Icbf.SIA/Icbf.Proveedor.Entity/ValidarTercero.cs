using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para validación de tercero
    /// </summary>
    public class ValidarTercero : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdValidarTercero
        /// </summary>
        public Int32 IdValidarTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public Int32 IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Observaciones
        /// </summary>
        public String Observaciones
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConfirmaYAprueba
        /// </summary>
        public Boolean ConfirmaYAprueba
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ValidarTercero()
        {
        }

        /// <summary>
        /// Propiedad Tipo Incidente
        /// </summary>
        public int TipoIncidente
        {
            get;
            set;
        }
    }
}
