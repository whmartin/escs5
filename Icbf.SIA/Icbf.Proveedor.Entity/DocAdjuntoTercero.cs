using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para documentos adjuntos al tercero
    /// </summary>
    
    public class DocAdjuntoTercero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdDocAdjunto
        /// </summary>
        public int IdDocAdjunto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDocumento
        /// </summary>
        public int IdDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoDocumento
        /// </summary>
        public String NombreTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MaxPermitidoKB
        /// </summary>
        public Int32 MaxPermitidoKB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ExtensionesPermitidas
        /// </summary>
        public String ExtensionesPermitidas
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoDocumento
        /// </summary>
        public String CodigoTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad LinkDocumento
        /// </summary>
        public String LinkDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Anno
        /// </summary>
        public int Anno
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTemporal
        /// </summary>
        public String IdTemporal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Obligatorio
        /// </summary>
        public Int16 Obligatorio
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public DocAdjuntoTercero()
        {
        }
    }
}
