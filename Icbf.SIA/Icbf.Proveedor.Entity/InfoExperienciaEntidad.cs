using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para la información de experiencia del proveedor
    /// </summary>
    public class InfoExperienciaEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdExpEntidad
        /// </summary>
        public int IdExpEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoSector
        /// </summary>
        public int IdTipoSector
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoSector
        /// </summary>
        public string TipoSector
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoEstadoExp
        /// </summary>
        public int IdTipoEstadoExp
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoModalidadExp
        /// </summary>
        public int IdTipoModalidadExp
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoModalidad
        /// </summary>
        public int IdTipoModalidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoPoblacionAtendida
        /// </summary>
        public int IdTipoPoblacionAtendida
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoRangoExpAcum
        /// </summary>
        public int IdTipoRangoExpAcum
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoCodUNSPSC
        /// </summary>
        public int IdTipoCodUNSPSC
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoEntidadContratante
        /// </summary>
        public int IdTipoEntidadContratante
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EntidadContratante
        /// </summary>
        public String EntidadContratante
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaInicio
        /// </summary>
        public DateTime FechaInicio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaFin
        /// </summary>
        public DateTime FechaFin
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ExperienciaMeses
        /// </summary>
        public Decimal ExperienciaMeses
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroContrato
        /// </summary>
        public String NumeroContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObjetoContrato
        /// </summary>
        public String ObjetoContrato
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Vigente
        /// </summary>
        public Boolean Vigente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Cuantia
        /// </summary>
        public Decimal Cuantia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoDocumental
        /// </summary>
        public int EstadoDocumental
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UnionTempConsorcio
        /// </summary>
        public Boolean UnionTempConsorcio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PorcentParticipacion
        /// </summary>
        public Decimal PorcentParticipacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad AtencionDeptos
        /// </summary>
        public Boolean AtencionDeptos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad JardinOPreJardin
        /// </summary>
        public Boolean JardinOPreJardin
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdTemporal
        /// </summary>
        public String IdTemporal
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad EstadoValidacion
        /// </summary>
        public int EstadoValidacion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad NroRevision
        /// </summary>
        public Int16 NroRevision
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Finalizado
        /// </summary>
        public bool Finalizado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Aprobado
        /// </summary>
        public String Aprobado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ContratoEjecucion
        /// </summary>
        public Boolean ContratoEjecucion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoDocDescripcion
        /// </summary>
        public String EstadoDocDescripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public InfoExperienciaEntidad()
        {
        }
    }
}
