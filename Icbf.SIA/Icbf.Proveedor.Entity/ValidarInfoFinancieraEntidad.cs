using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para módulo de validación de información financiera del proveedor
    /// </summary>
    [Serializable]
    public class ValidarInfoFinancieraEntidad : ValidarInfo
    {

        /// <summary>
        /// Propiedad IdValidarInfoFinancieraEntidad
        /// </summary>
        public Int32  IdValidarInfoFinancieraEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdInfoFin
        /// </summary>
        public Int32 IdInfoFin
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdVigencia
        /// </summary>
        public Int32 IdVigencia
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Anno
        /// </summary>
        public Int32 Anno
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        public ValidarInfoFinancieraEntidad()
        {
        }
    }
}
