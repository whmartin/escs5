﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para validación de datos de Integrantes del Proveedor
    /// </summary>
    public class ValidacionIntegrantesEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdValidacionIntegrantesEntidad
        /// </summary>
        public Int32 IdValidacionIntegrantesEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public Int32 IdEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdEstadoValidacionIntegrantes
        /// </summary>
        public Int32 IdEstadoValidacionIntegrantes
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad EstadoValidacionIntegrantes
        /// </summary>
        public String EstadoValidacionIntegrantes
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NroRevision
        /// </summary>
        public Int32 NroRevision
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Finalizado
        /// </summary>
        public bool Finalizado
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// 
        /// <summary>
        /// Constructor
        /// </summary>
        public ValidacionIntegrantesEntidad()
        {
        }
    }
}
