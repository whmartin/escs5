using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para Validación de información
    /// </summary>
    [Serializable]
    public class ValidarInfo : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad NroRevision
        /// </summary>
        public int NroRevision
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Componente
        /// </summary>
        public String Componente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Observaciones
        /// </summary>
        public String Observaciones
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConfirmaYAprueba
        /// </summary>
        public Boolean? ConfirmaYAprueba
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad iConfirmaYAprueba
        /// </summary>
        public int iConfirmaYAprueba
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Finalizado
        /// </summary>
        public bool Finalizado
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Liberar
        /// </summary>
        public bool Liberar
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad CorreoEnviado
        /// </summary>
        public int CorreoEnviado
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ValidarInfo()
        {
        }
    }
}
