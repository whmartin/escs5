using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para Contacto entidad
    /// </summary>
    public class ContactoEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdContacto
        /// </summary>
        public int IdContacto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdSucursal
        /// </summary>
        public int IdSucursal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTelContacto
        /// </summary>
        public int IdTelContacto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoDocIdentifica
        /// </summary>
        public int IdTipoDocIdentifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoCargoEntidad
        /// </summary>
        public int IdTipoCargoEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacion
        /// </summary>
        public Decimal NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerNombre
        /// </summary>
        public String PrimerNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoNombre
        /// </summary>
        public String SegundoNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerApellido
        /// </summary>
        public String PrimerApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoApellido
        /// </summary>
        public String SegundoApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Dependencia
        /// </summary>
        public String Dependencia
        {
            get;
            set;
        }
        /// <summary>
        /// Porpiedad Email
        /// </summary>
        public String Email
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ContactoEntidad()
        {
        }


        /*Atributos de lectura tablas relacionadas*/
        public String NombreCompleto
        { 
            get 
            {
                return (PrimerNombre + ' ' + SegundoNombre + ' ' + PrimerApellido + ' ' + SegundoApellido).Trim();
            }
        }
        /// <summary>
        /// Propiedad Celular
        /// </summary>
        public String Celular
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Cargo
        /// </summary>
        public String Cargo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IndicativoTelefono
        /// </summary>
        public string IndicativoTelefono { get; set; }
        /// <summary>
        /// Propiedad NumeroTelefono
        /// </summary>
        public string NumeroTelefono { get; set; }
        /// <summary>
        /// Propiedad ExtensionTelefono
        /// </summary>
        public string ExtensionTelefono { get; set; }
        /// <summary>
        /// Propiedad TelefonoCompleto
        /// </summary>
        public String TelefonoCompleto
        {
            get 
            {
                if (!String.IsNullOrEmpty(NumeroTelefono))
                    return (IndicativoTelefono + '-' + NumeroTelefono + '-' + ExtensionTelefono);
                else
                    return String.Empty;
            }
        }
        /// <summary>
        /// Propiedad Sucursal
        /// </summary>
        public string Sucursal { get; set; }

    }
}
