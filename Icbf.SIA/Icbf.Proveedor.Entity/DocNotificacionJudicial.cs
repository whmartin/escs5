using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para los documentos adjuntos en el módulo de notificación judicial
    /// </summary>
    public class DocNotificacionJudicial : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdDocNotJudicial
        /// </summary>
        public int IdDocNotJudicial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdNotJudicial
        /// </summary>
        public int IdNotJudicial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDocumento
        /// </summary>
        public int IdDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoDocumento
        /// </summary>
        public string  NombreTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad LinkDocumento
        /// </summary>
        public String LinkDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Anno
        /// </summary>
        public int Anno
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MaxPermitidoKB
        /// </summary>
        public String MaxPermitidoKB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ExtensionesPermitidas
        /// </summary>
        public String ExtensionesPermitidas
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public DocNotificacionJudicial()
        {
        }
    }
}
