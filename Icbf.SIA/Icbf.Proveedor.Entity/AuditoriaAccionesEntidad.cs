﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Icbf.Proveedor.Entity
{
    public class AuditoriaAccionesEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// IdAccion
        /// </summary>
        public Int64 IdAccion
        {
            get;
            set;
        }
        /// <summary>
        /// IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Fecha
        /// </summary>
        public DateTime Fecha
        {
            get;
            set;
        }

        /// <summary>
        /// Usuario
        /// </summary>
        public string Usuario
        {
            get;
            set;
        }

        /// <summary>
        /// IdSistema
        /// </summary>
        public int IdSistema
        {
            get;
            set;
        }

        /// <summary>
        /// Sistema
        /// </summary>
        public string Sistema
        {
            get;
            set;
        }

        /// <summary>
        /// Accion
        /// </summary>
        public string Accion
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public AuditoriaAccionesEntidad()
        { }

    }
}
