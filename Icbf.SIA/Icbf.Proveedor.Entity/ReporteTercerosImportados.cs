﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    public class ReporteTercerosImportados
    {
        public string ClaseActividad { get; set; }
        public string NivelInteres { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreRazonSocial { get; set; }
    }
}
