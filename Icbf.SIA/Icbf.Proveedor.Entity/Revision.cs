using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para el m�dulo de revisi�n
    /// </summary>
    public class Revision : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdRevision
        /// </summary>
        public Int32 IdRevision
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public Int32 IdEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Componente
        /// </summary>
        public String Componente
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad NroRevision
        /// </summary>
        public int NroRevision
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Finalizado
        /// </summary>
        public Boolean Finalizado
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public Revision()
        {
        }
    }
}
