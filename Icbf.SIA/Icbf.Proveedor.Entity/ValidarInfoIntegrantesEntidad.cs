﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{    /// <summary>
    /// Clase entidad para validación de datos de Integrantes del Proveedor
    /// </summary>
    [Serializable]
    public class ValidarInfoIntegrantesEntidad : ValidarInfo
    {

        /// <summary>
        /// Propiedad IdValidarInfoIntegrantesEntidad
        /// </summary>
        public Int32 IdValidarInfoIntegrantesEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public Int32 IdEntidad
        {
            get;
            set;
        }
        /// 
        /// <summary>
        /// Constructor
        /// </summary>
        public ValidarInfoIntegrantesEntidad()
        {
        }
    }
}
