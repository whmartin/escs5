using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para la información administrativa
    /// </summary>
    public class InfoAdminEntidad : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdInfoAdmin
        /// </summary>
        public int IdInfoAdmin
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdVigencia
        /// </summary>
        public int ?  IdVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoRegTrib
        /// </summary>
        public int ?  IdTipoRegTrib
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoOrigenCapital
        /// </summary>
        public int ?  IdTipoOrigenCapital
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoActividad
        /// </summary>
        public int ?  IdTipoActividad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoEntidad
        /// </summary>
        public int ?  IdTipoEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoNaturalezaJurid
        /// </summary>
        public int ?  IdTipoNaturalezaJurid
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoRangosTrabajadores
        /// </summary>
        public int ?  IdTipoRangosTrabajadores
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoRangosActivos
        /// </summary>
        public int ?  IdTipoRangosActivos
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdRepLegal
        /// </summary>
        public int ?  IdRepLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoCertificaTamano
        /// </summary>
        public int ?  IdTipoCertificaTamano
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoEntidadPublica
        /// </summary>
        public int ?  IdTipoEntidadPublica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamentoConstituida
        /// </summary>
        public int ?  IdDepartamentoConstituida
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdMunicipioConstituida
        /// </summary>
        public int ?  IdMunicipioConstituida
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamentoDirComercial
        /// </summary>
        public int ?  IdDepartamentoDirComercial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdMunicipioDirComercial
        /// </summary>
        public int ?  IdMunicipioDirComercial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DireccionComercial
        /// </summary>
        public String  DireccionComercial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdZona
        /// </summary>
        public String  IdZona
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad NombreComercial
        /// </summary>
        public String  NombreComercial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEstablecimiento
        /// </summary>
        public String  NombreEstablecimiento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Sigla
        /// </summary>
        public String  Sigla
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PorctjPrivado
        /// </summary>
        public int ?  PorctjPrivado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PorctjPublico
        /// </summary>
        public int ?  PorctjPublico
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SitioWeb
        /// </summary>
        public String  SitioWeb
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEntidadAcreditadora
        /// </summary>
        public String  NombreEntidadAcreditadora
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Organigrama
        /// </summary>
        public Boolean ?  Organigrama
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TotalPnalAnnoPrevio
        /// </summary>
        public int ?  TotalPnalAnnoPrevio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad VincLaboral
        /// </summary>
        public int ?  VincLaboral
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrestServicios
        /// </summary>
        public int ?  PrestServicios
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Voluntariado
        /// </summary>
        public int ?  Voluntariado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad VoluntPermanente
        /// </summary>
        public int ?  VoluntPermanente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Asociados
        /// </summary>
        public int ?  Asociados
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Mision
        /// </summary>
        public int ?  Mision
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PQRS
        /// </summary>
        public Boolean ? PQRS
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad GestionDocumental
        /// </summary>
        public Boolean ? GestionDocumental
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  AuditoriaInterna
        /// </summary>
        public Boolean ?  AuditoriaInterna
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ManProcedimiento
        /// </summary>
        public Boolean ?  ManProcedimiento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ManPracticasAmbiente
        /// </summary>
        public Boolean ?  ManPracticasAmbiente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ManComportOrg
        /// </summary>
        public Boolean ?  ManComportOrg
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ManFunciones
        /// </summary>
        public Boolean ?  ManFunciones
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ProcRegInfoContable
        /// </summary>
        public Boolean ?  ProcRegInfoContable
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  PartMesasTerritoriales
        /// </summary>
        public Boolean ?  PartMesasTerritoriales
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  PartAsocAgremia
        /// </summary>
        public Boolean ?  PartAsocAgremia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  PartConsejosComun
        /// </summary>
        public Boolean ?  PartConsejosComun
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ConvInterInst
        /// </summary>
        public Boolean ?  ConvInterInst
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ProcSeleccGral
        /// </summary>
        public Boolean ?  ProcSeleccGral
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  ProcSeleccEtnico
        /// </summary>
        public Boolean ?  ProcSeleccEtnico
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  PlanInduccCapac
        /// </summary>
        public Boolean ?  PlanInduccCapac
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  EvalDesemp
        /// </summary>
        public Boolean ?  EvalDesemp
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  PlanCualificacion
        /// </summary>
        public Boolean ?  PlanCualificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  NumSedes
        /// </summary>
        public int ?  NumSedes
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  SedesPropias
        /// </summary>
        public Boolean ?  SedesPropias
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  UsuarioCrea
        /// </summary>
        public String  UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  UsuarioModifica
        /// </summary>
        public String  UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  FechaCrea
        /// </summary>
        public DateTime ? FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad  FechaModifica
        /// </summary>
        public DateTime ? FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor 
        /// </summary>
        public InfoAdminEntidad()
        {
        }
    }
}
