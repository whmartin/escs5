using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Oferente.Entity;
namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para los Proveedores Oferente
    /// </summary>
    /// 
    public class EntidadProvOferente : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String ConsecutivoInterno
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public TipoPersona TipoPersona
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public Boolean? TipoEntOfProv
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public Tercero TerceroProveedor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public TelTerceros TelTerceroProveedor
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public InfoAdminEntidad InfoAdminEntidadProv
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public Tercero RepresentanteLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public TelTerceros TelRepresentanteLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoCiiuPrincipal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoCiiuSecundario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoSector
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoClaseEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoRamaPublica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoNivelGob
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoNivelOrganizacional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdTipoCertificadorCalidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaCiiuPrincipal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaCiiuSecundario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaConstitucion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaMatriculaMerc
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaExpiracion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public Boolean? TipoVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public Boolean? ExenMatriculaMer
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String MatriculaMercantil
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String ObserValidador
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? AnoRegistro
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public int? IdEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public DateTime? FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String Proveedor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String TipoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String NumeroIdentificacion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String TipoPersonaNombre
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String IdTemporal
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>  
        public String ActividadCiiuPrincipal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String SectorEntidad                                                                                                          
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String  RegimenTributario
        {
             get;
             set;

        }
        /// <summary>
        /// Propiedad 
        /// </summary>
        public String NombreMunicipio                                 
        {
             get;
             set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public String NombreDepartamento 
        {
             get;
             set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public bool Finalizado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EstadoProveedor
        /// </summary>
        public String EstadoProveedor
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad EstadoProveedor
        /// </summary>
        public String DescEstadoProv
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEstadoProveedor
        /// </summary>
        public int IdEstadoProveedor
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }

        public int DV
        {
            get;
            set;
        }

        public int IdSucursal { get; set; }
        public int IdDepartamento { get; set; }
        public int IdMunicipio { get; set; }
        public string NombreSucursal { get; set; }
        public string DireccionSucursal { get; set; }
        public string CorreoSucursal { get; set; }
        public string IndicativoSucursal { get; set; }
        public string TelefonoSucursal { get; set; }
        public string ExtensionSucursal { get; set; }
        public string CelularSucursal { get; set; }

        /// <summary>
        /// NumIntegrantes 
        /// </summary>
        public int NumIntegrantes
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad 
        /// </summary>
        public Boolean? OferenteMigrado
        {
            get;
            set;
        }

        public DateTime FechaIngresoICBF { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public EntidadProvOferente()
        {
        }

    }
}
