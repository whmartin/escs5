using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para validación de datos básicos del proveedor
    /// </summary>
    [Serializable]
    public class ValidarInfoDatosBasicosEntidad : ValidarInfo
    {

        /// <summary>
        /// Propiedad IdValidarInfoDatosBasicosEntidad
        /// </summary>
        public Int32  IdValidarInfoDatosBasicosEntidad
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public Int32 IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ValidarInfoDatosBasicosEntidad()
        {
        }
    }
}
