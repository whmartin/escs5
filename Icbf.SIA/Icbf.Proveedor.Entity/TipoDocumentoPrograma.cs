using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Proveedor.Entity
{
    /// <summary>
    /// Clase entidad para  tipos documento programa
    /// </summary>
    public class TipoDocumentoPrograma : Icbf.Seguridad.Entity.EntityAuditoria
    {

        /// <summary>
        /// Propiedad IdTipoDocumentoPrograma
        /// </summary>
        public int IdTipoDocumentoPrograma
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoDocumento
        /// </summary>
        public int IdTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoDocumento
        /// </summary>
        public String  CodigoTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdPrograma
        /// </summary>
        public int IdPrograma
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoPrograma
        /// </summary>
        public String CodigoPrograma
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombrePrograma
        /// </summary>
        public String NombrePrograma
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public bool  Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MaxPermitidoKB
        /// </summary>
        public int MaxPermitidoKB
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ExtensionesPermitidas
        /// </summary>
        public String ExtensionesPermitidas
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligRupNoRenovado
        /// </summary>
        public int ObligRupNoRenovado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligRupRenovado
        /// </summary>
        public int ObligRupRenovado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligPersonaJuridica
        /// </summary>
        public int ObligPersonaJuridica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligPersonaNatural
        /// </summary>
        public int ObligPersonaNatural
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligSectorPrivado
        /// </summary>
        public int ObligSectorPrivado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligSectorPublico
        /// </summary>
        public int ObligSectorPublico
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ObligENtidadNAcional
        /// </summary>
        public bool ObligENtidadNAcional
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligENtidadExtrajera
        /// </summary>
        public bool ObligENtidadExtrajera
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ObligConsorcio
        /// </summary>
        public bool ObligConsorcio
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ObligUnionTemporal
        /// </summary>
        public bool ObligUnionTemporal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public TipoDocumentoPrograma()
        {
        }
    }
}
