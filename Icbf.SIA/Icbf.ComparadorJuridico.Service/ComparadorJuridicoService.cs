﻿namespace Icbf.ComparadorJuridico.Service
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using Business;
    using Entity;
    using Utilities.Exceptions;

    public class ComparadorJuridicoService
    {
        UsuarioBLL vUsuarioBLL;
        TipoUsuarioBLL vTipoUsuarioBLL;
        NaturalezaProcesoBLL vNaturalezaProcesoBLL;
        FaseProcesoBLL vFaseProcesoBLL;
        SubFaseProcesoBLL vSubFaseProcesoBLL;
        ProcesosJuridicosBLL vProcesosJuridicos;
        ProcesosJuridicosDocumentosBLL vProcesosJuridicosDocumentos;
        private GeneradorArchivoSIIFBLL _vGeneradorArchivoSiifbll;
        ProcesosJuridicosHistoricoBLL vProcesosJuridicosHistoricoBLL;
        PlanContableCJRBLL vPlanContableCJRBLL;
        VigenciaBLL vVigenciaBLL;
        CuentasContablesNICSPBLL vCuentasContablesNICSPBLL;
        TipoProcesoBLL vTipoProcesoBLL;

        public ComparadorJuridicoService()
        {
            vUsuarioBLL = new Business.UsuarioBLL();
            vTipoUsuarioBLL = new Business.TipoUsuarioBLL();
            _vGeneradorArchivoSiifbll = new GeneradorArchivoSIIFBLL();
            vNaturalezaProcesoBLL = new NaturalezaProcesoBLL();
            vFaseProcesoBLL = new FaseProcesoBLL();
            vSubFaseProcesoBLL = new SubFaseProcesoBLL();
            vProcesosJuridicos = new ProcesosJuridicosBLL();
            vProcesosJuridicosDocumentos = new ProcesosJuridicosDocumentosBLL();
            vProcesosJuridicosHistoricoBLL = new ProcesosJuridicosHistoricoBLL();
            vPlanContableCJRBLL = new PlanContableCJRBLL();
            vVigenciaBLL = new VigenciaBLL();
            vCuentasContablesNICSPBLL = new CuentasContablesNICSPBLL();
            vTipoProcesoBLL = new TipoProcesoBLL();
        }

        public Vigencia ConsultarVigencia(int pIdVigencia)
        {
            try
            {
                return new VigenciaBLL().ConsultarVigencia(pIdVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vigencia> ConsultarVigencias(int? pVigencia, int? pIdMes, String pMes)
        {
            try
            {
                return new VigenciaBLL().ConsultarVigencias(pVigencia, pIdMes, pMes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta todas las vigencias
        /// </summary>
        /// <returns>Lista de Vigencias</returns>
        public List<Vigencia> ConsultarVigenciass()
        {
            try
            {
                return vVigenciaBLL.ConsultarVigenciass();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<IcbRegional> ConsultarIcbRegionals(String pRegNombre, String pRegCodReg, String pRegCnnDb)
        {
            try
            {
                return new IcbRegionalBLL().ConsultarIcbRegionals(pRegNombre, pRegCodReg, pRegCnnDb);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region Procesos Juridicos

        public int InsertarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicos.InsertarProcesosJuridicos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicos.ModificarProcesosJuridicos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarProcesosJuridicos(ProcesosJuridicos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicos.EliminarProcesosJuridicos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ProcesosJuridicos ConsultarProcesosJuridicos(int pIdProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicos.ConsultarProcesosJuridicos(pIdProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ProcesosJuridicos> ConsultarProcesosJuridicoss(int? pIdRegional, int? pIdVigencia, int? pIdProceso,
            int? pIdTercero, string pIdPlanContable, string pNumeroProceso, string pValorProceso, int? pEstado,
            int? pIdNaturaleza, int? pIdFase, int? pIdSubFase)
        {
            try
            {
                return vProcesosJuridicos.ConsultarProcesosJuridicoss(pIdRegional,
                    pIdVigencia, pIdProceso, pIdTercero, pIdPlanContable, pNumeroProceso, pValorProceso, pEstado,
                    pIdNaturaleza, pIdFase, pIdSubFase);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar el correo del contador.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoContador(int? pIdRegional, string pProviderKey)
        {
            try
            {
                return vProcesosJuridicos.ConsultarCorreoContador(pIdRegional, pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar el correo del administrador.
        /// </summary>
        /// <param name="pProviderKey">Filtro por ProviderKey del usuario</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoAdministrador(string pProviderKey)
        {
            try
            {
                return vProcesosJuridicos.ConsultarCorreoAdministrador(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta el CodigoCuenta y Descripcion del Plan Contable
        /// </summary>
        /// <param name="pIdNaturalezaProceso"></param>
        /// <param name="pIdFaseProceso"></param>
        /// <param name="pIdSubFaseProceso"></param>
        /// <param name="pIdProceso"></param>
        /// <returns></returns>
        public ProcesosJuridicos ConsultarProcesosJuridicosPlanContable(int pIdNaturalezaProceso,
            int pIdFaseProceso, int pIdSubFaseProceso, int pIdProceso)
        {
            try
            {
                return vProcesosJuridicos.ConsultarProcesosJuridicosPlanContable(pIdNaturalezaProceso,
                    pIdFaseProceso, pIdSubFaseProceso, pIdProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta si existe registros con la misma información
        /// </summary>
        /// <param name="pIdProceso">Id del Proceso</param>
        /// <param name="pIdNaturalezaProceso">Descripción de la Naturaleza del proceso</param>
        /// <param name="pIdFaseProceso">Descripción de la Fase del proceso</param>
        /// <param name="pIdSubFaseProceso">Descripción de la SubFase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarCombinacion(int pIdProceso, int? pIdNaturalezaProceso, int? pIdFaseProceso, int? pIdSubFaseProceso, int? pIdProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicos.ConsultarCombinacion(pIdProceso, pIdNaturalezaProceso, pIdFaseProceso, pIdSubFaseProceso, pIdProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Procesos Jurídicos Historico

        /// <summary>
        /// Método que consulta el historico de un Proceso Juridico
        /// </summary>
        /// <param name="pIdProcesosJuridicos">Id del dProceso Juridico</param>
        /// <returns>Lista de Registros Historicos del Proceso Jurídico</returns>
        public List<ProcesosJuridicosHistorico> ConsultarProcesosJuridicosDetalle(int pIdProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosHistoricoBLL.ConsultarProcesosJuridicosDetalle(pIdProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta el historico de un Proceso Juridico
        /// </summary>
        /// <param name="pIdProcesosJuridicos">Id del Historico</param>
        /// <returns>Lista de Registros Historicos</returns>
        public ProcesosJuridicosHistorico ConsultarProcesosJuridicosHistorico(int pIdHistorico)
        {
            try
            {
                return vProcesosJuridicosHistoricoBLL.ConsultarProcesosJuridicosHistorico(pIdHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        public int InsertarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                return new PlanContableBLL().InsertarPlanContable(pPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                return new PlanContableBLL().ModificarPlanContable(pPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarPlanContable(PlanContable pPlanContable)
        {
            try
            {
                return new PlanContableBLL().EliminarPlanContable(pPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public PlanContable ConsultarPlanContable(int pIdPlanContable)
        {
            try
            {
                return new PlanContableBLL().ConsultarPlanContable(pIdPlanContable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<PlanContable> ConsultarPlanContables(int? pCodigoCuenta, String pNombreCuenta, int? pCodigoSubcuenta, String pNombreSubcuenta, int? pCodigoTercerNivelCuenta, String pNombreTercerNivelCuenta)
        {
            try
            {
                return new PlanContableBLL().ConsultarPlanContables(pCodigoCuenta, pNombreCuenta, pCodigoSubcuenta, pNombreSubcuenta, pCodigoTercerNivelCuenta, pNombreTercerNivelCuenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarTercero(Tercero pTercero)
        {
            try
            {
                return new TerceroBLL().InsertarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTercero(Tercero pTercero)
        {
            try
            {
                return new TerceroBLL().ModificarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTercero(Tercero pTercero)
        {
            try
            {
                return new TerceroBLL().EliminarTercero(pTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Tercero ConsultarTercero(int pIdTercero)
        {
            try
            {
                return new TerceroBLL().ConsultarTercero(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Tercero> ConsultarTerceros(int? pIdTipoDocumento, String pNumeroDocumento, String pPrimerNombreTercero, String pSegundoNombreTercero, String pPrimerApellidoTercero, String pSegundoApellidoTercero, String pRazonSocial)
        {
            try
            {
                return new TerceroBLL().ConsultarTerceros(pIdTipoDocumento, pNumeroDocumento, pPrimerNombreTercero, pSegundoNombreTercero, pPrimerApellidoTercero, pSegundoApellidoTercero, pRazonSocial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(int pIdUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Usuario ConsultarUsuario(string pNombreUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuario(pNombreUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoUsuario> ConsultarTipoUsuarios(String pCodigoTipoUsuario, String pNombreTipoUsuario, String pEstado)
        {
            try
            {
                return vTipoUsuarioBLL.ConsultarTipoUsuarios(pCodigoTipoUsuario, pNombreTipoUsuario, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Usuario> ConsultarUsuarios(String pNumeroDocumento, String pPrimerNombre, String pPrimerApellido, int pPerfil, Boolean? pEstado, Int32? pTipoUsuario)
        {
            try
            {
                return vUsuarioBLL.ConsultarUsuarios(pNumeroDocumento, pPrimerNombre, pPrimerApellido, pPerfil, pEstado, pTipoUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarUsuario(Usuario pUsuario)
        {
            try
            {
                return vUsuarioBLL.InsertarUsuario(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarUsuario(Usuario pUsuario)
        {
            try
            {
                return vUsuarioBLL.ModificarUsuario(pUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TipoDocumento> ConsultarTodosTipoDocumento()
        {
            try
            {
                return new TipoDocumentoBLL().ConsultarTodosTipoDocumento();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public string CerrarVigencia(string NombreUsuario)
        {
            try
            {
                return new CierreVigenciaBLL().CerrarVigencia(NombreUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region GeneracionArchivoSIIF

        public List<GeneradorArchivoSIIF> ConsultarGeneradorArchivoSIIFs(String pTipoArchivo, DateTime? pFechaGeneracion, String pNombreArchivo, int? pIdRegional, DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                return _vGeneradorArchivoSiifbll.ConsultarGeneradorArchivoSIIFs(pTipoArchivo, pFechaGeneracion, pNombreArchivo, pIdRegional, pFechaInicio, pFechaFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public DataSet GeneracionArchivoJuridica(DateTime pFechaInicialMov, DateTime pFechaFinMov, int pIdRegional, string usuario)
        {
            try
            {
                return _vGeneradorArchivoSiifbll.GeneracionArchivoJuridica(pFechaInicialMov, pFechaFinMov, pIdRegional, usuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DataSet ConsultarRegistrosAGenerarSiiF(DateTime pFechaInicialMov, DateTime pFechaFinMov, int pIdRegional)
        {
            try
            {
                return _vGeneradorArchivoSiifbll.ConsultarRegistrosAGenerarSiiF(pFechaInicialMov, pFechaFinMov, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region Naturaleza del Proceso

        /// <summary>
        /// Consulta los datos de la Naturaleza del Proceso
        /// </summary>
        /// <param name="pCodigoNaturaleza">Código de la Naturaleza</param>
        /// <param name="pDescripcionNaturaleza">Descripción de la Naturaleza</param>        
        /// <param name="pEstado">Estado de la Naturaleza</param>
        /// <returns>Lista de Registros de Naturaleza del Proceso</returns>
        public List<NaturalezaProceso> ConsultarNaturalezaProceso(int? pIdNaturalezaProceso, string pCodigoNaturaleza, string pDescripcionNaturaleza, int? pEstado)
        {
            try
            {
                return vNaturalezaProcesoBLL.ConsultarNaturalezaProceso(pIdNaturalezaProceso, pCodigoNaturaleza, pDescripcionNaturaleza, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de Naturaleza Proceso
        /// </summary>
        /// <param name="pIdNaturaleza">Id de la Naturaleza del proceso</param>
        /// <param name="pDescripcionNaturalezaProceso">Nombre de la naturaleza del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarNaturalezaProcesoNombre(int? pIdNaturaleza, string pDescripcionNaturalezaProceso)
        {
            try
            {
                return vNaturalezaProcesoBLL.ConsultarNaturalezaProcesoNombre(pIdNaturaleza, pDescripcionNaturalezaProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de Naturaleza del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarNaturalezaproceso(NaturalezaProceso pNaturalezaProceso)
        {
            try
            {
                return vNaturalezaProcesoBLL.InsertarNaturalezaproceso(pNaturalezaProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Modificación de Naturaleza del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarNaturalezaproceso(NaturalezaProceso pNaturalezaProceso)
        {
            try
            {
                return vNaturalezaProcesoBLL.ModificarNaturalezaproceso(pNaturalezaProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la Naturaleza
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoNaturaleza()
        {
            try
            {
                return vNaturalezaProcesoBLL.ObtenerCodigoNaturaleza();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Naturaleza del Proceso Activos y concatenados por codigo y descripción
        /// </summary>
        /// <returns>Lista de Registros de Naturaleza del Proceso</returns>
        public List<NaturalezaProceso> ConsultarNaturalezaProcesoConcatenado()
        {
            try
            {
                return vNaturalezaProcesoBLL.ConsultarNaturalezaProcesoConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Fase del Proceso

        /// <summary>
        /// Consulta los datos de la Fase del Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase</param>
        /// <param name="pCodigoFaseProceso">Código de la Fase</param>
        /// <param name="pDescripcionFaseProceso">Descripción de la Fase del Proceso</param>
        /// <param name="pEstado">Estado de la Fase</param>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public List<FaseProceso> ConsultarFaseProceso(int? pIdFaseProceso, string pCodigoFaseProceso, string pDescripcionFaseProceso, int pEstado)
        {
            try
            {
                return vFaseProcesoBLL.ConsultarFaseProceso(pIdFaseProceso, pCodigoFaseProceso, pDescripcionFaseProceso, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de Fase Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase del proceso</param>
        /// <param name="pDescripcionFaseProceso">Descripción de la Fase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarFaseProcesoNombre(int? pIdFaseProceso, string pDescripcionFaseProceso)
        {
            try
            {
                return vFaseProcesoBLL.ConsultarFaseProcesoNombre(pIdFaseProceso, pDescripcionFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarFaseproceso(FaseProceso pFaseProceso)
        {
            try
            {
                return vFaseProcesoBLL.InsertarFaseproceso(pFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Modificación de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarFaseProceso(FaseProceso pFaseProceso)
        {
            try
            {
                return vFaseProcesoBLL.ModificarFaseproceso(pFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la Fase
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoFase()
        {
            try
            {
                return vFaseProcesoBLL.ObtenerCodigoFase();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Fase del Proceso Contatenado Código-Descripción
        /// </summary>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public List<FaseProceso> ConsultarFaseProcesoConcatenado()
        {
            try
            {
                return vFaseProcesoBLL.ConsultarFaseProcesoConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region SubFase del Proceso

        /// <summary>
        /// Consulta los datos de la SubFase del Proceso
        /// </summary>
        /// <param name="pIdSubFaseProceso">Id de la SubFase</param>
        /// <param name="pCodigoSubFaseProceso">Código de la SubFase</param>
        /// <param name="pDescripcionSubFaseProceso">Descripción de la SubFase del Proceso</param>
        /// <param name="pEstado">Estado de la SubFase</param>
        /// <returns>Lista de Registros de SubFase del Proceso</returns>
        public List<SubFaseProceso> ConsultarSubFaseProceso(int? pIdSubFaseProceso, string pCodigoSubFaseProceso, string pDescripcionSubFaseProceso, int pEstado)
        {
            try
            {
                return vSubFaseProcesoBLL.ConsultarSubFaseProceso(pIdSubFaseProceso, pCodigoSubFaseProceso, pDescripcionSubFaseProceso, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Nombre de SubFase Proceso
        /// </summary>
        /// <param name="pIdSubFaseProceso">Id de la SubFase del proceso</param>
        /// <param name="pDescripcionSubFaseProceso">Descripción de la SubFase del proceso</param>
        /// <returns>Valor indicando si existe el registro</returns>
        public int ConsultarSubFaseProcesoNombre(int? pIdSubFaseProceso, string pDescripcionSubFaseProceso)
        {
            try
            {
                return vSubFaseProcesoBLL.ConsultarSubFaseProcesoNombre(pIdSubFaseProceso, pDescripcionSubFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método parala inserción de SubFase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int InsertarSubFaseproceso(SubFaseProceso pSubFaseProceso)
        {
            try
            {
                return vSubFaseProcesoBLL.InsertarSubFaseproceso(pSubFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Modificación de SubFase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int ModificarSubFaseProceso(SubFaseProceso pSubFaseProceso)
        {
            try
            {
                return vSubFaseProcesoBLL.ModificarSubFaseproceso(pSubFaseProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Obtiene el código de la SubFase
        /// </summary>
        /// <returns>Código del Registro a Insertar</returns>
        public string ObtenerCodigoSubFase()
        {
            try
            {
                return vSubFaseProcesoBLL.ObtenerCodigoSubFase();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la SubFase del Proceso Contactenado Código-Descripción
        /// </summary>
        /// <returns>Lista de Registros de SubFase del Proceso</returns>
        public List<SubFaseProceso> ConsultarSubFaseProcesoConcatenado()
        {
            try
            {
                return vSubFaseProcesoBLL.ConsultarSubFaseProcesoConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region ProcesosJuridicosDocumentosBLL

        /// <summary>
        /// Elimina los documentos Asociados al Proceso Juridico
        /// </summary>
        /// <param name="pProcesosJuridicos">Parametro que envía el IdProcesosJuridicos</param>
        /// <returns>Resultado de la Operación</returns>
        public int EliminarProcesosJuridicosDocumentos(ProcesosJuridicosDocumentos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosDocumentos.EliminarProcesosJuridicosDocumentos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta los documentos Asociados al Proceso Juridico
        /// </summary>
        /// <param name="pProcesosJuridicos">Parametro de tipo Entidad ProcesosJuridicosDocumentos</param>
        /// <returns>Resultado de la Operación</returns>
        public int InsertarProcesosJuridicosDocumentos(ProcesosJuridicosDocumentos pProcesosJuridicos)
        {
            try
            {
                return vProcesosJuridicosDocumentos.InsertarProcesosJuridicosDocumentos(pProcesosJuridicos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los Documentos x IdProcesoJuridico
        /// </summary>
        /// <param name="pIdProcesoJuridico"></param>
        /// <returns>Lista de Documentos x Proceso Juridico</returns>
        public List<ProcesosJuridicosDocumentos> ConsultaDocumentosProcesosJuridicos(int pIdProcesoJuridico)
        {
            try
            {
                return vProcesosJuridicosDocumentos.ConsultaDocumentosProcesosJuridicos(pIdProcesoJuridico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region PlanContableCJR

        /// <summary>
        /// Consulta los datos del Plan Contable Concatenados
        /// </summary>
        /// <returns>Lista de Registros de Plan Contable Concatenados</returns>
        public List<PlanContableCJR> ConsultarPlanContableConcatenado()
        {
            try
            {
                return vPlanContableCJRBLL.ConsultarPlanContableConcatenado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion

        #region Cuenta Contable NICSP

        /// <summary>
        /// Consulta los datos de la Fase del Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase</param>
        /// <param name="pCodigoFaseProceso">Código de la Fase</param>
        /// <param name="pDescripcionFaseProceso">Descripción de la Fase del Proceso</param>
        /// <param name="pEstado">Estado de la Fase</param>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public IEnumerable<CuentasContablesNICSP> ConsultarCuentasContablesNICSP(
            int? pIdTipoProceso,
            int? pIdCuentasContablesNICSP,
            int? pIdFaseProceso,
            int? pIdSubFaseProceso,
            int? pIdNaturalezaProceso,
            int? pIdCodCtaConSiifConIntArea,
            int? pIdCodCtaConSiifDebito,
            int? pIdCodCtaConSiifCredito,
            bool? pEstado)
        {
            try
            {
                return vCuentasContablesNICSPBLL.ConsultarCuentasContablesNICSP(pIdCuentasContablesNICSP, pIdTipoProceso, pIdFaseProceso, pIdSubFaseProceso, pIdNaturalezaProceso, pIdCodCtaConSiifConIntArea, pIdCodCtaConSiifDebito, pIdCodCtaConSiifCredito, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los datos de la Fase del Proceso
        /// </summary>
        /// <param name="pIdFaseProceso">Id de la Fase</param>
        /// <param name="pCodigoFaseProceso">Código de la Fase</param>
        /// <param name="pDescripcionFaseProceso">Descripción de la Fase del Proceso</param>
        /// <param name="pEstado">Estado de la Fase</param>
        /// <returns>Lista de Registros de Fase del Proceso</returns>
        public IEnumerable<CuentasContablesNICSPResumen> ConsultarCuentasContablesNICSPResumen(
            int? pIdTipoProceso,
            int? pIdCuentasContablesNICSP,
            int? pIdFaseProceso,
            int? pIdSubFaseProceso,
            int? pIdNaturalezaProceso,
            int? pIdCodCtaConSiifConIntArea,
            int? pIdCodCtaConSiifDebito,
            int? pIdCodCtaConSiifCredito,
            bool? pEstado)
        {
            try
            {
                return vCuentasContablesNICSPBLL.ConsultarCuentasContablesNICSPResumen(pIdCuentasContablesNICSP, pIdTipoProceso, pIdFaseProceso, pIdSubFaseProceso, pIdNaturalezaProceso, pIdCodCtaConSiifConIntArea, pIdCodCtaConSiifDebito, pIdCodCtaConSiifCredito, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método parala inserción de Fase del Proceso.
        /// </summary>
        /// <returns>Resultado de la operación</returns>
        public int GuardarCuentasContablesNICSP(CuentasContablesNICSP pCuentasContablesNICSP)
        {
            try
            {
                return vCuentasContablesNICSPBLL.GuardarCuentasContablesNICSP(pCuentasContablesNICSP);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de cuentas contables de la base de datos de NMF
        /// </summary>
        /// <param name="pCodCtaContSiif">Código cuenta contable</param>
        /// <param name="pDescCtaContSiif">Descripción cuenta contable</param>
        /// <returns>Una lista con las cuentas contables que coincidan con los filtros seleccionados</returns>
        public List<TCON01Resumido> ConsultarTCON01(string pCodCtaContSiif = "", string pDescCtaContSiif = "")
        {
            try
            {
                return vCuentasContablesNICSPBLL.ConsultarTCON01(pCodCtaContSiif, pDescCtaContSiif);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para validar si existe una cuenta parametrizada
        /// </summary>
        /// <param name="pIdCuentasContablesNICSP">Identificador de cuentas contables NICSP</param>
        /// <param name="pIdTipoProceso">Id tipo de proceso</param>
        /// <param name="pIdCodCtaConSiif">Id cuenta contable debito/credito</param>
        /// <returns>True si existe ó False si no existe</returns>
        public bool ValidaContablesNICSP(
           int pIdCuentasContablesNICSP,
           int pIdTipoProceso,
           int pIdCodCtaConSiifDebito,
           int pIdCodCtaConSiifCredito)
        {
            try
            {
                return vCuentasContablesNICSPBLL.ValidaContablesNICSP(pIdCuentasContablesNICSP, pIdTipoProceso, pIdCodCtaConSiifDebito, pIdCodCtaConSiifCredito);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar todos los tipos de proceso
        /// </summary>
        /// <returns>Lista con los todos los tipos de proceso</returns>
        public List<TipoProceso> ConsultarTodosTipoProceso()
        {
            try
            {
                return vTipoProcesoBLL.ConsultarTodosTipoProceso(); ;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
    }
}

