﻿CREATE TABLE [AUDITA].[LogSIA] (
    [idLog]               NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [fecha]               DATETIME       NOT NULL,
    [usuario]             NVARCHAR (250) NOT NULL,
    [programa]            NVARCHAR (200) NOT NULL,
    [operacion]           NVARCHAR (20)  NOT NULL,
    [parametrosOperacion] TEXT           NOT NULL,
    [tabla]               NVARCHAR (50)  NOT NULL,
    [idRegistro]          NUMERIC (18)   NOT NULL,
    [direccionIp]         NVARCHAR (20)  NOT NULL,
    [navegador]           NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_LogSIA] PRIMARY KEY CLUSTERED ([idLog] ASC) WITH (FILLFACTOR = 90)
);

