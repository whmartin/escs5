﻿CREATE TABLE [AUDITA].[LogAccionesDatosBasicos] (
    [IdAccion]  NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [IdEntidad] INT            NULL,
    [Fecha]     DATETIME       NULL,
    [Usuario]   NVARCHAR (250) NULL,
    [IdSistema] INT            NULL,
    [Sistema]   NVARCHAR (500) NULL,
    [Accion]    NVARCHAR (50)  NULL,
    CONSTRAINT [PK_AUDITA.LogAccionesDatosBasicos] PRIMARY KEY CLUSTERED ([IdAccion] ASC),
    CONSTRAINT [FK_LogAccionesDatosBasicos_EntidadProvOferente] FOREIGN KEY ([IdEntidad]) REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
);

