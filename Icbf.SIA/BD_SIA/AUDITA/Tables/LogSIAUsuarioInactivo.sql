﻿CREATE TABLE [AUDITA].[LogSIAUsuarioInactivo] (
    [IDLog]                 NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [Usuario]               NVARCHAR (250) NOT NULL,
    [UsuarioCreacion]       NVARCHAR (250) NOT NULL,
    [FechaCreacion]         DATETIME       NOT NULL,
    [TipoDocumento]         NVARCHAR (100) NOT NULL,
    [NumeroDocumento]       NVARCHAR (20)  NOT NULL,
    [DV]                    NVARCHAR (1)   NOT NULL,
    [PrimerNombre]          NVARCHAR (150) NULL,
    [SegundoNombre]         NVARCHAR (150) NULL,
    [PrimerApellido]        NVARCHAR (150) NULL,
    [SegundoApellido]       NVARCHAR (150) NULL,
    [RazonSocial]           NVARCHAR (150) NULL,
    [Estado]                BIT            NULL,
    [TipoOperacion]         NVARCHAR (20)  NULL,
    [FechaOperacion]        DATETIME       NULL,
    [ValorMesesEliminacion] NVARCHAR (5)   NULL,
    [ValorDiasNotificacion] NVARCHAR (5)   NULL,
    [Motivo]                NVARCHAR (250) NULL,
    CONSTRAINT [PK_LogSIAUsuarioInactivo] PRIMARY KEY CLUSTERED ([IDLog] ASC) WITH (FILLFACTOR = 90)
);

