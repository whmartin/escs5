﻿CREATE TABLE [Tercero].[TipoPersona] (
    [IdTipoPersona]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoTipoPersona] NVARCHAR (128) NOT NULL,
    [NombreTipoPersona] NVARCHAR (256) NOT NULL,
    [Estado]            BIT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (256) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    [UsuarioModifica]   NVARCHAR (256) NULL,
    [FechaModifica]     DATETIME       NULL,
    CONSTRAINT [PK_TipoPersona] PRIMARY KEY CLUSTERED ([IdTipoPersona] ASC)
);

