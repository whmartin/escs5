﻿CREATE TABLE [Tercero].[TERCERO] (
    [IDTERCERO]            INT              IDENTITY (1, 1) NOT NULL,
    [IDTIPODOCIDENTIFICA]  INT              NULL,
    [IDESTADOTERCERO]      INT              NULL,
    [IdTipoPersona]        INT              NULL,
    [ProviderUserKey]      UNIQUEIDENTIFIER NULL,
    [NUMEROIDENTIFICACION] NVARCHAR (256)   NULL,
    [DIGITOVERIFICACION]   INT              NULL,
    [CORREOELECTRONICO]    NVARCHAR (256)   NULL,
    [PRIMERNOMBRE]         NVARCHAR (256)   NULL,
    [SEGUNDONOMBRE]        NVARCHAR (256)   NULL,
    [PRIMERAPELLIDO]       NVARCHAR (256)   NULL,
    [SEGUNDOAPELLIDO]      NVARCHAR (256)   NULL,
    [RAZONSOCIAL]          NVARCHAR (256)   NULL,
    [FECHAEXPEDICIONID]    DATETIME         NULL,
    [FECHANACIMIENTO]      DATETIME         NULL,
    [SEXO]                 CHAR (1)         NULL,
    [FECHACREA]            DATETIME         NOT NULL,
    [USUARIOCREA]          NVARCHAR (256)   NULL,
    [FECHAMODIFICA]        DATETIME         NULL,
    [USUARIOMODIFICA]      NVARCHAR (256)   NULL,
    [EsFundacion]          BIT              NULL,
    [ConsecutivoInterno]   VARCHAR (16)     NULL,
    [CreadoPorInterno]     BIT              CONSTRAINT [CreadoPorInterno_DEFAULT] DEFAULT ((1)) NULL,
    [IdTerceroEnOferentes] INT              NULL,
    CONSTRAINT [PK_TERCERO] PRIMARY KEY CLUSTERED ([IDTERCERO] ASC),
    CONSTRAINT [FK_TBL_TERCERO_EstadoTercero] FOREIGN KEY ([IDESTADOTERCERO]) REFERENCES [Tercero].[EstadoTercero] ([IdEstadoTercero]),
    CONSTRAINT [UN_Terceros] UNIQUE NONCLUSTERED ([IDTIPODOCIDENTIFICA] ASC, [NUMEROIDENTIFICACION] ASC)
);


GO

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 25-FEB-2014
-- Description:	Update of ConsecutivoInterno column AFTER insert a row on Terceros table
-- =============================================
CREATE TRIGGER [Tercero].[descUpdateConsecutivoInterno]
   ON  [Tercero].[TERCERO] FOR INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @idTercero INT = (SELECT IDTERCERO FROM INSERTED),
	@txtIdTercero VARCHAR(8),
	@anioCreacion VARCHAR(4),
	@mesCreacion VARCHAR(2)
	
	SELECT 
	@txtIdTercero=(RIGHT('0000000'+CAST([IDTERCERO] AS varchar), 8)),
	@anioCreacion=YEAR([FECHACREA]),
	@mesCreacion=(RIGHT('0'+CAST(MONTH([FECHACREA]) AS varchar), 2))
	FROM [Oferente].[TERCERO]
	WHERE IDTERCERO=@idTercero
	
	UPDATE Oferente.TERCERO
	SET ConsecutivoInterno=('T'+@anioCreacion+@mesCreacion+'-'+@txtIdTercero)
	WHERE IDTERCERO=@idTercero
	
	

END

