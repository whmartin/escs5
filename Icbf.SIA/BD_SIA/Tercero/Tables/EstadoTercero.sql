﻿CREATE TABLE [Tercero].[EstadoTercero] (
    [IdEstadoTercero]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoEstadotercero] NVARCHAR (20)  NOT NULL,
    [DescripcionEstado]   NVARCHAR (256) NOT NULL,
    [Estado]              BIT            NOT NULL,
    [UsuarioCrea]         NVARCHAR (256) NOT NULL,
    [FechaCrea]           DATETIME       NOT NULL,
    [UsuarioModifica]     NVARCHAR (256) NULL,
    [FechaModifica]       DATETIME       NULL,
    CONSTRAINT [PK_EstadoTercero] PRIMARY KEY CLUSTERED ([IdEstadoTercero] ASC)
);

