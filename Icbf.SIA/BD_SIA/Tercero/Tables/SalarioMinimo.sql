﻿CREATE TABLE [Tercero].[SalarioMinimo] (
    [IdSalarioMinimo] INT            IDENTITY (1, 1) NOT NULL,
    [Año]             INT            NOT NULL,
    [Valor]           INT            NOT NULL,
    [Estado]          BIT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_SalarioMinimo] PRIMARY KEY CLUSTERED ([IdSalarioMinimo] ASC)
);

