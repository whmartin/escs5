﻿CREATE TABLE [Tercero].[TelTerceros] (
    [IdTelTercero]            INT            IDENTITY (1, 1) NOT NULL,
    [IdTercero]               INT            NULL,
    [IndicativoTelefono]      INT            NULL,
    [NumeroTelefono]          INT            NULL,
    [ExtensionTelefono]       BIGINT         NULL,
    [Movil]                   BIGINT         NULL,
    [IndicativoFax]           INT            NULL,
    [NumeroFax]               INT            NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    [CorreoSede]              NVARCHAR (128) NULL,
    [IdTelTerceroEnOferentes] INT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TelTerceros] PRIMARY KEY CLUSTERED ([IdTelTercero] ASC),
    CONSTRAINT [FK_TelTerceros_TERCERO] FOREIGN KEY ([IdTercero]) REFERENCES [Tercero].[TERCERO] ([IDTERCERO])
);

