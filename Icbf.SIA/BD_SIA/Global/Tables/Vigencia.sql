﻿CREATE TABLE [Global].[Vigencia] (
    [IdVigencia]      INT            IDENTITY (1, 1) NOT NULL,
    [AcnoVigencia]    INT            NOT NULL,
    [Activo]          NVARCHAR (1)   NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Vigencia] PRIMARY KEY CLUSTERED ([IdVigencia] ASC) WITH (FILLFACTOR = 90)
);

