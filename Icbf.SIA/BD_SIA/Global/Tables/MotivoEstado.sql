﻿CREATE TABLE [Global].[MotivoEstado] (
    [IdMotivoEstado]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoMotivoEstado] NVARCHAR (128) NOT NULL,
    [NombreMotivoEstado] NVARCHAR (256) NOT NULL,
    [Estado]             NVARCHAR (1)   NOT NULL,
    [UsuarioCrea]        NVARCHAR (250) NOT NULL,
    [FechaCrea]          DATETIME       NOT NULL,
    [UsuarioModifica]    NVARCHAR (250) NULL,
    [FechaModifica]      DATETIME       NULL,
    CONSTRAINT [PK_MotivoEstado] PRIMARY KEY CLUSTERED ([IdMotivoEstado] ASC) WITH (FILLFACTOR = 90)
);

