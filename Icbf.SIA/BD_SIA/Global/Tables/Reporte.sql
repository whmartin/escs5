﻿CREATE TABLE [Global].[Reporte] (
    [IdReporte]       INT             IDENTITY (1, 1) NOT NULL,
    [NombreReporte]   NVARCHAR (512)  NOT NULL,
    [Descripcion]     NVARCHAR (1024) NULL,
    [Servidor]        NVARCHAR (512)  NOT NULL,
    [Carpeta]         NVARCHAR (512)  NOT NULL,
    [NombreArchivo]   NVARCHAR (512)  NOT NULL,
    [UsuarioCrea]     NVARCHAR (250)  NOT NULL,
    [FechaCrea]       DATETIME        NOT NULL,
    [UsuarioModifica] NVARCHAR (250)  NULL,
    [FechaModifica]   DATETIME        NULL,
    CONSTRAINT [PK_Reporte] PRIMARY KEY CLUSTERED ([IdReporte] ASC)
);

