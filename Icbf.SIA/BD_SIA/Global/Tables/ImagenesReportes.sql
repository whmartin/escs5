﻿CREATE TABLE [Global].[ImagenesReportes] (
    [IdImagen]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoImagen] NVARCHAR (256) NOT NULL,
    [RutaImagen]   NVARCHAR (256) NOT NULL,
    [Descripcion]  NVARCHAR (256) NULL,
    CONSTRAINT [PK_ImagenesReportes] PRIMARY KEY CLUSTERED ([IdImagen] ASC),
    UNIQUE NONCLUSTERED ([CodigoImagen] ASC)
);

