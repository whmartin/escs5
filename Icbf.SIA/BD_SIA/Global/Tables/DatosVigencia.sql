﻿CREATE TABLE [Global].[DatosVigencia] (
    [IdDatosVigencia] INT             IDENTITY (1, 1) NOT NULL,
    [Codigo]          NVARCHAR (250)  NOT NULL,
    [Descripcion]     NVARCHAR (50)   NOT NULL,
    [FechaInicial]    DATETIME        NOT NULL,
    [Valor]           INT             NULL,
    [Porcentaje]      DECIMAL (12, 6) NULL,
    [Normatividad]    NVARCHAR (256)  NULL,
    [BaseRenta]       NVARCHAR (250)  NULL,
    [Estado]          INT             NOT NULL,
    [UsuarioCrea]     NVARCHAR (250)  NOT NULL,
    [FechaCrea]       DATETIME        NOT NULL,
    [UsuarioModifica] NVARCHAR (250)  NULL,
    [FechaModifica]   DATETIME        NULL,
    CONSTRAINT [PK_DatosVigencia] PRIMARY KEY CLUSTERED ([IdDatosVigencia] ASC)
);

