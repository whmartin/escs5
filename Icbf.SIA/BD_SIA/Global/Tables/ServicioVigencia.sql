﻿CREATE TABLE [Global].[ServicioVigencia] (
    [IdServicioVigencia] INT            IDENTITY (1, 1) NOT NULL,
    [IdVigencia]         INT            NOT NULL,
    [IdServicio]         INT            NOT NULL,
    [IdRubro]            INT            NOT NULL,
    [CodigoServicio]     NVARCHAR (128) NOT NULL,
    [NombreServicio]     NVARCHAR (256) NOT NULL,
    [CodigoRubro]        NVARCHAR (128) NOT NULL,
    [NombreRubro]        NVARCHAR (256) NOT NULL,
    [Estado]             NVARCHAR (1)   NOT NULL,
    [UsuarioCrea]        NVARCHAR (250) NOT NULL,
    [FechaCrea]          DATETIME       NOT NULL,
    [UsuarioModifica]    NVARCHAR (250) NULL,
    [FechaModifica]      DATETIME       NULL,
    CONSTRAINT [PK_ServicioVigencia] PRIMARY KEY CLUSTERED ([IdServicioVigencia] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ServicioVigencia_Vigencia] FOREIGN KEY ([IdVigencia]) REFERENCES [Global].[Vigencia] ([IdVigencia])
);

