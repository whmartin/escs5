﻿CREATE TABLE [Global].[Areas] (
    [IdArea]          INT            IDENTITY (1, 1) NOT NULL,
    [IdRegional]      INT            NOT NULL,
    [NombreArea]      NVARCHAR (120) NOT NULL,
    [CodArea]         INT            NULL,
    [EstadoArea]      INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED ([IdArea] ASC)
);

