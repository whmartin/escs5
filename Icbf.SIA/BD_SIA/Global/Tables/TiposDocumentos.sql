﻿CREATE TABLE [Global].[TiposDocumentos] (
    [IdTipoDocumento]  INT           IDENTITY (1, 1) NOT NULL,
    [CodDocumento]     VARCHAR (10)  NOT NULL,
    [NomTipoDocumento] VARCHAR (50)  NULL,
    [codSiif]          NVARCHAR (16) NULL,
    CONSTRAINT [PK_TIPOS_DOCUMENTOS] PRIMARY KEY CLUSTERED ([IdTipoDocumento] ASC) WITH (FILLFACTOR = 90)
);

