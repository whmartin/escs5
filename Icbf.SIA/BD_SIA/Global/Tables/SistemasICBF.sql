﻿CREATE TABLE [Global].[SistemasICBF] (
    [IdSistema]      INT            IDENTITY (1, 1) NOT NULL,
    [Sistema]        NVARCHAR (200) NULL,
    [NombreCompleto] NVARCHAR (500) NULL,
    CONSTRAINT [PK_Global.SistemasICBF] PRIMARY KEY CLUSTERED ([IdSistema] ASC)
);

