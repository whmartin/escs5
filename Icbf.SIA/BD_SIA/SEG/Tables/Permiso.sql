﻿CREATE TABLE [SEG].[Permiso] (
    [IdPermiso]           INT            IDENTITY (1, 1) NOT NULL,
    [IdPrograma]          INT            NOT NULL,
    [IdRol]               INT            NOT NULL,
    [Insertar]            BIT            NOT NULL,
    [Modificar]           BIT            NOT NULL,
    [Eliminar]            BIT            NOT NULL,
    [Consultar]           BIT            NOT NULL,
    [UsuarioCreacion]     NVARCHAR (250) NOT NULL,
    [FechaCreacion]       DATETIME       NOT NULL,
    [UsuarioModificacion] NVARCHAR (250) NULL,
    [FechaModificacion]   DATETIME       NULL,
    CONSTRAINT [PK_Permiso] PRIMARY KEY CLUSTERED ([IdPermiso] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Permiso_Programa] FOREIGN KEY ([IdPrograma]) REFERENCES [SEG].[Programa] ([IdPrograma]),
    CONSTRAINT [FK_Permiso_Rol] FOREIGN KEY ([IdRol]) REFERENCES [SEG].[Rol] ([IdRol])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [Permiso_Programa_Rol]
    ON [SEG].[Permiso]([IdPrograma] ASC, [IdRol] ASC);

