﻿CREATE TABLE [SEG].[ProgramaRol] (
    [IdPrograma]      INT            NOT NULL,
    [IdRol]           INT            NOT NULL,
    [UsuarioCreacion] NVARCHAR (250) NOT NULL,
    [FechaCreacion]   DATETIME       NOT NULL,
    CONSTRAINT [PK_ProgramaRol] PRIMARY KEY CLUSTERED ([IdPrograma] ASC, [IdRol] ASC) WITH (FILLFACTOR = 90)
);

