﻿CREATE TABLE [SEG].[Programa] (
    [IdPrograma]          INT            IDENTITY (1, 1) NOT NULL,
    [IdModulo]            INT            NOT NULL,
    [NombrePrograma]      NVARCHAR (250) NOT NULL,
    [CodigoPrograma]      NVARCHAR (250) NOT NULL,
    [Posicion]            INT            NOT NULL,
    [Estado]              INT            NOT NULL,
    [UsuarioCreacion]     NVARCHAR (250) NOT NULL,
    [FechaCreacion]       DATETIME       NOT NULL,
    [UsuarioModificacion] NVARCHAR (250) NULL,
    [FechaModificacion]   DATETIME       NULL,
    [VisibleMenu]         BIT            DEFAULT ((1)) NOT NULL,
    [generaLog]           BIT            DEFAULT ((1)) NOT NULL,
    [Padre]               INT            NULL,
    [EsReporte]           INT            NULL,
    [IdReporte]           INT            NULL,
    CONSTRAINT [PK_Programa] PRIMARY KEY CLUSTERED ([IdPrograma] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Programa_Modulo] FOREIGN KEY ([IdModulo]) REFERENCES [SEG].[Modulo] ([IdModulo])
);

