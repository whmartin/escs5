﻿CREATE TABLE [SEG].[Parametro] (
    [IdParametro]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreParametro] NVARCHAR (128) NOT NULL,
    [ValorParametro]  NVARCHAR (256) NOT NULL,
    [ImagenParametro] NVARCHAR (256) NULL,
    [Estado]          BIT            NOT NULL,
    [Funcionalidad]   NVARCHAR (128) NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Parametro] PRIMARY KEY CLUSTERED ([IdParametro] ASC)
);

