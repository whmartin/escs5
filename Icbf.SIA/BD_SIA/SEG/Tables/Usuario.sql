﻿CREATE TABLE [SEG].[Usuario] (
    [IdUsuario]                INT            IDENTITY (1, 1) NOT NULL,
    [providerKey]              VARCHAR (125)  NOT NULL,
    [IdTipoDocumento]          INT            NOT NULL,
    [NumeroDocumento]          NVARCHAR (17)  NOT NULL,
    [PrimerNombre]             NVARCHAR (150) NOT NULL,
    [SegundoNombre]            NVARCHAR (150) NULL,
    [PrimerApellido]           NVARCHAR (150) NOT NULL,
    [SegundoApellido]          NVARCHAR (150) NULL,
    [TelefonoContacto]         NVARCHAR (10)  NOT NULL,
    [CorreoElectronico]        NVARCHAR (100) NULL,
    [Estado]                   BIT            NULL,
    [UsuarioCreacion]          NVARCHAR (250) NOT NULL,
    [FechaCreacion]            DATETIME       NOT NULL,
    [UsuarioModificacion]      NVARCHAR (250) NULL,
    [FechaModificacion]        DATETIME       NULL,
    [IdTipoUsuario]            INT            DEFAULT ((1)) NOT NULL,
    [IdRegional]               INT            NULL,
    [IdEntidadContratista]     INT            NULL,
    [IdTipoPersona]            INT            NULL,
    [RazonSocial]              NVARCHAR (150) NULL,
    [DV]                       NVARCHAR (1)   NULL,
    [OferentesMigrados]        BIT            DEFAULT ((0)) NOT NULL,
    [ValidarOferentesMigrados] BIT            DEFAULT ((0)) NOT NULL,
    [TodosRegional]            BIT            NULL,
    [AceptaEnvioCorreos]       BIT            NULL,
    [IdArea]                   INT            NULL,
    [CodPCI]                   NVARCHAR (20)  NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([IdUsuario] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Usuario_Regional_IdRegional] FOREIGN KEY ([IdRegional]) REFERENCES [DIV].[Regional] ([IdRegional]),
    CONSTRAINT [FK_Usuario_TiposDocumentos] FOREIGN KEY ([IdTipoDocumento]) REFERENCES [Global].[TiposDocumentos] ([IdTipoDocumento]),
    CONSTRAINT [FK_Usuario_TipoUsuario_IdTipoUsuario] FOREIGN KEY ([IdTipoUsuario]) REFERENCES [SEG].[TipoUsuario] ([IdTipoUsuario])
);


GO
ALTER TABLE [SEG].[Usuario] NOCHECK CONSTRAINT [FK_Usuario_Regional_IdRegional];


GO
ALTER TABLE [SEG].[Usuario] NOCHECK CONSTRAINT [FK_Usuario_TipoUsuario_IdTipoUsuario];

