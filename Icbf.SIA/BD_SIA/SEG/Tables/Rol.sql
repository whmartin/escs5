﻿CREATE TABLE [SEG].[Rol] (
    [IdRol]               INT            IDENTITY (1, 1) NOT NULL,
    [providerKey]         VARCHAR (125)  NOT NULL,
    [Nombre]              NVARCHAR (256) NULL,
    [Descripcion]         NVARCHAR (200) NOT NULL,
    [Estado]              BIT            NULL,
    [UsuarioCreacion]     NVARCHAR (250) NOT NULL,
    [FechaCreacion]       DATETIME       NOT NULL,
    [UsuarioModificacion] NVARCHAR (250) NULL,
    [FechaModificacion]   DATETIME       NULL,
    [EsAdministrador]     BIT            NULL,
    [IdPaso]              INT            NULL,
    [AprobacionPAC]       BIT            NULL,
    CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED ([IdRol] ASC) WITH (FILLFACTOR = 90)
);

