﻿CREATE TABLE [SEG].[TipoUsuario] (
    [IdTipoUsuario]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoTipoUsuario] NVARCHAR (128) NOT NULL,
    [NombreTipoUsuario] NVARCHAR (256) NOT NULL,
    [Estado]            NVARCHAR (1)   NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    [UsuarioModifica]   NVARCHAR (250) NULL,
    [FechaModifica]     DATETIME       NULL,
    CONSTRAINT [PK_TipoUsuario] PRIMARY KEY CLUSTERED ([IdTipoUsuario] ASC) WITH (FILLFACTOR = 90)
);

