﻿CREATE TABLE [SEG].[Modulo] (
    [IdModulo]            INT            IDENTITY (1, 1) NOT NULL,
    [NombreModulo]        NVARCHAR (250) NOT NULL,
    [Posicion]            INT            NOT NULL,
    [Estado]              BIT            NOT NULL,
    [UsuarioCreacion]     NVARCHAR (250) NOT NULL,
    [FechaCreacion]       DATETIME       NOT NULL,
    [UsuarioModificacion] NVARCHAR (250) NULL,
    [FechaModificacion]   DATETIME       NULL,
    CONSTRAINT [PK_Modulo] PRIMARY KEY CLUSTERED ([IdModulo] ASC) WITH (FILLFACTOR = 90)
);

