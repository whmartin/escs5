﻿CREATE TABLE [SPCP].[Archivos] (
    [IdArchivo]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Tipo]          INT            NOT NULL,
    [Valor]         NVARCHAR (250) NOT NULL,
    [FechaCreacion] DATETIME       CONSTRAINT [DF_Archivos_FechaCreacion] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Archivos] PRIMARY KEY CLUSTERED ([IdArchivo] ASC)
);

