﻿CREATE TABLE [SPCP].[Acciones] (
    [IdAcciones]      INT            IDENTITY (1, 1) NOT NULL,
    [Descripcion]     NVARCHAR (160) NULL,
    [Estado]          BIT            DEFAULT ((0)) NOT NULL,
    [CodAccion]       NVARCHAR (10)  NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_SPCP_Acciones_IdAcciones] PRIMARY KEY CLUSTERED ([IdAcciones] ASC)
);

