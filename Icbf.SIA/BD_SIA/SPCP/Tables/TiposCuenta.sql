﻿CREATE TABLE [SPCP].[TiposCuenta] (
    [IdTipoCuenta]    INT            IDENTITY (1, 1) NOT NULL,
    [TipoCuenta]      NVARCHAR (50)  NOT NULL,
    [IdRegional]      INT            NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_TiposCuenta] PRIMARY KEY CLUSTERED ([IdTipoCuenta] ASC)
);

