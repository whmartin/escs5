﻿CREATE TABLE [SPCP].[DatosAdmonObligaciones] (
    [IdDatosAdmonObligaciones] INT            IDENTITY (1, 1) NOT NULL,
    [IdDatosAdministracion]    INT            NOT NULL,
    [UsuarioCrea]              NVARCHAR (250) NOT NULL,
    [FechaCrea]                DATETIME       NOT NULL,
    [UsuarioModifica]          NVARCHAR (250) NULL,
    [FechaModifica]            DATETIME       NULL,
    CONSTRAINT [PK_DatosAdmonObligaciones] PRIMARY KEY CLUSTERED ([IdDatosAdmonObligaciones] ASC)
);

