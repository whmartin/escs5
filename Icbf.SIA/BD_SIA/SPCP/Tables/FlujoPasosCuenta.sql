﻿CREATE TABLE [SPCP].[FlujoPasosCuenta] (
    [IdFlujoPasoCuenta] INT            IDENTITY (1, 1) NOT NULL,
    [IdTipoCuenta]      INT            NOT NULL,
    [IdPaso]            INT            NOT NULL,
    [Orden]             INT            NOT NULL,
    [Estado]            INT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    [UsuarioModifica]   NVARCHAR (250) NULL,
    [FechaModifica]     DATETIME       NULL,
    CONSTRAINT [PK_FlujoPasosCuenta] PRIMARY KEY CLUSTERED ([IdFlujoPasoCuenta] ASC)
);

