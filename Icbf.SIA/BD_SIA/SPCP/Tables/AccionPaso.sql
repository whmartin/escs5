﻿CREATE TABLE [SPCP].[AccionPaso] (
    [IdAccionPaso]    INT            IDENTITY (1, 1) NOT NULL,
    [IdPaso]          INT            NOT NULL,
    [Estado]          BIT            DEFAULT ((0)) NOT NULL,
    [Descripcion]     NVARCHAR (160) NULL,
    [IdAccion]        INT            NOT NULL,
    [Aceptar]         BIT            DEFAULT ((0)) NOT NULL,
    [Rechazar]        BIT            DEFAULT ((0)) NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_SPCP_AccionPaso_IdAccionPaso] PRIMARY KEY CLUSTERED ([IdAccionPaso] ASC),
    CONSTRAINT [FK_SPCP_AccionPaso_IdAccion_AccionPaso] FOREIGN KEY ([IdAccion]) REFERENCES [SPCP].[Acciones] ([IdAcciones]),
    CONSTRAINT [FK_SPCP_AccionPaso_IdPaso_Paso] FOREIGN KEY ([IdPaso]) REFERENCES [SPCP].[Pasos] ([IdPaso])
);

