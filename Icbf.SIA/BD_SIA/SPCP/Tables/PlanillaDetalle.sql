﻿CREATE TABLE [SPCP].[PlanillaDetalle] (
    [IDPlanillaDetalle] INT            IDENTITY (1, 1) NOT NULL,
    [IdCuenta]          INT            NOT NULL,
    [IdPlanilla]        INT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    [UsuarioModifica]   NVARCHAR (250) NULL,
    [FechaModifica]     DATETIME       NULL,
    CONSTRAINT [PK_PlanillaDetalle] PRIMARY KEY CLUSTERED ([IDPlanillaDetalle] ASC)
);

