﻿CREATE TABLE [SPCP].[Presupuestos] (
    [IdPresupuesto]          INT            IDENTITY (1, 1) NOT NULL,
    [IdDatosAdministracion]  INT            NOT NULL,
    [CodPosicionGasto]       NVARCHAR (255) NOT NULL,
    [NomPosicionGasto]       NVARCHAR (255) NOT NULL,
    [NomFuenteFinanciacion]  NVARCHAR (255) NOT NULL,
    [CodRecursoPresupuestal] INT            NOT NULL,
    [NomRecursoPresupuestal] NVARCHAR (255) NOT NULL,
    [NomSituacionFondos]     NVARCHAR (255) NOT NULL,
    [ValorInicialRubro]      NVARCHAR (40)  NOT NULL,
    [ValorActualRubro]       NVARCHAR (40)  NOT NULL,
    [ValorSaldoRubro]        NVARCHAR (40)  NOT NULL,
    [UsuarioCrea]            NVARCHAR (250) NOT NULL,
    [FechaCrea]              DATETIME       NOT NULL,
    [UsuarioModifica]        NVARCHAR (250) NULL,
    [FechaModifica]          DATETIME       NULL,
    CONSTRAINT [PK_Presupuesto] PRIMARY KEY CLUSTERED ([IdPresupuesto] ASC),
    CONSTRAINT [FK_SPCP_DatosAdministracion_Presupuesto] FOREIGN KEY ([IdDatosAdministracion]) REFERENCES [SPCP].[DatosAdministracion] ([IdDatosAdministracion]),
    CONSTRAINT [FK_SPCP_DatosAdministracion_Presupuestos] FOREIGN KEY ([IdDatosAdministracion]) REFERENCES [SPCP].[DatosAdministracion] ([IdDatosAdministracion])
);

