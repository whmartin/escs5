﻿CREATE TABLE [SPCP].[ProductosContractuales] (
    [IdProductoContractual]   INT            IDENTITY (1, 1) NOT NULL,
    [IdObligacionContractual] INT            NOT NULL,
    [ProductoContractual]     NVARCHAR (500) NOT NULL,
    [NumeroPago]              INT            NOT NULL,
    [Estado]                  INT            NOT NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    CONSTRAINT [PK_ProductosContractuales] PRIMARY KEY CLUSTERED ([IdProductoContractual] ASC)
);

