﻿CREATE TABLE [SPCP].[HistoricoEstadoCuentasDeCobro] (
    [IDHistoricoEstadoCuentasDeCobro] INT            IDENTITY (1, 1) NOT NULL,
    [IDCuentaCobro]                   INT            NOT NULL,
    [IDFlujoPasoCuentaAnterior]       INT            NOT NULL,
    [IDFlujoPasoCuentaNuevo]          INT            NOT NULL,
    [Aprobado]                        BIT            NOT NULL,
    [Descripcion]                     NVARCHAR (800) NULL,
    [Roltransaccion]                  NVARCHAR (250) NOT NULL,
    [UsuarioCrea]                     NVARCHAR (250) NOT NULL,
    [FechaCrea]                       DATETIME       NOT NULL,
    [UsuarioModifica]                 NVARCHAR (250) NULL,
    [FechaModifica]                   DATETIME       NULL,
    CONSTRAINT [PK_HistoricoEstadoCuentasDeCobro] PRIMARY KEY CLUSTERED ([IDHistoricoEstadoCuentasDeCobro] ASC)
);

