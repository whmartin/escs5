﻿CREATE TABLE [SPCP].[HistoricoProductosContractuales] (
    [IdHistoricoProducto]     INT            IDENTITY (1, 1) NOT NULL,
    [IdProductoContractual]   INT            NOT NULL,
    [IdObligacionContractual] INT            NOT NULL,
    [ProductoContractual]     NVARCHAR (500) NOT NULL,
    [NumeroPago]              INT            NOT NULL,
    [Estado]                  INT            NOT NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    [HistoricoFechaCrea]      DATETIME       NOT NULL,
    CONSTRAINT [PK_HistoricoProductosContractuales] PRIMARY KEY CLUSTERED ([IdHistoricoProducto] ASC)
);

