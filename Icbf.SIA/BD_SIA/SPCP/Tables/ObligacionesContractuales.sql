﻿CREATE TABLE [SPCP].[ObligacionesContractuales] (
    [IdObligacionContractual]  INT            IDENTITY (1, 1) NOT NULL,
    [IdDatosAdmonObligaciones] INT            NOT NULL,
    [ObligacionContractual]    NVARCHAR (250) NOT NULL,
    [Estado]                   INT            NOT NULL,
    [UsuarioCrea]              NVARCHAR (250) NOT NULL,
    [FechaCrea]                DATETIME       NOT NULL,
    [UsuarioModifica]          NVARCHAR (250) NULL,
    [FechaModifica]            DATETIME       NULL,
    CONSTRAINT [PK_ObligacionesContractuales] PRIMARY KEY CLUSTERED ([IdObligacionContractual] ASC)
);

