﻿CREATE TABLE [SPCP].[Planilla] (
    [IDPlanilla]      INT            IDENTITY (1, 1) NOT NULL,
    [NumeroPlanilla]  INT            NOT NULL,
    [IdRegional]      INT            NOT NULL,
    [IdArea]          INT            NOT NULL,
    [Impreso]         BIT            CONSTRAINT [DF_SPCP_Planilla_Impreso] DEFAULT ((0)) NOT NULL,
    [Observaciones]   NVARCHAR (504) NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Planilla] PRIMARY KEY CLUSTERED ([IDPlanilla] ASC)
);

