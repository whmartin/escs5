﻿CREATE TABLE [SPCP].[Pasos] (
    [IdPaso]          INT            IDENTITY (1, 1) NOT NULL,
    [Paso]            NVARCHAR (50)  NOT NULL,
    [Estado]          INT            NOT NULL,
    [Financiera]      INT            NOT NULL,
    [IdAccion]        INT            NOT NULL,
    [AccionEstado]    INT            NOT NULL,
    [Obligatorio]     INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Pasos] PRIMARY KEY CLUSTERED ([IdPaso] ASC)
);

