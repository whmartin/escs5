﻿CREATE TABLE [SPCP].[FlujoPasosCuentaDetalle] (
    [IdFlujoPasoCuentaDetalle] INT            IDENTITY (1, 1) NOT NULL,
    [IdFlujoPasoCuenta]        INT            NOT NULL,
    [IdTipoCuenta]             INT            NOT NULL,
    [IdPaso]                   INT            NOT NULL,
    [Orden]                    INT            NOT NULL,
    [UsuarioCrea]              NVARCHAR (250) NOT NULL,
    [FechaCrea]                DATETIME       NOT NULL,
    [UsuarioModifica]          NVARCHAR (250) NULL,
    [FechaModifica]            DATETIME       NULL,
    CONSTRAINT [PK_FlujoPasosCuentaDetalle] PRIMARY KEY CLUSTERED ([IdFlujoPasoCuentaDetalle] ASC)
);

