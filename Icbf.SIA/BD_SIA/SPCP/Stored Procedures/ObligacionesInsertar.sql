﻿-- =============================================
-- Autor:<Grupo Desarrollo>
-- Fecha Creación: 16/12/2015 08:58:31 a.m.
-- Descripción:	<Procedimiento Almacenado para la tabla Obligaciones>
-- Nombre: [SPCP].[ObligacionesInsertar]
-- Email: 		
-- Defensoria del Pueblo Gestion Humana
-- =============================================
CREATE PROCEDURE [SPCP].[ObligacionesInsertar]
(
	@IdReporteObligaciones bigint = NULL output, 
	@NumeroDocumento nvarchar(50), 
	@FechadeRegistro datetime, 
	@FechadeCreacion datetime, 
	@Estado nvarchar(50), 
	@ValorActual numeric(18,2), 
	@ValorDeducciones numeric(18,2), 
	@ValorOblignoOrden numeric(18,2), 
	@Dependencia nvarchar(50), 
	@TipoIdentificacion nvarchar(50), 
	@NombreRazonSocial nvarchar(450), 
	@MediodePago nvarchar(50), 
	@TipoCuenta nvarchar(50) = NULL, 
	@NumeroCuenta nvarchar(50) = NULL, 
	@EstadoCuenta nvarchar(50) = NULL, 
	@EntidadNit nvarchar(50) = NULL, 
	@EntidadDescripcion nvarchar(450) = NULL, 
	@DependenciaDescripcion nvarchar(450) = NULL, 
	@Rubro nvarchar(50) = NULL, 
	@Descripcion nvarchar(450) = NULL, 
	@ValorInicial numeric(18,2) = NULL, 
	@ValorOperaciones numeric(18,2) = NULL, 
	@ValorActualOpera numeric(18,2) = NULL, 
	@SaldoporUtilizar numeric(18,2) = NULL, 
	@Fuente nvarchar(50) = NULL, 
	@Situacion nvarchar(50) = NULL, 
	@Recurso nvarchar(50) = NULL, 
	@Concepto nvarchar(50) = NULL, 
	@SolicitudCDP nvarchar(50) = NULL, 
	@CDP nvarchar(50) = NULL, 
	@Compromisos nvarchar(50) = NULL, 
	@CuentasporPagar nvarchar(50) = NULL, 
	@FechaCuentasporPagar datetime = NULL, 
	@Obligaciones nvarchar(50) = NULL, 
	@OrdenesdePago nvarchar(50) = NULL, 
	@Reintegros nvarchar(50) = NULL, 
	@FechaDocSoporteCompromiso datetime = NULL, 
	@TipoDocSoporteCompromiso nvarchar(50) = NULL, 
	@NumDocSoporteCompromiso nvarchar(50) = NULL, 
	@ObjetodelCompromiso nvarchar(450) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	
		BEGIN TRY
            SET XACT_ABORT ON
            BEGIN TRANSACTION
			
		INSERT
			INTO 
			[SPCP].[Obligaciones]	
			(
				[NumeroDocumento], 
				[FechadeRegistro], 
				[FechadeCreacion], 
				[Estado], 
				[ValorActual], 
				[ValorDeducciones], 
				[ValorOblignoOrden], 
				[Dependencia], 
				[TipoIdentificacion], 
				[NombreRazonSocial], 
				[MediodePago], 
				[TipoCuenta], 
				[NumeroCuenta], 
				[EstadoCuenta], 
				[EntidadNit], 
				[EntidadDescripcion], 
				[DependenciaDescripcion], 
				[Rubro], 
				[Descripcion], 
				[ValorInicial], 
				[ValorOperaciones], 
				[ValorActualOpera], 
				[SaldoporUtilizar], 
				[Fuente], 
				[Situacion], 
				[Recurso], 
				[Concepto], 
				[SolicitudCDP], 
				[CDP], 
				[Compromisos], 
				[CuentasporPagar], 
				[FechaCuentasporPagar], 
				[Obligaciones], 
				[OrdenesdePago], 
				[Reintegros], 
				[FechaDocSoporteCompromiso], 
				[TipoDocSoporteCompromiso], 
				[NumDocSoporteCompromiso], 
				[ObjetodelCompromiso]
			)
			VALUES
			(
				@NumeroDocumento, 
				@FechadeRegistro, 
				@FechadeCreacion, 
				@Estado, 
				@ValorActual, 
				@ValorDeducciones, 
				@ValorOblignoOrden, 
				@Dependencia, 
				@TipoIdentificacion, 
				@NombreRazonSocial, 
				@MediodePago, 
				@TipoCuenta, 
				@NumeroCuenta, 
				@EstadoCuenta, 
				@EntidadNit, 
				@EntidadDescripcion, 
				@DependenciaDescripcion, 
				@Rubro, 
				@Descripcion, 
				@ValorInicial, 
				@ValorOperaciones, 
				@ValorActualOpera, 
				@SaldoporUtilizar, 
				@Fuente, 
				@Situacion, 
				@Recurso, 
				@Concepto, 
				@SolicitudCDP, 
				@CDP, 
				@Compromisos, 
				@CuentasporPagar, 
				@FechaCuentasporPagar, 
				@Obligaciones, 
				@OrdenesdePago, 
				@Reintegros, 
				@FechaDocSoporteCompromiso, 
				@TipoDocSoporteCompromiso, 
				@NumDocSoporteCompromiso, 
				@ObjetodelCompromiso
			)

			SELECT @IdReporteObligaciones = SCOPE_IDENTITY()

	 	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		
        IF @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION

    END CATCH 	
END

