﻿-- =============================================
-- Autor:<Grupo Desarrollo>
-- Fecha Creación: 16/12/2015 08:58:30 a.m.
-- Descripción:	<Procedimiento Almacenado para la tabla Obligaciones>
-- Nombre: [SPCP].[ObligacionesSeleccionar]
-- Email: 		
-- Defensoria del Pueblo Gestion Humana
-- =============================================
CREATE PROCEDURE [SPCP].[ObligacionesSeleccionar]

AS
BEGIN

	SET NOCOUNT ON

	BEGIN TRY
	
		SELECT
			[IdReporteObligaciones],
			[NumeroDocumento],
			[FechadeRegistro],
			[FechadeCreacion],
			[Estado],
			[ValorActual],
			[ValorDeducciones],
			[ValorOblignoOrden],
			[Dependencia],
			[TipoIdentificacion],
			[NombreRazonSocial],
			[MediodePago],
			[TipoCuenta],
			[NumeroCuenta],
			[EstadoCuenta],
			[EntidadNit],
			[EntidadDescripcion],
			[DependenciaDescripcion],
			[Rubro],
			[Descripcion],
			[ValorInicial],
			[ValorOperaciones],
			[ValorActualOpera],
			[SaldoporUtilizar],
			[Fuente],
			[Situacion],
			[Recurso],
			[Concepto],
			[SolicitudCDP],
			[CDP],
			[Compromisos],
			[CuentasporPagar],
			[FechaCuentasporPagar],
			[Obligaciones],
			[OrdenesdePago],
			[Reintegros],
			[FechaDocSoporteCompromiso],
			[TipoDocSoporteCompromiso],
			[NumDocSoporteCompromiso],
			[ObjetodelCompromiso]
		FROM 
			[SPCP].[Obligaciones]  WITH (NOLOCK)
		
	END TRY
	
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0 
				ROLLBACK TRANSACTION				
	END CATCH
END
