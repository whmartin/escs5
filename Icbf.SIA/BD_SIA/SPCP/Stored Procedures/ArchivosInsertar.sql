﻿-- =============================================
-- Autor:<Grupo Desarrollo>
-- Fecha Creación: 08/12/2015 11:57:19 p.m.
-- Descripción:	<Procedimiento Almacenado para la tabla Archivos>
-- Nombre: [SPCP].[ArchivosInsertar]
-- Email: 		
-- Defensoria del Pueblo Gestion Humana
-- =============================================
CREATE PROCEDURE [SPCP].[ArchivosInsertar]
(
	@IdArchivo bigint = NULL output, 
	@Tipo int, 
	@Valor nvarchar(550),
	@IdRegistro INT,
	@ConsecutivoSIIF int
	
)
AS
BEGIN

	SET NOCOUNT OFF
	
		BEGIN TRY
            SET XACT_ABORT ON
            BEGIN TRANSACTION
			DECLARE @ContieneConsecutivo INT			
			SET @ContieneConsecutivo = 0;


			INSERT
				INTO 
				[SPCP].[Archivos]	
				(
					[Tipo], 
					[Valor]
				)
				VALUES
				(
					@Tipo, 
					@Valor
				)

			SELECT @IdArchivo = SCOPE_IDENTITY();

			SELECT 
				@ContieneConsecutivo = COUNT(*) 
			FROM 
				dbo.fnSplitString(@Valor,'');

			IF(@ContieneConsecutivo = 3)
			BEGIN
				DECLARE @Consecutivo INT;

				SELECT TOP(1) @Consecutivo = splitdata FROM dbo.fnSplitString(@Valor,'')				
				ORDER BY 1	

				PRINT (@Consecutivo);
			END
			ELSE
			BEGIN
				IF(@ContieneConsecutivo = 12)
				BEGIN			
					
					UPDATE 
						[SPCP].[CuentasdeCobro]
					SET
						[CodigoCuentaPorPagarSIIF] = @ConsecutivoSIIF
					WHERE
						[RegistroCuentaPagarSIIF] = @IdRegistro
					AND
						CONVERT(VARCHAR(25), [FechaCrea], 103) = (CONVERT(VARCHAR(25), (SELECT [FechaCreacion] FROM [SPCP].[Archivos] WHERE [IdArchivo] = @IdArchivo), 103))
					AND
						[CodigoCuentaPorPagarSIIF] IS NULL
				
				END				
			END

	 	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		
        IF @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION

    END CATCH 	
END
