﻿-- =============================================
-- Autor:<Grupo Desarrollo>
-- Fecha Creación: 08/12/2015 11:57:18 p.m.
-- Descripción:	<Procedimiento Almacenado para la tabla Archivos>
-- Nombre: [SPCP].[ArchivosSeleccionar]
-- Email: 		
-- Defensoria del Pueblo Gestion Humana
-- =============================================
CREATE PROCEDURE [SPCP].[ArchivosSeleccionar]

AS
BEGIN

	SET NOCOUNT ON

	BEGIN TRY
	
		SELECT
			[IdArchivo],
			[Tipo],
			[Valor]
		FROM 
			[SPCP].[Archivos]  WITH (NOLOCK)
		
	END TRY
	
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0 
				ROLLBACK TRANSACTION				
	END CATCH
END