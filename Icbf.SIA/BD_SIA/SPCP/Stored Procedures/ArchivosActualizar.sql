﻿-- =============================================
-- Autor:<Grupo Desarrollo>
-- Fecha Creación: 08/12/2015 11:57:19 p.m.
-- Descripción:	<Procedimiento Almacenado para la tabla Archivos>
-- Nombre: [SPCP].[ArchivosActualizar]
-- Email: 		
-- Defensoria del Pueblo Gestion Humana
-- =============================================
CREATE PROCEDURE [SPCP].[ArchivosActualizar]
(
	@IdArchivo bigint, 
	@Tipo int, 
	@Valor nvarchar(550)
)
AS
BEGIN

	SET NOCOUNT OFF
	
	BEGIN TRY
        SET XACT_ABORT ON
        BEGIN TRANSACTION	
	
		UPDATE 
			[SPCP].[Archivos]
		SET
			[Tipo] = @Tipo, 
			[Valor] = @Valor
		WHERE
			[IdArchivo] = @IdArchivo

		COMMIT TRANSACTION
    END TRY
	
	BEGIN CATCH
         	
         IF @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION

	END CATCH  

END