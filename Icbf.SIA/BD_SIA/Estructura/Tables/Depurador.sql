﻿CREATE TABLE [Estructura].[Depurador] (
    [IdDepurador]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoDepurador] NVARCHAR (256) NOT NULL,
    [NombreDepurador] NVARCHAR (256) NOT NULL,
    [Descripcion]     NVARCHAR (512) NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioCrea]     NVARCHAR (128) NOT NULL,
    [FechaModifica]   DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (128) NULL,
    CONSTRAINT [PK_DEPURADOR] PRIMARY KEY CLUSTERED ([IdDepurador] ASC) WITH (FILLFACTOR = 90)
);

