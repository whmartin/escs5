﻿CREATE TABLE [Estructura].[ColumnaDepurador] (
    [IdColumnaDepurador] INT            IDENTITY (1, 1) NOT NULL,
    [IdColumna]          INT            NOT NULL,
    [IdDepurador]        INT            NOT NULL,
    [UsuarioCrea]        NVARCHAR (128) NOT NULL,
    [FechaCrea]          DATETIME       NOT NULL,
    [UsuarioModifica]    NVARCHAR (128) NULL,
    [FechaModifica]      DATETIME       NULL,
    CONSTRAINT [PK_COLUMNADEPURADOR] PRIMARY KEY CLUSTERED ([IdColumnaDepurador] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ColumnaDepurador_Columna] FOREIGN KEY ([IdColumna]) REFERENCES [Estructura].[Columna] ([IdColumna]),
    CONSTRAINT [FK_ColumnaDepurador_Depurador] FOREIGN KEY ([IdDepurador]) REFERENCES [Estructura].[Depurador] ([IdDepurador])
);

