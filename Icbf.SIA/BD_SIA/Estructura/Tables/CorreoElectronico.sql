﻿CREATE TABLE [Estructura].[CorreoElectronico] (
    [IdCorreoElectronico] NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [IdArchivo]           NUMERIC (18)    NULL,
    [Destinatario]        NVARCHAR (256)  NOT NULL,
    [Mensaje]             NVARCHAR (4000) NOT NULL,
    [Estado]              NVARCHAR (1)    NOT NULL,
    [FechaIngreso]        DATETIME        NOT NULL,
    [FechaEnvio]          DATETIME        NULL,
    [UsuarioCrea]         NVARCHAR (250)  NOT NULL,
    [FechaCrea]           DATETIME        NOT NULL,
    [UsuarioModifica]     NVARCHAR (250)  NULL,
    [FechaModifica]       DATETIME        NULL,
    [TipoCorreo]          CHAR (1)        DEFAULT ('F') NOT NULL,
    CONSTRAINT [PK_CorreoElectronico] PRIMARY KEY CLUSTERED ([IdCorreoElectronico] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CorreoElectronico_Archivo] FOREIGN KEY ([IdArchivo]) REFERENCES [Estructura].[Archivo] ([IdArchivo])
);

