﻿CREATE TABLE [Estructura].[FormatoArchivo] (
    [IdFormatoArchivo] INT            IDENTITY (1, 1) NOT NULL,
    [IdTipoEstructura] INT            NOT NULL,
    [IdModalidad]      INT            NOT NULL,
    [TablaTemporal]    NVARCHAR (128) NULL,
    [Separador]        NVARCHAR (8)   NOT NULL,
    [Extension]        NVARCHAR (5)   NOT NULL,
    [Estado]           CHAR (1)       NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    [ValidarCampos]    BIT            CONSTRAINT [DF_FormatoArchivo_ValidarCampos] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FormatoArchivo] PRIMARY KEY CLUSTERED ([IdFormatoArchivo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FormatoArchivo_Modalidad] FOREIGN KEY ([IdModalidad]) REFERENCES [Estructura].[Modalidad] ([IdModalidad]),
    CONSTRAINT [FK_FormatoArchivo_TipoEstructura] FOREIGN KEY ([IdTipoEstructura]) REFERENCES [Estructura].[TipoEstructura] ([IdTipoEstructura])
);

