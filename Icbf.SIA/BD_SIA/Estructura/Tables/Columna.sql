﻿CREATE TABLE [Estructura].[Columna] (
    [IdColumna]        INT            IDENTITY (1, 1) NOT NULL,
    [IdFormatoArchivo] INT            NOT NULL,
    [CodigoColumna]    NVARCHAR (128) NULL,
    [NombreColumna]    NVARCHAR (128) NOT NULL,
    [TipoColumna]      NVARCHAR (128) NOT NULL,
    [Longitud]         INT            NULL,
    [Obligatorio]      CHAR (1)       NOT NULL,
    [Posicion]         INT            NOT NULL,
    [Estado]           CHAR (1)       NOT NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    CONSTRAINT [PK_Columna] PRIMARY KEY CLUSTERED ([IdColumna] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Columna_FormatoArchivo] FOREIGN KEY ([IdFormatoArchivo]) REFERENCES [Estructura].[FormatoArchivo] ([IdFormatoArchivo])
);

