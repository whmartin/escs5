﻿CREATE TABLE [Estructura].[Modalidad] (
    [IdModalidad]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreModalidad] NVARCHAR (128) NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [GrupoModalidad]  VARCHAR (64)   NULL,
    CONSTRAINT [PK_Modalidad] PRIMARY KEY CLUSTERED ([IdModalidad] ASC) WITH (FILLFACTOR = 90)
);

