﻿CREATE TABLE [Estructura].[Archivo] (
    [IdArchivo]        NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [IdUsuario]        INT            NOT NULL,
    [IdFormatoArchivo] INT            NOT NULL,
    [FechaRegistro]    DATETIME       NOT NULL,
    [NombreArchivo]    NVARCHAR (128) NOT NULL,
    [Estado]           NVARCHAR (1)   NOT NULL,
    [ResumenCarga]     NVARCHAR (256) NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    [ServidorFTP]      NVARCHAR (256) NULL,
    [NombreTabla]      VARCHAR (256)  NULL,
    [IdTabla]          VARCHAR (256)  NULL,
    [NombreArchivoOri] NVARCHAR (256) NULL,
    CONSTRAINT [PK_Archivo] PRIMARY KEY CLUSTERED ([IdArchivo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Archivo_FormatoArchivo] FOREIGN KEY ([IdFormatoArchivo]) REFERENCES [Estructura].[FormatoArchivo] ([IdFormatoArchivo]),
    CONSTRAINT [FK_Archivo_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [SEG].[Usuario] ([IdUsuario])
);

