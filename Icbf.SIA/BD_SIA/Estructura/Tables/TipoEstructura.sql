﻿CREATE TABLE [Estructura].[TipoEstructura] (
    [IdTipoEstructura] INT            IDENTITY (1, 1) NOT NULL,
    [NombreEstructura] NVARCHAR (256) NOT NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    [GrupoEstructura]  VARCHAR (64)   NULL,
    CONSTRAINT [PK_TipoEstructura] PRIMARY KEY CLUSTERED ([IdTipoEstructura] ASC) WITH (FILLFACTOR = 90)
);

