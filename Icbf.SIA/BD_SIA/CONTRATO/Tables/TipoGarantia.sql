﻿CREATE TABLE [CONTRATO].[TipoGarantia] (
    [IdTipoGarantia]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreTipoGarantia] NVARCHAR (128) NOT NULL,
    [Estado]             BIT            NOT NULL,
    [UsuarioCrea]        NVARCHAR (250) NOT NULL,
    [FechaCrea]          DATETIME       NOT NULL,
    [UsuarioModifica]    NVARCHAR (250) NULL,
    [FechaModifica]      DATETIME       NULL,
    CONSTRAINT [PK_TipoGarantia] PRIMARY KEY CLUSTERED ([IdTipoGarantia] ASC),
    CONSTRAINT [UQ_NombreTipoGarantia] UNIQUE NONCLUSTERED ([NombreTipoGarantia] ASC)
);

