﻿CREATE TABLE [CONTRATO].[RelacionarContratistas] (
    [IdContratistaContrato]                  INT            IDENTITY (1, 1) NOT NULL,
    [IdContrato]                             INT            NOT NULL,
    [NumeroIdentificacion]                   BIGINT         NOT NULL,
    [ClaseEntidad]                           NVARCHAR (1)   NOT NULL,
    [PorcentajeParticipacion]                INT            NOT NULL,
    [NumeroIdentificacionRepresentanteLegal] BIGINT         NOT NULL,
    [EstadoIntegrante]                       BIT            NULL,
    [UsuarioCrea]                            NVARCHAR (250) NOT NULL,
    [FechaCrea]                              DATETIME       NOT NULL,
    [UsuarioModifica]                        NVARCHAR (250) NULL,
    [FechaModifica]                          DATETIME       NULL,
    CONSTRAINT [PK_RelacionarContratistas] PRIMARY KEY CLUSTERED ([IdContratistaContrato] ASC)
);

