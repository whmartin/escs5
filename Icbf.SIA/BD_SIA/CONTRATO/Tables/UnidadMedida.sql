﻿CREATE TABLE [CONTRATO].[UnidadMedida] (
    [IdNumeroContrato]                INT            IDENTITY (1, 1) NOT NULL,
    [NumeroContrato]                  NVARCHAR (50)  NULL,
    [FechaInicioEjecuciónContrato]    DATETIME       NOT NULL,
    [FechaTerminacionInicialContrato] DATETIME       NOT NULL,
    [FechaFinalTerminacionContrato]   DATETIME       NOT NULL,
    [UsuarioCrea]                     NVARCHAR (250) NOT NULL,
    [FechaCrea]                       DATETIME       NOT NULL,
    [UsuarioModifica]                 NVARCHAR (250) NULL,
    [FechaModifica]                   DATETIME       NULL,
    CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED ([IdNumeroContrato] ASC)
);

