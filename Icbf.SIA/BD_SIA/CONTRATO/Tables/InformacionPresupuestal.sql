﻿CREATE TABLE [CONTRATO].[InformacionPresupuestal] (
    [IdInformacionPresupuestal] INT             IDENTITY (1, 1) NOT NULL,
    [NumeroCDP]                 INT             NOT NULL,
    [ValorCDP]                  NUMERIC (18, 3) NOT NULL,
    [FechaExpedicionCDP]        DATETIME        NOT NULL,
    [UsuarioCrea]               NVARCHAR (250)  NOT NULL,
    [FechaCrea]                 DATETIME        NOT NULL,
    [UsuarioModifica]           NVARCHAR (250)  NULL,
    [FechaModifica]             DATETIME        NULL,
    CONSTRAINT [PK_InformacionPresupuestal] PRIMARY KEY CLUSTERED ([IdInformacionPresupuestal] ASC),
    CONSTRAINT [UQ_NumeroCDP] UNIQUE NONCLUSTERED ([NumeroCDP] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Registro de información Presupuestal Información de CDP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Es el id', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'IdInformacionPresupuestal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Número del CDP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'NumeroCDP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Valor del CDP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'ValorCDP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha expedición CDP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'FechaExpedicionCDP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Usuario creador del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'UsuarioCrea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'FechaCrea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Usuario modificador del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'UsuarioModifica';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de modificación del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestal', @level2type = N'COLUMN', @level2name = N'FechaModifica';

