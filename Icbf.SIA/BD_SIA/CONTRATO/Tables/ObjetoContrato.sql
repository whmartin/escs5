﻿CREATE TABLE [CONTRATO].[ObjetoContrato] (
    [IdObjetoContratoContractual] INT            IDENTITY (1, 1) NOT NULL,
    [ObjetoContractual]           NVARCHAR (MAX) NULL,
    [Estado]                      BIT            NOT NULL,
    [UsuarioCrea]                 NVARCHAR (250) NOT NULL,
    [FechaCrea]                   DATETIME       NOT NULL,
    [UsuarioModifica]             NVARCHAR (250) NULL,
    [FechaModifica]               DATETIME       NULL,
    CONSTRAINT [PK_ObjetoContrato] PRIMARY KEY CLUSTERED ([IdObjetoContratoContractual] ASC)
);

