﻿CREATE TABLE [CONTRATO].[Obligacion] (
    [IdObligacion]     INT            IDENTITY (1, 1) NOT NULL,
    [IdTipoObligacion] INT            NOT NULL,
    [IdTipoContrato]   INT            NOT NULL,
    [Descripcion]      NVARCHAR (MAX) NULL,
    [Estado]           BIT            NOT NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    CONSTRAINT [PK_Obligacion] PRIMARY KEY CLUSTERED ([IdObligacion] ASC),
    CONSTRAINT [FK_OBLIGACI_FK_OBLIGA_TIPOCONT] FOREIGN KEY ([IdTipoContrato]) REFERENCES [CONTRATO].[TipoContrato] ([IdTipoContrato]),
    CONSTRAINT [FK_Obligacion_TipoObligacion1] FOREIGN KEY ([IdTipoObligacion]) REFERENCES [CONTRATO].[TipoObligacion] ([IdTipoObligacion])
);

