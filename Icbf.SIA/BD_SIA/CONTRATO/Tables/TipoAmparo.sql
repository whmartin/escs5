﻿CREATE TABLE [CONTRATO].[TipoAmparo] (
    [IdTipoAmparo]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreTipoAmparo] NVARCHAR (128) NULL,
    [Estado]           BIT            NOT NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    [IdTipoGarantia]   INT            NOT NULL,
    CONSTRAINT [PK_TipoAmparo] PRIMARY KEY CLUSTERED ([IdTipoAmparo] ASC),
    CONSTRAINT [FK_TIPOAMPA_FK_TIPO_A_TIPOGARA] FOREIGN KEY ([IdTipoGarantia]) REFERENCES [CONTRATO].[TipoGarantia] ([IdTipoGarantia]),
    CONSTRAINT [UQ_NombreTipoAmparo] UNIQUE NONCLUSTERED ([NombreTipoAmparo] ASC, [IdTipoGarantia] ASC)
);

