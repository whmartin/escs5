﻿CREATE TABLE [CONTRATO].[TipoContrato] (
    [IdTipoContrato]          INT            IDENTITY (1, 1) NOT NULL,
    [NombreTipoContrato]      NVARCHAR (128) NOT NULL,
    [IdCategoriaContrato]     INT            NOT NULL,
    [ActaInicio]              BIT            NOT NULL,
    [AporteCofinaciacion]     BIT            NOT NULL,
    [RecursoFinanciero]       BIT            NOT NULL,
    [RegimenContrato]         INT            NOT NULL,
    [DescripcionTipoContrato] NVARCHAR (128) NULL,
    [Estado]                  BIT            NOT NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    CONSTRAINT [PK_TipoContrato] PRIMARY KEY CLUSTERED ([IdTipoContrato] ASC),
    CONSTRAINT [FK_TIPOCONT_FK_TIPO_C_CATEGORI] FOREIGN KEY ([IdCategoriaContrato]) REFERENCES [CONTRATO].[CategoriaContrato] ([IdCategoriaContrato]),
    CONSTRAINT [UQ_NombreTipoContrato] UNIQUE NONCLUSTERED ([NombreTipoContrato] ASC)
);

