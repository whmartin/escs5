﻿CREATE TABLE [CONTRATO].[FormaPago] (
    [IdFormapago]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreFormaPago] NVARCHAR (50)  NOT NULL,
    [Descripcion]     NVARCHAR (128) NULL,
    [Estado]          BIT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED ([IdFormapago] ASC),
    CONSTRAINT [UQ_NombreFormaPago] UNIQUE NONCLUSTERED ([NombreFormaPago] ASC)
);

