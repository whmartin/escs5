﻿CREATE TABLE [CONTRATO].[CategoriaContrato] (
    [IdCategoriaContrato]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreCategoriaContrato] NVARCHAR (50)  NOT NULL,
    [Descripcion]             NVARCHAR (100) NULL,
    [Estado]                  BIT            NOT NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    CONSTRAINT [PK_CategoriaContrato] PRIMARY KEY CLUSTERED ([IdCategoriaContrato] ASC),
    CONSTRAINT [UQ_NombreCategoriaContrato] UNIQUE NONCLUSTERED ([NombreCategoriaContrato] ASC)
);

