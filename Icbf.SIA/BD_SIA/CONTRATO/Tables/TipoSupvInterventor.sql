﻿CREATE TABLE [CONTRATO].[TipoSupvInterventor] (
    [IdTipoSupvInterventor] INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]                NVARCHAR (128) NOT NULL,
    [Estado]                BIT            NOT NULL,
    [UsuarioCrea]           NVARCHAR (250) NOT NULL,
    [FechaCrea]             DATETIME       NOT NULL,
    [UsuarioModifica]       NVARCHAR (250) NULL,
    [FechaModifica]         DATETIME       NULL,
    CONSTRAINT [PK_TipoSupvInterventor] PRIMARY KEY CLUSTERED ([IdTipoSupvInterventor] ASC),
    CONSTRAINT [UQ_NombreTipoSupvInterventor] UNIQUE NONCLUSTERED ([Nombre] ASC)
);

