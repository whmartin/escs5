﻿CREATE TABLE [CONTRATO].[SecuenciaNumeroProceso] (
    [IdSecuenciaProceso] INT            IDENTITY (1, 1) NOT NULL,
    [Consecutivo]        INT            NOT NULL,
    [CodigoRegional]     INT            NOT NULL,
    [AnoVigencia]        INT            NOT NULL,
    [UsuarioCrea]        NVARCHAR (250) NOT NULL,
    [FechaCrea]          DATETIME       NOT NULL,
    [UsuarioModifica]    NVARCHAR (250) NULL,
    [FechaModifica]      DATETIME       NULL,
    CONSTRAINT [PK_SecuenciaNumeroProceso] PRIMARY KEY CLUSTERED ([IdSecuenciaProceso] ASC)
);

