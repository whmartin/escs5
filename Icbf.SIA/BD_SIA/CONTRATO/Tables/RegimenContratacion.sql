﻿CREATE TABLE [CONTRATO].[RegimenContratacion] (
    [IdRegimenContratacion]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreRegimenContratacion] NVARCHAR (128) NOT NULL,
    [Descripcion]               NVARCHAR (128) NULL,
    [Estado]                    BIT            NOT NULL,
    [UsuarioCrea]               NVARCHAR (250) NOT NULL,
    [FechaCrea]                 DATETIME       NOT NULL,
    [UsuarioModifica]           NVARCHAR (250) NULL,
    [FechaModifica]             DATETIME       NULL,
    CONSTRAINT [PK_RegimenContratacion] PRIMARY KEY CLUSTERED ([IdRegimenContratacion] ASC),
    CONSTRAINT [UQ_NombreRegimenContratacion] UNIQUE NONCLUSTERED ([NombreRegimenContratacion] ASC)
);

