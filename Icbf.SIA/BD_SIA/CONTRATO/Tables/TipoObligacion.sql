﻿CREATE TABLE [CONTRATO].[TipoObligacion] (
    [IdTipoObligacion]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreTipoObligacion] NVARCHAR (128) NOT NULL,
    [Descripcion]          NVARCHAR (128) NULL,
    [Estado]               BIT            NOT NULL,
    [UsuarioCrea]          NVARCHAR (250) NOT NULL,
    [FechaCrea]            DATETIME       NOT NULL,
    [UsuarioModifica]      NVARCHAR (250) NULL,
    [FechaModifica]        DATETIME       NULL,
    CONSTRAINT [PK_TipoObligacion] PRIMARY KEY CLUSTERED ([IdTipoObligacion] ASC)
);

