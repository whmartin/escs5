﻿CREATE TABLE [CONTRATO].[SupervisorIntervContrato] (
    [IDSupervisorInterv]       INT            IDENTITY (1, 1) NOT NULL,
    [IDContratoSupervisa]      INT            NOT NULL,
    [OrigenTipoSupervisor]     BIT            NOT NULL,
    [IDTipoSupvInterventor]    INT            NOT NULL,
    [IDTerceroExterno]         INT            NULL,
    [IDFuncionarioInterno]     INT            NULL,
    [IDContratoInterventoria]  INT            NULL,
    [IDTerceroInterventoria]   INT            NULL,
    [TipoVinculacion]          BIT            NOT NULL,
    [FechaInicia]              DATETIME       NOT NULL,
    [FechaFinaliza]            DATETIME       NOT NULL,
    [Estado]                   BIT            NOT NULL,
    [FechaModificaInterventor] DATETIME       NOT NULL,
    [FechaCrea]                DATETIME       NOT NULL,
    [UsuarioCrea]              NVARCHAR (128) NOT NULL,
    [FechaModifica]            DATETIME       NOT NULL,
    [UsuarioModifica]          NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_RelacionarSupervisorInterventor] PRIMARY KEY CLUSTERED ([IDSupervisorInterv] ASC),
    CONSTRAINT [FK_SupervisorIntervContrato_TipoSupvInterventor] FOREIGN KEY ([IDTipoSupvInterventor]) REFERENCES [CONTRATO].[TipoSupvInterventor] ([IdTipoSupvInterventor])
);

