﻿CREATE TABLE [CONTRATO].[LugarEjecucionContrato] (
    [IdContratoLugarEjecucion] INT            IDENTITY (1, 1) NOT NULL,
    [IDContrato]               INT            NOT NULL,
    [Nacional]                 BIT            NOT NULL,
    [UsuarioCrea]              NVARCHAR (250) NOT NULL,
    [FechaCrea]                DATETIME       NOT NULL,
    [UsuarioModifica]          NVARCHAR (250) NULL,
    [FechaModifica]            DATETIME       NULL,
    CONSTRAINT [PK_LugarEjecucionContrato] PRIMARY KEY CLUSTERED ([IdContratoLugarEjecucion] ASC)
);

