﻿CREATE TABLE [CONTRATO].[GestionarObligacion] (
    [IdGestionObligacion]   INT            IDENTITY (1, 1) NOT NULL,
    [IdGestionClausula]     INT            NOT NULL,
    [NombreObligacion]      NVARCHAR (10)  NOT NULL,
    [IdTipoObligacion]      INT            NOT NULL,
    [Orden]                 NVARCHAR (128) NOT NULL,
    [DescripcionObligacion] NVARCHAR (128) NOT NULL,
    [UsuarioCrea]           NVARCHAR (250) NOT NULL,
    [FechaCrea]             DATETIME       NOT NULL,
    [UsuarioModifica]       NVARCHAR (250) NULL,
    [FechaModifica]         DATETIME       NULL,
    CONSTRAINT [PK_GestionarObligacion] PRIMARY KEY CLUSTERED ([IdGestionObligacion] ASC),
    CONSTRAINT [FK_GestionarObligacion_GestionarClausulasContrato] FOREIGN KEY ([IdGestionClausula]) REFERENCES [CONTRATO].[GestionarClausulasContrato] ([IdGestionClausula]),
    CONSTRAINT [FK_GestionarObligacion_TipoObligacion] FOREIGN KEY ([IdTipoObligacion]) REFERENCES [CONTRATO].[TipoObligacion] ([IdTipoObligacion])
);

