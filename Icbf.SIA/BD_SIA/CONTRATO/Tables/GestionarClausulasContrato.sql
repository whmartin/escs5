﻿CREATE TABLE [CONTRATO].[GestionarClausulasContrato] (
    [IdGestionClausula]   INT            IDENTITY (1, 1) NOT NULL,
    [IdContrato]          NVARCHAR (3)   NOT NULL,
    [NombreClausula]      NVARCHAR (10)  NOT NULL,
    [TipoClausula]        INT            NOT NULL,
    [Orden]               NVARCHAR (128) NOT NULL,
    [DescripcionClausula] NVARCHAR (128) NOT NULL,
    [OrdenNumero]         INT            NULL,
    [UsuarioCrea]         NVARCHAR (250) NOT NULL,
    [FechaCrea]           DATETIME       NOT NULL,
    [UsuarioModifica]     NVARCHAR (250) NULL,
    [FechaModifica]       DATETIME       NULL,
    CONSTRAINT [PK_GestionarClausulasContrato] PRIMARY KEY CLUSTERED ([IdGestionClausula] ASC),
    CONSTRAINT [FK_GestionarClausulasContrato_ClausulaContrato] FOREIGN KEY ([TipoClausula]) REFERENCES [CONTRATO].[ClausulaContrato] ([IdClausulaContrato])
);

