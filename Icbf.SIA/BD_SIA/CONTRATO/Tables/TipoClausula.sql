﻿CREATE TABLE [CONTRATO].[TipoClausula] (
    [IdTipoClausula]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreTipoClausula] NVARCHAR (50)  NOT NULL,
    [Descripcion]        NVARCHAR (128) NULL,
    [Estado]             BIT            NOT NULL,
    [UsuarioCrea]        NVARCHAR (250) NOT NULL,
    [FechaCrea]          DATETIME       NOT NULL,
    [UsuarioModifica]    NVARCHAR (250) NULL,
    [FechaModifica]      DATETIME       NULL,
    CONSTRAINT [PK_TipoClausula] PRIMARY KEY CLUSTERED ([IdTipoClausula] ASC),
    CONSTRAINT [UQ_NombreTipoClausula] UNIQUE NONCLUSTERED ([NombreTipoClausula] ASC)
);

