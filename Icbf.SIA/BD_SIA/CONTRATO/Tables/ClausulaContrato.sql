﻿CREATE TABLE [CONTRATO].[ClausulaContrato] (
    [IdClausulaContrato]     INT            IDENTITY (1, 1) NOT NULL,
    [NombreClausulaContrato] NVARCHAR (128) NOT NULL,
    [IdTipoClausula]         INT            NOT NULL,
    [Contenido]              NVARCHAR (MAX) NULL,
    [IdTipoContrato]         INT            NOT NULL,
    [Estado]                 BIT            NOT NULL,
    [UsuarioCrea]            NVARCHAR (250) NOT NULL,
    [FechaCrea]              DATETIME       NOT NULL,
    [UsuarioModifica]        NVARCHAR (250) NULL,
    [FechaModifica]          DATETIME       NULL,
    CONSTRAINT [PK_ClausulaContrato] PRIMARY KEY CLUSTERED ([IdClausulaContrato] ASC),
    CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCLAU] FOREIGN KEY ([IdTipoClausula]) REFERENCES [CONTRATO].[TipoClausula] ([IdTipoClausula]),
    CONSTRAINT [FK_CLAUSULA_FK_CLAUSU_TIPOCONT] FOREIGN KEY ([IdTipoContrato]) REFERENCES [CONTRATO].[TipoContrato] ([IdTipoContrato]),
    CONSTRAINT [UQ_NombreClausulaContrato] UNIQUE NONCLUSTERED ([NombreClausulaContrato] ASC, [IdTipoClausula] ASC, [IdTipoContrato] ASC)
);

