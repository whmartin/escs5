﻿CREATE TABLE [CONTRATO].[AporteContrato] (
    [IdAporte]                      INT            IDENTITY (1, 1) NOT NULL,
    [IdContrato]                    NVARCHAR (3)   NOT NULL,
    [TipoAportante]                 NVARCHAR (11)  NOT NULL,
    [NumeroIdenticacionContratista] BIGINT         NOT NULL,
    [TipoAporte]                    NVARCHAR (10)  NOT NULL,
    [ValorAporte]                   INT            NOT NULL,
    [DescripcionAporteEspecie]      NVARCHAR (128) NOT NULL,
    [FechaRP]                       DATETIME       NOT NULL,
    [NumeroRP]                      INT            NOT NULL,
    [UsuarioCrea]                   NVARCHAR (250) NOT NULL,
    [FechaCrea]                     DATETIME       NOT NULL,
    [UsuarioModifica]               NVARCHAR (250) NULL,
    [FechaModifica]                 DATETIME       NULL,
    CONSTRAINT [PK_AporteContrato] PRIMARY KEY CLUSTERED ([IdAporte] ASC)
);

