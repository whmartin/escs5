﻿CREATE TABLE [CONTRATO].[ModalidadSeleccion] (
    [IdModalidad]     INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]          NVARCHAR (128) NOT NULL,
    [Sigla]           NVARCHAR (5)   NULL,
    [Estado]          BIT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_ModalidadSeleccion] PRIMARY KEY CLUSTERED ([IdModalidad] ASC),
    CONSTRAINT [UQ_NombreModalidadSeleccion] UNIQUE NONCLUSTERED ([Nombre] ASC, [Sigla] ASC)
);

