﻿CREATE TABLE [CONTRATO].[InformacionPresupuestalRP] (
    [IdInformacionPresupuestalRP] INT            IDENTITY (1, 1) NOT NULL,
    [FechaSolicitudRP]            DATETIME       NOT NULL,
    [NumeroRP]                    INT            NOT NULL,
    [ValorRP]                     INT            NOT NULL,
    [FechaExpedicionRP]           DATETIME       NOT NULL,
    [UsuarioCrea]                 NVARCHAR (250) NOT NULL,
    [FechaCrea]                   DATETIME       NOT NULL,
    [UsuarioModifica]             NVARCHAR (250) NULL,
    [FechaModifica]               DATETIME       NULL,
    CONSTRAINT [PK_InformacionPresupuestalRP] PRIMARY KEY CLUSTERED ([IdInformacionPresupuestalRP] ASC),
    CONSTRAINT [UQ_NumeroRP] UNIQUE NONCLUSTERED ([NumeroRP] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Registro de información Presupuestal Información de RP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Es el id', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'IdInformacionPresupuestalRP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de Solicitud RP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'FechaSolicitudRP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Número del RP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'NumeroRP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Valor del RP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'ValorRP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha expedición RP', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'FechaExpedicionRP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Usuario creador del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'UsuarioCrea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'FechaCrea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Usuario modificador del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'UsuarioModifica';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de modificación del registro', @level0type = N'SCHEMA', @level0name = N'CONTRATO', @level1type = N'TABLE', @level1name = N'InformacionPresupuestalRP', @level2type = N'COLUMN', @level2name = N'FechaModifica';

