﻿CREATE TABLE [CONTRATO].[LugarEjecucion] (
    [IdContratoLugarEjecucion] INT            NOT NULL,
    [IDDepartamento]           INT            NOT NULL,
    [IDMunicipio]              INT            NULL,
    [UsuarioCrea]              NVARCHAR (250) NOT NULL,
    [FechaCrea]                DATETIME       NOT NULL,
    [UsuarioModifica]          NVARCHAR (250) NULL,
    [FechaModifica]            DATETIME       NULL
);

