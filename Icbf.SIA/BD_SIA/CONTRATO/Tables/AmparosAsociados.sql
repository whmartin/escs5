﻿CREATE TABLE [CONTRATO].[AmparosAsociados] (
    [IdAmparo]                  INT             IDENTITY (1, 1) NOT NULL,
    [IdGarantia]                INT             NOT NULL,
    [IdTipoAmparo]              INT             NOT NULL,
    [FechaVigenciaDesde]        DATETIME        NOT NULL,
    [VigenciaHasta]             DATETIME        NOT NULL,
    [ValorParaCalculoAsegurado] NUMERIC (18, 3) NOT NULL,
    [IdTipoCalculo]             INT             NOT NULL,
    [ValorAsegurado]            NUMERIC (18, 3) NOT NULL,
    [UsuarioCrea]               NVARCHAR (250)  NOT NULL,
    [FechaCrea]                 DATETIME        NOT NULL,
    [UsuarioModifica]           NVARCHAR (250)  NULL,
    [FechaModifica]             DATETIME        NULL,
    CONSTRAINT [PK_AmparosAsociados] PRIMARY KEY CLUSTERED ([IdAmparo] ASC)
);

