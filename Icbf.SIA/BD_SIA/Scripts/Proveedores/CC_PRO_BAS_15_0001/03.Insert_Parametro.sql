﻿USE [SIA]
GO

IF NOT EXISTS	(
					SELECT	*
					FROM	[SEG].[Parametro]
					WHERE	[NombreParametro] = '¿Que debo registrar en códigos UNSPSC?'
				)
	BEGIN

		INSERT INTO [SEG].[Parametro]
				   ([NombreParametro]
				   ,[ValorParametro]
				   ,[ImagenParametro]
				   ,[Estado]
				   ,[Funcionalidad]
				   ,[UsuarioCrea]
				   ,[FechaCrea]
				   ,[UsuarioModifica]
				   ,[FechaModifica])
			 VALUES
				   ('¿Que debo registrar en códigos UNSPSC?'
				   ,'Debe seleccionar los códigos de los bienes y servicios UNSPSC para el proveedor'
				   ,null
				   ,0
				   ,'GestionProveedor'
				   ,'Adminsitrador'
				   ,GETDATE()
				   ,null
				   ,null)
	END
GO


