﻿USE SIA
GO
--------------------------------------------------------------
-- Adiciona campo a tabla [Proveedor].[ClasedeEntidad]
-- Fecha: 8 de maypo de 2015
-- Autor: Luz Angela Borda
--------------------------------------------------------------

IF NOT EXISTS
	(
		SELECT	*
		FROM	INFORMATION_SCHEMA.COLUMNS
		WHERE	TABLE_NAME = 'ClasedeEntidad'
		AND		TABLE_SCHEMA = 'PROVEEDOR'
		AND		COLUMN_NAME = 'IdRegimenTributario'
	)
	BEGIN
		alter table [Proveedor].[ClasedeEntidad] add IdRegimenTributario int null
	END
go

----------------------------------------------------------------------------------------------------------------
-- Inserta registro nuevo en la tabla de [ClasedeEntidad] para INSTITUCION DE EDUCACION SUPERIOR
----------------------------------------------------------------------------------------------------------------

IF NOT EXISTS
	(
		SELECT	*
		FROM	[Proveedor].[ClasedeEntidad]
		WHERE	Descripcion = 'INSTITUCION DE EDUCACION SUPERIOR'
	)
	BEGIN
		insert [Proveedor].[ClasedeEntidad]
		values (
			1,
			2,
			28, 
			'INSTITUCION DE EDUCACION SUPERIOR',
			1,
			'Administrador',
			getdate(),
			null,
			null,
			4
			)
	END
GO

----------------------------------------------------------------------------------------------------------------
-- aactualiza registros restantes
----------------------------------------------------------------------------------------------------------------
 update [Proveedor].[ClasedeEntidad] set IdRegimenTributario = 1 where CodigoClasedeEntidad <> 28
