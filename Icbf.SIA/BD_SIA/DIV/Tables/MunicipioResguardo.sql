﻿CREATE TABLE [DIV].[MunicipioResguardo] (
    [IdMunicipioResguardo] INT            IDENTITY (1, 1) NOT NULL,
    [CodigoMunicipio]      NVARCHAR (5)   NOT NULL,
    [CodigoResguardo]      NVARCHAR (3)   NOT NULL,
    [IdMunicipio]          INT            NOT NULL,
    [IdResguardo]          INT            NOT NULL,
    [Estado]               INT            NOT NULL,
    [UsuarioCrea]          NVARCHAR (250) NULL,
    [FechaCrea]            DATETIME       NULL,
    [UsuarioModifica]      NVARCHAR (250) NULL,
    [FechaModifica]        DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdMunicipioResguardo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_MunicipioResguardo_Municipio] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio]),
    CONSTRAINT [FK_MunicipioResguardo_Resguardo] FOREIGN KEY ([IdResguardo]) REFERENCES [DIV].[Resguardo] ([IdResguardo])
);

