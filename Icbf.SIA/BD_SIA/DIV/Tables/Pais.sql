﻿CREATE TABLE [DIV].[Pais] (
    [IdPais]          INT            IDENTITY (1, 1) NOT NULL,
    [CodigoPais]      NVARCHAR (3)   NOT NULL,
    [NombrePais]      NVARCHAR (50)  NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NULL,
    [FechaCrea]       DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdPais] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__CodigoPais] UNIQUE NONCLUSTERED ([CodigoPais] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombrePais] UNIQUE NONCLUSTERED ([NombrePais] ASC) WITH (FILLFACTOR = 90)
);

