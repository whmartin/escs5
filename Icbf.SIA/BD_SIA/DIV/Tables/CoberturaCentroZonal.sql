﻿CREATE TABLE [DIV].[CoberturaCentroZonal] (
    [IdCoberturaCentroZonal] INT            IDENTITY (1, 1) NOT NULL,
    [IdMunicipio]            INT            NOT NULL,
    [IdCentroZonal]          INT            NOT NULL,
    [CodigoMunicipio]        NVARCHAR (5)   NOT NULL,
    [CodigoCentroZonal]      NVARCHAR (4)   NOT NULL,
    [Estado]                 BIT            NOT NULL,
    [UsuarioCrea]            NVARCHAR (250) NOT NULL,
    [FechaCrea]              DATETIME       NOT NULL,
    [UsuarioModifica]        NVARCHAR (250) NULL,
    [FechaModifica]          DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdCoberturaCentroZonal] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CoberturaCentroZonal_CentroZonal_IdCentroZonal] FOREIGN KEY ([IdCentroZonal]) REFERENCES [DIV].[CentroZonal] ([IdCentroZonal]),
    CONSTRAINT [FK_CoberturaCentroZonal_Municipio_IdMunicipio] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio])
);

