﻿CREATE TABLE [DIV].[Barrio] (
    [IdBarrio]        INT            IDENTITY (1, 1) NOT NULL,
    [IdComuna]        INT            NOT NULL,
    [CodigoBarrio]    NVARCHAR (10)  NOT NULL,
    [NombreBarrio]    NVARCHAR (55)  NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NULL,
    [FechaCrea]       DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdBarrio] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Barrio_Comuna] FOREIGN KEY ([IdComuna]) REFERENCES [DIV].[Comuna] ([IdComuna]),
    CONSTRAINT [CU__CodigoBarrio] UNIQUE NONCLUSTERED ([CodigoBarrio] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreBarrio] UNIQUE NONCLUSTERED ([CodigoBarrio] ASC, [NombreBarrio] ASC) WITH (FILLFACTOR = 90)
);

