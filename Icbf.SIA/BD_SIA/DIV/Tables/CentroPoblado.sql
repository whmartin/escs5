﻿CREATE TABLE [DIV].[CentroPoblado] (
    [IdCentroPoblado]          INT            IDENTITY (1, 1) NOT NULL,
    [IdDepartamento]           INT            NOT NULL,
    [IdMunicipio]              INT            NOT NULL,
    [CodigoCentroPoblado]      NVARCHAR (8)   NOT NULL,
    [NombreCentroPoblado]      NVARCHAR (100) NOT NULL,
    [IdCategoriaCentroPoblado] INT            NOT NULL,
    [Estado]                   INT            NOT NULL,
    [UsuarioCrea]              NVARCHAR (250) NULL,
    [FechaCrea]                DATETIME       NULL,
    [UsuarioModifica]          NVARCHAR (250) NULL,
    [FechaModifica]            DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdCentroPoblado] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CentroPoblado_CategoriaCentroPoblado] FOREIGN KEY ([IdCategoriaCentroPoblado]) REFERENCES [DIV].[CategoriaCentroPoblado] ([IdCategoriaCentroPoblado]),
    CONSTRAINT [FK_CentroPoblado_Departamento] FOREIGN KEY ([IdDepartamento]) REFERENCES [DIV].[Departamento] ([IdDepartamento]),
    CONSTRAINT [FK_CentroPoblado_Municipio] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio]),
    CONSTRAINT [CU__CodigoCentroPoblado] UNIQUE NONCLUSTERED ([CodigoCentroPoblado] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreCentroPoblado] UNIQUE NONCLUSTERED ([CodigoCentroPoblado] ASC, [NombreCentroPoblado] ASC) WITH (FILLFACTOR = 90)
);

