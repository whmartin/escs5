﻿CREATE TABLE [DIV].[Cobertura] (
    [IdCobertura]     INT            IDENTITY (1, 1) NOT NULL,
    [IdCentroZonal]   INT            NOT NULL,
    [IdMunicipio]     INT            NOT NULL,
    [IdVigencia]      INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Cobertura] PRIMARY KEY CLUSTERED ([IdCobertura] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Cobertura_CentroZonal] FOREIGN KEY ([IdCentroZonal]) REFERENCES [DIV].[CentroZonal] ([IdCentroZonal]),
    CONSTRAINT [FK_Cobertura_Municipio] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio]),
    CONSTRAINT [FK_Cobertura_Municipio1] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio]),
    CONSTRAINT [FK_Cobertura_Vigencia] FOREIGN KEY ([IdVigencia]) REFERENCES [Global].[Vigencia] ([IdVigencia])
);

