﻿CREATE TABLE [DIV].[CategoriaCentroPoblado] (
    [IdCategoriaCentroPoblado]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoCategoriaCentroPoblado] NVARCHAR (5)   NOT NULL,
    [NombreCategoriaCentroPoblado] NVARCHAR (40)  NOT NULL,
    [Estado]                       INT            NOT NULL,
    [UsuarioCrea]                  NVARCHAR (250) NULL,
    [FechaCrea]                    DATETIME       NULL,
    [UsuarioModifica]              NVARCHAR (250) NULL,
    [FechaModifica]                DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdCategoriaCentroPoblado] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__CodigoCategoriaCentroPoblado] UNIQUE NONCLUSTERED ([CodigoCategoriaCentroPoblado] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreCategoriaCentroPoblado] UNIQUE NONCLUSTERED ([NombreCategoriaCentroPoblado] ASC) WITH (FILLFACTOR = 90)
);

