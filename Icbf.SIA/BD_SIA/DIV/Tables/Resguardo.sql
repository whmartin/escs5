﻿CREATE TABLE [DIV].[Resguardo] (
    [IdResguardo]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoResguardo] NVARCHAR (10)  NOT NULL,
    [NombreResguardo] NVARCHAR (100) NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NULL,
    [FechaCrea]       DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdResguardo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__CodigoResguardo] UNIQUE NONCLUSTERED ([CodigoResguardo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreResguardo] UNIQUE NONCLUSTERED ([CodigoResguardo] ASC, [NombreResguardo] ASC) WITH (FILLFACTOR = 90)
);

