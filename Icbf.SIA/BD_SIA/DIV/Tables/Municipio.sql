﻿CREATE TABLE [DIV].[Municipio] (
    [IdMunicipio]     INT            IDENTITY (1, 1) NOT NULL,
    [IdDepartamento]  INT            NOT NULL,
    [CodigoMunicipio] NVARCHAR (5)   NOT NULL,
    [NombreMunicipio] NVARCHAR (50)  NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NULL,
    [FechaCrea]       DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdMunicipio] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Municipio_Departamento] FOREIGN KEY ([IdDepartamento]) REFERENCES [DIV].[Departamento] ([IdDepartamento]),
    CONSTRAINT [CU__CodigoMunicipio] UNIQUE NONCLUSTERED ([CodigoMunicipio] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreMunicipio] UNIQUE NONCLUSTERED ([CodigoMunicipio] ASC, [NombreMunicipio] ASC) WITH (FILLFACTOR = 90)
);

