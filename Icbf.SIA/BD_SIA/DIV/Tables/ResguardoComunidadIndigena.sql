﻿CREATE TABLE [DIV].[ResguardoComunidadIndigena] (
    [IdResguardoComunidadIndigena] INT            IDENTITY (1, 1) NOT NULL,
    [CodigoMunicipio]              NVARCHAR (5)   NOT NULL,
    [CodigoResguardo]              NVARCHAR (3)   NOT NULL,
    [CodigoComunidadIndigena]      NVARCHAR (3)   NOT NULL,
    [IdMunicipio]                  INT            NOT NULL,
    [IdResguardo]                  INT            NOT NULL,
    [IdComunidadIndigena]          INT            NOT NULL,
    [Estado]                       BIT            NOT NULL,
    [UsuarioCrea]                  NVARCHAR (250) NOT NULL,
    [FechaCrea]                    DATETIME       NOT NULL,
    [UsuarioModifica]              NVARCHAR (250) NULL,
    [FechaModifica]                DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdResguardoComunidadIndigena] ASC) WITH (FILLFACTOR = 90)
);

