﻿CREATE TABLE [DIV].[ComunidadIndigena] (
    [IdComunidadIndigena]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoComunidadIndigena] NVARCHAR (4)   NOT NULL,
    [NombreComunidadIndigena] NVARCHAR (100) NOT NULL,
    [Estado]                  BIT            NOT NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdComunidadIndigena] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__CodigoComunidadIndigena] UNIQUE NONCLUSTERED ([CodigoComunidadIndigena] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreComunidadIndigena] UNIQUE NONCLUSTERED ([NombreComunidadIndigena] ASC) WITH (FILLFACTOR = 90)
);

