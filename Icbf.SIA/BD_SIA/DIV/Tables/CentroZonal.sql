﻿CREATE TABLE [DIV].[CentroZonal] (
    [IdCentroZonal]     INT            IDENTITY (1, 1) NOT NULL,
    [IdMunicipio]       INT            NOT NULL,
    [CodigoMunicipio]   NVARCHAR (5)   NOT NULL,
    [CodigoCentroZonal] NVARCHAR (4)   NOT NULL,
    [NombreCentroZonal] NVARCHAR (45)  NOT NULL,
    [Direccion]         NVARCHAR (100) NOT NULL,
    [Telefonos]         NVARCHAR (100) NOT NULL,
    [Estado]            INT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NULL,
    [FechaCrea]         DATETIME       NULL,
    [UsuarioModifica]   NVARCHAR (250) NULL,
    [FechaModifica]     DATETIME       NULL,
    [IdRegional]        INT            NULL,
    CONSTRAINT [PK__CentroZo__1C027691442B18F2] PRIMARY KEY CLUSTERED ([IdCentroZonal] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CentroZonal_Municipio] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio]),
    CONSTRAINT [FK_CentroZonal_Regional] FOREIGN KEY ([IdRegional]) REFERENCES [DIV].[Regional] ([IdRegional]),
    CONSTRAINT [CU__CodigoCentroZonal] UNIQUE NONCLUSTERED ([CodigoCentroZonal] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreCentroZonal] UNIQUE NONCLUSTERED ([CodigoCentroZonal] ASC, [NombreCentroZonal] ASC) WITH (FILLFACTOR = 90)
);

