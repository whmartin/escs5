﻿CREATE TABLE [DIV].[Departamento] (
    [IdDepartamento]     INT            IDENTITY (1, 1) NOT NULL,
    [IdPais]             INT            NOT NULL,
    [CodigoDepartamento] NVARCHAR (2)   NOT NULL,
    [NombreDepartamento] NVARCHAR (25)  NOT NULL,
    [Estado]             INT            NOT NULL,
    [UsuarioCrea]        NVARCHAR (250) NULL,
    [FechaCrea]          DATETIME       NULL,
    [UsuarioModifica]    NVARCHAR (250) NULL,
    [FechaModifica]      DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdDepartamento] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Departamento_Pais] FOREIGN KEY ([IdPais]) REFERENCES [DIV].[Pais] ([IdPais]),
    CONSTRAINT [CU__CodigoDepartamento] UNIQUE NONCLUSTERED ([CodigoDepartamento] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreDepartamento] UNIQUE NONCLUSTERED ([NombreDepartamento] ASC) WITH (FILLFACTOR = 90)
);

