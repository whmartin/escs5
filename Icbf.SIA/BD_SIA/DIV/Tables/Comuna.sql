﻿CREATE TABLE [DIV].[Comuna] (
    [IdComuna]        INT            IDENTITY (1, 1) NOT NULL,
    [IdMunicipio]     INT            NOT NULL,
    [CodigoComuna]    NVARCHAR (7)   NOT NULL,
    [NombreComuna]    NVARCHAR (60)  NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NULL,
    [FechaCrea]       DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdComuna] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Comuna_Municipio] FOREIGN KEY ([IdMunicipio]) REFERENCES [DIV].[Municipio] ([IdMunicipio]),
    CONSTRAINT [CU__CodigoComuna] UNIQUE NONCLUSTERED ([CodigoComuna] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreComuna] UNIQUE NONCLUSTERED ([CodigoComuna] ASC, [NombreComuna] ASC) WITH (FILLFACTOR = 90)
);

