﻿CREATE TABLE [DIV].[Regional] (
    [IdRegional]      INT            IDENTITY (1, 1) NOT NULL,
    [CodigoRegional]  NVARCHAR (2)   NOT NULL,
    [NombreRegional]  NVARCHAR (256) NOT NULL,
    [Estado]          INT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NULL,
    [FechaCrea]       DATETIME       NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [codPCI]          NVARCHAR (20)  NULL,
    CONSTRAINT [PK__Regional__FC9034F833008CF0] PRIMARY KEY CLUSTERED ([IdRegional] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__CodigoRegional] UNIQUE NONCLUSTERED ([CodigoRegional] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombreRegional] UNIQUE NONCLUSTERED ([NombreRegional] ASC) WITH (FILLFACTOR = 90)
);

