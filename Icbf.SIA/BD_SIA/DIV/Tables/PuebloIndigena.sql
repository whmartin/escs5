﻿CREATE TABLE [DIV].[PuebloIndigena] (
    [IdPuebloIndigena]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoPuebloIndigena] NVARCHAR (4)   NOT NULL,
    [NombrePuebloIndigena] NVARCHAR (100) NOT NULL,
    [Estado]               INT            NOT NULL,
    [UsuarioCrea]          NVARCHAR (250) NULL,
    [FechaCrea]            DATETIME       NULL,
    [UsuarioModifica]      NVARCHAR (250) NULL,
    [FechaModifica]        DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IdPuebloIndigena] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__CodigoPuebloIndigena] UNIQUE NONCLUSTERED ([CodigoPuebloIndigena] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CU__NombrePuebloIndigena] UNIQUE NONCLUSTERED ([NombrePuebloIndigena] ASC) WITH (FILLFACTOR = 90)
);

