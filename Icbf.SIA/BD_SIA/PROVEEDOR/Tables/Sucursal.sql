﻿CREATE TABLE [PROVEEDOR].[Sucursal] (
    [IdSucursal]      INT            IDENTITY (1, 1) NOT NULL,
    [IdEntidad]       INT            NOT NULL,
    [Indicativo]      INT            NULL,
    [Telefono]        INT            NULL,
    [Extension]       NUMERIC (10)   NULL,
    [Celular]         NUMERIC (10)   NULL,
    [Correo]          NVARCHAR (256) NULL,
    [Estado]          INT            NOT NULL,
    [Departamento]    INT            NULL,
    [Municipio]       INT            NULL,
    [IdZona]          INT            NULL,
    [Direccion]       NVARCHAR (256) NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [Nombre]          NVARCHAR (256) NULL,
    [Editable]        INT            NULL,
    CONSTRAINT [PK_IdSucursal] PRIMARY KEY CLUSTERED ([IdSucursal] ASC),
    CONSTRAINT [FK_Sucursal_IdEntidad] FOREIGN KEY ([IdEntidad]) REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
);

