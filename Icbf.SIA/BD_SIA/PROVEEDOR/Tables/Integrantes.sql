﻿CREATE TABLE [PROVEEDOR].[Integrantes] (
    [IdIntegrante]                       INT            IDENTITY (1, 1) NOT NULL,
    [IdEntidad]                          INT            NOT NULL,
    [IdTipoPersona]                      INT            NOT NULL,
    [IdTipoIdentificacionPersonaNatural] INT            NOT NULL,
    [NumeroIdentificacion]               NVARCHAR (256) NULL,
    [PorcentajeParticipacion]            DECIMAL (5, 2) NOT NULL,
    [ConfirmaCertificado]                NVARCHAR (5)   NULL,
    [ConfirmaPersona]                    NVARCHAR (5)   NULL,
    [UsuarioCrea]                        NVARCHAR (250) NOT NULL,
    [FechaCrea]                          DATETIME       NOT NULL,
    [UsuarioModifica]                    NVARCHAR (250) NULL,
    [FechaModifica]                      DATETIME       NULL,
    CONSTRAINT [PK_Integrantes] PRIMARY KEY CLUSTERED ([IdIntegrante] ASC)
);


GO

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 05/AGO/2014
-- Description:	Determina que estado colocar al módulo integrantes cuando se realizan
-- cambios en alguno de los integrantes asociados a una Entidad.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_ProveedorIntegrantes] 
   ON [PROVEEDOR].[Integrantes] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN

DECLARE @contador INT=0
DECLARE @IdEntidad INT
SELECT @contador=COUNT(IdEntidad) FROM deleted
IF (@contador >0)
BEGIN
	SELECT @IdEntidad=IdEntidad FROM deleted	
END
ELSE
BEGIN
	SELECT @IdEntidad=IdEntidad FROM inserted
END
EXEC [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado] @IdEntidad
END


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tabla que Almacena los datos de los integrantes del proveedor', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Es el id', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'IdIntegrante';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id del Proveedor', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'IdEntidad';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tipo de Persona o Asociación', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'IdTipoPersona';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tipo de Identificación', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'IdTipoIdentificacionPersonaNatural';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Número de Identificación', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'NumeroIdentificacion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Porcentaje de Participación', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'PorcentajeParticipacion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Confirma Certificado', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'ConfirmaCertificado';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Confirma Persona', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'ConfirmaPersona';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Usuario creador del registro', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'UsuarioCrea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de creación del registro', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'FechaCrea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Usuario modificador del registro', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'UsuarioModifica';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de modificación del registro', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'Integrantes', @level2type = N'COLUMN', @level2name = N'FechaModifica';

