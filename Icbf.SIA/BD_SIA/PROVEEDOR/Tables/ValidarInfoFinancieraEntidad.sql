﻿CREATE TABLE [PROVEEDOR].[ValidarInfoFinancieraEntidad] (
    [IdValidarInfoFinancieraEntidad] INT            IDENTITY (1, 1) NOT NULL,
    [NroRevision]                    INT            NOT NULL,
    [IdInfoFin]                      INT            NOT NULL,
    [ConfirmaYAprueba]               BIT            NOT NULL,
    [Observaciones]                  NVARCHAR (200) NOT NULL,
    [FechaCrea]                      DATETIME       NOT NULL,
    [UsuarioCrea]                    NVARCHAR (128) NOT NULL,
    [FechaModifica]                  DATETIME       NULL,
    [UsuarioModifica]                NVARCHAR (128) NULL,
    [CorreoEnviado]                  BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_IdValidarInfoFinancieraEntidad] PRIMARY KEY CLUSTERED ([IdValidarInfoFinancieraEntidad] ASC),
    CONSTRAINT [IdInfoFinancieraEntidad_InfoFinancieraEntidad] FOREIGN KEY ([IdInfoFin]) REFERENCES [PROVEEDOR].[InfoFinancieraEntidad] ([IdInfoFin])
);

