﻿CREATE TABLE [PROVEEDOR].[ParametrosEmail] (
    [IdParametroID]              INT            IDENTITY (1, 1) NOT NULL,
    [CodigoParametroEmail]       NVARCHAR (100) NOT NULL,
    [DescripcionParametrosEmail] NVARCHAR (255) NULL,
    [ValorMesesParametroEmail]   INT            NULL,
    CONSTRAINT [UNIC_ParametrosEmail] UNIQUE NONCLUSTERED ([CodigoParametroEmail] ASC)
);

