﻿CREATE TABLE [PROVEEDOR].[TipodeentidadPublica] (
    [IdTipodeentidadPublica]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoTipodeentidadPublica] NVARCHAR (128) NOT NULL,
    [Descripcion]                NVARCHAR (128) NOT NULL,
    [Estado]                     BIT            NOT NULL,
    [UsuarioCrea]                NVARCHAR (250) NOT NULL,
    [FechaCrea]                  DATETIME       NOT NULL,
    [UsuarioModifica]            NVARCHAR (250) NULL,
    [FechaModifica]              DATETIME       NULL,
    CONSTRAINT [PK_TipodeentidadPublica] PRIMARY KEY CLUSTERED ([IdTipodeentidadPublica] ASC)
);

