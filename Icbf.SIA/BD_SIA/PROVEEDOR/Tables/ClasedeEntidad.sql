﻿CREATE TABLE [PROVEEDOR].[ClasedeEntidad] (
    [IdClasedeEntidad]     INT            IDENTITY (1, 1) NOT NULL,
    [IdTipodeEntidad]      INT            NOT NULL,
    [IdTipodeActividad]    INT            NOT NULL,
    [CodigoClasedeEntidad] VARCHAR (10)   NULL,
    [Descripcion]          NVARCHAR (128) NOT NULL,
    [Estado]               BIT            NOT NULL,
    [UsuarioCrea]          NVARCHAR (250) NOT NULL,
    [FechaCrea]            DATETIME       NOT NULL,
    [UsuarioModifica]      NVARCHAR (250) NULL,
    [FechaModifica]        DATETIME       NULL,
    CONSTRAINT [PK_ClasedeEntidad] PRIMARY KEY CLUSTERED ([IdClasedeEntidad] ASC, [IdTipodeEntidad] ASC, [IdTipodeActividad] ASC),
    CONSTRAINT [FK_ClasedeEntidad_TipodeActividad] FOREIGN KEY ([IdTipodeActividad]) REFERENCES [PROVEEDOR].[TipodeActividad] ([IdTipodeActividad]),
    CONSTRAINT [FK_ClasedeEntidad_Tipoentidad] FOREIGN KEY ([IdTipodeEntidad]) REFERENCES [PROVEEDOR].[TipoEntidad] ([IdTipoentidad])
);

