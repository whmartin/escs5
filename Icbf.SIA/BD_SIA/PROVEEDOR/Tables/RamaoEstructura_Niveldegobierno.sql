﻿CREATE TABLE [PROVEEDOR].[RamaoEstructura_Niveldegobierno] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [IdRamaEstructura]  INT            NOT NULL,
    [IdNiveldegobierno] INT            NOT NULL,
    [Estado]            BIT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    CONSTRAINT [PK_RamaoEstructura_Niveldegobierno] PRIMARY KEY CLUSTERED ([Id] ASC, [IdNiveldegobierno] ASC, [IdRamaEstructura] ASC)
);

