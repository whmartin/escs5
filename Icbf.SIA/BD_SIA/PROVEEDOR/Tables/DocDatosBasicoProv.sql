﻿CREATE TABLE [PROVEEDOR].[DocDatosBasicoProv] (
    [IdDocAdjunto]    INT            IDENTITY (1, 1) NOT NULL,
    [IdEntidad]       INT            NULL,
    [NombreDocumento] NVARCHAR (128) NOT NULL,
    [LinkDocumento]   NVARCHAR (256) NOT NULL,
    [Observaciones]   NVARCHAR (256) NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [IdTipoDocumento] INT            NULL,
    [IdTemporal]      VARCHAR (20)   NULL,
    [Activo]          BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_DocDatosBasicoProv] PRIMARY KEY CLUSTERED ([IdDocAdjunto] ASC),
    CONSTRAINT [FK_DocDatosBasicoProv_EntidadProvOferente] FOREIGN KEY ([IdEntidad]) REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad]),
    CONSTRAINT [FK_DocDatosBasicoProv_TipoDocumento] FOREIGN KEY ([IdTipoDocumento]) REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
);

