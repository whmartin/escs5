﻿CREATE TABLE [PROVEEDOR].[ReglasEstadoProveedor] (
    [IdValidacionEstado]       INT            IDENTITY (1, 1) NOT NULL,
    [IdEstadoDatosBasicos]     INT            NOT NULL,
    [IdEstadoDatosFinancieros] INT            NULL,
    [IdEstadoDatosExperiencia] INT            NULL,
    [IdEstadoProveedor]        INT            NOT NULL,
    [Estado]                   BIT            CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_Estado] DEFAULT ((1)) NOT NULL,
    [UsuarioCrea]              NVARCHAR (100) CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_UsuarioCrea] DEFAULT (N'Administrador') NULL,
    [FechaCrea]                DATETIME       CONSTRAINT [DF_PROVEEDOR.ReglasEstadoProveedor_FechaCrea] DEFAULT (getdate()) NULL,
    [UsuarioModifica]          NVARCHAR (100) NULL,
    [Fechamodifica]            DATETIME       NULL,
    [IdEstadoIntegrantes]      INT            NULL,
    CONSTRAINT [PK_PROVEEDOR.ReglasEstadoProveedor] PRIMARY KEY CLUSTERED ([IdValidacionEstado] ASC),
    CONSTRAINT [FK_ReglasEstadoProveedor_EstadoDatosBasicos] FOREIGN KEY ([IdEstadoDatosBasicos]) REFERENCES [PROVEEDOR].[EstadoDatosBasicos] ([IdEstadoDatosBasicos]),
    CONSTRAINT [FK_ReglasEstadoProveedor_EstadoProveedor] FOREIGN KEY ([IdEstadoProveedor]) REFERENCES [PROVEEDOR].[EstadoProveedor] ([IdEstadoProveedor])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_PROVEEDOR_ReglasEstadoProveedor]
    ON [PROVEEDOR].[ReglasEstadoProveedor]([IdEstadoDatosBasicos] ASC, [IdEstadoDatosFinancieros] ASC, [IdEstadoDatosExperiencia] ASC, [IdEstadoIntegrantes] ASC);

