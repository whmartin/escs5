﻿CREATE TABLE [PROVEEDOR].[EstadoProveedor] (
    [IdEstadoProveedor] INT            IDENTITY (1, 1) NOT NULL,
    [Descripcion]       NVARCHAR (128) NOT NULL,
    [Estado]            BIT            NOT NULL,
    [IdEstadoTercero]   INT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    [UsuarioModifica]   NVARCHAR (250) NULL,
    [FechaModifica]     DATETIME       NULL,
    CONSTRAINT [PK_EstadoProveedor] PRIMARY KEY CLUSTERED ([IdEstadoProveedor] ASC)
);

