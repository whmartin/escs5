﻿CREATE TABLE [PROVEEDOR].[Acuerdos] (
    [IdAcuerdo]       INT            IDENTITY (1, 1) NOT NULL,
    [Nombre]          NVARCHAR (64)  NOT NULL,
    [ContenidoHtml]   NVARCHAR (MAX) NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_Acuerdos] PRIMARY KEY CLUSTERED ([IdAcuerdo] ASC)
);

