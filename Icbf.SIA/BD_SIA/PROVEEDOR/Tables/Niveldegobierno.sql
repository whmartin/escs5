﻿CREATE TABLE [PROVEEDOR].[Niveldegobierno] (
    [IdNiveldegobierno]     INT            IDENTITY (1, 1) NOT NULL,
    [IdRamaEstructura]      INT            NOT NULL,
    [CodigoNiveldegobierno] NVARCHAR (128) NOT NULL,
    [Descripcion]           NVARCHAR (128) NOT NULL,
    [Estado]                BIT            NOT NULL,
    [UsuarioCrea]           NVARCHAR (250) NOT NULL,
    [FechaCrea]             DATETIME       NOT NULL,
    [UsuarioModifica]       NVARCHAR (250) NULL,
    [FechaModifica]         DATETIME       NULL,
    CONSTRAINT [PK_Niveldegobierno] PRIMARY KEY CLUSTERED ([IdNiveldegobierno] ASC)
);

