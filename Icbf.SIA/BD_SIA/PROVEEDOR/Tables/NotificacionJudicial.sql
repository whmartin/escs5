﻿CREATE TABLE [PROVEEDOR].[NotificacionJudicial] (
    [IdNotJudicial]   INT            IDENTITY (1, 1) NOT NULL,
    [IdEntidad]       INT            NULL,
    [IdDepartamento]  INT            NULL,
    [IdMunicipio]     INT            NULL,
    [IdZona]          INT            NULL,
    [Direccion]       NVARCHAR (250) NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_NotificacionJudicial] PRIMARY KEY CLUSTERED ([IdNotJudicial] ASC),
    CONSTRAINT [FK_NotificacionJudicial_EntidadProvOferente] FOREIGN KEY ([IdEntidad]) REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
);

