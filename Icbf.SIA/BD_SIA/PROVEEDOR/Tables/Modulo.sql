﻿CREATE TABLE [PROVEEDOR].[Modulo] (
    [IdModulo]        INT            IDENTITY (1, 1) NOT NULL,
    [NombreModulo]    NVARCHAR (50)  NOT NULL,
    [Orden]           INT            NOT NULL,
    [Estado]          NVARCHAR (1)   NOT NULL,
    [Ruta]            NVARCHAR (500) NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [CodigoModulo]    INT            NULL
);

