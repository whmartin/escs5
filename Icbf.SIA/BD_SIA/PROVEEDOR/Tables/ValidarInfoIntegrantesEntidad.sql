﻿CREATE TABLE [PROVEEDOR].[ValidarInfoIntegrantesEntidad] (
    [IdValidarInfoIntegrantesEntidad] INT            IDENTITY (1, 1) NOT NULL,
    [NroRevision]                     INT            NOT NULL,
    [IdEntidad]                       INT            NOT NULL,
    [ConfirmaYAprueba]                BIT            NULL,
    [Observaciones]                   NVARCHAR (200) NOT NULL,
    [FechaCrea]                       DATETIME       NOT NULL,
    [UsuarioCrea]                     NVARCHAR (128) NOT NULL,
    [FechaModifica]                   DATETIME       NULL,
    [UsuarioModifica]                 NVARCHAR (128) NULL,
    [CorreoEnviado]                   BIT            DEFAULT ((0)) NOT NULL,
    [Activo]                          BIT            DEFAULT ((1)) NULL,
    CONSTRAINT [PK_IdValidarInfoIntegrantesEntidad] PRIMARY KEY CLUSTERED ([IdValidarInfoIntegrantesEntidad] ASC),
    CONSTRAINT [IdInfoIntegrantesEntidad_InfoIntegrantesEntidad] FOREIGN KEY ([IdEntidad]) REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key {0} Column', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'ValidarInfoIntegrantesEntidad', @level2type = N'COLUMN', @level2name = N'IdValidarInfoIntegrantesEntidad';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Link to EntidadProvOferente table', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'ValidarInfoIntegrantesEntidad', @level2type = N'COLUMN', @level2name = N'IdEntidad';

