﻿CREATE TABLE [PROVEEDOR].[EntidadProvOferente] (
    [IdEntidad]                 INT            IDENTITY (1, 1) NOT NULL,
    [ConsecutivoInterno]        NVARCHAR (128) NULL,
    [TipoEntOfProv]             BIT            NULL,
    [IdTercero]                 INT            NOT NULL,
    [IdTipoCiiuPrincipal]       INT            NULL,
    [IdTipoCiiuSecundario]      INT            NULL,
    [IdTipoSector]              INT            NULL,
    [IdTipoClaseEntidad]        INT            NULL,
    [IdTipoRamaPublica]         INT            NULL,
    [IdTipoNivelGob]            INT            NULL,
    [IdTipoNivelOrganizacional] INT            NULL,
    [IdTipoCertificadorCalidad] INT            NULL,
    [FechaCiiuPrincipal]        DATETIME       NULL,
    [FechaCiiuSecundario]       DATETIME       NULL,
    [FechaConstitucion]         DATETIME       NULL,
    [FechaVigencia]             DATETIME       NULL,
    [FechaMatriculaMerc]        DATETIME       NULL,
    [FechaExpiracion]           DATETIME       NULL,
    [TipoVigencia]              BIT            NULL,
    [ExenMatriculaMer]          BIT            NULL,
    [MatriculaMercantil]        NVARCHAR (20)  NULL,
    [ObserValidador]            NVARCHAR (256) NULL,
    [AnoRegistro]               INT            NULL,
    [IdEstado]                  INT            NULL,
    [UsuarioCrea]               NVARCHAR (250) NOT NULL,
    [FechaCrea]                 DATETIME       NOT NULL,
    [UsuarioModifica]           NVARCHAR (250) NULL,
    [FechaModifica]             DATETIME       NULL,
    [IdAcuerdo]                 INT            NULL,
    [NroRevision]               INT            NULL,
    [Finalizado]                BIT            NULL,
    [IdEstadoProveedor]         INT            CONSTRAINT [IdEstadoProveedorOfAssociate] DEFAULT ([dbo].[GetIdEstadoProveedorDEFAULT]()) NOT NULL,
    [NumIntegrantes]            INT            NULL,
    [OferentesMigrados]         BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_EntidadProvOferente] PRIMARY KEY CLUSTERED ([IdEntidad] ASC),
    CONSTRAINT [FK_EntidadProvOferente_EstadoDatosBasicos] FOREIGN KEY ([IdEstado]) REFERENCES [PROVEEDOR].[EstadoDatosBasicos] ([IdEstadoDatosBasicos]),
    CONSTRAINT [FK_EntidadProvOferente_Niveldegobierno] FOREIGN KEY ([IdTipoNivelGob]) REFERENCES [PROVEEDOR].[Niveldegobierno] ([IdNiveldegobierno]),
    CONSTRAINT [FK_EntidadProvOferente_NivelOrganizacional] FOREIGN KEY ([IdTipoNivelOrganizacional]) REFERENCES [PROVEEDOR].[NivelOrganizacional] ([IdNivelOrganizacional]),
    CONSTRAINT [FK_EntidadProvOferente_TipoCiiu] FOREIGN KEY ([IdTipoCiiuPrincipal]) REFERENCES [PROVEEDOR].[TipoCiiu] ([IdTipoCiiu]),
    CONSTRAINT [fk_IdAcuerdoProveedor] FOREIGN KEY ([IdAcuerdo]) REFERENCES [PROVEEDOR].[Acuerdos] ([IdAcuerdo]),
    CONSTRAINT [FK_Proveedor_EstadoProveedor] FOREIGN KEY ([IdEstadoProveedor]) REFERENCES [PROVEEDOR].[EstadoProveedor] ([IdEstadoProveedor])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_EntidadProvOferente_IdTercero]
    ON [PROVEEDOR].[EntidadProvOferente]([IdTercero] ASC);


GO




-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 11-Abril-2014
-- Description:	Cuando se inserta un Proveedor, se actualizan el estado de Proveedor 
-- en base a la tabla de Proveedor.ReglasEstadoProveedor
--Nota.- Tal vez de inicio no es necesario, pero se ha creado pensando que si cambian las reglas de
-- Estado, solo habria q hacer el Insert en la tabla de reglas y listo.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[EntidadProvOferenteINSERT] 
   ON  [PROVEEDOR].[EntidadProvOferente]
   AFTER INSERT
AS 
BEGIN
DECLARE @IdEntidad INT
,@IdEdoDatosBasicos INT
,@IdEstadoProveedor INT

SELECT @IdEntidad=IdEntidad,
@IdEdoDatosBasicos=IdEstado
FROM inserted

SELECT @IdEstadoProveedor=IdEstadoProveedor
FROM dbo.GetIdEstadoProveedor(@IdEdoDatosBasicos,0,0,NULL,NULL,NULL,NULL)

UPDATE [PROVEEDOR].[EntidadProvOferente]
SET IdEstadoProveedor=@IdEstadoProveedor
WHERE IdEntidad=@IdEntidad
END





GO

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 05/AGO/2014
-- Description:	Este disparador se ejecutará cuando para un consorcio o unión temporal se 
-- modifique el número de integrantes.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_EntidadProvOferenteUPDATEIntegrantes] 
   ON  [PROVEEDOR].[EntidadProvOferente]
   AFTER UPDATE
AS 
BEGIN
	DECLARE @IdEntidad INT
	DECLARE @Index		INT
	DECLARE @IndexTotal INT
	DECLARE @CodigoTipoPersona NVARCHAR(5)

	DECLARE @TBL AS TABLE
	(
		Id		  INT,
		IdEntidad INT
	)

	INSERT @TBL(ID,IdEntidad)
	SELECT ROW_NUMBER() OVER (order by Identidad),IdEntidad
	FROM inserted

	SET @INDEX = 1
	SELECT @INDEXTOTAL = MAX(Id)
	FROM @TBL

	WHILE(@INDEX <= @INDEXTOTAL)
	BEGIN
		SELECT @IdEntidad=IdEntidad
		FROM @TBL
		WHERE Id = @INDEX

		SELECT @CodigoTipoPersona=CodigoTipoPersona 
		FROM Oferente.TipoPersona
		WHERE IdTipoPersona =(
		SELECT TER.IdTipoPersona FROM 
		PROVEEDOR.EntidadProvOferente PROV
		INNER JOIN Oferente.TERCERO TER ON
		PROV.IdTercero=TER.IDTERCERO
		WHERE PROV.IdEntidad=@IdEntidad)

		IF(@CodigoTipoPersona NOT IN ('001','002'))
		BEGIN
			IF((SELECT NumIntegrantes FROM inserted WHERE IdEntidad=@IdEntidad)!=(SELECT NumIntegrantes FROM deleted WHERE IdEntidad=@IdEntidad))
			BEGIN
				IF(@CodigoTipoPersona='003')
				BEGIN
					DECLARE @PORCENTAJE FLOAT;
					DECLARE @NumIntegrantes INT;

					SELECT @NumIntegrantes=NumIntegrantes 
					FROM PROVEEDOR.EntidadProvOferente
					WHERE IdEntidad=@IdEntidad
					SET @PORCENTAJE = CAST(100 AS FLOAT)/CAST(@NumIntegrantes AS FLOAT)
				
					UPDATE PROVEEDOR.Integrantes
					SET PorcentajeParticipacion=@PORCENTAJE
					WHERE IdEntidad=@IdEntidad
				END
				ELSE
				BEGIN
					EXEC [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado] @IdEntidad		
				END			
			END
		END

		SET @INDEX = @INDEX + 1
	END
END

GO

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 16-Abril-2014
-- Description:	Actualiza Estado de Proveedor y de Tercero(Sólo cuando es VALIDADO) despues de haber actualizado el estado del este módulo.
-- Modificación: 30/07/2014 Juan Carlos Valverde Sámano
-- Descripción: Se eliminó la parte que heredaba el estado VALIDADO del Proveedor al Tercero, 
-- Esto dado por el requerimiento de que Tercero nunca debe heredar estado de Proveedor.
-- También se agregó la parte para gestionar los cambios de estado cuando se trata de un proveedor
-- de tipo consorcio o unión temporal.
-- =============================================
CREATE TRIGGER [PROVEEDOR].[TR_EntidadProvOferenteUPDATE] 
   ON  [PROVEEDOR].[EntidadProvOferente]
   AFTER UPDATE
AS 
BEGIN
	DECLARE @IdEntidad INT
	DECLARE @Index		INT
	DECLARE @IndexTotal INT
	DECLARE @IdEstadoDatosBasicos INT,
	@IdEstadoInfoFinanciera INT,
	@IdEstadoInfoExperiencia INT,
	@IdEstadoIntegrantes INT,
	@EsParcial BIT,
	@EsEnValidacion BIT,
	@IdEdoValidacionDocumental INT=0,
	@IdEdoValidacionDocIntegrantes INT =0
	DECLARE @TBLESTADOS TABLE
	(IdEstadoProveedor INT)

	DECLARE @TBL AS TABLE
	(
		Id		  INT,
		IdEntidad INT
	)

	INSERT @TBL(ID,IdEntidad)
	SELECT ROW_NUMBER() OVER (order by Identidad),IdEntidad
	FROM inserted

	SET @INDEX = 1
	SELECT @INDEXTOTAL = MAX(Id)
	FROM @TBL

	WHILE(@INDEX <= @INDEXTOTAL)
	BEGIN
		SELECT @IdEntidad=IdEntidad
		FROM @TBL
		WHERE Id = @INDEX

		IF((SELECT IdEstado FROM inserted where Identidad = @IdEntidad)!=(SELECT IdEstado FROM deleted where Identidad = @IdEntidad))
		BEGIN

			SET @IdEstadoDatosBasicos = 0
			SET @IdEstadoInfoFinanciera = 0
			SET @IdEstadoInfoExperiencia = 0
			SET @IdEstadoIntegrantes = 0
			SET @EsParcial=0
			SET @EsEnValidacion=0
		
			DELETE FROM @TBLESTADOS
		
			DECLARE @IdTipoPersona INT =(		
			SELECT IdTipoPersona FROM Oferente.TERCERO TER
			INNER JOIN PROVEEDOR.EntidadProvOferente PROV
			ON PROV.IdTercero= TER.IDTERCERO
			WHERE PROV.IdEntidad=@IdEntidad)
		
			IF (@IdTipoPersona=1 OR @IdTipoPersona=2)
			BEGIN
				IF EXISTS(SELECT IdEstado FROM PROVEEDOR.EntidadProvOferente
				WHERE IdEntidad = @IdEntidad)
				BEGIN
				SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
				WHERE IdEntidad = @IdEntidad
				END

				--SI EXISTE AL MENOS UN REGISTRO EN InfoFinancieraEntidad
				IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
				WHERE IdEntidad = @IdEntidad)
				BEGIN
					--SE ASEGURA QUE SOLO EXISTA UN REGISTRO
					IF((SELECT COUNT(EstadoValidacion) FROM PROVEEDOR.InfoFinancieraEntidad
					WHERE IdEntidad = @IdEntidad)=1)
					BEGIN
						SELECT @IdEstadoInfoFinanciera=EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
						WHERE IdEntidad = @IdEntidad
					END
					--SI HAY MAS DE UN REGISTRO
					ELSE IF((SELECT COUNT(EstadoValidacion) FROM PROVEEDOR.InfoFinancieraEntidad
					WHERE IdEntidad = @IdEntidad)>1)
					BEGIN
						--SI, AUNQUE HAY VARIOS, TODOS TIENEN EL MISMO ESTADO.
						IF((SELECT COUNT(DISTINCT(EstadoValidacion)) FROM PROVEEDOR.InfoFinancieraEntidad
						WHERE IdEntidad = @IdEntidad)=1)
						BEGIN
							SELECT TOP(1) @IdEstadoInfoFinanciera=EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
							WHERE IdEntidad = @IdEntidad
						END
						--SI HAY VARIOS Y AL MENOS UNO TIENE ESTADO DIFERENTE
						IF((SELECT COUNT(DISTINCT(EstadoValidacion)) FROM PROVEEDOR.InfoFinancieraEntidad
						WHERE IdEntidad = @IdEntidad)>1)
						BEGIN
						SET @IdEdoValidacionDocumental=(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
						WHERE Descripcion='EN VALIDACIÓN')
					
						IF EXISTS(SELECT EstadoValidacion FROM PROVEEDOR.InfoFinancieraEntidad
							WHERE IdEntidad = @IdEntidad AND EstadoValidacion=@IdEdoValidacionDocumental)
						BEGIN
							SET @EsEnValidacion=1;
						END
						ELSE
						BEGIN
							SET @EsParcial=1;
						END
						END
					END
				END
			
			
				IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
				WHERE Descripcion='EN VALIDACIÓN')=@IdEstadoInfoFinanciera)
				BEGIN
				SET @EsEnValidacion=1;
				END

				--IF(@EsParcial=1)
				--BEGIN
				IF((SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
					WHERE Descripcion='EN VALIDACIÓN') =@IdEstadoDatosBasicos)
					BEGIN
						SET @EsEnValidacion=1;
					END
				--END
				IF( @EsEnValidacion=0)
				BEGIN
					--SI EXISTE AL MENOS UN REGISTRO EN InfoExperienciaEntidad
					IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
					WHERE IdEntidad = @IdEntidad)
					BEGIN
						--SE ASEGURA QUE SOLO EXISTA UN REGISTRO
						IF((SELECT COUNT(EstadoDocumental) FROM PROVEEDOR.InfoExperienciaEntidad
						WHERE IdEntidad = @IdEntidad)=1)
						BEGIN
							SELECT @IdEstadoInfoExperiencia=EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
							WHERE IdEntidad = @IdEntidad
							IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
							WHERE Descripcion='EN VALIDACIÓN')=@IdEstadoInfoExperiencia)
							BEGIN
								SET @EsEnValidacion=1;
							END
						END
						--SI EXISTE MAS DE UN REGISTRO
						ELSE IF((SELECT COUNT(EstadoDocumental) FROM PROVEEDOR.InfoExperienciaEntidad
						WHERE IdEntidad = @IdEntidad)>1)
						BEGIN
							--SI, AUNQUE HAY VARIOS, TODOS TIENEN EL MISMO ESTADO.
							IF((SELECT COUNT(DISTINCT(EstadoDocumental)) FROM PROVEEDOR.InfoExperienciaEntidad
							WHERE IdEntidad = @IdEntidad)=1)
							BEGIN
								SELECT TOP(1) @IdEstadoInfoExperiencia=EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
								WHERE IdEntidad = @IdEntidad
								IF((SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumentaL 
								WHERE Descripcion='EN VALIDACIÓN')=@IdEstadoInfoExperiencia)
								BEGIN
									SET @EsEnValidacion=1;
								END
							END
							ELSE 		--SI  HAY VARIOS Y AL MENOS UNO TIENE ESTADO DIFERENTE
							IF((SELECT COUNT(DISTINCT(EstadoDocumental)) FROM PROVEEDOR.InfoExperienciaEntidad
							WHERE IdEntidad = @IdEntidad)>1)
							BEGIN 
								--------------------
								SET @IdEdoValidacionDocumental=(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental
								WHERE Descripcion='EN VALIDACIÓN')
								IF EXISTS(SELECT EstadoDocumental FROM PROVEEDOR.InfoExperienciaEntidad
									WHERE IdEntidad = @IdEntidad AND EstadoDocumental=@IdEdoValidacionDocumental)
								BEGIN
									SET @EsEnValidacion=1;
								END
								ELSE
								BEGIN
									SET @EsParcial=1;
								END
								-----------
							END
						END
					END
				END


				IF (@EsEnValidacion=1)
				BEGIN
				INSERT INTO @TBLESTADOS
				SELECT IdEstadoProveedor
				FROM dbo.GetIdEstadoProveedor(0,0,0,NULL,1,NULL,NULL)
				END
				ELSE IF(@EsParcial=1)
				BEGIN
				INSERT INTO @TBLESTADOS
				SELECT IdEstadoProveedor
				FROM dbo.GetIdEstadoProveedor(0,0,0,1,NULL,NULL,NULL)
				END
				ELSE
				BEGIN 
				INSERT INTO @TBLESTADOS
				SELECT IdEstadoProveedor
				FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,@IdEstadoInfoFinanciera,@IdEstadoInfoExperiencia,NULL,NULL,NULL,NULL)
				END
			END
			ELSE IF (@IdTipoPersona=3 OR @IdTipoPersona=4)
			BEGIN
				SELECT @IdEstadoDatosBasicos=IdEstado FROM PROVEEDOR.EntidadProvOferente
				WHERE IdEntidad = @IdEntidad
			
				SELECT @IdEdoValidacionDocIntegrantes = IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
				WHERE Descripcion='EN VALIDACIÓN'
			
				DECLARE  @IdEnValidacionDBasicos INT =(SELECT IdEstadoDatosBasicos FROM PROVEEDOR.EstadoDatosBasicos
				WHERE Descripcion='EN VALIDACIÓN')
				
				--SI EXISTE REGISTRO DE VALIDACIÓN PARA INTEGRANTES
				IF EXISTS(SELECT IdValidacionIntegrantesEntidad FROM PROVEEDOR.ValidacionIntegrantesEntidad
				WHERE IdEntidad = @IdEntidad)
				BEGIN
						SELECT  @IdEstadoIntegrantes=IdEstadoValidacionIntegrantes FROM PROVEEDOR.ValidacionIntegrantesEntidad
						WHERE IdEntidad = @IdEntidad
				END
				---SI ALGUNO DE LOS DOS ESTADOS ES EN VALIDACIÓN
				IF((@IdEnValidacionDBasicos=@IdEstadoDatosBasicos) OR
				(@IdEdoValidacionDocIntegrantes=@IdEstadoIntegrantes))
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,1,1,@IdEstadoIntegrantes)
				END
				ELSE
				--Si solo hay información en Datos Basicos
				IF (@IdEstadoIntegrantes =0)
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,NULL,1,@IdEstadoIntegrantes)
				END
				--Si los estados son Diferentes entonces es Parcial.
				ELSE IF(
				(SELECT Descripcion FROM PROVEEDOR.EstadoDatosBasicos
				WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos) !=
				(SELECT Descripcion FROM PROVEEDOR.EstadoIntegrantes WHERE
				IdEstadoIntegrantes=@IdEstadoIntegrantes))
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,1,NULL,1,@IdEstadoIntegrantes)
				END
				ELSE
				BEGIN
					INSERT INTO @TBLESTADOS
					SELECT IdEstadoProveedor
					FROM dbo.GetIdEstadoProveedor(@IdEstadoDatosBasicos,0,0,NULL,NULL,1,@IdEstadoIntegrantes)
				END
			
			END
				UPDATE PROVEEDOR.EntidadProvOferente
				SET IdEstadoProveedor =(SELECT TOP(1) IdEstadoProveedor FROM @TBLESTADOS)
				WHERE IdEntidad=@IdEntidad
		

	
		END

		SELECT @INDEX = @INDEX + 1
	END


END
