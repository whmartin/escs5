﻿CREATE TABLE [PROVEEDOR].[DocExperienciaEntidad] (
    [IdDocAdjunto]    INT            IDENTITY (1, 1) NOT NULL,
    [IdExpEntidad]    INT            NULL,
    [IdTipoDocumento] INT            NULL,
    [Descripcion]     NVARCHAR (128) NOT NULL,
    [LinkDocumento]   NVARCHAR (256) NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [NombreDocumento] NVARCHAR (128) NULL,
    [IdTemporal]      VARCHAR (20)   NULL,
    CONSTRAINT [PK_DocExperienciaEntidad] PRIMARY KEY CLUSTERED ([IdDocAdjunto] ASC),
    CONSTRAINT [FK_DocExperienciaEntidad_InfoExperienciaEntidad] FOREIGN KEY ([IdExpEntidad]) REFERENCES [PROVEEDOR].[InfoExperienciaEntidad] ([IdExpEntidad]),
    CONSTRAINT [FK_DocExperienciaEntidad_TipoDocumento] FOREIGN KEY ([IdTipoDocumento]) REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
);

