﻿CREATE TABLE [PROVEEDOR].[TipoRegimenTributario] (
    [IdTipoRegimenTributario] INT            IDENTITY (1, 1) NOT NULL,
    [CodigoRegimenTributario] NVARCHAR (128) NOT NULL,
    [Descripcion]             NVARCHAR (128) NOT NULL,
    [IdTipoPersona]           INT            NOT NULL,
    [Estado]                  BIT            NOT NULL,
    [UsuarioCrea]             NVARCHAR (250) NOT NULL,
    [FechaCrea]               DATETIME       NOT NULL,
    [UsuarioModifica]         NVARCHAR (250) NULL,
    [FechaModifica]           DATETIME       NULL,
    CONSTRAINT [PK_TipoRegimenTributario] PRIMARY KEY CLUSTERED ([IdTipoRegimenTributario] ASC)
);

