﻿CREATE TABLE [PROVEEDOR].[DocFinancieraProv] (
    [IdDocAdjunto]    INT            IDENTITY (1, 1) NOT NULL,
    [IdInfoFin]       INT            NULL,
    [NombreDocumento] NVARCHAR (128) NOT NULL,
    [LinkDocumento]   NVARCHAR (256) NOT NULL,
    [Observaciones]   NVARCHAR (256) NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [IdTipoDocumento] INT            NULL,
    [IdTemporal]      VARCHAR (20)   NULL,
    CONSTRAINT [PK_DocFinancieraProv] PRIMARY KEY CLUSTERED ([IdDocAdjunto] ASC),
    CONSTRAINT [FK_DocFinancieraProv_InfoFinancieraEntidad] FOREIGN KEY ([IdInfoFin]) REFERENCES [PROVEEDOR].[InfoFinancieraEntidad] ([IdInfoFin]),
    CONSTRAINT [FK_DocFinancieraProv_TipoDocumento] FOREIGN KEY ([IdTipoDocumento]) REFERENCES [PROVEEDOR].[TipoDocumento] ([IdTipoDocumento])
);

