﻿CREATE TABLE [PROVEEDOR].[ExperienciaCodUNSPSCEntidad] (
    [IdExpCOD]        INT            IDENTITY (1, 1) NOT NULL,
    [IdTipoCodUNSPSC] INT            NOT NULL,
    [IdExpEntidad]    INT            NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_ExperienciaCodUNSPSCEntidad] PRIMARY KEY CLUSTERED ([IdExpCOD] ASC),
    CONSTRAINT [FK_IdExpEntidad_InfoExperienciaEntidad] FOREIGN KEY ([IdExpEntidad]) REFERENCES [PROVEEDOR].[InfoExperienciaEntidad] ([IdExpEntidad]),
    CONSTRAINT [FK_IdTipoCodUNSPSC_TipoCodigoUNSPSC] FOREIGN KEY ([IdTipoCodUNSPSC]) REFERENCES [PROVEEDOR].[TipoCodigoUNSPSC] ([IdTipoCodUNSPSC])
);

