﻿CREATE TABLE [PROVEEDOR].[NivelGobierno] (
    [IdNivelGobierno]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoNivelGobierno] NVARCHAR (128) NOT NULL,
    [Descripcion]         NVARCHAR (128) NOT NULL,
    [Estado]              BIT            NOT NULL,
    [UsuarioCrea]         NVARCHAR (250) NOT NULL,
    [FechaCrea]           DATETIME       NOT NULL,
    [UsuarioModifica]     NVARCHAR (250) NULL,
    [FechaModifica]       DATETIME       NULL,
    CONSTRAINT [PK_NivelGobierno] PRIMARY KEY CLUSTERED ([IdNivelGobierno] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key {0} Column', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'NivelGobierno', @level2type = N'COLUMN', @level2name = N'IdNivelGobierno';

