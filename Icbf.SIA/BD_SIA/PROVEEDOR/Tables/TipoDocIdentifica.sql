﻿CREATE TABLE [PROVEEDOR].[TipoDocIdentifica] (
    [IdTipoDocIdentifica] INT            IDENTITY (1, 1) NOT NULL,
    [CodigoDocIdentifica] NUMERIC (18)   NOT NULL,
    [Descripcion]         NVARCHAR (128) NOT NULL,
    [UsuarioCrea]         NVARCHAR (250) NOT NULL,
    [FechaCrea]           DATETIME       NOT NULL,
    [UsuarioModifica]     NVARCHAR (250) NULL,
    [FechaModifica]       DATETIME       NULL,
    CONSTRAINT [PK_TipoDocIdentifica] PRIMARY KEY CLUSTERED ([IdTipoDocIdentifica] ASC)
);

