﻿CREATE TABLE [PROVEEDOR].[TablaParametrica] (
    [IdTablaParametrica]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoTablaParametrica] NVARCHAR (128) NOT NULL,
    [NombreTablaParametrica] NVARCHAR (128) NOT NULL,
    [Url]                    NVARCHAR (256) NOT NULL,
    [Estado]                 BIT            NOT NULL,
    [UsuarioCrea]            NVARCHAR (250) NOT NULL,
    [FechaCrea]              DATETIME       NOT NULL,
    [UsuarioModifica]        NVARCHAR (250) NULL,
    [FechaModifica]          DATETIME       NULL,
    CONSTRAINT [PK_TablaParametrica] PRIMARY KEY CLUSTERED ([IdTablaParametrica] ASC)
);

