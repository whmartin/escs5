﻿CREATE TABLE [PROVEEDOR].[TipoCodigoUNSPSC] (
    [IdTipoCodUNSPSC] INT            IDENTITY (1, 1) NOT NULL,
    [Codigo]          NVARCHAR (64)  NOT NULL,
    [Descripcion]     NVARCHAR (250) NULL,
    [Estado]          BIT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    [CodigoSegmento]  NVARCHAR (50)  NULL,
    [NombreSegmento]  NVARCHAR (250) NULL,
    [CodigoFamilia]   NVARCHAR (50)  NULL,
    [NombreFamilia]   NVARCHAR (250) NULL,
    [CodigoClase]     NVARCHAR (50)  NULL,
    [NombreClase]     NVARCHAR (250) NULL,
    CONSTRAINT [PK_TipoCodigoUNSPSC] PRIMARY KEY CLUSTERED ([IdTipoCodUNSPSC] ASC)
);

