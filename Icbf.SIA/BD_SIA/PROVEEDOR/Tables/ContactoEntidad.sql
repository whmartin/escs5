﻿CREATE TABLE [PROVEEDOR].[ContactoEntidad] (
    [IdContacto]           INT            IDENTITY (1, 1) NOT NULL,
    [IdEntidad]            INT            NOT NULL,
    [IdSucursal]           INT            NOT NULL,
    [IdTelContacto]        INT            NOT NULL,
    [IdTipoDocIdentifica]  INT            NOT NULL,
    [IdTipoCargoEntidad]   INT            NOT NULL,
    [NumeroIdentificacion] NUMERIC (30)   NOT NULL,
    [PrimerNombre]         NVARCHAR (128) NOT NULL,
    [SegundoNombre]        NVARCHAR (128) NOT NULL,
    [PrimerApellido]       NVARCHAR (128) NOT NULL,
    [SegundoApellido]      NVARCHAR (128) NOT NULL,
    [Dependencia]          NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (128) NOT NULL,
    [Estado]               BIT            NOT NULL,
    [UsuarioCrea]          NVARCHAR (250) NOT NULL,
    [FechaCrea]            DATETIME       NOT NULL,
    [UsuarioModifica]      NVARCHAR (250) NULL,
    [FechaModifica]        DATETIME       NULL,
    CONSTRAINT [PK_ContactoEntidad] PRIMARY KEY CLUSTERED ([IdContacto] ASC)
);

