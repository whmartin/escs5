﻿CREATE TABLE [PROVEEDOR].[TipoEntidad] (
    [IdTipoentidad]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoTipoentidad] NVARCHAR (128) NOT NULL,
    [Descripcion]       NVARCHAR (128) NOT NULL,
    [Estado]            BIT            NOT NULL,
    [UsuarioCrea]       NVARCHAR (250) NOT NULL,
    [FechaCrea]         DATETIME       NOT NULL,
    [UsuarioModifica]   NVARCHAR (250) NULL,
    [FechaModifica]     DATETIME       NULL,
    CONSTRAINT [PK_Tipoentidad] PRIMARY KEY CLUSTERED ([IdTipoentidad] ASC)
);

