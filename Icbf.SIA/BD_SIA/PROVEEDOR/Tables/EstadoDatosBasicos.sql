﻿CREATE TABLE [PROVEEDOR].[EstadoDatosBasicos] (
    [IdEstadoDatosBasicos] INT            IDENTITY (1, 1) NOT NULL,
    [Descripcion]          NVARCHAR (128) NOT NULL,
    [Estado]               BIT            NOT NULL,
    [UsuarioCrea]          NVARCHAR (250) NOT NULL,
    [FechaCrea]            DATETIME       NOT NULL,
    [UsuarioModifica]      NVARCHAR (250) NULL,
    [FechaModifica]        DATETIME       NULL,
    CONSTRAINT [PK_EstadoDatosBasicos] PRIMARY KEY CLUSTERED ([IdEstadoDatosBasicos] ASC)
);

