﻿CREATE TABLE [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] (
    [IdValidarInfoDatosBasicosEntidad] INT            IDENTITY (1, 1) NOT NULL,
    [NroRevision]                      INT            NOT NULL,
    [IdEntidad]                        INT            NOT NULL,
    [ConfirmaYAprueba]                 BIT            NULL,
    [Observaciones]                    NVARCHAR (200) NOT NULL,
    [FechaCrea]                        DATETIME       NOT NULL,
    [UsuarioCrea]                      NVARCHAR (128) NOT NULL,
    [FechaModifica]                    DATETIME       NULL,
    [UsuarioModifica]                  NVARCHAR (128) NULL,
    [CorreoEnviado]                    BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_IdValidarInfoDatosBasicosEntidad] PRIMARY KEY CLUSTERED ([IdValidarInfoDatosBasicosEntidad] ASC),
    CONSTRAINT [IdInfoDatosBasicosEntidad_InfoDatosBasicosEntidad] FOREIGN KEY ([IdEntidad]) REFERENCES [PROVEEDOR].[EntidadProvOferente] ([IdEntidad])
);

