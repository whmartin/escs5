﻿CREATE TABLE [PROVEEDOR].[Rel_RamaGobNivelOrgYTipoEnt] (
    [IdRelacion]             INT            IDENTITY (1, 1) NOT NULL,
    [IdRamaEstructura]       INT            NULL,
    [IdNivelGobierno]        INT            NULL,
    [IdNivelOrganizacional]  INT            NULL,
    [IdTipodeentidadPublica] INT            NULL,
    [Estado]                 BIT            CONSTRAINT [DF_Rel_RamaGobNivelOrgYTipoEnt_Estado] DEFAULT ((1)) NULL,
    [UsuarioCrea]            NVARCHAR (250) NULL,
    [FechaCrea]              DATETIME       NULL,
    [UsuarioModifica]        NVARCHAR (250) NULL,
    [FechaModifica]          DATETIME       NULL,
    CONSTRAINT [PK_Rel_RamaGobNivelOrgYTipoEnt] PRIMARY KEY CLUSTERED ([IdRelacion] ASC),
    CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelGobierno] FOREIGN KEY ([IdNivelGobierno]) REFERENCES [PROVEEDOR].[NivelGobierno] ([IdNivelGobierno]),
    CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_NivelOrganizacional] FOREIGN KEY ([IdNivelOrganizacional]) REFERENCES [PROVEEDOR].[NivelOrganizacional] ([IdNivelOrganizacional]),
    CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_RamaoEstructura] FOREIGN KEY ([IdRamaEstructura]) REFERENCES [PROVEEDOR].[RamaoEstructura] ([IdRamaEstructura]),
    CONSTRAINT [FK_Rel_RamaGobNivelOrgYTipoEnt_TipodeentidadPublica] FOREIGN KEY ([IdTipodeentidadPublica]) REFERENCES [PROVEEDOR].[TipodeentidadPublica] ([IdTipodeentidadPublica])
);

