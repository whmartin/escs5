﻿CREATE TABLE [PROVEEDOR].[EntidadModuloEstado] (
    [IdEntidad]   INT            NOT NULL,
    [IdModulo]    INT            NOT NULL,
    [IdEstado]    INT            NOT NULL,
    [FechaEstado] NVARCHAR (100) NOT NULL,
    [UsuarioCrea] NVARCHAR (250) NOT NULL,
    [FechaCrea]   DATETIME       NOT NULL
);

