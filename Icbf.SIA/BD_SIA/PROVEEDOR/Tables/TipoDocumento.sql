﻿CREATE TABLE [PROVEEDOR].[TipoDocumento] (
    [IdTipoDocumento]     INT            IDENTITY (1, 1) NOT NULL,
    [CodigoTipoDocumento] NVARCHAR (128) NOT NULL,
    [Descripcion]         NVARCHAR (128) NOT NULL,
    [Estado]              BIT            NOT NULL,
    [UsuarioCrea]         NVARCHAR (250) NOT NULL,
    [FechaCrea]           DATETIME       NOT NULL,
    [UsuarioModifica]     NVARCHAR (250) NULL,
    [FechaModifica]       DATETIME       NULL,
    CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED ([IdTipoDocumento] ASC)
);

