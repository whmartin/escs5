﻿CREATE TABLE [PROVEEDOR].[EstadoIntegrantes] (
    [IdEstadoIntegrantes] INT            IDENTITY (1, 1) NOT NULL,
    [Descripcion]         NVARCHAR (128) NOT NULL,
    [Estado]              BIT            NOT NULL,
    [UsuarioCrea]         NVARCHAR (250) NOT NULL,
    [FechaCrea]           DATETIME       NOT NULL,
    [UsuarioModifica]     NVARCHAR (250) NULL,
    [FechaModifica]       DATETIME       NULL,
    CONSTRAINT [PK_EstadoIntegrantes] PRIMARY KEY CLUSTERED ([IdEstadoIntegrantes] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key {0} Column', @level0type = N'SCHEMA', @level0name = N'PROVEEDOR', @level1type = N'TABLE', @level1name = N'EstadoIntegrantes', @level2type = N'COLUMN', @level2name = N'IdEstadoIntegrantes';

