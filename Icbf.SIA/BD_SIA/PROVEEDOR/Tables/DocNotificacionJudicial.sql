﻿CREATE TABLE [PROVEEDOR].[DocNotificacionJudicial] (
    [IdDocNotJudicial] INT            IDENTITY (1, 1) NOT NULL,
    [IdNotJudicial]    INT            NULL,
    [IdDocumento]      INT            NULL,
    [Descripcion]      NVARCHAR (128) NULL,
    [LinkDocumento]    NVARCHAR (256) NULL,
    [Anno]             NUMERIC (4)    NULL,
    [UsuarioCrea]      NVARCHAR (250) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioModifica]  NVARCHAR (250) NULL,
    [FechaModifica]    DATETIME       NULL,
    CONSTRAINT [PK_DocNotificacionJudicial] PRIMARY KEY CLUSTERED ([IdDocNotJudicial] ASC),
    CONSTRAINT [FK_DocNotificacionJudicial_NotificacionJudicial] FOREIGN KEY ([IdNotJudicial]) REFERENCES [PROVEEDOR].[NotificacionJudicial] ([IdNotJudicial])
);

