﻿CREATE TABLE [PROVEEDOR].[ValidarTercero] (
    [IdValidarTercero] INT            IDENTITY (1, 1) NOT NULL,
    [IdTercero]        INT            NOT NULL,
    [ConfirmaYAprueba] BIT            NOT NULL,
    [Observaciones]    NVARCHAR (200) NOT NULL,
    [FechaCrea]        DATETIME       NOT NULL,
    [UsuarioCrea]      NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_IdValidarTercero] PRIMARY KEY CLUSTERED ([IdValidarTercero] ASC)
);

