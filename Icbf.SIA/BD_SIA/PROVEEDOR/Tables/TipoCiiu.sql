﻿CREATE TABLE [PROVEEDOR].[TipoCiiu] (
    [IdTipoCiiu]      INT            IDENTITY (1, 1) NOT NULL,
    [CodigoCiiu]      NVARCHAR (128) NOT NULL,
    [Descripcion]     NVARCHAR (512) NULL,
    [Estado]          BIT            NOT NULL,
    [UsuarioCrea]     NVARCHAR (250) NOT NULL,
    [FechaCrea]       DATETIME       NOT NULL,
    [UsuarioModifica] NVARCHAR (250) NULL,
    [FechaModifica]   DATETIME       NULL,
    CONSTRAINT [PK_TipoCiiu] PRIMARY KEY CLUSTERED ([IdTipoCiiu] ASC)
);

