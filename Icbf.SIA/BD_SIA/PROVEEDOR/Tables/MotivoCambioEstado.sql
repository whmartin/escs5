﻿CREATE TABLE [PROVEEDOR].[MotivoCambioEstado] (
    [IdTercero]    INT            NULL,
    [IdTemporal]   NVARCHAR (20)  NULL,
    [Motivo]       NVARCHAR (250) NULL,
    [DatosBasicos] BIT            NULL,
    [Financiera]   BIT            NULL,
    [Experiencia]  BIT            NULL,
    [FechaCrea]    DATETIME       NULL,
    [UsuarioCrea]  NVARCHAR (128) NULL,
    [Integrantes]  BIT            DEFAULT ((0)) NOT NULL
);

