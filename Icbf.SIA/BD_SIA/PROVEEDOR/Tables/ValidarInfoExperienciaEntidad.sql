﻿CREATE TABLE [PROVEEDOR].[ValidarInfoExperienciaEntidad] (
    [IdValidarInfoExperienciaEntidad] INT            IDENTITY (1, 1) NOT NULL,
    [NroRevision]                     INT            NOT NULL,
    [IdExpEntidad]                    INT            NOT NULL,
    [ConfirmaYAprueba]                BIT            NULL,
    [Observaciones]                   NVARCHAR (200) NOT NULL,
    [FechaCrea]                       DATETIME       NOT NULL,
    [UsuarioCrea]                     NVARCHAR (128) NOT NULL,
    [FechaModifica]                   DATETIME       NULL,
    [UsuarioModifica]                 NVARCHAR (128) NULL,
    [CorreoEnviado]                   BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_IdValidarInfoExperienciaEntidad] PRIMARY KEY CLUSTERED ([IdValidarInfoExperienciaEntidad] ASC),
    CONSTRAINT [IdInfoExperienciaEntidad_InfoExperienciaEntidad] FOREIGN KEY ([IdExpEntidad]) REFERENCES [PROVEEDOR].[InfoExperienciaEntidad] ([IdExpEntidad])
);

