﻿-- =============================================
-- Author:		Julio Cesar Hernandez
-- Create date: 21/08/2015
-- Description:	Funcion para convertir numeros a letras
-- Convi_Convierte
-- =============================================

CREATE FUNCTION dbo.Convi_Convierte
( @Numero Int )
RETURNS nVarChar(Max)
AS BEGIN

	Declare @Resu nVarChar(Max)
	Set @Resu = dbo.Convi_PreConvierte(@Numero)
	If Right(@Resu,4) = 'un(o'
		Set @Resu = Left(@Resu,Len(@Resu)-4) + 'uno'
	Set @Resu = Replace(@Resu,'un(o','un')
	RETURN @Resu
	
END
