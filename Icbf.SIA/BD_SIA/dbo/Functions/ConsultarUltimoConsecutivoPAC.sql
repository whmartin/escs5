﻿
-- =============================================
-- Author:		Jorge Vizcaino
-- Create date: 2015-07-22
-- Description:	Consulta el paso siguiente o el anterior de un flujo
-- =============================================
CREATE FUNCTION [dbo].[ConsultarUltimoConsecutivoPAC]
(	
)
RETURNS int
AS
BEGIN
	
	
	DECLARE @ConsecutivoPAc int
			
	SELECT @ConsecutivoPAc = ISNULL(MAX(ConsecutivoPac),1)
	from SPCP.CuentasdeCobro
	

	RETURN @ConsecutivoPAc

END



