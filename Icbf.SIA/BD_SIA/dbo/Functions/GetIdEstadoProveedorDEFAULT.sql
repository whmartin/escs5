﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 10-Abril-2014
-- Description:	Retorna el ID Default que tendrá el campo IdEstadoProveedor en la tabla
-- PROVEEDOR.EntidadProvOferente. Está función se ha creado para poderla utilizar en el 
-- Constraint de Default Value del nuevo campor IdEstadoProveedor en la tabla PROVEEDOR.EntidadProvOferente
-- =============================================
CREATE FUNCTION [dbo].[GetIdEstadoProveedorDEFAULT] 
(
)
RETURNS INT
AS
BEGIN
			
		DECLARE @IdEstadoProveedor INT =(SELECT IdEstadoProveedor FROM PROVEEDOR.EstadoProveedor WHERE Descripcion='REGISTRADO')
		RETURN 	@IdEstadoProveedor

END
