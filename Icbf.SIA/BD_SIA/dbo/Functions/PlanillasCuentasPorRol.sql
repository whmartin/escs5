﻿

-- =============================================
-- Author:		Jorge Vizcaino
-- Create date: 2015-08-08
-- Description:	Consulta las planillas de las cuentas de cobro que puede ver un rol
-- =============================================
CREATE FUNCTION [dbo].[PlanillasCuentasPorRol]
(	
	@Rol nvarchar(80)
)
RETURNS @TablePlanillasCuentasPorRol TABLE 
(	IDPlanilla			INT
	,TipoDocumento		INT
	,NumDocIdentidad	NVARCHAR(80)
	,IdCuenta			INT
	,IdEstadoPaso		INT
	,IdTipoCuenta		INT
)
AS
BEGIN
	Declare @IdPasoAsignado INT

	
	Select @IdPasoAsignado = IdPaso
	From SEG.Rol
	where Nombre = ltrim(rtrim(@Rol))
	
	INSERT into @TablePlanillasCuentasPorRol
	SELECT PD.IDPlanilla 
			,da.TipoDocumento 
			,da.NumDocIdentidad 
			,pd.IdCuenta
			,cc.IdEstadoPaso
			,cc.IdTipoCuenta
	FROM  SPCP.PlanillaDetalle pd 
	INNER JOIN SPCP.CuentasDeCobro cc
	on pd.IdCuenta =cc.IdCuenta
	INNER JOIN SPCP.FlujoPasosCuenta fp
	on cc.IdEstadoPaso = fp.IdFlujoPasoCuenta
	INNER JOIN SPCP.Pasos p
	ON fp.IdPaso = p.IdPaso
	INNER JOIN SPCP.DatosAdministracion da
	on cc.IdDatosAdministracion = da.IdDatosAdministracion
	WHERE p.IdPaso = @IdPasoAsignado
	
	RETURN
END

