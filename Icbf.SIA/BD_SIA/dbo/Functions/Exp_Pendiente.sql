﻿-- ========================================================================================================================
-- Object:		ALTER FUNCTION [dbo].[Exp_Pendiente]
-- Fecha:		16/10/2014
-- Responsable:    TOMMY PUCCINI COLINA
-- Asunto:		FUNCION PARA DETERM;INAR EL ESTADO DE LA EXPERIENCIA
-- Descripcion:       FUNCION PARA DETERM;INAR EL ESTADO DE LA EXPERIENCIA
-- ========================================================================================================================

CREATE FUNCTION [dbo].[Exp_Pendiente](@idEntidad as int) 
RETURNS varchar(200) AS 
BEGIN 
DECLARE @Texto varchar(500),
        @sinvali int,   
        @con0 int,
        @con1 int
		begin
		
		 select @con0 = count(*)      
         FROM [PROVEEDOR].[ValidarInfoExperienciaEntidad] 
         where [IdValidarInfoExperienciaEntidad] in ( select max(FE2.[IdValidarInfoExperienciaEntidad])
                        from [PROVEEDOR].[ValidarInfoExperienciaEntidad] FE2
                        where FE2.IdExpEntidad in (select (IdExpEntidad)
                        from [PROVEEDOR].[InfoExperienciaEntidad] IX
                         where @idEntidad = IX.[IdEntidad])
                         group by IdexpEntidad) 
                         and [ConfirmaYAprueba] = 0
        end;
        begin   		
             select @con1 = count(*)      
             FROM [PROVEEDOR].[ValidarInfoExperienciaEntidad] 
             where [IdValidarInfoExperienciaEntidad] in ( select max(FE2.[IdValidarInfoExperienciaEntidad])
                        from [PROVEEDOR].[ValidarInfoExperienciaEntidad] FE2
                        where FE2.IdExpEntidad in (select (IdExpEntidad)
                        from [PROVEEDOR].[InfoExperienciaEntidad] IX
                         where @idEntidad = IX.[IdEntidad])
                         group by IdexpEntidad) 
                         and [ConfirmaYAprueba] = 1
		end;
        begin   		
		 select @sinvali = count(*)  
           from [PROVEEDOR].[InfoExperienciaEntidad] FE2
           where FE2.[IdExpEntidad] in (select max([IdExpEntidad])
           from [PROVEEDOR].[InfoExperienciaEntidad] IX
           where @idEntidad = IX.[IdEntidad])
           and  [EstadoDocumental]  in (1,5)
		end;   
		if  @sinvali > 0 
               return('SI')
			   
        if  @con1 = 0 and @con0 = 0
		    return ('Sin Informacion')			
		else	
          if @con0 = 0  and  @con1 > 0
             return ('NO')
          else				
             return ('SI')	  
        return ('SI')	  
END