﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 11-Abril-2014
-- Description:	Recibe los IDs de Estado de cada uno de los módulos de información
--Y Retorna los IdsEstado que deben ser actualizados en Proveedor y en Tercero.
-- Si recibe true en alguna de los parametros BIT, entonces se buscan los estados en la
-- tabla x ya sea PARCIAL o EN VALIDACIÓN.
-- Modificado: 30/07/2014 Juan Carlos Valverde Sámano
-- Descripción: Se eliminó de esta función el IdEstadoTercero. Antes está función retornaba
-- el IdEstadoProveedor y el IdEstadoTercero, Ahora solo el IdEstadoTercero ya que el tercero no
-- heredara nunca el estado del proveedor.
-- =============================================
CREATE FUNCTION [dbo].[GetIdEstadoProveedor]  
(	
@IdEstadoDatosBasicos INT,
@IdEstadoInfoFinanciera INT,
@IdEstadoInfoExperiencia INT,
@esParcial BIT=NULL,
@esEnValidacion BIT=NULL,
@esConsorcioOUnion BIT=NULL,
@IdEstadoIntegrantes INT=NULL
)
RETURNS @tblEstados TABLE 
(IdEstadoProveedor INT)
AS
BEGIN

IF(@esParcial IS NULL AND @esEnValidacion IS NULL)
BEGIN
IF(@esConsorcioOUnion IS NULL)
BEGIN
	IF EXISTS(SELECT IdEstadoProveedor 
	FROM [PROVEEDOR].[ReglasEstadoProveedor]
	WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
	AND IdEstadoDatosFinancieros=@IdEstadoInfoFinanciera
	AND	IdEstadoDatosExperiencia=@IdEstadoInfoExperiencia)
	BEGIN
		INSERT INTO @tblEstados
		SELECT IdEstadoProveedor 
		FROM [PROVEEDOR].[ReglasEstadoProveedor]
		WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
		AND IdEstadoDatosFinancieros=@IdEstadoInfoFinanciera
		AND	IdEstadoDatosExperiencia=@IdEstadoInfoExperiencia
	END
	ELSE
	BEGIN
		INSERT INTO @tblEstados
		SELECT IdEstadoProveedor 
		FROM PROVEEDOR.EstadoProveedor
		WHERE Descripcion='PARCIAL'
	END
END
ELSE IF(@esConsorcioOUnion = 1)
BEGIN
		INSERT INTO @tblEstados
		SELECT IdEstadoProveedor 
		FROM [PROVEEDOR].[ReglasEstadoProveedor]
		WHERE IdEstadoDatosBasicos=@IdEstadoDatosBasicos
		AND IdEstadoIntegrantes=@IdEstadoIntegrantes
END
END
ELSE IF(@esParcial=1)
BEGIN
INSERT INTO @tblEstados
SELECT IdEstadoProveedor 
FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion='PARCIAL'
END
ELSE IF (@esEnValidacion=1)
BEGIN
INSERT INTO @tblEstados
SELECT IdEstadoProveedor 
FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion='EN VALIDACIÓN'
END
RETURN
END



