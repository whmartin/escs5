﻿-- =============================================
-- Author:		Julio Cesar Hernandez
-- Create date: 21/08/2015
-- Description:	Funcion para convertir numeros a letras
-- Convi_EnLetras
-- =============================================

CREATE FUNCTION dbo.Convi_EnLetras
( @Numero Float )
RETURNS nVarChar(Max)
AS BEGIN

	Declare @TmpFloat Float
	Set @TmpFloat = Round(@Numero,2)
	Declare @Decs nVarChar(Max)
	Set @Decs = Right('00' + Convert(nVarChar(Max),Round((@TmpFloat - Floor(@TmpFloat))*100,0)),2)
	Declare @Resu nVarChar(Max)
	Set @Resu = dbo.Convi_Convierte(Floor(@TmpFloat))-- + ' con ' + @Decs + '/100'
	RETURN @Resu
	
END
