﻿-- =============================================
-- Author:		Julio Cesar Hernandez
-- Create date: 21/08/2015
-- Description:	Funcion para convertir numeros a letras
-- Convi_PreConvierte
-- =============================================

CREATE FUNCTION dbo.Convi_PreConvierte
( @Numero Int )
RETURNS nVarChar(Max)
AS BEGIN
	Declare @Resu nVarChar(Max)
	Set @Resu = ''
	If @Numero <= 0 Return @Resu
	Set @Resu = dbo.Convi_FindNum(@Numero)
	Declare @TmpPre Int
	Set @TmpPre = 0
	Declare @TmpSuf Int
	Set @TmpSuf = @Numero
	Declare @i Int
	Declare @TmpNum nVarChar(Max)
Set @i = Case 
		When CharIndex('###',@Resu) > 0 Then 3 
		When CharIndex('##',@Resu) > 0 Then 2
		When CharIndex('#',@Resu) > 0 Then 1
		Else 0
	End
	If @i > 0 Begin
		Set @TmpNum = Convert(nVarChar(Max),@Numero)
		Set @TmpPre = Convert(Int,Left(@TmpNum,@i))
		Set @Resu = Right(@Resu,Len(@Resu)-@i)
		Set @TmpSuf = Convert(Int,Right(@TmpNum,Len(@TmpNum)-@i))
	End
	If CharIndex('*',@Resu) > 0 Begin
		If @TmpPre > 0 
			Set @Resu = dbo.Convi_PreConvierte(@TmpPre) + Left(@Resu,Len(@Resu)-1) + dbo.Convi_PreConvierte(@TmpSuf)
		Else Begin
			Set @TmpNum = Convert(nVarChar(Max),@TmpSuf)
			Set @TmpSuf = Convert(Int,Right(@TmpNum,Len(@TmpNum)-1))
			Set @Resu = Left(@Resu,Len(@Resu)-1) + dbo.Convi_PreConvierte(@TmpSuf)
		End
	End Else
		If @TmpPre > 0
			Set @Resu = dbo.Convi_PreConvierte(@TmpPre) + @Resu
	RETURN @Resu
END
