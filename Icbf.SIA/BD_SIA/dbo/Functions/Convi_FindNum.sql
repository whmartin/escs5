﻿-- =============================================
-- Author:		Julio Cesar Hernandez
-- Create date: 21/08/2015
-- Description:	Funcion para convertir numeros a letras
-- Convi_FindNum 
-- =============================================

CREATE FUNCTION dbo.Convi_FindNum
( @Numero Int )
RETURNS nVarChar(Max)
AS BEGIN
	Declare @Resu nVarChar(Max)
	Set @Resu = Case @Numero
		When 1 Then 'un(o'
		When 2 Then 'dos'
		When 3 Then 'tres'
		When 4 Then 'cuatro'
		When 5 Then 'cinco'
		When 6 Then 'seis'
		When 7 Then 'siete'
		When 8 Then 'ocho'
		When 9 Then 'nueve'
		When 10 Then 'diez'
		When 11 Then 'once'
		When 12 Then 'doce'
		When 13 Then 'trece'
		When 14 Then 'catorce'
		When 15 Then 'quince'
		When 16 Then 'dieciseis'
		When 17 Then 'diecisiete'
		When 18 Then 'dieciocho'
		When 19 Then 'diecinueve'
		When 20 Then 'veinte'
		When 30 Then 'treinta'
		When 40 Then 'cuarenta'
		When 50 Then 'cincuenta'
		When 60 Then 'sesenta'
		When 70 Then 'setenta'
		When 80 Then 'ochenta'
		When 90 Then 'noventa'
		When 100 Then 'cien'
		Else Case
			When @Numero <= 29 Then 'veinti*'
			When @Numero <= 39 Then 'treinta y *'
			When @Numero <= 49 Then 'cuarenta y *'
			When @Numero <= 59 Then 'cincuenta y *'
			When @Numero <= 69 Then 'sesenta y *'
			When @Numero <= 79 Then 'setenta y *'
			When @Numero <= 89 Then 'ochenta y *'
			When @Numero <= 99 Then 'noventa y *'
			When @Numero <= 199 Then 'ciento *'
			When @Numero <= 299 Then 'doscientos *'
			When @Numero <= 399 Then 'trescientos *'
			When @Numero <= 499 Then 'cuatrocientos *'
			When @Numero <= 599 Then 'quinientos *'
			When @Numero <= 699 Then 'seiscientos *'
			When @Numero <= 799 Then 'setecientos *'
			When @Numero <= 899 Then 'ochocientos *'
			When @Numero <= 999 Then 'novecientos *'
			When @Numero <= 1999 Then 'un mil *'
			When @Numero <= 9999 Then '# mil *'
			When @Numero <= 99999 Then '## mil *'
			When @Numero <= 999999 Then '### mil *'
			When @Numero <= 1999999 Then 'un millón *'
			When @Numero <= 9999999 Then '# millones *'
			When @Numero <= 99999999 Then '## millones *'
			When @Numero <= 999999999 Then '### millones *'
			Else ''
			End
		End
	RETURN @Resu
END
