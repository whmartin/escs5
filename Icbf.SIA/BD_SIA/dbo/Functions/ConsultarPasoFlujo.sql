﻿-- =============================================
-- Author:		Jorge Vizcaino
-- Create date: 2015-07-22
-- Description:	Consulta el paso siguiente o el anterior de un flujo
-- =============================================
CREATE FUNCTION [dbo].ConsultarPasoFlujo
(
	@TipoPaso bit, --1 paso siguiente, 0 paso anterior
	@IdestadoPaso int
)
RETURNS int
AS
BEGIN
	
	DECLARE @IdPasoReturn int,
			@OrdenPasoSelec int,
			@IDTipoCuenta int

	
	select @OrdenPasoSelec = orden, @IDTipoCuenta = IdTipoCuenta
	from SPCP.FlujoPasosCuenta
	where IdFlujoPasoCuenta = @IdestadoPaso
	

	IF(@TipoPaso=1)
	BEGIN
		Select top 1 @IdPasoReturn = IdFlujoPasoCuenta
		from SPCP.FlujoPasosCuenta
		where IdTipoCuenta = @IDTipoCuenta
		and Orden > @OrdenPasoSelec
		order by Orden ASC
	END
	ELSE
	BEGIN
		Select top 1 @IdPasoReturn = idFlujoPasoCuenta
		from SPCP.FlujoPasosCuenta
		where IdTipoCuenta = @IDTipoCuenta
		and Orden < @OrdenPasoSelec
		order by Orden DESC
	END


	RETURN @IdPasoReturn

END
