﻿CREATE TYPE [dbo].[Table_Proveedores_IntegracionUsuarios] AS TABLE (
    [UserId]          NVARCHAR (125) NOT NULL,
    [PrimerNombre]    NVARCHAR (150) NULL,
    [SegundoNombre]   NVARCHAR (150) NULL,
    [PrimerApellido]  NVARCHAR (150) NULL,
    [SegundoApellido] NVARCHAR (150) NULL,
    [RazonSocial]     NVARCHAR (150) NULL,
    [ExisteTercero]   BIT            NULL,
    [IsApproved]      BIT            NULL);

