﻿-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicials_Consultar]
	@IdDocNotJudicial INT = NULL,
	@IdNotJudicial INT = NULL,
	@IdDocumento INT = NULL
AS
BEGIN
 SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdNotJudicial, 
		    Proveedor.DocNotificacionJudicial.IdDocumento, 
            Proveedor.DocNotificacionJudicial.Descripcion, 
            Proveedor.DocNotificacionJudicial.LinkDocumento, 
            Proveedor.DocNotificacionJudicial.Anno, 
            Proveedor.DocNotificacionJudicial.UsuarioCrea, 
            Proveedor.DocNotificacionJudicial.FechaCrea, 
            Proveedor.DocNotificacionJudicial.UsuarioModifica, 
            Proveedor.DocNotificacionJudicial.FechaModifica, 
            Proveedor.TipoDocumento.Descripcion AS NombreTipoDocumento
 FROM       Proveedor.DocNotificacionJudicial 
			INNER JOIN Proveedor.TipoDocumento 
			ON Proveedor.DocNotificacionJudicial.IdDocumento = Proveedor.TipoDocumento.IdTipoDocumento
 WHERE		IdDocNotJudicial = CASE WHEN @IdDocNotJudicial IS NULL THEN IdDocNotJudicial ELSE @IdDocNotJudicial END AND 
			IdNotJudicial = CASE WHEN @IdNotJudicial IS NULL THEN IdNotJudicial ELSE @IdNotJudicial END AND 
			IdDocumento = CASE WHEN @IdDocumento IS NULL THEN IdDocumento ELSE @IdDocumento END
END

