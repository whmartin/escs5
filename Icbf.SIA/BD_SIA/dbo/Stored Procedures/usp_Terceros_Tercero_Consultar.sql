﻿
-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que consulta un(a) Tercero
-- =============================================

CREATE PROCEDURE [dbo].[usp_Terceros_Tercero_Consultar]
@IdTercero INT
AS
BEGIN

  SELECT T.[IDTERCERO]
      ,T.[IDTIPODOCIDENTIFICA]
      ,T.[IDESTADOTERCERO]
      ,T.[IdTipoPersona]
      ,T.[ConsecutivoInterno]
      ,T.[NUMEROIDENTIFICACION]
      ,T.[DIGITOVERIFICACION]
      ,T.[CORREOELECTRONICO]
      ,T.[PRIMERNOMBRE]
      ,T.[SEGUNDONOMBRE]
      ,T.[PRIMERAPELLIDO]
      ,T.[SEGUNDOAPELLIDO]
      ,T.[RAZONSOCIAL]
      ,T.[FECHAEXPEDICIONID]
      ,T.[FECHANACIMIENTO]
      ,T.[SEXO]
      ,T.[FechaCrea]
      ,T.[USUARIOCREA]
      ,T.[FECHAMODIFICA]
      ,T.[USUARIOMODIFICA]
	  ,T.[ProviderUserKey]
	  ,T.[CreadoPorInterno]
FROM  Tercero.TERCERO T 

WHERE T.IdTercero = @IdTercero
     
END




