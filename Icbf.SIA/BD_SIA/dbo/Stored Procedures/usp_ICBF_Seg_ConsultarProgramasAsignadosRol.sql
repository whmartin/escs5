﻿-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta programas asignados a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramasAsignadosRol]
	@pIdRol INT
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo
FROM 
	SEG.Programa P
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo	
INNER JOIN
	SEG.ProgramaRol ON P.IdPrograma = SEG.ProgramaRol.IdPrograma
WHERE 
	SEG.ProgramaRol.IdRol = @pIdRol

END
