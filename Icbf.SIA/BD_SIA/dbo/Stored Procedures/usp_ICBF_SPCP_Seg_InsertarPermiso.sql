﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Permiso
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_InsertarPermiso]
		@IdPermiso INT OUTPUT, 	@IdPrograma INT,	@IdRol INT,	@Insertar BIT,	@Modificar BIT,	@Eliminar BIT,	@Consultar BIT, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Permiso(IdPrograma, IdRol, Insertar, Modificar, Eliminar, Consultar, UsuarioCreacion, FechaCreacion)
					  VALUES(@IdPrograma, @IdRol, @Insertar, @Modificar, @Eliminar, @Consultar, @UsuarioCreacion, GETDATE())
	SELECT @IdPermiso = @@IDENTITY 		
END

