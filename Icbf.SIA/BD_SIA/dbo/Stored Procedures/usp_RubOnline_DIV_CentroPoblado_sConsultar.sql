﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:54:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) CentroPoblado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroPoblado_sConsultar]
	@IdMunicipio INT = NULL,@CodigoCentroPoblado NVARCHAR(256) = NULL,@NombreCentroPoblado NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdCentroPoblado
      , IdMunicipio
      , CodigoCentroPoblado
      , NombreCentroPoblado
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica
 FROM [DIV].[CentroPoblado] 
 WHERE IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END AND CodigoCentroPoblado = CASE WHEN @CodigoCentroPoblado IS NULL THEN CodigoCentroPoblado ELSE @CodigoCentroPoblado END AND NombreCentroPoblado = CASE WHEN @NombreCentroPoblado IS NULL THEN NombreCentroPoblado ELSE @NombreCentroPoblado END
END

