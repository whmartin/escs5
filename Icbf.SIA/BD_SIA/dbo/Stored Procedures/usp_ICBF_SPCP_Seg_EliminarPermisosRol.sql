﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  19/10/2012 09:36:11 a.m.
-- Description:	Procedimiento almacenado que elimina un(a) Permiso
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_EliminarPermisosRol]
	@pIdRol INT
AS
BEGIN
	DELETE SEG.Permiso WHERE IdRol = @pIdRol
END

