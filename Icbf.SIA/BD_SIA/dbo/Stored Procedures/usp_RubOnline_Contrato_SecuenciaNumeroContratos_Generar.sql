﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroContratos_Generar]
	@NumeroContrato NVARCHAR(11) output, @CodigoRegional INT ,@AnoVigencia INT, @UsuarioCrea NVARCHAR(250) 
AS
BEGIN
	DECLARE @Consecutivo INT
	/*Validar esta creada un consecutivo por CodigoRegional y AnoVigencia*/
	if not exists(SELECT * FROM [Contrato].[SecuenciaNumeroContrato] WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia)
	BEGIN
			SET @Consecutivo = 1	
	END
	ELSE
	BEGIN
			SELECT @Consecutivo = MAX(Consecutivo) + 1 
			FROM [Contrato].[SecuenciaNumeroContrato] 
			WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia
	END
	/*Registra el la secuencia*/
	INSERT INTO Contrato.SecuenciaNumeroContrato(Consecutivo, CodigoRegional, AnoVigencia, UsuarioCrea, FechaCrea)
					  VALUES(@Consecutivo, @CodigoRegional, @AnoVigencia, @UsuarioCrea, GETDATE())
	/*
		Armar numero contrato
	*/
	SELECT @NumeroContrato = right( '00' + cast( @CodigoRegional AS varchar(2)), 2 ) + right( '00000' + cast( @Consecutivo AS varchar(5)), 5 ) + cast( @AnoVigencia AS varchar(4))  

	print @NumeroContrato
END

