﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reporte_Modificar]
		@IdReporte INT,	@NombreReporte NVARCHAR(512),	@Descripcion NVARCHAR(1024),	@Servidor NVARCHAR(512),	@Carpeta NVARCHAR(512),	@NombreArchivo NVARCHAR(512), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.Reporte SET NombreReporte = @NombreReporte
	                         ,Descripcion = @Descripcion
	                         ,Servidor = @Servidor
	                         ,Carpeta = @Carpeta
	                         ,NombreArchivo = @NombreArchivo
	                         ,UsuarioModifica = @UsuarioModifica
	                         ,FechaModifica = GETDATE() 
	WHERE IdReporte = @IdReporte
END
