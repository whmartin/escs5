﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ArchivoObligacioness_Consultar]
	@IdsCuentaCobro nvarchar(2000) 
AS
BEGIN
	DECLARE @TempMaestroObligacionesP as table( id int identity(1,1)
												,Consecutivo  int
												,Fecha Date
												,Codpci Nvarchar(255)
												,numeroCompromiso Nvarchar(255)
												,fechaPago	Date
												,AtributoContable Nvarchar(10)
												,RequiereDIP Nvarchar(10)
												,codTipoDocSoporte int
												,numDocSoporte Nvarchar(40)
												,FechaDocSoporte Date
												,Expedidor Nvarchar(10)
												,NombreFuncionario Nvarchar(10)
												,CargoFuncionario Nvarchar(10)
												,observacion Nvarchar(10)
												,urlDocumento Nvarchar(10)
												,urlDescripcion Nvarchar(10)
												,IDDatosAdmin int
												,IDCuentaCobro int)

	Insert into @TempMaestroObligacionesP
	SELECT ROW_NUMBER() over(ORDER BY cc.IdCuenta)  as Consecutivo
		,GETDATE() as fecha
		,da.Codpci as Codpci
		,cc.codcompromiso as numeroCompromiso
		,GETDATE() as fechaPago	
		,'5'  as AtributoContable	
		,'NO' as RequiereDIP	
		,da.CodTipoDocSoporte as codTipoDocSoporte	
		,da.NumDocSoporte as numDocSoporte	
		,da.FechaDocSoporte as FechaDocSoporte
		,'11' as Expedidor	
		,'' as NombreFuncionario	
		,'' as CargoFuncionario	
		,'' as observacion	
		,'' as urlDocumento	
		,'' as urlDescripcion
		,da.IdDatosAdministracion
		,cc.IdCuenta
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	INNER JOIN SPCP.PlanDePagos pp
	on cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	INNER JOIN SPCP.PlanillaDetalle pd
	ON cc.IdCuenta = pd.IdCuenta
	WHERE cc.IdCuenta IN (SELECT data FROM dbo.Split(@IdsCuentaCobro,';'))
		
		

	SELECT Consecutivo  
			,Fecha 
			,Codpci 
			,numeroCompromiso 
			,fechaPago	
			,AtributoContable 
			,RequiereDIP 
			,codTipoDocSoporte 
			,numDocSoporte 
			,FechaDocSoporte 
			,Expedidor 
			,NombreFuncionario 
			,CargoFuncionario 
			,observacion 
			,urlDocumento
			,urlDescripcion 
	from @TempMaestroObligacionesP

	
	--Como sacar el pago actual la cuenta de cobro no trae el numero de pago
	SELECT op.Consecutivo as ConsecutivoMaestro
			,ROW_NUMBER() over(ORDER BY op.id)  as NumRegistro
			,da.Codpci as codDepAfectacionGasto
			,pp.Rubro as codPosicionGasto	
			,pp.Recurso as codRecursoPresupuestal	--Codigo
			,(CASE p.NomSituacionFondos WHEN 'CSF' THEN '01'
			  when 'SSF' THEN '02'
			  END )codSituacionFondos	--Codigo
			,(CASE pp.Fuente WHEN 'NACION' THEN '01'
			  when 'PROPIOS' THEN '02'
			  END ) as codFuenteFinanciacion		--Codigo
			,pp.ValorDelPago as valorEnPesos	
			,'' as TipoDeGasto	
			,'15' as TipoDeOperacion	
			,'17' as UsoContable	
			,'550706' as CodigoCuentaContable
	from @TempMaestroObligacionesP op
	INNER JOIN SPCP.DatosAdministracion da 	ON op.IDDatosAdmin = da.IdDatosAdministracion
	INNER JOIN SPCP.Presupuestos p 	ON da.IdDatosAdministracion = p.IdDatosAdministracion
	INNER JOIN SPCP.CuentasdeCobro cc on op.IDCuentaCobro = cc.IdCuenta
	INNER JOIN SPCP.PlanDePagos pp ON cc.codcompromiso = pp.CodCompromiso and cc.IdDatosAdministracion = pp.IdDatosAdministracion and cc.NumeroDePago=pp.NumeroDePago
END
