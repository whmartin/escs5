﻿


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/4/2013 11:29:17 AM
-- Description:	Procedimiento almacenado que consulta un(a) TelTerceros por idtercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_ConsultarIdtercero]
@IdTercero INT
AS
BEGIN

	SELECT TOP 1
		IdTelTercero,
		IdTercero,
		IndicativoTelefono,
		NumeroTelefono,
		ExtensionTelefono,
		Movil,
		IndicativoFax,
		NumeroFax,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		CorreoSede
	FROM Tercero.TelTerceros
	WHERE IdTercero = @IdTercero

END


