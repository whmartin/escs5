﻿-- =============================================
-- Author:		ICBF\EquipoFinanciera
-- Create date:  04/03/2015 8:16:01
-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_DIV_Regional_Insertar]
		@IdRegional INT OUTPUT, 	@CodigoRegional NVARCHAR(128),	@NombreRegional NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.Regional(CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegional, @NombreRegional, @UsuarioCrea, GETDATE())
	SELECT @IdRegional = @@IDENTITY 		
END
