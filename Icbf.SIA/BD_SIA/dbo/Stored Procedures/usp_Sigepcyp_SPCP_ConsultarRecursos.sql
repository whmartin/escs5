﻿-- =============================================
-- Author:		Alexander Gutierrez
-- Create date:  21/12/2015 11:35:36 AM
-- Description:	Procedimiento almacenado que consulta un(os) Recurso(s)
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ConsultarRecursos]

AS
BEGIN

		SELECT DISTINCT pp.Recurso			
		FROM SPCP.CuentasdeCobro cc
		INNER JOIN SPCP.DatosAdministracion da
		ON cc.IdDatosAdministracion = da.IdDatosAdministracion
		INNER JOIN Global.TiposDocumentos td
		ON da.TipoDocumento = td.IdTipoDocumento
		INNER JOIN SPCP.PlanDePagos pp
		on cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
		INNER JOIN SPCP.PlanillaDetalle pd
		ON cc.IdCuenta = pd.IdCuenta
		
END
