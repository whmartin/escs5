﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Consultar]
	@IdDocAdjunto INT
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTipoDocumento, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE  IdDocAdjunto = @IdDocAdjunto
END



