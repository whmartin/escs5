﻿





-- ==========================================================================================
-- Author:		 José Ignacio De Los Reyes
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Se Crea para enivar correo de suspension de usuario si su estado es inactivo
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_QoS_Proveedor_UsuariosInactivos]

@UserId NVARCHAR(256)

AS
BEGIN

	DELETE [SEG].[Usuario]
	WHERE [providerKey] = @UserId
	
END--FIN PP


