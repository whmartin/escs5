﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventors_Consultar]
	@OrigenTipoSupervisor bit = NULL,
	@IDTipoSupvInterventor INT = NULL,
	@IDTerceroExterno INT = NULL,
	@TipoVinculacion bit = NULL,
	@FechaInicia DATETIME = NULL,
	@FechaFinaliza DATETIME = NULL,
	@Estado bit = NULL,
	@FechaModificaInterventor DATETIME = NULL
AS
BEGIN
 SELECT IDSupervisorInterv, IDContratoSupervisa, OrigenTipoSupervisor, IDTipoSupvInterventor, 
 IDTerceroExterno, IDFuncionarioInterno, IDContratoInterventoria, 
 TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, 
 FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica,IDTerceroInterventoria 
 FROM [Contrato].[SupervisorIntervContrato] 
 WHERE OrigenTipoSupervisor = CASE WHEN @OrigenTipoSupervisor IS NULL THEN OrigenTipoSupervisor ELSE @OrigenTipoSupervisor END AND IDTipoSupvInterventor = CASE WHEN @IDTipoSupvInterventor IS NULL THEN IDTipoSupvInterventor ELSE @IDTipoSupvInterventor END AND IDTerceroExterno = CASE WHEN @IDTerceroExterno IS NULL THEN IDTerceroExterno ELSE @IDTerceroExterno END AND TipoVinculacion = CASE WHEN @TipoVinculacion IS NULL THEN TipoVinculacion ELSE @TipoVinculacion END AND FechaInicia = CASE WHEN @FechaInicia IS NULL THEN FechaInicia ELSE @FechaInicia END AND FechaFinaliza = CASE WHEN @FechaFinaliza IS NULL THEN FechaFinaliza ELSE @FechaFinaliza END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END AND FechaModificaInterventor = CASE WHEN @FechaModificaInterventor IS NULL THEN FechaModificaInterventor ELSE @FechaModificaInterventor END
END

