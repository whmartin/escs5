﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:05:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) TiposCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_TiposCuentas_Consultar]
	@TipoCuenta NVARCHAR(50) = NULL,@Estado INT = NULL,@IdRegional INT = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','

Set @SqlParametros = '@TipoCuenta NVARCHAR(50),@Estado INT,@IdRegional INT'
Set @SqlExec = '
    SELECT TC.IdTipoCuenta, TC.TipoCuenta, TC.Estado, 
	(CASE TC.Estado WHEN 0 THEN ''Inactivo'' 
      WHEN 1 THEN ''Activo''
	  END ) as NombreEstado, 
	TC.IdRegional, R.NombreRegional, TC.UsuarioCrea, TC.FechaCrea, TC.UsuarioModifica, TC.FechaModifica 
      FROM [SPCP].[TiposCuenta] TC
		   INNER JOIN [DIV].[Regional] R ON TC.IdRegional = R.IdRegional
    WHERE 1=1 '
 If(@TipoCuenta Is Not Null) 
	Set @SqlExec = @SqlExec + ' And TC.TipoCuenta Like ''%'+@TipoCuenta+'%'''  
 If(@Estado Is Not Null) 
	Set @SqlExec = @SqlExec + ' And TC.Estado = @Estado' 
 If(@IdRegional Is Not Null) 
	Set @SqlExec = @SqlExec + ' And TC.IdRegional = @IdRegional' 
Exec sp_executesql  @SqlExec, @SqlParametros,@TipoCuenta,@Estado,@IdRegional
END
