﻿-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Modulo
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarModulo]
		@IdModulo INT,	@NombreModulo NVARCHAR(250),	@Posicion INT,	@Estado BIT, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Modulo SET NombreModulo = @NombreModulo, Posicion = @Posicion, Estado = @Estado, UsuarioModificacion = @UsuarioModificacion, FechaModificacion = GETDATE() WHERE IdModulo = @IdModulo
END
