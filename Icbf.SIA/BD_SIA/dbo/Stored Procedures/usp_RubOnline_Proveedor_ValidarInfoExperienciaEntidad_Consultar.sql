﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar]
	@IdValidarInfoExperienciaEntidad INT
AS
BEGIN
 SELECT IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoExperienciaEntidad] WHERE  IdValidarInfoExperienciaEntidad = @IdValidarInfoExperienciaEntidad
END




