﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que consulta un(a) CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_Consultar]
	@IdCentroZonal INT
AS
BEGIN
 SELECT IdCentroZonal, IdMunicipio, IdRegional, CodigoMunicipio, CodigoCentroZonal, NombreCentroZonal, Direccion, Telefonos, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[CentroZonal] WHERE  IdCentroZonal = @IdCentroZonal
END


