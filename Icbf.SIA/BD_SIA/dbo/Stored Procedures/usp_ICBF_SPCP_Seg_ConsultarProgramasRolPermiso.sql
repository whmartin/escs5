﻿

-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas que tiene asignado un rol
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarProgramasRolPermiso]
	@pNombreRol NVARCHAR(250)
AS
BEGIN

SELECT
	P.IdPrograma, 
	P.IdModulo, 
	P.NombrePrograma, 
	P.CodigoPrograma, 
	P.Posicion, 
	P.Estado, 
	P.VisibleMenu,
	P.UsuarioCreacion, 
	P.FechaCreacion, 
	P.UsuarioModificacion, 
	P.FechaModificacion,
	P.generaLog,
	((CASE WHEN PE.Consultar = 1 THEN 1 ELSE 0 END) + (CASE WHEN PE.Eliminar = 1 THEN 2 ELSE 0 END) + (CASE WHEN PE.Modificar  = 1 THEN 4 ELSE 0 END) + (CASE WHEN PE.Insertar  = 1 THEN 8 ELSE 0 END)) Permiso
FROM
	SEG.Programa P
INNER JOIN
	SEG.Permiso PE ON P.IdPrograma = PE.IdPrograma
INNER JOIN
	SEG.Rol RO ON RO.IdRol = PE.IdRol 	
WHERE
	RO.Nombre = @pNombreRol	


END


