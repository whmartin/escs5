﻿
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarProgramas]
	@pIdModulo INT,
	@pNombreModulo NVARCHAR(250)
AS
BEGIN

SELECT 
	P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
FROM 
	SEG.Programa P
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo	
WHERE 
	P.IdModulo = CASE WHEN @pIdModulo = -1 THEN P.IdModulo ELSE @pIdModulo END
AND
	P.NombrePrograma LIKE CASE WHEN @pNombreModulo = '' THEN P.NombrePrograma ELSE '%'+@pNombreModulo+'%' END


END

