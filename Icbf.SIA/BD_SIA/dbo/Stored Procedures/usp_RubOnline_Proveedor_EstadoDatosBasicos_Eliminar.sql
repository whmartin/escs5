﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Eliminar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
	DELETE Proveedor.EstadoDatosBasicos WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

