﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoFinancieraEntidad por IdInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar_InfoFinancieraEntidad]
	@IdInfoFin INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE  IdInfoFin = @IdInfoFin
 ORDER BY IdValidarInfoFinancieraEntidad DESC
END



