﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_Sigepcyp_SPCP_CuentasEnPlanillas_Consultar
	@IdPlanilla int
AS
BEGIN
	Select cc.IdCuenta,pd.IDPlanillaDetalle,pd.IdPlanilla
	FROM SPCP.PlanillaDetalle pd
	INNER JOIN SPCP.CuentasdeCobro cc
	on pd.IdCuenta = cc.IdCuenta
	WHERE pd.IdPlanilla = @IdPlanilla
END
