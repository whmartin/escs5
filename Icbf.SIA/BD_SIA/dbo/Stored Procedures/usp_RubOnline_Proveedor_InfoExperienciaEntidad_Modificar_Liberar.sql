﻿



--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoExperiencia
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoDocumental INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin
DECLARE @idEdoEnValidacion INT =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN')

	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = @Finalizado,
		EstadoDocumental = @EstadoDocumental
	where 
		IdEntidad = @IdEntidad 
		AND EstadoDocumental=@idEdoEnValidacion


end




