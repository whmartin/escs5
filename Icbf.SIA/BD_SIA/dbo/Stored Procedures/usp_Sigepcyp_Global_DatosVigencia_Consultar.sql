﻿

--- =================================================================
-- Author:		<Author,,Zulma Barrantes>
-- Create date: <13 de Agosto del 2015,12:03 p.m.,>
-- Description:	<Permite consultar Información de Datos Generales de una Vigencia,>
-- ====================================================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_DatosVigencia_Consultar]
	@IdDatosVigencia INT
AS
BEGIN
 SELECT IdDatosVigencia, Codigo, Descripcion, FechaInicial, Valor, Porcentaje, Normatividad, BaseRenta, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[DatosVigencia] WHERE  IdDatosVigencia = @IdDatosVigencia
END
