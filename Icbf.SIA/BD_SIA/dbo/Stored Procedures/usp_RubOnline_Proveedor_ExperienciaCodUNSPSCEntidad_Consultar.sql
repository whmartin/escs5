﻿-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Consultar]
	@IdExpCOD INT
AS
BEGIN
 SELECT IdExpCOD, IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] WHERE  IdExpCOD = @IdExpCOD
END

