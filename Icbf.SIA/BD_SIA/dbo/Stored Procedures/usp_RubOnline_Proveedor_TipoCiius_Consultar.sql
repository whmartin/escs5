﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiius_Consultar]
	@CodigoCiiu NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCiiu, CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
			FROM [Proveedor].[TipoCiiu] 
			WHERE 
				CodigoCiiu LIKE CASE WHEN @CodigoCiiu IS NULL THEN CodigoCiiu ELSE '%' + @CodigoCiiu + '%' END 
				AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE '%' + @Descripcion + '%' END 
				AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
			ORDER BY CodigoCiiu
END



