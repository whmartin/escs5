﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/6/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que consulta un(a) PlanDePagos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_PlanDePagos_Consultar]
	@IdPlanDePagos INT
AS
BEGIN
 SELECT IdPlanDePagos, IdDatosAdministracion, Codpci, Vigencia, ConsecutivoPACCO, CodCompromiso, Fecha, TipoDocumento, NumDocIdentidad, CodTipoDocSoporte, NumDocSoporte, ValorTotalContrato, Rubro, Recurso, Fuente, NumeroDePago, OrdenDePago, ValorDelPago, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[PlanDePagos] WHERE  IdPlanDePagos = @IdPlanDePagos
END