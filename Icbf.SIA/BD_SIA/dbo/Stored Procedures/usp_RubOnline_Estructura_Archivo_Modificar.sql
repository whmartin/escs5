﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que actualiza un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Modificar]
		    @IdArchivo NUMERIC(18,0)
		,	@IdUsuario INT
		,	@IdFormatoArchivo INT
		,	@FechaRegistro DATETIME
		,	@NombreArchivo NVARCHAR(128)
		,	@Estado NVARCHAR(1)
		,	@ResumenCarga NVARCHAR(256)
		, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Estructura.Archivo SET IdUsuario = @IdUsuario, IdFormatoArchivo = @IdFormatoArchivo, FechaRegistro = @FechaRegistro, NombreArchivo = @NombreArchivo, Estado = @Estado, ResumenCarga = @ResumenCarga, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdArchivo = @IdArchivo
END

