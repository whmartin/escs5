﻿
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Consultar]
	@IdTipoRegimenTributario INT
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t 
 ON trt.IdTipoPersona = t.IdTipoPersona
 --SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 --FROM [Proveedor].[TipoRegimenTributario] 
 WHERE  trt.IdTipoRegimenTributario = @IdTipoRegimenTributario
END


