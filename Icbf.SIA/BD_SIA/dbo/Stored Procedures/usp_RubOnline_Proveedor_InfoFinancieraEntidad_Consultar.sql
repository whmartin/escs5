﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Consultar]
	@IdInfoFin INT
AS
BEGIN
 SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, Finalizado, NroRevision FROM [Proveedor].[InfoFinancieraEntidad] WHERE  IdInfoFin = @IdInfoFin
END



