﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_ModificarEstado]
		@IdTercero INT, @IdEstadoTercero INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Oferente.Tercero 
	SET IdEstadoTercero = @IdEstadoTercero, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdTercero = @IdTercero
END


