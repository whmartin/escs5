﻿
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  10/04/2014
-- Description:	Procedimiento almacenado que consulta un(a) Estado del Proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_EstadoProveedor_Consultar]
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoProveedor, Descripcion FROM PROVEEDOR.EstadoProveedor
 WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


