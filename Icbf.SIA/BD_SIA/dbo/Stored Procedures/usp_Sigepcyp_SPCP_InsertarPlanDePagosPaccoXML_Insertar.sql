﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/27/2015 10:16:23 AM
-- Description:	Procedimiento almacenado que guarda un nuevo AsignarCodigoSIIFcc
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_InsertarPlanDePagosPaccoXML_Insertar]
	@XMLDatosPacco XML,
	@UsuarioCrea NVARCHAR(250)	
AS
BEGIN
	
	BEGIN TRY
        BEGIN TRANSACTION

			--Gurdamos los datos en una variable
			Declare @TempDatosPAccoIn as table(ID int identity(1,1),
												Codpci VARCHAR(250),
												Vigencia INT,
												ConsecutivoPACCO INT,
												CodCompromiso INT,
												Fecha Datetime,
												NumDocIdentidad VARCHAR(20),
												CodTipoDocSoporte INT,
												NumDocSoporte VARCHAR(40),
												ValorTotalContrato Decimal(17,3),
												Rubro VARCHAR(30),
												Recurso INT,
												Fuente VARCHAR(15),
												NumeroDePago VARCHAR(15),
												OrdenDePago INT,
												ValorDelPago Decimal(17,3),
												Estado INT)


			Insert into @TempDatosPAccoIn
			SELECT	DatosPaccoXML.value('(Codpci/text())[1]', 'VARCHAR(250)') AS Codpci,
					DatosPaccoXML.value('(Vigencia/text())[1]', 'INT') AS Vigencia,
					DatosPaccoXML.value('(ConsecutivoPACCO/text())[1]', 'INT') AS ConsecutivoPACCO,
					DatosPaccoXML.value('(CodCompromiso/text())[1]', 'INT') AS CodCompromiso,
					DatosPaccoXML.value('(Fecha/text())[1]', 'Datetime') AS Fecha,			
					DatosPaccoXML.value('(NumDocIdentidad/text())[1]', 'VARCHAR(20)') AS NumDocIdentidad,
					DatosPaccoXML.value('(CodTipoDocSoporte/text())[1]', 'INT') AS CodTipoDocSoporte,
					DatosPaccoXML.value('(NumDocSoporte/text())[1]', 'VARCHAR(40)') AS NumDocSoporte,
					DatosPaccoXML.value('(ValorTotalContrato/text())[1]', 'Decimal(17,3)') AS ValorTotalContrato,
					DatosPaccoXML.value('(Rubro/text())[1]', 'VARCHAR(30)') AS Rubro,
					DatosPaccoXML.value('(Recurso/text())[1]', 'INT') AS Recurso,
					DatosPaccoXML.value('(Fuente/text())[1]', 'VARCHAR(15)') AS Fuente,
					DatosPaccoXML.value('(NumeroDePago/text())[1]', 'VARCHAR(15)') AS NumeroDePago,
					DatosPaccoXML.value('(OrdenDePago/text())[1]', 'INT') AS OrdenDePago,
					DatosPaccoXML.value('(ValorDelPago/text())[1]', 'Decimal(17,3)') AS ValorDelPago,
					DatosPaccoXML.value('(Estado/text())[1]', 'INT') AS Estado			
			FROM @XMLDatosPacco.nodes('/DatosPaccoServicio/DatosPacco') AS TAp(DatosPaccoXML)


			--Eliminamos los registros que ya no llegan de PACCO

			DELETE FROM SPCP.PlanDePagos
			where IdPlanDePagos in (SELECT pp.IdPlanDePagos
									FROM SPCP.PlanDePagos pp
									INNER JOIN SPCP.DatosAdministracion da
									ON pp.IdDatosAdministracion = da.IdDatosAdministracion
									INNER JOIN Global.TiposDocumentos td
									ON pp.TipoDocumento = td.IdTipoDocumento
									and td.CodDocumento = 'CC'
									LEFT JOIN @TempDatosPAccoIn temdatos
									ON da.NumDocIdentidad = temdatos.NumDocIdentidad --Cruces con paco Unicos
									AND temdatos.NumeroDePago = pp.NumeroDePago
									AND temdatos.OrdenDePago = pp.OrdenDePago
									AND temdatos.Codpci = pp.Codpci
									AND temdatos.Vigencia = pp.Vigencia
									--AND temdatos.ConsecutivoPACCO = pp.ConsecutivoPACCO
									WHERE da.NumDocIdentidad in (select Temp2Pacco.NumDocIdentidad
																FROM @TempDatosPAccoIn as Temp2Pacco)
									AND temdatos.NumDocIdentidad IS NULL
									AND pp.Estado = 1
									AND pp.Vigencia in (select Temp3Pacco.Vigencia
																FROM @TempDatosPAccoIn as Temp3Pacco)
									AND pp.Codpci in (select Temp4Pacco.Codpci
																FROM @TempDatosPAccoIn as Temp4Pacco)
									--AND pp.ConsecutivoPACCO in (select Temp5Pacco.ConsecutivoPACCO
									--							FROM @TempDatosPAccoIn as Temp5Pacco)
									AND pp.OrdenDePago in (select Temp6Pacco.OrdenDePago
																FROM @TempDatosPAccoIn as Temp6Pacco))

			--Actualizamos los que ya existen
			UPDATE pg set pg.Codpci = updDatosPacco.Codpci
							,pg.Vigencia = updDatosPacco.Vigencia
							,pg.ConsecutivoPACCO = updDatosPacco.ConsecutivoPACCO
							,pg.CodCompromiso = updDatosPacco.CodCompromiso
							,pg.Fecha = updDatosPacco.Fecha
							,pg.NumDocIdentidad = updDatosPacco.NumDocIdentidad
							,pg.CodTipoDocSoporte = updDatosPacco.CodTipoDocSoporte
							,pg.NumDocSoporte = updDatosPacco.NumDocSoporte
							,pg.ValorTotalContrato = updDatosPacco.ValorTotalContrato
							,pg.Rubro = updDatosPacco.Rubro
							,pg.Recurso = updDatosPacco.Recurso
							,pg.Fuente = updDatosPacco.Fuente
							,pg.NumeroDePago = updDatosPacco.NumeroDePago
							,pg.OrdenDePago = updDatosPacco.OrdenDePago
							,pg.ValorDelPago = updDatosPacco.ValorDelPago
							,pg.Estado = updDatosPacco.Estado
							,pg.UsuarioModifica = @UsuarioCrea
							,pg.FechaModifica = GETDATE()
			FROM @TempDatosPAccoIn updDatosPacco
			INNER JOIN SPCP.DatosAdministracion da
			ON updDatosPacco.NumDocIdentidad = da.NumDocIdentidad
			INNER JOIN Global.TiposDocumentos td
			ON da.TipoDocumento = td.IdTipoDocumento
			AND td.CodDocumento = 'CC'
			INNER JOIN SPCP.PlanDePagos pg
			ON da.IdDatosAdministracion = pg.IdDatosAdministracion
			AND updDatosPacco.NumeroDePago = pg.NumeroDePago
			AND updDatosPacco.Vigencia = pg.Vigencia
			AND updDatosPacco.Codpci = pg.Codpci
			AND updDatosPacco.OrdenDePago = pg.OrdenDePago							
			--AND updDatosPacco.ConsecutivoPACCO = pg.ConsecutivoPACCO
			WHERE pg.Estado = 1 --Todos los que estan en pendiente de generar cuenta de cobro


			--insertammos todos los registros nuevos
			INSERT INTO SPCP.PlanDePagos (IdDatosAdministracion
											,Codpci
											,Vigencia
											,ConsecutivoPACCO
											,CodCompromiso
											,Fecha
											,TipoDocumento
											,NumDocIdentidad
											,CodTipoDocSoporte
											,NumDocSoporte
											,ValorTotalContrato
											,Rubro
											,Recurso
											,Fuente
											,NumeroDePago
											,OrdenDePago
											,ValorDelPago
											,Estado
											,UsuarioCrea
											,FechaCrea)

			SELECT	da.IdDatosAdministracion,
					insDatosPacco.Codpci,
					insDatosPacco.Vigencia,
					insDatosPacco.ConsecutivoPACCO,
					insDatosPacco.CodCompromiso,
					insDatosPacco.Fecha,
					td.IdTipoDocumento,
					insDatosPacco.NumDocIdentidad,
					insDatosPacco.CodTipoDocSoporte,
					insDatosPacco.NumDocSoporte,
					insDatosPacco.ValorTotalContrato,
					insDatosPacco.Rubro,
					insDatosPacco.Recurso,
					insDatosPacco.Fuente,
					insDatosPacco.NumeroDePago,
					insDatosPacco.OrdenDePago,
					insDatosPacco.ValorDelPago,
					insDatosPacco.Estado,
					@UsuarioCrea,
					GETDATE()
			FROM @TempDatosPAccoIn insDatosPacco
			INNER JOIN SPCP.DatosAdministracion da
			ON insDatosPacco.NumDocIdentidad = da.NumDocIdentidad
			INNER JOIN Global.TiposDocumentos td
			ON da.TipoDocumento = td.IdTipoDocumento
			AND td.CodDocumento = 'CC'
			LEFT JOIN SPCP.PlanDePagos pg
			ON da.IdDatosAdministracion = pg.IdDatosAdministracion
			AND insDatosPacco.NumeroDePago = pg.NumeroDePago
			AND insDatosPacco.Vigencia = pg.Vigencia
			AND insDatosPacco.Codpci = pg.Codpci
			AND insDatosPacco.OrdenDePago = pg.OrdenDePago							
			--AND insDatosPacco.ConsecutivoPACCO = pg.ConsecutivoPACCO
			WHERE pg.IdPlanDePagos is NULL
	
		COMMIT TRANSACTION
    END TRY        
    BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
        SELECT  @ErrorMessage = ERROR_MESSAGE()
        IF XACT_STATE() <> 0 
            ROLLBACK TRANSACTION
        RAISERROR (@ErrorMessage, 16, 1)         
    END CATCH
	
	

END