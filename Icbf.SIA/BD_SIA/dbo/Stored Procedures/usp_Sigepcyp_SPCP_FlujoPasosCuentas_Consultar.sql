﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:06:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) FlujoPasosCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuentas_Consultar]
	@IdTipoCuenta INT = NULL,@IdPaso INT = NULL,@Orden INT = NULL, @IdRegional Int=null
	,@esFinanciera bit = null
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@IdTipoCuenta INT,@IdPaso INT,@Orden INT, @IdRegional Int, @esFinanciera bit'
Set @SqlExec = '
  SELECT FPC.IdFlujoPasoCuenta, FPC.IdTipoCuenta, TC.TipoCuenta, FPC.IdPaso, p.Paso, FPC.Orden, FPC.UsuarioCrea, FPC.FechaCrea, FPC.UsuarioModifica, FPC.FechaModifica, TC.IdRegional 
   FROM [SPCP].[FlujoPasosCuenta] FPC
   INNER JOIN [SPCP].[TiposCuenta] TC ON FPC.IdTipoCuenta = TC.IdTipoCuenta
   INNER JOIN [SPCP].[Pasos] P ON P.IdPaso = FPC.IdPaso
   WHERE 1=1'
 If(@IdTipoCuenta Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPC.IdTipoCuenta = @IdTipoCuenta' 
 If(@IdPaso Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPC.IdPaso = @IdPaso' 
 If(@Orden Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPC.Orden = @Orden' 
 If(@IdRegional Is Not Null) 
	Set @SqlExec = @SqlExec + ' And TC.IdRegional  = @IdRegional' 
 If(@esFinanciera Is Not Null) 
	Set @SqlExec = @SqlExec + ' And P.Financiera = @esFinanciera' 

	set	@SqlExec = @SqlExec +'	
	order by FPC.Orden'

Exec sp_executesql  @SqlExec, @SqlParametros,@IdTipoCuenta,@IdPaso,@Orden,@IdRegional,@esFinanciera
END

