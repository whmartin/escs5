﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 4:44:03 PM
-- Description:	Procedimiento almacenado que consulta un(a) Departamento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Departamento_Consultar]
	@IdDepartamento INT
AS
BEGIN
 SELECT IdDepartamento, 
		IdPais, 
		CodigoDepartamento, 
		NombreDepartamento, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Departamento] WHERE  IdDepartamento = @IdDepartamento
END

