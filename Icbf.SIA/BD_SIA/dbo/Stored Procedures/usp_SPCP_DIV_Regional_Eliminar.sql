﻿-- =============================================
-- Author:		ICBF\EquipoFinanciera
-- Create date:  04/03/2015 8:16:01
-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_DIV_Regional_Eliminar]
	@IdRegional INT
AS
BEGIN
	DELETE DIV.Regional WHERE IdRegional = @IdRegional
END
