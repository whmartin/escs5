﻿


--===================================================================================
--Autor: Mauricio Martinez
--Fecha: 2013/07/30
--Descripcion: Para liberar InfoFinanciera
--===================================================================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar_Liberar]
(@IdEntidad INT, @EstadoValidacion INT, @UsuarioModifica VARCHAR(256), @Finalizado BIT )
as
begin

DECLARE @idEdoEnValidacion INT =(SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental WHERE Descripcion='EN VALIDACIÓN')
	update Proveedor.InfoFinancieraEntidad 
	set Finalizado = @Finalizado,
		EstadoValidacion = @EstadoValidacion,
		UsuarioModifica = @UsuarioModifica
	where 
		IdEntidad = @IdEntidad 
		AND EstadoValidacion=@idEdoEnValidacion

end



