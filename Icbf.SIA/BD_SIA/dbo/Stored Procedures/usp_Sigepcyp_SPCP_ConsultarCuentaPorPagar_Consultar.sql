﻿


-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ConsultarCuentaPorPagar_Consultar]
	@IdArea int = NULL
	,@pPlnilla int = null
	,@FechaCorte Datetime = NULL
AS
BEGIN
	
	Declare @IDPasoCXP int

	SELECT @IDPasoCXP  = IdPaso
	FROM SPCP.Pasos
	WHERE Paso = 'GENERAR ARCHIVO CXP'


	SELECT pd.IdPlanilla as IdPlanilla
			,a.NombreArea as NombreArea 
            ,pp.Rubro as NumeroRp 
            ,da.NumDocIdentidad as NumeroIdentificacion 
            ,da.NumDocSoporte as NumDocFuente 
            ,cc.IdCuenta as NumCuentaCobro 
            ,tab1.fechaAprobacion as FechaAprobacionCuenta
			,convert(numeric(30,2),pp.ValorDelPago) as ValorPagar
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.PlanillaDetalle pd
	ON cc.IdCuenta = pd.IdCuenta
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN SPCP.PlanDePagos pp
	ON cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	INNER JOIN Global.Areas a on da.IdArea = a.IdArea
	INNER JOIN (SELECT max(temhc.fechaCrea) fechaAprobacion,temhc.IDCuentaCobro
				from SPCP.HistoricoEstadoCuentasDeCobro temhc				
				Group by temhc.IDCuentaCobro) tab1
	ON cc.IdCuenta = tab1.IDCuentaCobro
	INNER JOIN SPCP.FlujoPasosCuenta fpc
	ON cc.IdEstadoPaso = fpc.IdFlujoPasoCuenta
	WHERE a.IdArea = ISNULL(@IdArea,a.IdArea)
	AND pd.IdPlanilla = ISNULL(@pPlnilla,pd.IdPlanilla)
	AND tab1.fechaAprobacion = ISNULL(@FechaCorte,tab1.fechaAprobacion)
	AND cc.GeneradoCxP = 0
	AND fpc.IdPaso = @IDPasoCXP
		
END
