﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:06:28 PM
-- Description:	Procedimiento almacenado que actualiza un(a) FlujoPasosCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuenta_Modificar]
		@IdFlujoPasoCuenta INT,	@IdTipoCuenta INT,	@IdPaso INT,	@Orden INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.FlujoPasosCuenta 
	   SET IdTipoCuenta = @IdTipoCuenta, IdPaso = @IdPaso, Orden = @Orden, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	 WHERE IdFlujoPasoCuenta = @IdFlujoPasoCuenta
END
