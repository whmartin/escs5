﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Insertar]
		@IdNiveldegobierno INT OUTPUT, 	@CodigoNiveldegobierno NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.Niveldegobierno(CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNiveldegobierno, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNiveldegobierno = SCOPE_IDENTITY() 		
END


