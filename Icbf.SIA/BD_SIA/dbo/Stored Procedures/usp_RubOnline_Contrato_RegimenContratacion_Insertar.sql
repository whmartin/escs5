﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Insertar]
		@IdRegimenContratacion INT OUTPUT, 	@NombreRegimenContratacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.RegimenContratacion(NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreRegimenContratacion, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdRegimenContratacion = @@IDENTITY 		
END
