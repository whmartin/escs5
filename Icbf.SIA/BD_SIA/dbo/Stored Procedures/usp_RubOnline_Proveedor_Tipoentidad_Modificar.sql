﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Tipoentidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidad_Modificar]
		@IdTipoentidad INT,	@CodigoTipoentidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Tipoentidad SET CodigoTipoentidad = @CodigoTipoentidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoentidad = @IdTipoentidad
END

