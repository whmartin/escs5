﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que consulta un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_sConsultar]
	@CodigoTituloObtenido NVARCHAR(128) = NULL,@NombreTituloObtenido NVARCHAR(128) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdTituloObtenido, CodigoTituloObtenido, NombreTituloObtenido, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[TituloObtenido] WHERE CodigoTituloObtenido = CASE WHEN @CodigoTituloObtenido IS NULL THEN CodigoTituloObtenido ELSE @CodigoTituloObtenido END AND NombreTituloObtenido = CASE WHEN @NombreTituloObtenido IS NULL THEN NombreTituloObtenido ELSE @NombreTituloObtenido END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

