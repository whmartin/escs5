﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:54:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) CentroPoblado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroPoblado_Consultar]
	@IdCentroPoblado INT
AS
BEGIN
 SELECT IdCentroPoblado
      , IdMunicipio
      , CodigoCentroPoblado
      , NombreCentroPoblado
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
 FROM [DIV].[CentroPoblado] 
 WHERE  IdCentroPoblado = @IdCentroPoblado
END

