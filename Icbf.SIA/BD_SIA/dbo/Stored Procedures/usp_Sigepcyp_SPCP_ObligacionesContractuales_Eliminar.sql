﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 2:14:48 PM
-- Description:	Procedimiento almacenado que elimina un(a) ObligacionesContractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ObligacionesContractuales_Eliminar]
	@IdObligacionContractual INT
AS
BEGIN
	DELETE SPCP.ObligacionesContractuales WHERE IdObligacionContractual = @IdObligacionContractual
END
