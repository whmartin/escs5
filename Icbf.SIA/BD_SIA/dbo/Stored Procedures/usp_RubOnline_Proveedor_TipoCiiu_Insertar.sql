﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Insertar]
		@IdTipoCiiu INT OUTPUT, 	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCiiu(CodigoCiiu, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoCiiu, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCiiu = SCOPE_IDENTITY() 		
END


