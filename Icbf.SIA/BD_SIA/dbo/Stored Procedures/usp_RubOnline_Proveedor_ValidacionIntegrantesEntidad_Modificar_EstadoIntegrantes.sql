﻿


-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date:  07/AGO/2014
-- Description:	Procedimiento almacenado que actualiza el estado de validacion del modulo integrantes
-- para una EntidadProveedor.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidacionIntegrantesEntidad_Modificar_EstadoIntegrantes]
		@IdEntidad INT,	
		@IdEstadoIntegrantes INT = NULL, 
		@UsuarioModifica NVARCHAR(250)= NULL,
		@Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.ValidacionIntegrantesEntidad 
			SET IdEstadoValidacionIntegrantes =ISNULL( @IdEstadoIntegrantes, IdEstadoValidacionIntegrantes ),
			    UsuarioModifica = @UsuarioModifica, 
			    FechaModifica = GETDATE(),
			    Finalizado = ISNULL(@Finalizado,Finalizado)
			   WHERE IdEntidad = @IdEntidad 
END




