﻿						
CREATE PROCEDURE [dbo].[usp_SPCP_Seg_ConsultarUsuarios]
	@IdPerfil int = 0,
	@NumeroDocumento NVARCHAR(250),
	@PrimerNombre NVARCHAR(250),
	@PrimerApellido NVARCHAR(250),
	@Estado BIT = NULL,
	@IdTipoUsuario INT = 1,
	@IdRegional INT = NULL,
	@IdArea INT = NULL
AS
BEGIN
  SELECT SEG.Usuario.IdUsuario
		,SEG.Usuario.IdTipoDocumento
		,Global.TiposDocumentos.NomTipoDocumento
		,SEG.Usuario.NumeroDocumento
		,SEG.Usuario.PrimerNombre
		,SEG.Usuario.SegundoNombre
		,SEG.Usuario.PrimerApellido
		,SEG.Usuario.SegundoApellido
		,SEG.Usuario.TelefonoContacto     
		,SEG.Usuario.CorreoElectronico
		,SEG.Usuario.Estado 
		,(CASE Estado WHEN 0 THEN 'Inactivo'
		  WHEN 1 THEN 'Activo'
		  END ) as NombreEstado
			,SEG.Usuario.providerKey
		,SEG.Usuario.UsuarioCreacion
		,SEG.Usuario.FechaCreacion
		,SEG.Usuario.UsuarioModificacion
		,SEG.Usuario.FechaModificacion
		,SEG.Usuario.IdTipoUsuario
		,SEG.Usuario.IdRegional
		,SEG.Usuario.IdArea
		,SEG.Usuario.CodPCI
  FROM SEG.Usuario INNER JOIN [Global].[TiposDocumentos] ON SEG.Usuario.IdTipoDocumento=[Global].[TiposDocumentos].[IdTipoDocumento]
 WHERE SEG.Usuario.UsuarioCreacion <> 'Administrador' AND IdRegional IS NOT NULL
        AND 
	    SEG.Usuario.NumeroDocumento  LIKE CASE WHEN @NumeroDocumento IS NULL THEN SEG.Usuario.NumeroDocumento ELSE '%'+@NumeroDocumento+'%' END
		AND SEG.Usuario.PrimerNombre LIKE CASE WHEN @PrimerNombre IS NULL 
											   THEN SEG.Usuario.PrimerNombre 
											   ELSE '%'+@PrimerNombre+'%' 
												END
		AND SEG.Usuario.PrimerApellido LIKE CASE WHEN @PrimerApellido IS NULL 
												 THEN SEG.Usuario.PrimerApellido 
												 ELSE '%'+@PrimerApellido+'%' 
												 END
		AND SEG.Usuario.Estado = CASE WHEN @Estado	IS NULL 
									  THEN SEG.Usuario.Estado 
									  ELSE @Estado 
									  END
		AND SEG.Usuario.IdRegional = CASE WHEN @IdRegional	IS NULL 
										  THEN SEG.Usuario.IdRegional 
										  ELSE @IdRegional 
										  END
		AND SEG.Usuario.IdArea = CASE WHEN @IdArea	IS NULL 
									  THEN SEG.Usuario.IdArea 
									  ELSE @IdArea 
									  END
  order by 5
END



