﻿-- =============================================
-- Author:		Oscar Javier  Sosa Parada
-- Create date:  16/10/2012
-- Description:	Procedimiento almacenado que elimina programas permitidos a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_Seg_EliminarProgramasRol]
	@pIdRol INT
AS
BEGIN
	DELETE FROM SEG.ProgramaRol WHERE IdRol = @pIdRol
END
