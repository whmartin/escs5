﻿
-- =============================================    
-- Author:  Jorge Vizcaino    
-- Create date:  7/18/2015 3:21:49 PM    
-- Description: Procedimiento almacenado que consulta un(a) Planilla    
-- =============================================    
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Planilla_Consultar]    
 @IDPlanilla INT    
AS    
BEGIN    
 SELECT ROW_NUMBER() OVER(ORDER BY p.IDPlanilla ASC) AS Item    
   ,p.IDPlanilla    
   ,p.FechaCrea    
   ,da.TipoDocumento    
   ,da.CodTipoDocSoporte as tipoDocFuente    
   ,da.FechaDocSoporte as fechaDocFuente    
   ,da.NumDocSoporte as NumeroDocFuente    
   ,da.NumDocIdentidad    
   ,da.NomTercero          
   ,cc.CodCompromiso as numeroRP    
   ,rp.CodPosicionGasto  as NombreIdentificadorPre    
   ,rp.CodRecursoPresupuestal  as RecursoPresupuestal     
   ,rp.NomPosicionGasto    
   ,Convert(numeric(30,2),pp.ValorDelPago) as HonorarioPagar    
   ,p.IdRegional    
   ,p.IdArea    
   ,p.UsuarioCrea    
   ,p.FechaCrea    
   ,p.UsuarioModifica    
   ,p.FechaModifica    
   ,cc.ConsecutivoPac  
 FROM SPCP.Planilla p    
 INNER JOIN SPCP.PlanillaDetalle pd      ON p.IDPlanilla = pd.IdPlanilla    
 INNER JOIN SPCP.CuentasDeCobro cc       ON pd.IdCuenta = cc.IdCuenta    
 INNER JOIN SPCP.DatosAdministracion da  ON cc.IdDatosAdministracion = da.IdDatosAdministracion    
 INNER JOIN SPCP.Presupuestos rp		 ON da.IdDatosAdministracion = rp.IdDatosAdministracion    
 INNER JOIN SPCP.PlanDePagos pp			 ON cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion  
 WHERE p.IDPlanilla = @IDPlanilla    
END    


