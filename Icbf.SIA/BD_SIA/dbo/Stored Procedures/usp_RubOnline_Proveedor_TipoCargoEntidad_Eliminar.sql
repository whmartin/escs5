﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Eliminar]
	@IdTipoCargoEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoCargoEntidad 
	WHERE IdTipoCargoEntidad = @IdTipoCargoEntidad
END

