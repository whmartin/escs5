﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocDatosBasico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdEntidad INT = NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN

	SET @IdDocAdjunto = 0
	IF (@IdEntidad = 0) SET @IdEntidad = NULL  
	UPDATE Proveedor.DocDatosBasicoProv
	SET Activo=0 
	WHERE IdTemporal=@IdTemporal and IdTipoDocumento=@IdTipoDocumento AND IdEntidad IS NULL
	
	INSERT INTO Proveedor.DocDatosBasicoProv(NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES( @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = SCOPE_IDENTITY() 		
END
