﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucions_Consultar]
@IdContratoLugarEjecucion	INT
AS
BEGIN
 SELECT IdContratoLugarEjecucion, IDDepartamento, IDMunicipio, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[LugarEjecucion] 
 WHERE 
 IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

