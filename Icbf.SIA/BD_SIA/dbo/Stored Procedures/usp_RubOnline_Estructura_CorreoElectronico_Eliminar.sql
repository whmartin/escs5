﻿-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que elimina un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Eliminar]
	@IdCorreoElectronico NUMERIC(18,0)
AS
BEGIN
	DELETE Estructura.CorreoElectronico WHERE IdCorreoElectronico = @IdCorreoElectronico
END


