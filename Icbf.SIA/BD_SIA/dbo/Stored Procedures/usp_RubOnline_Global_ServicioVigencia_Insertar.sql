﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que guarda un nuevo ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Insertar]
		@IdServicioVigencia INT OUTPUT, 	@IdVigencia INT,	@IdServicio INT,	@IdRubro INT,	@CodigoServicio NVARCHAR(128),	@NombreServicio NVARCHAR(256),	@CodigoRubro NVARCHAR(128),	@NombreRubro NVARCHAR(256),	@Estado NVARCHAR, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.ServicioVigencia(IdVigencia, IdServicio, IdRubro, CodigoServicio, NombreServicio, CodigoRubro, NombreRubro, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdVigencia, @IdServicio, @IdRubro, @CodigoServicio, @NombreServicio, @CodigoRubro, @NombreRubro, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdServicioVigencia = @@IDENTITY 		
END

