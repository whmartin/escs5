﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Eliminar]
	@IdRegimenContratacion INT
AS
BEGIN
	DELETE Contrato.RegimenContratacion WHERE IdRegimenContratacion = @IdRegimenContratacion
END

