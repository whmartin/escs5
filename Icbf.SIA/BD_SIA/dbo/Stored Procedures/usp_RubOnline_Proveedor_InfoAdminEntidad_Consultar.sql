﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Consultar]
	@IdInfoAdmin INT
AS
BEGIN
 SELECT IdInfoAdmin, IdVigencia, IdEntidad, IdTipoRegTrib, IdTipoOrigenCapital, IdTipoActividad, IdTipoEntidad, IdTipoNaturalezaJurid, 
 IdTipoRangosTrabajadores, IdTipoRangosActivos, IdRepLegal, IdTipoCertificaTamano, IdTipoEntidadPublica, IdDepartamentoConstituida, IdMunicipioConstituida, 
 IdDepartamentoDirComercial, IdMunicipioDirComercial, DireccionComercial, IdZona, NombreComercial, NombreEstablecimiento, Sigla, PorctjPrivado, 
 PorctjPublico, SitioWeb, NombreEntidadAcreditadora, Organigrama, TotalPnalAnnoPrevio, VincLaboral, PrestServicios, Voluntariado, VoluntPermanente, Asociados, 
 Mision, PQRS, GestionDocumental, AuditoriaInterna, ManProcedimiento, ManPracticasAmbiente, ManComportOrg, ManFunciones, ProcRegInfoContable, 
 PartMesasTerritoriales, PartAsocAgremia, PartConsejosComun, ConvInterInst, ProcSeleccGral, ProcSeleccEtnico, PlanInduccCapac, EvalDesemp, PlanCualificacion, 
 NumSedes, SedesPropias, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[InfoAdminEntidad] 
 WHERE  IdInfoAdmin = @IdInfoAdmin
END

