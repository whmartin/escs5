﻿
CREATE procedure [dbo].[usp_ICBF_Proveedores_GetDataRepProvActBienServ]
	 @CodigoDepartamento nvarchar(max)
	,@CodigoCiiu nvarchar(max)
	,@CodigoSegmento nvarchar(max)
	,@CodigoFamilia nvarchar(max)
	,@CodigoClase nvarchar(max)
	,@codigoUNSPSC nvarchar(max)
	with recompile
as
begin

Declare @query nvarchar(max) = ''
	set	@query = 'select	Proveedor = 
				  (
				  case when z.[IdTipoPersona] = 1 
					   then  (t.[PRIMERNOMBRE]+'' ''+t.[SEGUNDONOMBRE]+'' ''+t.[PRIMERAPELLIDO]+'' ''+t.[SEGUNDOAPELLIDO])
					   else  t.[RAZONSOCIAL]
						end
					)
	   ,Persona =
				(
				case	when z.[IdTipoPersona] = 4 
						then  ''U.TEMPORAL''
						else  z.[NombreTipoPersona] 
				end
				)
		,T_ID = b.[CodDocumento]
		,Identificacion = t.[NUMEROIDENTIFICACION]
		,DV = t.[DIGITOVERIFICACION]
		,Departamento = d.[NombreDepartamento]
		,Municipio = e.[NombreMunicipio]
		,Direccion = a.[DireccionComercial] 
		,Telefono = ( Select (convert(nvarchar,TELT.[NumeroTelefono])+'' ''+(convert(nvarchar,TELT.[ExtensionTelefono]))) from [Oferentes].[Oferente].[TelTerceros] TELT where TELT.IdTelTercero in 
					(select MAX(TELT.IdTelTercero) FROM [Oferentes].[Oferente].[TelTerceros] TELT WHERE telt.IdTercero=x.IDTERCERO))
		,Celular = ( Select TELT.[Movil] from [Oferentes].[Oferente].[TelTerceros] TELT where TELT.IdTelTercero in 
					(select MAX(TELT.IdTelTercero) FROM [Oferentes].[Oferente].[TelTerceros] TELT WHERE telt.IdTercero=x.IDTERCERO))
		,Correo_electronico = t.[CORREOELECTRONICO]
		,Contacto =  (Select CE.[PrimerNombre]+'' ''+CE.[SEGUNDONOMBRE]+'' ''+CE.[PRIMERAPELLIDO]+'' ''+CE.[SEGUNDOAPELLIDO] from [Oferentes].[Oferente].[ContactosEntidad] CE 
						where CE.[FechaCrea] in (select MAX(CE.[FechaCrea]) FROM [Oferentes].[Oferente].[ContactosEntidad] CE WHERE CE.[IdEntidad]  = x.[IdEntidad]))
		,Datos_Contacto =  (Select convert(nvarchar,CE.[NumeroTelefono])+'' ''+CE.[Email] from [Oferentes].[Oferente].[ContactosEntidad] CE 
							 where CE.[FechaCrea] in (select MAX(CE.[FechaCrea]) FROM [Oferentes].[Oferente].[ContactosEntidad] CE WHERE CE.[IdEntidad]  = x.[IdEntidad]))
		,Actividad_CIIU_Ppal= c.[CodigoCiiu]+'' ''+c.[Descripcion]
		,Act_CIIU_Sec = (Select c.[CodigoCiiu]+'' ''+c.[Descripcion] from [PROVEEDOR].[TipoCiiu] c where c.[IdTipoCiiu] = x.[IdTipoCiiuSecundario])
		,UNSPSC = uns.[Codigo]+'' ''+uns.[Descripcion] 
from  [SIA].[PROVEEDOR].[EntidadProvOferente] x
	  INNER  JOIN [Oferentes].[Oferente].[TERCERO] t WITH(NOLOCK) ON x.IdTercero = t.[IDTERCERO]  
	  INNER  JOIN [SIA].[PROVEEDOR].[TipoCiiu] c WITH(NOLOCK) ON c.[IdTipoCiiu] = x.[IdTipoCiiuPrincipal]
	  INNER  JOIN [Oferentes].[Oferente].[TipoPersona] z WITH(NOLOCK) ON z.[IdTipoPersona] = t.[IdTipoPersona] 
	  INNER  JOIN [Global].[TiposDocumentos] b WITH(NOLOCK) ON t.[IDTIPODOCIDENTIFICA] = b.[IdTipoDocumento] 
	  INNER  JOIN [PROVEEDOR].[InfoAdminEntidad] a  WITH(NOLOCK) ON x.[IdEntidad] = a.[IdEntidad]
	  INNER  JOIN [Oferentes].[DIV].[Departamento] d WITH(NOLOCK) ON d.[IdDepartamento] = a.[IdDepartamentoDirComercial]
	  INNER  JOIN [Oferentes].[DIV].[Municipio] e WITH(NOLOCK) ON e.[IdMunicipio] = a.[IdMunicipioDirComercial]
	  INNER  JOIN [PROVEEDOR].[InfoExperienciaEntidad] inf WITH(NOLOCK) ON x.[IdEntidad] = inf.[IdEntidad]
	  INNER  JOIN [PROVEEDOR].[TipoCodigoUNSPSC] uns WITH(NOLOCK) ON inf.[IdTipoCodUNSPSC] = uns.[IdTipoCodUNSPSC]'


-->	Agregando Filtro de Departamento @CodigoDepartamento
	if not exists(select 1 from dbo.StringSplit(@CodigoDepartamento,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoDepartamento,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoDepartamento,1,0) fd on fd.val = d.[CodigoDepartamento] and fd.val != -1' 			
				end
		end

-->	Agregando Filtro de CodigoCIIU @CodigoCiiu
	if not exists(select 1 from dbo.StringSplit(@CodigoCiiu,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoCiiu,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoCiiu,1,0) fc on fc.val = c.[CodigoCiiu] and fc.val != -1' 			
				end
		end

-->	Agregando Filtro de Segmento @CodigoSegmento
	if not exists(select 1 from dbo.StringSplit(@CodigoSegmento,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoSegmento,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoSegmento,1,0) fcs on fcs.val = uns.[CodigoSegmento] and fcs.val != -1' 			
				end
		end


-->	Agregando Filtro de Familia @CodigoFamilia
	if not exists(select 1 from dbo.StringSplit(@CodigoFamilia,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoFamilia,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoFamilia,1,0) fcf on fcf.val = uns.[CodigoFamilia] and fcf.val != -1' 			
				end
		end

-->	Agregando Filtro de Clase @CodigoClase
	if not exists(select 1 from dbo.StringSplit(@CodigoClase,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoClase,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoClase,1,0) fcc on fcc.val = uns.[CodigoClase] and fcc.val != -1' 			
				end
		end

-->	Agregando Filtro de Producto @codigoUNSPSC
	if not exists(select 1 from dbo.StringSplit(@codigoUNSPSC,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@codigoUNSPSC,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@codigoUNSPSC,1,0) fcp on fcp.val = uns.[Codigo] and fcp.val != -1' 			
				end
		end




	set	@query = @query +'	
	order by  t.[NUMEROIDENTIFICACION]'

	--print @query
	EXEC sp_executesql @query,N'@CodigoDepartamento nvarchar, @CodigoCiiu nvarchar, @CodigoSegmento nvarchar, @CodigoFamilia nvarchar, @CodigoClase nvarchar, @codigoUNSPSC nvarchar',
	@CodigoDepartamento, @CodigoCiiu, @CodigoSegmento, @CodigoFamilia, @CodigoClase, @codigoUNSPSC 

end

/****************************************************************************************************************************************************************************/
/****************************************************************************************************************************************************************************/
