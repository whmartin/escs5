﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Modificar]
		@IdInformacionPresupuestalRP INT,	@FechaSolicitudRP DATETIME,	@NumeroRP INT,	@ValorRP INT,	@FechaExpedicionRP DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.InformacionPresupuestalRP SET FechaSolicitudRP = @FechaSolicitudRP, NumeroRP = @NumeroRP, ValorRP = @ValorRP, FechaExpedicionRP = @FechaExpedicionRP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END

