﻿

-- =============================================
-- Author:		Julio Cesar Hernandez
-- Create date:  04/12/2015
-- Description:	Procedimiento almacenado para la generacion del reporte de la cuenta de cobro
-- usp_Sigepcyp_SPCP_Reporte_Obligaciones_X_Producto_Consultar 18
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Reporte_Obligaciones_X_Producto_Consultar]
	@IdCuenta INT = NULL
AS
BEGIN

SELECT  pc.ProductoContractual,
		oc.ObligacionContractual
FROM	[SPCP].[ProductosContractuales] pc
		INNER JOIN [SPCP].[ObligacionesContractuales] oc
			ON pc.IdObligacionContractual = oc.IdObligacionContractual
WHERE	pc.NumeroPago = @IdCuenta

END
