﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  12/07/2013 22:42:55 
-- Description:	Procedimiento almacenado que consulta Proveedores para Validar
-- Modificado: 10/04/2014 Leticia Elizabeth Gonzalez
-- Descripcion: Se agrega la nueva columna Estado Proveedor
-- 
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Validar_Consultar] 
	@IdEstado INT = NULL,
	@IdTipoPersona INT = NULL,
	@IDTIPODOCIDENTIFICA INT= NULL,
	@NUMEROIDENTIFICACION BIGINT = NULL,
	@IdTipoCiiuPrincipal INT = NULL,
	@IdMunicipioDirComercial INT = NULL,
	@IdDepartamentoDirComercial INT = NULL,
	@IdTipoSector INT = NULL,
	@IdTipoRegimenTributario INT = NULL,
	@Proveedor NVARCHAR(256) = NULL	
	
AS
BEGIN


DECLARE @TBL_PROVEEDORES AS TABLE(
IdEntidad	INT
,ConsecutivoInterno	NVARCHAR(50)
,IdTipoPersona INT
,NombreTipoPersona NVARCHAR(50)
,CodDocumento NVARCHAR(10)
,IDTIPODOCIDENTIFICA INT
,NumeroIdentificacion NVARCHAR(50)
,IdTercero INT
,Razonsocial NVARCHAR(500)
,Estado NVARCHAR(50)
,IdEstado INT
,UsuarioCrea NVARCHAR(256)
,IdTipoCiiuPrincipal INT
,ActividadCiiuPrincipal NVARCHAR(MAX)
,IdTipoSectorEntidad NVARCHAR(5)
,SectorEntidad NVARCHAR(50)
,IdTipoRegimenTributario NVARCHAR(5)
,RegimenTributario NVARCHAR(100)
,IdTipoSector NVARCHAR(5)
,NombreMunicipio NVARCHAR(256)
,NombreDepartamento NVARCHAR(256)
,IdMunicipioDirComercial INT
,IdDepartamento INT
,FechaCrea DATE
,EstadoProveedor NVARCHAR(50))

INSERT INTO @TBL_PROVEEDORES
SELECT     EP.IdEntidad, EP.ConsecutivoInterno, T.IdTipoPersona, TP.NombreTipoPersona, TD.CodDocumento, T.IDTIPODOCIDENTIFICA, 
                      T.NUMEROIDENTIFICACION AS NumeroIdentificacion, EP.IdTercero, LTRIM(RTRIM(ISNULL(T.PRIMERNOMBRE, '') + ' ' + ISNULL(T.SEGUNDONOMBRE, '') 
                      + ' ' + ISNULL(T.PRIMERAPELLIDO, '') + ' ' + ISNULL(T.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(T.RAZONSOCIAL, ''))) AS Razonsocial, E.Descripcion AS Estado, 
                      EP.IdEstado, EP.UsuarioCrea, EP.IdTipoCiiuPrincipal, 
                      Proveedor.TipoCiiu.CodigoCiiu + '-' + Proveedor.TipoCiiu.Descripcion AS ActividadCiiuPrincipal, ISNULL(Proveedor.TipoSectorEntidad.IdTipoSectorEntidad,0), 
                      ISNULL(Proveedor.TipoSectorEntidad.Descripcion,'') AS SectorEntidad, ISNULL(Proveedor.TipoRegimenTributario.IdTipoRegimenTributario,0), 
                      ISNULL(Proveedor.TipoRegimenTributario.Descripcion,'') AS RegimenTributario, ISNULL(EP.IdTipoSector,0), DIV.Municipio.NombreMunicipio, DIV.Departamento.NombreDepartamento, 
                      IAE.IdMunicipioDirComercial, DIV.Municipio.IdDepartamento, EP.FechaCrea, EdoProveedor.Descripcion AS EstadoProveedor
FROM Proveedor.EntidadProvOferente AS EP
	INNER JOIN Oferente.TERCERO AS T ON EP.IdTercero = T.IdTercero
	INNER JOIN Proveedor.InfoAdminEntidad AS IAE ON EP.IdEntidad = IAE.IdEntidad
	INNER JOIN Oferente.TipoPersona AS TP ON T.IdTipoPersona = TP.IdTipoPersona
	INNER JOIN Global.TiposDocumentos AS TD ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
	INNER JOIN Proveedor.EstadoDatosBasicos AS E ON EP.IdEstado = E.IdEstadoDatosBasicos
	LEFT JOIN Proveedor.TipoCiiu ON EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu
		AND EP.IdTipoCiiuPrincipal = Proveedor.TipoCiiu.IdTipoCiiu
	LEFT JOIN Proveedor.TipoSectorEntidad ON EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad
		AND EP.IdTipoSector = Proveedor.TipoSectorEntidad.IdTipoSectorEntidad
	LEFT JOIN Proveedor.TipoRegimenTributario ON IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario
		AND IAE.IdTipoRegTrib = Proveedor.TipoRegimenTributario.IdTipoRegimenTributario
	LEFT JOIN DIV.Municipio ON IAE.IdMunicipioDirComercial = DIV.Municipio.IdMunicipio 
	LEFT JOIN DIV.Departamento ON DIV.Municipio.IdDepartamento = DIV.Departamento.IdDepartamento
	INNER JOIN PROVEEDOR.EstadoProveedor EdoProveedor ON EP.IdEstadoProveedor = EdoProveedor.IdEstadoProveedor
WHERE (EP.IdEstadoProveedor = CASE WHEN @IdEstado IS NULL THEN EP.IdEstadoProveedor ELSE @IdEstado END )
AND (T.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN T.IdTipoPersona ELSE @IdTipoPersona END )--1 
AND (T.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN T.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END ) 
AND (T.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN T.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END) 
AND (isnull(EP.IdTipoCiiuPrincipal,0) = CASE WHEN @IdTipoCiiuPrincipal IS NULL THEN isnULL(EP.IdTipoCiiuPrincipal,0) ELSE @IdTipoCiiuPrincipal END) 
AND (isnull(IAE.IdMunicipioDirComercial,0) = CASE WHEN @IdMunicipioDirComercial IS NULL THEN isnull(IAE.IdMunicipioDirComercial,0) ELSE @IdMunicipioDirComercial END) 
AND (isnull(IAE.IdDepartamentoDirComercial,0) = CASE WHEN @IdDepartamentoDirComercial IS NULL THEN isnull(IAE.IdDepartamentoDirComercial,0) ELSE @IdDepartamentoDirComercial END) 
AND (
T.PRIMERNOMBRE LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERNOMBRE ELSE  '%'+ @Proveedor +'%' END
OR T.SEGUNDONOMBRE  LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDONOMBRE ELSE  '%'+ @Proveedor +'%' END
OR T.PRIMERAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.PRIMERAPELLIDO ELSE  '%'+ @Proveedor +'%' END
OR T.SEGUNDOAPELLIDO LIKE CASE WHEN @Proveedor IS NULL THEN T.SEGUNDOAPELLIDO ELSE  '%'+ @Proveedor +'%' END
OR T.RAZONSOCIAL  LIKE CASE WHEN @Proveedor IS NULL THEN T.RAZONSOCIAL ELSE  '%'+ @Proveedor +'%' END
OR ISNULL(T.PRIMERNOMBRE,'')+' '+ISNULL(T.SEGUNDONOMBRE,'')+' '+ISNULL(T.PRIMERAPELLIDO,'')+' '+ISNULL(T.SEGUNDOAPELLIDO,'') LIKE '%'+RTRIM(LTRIM(@Proveedor))+'%'
)



SELECT
IdEntidad
,ConsecutivoInterno
,IdTipoPersona
,NombreTipoPersona
,CodDocumento
,IDTIPODOCIDENTIFICA
,NumeroIdentificacion
,IdTercero
,Razonsocial
,Estado
,IdEstado
,UsuarioCrea
,IdTipoCiiuPrincipal
,ActividadCiiuPrincipal
,CASE WHEN IdTipoSectorEntidad=0 THEN '' ELSE IdTipoSectorEntidad END AS IdTipoSectorEntidad
,SectorEntidad
,CASE WHEN IdTipoRegimenTributario=0 THEN '' ELSE IdTipoRegimenTributario END AS IdTipoRegimenTributario
,RegimenTributario
,CASE WHEN IdTipoSector=0 THEN '' ELSE IdTipoSector END AS IdTipoSector
,NombreMunicipio
,NombreDepartamento
,IdMunicipioDirComercial
,IdDepartamento
,FechaCrea
,EstadoProveedor
 FROM @TBL_PROVEEDORES WHERE
IdTipoSectorEntidad= CASE WHEN @IdTipoSector IS NULL THEN IdTipoSectorEntidad ELSE @IdTipoSector END
AND IdTipoRegimenTributario = CASE WHEN @IdTipoRegimenTributario IS NULL THEN IdTipoRegimenTributario ELSE @IdTipoRegimenTributario END


END


