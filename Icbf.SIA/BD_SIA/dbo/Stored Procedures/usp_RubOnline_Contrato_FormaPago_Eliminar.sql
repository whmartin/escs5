﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Eliminar]
	@IdFormapago INT
AS
BEGIN
	DELETE Contrato.FormaPago WHERE IdFormapago = @IdFormapago
END

