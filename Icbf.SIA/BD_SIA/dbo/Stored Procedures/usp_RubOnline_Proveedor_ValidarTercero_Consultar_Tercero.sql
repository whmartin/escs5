﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarTercero por IdTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar_Tercero]
	@IdTercero INT
AS
BEGIN
 SELECT TOP 1 IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarTercero] 
 WHERE  IdTercero = @IdTercero
 ORDER BY IdValidarTercero DESC
END


