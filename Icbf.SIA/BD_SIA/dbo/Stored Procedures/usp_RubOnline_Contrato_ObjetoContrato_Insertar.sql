﻿


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Insertar]
		@IdObjetoContratoContractual INT OUTPUT, 	
		@ObjetoContractual nvarchar(MAX),	
		@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	if not EXISTS (SELECT 1 FROM Contrato.ObjetoContrato WHERE ObjetoContractual = @ObjetoContractual)
	BEGIN
	INSERT INTO Contrato.ObjetoContrato(ObjetoContractual, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@ObjetoContractual, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObjetoContratoContractual = @@IDENTITY 		
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	END
END


