﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que actualiza un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Modificar]
		@IdVigencia INT,	@AcnoVigencia INT,	@Activo NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.Vigencia SET AcnoVigencia = @AcnoVigencia, Activo = @Activo, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdVigencia = @IdVigencia
END

