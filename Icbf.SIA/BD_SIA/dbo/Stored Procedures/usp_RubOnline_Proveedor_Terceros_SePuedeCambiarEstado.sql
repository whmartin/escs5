﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 28-Feb-2014
-- Description:	De acuerdo a los requerimientos del control de cambios 026
--Aquí se recibe el ID del tercero y se verifica si está asociado a un Proveedor
--y de ser así verifica el estado del proveedor, ya que se es 'VALIDADO'
-- El Tercero no podrá cambiar de estado 'VALIDADO' a--> 'En Validación'
--  retorna 0 .... indica que no se puede validar el tercero
--  retorna 1 .... indica que si se puede validar el tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_SePuedeCambiarEstado] 
	-- Add the parameters for the stored procedure here
	@idTercero INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF EXISTS(
SELECT IdEntidad 
FROM PROVEEDOR.EntidadProvOferente 
where IdTercero=@idTercero)
BEGIN
	IF EXISTS
	(SELECT ESTADO.Descripcion
	FROM PROVEEDOR.EntidadProvOferente PROV
	INNER JOIN Proveedor.EstadoDatosBasicos ESTADO 
	ON PROV.IdEstado=ESTADO.IdEstadoDatosBasicos
	where IdTercero=@idTercero AND ESTADO.Descripcion='VALIDADO')
	BEGIN
		SELECT 0 'sePuedeCambiar'
	END
	ELSE
	BEGIN 
		SELECT 1 'sePuedeCambiar'
	END
END
ELSE
BEGIN
	SELECT 1 'sePuedeCambiar'
END
END



