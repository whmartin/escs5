﻿

-- =============================================
-- Author:		ICBF\brayher.gomez
-- Create date:  13/03/2015 9:36:01 p. m.
-- Description:	Procedimiento almacenado que guarda un nuevo DatosAdministracion
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdministracion_Insertar]
		@IdDatosAdministracion INT OUTPUT, 	
		@Codpci NVARCHAR(255),	
		@CodCompromiso INT,	
		@Fecha DATETIME,	
		@Codcdp INT,	
		@Vigencia INT,	
		@Descripcion NVARCHAR(255),	
		@ValorInicial NVARCHAR(40),	
		@ValorActual NVARCHAR(40),	
		@Saldo NVARCHAR(40),	
		@NumCtaBancaria NVARCHAR(255),	
		@NomEntidadFinanciera NVARCHAR(255),	
		@NomTipoCta NVARCHAR(255),	
		@EstadoCta NVARCHAR(255),	
		@FechaDocSoporte DATETIME,	
		@CodTipoDocSoporte INT,	
		@NomTipoDocSoporte NVARCHAR(255),	
		@NumDocSoporte NVARCHAR(40),	
		@TipoDocumento INT,
		@NumDocIdentidad NVARCHAR(20),	
		@NomTercero NVARCHAR(255),	
		@IdArea INT,	
		@InicioEjecucionContrato DATETIME,	
		@FinalEjecucionContrato DATETIME, 
		@UsuarioCrea NVARCHAR(250),
		@VigenciaDatosAdministracion INT,
		@CorreoInstitucional NVARCHAR(255)
AS
BEGIN
	INSERT INTO SPCP.DatosAdministracion(Codpci, CodCompromiso, Fecha, Codcdp, Vigencia, Descripcion, ValorInicial, ValorActual, Saldo, NumCtaBancaria, NomEntidadFinanciera, NomTipoCta, EstadoCta, FechaDocSoporte, CodTipoDocSoporte, NomTipoDocSoporte, NumDocSoporte, TipoDocumento, NumDocIdentidad, NomTercero, IdArea, InicioEjecucionContrato, FinalEjecucionContrato, VigenciaDatosAdministracion,UsuarioCrea, FechaCrea, CorreoInstitucional)
					  VALUES(@Codpci, @CodCompromiso, @Fecha, @Codcdp, @Vigencia, @Descripcion, @ValorInicial, @ValorActual, @Saldo, @NumCtaBancaria, @NomEntidadFinanciera, @NomTipoCta, @EstadoCta, @FechaDocSoporte, @CodTipoDocSoporte, @NomTipoDocSoporte, @NumDocSoporte, @TipoDocumento, @NumDocIdentidad, @NomTercero, @IdArea, @InicioEjecucionContrato, @FinalEjecucionContrato, @VigenciaDatosAdministracion, @UsuarioCrea, GETDATE(),@CorreoInstitucional)
	SELECT @IdDatosAdministracion = @@IDENTITY 		
END

