﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que consulta un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Consultar]
	@IDSupervisorInterv INT
AS
BEGIN
 SELECT IDSupervisorInterv, IDContratoSupervisa, OrigenTipoSupervisor, 
 IDTipoSupvInterventor, IDTerceroExterno, IDFuncionarioInterno, 
 IDContratoInterventoria, TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, 
 FechaCrea, UsuarioCrea, FechaModifica, UsuarioModifica, IDTerceroInterventoria
 FROM [Contrato].[SupervisorIntervContrato] 
 WHERE  IDSupervisorInterv = @IDSupervisorInterv
END

