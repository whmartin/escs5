﻿
-- =============================================
-- Author:		Zulma Barrantes -- Yo
-- Create date:  25/06/2015 6:00:41 PM
-- Description:	Procedimiento almacenado que guarda el estado de cada módulo por proveedor
-- =============================================

Create PROCEDURE [dbo].[usp_ICBF_Proveedores_GetDataRepProvGeneral2]
			@sector nvarchar(50), 
			@est  nvarchar(50), 
			@fini date, 
			@ffin date,
			@dep  nvarchar(max), 
			@muni  nvarchar(max), 
			@tipopersona  nvarchar(50)
AS

begin 
     
	 begin
	 execute [dbo].[usp_ICBF_Proveedores_EstadoModulo]
	 end

		SELECT 
			   sector = y.[Descripcion] 
			  ,d.[NombreDepartamento]
			  ,e.[NombreMunicipio]
			  ,z.[NombreTipoPersona]
			  ,b.[CodDocumento] TipoID
			  ,Identi=(t.[NUMEROIDENTIFICACION]+'-'+convert(nvarchar(18),t.[DIGITOVERIFICACION])) 
			  , Proveedor = (case when t.[RAZONSOCIAL] is null then 
								  (ISNULL(t.[PRIMERNOMBRE],'')+' '+ISNULL(t.[SEGUNDONOMBRE],'')+' '+ISNULL(t.[PRIMERAPELLIDO],'')+' '+isnull(t.[SEGUNDOAPELLIDO],''))
								  else  [RAZONSOCIAL]
							 end)
			  ,Telefono= case when (Select count(*) from [Oferentes].[Oferente].[TelTerceros] TELT where TELT.Idtercero=t.IdTercero) >0
					          then (select '('+convert(varchar,cast(isnull(TELT.[IndicativoTelefono],'') as int))+') '+
									convert(varchar,cast(isnull(TELT.[NumeroTelefono],'')as bigint))+' E: '+
									convert(varchar,cast(isnull(TELT.[ExtensionTelefono],'')as int))  
					    	   from [Oferentes].[Oferente].[TelTerceros] TELT 
							  where TELT.Idtercero=t.IdTercero
								and TELT.IdTelTercero = (Select max(TELT.IdTelTercero) FROM [Oferentes].[Oferente].[TelTerceros] TELT where TELT.Idtercero=t.IdTercero)
							)
							else 'Sin Información'
							end
			  ,Celular = case when (Select count (*) from [Oferentes].[Oferente].[TelTerceros] TELT  where TELT.Idtercero=t.IdTercero) >0
							  then (Select convert(varchar,cast(isnull(TELT.Movil,'') as bigint)) from [Oferentes].[Oferente].[TelTerceros] TELT  where TELT.Idtercero=t.IdTercero 
									   and TELT.IdTelTercero = (Select max(TELT.IdTelTercero) FROM [Oferentes].[Oferente].[TelTerceros] TELT where TELT.Idtercero=t.IdTercero))
							  end
			  ,valorbas= (
						  case when (SELECT [ConfirmaYAprueba]      
						  FROM [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]
						  where [IdValidarInfoDatosBasicosEntidad] in ( select max(FE2.[IdValidarInfoDatosBasicosEntidad])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] FE2
												where FE2.[FechaCrea] in (select max(v.[FechaCrea])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] v
												where x.[IdEntidad] = v.[IdEntidad]))) = 1 then  'SI'
            
						  when (SELECT [ConfirmaYAprueba]      
						  FROM [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]
						  where [IdValidarInfoDatosBasicosEntidad] in ( select max(FE2.[IdValidarInfoDatosBasicosEntidad])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] FE2
												where FE2.[FechaCrea] in (select max(v.[FechaCrea])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] v
												where x.[IdEntidad] = v.[IdEntidad]))) = 0 then  'NO'                   
						  else   'Sin Validar o En Proceso'
						  END) 
			  ,valorfi=  (
						  case when (SELECT [ConfirmaYAprueba]      
						  FROM [PROVEEDOR].[ValidarInfoFinancieraEntidad]
						  where [IdValidarInfoFinancieraEntidad] in ( select max(FE2.[IdValidarInfoFinancieraEntidad])
												from [PROVEEDOR].[ValidarInfoFinancieraEntidad] FE2
												where FE2.[IdInfoFin] in (select max([IdInfoFin])
												from [PROVEEDOR].[InfoFinancieraEntidad] IX
												 where x.[IdEntidad] = IX.[IdEntidad]))) = 0 then  'SI'
            
						  when (SELECT [ConfirmaYAprueba]     
						  FROM [PROVEEDOR].[ValidarInfoFinancieraEntidad] 
						  where [IdValidarInfoFinancieraEntidad] in ( select max(FE2.[IdValidarInfoFinancieraEntidad])
												from [PROVEEDOR].[ValidarInfoFinancieraEntidad] FE2
												where FE2.[IdInfoFin] in (select max([IdInfoFin])
												from [PROVEEDOR].[InfoFinancieraEntidad] IX
												 where x.[IdEntidad] = IX.[IdEntidad]))) = 1 then  'NO'
						  else 
							 (case when (select count(*)
										 from [PROVEEDOR].[InfoFinancieraEntidad] FE2
										 where FE2.[IdInfoFin] in (select max([IdInfoFin])
										 from [PROVEEDOR].[InfoFinancieraEntidad] IX
										 where x.[IdEntidad] = IX.[IdEntidad])
										 and [EstadoValidacion]  in (1,5) ) > 0 then  'SI'
									else 'Sin Información'          
                 					end)            
						  END) 
			  ,valorexp=(select dbo.Exp_pendiente(x.[IdEntidad])) 
			  ,c.[CodigoCiiu]
			  ,Actividad=c.[Descripcion] 
			  ,t.[CORREOELECTRONICO]
			  ,EstadoDatosBasicos = case when (select count(eme.IdEntidad) from [Proveedor].[EntidadModuloEstado] eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1) > 0
										 then 
											(select evd.descripcion from PROVEEDOR.EstadoValidacionDocumental evd 
												where evd.CodigoEstadoValidacionDocumental = (select eme.IdEstado from [Proveedor].[EntidadModuloEstado] eme 
																								where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1))
										 else 'Sin Información'
										 end
			  ,FechaEstadoDatosBasicos = case when (select count(eme.IdEntidad) from [Proveedor].[EntidadModuloEstado] eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1) > 0
										 then 
										 (select eme.FechaEstado from [Proveedor].[EntidadModuloEstado] eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1)
										 else 'Sin Información'
										 end
			  ,ObservacionesDatosBasicos = (select case when (DDBP.Observaciones) = '' then 'Sin Información'
														else  (DDBP.Observaciones) 
														end
											  from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] DDBP  
											  where DDBP.[IdEntidad] = x.[IdEntidad] 
											  and DDBP.IdValidarInfoDatosBasicosEntidad in(select max(DDBP.IdValidarInfoDatosBasicosEntidad) 
																							 from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] DDBP 
																					        where DDBP.[IdEntidad] = x.[IdEntidad]
																						   )
											)
			  ,EstadoDatosFinancieros = case when (select count(eme.IdEntidad) from [Proveedor].[EntidadModuloEstado] eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3) > 0
										 then
										 (select evd.descripcion from PROVEEDOR.EstadoValidacionDocumental evd 
										  where evd.CodigoEstadoValidacionDocumental = (select eme.IdEstado from [Proveedor].[EntidadModuloEstado] eme 
																						 where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3))
										else 'Sin Información'
										 end
			  ,FechaEstadoDatosFinancieros=  case when (select count(eme.IdEntidad) from [Proveedor].[EntidadModuloEstado] eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3) > 0
										 then
										 (select eme.FechaEstado from [Proveedor].[EntidadModuloEstado] eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3)
											else 'Sin Información'
										 end
			  ,EstadoExperiencia = case when (select count(eme.IdEntidad) from [Proveedor].[EntidadModuloEstado] eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4) > 0
										 then
										 (select evd.descripcion from PROVEEDOR.EstadoValidacionDocumental evd 
										  where evd.CodigoEstadoValidacionDocumental = (select eme.IdEstado from [Proveedor].[EntidadModuloEstado] eme 
																						 where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4))
								   else 'Sin Información'
										 end
			  ,FechaEstadoExperiencias=  case when (select count(eme.IdEntidad) from [Proveedor].[EntidadModuloEstado] eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4) > 0
										 then
										 (select eme.FechaEstado from [Proveedor].[EntidadModuloEstado] eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4)
										else 'Sin Información'
										 end
		  FROM [PROVEEDOR].[EntidadProvOferente] x
				INNER  JOIN [PROVEEDOR].[TipoSectorEntidad] y WITH(NOLOCK) ON x.[IdTipoSector] = y.[IdTipoSectorEntidad] 
				INNER  JOIN [PROVEEDOR].[InfoAdminEntidad] a WITH(NOLOCK) ON a.[IdEntidad] = x.[IdEntidad]
				INNER  JOIN [DIV].[Departamento] d WITH(NOLOCK) ON a.[IdDepartamentoDirComercial]= d.[IdDepartamento]
				INNER  JOIN [DIV].[Municipio] e WITH(NOLOCK) ON a.[IdMunicipioDirComercial] = e.[IdMunicipio]
				INNER  JOIN [Oferentes].[Oferente].[TERCERO] t  WITH(NOLOCK) ON x.IdTercero = t.[IDTERCERO]
				INNER  JOIN [Oferentes].[Oferente].[TipoPersona] z WITH(NOLOCK) ON z.[IdTipoPersona] = t.[IdTipoPersona]
				INNER  JOIN [Global].[TiposDocumentos] b WITH(NOLOCK) ON t.[IDTIPODOCIDENTIFICA] = b.[IdTipoDocumento]
				INNER  JOIN [PROVEEDOR].[TipoCiiu] c WITH(NOLOCK) ON c.[IdTipoCiiu] = x.[IdTipoCiiuPrincipal] 

				INNER  JOIN StringSplit((@sector),1,0) sec on sec.val = y.[IdTipoSectorEntidad]
				INNER  JOIN StringSplit((@est),1,0) es on es.val = x.IdEstadoProveedor

				INNER  JOIN StringSplit((@dep),1,0) depto on depto.val = d.[IdDepartamento]
				INNER  JOIN StringSplit((@muni),1,0) munic on munic.val = e.[IdMunicipio]
				INNER  JOIN StringSplit((@tipopersona),1,0) per on per.val = t.[IdTipoPersona]
		where  	(x.[FechaCrea] >= @fini or @fini is null)
				and (x.[FechaCrea] <= @ffin or @ffin is null)	
		order by  2,3

end

