﻿
-- =============================================
-- Author:		Julio Cesar Hernandez
-- Create date:  04/12/2015
-- Description:	Procedimiento almacenado para la generacion del reporte de la cuenta de cobro
-- usp_Sigepcyp_SPCP_Reporte_CuentasdeCobros_Consultar 18
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Reporte_CuentasdeCobros_Consultar]
	@IdCuenta INT = NULL
AS
BEGIN

DECLARE @Acumulado decimal(30,8),
		@Pagos VARCHAR(10),
		@IdDatosAdministracion INT, 
		@CodCompromiso INT,
		@sValorDelPago decimal(30,8)

SELECT	@IdDatosAdministracion = da.IdDatosAdministracion, 
		@CodCompromiso = da.CodCompromiso
FROM	[SPCP].[DatosAdministracion] da			
		INNER JOIN [SPCP].[CuentasdeCobro] cc
			ON cc.IdDatosAdministracion = da.IdDatosAdministracion
WHERE	cc.IdCuenta = @IdCuenta 
	
SELECT	@Pagos = NumeroDePago
FROM	[SPCP].[PlanDePagos] pp
		INNER JOIN (
					SELECT	pp.[IdDatosAdministracion], pp.[CodCompromiso], max(pp.[OrdenDePago]) AS OrdenDePago
					FROM	[SPCP].[PlanDePagos] pp
					WHERE	[IdDatosAdministracion] = @IdDatosAdministracion
							AND [CodCompromiso] = @CodCompromiso
					GROUP	BY pp.[IdDatosAdministracion], pp.[CodCompromiso]
					) np
			ON pp.[IdDatosAdministracion] = np.[IdDatosAdministracion]
			AND pp.[OrdenDePago] = np.[OrdenDePago]
			AND pp.[CodCompromiso] = np.[CodCompromiso]

SELECT	cc.IdCuenta,
		da.NumDocSoporte,
		da.FechaDocSoporte, 
		r.NombreRegional Regional,
		pp.Rubro,
		da.CodCompromiso,
		da.NumCtaBancaria,
		da.NomEntidadFinanciera,
		da.NomTipoCta, 
		da.NumDocIdentidad,
		da.NomTercero,
		da.InicioEjecucionContrato, 
		da.FinalEjecucionContrato, 
		Case cc.IdRegimen WHEN 1 THEN 'Común' ELSE 'Simplificado' END Regimen, 
		Case cc.Pensionado WHEN 1 THEN 'SI' ELSE 'NO' END Pensionado, 
		Case cc.Declarante WHEN 1 THEN 'SI' ELSE 'NO' END Declarante, 
		Case cc.AporteARL WHEN 0 THEN 'NO' ELSE 'SI' END PagoArl, 
		cc.PlanillaAportes,
		cc.IdNivelARL,
		cc.NumeroFactura,
		cc.AporteSalud,
		cc.AportePension,
		cc.SolidaridadPensional,
		cc.AporteAFC,
		cc.AporteARL,
		cc.NombreSupervisor,
		cc.CargoSupervisor,
		pp.Recurso, 
		pp.NumerodePago, 
		@Pagos Pagos,
		pp.ValorTotalContrato, 
		pp.ValorDelPago,
		dbo.Convi_EnLetras(cc.HonorarioSinIva) As EnLetras,
		cc.HonorarioAcumulado Acumulado
FROM	[SPCP].[CuentasdeCobro] cc
		INNER JOIN [SPCP].[DatosAdministracion] da
			ON cc.IdDatosAdministracion = da.IdDatosAdministracion
		INNER JOIN [SPCP].[PlandePagos] pp
			ON cc.IdDatosAdministracion = pp.IdDatosAdministracion
			AND cc.CodCompromiso = pp.CodCompromiso
			AND cc.NumeroDePago = pp.NumeroDePago
		INNER JOIN [Global].[Areas] a
			ON da.IdArea = a.IdArea
		INNER JOIN [DIV].[Regional] r
			ON a.IdRegional = r.IdRegional
WHERE	cc.IdCuenta = @IdCuenta

END


