﻿
--- =================================================================
-- Author:		<Author,,Zulma Barrantes>
-- Create date: <13 de Agosto del 2015,12:03 p.m.,>
-- Description:	<Permite Modificar  Información de Datos Generales de una Vigencia,>
-- ====================================================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_DatosVigencia_Modificar]
		@IdDatosVigencia INT,	@Codigo NVARCHAR(250),	@Descripcion NVARCHAR(50),	@FechaInicial DATETIME,	@Valor INT,	@Porcentaje INT,	@Normatividad NVARCHAR(256),	@BaseRenta NVARCHAR(250),	@Estado INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.DatosVigencia SET Codigo = @Codigo, Descripcion = @Descripcion, FechaInicial = @FechaInicial, Valor = @Valor, Porcentaje = @Porcentaje, Normatividad = @Normatividad, BaseRenta = @BaseRenta, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDatosVigencia = @IdDatosVigencia
END
