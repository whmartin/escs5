﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Consultar]
	@IdValidarTercero INT
AS
BEGIN
 SELECT IdValidarTercero, IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarTercero] WHERE  IdValidarTercero = @IdValidarTercero
END


