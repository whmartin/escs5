﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedidas_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedidas_Consultar]
	@IdNumeroContrato INT = NULL,
	@NumeroContrato NVARCHAR(50) = NULL,
	@FechaInicioEjecuciónContrato DATETIME = NULL,
	@FechaTerminacionInicialContrato DATETIME = NULL,
	@FechaFinalTerminacionContrato DATETIME = NULL
AS
BEGIN
 SELECT IdNumeroContrato,NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[UnidadMedida] 
 WHERE 
 IdNumeroContrato = CASE WHEN @IdNumeroContrato IS NULL THEN IdNumeroContrato 
 ELSE  @IdNumeroContrato END 
 AND FechaInicioEjecuciónContrato = CASE WHEN @FechaInicioEjecuciónContrato IS NULL THEN FechaInicioEjecuciónContrato ELSE @FechaInicioEjecuciónContrato END 
 AND FechaTerminacionInicialContrato = CASE WHEN @FechaTerminacionInicialContrato IS NULL THEN FechaTerminacionInicialContrato ELSE @FechaTerminacionInicialContrato END 
 AND FechaFinalTerminacionContrato = CASE  WHEN @FechaFinalTerminacionContrato IS NULL THEN FechaFinalTerminacionContrato ELSE @FechaFinalTerminacionContrato END
 AND NumeroContrato = CASE WHEN @NumeroContrato = '' THEN NumeroContrato ELSE  @NumeroContrato END 
END
