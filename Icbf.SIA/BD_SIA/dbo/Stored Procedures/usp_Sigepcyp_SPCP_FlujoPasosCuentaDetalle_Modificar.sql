﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/1/2015 9:26:10 PM
-- Description:	Procedimiento almacenado que actualiza un(a) FlujoPasosCuentaDetalle
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuentaDetalle_Modificar]
		@IdFlujoPasoCuentaDetalle INT,	@IdFlujoPasoCuenta INT,	@IdTipoCuenta INT,	@IdPaso INT,	@Orden INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.FlujoPasosCuentaDetalle 
	SET IdFlujoPasoCuenta = @IdFlujoPasoCuenta, IdTipoCuenta = @IdTipoCuenta, IdPaso = @IdPaso, Orden = @Orden, 
	UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdFlujoPasoCuentaDetalle = @IdFlujoPasoCuentaDetalle
END

