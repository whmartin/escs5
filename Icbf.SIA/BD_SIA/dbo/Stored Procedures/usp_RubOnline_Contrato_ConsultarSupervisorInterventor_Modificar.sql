﻿
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que actualiza un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Modificar]
		@TipoSupervisorInterventor INT,	@NumeroContrato NVARCHAR(50),	@NumeroIdentificacion INT,	@NombreRazonSocialSupervisorInterventor NVARCHAR(50),	@NumeroIdentificacionDirectorInterventoria INT,	@NombreRazonSocialDirectorInterventoria NVARCHAR(50), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ConsultarSupervisorInterventor SET NumeroContrato = @NumeroContrato, NumeroIdentificacion = @NumeroIdentificacion, NombreRazonSocialSupervisorInterventor = @NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria = @NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria = @NombreRazonSocialDirectorInterventoria, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE TipoSupervisorInterventor = @TipoSupervisorInterventor
END


