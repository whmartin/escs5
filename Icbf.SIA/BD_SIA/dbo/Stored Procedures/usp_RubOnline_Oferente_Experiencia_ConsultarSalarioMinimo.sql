﻿
--
	-- =============================================
	-- Author:		IIS APPPOOL\PoolGenerador
	-- Create date:  5/30/2013 8:05:41 AM
	-- Description:	Procedimiento almacenado que consulta un(a) DepMunEjecucion
	-- =============================================
	CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarSalarioMinimo]
		@Ano INT
	AS
	BEGIN
		SELECT [IdSalarioMinimo]
		  ,[Año]
		  ,[Valor]
		  ,[Estado]
		  ,[UsuarioCrea]
		  ,[FechaCrea]
		  ,[UsuarioModifica]
		  ,[FechaModifica]
		FROM [Tercero].[SalarioMinimo]
		WHERE [Tercero].[SalarioMinimo].AÑO = @Ano
	END
