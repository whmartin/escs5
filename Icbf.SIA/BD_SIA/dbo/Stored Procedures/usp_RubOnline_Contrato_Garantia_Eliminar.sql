﻿

-- =============================================
-- Author:		Ares\Henry
-- Create date:  06/08/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Eliminar]
	@IDGarantia INT
AS
BEGIN
	DELETE Contrato.Garantia 
	WHERE IDGarantia = @IDGarantia
END



