﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que consulta un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Consultar]
	@IdTituloObtenido INT
AS
BEGIN
 SELECT IdTituloObtenido, CodigoTituloObtenido, NombreTituloObtenido, Estado, 
		UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[TituloObtenido] 
 WHERE  IdTituloObtenido = @IdTituloObtenido
END

