﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que actualiza un(a) AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AccionPaso_Modificar]
		@IdAccionPaso INT,	@IdPaso INT,	@Estado BIT,	@Descripcion NVARCHAR(160),	@IdAccion INT,	@Aceptar BIT,	@Rechazar BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.AccionPaso
	SET	IdPaso = @IdPaso,
		Estado = @Estado,
		Descripcion = @Descripcion,
		IdAccion = @IdAccion,
		Aceptar = @Aceptar,
		Rechazar = @Rechazar,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE()
	WHERE IdAccionPaso = @IdAccionPaso
END