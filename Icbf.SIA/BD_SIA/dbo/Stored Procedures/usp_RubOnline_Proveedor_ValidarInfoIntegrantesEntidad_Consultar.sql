﻿






-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoIntegrantesEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_Consultar]
	@IdEntidad int = NULL,@Observaciones NVARCHAR(200) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoIntegrantesEntidad, IdEntidad, NroRevision, Observaciones, ISNULL(ConfirmaYAprueba,0) AS ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoIntegrantesEntidad] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
  AND (@Observaciones IS NULL OR Observaciones = @Observaciones)
  AND (@ConfirmaYAprueba IS NULL OR ConfirmaYAprueba = @ConfirmaYAprueba)
 ORDER BY FechaCrea DESC
END







