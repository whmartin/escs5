﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/6/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que actualiza un(a) PlanDePagos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_PlanDePagos_Modificar] -- [usp_Sigepcyp_SPCP_PlanDePagos_Modificar]  1083, 150000, 2,'robinson'
		@IdPlanDePagos INT,	
		--@IdDatosAdministracion INT,	
		--@Codpci NVARCHAR(255),	
		--@Vigencia INT,	
		--@ConsecutivoPACCO INT,	
		--@CodCompromiso INT,	
		--@Fecha DATETIME,	
		--@TipoDocumento INT,	
		--@NumDocIdentidad NVARCHAR(20),	
		--@CodTipoDocSoporte INT,	
		--@NumDocSoporte NVARCHAR(40),	
		--@ValorTotalContrato INT,	
		--@Rubro NVARCHAR(30),	
		--@Recurso INT,	
		--@Fuente NVARCHAR(15),	
		--@NumeroDePago NVARCHAR(15),	
		--@OrdenDePago INT,	
		@ValorDelPago decimal(32,8),	
		@Estado INT, 
		@UsuarioModifica NVARCHAR(250)
AS

declare @ValorDelPagoOriginal as decimal(32,8)
declare @ConseuctivoOriginal as nvarchar(15)
declare @OrdenOriginal as integer


select 
@ValorDelPagoOriginal  = ValorDelPago ,
@ConseuctivoOriginal = [NumeroDePago],
@OrdenOriginal =OrdenDePago
from SPCP.PlanDePagos 
where
IdPlanDePagos = @IdPlanDePagos

if @ValorDelPagoOriginal  = @ValorDelPago
BEGIN
	--solo actualiza el estado
		UPDATE SPCP.PlanDePagos 
		SET  
		Estado = @Estado, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
		where
		IdPlanDePagos = @IdPlanDePagos

END
else
begin
-- actualiza el valor y crea un nuevo registro
declare @NuevoValor as decimal(32,8)
set @NuevoValor = @ValorDelPagoOriginal - @ValorDelPago

declare @nuevoConseuctivo as nvarchar(15)
declare @nuevoOrden as integer

set @nuevoConseuctivo =
	case when CHARINDEX('.', @ConseuctivoOriginal)>0 then 
	left(@ConseuctivoOriginal,CHARINDEX('.', @ConseuctivoOriginal)) +  convert(nvarchar, (convert(int,right(@ConseuctivoOriginal, (len(@ConseuctivoOriginal)-CHARINDEX('.', @ConseuctivoOriginal)) )) +1))
	else 
	@ConseuctivoOriginal + '.1'
	END
set @nuevoOrden = @OrdenOriginal + 5

print '@nuevoConseuctivo'
print @nuevoConseuctivo
print '@nuevoOrden'
print @nuevoOrden 
print '@NuevoValor '
print @NuevoValor 


INSERT INTO [SPCP].[PlanDePagos]
           ([IdDatosAdministracion]
           ,[Codpci]
           ,[Vigencia]
           ,[ConsecutivoPACCO]
           ,[CodCompromiso]
           ,[Fecha]
           ,[TipoDocumento]
           ,[NumDocIdentidad]
           ,[CodTipoDocSoporte]
           ,[NumDocSoporte]
           ,[ValorTotalContrato]
           ,[Rubro]
           ,[Recurso]
           ,[Fuente]
           ,[NumeroDePago]
           ,[OrdenDePago]
           ,[ValorDelPago]
           ,[Estado]
           ,[UsuarioCrea]
           ,[FechaCrea]
           ,[UsuarioModifica]
           ,[FechaModifica])
    
	 select [IdDatosAdministracion]
           ,[Codpci]
           ,[Vigencia]
           ,[ConsecutivoPACCO]
           ,[CodCompromiso]
           ,[Fecha]
           ,[TipoDocumento]
           ,[NumDocIdentidad]
           ,[CodTipoDocSoporte]
           ,[NumDocSoporte]
           ,[ValorTotalContrato]
           ,[Rubro]
           ,[Recurso]
           ,[Fuente]
           ,@nuevoConseuctivo
           ,@nuevoOrden
           ,@NuevoValor
           ,[Estado]
           ,@UsuarioModifica
           ,getdate()
           ,@UsuarioModifica
           ,getdate()
		   from 
			[SPCP].[PlanDePagos] 
			where
			IdPlanDePagos = @IdPlanDePagos 


			UPDATE SPCP.PlanDePagos 
			SET  
			ValorDelPago =  @ValorDelPago,
			Estado = @Estado, 
			UsuarioModifica = @UsuarioModifica, 
			FechaModifica = GETDATE() 
			where
			IdPlanDePagos = @IdPlanDePagos
 
END

select 1





--Si cambia el valor registrar un nuevo plan de pagos
--Validaciones de la modificacion, el valor nuevo no puede ser mayor al valor inicial
--El valor no puede ser negativo
--si no cambia el valor solo cambiar el estado




