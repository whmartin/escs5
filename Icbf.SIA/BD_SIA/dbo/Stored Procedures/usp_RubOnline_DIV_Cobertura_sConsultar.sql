﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que consulta un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_sConsultar]
	@IdCentroZonal INT = NULL,@IdMunicipio INT = NULL,@IdVigencia INT = NULL
AS
BEGIN
 SELECT IdCobertura, IdCentroZonal, IdMunicipio, IdVigencia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Cobertura] WHERE IdCentroZonal = CASE WHEN @IdCentroZonal IS NULL THEN IdCentroZonal ELSE @IdCentroZonal END AND IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END
END

