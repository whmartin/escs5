﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/3/2015 9:34:19 AM
-- Description:	Procedimiento almacenado que guarda un nuevo ProductosContractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_DIV_RegionalCodPCI_Consultar]
	@CodPCI NVARCHAR(255)
AS
BEGIN
 SELECT IdRegional, CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, codPCI FROM [DIV].[Regional] WHERE  codPCI = @CodPCI
END
