﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/15/2015 6:00:03 PM
-- Description:	Procedimiento almacenado que consulta un(a) ObligacionesContractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ObligacionesContractualess_Consultar]
	@IdDatosAdmonObligaciones INT = NULL,@ObligacionContractual NVARCHAR(250) = NULL,@Estado INT = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@IdDatosAdmonObligaciones INT,@ObligacionContractual NVARCHAR(250),@Estado INT'
Set @SqlExec = '
 SELECT IdObligacionContractual, IdDatosAdmonObligaciones, ObligacionContractual, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[ObligacionesContractuales] WHERE 1=1 'If(@IdDatosAdmonObligaciones Is Not Null) Set @SqlExec = @SqlExec + ' And IdDatosAdmonObligaciones = @IdDatosAdmonObligaciones' If(@ObligacionContractual Is Not Null) Set @SqlExec = @SqlExec + ' And ObligacionContractual = @ObligacionContractual' If(@Estado Is Not Null) Set @SqlExec = @SqlExec + ' And Estado = @Estado' 
Exec sp_executesql  @SqlExec, @SqlParametros,@IdDatosAdmonObligaciones,@ObligacionContractual,@Estado
END