﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Consultar]
	@IdValidarInfoFinancieraEntidad INT
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoFinancieraEntidad] WHERE  IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END



