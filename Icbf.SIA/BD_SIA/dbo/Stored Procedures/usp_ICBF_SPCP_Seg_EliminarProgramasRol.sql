﻿
-- =============================================
-- Author:		Oscar Javier  Sosa Parada
-- Create date:  15/11/2012
-- Description:	Procedimiento almacenado que elimina programas permitidos a un rol
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_EliminarProgramasRol]
	@pIdRol INT
AS
BEGIN
	DELETE FROM SEG.ProgramaRol WHERE IdRol = @pIdRol
END

