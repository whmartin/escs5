﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/16/2015 11:22:03 AM
-- Description:	Procedimiento almacenado que consulta un(a) Areas
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_Areass_Consultar]
	@IdRegional INT = NULL,
	@NombreArea NVARCHAR(50) = NULL,
	@CodArea INT = NULL,
	@EstadoArea INT = NULL
AS
BEGIN

SELECT area.IdArea, 
	   area.IdRegional, 
	   regional.NombreRegional,
	   (CASE area.EstadoArea WHEN 0 THEN 'Inactivo' WHEN 1 THEN 'Activo' END ) as NombreEstado,
	   area.NombreArea, 
	   area.CodArea, 
	   area.EstadoArea,
	   area.UsuarioCrea, 
	   area.FechaCrea, 
	   area.UsuarioModifica, 
	   area.FechaModifica 
       FROM [Global].[Areas] area  INNER JOIN DIV.Regional regional on regional.IdRegional = area.IdRegional
	   WHERE regional.IdRegional = CASE WHEN @IdRegional Is Null	 
								   THEN regional.IdRegional 
								   ELSE @IdRegional 
								   END
	   AND 
		     area.NombreArea =  CASE WHEN @NombreArea Is Null	 
								THEN area.NombreArea 
								ELSE @NombreArea 
								END
	   AND 
		     area.CodArea   =  CASE WHEN @CodArea Is Null	 
								THEN area.CodArea 
								ELSE @CodArea 
								END
	   AND 
		     area.EstadoArea   =  CASE WHEN @EstadoArea Is Null	 
								THEN area.EstadoArea 
								ELSE @EstadoArea 
								END

		order by area.NombreArea ASC
END

