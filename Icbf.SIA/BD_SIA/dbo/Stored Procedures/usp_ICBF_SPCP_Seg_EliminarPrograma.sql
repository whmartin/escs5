﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) Programa
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_EliminarPrograma]
	@IdPrograma INT
AS
BEGIN
	DELETE SEG.Programa WHERE IdPrograma = @IdPrograma
END

