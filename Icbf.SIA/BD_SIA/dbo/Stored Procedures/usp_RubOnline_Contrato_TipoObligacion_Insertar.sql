﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Insertar]
		@IdTipoObligacion INT OUTPUT, 	@NombreTipoObligacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoObligacion(NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoObligacion, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoObligacion = @@IDENTITY 		
END

