﻿
--- =================================================================
-- Author:		<Author,,Zulma Barrantes>
-- Create date: <13 de Agosto del 2015,12:03 p.m.,>
-- Description:	<Permite consultar todos  Información de Datos Generales de una Vigencia,>
-- ====================================================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_DatosVigencias_Consultar]
	@Codigo NVARCHAR(250) = NULL,@Descripcion NVARCHAR(50) = NULL,@FechaInicial DATETIME = NULL,@Valor INT = NULL,@Porcentaje INT = NULL,@Normatividad NVARCHAR(256) = NULL,@BaseRenta NVARCHAR(250) = NULL,@Estado INT = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@Codigo NVARCHAR(250),@Descripcion NVARCHAR(50),@FechaInicial DATETIME,@Valor INT,@Porcentaje INT,@Normatividad NVARCHAR(256),@BaseRenta NVARCHAR(250),@Estado INT'
Set @SqlExec = '
 SELECT IdDatosVigencia, Codigo, Descripcion, FechaInicial, Valor, Porcentaje, Normatividad, BaseRenta, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[DatosVigencia] WHERE 1=1 'If(@Codigo Is Not Null) Set @SqlExec = @SqlExec + ' And Codigo = @Codigo' If(@Descripcion Is Not Null) Set @SqlExec = @SqlExec + ' And Descripcion = @Descripcion' If(@FechaInicial Is Not Null) Set @SqlExec = @SqlExec + ' And FechaInicial = @FechaInicial' If(@Valor Is Not Null) Set @SqlExec = @SqlExec + ' And Valor = @Valor' If(@Porcentaje Is Not Null) Set @SqlExec = @SqlExec + ' And Porcentaje = @Porcentaje' If(@Normatividad Is Not Null) Set @SqlExec = @SqlExec + ' And Normatividad = @Normatividad' If(@BaseRenta Is Not Null) Set @SqlExec = @SqlExec + ' And BaseRenta = @BaseRenta' If(@Estado Is Not Null) Set @SqlExec = @SqlExec + ' And Estado = @Estado' 
Exec sp_executesql  @SqlExec, @SqlParametros,@Codigo,@Descripcion,@FechaInicial,@Valor,@Porcentaje,@Normatividad,@BaseRenta,@Estado
END
