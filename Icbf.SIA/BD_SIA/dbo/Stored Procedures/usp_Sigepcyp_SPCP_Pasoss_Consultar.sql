﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/18/2015 1:47:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Pasos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Pasoss_Consultar]
	@Paso NVARCHAR(50) = NULL,@Estado INT = NULL,@Financiera INT = NULL,@IdAccion INT = NULL,@AccionEstado INT = NULL,@Obligatorio INT = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@Paso NVARCHAR(50),@Estado INT,@Financiera INT,@IdAccion INT,@AccionEstado INT,@Obligatorio INT'
Set @SqlExec = '
 SELECT i.IdPaso, i.Paso,
  (CASE i.Estado WHEN 0 THEN ''Inactivo''
			  WHEN 1 THEN ''Activo''
			  END ) as NombreEstado, 
  (CASE i.Financiera WHEN 0 THEN ''NO''
			  WHEN 1 THEN ''SI''
			  END ) as NombreFinanciera, 
  a.Descripcion as NombreAccion, 
 (CASE i.AccionEstado WHEN 0 THEN ''Inactivo''
			  WHEN 1 THEN ''Activo''
			  END ) as NombreAccionEstado, 
  (CASE i.Obligatorio WHEN 0 THEN ''NO''
			  WHEN 1 THEN ''SI''
			  END ) as NombreObligatorio, 
 i.UsuarioCrea, i.FechaCrea, i.UsuarioModifica, i.FechaModifica 
 FROM [SPCP].[Pasos] i
 inner join SPCP.Acciones a on i.IdAccion=a.IdAcciones
 WHERE 1=1 '
 If(@Paso Is Not Null) Set @SqlExec = @SqlExec + ' And Paso = @Paso' 
 If(@Estado Is Not Null) Set @SqlExec = @SqlExec + ' And Estado = @Estado' 
 If(@Financiera Is Not Null) Set @SqlExec = @SqlExec + ' And Financiera = @Financiera' 
 If(@IdAccion Is Not Null) Set @SqlExec = @SqlExec + ' And IdAccion = @IdAccion' 
 If(@AccionEstado Is Not Null) Set @SqlExec = @SqlExec + ' And AccionEstado = @AccionEstado' 
 If(@Obligatorio Is Not Null) Set @SqlExec = @SqlExec + ' And Obligatorio = @Obligatorio' 

 set	@SqlExec = @SqlExec +'	
	order by  i.Paso'

Exec sp_executesql  @SqlExec, @SqlParametros,@Paso,@Estado,@Financiera,@IdAccion,@AccionEstado,@Obligatorio
END
