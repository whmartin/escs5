﻿

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_ConsultarAll]
	AS
BEGIN
 SELECT IdTipoSectorEntidad,Descripcion FROM [Proveedor].[TipoSectorEntidad] WHERE  Estado = 1
 ORDER BY Descripcion
END



