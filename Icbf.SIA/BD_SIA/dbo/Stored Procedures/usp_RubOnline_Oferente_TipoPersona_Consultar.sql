﻿
-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que consulta persona Oferente
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TipoPersona_Consultar]
	@IdTipoPersona INT
AS
BEGIN
 SELECT IdTipoPersona, 
		CodigoTipoPersona, 
		NombreTipoPersona, 
		Estado, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
		FROM [Tercero].[TipoPersona] WHERE  IdTipoPersona = @IdTipoPersona
END

