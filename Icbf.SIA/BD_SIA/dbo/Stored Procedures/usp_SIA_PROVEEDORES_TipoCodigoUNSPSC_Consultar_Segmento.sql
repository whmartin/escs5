﻿-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  02/26/2014 1:56:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) Segmento
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Segmento]

AS
BEGIN
 SELECT DISTINCT(CodigoSegmento),
		NombreSegmento
  FROM	PROVEEDOR.TipoCodigoUNSPSC
  WHERE CodigoSegmento IS NOT NULL
END