﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/27/2015 10:16:23 AM
-- Description:	Procedimiento almacenado que guarda un nuevo AsignarCodigoSIIFcc
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AsignarCodigoSIIFcc_Insertar]
	@XMLDataActualzad XML,
	@UsuarioModifica NVARCHAR(250),
	@RolTransaccion nvarchar(250)
AS
BEGIN

	DEclare @CantReg int,
			@Conteo int,
			@idCtaEnviar int

	Declare @TabCuentas Table(Id int not null identity(1,1),
									IdCuentaCobro	int)	


	 BEGIN TRY
        BEGIN TRANSACTION
		
				UPDATE temcc set temcc.CodigoCuentaPorPagarSIIF = CodigoSiifCxP.value('@CodigosSiif','Nvarchar(80)')	
												,UsuarioModifica = @UsuarioModifica		 
				FROM @XMLDataActualzad.nodes('/DataCodigoAsiganarCc/CodigosSiif') AS TAp(CodigoSiifCxP)
				,SPCP.CuentasdeCobro temcc
				WHERE temcc.IdCuenta = CodigoSiifCxP.value('@IDCuentaCobro', 'INT')

				--- Despues de guardar el codigo SIIF cambiamos las cuentas de cobro al siguiente paso
				INSERT INTO @TabCuentas
				SELECT CodigoSiifCxP2.value('@IDCuentaCobro', 'INT')
				FROM @XMLDataActualzad.nodes('/DataCodigoAsiganarCc/CodigosSiif') AS TAp(CodigoSiifCxP2)

				SELECT @CantReg = COUNT(Id)
				from @TabCuentas
				
				set @Conteo = 1
				WHILE(@Conteo <= @CantReg)
				BEGIN
					SELECT @idCtaEnviar = IdCuentaCobro
					FROM @TabCuentas
					WHERE Id = @Conteo

					--movemos de estado la cuenta de cobro
					exec usp_Sigepcyp_SPCP_GestionPagos_ActualizarCuentaCobro @idCtaEnviar,1,'',@UsuarioModifica,@RolTransaccion


					SET @Conteo = @Conteo +1
				END
			
			

		COMMIT TRANSACTION
    END TRY        
    BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
        SELECT  @ErrorMessage = ERROR_MESSAGE()
        IF XACT_STATE() <> 0 
            ROLLBACK TRANSACTION
        RAISERROR (@ErrorMessage, 16, 1)         
    END CATCH

END
