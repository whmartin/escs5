﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reportes_Consultar]
	 @NombreReporte NVARCHAR(512) = NULL
	,@Servidor NVARCHAR(512) = NULL
AS
BEGIN
 SELECT IdReporte
       ,NombreReporte
       ,Descripcion
       ,Servidor
       ,Carpeta
       ,NombreArchivo
       ,UsuarioCrea
       ,FechaCrea
       ,UsuarioModifica
       ,FechaModifica 
 FROM [Global].[Reporte] (NoLock)
 WHERE (NombreReporte = @NombreReporte Or @NombreReporte Is Null)
       AND (Servidor = @Servidor Or @Servidor Is Null)
END
