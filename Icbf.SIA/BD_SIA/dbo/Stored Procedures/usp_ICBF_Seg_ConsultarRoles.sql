﻿-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado para la consulta de roles
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarRoles]
	@Nombre NVARCHAR(20)
	, @Estado BIT
AS
BEGIN
	IF @Estado IS NULL
	BEGIN
		SELECT [IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[Estado]
		FROM SEG.Rol
		WHERE [Nombre] LIKE ('%' + @Nombre + '%')
	END
	ELSE
	BEGIN
		SELECT 
			[IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[Estado]
		FROM SEG.Rol
		WHERE [Nombre] LIKE ('%' + @Nombre + '%')
			AND [Estado] = @Estado
	END
END
