﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que consulta un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList]
	@IdModalidad INT 
   ,@FechaRegistro DATETIME 
   ,@Estado NVARCHAR(1) = NULL
   ,@Usuario NVARCHAR(250) = NULL
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , [Archivo].[IdFormatoArchivo]
      , FechaRegistro
      , NombreArchivo
      , [Archivo].[Estado]
      , ResumenCarga
      , [Archivo].[UsuarioCrea]
      , [Archivo].[FechaCrea]
      , [Archivo].[UsuarioModifica]
      , [Archivo].[FechaModifica]
      , [TipoEstructura].[NombreEstructura]
 FROM [Estructura].[Archivo] 
	Inner Join [Estructura].[FormatoArchivo] on [Archivo].[IdFormatoArchivo]=[FormatoArchivo].[IdFormatoArchivo]
    Inner Join [Estructura].[TipoEstructura] on [FormatoArchivo].[IdTipoEstructura]=[TipoEstructura].[IdTipoEstructura]
 WHERE  IdModalidad=@IdModalidad 
    And [Archivo].[Estado] = CASE WHEN @Estado = 'N' THEN [Archivo].[Estado] ELSE @Estado END
    And DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
    --And @Usuario = CASE WHEN Len(@Usuario) = 0  THEN @Usuario ELSE [Archivo].[UsuarioCrea] END
    And [Archivo].[UsuarioCrea] = CASE WHEN @Usuario IS NULL THEN [Archivo].[UsuarioCrea] ELSE @Usuario END
END

