﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 1:25:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributario_Insertar]
		@IdTipoRegimenTributario INT OUTPUT, 	@CodigoRegimenTributario NVARCHAR(128),	@Descripcion NVARCHAR(128),	@IdTipoPersona INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoRegimenTributario(CodigoRegimenTributario, Descripcion, IdTipoPersona, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegimenTributario, @Descripcion, @IdTipoPersona, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoRegimenTributario = SCOPE_IDENTITY() 		
END


