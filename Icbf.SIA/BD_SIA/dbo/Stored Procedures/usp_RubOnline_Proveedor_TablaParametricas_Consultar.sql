﻿


create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametricas_Consultar]
	@CodigoTablaParametrica NVARCHAR(128) = NULL,
	@NombreTablaParametrica NVARCHAR(128) = NULL,
	@Estado					BIT = NULL
AS
BEGIN
	SELECT
		IdTablaParametrica,
		CodigoTablaParametrica,
		NombreTablaParametrica,
		Url,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[TablaParametrica]
	WHERE CodigoTablaParametrica = CASE WHEN @CodigoTablaParametrica IS NULL 
										THEN CodigoTablaParametrica 
										ELSE @CodigoTablaParametrica
								   END
	AND NombreTablaParametrica LIKE '%' + CASE WHEN @NombreTablaParametrica IS NULL 
											   THEN NombreTablaParametrica 
											   ELSE @NombreTablaParametrica
										  END + '%'
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado
		END
	ORDER BY NombreTablaParametrica
END--FIN PP

