﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Consultar]
	@IdTipodeentidadPublica INT
AS
BEGIN
 SELECT IdTipodeentidadPublica, CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeentidadPublica] WHERE  IdTipodeentidadPublica = @IdTipodeentidadPublica
END

