﻿
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  23/06/2014 10:55 AM
-- Description:	Procedimiento almacenado que valida el numero de integrantes vs el numero de registrados
-- Modificado: Juan Carlos Valverde Sámano
-- Fecha: 08/07/2014
-- Descripción: El procedimiento no estaba tomando en cuenta los casos en que aun no se tenía nungun integrant4e registrado.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_ValidaIntegrantes]
 @IdEntidad INT
 AS
 BEGIN
 
 
IF EXISTS (SELECT IdIntegrante FROM PROVEEDOR.Integrantes
			WHERE IdEntidad=@IdEntidad)
			BEGIN
				SELECT	COUNT(Integrantes.IdIntegrante) AS Integrantes,
						Entidad.NumIntegrantes,
						Tercero.IdTipoPersona,
						SUM(Integrantes.PorcentajeParticipacion) AS PorcentajeParticipacion
				  FROM	PROVEEDOR.EntidadProvOferente Entidad
				  INNER		JOIN	OFERENTE.TERCERO Tercero
				  ON	Entidad.IdTercero = Tercero.IDTERCERO
				  INNER JOIN	PROVEEDOR.Integrantes Integrantes
				  ON	Integrantes.IdEntidad = Entidad.IdEntidad
				  WHERE Integrantes.IdEntidad = @IdEntidad
				  GROUP BY Integrantes.IdEntidad, Entidad.NumIntegrantes, Tercero.IdTipoPersona
			END
			ELSE
			BEGIN
				   SELECT	0 AS Integrantes,
				   Entidad.NumIntegrantes,
				   Tercero.IdTipoPersona,
					0 AS PorcentajeParticipacion
				  FROM	PROVEEDOR.EntidadProvOferente Entidad
				  INNER		JOIN	OFERENTE.TERCERO Tercero
				  ON	Entidad.IdTercero = Tercero.IDTERCERO
				  WHERE Entidad.IdEntidad=@IdEntidad
			END
END
