﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 02/04/2014
-- Description:	Modifica la Razón Social Para un Usuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Usuario_ModificarRazonSocial]
@razonSocial NVARCHAR(150), @providerLey NVARCHAR(125)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE SEG.Usuario
	SET RazonSocial=@razonSocial
	WHERE providerKey=@providerLey
END

