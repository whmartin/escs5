﻿
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que guarda programa permitido a un rol
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_InsertarProgramaRol]
		@pIdRol INT
		, @pIdPrograma INT
		, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.ProgramaRol([IdRol], [IdPrograma], [UsuarioCreacion], FechaCreacion)
					  VALUES(@pIdRol, @pIdPrograma, @UsuarioCreacion, GETDATE())
END

