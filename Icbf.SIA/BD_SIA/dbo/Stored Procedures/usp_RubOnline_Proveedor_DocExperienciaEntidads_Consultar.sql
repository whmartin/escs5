﻿-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidads_Consultar]
	@IdExpEntidad INT = NULL,
	@Descripcion NVARCHAR(128) = NULL,
	@LinkDocumento NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdExpEntidad, D.IdTipoDocumento, D.Descripcion, LinkDocumento, TD.Descripcion AS DescripcionDocumento,
	D.UsuarioCrea, D.FechaCrea, D.UsuarioModifica, D.FechaModifica 
 FROM [Proveedor].[DocExperienciaEntidad] D
 INNER JOIN [Proveedor].[TipoDocumento] TD ON D.IdTipoDocumento=TD.IdTipoDocumento
 WHERE IdExpEntidad = CASE WHEN @IdExpEntidad IS NULL THEN IdExpEntidad ELSE @IdExpEntidad END 
 AND D.Descripcion = CASE WHEN @Descripcion IS NULL THEN D.Descripcion ELSE @Descripcion END 
 AND LinkDocumento = CASE WHEN @LinkDocumento IS NULL THEN LinkDocumento ELSE @LinkDocumento END
END

