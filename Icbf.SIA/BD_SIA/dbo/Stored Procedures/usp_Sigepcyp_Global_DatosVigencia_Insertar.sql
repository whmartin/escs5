﻿
--- =================================================================
-- Author:		<Author,,Zulma Barrantes>
-- Create date: <13 de Agosto del 2015,12:03 p.m.,>
-- Description:	<Permite Insertar  Información de Datos Generales de una Vigencia,>
-- ====================================================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_DatosVigencia_Insertar]
		@IdDatosVigencia INT OUTPUT, 	@Codigo NVARCHAR(250),	@Descripcion NVARCHAR(50),	@FechaInicial DATETIME,	@Valor INT,	@Porcentaje INT,	@Normatividad NVARCHAR(256),	@BaseRenta NVARCHAR(250),	@Estado INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.DatosVigencia(Codigo, Descripcion, FechaInicial, Valor, Porcentaje, Normatividad, BaseRenta, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Codigo, @Descripcion, @FechaInicial, @Valor, @Porcentaje, @Normatividad, @BaseRenta, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdDatosVigencia = @@IDENTITY 		
END
