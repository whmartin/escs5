﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Insertar]
		@IdContratoLugarEjecucion INT, 	
		@IDDepartamento INT,	
		@IDMunicipio INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.LugarEjecucion(IdContratoLugarEjecucion,IDDepartamento, IDMunicipio, UsuarioCrea, FechaCrea)
	VALUES(@IdContratoLugarEjecucion,@IDDepartamento, @IDMunicipio, @UsuarioCrea, GETDATE())
END

