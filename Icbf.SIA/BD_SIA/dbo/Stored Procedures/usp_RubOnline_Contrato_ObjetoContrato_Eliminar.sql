﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Eliminar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Eliminar]
	@IdObjetoContratoContractual INT
AS
BEGIN
	DELETE Contrato.ObjetoContrato WHERE IdObjetoContratoContractual = @IdObjetoContratoContractual
END

