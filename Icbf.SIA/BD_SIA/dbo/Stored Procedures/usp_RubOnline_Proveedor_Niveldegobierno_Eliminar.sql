﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que elimina un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobierno_Eliminar]
	@IdNiveldegobierno INT
AS
BEGIN
	DELETE Proveedor.Niveldegobierno WHERE IdNiveldegobierno = @IdNiveldegobierno
END

