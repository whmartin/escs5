﻿

CREATE procedure [dbo].[usp_ICBF_Proveedores_GetDataFamilia]
@CodigoSegmento nvarchar(max)
as
begin

declare @query as nvarchar(max) = '
	select	-1 as [CodigoFamilia],''  Seleccionar Todo'' as [NombreFamilia]	
union
	select	distinct uns.[CodigoFamilia],uns.[NombreFamilia]
	from	[PROVEEDOR].[TipoCodigoUNSPSC] uns'

-->	Agregando Filtro de Segmento @CodigoSegmento
	if not exists(select 1 from dbo.StringSplit(@CodigoSegmento,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoSegmento,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN StringSplit((@CodigoSegmento),1,0) fcs on fcs.val = uns.[CodigoSegmento] and fcs.val != -1' 			
				end
		end
	set @query = @query+'	
	where	uns.[CodigoFamilia] is not null
	order by 2 asc'

--	print @query
	EXEC sp_executesql @query,N'@CodigoSegmento nvarchar(max)',@CodigoSegmento
end


/**********************************************************************************************************************************************************************/

