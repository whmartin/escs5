﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Modificar]
		@IdSucursal INT,	@IdEntidad INT,	@Nombre VARCHAR(256), @Indicativo INT,	@Telefono INT,	@Extension NUMERIC(10) = NULL,	@Celular  NUMERIC(10),	@Correo NVARCHAR(256),	@Estado INT, @IdZona INT,	@Departamento INT,	@Municipio INT,	@Direccion NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.Sucursal SET IdEntidad = @IdEntidad, Nombre = @Nombre, Indicativo = @Indicativo, Telefono = @Telefono, Extension = @Extension, Celular = @Celular, Correo = @Correo, Estado = @Estado, IdZona = @IdZona, Departamento = @Departamento, Municipio = @Municipio, Direccion = @Direccion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdSucursal = @IdSucursal
END



