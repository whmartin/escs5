﻿-- =============================================
-- Author:		ICBF\EquipoFinanciera
-- Create date:  04/03/2015 8:16:01
-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_DIV_Regional_Modificar]
		@IdRegional INT,	@CodigoRegional NVARCHAR(128),	@NombreRegional NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE DIV.Regional SET CodigoRegional = @CodigoRegional, NombreRegional = @NombreRegional, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRegional = @IdRegional
END
