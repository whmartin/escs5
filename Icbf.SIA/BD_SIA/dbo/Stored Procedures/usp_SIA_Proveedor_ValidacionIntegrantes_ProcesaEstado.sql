﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 05/AGO/2014
-- Description: Determina de acuerdo al número de integrantes definidos para una entidad,
-- y los ya asociados a este; que estado de validación debe asignarse al módulo Integrantes.
-- o Elimina el registro de Validación del módulo si es que los integrantes no han sido asociados
-- en su totalidad.
-- Modificado Por: Juan Carlos Valverde Sámano
-- Fecha: 20/AGO/2014
-- Descripción: De acuerdo a un análisis realizado con Sandra.Mendez se ha hecho el cambio para que
-- la información del módulo integrantes pueda ser validada aún cuando no se ha completado en su totalidad
-- la asociación de Integrantes; en la conclusión de que eso es responsabilidad del usuario.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_ValidacionIntegrantes_ProcesaEstado]
@IdEntidad INT
AS
BEGIN
	DECLARE 
	@IntegrantesAsocidos INT

	DECLARE @IdEstadoRegistrado INT =(
	SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='REGISTRADO')
	DECLARE @IdEstadoPorValidar INT = (
	SELECT IdEstadoIntegrantes FROM PROVEEDOR.EstadoIntegrantes
	WHERE Descripcion='POR VALIDAR')

	

	SET @IntegrantesAsocidos=(SELECT COUNT(IdIntegrante) FROM PROVEEDOR.Integrantes 
	WHERE IdEntidad=@IdEntidad)
	IF(@IntegrantesAsocidos =0)
	BEGIN
		DELETE FROM PROVEEDOR.ValidacionIntegrantesEntidad
		WHERE IdEntidad=@IdEntidad
		UPDATE PROVEEDOR.ValidarInfoIntegrantesEntidad
		SET Activo=0 WHERE IdEntidad=@IdEntidad
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT IdValidacionIntegrantesEntidad FROM PROVEEDOR.ValidacionIntegrantesEntidad
		WHERE IdEntidad=@IdEntidad)
		BEGIN
			UPDATE PROVEEDOR.ValidacionIntegrantesEntidad
			SET IdEstadoValidacionIntegrantes=@IdEstadoPorValidar,
			FechaModifica = GETDATE(),
			UsuarioModifica= 'TR_ProveedorIntegrantes'
			WHERE IdEntidad=@IdEntidad
		END
		ELSE
		BEGIN
			DECLARE @CheckNroRevision INT=1
			IF EXISTS (SELECT NroRevision FROM 
			PROVEEDOR.ValidarInfoIntegrantesEntidad
			WHERE IdEntidad=@IdEntidad)
			BEGIN		
				SET @CheckNroRevision=((SELECT MAX(NroRevision) FROM 
				PROVEEDOR.ValidarInfoIntegrantesEntidad
				WHERE IdEntidad=@IdEntidad) +1)
			END
			INSERT INTO PROVEEDOR.ValidacionIntegrantesEntidad
			(IdEntidad, IdEstadoValidacionIntegrantes, NroRevision, Finalizado, FechaCrea, UsuarioCrea, CorreoEnviado)
			VALUES(@IdEntidad, @IdEstadoRegistrado, @CheckNroRevision, 0, GETDATE(), 'TR_ProveedorIntegrantes',0)
		END
	END
END


