﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_Sigepcyp_SPCP_HistoriocoCuentasCobro_Consultar
	@IdPlanilla int,
	@UltimaFecha bit
AS
BEGIN
	
	SELECT top 1 IDHistoricoEstadoCuentasDeCobro,IDCuentaCobro,Aprobado,Descripcion
	FROM SPCP.HistoricoEstadoCuentasDeCobro
	WHERE IDCuentaCobro = @IdPlanilla
	ORDER by FechaCrea DESC

END
