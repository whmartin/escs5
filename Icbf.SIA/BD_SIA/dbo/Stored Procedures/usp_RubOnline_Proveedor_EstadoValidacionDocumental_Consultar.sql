﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Consultar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE  IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END

