﻿
--- =================================================================
-- Author:      Grupo de Apoyo - Desarrollo Caja Menor
-- Create date:  13/11/2015 9:11:50 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoIncremento
-- =============================================
					   
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ConsultaCuentas_Consultar]
	@NumDocIdentidad NVARCHAR(20) = NULL, @IdRegional int = null, @IdCuentaCobro int = null, @IdArea int = null,
	@NumeroContrato NVARCHAR(20) = NULL, @NomTercero NVARCHAR(100) = NULL
	
AS
BEGIN

select  
	   hcc.IDCuentaCobro
	   ,r.IdRegional
	   ,r.NombreRegional as regional
	   ,hcc.Roltransaccion
	   ,hcc.UsuarioCrea as UsuarioAprobo
	   ,hcc.FechaCrea as FechaAprobo
	   ,da.NumDocIdentidad
	   ,da.NumDocSoporte as NumeroContrato
	   ,da.NomTercero
	   ,da.IdArea	
	   ,hcc.Descripcion
 from SPCP.HistoricoEstadoCuentasDeCobro hcc
 INNER JOIN SPCP.CuentasdeCobro cc on cc.IdCuenta = hcc.IDCuentaCobro
 INNER JOIN  SPCP.DatosAdministracion da on da.IdDatosAdministracion = cc.IdDatosAdministracion
 INNER join DIV.Regional r on da.Codpci = r.codPCI
 where 1=1
   and da.NumDocIdentidad = isnull(@NumDocIdentidad,da.NumDocIdentidad)
   and r.IdRegional = isnull(@IdRegional,r.IdRegional)	
   and hcc.IDCuentaCobro = isnull(@IDCuentaCobro,hcc.IDCuentaCobro)
   and da.IdArea = isnull(@IdArea,da.IdArea)
   and da.NumDocSoporte = isnull(@NumeroContrato,da.NumDocSoporte)
   AND ((@NomTercero IS NULL) OR (da.NomTercero LIKE '%'+ @NomTercero + '%'))

end 


