﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Eliminar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
	DELETE Contrato.LugarEjecucionContrato WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

