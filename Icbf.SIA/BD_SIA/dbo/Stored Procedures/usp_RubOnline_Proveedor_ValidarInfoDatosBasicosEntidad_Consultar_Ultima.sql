﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_Ultima] (@IdEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoDatosBasicosEntidad,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.EntidadProvOferente e
		inner join Proveedor.ValidarInfoDatosBasicosEntidad v
		on v.IdEntidad = e.IdEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end

