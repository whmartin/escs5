﻿
/****** Object:  StoredProcedure [dbo].[usp_Sigepcyp_SPCP_GestionPagos_ActualizarCuentaCobro]    Script Date: 15/12/2015 10:14:04 p. m. ******/
--DROP PROCEDURE [dbo].[usp_Sigepcyp_SPCP_GestionPagos_ActualizarCuentaCobro]
--GO

-- =============================================
-- Author:		Jorge Vizcaino
-- Create date:  2015-08-05
-- Description:	Procedimiento almacenado que actualiza el estado de la cuenta de cobro en la gestion de pagos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_GestionPagos_ActualizarCuentaCobro]
	@idCuentaCobro int = NULL,
	@Estado bit = NULL,	
	@DEscripcion nvarchar(800) = NULL,
	@IdUsuario nvarchar(250) = NULL,
	@RolTransaccion nvarchar(250) = NULL,
	@PasoDirecto int = NULL,
	@IdsPlanillas nvarchar(800) = NULL,
	@IDTipoCuenta int = NULL
AS
BEGIN
	
	Declare @IDTipoCuentaCuentaCobro int,
			@EsFinanciera bit,
			@IdPasoActulizarCuentaPagada int,
			@IdTipoCuentaGen int,
			@EstadoPrimerPaso int,
			@IDTipoCuentaGenerico int,
			@IDUltimoPasoGenerico int,
			@IDPasoCXP int,
			@IDPasoEncargadoArea int

			

	Declare @TabIdCuentaCobro Table(Id int not null identity(1,1),
									IdCuentaCobro	int,
									IdPasoActual	int,
									IdTipoCuenta	int,
									IdPasoSiguiente int,
									IdPasoAnterior	int,
									IdPasoHistorio	int)	

	
	 BEGIN TRY
        BEGIN TRANSACTION
		
			--Buscamos las cuentas de cobros a actualizar 
			IF(@IdsPlanillas IS NOT NULL)--Si seleccionaron por planillas
			BEGIN
				INSERT INTO @TabIdCuentaCobro (IdCuentaCobro,IdPasoActual,IdTipoCuenta)
				SELECT IdCuenta,IdEstadoPaso,IdTipoCuenta
				FROM dbo.PlanillasCuentasPorRol(@RolTransaccion)		
				WHERE IDPlanilla in (select Data from dbo.Split(@IdsPlanillas,';'))
			END
			ELSE
			BEGIN
				INSERT INTO @TabIdCuentaCobro  (IdCuentaCobro,IdPasoActual,IdTipoCuenta)
				Select IdCuenta,IdEstadoPaso,IdTipoCuenta
				from SPCP.CuentasDeCobro
				where IdCuenta = @idCuentaCobro
			END


			--Si nos envian el tipo de cuenta y la cuenta de cobro esta en un paso de financiera cambiamos el tipo de cuenta
			IF(@IDTipoCuenta IS NOT NULL)
			BEGIN
				UPDATE SPCP.CuentasdeCobro set IdTipoCuenta = @IDTipoCuenta, FechaModifica = GETDATE()
				Where IdCuenta in (SELECT tcc.IdCuentaCobro 
									FROM @TabIdCuentaCobro tcc
									--INNER JOIN SPCP.FlujoPasosCuenta fpc
									--ON tcc.IdTipoCuenta = fpc.IdTipoCuenta
									--INNER JOIN SPCP.Pasos tp
									--ON fpc.IdPaso = tp.IdPaso
									--WHERE tp.Financiera = 1 
									)
				
			END

			-- Buscamos el paso anterior y siguiente de cada cuenta de cobro
			update @TabIdCuentaCobro set IdPasoSiguiente = dbo.ConsultarPasoFlujo(1,IdPasoActual)
								,IdPasoAnterior = dbo.ConsultarPasoFlujo(0,IdPasoActual)

			--Si aprobaron las cuentas
			If(@Estado = 1)
			Begin
				
				--Buscado el esatdo que tiene la accion para actualizar la cuenta de cobro a pagada
				SELECT	@IdPasoActulizarCuentaPagada = ap.IdPaso
				FROM SPCP.AccionPaso ap
				INNER JOIN SPCP.Acciones a
				ON ap.IdAccion = a.IdAcciones
				WHERE a.CodAccion ='AccP'


				UPDATE Epp SET Epp.Estado = 3 --Actualizamos el plan de pago a pagada 
				from SPCP.CuentasdeCobro Ecc
				INNER JOIN SPCP.PlanDePagos Epp
				ON Ecc.IdPlanDePagos = Epp.IdPlanDePagos
				WHERE Ecc.IdCuenta IN (SELECT Etemcc.IdCuentaCobro FROM @TabIdCuentaCobro Etemcc)
				AND Ecc.IdEstadoPaso = @IdPasoActulizarCuentaPagada


				IF EXISTS(select 1 from @TabIdCuentaCobro WHERE IdPasoSiguiente IS NULL)
				BEGIN	
					-- si se cambia le tipo de cuenta se controla que actulice el primer estado de ese tipo de cuenta	
					IF(@IDTipoCuenta IS NOT NULL)				
					BEGIN						
						SELECT top 1 @EstadoPrimerPaso = IdFlujoPasoCuenta
						FROM SPCP.FlujoPasosCuenta
						WHERE IdTipoCuenta = @IDTipoCuenta
						AND estado = 1
						ORDER BY Orden asc

						IF(@EstadoPrimerPaso IS NOT NULL)
						BEGIN
							update @TabIdCuentaCobro set IdPasoSiguiente = @EstadoPrimerPaso
							WHERE IdPasoSiguiente IS NULL
							and IdCuentaCobro in (SELECT tcc.IdCuentaCobro 
												FROM @TabIdCuentaCobro tcc
												INNER JOIN SPCP.FlujoPasosCuenta fpc
												ON tcc.IdTipoCuenta = fpc.IdTipoCuenta
												INNER JOIN SPCP.Pasos tp
												ON fpc.IdPaso = tp.IdPaso
												WHERE tp.Financiera = 1 )
						END
						ELSE
						BEGIN
							RAISERROR ('Uno de los Flujo no tiene más pasos configurados', 16, 1)
						END
					END
					ELSE
					BEGIN
						RAISERROR ('Uno de los Flujo no tiene más pasos configurados', 16, 1)
					END
				END

				UPDATE taCuenta set taCuenta.IdEstadoPaso = tempEst.IdPasoSiguiente, taCuenta.Devuelta = 0, taCuenta.FechaModifica = GETDATE()
				from  SPCP.CuentasDeCobro taCuenta
				, @TabIdCuentaCobro tempEst
				Where taCuenta.IdCuenta = tempEst.IdCuentaCobro

				UPDATE @TabIdCuentaCobro set IdPasoHistorio = IdPasoSiguiente

				IF EXISTS (SELECT 1 FROM SEG.Rol WHERE Nombre = LTRIM(RTRIM(@RolTransaccion)) AND AprobacionPAC = 1)
				BEGIN
					--Actualizamos todas las cuentas de cobro al mismo consecutivo pac
					UPDATE cc set cc.ConsecutivoPac = [dbo].[ConsultarUltimoConsecutivoPAC]() + 1		
					from @TabIdCuentaCobro Tcc
						, SPCP.CuentasdeCobro cc
					WHERE Tcc.IdCuentaCobro = cc.IdCuenta
				END

			END
			ELSE
			BEGIN
				IF(@PasoDirecto is NULL)
				BEGIN
					-- si no aprobaron la cuenta y no enviaron el paso a devolver la cuenta

					IF EXISTS(select 1 from @TabIdCuentaCobro WHERE IdPasoAnterior IS NULL)
					BEGIN
						
						SELECT  @IDTipoCuentaGenerico = IdTipoCuenta
						FROM	SPCP.TiposCuenta
						WHERE	TipoCuenta = 'GENERICO'

						
						SELECT top 1 @IDUltimoPasoGenerico = IdFlujoPasoCuenta
						FROM SPCP.FlujoPasosCuenta
						WHERE IdTipoCuenta = @IDTipoCuentaGenerico
						ORDER BY Orden DESC


						--SI LA CUENTA ESTA EN TIPO DE CUENTA GENERICO EL PASO ANTERIOR ES QUE EL CONTRATISTA REVISE DE NUEVO LA CUENTA
						update @TabIdCuentaCobro set IdPasoAnterior = 0
						WHERE IdPasoAnterior IS NULL
						AND IdTipoCuenta = @IDTipoCuentaGenerico

						--SI LA CUENTA ESTA EN TIPO DE CUENTA GENERICO EL PASO ANTERIOR ES QUE EL CONTRATISTA REVISE DE NUEVO LA CUENTA
						update taCuenta2 set taCuenta2.IdEstadoGestion = 0, taCuenta2.IdTipoCuenta = 0
						from  SPCP.CuentasDeCobro taCuenta2
						, @TabIdCuentaCobro tempEst2
						Where taCuenta2.IdCuenta = tempEst2.IdCuentaCobro
						--AND tempEst2.IdPasoAnterior = 0
						AND tempEst2.IdTipoCuenta = @IDTipoCuentaGenerico
						
						--SI LA CUENTA ESTA EN TIPO DE CUENTA DIFERENTE A GENERICO EL PASO ANTERIOR ES EL ULTIMO DEL GENERICO
						update @TabIdCuentaCobro set IdPasoAnterior = @IDUltimoPasoGenerico
						WHERE IdPasoAnterior IS NULL
						AND IdTipoCuenta <> @IDTipoCuentaGenerico

						--SI LA CUENTA ESTA EN TIPO DE CUENTA DIFERENTE A GENERICO EL PASO ANTERIOR ES EL ULTIMO DEL GENERICO
						--update taCuenta2 set taCuenta2.IdEstadoGestion = @IDUltimoPasoGenerico, taCuenta2.IdTipoCuenta = @IDTipoCuentaGenerico
						update taCuenta2 set taCuenta2.IdTipoCuenta = @IDTipoCuentaGenerico
						from  SPCP.CuentasDeCobro taCuenta2
						, @TabIdCuentaCobro tempEst2
						Where taCuenta2.IdCuenta = tempEst2.IdCuentaCobro						
						AND tempEst2.IdTipoCuenta <> @IDTipoCuentaGenerico
		
					END

					update taCuenta set taCuenta.IdEstadoPaso = tempEst.IdPasoAnterior, taCuenta.Devuelta = 1, taCuenta.FechaModifica = GETDATE()
					from  SPCP.CuentasDeCobro taCuenta
					, @TabIdCuentaCobro tempEst
					Where taCuenta.IdCuenta = tempEst.IdCuentaCobro

					--Selecciona el paso 
					SELECT @IDPasoEncargadoArea  = IdPaso
					FROM SPCP.Pasos
					WHERE ltrim(rtrim(Paso)) = 'ENCARGADO DE AREA'

					--si vamos a devolver a generar planilla limpianos la tabla planilla detalle
					DELETE from SPCP.PlanillaDetalle
					WHERE IdCuenta in (SELECT IdCuentaCobro FROM @TabIdCuentaCobro WHERE IdPasoAnterior = @IDPasoEncargadoArea)


					UPDATE @TabIdCuentaCobro set IdPasoHistorio = IdPasoAnterior
				END
				ELSE
				BEGIN
					-- si el usuario es tesoreria y envio la cuenta a estado especifico
									
					SELECT @IDPasoCXP  = IdPaso
					FROM SPCP.Pasos
					WHERE ltrim(rtrim(Paso)) = 'GENERAR ARCHIVO CXP'
					
											
					update SPCP.CuentasDeCobro set IdEstadoPaso = @PasoDirecto, Devuelta = 1, FechaModifica = GETDATE()
					,GeneradoCxP = case when @PasoDirecto <> @IDPasoCXP then GeneradoCxP else 0 end -- Linea nueva jorge vizcaino Si la cuenta fue enviada a paso de cXp limpiamos el campo
					,CodigoCuentaPorPagarSIIF = case when @PasoDirecto <> @IDPasoCXP then CodigoCuentaPorPagarSIIF else '' end	-- Linea nueva jorge vizcaino Si la cuenta fue enviada a paso de cXp limpiamos el campo				
					Where IdCuenta in (SELECT IdCuentaCobro FROM @TabIdCuentaCobro)
										
					--update SPCP.CuentasDeCobro set IdEstadoPaso = @PasoDirecto
					--Where IdCuenta in (SELECT IdCuentaCobro FROM @TabIdCuentaCobro)

					UPDATE @TabIdCuentaCobro set IdPasoHistorio = @PasoDirecto
				END
			END

			--Guardamos transaccion del cambio del estado de la cuenta de cobro
			Insert into [SPCP].HistoricoEstadoCuentasDeCobro (IDFlujoPasoCuentaAnterior,
														IDFlujoPasoCuentaNuevo,
														IDCuentaCobro,
														Aprobado,
														Descripcion,
														Roltransaccion,
														UsuarioCrea,
														FechaCrea)
			SELECT	IdPasoActual
					,IdPasoHistorio
					,IdCuentaCobro
					,@Estado
					,@DEscripcion
					,@RolTransaccion
					,@IdUsuario
					,GETDATE()
			FROM @TabIdCuentaCobro

			

		COMMIT TRANSACTION
    END TRY        
    BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
        SELECT  @ErrorMessage = ERROR_MESSAGE()
        IF XACT_STATE() <> 0 
            ROLLBACK TRANSACTION
        RAISERROR (@ErrorMessage, 16, 1)         
    END CATCH
		
END











