﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Eliminar]
	@IdTipoCiiu INT
AS
BEGIN
	DELETE Proveedor.TipoCiiu WHERE IdTipoCiiu = @IdTipoCiiu
END

