﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que guarda un nuevo DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @IdTipoDocumento int, @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20)= NULL
AS
BEGIN
	INSERT INTO Proveedor.DocFinancieraProv(IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, IdTipoDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdInfoFin, @NombreDocumento, @LinkDocumento, @Observaciones, @IdTipoDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = SCOPE_IDENTITY() 		
END



