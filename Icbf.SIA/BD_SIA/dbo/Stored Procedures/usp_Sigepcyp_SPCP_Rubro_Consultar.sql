﻿

-- =============================================
-- Author:		Jorge Vizcaino
-- Create date:  7/18/2015 3:21:49 PM
-- Description:	Procedimiento almacenado que consulta un(a) Planilla
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Rubro_Consultar]
	
AS
BEGIN
	
	SELECT  Rubro as NroIndicadorPresupuestal
	FROM SPCP.PlanDePagos
	GROUP by Rubro

END





