﻿
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) Integrantes
-- Modificado por: Juan Carlos Valverde Sámano
-- Fecha: 10/AGO/2014
-- Descripción: Se concatenó Tipo Documento y Numero de Identificación al Nombre o Razón Social según el Tipo de Persona.
-- Modificado Por: Juan Carlos Valverde Sámano
-- Fecha: 19/AGO/2014
-- Descripción: Se agrega al resultado de la Columna los Datos de Representante legal cuando
-- se trata de una persona Jurídica, y se agrega también el Link Documento para personas Juridicas
-- para el Documento: Certificado de existencia y representación legal o de domicilio para entidades extranjeras
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantess_Consultar]
	@IdEntidad INT = NULL
AS
BEGIN

DECLARE @IdTipoDocumento INT=
(SELECT IdTipoDocumento FROM PROVEEDOR.TipoDocumento
WHERE Descripcion='Certificado de existencia y representación legal o de domicilio para entidades extranjeras')

		SELECT	IdIntegrante,
				CASE Integrantes.IdTipoPersona 
				WHEN 1 THEN (TD.CodDocumento+' '+ T.NUMEROIDENTIFICACION+' '+ T.PRIMERNOMBRE +' '+ISNULL(T.SEGUNDONOMBRE,'') +' ' + T.PRIMERAPELLIDO +' '+ ISNULL(T.SEGUNDOAPELLIDO,'') )
				ELSE TD.CodDocumento+' '+ T.NUMEROIDENTIFICACION+' '+T.RAZONSOCIAL END as Integrante, 
				TP.IdTipoPersona,
				TP.NombreTipoPersona, 
				PorcentajeParticipacion,
				ISNULL(CASE Integrantes.IdTipoPersona 
				WHEN 2 THEN (
				ISNULL((SELECT CodDocumento FROM GLOBAL.TiposDocumentos WHERE IdTipoDocumento=(SELECT IDTIPODOCIDENTIFICA FROM Oferente.TERCERO WHERE IDTERCERO=INFO.IdRepLegal)),'') +' '+ 
				(SELECT ISNULL(NUMEROIDENTIFICACION,'')+' '+ISNULL(PRIMERNOMBRE,'')+' '+
				ISNULL(SEGUNDONOMBRE,'')+' '+ISNULL(PRIMERAPELLIDO,'')+' '+ISNULL(SEGUNDOAPELLIDO,'') FROM Oferente.TERCERO WHERE IDTERCERO=INFO.IdRepLegal))
				ELSE ' ' END,'') AS 'Representante',
				CASE Integrantes.IdTipoPersona
				WHEN 2 THEN (SELECT LinkDocumento FROM PROVEEDOR.DocDatosBasicoProv
				WHERE IdTipoDocumento=@IdTipoDocumento AND IdEntidad=
				(SELECT IdEntidad FROM PROVEEDOR.EntidadProvOferente WHERE IdTercero=
				(SELECT IdTercero FROM Oferente.TERCERO WHERE NUMEROIDENTIFICACION=Integrantes.NumeroIdentificacion)) AND Activo=1)
				ELSE ' ' END AS 'LinkDoc'
		 FROM [PROVEEDOR].[Integrantes] Integrantes
		 INNER JOIN oferente.TERCERO T ON Integrantes.NumeroIdentificacion=T.NUMEROIDENTIFICACION
		 INNER JOIN Oferente.TipoPersona TP ON Integrantes.IdTipoPersona=TP.IdTipoPersona  
		 INNER JOIN Global.TiposDocumentos TD ON T.IDTIPODOCIDENTIFICA=TD.IdTipoDocumento
		 LEFT JOIN PROVEEDOR.EntidadProvOferente PROV ON T.IdTercero=PROV.IdTercero
		 LEFT JOIN PROVEEDOR.InfoAdminEntidad INFO ON PROV.IdEntidad=INFO.IdEntidad
		 WHERE Integrantes.IdEntidad = CASE WHEN @IdEntidad IS NULL 
			THEN Integrantes.IdEntidad ELSE @IdEntidad END
			AND T.IDESTADOTERCERO IS NOT NULL
		 
END


