﻿

CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarModuloPorNombre]
	@pNombreModulo NVARCHAR(200) = NULL
AS
BEGIN

SELECT 
	IdModulo, NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Modulo
WHERE 
	NombreModulo  LIKE CASE WHEN @pNombreModulo IS NULL THEN NombreModulo ELSE '%'+@pNombreModulo+'%' END

END
