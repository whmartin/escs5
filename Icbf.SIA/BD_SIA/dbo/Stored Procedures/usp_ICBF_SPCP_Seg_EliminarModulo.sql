﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) Modulo
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_EliminarModulo]
	@IdModulo INT
AS
BEGIN
	DELETE SEG.Modulo WHERE IdModulo = @IdModulo
END

