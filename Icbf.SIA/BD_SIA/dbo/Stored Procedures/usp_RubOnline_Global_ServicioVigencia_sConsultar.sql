﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que consulta un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_sConsultar]
	@IdVigencia INT = NULL,@IdServicio INT = NULL,@IdRubro INT = NULL,@CodigoServicio NVARCHAR(128) = NULL,@CodigoRubro NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdServicioVigencia, IdVigencia, IdServicio, IdRubro, CodigoServicio, NombreServicio, CodigoRubro, NombreRubro, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[ServicioVigencia] 
 WHERE IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END AND IdServicio = CASE WHEN @IdServicio IS NULL THEN IdServicio ELSE @IdServicio END AND IdRubro = CASE WHEN @IdRubro IS NULL THEN IdRubro ELSE @IdRubro END AND CodigoServicio = CASE WHEN @CodigoServicio IS NULL THEN CodigoServicio ELSE @CodigoServicio END AND CodigoRubro = CASE WHEN @CodigoRubro IS NULL THEN CodigoRubro ELSE @CodigoRubro END
 ORDER BY NombreServicio desc
END

