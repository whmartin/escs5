﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/11/2015 5:46:57 PM
-- Description:	Procedimiento almacenado que actualiza un(a) CuentasdeCobro
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobro_Modificar]
		@IdCuenta INT,@IdContrato INT,@IdEstadoPaso INT,@IdTipoCuenta INT,@IdEstadoGestion BIT,@Pensionado BIT,@declarante BIT,@IdRegimen INT,@PlanillaAportes NVARCHAR(50),@IdNivelARL INT,@IdPlanDePagos INT,@NumeroFactura NVARCHAR(50),@AporteSalud INT,@AportePension INT,@SolidaridadPensional INT,@AporteAFC INT,@AporteARL INT,@IdDatosAdministracion INT,@CodCompromiso INT,@NumeroDePago NVARCHAR(15),@NombreSupervisor NVARCHAR(250),@CargoSupervisor NVARCHAR(250), @TotalContrato DECIMAL(15,2), @HonorarioIva DECIMAL(15,2), @HonorarioSinIva DECIMAL(15,2), @HonorarioAcumulado DECIMAL(15,2), @Saldo DECIMAL(15,2),@UsuarioModifica NVARCHAR(250)
AS
BEGIN

	IF (@IdEstadoGestion = 1)
	BEGIN
		SELECT  @IdTipoCuenta = IdTipoCuenta
		FROM	SPCP.TiposCuenta
		WHERE	TipoCuenta = 'GENERICO'
		
		SELECT	TOP 1 @IdEstadoPaso = IdFlujoPasoCuenta
		FROM	SPCP.FlujoPasosCuenta
		WHERE	IdTipoCuenta = @IdTipoCuenta
				AND estado = 1
		ORDER BY Orden ASC		
		
		--INSERT INTO SPCP.HistoricoCuentasdeCobro
  --         ([IdCuenta]
  --         ,[IdContrato]
  --         ,[IdEstadoPaso]
  --         ,[IdTipoCuenta]
  --         ,[IdEstadoGestion]
  --         ,[Pensionado]
  --         ,[IdRegimen]
  --         ,[PlanillaAportes]
  --         ,[IdNivelARL]
  --         ,[IdPlanDePagos]
  --         ,[NumeroFactura]
  --         ,[AporteSalud]
  --         ,[AportePension]
  --         ,[SolidaridadPensional]
  --         ,[AporteAFC]
  --         ,[AporteARL]
  --         ,[IdDatosAdministracion]
  --         ,[NombreSupervisor]
  --         ,[CargoSupervisor]
  --         ,[UsuarioModifica]
  --         ,[FechaModifica]
  --         ,[HistoricoFechaCrea])
		--VALUES(@IdCuenta, @IdContrato, @IdEstadoPaso, @IdTipoCuenta, @IdEstadoGestion, @Pensionado, @IdRegimen, @PlanillaAportes, @IdNivelARL, @IdPlanDePagos, @NumeroFactura, @AporteSalud, @AportePension, @SolidaridadPensional, @AporteAFC, @AporteARL, @IdDatosAdministracion, @NombreSupervisor, @CargoSupervisor, @UsuarioModifica, GETDATE(), GETDATE())
	END 
	
	
	UPDATE	SPCP.CuentasdeCobro 
	SET		IdContrato = @IdContrato, 
			IdEstadoPaso = @IdEstadoPaso, 
			IdTipoCuenta = @IdTipoCuenta, 
			IdEstadoGestion = @IdEstadoGestion, 
			Pensionado = @Pensionado, 
			Declarante = @Declarante,
			IdRegimen = @IdRegimen, 
			PlanillaAportes = @PlanillaAportes, 
			IdNivelARL = @IdNivelARL, 
			IdPlanDePagos = @IdPlanDePagos, 
			NumeroFactura = @NumeroFactura, 
			AporteSalud = @AporteSalud, 
			AportePension = @AportePension, 
			SolidaridadPensional = @SolidaridadPensional, 
			AporteAFC = @AporteAFC, 
			AporteARL = @AporteARL, 
			IdDatosAdministracion = @IdDatosAdministracion, 
			CodCompromiso = @CodCompromiso,
			NumeroDePago = @NumeroDePago,
			NombreSupervisor = @NombreSupervisor, 
			CargoSupervisor = @CargoSupervisor,
			TotalContrato = @TotalContrato, 
			HonorarioIva = @HonorarioIva, 
			HonorarioSinIva = @HonorarioSinIva, 
			HonorarioAcumulado = @HonorarioAcumulado, 
			Saldo = @Saldo,
			UsuarioModifica = @UsuarioModifica, 
			FechaModifica = GETDATE()
	WHERE IdCuenta = @IdCuenta
END
