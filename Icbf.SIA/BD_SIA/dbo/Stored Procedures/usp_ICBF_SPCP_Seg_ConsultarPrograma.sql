﻿

-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 03-09-2012
-- Description:	Procedimiento almacenado que consulta un programa
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarPrograma]
	@IdPrograma INT
AS
BEGIN

SELECT 
	IdPrograma, IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, VisibleMenu, generaLog
FROM 
	SEG.Programa
WHERE 
	IdPrograma = @IdPrograma


END
