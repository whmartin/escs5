﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que elimina un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Eliminar]
	@IdInformacionPresupuestal INT
AS
BEGIN
	DELETE Contrato.InformacionPresupuestal WHERE IdInformacionPresupuestal = @IdInformacionPresupuestal
END
