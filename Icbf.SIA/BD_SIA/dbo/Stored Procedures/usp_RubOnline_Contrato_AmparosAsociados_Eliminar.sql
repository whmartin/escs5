﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Eliminar]
	@IdAmparo INT
AS
BEGIN
	DELETE Contrato.AmparosAsociados WHERE IdAmparo = @IdAmparo
END

