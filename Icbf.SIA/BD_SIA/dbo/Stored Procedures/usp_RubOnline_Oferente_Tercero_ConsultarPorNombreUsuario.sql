﻿



-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 26/08/2014
-- Description:	Consulta un Tercero por parametro CorreoElectronico
-- Modificación: Juan Carlos Valverde Sámano
-- Fecha: 2014/10/16
-- Descripción: Se modificó ya que en terceros buscaba el registro con el parametro
-- de entrada @correoElectronico que está recibiendo el correo electrónico de la cuenta de usuario
-- con que se creó el tercero. Esto entraba en conflicto cuando se modificaba la cuenta de correo de
-- registro del TERCERO. Entonces ahora se obtiene el ProviderKey del usuario y se busca en 
-- TERCEROS el tercero creado con tal providerkey.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarPorNombreUsuario]
	@correoElectronico NVARCHAR (50)
AS
BEGIN

	SELECT
		IDTERCERO,
		Numeroidentificacion,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica,
		CreadoPorInterno
	FROM oferente.TERCERO
	WHERE ProviderUserKey=(	
	SELECT providerKey FROM SEG.Usuario
	WHERE CorreoElectronico=@correoElectronico)
END




