﻿
--- =================================================================
-- Author:		<Author,,Grupo Desarrollo SIGEPCYP>
-- Create date: <8/20/2015 8:24:06 PM,>
-- Description:	<Consultar Información de Acciones,>
-- ====================================================================
--  exec [dbo].[usp_Sigepcyp_SPCP_Accioness_Consultar]
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Accioness_Consultar]
	@Descripcion NVARCHAR(160) = NULL,@Estado INT = NULL,@CodAccion NVARCHAR(10) = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@Descripcion NVARCHAR(160),@Estado INT,@CodAccion NVARCHAR(10)'
Set @SqlExec = '
 SELECT IdAcciones, Descripcion, 
 (CASE Estado WHEN ''True'' THEN 1 
      WHEN ''False'' THEN 0 
	  END ) as Estado, 
 CodAccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[Acciones] WHERE 1=1 '
 If(@Descripcion Is Not Null) Set @SqlExec = @SqlExec + ' And Descripcion = @Descripcion' If(@Estado Is Not Null) Set @SqlExec = @SqlExec + ' And Estado = @Estado' If(@CodAccion Is Not Null) Set @SqlExec = @SqlExec + ' And CodAccion = @CodAccion' 
Exec sp_executesql  @SqlExec, @SqlParametros,@Descripcion,@Estado,@CodAccion
END
