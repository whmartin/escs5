﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que consulta un(a) CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_sConsultar]
	@IdMunicipio INT = NULL
	,@IdRegional INT = NULL
	,@NombreCentroZonal NVARCHAR(45) = NULL
	,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdCentroZonal
	, IdMunicipio
	, IdRegional
	, CodigoMunicipio
	, CodigoCentroZonal
	, NombreCentroZonal
	, Direccion
	, Telefonos
	, Estado
	, UsuarioCrea
	, FechaCrea
	, UsuarioModifica
	, FechaModifica 
FROM [DIV].[CentroZonal] 
WHERE 
	IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END 
	AND IdRegional = CASE WHEN @IdRegional IS NULL THEN IdRegional ELSE @IdRegional END 
	AND NombreCentroZonal = CASE WHEN @NombreCentroZonal IS NULL THEN NombreCentroZonal ELSE @NombreCentroZonal END 
	AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

