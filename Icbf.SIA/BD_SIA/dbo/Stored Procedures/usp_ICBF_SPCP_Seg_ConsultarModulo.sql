﻿
-- =============================================
-- Author:		Yuri Gereda
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que consulta un Módulo
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarModulo]
	@IdModulo INT
AS
BEGIN

SELECT 
	IdModulo, NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Modulo
WHERE 
	IdModulo = @IdModulo


END

