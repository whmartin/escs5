﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que elimina un(a) EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Eliminar]
	@IdEntidad INT
AS
BEGIN
	DELETE Proveedor.EntidadProvOferente WHERE IdEntidad = @IdEntidad
END

