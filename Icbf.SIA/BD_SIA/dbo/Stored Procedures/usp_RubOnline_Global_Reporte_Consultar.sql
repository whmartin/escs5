﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reporte_Consultar]
	@IdReporte INT
AS
BEGIN
 SELECT IdReporte
       ,NombreReporte
       ,Descripcion
       ,Servidor
       ,Carpeta
       ,NombreArchivo
       ,UsuarioCrea
       ,FechaCrea
       ,UsuarioModifica
       ,FechaModifica 
 FROM [Global].[Reporte] 
 WHERE  IdReporte = @IdReporte
END
