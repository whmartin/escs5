﻿

-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que consulta un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contratos_Consultar_lupa]
@FechaRegistro DATETIME = NULL,
@NumeroProceso NVARCHAR (50) = NULL,
@NumeroContrato nvarchar (50) = NULL,
@IdModalidad INT = NULL,
@IdCategoriaContrato INT = NULL,
@IdTipoContrato INT = NULL,
@ClaseContrato	nchar(1) = null
AS
BEGIN

SELECT
	CONVERT(date, FechaRegistro),
	IdContrato,
	FechaRegistro,
	NumeroProceso,
	NumeroContrato,
	FechaAdjudicacion,
	contr.IdModalidad,
	contr.IdCategoriaContrato,
	contr.IdTipoContrato,
	IdCodigoModalidad,
	IdModalidadAcademica,
	IdCodigoProfesion,
	IdNombreProfesion,
	IdRegionalContrato,
	RequiereActa,
	ManejaAportes,
	ManejaRecursos,
	ManejaVigenciasFuturas,
	IdRegimenContratacion,
	CodigoRegional,
	NombreSolicitante,
	DependenciaSolicitante,
	CargoSolicitante,
	ObjetoContrato,
	AlcanceObjetoContrato,
	ValorInicialContrato,
	ValorTotalAdiciones,
	ValorFinalContrato,
	ValorAportesICBF,
	ValorAportesOperador,
	ValorTotalReduccion,
	JustificacionAdicionSuperior50porc,
	FechaSuscripcion,
	FechaInicioEjecucion,
	FechaFinalizacionInicial,
	PlazoInicial,
	FechaInicialTerminacion,
	FechaFinalTerminacion,
	FechaProyectadaLiquidacion,
	FechaAnulacion,
	Prorrogas,
	PlazoTotal,
	FechaFirmaActaInicio,
	VigenciaFiscalInicial,
	VigenciaFiscalFinal,
	IdUnidadEjecucion,
	IdLugarEjecucion,
	DatosAdicionales,
	IdEstadoContrato,
	IdTipoDocumentoContratista,
	IdentificacionContratista,
	NombreContratista,
	IdFormaPago,
	IdTipoEntidad,
	contr.UsuarioCrea,
	contr.FechaCrea,
	contr.UsuarioModifica,
	contr.FechaModifica,
	ClaseContrato,
	Consecutivo,
	AfectaPlanCompras,
	IdSolicitante,
	IdProducto,
	FechaLiquidacion,
	NumeroDocumentoVigenciaFutura,
	RequiereGarantia,
	modalidad.Nombre as Modalidad,
	categoria.NombreCategoriaContrato as CategoriaContrato,
	tipcontr.NombreTipoContrato as TipoContrato
FROM Contrato.Contrato contr inner join Contrato.TipoContrato tipcontr
on (tipcontr.IdTipoContrato = contr.IdTipoContrato) left join Contrato.ModalidadSeleccion modalidad
on (modalidad.IdModalidad = contr.IdModalidad) left join Contrato.CategoriaContrato categoria
on (categoria.IdCategoriaContrato = contr.IdCategoriaContrato)
WHERE (NumeroProceso = @NumeroProceso OR @NumeroProceso IS NULL)
AND (NumeroContrato = @NumeroContrato OR @NumeroContrato IS NULL)
AND (contr.IdModalidad = @IdModalidad OR @IdModalidad IS NULL)
AND (contr.IdCategoriaContrato = @IdCategoriaContrato OR @IdCategoriaContrato IS NULL)
AND (contr.IdTipoContrato = @IdTipoContrato OR @IdTipoContrato IS NULL)
AND FechaRegistro =
--AND (CONVERT(date, FechaRegistro) = CONVERT(date, @FechaRegistro))
	CASE
		WHEN @FechaRegistro IS NULL THEN FechaRegistro ELSE @FechaRegistro
	END
AND ClaseContrato = ISNULL(@ClaseContrato,ClaseContrato)
END



