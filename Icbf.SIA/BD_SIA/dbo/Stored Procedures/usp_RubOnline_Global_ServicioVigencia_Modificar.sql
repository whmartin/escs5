﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que actualiza un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Modificar]
		@IdServicioVigencia INT,	@IdVigencia INT,	@IdServicio INT,	@IdRubro INT,	@CodigoServicio NVARCHAR(128),	@NombreServicio NVARCHAR(256),	@CodigoRubro NVARCHAR(128),	@NombreRubro NVARCHAR(256),	@Estado NVARCHAR, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.ServicioVigencia SET IdVigencia = @IdVigencia, IdServicio = @IdServicio, IdRubro = @IdRubro, CodigoServicio = @CodigoServicio, NombreServicio = @NombreServicio, CodigoRubro = @CodigoRubro, NombreRubro = @NombreRubro, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdServicioVigencia = @IdServicioVigencia
END

