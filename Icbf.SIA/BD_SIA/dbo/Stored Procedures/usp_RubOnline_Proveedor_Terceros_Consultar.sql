﻿
-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar]
	@IDTIPODOCIDENTIFICA varchar (10)= NULL,
	@IDESTADOTERCERO varchar(10) = NULL,
	@IdTipoPersona varchar(10) = NULL,
	@NUMEROIDENTIFICACION varchar(250) = NULL,
	@USUARIOCREA varchar(128) = NULL,
	@TERCERO varchar(128) =null
	
AS
BEGIN

Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@IDTIPODOCIDENTIFICA varchar(10),	@IDESTADOTERCERO varchar(10),	@IdTipoPersona varchar(10),	@NUMEROIDENTIFICACION varchar(250),	@USUARIOCREA varchar(128),	@TERCERO varchar(128)'
Set @SqlExec = '
	SELECT       Tercero.TERCERO.IDTERCERO  AS IDTERCERO, 
				 Tercero.TERCERO.IDTIPODOCIDENTIFICA  AS IDTIPODOCIDENTIFICA, 
				 Tercero.TERCERO.IDESTADOTERCERO AS IDESTADOTERCERO, 
				 Tercero.TERCERO.IdTipoPersona  AS IdTipoPersona, 
                 CAST(Tercero.TERCERO.NUMEROIDENTIFICACION AS VARCHAR(50)) AS  NUMEROIDENTIFICACION,
				 Tercero.TERCERO.DIGITOVERIFICACION, 
                 Tercero.TERCERO.CORREOELECTRONICO, 
                 Tercero.TERCERO.PRIMERNOMBRE, 
                 Tercero.TERCERO.SEGUNDONOMBRE, 
                 Tercero.TERCERO.PRIMERAPELLIDO, 
                 Tercero.TERCERO.SEGUNDOAPELLIDO, 
                 Tercero.TERCERO.RAZONSOCIAL, 
                 Tercero.TERCERO.FECHAEXPEDICIONID, 
                 Tercero.TERCERO.FECHANACIMIENTO, 
                 Tercero.TERCERO.SEXO, 
                 Tercero.TERCERO.FECHACREA, 
                 Tercero.TERCERO.USUARIOCREA, 
                 Tercero.TERCERO.FECHAMODIFICA, 
                 Tercero.TERCERO.USUARIOMODIFICA, 
                 Global.TiposDocumentos.NomTipoDocumento AS NombreTipoIdentificacionPersonaNatural, 
                 Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural, 
                 Tercero.EstadoTercero.CodigoEstadotercero, 
                 Tercero.EstadoTercero.DescripcionEstado, 
                 Tercero.TipoPersona.CodigoTipoPersona, 
                 Tercero.TipoPersona.NombreTipoPersona,
				 Tercero.TERCERO.ProviderUserKey
	FROM         Tercero.TERCERO 
				 INNER JOIN Global.TiposDocumentos  ON 
				 Tercero.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento 
				 INNER JOIN Tercero.EstadoTercero ON 
				 Tercero.TERCERO.IDESTADOTERCERO = Tercero.EstadoTercero.IdEstadoTercero 
				 INNER JOIN Tercero.TipoPersona ON 
				 Tercero.TERCERO.IdTipoPersona = Tercero.TipoPersona.IdTipoPersona
	WHERE 1=1 '
		  If(@TERCERO Is Not Null AND @TERCERO <>'') Set @SqlExec = @SqlExec + ' And 
		  ( REPLACE ((Tercero.TERCERO.PRIMERNOMBRE+'' ''+Tercero.TERCERO.SEGUNDONOMBRE+'' ''+Tercero.TERCERO.PRIMERAPELLIDO+'' ''+Tercero.TERCERO.SEGUNDOAPELLIDO),'' '','''')  Like REPLACE (''%'+@TERCERO+'%'','' '','''')
           OR Tercero.TERCERO.RAZONSOCIAL     Like ''%'+@TERCERO+'%''
		   )'
		  If(@IDTIPODOCIDENTIFICA Is Not Null)  Set @SqlExec = @SqlExec + ' And Tercero.TERCERO.IDTIPODOCIDENTIFICA  = '+@IDTIPODOCIDENTIFICA+'' 
		  If(@IDESTADOTERCERO Is Not Null)      Set @SqlExec = @SqlExec + ' And Tercero.TERCERO.IDESTADOTERCERO      = '+@IDESTADOTERCERO+''
		  If(@IdTipoPersona Is Not Null)        Set @SqlExec = @SqlExec + ' And Tercero.TERCERO.IdTipoPersona        = '+@IdTipoPersona+'' 
		  If(@NUMEROIDENTIFICACION Is Not Null) Set @SqlExec = @SqlExec + ' And Tercero.TERCERO.NUMEROIDENTIFICACION = '''+@NUMEROIDENTIFICACION+'''' 
		  If(@USUARIOCREA Is Not Null)          Set @SqlExec = @SqlExec + ' And Tercero.TERCERO.USUARIOCREA          = '''+@USUARIOCREA+''''

  set	@SqlExec = @SqlExec +'	
	order by 4,2,5 '
	----PRINT @SqlExec
Exec sp_executesql  @SqlExec, @SqlParametros,@TERCERO,@IDTIPODOCIDENTIFICA,@IDESTADOTERCERO,@IdTipoPersona,@NUMEROIDENTIFICACION,@USUARIOCREA

END

