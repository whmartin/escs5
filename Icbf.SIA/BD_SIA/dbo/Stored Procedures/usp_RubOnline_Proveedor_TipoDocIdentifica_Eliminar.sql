﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Eliminar]
	@IdTipoDocIdentifica INT
AS
BEGIN
	DELETE Proveedor.TipoDocIdentifica WHERE IdTipoDocIdentifica = @IdTipoDocIdentifica
END

