﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Programa
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_InsertarPrograma]
		@IdPrograma INT OUTPUT, 	
		@IdModulo INT,	
		@NombrePrograma NVARCHAR(250),	
		@CodigoPrograma NVARCHAR(250),	
		@Posicion INT,	
		@Estado INT, 
		@UsuarioCreacion NVARCHAR(250),
		@VisibleMenu	BIT,
		@GeneraLog	BIT
AS
BEGIN
	INSERT INTO SEG.Programa(IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, VisibleMenu, generaLog)
					  VALUES(@IdModulo, @NombrePrograma, @CodigoPrograma, @Posicion, @Estado, @UsuarioCreacion, GETDATE(), @VisibleMenu, @GeneraLog)
	SELECT @IdPrograma = @@IDENTITY 		
END

