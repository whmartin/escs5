﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que actualiza un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Modificar]
		@IdCargo INT,	@TipoCargo NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.CargoTipoCargo 
	SET IdCargo = @IdCargo, TipoCargo = @TipoCargo, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdCargo=@IdCargo AND TipoCargo=@TipoCargo
END

