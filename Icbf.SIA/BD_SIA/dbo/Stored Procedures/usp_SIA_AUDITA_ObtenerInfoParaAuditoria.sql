﻿
/***
Autor: Juan Carlos Valverde Sámano
Fecha: 20/07/2014
Descripción: Obtiene la información necesaria para generar el Log de auditoria
de usuarios inactivos.
***/

CREATE PROCEDURE [dbo].[usp_SIA_AUDITA_ObtenerInfoParaAuditoria]
@UserId NVARCHAR(125)
AS
BEGIN
SELECT 
U.UsuarioCreacion
,U.FechaCreacion
,(CONVERT(VARCHAR,DOC.IdTipoDocumento) +'-'+DOC.NomTipoDocumento) 'TipoDocumento'
,U.NumeroDocumento
,U.DV
,U.PrimerNombre
,U.SegundoNombre
,U.PrimerApellido
,U.SegundoApellido
,U.RazonSocial
,U.Estado
,U.CorreoElectronico
FROM SEG.Usuario U
INNER JOIN Global.TiposDocumentos DOC 
ON U.IdTipoDocumento=DOC.IdTipoDocumento
LEFT JOIN Oferente.Tercero T 
ON U.providerKey=T.ProviderUserKey
WHERE U.providerKey=@UserId


END


