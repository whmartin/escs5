﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Modificar]
		@IdRegimenContratacion INT,	@NombreRegimenContratacion NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.RegimenContratacion SET NombreRegimenContratacion = @NombreRegimenContratacion, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRegimenContratacion = @IdRegimenContratacion
END
