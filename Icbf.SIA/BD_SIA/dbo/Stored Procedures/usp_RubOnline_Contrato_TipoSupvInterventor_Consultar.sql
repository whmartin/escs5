﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Consultar]
	@IdTipoSupvInterventor INT
AS
BEGIN
 SELECT IdTipoSupvInterventor, Nombre, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoSupvInterventor] WHERE  IdTipoSupvInterventor = @IdTipoSupvInterventor
END

