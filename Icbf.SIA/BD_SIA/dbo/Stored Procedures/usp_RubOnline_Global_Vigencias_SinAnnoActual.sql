﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  20/06/2013
-- Description:	Procedimiento almacenado que consulta las vigencias sin incluir el año actual
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_Vigencias_SinAnnoActual]
	@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT TOP 5 IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Global].[Vigencia] 
 WHERE AcnoVigencia < Year(getdate())
 AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
 ORDER BY AcnoVigencia DESC
END


