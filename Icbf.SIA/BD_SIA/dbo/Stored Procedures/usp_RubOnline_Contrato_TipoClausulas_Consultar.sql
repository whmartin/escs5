﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausulas_Consultar
SE ADICIONA EL ORDEN POR NOMBRE TIPO CLAUSULA
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausulas_Consultar]

	@NombreTipoClausula NVARCHAR (50) = NULL,
	@Descripcion NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdTipoClausula,
		NombreTipoClausula,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoClausula]
	WHERE NombreTipoClausula LIKE '%' + 
		CASE
			WHEN @NombreTipoClausula IS NULL THEN NombreTipoClausula ELSE @NombreTipoClausula
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreTipoClausula

END


