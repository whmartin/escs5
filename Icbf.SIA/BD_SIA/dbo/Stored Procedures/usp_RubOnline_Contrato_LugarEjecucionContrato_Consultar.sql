﻿
-- =============================================
-- Author:		@Res\Jonathan Acosta
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Consultar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
 SELECT 
	IdContratoLugarEjecucion, IDContrato, 
	Nacional, UsuarioCrea, FechaCrea, 
	UsuarioModifica, FechaModifica 
	FROM [Contrato].[LugarEjecucionContrato] 
	WHERE  IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END


