﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Modificar]
		@IdTipoContrato INT,	@NombreTipoContrato NVARCHAR(128),	@IdCategoriaContrato INT,	@ActaInicio BIT,	@AporteCofinaciacion BIT,	@RecursoFinanciero BIT,	@RegimenContrato INT,	@DescripcionTipoContrato NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoContrato SET NombreTipoContrato = @NombreTipoContrato, IdCategoriaContrato = @IdCategoriaContrato, ActaInicio = @ActaInicio, AporteCofinaciacion = @AporteCofinaciacion, RecursoFinanciero = @RecursoFinanciero, RegimenContrato = @RegimenContrato, DescripcionTipoContrato = @DescripcionTipoContrato, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoContrato = @IdTipoContrato
END
