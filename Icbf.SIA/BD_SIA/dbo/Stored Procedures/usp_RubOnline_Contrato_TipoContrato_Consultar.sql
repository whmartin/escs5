﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Consultar]
	@IdTipoContrato INT
AS
BEGIN
 SELECT IdTipoContrato, NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoContrato] WHERE  IdTipoContrato = @IdTipoContrato
END
