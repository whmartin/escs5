﻿
-- =============================================
-- Author:		Jorge Vizcaino
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que consulta un(a) AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_EstadoCuentaDeCobro_Consultar]
	@IdCuentaCobro INT
AS
BEGIN
	SELECT cc.IdCuenta,p.IdPaso,p.Paso,fc.IdFlujoPasoCuenta
	FROM  SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.FlujoPasosCuenta fc
	on cc.IdEstadoPaso = fc.IdFlujoPasoCuenta
	INNER JOIN SPCP.Pasos p
	on fc.IdPaso = p.IdPaso
	WHERE cc.IdCuenta = @IdCuentaCobro	
END
