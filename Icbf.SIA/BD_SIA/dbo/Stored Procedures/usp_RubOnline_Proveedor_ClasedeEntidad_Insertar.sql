﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Insertar]
		@IdClasedeEntidad INT OUTPUT, 	@IdTipodeActividad INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ClasedeEntidad(IdTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipodeActividad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdClasedeEntidad = SCOPE_IDENTITY() 		
END


