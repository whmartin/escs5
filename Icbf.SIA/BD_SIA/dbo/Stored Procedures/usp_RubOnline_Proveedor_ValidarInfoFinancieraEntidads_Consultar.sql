﻿



-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidads_Consultar]
	@IdInfoFin int = NULL,@Observaciones NVARCHAR(200) = NULL,@ConfirmaYAprueba BIT = NULL
AS
BEGIN
 SELECT IdValidarInfoFinancieraEntidad, IdInfoFin, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea
 FROM [Proveedor].[ValidarInfoFinancieraEntidad] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END 
 AND Observaciones = CASE WHEN @Observaciones IS NULL THEN Observaciones ELSE @Observaciones END 
 AND ConfirmaYAprueba = CASE WHEN @ConfirmaYAprueba IS NULL THEN ConfirmaYAprueba ELSE @ConfirmaYAprueba END
 ORDER BY FechaCrea DESC
END





