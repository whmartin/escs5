﻿-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  02/26/2014 3:50:54 PM
-- Description:	Procedimiento almacenado que consulta un(a) Clase
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Clase]
	@CodigoFamilia NVARCHAR(50)
AS
BEGIN
 SELECT DISTINCT(CodigoClase),
		NombreClase
  FROM	PROVEEDOR.TipoCodigoUNSPSC
  WHERE	CodigoFamilia = @CodigoFamilia
END