﻿---========================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/6/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que guarda un nuevo PlanDePagos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_PlanDePagos_Insertar]
		@IdPlanDePagos INT OUTPUT, 	@IdDatosAdministracion INT,	@Codpci NVARCHAR(255),	@Vigencia INT,	@ConsecutivoPACCO INT,	@CodCompromiso INT,	@Fecha DATETIME,	@TipoDocumento INT,	@NumDocIdentidad NVARCHAR(20),	@CodTipoDocSoporte INT,	@NumDocSoporte NVARCHAR(40),	@ValorTotalContrato INT,	@Rubro NVARCHAR(30),	@Recurso INT,	@Fuente NVARCHAR(15),	@NumeroDePago NVARCHAR(15),	@OrdenDePago INT,	@ValorDelPago INT,	@Estado INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.PlanDePagos(IdDatosAdministracion, Codpci, Vigencia, ConsecutivoPACCO, CodCompromiso, Fecha, TipoDocumento, NumDocIdentidad, CodTipoDocSoporte, NumDocSoporte, ValorTotalContrato, Rubro, Recurso, Fuente, NumeroDePago, OrdenDePago, ValorDelPago, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdDatosAdministracion, @Codpci, @Vigencia, @ConsecutivoPACCO, @CodCompromiso, @Fecha, @TipoDocumento, @NumDocIdentidad, @CodTipoDocSoporte, @NumDocSoporte, @ValorTotalContrato, @Rubro, @Recurso, @Fuente, @NumeroDePago, @OrdenDePago, @ValorDelPago, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdPlanDePagos = @@IDENTITY 		
END
