﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:34:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Barrio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Barrio_Consultar]
	@IdBarrio INT
AS
BEGIN
 SELECT IdBarrio
      , CodigoBarrio
      , NombreBarrio
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
 FROM [DIV].[Barrio] 
 WHERE  IdBarrio = @IdBarrio
END

