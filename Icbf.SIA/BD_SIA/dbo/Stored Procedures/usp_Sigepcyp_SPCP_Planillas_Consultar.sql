﻿
-- =============================================  
-- Author:  Jorge Vizcaino  
-- Create date:  7/18/2015 3:21:49 PM  
-- Description: Procedimiento almacenado que consulta un(a) Planilla  
-- =============================================  
--exec [usp_Sigepcyp_SPCP_Planillas_Consultar] @IdRegional = 34,@IdPasoRol = 'GESTION HUMANA'
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Planillas_Consultar]  
 @IdRegional INT = NULL,  
 @IdArea INT = NULL,  
 @Impreso bit = NULL,  
 @IdPlanilla int = NULL,  
 @IdPasoRol nvarchar(80) = NULL,  
 @IdTipoDoc int = NULL,  
 @NumeroDoc nvarchar(80) = NULL,  
 @FechaPlanilla Datetime = NULL  
AS  
BEGIN  
   
  
 Declare @IdRol int  
   --@IdPasoAsignado int = null  
  
  
 if(@IdPasoRol is not null)  
 Begin  
  Select @IdRol = IdRol  
  from SEG.Rol  
  where Nombre = ltrim(rtrim(@IdPasoRol))  
  
  If(Not Exists(Select 1 From SEG.Rol Where IdRol = @IdRol and IdPaso IS NOT NULL))  
  BEGIN  
    RAISERROR ('El Rol asignado no tiene un paso configurado', 16, 1)        
  END  
  --Select @IdPasoAsignado = IdPaso  
  --From SEG.Rol  
  --WHERE IdRol = @IdRol  
 END  
  
 IF(@IdPasoRol is NULL)  
 BEGIN  
  SELECT  p.IDPlanilla  
    ,p.FechaCrea  
    ,p.Impreso     
  FROM SPCP.Planilla p   
  WHERE p.IdRegional = isnull(@IdRegional,p.IdRegional)   
  --AND p.IdArea = isnull(@IdArea,p.IdArea)  
  AND p.Impreso = ISNULL(@Impreso,p.Impreso)  
  AND p.IDPlanilla = ISNULL(@IdPlanilla,p.IDPlanilla)   
  and p.FechaCrea = ISNULL(@FechaPlanilla,p.FechaCrea)  
 END  
 ELSE  
 BEGIN  
  SELECT  p.IDPlanilla  
    ,p.FechaCrea  
    ,p.Impreso     
  FROM SPCP.Planilla p   
  WHERE p.IdRegional = isnull(@IdRegional,p.IdRegional)   
  --AND p.IdArea = isnull(@IdArea,p.IdArea)  
  AND p.Impreso = ISNULL(@Impreso,p.Impreso)  
  AND p.IDPlanilla = ISNULL(@IdPlanilla,p.IDPlanilla)  
  and p.FechaCrea = ISNULL(@FechaPlanilla,p.FechaCrea)  
  AND p.IDPlanilla in (SELECT tpRol.IDPlanilla  
        FROM DBO.PlanillasCuentasPorRol(@IdPasoRol) tpRol  
        WHERE tpRol.TipoDocumento = ISNULL(@IdTipoDoc,tpRol.TipoDocumento)  
        AND tpRol.NumDocIdentidad = ISNULL(@NumeroDoc,tpRol.NumDocIdentidad))  
  --AND p.IDPlanilla in ( Select pd.IdPlanilla  
  --      from SPCP.PlanillaDetalle pd  
  --      INNER JOIN SPCP.CuentasDeCobro cc  
  --      on pd.IdCuenta =cc.IdCuenta  
  --      INNER JOIN SPCP.FlujoPasosCuenta fp  
  --      on cc.IdEstadoPaso = fp.IdFlujoPasoCuenta  
  --      INNER JOIN SPCP.Pasos p  
  --      ON fp.IdPaso = p.IdPaso  
  --      INNER JOIN SPCP.DatosAdministracion da  
  --      on cc.IdDatosAdministracion = da.IdDatosAdministracion  
  --      WHERE p.IdPaso =isnull(@IdPasoAsignado,p.IdPaso)  
  --      AND da.TipoDocumento = ISNULL(@IdTipoDoc,da.TipoDocumento)  
  --      AND da.NumDocIdentidad = ISNULL(@NumeroDoc,da.NumDocIdentidad))  
 END  
END  
  
  
  
  
  