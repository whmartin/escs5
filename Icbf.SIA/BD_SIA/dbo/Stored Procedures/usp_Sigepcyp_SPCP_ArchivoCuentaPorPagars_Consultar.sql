﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:25 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoCuentaPorPagar

-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ArchivoCuentaPorPagars_Consultar]
	@IdsCuentaCobro nvarchar(2000) 
AS
BEGIN
	
	DECLARE @TempMaestroCxP as table( id int identity(1,1)
										,PCIConexion Nvarchar(255)
										,Consecutivo  int
										,FechaRegistro DateTime
										,TipoDocumentoIdentidad Nvarchar(10)
										,numeroDocumentoTercero nvarchar(20)
										,consecutivoCompromiso int
										,TipoCuentaPorPagar Nvarchar(10)
										,ValorPesos  Nvarchar(40)
										,TasaCambio Nvarchar(10)
										,ValorIva numeric(18,2)
										,TextoCreacion  Nvarchar(255)
										,TipoDocSoporte int
										,NumeroDocSoporte Nvarchar(40)
										,FechaSolicitud dateTime
										,Expedidor Nvarchar(10)
										,NombreFuncionario Nvarchar(10)
										,CargoFuncionario Nvarchar(10)
										,Observacion Nvarchar(10)
										,urlDocumento Nvarchar(10)
										,urlDescripcion Nvarchar(10) 
										,IDDatosAdmin int)

	INSERT INTO @TempMaestroCxP
	SELECT da.Codpci as PCIConexion
			,ROW_NUMBER() over(ORDER BY cc.IdCuenta)  as Consecutivo
			,cc.FechaCrea as FechaRegistro
			,td.codSiif as TipoDocumentoIdentidad
			,da.NumDocIdentidad as numeroDocumentoTercero
			,da.CodCompromiso as consecutivoCompromiso
			,'64' as TipoCuentaPorPagar
			,convert(numeric(30,0),isnull(cc.HonorarioSinIva,cc.HonorarioIva)) as  ValorPesos
			---,da.ValorActual as ValorPesos
			,'' as TasaCambio
			,0  as ValorIva
			,'' as TextoCreacion
			,'36' as TipoDocSoporte     -------da.CodTipoDocSoporte as TipoDocSoporte
			,da.NumDocSoporte   as NumeroDocSoporte 
			,da.FechaDocSoporte as FechaSolicitud
			,'11' as Expedidor
			,'' as NombreFuncionario
			,'' as CargoFuncionario
			,'' as Observacion
			,'' as urlDocumento
			,'' as urlDescripcion
			,da.IdDatosAdministracion
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.PlanillaDetalle pd
	ON cc.IdCuenta = pd.IdCuenta
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN SPCP.PlanDePagos pp
	ON cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	INNER JOIN Global.Areas a on da.IdArea = a.IdArea
	INNER JOIN (SELECT max(temhc.fechaCrea) fechaAprobacion,temhc.IDCuentaCobro
				from SPCP.HistoricoEstadoCuentasDeCobro temhc				
				Group by temhc.IDCuentaCobro) tab1
	ON cc.IdCuenta = tab1.IDCuentaCobro
	WHERE cc.IdCuenta IN (SELECT data FROM dbo.Split(@IdsCuentaCobro,';'))


	SELECT 	PCIConexion
			,Consecutivo
			,FechaRegistro
			,TipoDocumentoIdentidad
			,numeroDocumentoTercero 
			,consecutivoCompromiso 
			,TipoCuentaPorPagar 
			,ValorPesos  
			,TasaCambio 
			,ValorIva 
			,TextoCreacion  
			,TipoDocSoporte 
			,NumeroDocSoporte 
			,FechaSolicitud 
			,Expedidor 
			,NombreFuncionario 
			,CargoFuncionario 
			,Observacion 
			,urlDocumento 
			,urlDescripcion 
	FROM @TempMaestroCxP

	SELECT cxp.Consecutivo as ConsecutivoMaestro
			,'6' as DocumentoSoporte    ----da.CodTipoDocSoporte as TipoDocSoporte
	FROM @TempMaestroCxP cxp
	INNER JOIN SPCP.DatosAdministracion da
	ON cxp.IDDatosAdmin = da.IdDatosAdministracion

END

