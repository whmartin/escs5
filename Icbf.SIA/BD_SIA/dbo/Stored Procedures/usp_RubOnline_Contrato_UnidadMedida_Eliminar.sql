﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Eliminar]
	@IdNumeroContrato INT
AS
BEGIN
	DELETE Contrato.UnidadMedida WHERE IdNumeroContrato = @IdNumeroContrato
END
