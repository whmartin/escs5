﻿CREATE PROCEDURE [dbo].[usp_Proveedor_Terceros_Consultar]
	@IDTIPODOCIDENTIFICA int = NULL,
	@IDESTADOTERCERO int = NULL,
	@IdTipoPersona int = NULL,
	@NUMEROIDENTIFICACION varchar(30) = NULL,
	@USUARIOCREA varchar(128) = NULL,
	@TERCERO varchar(128) =null
	
AS
BEGIN
	SELECT       ter.IDTERCERO, 
				 ter.IDTIPODOCIDENTIFICA, 
				 ter.IDESTADOTERCERO, 
				 ter.IdTipoPersona, 
                 ter.NUMEROIDENTIFICACION, 
                 ter.DIGITOVERIFICACION, 
                 ter.CORREOELECTRONICO, 
                 ter.PRIMERNOMBRE, 
                 ter.SEGUNDONOMBRE, 
                 ter.PRIMERAPELLIDO, 
                 ter.SEGUNDOAPELLIDO, 
                 ter.RAZONSOCIAL, 
                 ter.FECHAEXPEDICIONID, 
                 ter.FECHANACIMIENTO, 
                 ter.SEXO, 
                 ter.FECHACREA, 
                 ter.USUARIOCREA, 
                 ter.FECHAMODIFICA, 
                 ter.USUARIOMODIFICA, 
                 Global.TiposDocumentos.NomTipoDocumento AS NombreTipoIdentificacionPersonaNatural, 
                 Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural, 
                 EstadoTercero.CodigoEstadotercero, 
                 EstadoTercero.DescripcionEstado, 
                 TipoPersona.CodigoTipoPersona, 
                 TipoPersona.NombreTipoPersona,
				 ter.ProviderUserKey
	FROM         tercero.TERCERO ter  INNER JOIN Global.TiposDocumentos ON  ter.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento 
						      INNER JOIN tercero.EstadoTercero			ON  ter.IDESTADOTERCERO = tercero.EstadoTercero.IdEstadoTercero 
						      INNER JOIN tercero.TipoPersona			ON  ter.IdTipoPersona = tercero.TipoPersona.IdTipoPersona
	WHERE 
				ter.IDTIPODOCIDENTIFICA = CASE WHEN @IDTIPODOCIDENTIFICA IS NULL THEN ter.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA END
				AND ter.IDESTADOTERCERO = CASE WHEN @IDESTADOTERCERO IS NULL THEN ter.IDESTADOTERCERO ELSE @IDESTADOTERCERO END
				AND ter.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN ter.IdTipoPersona ELSE @IdTipoPersona END 
				AND ter.NUMEROIDENTIFICACION = CASE WHEN @NUMEROIDENTIFICACION IS NULL THEN ter.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION END 
				AND ter.USUARIOCREA = CASE WHEN @USUARIOCREA IS NULL THEN ter.USUARIOCREA ELSE @USUARIOCREA END 
		
	
     
END

