﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Consultar]
	@IdEstadoDatosBasicos INT
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
    FROM [Proveedor].[EstadoDatosBasicos] 
    WHERE  IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

