﻿
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que guarda un nuevo TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Insertar]
		@IdTipoUsuario INT OUTPUT, 	@CodigoTipoUsuario NVARCHAR(128),	@NombreTipoUsuario NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.TipoUsuario(CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoUsuario, @NombreTipoUsuario, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoUsuario = @@IDENTITY 		
END

