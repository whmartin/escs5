﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Consultar]
	@IdTipoGarantia INT
AS
BEGIN
 SELECT IdTipoGarantia, NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoGarantia] WHERE  IdTipoGarantia = @IdTipoGarantia
END

