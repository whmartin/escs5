﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que actualiza un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Modificar]
		@IdCobertura INT,	@IdCentroZonal INT,	@IdMunicipio INT,	@IdVigencia INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE DIV.Cobertura SET IdCentroZonal = @IdCentroZonal, IdMunicipio = @IdMunicipio, IdVigencia = @IdVigencia, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdCobertura = @IdCobertura
END

