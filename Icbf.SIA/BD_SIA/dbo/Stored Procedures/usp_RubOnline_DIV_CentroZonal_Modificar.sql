﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que actualiza un(a) CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_Modificar]
		@IdCentroZonal INT
		,	@IdMunicipio INT
		,	@IdRegional INT
		,	@CodigoMunicipio NVARCHAR(5)
		,	@CodigoCentroZonal NVARCHAR(4)
		,	@NombreCentroZonal NVARCHAR(45)
		,	@Direccion NVARCHAR(100)
		,	@Telefonos NVARCHAR(100)
		,	@Estado NVARCHAR(1)
		, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE 
		DIV.CentroZonal SET IdMunicipio = @IdMunicipio
		, IdRegional = @IdRegional
		, CodigoMunicipio = @CodigoMunicipio
		, CodigoCentroZonal = @CodigoCentroZonal
		, NombreCentroZonal = @NombreCentroZonal
		, Direccion = @Direccion
		, Telefonos = @Telefonos
		, Estado = @Estado
		, UsuarioModifica = @UsuarioModifica
		, FechaModifica = GETDATE() 
	WHERE IdCentroZonal = @IdCentroZonal
END

