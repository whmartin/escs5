﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que guarda un nuevo Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Insertar]
		@IdRegional INT OUTPUT, 	@CodigoRegional NVARCHAR(128),	@NombreRegional NVARCHAR(256), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.Regional(CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoRegional, @NombreRegional, @UsuarioCrea, GETDATE())
	SELECT @IdRegional = @@IDENTITY 		
END

