﻿


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Consultar]
	@idEntidad INT
AS
BEGIN

	 SELECT E.IdExpCOD, E.IdTipoCodUNSPSC,T.CodigoClase,T.Codigo as CodUNSPSC, IdExpEntidad,T.NombreClase, E.UsuarioCrea, E.FechaCrea, E.UsuarioModifica, E.FechaModifica,
		T.Codigo, T.Descripcion 
	 FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] E
	 INNER JOIN [Proveedor].[TipoCodigoUNSPSC] T ON E.IdTipoCodUNSPSC=T.IdTipoCodUNSPSC
	 WHERE IdExpEntidad = ISNULL(@idEntidad, IdExpEntidad)

END

