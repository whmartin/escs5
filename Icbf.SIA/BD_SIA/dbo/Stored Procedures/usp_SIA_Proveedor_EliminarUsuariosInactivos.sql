﻿





-- ==========================================================================================
-- Author:		 Bayron Lara
-- Create date:  14/06/2013 11:38:19 AM
-- Description:	 Se crea para eliminar los usuarios incativos de la base de datos SIA
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_EliminarUsuariosInactivos]

@UserId NVARCHAR(256)

AS
BEGIN

	DELETE [SEG].[Usuario]
	WHERE [providerKey] = @UserId
	
END--FIN PP




