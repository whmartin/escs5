﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 2:14:48 PM
-- Description:	Procedimiento almacenado que actualiza un(a) ObligacionesContractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ObligacionesContractuales_Modificar]
		@IdObligacionContractual INT,	@IdDatosAdmonObligaciones INT,	@ObligacionContractual NVARCHAR(250),	@Estado INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.ObligacionesContractuales SET IdDatosAdmonObligaciones = @IdDatosAdmonObligaciones, ObligacionContractual = @ObligacionContractual, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObligacionContractual = @IdObligacionContractual
END
