﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  7/22/2015 2:15:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) CuentasdeCobro
-- [usp_Sigepcyp_SPCP_CuentasdeCobros_Consultar] null, false, null, null, 72202092
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobros_ConsultarList]
	@Vigencia INT = NULL,@IdEstadoGestion BIT = NULL,@FechaCrea DATETIME = NULL,@IdCuenta INT = NULL, @NumeroDocumento VARCHAR(20) = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@Vigencia INT,@IdEstadoGestion INT,@FechaCrea DATETIME,@IdCuenta INT,@NumeroDocumento VARCHAR(20)'
Set @SqlExec = '
				SELECT	cc.IdCuenta,
						cc.IdEstadoPaso, 
						cc.IdDatosAdministracion,
						da.NomTipoDocSoporte TipoDocumento, 
						da.NumDocSoporte NumeroDocumento, 
						da.FechaDocSoporte FechaDocumento,
						da.CodCompromiso,
						r.NombreRegional Regional,
						h.Descripcion Devolucion
				FROM	[SPCP].[CuentasdeCobro] cc
						INNER JOIN [SPCP].[DatosAdministracion] da
							ON cc.IdDatosAdministracion = da.IdDatosAdministracion
						INNER JOIN [Global].[Areas] a
							ON da.IdArea = a.IdArea
						INNER JOIN [DIV].[Regional] r
							ON a.IdRegional = r.IdRegional
						LEFT JOIN [SPCP].[HistoricoEstadoCuentasDeCobro] h
							ON h.IDCuentaCobro = cc.IdCuenta
							AND h.IdHistoricoEstadoCuentasDeCobro in
							(
								SELECT	max(IdHistoricoEstadoCuentasDeCobro)
								FROM	[SPCP].[HistoricoEstadoCuentasDeCobro] 
								WHERE	IDCuentaCobro = cc.IdCuenta
							)
				WHERE	1=1 '
						If(@Vigencia Is Not Null) Set @SqlExec = @SqlExec + ' And da.Vigencia = @Vigencia' 
						If(@IdEstadoGestion Is Not Null) Set @SqlExec = @SqlExec + ' And cc.IdEstadoGestion = @IdEstadoGestion' 
						If(@FechaCrea Is Not Null) Set @SqlExec = @SqlExec + ' And CONVERT (char(10), cc.FechaCrea, 103) = CONVERT (char(10), @FechaCrea, 103)' 
						If(@IdCuenta Is Not Null) Set @SqlExec = @SqlExec + ' And cc.IdCuenta = @IdCuenta' 
						If(@NumeroDocumento Is Not Null) Set @SqlExec = @SqlExec + ' And da.NumDocIdentidad = @NumeroDocumento' 
												
Exec sp_executesql  @SqlExec, @SqlParametros,@Vigencia,@IdEstadoGestion,@FechaCrea,@IdCuenta,@NumeroDocumento
END

