﻿
-- =============================================
-- Author:		Jorge Vizcaino
-- Create date:  7/18/2015 3:21:49 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Planilla
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Planilla_Insertar]
		@IDPlanilla INT OUTPUT, 	
		@IdRegional INT,	
		@IdArea INT, 
		@UsuarioCrea NVARCHAR(250),
		@IdCuentaCobro NVARCHAR(800)
AS
BEGIN
 
	SET NOCOUNT ON;

	declare @numPlanilla int,
			@IDPasoActual int

	select @numPlanilla = isnull(max(NumeroPlanilla),0) + 1
	from SPCP.Planilla
	     
    BEGIN TRY
        BEGIN TRANSACTION

				INSERT INTO SPCP.Planilla(NumeroPlanilla,IdRegional, IdArea, UsuarioCrea, FechaCrea)
								  VALUES(@numPlanilla,@IdRegional, @IdArea, @UsuarioCrea, GETDATE())
				SELECT @IDPlanilla = @@IDENTITY 		

				INSERT INTO SPCP.PlanillaDetalle(IdCuenta,IdPlanilla,UsuarioCrea,FechaCrea)
				select ltrim(rtrim(data)),@IDPlanilla, @UsuarioCrea, GETDATE()
				from dbo.Split(@IdCuentaCobro,';')

				--update SPCP.CuentasDeCobro set IdEstadoPaso = dbo.ConsultarPasoFlujo(1,IdEstadoPaso)
				--Where IdCuenta in (SELECT data from  dbo.Split(@IdCuentaCobro,';'))

		COMMIT TRANSACTION
    END TRY        
    BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
        SELECT  @ErrorMessage = ERROR_MESSAGE()
        IF XACT_STATE() <> 0 
            ROLLBACK TRANSACTION
        RAISERROR (@ErrorMessage, 16, 1)         
    END CATCH


END
