﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacions_Consultar]
	@IdGestionObligacion INT = NULL,@IdGestionClausula INT = NULL,@NombreObligacion NVARCHAR(10) = NULL,@IdTipoObligacion INT = NULL,@Orden NVARCHAR(128) = NULL,@DescripcionObligacion NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdGestionObligacion, IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarObligacion] WHERE IdGestionObligacion = CASE WHEN @IdGestionObligacion IS NULL THEN IdGestionObligacion ELSE @IdGestionObligacion END AND IdGestionClausula = CASE WHEN @IdGestionClausula IS NULL THEN IdGestionClausula ELSE @IdGestionClausula END AND NombreObligacion = CASE WHEN @NombreObligacion IS NULL THEN NombreObligacion ELSE @NombreObligacion END AND IdTipoObligacion = CASE WHEN @IdTipoObligacion IS NULL THEN IdTipoObligacion ELSE @IdTipoObligacion END AND Orden = CASE WHEN @Orden IS NULL THEN Orden ELSE @Orden END AND DescripcionObligacion = CASE WHEN @DescripcionObligacion IS NULL THEN DescripcionObligacion ELSE @DescripcionObligacion END
END
