﻿

-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantia_Insertar]
@IDGarantia INT OUTPUT, 	
@IDContrato INT,	
@IDTipoGarantia INT,	
@IDterceroaseguradora INT,
@IDtercerosucursal INT,
@IDcontratistaContato INT,
@IDtipobeneficiario INT,
@NITICBF NVARCHAR(50),
@DescBeneficiario NVARCHAR(200),
@NumGarantia NVARCHAR(128),	
@FechaExpedicion DATETIME,	
@FechaVencInicial DATETIME,	
@FechaRecibo DATETIME,	
@FechaDevolucion DATETIME,	
@MotivoDevolucion NVARCHAR(128),	
@FechaAprobacion DATETIME,	
@FechaCertificacionPago DATETIME,	
@Estado BIT,	
@Anexo BIT,	
@ObservacionAnexo NVARCHAR(256), 
@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.Garantia(IDContrato, IDTipoGarantia, IDterceroaseguradora, IDtercerosucursal,IDcontratistaContato,IDtipobeneficiario,NITICBF,DescBeneficiario, NumGarantia, FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Estado, Anexo, ObservacionAnexo, UsuarioCrea, FechaCrea)
					  VALUES(@IDContrato, @IDTipoGarantia,@IDterceroaseguradora, @IDtercerosucursal,@IDcontratistaContato,@IDtipobeneficiario,@NITICBF ,@DescBeneficiario, @NumGarantia, @FechaExpedicion, @FechaVencInicial, @FechaRecibo, @FechaDevolucion, @MotivoDevolucion, @FechaAprobacion, @FechaCertificacionPago, @Estado, @Anexo, @ObservacionAnexo, @UsuarioCrea, GETDATE())
	SELECT @IDGarantia = @@IDENTITY 		
END



