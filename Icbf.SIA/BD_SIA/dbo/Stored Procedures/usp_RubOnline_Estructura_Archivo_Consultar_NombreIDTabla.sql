﻿
-- =============================================
-- Author:		Gonte\Jorge vizcaino
-- Create date: 2013-05-21
-- Description:	Procedimiento almacenado que consulta un(a) Archivo por id y nombre de tablas relacionados
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Consultar_NombreIDTabla]
	@idTabla varchar(256) = NULL,
	@NombreTabla varchar(256) = NULL
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , IdFormatoArchivo
      , FechaRegistro
      , NombreArchivo
      , ServidorFTP
      , Estado
      , ResumenCarga
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
      , NombreArchivoOri      
 FROM [Estructura].[Archivo] 
 WHERE idtabla = CASE WHEN @idTabla IS NULL THEN idtabla ELSE @idTabla END
 and	NombreTabla = CASE WHEN @NombreTabla IS NULL THEN NombreTabla ELSE @NombreTabla END
 
END


