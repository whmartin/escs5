﻿
/*
Modificado Por: Juan Carlos Valverde Sámano
Fecha:25-06-2014
Descripción: Consulta los documentos adjuntos para un tercero, apartir de de su
IdTipoPersona y IdTemporal
*/

CREATE procedure [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTerceros_Consultar_IdTemporal_TipoPersona] 
( @IdTemporal varchar(20), @TipoPersona varchar(128) )
as


declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'PROVEEDOR/GestionTercero';
DECLARE @TBLDOCS TABLE
(IDROW INT IDENTITY,
IdDocAdjunto INT,
IdTercero INT,
NombreDocumento NVARCHAR(256),
LinkDocumento NVARCHAR(256),
Obligatorio INT,
UsuarioCrea NVARCHAR(256),
FechaCrea DATETIME,
UsuarioModifica NVARCHAR(256),
FechaModifica DATETIME,
IdDocumento INT,
MaxPermitidoKB INT,
ExtensionesPermitidas NVARCHAR(50),
IdTemporal NVARCHAR(20))

INSERT INTO @TBLDOCS
SELECT	MAX(IdDocAdjunto) as IdDocAdjunto,
		MAX(IdTercero) as IdTercero,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		--Max(Observaciones) as Observaciones,
		Max(Obligatorio) as Obligatorio,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas,
		Max(IdTemporal) as IdTemporal
		
FROM 
(SELECT      d.IdDocAdjunto,
		 	 d.IdTercero,
			 t.Descripcion as NombreDocumento, 
             d.LinkDocumento,                       
             --d.Observaciones,
             --CASE substring(t .CodigoTipoDocumento, 1, 1) WHEN @TipoPersona THEN 1 ELSE 0 END AS Obligatorio,
             CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica
								WHEN '3' THEN tdp.ObligConsorcio
								WHEN '4' THEN tdp.ObligUnionTemporal END AS Obligatorio,
             t.IdTipoDocumento,
             d.UsuarioCrea,
			 d.FechaCrea,
			 d.UsuarioModifica,
			 d.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 d.IdTemporal
FROM         Proveedor.DocAdjuntoTercero AS d 
			INNER JOIN SEG.Programa AS p 
			INNER JOIN Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma 
			INNER JOIN Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento ON d.IdDocumento = tdp.IdTipoDocumento
WHERE     d.Activo=1 AND (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
		AND IdTemporal = @IdTemporal
		AND tdp.estado=1 
UNION                      
SELECT       0 as IdDocAdjunto,
			 0 as IdTercero,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
		--	 '' as Observaciones,
			 CASE @TipoPersona	WHEN '1' THEN tdp.ObligPersonaNatural 
								WHEN '2' THEN tdp.ObligPersonaJuridica 
								WHEN '3' THEN tdp.ObligConsorcio
								WHEN '4' THEN tdp.ObligUnionTemporal END AS Obligatorio,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas,
			 '' as IdTemporal
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
AND tdp.estado=1
) as Tabla
WHERE Obligatorio = 1
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento





DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBLDOCS)
DECLARE @nWhile Int --Almacenará la cantidad de veces que se esta recorriendo en el Bucle.
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SET @IdDoc=(SELECT IdDocAdjunto FROM @TBLDOCS WHERE IDROW=@nWhile)

UPDATE @TBLDOCS
SET LinkDocumento=ISNULL((SELECT LINKDOCUMENTO FROM PROVEEDOR.DocAdjuntoTercero WHERE IDDOCADJUNTO=@IdDoc),'')
WHERE IdDocAdjunto=@IdDoc

SET @nWhile=@nWhile+1
END

SELECT 
IdDocAdjunto,
IdTercero,
NombreDocumento,
LinkDocumento,
Obligatorio,
UsuarioCrea,
FechaCrea,
UsuarioModifica,
FechaModifica,
IdDocumento,
MaxPermitidoKB,
ExtensionesPermitidas,
IdTemporal
FROM @TBLDOCS




