﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Insertar]
		@IdTipodeentidadPublica INT OUTPUT, 	@CodigoTipodeentidadPublica NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipodeentidadPublica(CodigoTipodeentidadPublica, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipodeentidadPublica, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipodeentidadPublica = SCOPE_IDENTITY() 		
END


