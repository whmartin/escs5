﻿

-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013-07-
-- Description:	Procedimiento almacenado que cambia el estado del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Modificar_Estado]
	@IDTERCERO int ,
	@IDESTADOTERCERO int,
	@USUARIOMODIFICA varchar(100)
AS
BEGIN
	UPDATE 
		Oferente.TERCERO
	SET 
		Oferente.TERCERO.IDESTADOTERCERO = @IDESTADOTERCERO, 
		Oferente.TERCERO.USUARIOMODIFICA = @USUARIOMODIFICA,
		Oferente.TERCERO.FECHAMODIFICA = GETDATE()
	WHERE
		Oferente.TERCERO.IDTERCERO = @IDTERCERO
END


