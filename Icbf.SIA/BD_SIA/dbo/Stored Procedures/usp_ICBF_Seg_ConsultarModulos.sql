﻿-- =============================================
-- Author:		Yuri Gereda
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta un Módulo por Nombre y Estado
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarModulos]
	@pNombreModulo NVARCHAR(200) = NULL,
	@pEstado	   BIT = NULL
AS
BEGIN

SELECT 
	IdModulo, NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Modulo
WHERE 
	NombreModulo  LIKE CASE WHEN @pNombreModulo IS NULL THEN NombreModulo ELSE '%'+@pNombreModulo+'%' END
AND
	Estado = CASE WHEN @pEstado IS NULL THEN	Estado ELSE @pEstado END

END
