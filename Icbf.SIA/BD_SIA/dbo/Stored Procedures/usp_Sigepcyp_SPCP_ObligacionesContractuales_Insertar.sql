﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 2:14:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ObligacionesContractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ObligacionesContractuales_Insertar]
		@IdObligacionContractual INT OUTPUT, 	@IdDatosAdmonObligaciones INT,	@ObligacionContractual NVARCHAR(250),	@Estado INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.ObligacionesContractuales(IdDatosAdmonObligaciones, ObligacionContractual, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdDatosAdmonObligaciones, @ObligacionContractual, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObligacionContractual = @@IDENTITY 		
END
