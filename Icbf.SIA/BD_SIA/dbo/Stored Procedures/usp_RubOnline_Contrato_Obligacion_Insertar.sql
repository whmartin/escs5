﻿
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que inserta Obligacion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Insertar]
		@IdObligacion INT OUTPUT, 	
		@IdTipoObligacion INT,	
		@IdTipoContrato INT,	
		@Descripcion nvarchar(MAX),	
		@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.Obligacion(IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoObligacion, @IdTipoContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdObligacion = @@IDENTITY 		
END



