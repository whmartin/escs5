﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Eliminar]
	@IdTipoObligacion INT
AS
BEGIN
	DELETE Contrato.TipoObligacion WHERE IdTipoObligacion = @IdTipoObligacion
END

