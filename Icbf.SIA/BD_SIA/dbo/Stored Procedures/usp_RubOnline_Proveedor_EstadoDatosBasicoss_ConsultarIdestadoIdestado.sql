﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicoss_ConsultarIdestadoIdestado]
	@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoDatosBasicos, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
     FROM [Proveedor].[EstadoDatosBasicos] 
        WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

