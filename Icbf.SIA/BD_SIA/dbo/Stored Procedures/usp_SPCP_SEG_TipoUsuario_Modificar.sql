﻿
CREATE PROCEDURE [dbo].[usp_SPCP_SEG_TipoUsuario_Modificar]
		@IdTipoUsuario INT,	@CodigoTipoUsuario NVARCHAR(128),	@NombreTipoUsuario NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.TipoUsuario 
	   SET CodigoTipoUsuario = @CodigoTipoUsuario, 
		   NombreTipoUsuario = @NombreTipoUsuario, 
		   Estado = @Estado, UsuarioModifica = @UsuarioModifica, 
		   FechaModifica = GETDATE() 
	 WHERE IdTipoUsuario = @IdTipoUsuario
END

