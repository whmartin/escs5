﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/6/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que elimina un(a) PlanDePagos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_PlanDePagos_Eliminar]
	@IdPlanDePagos INT
AS
BEGIN
	DELETE SPCP.PlanDePagos WHERE IdPlanDePagos = @IdPlanDePagos
END
