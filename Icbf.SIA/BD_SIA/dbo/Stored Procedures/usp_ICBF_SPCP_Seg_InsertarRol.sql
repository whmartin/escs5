﻿
-- =============================================
-- Author:		Zulma.Barrantes
-- Create date:  30/11/2015
-- Description:	Incluye los campos IdPaso, AprobacionPAC 
-- Grupo de Apoyo SSII - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_InsertarRol]
		  @IdRol INT OUTPUT
		, @ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(250)
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioCreacion NVARCHAR(250)
		, @IdPaso int
		, @AprobacionPAC BIT 
		
		
		
AS
BEGIN

	IF EXISTS(SELECT * FROM SEG.Rol WHERE Nombre = @Nombre )
	BEGIN
		RAISERROR ('Rol ya existe, por favor verifique', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	    RETURN
	END

	INSERT INTO SEG.Rol([providerKey], [Nombre], [Descripcion], [Estado],[UsuarioCreacion], [FechaCreacion], [UsuarioModificacion],[FechaModificacion],[EsAdministrador],[IdPaso],[AprobacionPAC])
			      VALUES(@ProviderKey, @Nombre, @Descripcion, @Estado,@UsuarioCreacion, GETDATE(),null,null,0,@IdPaso, @AprobacionPAC)
	SELECT @IdRol = @@IDENTITY 					  
END

