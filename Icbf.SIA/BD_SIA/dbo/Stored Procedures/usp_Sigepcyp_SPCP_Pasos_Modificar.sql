﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/18/2015 1:47:11 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Pasos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Pasos_Modificar]
		@IdPaso INT,	@Paso NVARCHAR(50),	@Estado INT,	@Financiera INT,	@IdAccion INT,	@AccionEstado INT,	@Obligatorio INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.Pasos SET Paso = @Paso, Estado = @Estado, Financiera = @Financiera, IdAccion = @IdAccion, AccionEstado = @AccionEstado, Obligatorio = @Obligatorio, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdPaso = @IdPaso
END
