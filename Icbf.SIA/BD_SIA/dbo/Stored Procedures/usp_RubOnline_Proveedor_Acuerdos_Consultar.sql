﻿
-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  27/06/2013 
-- Description:	Procedimiento almacenado que consulta un(a) Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Consultar]
@IdAcuerdo INT
AS
BEGIN
SELECT
	IdAcuerdo,
	Nombre,
	ContenidoHtml,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[Acuerdos]
WHERE IdAcuerdo = @IdAcuerdo
END



