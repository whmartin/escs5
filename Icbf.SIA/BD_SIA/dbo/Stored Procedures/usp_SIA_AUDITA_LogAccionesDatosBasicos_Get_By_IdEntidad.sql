﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 07/NOV/2014
-- Description:	Obtiene la auditoria de acciones en Datos Basicos de una EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_By_IdEntidad]
@IdEntidad INT
AS
BEGIN
	
	SELECT 
	IdAccion
	,IdEntidad
	,Fecha
	,Usuario
	,IdSistema
	,Sistema
	,Accion
	 FROM AUDITA.LogAccionesDatosBasicos
	 WHERE IdEntidad=@IdEntidad
	
END

