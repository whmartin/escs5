﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Consultar]
	@IdInformacionPresupuestalRP INT
AS
BEGIN
 SELECT IdInformacionPresupuestalRP, FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestalRP] WHERE  IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END

