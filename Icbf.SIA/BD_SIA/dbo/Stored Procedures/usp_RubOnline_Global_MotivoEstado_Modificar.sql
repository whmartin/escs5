﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que actualiza un(a) MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Modificar]
		@IdMotivoEstado INT,	@CodigoMotivoEstado NVARCHAR(128),	@NombreMotivoEstado NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.MotivoEstado SET CodigoMotivoEstado = @CodigoMotivoEstado, NombreMotivoEstado = @NombreMotivoEstado, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdMotivoEstado = @IdMotivoEstado
END


