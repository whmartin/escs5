﻿






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoDatosBasicosEntidad, v.IdEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoDatosBasicosEntidad v
			left join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			ORDER BY v.FechaCrea DESC
END








