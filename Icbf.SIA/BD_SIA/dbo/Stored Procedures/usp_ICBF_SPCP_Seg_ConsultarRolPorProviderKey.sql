﻿
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta un rol
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarRolPorProviderKey]
	@ProviderKey varchar(125)
AS
BEGIN
  SELECT [IdRol]
	   ,[providerKey]
	   ,[Nombre]
	   ,[Descripcion]
	   ,[Estado]
       ,[UsuarioCreacion]
       ,[FechaCreacion]
       ,[UsuarioModificacion]
       ,[FechaModificacion]
  FROM SEG.Rol
  WHERE
	providerKey = @ProviderKey
END

