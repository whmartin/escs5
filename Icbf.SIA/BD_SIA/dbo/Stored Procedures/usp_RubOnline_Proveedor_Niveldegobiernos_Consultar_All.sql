﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_All]
	
AS
BEGIN

 SELECT IdNiveldegobierno, CodigoNiveldegobierno, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[Niveldegobierno] 
 WHERE  Estado =1
 
 END

