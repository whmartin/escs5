﻿-- =============================================
-- Author:		Oscar Javier  Sosa Parada
-- Create date:  15/11/2012
-- Description:	Procedimiento almacenado que elimina programas permitidos a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarProgramasRol]
	@pIdRol INT
AS
BEGIN
	DELETE FROM SEG.ProgramaRol WHERE IdRol = @pIdRol
END
