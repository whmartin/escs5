﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar]
	@IdValidarInfoDatosBasicosEntidad INT
AS
BEGIN
 SELECT IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] WHERE  IdValidarInfoDatosBasicosEntidad = @IdValidarInfoDatosBasicosEntidad
END




