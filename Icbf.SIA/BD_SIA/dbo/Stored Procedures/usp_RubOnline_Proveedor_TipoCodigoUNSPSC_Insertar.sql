﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Insertar]
		@IdTipoCodUNSPSC INT OUTPUT, @Codigo NVARCHAR(64),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoCodigoUNSPSC(Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Codigo, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCodUNSPSC = SCOPE_IDENTITY() 		
END


