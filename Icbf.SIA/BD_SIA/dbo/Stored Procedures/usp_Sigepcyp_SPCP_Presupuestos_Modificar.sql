﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  3/12/2015 2:21:15 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Presupuestos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Presupuestos_Modificar]
		@IdPresupuesto INT,	@IdDatosAdministracion NVARCHAR(255),	@CodPosicionGasto NVARCHAR(255),	
		@NomPosicionGasto NVARCHAR(255),	@NomFuenteFinanciacion NVARCHAR(255),	
		@CodRecursoPresupuestal INT,	@NomRecursoPresupuestal NVARCHAR(255),	@NomSituacionFondos NVARCHAR(255), 
		@ValorInicialRubro decimal=NULL,
		@ValorActualRubro decimal=NULL,
		@ValorSaldoRubro decimal=NULL,
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.Presupuestos SET IdDatosAdministracion = @IdDatosAdministracion, 
	CodPosicionGasto = @CodPosicionGasto, NomPosicionGasto = @NomPosicionGasto, NomFuenteFinanciacion = @NomFuenteFinanciacion, 
	CodRecursoPresupuestal = @CodRecursoPresupuestal, NomRecursoPresupuestal = @NomRecursoPresupuestal, 
	NomSituacionFondos = @NomSituacionFondos, 
	ValorInicialRubro=@ValorInicialRubro,ValorActualRubro=@ValorActualRubro,ValorSaldoRubro=@ValorSaldoRubro,
	UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdPresupuesto = @IdPresupuesto
END
