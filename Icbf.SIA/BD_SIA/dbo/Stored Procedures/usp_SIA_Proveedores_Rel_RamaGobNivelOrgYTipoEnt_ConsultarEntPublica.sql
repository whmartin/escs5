﻿-- =============================================
-- Author:		Leticia Elizabeth González
-- Create date:  01/04/2014 17:31 PM
-- Description:	Procedimiento almacenado que consulta TipoEntidadPublica por RamaEstructura, NivelGobierno y NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarEntPublica]

	@IdRamaEstructura INT, @IdNivelGobierno INT, @IdNivelOrganizacional INT
AS
BEGIN

	SELECT	RelRamaGob.IdTipodeentidadPublica,
			CodigoTipodeentidadPublica,
			Descripcion
	FROM	PROVEEDOR.Rel_RamaGobNivelOrgYTipoEnt RelRamaGob
	JOIN
			Proveedor.TipodeentidadPublica TipoEntidad
	ON
			RelRamaGob.IdTipodeentidadPublica = TipoEntidad.IdTipodeentidadPublica
	WHERE	
			RelRamaGob.IdRamaEstructura = @IdRamaEstructura
	AND
			RelRamaGob.IdNivelGobierno = @IdNivelGobierno
	AND
			RelRamaGob.IdNivelOrganizacional = @IdNivelOrganizacional
	ORDER BY
		Descripcion ASC

END