﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/20/2013 10:09:07 PM
-- Description:	Procedimiento almacenado que consulta un(a) SecuenciaNumeroProceso
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_SecuenciaNumeroProceso_Generar]
	@NumeroProceso NVARCHAR(25) output, @CodigoRegional INT ,@Sigla nvarchar(3), @AnoVigencia INT, @UsuarioCrea NVARCHAR(250) 
AS
BEGIN
	DECLARE @Consecutivo INT
	DECLARE @AnoVigenciaTransformado NVARCHAR(2)
	DECLARE @DelimitadorSecuencia NVARCHAR(1)

	SET @DelimitadorSecuencia = '.'
	/*Validar esta creada un consecutivo por CodigoRegional y AnoVigencia*/
	if not exists(SELECT * FROM [Contrato].[SecuenciaNumeroProceso] WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia)
	BEGIN
			SET @Consecutivo = 1	
	END
	ELSE
	BEGIN
			SELECT @Consecutivo = MAX(Consecutivo) + 1 
			FROM [Contrato].[SecuenciaNumeroProceso] 
			WHERE CodigoRegional = @CodigoRegional AND AnoVigencia = @AnoVigencia
	END
	
	/*
		Armar numero contrato
	*/
		
	SET @AnoVigenciaTransformado = CASE WHEN LEN(CONVERT(NVARCHAR(4),@AnoVigencia)) = 4 THEN SUBSTRING(CONVERT(NVARCHAR(4),@AnoVigencia),3,2) ELSE CONVERT(NVARCHAR(4),@AnoVigencia) END
	
	SELECT @NumeroProceso = right( '00' + cast( @CodigoRegional AS varchar(2)), 2 ) + @DelimitadorSecuencia + @Sigla + @DelimitadorSecuencia + @AnoVigenciaTransformado + @DelimitadorSecuencia + right( '00000' + cast( @Consecutivo AS varchar(5)), 5 )   

	print @NumeroProceso

	/*Registra el la secuencia*/
	 IF @NumeroProceso IS NOT NULL
	 BEGIN
		INSERT INTO Contrato.SecuenciaNumeroProceso(Consecutivo, CodigoRegional, AnoVigencia, UsuarioCrea, FechaCrea)
		VALUES(@Consecutivo, @CodigoRegional, @AnoVigencia, @UsuarioCrea, GETDATE())
	END
END
