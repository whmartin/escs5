﻿-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que actualiza un(a) NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Modificar]
		@IdNotJudicial INT,	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT,	
		@IdZona INT,	
		@Direccion NVARCHAR(250), 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.NotificacionJudicial 
	SET IdEntidad = @IdEntidad, 
	IdDepartamento = @IdDepartamento,
	IdMunicipio	= @IdMunicipio,
	IdZona = @IdZona,
	Direccion = @Direccion,
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdNotJudicial = @IdNotJudicial 
END
