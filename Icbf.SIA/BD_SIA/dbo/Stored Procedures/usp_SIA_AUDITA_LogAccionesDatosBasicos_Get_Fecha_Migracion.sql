﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 06/NOV/2014
-- Description:	Obtiene la Fecha de Migración de alguna Entidad.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_AUDITA_LogAccionesDatosBasicos_Get_Fecha_Migracion]
	@IdEntidad INT
	AS
BEGIN
SELECT Fecha FROM AUDITA.LogAccionesDatosBasicos
WHERE IdEntidad =@IdEntidad AND Accion='Migración'
END

