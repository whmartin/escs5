﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantias_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantias_Consultar]
@NombreTipoGarantia NVARCHAR (128) = NULL,
@Estado bit = null
AS
BEGIN
SELECT
	IdTipoGarantia,
	NombreTipoGarantia,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[TipoGarantia]
WHERE NombreTipoGarantia LIKE '%' + 
	CASE
		WHEN @NombreTipoGarantia IS NULL THEN NombreTipoGarantia ELSE @NombreTipoGarantia
	END + '%'
AND (Estado = @Estado OR @Estado IS NULL)
ORDER BY NombreTipoGarantia
END




