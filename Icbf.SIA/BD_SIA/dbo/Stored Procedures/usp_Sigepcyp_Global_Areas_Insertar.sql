﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/16/2015 11:22:03 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Areas
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_Areas_Insertar]
		@IdArea INT OUTPUT, 	@IdRegional INT,	@NombreArea NVARCHAR(50),	@CodArea INT,	@EstadoArea INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.Areas(IdRegional, NombreArea, CodArea, EstadoArea, UsuarioCrea, FechaCrea)
					  VALUES(@IdRegional, @NombreArea, @CodArea, @EstadoArea, @UsuarioCrea, GETDATE())
	SELECT @IdArea = @@IDENTITY 		
END
