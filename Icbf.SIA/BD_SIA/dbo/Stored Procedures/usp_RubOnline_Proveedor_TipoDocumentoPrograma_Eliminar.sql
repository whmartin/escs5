﻿-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Eliminar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
	UPDATE Proveedor.TipoDocumentoPrograma
	SET Estado = 0
	WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END
