﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Insertar]
		@IdAmparo INT OUTPUT, 	@IdGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@VigenciaHasta DATETIME,	@ValorParaCalculoAsegurado NUMERIC(18,3),	@IdTipoCalculo INT,	@ValorAsegurado NUMERIC(18,3), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.AmparosAsociados(IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea)
					  VALUES(@IdGarantia, @IdTipoAmparo, @FechaVigenciaDesde, @VigenciaHasta, @ValorParaCalculoAsegurado, @IdTipoCalculo, @ValorAsegurado, @UsuarioCrea, GETDATE())
	SELECT @IdAmparo = @@IDENTITY 		
END

