﻿-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que consulta un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Consultar]
	@IdCorreoElectronico NUMERIC(18,0)
AS
BEGIN
 SELECT IdCorreoElectronico, IdArchivo, Destinatario, Mensaje, Estado, FechaIngreso, FechaEnvio, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, TipoCorreo FROM [Estructura].[CorreoElectronico] WHERE  IdCorreoElectronico = @IdCorreoElectronico
END

