﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/1/2015 9:26:10 PM
-- Description:	Procedimiento almacenado que consulta un(a) FlujoPasosCuentaDetalle
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuentaDetalle_Consultar]
	@IdFlujoPasoCuentaDetalle INT
AS
BEGIN
 SELECT IdFlujoPasoCuentaDetalle, IdFlujoPasoCuenta, IdTipoCuenta, IdPaso, Orden, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
   FROM [SPCP].[FlujoPasosCuentaDetalle] 
  WHERE  IdFlujoPasoCuentaDetalle = @IdFlujoPasoCuentaDetalle
END

