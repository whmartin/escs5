﻿

-- =============================================
-- Author:		Zulma Barrantes
-- Create date:  25/06/2015 6:00:41 PM
-- Description:	Procedimiento almacenado que guarda el estado de cada módulo por proveedor
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Proveedores_EstadoModulo]

AS
BEGIN

BEGIN TRANSACTION t1


---experiencias


delete from [Proveedor].[EntidadModuloEstado] where [IdModulo]= 4

 INSERT INTO [Proveedor].[EntidadModuloEstado]
		select	t.IdEntidad, 4, t.IdEstado, max(FechaEstado) as FechaEstado,'RepGenProv',GETDATE() 
		from	(
				select	distinct
						ie.IdEntidad
						,case when t.IdEntidad is null then 4 else 3 end							as IdEstado
						,convert(varchar,cast(isnull(ie.FechaModifica,ie.FechaCrea)	 as date),103)	as FechaEstado
				from	Proveedor.InfoExperienciaEntidad ie with(nolock)
						left join
						(
						select	distinct ie2.IdEntidad
						from	Proveedor.InfoExperienciaEntidad ie2 with(nolock)
						where	ie2.Finalizado = 0
						) as t
						on t.IdEntidad = ie.IdEntidad
				) t
		group by t.IdEntidad,t.IdEstado
		order by t.IdEntidad
		

--------------------------------------------------------------------------------------------------------------

		---Financiero
		delete from [Proveedor].[EntidadModuloEstado] where [IdModulo]= 3

		INSERT INTO [Proveedor].[EntidadModuloEstado]
		select	t.IdEntidad, 3, t.IdEstado, max(FechaEstado) as FechaEstado,'RepGenProv',GETDATE() 
		from	(
				select	distinct
						ie.IdEntidad
						,case when t.IdEntidad is null then 4 else 3 end							as IdEstado
						,convert(varchar,cast(isnull(ie.FechaModifica,ie.FechaCrea)	 as date),103)	as FechaEstado
				from	Proveedor.InfoFinancieraEntidad ie with(nolock)
						left join
						(
						select	distinct ie2.IdEntidad
						from	Proveedor.InfoFinancieraEntidad ie2 with(nolock)
						where	ie2.Finalizado = 0
						) as t
						on t.IdEntidad = ie.IdEntidad
				) t
		group by t.IdEntidad,t.IdEstado
		order by t.IdEntidad

-----------------------------------------------------------------------------------------------------------------
		---Datos Básicos
		delete from [Proveedor].[EntidadModuloEstado] where [IdModulo]= 1

		INSERT INTO [Proveedor].[EntidadModuloEstado]
		select	t.IdEntidad, 1, t.IdEstado, max(FechaEstado) as FechaEstado,'RepGenProv',GETDATE() 
		from	(
				select	distinct
						ie.IdEntidad
						,case when t.IdEntidad is null then 4 else 3 end							as IdEstado
						,convert(varchar,cast(isnull(ie.FechaModifica,ie.FechaCrea)	 as date),103)	as FechaEstado
				from	Proveedor.ValidarInfoDatosBasicosEntidad ie with(nolock)
						left join
						(
						select	distinct ie2.IdEntidad
						from	Proveedor.ValidarInfoDatosBasicosEntidad ie2 with(nolock)
						where	ie2.ConfirmaYAprueba = 0
						) as t
						on t.IdEntidad = ie.IdEntidad
				) t
		group by t.IdEntidad,t.IdEstado
		order by t.IdEntidad



COMMIT TRANSACTION t1

END

