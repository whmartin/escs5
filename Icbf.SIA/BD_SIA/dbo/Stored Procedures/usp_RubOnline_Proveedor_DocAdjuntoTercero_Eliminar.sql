﻿-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocAdjuntoTercero WHERE IdDocAdjunto = @IdDocAdjunto 
END

