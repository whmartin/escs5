﻿
--- =================================================================
-- Author:      Grupo de Apoyo - Desarrollo Caja Menor
-- Create date:  9/17/2015 2:58:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoIncremento
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ConsultarGestionCuentaCobro]
	@NumDocIdentidad NVARCHAR(20) = NULL, @IdRegional int, @IDCuentaCobro int = 0
AS
BEGIN

select  
	   hcc.IDCuentaCobro
	   ,r.IdRegional
	   ,hcc.Roltransaccion
	   ,hcc.UsuarioCrea
	   ,hcc.FechaCrea
	   ,da.NumDocIdentidad
	   ,da.NomTercero	   
 from SPCP.HistoricoEstadoCuentasDeCobro hcc
 INNER JOIN SPCP.CuentasdeCobro cc on cc.IdCuenta = hcc.IDCuentaCobro
 INNER JOIN  SPCP.DatosAdministracion da on da.IdDatosAdministracion = cc.IdDatosAdministracion
 INNER join DIV.Regional r on da.Codpci = r.codPCI
 where hcc.Aprobado=1
   --and da.NumDocIdentidad = @NumDocIdentidad
   --and da.Codpci = @IdRegional
   --and hcc.IDCuentaCobro = @IDCuentaCobro
 and hcc.IDCuentaCobro = 3
 
end 