﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Modulo
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_InsertarModulo]
		@IdModulo INT OUTPUT, 	@NombreModulo NVARCHAR(250),	@Posicion INT,	@Estado BIT, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Modulo(NombreModulo, Posicion, Estado, UsuarioCreacion, FechaCreacion)
					  VALUES(@NombreModulo, @Posicion, @Estado, @UsuarioCreacion, GETDATE())
	SELECT @IdModulo = @@IDENTITY 		
END

