﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que consulta un(a) AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AccionPasos_Consultar]
	@IdPaso INT = NULL,@Estado BIT = NULL,@Descripcion NVARCHAR(160) = NULL,@IdAccion INT = NULL,@Aceptar BIT = NULL,@Rechazar BIT = NULL
AS
BEGIN
	Declare @SqlExec as NVarchar(Max)
	Declare @SqlParametros as NVarchar(3000)=','
	SET @SqlParametros = '@IdPaso INT,@Estado BIT,@Descripcion NVARCHAR(160),@IdAccion INT,@Aceptar BIT,@Rechazar BIT'
	SET @SqlExec = '
	 SELECT IdAccionPaso, IdPaso, Estado, Descripcion, IdAccion, Aceptar, Rechazar, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[AccionPaso] WHERE 1=1 '
	IF (@IdPaso IS NOT NULL) SET @SqlExec = @SqlExec + ' And IdPaso = @IdPaso'
	IF (@Estado IS NOT NULL) SET @SqlExec = @SqlExec + ' And Estado = @Estado'
	IF (@Descripcion IS NOT NULL) SET @SqlExec = @SqlExec + ' And Descripcion = @Descripcion'
	IF (@IdAccion IS NOT NULL) SET @SqlExec = @SqlExec + ' And IdAccion = @IdAccion'
	IF (@Aceptar IS NOT NULL) SET @SqlExec = @SqlExec + ' And Aceptar = @Aceptar'
	IF (@Rechazar IS NOT NULL) SET @SqlExec = @SqlExec + ' And Rechazar = @Rechazar'
	EXEC sp_executesql	@SqlExec,
						@SqlParametros,
						@IdPaso,
						@Estado,
						@Descripcion,
						@IdAccion,
						@Aceptar,
						@Rechazar
END