﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/3/2015 9:34:19 AM
-- Description:	Procedimiento almacenado que guarda un nuevo ProductosContractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ProductosContractuales_Insertar]
		@IdProductoContractual INT OUTPUT, 	@IdObligacionContractual INT,	@ProductoContractual NVARCHAR(500),	@NumeroPago INT,	@Estado INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.ProductosContractuales(IdObligacionContractual, ProductoContractual, NumeroPago, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdObligacionContractual, @ProductoContractual, @NumeroPago, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdProductoContractual = @@IDENTITY 		
END
