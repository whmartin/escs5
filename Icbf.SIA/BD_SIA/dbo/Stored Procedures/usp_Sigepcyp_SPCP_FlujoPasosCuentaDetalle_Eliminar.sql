﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  5/31/2015 3:21:43 PM
-- Description:	Procedimiento almacenado que elimina un(a) FlujoPasosCuentaDetalle
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuentaDetalle_Eliminar]
	@IdFlujoPasoCuentaDetalle INT,@IdFlujoPasoCuenta INT
AS
BEGIN
	DELETE SPCP.FlujoPasosCuentaDetalle 
	 WHERE IdFlujoPasoCuentaDetalle = @IdFlujoPasoCuentaDetalle 
	   AND IdFlujoPasoCuenta = @IdFlujoPasoCuenta
END

