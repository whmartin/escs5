﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Eliminar]
	@IdTipoGarantia INT
AS
BEGIN
	DELETE Contrato.TipoGarantia WHERE IdTipoGarantia = @IdTipoGarantia
END

