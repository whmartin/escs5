﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Eliminar]
	@IdModalidad INT
AS
BEGIN
	DELETE Contrato.ModalidadSeleccion WHERE IdModalidad = @IdModalidad
END

