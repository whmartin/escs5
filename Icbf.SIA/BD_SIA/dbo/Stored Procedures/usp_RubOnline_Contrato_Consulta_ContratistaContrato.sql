﻿
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  08/01/2013 11:48:15 AM
-- Description:	Procedimiento almacenado Consulta los Contratista asociados a un contrato
-- =============================================


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Consulta_ContratistaContrato]
@IDCONTRATO INT
AS

	SELECT 
	Contrato.RelacionarContratistas.IdContratistaContrato ,
	Contrato.RelacionarContratistas.NumeroIdentificacion,
	Oferente.TERCERO.RAZONSOCIAL,
	CASE Contrato.RelacionarContratistas.ClaseEntidad
		WHEN 1 THEN 'Contratista'
		WHEN 2 THEN 'Consorcio/Unión Temporal'
	END ENTIDAD
	FROM
	Contrato.RelacionarContratistas,
	Oferente.TERCERO
	where
	Oferente.TERCERO.NUMEROIDENTIFICACION = Contrato.RelacionarContratistas.NUMEROIDENTIFICACION
	AND IDCONTRATO = @IDCONTRATO



