﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Insertar]
		@IdTipoAmparo INT OUTPUT, 	
		@NombreTipoAmparo NVARCHAR(128),	@IdTipoGarantia INT,	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoAmparo(NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoAmparo, @IdTipoGarantia, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoAmparo = @@IDENTITY 		
END


