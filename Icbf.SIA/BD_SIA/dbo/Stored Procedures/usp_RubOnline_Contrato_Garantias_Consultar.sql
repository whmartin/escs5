﻿
-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- EXEC [usp_RubOnline_Contrato_Garantias_Consultar]
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantias_Consultar]
@IDGarantia INT = NULL,
@NumGarantia NVARCHAR (128) = NULL,
@NumContrato NUMERIC (25) = NULL,
@Estado bit = null,
@IDterceroaseguradora INT = NULL
AS
BEGIN
SELECT
	[Contrato].[Garantia].IDGarantia,
	[Contrato].[Garantia].IDContrato,
	[Contrato].[Garantia].IDTipoGarantia,
	[Contrato].[Garantia].IDterceroaseguradora,
	[Contrato].[Garantia].IDtercerosucursal,
	[Contrato].[Garantia].IDcontratistaContato,
	[Contrato].[Garantia].IDtipobeneficiario,
	[Contrato].[Garantia].NITICBF,
	[Contrato].[Garantia].DescBeneficiario,
	[Contrato].[Garantia].NumGarantia,
	[Contrato].[Garantia].FechaExpedicion,
	[Contrato].[Garantia].FechaVencInicial,
	[Contrato].[Garantia].FechaRecibo,
	[Contrato].[Garantia].FechaDevolucion,
	[Contrato].[Garantia].MotivoDevolucion,
	[Contrato].[Garantia].FechaAprobacion,
	[Contrato].[Garantia].FechaCertificacionPago,
	[Contrato].[Garantia].Valor,
	[Contrato].[Garantia].Valortotal,
	[Contrato].[Garantia].Estado,
	[Contrato].[Garantia].Anexo,
	[Contrato].[Garantia].ObservacionAnexo,
	[Contrato].[Garantia].UsuarioCrea,
	[Contrato].[Garantia].FechaCrea,
	[Contrato].[Garantia].UsuarioModifica,
	[Contrato].[Garantia].FechaModifica,
	Oferente.TERCERO.RAZONSOCIAL RAZONSOCIAL_ASEGURADORA,
	Oferente.TERCERO.PRIMERNOMBRE,
	Oferente.TERCERO.SEGUNDONOMBRE,
	Oferente.TERCERO.PRIMERAPELLIDO,
	Oferente.TERCERO.SEGUNDOAPELLIDO,
	contrato.contrato.NumeroContrato,
	Contrato.RelacionarContratistas.NumeroIdentificacion,
	CASE CONTRATISTA.IdTipoPersona
		WHEN 2 THEN CONTRATISTA.RAZONSOCIAL
		WHEN 1 THEN CONTRATISTA.PRIMERNOMBRE + ' ' + CONTRATISTA.PRIMERAPELLIDO
	END RAZONSOCIAL_CONTRATISTA,
	Oferente.TERCERO.NUMEROIDENTIFICACION NIT_ASEGURADORA
FROM	[Contrato].[Garantia],
		Oferente.TERCERO,
		Oferente.TERCERO CONTRATISTA,
		contrato.contrato,
		Contrato.RelacionarContratistas
WHERE ([Contrato].[Garantia].IDGarantia = @IDGarantia OR @IDGarantia IS NULL)
AND Oferente.TERCERO.IDTERCERO = [Contrato].[Garantia].IDterceroaseguradora
AND contrato.contrato.IdContrato = [Contrato].[Garantia].IDContrato
AND Contrato.RelacionarContratistas.IdContrato = [Contrato].[Garantia].IDContrato
--AND Contrato.RelacionarContratistas.IdContratistaContrato = [Contrato].[Garantia].IDcontratistaContato
AND CONTRATISTA.NUMEROIDENTIFICACION = Contrato.RelacionarContratistas.NumeroIdentificacion
AND ([Contrato].[Garantia].IDContrato = @NumContrato OR @NumContrato IS NULL)
AND ([Contrato].[Garantia].NumGarantia = @NumGarantia OR @NumGarantia IS NULL)
AND ([Contrato].[Garantia].Estado = @Estado OR @Estado IS NULL)
AND ([Contrato].[Garantia].IDterceroaseguradora = @IDterceroaseguradora OR @IDterceroaseguradora IS NULL)
END



