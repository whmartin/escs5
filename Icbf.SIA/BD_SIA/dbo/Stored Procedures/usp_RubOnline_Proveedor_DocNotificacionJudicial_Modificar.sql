﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Modificar]
@IdDocNotJudicial INT,	
@IdNotJudicial INT,	
@IdDocumento INT,	
@Descripcion NVARCHAR(128),	
@LinkDocumento NVARCHAR(256),	
@Anno INT, 
@UsuarioModifica NVARCHAR(250)
AS
BEGIN
UPDATE Proveedor.DocNotificacionJudicial
SET	IdNotJudicial  = @IdNotJudicial,
	IdDocumento=@IdDocumento,
	Descripcion = @Descripcion,
	LinkDocumento = @LinkDocumento,
	Anno = @Anno,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdDocNotJudicial = @IdDocNotJudicial 
END
