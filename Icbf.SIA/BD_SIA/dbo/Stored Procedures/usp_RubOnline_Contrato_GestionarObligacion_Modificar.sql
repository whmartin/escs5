﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Modificar]
		@IdGestionObligacion INT,	@IdGestionClausula INT,	@NombreObligacion NVARCHAR(10),	@IdTipoObligacion INT,	@Orden NVARCHAR(128),	@DescripcionObligacion NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.GestionarObligacion SET IdGestionClausula = @IdGestionClausula, NombreObligacion = @NombreObligacion, IdTipoObligacion = @IdTipoObligacion, Orden = @Orden, DescripcionObligacion = @DescripcionObligacion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdGestionObligacion = @IdGestionObligacion
END
