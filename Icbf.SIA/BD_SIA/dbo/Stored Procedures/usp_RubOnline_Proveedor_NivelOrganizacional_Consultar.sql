﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Consultar]
	@IdNivelOrganizacional INT
AS
BEGIN
 SELECT IdNivelOrganizacional, CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[NivelOrganizacional] WHERE  IdNivelOrganizacional = @IdNivelOrganizacional
END

