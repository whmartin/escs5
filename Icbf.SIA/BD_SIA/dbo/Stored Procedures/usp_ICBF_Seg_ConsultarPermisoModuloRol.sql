﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================
-- =============================================
-- Author:		Yuri Gereda
-- Create date: 13-11-2012
-- Description:	Procedimiento almacenado que consulta los permisos de un Rol por Identificador del Módulo y Nombre de Rol
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPermisoModuloRol]
	 @nombreRol NVARCHAR(250)
	,@IdModulo INT
AS
BEGIN

SELECT P.IdPrograma
      ,P.IdModulo
      ,P.NombrePrograma
      ,P.CodigoPrograma
      ,P.Posicion
      ,P.Estado
      ,P.UsuarioCreacion
      ,P.FechaCreacion
      ,P.UsuarioModificacion
      ,P.FechaModificacion
      ,M.NombreModulo
      ,P.VisibleMenu
      ,P.EsReporte
      ,P.IdReporte  
FROM SEG.Permiso PR (NoLock)
	INNER JOIN SEG.Rol R (NoLock) ON PR.IdRol = R.IdRol
	INNER JOIN SEG.Programa P (NoLock) ON PR.IdPrograma = P.IdPrograma
	INNER JOIN SEG.Modulo M (NoLock) ON P.IdModulo = M.IdModulo			
WHERE R.Nombre = @nombreRol	
	  AND M.IdModulo = @IdModulo
	  AND (PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)	
	  AND P.VisibleMenu = 1	
GROUP BY P.IdPrograma
        ,P.IdModulo
        ,P.NombrePrograma
        ,P.CodigoPrograma
        ,P.Posicion
        ,P.Estado
        ,P.UsuarioCreacion
        ,P.FechaCreacion
        ,P.UsuarioModificacion
        ,P.FechaModificacion
        ,M.NombreModulo
        ,P.VisibleMenu
        ,P.EsReporte
        ,P.IdReporte        
ORDER BY P.Posicion DESC	

END
