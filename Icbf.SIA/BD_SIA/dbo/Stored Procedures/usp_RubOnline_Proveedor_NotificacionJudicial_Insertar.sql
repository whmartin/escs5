﻿
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:21:32 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NotificacionJudicial_Insertar]
		@IdNotJudicial INT OUTPUT, 	
		@IdEntidad INT,	
		@IdDepartamento INT,	
		@IdMunicipio INT , 	
		@IdZona INT , 	
		@Direccion NVARCHAR(250) ,  
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NotificacionJudicial(IdEntidad, IdDepartamento,IdMunicipio,IdZona, Direccion, UsuarioCrea, FechaCrea)
	VALUES(@IdEntidad, @IdDepartamento,@IdMunicipio,@IdZona,@Direccion, @UsuarioCrea, GETDATE())
	SELECT @IdNotJudicial = SCOPE_IDENTITY() 		
	RETURN @IdNotJudicial
END

