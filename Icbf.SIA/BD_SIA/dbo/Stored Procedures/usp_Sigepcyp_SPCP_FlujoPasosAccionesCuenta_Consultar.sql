﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:06:28 PM
-- Description:	Procedimiento almacenado que consulta un(a) FlujoPasosCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosAccionesCuenta_Consultar]
	@IdFlujoPasoCuenta INT
AS
BEGIN
 SELECT fp.IdFlujoPasoCuenta, fp.IdTipoCuenta, fp.IdPaso, fp.Orden, fp.UsuarioCrea
 , fp.FechaCrea
 , fp.UsuarioModifica
 , fp.FechaModifica 
 ,p.Financiera
   FROM [SPCP].[FlujoPasosCuenta]  fp
   INNER JOIN SPCP.Pasos p
   on fp.IdPaso = p.IdPaso
   INNER join SPCP.Acciones acc ON p.IdAccion = acc.IdAcciones
  WHERE IdFlujoPasoCuenta = @IdFlujoPasoCuenta and acc.Descripcion = 'Asignar Tipo Cuenta'
END


