﻿


-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
--Modifica´do Por: Juan Carlos Valverden Sámano
--Fecha: 31/03/2014
-- Descripción: Se agregó el Filtro de Estado=1 para que retorne sólo los registros habilitados.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar_TipoPersona]
	@IdTipoPersona INT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE t.IdTipoPersona = @IdTipoPersona AND trt.Estado =1
 ORDER BY Descripcion
 END



