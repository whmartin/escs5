﻿

-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  12/10/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Programa
-- Grupo de Apoyo SSII - SIGEPCYP 31/07/2015
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ModificarPrograma]
		@IdPrograma INT,	@IdModulo INT,	@NombrePrograma NVARCHAR(250),	@CodigoPrograma NVARCHAR(250),	@Posicion INT,	@Estado INT, @UsuarioModificacion NVARCHAR(250),	@VisibleMenu	BIT, @GeneraLog	BIT
AS
BEGIN
	UPDATE SEG.Programa SET IdModulo = @IdModulo, NombrePrograma = @NombrePrograma, CodigoPrograma = @CodigoPrograma, Posicion = @Posicion, Estado = @Estado, UsuarioModificacion = @UsuarioModificacion, FechaModificacion = GETDATE(), VisibleMenu = @VisibleMenu, generaLog = @GeneraLog  WHERE IdPrograma = @IdPrograma
END

