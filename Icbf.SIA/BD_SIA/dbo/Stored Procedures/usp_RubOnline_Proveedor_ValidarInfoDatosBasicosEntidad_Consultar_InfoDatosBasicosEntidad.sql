﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoDatosBasicosEntidad por IdInfoDatosBasicosEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_Consultar_InfoDatosBasicosEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoDatosBasicosEntidad, IdEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoDatosBasicosEntidad] 
 WHERE  IdEntidad = @IdEntidad
 ORDER BY IdValidarInfoDatosBasicosEntidad DESC
END




