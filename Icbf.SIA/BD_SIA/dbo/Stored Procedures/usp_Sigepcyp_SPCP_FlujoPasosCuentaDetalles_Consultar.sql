﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/1/2015 9:26:10 PM
-- Description:	Procedimiento almacenado que consulta un(a) FlujoPasosCuentaDetalle
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuentaDetalles_Consultar]
	@IdFlujoPasoCuenta INT = NULL,@IdTipoCuenta INT = NULL,@IdPaso INT = NULL,@Orden INT = NULL,@IdRegional INT = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@IdFlujoPasoCuenta INT,@IdTipoCuenta INT,@IdPaso INT,@Orden INT,@IdRegional INT'
Set @SqlExec = '
 SELECT FPCD.IdFlujoPasoCuentaDetalle, FPCD.IdFlujoPasoCuenta, FPCD.IdTipoCuenta, FPCD.IdPaso, FPCD.Orden, FPCD.UsuarioCrea, FPCD.FechaCrea, FPCD.UsuarioModifica, FPCD.FechaModifica, FPC.IdRegional 
 FROM [SPCP].[FlujoPasosCuentaDetalle] FPCD
 INNER JOIN [SPCP].[FlujoPasosCuenta] FPC ON FPCD.IdFlujoPasoCuenta = FPC.IdFlujoPasoCuenta
 WHERE 1=1 '
 If(@IdFlujoPasoCuenta Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPCD.IdFlujoPasoCuenta = @IdFlujoPasoCuenta' 
 If(@IdTipoCuenta Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPCD.IdTipoCuenta = @IdTipoCuenta' 
 If(@IdPaso Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPCD.IdPaso = @IdPaso' 
 If(@Orden Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPCD.Orden = @Orden' 
	If(@Idregional Is Not Null) 
	Set @SqlExec = @SqlExec + ' And FPC.Idregional = @IdRegional' 
Exec sp_executesql  @SqlExec, @SqlParametros,@IdFlujoPasoCuenta,@IdTipoCuenta,@IdPaso,@Orden,@IdRegional
END