﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 12:03:54 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DatosAdmonObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdmonObligaciones_Insertar]
		@IdDatosAdmonObligaciones INT OUTPUT, 	@IdDatosAdministracion INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.DatosAdmonObligaciones(IdDatosAdministracion, UsuarioCrea, FechaCrea)
					  VALUES(@IdDatosAdministracion, @UsuarioCrea, GETDATE())
	SELECT @IdDatosAdmonObligaciones = @@IDENTITY 		
END
