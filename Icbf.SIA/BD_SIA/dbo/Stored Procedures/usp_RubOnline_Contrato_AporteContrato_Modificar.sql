﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Modificar]
		@IdAporte INT,	@IdContrato NVARCHAR(3),	@TipoAportante NVARCHAR(11),	@NumeroIdenticacionContratista bigint,	@TipoAporte NVARCHAR(10),	@ValorAporte INT,	@DescripcionAporteEspecie NVARCHAR(128),	@FechaRP DATETIME,	@NumeroRP INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AporteContrato SET IdContrato = @IdContrato, TipoAportante = @TipoAportante, NumeroIdenticacionContratista = @NumeroIdenticacionContratista, TipoAporte = @TipoAporte, ValorAporte = @ValorAporte, DescripcionAporteEspecie = @DescripcionAporteEspecie, FechaRP = @FechaRP, NumeroRP = @NumeroRP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAporte = @IdAporte
END
