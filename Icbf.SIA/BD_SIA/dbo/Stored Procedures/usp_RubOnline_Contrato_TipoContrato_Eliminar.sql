﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Eliminar]
	@IdTipoContrato INT
AS
BEGIN
	DELETE Contrato.TipoContrato WHERE IdTipoContrato = @IdTipoContrato
END

