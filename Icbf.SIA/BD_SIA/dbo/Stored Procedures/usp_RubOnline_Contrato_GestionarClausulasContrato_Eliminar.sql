﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Eliminar]
	@IdGestionClausula INT
AS
BEGIN
	DELETE Contrato.GestionarClausulasContrato WHERE IdGestionClausula = @IdGestionClausula
END

