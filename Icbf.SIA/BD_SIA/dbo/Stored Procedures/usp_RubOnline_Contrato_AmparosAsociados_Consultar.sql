﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Consultar]
	@IdAmparo INT
AS
BEGIN
 SELECT IdAmparo, IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosAsociados] WHERE  IdAmparo = @IdAmparo
END

