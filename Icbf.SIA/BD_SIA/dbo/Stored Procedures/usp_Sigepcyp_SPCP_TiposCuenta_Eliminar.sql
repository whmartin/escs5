﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:05:12 PM
-- Description:	Procedimiento almacenado que elimina un(a) TiposCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_TiposCuenta_Eliminar]
	@IdTipoCuenta INT
AS
BEGIN
	DELETE SPCP.TiposCuenta WHERE IdTipoCuenta = @IdTipoCuenta
END
