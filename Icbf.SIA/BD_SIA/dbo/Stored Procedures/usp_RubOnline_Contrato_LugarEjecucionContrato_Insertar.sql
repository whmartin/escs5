﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que guarda un nuevo LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Insertar]
		@IdContratoLugarEjecucion INT OUTPUT, 	
		@IDContrato INT,	
		@Nacional BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.LugarEjecucionContrato(IDContrato,  Nacional, UsuarioCrea, FechaCrea)
					  VALUES(@IDContrato,  @Nacional, @UsuarioCrea, GETDATE())
	SELECT @IdContratoLugarEjecucion = @@IDENTITY 		
END

