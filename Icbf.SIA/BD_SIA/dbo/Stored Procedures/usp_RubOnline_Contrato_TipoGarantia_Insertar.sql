﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoGarantia_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoGarantia_Insertar]
		@IdTipoGarantia INT OUTPUT, 	@NombreTipoGarantia NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoGarantia(NombreTipoGarantia, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoGarantia, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoGarantia = @@IDENTITY 		
END

