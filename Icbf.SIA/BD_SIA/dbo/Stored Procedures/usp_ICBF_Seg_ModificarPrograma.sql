﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  12/10/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Programa
-- =============================================



CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ModificarPrograma]
		 @IdPrograma INT
		,@IdModulo INT
		,@NombrePrograma NVARCHAR(250)
		,@CodigoPrograma NVARCHAR(250)
		,@Posicion INT
		,@Estado INT
		,@UsuarioModificacion NVARCHAR(250)
		,@VisibleMenu	BIT
		,@GeneraLog	BIT
		,@EsReporte INT
		,@IdReporte INT
AS
BEGIN
	UPDATE SEG.Programa SET IdModulo = @IdModulo
	                       ,NombrePrograma = @NombrePrograma
	                       ,CodigoPrograma = @CodigoPrograma
	                       ,Posicion = @Posicion
	                       ,Estado = @Estado
	                       ,UsuarioModificacion = @UsuarioModificacion
	                       ,FechaModificacion = GETDATE()
	                       ,VisibleMenu = @VisibleMenu
	                       ,generaLog = @GeneraLog  
	                       ,EsReporte = @EsReporte
	                       ,IdReporte = @IdReporte
	WHERE IdPrograma = @IdPrograma
END
