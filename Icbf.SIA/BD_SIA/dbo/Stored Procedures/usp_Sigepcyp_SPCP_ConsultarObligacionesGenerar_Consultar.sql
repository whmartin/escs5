﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ConsultarObligacionesGenerar_Consultar]
	 @recurso VARCHAR(MAX)= NULL
	,@Indicador VARCHAR(MAX)= NULL
	,@Planilla INT=NULL
	,@FechaCorte DateTime=NULL
	,@pPlanilla INT=NULL
AS
BEGIN

	SELECT pp.Recurso
			,pp.Rubro as Identificador
			,pd.IdPlanilla as IdPlanilla
			,cc.IdCuenta as NumeroCuentaCobro
			,pp.NumDocSoporte as NumeroDocFuente
			,Convert(char(10),cc.FechaCrea,111) as FechaAprobacionCuenta
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	INNER JOIN SPCP.PlanDePagos pp
	on cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	INNER JOIN SPCP.PlanillaDetalle pd
	ON cc.IdCuenta = pd.IdCuenta
	WHERE cc.GeneradoObligaciones = 0
	AND pd.IdPlanilla = isnull(@Planilla,pd.IdPlanilla)
	AND pd.IdPlanilla = isnull(@pPlanilla,pd.IdPlanilla)
	AND pp.Recurso in (select data from dbo.Split(isnull(@recurso,pp.Recurso),';')) 
	AND pp.Rubro in (select data from dbo.Split((isnull(@Indicador,pp.Rubro)),';'))  
	AND cc.FechaCrea = isnull(@FechaCorte,cc.FechaCrea)
	--AND (pp.Rubro IN (select strIndicador from @tblIndicadores) OR @Indicador  IS NULL)
	--AND (pp.Recurso IN (select strRecurso from @tblRecursos)	OR @recurso IS NULL)

END



