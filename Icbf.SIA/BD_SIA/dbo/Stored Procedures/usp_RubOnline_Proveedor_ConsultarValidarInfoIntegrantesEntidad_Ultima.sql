﻿



-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento que consulta el ultimo ValidarInfoIntegrantesEntidad
-- =============================================
CREATE procedure [dbo].[usp_RubOnline_Proveedor_ConsultarValidarInfoIntegrantesEntidad_Ultima] (@IdEntidad INT)
as
begin
	select top 1
		v.IdValidarInfoIntegrantesEntidad,
		e.IdEntidad,
		ISNULL(v.NroRevision,1) as NroRevision,
		v.ConfirmaYAprueba,
		Observaciones,
		v.UsuarioCrea,
		v.FechaCrea
	from  Proveedor.ValidacionIntegrantesEntidad e
		inner join Proveedor.ValidarInfoIntegrantesEntidad v
		on v.IdEntidad = e.IdEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and e.NroRevision = v.NroRevision
	order by NroRevision DESC
end



