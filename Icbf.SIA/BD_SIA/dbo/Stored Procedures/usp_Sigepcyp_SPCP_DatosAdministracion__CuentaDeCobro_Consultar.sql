﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:43 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoOPP
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdministracion__CuentaDeCobro_Consultar]
	@IDCuentaDeCobro int 
AS

BEGIN
	
	SELECT da.IdDatosAdministracion,da.CorreoInstitucional
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	WHERE cc.IdCuenta = @IDCuentaDeCobro

END

