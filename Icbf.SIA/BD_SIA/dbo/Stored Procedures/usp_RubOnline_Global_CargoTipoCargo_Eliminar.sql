﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que elimina un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Eliminar]
	@IdCargo INT
	,@TipoCargo NVARCHAR(1)
AS
BEGIN
	DELETE Global.CargoTipoCargo WHERE IdCargo=@IdCargo AND TipoCargo=@TipoCargo
END

