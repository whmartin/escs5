﻿
/*
exec dbo.usp_ICBF_Proveedores_GetDataClase '39,39,86,94,50,80,72,81,90,93,93,93,86,94,50,80,72,81,90,93,77,85,73,56,26,84,43,52,78,46,95,31,83,82,42,92,10,25,30'
*/

CREATE procedure [dbo].[usp_ICBF_Proveedores_GetDataClase]
	 @CodigoSegmento nvarchar(max)
	,@CodigoFamilia nvarchar(max)
as
begin
declare @query as nvarchar(max) = '
		select	-1 as [CodigoClase],''  Seleccionar Todo'' as [NombreClase]	
	union
		select	distinct uns.[CodigoClase],uns.[NombreClase]
		from	[PROVEEDOR].[TipoCodigoUNSPSC]uns'
			
-->	Agregando Filtro de Segmento @CodigoSegmento 
	if not exists(select 1 from dbo.StringSplit(@CodigoSegmento,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoSegmento,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN StringSplit((@CodigoSegmento),1,0) fcs on fcs.val = uns.[CodigoSegmento] and fcs.val != -1'
				end
		end

-->	Agregando Filtro de Familia @CodigoFamilia
	if not exists(select 1 from dbo.StringSplit(@CodigoFamilia,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoFamilia,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoFamilia,1,0) fcf on fcf.val = uns.[CodigoFamilia] and fcf.val != -1 '
				end
		end

	set @query = @query+'	
	where	uns.[CodigoClase] is not null
	order by 2 asc'

--	print @query
	EXEC sp_executesql @query,N'@CodigoSegmento nvarchar(max),@CodigoFamilia nvarchar(max)',@CodigoSegmento,@CodigoFamilia

end	


/*****************************************************************************************************************************************************************************/

