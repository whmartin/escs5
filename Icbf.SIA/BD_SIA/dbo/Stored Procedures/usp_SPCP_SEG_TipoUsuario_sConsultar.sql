﻿
CREATE PROCEDURE [dbo].[usp_SPCP_SEG_TipoUsuario_sConsultar]
	@CodigoTipoUsuario NVARCHAR(128) = NULL,@NombreTipoUsuario NVARCHAR(256) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdTipoUsuario, CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
   FROM [SEG].[TipoUsuario] 
  WHERE CodigoTipoUsuario = CASE WHEN @CodigoTipoUsuario IS NULL 
								 THEN CodigoTipoUsuario ELSE @CodigoTipoUsuario END 
					  AND NombreTipoUsuario = CASE WHEN @NombreTipoUsuario IS NULL THEN NombreTipoUsuario ELSE @NombreTipoUsuario END 
					  AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

