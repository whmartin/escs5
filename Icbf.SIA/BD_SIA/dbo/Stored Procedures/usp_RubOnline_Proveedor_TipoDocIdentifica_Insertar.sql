﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/19/2013 12:25:26 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocIdentifica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_Insertar]
		@IdTipoDocIdentifica INT OUTPUT, 	@CodigoDocIdentifica NUMERIC,	@Descripcion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoDocIdentifica(CodigoDocIdentifica, Descripcion, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoDocIdentifica, @Descripcion, @UsuarioCrea, GETDATE())
	SELECT @IdTipoDocIdentifica = SCOPE_IDENTITY() 		
END


