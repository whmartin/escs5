﻿

-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Actualiza tercero, modificación por la adición del campo EsFundacion a la tabla Tercero
-- =============================================


CREATE PROCEDURE [dbo].[usp_Terceros_Tercero_Modificar]
	 @IdTercero  INT,
	 @IdDListaTipoDocumento INT= NULL,
	 @IdTipoPersona INT= NULL,
	 @Email nvarchar(40)= NULL,
	 @NumeroIdentificacion nvarchar(128)= NULL,
	 @FechaExpedicionId datetime= NULL,
	 @Sexo nvarchar(1)= NULL,
	 @RazonSocial  nvarchar(128)= NULL,
	 @Digitoverificacion INT= NULL,
	 @PrimerNombre nvarchar(128)= NULL,
	 @SegundoNombre nvarchar(128)= NULL,
	 @PrimerApellido nvarchar(128)= NULL,
	 @SegundoApellido nvarchar(128)= NULL,
	 @FechaNacimiento datetime= NULL,
	 @UsuarioModifica nvarchar(128)= NULL,
	 @FechaModifica datetime= NULL,
	 @EsFundacion bit = NULL,
	 @IdTemporal VARCHAR(20)= NULL,
	 @CreadoPorInterno BIT=NULL
AS
BEGIN
	UPDATE Tercero.TERCERO SET
	 IDTIPODOCIDENTIFICA= ISNULL(@IdDListaTipoDocumento,IDTIPODOCIDENTIFICA),
		IdTipoPersona= ISNULL(@IdTipoPersona, IdTipoPersona),
		CORREOELECTRONICO = ISNULL( @Email, CORREOELECTRONICO ),
		NUMEROIDENTIFICACION = ISNULL( @NumeroIdentificacion, NUMEROIDENTIFICACION ),
		FECHAEXPEDICIONID = ISNULL( @FechaExpedicionId, FECHAEXPEDICIONID ),
		SEXO = ISNULL( @Sexo, SEXO ),
		RAZONSOCIAL= ISNULL(@RazonSocial, RAZONSOCIAL),
		PRIMERNOMBRE= ISNULL(@PrimerNombre, PRIMERNOMBRE),
		SEGUNDONOMBRE= ISNULL(@SegundoNombre, SEGUNDONOMBRE),
		PRIMERAPELLIDO= ISNULL(@PrimerApellido, PRIMERAPELLIDO),
		SEGUNDOAPELLIDO= ISNULL(@SegundoApellido, SEGUNDOAPELLIDO),
		DIGITOVERIFICACION= ISNULL(@Digitoverificacion, DIGITOVERIFICACION),
		FECHANACIMIENTO= ISNULL(@FechaNacimiento, FECHANACIMIENTO),
		USUARIOMODIFICA = ISNULL( @UsuarioModifica, USUARIOMODIFICA ),
		FECHAMODIFICA = ISNULL( @FechaModifica ,  FECHAMODIFICA ),
		ESFUNDACION = ISNULL( @EsFundacion ,  ESFUNDACION ),
		CreadoPorInterno = @CreadoPorInterno

	WHERE IDTERCERO = @IdTercero

-----Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------
DECLARE @TBL_DOCS_ORIGINAL TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

DECLARE @TBL_DOCS_TEMPORAL TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

INSERT INTO @TBL_DOCS_ORIGINAL
	SELECT IDDOCADJUNTO,IDTERCERO,IDDOCUMENTO,IdTemporal,ACTIVO FROM [Proveedor].[DocAdjuntoTercero] 
	WHERE IDTERCERO=@IdTercero AND
	IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal

INSERT INTO @TBL_DOCS_TEMPORAL
	SELECT IDDOCADJUNTO,IDTERCERO,IDDOCUMENTO,IdTemporal,ACTIVO FROM [Proveedor].[DocAdjuntoTercero] 
	WHERE IDTERCERO IS NULL
	AND IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal AND Activo=1
	
DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_ORIGINAL)
DECLARE @nWhile Int --Almacenará la cantidad de veces que se esta recorriendo en el Bucle.
DECLARE @IDDOCTEMP INT
DECLARE @IDDOCORIGINAL INT
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SELECT @IDDOCORIGINAL=IDDOCADJUNTO, 
@IdDoc=IDDOCUMENTO FROM @TBL_DOCS_ORIGINAL WHERE IDROW=@nWhile
IF EXISTS(SELECT IDROW FROM @TBL_DOCS_TEMPORAL WHERE IDDOCUMENTO=@IdDoc)
BEGIN
SET @IDDOCTEMP=(SELECT IDDOCADJUNTO FROM @TBL_DOCS_TEMPORAL WHERE IDDOCUMENTO=@IdDoc)
UPDATE [Proveedor].[DocAdjuntoTercero] 
SET Activo=0 WHERE IDDOCADJUNTO=@IDDOCORIGINAL

UPDATE [Proveedor].[DocAdjuntoTercero] 
SET IDTERCERO=@IdTercero WHERE IDDOCADJUNTO=@IDDOCTEMP
END

SET @nWhile=@nWhile+1
END

SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_TEMPORAL)
SET @nWhile=1
--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdTipoDoc INT
SELECT @IDDOCTEMP=IDDOCADJUNTO, 
@IdTipoDoc=IDDOCUMENTO FROM @TBL_DOCS_TEMPORAL WHERE IDROW=@nWhile
IF NOT EXISTS(SELECT * FROM @TBL_DOCS_ORIGINAL WHERE IDDOCUMENTO=@IdTipoDoc)
BEGIN
UPDATE [Proveedor].[DocAdjuntoTercero] 
SET IDTERCERO=@IdTercero WHERE IDDOCADJUNTO=@IDDOCTEMP
END
SET @nWhile=@nWhile+1
END
------------------------------------------------------------------------------------------
	
IF(@CreadoPorInterno=0)
BEGIN	
	DECLARE @ProviderUserKey uniqueidentifier
	SET @ProviderUserKey=(
	SELECT ProviderUserKey 
	FROM Tercero.TERCERO 
	WHERE IDTERCERO=@IdTercero )
	--VERIFICAR SI SE TRATA DE UN USUARIO EXTERNO (REGISTRADO DESDE PROVEEDORES)---- SI ES ASI PARA PASAR A VALIDAR A ACTUALIZAR SU INFORMACIÓN
	--EN LA ENTIDAD USUARIO, EN CASO CONTRARIO NO REALIZA LA ACTUALIZACIÓN.
	IF EXISTS (SELECT IdUsuario FROM SEG.Usuario
	WHERE providerKey=@ProviderUserKey)
	BEGIN
	--------------VERIFICAR DATOS CON SEG.Usuario Y SI DIFIEREN ....ACTUALIZARLOS--------------------------------
				
			IF (@IdTipoPersona=1)
			BEGIN
				DECLARE @primer_Nombre NVARCHAR(150),
				 @segundo_Nombre NVARCHAR(150),
				 @primer_Apellido NVARCHAR(150),
				 @segundo_Apellido NVARCHAR(150)
				 
				 SELECT @primer_Nombre=ISNULL([PrimerNombre],''),
				 @segundo_Nombre=ISNULL([SegundoNombre],''),
				 @primer_Apellido=ISNULL([PrimerApellido],''),
				 @segundo_Apellido=ISNULL([SegundoApellido],'')
				 FROM SEG.Usuario
				 WHERE providerKey=@ProviderUserKey
				 
				 IF(@PrimerNombre!=@primer_Nombre)
				 BEGIN
					 UPDATE SEG.Usuario SET PrimerNombre=@PrimerNombre
					 WHERE providerKey=@ProviderUserKey
				 END
				 
				 IF(@SegundoNombre!=@segundo_Nombre)
				 BEGIN
					 UPDATE SEG.Usuario SET SegundoNombre=@SegundoNombre
					 WHERE providerKey=@ProviderUserKey
				 END
				 
				 IF(@PrimerApellido!= @primer_Apellido)
				 BEGIN
					 UPDATE SEG.Usuario SET PrimerApellido=@PrimerApellido
					 WHERE providerKey=@ProviderUserKey
				 END
				 
				 IF(@SegundoApellido!=@segundo_Apellido)
				 BEGIN
					 UPDATE SEG.Usuario SET SegundoApellido=@SegundoApellido
					 WHERE providerKey=@ProviderUserKey
				 END
			END 
			ELSE IF (@IdTipoPersona=2 Or @IdTipoPersona=3 Or @IdTipoPersona=4)
			BEGIN
				DECLARE @razon_Social NVARCHAR(256)
				SELECT @razon_Social=ISNULL([RazonSocial],'')
				FROM SEG.Usuario
				WHERE providerKey=@ProviderUserKey
				
				IF(@RAZONSOCIAL!=@razon_Social)
				 BEGIN
					 UPDATE SEG.Usuario SET RazonSocial=@RAZONSOCIAL
					 WHERE providerKey=@ProviderUserKey
				 END
			END
	END	
END
	
	------------------------------------------------
END




