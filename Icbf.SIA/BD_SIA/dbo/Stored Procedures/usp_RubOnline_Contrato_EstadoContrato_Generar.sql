﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/20/2013 10:09:07 PM
-- Description:	Procedimiento almacenado que consulta un(a) Estado de un Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_EstadoContrato_Generar]
	@EstadoContrato NVARCHAR(128) output, @NumeroContrato NUMERIC(25) 
AS
BEGIN
	DECLARE @EstadoActual NVARCHAR(128)
	SET @EstadoActual = 'DESCONOCIDO'

	-- SUSCRITO
		SELECT @EstadoActual = 'SUSCRITO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND FechaSuscripcion IS NOT NULL

	-- EJECUCION
		SELECT @EstadoActual = 'EJECUCION'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() BETWEEN FechaInicioEjecucion AND FechaFinalTerminacion

		-- SUSPENDIDO
		/*SELECT @EstadoActual = 'SUSPENDIDO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato*/

	-- TERMINADO
		SELECT @EstadoActual = 'TERMINADO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() > FechaFinalTerminacion 

	-- LIQUIDADO
		SELECT @EstadoActual = 'LIQUIDADO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() >= FechaProyectadaLiquidacion 

		-- ANULADO
		/*SELECT @EstadoActual = 'ANULADO'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND FechaSuscripcion IS NOT NULL	*/

	-- PERDIDA COMPETENCIA
		SELECT @EstadoActual = 'PERDIDA'
		FROM Contrato.Contrato
		WHERE NumeroContrato = @NumeroContrato AND GETDATE() > FechaFinalTerminacion AND DATEDIFF(m,FechaFinalTerminacion,GETDATE()) >= 30
	

	SELECT @EstadoContrato
END
