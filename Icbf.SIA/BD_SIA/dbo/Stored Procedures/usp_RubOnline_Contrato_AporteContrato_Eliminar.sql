﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Eliminar
***********************************************/
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Eliminar]
	@IdAporte INT
AS
BEGIN
	DELETE Contrato.AporteContrato WHERE IdAporte = @IdAporte
END
