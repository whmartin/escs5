﻿

-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 3:11:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoRegimenTributario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoRegimenTributarios_Consultar]
	@CodigoRegimenTributario NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@IdTipoPersona INT = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoRegimenTributario, CodigoRegimenTributario, Descripcion, t.IdTipoPersona, trt.Estado, trt.UsuarioCrea, trt.FechaCrea, trt.UsuarioModifica, trt.FechaModifica, NombreTipoPersona as DescripcionTipoPersona
 FROM [Proveedor].[TipoRegimenTributario] trt INNER JOIN Oferente.TipoPersona t ON trt.IdTipoPersona = t.IdTipoPersona
 WHERE CodigoRegimenTributario = CASE WHEN @CodigoRegimenTributario IS NULL THEN CodigoRegimenTributario ELSE @CodigoRegimenTributario END 
 AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END 
 AND trt.IdTipoPersona = CASE WHEN @IdTipoPersona IS NULL THEN trt.IdTipoPersona ELSE @IdTipoPersona END 
 AND trt.Estado = CASE WHEN @Estado IS NULL THEN trt.Estado ELSE @Estado END
END



