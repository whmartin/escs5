﻿-- =============================================
-- Author:		Jonnathan NIño
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que consulta un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Consultar]
	@IdContacto INT
AS
BEGIN
 SELECT CE.IdContacto, CE.IdEntidad, CE.IdSucursal, CE.IdTelContacto, CE.IdTipoDocIdentifica, CE.IdTipoCargoEntidad, CE.NumeroIdentificacion, CE.PrimerNombre, 
	CE.SegundoNombre, CE.PrimerApellido, CE.SegundoApellido, CE.Dependencia, CE.Email, CE.Estado, CE.UsuarioCrea, CE.FechaCrea, CE.UsuarioModifica, CE.FechaModifica,
	TT.Movil AS Celular, TT.IndicativoTelefono, TT.NumeroTelefono, TT.ExtensionTelefono,
	TC.Descripcion AS Cargo
 FROM [Proveedor].[ContactoEntidad] AS CE
 LEFT JOIN [Oferente].[TelTerceros] AS TT ON CE.IdTelContacto = TT.IdTelTercero
 LEFT JOIN [Proveedor].[TipoCargoEntidad] AS TC ON CE.IdTipoCargoEntidad = TC.IdTipoCargoEntidad
 WHERE  IdContacto = @IdContacto
END

