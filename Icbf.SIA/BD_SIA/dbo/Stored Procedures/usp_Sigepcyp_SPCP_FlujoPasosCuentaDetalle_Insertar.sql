﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/1/2015 9:26:10 PM
-- Description:	Procedimiento almacenado que guarda un nuevo FlujoPasosCuentaDetalle
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuentaDetalle_Insertar]
		@IdFlujoPasoCuentaDetalle INT OUTPUT, 	@IdFlujoPasoCuenta INT,	@IdTipoCuenta INT,	@IdPaso INT,	@Orden INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.FlujoPasosCuentaDetalle(IdFlujoPasoCuenta, IdTipoCuenta, IdPaso, Orden, UsuarioCrea, FechaCrea)
					  VALUES(@IdFlujoPasoCuenta, @IdTipoCuenta, @IdPaso, @Orden, @UsuarioCrea, GETDATE())
	SELECT @IdFlujoPasoCuentaDetalle = @@IDENTITY 		
END

