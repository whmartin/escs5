﻿


-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta las sucursales de una aseguradora
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GarantiaSucursal_consultar]
@IDTERCERO INT
AS
/*
EXEC DBO.usp_RubOnline_Contrato_GarantiaSucursal_consultar 277
*/
SELECT
	  Proveedor.Sucursal.IdSucursal,
      DIV.Departamento.NombreDepartamento,
      DIV.Municipio.NombreMunicipio,
      Proveedor.Sucursal.Nombre,
      Proveedor.Sucursal.Direccion,
      Proveedor.Sucursal.Correo,
      Proveedor.Sucursal.Indicativo,
      Proveedor.Sucursal.Telefono,
      Proveedor.Sucursal.Extension,
      Proveedor.Sucursal.Celular
FROM Oferente.TERCERO
INNER JOIN Proveedor.EntidadProvOferente
      ON Oferente.TERCERO.IDTERCERO = Proveedor.EntidadProvOferente.IdTercero
INNER JOIN Proveedor.Sucursal
      ON Proveedor.EntidadProvOferente.IdEntidad = Proveedor.Sucursal.IdEntidad
INNER JOIN DIV.Departamento
      ON Proveedor.Sucursal.Departamento = DIV.Departamento.IdDepartamento
INNER JOIN DIV.Municipio
      ON Proveedor.Sucursal.Municipio = DIV.Municipio.IdMunicipio
WHERE Proveedor.EntidadProvOferente.IdTercero = @IDTERCERO
--AND Oferente.TERCERO.RAZONSOCIAL LIKE '%ASEGURADORA%'



