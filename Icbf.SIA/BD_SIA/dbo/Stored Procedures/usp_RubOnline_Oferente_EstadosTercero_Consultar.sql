﻿

-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los estados del Tercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_EstadosTercero_Consultar]
	@Estado BIT = NULL
AS
BEGIN

 SELECT IdEstadoTercero, 
		CodigoEstadotercero, 
		DescripcionEstado, 
		Estado, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
 FROM [Oferente].[EstadoTercero] 
 WHERE Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

