﻿-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que consulta un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidads_Consultar]
	@IdExpEntidad INT = NULL
AS
BEGIN
 SELECT IdExpCOD, E.IdTipoCodUNSPSC, IdExpEntidad, E.UsuarioCrea, E.FechaCrea, E.UsuarioModifica, E.FechaModifica,
	T.Codigo, T.Descripcion 
 FROM [Proveedor].[ExperienciaCodUNSPSCEntidad] E
 INNER JOIN [Proveedor].[TipoCodigoUNSPSC] T ON E.IdTipoCodUNSPSC=T.IdTipoCodUNSPSC
 WHERE IdExpEntidad = ISNULL(@IdExpEntidad, IdExpEntidad)
END

