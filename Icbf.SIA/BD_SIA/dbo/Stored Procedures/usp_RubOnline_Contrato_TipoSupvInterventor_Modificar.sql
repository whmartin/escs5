﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Modificar]
		@IdTipoSupvInterventor INT,	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoSupvInterventor SET Nombre = @Nombre, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoSupvInterventor = @IdTipoSupvInterventor
END

