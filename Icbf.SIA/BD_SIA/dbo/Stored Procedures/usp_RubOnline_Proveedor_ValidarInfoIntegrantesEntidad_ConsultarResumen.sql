﻿

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date:  2014/07/25 09:18 AM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoIntegrantesEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoIntegrantesEntidad, v.IdEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoIntegrantesEntidad v
			left join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = v.IdEntidad
			where p.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			ORDER BY v.FechaCrea DESC
END




