﻿-- =============================================
-- Author:		Yuri Gereda
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que consulta los permisos de un Rol por Identificador del Rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarPermisosRol]
	@pIdRol INT
AS
BEGIN

SELECT
	PE.IdPermiso, PE.IdPrograma, PR.NombrePrograma, PE.IdRol, PE.Insertar, PE.Modificar, PE.Eliminar, PE.Consultar, PE.UsuarioCreacion, PE.FechaCreacion, PE.UsuarioModificacion, PE.FechaModificacion
FROM
	SEG.Permiso PE	
INNER JOIN
	SEG.Rol RO ON PE.IdRol = RO.IdRol
INNER JOIN
	SEG.Programa PR	ON PE.IdPrograma = PR.IdPrograma
WHERE
	RO.IdRol = @pIdRol
	
UNION

SELECT 
	-1, IdPrograma, NombrePrograma, @pIdRol , 0, 0, 0, 0, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion
FROM 
	SEG.Programa 
WHERE 
	IdPrograma NOT IN(	SELECT
							PE.IdPrograma
						FROM
							SEG.Permiso PE	
						INNER JOIN
							SEG.Rol RO ON PE.IdRol = RO.IdRol
						WHERE
							RO.IdRol = @pIdRol)

END
