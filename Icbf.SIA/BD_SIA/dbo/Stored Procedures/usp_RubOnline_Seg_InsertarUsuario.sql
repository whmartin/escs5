﻿


-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  08/10/2012
-- Description:	Procedimiento almacenado que guarda información de un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_InsertarUsuario]
		  @IdUsuario INT OUTPUT
		, @IdTipoPersona INT
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @DV NVARCHAR(1)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @RazonSocial NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10)
		, @CorreoElectronico NVARCHAR(150)
		, @Estado BIT
		, @ProviderKey VARCHAR(125)
		, @UsuarioCreacion NVARCHAR(250)
		, @IdTipoUsuario int
		, @IdRegional int
AS
BEGIN
	INSERT INTO SEG.Usuario([IdTipoPersona]
		   ,[IdTipoDocumento]
           ,[NumeroDocumento]
		   ,[DV]
           ,[PrimerNombre]
           ,[SegundoNombre]
           ,[PrimerApellido]
           ,[SegundoApellido]
		   ,[RazonSocial]
           ,[TelefonoContacto]
           ,[CorreoElectronico]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[providerKey]
		   ,[IdTipoUsuario]
		   ,[IdRegional])
	VALUES(@IdTipoPersona
		   ,@IdTipoDocumento
           ,@NumeroDocumento
		   ,@DV
           ,@PrimerNombre
           ,@SegundoNombre
           ,@PrimerApellido
           ,@SegundoApellido
		   ,@RazonSocial
           ,@TelefonoContacto
           ,@CorreoElectronico
           ,@Estado
           ,@UsuarioCreacion
           ,GETDATE()
           ,@ProviderKey
		   ,@IdTipoUsuario
		   ,@IdRegional)
	SELECT @IdUsuario = @@IDENTITY
END



