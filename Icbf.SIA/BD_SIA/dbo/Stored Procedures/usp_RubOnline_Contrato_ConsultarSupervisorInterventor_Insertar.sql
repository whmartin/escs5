﻿
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que guarda un nuevo ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Insertar]
		@TipoSupervisorInterventor INT OUTPUT, 	@NumeroContrato NVARCHAR(50),	@NumeroIdentificacion INT,	@NombreRazonSocialSupervisorInterventor NVARCHAR(50),	@NumeroIdentificacionDirectorInterventoria INT,	@NombreRazonSocialDirectorInterventoria NVARCHAR(50), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ConsultarSupervisorInterventor(NumeroContrato, NumeroIdentificacion, NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroContrato, @NumeroIdentificacion, @NombreRazonSocialSupervisorInterventor, @NumeroIdentificacionDirectorInterventoria, @NombreRazonSocialDirectorInterventoria, @UsuarioCrea, GETDATE())
	SELECT @TipoSupervisorInterventor = @@IDENTITY 		
END


