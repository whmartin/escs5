﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que consulta un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_sConsultar]
	@AcnoVigencia INT = NULL,@Activo NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[Vigencia] WHERE AcnoVigencia = CASE WHEN @AcnoVigencia IS NULL THEN AcnoVigencia ELSE @AcnoVigencia END AND Activo = CASE WHEN @Activo IS NULL THEN Activo ELSE @Activo END
END

