﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Insertar]
		@IdDocAdjunto INT OUTPUT, 	@IdExpEntidad INT = NULL,	@IdTipoDocumento INT,	
		@Descripcion NVARCHAR(128),	@LinkDocumento NVARCHAR(256), @UsuarioCrea NVARCHAR(250),
		@IdTemporal NVARCHAR(50)
AS
BEGIN
	INSERT INTO Proveedor.DocExperienciaEntidad(IdExpEntidad, IdTipoDocumento, Descripcion, LinkDocumento, UsuarioCrea, FechaCrea, IdTemporal)
					  VALUES(@IdExpEntidad, @IdTipoDocumento, @Descripcion, @LinkDocumento, @UsuarioCrea, GETDATE(), @IdTemporal)
	SELECT @IdDocAdjunto = SCOPE_IDENTITY() 		
END


