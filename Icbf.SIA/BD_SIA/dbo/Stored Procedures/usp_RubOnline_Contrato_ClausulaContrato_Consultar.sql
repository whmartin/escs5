﻿   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que consulta un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Consultar]
	@IdClausulaContrato INT
AS
BEGIN
 SELECT IdClausulaContrato, NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ClausulaContrato] WHERE  IdClausulaContrato = @IdClausulaContrato
END



