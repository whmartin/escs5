﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar_Table]
	@NumeroGarantia NVARCHAR(50) = NULL,
	@IdTipoAmparo INT = NULL,
	@VigenciaDesde DATETIME = NULL,
	@VigenciaHasta	DATETIME = NULL	
AS
BEGIN
 SELECT IdAmparo, amparo.IdGarantia, amparo.IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, 
 IdTipoCalculo, ValorAsegurado, amparo.UsuarioCrea, amparo.FechaCrea, amparo.UsuarioModifica, amparo.FechaModifica ,
 tipoamparo.NombreTipoAmparo as tipoamparo, garantia.NumGarantia
 FROM [Contrato].[AmparosAsociados] amparo inner join Contrato.Garantia garantia
 on (garantia.IDGarantia = amparo.IdGarantia) INNER JOIN Contrato.TipoAmparo tipoamparo
 on (tipoamparo.IdTipoAmparo = amparo.IdTipoAmparo)
 WHERE amparo.IdTipoAmparo = CASE WHEN @IdTipoAmparo IS NULL THEN amparo.IdTipoAmparo ELSE @IdTipoAmparo END 
 AND FechaVigenciaDesde >= CASE WHEN @VigenciaDesde IS NULL THEN FechaVigenciaDesde ELSE @VigenciaDesde END 
 AND VigenciaHasta <= CASE WHEN @VigenciaHasta IS NULL THEN VigenciaHasta ELSE @VigenciaHasta END
 AND garantia.NumGarantia = CASE WHEN @NumeroGarantia IS NULL THEN garantia.NumGarantia ELSE @NumeroGarantia END
END



