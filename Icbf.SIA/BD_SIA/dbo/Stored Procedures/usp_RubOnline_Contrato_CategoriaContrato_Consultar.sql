﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Consultar]
	@IdCategoriaContrato INT
AS
BEGIN
 SELECT IdCategoriaContrato, NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[CategoriaContrato] WHERE  IdCategoriaContrato = @IdCategoriaContrato
END

