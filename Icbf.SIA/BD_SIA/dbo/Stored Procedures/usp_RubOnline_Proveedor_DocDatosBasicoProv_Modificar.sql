﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocDatosBasicoProv
-- Modificación por: Juan Carlos Valverde Sámano
-- Fecha Modificación: 25/03/2014
-- Descripción: En base al requerimiento del Control de Cambios 016, se ha realizado el cambio
-- para que este UPDATE solo modifique la Columna Activo colocandole un false, Y enseguida se realiza un INSERT
-- a la misma Tabla con los datos para el Nuevo Doc, y el campo Activo es por defautl true.
-- de esta manera no se borran los registros de los documentos, se mantiene el historial y solo
-- un documento es el Activo.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Modificar]
		@IdDocAdjunto INT,	@IdEntidad INT = NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocDatosBasicoProv SET UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(), Activo=0  WHERE IdDocAdjunto = @IdDocAdjunto
	
	DECLARE @IdTipoDoc INT
	SET @IdTipoDoc=(SELECT CONVERT(INT,IdTipoDocumento) FROM Proveedor.DocDatosBasicoProv WHERE IdDocAdjunto = @IdDocAdjunto)

DECLARE @IdDocAdjuntoRetorno INT
	EXEC usp_RubOnline_Proveedor_DocDatosBasicoProv_Insertar
	@IdDocAdjuntoRetorno, @IdEntidad, @NombreDocumento, @LinkDocumento,  @Observaciones, @IdTipoDoc, @UsuarioModifica, NULL
END



