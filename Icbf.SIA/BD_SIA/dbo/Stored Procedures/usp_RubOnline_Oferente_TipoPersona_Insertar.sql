﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TipoPersona_Insertar]
		@IdTipoPersona INT OUTPUT, 	@CodigoTipoPersona NVARCHAR(128),	@NombreTipoPersona NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Tercero.TipoPersona(CodigoTipoPersona, NombreTipoPersona, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoPersona, @NombreTipoPersona, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoPersona = @@IDENTITY 		
END

