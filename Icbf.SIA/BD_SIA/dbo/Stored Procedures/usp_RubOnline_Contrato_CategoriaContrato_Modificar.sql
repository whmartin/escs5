﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Modificar]
		@IdCategoriaContrato INT,	@NombreCategoriaContrato NVARCHAR(50),	@Descripcion NVARCHAR(100),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.CategoriaContrato SET NombreCategoriaContrato = @NombreCategoriaContrato, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdCategoriaContrato = @IdCategoriaContrato
END

