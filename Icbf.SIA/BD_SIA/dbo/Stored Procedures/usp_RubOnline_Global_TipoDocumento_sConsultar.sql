﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  12/12/2012 9:48:16
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TipoDocumento_sConsultar]
	@CodigoDocumento NVARCHAR(10) = NULL,@NombreDocumento NVARCHAR(50) = NULL
AS
BEGIN
 SELECT IdTipoDocumento, CodigoDocumento, NombreDocumento, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[TipoDocumento] WHERE CodigoDocumento = CASE WHEN @CodigoDocumento IS NULL THEN CodigoDocumento ELSE @CodigoDocumento END AND NombreDocumento = CASE WHEN @NombreDocumento IS NULL THEN NombreDocumento ELSE @NombreDocumento END
END

