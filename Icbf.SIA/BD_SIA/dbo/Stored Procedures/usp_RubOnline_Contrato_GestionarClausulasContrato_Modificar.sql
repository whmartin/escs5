﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Modificar]
		@IdGestionClausula INT,	@IdContrato NVARCHAR(3),	@NombreClausula NVARCHAR(10),	@TipoClausula INT,	@Orden NVARCHAR(128), @OrdenNumero INT,	@DescripcionClausula NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.GestionarClausulasContrato SET IdContrato = @IdContrato, NombreClausula = @NombreClausula, TipoClausula = @TipoClausula, Orden = @Orden, OrdenNumero = @OrdenNumero, DescripcionClausula = @DescripcionClausula, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdGestionClausula = @IdGestionClausula
END

