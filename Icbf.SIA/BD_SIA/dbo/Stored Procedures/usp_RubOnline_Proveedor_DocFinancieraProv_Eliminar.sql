﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que elimina un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocFinancieraProv WHERE IdDocAdjunto = @IdDocAdjunto
END


