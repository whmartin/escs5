﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Modificar]
		@IdTipoAmparo INT,	
		@NombreTipoAmparo NVARCHAR(128),	
		@IdTipoGarantia INT,	@Estado BIT, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoAmparo SET NombreTipoAmparo = @NombreTipoAmparo, IdTipoGarantia = @IdTipoGarantia, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoAmparo = @IdTipoAmparo
END

