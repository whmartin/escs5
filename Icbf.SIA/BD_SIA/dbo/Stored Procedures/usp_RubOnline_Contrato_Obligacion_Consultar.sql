﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_Obligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Consultar]
	@IdObligacion INT
AS
BEGIN
 SELECT IdObligacion, IdTipoObligacion, IdTipoContrato, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[Obligacion] WHERE  IdObligacion = @IdObligacion
END


