﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:03:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) Municipio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Municipio_Consultar]
	@IdMunicipio INT
AS
BEGIN
 SELECT IdMunicipio, 
		IdDepartamento, 
		CodigoMunicipio, 
		NombreMunicipio, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Municipio] WHERE  IdMunicipio = @IdMunicipio
END

