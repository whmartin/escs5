﻿-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  02/26/2014 3:16:21 PM
-- Description:	Procedimiento almacenado que consulta un(a) Familias
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDORES_TipoCodigoUNSPSC_Consultar_Familia]
	@CodigoSegmento NVARCHAR(50)
AS
BEGIN
 SELECT DISTINCT(CodigoFamilia),
		NombreFamilia
  FROM	PROVEEDOR.TipoCodigoUNSPSC
  WHERE	CodigoSegmento = @CodigoSegmento
END