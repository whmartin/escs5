﻿
-- =============================================
-- Author:		Zulma Barrantes --
-- Author Modify:       Alexander Gutierrez 
-- Modify date:  22/12/2015 5:30:41 PM
-- Description:	Procedimiento almacenado que guarda el estado de cada módulo por proveedor y genera el reporte de proveedores
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Proveedores_GetDataRepProvGeneral2Tmp]
			@sector nvarchar(50), 
			@est  nvarchar(50), 
			@fini date, 
			@ffin date,
			@dep  nvarchar(max), 
			@muni  nvarchar(max), 
			@tipopersona  nvarchar(50)
AS

begin 

	--------------------------------------------------------------------------------------------------------------------------
	

		DECLARE @TmpEntidadModuloEstado TABLE (
				[IdEntidad] [INT]   NOT NULL,
				[IdModulo] [INT]  NOT NULL,
				[IdEstado]  [NVARCHAR] (20) NOT NULL,
				[FechaEstado]   [NVARCHAR] (100) NULL,
				[FechaEstado2]   [NVARCHAR] (100) NULL,
				[Observacion] [NVARCHAR] (255) NULL,
				[UsuarioCrea] [NVARCHAR] (250)  NOT NULL,
				[FechaCrea]   [DATETIME]  NOT NULL
				)

     
	 --------------------------------------------------------------------------------------------------------------------------
				 ---financiero

	    INSERT INTO @TmpEntidadModuloEstado

				select	t.IdEntidad, 3, t.IdEstado
						,FechaEstado= 
										(select convert(varchar,cast(isnull(max(vi.FechaModifica),max(vi.FechaCrea)) as date),103)
										  from PROVEEDOR.ValidarInfoFinancieraEntidad vi
										 where vi.IdInfoFin in (select	a.IdInfoFin
																from	[PROVEEDOR].[InfoFinancieraEntidad] a
																where a.IdEntidad=t.idEntidad
															   )
										)
						,FechaEstado2 = (Select convert(varchar,cast(isnull(max(ie.FechaModifica),max(ie.FechaCrea)) as date),103)
												from PROVEEDOR.InfoFinancieraEntidad ie
										 where ie.IdInfoFin in (select	a.IdInfoFin
																from	[PROVEEDOR].[InfoFinancieraEntidad] a
																where a.IdEntidad=t.idEntidad
															   )
										 )
						,NULL
						,'RepGenProv',GETDATE() 
				from	(
						select	distinct
								ie.IdEntidad
								,case when t.IdEntidad is null then 'VALIDADO' else 'PARCIAL' end	as IdEstado
						from	Proveedor.InfoFinancieraEntidad ie with(nolock)
								left join
								(
								select	distinct ie2.IdEntidad
								from	Proveedor.InfoFinancieraEntidad ie2 with(nolock)
								where	ie2.Finalizado = 0
								) as t
								on t.IdEntidad = ie.IdEntidad
						
						) t
				group by t.IdEntidad,t.IdEstado--,t.FechaEstado2
				order by t.IdEntidad



--------------------------------------------------------------------------------------------------------------
---experiencias

			INSERT INTO @TmpEntidadModuloEstado
				select	distinct t.IdEntidad, 4, t.IdEstado
						,FechaEstado = 
										(select convert(varchar,cast(isnull(max(vi.FechaModifica),max(vi.FechaCrea)) as date),103)
											from PROVEEDOR.ValidarInfoExperienciaEntidad vi
											where vi.IdExpEntidad in (select	a.IdExpEntidad
																from PROVEEDOR.InfoExperienciaEntidad a
																where a.IdEntidad=t.idEntidad
																)
										)
						,FechaEstado2 = (Select convert(varchar,cast(isnull(max(ie.FechaModifica),max(ie.FechaCrea)) as date),103)
												from PROVEEDOR.InfoExperienciaEntidad ie
										 where ie.IdExpEntidad in (select	a.IdExpEntidad
																from	[PROVEEDOR].[InfoExperienciaEntidad] a
																where a.IdEntidad=t.idEntidad
															   )
										 )
						,NULL,'RepGenProv',GETDATE() 
				from	(
						select	distinct
								ie.IdEntidad
								,case when t.IdEntidad is null then 'VALIDADO' else 'PARCIAL' end			as IdEstado
								,convert(varchar,cast(isnull(ie.FechaModifica,ie.FechaCrea)	 as date),103)	as FechaEstado2
						from	Proveedor.InfoExperienciaEntidad ie with(nolock)
								left join
								(
								select	distinct ie2.IdEntidad
								from	Proveedor.InfoExperienciaEntidad ie2 with(nolock)
								where	ie2.Finalizado = 0
								) as t
								on t.IdEntidad = ie.IdEntidad
						) t
				group by t.IdEntidad,t.IdEstado,t.FechaEstado2
				order by t.IdEntidad
		

--------------------------------------------------------------------------------------------------------------

		---Datos Básicos
		INSERT INTO @TmpEntidadModuloEstado

			select	distinct t.IdEntidad, 1, es.Descripcion, max(FechaEstado) as FechaEstado,null
					,Observacion= 
									(select observaciones
										from PROVEEDOR.ValidarInfoDatosBasicosEntidad vi
										where vi.IdValidarInfoDatosBasicosEntidad in (select max(a.IdValidarInfoDatosBasicosEntidad)
															from	PROVEEDOR.ValidarInfoDatosBasicosEntidad a
															where a.IdEntidad=t.idEntidad)
									)
			
					,'RepGenProv',GETDATE() 
			from	(
					select	distinct
							ie.IdEntidad
							,ie.IdEstado
							,convert(varchar,cast(isnull(ie.FechaModifica,ie.FechaCrea)	 as date),103)	as FechaEstado
					from	PROVEEDOR.EntidadProvOferente ie with(nolock)
							left join
							(
							select	distinct ie2.IdEntidad
							from	PROVEEDOR.EntidadProvOferente ie2 with(nolock)
							where	ie2.Finalizado = 0
							) as t
							on t.IdEntidad = ie.IdEntidad
					) t
					INNER JOIN PROVEEDOR.EstadoDatosBasicos ES ON ES.IdEstadoDatosBasicos = t.IdEstado
			group by t.IdEntidad,es.Descripcion
			order by t.IdEntidad

			update @TmpEntidadModuloEstado SET Observacion='Sin Información'
			where Observacion ='' and IdModulo=1

			----select * from @TmpEntidadModuloEstado

--------------------------------------------------------------------------------------------------------------------------
 ---REPORTE-----
 ---------------------------------------------------------------------------------------------------------------------------

 		SELECT 
			   sector = y.[Descripcion] 
			  ,NombreDepartamento=d.[NombreDepartamento]
			  ,NombreMunicipio=e.[NombreMunicipio]
			  ,NombreTipoPersona=z.[NombreTipoPersona]
			  ,CodDocumento=b.[CodDocumento] 
			  ,Identi= (t.[NUMEROIDENTIFICACION]+'-'+convert(nvarchar(18),t.[DIGITOVERIFICACION])) 
			  , Proveedor = (case when t.[RAZONSOCIAL] is null then 
								  (ISNULL(t.[PRIMERNOMBRE],'')+' '+ISNULL(t.[SEGUNDONOMBRE],'')+' '+ISNULL(t.[PRIMERAPELLIDO],'')+' '+isnull(t.[SEGUNDOAPELLIDO],''))
								  else  [RAZONSOCIAL]
							 end)
			  ,Telefono= 
					case when (Select count(*) from [Tercero].[TelTerceros] TELT where TELT.Idtercero=t.IdTercero) >0
					          then (select '('+convert(varchar,cast(isnull(TELT.[IndicativoTelefono],'') as int))+') '+
									convert(varchar,cast(isnull(TELT.[NumeroTelefono],'')as bigint))+' E: '+
									convert(varchar,cast(isnull(TELT.[ExtensionTelefono],'')as int))  
					    	   from [Tercero].[TelTerceros] TELT 
							  where TELT.Idtercero=t.IdTercero
								and TELT.IdTelTercero = (Select max(TELT.IdTelTercero) FROM [Tercero].[TelTerceros] TELT where TELT.Idtercero=t.IdTercero)
							)
							else 'Sin Información'
							end
			  ,Celular= case when (Select count (*) from [Tercero].[TelTerceros] TELT  where TELT.Idtercero=t.IdTercero) >0
							  then (Select convert(varchar,cast(isnull(TELT.Movil,'') as bigint)) from [Tercero].[TelTerceros] TELT  where TELT.Idtercero=t.IdTercero 
									   and TELT.IdTelTercero = (Select max(TELT.IdTelTercero) FROM [Tercero].[TelTerceros] TELT where TELT.Idtercero=t.IdTercero))
							  end
			  ,valorbas= (
						  case when (SELECT [ConfirmaYAprueba]      
						  FROM [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]
						  where [IdValidarInfoDatosBasicosEntidad] in ( select max(FE2.[IdValidarInfoDatosBasicosEntidad])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] FE2
												where FE2.[FechaCrea] in (select max(v.[FechaCrea])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] v
												where x.[IdEntidad] = v.[IdEntidad]))) = 1 then  'SI'
            
						  when (SELECT [ConfirmaYAprueba]      
						  FROM [PROVEEDOR].[ValidarInfoDatosBasicosEntidad]
						  where [IdValidarInfoDatosBasicosEntidad] in ( select max(FE2.[IdValidarInfoDatosBasicosEntidad])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] FE2
												where FE2.[FechaCrea] in (select max(v.[FechaCrea])
												from [PROVEEDOR].[ValidarInfoDatosBasicosEntidad] v
												where x.[IdEntidad] = v.[IdEntidad]))) = 0 then  'NO'                   
						  else   'Sin Validar o En Proceso'
						  END) 
			  ,valorfi=  (
						  case when (SELECT [ConfirmaYAprueba]      
						  FROM [PROVEEDOR].[ValidarInfoFinancieraEntidad]
						  where [IdValidarInfoFinancieraEntidad] in ( select max(FE2.[IdValidarInfoFinancieraEntidad])
												from [PROVEEDOR].[ValidarInfoFinancieraEntidad] FE2
												where FE2.[IdInfoFin] in (select max([IdInfoFin])
												from [PROVEEDOR].[InfoFinancieraEntidad] IX
												 where x.[IdEntidad] = IX.[IdEntidad]))) = 0 then  'SI'
            
						  when (SELECT [ConfirmaYAprueba]     
						  FROM [PROVEEDOR].[ValidarInfoFinancieraEntidad] 
						  where [IdValidarInfoFinancieraEntidad] in ( select max(FE2.[IdValidarInfoFinancieraEntidad])
												from [PROVEEDOR].[ValidarInfoFinancieraEntidad] FE2
												where FE2.[IdInfoFin] in (select max([IdInfoFin])
												from [PROVEEDOR].[InfoFinancieraEntidad] IX
												 where x.[IdEntidad] = IX.[IdEntidad]))) = 1 then  'NO'
						  else 
							 (case when (select count(*)
										 from [PROVEEDOR].[InfoFinancieraEntidad] FE2
										 where FE2.[IdInfoFin] in (select max([IdInfoFin])
										 from [PROVEEDOR].[InfoFinancieraEntidad] IX
										 where x.[IdEntidad] = IX.[IdEntidad])
										 and [EstadoValidacion]  in (1,5) ) > 0 then  'SI'
									else 'Sin Información'          
                 					end)            
						  END) 
			  ,valorexp=(select dbo.Exp_pendiente(x.[IdEntidad])) 
			  ,c.[CodigoCiiu]
			  ,Actividad=c.[Descripcion] 
			  ,t.[CORREOELECTRONICO]
			  ,EstadoDatosBasicos = case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1) > 0
										 then 
											(select eme.IdEstado from @TmpEntidadModuloEstado eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1)
										 else 'Sin Información'
										 end
			  ,FechaEstadoDatosBasicos = case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1) > 0
										 then 
										 (select eme.FechaEstado from @TmpEntidadModuloEstado eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1)
										 else 'Sin Información'
										 end
			  ,ObservacionesDatosBasicos = case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1) > 0
										 then 
										 (select eme.Observacion from @TmpEntidadModuloEstado eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 1)
										 else 'Sin Información'
										 end
			  ,EstadoDatosFinancieros = case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3) > 0
										 then
										 (select eme.IdEstado from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3)
										else 'Sin Información'
										 end
			  ,FechaEstadoDatosFinancieros=  case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3) > 0
										 then
										 (select isnull(eme.FechaEstado,eme.FechaEstado2) from @TmpEntidadModuloEstado eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 3)
											else 'Sin Información'
										 end
			  ,EstadoExperiencia = case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4) > 0
										 then
										 (select eme.IdEstado from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4)
								   else 'Sin Información'
										 end
			  ,FechaEstadoExperiencias=  case when (select count(eme.IdEntidad) from @TmpEntidadModuloEstado eme  where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4) > 0
										 then
										 (select isnull(eme.FechaEstado,eme.FechaEstado2)  from @TmpEntidadModuloEstado eme where eme.IdEntidad = x.IdEntidad and eme.IdModulo = 4)
										else 'Sin Información'
										 end
          FROM [PROVEEDOR].[EntidadProvOferente] x
				INNER  JOIN [PROVEEDOR].[TipoSectorEntidad] y WITH(NOLOCK) ON x.[IdTipoSector] = y.[IdTipoSectorEntidad] 
				INNER  JOIN [PROVEEDOR].[InfoAdminEntidad] a WITH(NOLOCK) ON a.[IdEntidad] = x.[IdEntidad]
				INNER  JOIN [DIV].[Departamento] d WITH(NOLOCK) ON a.[IdDepartamentoDirComercial]= d.[IdDepartamento]
				INNER  JOIN [DIV].[Municipio] e WITH(NOLOCK) ON a.[IdMunicipioDirComercial] = e.[IdMunicipio]
				INNER  JOIN [Tercero].[TERCERO] t  WITH(NOLOCK) ON x.IdTercero = t.[IDTERCERO]
				INNER  JOIN [Tercero].[TipoPersona] z WITH(NOLOCK) ON z.[IdTipoPersona] = t.[IdTipoPersona]
				INNER  JOIN [Global].[TiposDocumentos] b WITH(NOLOCK) ON t.[IDTIPODOCIDENTIFICA] = b.[IdTipoDocumento]
				LEFT  JOIN [PROVEEDOR].[TipoCiiu] c WITH(NOLOCK) ON c.[IdTipoCiiu] = x.[IdTipoCiiuPrincipal] 

				INNER  JOIN StringSplit((@sector),1,0) sec on sec.val = y.[IdTipoSectorEntidad]
				INNER  JOIN StringSplit((@est),1,0) es on es.val = x.IdEstadoProveedor
				INNER  JOIN StringSplit((@dep),1,0) depto on depto.val = d.[IdDepartamento]
				INNER  JOIN StringSplit((@muni),1,0) munic on munic.val = e.[IdMunicipio]
				INNER  JOIN StringSplit((@tipopersona),1,0) per on per.val = t.[IdTipoPersona]
		where  	(x.[FechaCrea] >= @fini or @fini is null)
				and (x.[FechaCrea] <= @ffin or @ffin is null)	
		order by  2,3


		
end

