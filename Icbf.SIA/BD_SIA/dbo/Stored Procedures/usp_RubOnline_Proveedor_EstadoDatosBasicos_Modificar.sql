﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Modificar]
		@IdEstadoDatosBasicos INT,	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoDatosBasicos 
		SET Descripcion = @Descripcion, 
			Estado = @Estado, 
			UsuarioModifica = @UsuarioModifica, 
			FechaModifica = GETDATE() 
		WHERE IdEstadoDatosBasicos = @IdEstadoDatosBasicos
END

