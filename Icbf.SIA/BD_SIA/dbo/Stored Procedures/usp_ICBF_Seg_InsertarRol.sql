﻿

-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que guarda información de un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarRol]
		  @IdRol INT OUTPUT
		, @ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(20)
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioCreacion NVARCHAR(250)
		, @EsAdministrador BIT = 0
AS
BEGIN
	INSERT INTO SEG.Rol([providerKey], [Nombre], [Descripcion], [Estado], [UsuarioCreacion], FechaCreacion,EsAdministrador)
					  VALUES(@ProviderKey, @Nombre, @Descripcion, @Estado, @UsuarioCreacion, GETDATE(),@EsAdministrador)
	SELECT @IdRol = @@IDENTITY 					  
END

