﻿
-- =============================================
-- Author:		Fabián Valencia
-- Create date:  2013-06-09
-- Description:	Procedimiento almacenado que consulta los Tercero y los proveedores que tengan estado validado
-- Modificado: 10/04/2014
-- Descripcion: Cuando se consulta un proveedor asigna la columna de estado para el estado del proveedor y no 
-- el estado de datos básicos
-- Modificado: 11-AGO-2014 Juan Carlos Valverde Sámano
-- Descripción: Se agrega la consulta la información para Consorción y Uniones temporales.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Terceros_Consultar_Validados]

@isTercero int = NULL,
@IDTIPODOCIDENTIFICA int = NULL,
@IdTipoPersona int = NULL,
@NUMEROIDENTIFICACION varchar (30) = NULL,
@USUARIOCREA varchar (128) = NULL,
@Tercero varchar (128) = NULL
AS
BEGIN
IF @isTercero = 1
BEGIN
SELECT DISTINCT
	Oferente.TERCERO.IDTERCERO,
	Global.TiposDocumentos.CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
	Oferente.TERCERO.NUMEROIDENTIFICACION,
	Oferente.TERCERO.PRIMERNOMBRE,
	Oferente.TERCERO.SEGUNDONOMBRE,
	Oferente.TERCERO.PRIMERAPELLIDO,
	Oferente.TERCERO.SEGUNDOAPELLIDO,
	Oferente.TERCERO.RAZONSOCIAL,
	Oferente.TERCERO.FECHACREA,
	Oferente.EstadoTercero.CodigoEstadotercero,
	Oferente.EstadoTercero.DescripcionEstado
FROM Oferente.TERCERO
INNER JOIN Global.TiposDocumentos
	ON Oferente.TERCERO.IDTIPODOCIDENTIFICA = Global.TiposDocumentos.IdTipoDocumento
INNER JOIN Oferente.EstadoTercero
	ON Oferente.TERCERO.IDESTADOTERCERO = Oferente.EstadoTercero.IdEstadoTercero
INNER JOIN Oferente.TipoPersona
	ON Oferente.TERCERO.IdTipoPersona = Oferente.TipoPersona.IdTipoPersona
WHERE Oferente.TERCERO.IDTIPODOCIDENTIFICA =
	CASE
		WHEN @IDTIPODOCIDENTIFICA IS NULL THEN Oferente.TERCERO.IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
	END
AND Oferente.TERCERO.IdTipoPersona =
	CASE
		WHEN @IdTipoPersona IS NULL THEN Oferente.TERCERO.IdTipoPersona ELSE @IdTipoPersona
	END
AND Oferente.TERCERO.NUMEROIDENTIFICACION =
	CASE
		WHEN @NUMEROIDENTIFICACION IS NULL THEN Oferente.TERCERO.NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
	END
--AND Oferente.TERCERO.USUARIOCREA =
--	CASE
--		WHEN @USUARIOCREA IS NULL THEN Oferente.TERCERO.USUARIOCREA ELSE @USUARIOCREA
--	END
AND (
ISNULL(Oferente.TERCERO.PRIMERNOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDONOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.PRIMERAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.RAZONSOCIAL, '') LIKE CASE
	WHEN @Tercero IS NULL THEN ISNULL(Oferente.TERCERO.PRIMERNOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDONOMBRE, '') + ' ' + ISNULL(Oferente.TERCERO.PRIMERAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.SEGUNDOAPELLIDO, '') + ' ' + ISNULL(Oferente.TERCERO.RAZONSOCIAL, '') ELSE '%' + @Tercero + '%'
END
)
AND Oferente.EstadoTercero.CodigoEstadotercero = '004' --Estado validado
END
ELSE
BEGIN
DECLARE @Validado INT
SET @Validado = (SELECT
	IdEstadoProveedor
FROM Proveedor.EstadoProveedor
WHERE Descripcion = 'VALIDADO')
PRINT @Validado

/*Estado validado*/


SELECT DISTINCT
	IdTercero,
	CodDocumento AS CodigoTipoIdentificacionPersonaNatural,
	NUMEROIDENTIFICACION,
	PRIMERNOMBRE,
	SEGUNDONOMBRE,
	PRIMERAPELLIDO,
	SEGUNDOAPELLIDO,
	RAZONSOCIAL,
	FECHACREA,
	IdEstadoProveedor AS CodigoEstadotercero,
	Estado AS DescripcionEstado
FROM (SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.PRIMERNOMBRE + ' ' + ISNULL(T.SEGUNDONOMBRE, '') + ' ' + T.PRIMERAPELLIDO + ' ' + ISNULL(T.SEGUNDOAPELLIDO, '')) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstadoProveedor,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
INNER JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
--INNER JOIN Proveedor.EstadoDatosBasicos E
--	ON EP.IdEstado = E.IdEstadoDatosBasicos
INNER JOIN PROVEEDOR.EstadoProveedor E ON EP.IdEstadoProveedor = E.IdEstadoProveedor
WHERE T.IdTipoPersona = 1 UNION SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.RAZONSOCIAL) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstadoProveedor,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
LEFT JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
--INNER JOIN Proveedor.EstadoDatosBasicos E
--	ON EP.IdEstado = E.IdEstadoDatosBasicos
INNER JOIN PROVEEDOR.EstadoProveedor E ON EP.IdEstadoProveedor = E.IdEstadoProveedor
WHERE T.IdTipoPersona = 2 UNION SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.RAZONSOCIAL) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstadoProveedor,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
LEFT JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
--INNER JOIN Proveedor.EstadoDatosBasicos E
--	ON EP.IdEstado = E.IdEstadoDatosBasicos
INNER JOIN PROVEEDOR.EstadoProveedor E ON EP.IdEstadoProveedor = E.IdEstadoProveedor
WHERE T.IdTipoPersona = 2	UNION SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.RAZONSOCIAL) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstadoProveedor,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
LEFT JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
--INNER JOIN Proveedor.EstadoDatosBasicos E
--	ON EP.IdEstado = E.IdEstadoDatosBasicos
INNER JOIN PROVEEDOR.EstadoProveedor E ON EP.IdEstadoProveedor = E.IdEstadoProveedor
WHERE T.IdTipoPersona = 3 UNION SELECT
	EP.IdEntidad,
	EP.ConsecutivoInterno,
	T.IdTipoPersona,
	Tp.NombreTipoPersona,
	TD.CodDocumento,
	T.IDTIPODOCIDENTIFICA,
	T.NUMEROIDENTIFICACION AS NumeroIdentificacion,
	EP.IdTercero,
	(T.RAZONSOCIAL) AS Razonsocila,
	E.Descripcion AS Estado,
	EP.IdEstadoProveedor,
	EP.ObserValidador,
	EP.UsuarioCrea,
	T.PRIMERNOMBRE,
	T.SEGUNDONOMBRE,
	T.PRIMERAPELLIDO,
	T.SEGUNDOAPELLIDO,
	T.RAZONSOCIAL,
	EP.FechaCrea
FROM [Proveedor].[EntidadProvOferente] EP
INNER JOIN oferente.TERCERO T
	ON EP.IdTercero = T.IDTERCERO
LEFT JOIN Proveedor.InfoAdminEntidad IAE
	ON EP.IdEntidad = IAE.IdEntidad
INNER JOIN Oferente.TipoPersona TP
	ON T.IdTipoPersona = TP.IdTipoPersona
INNER JOIN [Global].TiposDocumentos TD
	ON T.IDTIPODOCIDENTIFICA = TD.IdTipoDocumento
--INNER JOIN Proveedor.EstadoDatosBasicos E
--	ON EP.IdEstado = E.IdEstadoDatosBasicos
INNER JOIN PROVEEDOR.EstadoProveedor E ON EP.IdEstadoProveedor = E.IdEstadoProveedor
WHERE T.IdTipoPersona = 4)
DT

WHERE IdTipoPersona =
	CASE
		WHEN @IdTipoPersona IS NULL THEN IdTipoPersona ELSE @IdTipoPersona
	END
AND IDTIPODOCIDENTIFICA =
	CASE
		WHEN @IDTIPODOCIDENTIFICA IS NULL THEN IDTIPODOCIDENTIFICA ELSE @IDTIPODOCIDENTIFICA
	END
AND NUMEROIDENTIFICACION =
	CASE
		WHEN @NUMEROIDENTIFICACION IS NULL THEN NUMEROIDENTIFICACION ELSE @NUMEROIDENTIFICACION
	END
AND (Razonsocila) LIKE CASE
	WHEN @Tercero IS NULL THEN (Razonsocila) ELSE '%' + @Tercero + '%'
END
AND IdEstadoProveedor =
	CASE
		WHEN @Validado IS NULL THEN IdEstadoProveedor ELSE @Validado
	END
--AND UsuarioCrea =
--	CASE
--		WHEN @UsuarioCrea IS NULL THEN UsuarioCrea ELSE @UsuarioCrea
--	END



END

END


