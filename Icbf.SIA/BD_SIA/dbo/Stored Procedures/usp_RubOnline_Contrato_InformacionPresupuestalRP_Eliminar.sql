﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que elimina un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Eliminar]
	@IdInformacionPresupuestalRP INT
AS
BEGIN
	DELETE Contrato.InformacionPresupuestalRP WHERE IdInformacionPresupuestalRP = @IdInformacionPresupuestalRP
END

