﻿

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_SalarioMinimo_Consultar_IdVigencia]
	@IdVigencia INT = NULL
AS
 
 declare @SMLV int
 
 SELECT @SMLV = Valor FROM oferente.SalarioMinimo s
 inner join [Global].[Vigencia] v on [Año] = [AcnoVigencia]
 and v.Activo = 1 and s.Estado = 1
 and @IdVigencia = IdVigencia

 SELECT @SMLV
