﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que elimina un(a) LugarEjecucion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucion_Eliminar]
	@IdContratoLugarEjecucion INT
AS
BEGIN
	DELETE Contrato.LugarEjecucion WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

