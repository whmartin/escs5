﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que actualiza un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Modificar]
		@IdRamaEstructura INT,	@CodigoRamaEstructura NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.RamaoEstructura SET CodigoRamaEstructura = @CodigoRamaEstructura, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRamaEstructura = @IdRamaEstructura
END

