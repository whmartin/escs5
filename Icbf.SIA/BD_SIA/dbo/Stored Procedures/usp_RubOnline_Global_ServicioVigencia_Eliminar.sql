﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que elimina un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Eliminar]
	@IdServicioVigencia INT
AS
BEGIN
	DELETE Global.ServicioVigencia WHERE IdServicioVigencia = @IdServicioVigencia
END

