﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/18/2015 1:47:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Pasos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Pasos_Consultar]
	@IdPaso INT
AS
BEGIN
 SELECT IdPaso, Paso, Estado, Financiera, IdAccion, AccionEstado, Obligatorio, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[Pasos] WHERE  IdPaso = @IdPaso
END
