﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que consulta un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Consultar]
	@IdTipoUsuario INT
AS
BEGIN
 SELECT IdTipoUsuario, CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SEG].[TipoUsuario] WHERE  IdTipoUsuario = @IdTipoUsuario
END
