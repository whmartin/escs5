﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que elimina un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Eliminar]
	@IdArchivo NUMERIC(18,0)
AS
BEGIN
	DELETE Estructura.Archivo WHERE IdArchivo = @IdArchivo
END

