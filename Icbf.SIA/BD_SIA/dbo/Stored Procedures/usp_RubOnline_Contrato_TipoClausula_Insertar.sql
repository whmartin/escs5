﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Insertar]
		@IdTipoClausula INT OUTPUT, 	@NombreTipoClausula NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoClausula(NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoClausula, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoClausula = @@IDENTITY 		
END

