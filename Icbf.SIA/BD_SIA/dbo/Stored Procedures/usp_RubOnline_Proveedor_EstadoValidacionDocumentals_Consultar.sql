﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que consulta un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumentals_Consultar]
	@CodigoEstadoValidacionDocumental NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdEstadoValidacionDocumental, CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[EstadoValidacionDocumental] WHERE CodigoEstadoValidacionDocumental = CASE WHEN @CodigoEstadoValidacionDocumental IS NULL THEN CodigoEstadoValidacionDocumental ELSE @CodigoEstadoValidacionDocumental END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

