﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:06:28 PM
-- Description:	Procedimiento almacenado que guarda un nuevo FlujoPasosCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuenta_Insertar]
		@IdFlujoPasoCuenta INT OUTPUT, 	@IdTipoCuenta INT,	@IdPaso INT,	@Orden INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.FlujoPasosCuenta(IdTipoCuenta, IdPaso, Orden, UsuarioCrea, FechaCrea,estado)
					  VALUES(@IdTipoCuenta, @IdPaso, @Orden, @UsuarioCrea, GETDATE(),1)
	SELECT @IdFlujoPasoCuenta = @@IDENTITY 		
END

