﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 11:20:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoExperienciaEntidad_Eliminar]
	@IdExpEntidad INT
AS
BEGIN
	DELETE Proveedor.InfoExperienciaEntidad WHERE IdExpEntidad = @IdExpEntidad
END

