﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 12:03:54 PM
-- Description:	Procedimiento almacenado que actualiza un(a) DatosAdmonObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdmonObligaciones_Modificar]
		@IdDatosAdmonObligaciones INT,	@IdDatosAdministracion INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.DatosAdmonObligaciones SET IdDatosAdministracion = @IdDatosAdministracion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDatosAdmonObligaciones = @IdDatosAdmonObligaciones
END
