﻿

-- =============================================
-- Author:        Oscar Javier Sosa Parada
-- Create date: 08-10-2012
-- Description:   Procedimiento almacenado para la consulta de tipos de documento
-- =============================================
CREATE PROCEDURE [dbo].[usp_Global_Seg_ConsultarTodosTipoDocumento]
AS
BEGIN
  SELECT 
         IdTipoDocumento
      ,CodDocumento
      ,NomTipoDocumento
  FROM Global.TiposDocumentos
END


