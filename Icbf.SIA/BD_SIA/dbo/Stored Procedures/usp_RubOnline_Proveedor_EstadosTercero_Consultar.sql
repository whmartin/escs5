﻿-- =============================================
-- Author:		<Fabian Valencia>
-- Create date: <12/06/2013>
-- Description:	<Obtiene el estado a través de su id>
-- =============================================
Create PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadosTercero_Consultar]
	 @IdTercero INT
AS
BEGIN
	SELECT     Oferente.EstadoTercero.*
	FROM         Oferente.EstadoTercero
	WHERE		Oferente.EstadoTercero.IdEstadoTercero = @IdTercero
END

