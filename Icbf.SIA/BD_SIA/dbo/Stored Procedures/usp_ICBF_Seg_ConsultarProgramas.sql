﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  programas
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Seg_ConsultarProgramas]
	@pIdModulo INT,
	@pNombreModulo NVARCHAR(250)
AS
BEGIN

SELECT 
	 P.IdPrograma
	,P.IdModulo
	,P.NombrePrograma
	,P.CodigoPrograma
	,P.Posicion
	,P.Estado
	,P.UsuarioCreacion
	,P.FechaCreacion
	,P.UsuarioModificacion
	,P.FechaModificacion
	,M.NombreModulo
	,P.VisibleMenu
	,P.EsReporte
	,P.IdReporte  
FROM SEG.Programa P (NoLock)
     INNER JOIN SEG.Modulo M (NoLock) ON P.IdModulo = M.IdModulo	
WHERE   (P.IdModulo = @pIdModulo Or @pIdModulo Is Null)
		AND P.NombrePrograma LIKE CASE WHEN @pNombreModulo = '' THEN P.NombrePrograma ELSE '%'+@pNombreModulo+'%' END

END
