﻿





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que modifica un ValidarInfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_Modificar]
		@IdValidarInfoFinancieraEntidad INT, @IdInfoFin INT, @NroRevision INT, @Observaciones NVARCHAR(200),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	UPDATE Proveedor.ValidarInfoFinancieraEntidad
	SET  Observaciones = @Observaciones,
		 ConfirmaYAprueba = @ConfirmaYAprueba, 
		 UsuarioCrea = @UsuarioCrea,
		 FechaCrea = GETDATE()
	WHERE 
		IdInfoFin = @IdInfoFin
		and NroRevision = @NroRevision			
		and IdValidarInfoFinancieraEntidad = @IdValidarInfoFinancieraEntidad
END




