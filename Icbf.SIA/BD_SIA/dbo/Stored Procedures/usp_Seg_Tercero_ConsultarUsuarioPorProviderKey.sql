﻿
-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que consulta un usuario por provideerkey
-- =============================================


CREATE PROCEDURE [dbo].[usp_Seg_Tercero_ConsultarUsuarioPorProviderKey]
	@ProviderKey varchar(125)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
	  ,SEG.Usuario.IdRegional
	  ,SEG.Usuario.IdTipoUsuario
	  ,SEG.Usuario.OferentesMigrados
	  ,SEG.Usuario.ValidarOferentesMigrados
	  ,SEG.Usuario.TodosRegional
	  ,SEG.Usuario.AceptaEnvioCorreos
  FROM SEG.Usuario
  WHERE
	providerKey = @ProviderKey
END
