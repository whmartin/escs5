﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRP_Insertar]
		@IdInformacionPresupuestalRP INT OUTPUT, 	@FechaSolicitudRP DATETIME,	@NumeroRP INT,	@ValorRP INT,	@FechaExpedicionRP DATETIME, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.InformacionPresupuestalRP(FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea)
					  VALUES(@FechaSolicitudRP, @NumeroRP, @ValorRP, @FechaExpedicionRP, @UsuarioCrea, GETDATE())
	SELECT @IdInformacionPresupuestalRP = @@IDENTITY 		
END

