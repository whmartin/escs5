﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que guarda un nuevo MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Insertar]
		@IdMotivoEstado INT OUTPUT, 	@CodigoMotivoEstado NVARCHAR(128),	@NombreMotivoEstado NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.MotivoEstado(CodigoMotivoEstado, NombreMotivoEstado, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoMotivoEstado, @NombreMotivoEstado, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdMotivoEstado = @@IDENTITY 		
END


