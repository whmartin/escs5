﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	Se agregan los campos EsReporte y IdReporte
-- =============================================
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo Programa
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarPrograma]
		 @IdPrograma INT OUTPUT
		,@IdModulo INT
		,@NombrePrograma NVARCHAR(250)
		,@CodigoPrograma NVARCHAR(250)
		,@Posicion INT
		,@Estado INT
		,@UsuarioCreacion NVARCHAR(250)
		,@VisibleMenu	BIT
		,@GeneraLog	BIT
		,@EsReporte INT
		,@IdReporte INT
AS
BEGIN
	INSERT INTO SEG.Programa(IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, VisibleMenu, generaLog,EsReporte,IdReporte)
					  VALUES(@IdModulo, @NombrePrograma, @CodigoPrograma, @Posicion, @Estado, @UsuarioCreacion, GETDATE(), @VisibleMenu, @GeneraLog,@EsReporte,@IdReporte)
	SELECT @IdPrograma = @@IDENTITY 		
END
