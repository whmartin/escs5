﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Insertar]
		@IdFormapago INT OUTPUT, 	@NombreFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.FormaPago(NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreFormaPago, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdFormapago = @@IDENTITY 		
END

