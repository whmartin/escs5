﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 12:03:54 PM
-- Description:	Procedimiento almacenado que elimina un(a) DatosAdmonObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdmonObligaciones_Eliminar]
	@IdDatosAdmonObligaciones INT
AS
BEGIN
	DELETE SPCP.DatosAdmonObligaciones WHERE IdDatosAdmonObligaciones = @IdDatosAdmonObligaciones
END
