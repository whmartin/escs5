﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/6/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que consulta un(a) PlanDePagos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_PlanDePagoss_Consultar]
	@Codpci NVARCHAR(255) = NULL,
	@Vigencia INT = NULL,
	@ConsecutivoPACCO INT = NULL,
	@CodCompromiso INT = NULL,
	@NumDocIdentidad NVARCHAR(20) = NULL,
	@Estado INT = NULL
AS
BEGIN

 SELECT IdPlanDePagos, 
 IdDatosAdministracion, 
 Codpci, 
 Vigencia, 
 ConsecutivoPACCO, 
 CodCompromiso, 
 Fecha, 
 TipoDocumento, 
 NumDocIdentidad, 
 CodTipoDocSoporte, 
 NumDocSoporte, 
 ValorTotalContrato, 
 Rubro, 
 Recurso, 
 Fuente, 
 NumeroDePago, 
 OrdenDePago, 
 ValorDelPago, 
 Estado, 
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica
 --, EstadoView
 FROM [SPCP].[PlanDePagos] 
 WHERE Codpci = isnull(@Codpci,Codpci)
 and Estado = isnull(@Estado,Estado)
and Vigencia = isnull(@Vigencia,Vigencia)
And ConsecutivoPACCO = isnull(@ConsecutivoPACCO,ConsecutivoPACCO)
And CodCompromiso = isnull(@CodCompromiso,CodCompromiso)
And NumDocIdentidad = isnull(@NumDocIdentidad,NumDocIdentidad)

END