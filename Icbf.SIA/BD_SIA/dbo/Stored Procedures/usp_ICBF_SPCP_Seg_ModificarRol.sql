﻿
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que modifica información de un rol
-- Grupo de Apoyo SSII - SIGEPCYP 31/07/2015
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ModificarRol]
		  @pIdRol INT
		, @ProviderKey VARCHAR(125)
		, @Nombre NVARCHAR(20)
		, @IdPaso int
		, @AprobacionPAC BIT
		, @Descripcion NVARCHAR(200)
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Rol
	SET providerKey = @ProviderKey
		, [Nombre]=@Nombre
		, [Descripcion]=@Descripcion
		, [IdPaso]=@IdPaso
		, [AprobacionPAC]=@AprobacionPAC
		, [Estado]=@Estado
		, [UsuarioModificacion]=@UsuarioModificacion
		, FechaModificacion=GETDATE()
	WHERE IdRol=@pIdRol
END

