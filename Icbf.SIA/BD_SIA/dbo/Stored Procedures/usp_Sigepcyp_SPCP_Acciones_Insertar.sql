﻿
--- =================================================================
-- Author:		<Author,,Grupo Desarrollo SIGEPCYP>
-- Create date:  8/20/2015 8:24:06 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Acciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Acciones_Insertar]
		@IdAcciones INT OUTPUT, 	@Descripcion NVARCHAR(160),	@Estado INT,	@CodAccion NVARCHAR(10), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.Acciones(Descripcion, Estado, CodAccion, UsuarioCrea, FechaCrea)
					  VALUES(@Descripcion, @Estado, @CodAccion, @UsuarioCrea, GETDATE())
	SELECT @IdAcciones = @@IDENTITY 		
END
