﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/15/2015 6:00:03 PM
-- Description:	Procedimiento almacenado que consulta un(a) Obligaciones por Productos Contractuales
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Obligaciones_X_ProductosContractuales_Consultar]
	@IdDatosAdministracion INT = NULL,@NumeroPago INT = NULL,@Estado INT = NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@IdDatosAdministracion INT,@NumeroPago INT,@Estado INT'
Set @SqlExec = '
				SELECT	oc.IdObligacionContractual, 
						ObligacionContractual,
						IdProductoContractual,
						ProductoContractual,
						pc.NumeroPago,
						pc.Estado
				FROM	[SPCP].[DatosAdmonObligaciones] da
						INNER JOIN [SPCP].[ObligacionesContractuales] oc
							ON da.IdDatosAdmonObligaciones = oc.IdDatosAdmonObligaciones
						INNER JOIN [SPCP].[ProductosContractuales] pc
							ON oc.IdObligacionContractual = pc.IdObligacionContractual
				WHERE	1=1 '
						If(@IdDatosAdministracion Is Not Null) Set @SqlExec = @SqlExec + ' And da.IdDatosAdministracion = @IdDatosAdministracion' 
						If(@NumeroPago Is Not Null) Set @SqlExec = @SqlExec + ' And pc.NumeroPago = @NumeroPago' 
						If(@Estado Is Not Null) Set @SqlExec = @SqlExec + ' And pc.Estado = @Estado' 
						
Exec sp_executesql  @SqlExec, @SqlParametros,@IdDatosAdministracion,@NumeroPago,@Estado
END
