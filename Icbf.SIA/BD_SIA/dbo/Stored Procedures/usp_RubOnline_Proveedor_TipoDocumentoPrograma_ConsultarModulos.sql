﻿
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 28/06/2013
-- Description:	Obtiene los programas del proveedor
-- =============================================
CREATE PROCEDURE  [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_ConsultarModulos] 
	@idModulo INT
AS
BEGIN
SELECT     IdPrograma, IdModulo, NombrePrograma, CodigoPrograma, Posicion, Estado, UsuarioCreacion, FechaCreacion, UsuarioModificacion, FechaModificacion, VisibleMenu,
                       generaLog, Padre
FROM         SEG.Programa
WHERE     (CodigoPrograma  in ( 'PROVEEDOR/DocFinancieraProv','PROVEEDOR/GestionProveedores','PROVEEDOR/GestionTercero', 'PROVEEDOR/INFOEXPERIENCIAENTIDAD' ) AND
			IdModulo = @idModulo)
END

