﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/8/2013 12:21:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Eliminar]
	@IdInfoFin INT
AS
BEGIN
	DELETE Proveedor.InfoFinancieraEntidad WHERE IdInfoFin = @IdInfoFin
END

