﻿

-- =============================================
-- Autor:	 Juan.Valverde, 
-- Fecha:	 12-AGO-2014
-- Descripción: Inserta el Motivo de cambio de estado y cambia el estado del tercero Unicamente.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstadoTercero_Insertar]
@IdTercero INT, @IdTemporal NVARCHAR (20), @Motivo NVARCHAR (128), @UsuarioCrea NVARCHAR (250)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION t1
		INSERT INTO Proveedor.MotivoCambioEstado (IdTercero, IdTemporal, Motivo, DatosBasicos, Financiera, Experiencia, FechaCrea, UsuarioCrea,Integrantes)
			VALUES (@IdTercero, @IdTemporal, @Motivo, 0, 0, 0, GETDATE(), @UsuarioCrea,0)


		DECLARE @PorValidar NVARCHAR (10)
		
		
		SET @PorValidar = (SELECT
			IdEstadoTercero
		FROM Oferente.EstadoTercero
		WHERE CodigoEstadotercero = '005')
		UPDATE Oferente.TERCERO
		SET IDESTADOTERCERO = @PorValidar
		WHERE IDTERCERO = @IdTercero	
COMMIT TRANSACTION t1
END TRY
BEGIN CATCH
ROLLBACK TRANSACTION t1
SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_MESSAGE() AS ErrorMessage;
END CATCH;
END





