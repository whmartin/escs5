﻿
-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que guarda un nuevo Archivo
-- =============================================
-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date: 05/04/2013
-- Description: Agrega el campo NombreArchivoOri para guardar el nombre del archivo que carga un usuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Insertar]
		    @IdArchivo NUMERIC(18,0) OUTPUT
		, 	@IdUsuario INT
		,	@IdFormatoArchivo INT
		,	@FechaRegistro DATETIME
		,	@NombreArchivo NVARCHAR(128)
		,	@ServidorFTP NVARCHAR(256)=NULL
		,	@Estado NVARCHAR(1)
		,	@ResumenCarga NVARCHAR(256)
		,   @UsuarioCrea NVARCHAR(250)
		,	@NombreArchivoOri NVARCHAR(256)
		,	@idTabla varchar(256) = NULL
		,	@NombreTabla varchar(256) = NULL
AS
BEGIN
	INSERT INTO Estructura.Archivo(IdUsuario
	                             , IdFormatoArchivo
	                             , FechaRegistro
	                             , NombreArchivo
	                             , ServidorFTP
	                             , Estado
	                             , ResumenCarga
	                             , UsuarioCrea
	                             , FechaCrea
	                             , NombreArchivoOri
	                             ,NombreTabla
	                             ,IdTabla)
					  VALUES(@IdUsuario
					       , @IdFormatoArchivo
					       , @FechaRegistro
					       , @NombreArchivo
					       , @ServidorFTP
					       , @Estado
					       , @ResumenCarga
					       , @UsuarioCrea
					       , GETDATE()
					       , @NombreArchivoOri
					       , @NombreTabla
					       , @idTabla)
	SELECT @IdArchivo = @@IDENTITY 		
END


