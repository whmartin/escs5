﻿-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:36:00 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) Modulo
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarModulo]
	@IdModulo INT
AS
BEGIN
	DELETE SEG.Modulo WHERE IdModulo = @IdModulo
END
