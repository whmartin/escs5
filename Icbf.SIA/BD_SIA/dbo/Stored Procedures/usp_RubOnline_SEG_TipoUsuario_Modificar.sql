﻿

-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que actualiza un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Modificar]
		@IdTipoUsuario INT,	@CodigoTipoUsuario NVARCHAR(128),	@NombreTipoUsuario NVARCHAR(256),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.TipoUsuario SET CodigoTipoUsuario = @CodigoTipoUsuario, NombreTipoUsuario = @NombreTipoUsuario, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoUsuario = @IdTipoUsuario
END

