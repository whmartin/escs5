﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/16/2015 11:22:03 AM
-- Description:	Procedimiento almacenado que consulta un(a) Areas
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_Areas_Consultar]
	@IdArea INT
AS
BEGIN
 SELECT IdArea, IdRegional, NombreArea, CodArea, EstadoArea, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[Areas] WHERE  IdArea = @IdArea
END
