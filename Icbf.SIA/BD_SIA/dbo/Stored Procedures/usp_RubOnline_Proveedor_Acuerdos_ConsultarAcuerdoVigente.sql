﻿

-- =============================================
-- Author:		Saul Rodriguez
-- Create date:  27/06/2013 
-- Description:	Procedimiento almacenado que consulta el acuerdo vigente
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_ConsultarAcuerdoVigente]
AS
BEGIN
	SELECT
		IdAcuerdo,
		Nombre,
		ContenidoHtml,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[Acuerdos]
	WHERE IdAcuerdo = (SELECT MAX(IdAcuerdo) FROM [Proveedor].[Acuerdos])
	
END





