﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/7/2013 6:01:36 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoCiiu
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCiiu_Modificar]
		@IdTipoCiiu INT,	@CodigoCiiu NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoCiiu SET CodigoCiiu = @CodigoCiiu, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoCiiu = @IdTipoCiiu
END

