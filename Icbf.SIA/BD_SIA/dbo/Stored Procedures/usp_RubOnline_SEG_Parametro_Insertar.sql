﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Insertar]
		@IdParametro INT OUTPUT, 	@NombreParametro NVARCHAR(128),	@ValorParametro NVARCHAR(256),	
		@ImagenParametro NVARCHAR(256),	@Estado BIT, @Funcionalidad INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.Parametro(NombreParametro, ValorParametro, ImagenParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea)
					  VALUES(@NombreParametro, @ValorParametro, @ImagenParametro, @Estado, @Funcionalidad, @UsuarioCrea, GETDATE())
	SELECT @IdParametro = @@IDENTITY 		
END

