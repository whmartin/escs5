﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que guarda un nuevo AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AccionPaso_Insertar]
		@IdAccionPaso INT OUTPUT, 	@IdPaso INT,	@Estado BIT,	@Descripcion NVARCHAR(160),	@IdAccion INT,	@Aceptar BIT,	@Rechazar BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.AccionPaso (IdPaso, Estado, Descripcion, IdAccion, Aceptar, Rechazar, UsuarioCrea, FechaCrea)
		VALUES (@IdPaso, @Estado, @Descripcion, @IdAccion, @Aceptar, @Rechazar, @UsuarioCrea, GETDATE())
	
	SELECT @IdAccionPaso = @@IDENTITY
END