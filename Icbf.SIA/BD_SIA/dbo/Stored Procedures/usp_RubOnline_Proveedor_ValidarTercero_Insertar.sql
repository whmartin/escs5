﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/03 9:25:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ValidarTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarTercero_Insertar]
		@IdValidarTercero INT OUTPUT, 	@IdTercero INT,	@Observaciones NVARCHAR(200),	@ConfirmaYAprueba BIT, @UsuarioCrea NVARCHAR(128)
AS
BEGIN
	INSERT INTO Proveedor.ValidarTercero(IdTercero, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea)
					  VALUES(@IdTercero, @Observaciones, @ConfirmaYAprueba, @UsuarioCrea, GETDATE())
	SELECT @IdValidarTercero = SCOPE_IDENTITY() 		
END




