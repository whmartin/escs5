﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 9:36:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) Tipoentidad
--Modificado por: Juan Carlos Valverde Sámano
--Fecha: 01/04/2014
--Descripción: en base a un requerimiento del control de cambios 16, se agregan los
-- paramétros: @IdTipoPersona y @IdSector para realizar un filtrado.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Tipoentidads_Consultar_All]	
@IdTipoPersona INT=NULL, @IdSector INT=NULL
AS
BEGIN
	IF ((@IdTipoPersona=1 AND @IdSector=1) OR (@IdTipoPersona=2 AND @IdSector=2))
	BEGIN
		SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
		FROM [Proveedor].[Tipoentidad]
		WHERE Estado=1 AND Descripcion!='EXTRANJERA'
		ORDER BY Descripcion ASC 
	END
	ELSE IF ((@IdTipoPersona=2 AND @IdSector=1)) 
	BEGIN
		SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
		FROM [Proveedor].[Tipoentidad]
		WHERE Estado=1
		ORDER BY Descripcion ASC
	END
	ELSE
		BEGIN
		SELECT IdTipoentidad, CodigoTipoentidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
		FROM [Proveedor].[Tipoentidad]
		WHERE Estado=1
		ORDER BY Descripcion ASC
	END
END




