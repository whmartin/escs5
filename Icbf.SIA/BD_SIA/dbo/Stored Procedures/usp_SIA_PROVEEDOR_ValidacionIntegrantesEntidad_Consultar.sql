﻿



-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 07/AGO/2014
-- Description:	Realiza una consulta al registro de validación de Integrantes para una Entidad.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_ValidacionIntegrantesEntidad_Consultar]
@IdEntidad INT
AS
BEGIN
	SELECT 
	IdValidacionIntegrantesEntidad
	,IdEntidad
	,IdEstadoValidacionIntegrantes
	,(SELECT Descripcion FROM PROVEEDOR.EstadoIntegrantes
	WHERE IdEstadoIntegrantes=IdEstadoValidacionIntegrantes) AS 'EstadoValidacion'
	,NroRevision
	,Finalizado
	,FechaCrea
	,UsuarioCrea
	,FechaModifica
	,UsuarioModifica
	,CorreoEnviado
	FROM 
	PROVEEDOR.ValidacionIntegrantesEntidad
	WHERE IdEntidad=@IdEntidad
	


END


