﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/15/2015 6:00:03 PM
-- Description:	Procedimiento almacenado que consulta el Numero de Plan de Pagos
-- [usp_Sigepcyp_SPCP_NumeroPlanDePagos_Consultar] 63,111115
-- =============================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_NumeroPlanDePagos_Consultar]
	@IdDatosAdministracion INT = NULL, @CodCompromiso INT = NULL
AS
BEGIN
  
DECLARE @Pagos VARCHAR(10)

SELECT	@Pagos = NumeroDePago
FROM	[SPCP].[PlanDePagos] pp
		INNER JOIN (
					SELECT	[IdDatosAdministracion], [CodCompromiso], max([OrdenDePago]) AS OrdenDePago
					FROM	[SPCP].[PlanDePagos]
					WHERE	[IdDatosAdministracion] = @IdDatosAdministracion
							AND [CodCompromiso] = @CodCompromiso
					GROUP	BY [IdDatosAdministracion], [CodCompromiso]
					) np
			ON pp.[IdDatosAdministracion] = np.[IdDatosAdministracion]
			AND pp.[OrdenDePago] = np.[OrdenDePago]
			AND pp.[CodCompromiso] = np.[CodCompromiso]
			
SELECT	DISTINCT [NumeroDePago], [NumeroDePago] + ' / ' + @Pagos Pagos, 
		CASE WHEN [NumeroDePago] IN (	SELECT	[NumeroDePago] 
								FROM	SPCP.CuentasdeCobro 
								WHERE	[IdDatosAdministracion] = @IdDatosAdministracion
										AND [CodCompromiso] = @CodCompromiso) THEN 'E' ELSE 'N' END AS Tipo
FROM	[SPCP].[PlanDePagos]
WHERE	Estado = 2 
		AND [IdDatosAdministracion] = @IdDatosAdministracion
		AND [CodCompromiso] = @CodCompromiso

END


