﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que consulta un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_sConsultar]
	@IdUsuario INT = NULL
   ,@IdFormatoArchivo INT = NULL
   ,@FechaRegistro DATETIME
AS
BEGIN
 SELECT IdArchivo
       ,IdUsuario
       ,IdFormatoArchivo
       ,FechaRegistro
       ,NombreArchivo
       ,ServidorFTP
       ,Estado
       ,ResumenCarga
       ,UsuarioCrea
       ,FechaCrea
       ,UsuarioModifica
       ,FechaModifica 
 FROM [Estructura].[Archivo] 
 WHERE IdUsuario = CASE WHEN @IdUsuario IS NULL THEN IdUsuario ELSE @IdUsuario END 
        AND IdFormatoArchivo = CASE WHEN @IdFormatoArchivo IS NULL THEN IdFormatoArchivo ELSE @IdFormatoArchivo END
        AND DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
END


