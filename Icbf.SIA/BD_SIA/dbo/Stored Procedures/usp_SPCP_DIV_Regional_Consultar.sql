﻿
-- =============================================
-- Author:		ICBF\EquipoFinanciera
-- Create date:  04/03/2015 8:16:01
-- Description:	Procedimiento almacenado que consulta un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_DIV_Regional_Consultar]
	@IdRegional INT
AS
BEGIN
 SELECT IdRegional, CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, codPCI 
 FROM [DIV].[Regional] WHERE  IdRegional = @IdRegional
END

