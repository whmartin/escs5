﻿
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que consulta un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Consultar]
	@TipoSupervisorInterventor INT
AS
BEGIN
 SELECT TipoSupervisorInterventor, NumeroContrato, NumeroIdentificacion, NombreRazonSocialSupervisorInterventor, NumeroIdentificacionDirectorInterventoria, NombreRazonSocialDirectorInterventoria, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ConsultarSupervisorInterventor] WHERE  TipoSupervisorInterventor = @TipoSupervisorInterventor
END


