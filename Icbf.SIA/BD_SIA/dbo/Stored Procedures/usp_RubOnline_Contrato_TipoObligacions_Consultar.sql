﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacions_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacions_Consultar]
@NombreTipoObligacion NVARCHAR (128) = NULL,
@Descripcion NVARCHAR (128) = NULL,
@Estado bit = null
AS
BEGIN

	SELECT
		IdTipoObligacion,
		NombreTipoObligacion,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TipoObligacion]
	WHERE NombreTipoObligacion LIKE '%' + 
		CASE
			WHEN @NombreTipoObligacion IS NULL THEN NombreTipoObligacion ELSE @NombreTipoObligacion
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreTipoObligacion

END



