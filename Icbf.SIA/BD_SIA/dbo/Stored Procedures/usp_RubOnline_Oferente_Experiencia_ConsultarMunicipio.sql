﻿--
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  /605/2013 4:49:27 PM
-- Description:	Procedimiento almacenado que consulta Departamentos
-- =============================================
	CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarMunicipio]		
		@IdDepartamento INT
	AS
	BEGIN
		SELECT     IdMunicipio, NombreMunicipio
		FROM DIV.Municipio WHERE IdDepartamento = @IdDepartamento
		ORDER BY NombreMunicipio DESC
	END

