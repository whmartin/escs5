﻿

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructuras_Consultar_All]
	
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[RamaoEstructura] 
 WHERE Estado = 1
 ORDER BY Descripcion ASC
  END




