﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 4:08:14 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeentidadPublica
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeentidadPublica_Eliminar]
	@IdTipodeentidadPublica INT
AS
BEGIN
	DELETE Proveedor.TipodeentidadPublica WHERE IdTipodeentidadPublica = @IdTipodeentidadPublica
END

