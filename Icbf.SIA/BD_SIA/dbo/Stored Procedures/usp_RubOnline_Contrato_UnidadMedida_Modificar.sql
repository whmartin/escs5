﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Modificar]
		@IdNumeroContrato INT,	
		@NumeroContrato NVARCHAR(50),
		@FechaInicioEjecuciónContrato DATETIME,	@FechaTerminacionInicialContrato DATETIME,	@FechaFinalTerminacionContrato DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.UnidadMedida 
	SET NumeroContrato = @NumeroContrato,
	FechaInicioEjecuciónContrato = @FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato = @FechaTerminacionInicialContrato, FechaFinalTerminacionContrato = @FechaFinalTerminacionContrato, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdNumeroContrato = @IdNumeroContrato
END

