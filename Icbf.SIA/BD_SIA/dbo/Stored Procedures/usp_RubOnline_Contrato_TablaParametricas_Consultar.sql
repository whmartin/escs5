﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 10:10:45 PM
-- Description:	Procedimiento almacenado que consulta un(a) TablaParametrica
-- =============================================

create PROCEDURE [dbo].[usp_RubOnline_Contrato_TablaParametricas_Consultar]
	@CodigoTablaParametrica NVARCHAR(128) = NULL,
	@NombreTablaParametrica NVARCHAR(128) = NULL,
	@Estado					BIT = NULL
AS
BEGIN
	SELECT
		IdTablaParametrica,
		CodigoTablaParametrica,
		NombreTablaParametrica,
		Url,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[TablaParametrica]
	WHERE CodigoTablaParametrica = CASE WHEN @CodigoTablaParametrica IS NULL 
										THEN CodigoTablaParametrica 
										ELSE @CodigoTablaParametrica
								   END
	AND NombreTablaParametrica LIKE '%' + CASE WHEN @NombreTablaParametrica IS NULL 
											   THEN NombreTablaParametrica 
											   ELSE @NombreTablaParametrica
										  END + '%'
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado
		END
	ORDER BY NombreTablaParametrica
END--FIN PP


