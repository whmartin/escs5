﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que guarda un nuevo Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Insertar]
		@IdCobertura INT OUTPUT, 	@IdCentroZonal INT,	@IdMunicipio INT,	@IdVigencia INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.Cobertura(IdCentroZonal, IdMunicipio, IdVigencia, UsuarioCrea, FechaCrea)
					  VALUES(@IdCentroZonal, @IdMunicipio, @IdVigencia, @UsuarioCrea, GETDATE())
	SELECT @IdCobertura = @@IDENTITY 		
END

