﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Consultar]
	@IdGestionClausula INT
AS
BEGIN
 SELECT IdGestionClausula, IdContrato, NombreClausula, TipoClausula, Orden, OrdenNumero, DescripcionClausula, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarClausulasContrato] WHERE  IdGestionClausula = @IdGestionClausula
END

