﻿


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/16/2013 9:24:30 AM
-- Description:	Procedimiento almacenado que guarda un nuevo EntidadProvOferente
--Modificado por: Juan Carlos Valverde Sámano
--Fecha Modificación: 20/03/2014
--Descripción: En base a un requerimiento de Mejors del CO016 se agrega lo siguiente:
--el procedimiento recibe los parametros primer nombre, segundo nombre, primer apeliido, segundo apellido y 
-- correo electrónico para actualizarlos en TERCEROS.(Por si caso hubo alguna modificación al momento de editar
-- los datos basicos del proveedor.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_Insertar]
		@IdEntidad INT OUTPUT, 	
		@ConsecutivoInterno NVARCHAR(128),
		@TipoEntOfProv BIT=NULL,	
		@IdTercero INT=NULL,	
		@IdTipoCiiuPrincipal INT=NULL,	
		@IdTipoCiiuSecundario INT=NULL,	
		@IdTipoSector INT=NULL,	
		@IdTipoClaseEntidad INT=NULL,	
		@IdTipoRamaPublica INT=NULL,	
		@IdTipoNivelGob INT=NULL,	
		@IdTipoNivelOrganizacional INT=NULL,	
		@IdTipoCertificadorCalidad INT=NULL,	
		@FechaCiiuPrincipal DATETIME=NULL,	
		@FechaCiiuSecundario DATETIME=NULL,	
		@FechaConstitucion DATETIME=NULL,	
		@FechaVigencia DATETIME=NULL,	
		@FechaMatriculaMerc DATETIME=NULL,	
		@FechaExpiracion DATETIME=NULL,	
		@TipoVigencia BIT=NULL,	
		@ExenMatriculaMer BIT=NULL,	
		@MatriculaMercantil NVARCHAR(20)=NULL,	
		@ObserValidador NVARCHAR(256)=NULL,	
		@AnoRegistro INT=NULL,	
		@IdEstado INT=NULL, 
		@UsuarioCrea NVARCHAR(250),
		@IdTemporal VARCHAR(20) = NULL,
		@PrimerNombre NVARCHAR(256) ='',
		@SegundoNombre NVARCHAR(256) ='',
		@PrimerApellido NVARCHAR(256)='',
		@SegundoApellido NVARCHAR(256)='',
		@CorreoElectronico NVARCHAR(256) = '',
		@NumIntegrantes INT =NULL
AS
BEGIN
	INSERT INTO Proveedor.EntidadProvOferente(TipoEntOfProv, IdTercero, IdTipoCiiuPrincipal, IdTipoCiiuSecundario, IdTipoSector, IdTipoClaseEntidad, IdTipoRamaPublica, IdTipoNivelGob, IdTipoNivelOrganizacional, IdTipoCertificadorCalidad, FechaCiiuPrincipal, FechaCiiuSecundario, FechaConstitucion, FechaVigencia, FechaMatriculaMerc, FechaExpiracion, TipoVigencia, ExenMatriculaMer, MatriculaMercantil, ObserValidador, AnoRegistro, IdEstado, UsuarioCrea, FechaCrea, NroRevision, NumIntegrantes)
					  VALUES(@TipoEntOfProv, @IdTercero, @IdTipoCiiuPrincipal, @IdTipoCiiuSecundario, @IdTipoSector, @IdTipoClaseEntidad, @IdTipoRamaPublica, @IdTipoNivelGob, @IdTipoNivelOrganizacional, @IdTipoCertificadorCalidad, @FechaCiiuPrincipal, @FechaCiiuSecundario, @FechaConstitucion, @FechaVigencia, @FechaMatriculaMerc, @FechaExpiracion, @TipoVigencia, @ExenMatriculaMer, @MatriculaMercantil, @ObserValidador, @AnoRegistro, @IdEstado, @UsuarioCrea, GETDATE(), 1, @NumIntegrantes)
	SELECT @IdEntidad = SCOPE_IDENTITY() 		
	
	UPDATE [Proveedor].[DocDatosBasicoProv] set IdEntidad = @IdEntidad where IdTemporal = @IdTemporal
	
	UPDATE Proveedor.EntidadProvOferente SET ConsecutivoInterno=@ConsecutivoInterno +  RIGHT('00000' + Ltrim(Rtrim(@IdEntidad)),8) WHERE IdEntidad=@IdEntidad
	
	
	UPDATE Oferente.TERCERO
   SET
   PRIMERNOMBRE=@PrimerNombre,
   SEGUNDONOMBRE=@SegundoNombre,
   PRIMERAPELLIDO=@PrimerApellido,
   SEGUNDOAPELLIDO=@SegundoApellido,
   CORREOELECTRONICO=@CorreoElectronico,
   USUARIOMODIFICA=@UsuarioCrea,
   FECHAMODIFICA=GETDATE()
   WHERE IDTERCERO=@IdTercero
   
   
-----Actualizar el Documento que viene con IDTemporal, colocarle el ID del tercero-------
-----Y actualizar la columna activo para el o los documentos anteriores--------------------
DECLARE @TBL_DOCS_ORIGINAL TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

DECLARE @TBL_DOCS_TEMPORAL TABLE
(IDROW INT IDENTITY, IDDOCADJUNTO INT,IDTERCERO INT,IDDOCUMENTO INT,IdTemporal NVARCHAR(20),ACTIVO BIT)

INSERT INTO @TBL_DOCS_ORIGINAL
	SELECT IDDOCADJUNTO,IDTERCERO,IDDOCUMENTO,IdTemporal,ACTIVO FROM [Proveedor].[DocAdjuntoTercero] 
	WHERE IDTERCERO=@IdTercero AND
	IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal

INSERT INTO @TBL_DOCS_TEMPORAL
	SELECT IDDOCADJUNTO,IDTERCERO,IDDOCUMENTO,IdTemporal,ACTIVO FROM [Proveedor].[DocAdjuntoTercero] 
	WHERE IDTERCERO IS NULL
	AND IDDOCUMENTO IN (SELECT IDDOCUMENTO FROM [Proveedor].[DocAdjuntoTercero] WHERE IdTemporal=@IdTemporal)
	AND IdTemporal=@IdTemporal AND Activo=1
	
DECLARE @nRegistros Int --Almacena la cantidad de registro que retorna la consulta.
SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_ORIGINAL)
DECLARE @nWhile Int --Almacenará la cantidad de veces que se esta recorriendo en el Bucle.
DECLARE @IDDOCTEMP INT
DECLARE @IDDOCORIGINAL INT
SET @nWhile=1

--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdDoc INT
SELECT @IDDOCORIGINAL=IDDOCADJUNTO, 
@IdDoc=IDDOCUMENTO FROM @TBL_DOCS_ORIGINAL WHERE IDROW=@nWhile
IF EXISTS(SELECT IDROW FROM @TBL_DOCS_TEMPORAL WHERE IDDOCUMENTO=@IdDoc)
BEGIN
SET @IDDOCTEMP=(SELECT IDDOCADJUNTO FROM @TBL_DOCS_TEMPORAL WHERE IDDOCUMENTO=@IdDoc)
UPDATE [Proveedor].[DocAdjuntoTercero] 
SET Activo=0 WHERE IDDOCADJUNTO=@IDDOCORIGINAL

UPDATE [Proveedor].[DocAdjuntoTercero] 
SET IDTERCERO=@IdTercero WHERE IDDOCADJUNTO=@IDDOCTEMP
END
SET @nWhile=@nWhile+1
END

SET @nRegistros=(SELECT COUNT(*) FROM @TBL_DOCS_TEMPORAL)
SET @nWhile=1
--Recorremos la tabla mediante un bucle While.
WHILE(@nRegistros>0 AND @nWhile<=@nRegistros)
BEGIN

DECLARE @IdTipoDoc INT
SELECT @IDDOCTEMP=IDDOCADJUNTO, 
@IdTipoDoc=IDDOCUMENTO FROM @TBL_DOCS_TEMPORAL WHERE IDROW=@nWhile
IF NOT EXISTS(SELECT * FROM @TBL_DOCS_ORIGINAL WHERE IDDOCUMENTO=@IdTipoDoc)
BEGIN
UPDATE [Proveedor].[DocAdjuntoTercero] 
SET IDTERCERO=@IdTercero WHERE IDDOCADJUNTO=@IDDOCTEMP
END
SET @nWhile=@nWhile+1
END
------------------------------------------------------------------------------------------
   
END








