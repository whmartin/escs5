﻿
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Insertar]
		@IdTipoSectorEntidad INT OUTPUT, 	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TipoSectorEntidad(CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoSectorEntidad, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSectorEntidad = SCOPE_IDENTITY() 		
END


