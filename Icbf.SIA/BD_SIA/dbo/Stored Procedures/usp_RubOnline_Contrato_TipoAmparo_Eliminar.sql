﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Eliminar]
	@IdTipoAmparo INT
AS
BEGIN
	DELETE Contrato.TipoAmparo WHERE IdTipoAmparo = @IdTipoAmparo
END

