﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que consulta un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Consultar]
	@IdCobertura INT
AS
BEGIN
 SELECT IdCobertura, IdCentroZonal, IdMunicipio, IdVigencia, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Cobertura] WHERE  IdCobertura = @IdCobertura
END

