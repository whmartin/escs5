﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que guarda un nuevo InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Insertar]
		@IdInformacionPresupuestal INT OUTPUT, 	@NumeroCDP INT,	@ValorCDP NUMERIC(18,3),	@FechaExpedicionCDP DATETIME, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.InformacionPresupuestal(NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroCDP, @ValorCDP, @FechaExpedicionCDP, @UsuarioCrea, GETDATE())
	SELECT @IdInformacionPresupuestal = @@IDENTITY 		
END

