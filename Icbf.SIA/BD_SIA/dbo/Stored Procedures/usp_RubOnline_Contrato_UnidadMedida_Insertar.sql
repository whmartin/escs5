﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Insertar]
		@IdNumeroContrato INT OUTPUT, 
		@NumeroContrato	NVARCHAR(50),
		@FechaInicioEjecuciónContrato DATETIME,	
		@FechaTerminacionInicialContrato DATETIME,	
		@FechaFinalTerminacionContrato DATETIME, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.UnidadMedida(NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea)
					  VALUES(@NumeroContrato, @FechaInicioEjecuciónContrato, @FechaTerminacionInicialContrato, @FechaFinalTerminacionContrato, @UsuarioCrea, GETDATE())
	SELECT @IdNumeroContrato = @@IDENTITY 		
END

