﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que consulta un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Consultar]
	@IdRegional INT
AS
BEGIN
 SELECT IdRegional, CodigoRegional, NombreRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [DIV].[Regional] WHERE  IdRegional = @IdRegional
END

