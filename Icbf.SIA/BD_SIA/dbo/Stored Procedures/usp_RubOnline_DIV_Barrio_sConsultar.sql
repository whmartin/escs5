﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:34:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) Barrio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Barrio_sConsultar]
    @IdComuna NVARCHAR(256) = NULL
   ,@CodigoBarrio NVARCHAR(256) = NULL
   ,@NombreBarrio NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdBarrio
      , IdComuna
      , CodigoBarrio
      , NombreBarrio
      , Estado
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
  FROM [DIV].[Barrio] 
  WHERE IdComuna = CASE WHEN @IdComuna IS NULL THEN IdComuna ELSE @IdComuna END 
        AND CodigoBarrio = CASE WHEN @CodigoBarrio IS NULL THEN CodigoBarrio ELSE @CodigoBarrio END 
        AND NombreBarrio = CASE WHEN @NombreBarrio IS NULL THEN NombreBarrio ELSE @NombreBarrio END
  ORDER BY NombreBarrio DESC      
END

