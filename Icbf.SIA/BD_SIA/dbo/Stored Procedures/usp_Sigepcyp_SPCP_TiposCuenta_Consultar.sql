﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:05:12 PM
-- Description:	Procedimiento almacenado que consulta un(a) TiposCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_TiposCuenta_Consultar]
	@IdTipoCuenta INT
AS
BEGIN
 SELECT IdTipoCuenta, TipoCuenta, Estado, IdRegional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [SPCP].[TiposCuenta] WHERE  IdTipoCuenta = @IdTipoCuenta
END
