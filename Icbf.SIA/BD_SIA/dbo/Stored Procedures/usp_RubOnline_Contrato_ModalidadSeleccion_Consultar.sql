﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Consultar]
	@IdModalidad INT
AS
BEGIN
 SELECT IdModalidad, Nombre, Sigla, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[ModalidadSeleccion] WHERE  IdModalidad = @IdModalidad
END

