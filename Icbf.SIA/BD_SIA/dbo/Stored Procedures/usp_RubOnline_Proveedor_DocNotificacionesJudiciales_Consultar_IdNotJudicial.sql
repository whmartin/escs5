﻿
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 02/07/2013
-- Description:	Obtiene los documentos de l proveedor notificaiones judiciales
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionesJudiciales_Consultar_IdNotJudicial]
	@IdNotJudicial INT
AS
BEGIN
--	declare @IdNotJudicial int
--set @IdNotJudicial=13

declare @IdPrograma int;
select @IdPrograma = IdPrograma FROM SEG.PROGRAMA WHERE CodigoPrograma = 'Proveedor/NotificacionJudicial';
PRINT @IdPrograma
SELECT	MAX(IdDocNotJudicial) as IdDocNotJudicial,
		MAX(IdNotJudicial) as IdNotJudicial,
		NombreDocumento,
		Max(LinkDocumento) as LinkDocumento,
		Max(Descripcion) as Descripcion,
		Max(UsuarioCrea) as UsuarioCrea,
		Max(FechaCrea) as FechaCrea,
		Max(UsuarioModifica) as UsuarioModifica,
		Max(FechaModifica) as FechaModifica,
		Max(IdTipoDocumento) as IdDocumento,
		Max(MaxPermitidoKB) as MaxPermitidoKB,
		Max(ExtensionesPermitidas) as ExtensionesPermitidas
		
FROM 
(
SELECT     Proveedor.DocNotificacionJudicial.IdDocNotJudicial ,
			Proveedor.DocNotificacionJudicial.IdNotJudicial,t.Descripcion AS NombreDocumento,
			Proveedor.DocNotificacionJudicial.LinkDocumento, 
			Proveedor.DocNotificacionJudicial.Descripcion ,
			t.IdTipoDocumento  ,t.UsuarioCrea , t.FechaCrea, t.UsuarioModifica ,t.FechaModifica ,
			 
			tdp.MaxPermitidoKB, tdp.ExtensionesPermitidas                   
FROM         SEG.Programa AS p INNER JOIN
                      Proveedor.TipoDocumentoPrograma AS tdp ON p.IdPrograma = tdp.IdPrograma INNER JOIN
                      Proveedor.TipoDocumento AS t ON tdp.IdTipoDocumento = t.IdTipoDocumento INNER JOIN
                      Proveedor.DocNotificacionJudicial ON tdp.IdTipoDocumento = Proveedor.DocNotificacionJudicial.IdDocumento
WHERE     (tdp.IdPrograma = @IdPrograma) AND (Proveedor.DocNotificacionJudicial.IdNotJudicial = @IdNotJudicial)-- MODULO DE GESTION PROVEEDOR
		
UNION                      
SELECT       0 as IdDocNotJudicial,
			 0 as IdNotJudicial,
			 Descripcion   as NombreDocumento,
			 '' as LinkDocumento,
			 '' as Descripcion,
			 tdp.IdTipoDocumento,
			 '' as UsuarioCrea,
			 t.FechaCrea,
			 '' as UsuarioModifica,
			 t.FechaModifica,
			 tdp.MaxPermitidoKB,
			 tdp.ExtensionesPermitidas
FROM         Proveedor.TipoDocumentoPrograma tdp INNER JOIN
                      Proveedor.TipoDocumento t
                      ON tdp.IdTipoDocumento = t.IdTipoDocumento 
WHERE     (tdp.IdPrograma = @IdPrograma) -- MODULO DE GESTION PROVEEDOR
) as Tabla
GROUP BY IdTipoDocumento,NombreDocumento
ORDER BY IdTipoDocumento
END

