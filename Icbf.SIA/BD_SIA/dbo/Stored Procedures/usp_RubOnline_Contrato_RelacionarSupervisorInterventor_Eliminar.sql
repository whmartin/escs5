﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Eliminar]
	@IDSupervisorInterv INT
AS
BEGIN
	DELETE Contrato.SupervisorIntervContrato 
	WHERE IDSupervisorInterv = @IDSupervisorInterv
END


