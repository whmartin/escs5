﻿


-- =============================================
-- Author:  Grupo de Sistemas de Apoyo de la SSII
-- Create date:  15/10/2015
-- Description:	Creación deL PROCEDIMIENTO usp_SPCP_Seg_ConsultarUsuarioPorDocumento
-- Aplicativo SIGEPCYP
-- =============================================

CREATE PROCEDURE [dbo].[usp_SPCP_Seg_ConsultarUsuarioPorDocumento]
	@pIdTipoDocumento INT
	,@pNumeroDocumento NVARCHAR(17)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoUsuario
	  ,SEG.Usuario.CodPCI
  FROM SEG.Usuario 
  WHERE
	SEG.Usuario.IdTipoDocumento = @pIdTipoDocumento
    and SEG.Usuario.NumeroDocumento  LIKE CASE WHEN @pNumeroDocumento IS NULL THEN SEG.Usuario.NumeroDocumento ELSE '%'+@pNumeroDocumento+'%' END
END



