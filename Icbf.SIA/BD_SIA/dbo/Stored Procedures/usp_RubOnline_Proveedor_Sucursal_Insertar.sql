﻿
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  03/04/2014 1:56:36 PM
-- Description:	Procedimiento almacenado que Inserta sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Insertar]
		@IdSucursal INT OUTPUT, 	
		@IdEntidad INT, 
		@Nombre NVARCHAR(256), 
		@Indicativo INT = NULL,	
		@Telefono INT = NULL,	
		@Extension NUMERIC(10) = NULL,	
		@Celular NUMERIC(10),	
		@Correo NVARCHAR(256),	
		@Estado INT, @IdZona INT,	
		@Departamento INT,	
		@Municipio INT,	
		@Direccion NVARCHAR(256), 
		@UsuarioCrea NVARCHAR(250),
		@Editable INT
AS
BEGIN
	INSERT INTO Proveedor.Sucursal(IdEntidad,Nombre, Indicativo, Telefono, Extension, Celular, Correo, Estado, IdZona, Departamento, Municipio, Direccion, UsuarioCrea, FechaCrea, Editable)
					  VALUES(@IdEntidad, @Nombre, @Indicativo, @Telefono, @Extension, @Celular, @Correo, @Estado, @IdZona, @Departamento, @Municipio, @Direccion, @UsuarioCrea, GETDATE(),@Editable)
	SELECT @IdSucursal = SCOPE_IDENTITY() 		
END



