﻿
-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que consulta un(a) Tercero por tipo y numero de documento
-- =============================================


CREATE PROCEDURE [dbo].[usp_Terceros_Tercero_Consultar_TipoNumeroIdentificacion]
@IdTipoIdentificacion INT, @NumeroIdentificacion nvarchar(128)
AS
BEGIN
SELECT [IDTERCERO]
      ,[IDTIPODOCIDENTIFICA]
      ,[IDESTADOTERCERO]
      ,[IdTipoPersona]
      ,[NUMEROIDENTIFICACION]
      ,[DIGITOVERIFICACION]
      ,[CORREOELECTRONICO]
      ,[PRIMERNOMBRE]
      ,[SEGUNDONOMBRE]
      ,[PRIMERAPELLIDO]
      ,[SEGUNDOAPELLIDO]
      ,[RAZONSOCIAL]
      ,[FECHAEXPEDICIONID]
      ,[FECHANACIMIENTO]
      ,[SEXO]
      ,[FECHACREA]
      ,[USUARIOCREA]
      ,[FECHAMODIFICA]
      ,[USUARIOMODIFICA]
      ,[ProviderUserKey]
FROM Tercero.TERCERO WITH(NOLOCK)
WHERE IDTIPODOCIDENTIFICA = @IdTipoIdentificacion AND NUMEROIDENTIFICACION=@NumeroIdentificacion
     
END



