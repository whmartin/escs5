﻿
-- =============================================
-- Author:		JHON DIAZ
-- Create date:  5/23/2013 5:33:49 PM
-- Description:	Procedimiento almacenado que consulta un(a) Tercero
-- Modificación: Juan Carlos Valverde Sámano
-- Fecha: 20140410
-- Descripción: Se agrego el campo CreadoporInterno a la Consulta
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Consultar]
@IdTercero INT
AS
BEGIN
--SELECT [IDTERCERO]
--      ,[IDTIPODOCIDENTIFICA]
--      ,[IDESTADOTERCERO]
--      ,[IdTipoPersona]
--      ,[NUMEROIDENTIFICACION]
--      ,[DIGITOVERIFICACION]
--      ,[CORREOELECTRONICO]
--      ,[PRIMERNOMBRE]
--      ,[SEGUNDONOMBRE]
--      ,[PRIMERAPELLIDO]
--      ,[SEGUNDOAPELLIDO]
--      ,[RAZONSOCIAL]
--      ,[FECHAEXPEDICIONID]
--      ,[FECHANACIMIENTO]
--      ,[SEXO]
--      ,[FECHACREA]
--      ,[USUARIOCREA]
--      ,[FECHAMODIFICA]
--      ,[USUARIOMODIFICA]
--	  ,[ProviderUserKey]
--	  ,[CreadoPorInterno]
--FROM [Oferente].[Tercero]
--WHERE IdTercero = @IdTercero
  
  SELECT T.[IDTERCERO]
      ,T.[IDTIPODOCIDENTIFICA]
      ,T.[IDESTADOTERCERO]
      ,T.[IdTipoPersona]
      ,T.[ConsecutivoInterno]
      ,T.[NUMEROIDENTIFICACION]
      ,T.[DIGITOVERIFICACION]
      ,T.[CORREOELECTRONICO]
      ,T.[PRIMERNOMBRE]
      ,T.[SEGUNDONOMBRE]
      ,T.[PRIMERAPELLIDO]
      ,T.[SEGUNDOAPELLIDO]
      ,T.[RAZONSOCIAL]
      ,T.[FECHAEXPEDICIONID]
      ,T.[FECHANACIMIENTO]
      ,T.[SEXO]
      ,T.[FechaCrea]
      ,T.[USUARIOCREA]
      ,T.[FECHAMODIFICA]
      ,T.[USUARIOMODIFICA]
	  ,T.[ProviderUserKey]
	  ,T.[CreadoPorInterno]
FROM  Tercero.TERCERO T 

WHERE T.IdTercero = @IdTercero
     
END




