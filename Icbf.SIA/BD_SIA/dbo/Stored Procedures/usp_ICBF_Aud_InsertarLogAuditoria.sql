﻿
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 14/01/2013
-- Description:	Procedimiento para Guardar la Auditoria del Sistema
-- =============================================
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date: 23/01/2013
-- Description:	Ajuste longitud del parametro @pOperacion de 20 a 200
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Aud_InsertarLogAuditoria]
	@pUsuario NVARCHAR(250),
	@pPrograma NVARCHAR(200),
	@pOperacion NVARCHAR(20),
	@pParametrosOperacion TEXT,
	@pTabla NVARCHAR(50),
	@pIdRegistro NUMERIC(18,0),
	@pDireccionIp NVARCHAR(20),
	@pNavegador NVARCHAR(250)
AS
BEGIN

INSERT INTO [AUDITA].[LogSIA](
	[fecha],
	[usuario],
	[programa],
	[operacion],
	[parametrosOperacion],
	[tabla],
	[idRegistro],
	[direccionIp],
	[navegador])
VALUES (
	GETDATE(),
	@pUsuario,
	@pPrograma,
	@pOperacion,
	@pParametrosOperacion,
	@pTabla,
	@pIdRegistro,
	@pDireccionIp,
	@pNavegador)

END

