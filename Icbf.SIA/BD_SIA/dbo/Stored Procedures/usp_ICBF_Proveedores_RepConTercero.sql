﻿
---   exec [dbo].[usp_ICBF_Proveedores_RepConTercero]
-- =============================================
-- Author Create:		Zulma Barrantes --
-- Author Modify:       Alexander Gutierrez 
-- Modify date:  23/12/2015 2:21:41 PM
-- Description:	Procedimiento almacenado que guarda el número telefono por tercero y genera el reporte de terceros
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_Proveedores_RepConTercero]
	 @DescripcionEstado varchar(20), 
	 @IdTipoPersona varchar(20) ,
	 @CreadoPorInterno varchar(20) 
AS

begin 

	--------------------------------------------------------------------------------------------------------------------------
	print @DescripcionEstado

		DECLARE @TmpTelefonoTercero TABLE (
				[IDTERCERO] [INT]   NOT NULL,
				[NUMEROIDENTIFICACION]  [NVARCHAR] (50) NOT NULL,
				[TelefonoUsuario]   [NVARCHAR] (50) NULL,
				[TelefonoTercero]   [NVARCHAR] (50) NULL,
				[Telefonofijo] [NVARCHAR] (55) NULL
				)

	 --------------------------------------------------------------------------------------------------------------------------
				 --- revisa los telefonos de los terceros

	    INSERT INTO @TmpTelefonoTercero

				select distinct tt.IDTERCERO,tt.NUMEROIDENTIFICACION,
								convert (nvarchar,(max(us.TelefonoContacto))) TelefonoUsuario,
								convert (nvarchar,cast(Max(tel.Movil)as BIGINT)) TelefonoTercero,
								convert (varchar,cast(max(tel.NumeroTelefono)as int)) TelefonoFijo
				from Tercero.TERCERO tt
				left JOIN SEG.Usuario us on tt.NUMEROIDENTIFICACION=us.NumeroDocumento
				LEFT JOIN Tercero.TelTerceros tel on tt.IDTERCERO = tel.IdTercero
				LEFT JOIN PROVEEDOR.EntidadProvOferente epo on tt.IDTERCERO = epo.IdTercero
				LEFT JOIN PROVEEDOR.Sucursal suc on suc.IdEntidad = epo.IdEntidad
				group by  tt.IDTERCERO,tt.NUMEROIDENTIFICACION
				order by tt.IdTercero

				delete from @TmpTelefonoTercero
				where TelefonoUsuario is null
				  and TelefonoTercero is null
				  and Telefonofijo	  is null 

				UPDATE @TmpTelefonoTercero 
				set TelefonoUsuario = telefonoTercero
				where TelefonoUsuario is null or TelefonoUsuario='0' or TelefonoUsuario = ''
				
				
				UPDATE @TmpTelefonoTercero 
				set TelefonoUsuario = TelefonoFijo
				where TelefonoUsuario is null or TelefonoUsuario='0' or TelefonoUsuario = ''

------------------------------------------------------------------------------------------------------
--- REPORTE TERCEROS
------------------------------------------------------------------------------------------------------

		Select	Consecutivo_Interno = TT.ConsecutivoInterno
			,Tipo_Persona  = CASE WHEN convert (nvarchar,TT.[IdTipoPersona]) is null
								  THEN '' 
			   					 ELSE (select TP.[NombreTipoPersona] from [Tercero].[TipoPersona] TP where TT.[IdTipoPersona] = TP.[IdTipoPersona])
							 END 
			,Tipo_Identificacion = GTD.[CodDocumento]
			,Identificacion = TT.[NUMEROIDENTIFICACION]
			,DV = CASE WHEN convert (nvarchar,TT.[DIGITOVERIFICACION]) is null THEN '' 
							  ELSE TT.[DIGITOVERIFICACION]
							  END 
			,TERCERO = CASE WHEN convert (nvarchar,TT.[IdTipoPersona]) is null THEN '' 
						 WHEN TT.[IdTipoPersona] = 1 THEN TT.[PRIMERNOMBRE]+' '+TT.[SEGUNDONOMBRE]+' '+TT.[PRIMERAPELLIDO]+' '+TT.[SEGUNDOAPELLIDO]
						 ELSE TT.[RAZONSOCIAL]
					   END 
			,CORREO_ELECTRONICO = TT.[CORREOELECTRONICO]
			,Telefono = CASE WHEN (Select count(yy.IDTERCERO) from @TmpTelefonoTercero yy where  yy.IDTERCERO = TT.IDTERCERO) = 0
							 then ''
							 else (Select yy.TelefonoUsuario from @TmpTelefonoTercero yy where  yy.IDTERCERO = TT.IDTERCERO)
							 END
			,Estado_Tercero = CASE WHEN convert (nvarchar,TT.[IdEstadoTercero]) is null
								   THEN '' 
			   					   ELSE (select ET.[DescripcionEstado] from [Tercero].[EstadoTercero] ET where ET.[IdEstadoTercero] = TT.[IDESTADOTERCERO])
							  END 
			,Fecha_Estado = CASE when convert (nvarchar, TT.[IDESTADOTERCERO]) IS NULL then ''
								WHEN TT.[IDESTADOTERCERO] = 4 THEN CONVERT(VARCHAR(10), TT.[FECHACREA], 103) 
								WHEN TT.[IDESTADOTERCERO] = 1 THEN CONVERT(VARCHAR(10), max(TT.[FECHAMODIFICA]), 103) 
								ELSE CONVERT(VARCHAR(10),(SELECT max(PVT.[FechaCrea]) FROM [PROVEEDOR].[ValidarTercero] PVT WHERE PVT.[IdTercero] = TT.IDTERCERO), 103) 
							END   
			,Observacion = (
							SELECT Observaciones
							 FROM [PROVEEDOR].[ValidarTercero] PVT WHERE 
							  PVT.[FechaCrea] = (SELECT max(PVT.[FechaCrea]) FROM [PROVEEDOR].[ValidarTercero] PVT WHERE PVT.[IdTercero] IN (SELECT IDTERCERO FROM [Tercero].[TERCERO] where NUMEROIDENTIFICACION = TT.[NUMEROIDENTIFICACION]))
			)
			,Validado_Por = (
								SELECT UsuarioCrea
								 FROM [PROVEEDOR].[ValidarTercero] PVT WHERE 
								  PVT.[FechaCrea] = (SELECT max(PVT.[FechaCrea]) FROM [PROVEEDOR].[ValidarTercero] PVT WHERE PVT.[IdTercero] IN (SELECT IDTERCERO FROM [Tercero].[TERCERO] where NUMEROIDENTIFICACION = TT.[NUMEROIDENTIFICACION]))

			
			)
			,TipoUsuario = CASE convert (nvarchar,TT.[CreadoPorInterno])
							    WHEN 1 THEN 'INTERNO'
								WHEN 0 THEN 'EXTERNO'
								ELSE TT.[ConsecutivoInterno]
						   END 
			,Registrado_Por = TT.USUARIOCREA
			,Fecha_Registro = CONVERT(VARCHAR(10), TT.[FECHACREA], 103)
			,Registrado_Proveedor = Case When (SELECT COUNT(*) FROM [PROVEEDOR].[EntidadProvOferente] EPO WHERE EPO.IdTercero = TT.IDTERCERO) > 0
										 then (SELECT EPO.[ConsecutivoInterno] FROM [PROVEEDOR].[EntidadProvOferente] EPO WHERE EPO.IdTercero = TT.IDTERCERO)
										 else 'NO'
										 End
	        FROM	[Tercero].[TERCERO] TT
			INNER JOIN [Global].[TiposDocumentos] GTD WITH(NOLOCK) ON GTD.[IdTipoDocumento] = TT.[IDTIPODOCIDENTIFICA]
			LEFT JOIN [Tercero].[TelTerceros] TELT WITH(NOLOCK) ON TT.IDTERCERO = TELT.IdTercero
			LEFT JOIN [Tercero].[EstadoTercero] ET WITH(NOLOCK) ON TT.IdEstadoTercero = ET.IdEstadoTercero

			inner join dbo.[StringSplit] (@DescripcionEstado,1,0) spt on TT.[IdEstadoTercero] = convert(int, spt.val)
			inner join dbo.[StringSplit] (@IdTipoPersona,1,0) spt2 on TT.[IdTipoPersona] = convert(int, spt2.val)
			inner join dbo.[StringSplit] (@CreadoPorInterno,1,0) spt3 on TT.[CreadoPorInterno] = convert(BIT, spt3.val)
	 WHERE  tt.IdTipoPersona is not NULL

	 GROUP BY TT.ConsecutivoInterno
				,TT.IDTERCERO
				,TT.[NUMEROIDENTIFICACION]
				,TT.[DIGITOVERIFICACION]
				,TT.[CORREOELECTRONICO]
				,TT.[FECHACREA]
				,TT.[FECHAMODIFICA]
				,TT.[CreadoPorInterno]
				,TT.[PRIMERNOMBRE],TT.[SEGUNDONOMBRE],TT.[PRIMERAPELLIDO],TT.[SEGUNDOAPELLIDO]
				,TT.[RAZONSOCIAL]
				,TT.USUARIOCREA
				,TT.[IdTipoPersona]
				,TT.[IDESTADOTERCERO]
				,GTD.[CodDocumento]
				,TELT.[Movil]
				,ET.[DescripcionEstado]
	ORDER BY TT.[IdTipoPersona],TT.[NUMEROIDENTIFICACION]

end