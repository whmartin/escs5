﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 4:44:03 PM
-- Description:	Procedimiento almacenado que consulta un(a) Departamento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Departamento_sConsultar]
	@IdPais INT = NULL,@CodigoDepartamento NVARCHAR(128) = NULL,@NombreDepartamento NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdDepartamento, 
		IdPais, 
		CodigoDepartamento, 
		NombreDepartamento, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Departamento] 
	WHERE IdPais = CASE WHEN @IdPais IS NULL THEN IdPais ELSE @IdPais END 
	      AND CodigoDepartamento = CASE WHEN @CodigoDepartamento IS NULL THEN CodigoDepartamento ELSE @CodigoDepartamento END 
	      AND NombreDepartamento = CASE WHEN @NombreDepartamento IS NULL THEN NombreDepartamento ELSE @NombreDepartamento END
	ORDER BY NombreDepartamento desc
END

