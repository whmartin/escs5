﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta formatos de archivo

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_FormatoArchivo_Consultar]
	@TablaTemporar varchar(256)
   ,@Ext  varchar(10) = NULL
AS
BEGIN
	SELECT IdFormatoArchivo
		,IdTipoEstructura
		,IdModalidad
		,TablaTemporal
		,Separador
		,Extension
		,Estado
		,FechaCrea
		,UsuarioCrea
		,UsuarioModifica
		,FechaModifica
	FROM Estructura.FormatoArchivo
	WHERE TablaTemporal = @TablaTemporar
    AND Extension = CASE WHEN @Ext IS NULL THEN Extension ELSE @Ext END
END

