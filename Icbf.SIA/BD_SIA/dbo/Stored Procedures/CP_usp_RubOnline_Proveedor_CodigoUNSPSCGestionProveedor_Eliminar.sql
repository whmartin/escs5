﻿-- =============================================
-- Author:		Luz Angela Borda
-- Create date:	14/05/2015 12:09 pm
-- Description:	Procedimiento almacenado que elimina un(a) Codigo UNSPSC de proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_CodigoUNSPSCGestionProveedor_Eliminar]
	@CodUNSPSC INT
AS
BEGIN
	DELETE Proveedor.CodUNSPSC WHERE CodUNSPSC = @CodUNSPSC
END

