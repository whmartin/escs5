﻿

-- =============================================
-- Author:		Faiber Losada
-- Create date:  7/23/2013 1:25:13 PM
-- Description:	Procedimiento almacenado que consulta un(a) Niveldegobierno por Rama o Estructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Niveldegobiernos_Consultar_IdRamaEstructura] @IdRamaEstructura INT =0,@idEstado INT =1

					
AS
BEGIN

 SELECT ng.IdNiveldegobierno, ng.CodigoNiveldegobierno, ng.Descripcion, ng.Estado, ng.UsuarioCrea, ng.FechaCrea, ng.UsuarioModifica, ng.FechaModifica 
 FROM Proveedor.Niveldegobierno AS ng
	INNER JOIN Proveedor.RamaoEstructura_Niveldegobierno AS rsng ON ng.IdNiveldegobierno=rsng.IdNiveldegobierno
	WHERE rsng.IdRamaEstructura= @IdRamaEstructura AND   ng.Estado =@idEstado
	ORDER BY Descripcion ASC
 END



