﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/18/2013 6:02:08 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocDatosBasicoProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProv_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocDatosBasicoProv WHERE IdDocAdjunto = @IdDocAdjunto
END

