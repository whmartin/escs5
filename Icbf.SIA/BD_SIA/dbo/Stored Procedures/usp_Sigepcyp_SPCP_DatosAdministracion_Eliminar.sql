﻿

-- =============================================
-- Author:		ICBF\brayher.gomez
-- Create date:  13/03/2015 9:36:01 p. m.
-- Description:	Procedimiento almacenado que elimina un(a) DatosAdministracion
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdministracion_Eliminar]
	@IdDatosAdministracion INT
AS
BEGIN
	DELETE SPCP.DatosAdministracion WHERE IdDatosAdministracion = @IdDatosAdministracion
END
