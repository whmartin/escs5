﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que elimina un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Eliminar]
	@IdEstadoValidacionDocumental INT
AS
BEGIN
	DELETE Proveedor.EstadoValidacionDocumental WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END

