﻿-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que actualiza un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Modificar]
		@IdCorreoElectronico NUMERIC(18,0),	
		@IdArchivo NUMERIC(18,0),	
		@Destinatario NVARCHAR(256),	
		@Mensaje NVARCHAR(4000),	
		@Estado NVARCHAR(1),	
		@FechaIngreso DATETIME,	
		@FechaEnvio DATETIME, 
		@UsuarioModifica NVARCHAR(250),
		@TipoCorreo CHAR(1)
AS
BEGIN
	IF(@FechaEnvio IS NULL)
		SET @FechaEnvio = GETDATE()
	UPDATE 
		Estructura.CorreoElectronico 
	SET 
		IdArchivo = @IdArchivo, 
		Destinatario = @Destinatario, 
		Mensaje = @Mensaje, 
		Estado = @Estado, 
		FechaIngreso = @FechaIngreso, 
		FechaEnvio = @FechaEnvio, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE(), 
		TipoCorreo = @TipoCorreo
	WHERE 
		IdCorreoElectronico = @IdCorreoElectronico
END

