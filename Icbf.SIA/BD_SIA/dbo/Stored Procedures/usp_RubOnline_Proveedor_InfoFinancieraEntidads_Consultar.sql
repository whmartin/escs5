﻿



-- Modificado Por: Juan Carlos Valverde Sámano
-- Fecha: 2014/05/19
-- Descripción: Se modificó para que cuando no se ha validado un registro, la columna Aprobado no muestre ni SI ni NO, 
-- solo se muestre1 sin texto.

CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidads_Consultar]
	@IdEntidad INT = NULL, @IdVigencia INT = NULL
AS
BEGIN
 --SELECT IdInfoFin, IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, Finalizado
 --FROM [Proveedor].[InfoFinancieraEntidad] 
 --WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 --AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
 --ORDER BY IdVigencia DESC 
 
 
 
 --SELECT IFEnt.IdInfoFin, IFEnt.IdEntidad, IFEnt.IdVigencia, IFEnt.ActivoCte, IFEnt.ActivoTotal, IFEnt.PasivoCte, IFEnt.PasivoTotal, IFEnt.Patrimonio, IFEnt.GastosInteresFinancieros, IFEnt.UtilidadOperacional, IFEnt.ConfirmaIndicadoresFinancieros, IFEnt.RupRenovado, IFEnt.EstadoValidacion, IFEnt.ObservacionesInformacionFinanciera, IFEnt.ObservacionesValidadorICBF, IFEnt.UsuarioCrea, IFEnt.FechaCrea, IFEnt.UsuarioModifica, IFEnt.FechaModifica, IFEnt.Finalizado
 --,ValIFEnt.ConfirmaYAprueba
 --FROM [Proveedor].[InfoFinancieraEntidad] IFEnt
 --LEFT JOIN  [Proveedor].ValidarInfoFinancieraEntidad ValIFEnt
 --ON IFEnt.IdInfoFin = ValIFEnt.IdInfoFin
 --WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 --AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END 
 
 --ORDER BY IdVigencia DESC,  IFEnt.FechaCrea DESC
 
 --
SELECT ROW_NUMBER() over (order by IFEnt.IdEntidad DESC, IFEnt.IdVigencia ASC , isnull(ValIFEnt.FechaCrea, IFEnt.FechaCrea) DESC ) numRenglon, IFEnt.IdInfoFin, IFEnt.IdEntidad, IFEnt.IdVigencia, IFEnt.ActivoCte, IFEnt.ActivoTotal, IFEnt.PasivoCte, IFEnt.PasivoTotal, IFEnt.Patrimonio, IFEnt.GastosInteresFinancieros, IFEnt.UtilidadOperacional, IFEnt.ConfirmaIndicadoresFinancieros, IFEnt.RupRenovado, IFEnt.EstadoValidacion, IFEnt.ObservacionesInformacionFinanciera, IFEnt.ObservacionesValidadorICBF, IFEnt.UsuarioCrea, IFEnt.UsuarioModifica, IFEnt.FechaModifica, IFEnt.Finalizado,
		CASE 
		WHEN (IFEnt.EstadoValidacion = (SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental	
														WHERE Descripcion='EN VALIDACIÓN')) 
		THEN
		' ' 
		WHEN (IFEnt.EstadoValidacion = (SELECT IdEstadoValidacionDocumental FROM PROVEEDOR.EstadoValidacionDocumental	
														WHERE Descripcion='POR VALIDAR')) 
		THEN
		' ' 
		WHEN (ValIFEnt.ConfirmaYAprueba=1) 
														THEN 'SI'
		WHEN (ValIFEnt.ConfirmaYAprueba=0) 
														THEN 'NO'														
		END AS'ConfirmaYAprueba',
IFEnt.FechaCrea, ValIFEnt.FechaCrea AS FechaCreaVal, ValIFEnt.FechaModifica as FechaModVal
 into #tablaEntidad
 FROM [Proveedor].[InfoFinancieraEntidad] IFEnt
 LEFT JOIN  [Proveedor].ValidarInfoFinancieraEntidad ValIFEnt
 ON IFEnt.IdInfoFin = ValIFEnt.IdInfoFin
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END 
 AND IdVigencia = CASE WHEN @IdVigencia IS NULL THEN IdVigencia ELSE @IdVigencia END
 --ORDER BY IdVigencia DESC, IFEnt.FechaCrea DESC


DECLARE @indice int 
DECLARE @Total int
DECLARE @IdVigenciaCiclo int
DECLARE @IdVigenciaAnterior int
select @indice = 1;
select @Total = COUNT(*) from #tablaEntidad
SET @IdVigenciaAnterior = -1
WHILE(@indice <= @Total)
BEGIN
	select @IdVigenciaCiclo = IdVigencia from #tablaEntidad where numRenglon = @indice;
	
	if(@IdVigenciaAnterior = @IdVigenciaCiclo)
		delete from #tablaEntidad where numRenglon = @indice;
	
	SET @IdVigenciaAnterior = @IdVigenciaCiclo
	
	SET @indice = @indice + 1;
END

 
 select * from  #tablaEntidad
 
 drop table #tablaEntidad
 
END







