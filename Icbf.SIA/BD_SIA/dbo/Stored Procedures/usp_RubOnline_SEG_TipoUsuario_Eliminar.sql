﻿

-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que elimina un(a) TipoUsuario
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_TipoUsuario_Eliminar]
	@IdTipoUsuario INT
AS
BEGIN
	DELETE SEG.TipoUsuario WHERE IdTipoUsuario = @IdTipoUsuario
END

