﻿

-- =============================================
-- Author:		Faiber Losada	
-- Create date:  2014/07/29 9:01:36 PM
-- Description:	Procedimiento almacenado que actualiza el 
-- estado del mod. Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoIntegrantesEntidad_ModificarEstado]
		@IdEntidad INT, @IdEstadoInfoDatosIntegrantes INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[ValidacionIntegrantesEntidad] 
	SET IdEstadoValidacionIntegrantes = @IdEstadoInfoDatosIntegrantes, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdEntidad = @IdEntidad
END




