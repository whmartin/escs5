﻿

CREATE procedure [dbo].[usp_ICBF_Proveedores_GetDataProducto]
	@CodigoSegmento nvarchar(max)
	,@CodigoFamilia nvarchar(max)
	,@CodigoClase nvarchar(max)
as
begin

declare @query as nvarchar(max) = '
		select	-1 as [Codigo],''  Seleccionar Todo'' as [Producto]	
	union
		select	distinct uns.[Codigo],uns.[Descripcion] Producto
		from	[PROVEEDOR].[TipoCodigoUNSPSC] uns'
				
-->	Agregando Filtro de Segmento @CodigoSegmento 
	if not exists(select 1 from dbo.StringSplit(@CodigoSegmento,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoSegmento,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN StringSplit((@CodigoSegmento),1,0) fcs on fcs.val = uns.[CodigoSegmento] and fcs.val != -1'
				end
		end

-->	Agregando Filtro de Familia @CodigoFamilia
	if not exists(select 1 from dbo.StringSplit(@CodigoFamilia,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoFamilia,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoFamilia,1,0) fcf on fcf.val = uns.[CodigoFamilia] and fcf.val != -1 '
				end
		end

-->	Agregando Filtro de  Clase @CodigoClase
	if not exists(select 1 from dbo.StringSplit(@CodigoClase,1,0) where val = -1)
		begin
			if ((select COUNT(*) from dbo.StringSplit(@CodigoClase,1,0) where val != -1) >0)
				begin
					set @query = @query+'
					INNER  JOIN dbo.StringSplit(@CodigoClase,1,0) fcc on fcc.val = uns.[CodigoClase] and fcc.val != -1 '
				end
		end


	set @query = @query+'	
	where	uns.[Codigo] is not null
	order by 2 asc'

	print @query
	EXEC sp_executesql @query,N'@CodigoSegmento nvarchar(max),@CodigoFamilia nvarchar(max),@CodigoClase nvarchar(max)',@CodigoSegmento,@CodigoFamilia, @CodigoClase
	
end

/*****************************************************************************************************************************************************************************/
