﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_UnidadMedida_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_UnidadMedida_Consultar]
	@IdNumeroContrato INT
AS
BEGIN
 SELECT IdNumeroContrato,NumeroContrato, FechaInicioEjecuciónContrato, FechaTerminacionInicialContrato, FechaFinalTerminacionContrato, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[UnidadMedida] WHERE  IdNumeroContrato = @IdNumeroContrato
END
