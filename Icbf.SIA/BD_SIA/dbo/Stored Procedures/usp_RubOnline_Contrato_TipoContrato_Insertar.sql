﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoContrato_Insertar]
		@IdTipoContrato INT OUTPUT, 	@NombreTipoContrato NVARCHAR(128),	@IdCategoriaContrato INT,	@ActaInicio BIT,	@AporteCofinaciacion BIT,	@RecursoFinanciero BIT,	@RegimenContrato INT,	@DescripcionTipoContrato NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoContrato(NombreTipoContrato, IdCategoriaContrato, ActaInicio, AporteCofinaciacion, RecursoFinanciero, RegimenContrato, DescripcionTipoContrato, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreTipoContrato, @IdCategoriaContrato, @ActaInicio, @AporteCofinaciacion, @RecursoFinanciero, @RegimenContrato, @DescripcionTipoContrato, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoContrato = @@IDENTITY 		
END

