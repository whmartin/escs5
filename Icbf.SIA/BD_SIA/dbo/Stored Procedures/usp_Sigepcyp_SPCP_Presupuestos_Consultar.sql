﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  3/12/2015 2:21:15 PM
-- Description:	Procedimiento almacenado que consulta un(a) Presupuestos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Presupuestos_Consultar]
	@IdPresupuesto INT
AS
BEGIN
 SELECT IdPresupuesto, IdDatosAdministracion, CodPosicionGasto, NomPosicionGasto, NomFuenteFinanciacion, CodRecursoPresupuestal, 
 NomRecursoPresupuestal, NomSituacionFondos, 
 ValorInicialRubro,ValorActualRubro,ValorSaldoRubro,
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[Presupuestos] WHERE  IdPresupuesto = @IdPresupuesto
END
