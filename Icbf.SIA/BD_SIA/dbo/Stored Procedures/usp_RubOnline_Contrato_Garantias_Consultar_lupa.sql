﻿



-- =============================================
-- Author:		Ares\Jonathan Acosta
-- Create date:  7/1/2013 11:23:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) Garantia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Garantias_Consultar_lupa]
	@IDTipoGarantia INT = NULL,
	@NumGarantia NVARCHAR(128) = NULL,
	@NumContrato nvarchar(50) = NULL,
	@Estado		bit = null
AS
BEGIN
 SELECT IDGarantia, garantia.IDContrato, garantia.IDTipoGarantia, NumGarantia, 
 FechaExpedicion, FechaVencInicial, FechaRecibo, FechaDevolucion, 
 MotivoDevolucion, FechaAprobacion, FechaCertificacionPago, Valor, 
 garantia.Estado, Anexo, ObservacionAnexo, garantia.UsuarioCrea, garantia.FechaCrea, garantia.UsuarioModifica, 
 garantia.FechaModifica, contrato.NumeroContrato,
 tipogaratia.NombreTipoGarantia as TipoGarantia
 FROM [Contrato].[Garantia] as garantia inner join Contrato.Contrato as contrato 
 on (contrato.IdContrato = garantia.IDContrato) INNER JOIN Contrato.TipoGarantia tipogaratia
 on (tipogaratia.IdTipoGarantia = garantia.IDTipoGarantia)
 WHERE garantia.IDTipoGarantia = CASE WHEN @IDTipoGarantia IS NULL THEN garantia.IDTipoGarantia ELSE @IDTipoGarantia END 
 AND NumGarantia = CASE WHEN @NumGarantia IS NULL THEN NumGarantia ELSE @NumGarantia END
 AND contrato.NumeroContrato = CASE WHEN @NumContrato IS NULL THEN NumeroContrato ELSE @NumContrato END
 AND garantia.Estado = CASE WHEN @Estado IS NULL THEN garantia.Estado ELSE @Estado END
END





