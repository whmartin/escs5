﻿
-- =============================================
-- Author:		Juan Pablo Isaquita Pacheco
-- Create date: 13/11/2012
-- Description:	Procedimiento almacenado que consulta  la auditoria
-- Grupo de Apoyo SSII - SIGEPCYP 31/07/2015
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarAuditoria]
	@pNombrePrograma NVARCHAR(200),
	@pIdRegistro NUMERIC(18,0)
AS
BEGIN

SELECT
	A.fecha Fecha,
	U.PrimerNombre + ' ' + U.PrimerApellido,
	A.operacion Operacion,
	A.parametrosOperacion ParametrosOperacion,
	A.tabla Tabla,
	A.direccionIp DireccionIp,
	A.navegador Navegador
FROM
	AUDITA.LogRubOnline A
LEFT OUTER JOIN
	SEG.Usuario U ON A.usuario = U.IdUsuario
WHERE
	A.programa = @pNombrePrograma
AND
	A.idRegistro = 	@pIdRegistro


END

