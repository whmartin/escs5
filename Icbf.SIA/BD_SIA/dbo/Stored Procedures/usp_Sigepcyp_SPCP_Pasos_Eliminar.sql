﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/18/2015 1:47:11 PM
-- Description:	Procedimiento almacenado que elimina un(a) Pasos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Pasos_Eliminar]
	@IdPaso INT
AS
BEGIN
	DELETE SPCP.Pasos WHERE IdPaso = @IdPaso
END
