﻿
create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Insertar]
		@IdTablaParametrica INT OUTPUT, 	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.TablaParametrica(CodigoTablaParametrica, NombreTablaParametrica, Url, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTablaParametrica, @NombreTablaParametrica, @Url, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTablaParametrica = SCOPE_IDENTITY() 		
END


