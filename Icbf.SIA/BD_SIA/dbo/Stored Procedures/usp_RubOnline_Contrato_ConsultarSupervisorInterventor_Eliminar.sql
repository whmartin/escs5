﻿
-- =============================================
-- Author:		@Res\Andrés Morales
-- Create date:  6/30/2013 11:32:25 AM
-- Description:	Procedimiento almacenado que elimina un(a) ConsultarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ConsultarSupervisorInterventor_Eliminar]
	@TipoSupervisorInterventor INT
AS
BEGIN
	DELETE Contrato.ConsultarSupervisorInterventor WHERE TipoSupervisorInterventor = @TipoSupervisorInterventor
END


