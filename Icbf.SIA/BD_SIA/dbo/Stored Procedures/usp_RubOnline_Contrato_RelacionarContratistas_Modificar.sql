﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Modificar]
		@IdContratistaContrato INT,	
		@IdContrato INT,	
		@NumeroIdentificacion BIGINT,	
		@ClaseEntidad NVARCHAR,	
		@PorcentajeParticipacion INT,	
		@NumeroIdentificacionRepresentanteLegal BIGINT, 
		@EstadoIntegrante bit, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN

	UPDATE 
	Contrato.RelacionarContratistas 
	SET 
	IdContrato = @IdContrato, 
	NumeroIdentificacion = @NumeroIdentificacion, 
	ClaseEntidad = @ClaseEntidad, 
	PorcentajeParticipacion = @PorcentajeParticipacion, 
	NumeroIdentificacionRepresentanteLegal = @NumeroIdentificacionRepresentanteLegal, 
	EstadoIntegrante = @EstadoIntegrante, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE 
	IdContratistaContrato = @IdContratistaContrato
	
END

