﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que elimina un(a) NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Eliminar]
	@IdNivelOrganizacional INT
AS
BEGIN
	DELETE Proveedor.NivelOrganizacional WHERE IdNivelOrganizacional = @IdNivelOrganizacional
END

