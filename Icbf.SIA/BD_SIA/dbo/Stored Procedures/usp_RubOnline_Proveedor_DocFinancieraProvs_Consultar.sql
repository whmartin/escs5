﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProvs_Consultar]
	@IdInfoFin int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdInfoFin, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocFinancieraProv] 
 WHERE IdInfoFin = CASE WHEN @IdInfoFin IS NULL THEN IdInfoFin ELSE @IdInfoFin END
END


