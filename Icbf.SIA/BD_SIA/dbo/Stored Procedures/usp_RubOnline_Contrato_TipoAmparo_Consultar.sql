﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoAmparo_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoAmparo_Consultar]
	@IdTipoAmparo INT
AS
BEGIN
 SELECT IdTipoAmparo, NombreTipoAmparo, IdTipoGarantia, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoAmparo] WHERE  IdTipoAmparo = @IdTipoAmparo
END


