﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que consulta un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Consultar]
	@IdParametro INT
AS
BEGIN
 SELECT IdParametro, NombreParametro, ValorParametro, ImagenParametro, Estado, Funcionalidad, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [SEG].[Parametro] 
 WHERE  IdParametro = @IdParametro
END

