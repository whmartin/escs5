﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que elimina un(a) AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AccionPaso_Eliminar]
	@IdAccionPaso INT
AS
BEGIN
	DELETE SPCP.AccionPaso WHERE IdAccionPaso = @IdAccionPaso
END
