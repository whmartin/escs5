﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  30/11/2012 15:06:01
-- Description:	Procedimiento almacenado que consulta un(a) Archivo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_Consultar]
	@IdArchivo NUMERIC(18,0)
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , IdFormatoArchivo
      , FechaRegistro
      , NombreArchivo
      , ServidorFTP
      , Estado
      , ResumenCarga
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
 FROM [Estructura].[Archivo] 
 WHERE  IdArchivo = @IdArchivo
END


