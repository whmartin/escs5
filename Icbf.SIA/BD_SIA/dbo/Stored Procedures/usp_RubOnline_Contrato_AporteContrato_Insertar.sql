﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Insertar
***********************************************/


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Insertar]
		@IdAporte INT OUTPUT, 	@IdContrato NVARCHAR(3),	@TipoAportante NVARCHAR(11),	@NumeroIdenticacionContratista bigint,	@TipoAporte NVARCHAR(10),	@ValorAporte INT,	@DescripcionAporteEspecie NVARCHAR(128),	@FechaRP DATETIME,	@NumeroRP INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.AporteContrato(IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, @TipoAportante, @NumeroIdenticacionContratista, @TipoAporte, @ValorAporte, @DescripcionAporteEspecie, @FechaRP, @NumeroRP, @UsuarioCrea, GETDATE())
	SELECT @IdAporte = @@IDENTITY 		
END

