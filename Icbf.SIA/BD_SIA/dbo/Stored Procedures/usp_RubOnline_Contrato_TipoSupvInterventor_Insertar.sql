﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Insertar]
		@IdTipoSupvInterventor INT OUTPUT, 	@Nombre NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.TipoSupvInterventor(Nombre, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoSupvInterventor = @@IDENTITY 		
END

