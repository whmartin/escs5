﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestals_Consultar]
	@NumeroCDP INT = NULL,@FechaExpedicionCDP DATETIME = NULL
AS
BEGIN
 SELECT IdInformacionPresupuestal, NumeroCDP, ValorCDP, FechaExpedicionCDP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestal] WHERE NumeroCDP = CASE WHEN @NumeroCDP IS NULL THEN NumeroCDP ELSE @NumeroCDP END AND FechaExpedicionCDP = CASE WHEN @FechaExpedicionCDP IS NULL THEN FechaExpedicionCDP ELSE @FechaExpedicionCDP END
END

