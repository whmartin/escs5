﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Consultar]
	@IdTipodeActividad INT
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeActividad] WHERE  IdTipodeActividad = @IdTipodeActividad
END

