﻿-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidads_Consultar]
	@CodigoSectorEntidad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoSectorEntidad, CodigoSectorEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipoSectorEntidad] WHERE CodigoSectorEntidad = CASE WHEN @CodigoSectorEntidad IS NULL THEN CodigoSectorEntidad ELSE @CodigoSectorEntidad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

