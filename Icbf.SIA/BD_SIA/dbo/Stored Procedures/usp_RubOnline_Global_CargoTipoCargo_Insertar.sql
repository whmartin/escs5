﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que guarda un nuevo CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Insertar]
		@IdCargo INT,	@TipoCargo NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.CargoTipoCargo(IdCargo, TipoCargo, UsuarioCrea, FechaCrea)
					  VALUES(@IdCargo, @TipoCargo, @UsuarioCrea, GETDATE())
END

