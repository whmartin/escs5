﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:05:12 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TiposCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_TiposCuenta_Insertar]
		@IdTipoCuenta INT OUTPUT, 	@TipoCuenta NVARCHAR(50),	@Estado INT,	@IdRegional INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

	IF EXISTS(SELECT * FROM SPCP.TiposCuenta WHERE TipoCuenta = @TipoCuenta and IdRegional = @IdRegional)
	BEGIN
		RAISERROR ('Tipo de Cuenta ya existe, por favor verifique', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	    RETURN
	END

	INSERT INTO SPCP.TiposCuenta(TipoCuenta, Estado, IdRegional, UsuarioCrea, FechaCrea)
					  VALUES(@TipoCuenta, @Estado, @IdRegional, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCuenta = @@IDENTITY 		
END
