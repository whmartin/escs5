﻿
-- =============================================
-- Author:		Yuri Gereda
-- Create date: 06-11-2012
-- Description:	Procedimiento almacenado que consulta los Módulos permitidos para un  Rol
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarModulosRol]
	@nombreRol NVARCHAR(250)
AS
BEGIN

SELECT 
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion
FROM 
	SEG.ProgramaRol PR
INNER JOIN 
	SEG.Rol R ON PR.IdRol = R.IdRol
INNER JOIN
	SEG.Programa P ON PR.IdPrograma = P.IdPrograma
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo			
WHERE
	R.Nombre = @nombreRol	
GROUP BY
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion	
ORDER BY
	M.Posicion DESC	

END

