﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que consulta un(a) AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AccionPasoPorCuentaCobro_Consultar]
	@IdCuentaCobro INT
AS
BEGIN
	SELECT a.CodAccion
			,ap.Aceptar
			,ap.Rechazar
	FROM SPCP.AccionPaso ap
	INNER JOIN SPCP.Acciones a
	ON ap.IdAccion = a.IdAcciones
	INNER JOIN SPCP.FlujoPasosCuenta fpc
	ON ap.IdPaso = fpc.IdPaso
	INNER JOIN SPCP.CuentasdeCobro cc
	ON fpc.IdFlujoPasoCuenta = cc.IdEstadoPaso
	WHERE cc.IdCuenta = @IdCuentaCobro	
END