﻿

--- =================================================================
-- Author:		<Author,,Zulma Barrantes>
-- Create date: <13 de Agosto del 2015,12:03 p.m.,>
-- Description:	<Permite eliminar  Información de Datos Generales de una Vigencia,>
-- ====================================================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_DatosVigencia_Eliminar]
	@IdDatosVigencia INT
AS
BEGIN
	DELETE Global.DatosVigencia WHERE IdDatosVigencia = @IdDatosVigencia
END
