﻿


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocDatosBasicoProv
---- Desarrollador Modificacion: Juan Carlos Valverde Sámano
-- Fecha modificación: 25/03/2014
-- Descripción: Se agregó en el WHERE la condición de que solo muestre los registros donde el campo Activo es = 1.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocDatosBasicoProvs_Consultar]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT IdDocAdjunto, IdEntidad, NombreDocumento, LinkDocumento, Observaciones, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, IdTemporal
 FROM [Proveedor].[DocDatosBasicoProv] 
 WHERE IdEntidad = CASE WHEN @IdEntidad IS NULL THEN IdEntidad ELSE @IdEntidad END
 AND Activo=1
END




