﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Consultar]
	@IdGestionObligacion INT
AS
BEGIN
 SELECT IdGestionObligacion, IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[GestionarObligacion] WHERE  IdGestionObligacion = @IdGestionObligacion
END
