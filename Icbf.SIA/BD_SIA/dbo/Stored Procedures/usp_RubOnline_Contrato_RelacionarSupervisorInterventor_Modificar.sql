﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/30/2013 3:48:51 PM
-- Description:	Procedimiento almacenado que actualiza un(a) RelacionarSupervisorInterventor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Modificar]
@IDSupervisorInterv INT, 	
@IDContratoSupervisa INT,	
@OrigenTipoSupervisor bit,	
@IDTipoSupvInterventor INT,	
@IDTerceroExterno INT,	
@IDFuncionarioInterno INT,	
@IDContratoInterventoria INT,
@IDTerceroInterventoria int,	
@TipoVinculacion bit,	
@FechaInicia DATETIME,	
@FechaFinaliza DATETIME,	
@Estado bit,	
@FechaModificaInterventor DATETIME,	
@UsuarioModifica nvarchar(128)
AS
BEGIN
	UPDATE Contrato.SupervisorIntervContrato 
	SET IDContratoSupervisa = @IDContratoSupervisa, 
	OrigenTipoSupervisor = @OrigenTipoSupervisor, 
	IDTipoSupvInterventor = @IDTipoSupvInterventor, 
	IDTerceroExterno = @IDTerceroExterno, 
	IDFuncionarioInterno = @IDFuncionarioInterno, 
	IDContratoInterventoria = @IDContratoInterventoria,
	IDTerceroInterventoria = @IDTerceroInterventoria,
	TipoVinculacion = @TipoVinculacion, 
	FechaInicia = @FechaInicia, 
	FechaFinaliza = @FechaFinaliza, 
	Estado = @Estado, 
	FechaModificaInterventor = @FechaModificaInterventor, 
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IDSupervisorInterv = @IDSupervisorInterv
END

