﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/21/2013 2:27:09 PM
-- Description:	Procedimiento almacenado que elimina un(a) Sucursal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Sucursal_Eliminar]
	@IdSucursal INT
AS
BEGIN
	DELETE Proveedor.Sucursal WHERE IdSucursal = @IdSucursal
END

