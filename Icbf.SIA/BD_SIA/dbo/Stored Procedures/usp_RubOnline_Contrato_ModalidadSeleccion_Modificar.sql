﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Modificar]
		@IdModalidad INT,	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(5),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ModalidadSeleccion SET Nombre = @Nombre, Sigla = @Sigla, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdModalidad = @IdModalidad
END


