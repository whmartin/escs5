﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ModalidadSeleccion_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ModalidadSeleccion_Insertar]
		@IdModalidad INT OUTPUT, 	@Nombre NVARCHAR(128),	@Sigla NVARCHAR(5),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ModalidadSeleccion(Nombre, Sigla, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Nombre, @Sigla, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdModalidad = @@IDENTITY 		
END


