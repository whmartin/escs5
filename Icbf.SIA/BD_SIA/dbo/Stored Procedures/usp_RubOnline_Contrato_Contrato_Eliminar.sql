﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/1/2013 11:48:15 AM
-- Description:	Procedimiento almacenado que elimina un(a) Contrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Contrato_Eliminar]
	@IdContrato INT
AS
BEGIN
	DELETE Contrato.Contrato WHERE IdContrato = @IdContrato
END
