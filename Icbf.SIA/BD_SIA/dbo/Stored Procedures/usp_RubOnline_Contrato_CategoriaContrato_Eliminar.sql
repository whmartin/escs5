﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Eliminar]
	@IdCategoriaContrato INT
AS
BEGIN
	DELETE Contrato.CategoriaContrato WHERE IdCategoriaContrato = @IdCategoriaContrato
END

