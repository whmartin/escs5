﻿
-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  26/12/2012 14:34:48
-- Description:	Procedimiento almacenado que consulta un(a) TipoUsuario
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 03/08/2015 se agregan los campos IdArea y IdRegional
-- =============================================



CREATE PROCEDURE [dbo].[usp_SPCP_SEG_TipoUsuario_Consultar]
	@IdTipoUsuario INT
AS
BEGIN
 SELECT IdTipoUsuario, CodigoTipoUsuario, NombreTipoUsuario, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [SEG].[TipoUsuario] 
 WHERE  IdTipoUsuario = @IdTipoUsuario
END

