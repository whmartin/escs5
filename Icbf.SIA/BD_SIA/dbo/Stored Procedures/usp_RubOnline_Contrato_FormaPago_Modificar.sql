﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Modificar]
		@IdFormapago INT,	@NombreFormaPago NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.FormaPago SET NombreFormaPago = @NombreFormaPago, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdFormapago = @IdFormapago
END

