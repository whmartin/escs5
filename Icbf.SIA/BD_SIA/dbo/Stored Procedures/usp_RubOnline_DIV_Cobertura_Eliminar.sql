﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  06/12/2012 17:17:30
-- Description:	Procedimiento almacenado que elimina un(a) Cobertura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Cobertura_Eliminar]
	@IdCobertura INT
AS
BEGIN
	DELETE DIV.Cobertura WHERE IdCobertura = @IdCobertura
END

