﻿-- =============================================
-- Author:		ICBF\Tommy.Puccini
-- Create date:  13/12/2012 10:52:54
-- Description:	Procedimiento almacenado que consulta un(a) ServicioVigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_ServicioVigencia_Consultar]
	@IdServicioVigencia INT
AS
BEGIN
 SELECT IdServicioVigencia, IdVigencia, IdServicio, IdRubro, CodigoServicio, NombreServicio, CodigoRubro, NombreRubro, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[ServicioVigencia] WHERE  IdServicioVigencia = @IdServicioVigencia
END

