﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/11/2015 5:46:57 PM
-- Description:	Procedimiento almacenado que consulta un(a) CuentasdeCobro
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobro_Consultar]
	@IdCuenta INT
AS
BEGIN
 SELECT IdCuenta, IdContrato, IdEstadoPaso, IdTipoCuenta, IdEstadoGestion, Pensionado, Declarante, IdRegimen, PlanillaAportes, IdNivelARL, IdPlanDePagos, NumeroFactura, AporteSalud, AportePension, SolidaridadPensional, AporteAFC, AporteARL, IdDatosAdministracion, CodCompromiso, NumeroDePago, NombreSupervisor, CargoSupervisor, TotalContrato, HonorarioIva, HonorarioSinIva, HonorarioAcumulado, Saldo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[CuentasdeCobro] WHERE  IdCuenta = @IdCuenta
END

