﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AporteContrato_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AporteContrato_Consultar]
	@IdAporte INT
AS
BEGIN
 SELECT IdAporte, IdContrato, TipoAportante, NumeroIdenticacionContratista, TipoAporte, ValorAporte, DescripcionAporteEspecie, FechaRP, NumeroRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AporteContrato] WHERE  IdAporte = @IdAporte
END
