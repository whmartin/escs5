﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  7/6/2013 9:48:30 PM
-- Description:	Procedimiento almacenado que actualiza un(a) LugarEjecucionContrato
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_LugarEjecucionContrato_Modificar]
		@IdContratoLugarEjecucion INT,	
		@IDContrato INT,	
		@Nacional BIT, 
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE 
		Contrato.LugarEjecucionContrato 
	SET IDContrato = @IDContrato, 
		Nacional = @Nacional, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdContratoLugarEjecucion = @IdContratoLugarEjecucion
END

