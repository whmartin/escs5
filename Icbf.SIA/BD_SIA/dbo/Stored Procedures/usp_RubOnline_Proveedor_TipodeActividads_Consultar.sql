﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividads_Consultar]
	@CodigoTipodeActividad NVARCHAR(128) = NULL,@Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipodeActividad, CodigoTipodeActividad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[TipodeActividad] WHERE CodigoTipodeActividad = CASE WHEN @CodigoTipodeActividad IS NULL THEN CodigoTipodeActividad ELSE @CodigoTipodeActividad END AND Descripcion = CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

