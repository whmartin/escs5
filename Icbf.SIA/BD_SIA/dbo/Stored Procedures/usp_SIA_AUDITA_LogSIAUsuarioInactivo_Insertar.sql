﻿
/***
Autor: Juan Carlos Valverde Sámano
Fecha: 18/07/2014
Descripción: Inserta un Log en la entidad AUDITA.LogSIAUsuarioInactivo
***/
CREATE PROCEDURE [dbo].[usp_SIA_AUDITA_LogSIAUsuarioInactivo_Insertar]
@UserId NVARCHAR(125)
,@IsApproved bit
,@TipoOperacion [nvarchar](20)
,@ValorMesesEliminacion [nvarchar](5)
,@ValorDiasNotificacion [nvarchar](5)
,@Usuario [nvarchar](250)
,@UsuarioCreacion [nvarchar](250)
,@FechaCreacion [datetime]
,@TipoDocumento [nvarchar](100)
,@NumeroDocumento [nvarchar](20)
,@DV [nvarchar](1)
,@PrimerNombre [nvarchar](150)
,@SegundoNombre [nvarchar](150)
,@PrimerApellido [nvarchar](150)
,@SegundoApellido [nvarchar](150)
,@RazonSocial [nvarchar](150)
,@Estado [bit]
,@FechaOperacion [datetime]
,@Motivo [nvarchar](250)
AS
BEGIN

INSERT INTO [AUDITA].[LogSIAUsuarioInactivo]
           (Usuario,
			UsuarioCreacion,
			FechaCreacion,
			TipoDocumento,
			NumeroDocumento,
			DV,
			PrimerNombre,
			SegundoNombre,
			PrimerApellido,
			SegundoApellido,
			RazonSocial,
			Estado,
			TipoOperacion,
			FechaOperacion,
			ValorMesesEliminacion,
			ValorDiasNotificacion,
			Motivo)
     VALUES(
			@Usuario,
			@UsuarioCreacion,
			@FechaCreacion,
			@TipoDocumento,
			@NumeroDocumento,
			@DV,
			@PrimerNombre,
			@SegundoNombre,
			@PrimerApellido,
			@SegundoApellido,
			@RazonSocial,
			@Estado,
			@TipoOperacion,
			GETDATE(),
			@ValorMesesEliminacion,
			@ValorDiasNotificacion,
			@Motivo
     )
END


