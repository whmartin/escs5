﻿
-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCargoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidads_Consultar]
	@CodigoTipoCargoEntidad NVARCHAR(128) = NULL, @Descripcion NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoCargoEntidad, CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCargoEntidad] 
 WHERE CodigoTipoCargoEntidad LIKE CASE WHEN @CodigoTipoCargoEntidad IS NULL THEN CodigoTipoCargoEntidad ELSE @CodigoTipoCargoEntidad + '%' END 
 AND Descripcion LIKE CASE WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion + '%' END 
 AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


