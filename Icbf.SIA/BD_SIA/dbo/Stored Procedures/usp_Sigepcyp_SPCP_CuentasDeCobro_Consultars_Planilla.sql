﻿


-- =============================================
-- Author:		Jorge Vizcaino
-- Create date: Script Date: 10/10/2015 11:11:07 a. m.
-- Description:	Procedimiento almacenado que consulta un(a) Planilla
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasDeCobro_Consultars_Planilla]
	@IdRegional int = NULL,
	@IdArea int = NULL,
    @NumeroCtaCobro int = NULL,
    @IdTipoDoc int = NULL,
    @NumeroDoc nvarchar(50) = NULL
AS
BEGIN

	Declare @tempTipoCuenta as table(idTemp int not null identity(1,1),
									idTipoCuenta int,
									PrimerPaso int,
									SegundoPaso int)


	INSERT INTO @tempTipoCuenta(idTipoCuenta)
	SELECT IdTipoCuenta
	FROM SPCP.FlujoPasosCuenta
	WHERE estado = 1
	group by IdTipoCuenta

	DECLARE @i int,
			@cant int,
			@IdTipoCuentaSel int,
			@EstadoPaso int,
			@EstadoPrimerPaso int


	SELECT @cant = COUNT(idTemp)
	FROM @tempTipoCuenta

	set @i  = 1

	While (@i <= @cant)
	BEGIN

		SELECT @IdTipoCuentaSel = idTipoCuenta
		FROM @tempTipoCuenta
		where idTemp = @i

		SELECT top 1 @EstadoPaso = IdFlujoPasoCuenta
		FROM (SELECT top 2 IdFlujoPasoCuenta,ROW_NUMBER() OVER(ORDER BY IdFlujoPasoCuenta ASC) AS ContRow
				FROM SPCP.FlujoPasosCuenta
				WHERE IdTipoCuenta = @IdTipoCuentaSel
				AND estado = 1
				ORDER BY Orden asc	) tabFlujo		
		ORDER BY ContRow DESC		

		SELECT top 1 @EstadoPrimerPaso = IdFlujoPasoCuenta
		FROM SPCP.FlujoPasosCuenta
		WHERE IdTipoCuenta = @IdTipoCuentaSel
		AND estado = 1
		ORDER BY Orden asc

		UPDATE @tempTipoCuenta set SegundoPaso = @EstadoPaso, PrimerPaso = @EstadoPrimerPaso
		WHERE idTemp = @i
		
		set @i = @i+1
	END

		
	SELECT	cc.IdCuenta,
			cc.IdContrato,
			cc.IdEstadoPaso,
			cc.IdTipoCuenta,
			cc.UsuarioCrea,
			cc.FechaCrea,
			cc.UsuarioModifica,
			cc.FechaModifica,
			pp.Vigencia,
			a.NombreArea,
			cc.NombreSupervisor as supervisor,
			da.TipoDocumento,
			da.CodTipoDocSoporte as tipoDocFuente,
			da.FechaDocSoporte as fechaDocFuente,
			da.NumDocSoporte as NumeroDocFuente,
			da.NumDocIdentidad,
			da.NomTercero,
			--pp.CodCompromiso,
			cc.CodCompromiso as CodCompromiso,			
			pp.Recurso as numeroRP,
			pp.Rubro as NombreIdentificadorPre,			
			pp.NumeroDePago as numeroPago,
			--Convert(numeric(30,2),pp.ValorDelPago) as HonorarioPagar,
			Convert(numeric(30,2),cc.HonorarioSinIva) as HonorarioPagar,
			CASE WHEN cc.IdEstadoPaso = (select temtipoc2.PrimerPaso 
										from @tempTipoCuenta temtipoc2 
										where temtipoc2.idTipoCuenta = cc.IdTipoCuenta) THEN 'TRUE' ELSE 'FALSE' END as MostrarDetCuentaCobro
	FROM SPCP.CuentasDeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN Global.Areas a on da.IdArea = a.IdArea
	INNER JOIN SPCP.Presupuestos rp on da.IdDatosAdministracion = rp.IdDatosAdministracion
	INNER JOIN SPCP.PlanDePagos PP
	ON cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	WHERE (IdEstadoPaso in (select SegundoPaso from @tempTipoCuenta) or IdEstadoPaso in (select PrimerPaso from @tempTipoCuenta))
	AND a.IdRegional = isnull(@IdRegional,a.IdRegional)	
	AND a.IdArea = isnull(@IdArea,a.IdArea)
	AND cc.IdCuenta = isnull(@NumeroCtaCobro,cc.IdCuenta)
	AND da.TipoDocumento = isnull(@IdTipoDoc,da.TipoDocumento)
	AND da.NumDocIdentidad = isnull(@NumeroDoc,da.NumDocIdentidad)
	AND cc.IdCuenta not in (select pdt.IdCuenta from SPCP.PlanillaDetalle pdt)

	END