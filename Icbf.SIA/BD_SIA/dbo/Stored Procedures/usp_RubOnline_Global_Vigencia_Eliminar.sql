﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que elimina un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Eliminar]
	@IdVigencia INT
AS
BEGIN
	DELETE Global.Vigencia WHERE IdVigencia = @IdVigencia
END

