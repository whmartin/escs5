﻿	
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ClausulaContrato_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Eliminar]
	@IdClausulaContrato INT
	AS
	BEGIN
		DELETE Contrato.ClausulaContrato WHERE IdClausulaContrato = @IdClausulaContrato
	END

