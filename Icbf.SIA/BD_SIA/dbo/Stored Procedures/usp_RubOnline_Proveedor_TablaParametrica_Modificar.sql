﻿create PROCEDURE [dbo].[usp_RubOnline_Proveedor_TablaParametrica_Modificar]
		@IdTablaParametrica INT,	@CodigoTablaParametrica NVARCHAR(128),	@NombreTablaParametrica NVARCHAR(128),	@Url NVARCHAR(256),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TablaParametrica SET CodigoTablaParametrica = @CodigoTablaParametrica, NombreTablaParametrica = @NombreTablaParametrica, Url = @Url, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTablaParametrica = @IdTablaParametrica
END

