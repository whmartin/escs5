﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  7/18/2015 3:21:49 PM
-- Description:	Procedimiento almacenado que elimina un(a) Planilla
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Planilla_Eliminar]
	@IDPlanilla INT
AS
BEGIN
	DELETE SPCP.Planilla WHERE IDPlanilla = @IDPlanilla
END
