﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TipoPersona_Modificar]
		@IdTipoPersona INT,	@CodigoTipoPersona NVARCHAR(128),	@NombreTipoPersona NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Tercero.TipoPersona SET CodigoTipoPersona = @CodigoTipoPersona, NombreTipoPersona = @NombreTipoPersona, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoPersona = @IdTipoPersona
END

