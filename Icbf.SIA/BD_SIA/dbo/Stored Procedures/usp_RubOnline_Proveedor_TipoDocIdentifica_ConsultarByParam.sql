﻿
-- =============================================
-- Author:		Fabian Valencia
-- Create date: 22/06/2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocIdentifica_ConsultarByParam] 
	@IdDocumento int,
	@Programa NVARCHAR(30) 
AS
BEGIN
	DECLARE @IdPrograma INT
	SET @IdPrograma = (SELECT DISTINCT IdPrograma  FROM SEG.Programa WHERE CodigoPrograma = @Programa)
	
	
	SELECT     Proveedor.TipoDocumentoPrograma.IdTipoDocumento,  
			   Proveedor.TipoDocumentoPrograma.MaxPermitidoKB, 
               Proveedor.TipoDocumentoPrograma.ExtensionesPermitidas, 
               Proveedor.TipoDocumento.CodigoTipoDocumento, 
               Proveedor.TipoDocumento.Descripcion, 
               Proveedor.TipoDocumento.Estado, 
               Proveedor.TipoDocumento.UsuarioCrea, 
               Proveedor.TipoDocumento.FechaCrea
	FROM       Proveedor.TipoDocumentoPrograma 
			   INNER JOIN
			  Proveedor.TipoDocumento ON 
			  Proveedor.TipoDocumentoPrograma.IdTipoDocumento = Proveedor.TipoDocumento.IdTipoDocumento
	WHERE     (Proveedor.TipoDocumentoPrograma.IdTipoDocumento = @IdDocumento) AND (Proveedor.TipoDocumentoPrograma.IdPrograma = @IdPrograma)
END

