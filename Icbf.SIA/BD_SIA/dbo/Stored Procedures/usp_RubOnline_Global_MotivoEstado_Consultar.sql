﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que consulta un(a) MotivoEstado
-- =============================================
create PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Consultar]
	@IdMotivoEstado INT
AS
BEGIN
 SELECT IdMotivoEstado, CodigoMotivoEstado, NombreMotivoEstado, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[MotivoEstado] WHERE  IdMotivoEstado = @IdMotivoEstado
END


