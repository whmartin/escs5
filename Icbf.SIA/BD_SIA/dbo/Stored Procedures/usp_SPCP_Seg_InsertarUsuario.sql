﻿
-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  08/10/2012
-- Description:	Procedimiento almacenado que guarda información de un usuario
-- Modificacion: 2013/05/26 - Bayron Lara - Se agregan 3 columnas a la tabla 
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 03/08/2015 se agregan los campos IdArea y IdRegional
-- =============================================

CREATE PROCEDURE [dbo].[usp_SPCP_Seg_InsertarUsuario]
		  @IdUsuario INT OUTPUT
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10)
		, @CorreoElectronico NVARCHAR(150)
		, @Estado BIT
		, @ProviderKey VARCHAR(125)
		, @UsuarioCreacion NVARCHAR(250)
		, @IdTipoUsuario	INT
		, @IdRegional INT
		, @IdArea INT
AS
BEGIN
	INSERT INTO SEG.Usuario(
		   [IdTipoDocumento]
           ,[NumeroDocumento]
		   ,[PrimerNombre]
           ,[SegundoNombre]
           ,[PrimerApellido]
           ,[SegundoApellido]
		   ,[TelefonoContacto]
           ,[CorreoElectronico]
           ,[Estado]
           ,[UsuarioCreacion]
           ,[FechaCreacion]
           ,[providerKey]
           ,IdTipoUsuario
		   ,[IdRegional]
		   ,[IdArea]
           )
	VALUES(
		   @IdTipoDocumento
           ,@NumeroDocumento
           ,@PrimerNombre
           ,@SegundoNombre
           ,@PrimerApellido
           ,@SegundoApellido
           ,@TelefonoContacto
           ,@CorreoElectronico
           ,@Estado
           ,@UsuarioCreacion
           ,GETDATE()
           ,@ProviderKey
           ,@IdTipoUsuario
		   ,@IdRegional
		   ,@IdArea
           )
	SELECT @IdUsuario = @@IDENTITY
END



