﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:44:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) Comuna
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Comuna_sConsultar]
	@IdMunicipio INT = NULL,@CodigoComuna NVARCHAR(256) = NULL,@NombreComuna NVARCHAR(256) = NULL
AS
BEGIN
 SELECT IdComuna
      , IdMunicipio
      , CodigoComuna
      , NombreComuna
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
  FROM [DIV].[Comuna] 
  WHERE IdMunicipio = CASE WHEN @IdMunicipio IS NULL THEN IdMunicipio ELSE @IdMunicipio END AND CodigoComuna = CASE WHEN @CodigoComuna IS NULL THEN CodigoComuna ELSE @CodigoComuna END AND NombreComuna = CASE WHEN @NombreComuna IS NULL THEN NombreComuna ELSE @NombreComuna END
END

