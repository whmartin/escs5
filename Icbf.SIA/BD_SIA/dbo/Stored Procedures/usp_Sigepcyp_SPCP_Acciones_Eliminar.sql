﻿
--- =================================================================
-- Author:		<Author,,Grupo Desarrollo SIGEPCYP>
-- Create date:  8/20/2015 8:24:06 PM
-- Description:	Procedimiento almacenado que elimina un(a) Acciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Acciones_Eliminar]
	@IdAcciones INT
AS
BEGIN
	DELETE SPCP.Acciones WHERE IdAcciones = @IdAcciones
END
