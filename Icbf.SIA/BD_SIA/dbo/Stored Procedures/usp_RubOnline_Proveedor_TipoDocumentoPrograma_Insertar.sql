﻿


-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Insertar]
@IdTipoDocumentoPrograma INT OUTPUT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR(50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT,  @ObligSectorPrivado INT, @ObligSectorPublico INT, @UsuarioCrea NVARCHAR (250),@ObligEntidadNacioanl BIT, @ObligEntidadExtrajera BIT, @ObligConsorcio BIT, @ObligUnionTemporal BIT

AS
BEGIN
INSERT INTO Proveedor.TipoDocumentoPrograma (IdTipoDocumento, IdPrograma, Estado, MaxPermitidoKB, ExtensionesPermitidas, ObligRupNoRenovado, ObligRupRenovado, ObligPersonaJuridica, ObligPersonaNatural, ObligSectorPrivado, ObligSectorPublico, UsuarioCrea, FechaCrea,ObligEntNacional,ObligEntExtranjera, ObligConsorcio, ObligUnionTemporal)
	VALUES (@IdTipoDocumento, @IdPrograma, 1, @MaxPermitidoKB, @ExtensionesPermitidas, @ObligRupNoRenovado, @ObligRupRenovado, @ObligPersonaJuridica, @ObligPersonaNatural, @ObligSectorPrivado, @ObligSectorPublico, @UsuarioCrea, GETDATE(),@ObligEntidadNacioanl,@ObligEntidadExtrajera,@ObligConsorcio,@ObligUnionTemporal)
SELECT
	@IdTipoDocumentoPrograma = SCOPE_IDENTITY()
END




