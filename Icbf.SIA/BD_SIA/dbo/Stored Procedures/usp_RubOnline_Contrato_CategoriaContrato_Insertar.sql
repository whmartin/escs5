﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_CategoriaContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_CategoriaContrato_Insertar]
		@IdCategoriaContrato INT OUTPUT, 	@NombreCategoriaContrato NVARCHAR(50),	@Descripcion NVARCHAR(100),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.CategoriaContrato(NombreCategoriaContrato, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreCategoriaContrato, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdCategoriaContrato = @@IDENTITY 		
END

