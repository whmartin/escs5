﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que guarda un nuevo TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Insertar]
		@IdTituloObtenido INT OUTPUT, 	@CodigoTituloObtenido NVARCHAR(128),	@NombreTituloObtenido NVARCHAR(128),	@Estado NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.TituloObtenido(CodigoTituloObtenido, NombreTituloObtenido, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTituloObtenido, @NombreTituloObtenido, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTituloObtenido = @@IDENTITY 		
END

