﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  5/23/2013 5:33:48 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Tercero
-- Modificación: 24-FEB-2014
-- Desarrolldaor Juan Carlos Valverde Sámano
-- Descripción: En base al control de cambios CO_024, se ha agrega lo sigueinte.
--				Compara si los datos que vienen del formulario de registro de terceros
--				Son los mismos al macenados en SEG.Usuario (nombres y apellidos ó razón social) 
--				de acuerdo al tipo de Persona, y en caso de ser diferentes actualiza la tabla SEG.Usuario
--				con esta nueva información.
-- Modificación: 03-ABRIL-2014
-- Desarrolldaor Juan Carlos Valverde Sámano
-- Descripción: Se agregó un validación mas que dice: Si se trata de un Usuario Externo
-- Entonces pasa a actualizar la información en la entidad de Usuario, en caso de ser un
-- Usuario Interno o Administrador, solo registra el Tercero y termina.
-- Modificacion: 28/08/2014 Juan Carlos Valverde Sámano
-- Descripción: Se agregó la parte de que si es Consorcio o Union Temporal se actualice la Razón Social en
-- La entidad SEG.Usuario.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_Whit_Temporal_Insertar]

	@IdTercero INT OUTPUT, 
	@IdDListaTipoDocumento INT, 
	@IdTipoPersona INT=NULL,
	@NumeroIdentificacion NVARCHAR (255), 
	@PrimerNombre NVARCHAR (255), 
	@SegundoNombre NVARCHAR (255), 
	@PrimerApellido NVARCHAR (255), 
	@SegundoApellido NVARCHAR (255), 
	@Email NVARCHAR (255), 
	@Sexo NVARCHAR (1), 
	@ProviderUserKey UNIQUEIDENTIFIER=NULL,
	@DIGITOVERIFICACION INT=NULL,
	@IDESTADOTERCERO INT =NULL,
	@RAZONSOCIAL NVARCHAR (250)=NULL,
	@FechaExpedicionId DATETIME =null, 
	@FechaNacimiento DATETIME =null, 
	@UsuarioCrea NVARCHAR (250),
	@IdTemporal VARCHAR(20)= NULL,
	@CreadoPorInterno BIT = NULL
	
AS
BEGIN
IF NOT EXISTS(SELECT IDTERCERO FROM Oferente.Tercero WHERE
IDTIPODOCIDENTIFICA=@IdDListaTipoDocumento AND NUMEROIDENTIFICACION=@NumeroIdentificacion)
BEGIN
	INSERT INTO Oferente.Tercero 
			 ([IDTIPODOCIDENTIFICA]
			 ,[IDESTADOTERCERO]
			 ,[IdTipoPersona]
			 ,[ProviderUserKey]
			 ,[NUMEROIDENTIFICACION]
			 ,[DIGITOVERIFICACION]
			 ,[CORREOELECTRONICO]
			 ,[PRIMERNOMBRE]
			 ,[SEGUNDONOMBRE]
			 ,[PRIMERAPELLIDO]
			 ,[SEGUNDOAPELLIDO]
			 ,[RAZONSOCIAL]
			 ,[FECHAEXPEDICIONID]
			 ,[FECHANACIMIENTO]
			 ,[SEXO]
			 ,UsuarioCrea
			 ,FechaCrea
			 ,CreadoPorInterno)
		VALUES (@IdDListaTipoDocumento,
				@IDESTADOTERCERO,
				@IdTipoPersona,
				@ProviderUserKey,
				@NumeroIdentificacion, 
				@DIGITOVERIFICACION,
				@Email, 
				@PrimerNombre, 
				@SegundoNombre, 
				@PrimerApellido, 
				@SegundoApellido, 
				@RAZONSOCIAL,
				@FechaExpedicionId, 
				@FechaNacimiento,
				@Sexo, 
				@UsuarioCrea, 
				GETDATE(),
				@CreadoPorInterno)

	SELECT @IdTercero=@@IDENTITY

	UPDATE [Proveedor].[DocAdjuntoTercero]  
		set IdTercero = @IdTercero
		where IdTemporal = @IdTemporal


	IF(@CreadoPorInterno=0)
	BEGIN
		--VERIFICAR SI SE TRATA DE UN USUARIO EXTERNO (REGISTRADO DESDE PROVEEDORES)---- SI ES ASI PARA PASAR A VALIDAR A ACTUALIZAR SU INFORMACIÓN
		--EN LA ENTIDAD USUARIO, EN CASO CONTRARIO NO REALIZA LA ACTUALIZACIÓN.
		IF((SELECT CorreoElectronico FROM SEG.Usuario
		WHERE providerKey=@ProviderUserKey) = @Email)
		BEGIN
			--------------VERIFICAR DATOS CON SEG.Usuario Y SI DIFIEREN ....ACTUALIZARLOS--------------------------------
			IF (@IdTipoPersona=1)
			BEGIN
				DECLARE @primer_Nombre NVARCHAR(150),
				 @segundo_Nombre NVARCHAR(150),
				 @primer_Apellido NVARCHAR(150),
				 @segundo_Apellido NVARCHAR(150)
				 
				 SELECT @primer_Nombre=ISNULL([PrimerNombre],''),
				 @segundo_Nombre=ISNULL([SegundoNombre],''),
				 @primer_Apellido=ISNULL([PrimerApellido],''),
				 @segundo_Apellido=ISNULL([SegundoApellido],'')
				 FROM SEG.Usuario
				 WHERE providerKey=@ProviderUserKey
				 
				 IF(@PrimerNombre!=@primer_Nombre)
				 BEGIN
					 UPDATE SEG.Usuario SET PrimerNombre=@PrimerNombre
					 WHERE providerKey=@ProviderUserKey
				 END
				 
				 IF(@SegundoNombre!=@segundo_Nombre)
				 BEGIN
					 UPDATE SEG.Usuario SET SegundoNombre=@SegundoNombre
					 WHERE providerKey=@ProviderUserKey
				 END
				 
				 IF(@PrimerApellido!= @primer_Apellido)
				 BEGIN
					 UPDATE SEG.Usuario SET PrimerApellido=@PrimerApellido
					 WHERE providerKey=@ProviderUserKey
				 END
				 
				 IF(@SegundoApellido!=@segundo_Apellido)
				 BEGIN
					 UPDATE SEG.Usuario SET SegundoApellido=@SegundoApellido
					 WHERE providerKey=@ProviderUserKey
				 END
			END 
				ELSE IF (@IdTipoPersona=2 Or @IdTipoPersona=3 Or @IdTipoPersona=4)
				BEGIN
					DECLARE @razon_Social NVARCHAR(256)
					SELECT @razon_Social=ISNULL([RazonSocial],'')
					FROM SEG.Usuario
					WHERE providerKey=@ProviderUserKey
					
					IF(@RAZONSOCIAL!=@razon_Social)
					 BEGIN
						 UPDATE SEG.Usuario SET RazonSocial=@RAZONSOCIAL
						 WHERE providerKey=@ProviderUserKey
					 END
				END
		END
			------------------------------------------------
	END
		
		SELECT @IdTercero


	RETURN @@IDENTITY
END
ELSE
BEGIN
	SET @IdTercero=0;
	DECLARE @TipoDoc NVARCHAR(10)=(
	SELECT CodDocumento FROM Global.TiposDocumentos		
	WHERE IdTipoDocumento=@IdDListaTipoDocumento)
	
	DECLARE @ErrorMessage NVARCHAR(600)='Ya hay un Tercero registrado con '+ @TipoDoc +'  ' +@NumeroIdentificacion;
	RAISERROR (@ErrorMessage, -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
    			
END
END





