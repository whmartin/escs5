﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 3:08:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo NivelOrganizacional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_NivelOrganizacional_Insertar]
		@IdNivelOrganizacional INT OUTPUT, 	@CodigoNivelOrganizacional NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.NivelOrganizacional(CodigoNivelOrganizacional, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoNivelOrganizacional, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdNivelOrganizacional = SCOPE_IDENTITY() 		
END


