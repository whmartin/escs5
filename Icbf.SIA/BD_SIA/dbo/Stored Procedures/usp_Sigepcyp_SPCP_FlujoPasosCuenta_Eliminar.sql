﻿
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_FlujoPasosCuenta_Eliminar]
	@IdFlujoPasoCuenta INT
AS
BEGIN
	DELETE SPCP.FlujoPasosCuenta WHERE IdFlujoPasoCuenta = @IdFlujoPasoCuenta
END
