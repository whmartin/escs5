﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que consulta un(a) Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Consultar]
	@IdVigencia INT
AS
BEGIN
 SELECT IdVigencia, AcnoVigencia, Activo, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[Vigencia] WHERE  IdVigencia = @IdVigencia
END

