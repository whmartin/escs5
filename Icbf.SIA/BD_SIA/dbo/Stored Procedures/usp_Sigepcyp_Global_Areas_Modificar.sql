﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/16/2015 11:22:03 AM
-- Description:	Procedimiento almacenado que actualiza un(a) Areas
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_Areas_Modificar]
		@IdArea INT,	@IdRegional INT,	@NombreArea NVARCHAR(50),	@CodArea INT,	@EstadoArea INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.Areas SET IdRegional = @IdRegional, NombreArea = @NombreArea, CodArea = @CodArea, EstadoArea = @EstadoArea, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdArea = @IdArea
END
