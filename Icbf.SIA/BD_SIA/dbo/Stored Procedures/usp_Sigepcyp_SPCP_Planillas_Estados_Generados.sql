﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:36 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Planillas_Estados_Generados]
	@GeneradoObligaciones bit = NULL,
	@GeneradoOPP bit= NULL,
	@GeneradoCxP bit= NULL
AS
BEGIN
	
	SELECT pd.IdPlanilla
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	INNER JOIN SPCP.PlanDePagos pp
	on cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	INNER JOIN SPCP.PlanillaDetalle pd
	ON cc.IdCuenta = pd.IdCuenta
	WHERE cc.GeneradoObligaciones = isnull(@GeneradoObligaciones,cc.GeneradoObligaciones)
	and cc.GeneradoOPP = isnull(@GeneradoOPP,cc.GeneradoOPP)
	and cc.GeneradoCxP = isnull(@GeneradoCxP,cc.GeneradoCxP)
	

	
END





	