﻿

CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TerceroProviderUserKey_Consultar]
@ProviderUserKey UNIQUEIDENTIFIER
AS
BEGIN
SELECT [IDTERCERO]
      ,[IDTIPODOCIDENTIFICA]
      ,[IDESTADOTERCERO]
      ,[IdTipoPersona]
      ,[NUMEROIDENTIFICACION]
      ,[DIGITOVERIFICACION]
      ,[CORREOELECTRONICO]
      ,[PRIMERNOMBRE]
      ,[SEGUNDONOMBRE]
      ,[PRIMERAPELLIDO]
      ,[SEGUNDOAPELLIDO]
      ,[RAZONSOCIAL]
      ,[FECHAEXPEDICIONID]
      ,[FECHANACIMIENTO]
      ,[SEXO]
      ,[ProviderUserKey]
      ,[FECHACREA]
      ,[USUARIOCREA]
      ,[FECHAMODIFICA]
      ,[USUARIOMODIFICA]
      ,[ESFUNDACION]
FROM Tercero.TERCERO
WHERE ProviderUserKey = @ProviderUserKey
     
END

