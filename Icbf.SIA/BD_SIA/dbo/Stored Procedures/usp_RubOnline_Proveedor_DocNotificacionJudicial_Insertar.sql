﻿
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que guarda un nuevo DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Insertar]
		@IdDocNotJudicial INT OUTPUT , 	
		@IdNotJudicial INT , 	
		@IdDocumento INT , 	
		@Descripcion NVARCHAR(128),	
		@LinkDocumento NVARCHAR(256),	
		@Anno INT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.DocNotificacionJudicial(IdNotJudicial,IdDocumento, Descripcion, LinkDocumento, Anno, UsuarioCrea, FechaCrea)
					  VALUES(@IdNotJudicial,@IdDocumento, @Descripcion, @LinkDocumento, @Anno, @UsuarioCrea, GETDATE())
	SELECT @IdDocNotJudicial = SCOPE_IDENTITY() 		
	RETURN SCOPE_IDENTITY()
END


