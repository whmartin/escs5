﻿-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:20 PM
-- Description:	Procedimiento almacenado que elimina un(a) ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ExperienciaCodUNSPSCEntidad_Eliminar]
	@IdExpCOD INT
AS
BEGIN
	DELETE Proveedor.ExperienciaCodUNSPSCEntidad WHERE IdExpCOD = @IdExpCOD
END

