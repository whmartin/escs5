﻿
-- =============================================
-- Author:		ICBF\brayher.gomez
-- Create date:  13/03/2015 9:36:01 p. m.
-- Description:	Procedimiento almacenado que consulta un(a) DatosAdministracion
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdministracions_Consultar]
	@Codpci NVARCHAR(255) = NULL,
	@CodCompromiso INT = NULL,
	@Fecha DATETIME = NULL,
	@Codcdp INT = NULL,
	@Vigencia INT = NULL,
	@Descripcion NVARCHAR(255) = NULL,
	@ValorInicial NVARCHAR(40) = NULL,
	@ValorActual NVARCHAR(40) = NULL,
	@Saldo NVARCHAR(40) = NULL,
	@NumCtaBancaria NVARCHAR(255) = NULL,
	@NomEntidadFinanciera NVARCHAR(255) = NULL,
	@NomTipoCta NVARCHAR(255) = NULL,
	@EstadoCta NVARCHAR(255) = NULL,
	@FechaDocSoporte DATETIME = NULL,
	@CodTipoDocSoporte INT = NULL,
	@NomTipoDocSoporte NVARCHAR(255) = NULL,
	@NumDocSoporte NVARCHAR(40) = NULL,
	@TipoDocumento INT = NULL,
	@NumDocIdentidad NVARCHAR(20) = NULL,
	@NomTercero NVARCHAR(255) = NULL,
	@IdArea INT = NULL,
	@InicioEjecucionContrato DATETIME = NULL,
	@FinalEjecucionContrato DATETIME = NULL,
	@VigenciaDatosAdministracion INT=NULL
AS
BEGIN


Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@Codpci NVARCHAR(255),@CodCompromiso INT,@Fecha DATETIME,@Codcdp INT,@Vigencia INT,@Descripcion NVARCHAR(255),@ValorInicial NVARCHAR(40),@ValorActual NVARCHAR(40),@Saldo NVARCHAR(40),@NumCtaBancaria NVARCHAR(255),@NomEntidadFinanciera NVARCHAR(255),@NomTipoCta NVARCHAR(255),@EstadoCta NVARCHAR(255),@FechaDocSoporte DATETIME,@CodTipoDocSoporte INT,@NomTipoDocSoporte NVARCHAR(255),@NumDocSoporte NVARCHAR(40),@TipoDocumento INT,@NumDocIdentidad NVARCHAR(20),@NomTercero NVARCHAR(255),@IdArea INT,@InicioEjecucionContrato DATETIME,@FinalEjecucionContrato DATETIME, @VigenciaDatosAdministracion INT'
Set @SqlExec = '
 SELECT IdDatosAdministracion, Codpci, CodCompromiso, Fecha, Codcdp, Vigencia, Descripcion, ValorInicial, ValorActual, Saldo, NumCtaBancaria, NomEntidadFinanciera, NomTipoCta, EstadoCta, FechaDocSoporte, CodTipoDocSoporte, NomTipoDocSoporte, NumDocSoporte, TipoDocumento, NumDocIdentidad, NomTercero, IdArea, InicioEjecucionContrato, FinalEjecucionContrato, VigenciaDatosAdministracion,
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[DatosAdministracion] WHERE 1=1 '
 If(@Codpci Is Not Null) Set @SqlExec = @SqlExec + ' And Codpci = @Codpci' 
 If(@CodCompromiso Is Not Null) Set @SqlExec = @SqlExec + ' And CodCompromiso = @CodCompromiso' 
 If(@Fecha Is Not Null) Set @SqlExec = @SqlExec + ' And Fecha = @Fecha' 
 If(@Codcdp Is Not Null) Set @SqlExec = @SqlExec + ' And Codcdp = @Codcdp' 
 If(@Vigencia Is Not Null) Set @SqlExec = @SqlExec + ' And Vigencia = @Vigencia' 
 If(@Descripcion Is Not Null) Set @SqlExec = @SqlExec + ' And Descripcion = @Descripcion' 
 If(@ValorInicial Is Not Null) Set @SqlExec = @SqlExec + ' And ValorInicial = @ValorInicial' 
 If(@ValorActual Is Not Null) Set @SqlExec = @SqlExec + ' And ValorActual = @ValorActual' 
 If(@Saldo Is Not Null) Set @SqlExec = @SqlExec + ' And Saldo = @Saldo' 
 If(@NumCtaBancaria Is Not Null) Set @SqlExec = @SqlExec + ' And NumCtaBancaria = @NumCtaBancaria' 
 If(@NomEntidadFinanciera Is Not Null) Set @SqlExec = @SqlExec + ' And NomEntidadFinanciera = @NomEntidadFinanciera' 
 If(@NomTipoCta Is Not Null) Set @SqlExec = @SqlExec + ' And NomTipoCta = @NomTipoCta' 
 If(@EstadoCta Is Not Null) Set @SqlExec = @SqlExec + ' And EstadoCta = @EstadoCta' 
 If(@FechaDocSoporte Is Not Null) Set @SqlExec = @SqlExec + ' And FechaDocSoporte = @FechaDocSoporte' 
 If(@CodTipoDocSoporte Is Not Null) Set @SqlExec = @SqlExec + ' And CodTipoDocSoporte = @CodTipoDocSoporte' 
 If(@NomTipoDocSoporte Is Not Null) Set @SqlExec = @SqlExec + ' And NomTipoDocSoporte = @NomTipoDocSoporte' 
 If(@NumDocSoporte Is Not Null) Set @SqlExec = @SqlExec + ' And NumDocSoporte = @NumDocSoporte' 
 If(@TipoDocumento Is Not Null) Set @SqlExec = @SqlExec + ' And TipoDocumento = @TipoDocumento' 
 If(@NumDocIdentidad Is Not Null) Set @SqlExec = @SqlExec + ' And NumDocIdentidad = @NumDocIdentidad' 
 If(@NomTercero Is Not Null) Set @SqlExec = @SqlExec + ' And NomTercero = @NomTercero' 
 If(@IdArea Is Not Null) Set @SqlExec = @SqlExec + ' And IdArea = @IdArea' 
 If(@InicioEjecucionContrato Is Not Null) Set @SqlExec = @SqlExec + ' And InicioEjecucionContrato = @InicioEjecucionContrato' 
 If(@FinalEjecucionContrato Is Not Null) Set @SqlExec = @SqlExec + ' And FinalEjecucionContrato = @FinalEjecucionContrato'
 If(@VigenciaDatosAdministracion Is Not Null) Set @SqlExec = @SqlExec + ' And VigenciaDatosAdministracion = @VigenciaDatosAdministracion' 
Exec sp_executesql  @SqlExec, @SqlParametros,@Codpci,@CodCompromiso,@Fecha,@Codcdp,@Vigencia,@Descripcion,@ValorInicial,@ValorActual,@Saldo,@NumCtaBancaria,@NomEntidadFinanciera,@NomTipoCta,@EstadoCta,@FechaDocSoporte,@CodTipoDocSoporte,@NomTipoDocSoporte,@NumDocSoporte,@TipoDocumento,@NumDocIdentidad,@NomTercero,@IdArea,@InicioEjecucionContrato,@FinalEjecucionContrato,@VigenciaDatosAdministracion
END
