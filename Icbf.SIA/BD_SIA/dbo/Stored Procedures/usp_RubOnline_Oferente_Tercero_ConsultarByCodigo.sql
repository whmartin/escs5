﻿

-- =============================================
-- Author:		Fabian Valencia
-- Create date: 25/06/2013
-- Description:	Obtiene el estado a través de su código
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ConsultarByCodigo]
	@codigoTercero NVARCHAR (5)
AS
BEGIN
	SELECT
		IdEstadoTercero,
		CodigoEstadotercero,
		DescripcionEstado,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM oferente.EstadoTercero
	WHERE (CodigoEstadotercero = @codigoTercero)
END

