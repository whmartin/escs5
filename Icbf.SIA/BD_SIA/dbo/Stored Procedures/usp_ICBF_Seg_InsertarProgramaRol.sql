﻿-- =============================================
-- Author:		Oscar Javier Sosa Parada
-- Create date:  13/11/2012
-- Description:	Procedimiento almacenado que guarda programa permitido a un rol
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_InsertarProgramaRol]
		@pIdRol INT
		, @pIdPrograma INT
		, @UsuarioCreacion NVARCHAR(250)
AS
BEGIN
	INSERT INTO SEG.ProgramaRol([IdRol], [IdPrograma], [UsuarioCreacion], FechaCreacion)
					  VALUES(@pIdRol, @pIdPrograma, @UsuarioCreacion, GETDATE())
END
