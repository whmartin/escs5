﻿

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Modificar]
		@IdObjetoContratoContractual INT,	
		@ObjetoContractual nvarchar(MAX),	
		@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM Contrato.ObjetoContrato WHERE ObjetoContractual = @ObjetoContractual AND IdObjetoContratoContractual NOT IN (@IdObjetoContratoContractual))
	BEGIN
		UPDATE Contrato.ObjetoContrato SET ObjetoContractual = @ObjetoContractual, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObjetoContratoContractual = @IdObjetoContratoContractual
	END
	ELSE
	BEGIN
		RAISERROR ('Registro duplicado, verifique por favor.', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	END
END

