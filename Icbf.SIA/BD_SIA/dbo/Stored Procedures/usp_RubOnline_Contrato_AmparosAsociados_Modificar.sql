﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociados_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociados_Modificar]
		@IdAmparo INT,	@IdGarantia INT,	@IdTipoAmparo INT,	@FechaVigenciaDesde DATETIME,	@VigenciaHasta DATETIME,	@ValorParaCalculoAsegurado NUMERIC(18,3),	@IdTipoCalculo INT,	@ValorAsegurado NUMERIC(18,3), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.AmparosAsociados SET IdGarantia = @IdGarantia, IdTipoAmparo = @IdTipoAmparo, FechaVigenciaDesde = @FechaVigenciaDesde, VigenciaHasta = @VigenciaHasta, ValorParaCalculoAsegurado = @ValorParaCalculoAsegurado, IdTipoCalculo = @IdTipoCalculo, ValorAsegurado = @ValorAsegurado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAmparo = @IdAmparo
END

