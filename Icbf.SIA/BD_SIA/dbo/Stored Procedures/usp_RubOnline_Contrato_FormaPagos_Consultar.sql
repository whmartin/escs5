﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPagos_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPagos_Consultar]

	@NombreFormaPago NVARCHAR (50) = NULL,
	@Descripcion NVARCHAR (128) = NULL,
	@Estado bit = null

AS
BEGIN

	SELECT
		IdFormapago,
		NombreFormaPago,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[FormaPago]
	WHERE NombreFormaPago  LIKE '%' + 
		CASE
			WHEN @NombreFormaPago IS NULL THEN NombreFormaPago ELSE @NombreFormaPago
		END + '%'
	AND Descripcion  LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreFormaPago

END


