﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacions_Consultar
Modificado por Jonathan Acosta
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacions_Consultar]

@NombreRegimenContratacion NVARCHAR (128) = NULL,
@Descripcion NVARCHAR (128) = NULL,
@Estado bit = null

AS
BEGIN

	SELECT
		IdRegimenContratacion,
		NombreRegimenContratacion,
		Descripcion,
		Estado,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[RegimenContratacion]
	WHERE NombreRegimenContratacion LIKE '%' + 
		CASE
			WHEN @NombreRegimenContratacion IS NULL THEN NombreRegimenContratacion ELSE @NombreRegimenContratacion
		END + '%'
	AND Descripcion LIKE '%' + 
		CASE
			WHEN @Descripcion IS NULL THEN Descripcion ELSE @Descripcion
		END + '%'
	AND (Estado = @Estado OR @Estado IS NULL)
	ORDER BY NombreRegimenContratacion

END


