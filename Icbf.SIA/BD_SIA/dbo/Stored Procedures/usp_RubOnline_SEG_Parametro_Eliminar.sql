﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que elimina un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Eliminar]
	@IdParametro INT
AS
BEGIN
	DELETE SEG.Parametro WHERE IdParametro = @IdParametro
END

