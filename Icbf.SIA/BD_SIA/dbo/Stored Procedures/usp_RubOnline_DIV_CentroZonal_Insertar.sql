﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/26/2012 2:57:50 AM
-- Description:	Procedimiento almacenado que guarda un nuevo CentroZonal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_CentroZonal_Insertar]
		@IdCentroZonal INT OUTPUT
		, 	@IdMunicipio INT
		,	@IdRegional INT
		,	@CodigoMunicipio NVARCHAR(5)
		,	@CodigoCentroZonal NVARCHAR(4)
		,	@NombreCentroZonal NVARCHAR(45)
		,	@Direccion NVARCHAR(100)
		,	@Telefonos NVARCHAR(100)
		,	@Estado NVARCHAR(1)
		, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO DIV.CentroZonal(IdMunicipio, IdRegional, CodigoMunicipio, CodigoCentroZonal, NombreCentroZonal, Direccion, Telefonos, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdMunicipio, @IdRegional, @CodigoMunicipio, @CodigoCentroZonal, @NombreCentroZonal, @Direccion, @Telefonos, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdCentroZonal = @@IDENTITY 		
END

