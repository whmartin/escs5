﻿
--- =================================================================
-- Author:		<Author,,Grupo Desarrollo SIGEPCYP>
-- Create date:  8/20/2015 8:24:06 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Acciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Acciones_Modificar]
		@IdAcciones INT,	@Descripcion NVARCHAR(160),	@Estado INT,	@CodAccion NVARCHAR(10), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.Acciones SET Descripcion = @Descripcion, Estado = @Estado, CodAccion = @CodAccion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdAcciones = @IdAcciones
END
