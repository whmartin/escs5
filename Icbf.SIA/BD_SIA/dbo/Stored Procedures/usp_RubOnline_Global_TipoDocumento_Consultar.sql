﻿-- =============================================
-- Author:        ICBF\Tommy.Puccini
-- Create date:  12/12/2012 9:48:16
-- Description:   Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TipoDocumento_Consultar]
      @IdTipoDocumento INT
AS
BEGIN
SELECT [IdTipoDocumento]
      ,[CodDocumento]
      ,[NomTipoDocumento]
  FROM [Global].[TiposDocumentos]
WHERE  IdTipoDocumento = @IdTipoDocumento
END

