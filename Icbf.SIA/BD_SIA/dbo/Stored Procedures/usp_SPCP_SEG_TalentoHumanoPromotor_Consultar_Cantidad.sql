﻿

-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta los promotores por contrato

-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_SEG_TalentoHumanoPromotor_Consultar_Cantidad]
	      @IdContrato INT,
	      @CantidadMax INT
AS
BEGIN
	SELECT     PAE.TalentoHumano.IdTalentoHumano, 
			   PAE.TalentoHumano.PrimerNombre, 
			   PAE.TalentoHumano.SegundoNombre, 
			   PAE.TalentoHumano.PrimerApellido, 
			   PAE.TalentoHumano.SegundoApellido,
			   PAE.TalentoHumano.Identificacion
	FROM GCB.ContratoTalentoHumano INNER JOIN PAE.TalentoHumano 
								   ON PAE.TalentoHumano.IdTalentoHumano = GCB.ContratoTalentoHumano.IdTalentoHumano
									INNER JOIN ECO.Cargo
								   ON ECO.Cargo.IdCargo = GCB.ContratoTalentoHumano.IdCargo
	WHERE     (ECO.Cargo.NombreCargo = 'PROMOTOR DE DERECHOS') 
	AND (GCB.ContratoTalentoHumano.IdContrato = @IdContrato)
	and   PAE.TalentoHumano.IdTalentoHumano NOT IN (SELECT IdPromotor
														from GCB.GruposContrato
														group by IdPromotor
														HAVING COUNT(IdPromotor) >= @CantidadMax )

END



