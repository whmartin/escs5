﻿-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 02:36:45 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) Programa
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_Seg_EliminarPrograma]
	@IdPrograma INT
AS
BEGIN
	DELETE SEG.Programa WHERE IdPrograma = @IdPrograma
END
