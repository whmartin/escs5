﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/19/2013 11:00:36 PM
-- Description:	Procedimiento almacenado que guarda un nuevo TipoCargoEntidad
-- Modificado Por: Juan Carlos Valverde Sámano
-- Fecha: 12/SEPT/2014
-- Descripción se agregó la validación para no permitir ingresar registros con el mismo
-- Código ni con la misma Descripción.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCargoEntidad_Insertar]
		@IdTipoCargoEntidad INT OUTPUT, @CodigoTipoCargoEntidad NVARCHAR(10),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

IF EXISTS (SELECT IdTipoCargoEntidad FROM PROVEEDOR.TipoCargoEntidad WHERE CodigoTipoCargoEntidad=@CodigoTipoCargoEntidad)
BEGIN
 RAISERROR ('CodigoUnico', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
END
ELSE IF EXISTS (SELECT IdTipoCargoEntidad FROM PROVEEDOR.TipoCargoEntidad WHERE Descripcion=@Descripcion)
BEGIN
	RAISERROR ('NombreUnico', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
END
ELSE
BEGIN
	INSERT INTO Proveedor.TipoCargoEntidad(CodigoTipoCargoEntidad, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoTipoCargoEntidad,@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdTipoCargoEntidad = SCOPE_IDENTITY() 		
END
END




