﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/18/2015 1:47:11 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Pasos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Pasos_Insertar]
		@IdPaso INT OUTPUT, 	@Paso NVARCHAR(50),	@Estado INT,	@Financiera INT,	@IdAccion INT,	@AccionEstado INT,	@Obligatorio INT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN

	IF EXISTS(SELECT * FROM SPCP.Pasos WHERE Paso = @Paso )
	BEGIN
		RAISERROR ('Paso ya existe, por favor verifique', -- Message text.
					16, -- Severity.
					1, -- State.
					2601);
	    RETURN
	END

	INSERT INTO SPCP.Pasos(Paso, Estado, Financiera, IdAccion, AccionEstado, Obligatorio, UsuarioCrea, FechaCrea)
					  VALUES(@Paso, @Estado, @Financiera, @IdAccion, @AccionEstado, @Obligatorio, @UsuarioCrea, GETDATE())
	SELECT @IdPaso = @@IDENTITY 		
END
