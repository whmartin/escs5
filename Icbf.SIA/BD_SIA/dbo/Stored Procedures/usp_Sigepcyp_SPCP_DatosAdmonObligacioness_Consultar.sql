﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/14/2015 12:03:54 PM
-- Description:	Procedimiento almacenado que consulta un(a) DatosAdmonObligaciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdmonObligacioness_Consultar]
	
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = ''
Set @SqlExec = '
 SELECT IdDatosAdmonObligaciones, IdDatosAdministracion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[DatosAdmonObligaciones] WHERE 1=1 '
Exec sp_executesql  @SqlExec, @SqlParametros
END
