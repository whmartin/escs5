﻿

--
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  /605/2013 4:49:27 PM
-- Description:	Procedimiento almacenado que consulta Departamentos
-- =============================================
	CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Experiencia_ConsultarDepto]		
	AS
	BEGIN
		SELECT     IdDepartamento, NombreDepartamento
		FROM         DIV.Departamento
		ORDER BY NombreDepartamento DESC;
	END

