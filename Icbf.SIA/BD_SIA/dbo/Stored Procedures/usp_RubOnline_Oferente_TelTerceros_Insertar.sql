﻿

-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/4/2013 11:29:17 AM
-- Description:	Procedimiento almacenado que guarda un nuevo TelTerceros
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_Insertar]

	@IdTelTercero INT OUTPUT,
	@IdTercero INT,
	@IndicativoTelefono INT = NULL,
	@NumeroTelefono INT = NULL,
	@ExtensionTelefono BIGINT = NULL,
	@Movil BIGINT = NULL,
	@IndicativoFax INT = NULL,
	@NumeroFax INT = NULL,
	@CorreoSede NVARCHAR(128) = NULL,
	@UsuarioCrea NVARCHAR (250)

AS
BEGIN

	INSERT INTO Tercero.TelTerceros 
		(IdTercero, 
		IndicativoTelefono, 
		NumeroTelefono, 
		ExtensionTelefono, 
		Movil, 
		IndicativoFax, 
		NumeroFax, 
		UsuarioCrea, 
		FechaCrea,
		CorreoSede)
	VALUES 
		(@IdTercero, 
		@IndicativoTelefono, 
		@NumeroTelefono, 
		@ExtensionTelefono, 
		@Movil, 
		@IndicativoFax, 
		@NumeroFax, 
		@UsuarioCrea, 
		GETDATE(),
		@CorreoSede)
	
SELECT
	@IdTelTercero = @@IDENTITY
END



