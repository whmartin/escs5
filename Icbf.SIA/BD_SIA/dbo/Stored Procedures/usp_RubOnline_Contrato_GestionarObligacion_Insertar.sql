﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Insertar]
		@IdGestionObligacion INT OUTPUT, 	@IdGestionClausula INT,	@NombreObligacion NVARCHAR(10),	@IdTipoObligacion INT,	@Orden NVARCHAR(128),	@DescripcionObligacion NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.GestionarObligacion(IdGestionClausula, NombreObligacion, IdTipoObligacion, Orden, DescripcionObligacion, UsuarioCrea, FechaCrea)
					  VALUES(@IdGestionClausula, @NombreObligacion, @IdTipoObligacion, @Orden, @DescripcionObligacion, @UsuarioCrea, GETDATE())
	SELECT @IdGestionObligacion = @@IDENTITY 		
END
