﻿


-- =============================================  
-- Author:  Jorge Vizcaino  
-- Create date:  7/30/2015 3:21:49 PM  
-- Description: Procedimiento almacenado que consulta el proceso de la cuenta de cobro   
-- =============================================  
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_GestionPagos_Consultars]  
 @Rol nvarchar(80),  
 @IdRegional INT,  
 @IdArea INT,  
 @IdPlanilla INT,  
 @IdTipoDoc int = NULL,  
 @NumeroDoc nvarchar(80) = NULL  
AS  
BEGIN  
   
 Declare @IdPasoAsignado int,  
   @AprobacionPAC bit,  
   @IdRol int  
  
  
 Select @IdRol = IdRol  
 from SEG.Rol  
 where Nombre = ltrim(rtrim(@Rol))  
  
  
 If(Not Exists(Select 1 From SEG.Rol Where IdRol = @IdRol and IdPaso IS NOT NULL))  
 BEGIN  
   RAISERROR ('El Rol asignado no tiene un paso configurado', 16, 1)        
 END  
  
 Select @IdPasoAsignado = IdPaso,@AprobacionPAC = AprobacionPAC  
 From SEG.Rol  
 WHERE IdRol = @IdRol  
  
 Select pp.Vigencia as AcnoVigencia  
   ,da.CodTipoDocSoporte as tipoDocFuente  
   ,da.FechaDocSoporte as fechaDocFuente  
   ,da.NumDocSoporte as NumeroDocFuente  
   ,da.NumDocIdentidad  
   ,da.NomTercero  
   ,pp.CodCompromiso as numeroRP  
   ,pp.NumeroDePago as NumeroPago  
   ,tc.TipoCuenta  
   ,cc.UsuarioCrea  
   ,cc.FechaCrea  
   ,cc.UsuarioModifica  
   ,cc.FechaModifica  
   ,cc.IdCuenta  
   ,cc.Devuelta
 FROM SPCP.CuentasDeCobro cc  
 INNER JOIN SPCP.FlujoPasosCuenta fp   
 ON cc.IdEstadoPaso = fp.IdFlujoPasoCuenta  
 INNER JOIN SPCP.Pasos p  
 ON fp.IdPaso = p.IdPaso  
 INNER JOIN SPCP.DatosAdministracion da  
 ON cc.IdDatosAdministracion = da.IdDatosAdministracion  
 --INNER JOIN Global.Vigencia v  
 --on da.Vigencia = v.IdVigencia  
 INNER JOIN SPCP.Presupuestos rp   
 ON da.IdDatosAdministracion = rp.IdDatosAdministracion  
 INNER JOIN SPCP.TiposCuenta tc  
 ON fp.IdTipoCuenta = tc.IdTipoCuenta  
 INNER JOIN Global.Areas a   
 ON da.IdArea = a.IdArea  
 inner join SPCP.PlanillaDetalle pd  
 on cc.IdCuenta = pd.IdCuenta  
 INNER JOIN SPCP.PlanDePagos pp  
 ON cc.IdPlanDePagos = pp.IdPlanDePagos  
 WHERE a.IdRegional = isnull(@IdRegional ,a.IdRegional)  
 --AND a.IdArea = isnull(@IdArea,a.IdArea)  
 AND p.IdPaso = @IdPasoAsignado  
 and pd.IdPlanilla = ISNULL(@IdPlanilla,pd.IdPlanilla)  
 AND da.TipoDocumento = ISNULL(@IdTipoDoc,da.TipoDocumento)  
 AND da.NumDocIdentidad = ISNULL(@NumeroDoc,da.NumDocIdentidad)  
 --ORDER BY v.AcnoVigencia ASC  
   
   
END  
  
  
  
    
