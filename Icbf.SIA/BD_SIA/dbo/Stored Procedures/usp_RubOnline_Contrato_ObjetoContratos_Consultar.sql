﻿


CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContratos_Consultar]
@IdObjetoContratoContractual INT = NULL, 
@ObjetoContractual nvarchar(MAX) = NULL, 
@Estado BIT = NULL
AS
BEGIN

SELECT
	IdObjetoContratoContractual,
	ObjetoContractual,
	Estado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[ObjetoContrato]
WHERE IdObjetoContratoContractual =
	CASE
		WHEN @IdObjetoContratoContractual IS NULL THEN IdObjetoContratoContractual ELSE @IdObjetoContratoContractual
	END 
	AND ObjetoContractual LIKE '%' + 
	CASE
		WHEN @ObjetoContractual IS NULL THEN ObjetoContractual ELSE @ObjetoContractual
	END   + '%'
	AND Estado =
	CASE
		WHEN @Estado IS NULL THEN Estado ELSE @Estado
	END
	
END


