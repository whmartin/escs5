﻿-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Modificar]
		@IdTipoSectorEntidad INT,	@CodigoSectorEntidad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipoSectorEntidad SET CodigoSectorEntidad = @CodigoSectorEntidad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoSectorEntidad = @IdTipoSectorEntidad
END

