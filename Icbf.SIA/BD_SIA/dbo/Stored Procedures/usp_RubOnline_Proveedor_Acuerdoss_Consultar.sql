﻿
-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que consulta un(a) Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdoss_Consultar]
@Nombre NVARCHAR (64) = NULL
AS
BEGIN

	SELECT
		IdAcuerdo,
		Nombre,
		ContenidoHtml,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Proveedor].[Acuerdos]
	WHERE Nombre =
		CASE
			WHEN @Nombre IS NULL THEN Nombre ELSE @Nombre
		END 
		
END




