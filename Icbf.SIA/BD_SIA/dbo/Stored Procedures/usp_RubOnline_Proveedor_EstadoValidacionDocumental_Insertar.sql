﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Insertar]
		@IdEstadoValidacionDocumental INT OUTPUT, 	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoValidacionDocumental(CodigoEstadoValidacionDocumental, Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@CodigoEstadoValidacionDocumental, @Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoValidacionDocumental = SCOPE_IDENTITY() 		
END


