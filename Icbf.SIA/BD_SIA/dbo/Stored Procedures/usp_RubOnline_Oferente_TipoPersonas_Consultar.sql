﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TipoPersonas_Consultar]
	@CodigoTipoPersona NVARCHAR(128) = NULL,@NombreTipoPersona NVARCHAR(128) = NULL,@Estado BIT = NULL
AS
BEGIN
 SELECT IdTipoPersona, CodigoTipoPersona, NombreTipoPersona, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Tercero].[TipoPersona] 
 WHERE CodigoTipoPersona = CASE WHEN @CodigoTipoPersona IS NULL THEN CodigoTipoPersona ELSE @CodigoTipoPersona END
	AND NombreTipoPersona LIKE '%' + CASE WHEN @NombreTipoPersona IS NULL THEN NombreTipoPersona ELSE @NombreTipoPersona END + '%'
	AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END

