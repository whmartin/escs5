﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_AmparosAsociadoss_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_AmparosAsociadoss_Consultar]
	@IdGarantia INT = NULL,@FechaVigenciaDesde DATETIME = NULL,@VigenciaHasta DATETIME = NULL
AS
BEGIN
 SELECT IdAmparo, IdGarantia, IdTipoAmparo, FechaVigenciaDesde, VigenciaHasta, ValorParaCalculoAsegurado, IdTipoCalculo, ValorAsegurado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[AmparosAsociados] WHERE IdGarantia = CASE WHEN @IdGarantia IS NULL THEN IdGarantia ELSE @IdGarantia END AND FechaVigenciaDesde = CASE WHEN @FechaVigenciaDesde IS NULL THEN FechaVigenciaDesde ELSE @FechaVigenciaDesde END AND VigenciaHasta = CASE WHEN @VigenciaHasta IS NULL THEN VigenciaHasta ELSE @VigenciaHasta END
END


