﻿-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que consulta un(a) DocAdjuntoTercero
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocAdjuntoTercero_Consultar]
@IdDocAdjunto INT
AS
BEGIN
SELECT
	IdDocAdjunto,
	IdTercero,
	IdDocumento,
	Descripcion,
	LinkDocumento,
	Anno,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Proveedor].[DocAdjuntoTercero]
WHERE IdDocAdjunto = @IdDocAdjunto
END
