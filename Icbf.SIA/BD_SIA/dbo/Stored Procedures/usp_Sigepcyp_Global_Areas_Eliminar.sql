﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  4/16/2015 11:22:03 AM
-- Description:	Procedimiento almacenado que elimina un(a) Areas
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_Global_Areas_Eliminar]
	@IdArea INT
AS
BEGIN
	DELETE Global.Areas WHERE IdArea = @IdArea
END
