﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/6/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que consulta un(a) PlanDePagos
-- usp_Sigepcyp_SPCP_PlanDePagosParametros_Consultar 64, 66515, '9'
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_PlanDePagosParametros_Consultar]
	@IdDatosAdministracion INT,
	@CodCompromiso INT,
	@NumeroDePago NVARCHAR(15)
AS
BEGIN

DECLARE @Acumulado decimal(30,8)

SELECT	@Acumulado = ISNULL(SUM(ValorDelPago),0)
FROM	[SPCP].[PlanDePagos] pp
		INNER JOIN (
					SELECT  [NumeroDePago], [IdDatosAdministracion], [CodCompromiso]
					FROM	[SPCP].[CuentasdeCobro] cc
					WHERE	cc.[IdDatosAdministracion] = @IdDatosAdministracion
							AND cc.[CodCompromiso] = @CodCompromiso) np
		ON pp.[IdDatosAdministracion] = np.[IdDatosAdministracion]
			AND pp.[NumeroDePago] = np.[NumeroDePago]
			AND pp.[CodCompromiso] = np.[CodCompromiso]
 
SELECT	IdPlanDePagos, 
		IdDatosAdministracion, 
		Codpci, 
		Vigencia, 
		ConsecutivoPACCO, 
		CodCompromiso, 
		Fecha, 
		TipoDocumento, 
		NumDocIdentidad, 
		CodTipoDocSoporte, 
		NumDocSoporte, 
		ValorTotalContrato, 
		Rubro, 
		Recurso, 
		Fuente, 
		NumeroDePago, 
		OrdenDePago, 
		ValorDelPago, 
		Estado, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica,
		@Acumulado Acumulado
FROM	[SPCP].[PlanDePagos] 
WHERE	IdDatosAdministracion = @IdDatosAdministracion
		AND CodCompromiso = @CodCompromiso
		AND NumeroDePago = @NumeroDePago

END