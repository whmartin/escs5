﻿-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que consulta un(a) CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_sConsultar]
	@IdArchivo NUMERIC(18,0) = NULL,@Destinatario NVARCHAR(256) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdCorreoElectronico, IdArchivo, Destinatario, Mensaje, Estado, FechaIngreso, FechaEnvio, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica, TipoCorreo FROM [Estructura].[CorreoElectronico] WHERE IdArchivo = CASE WHEN @IdArchivo IS NULL THEN IdArchivo ELSE @IdArchivo END AND Destinatario = CASE WHEN @Destinatario IS NULL THEN Destinatario ELSE @Destinatario END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END
