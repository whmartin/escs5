﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 10:21:28 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocExperienciaEntidad_Eliminar]
	@IdDocAdjunto INT
AS
BEGIN
	DELETE Proveedor.DocExperienciaEntidad WHERE IdDocAdjunto = @IdDocAdjunto
END

