﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_Obligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Eliminar]
	@IdObligacion INT
AS
BEGIN
	DELETE Contrato.Obligacion WHERE IdObligacion = @IdObligacion
END
