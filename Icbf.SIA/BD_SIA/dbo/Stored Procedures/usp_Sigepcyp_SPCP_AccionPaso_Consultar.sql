﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/13/2015 2:10:46 PM
-- Description:	Procedimiento almacenado que consulta un(a) AccionPaso
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AccionPaso_Consultar]
	@IdAccionPaso INT
AS
BEGIN
	SELECT
		IdAccionPaso,
		IdPaso,
		Estado,
		Descripcion,
		IdAccion,
		Aceptar,
		Rechazar,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [SPCP].[AccionPaso]
	WHERE IdAccionPaso = @IdAccionPaso
END