﻿-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  04/30/2014 12:24 PM
-- Description:	Procedimiento almacenado que Finaliza InfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_InfoExperienciaEntidad_Finalizar]

	@IdEntidad INT
	
AS
BEGIN

	update Proveedor.InfoExperienciaEntidad 
	set Finalizado = 1	
	from  Proveedor.EntidadProvOferente e
				inner join Proveedor.InfoExperienciaEntidad i
					on e.IdEntidad = i.IdEntidad
				inner join Proveedor.ValidarInfoExperienciaEntidad v
					on v.IdExpEntidad = i.IdExpEntidad
	where 
		e.IdEntidad = @IdEntidad 
		and v.NroRevision = i.NroRevision
		and i.EstadoDocumental = 4 --SI ESTA VALIDADO SE FINALIZA
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMó CON SI

END