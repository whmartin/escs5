﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  2/19/2013 10:06:04 AM
-- Description:	Procedimiento almacenado que consulta un(a) CargoTipoCargo
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_CargoTipoCargo_Consultar]
	@IdCargo INT
	,@TipoCargo NVARCHAR(1)
AS
BEGIN
 SELECT C.*, E.NombreCargo 
 FROM [Global].[CargoTipoCargo] C
 INNER JOIN ECO.Cargo E ON C.IdCargo=E.IdCargo
 WHERE C.IdCargo=@IdCargo AND C.TipoCargo=@TipoCargo
END

