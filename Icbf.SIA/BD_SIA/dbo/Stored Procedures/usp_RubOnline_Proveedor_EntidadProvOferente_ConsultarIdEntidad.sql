﻿
-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que consulta un(a) DocFinancieraProv
-- Modificación Por: Juan Carlos Valverde Sámano
-- Fecha Modificación: 02/04/2014
-- Descripción:  Se agregó al Select el providerKey del TERCERO
-- Modificación Por: Juan Carlos Valverde Sámano
-- Fecha Modificación: 19/08/2014
-- Modificado por: Juan Carlos Valverde Sámano
-- Fecha Modificacón: 06/NOV/2014
-- Descripción:Se agregó al Select el dato OferentesMigrados para saber si se trata o no de un registro
-- que fue migrado desde Oferentes.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EntidadProvOferente_ConsultarIdEntidad]
	@IdEntidad INT
AS
BEGIN
 SELECT P.IdEntidad
      ,P.ConsecutivoInterno
      ,P.TipoEntOfProv
      ,P.IdTercero
      ,P.IdTipoCiiuPrincipal
      ,P.IdTipoCiiuSecundario
      ,P.IdTipoSector
      ,P.IdTipoClaseEntidad
      ,P.IdTipoRamaPublica
      ,P.IdTipoNivelGob
      ,P.IdTipoNivelOrganizacional
      ,P.IdTipoCertificadorCalidad
      ,P.FechaCiiuPrincipal
      ,P.FechaCiiuSecundario
      ,P.FechaConstitucion
      ,P.FechaVigencia
      ,P.FechaMatriculaMerc
      ,P.FechaExpiracion
      ,P.TipoVigencia
      ,P.ExenMatriculaMer
      ,P.MatriculaMercantil
      ,P.ObserValidador
      ,P.AnoRegistro
      ,P.IdEstado
      ,E.Descripcion
      ,P.UsuarioCrea
      ,P.FechaCrea
      ,P.UsuarioModifica
      ,P.FechaModifica
      ,P.IdAcuerdo
      ,P.Finalizado
      ,T.ProviderUserKey
      ,ISNULL(I.IdTipoEntidad,0) IdTipoEntidad
      ,P.IdEstadoProveedor
      ,P.NumIntegrantes
      ,T.IdTipoPersona
      ,(SELECT Descripcion FROM PROVEEDOR.EstadoProveedor WHERE IdEstadoProveedor=P.IdEstadoProveedor) AS 'DescEstadoProv'
      ,P.OferentesMigrados
 FROM [Proveedor].[EntidadProvOferente] P
 INNER JOIN Oferente.TERCERO T ON P.IdTercero=T.IDTERCERO 
 INNER JOIN PROVEEDOR.InfoAdminEntidad I ON P.IdEntidad =I.IdEntidad
 INNER JOIN PROVEEDOR.EstadoDatosBasicos E ON P.IdEstado=E.IdEstadoDatosBasicos
 WHERE  P.IdEntidad = @IdEntidad
END




