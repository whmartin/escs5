﻿




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que guarda un nuevo InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Insertar]
		@IdInfoFin INT OUTPUT, 	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(21, 3),	@ActivoTotal NUMERIC(21, 3),	@PasivoCte NUMERIC(21, 3),	@PasivoTotal NUMERIC(21, 3),	@Patrimonio NUMERIC(21, 3),	@GastosInteresFinancieros NUMERIC(21, 3),	@UtilidadOperacional NUMERIC(21, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioCrea NVARCHAR(250), @IdTemporal VARCHAR(20) = NULL, @Finalizado BIT = NULL
AS
BEGIN
	INSERT INTO Proveedor.InfoFinancieraEntidad(IdEntidad, IdVigencia, ActivoCte, ActivoTotal, PasivoCte, PasivoTotal, Patrimonio, GastosInteresFinancieros, UtilidadOperacional, ConfirmaIndicadoresFinancieros, RupRenovado, EstadoValidacion, ObservacionesInformacionFinanciera, ObservacionesValidadorICBF, UsuarioCrea, FechaCrea, NroRevision, Finalizado)
					  VALUES(@IdEntidad, @IdVigencia, @ActivoCte, @ActivoTotal, @PasivoCte, @PasivoTotal, @Patrimonio, @GastosInteresFinancieros, @UtilidadOperacional, @ConfirmaIndicadoresFinancieros, @RupRenovado, @EstadoValidacion, @ObservacionesInformacionFinanciera, @ObservacionesValidadorICBF, @UsuarioCrea, GETDATE(), 1, @Finalizado)
	SELECT @IdInfoFin = SCOPE_IDENTITY() 		
	
	UPDATE [Proveedor].[DocFinancieraProv] 
	set IdInfoFin = @IdInfoFin
	where IdTemporal = @IdTemporal
	
	SELECT @IdInfoFin
	
END






