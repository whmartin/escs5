﻿
CREATE PROCEDURE [dbo].[usp_SPCP_Seg_ModificarUsuario]
		  @IdUsuario INT
		, @IdTipoPersona INT = NULL
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @RazonSocial NVARCHAR(150) = NULL
		, @CorreoElectronico NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10) = ''
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
		, @IdTipoUsuario INT
		, @IdRegional INT
		, @IdArea INT
AS
BEGIN
	UPDATE SEG.Usuario
		SET [IdTipoDocumento] = @IdTipoDocumento
		   ,[NumeroDocumento] = @NumeroDocumento
		   ,[PrimerNombre] = @PrimerNombre
		   ,[SegundoNombre] = @SegundoNombre
		   ,[PrimerApellido] = @PrimerApellido
		   ,[SegundoApellido] = @SegundoApellido
		   ,[CorreoElectronico] = @CorreoElectronico
		   ,[Estado] = @Estado
		   ,[UsuarioModificacion] = @UsuarioModificacion
		   ,[FechaModificacion] = GETDATE()
		   ,[TelefonoContacto] = @TelefonoContacto
		   ,IdTipoUsuario = @IdTipoUsuario
		   ,IdRegional = @IdRegional
		   ,IdArea = @IdArea
	WHERE [IdUsuario] = @IdUsuario
END




