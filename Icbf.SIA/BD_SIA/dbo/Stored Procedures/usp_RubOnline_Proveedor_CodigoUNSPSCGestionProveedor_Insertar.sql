﻿-- =============================================
-- Author:		Luz Angela Borda
-- Create date:	13 Mayo de 2015 09:12:19 AM
-- Description:	Procedimiento almacenado que guarda un nuevo Codigo UNSPSC del Proveedor
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_CodigoUNSPSCGestionProveedor_Insertar]
	@CodUNSPSC INT OUTPUT,
	@IdTercero INT, 	
	@IdTipoCodUNSPSC INT,	
	@UsuarioCrea NVARCHAR(250)
AS
BEGIN

	-------------------------------------------------------
	-- Borrar código UNSPS en caso de que exista 
	-------------------------------------------------------

	DELETE 
	FROM	Proveedor.CodUNSPSC
	WHERE	IDTERCERO = @IdTercero
	AND		IdTipoCodUNSPSC = @IdTipoCodUNSPSC

	-------------------------------------------------------
	-- Insertar código UNSPS  
	-------------------------------------------------------

	INSERT INTO Proveedor.CodUNSPSC
	(	
		IDTERCERO, 
		IdTipoCodUNSPSC, 
		UsuarioCrea, 
		FechaCrea
	)
	
	VALUES
	(
		@IdTercero, 
		@IdTipoCodUNSPSC, 
		@UsuarioCrea, 
		GETDATE()
	)
	
	SELECT @CodUNSPSC = SCOPE_IDENTITY() 		
END



