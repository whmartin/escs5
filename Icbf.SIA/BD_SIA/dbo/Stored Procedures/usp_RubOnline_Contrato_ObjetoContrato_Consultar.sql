﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_ObjetoContrato_Consultar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ObjetoContrato_Consultar]
	@IdObjetoContratoContractual INT
AS
BEGIN
 SELECT IdObjetoContratoContractual, ObjetoContractual, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Contrato].[ObjetoContrato] 
 WHERE  IdObjetoContratoContractual = @IdObjetoContratoContractual
END
