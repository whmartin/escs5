﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:44:50 PM
-- Description:	Procedimiento almacenado que consulta un(a) Comuna
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Comuna_Consultar]
	@IdComuna INT
AS
BEGIN
 SELECT IdComuna
      , IdMunicipio
      , CodigoComuna
      , NombreComuna
      , UsuarioCrea
      , FechaCrea
      , UsuarioModifica
      , FechaModifica 
  FROM [DIV].[Comuna] 
  WHERE  IdComuna = @IdComuna
END

