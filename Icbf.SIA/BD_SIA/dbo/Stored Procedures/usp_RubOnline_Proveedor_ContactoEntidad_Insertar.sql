﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Insertar]
		@IdContacto INT OUTPUT, 	@IdEntidad INT,	@IdSucursal INT, @IdTelContacto INT,	@IdTipoDocIdentifica INT,	
		@IdTipoCargoEntidad INT,	@NumeroIdentificacion NUMERIC(30),	@PrimerNombre NVARCHAR(128),	@SegundoNombre NVARCHAR(128),	
		@PrimerApellido NVARCHAR(128),	@SegundoApellido NVARCHAR(128),	@Dependencia NVARCHAR(128),	@Email NVARCHAR(128),	@Estado BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ContactoEntidad(IdEntidad, IdSucursal, IdTelContacto, IdTipoDocIdentifica, IdTipoCargoEntidad, NumeroIdentificacion, PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido, Dependencia, Email, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@IdEntidad, @IdSucursal, @IdTelContacto, @IdTipoDocIdentifica, @IdTipoCargoEntidad, @NumeroIdentificacion, @PrimerNombre, @SegundoNombre, @PrimerApellido, @SegundoApellido, @Dependencia, @Email, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdContacto = SCOPE_IDENTITY() 		
END


