﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoAdminEntidad
-- Modificado: Juan Carlos Valverde Sámano
-- Fecha: 24/OCT/2014
-- Descripción: Se cambió la longitud del parámetro DireccionComercial a nvarchar(128)
-- y la ongitud del parámetro @NombreEstablecimiento a nvarchar(256)
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Modificar]
		@IdInfoAdmin  INT = NULL,	@IdVigencia  INT = NULL,	@IdEntidad  INT = NULL,	@IdTipoRegTrib  INT = NULL,	@IdTipoOrigenCapital  INT = NULL,	@IdTipoActividad  INT = NULL,	
		@IdTipoEntidad  INT = NULL,	@IdTipoNaturalezaJurid  INT = NULL,	@IdTipoRangosTrabajadores  INT = NULL,	@IdTipoRangosActivos  INT = NULL,	@IdRepLegal  INT = NULL,	
		@IdTipoCertificaTamano  INT = NULL,	@IdTipoEntidadPublica  INT = NULL,	@IdDepartamentoConstituida  INT = NULL,	@IdMunicipioConstituida  INT = NULL,	@IdDepartamentoDirComercial  INT = NULL,	
		@IdMunicipioDirComercial  INT = NULL,	@DireccionComercial NVARCHAR(256)= NULL,	@IdZona  NVARCHAR(128) = NULL,@NombreComercial NVARCHAR(128)= NULL,
		@NombreEstablecimiento NVARCHAR(256) = NULL,	@Sigla NVARCHAR(50)= NULL,	
		@PorctjPrivado  INT = NULL,	@PorctjPublico  INT = NULL,	@SitioWeb NVARCHAR(128)= NULL,	@NombreEntidadAcreditadora NVARCHAR = NULL,	@Organigrama  BIT = NULL,	
		@TotalPnalAnnoPrevio  numeric(10) = NULL,	@VincLaboral  numeric(10) = NULL,	@PrestServicios  numeric(10) = NULL,	@Voluntariado  numeric(10) = NULL,	
		@VoluntPermanente  numeric(10) = NULL,	@Asociados  numeric(10) = NULL,	@Mision  numeric(10) = NULL,	@PQRS  BIT = NULL,	@GestionDocumental  BIT = NULL,	@Auditoria INT = NULL, @AuditoriaInterna  BIT = NULL,	
		@ManProcedimiento  BIT = NULL,	@ManPracticasAmbiente  BIT = NULL,	@ManComportOrg  BIT = NULL,	@ManFunciones  BIT = NULL,	@ProcRegInfoContable  BIT = NULL,	@PartMesasTerritoriales  BIT = NULL,	
		@PartAsocAgremia  BIT = NULL,	@PartConsejosComun  BIT = NULL,	@Conv INT = NULL ,@ConvInterInst  BIT = NULL,	@ProcSeleccGral  BIT = NULL,	@ProcSeleccEtnico  BIT = NULL,	@PlanInduccCapac  BIT = NULL,	
		@EvalDesemp  BIT = NULL,	@PlanCualificacion  BIT = NULL,	@NumSedes  INT = NULL,	@SedesPropias  BIT = NULL, @UsuarioModifica NVARCHAR(250)= NULL
AS
BEGIN
	UPDATE Proveedor.InfoAdminEntidad 
	SET 
	IdVigencia = ISNULL( @IdVigencia, IdVigencia ),
	IdEntidad = ISNULL( @IdEntidad, IdEntidad ),
	IdTipoRegTrib = ISNULL( @IdTipoRegTrib, IdTipoRegTrib ),
	IdTipoOrigenCapital = ISNULL( @IdTipoOrigenCapital,IdTipoOrigenCapital ),
	IdTipoActividad = ISNULL( @IdTipoActividad, IdTipoActividad ),
	IdTipoEntidad = ISNULL( @IdTipoEntidad, IdTipoEntidad ),
	IdTipoNaturalezaJurid = ISNULL( @IdTipoNaturalezaJurid, IdTipoNaturalezaJurid ),
	IdTipoRangosTrabajadores = ISNULL( @IdTipoRangosTrabajadores, IdTipoRangosTrabajadores ),
	IdTipoRangosActivos = ISNULL( @IdTipoRangosActivos, IdTipoRangosActivos ),
	IdRepLegal = ISNULL( @IdRepLegal, IdRepLegal ),
	IdTipoCertificaTamano = ISNULL( @IdTipoCertificaTamano, IdTipoCertificaTamano ),
	IdTipoEntidadPublica = ISNULL( @IdTipoEntidadPublica, IdTipoEntidadPublica ),
	IdDepartamentoConstituida = ISNULL( @IdDepartamentoConstituida, IdDepartamentoConstituida ),
	IdMunicipioConstituida = ISNULL( @IdMunicipioConstituida, IdMunicipioConstituida ),
	IdDepartamentoDirComercial = ISNULL( @IdDepartamentoDirComercial, IdDepartamentoDirComercial ),
	IdMunicipioDirComercial = ISNULL( @IdMunicipioDirComercial, IdMunicipioDirComercial ),
	DireccionComercial = ISNULL( @DireccionComercial, DireccionComercial ),
	IdZona = ISNULL( @IdZona,  IdZona ),
	NombreComercial = ISNULL( @NombreComercial, NombreComercial ),
	NombreEstablecimiento = ISNULL( @NombreEstablecimiento, NombreEstablecimiento ),
	Sigla = ISNULL( @Sigla, Sigla ),
	PorctjPrivado = ISNULL( @PorctjPrivado, PorctjPrivado ),
	PorctjPublico = ISNULL( @PorctjPublico, PorctjPublico ),
	SitioWeb = ISNULL( @SitioWeb, SitioWeb ),
	NombreEntidadAcreditadora = ISNULL( @NombreEntidadAcreditadora, NombreEntidadAcreditadora ),
	Organigrama = ISNULL( @Organigrama, Organigrama ),
	TotalPnalAnnoPrevio = ISNULL( @TotalPnalAnnoPrevio, TotalPnalAnnoPrevio ),
	VincLaboral = ISNULL( @VincLaboral, VincLaboral ),
	PrestServicios = ISNULL( @PrestServicios, PrestServicios ),
	Voluntariado = ISNULL( @Voluntariado, Voluntariado ),
	VoluntPermanente = ISNULL( @VoluntPermanente, VoluntPermanente ),
	Asociados = ISNULL( @Asociados, Asociados ),
	Mision = ISNULL( @Mision, Mision ),
	PQRS = ISNULL( @PQRS, PQRS ),
	GestionDocumental = ISNULL( @GestionDocumental, GestionDocumental ),
	AuditoriaInterna = ISNULL( @AuditoriaInterna, AuditoriaInterna ),
	ManProcedimiento = ISNULL( @ManProcedimiento, ManProcedimiento ),
	ManPracticasAmbiente = ISNULL( @ManPracticasAmbiente, ManPracticasAmbiente ),
	ManComportOrg = ISNULL( @ManComportOrg, ManComportOrg ),
	ManFunciones = ISNULL( @ManFunciones, ManFunciones ),
	ProcRegInfoContable = ISNULL( @ProcRegInfoContable, ProcRegInfoContable ),
	PartMesasTerritoriales = ISNULL( @PartMesasTerritoriales, PartMesasTerritoriales ),
	PartAsocAgremia = ISNULL( @PartAsocAgremia, PartAsocAgremia ),
	PartConsejosComun = ISNULL( @PartConsejosComun, PartConsejosComun ),
	ConvInterInst = ISNULL( @ConvInterInst, ConvInterInst ),
	ProcSeleccGral = ISNULL( @ProcSeleccGral, ProcSeleccGral ),
	ProcSeleccEtnico = ISNULL( @ProcSeleccEtnico, ProcSeleccEtnico ),
	PlanInduccCapac = ISNULL( @PlanInduccCapac, PlanInduccCapac ),
	EvalDesemp = ISNULL( @EvalDesemp, EvalDesemp ),
	PlanCualificacion = ISNULL( @PlanCualificacion, PlanCualificacion ),
	NumSedes = ISNULL( @NumSedes, NumSedes ),
	SedesPropias = ISNULL( @SedesPropias, SedesPropias ),
	UsuarioModifica = @UsuarioModifica, 
	FechaModifica = GETDATE() 
	WHERE IdInfoAdmin = @IdInfoAdmin
END


