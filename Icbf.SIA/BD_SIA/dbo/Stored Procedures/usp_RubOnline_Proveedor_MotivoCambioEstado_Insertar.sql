﻿

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/16/2013 5:35:41 PM
-- Description:	Procedimiento almacenado que guarda un nuevo cambio de estado
-- Modificado: Juan.Valverde, 12-AGO-2014
-- Descripción: Se aregan las lineas para el cambio de estado del módulo Integrantes, y se quitán
-- las líneas que actualizaban el estado del Tercero ya que el estado del Tercero es independiente de 
-- el estado del proveedor.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_MotivoCambioEstado_Insertar]
@IdTercero INT, @DatosBasicos BIT, @Financiera BIT, @Experiencia BIT, @Integrantes BIT, @IdTemporal NVARCHAR (20), @Motivo NVARCHAR (128), @UsuarioCrea NVARCHAR (250)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION t1
		INSERT INTO Proveedor.MotivoCambioEstado (IdTercero, IdTemporal, Motivo, DatosBasicos, Financiera, Experiencia, FechaCrea, UsuarioCrea, Integrantes)
			VALUES (@IdTercero, @IdTemporal, @Motivo, @DatosBasicos, @Financiera, @Experiencia, GETDATE(), @UsuarioCrea, @Integrantes)


		DECLARE @PorValidar NVARCHAR (10)
		
		

		

		--Se obtiene el id de el proveedor para asi actualizar su respectivo estado
		DECLARE @IdEntidad INT
		SET @IdEntidad = (SELECT DISTINCT TOP(1)
			Proveedor.EntidadProvOferente.IdEntidad
		FROM Proveedor.EntidadProvOferente
		INNER JOIN Oferente.TERCERO
			ON Proveedor.EntidadProvOferente.IdTercero = Oferente.TERCERO.IDTERCERO
		WHERE (Oferente.TERCERO.IDTERCERO = @IdTercero))
		

		--Se actualiza el estado a Datos Básicos
		SET @PorValidar = (SELECT
			IdEstadoDatosBasicos 
		FROM Proveedor.EstadoDatosBasicos    
		WHERE Descripcion  = 'EN VALIDACIÓN')
		
		--Carlos Cubillos: Se establece que independiente de si está marcado DatosBasicos se modifica el estado de este.
	
		UPDATE Proveedor.EntidadProvOferente
		SET IdEstado = @PorValidar, Finalizado  = 0
		WHERE Proveedor.EntidadProvOferente.IdEntidad = @IdEntidad
		
		
		--Se actualiza el estado a Financiera
		SET @PorValidar = (SELECT
			IdEstadoValidacionDocumental 
		FROM Proveedor.EstadoValidacionDocumental
		WHERE CodigoEstadoValidacionDocumental    = '05')
		IF @Financiera = 1
		BEGIN
		UPDATE Proveedor.InfoFinancieraEntidad
		SET EstadoValidacion = @PorValidar, Finalizado  = 0
		WHERE Proveedor.InfoFinancieraEntidad.IdEntidad = @IdEntidad
		END
		
		
		
		--Se actualiza el estado a Experiencia
		--Carlos Cubillos: Se cambio el set antes del if, estaba actualizando siempre
		SET @PorValidar = (SELECT
			IdEstadoValidacionDocumental 
		FROM Proveedor.EstadoValidacionDocumental    
		WHERE CodigoEstadoValidacionDocumental    = '05')
		IF @Experiencia = 1
		BEGIN
		UPDATE Proveedor.InfoExperienciaEntidad
		SET EstadoDocumental = @PorValidar, Finalizado  = 0
		WHERE Proveedor.InfoExperienciaEntidad.IdEntidad = @IdEntidad
		END
		
		--Se actualiza el estado a Integrantes
		SET @PorValidar = (SELECT
			IdEstadoIntegrantes 
		FROM Proveedor.EstadoIntegrantes    
		WHERE Descripcion    = 'EN VALIDACIÓN')
		IF @Integrantes = 1
		BEGIN
			UPDATE PROVEEDOR.ValidacionIntegrantesEntidad
			SET IdEstadoValidacionIntegrantes=@PorValidar,
			Finalizado=0
			WHERE IdEntidad=@IdEntidad
		END
		

		
		
		
COMMIT TRANSACTION t1
END TRY
BEGIN CATCH
ROLLBACK TRANSACTION t1
SELECT
	ERROR_NUMBER() AS ErrorNumber,
	ERROR_MESSAGE() AS ErrorMessage;
END CATCH;
END





