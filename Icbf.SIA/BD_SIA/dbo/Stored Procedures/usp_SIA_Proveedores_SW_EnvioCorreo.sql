﻿


-- ==========================================================================================
-- Author:		 Bayron lara
-- Create date:  04/07/2014 11:38:19 AM
-- Description:	 Procedimineto que envia correo a los proveedores antes de ser eleminidaos
-- ==========================================================================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedores_SW_EnvioCorreo]

@Email_Destino NVARCHAR(256), --- Nombre de la aplicacion en el membership
@Usuario NVARCHAR(256), ---Rol del usuario
@TextoEmail NVARCHAR(MAX), --Texto del email enviado
@TituloEmail NVARCHAR(256), --Título del email enviado
@Paso1 NVARCHAR(256), --Paso 1 de ingreso al sistema email
@Paso2 NVARCHAR(256), --Paso 2 de ingreso al sistema email
@Paso3 NVARCHAR(256), --Paso 3 de ingreso al sistema email
@Paso4 NVARCHAR(256) --Paso 4 de ingreso al sistema email



AS
BEGIN

DECLARE @Email_Body	NVARCHAR(MAX)
		
		
				
		SELECT @Email_Body = ''
		SELECT @Email_Body = @Email_Body + '<table width=100% height=412 border=0 cellpadding=0 cellspacing=0>
												<tr>
													<td colspan=3 bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td width=10% bgcolor=#81BA3D>&nbsp;</td>
													<td>
														<table width=100% border=0 align=center>
															<tr>
																<td width=5%>&nbsp;</td>
																<td align=center><strong> '
		--Cambiar por parámetro
        SELECT @Email_Body = @Email_Body + @TituloEmail + '						</strong></td>
		
		
																
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td align=center><strong> '
        SELECT @Email_Body = @Email_Body + '						¡Eliminación de Registro!</strong></td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td><strong>Apreciado(a):</strong>&nbsp;' + @Usuario + ',</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>' + @TextoEmail + ':
																</td>
																<td>&nbsp;</td>
															</tr>'  
        SELECT @Email_Body = @Email_Body + '				<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td><strong>Activa tu cuenta</strong> '
        SELECT @Email_Body = @Email_Body + '						para empezar a utilizar el sistema, es muy <strong>fácil y rápido</strong>
																</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>'
		SELECT @Email_Body = @Email_Body + '    					<strong>1.- </strong>' + @Paso1 + '
																</td><td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>2.- </strong>' + @Paso2 + '</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>3.- </strong>' +  @Paso3 + '</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td> '
        SELECT @Email_Body = @Email_Body + '						<strong>4.- </strong>' +  @Paso4 + '</td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
															</tr>
															<tr> '
		SELECT @Email_Body = @Email_Body + '					<td width=5%>&nbsp;</td>
																<td>&nbsp;</td>
																<td width=5%>&nbsp;</td>
															</tr>
														</table>
													</td>
													<td width=10% bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>&nbsp;</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo. </td>
												</tr>
												<tr> '
        SELECT @Email_Body = @Email_Body + '		<td colspan=3 align=center bgcolor=#81BA3D>Si tienes alguna duda, puedes dirigirte a nuestra sección de  '
        SELECT @Email_Body = @Email_Body + '		<a href=http://www.icbf.gov.co/ target=_blank>Asistencia y Soporte.</a>
													</td>
												</tr>
												<tr>
													<td colspan=3 align=center bgcolor=#81BA3D>&nbsp;</td>
												</tr>
											</table>'
											
		EXEC msdb.dbo.sp_send_dbmail 
			@profile_name = 'GonetMail', --Colocar perfil que se tenga configurado
			@recipients = @Email_Destino, --Destinatario
			@subject = 'Proceso de Eliminar Usuarios',--Asunto
			@body = @Email_Body, --Cuerpo de correo
			@body_format = 'HTML' ; --Formato de correo
						
		
		
		
		
	
	
	
	
	
	
	
END--FIN PP





