﻿
-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumentoPrograma
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Consultar]
@IdTipoDocumentoPrograma INT
AS
BEGIN
SELECT
	IdTipoDocumentoPrograma,
	IdTipoDocumento,
	IdPrograma,
	Estado,
	MaxPermitidoKB,
	ExtensionesPermitidas,
	ObligRupNoRenovado,
	ObligRupRenovado,
	ObligPersonaJuridica,
	ObligPersonaNatural,
	ObligSectorPublico,
	ObligSectorPrivado,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica,
	ObligEntNacional,
	ObligEntExtranjera,
	ObligConsorcio,
	ObligUnionTemporal
FROM [Proveedor].[TipoDocumentoPrograma]
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma 
--AND Estado = 1
END

