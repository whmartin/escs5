﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar
***********************************************/

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarSupervisorInterventor_Insertar]
		@IDSupervisorInterv INT OUTPUT, 	
		@IDContratoSupervisa INT,	
		@OrigenTipoSupervisor bit,	
		@IDTipoSupvInterventor INT,	
		@IDTerceroExterno INT,	
		@IDFuncionarioInterno INT,	
		@IDContratoInterventoria INT,
		@IDTerceroInterventoria	int,
		@TipoVinculacion bit,	
		@FechaInicia DATETIME,	
		@FechaFinaliza DATETIME,	
		@Estado bit,	
		@FechaModificaInterventor DATETIME,	
		@UsuarioCrea nvarchar(128),
		@UsuarioModifica nvarchar(128)
AS
BEGIN
	INSERT INTO Contrato.SupervisorIntervContrato(IDContratoSupervisa, OrigenTipoSupervisor, IDTipoSupvInterventor, IDTerceroExterno, IDFuncionarioInterno, IDContratoInterventoria, TipoVinculacion, FechaInicia, FechaFinaliza, Estado, FechaModificaInterventor, UsuarioCrea, FechaCrea, IDTerceroInterventoria,UsuarioModifica, FechaModifica)
	VALUES(@IDContratoSupervisa, @OrigenTipoSupervisor, @IDTipoSupvInterventor, @IDTerceroExterno, @IDFuncionarioInterno, @IDContratoInterventoria, @TipoVinculacion, @FechaInicia, @FechaFinaliza, @Estado, @FechaModificaInterventor, @UsuarioCrea, GETDATE(), @IDTerceroInterventoria,@UsuarioModifica,GETDATE())
	SELECT @IDSupervisorInterv = @@IDENTITY 		
END


