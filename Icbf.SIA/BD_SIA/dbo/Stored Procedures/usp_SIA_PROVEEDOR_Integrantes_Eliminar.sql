﻿
-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que elimina un(a) Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Eliminar]
	@IdIntegrante INT
AS
BEGIN
	DELETE PROVEEDOR.Integrantes WHERE IdIntegrante = @IdIntegrante
END
