﻿
-- =============================================
-- Author:		<Author,,Grupo de Sistemas de Información - Brayher Gómez Reyes>
-- Create date: <15-04-2015,,>
-- Description:	<Consultar Obligaciones Registradas por Vigencia,,>
-- =============================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ObligacionesRegistradas_Consultar] 
	@NumDocIdentidad NVARCHAR(255),@Codpci NVARCHAR(255),@Fecha DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DAO.[IdDatosAdmonObligaciones], DA.[IdDatosAdministracion]
	FROM [SPCP].[DatosAdministracion] DA
	INNER JOIN [SPCP].[DatosAdmonObligaciones] DAO WITH(NOLOCK) ON DA.[IdDatosAdministracion] = DAO.IdDatosAdministracion
	WHERE DA.NumDocIdentidad = @NumDocIdentidad 
	AND DA.Codpci = @Codpci
	AND year(DA.Fecha) = year(@Fecha)
END

