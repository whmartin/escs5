﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_RegimenContratacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RegimenContratacion_Consultar]
	@IdRegimenContratacion INT
AS
BEGIN
 SELECT IdRegimenContratacion, NombreRegimenContratacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[RegimenContratacion] WHERE  IdRegimenContratacion = @IdRegimenContratacion
END
