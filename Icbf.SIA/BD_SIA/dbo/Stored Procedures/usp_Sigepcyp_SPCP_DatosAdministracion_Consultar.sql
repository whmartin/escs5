﻿

-- =============================================
-- Author:		ICBF\brayher.gomez
-- Create date:  13/03/2015 9:36:02 p. m.
-- Description:	Procedimiento almacenado que consulta un(a) DatosAdministracion
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdministracion_Consultar]
	@IdDatosAdministracion INT
AS
BEGIN
 SELECT IdDatosAdministracion, Codpci, CodCompromiso, Fecha, Codcdp, Vigencia, Descripcion, ValorInicial, ValorActual, Saldo, 
 NumCtaBancaria, NomEntidadFinanciera, NomTipoCta, EstadoCta, FechaDocSoporte, CodTipoDocSoporte, NomTipoDocSoporte, NumDocSoporte, 
 TipoDocumento, NumDocIdentidad, NomTercero, IdArea, InicioEjecucionContrato, FinalEjecucionContrato, VigenciaDatosAdministracion,
 CorreoInstitucional, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [SPCP].[DatosAdministracion] WHERE  IdDatosAdministracion = @IdDatosAdministracion
END
