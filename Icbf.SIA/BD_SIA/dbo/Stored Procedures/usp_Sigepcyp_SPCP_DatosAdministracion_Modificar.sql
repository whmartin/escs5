﻿

-- =============================================
-- Author:		ICBF\brayher.gomez
-- Create date:  13/03/2015 9:36:01 p. m.
-- Description:	Procedimiento almacenado que actualiza un(a) DatosAdministracion
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_DatosAdministracion_Modificar]
		@IdDatosAdministracion INT,	@Codpci NVARCHAR(255),	@CodCompromiso INT,	@Fecha DATETIME,	@Codcdp INT,	@Vigencia INT,	
		@Descripcion NVARCHAR(255),	@ValorInicial NVARCHAR(40),	@ValorActual NVARCHAR(40),	@Saldo NVARCHAR(40),	
		@NumCtaBancaria NVARCHAR(255),	@NomEntidadFinanciera NVARCHAR(255),	@NomTipoCta NVARCHAR(255),	@EstadoCta NVARCHAR(255),	
		@FechaDocSoporte DATETIME,	@CodTipoDocSoporte INT,	@NomTipoDocSoporte NVARCHAR(255),	@NumDocSoporte NVARCHAR(40),	
		@TipoDocumento INT,	@NumDocIdentidad NVARCHAR(20),	@NomTercero NVARCHAR(255),	
		@IdArea INT,	@InicioEjecucionContrato DATETIME,	@FinalEjecucionContrato DATETIME, 
		@VigenciaDatosAdministracion INT, @UsuarioModifica NVARCHAR(250), @CorreoInstitucional NVARCHAR(255)
AS
BEGIN
	UPDATE SPCP.DatosAdministracion SET Codpci = @Codpci, CodCompromiso = @CodCompromiso, Fecha = @Fecha, Codcdp = @Codcdp, 
	Vigencia = @Vigencia, Descripcion = @Descripcion, ValorInicial = @ValorInicial, ValorActual = @ValorActual, Saldo = @Saldo,
	 NumCtaBancaria = @NumCtaBancaria, NomEntidadFinanciera = @NomEntidadFinanciera, NomTipoCta = @NomTipoCta, EstadoCta = @EstadoCta, 
	 FechaDocSoporte = @FechaDocSoporte, CodTipoDocSoporte = @CodTipoDocSoporte, NomTipoDocSoporte = @NomTipoDocSoporte, 
	 NumDocSoporte = @NumDocSoporte, TipoDocumento = @TipoDocumento, NumDocIdentidad = @NumDocIdentidad, NomTercero = @NomTercero, 
	 IdArea = @IdArea, InicioEjecucionContrato = @InicioEjecucionContrato, FinalEjecucionContrato = @FinalEjecucionContrato, 
	 VigenciaDatosAdministracion = @VigenciaDatosAdministracion, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE(),CorreoInstitucional= @CorreoInstitucional 
	 WHERE IdDatosAdministracion = @IdDatosAdministracion
END
