﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  3/12/2015 2:21:15 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Presupuestos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Presupuestos_Insertar]
		@IdPresupuesto INT OUTPUT, 	@IdDatosAdministracion NVARCHAR(255),	@CodPosicionGasto NVARCHAR(255),	
		@NomPosicionGasto NVARCHAR(255),	@NomFuenteFinanciacion NVARCHAR(255),	@CodRecursoPresupuestal INT,	
		@NomRecursoPresupuestal NVARCHAR(255),	@NomSituacionFondos NVARCHAR(255), 
		@ValorInicialRubro decimal=NULL,
		@ValorActualRubro decimal=NULL,
		@ValorSaldoRubro decimal=NULL,
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.Presupuestos(IdDatosAdministracion, CodPosicionGasto, NomPosicionGasto, NomFuenteFinanciacion, CodRecursoPresupuestal, NomRecursoPresupuestal, NomSituacionFondos, ValorInicialRubro,ValorActualRubro,ValorSaldoRubro,UsuarioCrea, FechaCrea)
					  VALUES(@IdDatosAdministracion, @CodPosicionGasto, @NomPosicionGasto, @NomFuenteFinanciacion, @CodRecursoPresupuestal, @NomRecursoPresupuestal, @NomSituacionFondos, @ValorInicialRubro,@ValorActualRubro,@ValorSaldoRubro,@UsuarioCrea, GETDATE())
	SELECT @IdPresupuesto = @@IDENTITY 		
END
