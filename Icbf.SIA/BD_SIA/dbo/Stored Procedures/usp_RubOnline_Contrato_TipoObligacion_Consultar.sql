﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoObligacion_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoObligacion_Consultar]
	@IdTipoObligacion INT
AS
BEGIN
 SELECT IdTipoObligacion, NombreTipoObligacion, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoObligacion] WHERE  IdTipoObligacion = @IdTipoObligacion
END

