﻿

-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date: 02/04/2014
-- Description:	Modfica la Razón Social de un Tercero.
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_Tercero_ModificarRazonSocial]
	-- Add the parameters for the stored procedure here
	@idTercero INT, @razonSocial NVARCHAR(256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE Tercero.TERCERO 
	SET RAZONSOCIAL=@razonSocial
	WHERE IDTERCERO=@idTercero
END


