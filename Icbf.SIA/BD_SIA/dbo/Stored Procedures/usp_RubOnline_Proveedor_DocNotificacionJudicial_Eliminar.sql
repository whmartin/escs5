﻿-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/21/2013 6:56:05 PM
-- Description:	Procedimiento almacenado que elimina un(a) DocNotificacionJudicial
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocNotificacionJudicial_Eliminar]
	@IdDocNotJudicial INT
AS
BEGIN
	DELETE Proveedor.DocNotificacionJudicial 
	WHERE IdDocNotJudicial = @IdDocNotJudicial 
END

