﻿





-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoDatosBasicosEntidad_ModificarEstado]
		@IdEntidad INT, @IdEstadoInfoDatosBasicosEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[EntidadProvOferente] 
	SET IdEstado = @IdEstadoInfoDatosBasicosEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdEntidad = @IdEntidad
END







