﻿


-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 8:08:36 AM
-- Description:	Procedimiento almacenado que consulta el más reciente ValidarInfoExperienciaEntidad por IdInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_Consultar_InfoExperienciaEntidad]
	@IdExpEntidad INT
AS
BEGIN
 SELECT TOP 1 IdValidarInfoExperienciaEntidad, IdExpEntidad, NroRevision, Observaciones, ConfirmaYAprueba, UsuarioCrea, FechaCrea 
 FROM [Proveedor].[ValidarInfoExperienciaEntidad] 
 WHERE  IdExpEntidad = @IdExpEntidad
 ORDER BY IdValidarInfoExperienciaEntidad DESC
END




