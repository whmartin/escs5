﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Modificar]
		@IdTipodeActividad INT,	@CodigoTipodeActividad NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.TipodeActividad SET CodigoTipodeActividad = @CodigoTipodeActividad, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipodeActividad = @IdTipodeActividad
END

