﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que actualiza un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Modificar]
		@IdTituloObtenido INT,	@CodigoTituloObtenido NVARCHAR(128),	@NombreTituloObtenido NVARCHAR(128),	@Estado NVARCHAR(1), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Global.TituloObtenido SET CodigoTituloObtenido = @CodigoTituloObtenido, NombreTituloObtenido = @NombreTituloObtenido, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTituloObtenido = @IdTituloObtenido
END

