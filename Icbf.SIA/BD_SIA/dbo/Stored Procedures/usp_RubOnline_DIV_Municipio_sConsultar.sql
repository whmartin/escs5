﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 5:03:47 PM
-- Description:	Procedimiento almacenado que consulta un(a) Municipio
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Municipio_sConsultar]
	@IdDepartamento INT = NULL,@CodigoMunicipio NVARCHAR(128) = NULL,@NombreMunicipio NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdMunicipio, 
		IdDepartamento, 
		CodigoMunicipio, 
		NombreMunicipio, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Municipio] 
	WHERE IdDepartamento = CASE WHEN @IdDepartamento IS NULL THEN IdDepartamento ELSE @IdDepartamento END 
	      AND CodigoMunicipio = CASE WHEN @CodigoMunicipio IS NULL THEN CodigoMunicipio ELSE @CodigoMunicipio END 
	      AND NombreMunicipio = CASE WHEN @NombreMunicipio IS NULL THEN NombreMunicipio ELSE @NombreMunicipio END
	ORDER BY NombreMunicipio DESC      
END

