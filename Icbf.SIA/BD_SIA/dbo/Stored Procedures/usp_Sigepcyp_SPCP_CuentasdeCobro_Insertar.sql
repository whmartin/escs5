﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/11/2015 5:46:57 PM
-- Description:	Procedimiento almacenado que guarda un nuevo CuentasdeCobro
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobro_Insertar]
		@IdCuenta INT OUTPUT, 	@IdContrato INT,@IdEstadoPaso INT,@IdTipoCuenta INT,@IdEstadoGestion BIT,@Pensionado BIT,@Declarante BIT,@IdRegimen INT,@PlanillaAportes NVARCHAR(50),@IdNivelARL INT,@IdPlanDePagos INT,@NumeroFactura NVARCHAR(50),@AporteSalud INT,@AportePension INT,@SolidaridadPensional INT,@AporteAFC INT,@AporteARL INT,@IdDatosAdministracion INT,@CodCompromiso INT,@NumeroDePago NVARCHAR(15),@NombreSupervisor NVARCHAR(250),@CargoSupervisor NVARCHAR(250), @TotalContrato DECIMAL(15,2), @HonorarioIva DECIMAL(15,2), @HonorarioSinIva DECIMAL(15,2), @HonorarioAcumulado DECIMAL(15,2), @Saldo DECIMAL(15,2),@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO SPCP.CuentasdeCobro(IdContrato, IdEstadoPaso, IdTipoCuenta, IdEstadoGestion, Pensionado, Declarante, IdRegimen, PlanillaAportes, IdNivelARL, IdPlanDePagos, NumeroFactura, AporteSalud, AportePension, SolidaridadPensional, AporteAFC, AporteARL, IdDatosAdministracion, CodCompromiso, NumeroDePago, NombreSupervisor, CargoSupervisor, TotalContrato, HonorarioIva, HonorarioSinIva, HonorarioAcumulado, Saldo, UsuarioCrea, FechaCrea)
					  VALUES(@IdContrato, @IdEstadoPaso, @IdTipoCuenta, @IdEstadoGestion, @Pensionado, @Declarante, @IdRegimen, @PlanillaAportes, @IdNivelARL, @IdPlanDePagos, @NumeroFactura, @AporteSalud, @AportePension, @SolidaridadPensional, @AporteAFC, @AporteARL, @IdDatosAdministracion, @CodCompromiso, @NumeroDePago, @NombreSupervisor, @CargoSupervisor, @TotalContrato, @HonorarioIva, @HonorarioSinIva, @HonorarioAcumulado, @Saldo, @UsuarioCrea, GETDATE())
	SELECT @IdCuenta = @@IDENTITY 		
END
