﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Consultar]
	@IdTipoClausula INT
AS
BEGIN
 SELECT IdTipoClausula, NombreTipoClausula, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[TipoClausula] WHERE  IdTipoClausula = @IdTipoClausula
END

