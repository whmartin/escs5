﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que actualiza un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Modificar]
		@IdRegional INT,	@CodigoRegional NVARCHAR(128),	@NombreRegional NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE DIV.Regional SET CodigoRegional = @CodigoRegional, NombreRegional = @NombreRegional, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdRegional = @IdRegional
END

