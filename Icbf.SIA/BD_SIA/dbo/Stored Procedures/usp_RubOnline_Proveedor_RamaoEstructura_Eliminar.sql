﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que elimina un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Eliminar]
	@IdRamaEstructura INT
AS
BEGIN
	DELETE Proveedor.RamaoEstructura WHERE IdRamaEstructura = @IdRamaEstructura
END

