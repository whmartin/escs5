﻿

CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametros_Consultar]
@NombreParametro NVARCHAR (128) = NULL, @ValorParametro NVARCHAR (256) = NULL, @Estado BIT = NULL,
@Funcionalidad NVARCHAR (128) = NULL
AS
BEGIN

	SELECT
		IdParametro,
		NombreParametro,
		ValorParametro,
		ImagenParametro,
		Estado,
		Funcionalidad,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [SEG].[Parametro]
	WHERE NombreParametro LIKE CASE
		WHEN @NombreParametro IS NULL THEN NombreParametro ELSE @NombreParametro + '%'
	END
	AND ValorParametro LIKE
		CASE
			WHEN @ValorParametro IS NULL THEN ValorParametro ELSE @ValorParametro + '%'
		END
	AND Estado =
		CASE
			WHEN @Estado IS NULL THEN Estado ELSE @Estado 
		END
	AND Funcionalidad LIKE CASE
		WHEN @Funcionalidad IS NULL THEN Funcionalidad ELSE @Funcionalidad + '%'
	END
	
END


