﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Eliminar]
	@IdTipoClausula INT
AS
BEGIN
	DELETE Contrato.TipoClausula WHERE IdTipoClausula = @IdTipoClausula
END

