﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 12:02:38 PM
-- Description:	Procedimiento almacenado que consulta un(a) RamaoEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_RamaoEstructura_Consultar]
	@IdRamaEstructura INT
AS
BEGIN
 SELECT IdRamaEstructura, CodigoRamaEstructura, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Proveedor].[RamaoEstructura] WHERE  IdRamaEstructura = @IdRamaEstructura
END

