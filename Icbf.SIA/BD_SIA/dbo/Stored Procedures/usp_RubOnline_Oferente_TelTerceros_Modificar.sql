﻿


-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/4/2013 11:29:17 AM
-- Description:	Procedimiento almacenado que actualiza un(a) TelTerceros
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Oferente_TelTerceros_Modificar]
		@IdTelTercero INT,	
		@IdTercero INT=NULL,	
		@IndicativoTelefono INT=NULL,	
		@NumeroTelefono INT=NULL,	
		@ExtensionTelefono BIGINT=NULL,	
		@Movil BIGINT=NULL,	
		@IndicativoFax INT=NULL,	
		@NumeroFax INT=NULL, 
		@UsuarioModifica NVARCHAR(250)=NULL
AS
BEGIN
	UPDATE Tercero.TelTerceros 
	  SET 
		 IdTercero = ISNULL( @IdTercero, IdTercero ),
		 IndicativoTelefono = @IndicativoTelefono,
		 NumeroTelefono =  @NumeroTelefono,
		 ExtensionTelefono =@ExtensionTelefono,
		 Movil = @Movil,
		 IndicativoFax = @IndicativoFax,
		 NumeroFax = @NumeroFax,
		 UsuarioModifica =@UsuarioModifica,
		 FechaModifica = GETDATE() 
	 WHERE IdTelTercero = @IdTelTercero
END



