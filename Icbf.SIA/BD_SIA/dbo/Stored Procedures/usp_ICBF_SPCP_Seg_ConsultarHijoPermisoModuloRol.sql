﻿
-- =============================================
-- Author:		Jorge Vizcaino
-- Create date: 19-04-2013
-- Description:	Procedimiento almacenado que consulta los permisos hijos de un Rol por Identificador del Módulo y Nombre de Rol
-- Grupo de Apoyo SSII - SIGEPCYP 31/07/2015
-- =============================================

CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarHijoPermisoModuloRol]
	@nombreRol NVARCHAR(250),
	@IdModulo INT,
	@ID int = null
AS
BEGIN

	if (@ID IS NULL) 
	BEGIN		
		SELECT 
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		FROM 
			SEG.Permiso PR
		INNER JOIN 
			SEG.Rol R ON PR.IdRol = R.IdRol
		INNER JOIN
			SEG.Programa P ON PR.IdPrograma = P.IdPrograma
		INNER JOIN
			SEG.Modulo M ON P.IdModulo = M.IdModulo			
		WHERE
			R.Nombre = @nombreRol	
		AND
			M.IdModulo = @IdModulo
		AND
			(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)	
		AND
			P.VisibleMenu = 1			
		AND 
			P.padre IS NULL
		GROUP BY
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		ORDER BY
			P.Posicion DESC	
	END
	ELSE 
	BEGIN
		SELECT 
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		FROM 
			SEG.Permiso PR
		INNER JOIN 
			SEG.Rol R ON PR.IdRol = R.IdRol
		INNER JOIN
			SEG.Programa P ON PR.IdPrograma = P.IdPrograma
		INNER JOIN
			SEG.Modulo M ON P.IdModulo = M.IdModulo			
		WHERE
			R.Nombre = @nombreRol	
		AND
			M.IdModulo = @IdModulo
		AND
			(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)	
		AND
			P.VisibleMenu = 1	
		AND 
			P.padre = @ID 
		GROUP BY
			P.IdPrograma, P.IdModulo, P.NombrePrograma, P.CodigoPrograma, P.Posicion, P.Estado, P.UsuarioCreacion, P.FechaCreacion, P.UsuarioModificacion, P.FechaModificacion, M.NombreModulo, P.VisibleMenu
		ORDER BY
			P.Posicion DESC	
	END 


END

