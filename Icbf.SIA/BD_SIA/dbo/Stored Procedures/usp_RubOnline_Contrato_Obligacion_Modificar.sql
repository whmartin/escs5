﻿
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         15/06/2013 08:10
   -- Description:          Procedimiento almacenado que actualiza Obligacion
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_Obligacion_Modificar]
		@IdObligacion INT,	@IdTipoObligacion INT,	@IdTipoContrato INT,	
		@Descripcion nvarchar(MAX),	
		@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.Obligacion SET IdTipoObligacion = @IdTipoObligacion, IdTipoContrato = @IdTipoContrato, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdObligacion = @IdObligacion
END



