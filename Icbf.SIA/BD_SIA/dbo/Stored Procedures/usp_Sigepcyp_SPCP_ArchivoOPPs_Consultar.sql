﻿


-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 - Jorge Vizcaino
-- Create date:  8/20/2015 11:35:43 AM
-- Description:	Procedimiento almacenado que consulta y genera un(a) ArchivoOPP
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ArchivoOPPs_Consultar]
	@IdsCuentaCobro nvarchar(2000) 
AS

BEGIN
	DECLARE @TempMaestroCargarOPP as table(id int identity(1,1),
									 Consecutivo int
									,PCI Nvarchar(255)
									,FechaRegistro Date
									,ConsecutivoObligacion	Nvarchar(255)
									,CodDependenciaAfectacionGasto Nvarchar(255)
									,CodPosicionPAC Nvarchar(10)
									,FechaPago Date
									,ValorTotalLinea Numeric(18,2)
									,CodTipoBeneficiario Nvarchar(10)
									,CodMedioPago Nvarchar(8)
									,CodPCITesoreria Nvarchar(255)
									,CodTipoDocBeneficiario Nvarchar(10)
									,NumDocIdenBeneficiario Nvarchar(20)
									,NumeroCuentaBen Nvarchar(255)
									,TipoCuentaBen Nvarchar(255)
									,FechaLimitePago date
									,CodTipoDocSoporte int
									,NumDocSoporte Nvarchar(40)
									,FechaDocSoporte Date
									,Expendidor Nvarchar(8)
									,NombreFuncionario Nvarchar(255)
									,CargoFuncionario Nvarchar(255)
									,Notas Nvarchar(255)
									,IDDatosAdmin int
									,IDCuentaCobro int
									)


SET DATEFORMAT YMD
	INSERT INTO @TempMaestroCargarOPP
	SELECT  ROW_NUMBER() over(ORDER BY cc.IdCuenta)  as Consecutivo
			--cc.IdCuenta  as Consecutivo
			,da.Codpci as PCI	
			,convert(VARCHAR(8),cc.FechaCrea,112) as FECHADEREGISTRO	
			,cc.NumeroObligacionSIIF as ConsecutivoObligacion	
			,da.Codpci as CodDependenciaAfectacionGasto
			,'9-1.' as CodPosicionPAC
			,cast(GetDate() as date) as FECHADEPAGO
			,0    as ValorTotalLinea
			,'B'  as CodTipoBeneficiario
			,'AC' as CodMedioPago
			,''   as CodPCITesoreria
			,td.CodSIIF as CodTipoDocBeneficiario
			,da.NumDocIdentidad as NumDocIdenBeneficiario
			,Convert(varchar,da.NumCtaBancaria) as NumeroCuentaBen
			,(CASE da.NomTipoCta WHEN 'AHORRO' THEN 'AHR'
			  when 'CORRIENTE' THEN 'CRR'
			  END ) as TipoCuentaBen -- es  codigo
			,DATEADD(DAY,5,getdate()) as FechaLimitePago
			,da.CodTipoDocSoporte 
			,da.NumDocSoporte
			,cast(da.FechaDocSoporte as DATE) as FechaDocSoporte
			,'11' as Expendidor
			,'' as NombreFuncionario
			,'' as CargoFuncionario
			,'Creacion OPP' as Notas
			,da.IdDatosAdministracion
			,cc.IdCuenta
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	WHERE cc.IdCuenta IN (SELECT data FROM dbo.Split(@IdsCuentaCobro,';'))


	SELECT  Consecutivo 
			,PCI 
			,FechaRegistro 
			,ConsecutivoObligacion	
			,CodDependenciaAfectacionGasto 
			,CodPosicionPAC 
			,FechaPago 
			,ValorTotalLinea 
			,CodTipoBeneficiario 
			,CodMedioPago 
			,CodPCITesoreria 
			,CodTipoDocBeneficiario 
			,NumDocIdenBeneficiario 
			,NumeroCuentaBen 
			,TipoCuentaBen 
			,FechaLimitePago 
			,CodTipoDocSoporte 
			,NumDocSoporte 
			,FechaDocSoporte 
			,Expendidor 
			,NombreFuncionario 
			,CargoFuncionario 
			,Notas 
	FROM @TempMaestroCargarOPP

	UPDATE SPCP.CuentasdeCobro set [GeneradoOPP] = 1
	FROM @TempMaestroCargarOPP tmopp
	WHERE SPCP.CuentasdeCobro.IdCuenta = tmopp.IDCuentaCobro

	SELECT opp.Consecutivo
			,ROW_NUMBER() over(ORDER BY opp.id)  as NumRegistro
			,da.Codpci as DepAfectaGasto
			,pp.Rubro as codPosicionGasto	
			,(CASE pp.Fuente WHEN 'NACION' THEN '01'
			  when 'PROPIOS' THEN '02'
			  END ) as codFuenteFinanciacion	
			,pp.Recurso as codRecursoPresupuestal	
			,(CASE p.NomSituacionFondos WHEN 'CSF' THEN '01'
			  when 'SSF' THEN '02'
			  END )  as codSituacionFondos	
			,convert(NUMERIC(30,2),pp.ValorDelPago) as valorEnPesos	
	FROM @TempMaestroCargarOPP opp
	INNER JOIN SPCP.DatosAdministracion da
	ON opp.IDDatosAdmin = da.IdDatosAdministracion
	INNER JOIN SPCP.Presupuestos p
	ON da.IdDatosAdministracion = p.IdDatosAdministracion
	INNER JOIN SPCP.CuentasdeCobro cc
	on opp.IDCuentaCobro = cc.IdCuenta
	INNER JOIN SPCP.PlanDePagos pp
	ON cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion

END

