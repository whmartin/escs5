﻿-- =============================================
-- Author:		Faiber Losada Zuñiga
-- Create date:  6/10/2013 10:14:41 AM
-- Description:	Procedimiento almacenado que elimina un(a) TipoSectorEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoSectorEntidad_Eliminar]
	@IdTipoSectorEntidad INT
AS
BEGIN
	DELETE Proveedor.TipoSectorEntidad WHERE IdTipoSectorEntidad = @IdTipoSectorEntidad
END

