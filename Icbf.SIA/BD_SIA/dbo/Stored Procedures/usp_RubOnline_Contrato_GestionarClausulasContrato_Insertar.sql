﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarClausulasContrato_Insertar]
		@IdGestionClausula INT OUTPUT, 	@IdContrato NVARCHAR(3),	@NombreClausula NVARCHAR(10),	@TipoClausula INT,	@Orden NVARCHAR(128), @OrdenNumero INT, @DescripcionClausula NVARCHAR(128), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.GestionarClausulasContrato(IdContrato, NombreClausula, TipoClausula, Orden, OrdenNumero, DescripcionClausula, UsuarioCrea, FechaCrea)
				VALUES(@IdContrato, @NombreClausula, @TipoClausula, @Orden,  @OrdenNumero, @DescripcionClausula, @UsuarioCrea, GETDATE())
	SELECT @IdGestionClausula = @@IDENTITY 		
END

