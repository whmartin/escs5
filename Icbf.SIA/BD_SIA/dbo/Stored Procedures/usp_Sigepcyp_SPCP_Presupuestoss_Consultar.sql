﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  3/12/2015 2:21:15 PM
-- Description:	Procedimiento almacenado que consulta un(a) Presupuestos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Presupuestoss_Consultar]
	@IdDatosAdministracion NVARCHAR(255) = NULL,@CodPosicionGasto NVARCHAR(255) = NULL,@NomPosicionGasto NVARCHAR(255) = NULL,
	@NomFuenteFinanciacion NVARCHAR(255) = NULL,@CodRecursoPresupuestal INT = NULL,@NomRecursoPresupuestal NVARCHAR(255) = NULL,
	@NomSituacionFondos NVARCHAR(255) = NULL,
	@ValorInicialRubro decimal=NULL,
	@ValorActualRubro decimal=NULL,
	@ValorSaldoRubro decimal=NULL
AS
BEGIN
Declare @SqlExec as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','
Set @SqlParametros = '@IdDatosAdministracion NVARCHAR(255),@CodPosicionGasto NVARCHAR(255),@NomPosicionGasto NVARCHAR(255),
@NomFuenteFinanciacion NVARCHAR(255),@CodRecursoPresupuestal INT,@NomRecursoPresupuestal NVARCHAR(255),
@NomSituacionFondos NVARCHAR(255),@ValorInicialRubro,@ValorActualRubro,@ValorSaldoRubro '
Set @SqlExec = '
 SELECT IdPresupuesto, IdDatosAdministracion, CodPosicionGasto, NomPosicionGasto, NomFuenteFinanciacion, CodRecursoPresupuestal, 
 NomRecursoPresupuestal, NomSituacionFondos, ValorInicialRubro,ValorActualRubro,ValorSaldoRubro
 UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[Presupuestos] WHERE 1=1 '
 If(@IdDatosAdministracion Is Not Null) Set @SqlExec = @SqlExec + ' And IdDatosAdministracion = @IdDatosAdministracion' 
 If(@CodPosicionGasto Is Not Null) Set @SqlExec = @SqlExec + ' And CodPosicionGasto = @CodPosicionGasto' 
 If(@NomPosicionGasto Is Not Null) Set @SqlExec = @SqlExec + ' And NomPosicionGasto = @NomPosicionGasto' 
 If(@NomFuenteFinanciacion Is Not Null) Set @SqlExec = @SqlExec + ' And NomFuenteFinanciacion = @NomFuenteFinanciacion' 
 If(@CodRecursoPresupuestal Is Not Null) Set @SqlExec = @SqlExec + ' And CodRecursoPresupuestal = @CodRecursoPresupuestal' 
 If(@NomRecursoPresupuestal Is Not Null) Set @SqlExec = @SqlExec + ' And NomRecursoPresupuestal = @NomRecursoPresupuestal' 
 If(@NomSituacionFondos Is Not Null) Set @SqlExec = @SqlExec + ' And NomSituacionFondos = @NomSituacionFondos' 
 If(@ValorInicialRubro Is Not Null) Set @SqlExec = @SqlExec + ' And ValorInicialRubro = @ValorInicialRubro' 
 If(@ValorActualRubro Is Not Null) Set @SqlExec = @SqlExec + ' And ValorInicialRubro = @ValorActualRubro' 
 If(@ValorSaldoRubro Is Not Null) Set @SqlExec = @SqlExec + ' And ValorInicialRubro = @ValorSaldoRubro' 
Exec sp_executesql  @SqlExec, @SqlParametros,@IdDatosAdministracion,@CodPosicionGasto,@NomPosicionGasto,@NomFuenteFinanciacion,
@CodRecursoPresupuestal,@NomRecursoPresupuestal,@NomSituacionFondos,@ValorInicialRubro,@ValorActualRubro,@ValorSaldoRubro
END
