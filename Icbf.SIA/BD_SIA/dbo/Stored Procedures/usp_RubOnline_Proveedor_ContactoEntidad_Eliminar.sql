﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/16/2013 12:40:25 PM
-- Description:	Procedimiento almacenado que elimina un(a) ContactoEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ContactoEntidad_Eliminar]
	@IdContacto INT
AS
BEGIN
	DELETE Proveedor.ContactoEntidad WHERE IdContacto = @IdContacto
END

