﻿


-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que guarda un nuevo Integrantes
-- Modificación: Juan Carlos Valverde Sámano
-- Fecha: 08/07/2014
-- Descripción: En la validación de que ya existe el integrante asociado, hacia falta comprobar con IdEntidad, Solo comprobaba por NumIdentificación.
-- Modificación: Juan Carlos Valverde Sámano
-- Fecha: 06/08/2014
-- Descripción: se cambió la funcion @@IDENTITY por SCOPE_IDENTITY(), ya que al insertar en esta Tabla se ejecuta un trigger, que inserta en 
-- otra tabla, entonces @@Identity nos estaba devolviendo el Id del insertado por el trigger y no el de este procedimiento.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Insertar]
		@IdIntegrante INT OUTPUT, @IdEntidad INT,	@IdTipoPersona INT,	@IdTipoIdentificacionPersonaNatural INT,	@NumeroIdentificacion NVARCHAR(50),	@PorcentajeParticipacion NUMERIC(5,2),	@ConfirmaCertificado NVARCHAR(5),	@ConfirmaPersona NVARCHAR(5), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM PROVEEDOR.Integrantes WHERE NumeroIdentificacion=@NumeroIdentificacion AND IdEntidad=@IdEntidad)
	BEGIN
		RAISERROR('Este integrante ya fue asociado a este Proveedor.',16,1)
	    RETURN
	END
	INSERT INTO PROVEEDOR.Integrantes(IdTipoPersona, IdEntidad, IdTipoIdentificacionPersonaNatural, NumeroIdentificacion, PorcentajeParticipacion, ConfirmaCertificado, ConfirmaPersona, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoPersona, @IdEntidad, @IdTipoIdentificacionPersonaNatural, @NumeroIdentificacion, @PorcentajeParticipacion, @ConfirmaCertificado, @ConfirmaPersona, @UsuarioCrea, GETDATE())
	SELECT @IdIntegrante = SCOPE_IDENTITY() 		
END


