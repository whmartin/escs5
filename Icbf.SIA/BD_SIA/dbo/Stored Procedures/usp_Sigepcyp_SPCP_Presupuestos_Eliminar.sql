﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  3/12/2015 2:21:15 PM
-- Description:	Procedimiento almacenado que elimina un(a) Presupuestos
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Presupuestos_Eliminar]
	@IdPresupuesto INT
AS
BEGIN
	DELETE SPCP.Presupuestos WHERE IdPresupuesto = @IdPresupuesto
END
