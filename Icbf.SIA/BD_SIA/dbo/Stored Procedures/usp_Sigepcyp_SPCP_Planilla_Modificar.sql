﻿

-- =============================================
-- Author:		Jorge Vizcaino
-- Create date:  7/18/2015 3:21:49 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Planilla
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Planilla_Modificar]
		@IDPlanilla INT,	
		@IdRegional INT,	
		@IdArea INT, 
		@Impreso bit,
		@UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.Planilla
	SET	IdRegional = @IdRegional,
		IdArea = @IdArea,
		UsuarioModifica = @UsuarioModifica,
		FechaModifica = GETDATE(),
		Impreso = @Impreso
	WHERE IDPlanilla = @IDPlanilla
END
