﻿
-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  6/10/2013 3:25:43 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_SEG_Parametro_Modificar]
		@IdParametro INT,	@NombreParametro NVARCHAR(128),	@ValorParametro NVARCHAR(256),	
		@ImagenParametro NVARCHAR(256) = NULL,	@Estado BIT, @Funcionalidad NVARCHAR(128), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Parametro 
	SET NombreParametro = @NombreParametro, ValorParametro = @ValorParametro, 
	ImagenParametro = ISNULL(@ImagenParametro,ImagenParametro), 
	Estado = @Estado, Funcionalidad = @Funcionalidad,
	UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() 
	WHERE IdParametro = @IdParametro
END

