﻿-- =============================================
-- Author:		ICBF\Yuri.Gereda
-- Create date:  07/02/2013 03:47:22 p.m.
-- Description:	Procedimiento almacenado que elimina un(a) TituloObtenido
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_TituloObtenido_Eliminar]
	@IdTituloObtenido INT
AS
BEGIN
	DELETE Global.TituloObtenido WHERE IdTituloObtenido = @IdTituloObtenido
END

