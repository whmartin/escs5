﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_FormaPago_Consultar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_FormaPago_Consultar]
	@IdFormapago INT
AS
BEGIN
 SELECT IdFormapago, NombreFormaPago, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[FormaPago] WHERE  IdFormapago = @IdFormapago
END

