﻿

-- =============================================
-- Author:		Fabian Valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TipoDocumentoPrograma
-- Modificación: 16/06/2014 Juan Carlos Valverde Sámano
-- Descripción:  Se agregó el parametro @ObligConsorcioUnion
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumentoPrograma_Modificar]
@IdTipoDocumentoPrograma INT, @IdTipoDocumento INT, @IdPrograma INT, @Estado INT, @MaxPermitidoKB INT, @ExtensionesPermitidas NVARCHAR (50), @ObligRupNoRenovado INT, @ObligRupRenovado INT, @ObligPersonaJuridica INT, @ObligPersonaNatural INT, @ObligSectorPrivado INT, @ObligSectorPublico INT, @UsuarioModifica NVARCHAR (250)
,@ObligEntidadNacioanl BIT, @ObligEntidadExtrajera BIT, @ObligConsorcio BIT, @ObligUnionTemporal BIT
AS
BEGIN
UPDATE Proveedor.TipoDocumentoPrograma
SET	IdTipoDocumento = @IdTipoDocumento,
	IdPrograma = @IdPrograma,
	Estado = @Estado,
	MaxPermitidoKB = @MaxPermitidoKB,
	ExtensionesPermitidas = @ExtensionesPermitidas,
	ObligRupNoRenovado = @ObligRupNoRenovado,
	ObligRupRenovado = @ObligRupRenovado,
	ObligPersonaJuridica = @ObligPersonaJuridica,
	ObligPersonaNatural = @ObligPersonaNatural,
	ObligSectorPublico = @ObligSectorPublico,
	ObligSectorPrivado = @ObligSectorPrivado,
	ObligEntNacional=@ObligEntidadNacioanl,
	ObligEntExtranjera=@ObligEntidadExtrajera,
	ObligConsorcio= @ObligConsorcio,
	ObligUnionTemporal = @ObligUnionTemporal,
	UsuarioModifica = @UsuarioModifica,
	FechaModifica = GETDATE()
WHERE IdTipoDocumentoPrograma = @IdTipoDocumentoPrograma
END


