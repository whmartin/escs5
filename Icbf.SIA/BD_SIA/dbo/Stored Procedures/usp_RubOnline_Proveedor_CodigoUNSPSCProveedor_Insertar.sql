﻿-- =============================================
-- Author:		Jonnathan Niño
-- Create date:  7/15/2013 11:30:19 PM
-- Description:	Procedimiento almacenado que guarda un nuevo ExperienciaCodUNSPSCEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_CodigoUNSPSCProveedor_Insertar]
		@CodUNSPSC INT OUTPUT
		,@IdTercero INT
		,@IdTipoCodUNSPSC INT
		,@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.ExperienciaCodUNSPSCEntidad(IdTipoCodUNSPSC, IdExpEntidad, UsuarioCrea, FechaCrea)
					  VALUES(@IdTipoCodUNSPSC, @IdTercero, @UsuarioCrea, GETDATE())
	SELECT @CodUNSPSC = @@IDENTITY 		
END
