﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Description:	Procedimiento almacenado que consulta formatos de archivo

-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_Archivo_ConsultarList_GCB]
   @FechaRegistro DATETIME 
   ,@Usuario NVARCHAR(250) = NULL
   ,@IdLlave INT
   ,@IdFormatoArchivo INT
   ,@Sigla NVARCHAR(3)
AS
BEGIN
 SELECT IdArchivo
      , IdUsuario
      , [Archivo].[IdFormatoArchivo]
      , FechaRegistro
      , NombreArchivo
      , [Archivo].[Estado]
      , ResumenCarga
      , [Archivo].[UsuarioCrea]
      , [Archivo].[FechaCrea]
      , [Archivo].[UsuarioModifica]
      , [Archivo].[FechaModifica]
      , [TipoEstructura].[NombreEstructura]
 FROM [Estructura].[Archivo] Inner Join [Estructura].[FormatoArchivo] on [Archivo].[IdFormatoArchivo]=[FormatoArchivo].[IdFormatoArchivo]
      Inner Join [Estructura].[TipoEstructura] on [FormatoArchivo].[IdTipoEstructura]=[TipoEstructura].[IdTipoEstructura]
 WHERE  DATEDIFF(dd, @FechaRegistro, CASE WHEN @FechaRegistro = '1900/01/01' THEN @FechaRegistro ELSE [FechaRegistro] END) = 0
        And @Usuario = CASE WHEN Len(@Usuario) = 0  THEN @Usuario ELSE [Archivo].[UsuarioCrea] END
        AND [Archivo].[IdFormatoArchivo]=@IdFormatoArchivo
        AND NombreArchivo like '%_' + @Sigla + CONVERT(NVARCHAR(256),@IdLlave) + '_%'
END

