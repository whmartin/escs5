﻿
-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que consulta un usuario por documento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarioPorDocumento]
	@pIdTipoDocumento INT
	,@pNumeroDocumento NVARCHAR(17)
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
	  ,SEG.Usuario.IdRegional
	  ,SEG.Usuario.TodosRegional
	  ,SEG.Usuario.AceptaEnvioCorreos
  FROM SEG.Usuario 
  WHERE
	SEG.Usuario.IdTipoDocumento = @pIdTipoDocumento
    AND SEG.Usuario.NumeroDocumento = @pNumeroDocumento
END


