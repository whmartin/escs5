﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/23/2013 7:11:00 PM
-- Description:	Procedimiento almacenado que actualiza un(a) InformacionPresupuestal
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestal_Modificar]
		@IdInformacionPresupuestal INT,	@NumeroCDP INT,	@ValorCDP NUMERIC(18,3),	@FechaExpedicionCDP DATETIME, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.InformacionPresupuestal SET NumeroCDP = @NumeroCDP, ValorCDP = @ValorCDP, FechaExpedicionCDP = @FechaExpedicionCDP, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdInformacionPresupuestal = @IdInformacionPresupuestal
END

