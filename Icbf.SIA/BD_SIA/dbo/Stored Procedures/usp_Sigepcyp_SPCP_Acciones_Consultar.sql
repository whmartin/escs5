﻿
--- =================================================================
-- Author:		<Author,,Grupo Desarrollo SIGEPCYP>
-- Create date: <8/20/2015 8:24:06 PM,>
-- Description:	<Consultar Información de Una Acción,>
-- ====================================================================

CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_Acciones_Consultar]
	@IdAcciones INT
AS
BEGIN
 SELECT IdAcciones, Descripcion, Estado, CodAccion, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [SPCP].[Acciones] WHERE  IdAcciones = @IdAcciones
END
