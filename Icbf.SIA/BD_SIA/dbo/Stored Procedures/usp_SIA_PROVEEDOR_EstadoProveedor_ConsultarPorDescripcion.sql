﻿
-- =============================================
-- Author:		Juan Carlos Valverde Sámano
-- Create date:  09/07/2014
-- Description:	Procedimiento almacenado que consulta un
-- EstadoProveedor a partir de su descripción.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_EstadoProveedor_ConsultarPorDescripcion]
	@descripcionEstado NVARCHAR(50)
AS
BEGIN
SELECT IdEstadoProveedor, Descripcion, Estado FROM PROVEEDOR.EstadoProveedor
WHERE Descripcion=@descripcionEstado
END


