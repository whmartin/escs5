﻿


-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/27/2015 10:16:23 AM
-- Description:	Procedimiento almacenado que consulta un(a) AsignarCodigoSIIFcc
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_AsignarCodigoSIIFccs_Consultar]
	@IdRegional int = NULL,
    @IdCuentaCobro int = NULL,
	@IdPlanilla int = NULL,
    @IdTipoIdentificacion int = NULL,
    @NumeroIdentificacion nvarchar(50) = NULL,
	@EstadoConsulta INT = 1
AS
BEGIN

	Declare @IdPasoAgregarCodigo int
			,@ValoConsultar int

	--SELECT	@IdPasoAgregarCodigo = ap.IdPaso
	--FROM SPCP.AccionPaso ap
	--INNER JOIN SPCP.Acciones a
	--ON ap.IdAccion = a.IdAcciones
	--WHERE a.CodAccion ='AsigCod'

	SELECT @IdPasoAgregarCodigo  = IdPaso
	FROM SPCP.Pasos
	WHERE Paso = 'GENERAR ARCHIVO CXP'


	SELECT cc.IdCuenta as numeroCuenta
			,td.NomTipoDocumento
			,da.NumDocIdentidad
			,da.NomTercero 
			,pd.IdPlanilla as NumeroPlanilla
			,cc.CodigoCuentaPorPagarSIIF
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN SPCP.PlanillaDetalle pd
	ON cc.IdCuenta = pd.IdCuenta
	INNER JOIN SPCP.FlujoPasosCuenta fp
	ON cc.IdEstadoPaso = fp.IdFlujoPasoCuenta
	INNER JOIN Global.TiposDocumentos td
	ON da.TipoDocumento = td.IdTipoDocumento
	INNER JOIN SPCP.Planilla p
	ON pd.IdPlanilla = p.IDPlanilla	
	WHERE fp.IdPaso = @IdPasoAgregarCodigo
	AND p.IdRegional = isnull(@IdRegional,p.IdRegional)
	AND cc.IdCuenta = ISNULL(@IdCuentaCobro,cc.IdCuenta)
	AND p.IDPlanilla = ISNULL(@IdPlanilla,p.IDPlanilla)
	AND da.TipoDocumento = ISNULL(@IdTipoIdentificacion,da.TipoDocumento)
	AND da.NumDocIdentidad = ISNULL(@NumeroIdentificacion,da.NumDocIdentidad)
	AND cc.CodigoCuentaPorPagarSIIF != CASE WHEN (@EstadoConsulta = 1 OR @EstadoConsulta = 3)
											THEN '-3'  -- valo caulquiera para que tariga todos los registro
											WHEN @EstadoConsulta = 2 --Estados Asignados
											THEN 0 END

	AND cc.CodigoCuentaPorPagarSIIF = CASE WHEN (@EstadoConsulta = 1 OR @EstadoConsulta = 2)
											THEN cc.CodigoCuentaPorPagarSIIF
											WHEN @EstadoConsulta = 3 --Estados Sin Asignar
											THEN '0' END


END


