﻿
-- =============================================
-- Author:		Generado Automaticamente
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que actualiza un(a) Permiso
-- Grupo de Apoyo SSII - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ModificarPermiso]
		@IdPermiso INT,	@IdPrograma INT,	@IdRol INT,	@Insertar BIT,	@Modificar BIT,	@Eliminar BIT,	@Consultar BIT, @UsuarioModificacion NVARCHAR(250)
AS
BEGIN
	UPDATE SEG.Permiso SET IdPrograma = @IdPrograma, IdRol = @IdRol, Insertar = @Insertar, Modificar = @Modificar, Eliminar = @Eliminar, Consultar = @Consultar, UsuarioModificacion = @UsuarioModificacion, FechaModificacion = GETDATE() WHERE IdPermiso = @IdPermiso
END

