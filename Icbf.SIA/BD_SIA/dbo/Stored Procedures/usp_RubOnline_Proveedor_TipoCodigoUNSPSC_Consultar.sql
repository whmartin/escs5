﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/22/2013 1:07:57 AM
-- Description:	Procedimiento almacenado que consulta un(a) TipoCodigoUNSPSC
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoCodigoUNSPSC_Consultar]
	@IdTipoCodUNSPSC INT
AS
BEGIN
 SELECT IdTipoCodUNSPSC, Codigo, Descripcion, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica 
 FROM [Proveedor].[TipoCodigoUNSPSC] 
 WHERE  IdTipoCodUNSPSC = @IdTipoCodUNSPSC
END

