﻿-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/13/2013 4:34:06 PM
-- Description:	Procedimiento almacenado que actualiza un(a) EstadoValidacionDocumental
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoValidacionDocumental_Modificar]
		@IdEstadoValidacionDocumental INT,	@CodigoEstadoValidacionDocumental NVARCHAR(128),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.EstadoValidacionDocumental SET CodigoEstadoValidacionDocumental = @CodigoEstadoValidacionDocumental, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdEstadoValidacionDocumental = @IdEstadoValidacionDocumental
END

