﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 3:52:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Pais
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Pais_sConsultar]
	@CodigoPais NVARCHAR(128) = NULL,@NombrePais NVARCHAR(128) = NULL
AS
BEGIN
 SELECT IdPais, 
		CodigoPais, 
		NombrePais, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Pais] WHERE CodigoPais = CASE WHEN @codigoPais IS NULL THEN CodigoPais ELSE @CodigoPais END AND NombrePais = CASE WHEN @NombrePais IS NULL THEN NombrePais ELSE @NombrePais END
END

