﻿-- =============================================
-- Author:		Equipo Sistemas de Información Financiera
-- Create date:  04/03/2015
-- =============================================

CREATE PROCEDURE [dbo].[usp_SPCP_DIV_Regional_sConsultar]
	@CodigoRegional NVARCHAR(2) = NULL
	,@NombreRegional NVARCHAR(256) = NULL
AS
BEGIN
Declare @SqlExec NVarchar(Max)=Null
	Set @SqlExec ='
 SELECT IdRegional, 
		CodigoRegional, 
		NombreRegional,
		CodPCI,
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
 FROM	[DIV].[Regional] 
 WHERE 1 = 1 '
	If(@CodigoRegional Is Not Null And LEN(@CodigoRegional)>0) Set @SqlExec = @SqlExec + ' And CodigoRegional=' + Char(39) + @CodigoRegional + Char(39)
	If(@NombreRegional Is Not NUll And LEN(@NombreRegional)>0) Set @SqlExec = @SqlExec + ' And UPPER(RTRIM(NombreRegional)) =' + Char(39) + UPPER(RTRIM(@NombreRegional)) + Char(39)
	Set @SqlExec = @SqlExec + ' ORDER BY NombreRegional DESC '
	Exec (@SqlExec)
END