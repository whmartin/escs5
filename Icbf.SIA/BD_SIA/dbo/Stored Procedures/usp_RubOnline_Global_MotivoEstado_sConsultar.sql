﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que consulta un(a) MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_sConsultar]
	@CodigoMotivoEstado NVARCHAR(128) = NULL,@NombreMotivoEstado NVARCHAR(256) = NULL,@Estado NVARCHAR(1) = NULL
AS
BEGIN
 SELECT IdMotivoEstado, CodigoMotivoEstado, NombreMotivoEstado, Estado, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Global].[MotivoEstado] WHERE CodigoMotivoEstado = CASE WHEN @CodigoMotivoEstado IS NULL THEN CodigoMotivoEstado ELSE @CodigoMotivoEstado END AND NombreMotivoEstado = CASE WHEN @NombreMotivoEstado IS NULL THEN NombreMotivoEstado ELSE @NombreMotivoEstado END AND Estado = CASE WHEN @Estado IS NULL THEN Estado ELSE @Estado END
END


