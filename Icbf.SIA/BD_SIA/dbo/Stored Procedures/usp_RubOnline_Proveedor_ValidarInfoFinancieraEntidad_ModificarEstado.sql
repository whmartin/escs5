﻿




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado del EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoFinancieraEntidad_ModificarEstado]
		@IdInfoFin INT, @IdEstadoInfoFinancieraEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	update [Proveedor].[InfoFinancieraEntidad]
	SET EstadoValidacion = @IdEstadoInfoFinancieraEntidad,
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdInfoFin = @IdInfoFin
END






