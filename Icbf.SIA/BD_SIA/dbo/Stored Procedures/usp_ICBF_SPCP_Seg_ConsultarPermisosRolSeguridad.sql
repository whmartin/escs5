﻿
-- =============================================
-- Author:		Yuri Gereda
-- Create date:  13/11/2012 10:38:00 a.m.
-- Description:	Procedimiento almacenado que consulta los permisos de un Rol por Nombre de Rol
-- Modificado Por:	Zulma Barrantes
-- Create date:  11/03/2016 
-- Description:	Se ajusta para que los usuarios de SIA tengan un mneú diferente al mismo Rol de SIGEPCYP
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 31/07/2015
-- =============================================
CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarPermisosRolSeguridad]
	@nombreRol NVARCHAR(250)
AS
BEGIN

SELECT 
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion
FROM 
	SEG.Permiso PR
INNER JOIN 
	SEG.Rol R ON PR.IdRol = R.IdRol
INNER JOIN
	SEG.Programa P ON PR.IdPrograma = P.IdPrograma
INNER JOIN
	SEG.Modulo M ON P.IdModulo = M.IdModulo			
WHERE  M.NombreModulo not in ('Proveedores') and 
	R.Nombre = @nombreRol	
AND
	(PR.Insertar = 1 OR PR.Modificar =1 OR PR.Eliminar = 1 OR PR.Consultar=1)
AND 
	P.VisibleMenu = 1	
GROUP BY
	M.IdModulo, M.NombreModulo, M.Posicion, M.Estado, M.UsuarioCreacion, M.FechaCreacion, M.UsuarioModificacion, M.FechaModificacion	
ORDER BY
	M.Posicion DESC	

END

