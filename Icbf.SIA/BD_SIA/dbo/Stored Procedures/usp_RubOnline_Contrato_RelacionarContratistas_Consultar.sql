﻿

CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Consultar]
@IdContratistaContrato INT
AS
BEGIN
SELECT
	IdContratistaContrato,
	IdContrato,
	NumeroIdentificacion,
	ClaseEntidad,
	PorcentajeParticipacion,
	EstadoIntegrante,
	NumeroIdentificacionRepresentanteLegal,
	UsuarioCrea,
	FechaCrea,
	UsuarioModifica,
	FechaModifica
FROM [Contrato].[RelacionarContratistas]
WHERE IdContratistaContrato = @IdContratistaContrato
END


