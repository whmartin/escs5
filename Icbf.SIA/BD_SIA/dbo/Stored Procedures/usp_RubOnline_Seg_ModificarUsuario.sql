﻿
-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que modifica información de un usuario
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ModificarUsuario]
		  @IdUsuario INT
		, @IdTipoPersona INT
		, @IdTipoDocumento INT
		, @NumeroDocumento NVARCHAR(17)
		, @DV NVARCHAR(1)
		, @PrimerNombre NVARCHAR(150)
		, @SegundoNombre NVARCHAR(150)
		, @PrimerApellido NVARCHAR(150)
		, @SegundoApellido NVARCHAR(150)
		, @RazonSocial NVARCHAR(150)
		, @CorreoElectronico NVARCHAR(150)
		, @TelefonoContacto NVARCHAR(10)
		, @Estado BIT
		, @UsuarioModificacion NVARCHAR(250)
		, @IdTipoUsuario int
		, @IdRegional int
		, @ValidarOferentesMigrados	BIT = 0
		, @TodosRegional bit = NULL
		, @AceptaEnvioCorreos bit = NULL

AS
BEGIN
	UPDATE SEG.Usuario
		SET [IdTipoDocumento] = @IdTipoDocumento
		   ,[NumeroDocumento] = @NumeroDocumento
		   ,[PrimerNombre] = @PrimerNombre
		   ,[SegundoNombre] = @SegundoNombre
		   ,[PrimerApellido] = @PrimerApellido
		   ,[SegundoApellido] = @SegundoApellido
		   ,[CorreoElectronico] = @CorreoElectronico
		   ,[Estado] = @Estado
		   ,[UsuarioModificacion] = @UsuarioModificacion
		   ,[FechaModificacion] = GETDATE()
		   ,[IdTipoPersona] = @IdTipoPersona
		   ,[DV] = @DV
		   ,[RazonSocial] = @RazonSocial
		   ,[TelefonoContacto] = @TelefonoContacto
		   ,IdTipoUsuario = @IdTipoUsuario
		   ,IdRegional = @IdRegional
		   ,ValidarOferentesMigrados = @ValidarOferentesMigrados
		   ,todosregional = @TodosRegional
		   ,AceptaEnvioCorreos = @AceptaEnvioCorreos
	WHERE [IdUsuario] = @IdUsuario
END
