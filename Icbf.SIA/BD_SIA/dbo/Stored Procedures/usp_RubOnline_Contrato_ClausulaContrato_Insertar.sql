﻿
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que inserta un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Insertar]
		@IdClausulaContrato INT OUTPUT, 	
		@NombreClausulaContrato NVARCHAR(128),	
		@IdTipoClausula INT,	
		@Contenido nvarchar(MAX),	
		@IdTipoContrato INT,	
		@Estado BIT, 
		@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Contrato.ClausulaContrato(NombreClausulaContrato, IdTipoClausula, Contenido, IdTipoContrato, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@NombreClausulaContrato, @IdTipoClausula, @Contenido, @IdTipoContrato, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdClausulaContrato = @@IDENTITY 		
END


