﻿

-- =============================================
-- Author:		GoNet\Saul Rodriguez
-- Create date:  26/06/2013 
-- Description:	Procedimiento almacenado que guarda un nuevo Acuerdos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_Acuerdos_Insertar]

@IdAcuerdo INT OUTPUT, 
@Nombre NVARCHAR (64), 
@ContenidoHtml NVARCHAR (MAX), 
@UsuarioCrea NVARCHAR (250)

AS
BEGIN

INSERT INTO Proveedor.Acuerdos (Nombre, ContenidoHtml, UsuarioCrea, FechaCrea)
	VALUES (@Nombre, @ContenidoHtml, @UsuarioCrea, GETDATE())
SELECT
	@IdAcuerdo = SCOPE_IDENTITY()
	
END





