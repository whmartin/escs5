﻿-- =============================================
-- Author:		Leticia Elizabeth González
-- Create date:  01/04/2014 17:17 PM
-- Description:	Procedimiento almacenado que consulta Niveles de Gobiernno por RamaEstructura
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedores_Rel_RamaGobNivelOrgYTipoEnt_ConsultarNivelGobierno]

	@IdRamaEstructura INT
AS
BEGIN

	SELECT	DISTINCT(RelRamaGob.IdNivelGobierno),
			CodigoNivelGobierno,
			Descripcion
	FROM	PROVEEDOR.Rel_RamaGobNivelOrgYTipoEnt RelRamaGob
	JOIN
			Proveedor.NivelGobierno NivelGob
	ON
			RelRamaGob.IdNivelGobierno = NivelGob.IdNivelGobierno
	WHERE	
			RelRamaGob.IdRamaEstructura = @IdRamaEstructura
			
	ORDER BY
			Descripcion ASC

END