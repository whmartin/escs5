﻿-- =============================================
-- Author:		Zulma.Barrantes
-- Create date: 25/03/2015
-- Description:	Consulta Roles de la tabla SEG.Rol se utiliza sp_executesql
-- Grupo de Apoyo SSII - ESQUEMA SPCP - SIGEPCYP 03/08/2015 se agregan el filtro de Director Financiero, Coordinador Regional y Administrador
-- =============================================


CREATE PROCEDURE [dbo].[usp_ICBF_SPCP_Seg_ConsultarRoles]
	 @Nombre NVARCHAR(20)=NULL
	,@Estado BIT=NULL
	,@AprobacionPAC BIT=NULL
	,@IdPaso int =NULL


As

BEGIN

Declare @SQL as NVarchar(Max)
Declare @SqlParametros as NVarchar(3000)=','



Set @SqlParametros = '@Nombre NVARCHAR(20)
	,@Estado BIT
	,@AprobacionPAC Bit
	,@IdPaso int
	'


Set @SQL = '

		SELECT [IdRol]
			,[providerKey]
			,[Nombre]
			,[Descripcion]
			,[IdPaso]
			,[AprobacionPAC]
			,[Estado]
		FROM SEG.Rol 
		WHERE  [Nombre]  <> ''ADMINISTRADOR'' '

			if (@Estado Is Not  Null) 
			BEGIN
				Set @SQL = @SQL + ' AND [Estado] =  @Estado '
			END

			if (@AprobacionPAC Is Not  Null) 
			BEGIN
				Set @SQL = @SQL + ' AND [AprobacionPAC] = @AprobacionPAC ' 
			END

			if (@IdPaso Is Not  Null) 
			BEGIN
				Set @SQL = @SQL + ' AND [IdPaso] = @IdPaso '
			END 

			if (@Nombre Is Not  Null) 
			BEGIN
				Set @SQL = @SQL + ' AND [Nombre] like ''%'+@Nombre+'%'''
			END

---PRINT @SQL


	set	@SQL = @SQL +'	
	order by  [Nombre]'

Exec sp_executesql  @SQL
                    ,@SqlParametros
					,@Nombre 
					,@Estado 
					,@AprobacionPAC 
					,@IdPaso 
					
END





----<> ''DIRECTOR FINANCIERO'' and [Nombre] <> ''COORDINADOR REGIONAL'' and [Nombre]


