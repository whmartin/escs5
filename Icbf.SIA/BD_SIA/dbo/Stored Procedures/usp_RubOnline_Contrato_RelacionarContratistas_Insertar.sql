﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistas_Insertar]
		@IdContratistaContrato INT OUTPUT, 	@IdContrato INT,	@NumeroIdentificacion BIGINT,	@ClaseEntidad NVARCHAR,	@PorcentajeParticipacion INT,	@NumeroIdentificacionRepresentanteLegal BIGINT, @EstadoIntegrante bit, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	DECLARE @VALIDAPORCENTAJE INT
	IF  EXISTS (SELECT IdContrato FROM Contrato.RelacionarContratistas WHERE IdContrato = @IdContrato)
	BEGIN
		SET @VALIDAPORCENTAJE = (SELECT SUM(PorcentajeParticipacion)+@PorcentajeParticipacion  FROM Contrato.RelacionarContratistas
		WHERE IdContrato = @IdContrato)
		IF  @VALIDAPORCENTAJE <= 100 
		BEGIN 
			INSERT INTO Contrato.RelacionarContratistas(IdContrato, NumeroIdentificacion, ClaseEntidad, PorcentajeParticipacion, NumeroIdentificacionRepresentanteLegal, EstadoIntegrante,  UsuarioCrea, FechaCrea)
			VALUES(@IdContrato, @NumeroIdentificacion, @ClaseEntidad, @PorcentajeParticipacion, @NumeroIdentificacionRepresentanteLegal, @EstadoIntegrante,  @UsuarioCrea, GETDATE())
		END
		ELSE
		BEGIN
			RAISERROR ('ERROR',16,1);
			RETURN
		END
	END
	ELSE
	BEGIN	 
		INSERT INTO Contrato.RelacionarContratistas(IdContrato, NumeroIdentificacion, ClaseEntidad, PorcentajeParticipacion, NumeroIdentificacionRepresentanteLegal, EstadoIntegrante,  UsuarioCrea, FechaCrea)
		VALUES(@IdContrato, @NumeroIdentificacion, @ClaseEntidad, @PorcentajeParticipacion, @NumeroIdentificacionRepresentanteLegal, @EstadoIntegrante,  @UsuarioCrea, GETDATE())
	END 
	SELECT @IdContratistaContrato = @@IDENTITY
END


