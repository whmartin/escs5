﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/12/2013 5:47:30 PM
-- Description:	Procedimiento almacenado que elimina un(a) TipodeActividad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipodeActividad_Eliminar]
	@IdTipodeActividad INT
AS
BEGIN
	DELETE Proveedor.TipodeActividad WHERE IdTipodeActividad = @IdTipodeActividad
END

