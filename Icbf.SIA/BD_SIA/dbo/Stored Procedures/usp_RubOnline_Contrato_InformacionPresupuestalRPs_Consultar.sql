﻿-- =============================================
-- Author:		@ReS Cesar Casanova
-- Create date:  7/24/2013 10:12:11 PM
-- Description:	Procedimiento almacenado que consulta un(a) InformacionPresupuestalRP
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_InformacionPresupuestalRPs_Consultar]
	@FechaSolicitudRP DATETIME = NULL,@NumeroRP INT = NULL
AS
BEGIN
 SELECT IdInformacionPresupuestalRP, FechaSolicitudRP, NumeroRP, ValorRP, FechaExpedicionRP, UsuarioCrea, FechaCrea, UsuarioModifica, FechaModifica FROM [Contrato].[InformacionPresupuestalRP] WHERE FechaSolicitudRP = CASE WHEN @FechaSolicitudRP IS NULL THEN FechaSolicitudRP ELSE @FechaSolicitudRP END AND NumeroRP = CASE WHEN @NumeroRP IS NULL THEN NumeroRP ELSE @NumeroRP END
END

