﻿
-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/10/2013 11:53:55 AM
-- Description:	Procedimiento almacenado que actualiza un(a) DocFinancieraProv
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_DocFinancieraProv_Modificar]
		@IdDocAdjunto INT,	@IdInfoFin INT=NULL,	@NombreDocumento NVARCHAR(128),	@LinkDocumento NVARCHAR(256),	@Observaciones NVARCHAR(256), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Proveedor.DocFinancieraProv SET IdInfoFin = @IdInfoFin, NombreDocumento = @NombreDocumento, LinkDocumento = @LinkDocumento, Observaciones = @Observaciones, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdDocAdjunto = @IdDocAdjunto
END


