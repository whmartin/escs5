﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  11/30/2012 3:52:09 PM
-- Description:	Procedimiento almacenado que consulta un(a) Pais
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Pais_Consultar]
	@IdPais INT
AS
BEGIN
 SELECT IdPais, 
		CodigoPais, 
		NombrePais, 
		UsuarioCrea, 
		FechaCrea, 
		UsuarioModifica, 
		FechaModifica 
	FROM [DIV].[Pais] WHERE  IdPais = @IdPais
END

