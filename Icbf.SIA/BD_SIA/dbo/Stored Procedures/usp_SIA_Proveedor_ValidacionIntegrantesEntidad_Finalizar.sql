﻿
-- =============================================
-- Author:		Juan Carlos Valverde
-- Create date:  09/AGO/2014
-- Description:	Procedimiento almacenado que Finaliza el Módulo Integrantes
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_ValidacionIntegrantesEntidad_Finalizar]

	@IdEntidad INT
	
AS
BEGIN


	update Proveedor.ValidacionIntegrantesEntidad    
	set Finalizado = 1
	from Proveedor.ValidacionIntegrantesEntidad e
			inner join Proveedor.ValidarInfoIntegrantesEntidad v
					on v.IdEntidad = e.IdEntidad AND v.NroRevision=e.NroRevision
	where e.IdEntidad = @IdEntidad
		and e.IdEstadoValidacionIntegrantes = 4  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI

END
