﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/17/2013 9:46:36 AM
-- Description:	Procedimiento almacenado que elimina un(a) InfoAdminEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoAdminEntidad_Eliminar]
	@IdInfoAdmin INT
AS
BEGIN
	DELETE Proveedor.InfoAdminEntidad WHERE IdInfoAdmin = @IdInfoAdmin
END

