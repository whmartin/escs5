﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  07/12/2012 10:20:03
-- Description:	Procedimiento almacenado que guarda un nuevo Vigencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Vigencia_Insertar]
		@IdVigencia INT OUTPUT, 	@AcnoVigencia INT,	@Activo NVARCHAR(1), @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.Vigencia(AcnoVigencia, Activo, UsuarioCrea, FechaCrea)
					  VALUES(@AcnoVigencia, @Activo, @UsuarioCrea, GETDATE())
	SELECT @IdVigencia = @@IDENTITY 		
END

