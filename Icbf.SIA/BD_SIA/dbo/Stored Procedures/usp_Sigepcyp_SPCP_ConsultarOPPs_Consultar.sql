﻿

-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  8/20/2015 11:35:43 AM
-- Description:	Procedimiento almacenado que consulta un(a) ArchivoOPP
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_ConsultarOPPs_Consultar]
	@recurso VARCHAR(MAX)=NULL
	,@Indicador VARCHAR(MAX)=NULL
	,@TipoDocumento INT=NULL
	,@FechaCorte DateTime=NULL
AS
BEGIN
	
	
	------cuenta la ocurrencia del ; 
	----DECLARE @IndicadorSub VARCHAR(max), @CountOcurrencia int, @cont int = 0 , @UbicacionChar int = 0;

	----IF (@Indicador IS NOT NULL)
	----	BEGIN
	----	select @CountOcurrencia = (len(@Indicador) - len(replace(@Indicador, ';', ''))) / len(';');

	----	DECLARE @tblIndicadores TABLE (strIndicador VARCHAR(MAX))

	----	--Carga la tabla con las ocurrencias de indicadores encontradas
	----	WHILE @cont < @CountOcurrencia
	----	BEGIN
	----		set @UbicacionChar = charindex(';', @Indicador);
	----		set @IndicadorSub = SUBSTRING (@Indicador, 0, @UbicacionChar);   
	----		INSERT INTO @tblIndicadores(strIndicador) VALUES(@IndicadorSub);
	----		set @Indicador =  SUBSTRING (@Indicador, @UbicacionChar + 1, len(@Indicador));	
	----		set @cont = @cont + 1; 		
	----	END;
	
	----END;

	----select @Indicador 

	----SET @CountOcurrencia  = 0 ;
	----SET @cont  = 0; 
	----SET @UbicacionChar  = 0;

	----IF (@recurso IS NOT NULL)
	----BEGIN

	----	DECLARE @RecursoSub VARCHAR(max);
		
	----	--cuenta la ocurrencia del ; 		
	----	select @CountOcurrencia = (len(@recurso) - len(replace(@recurso, ';', ''))) / len(';');

	----	DECLARE @tblRecursos TABLE (strRecurso VARCHAR(MAX))

	----	--Carga la tabla con las ocurrencias de recursos encontrados
	----	WHILE @cont < @CountOcurrencia
	----	BEGIN
	----		set @UbicacionChar = charindex(';', @recurso);
	----		set @RecursoSub = SUBSTRING (@recurso, 0, @UbicacionChar);   
	----		INSERT INTO @tblRecursos(strRecurso) VALUES(@RecursoSub);
	----		set @recurso =  SUBSTRING (@recurso, @UbicacionChar + 1, len(@recurso));	
	----		set @cont = @cont + 1; 		
	----	END;
	----END;

	SELECT pp.Recurso
			,pp.Rubro as Identificador
			,cc.IdCuenta as NumCuentaCobro
			--,tab1.fechaAprobacion as FechaAprobacionCuenta
			,'01/01/2015' as FechaAprobacionCuenta
			,pp.NumDocSoporte as NumDocFuente
			,p.CodRecursoPresupuestal as NumeroObligacion			
	FROM SPCP.CuentasdeCobro cc
	INNER JOIN SPCP.DatosAdministracion da
	ON cc.IdDatosAdministracion = da.IdDatosAdministracion
	INNER JOIN SPCP.Presupuestos p
	on cc.IdDatosAdministracion =p.IdDatosAdministracion
	INNER JOIN SPCP.PlanDePagos pp
	ON cc.CodCompromiso = pp.CodCompromiso and cc.NumeroDePago = pp.NumeroDePago and cc.IdDatosAdministracion = pp.IdDatosAdministracion
	--INNER JOIN (SELECT max(temhc.fechaCrea) fechaAprobacion,temhc.IDCuentaCobro
	--			from SPCP.HistoricoEstadoCuentasDeCobro temhc				
	--			Group by temhc.IDCuentaCobro) tab1
	--ON cc.IdCuenta = tab1.IDCuentaCobro
	WHERE cc.GeneradoOPP= 0
	--AND tab1.fechaAprobacion = ISNULL(@FechaCorte,tab1.fechaAprobacion)
	AND pp.Recurso in (select data from dbo.Split(isnull(@recurso,pp.Recurso),';')) 
	AND pp.Rubro in (select data from dbo.Split((isnull(@Indicador,pp.Rubro)),';'))  	
	AND da.TipoDocumento = isnull(@TipoDocumento,da.TipoDocumento)
END
