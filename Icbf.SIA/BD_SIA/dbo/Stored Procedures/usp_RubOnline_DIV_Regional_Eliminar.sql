﻿-- =============================================
-- Author:		ICBF\Jose.Molina
-- Create date:  11/12/2012 8:16:01
-- Description:	Procedimiento almacenado que elimina un(a) Regional
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_DIV_Regional_Eliminar]
	@IdRegional INT
AS
BEGIN
	DELETE DIV.Regional WHERE IdRegional = @IdRegional
END

