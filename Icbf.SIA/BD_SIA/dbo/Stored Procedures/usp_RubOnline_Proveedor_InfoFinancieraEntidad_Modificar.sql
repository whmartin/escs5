﻿


-- =============================================
-- Author:		IIS APPPOOL\PoolGenerador
-- Create date:  6/11/2013 10:54:13 AM
-- Description:	Procedimiento almacenado que actualiza un(a) InfoFinancieraEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_InfoFinancieraEntidad_Modificar]
		@IdInfoFin INT,	@IdEntidad INT,	@IdVigencia INT,	@ActivoCte NUMERIC(21, 3),	@ActivoTotal NUMERIC(21, 3),	@PasivoCte NUMERIC(21, 3),	@PasivoTotal NUMERIC(21, 3),	@Patrimonio NUMERIC(21, 3),	@GastosInteresFinancieros NUMERIC(21, 3),	@UtilidadOperacional NUMERIC(21, 3),	@ConfirmaIndicadoresFinancieros BIT,	@RupRenovado BIT,	@EstadoValidacion INT,	@ObservacionesInformacionFinanciera NVARCHAR(256),	@ObservacionesValidadorICBF NVARCHAR(256), @UsuarioModifica NVARCHAR(250), @Finalizado BIT = NULL
AS
BEGIN
	UPDATE Proveedor.InfoFinancieraEntidad SET IdEntidad = @IdEntidad, IdVigencia = @IdVigencia, ActivoCte = @ActivoCte, ActivoTotal = @ActivoTotal, PasivoCte = @PasivoCte, PasivoTotal = @PasivoTotal, Patrimonio = @Patrimonio, GastosInteresFinancieros = @GastosInteresFinancieros, UtilidadOperacional = @UtilidadOperacional, ConfirmaIndicadoresFinancieros = @ConfirmaIndicadoresFinancieros, RupRenovado = @RupRenovado, EstadoValidacion = @EstadoValidacion, ObservacionesInformacionFinanciera = @ObservacionesInformacionFinanciera, ObservacionesValidadorICBF = @ObservacionesValidadorICBF, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() , Finalizado = ISNULL(@Finalizado, Finalizado) WHERE IdInfoFin = @IdInfoFin
END




