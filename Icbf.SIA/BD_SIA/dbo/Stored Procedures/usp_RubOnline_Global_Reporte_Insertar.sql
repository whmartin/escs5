﻿
-- =============================================
-- Author:		Jose.Molina
-- Create date:  22/05/2012 07:00:00 a.m.
-- Description:	SP's para CRUD de Global.Reporte
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Global_Reporte_Insertar]
		 @IdReporte INT OUTPUT
		,@NombreReporte NVARCHAR(512)
		,@Descripcion NVARCHAR(1024)
		,@Servidor NVARCHAR(512)
		,@Carpeta NVARCHAR(512)
		,@NombreArchivo NVARCHAR(512)
		,@UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Global.Reporte(NombreReporte, Descripcion, Servidor, Carpeta, NombreArchivo, UsuarioCrea, FechaCrea)
					  VALUES(@NombreReporte, @Descripcion, @Servidor, @Carpeta, @NombreArchivo, @UsuarioCrea, GETDATE())
	SELECT @IdReporte = @@IDENTITY 		
END
