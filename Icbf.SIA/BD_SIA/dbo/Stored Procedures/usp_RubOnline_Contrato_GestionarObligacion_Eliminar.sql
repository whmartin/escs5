﻿
/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_GestionarObligacion_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_GestionarObligacion_Eliminar]
	@IdGestionObligacion INT
AS
BEGIN
	DELETE Contrato.GestionarObligacion WHERE IdGestionObligacion = @IdGestionObligacion
END
