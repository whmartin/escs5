﻿-- =============================================
-- Author:		ICBF\Fabio.Plata
-- Create date:  12/17/2012 11:47:32 AM
-- Description:	Procedimiento almacenado que elimina un(a) MotivoEstado
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Global_MotivoEstado_Eliminar]
	@IdMotivoEstado INT
AS
BEGIN
	DELETE Global.MotivoEstado WHERE IdMotivoEstado = @IdMotivoEstado
END


