﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0
-- Create date:  6/2/2015 3:05:12 PM
-- Description:	Procedimiento almacenado que actualiza un(a) TiposCuenta
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_TiposCuenta_Modificar]
		@IdTipoCuenta INT,	@TipoCuenta NVARCHAR(50),	@Estado INT,	@IdRegional INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE SPCP.TiposCuenta SET TipoCuenta = @TipoCuenta, Estado = @Estado, IdRegional = @IdRegional, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoCuenta = @IdTipoCuenta
END
