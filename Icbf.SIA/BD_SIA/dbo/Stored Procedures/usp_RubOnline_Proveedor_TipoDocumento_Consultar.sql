﻿

-- =============================================
-- Author:		Fabian valencia
-- Create date:  6/27/2013 7:35:18 PM
-- Description:	Procedimiento almacenado que consulta un(a) TipoDocumento
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_TipoDocumento_Consultar]
	@IdTipoDocumento INT =NULL
AS
BEGIN
	SELECT     Proveedor.TipoDocumento.*
	FROM         Proveedor.TipoDocumento
	WHERE IdTipoDocumento =
	CASE
		WHEN @IdTipoDocumento IS NULL THEN IdTipoDocumento ELSE @IdTipoDocumento
	END
	AND Estado = 1
END


