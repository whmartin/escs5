﻿-- =============================================
-- Author:		ICBF\Juan.Isaquita
-- Create date:  28/12/2012 08:33:18 a.m.
-- Description:	Procedimiento almacenado que guarda un nuevo CorreoElectronico
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Estructura_CorreoElectronico_Insertar]
		@IdCorreoElectronico NUMERIC(18,0) OUTPUT, 	
		@IdArchivo NUMERIC(18,0),	
		@Destinatario NVARCHAR(256),	
		@Mensaje NVARCHAR(4000),	
		@Estado NVARCHAR(1),	
		@FechaIngreso DATETIME,	
		@FechaEnvio DATETIME, 
		@UsuarioCrea NVARCHAR(250),
		@TipoCorreo CHAR(1)
AS
BEGIN
	INSERT INTO Estructura.CorreoElectronico(IdArchivo, Destinatario, Mensaje, Estado, FechaIngreso, FechaEnvio, UsuarioCrea, FechaCrea, TipoCorreo)
					  VALUES(@IdArchivo, @Destinatario, @Mensaje, @Estado, @FechaIngreso, @FechaEnvio, @UsuarioCrea, GETDATE(), @TipoCorreo)
	SELECT @IdCorreoElectronico = @@IDENTITY 		
END

