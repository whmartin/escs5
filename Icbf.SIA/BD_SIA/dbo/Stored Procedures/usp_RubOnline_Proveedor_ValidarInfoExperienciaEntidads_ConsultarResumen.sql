﻿






-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 6:01:36 PM
-- Description:	Procedimiento almacenado que consulta un(a) ValidarInfoExperienciaEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidads_ConsultarResumen]
	@IdEntidad int = NULL
AS
BEGIN
 SELECT v.IdValidarInfoExperienciaEntidad, v.IdExpEntidad, v.NroRevision, v.Observaciones, v.ConfirmaYAprueba, v.UsuarioCrea, v.FechaCrea
 FROM Proveedor.ValidarInfoExperienciaEntidad v
			inner join [Proveedor].[InfoExperienciaEntidad] e on e.IdExpEntidad = v.IdExpEntidad
						inner join [Proveedor].[EntidadProvOferente] p on p.IdEntidad = e.IdEntidad
			where e.IdEntidad  = @IdEntidad
			--and ISNULL(p.NroRevision,1) = ISNULL(v.NroRevision,p.NroRevision)
			ORDER BY v.FechaCrea DESC
END








