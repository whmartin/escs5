﻿
   -- =============================================
   -- Author:                  @ReS\Cesar Casanova
   -- Create date:         11/06/2013 09:33
   -- Description:          Procedimiento almacenado que modifica un(a) ClausulaContrato
   -- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_ClausulaContrato_Modificar]
		@IdClausulaContrato INT,	@NombreClausulaContrato NVARCHAR(128),	@IdTipoClausula INT,	@Contenido nvarchar(MAX),	@IdTipoContrato INT,	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.ClausulaContrato SET NombreClausulaContrato = @NombreClausulaContrato, IdTipoClausula = @IdTipoClausula, Contenido = @Contenido, IdTipoContrato = @IdTipoContrato, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdClausulaContrato = @IdClausulaContrato
END

