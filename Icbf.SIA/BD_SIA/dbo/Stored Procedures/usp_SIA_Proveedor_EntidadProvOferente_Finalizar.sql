﻿-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  04/30/2014 12:20 PM
-- Description:	Procedimiento almacenado que Finaliza EntidadProvOferente
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_Proveedor_EntidadProvOferente_Finalizar]

	@IdEntidad INT
	
AS
BEGIN

	update Proveedor.EntidadProvOferente    
	set Finalizado = 1
	from Proveedor.EntidadProvOferente e
			inner join Proveedor.ValidarInfoDatosBasicosEntidad v
					on v.IdEntidad = e.IdEntidad
	where e.IdEntidad = @IdEntidad
		and IdEstado = 2  -- SI ESTA VALIDADO
		and v.ConfirmaYAprueba = 1 -- Y SI CONFIRMO CON UN SI

END