﻿




-- =============================================
-- Author:		Mauricio Martinez
-- Create date:  2013/07/04 08:47:36 AM
-- Description:	Procedimiento almacenado que actualiza el estado de InfoExperiencia
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ValidarInfoExperienciaEntidad_ModificarEstado]
		@IdExpEntidad INT, @IdEstadoInfoExperienciaEntidad INT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE [Proveedor].[InfoExperienciaEntidad] 
	SET EstadoDocumental = @IdEstadoInfoExperienciaEntidad, 
		UsuarioModifica = @UsuarioModifica, 
		FechaModifica = GETDATE() 
	WHERE IdExpEntidad = @IdExpEntidad
END






