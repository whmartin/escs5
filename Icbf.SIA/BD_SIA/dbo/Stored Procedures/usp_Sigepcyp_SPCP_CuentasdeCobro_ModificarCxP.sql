﻿
-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic
-- Create date:  8/11/2015 5:46:57 PM
-- Description:	Procedimiento almacenado que actualiza un(a) CuentasdeCobro
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobro_ModificarCxP]
		@IdCuenta nvarchar(4000)
		,@GeneradoCxP bit
AS
BEGIN	
	UPDATE	SPCP.CuentasdeCobro 
	SET		GeneradoCxP = @GeneradoCxP
	WHERE IdCuenta in (select data from dbo.Split(@IdCuenta,';'))
END

