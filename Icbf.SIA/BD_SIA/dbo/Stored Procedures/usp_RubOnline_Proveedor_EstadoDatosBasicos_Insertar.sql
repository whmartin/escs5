﻿
-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/19/2013 11:47:47 PM
-- Description:	Procedimiento almacenado que guarda un nuevo EstadoDatosBasicos
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_EstadoDatosBasicos_Insertar]
		@IdEstadoDatosBasicos INT OUTPUT, 	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioCrea NVARCHAR(250)
AS
BEGIN
	INSERT INTO Proveedor.EstadoDatosBasicos(Descripcion, Estado, UsuarioCrea, FechaCrea)
					  VALUES(@Descripcion, @Estado, @UsuarioCrea, GETDATE())
	SELECT @IdEstadoDatosBasicos = SCOPE_IDENTITY() 		
END


