﻿
-- =============================================
-- Author:		ICBF\ JHON DIAZ gOZALEZ
-- Create date: 14-03-2013
-- Description:	Procedimiento almacenado que consulta un(a) TalentoHumano Promotor
-- =============================================
CREATE PROCEDURE [dbo].[usp_SPCP_SEG_TalentoHumanoPromotor_Consultar]
	      @IdContrato INT
AS
BEGIN
SELECT     PAE.TalentoHumano.IdTalentoHumano, 
           PAE.TalentoHumano.PrimerNombre, 
           PAE.TalentoHumano.SegundoNombre, 
           PAE.TalentoHumano.PrimerApellido, 
           PAE.TalentoHumano.SegundoApellido,
           PAE.TalentoHumano.Identificacion
FROM         ECO.Cargo INNER JOIN GCB.ContratoTalentoHumano ON ECO.Cargo.IdCargo = GCB.ContratoTalentoHumano.IdCargo 
					   INNER JOIN PAE.TalentoHumano ON PAE.TalentoHumano.IdTalentoHumano = GCB.ContratoTalentoHumano.IdTalentoHumano
WHERE     (ECO.Cargo.NombreCargo = 'PROMOTOR DE DERECHOS') 
AND (GCB.ContratoTalentoHumano.IdContrato = @IdContrato)

END

