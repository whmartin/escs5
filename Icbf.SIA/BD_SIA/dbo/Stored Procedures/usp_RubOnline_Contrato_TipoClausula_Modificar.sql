﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoClausula_Modificar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoClausula_Modificar]
		@IdTipoClausula INT,	@NombreTipoClausula NVARCHAR(50),	@Descripcion NVARCHAR(128),	@Estado BIT, @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	UPDATE Contrato.TipoClausula SET NombreTipoClausula = @NombreTipoClausula, Descripcion = @Descripcion, Estado = @Estado, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdTipoClausula = @IdTipoClausula
END

