﻿-- =============================================
-- Author:		Faiber Losada
-- Create date:  6/13/2013 7:22:51 PM
-- Description:	Procedimiento almacenado que elimina un(a) ClasedeEntidad
-- =============================================
CREATE PROCEDURE [dbo].[usp_RubOnline_Proveedor_ClasedeEntidad_Eliminar]
	@IdClasedeEntidad INT
AS
BEGIN
	DELETE Proveedor.ClasedeEntidad WHERE IdClasedeEntidad = @IdClasedeEntidad
END

