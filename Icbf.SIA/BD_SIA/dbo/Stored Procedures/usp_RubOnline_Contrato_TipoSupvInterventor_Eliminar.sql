﻿/***********************************************
Creado Por: @ReS Soluciones.
			Cesar Casanova
Permite : Crear el usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar
***********************************************/
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_TipoSupvInterventor_Eliminar]
	@IdTipoSupvInterventor INT
AS
BEGIN
	DELETE Contrato.TipoSupvInterventor WHERE IdTipoSupvInterventor = @IdTipoSupvInterventor
END

