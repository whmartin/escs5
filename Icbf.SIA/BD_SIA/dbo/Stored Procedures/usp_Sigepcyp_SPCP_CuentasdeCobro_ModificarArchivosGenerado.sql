﻿-- =============================================
-- Author:		IIS APPPOOL\ASP.NET v4.0 Classic - Jorge Vizcaino
-- Create date:  8/11/2015 5:46:57 PM
-- Description:	Procedimiento almacenado que actualiza Histórico de CuentasdeCobro
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobro_ModificarArchivosGenerado]
		@IdCuenta nvarchar(4000)
		,@GeneradoCxP bit = null
		,@GeneradoObligaciones bit = null
		,@GeneradoOPP bit = null
		,@UsuarioModifica nvarchar(250)
AS
BEGIN	
	UPDATE	SPCP.CuentasdeCobro 
	SET		GeneradoCxP = isnull(@GeneradoCxP,GeneradoCxP)
	, GeneradoObligaciones = isnull(@GeneradoObligaciones,GeneradoObligaciones)
	, GeneradoOPP = isnull(@GeneradoOPP,GeneradoOPP)
	,UsuarioModifica = @UsuarioModifica
	,FechaModifica = GETDATE()
	WHERE IdCuenta in (select data from dbo.Split(@IdCuenta,';'))


	IF(@GeneradoObligaciones is NOT NULL )
	BEGIN
		INSERT INTO SPCP.HistoricoEstadoCuentasDeCobro (IDCuentaCobro,
														IDFlujoPasoCuentaAnterior,
														IDFlujoPasoCuentaNuevo,
														Aprobado,
														Descripcion,
														Roltransaccion,
														UsuarioCrea,
														FechaCrea)
		select data,0,0,0,'Se genero el archivo de obligaciones','Generado Archivo Obligaciones',@UsuarioModifica,GETDATE() from dbo.Split(@IdCuenta,';')
	END

	IF(@GeneradoOPP is NOT NULL )
	BEGIN
		INSERT INTO SPCP.HistoricoEstadoCuentasDeCobro (IDCuentaCobro,
														IDFlujoPasoCuentaAnterior,
														IDFlujoPasoCuentaNuevo,
														Aprobado,
														Descripcion,
														Roltransaccion,
														UsuarioCrea,
														FechaCrea)
		select data,0,0,0,'Se genero el archivo de orden de pago','Generado Archivo OPP',@UsuarioModifica,GETDATE() from dbo.Split(@IdCuenta,';')
	END
	
END



