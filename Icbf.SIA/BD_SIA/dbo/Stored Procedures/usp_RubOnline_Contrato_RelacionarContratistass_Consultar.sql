﻿
CREATE PROCEDURE [dbo].[usp_RubOnline_Contrato_RelacionarContratistass_Consultar]

	@IdContratistaContrato INT = NULL,
	@IdContrato INT = NULL,
	@NumeroIdentificacion BIGINT = NULL,
	@ClaseEntidad NVARCHAR = NULL,
	@PorcentajeParticipacion INT = NULL,
	@NumeroIdentificacionRepresentanteLegal BIGINT = NULL,
	@EstadoIntegrante Bit = Null

AS
BEGIN

	SELECT
		IdContratistaContrato,
		IdContrato,
		NumeroIdentificacion,
		ClaseEntidad,
		PorcentajeParticipacion,
		NumeroIdentificacionRepresentanteLegal,
		EstadoIntegrante,
		UsuarioCrea,
		FechaCrea,
		UsuarioModifica,
		FechaModifica
	FROM [Contrato].[RelacionarContratistas]
	WHERE IdContratistaContrato =
		CASE
			WHEN @IdContratistaContrato IS NULL THEN IdContratistaContrato ELSE @IdContratistaContrato
		END AND IdContrato =
		CASE
			WHEN @IdContrato IS NULL THEN IdContrato ELSE @IdContrato
		END AND NumeroIdentificacion =
		CASE
			WHEN @NumeroIdentificacion IS NULL THEN NumeroIdentificacion ELSE @NumeroIdentificacion
		END AND ClaseEntidad =
		CASE
			WHEN @ClaseEntidad IS NULL THEN ClaseEntidad ELSE @ClaseEntidad
		END AND PorcentajeParticipacion =
		CASE
			WHEN @PorcentajeParticipacion IS NULL THEN PorcentajeParticipacion ELSE @PorcentajeParticipacion
		END AND NumeroIdentificacionRepresentanteLegal =
		CASE
			WHEN @NumeroIdentificacionRepresentanteLegal IS NULL THEN NumeroIdentificacionRepresentanteLegal ELSE @NumeroIdentificacionRepresentanteLegal
		END
	END



