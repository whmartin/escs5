﻿
-- =============================================
-- Author:		Julio Cesar Hernandez Eugenio
-- Create date:  17/12/2015 11:03:34 AM
-- Description:	Procedimiento almacenado que consulta una cuenta de cobro por plan de pago
-- usp_Sigepcyp_SPCP_CuentasdeCobro_PlandePago_Consultar 9
-- =============================================
CREATE PROCEDURE [dbo].[usp_Sigepcyp_SPCP_CuentasdeCobro_PlandePago_Consultar]
	@IdDatosAdministracion INT, 
	@CodCompromiso INT,
	@NumeroDePago Varchar(15)
AS
BEGIN

SELECT	IdCuenta
FROM	[SPCP].[CuentasdeCobro]
WHERE	IdDatosAdministracion = @IdDatosAdministracion
		AND CodCompromiso = @CodCompromiso
		AND NumeroDePago = @NumeroDePago

END