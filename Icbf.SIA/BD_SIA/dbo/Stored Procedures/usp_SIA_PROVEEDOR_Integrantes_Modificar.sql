﻿-- =============================================
-- Author:		Leticia Elizabeth Gonzalez
-- Create date:  6/16/2014 4:43:12 PM
-- Description:	Procedimiento almacenado que actualiza un(a) Integrantes
-- Modificación: Juan Carlos Valverde Sámano
-- Fecha: 08/07/2014
-- Descripción: En la validación de que ya existe el integrante asociado, hacia falta comprobar con IdEntidad, Solo comprobaba por NumIdentificación.
-- =============================================
CREATE PROCEDURE [dbo].[usp_SIA_PROVEEDOR_Integrantes_Modificar]
		@IdIntegrante INT, @IdEntidad INT,	@IdTipoPersona INT,	@IdTipoIdentificacionPersonaNatural INT,	@NumeroIdentificacion NVARCHAR(50),	@PorcentajeParticipacion NUMERIC(5,2),	@ConfirmaCertificado NVARCHAR(5),	@ConfirmaPersona NVARCHAR(5), @UsuarioModifica NVARCHAR(250)
AS
BEGIN
	IF EXISTS(SELECT * FROM PROVEEDOR.Integrantes WHERE NumeroIdentificacion=@NumeroIdentificacion AND IdEntidad=@IdEntidad AND IdIntegrante <> @IdIntegrante)
	BEGIN
		RAISERROR('Este integrante ya fue asociado a este Proveedor.',16,1)
	    RETURN
	END
	UPDATE PROVEEDOR.Integrantes SET IdTipoPersona = @IdTipoPersona, IdEntidad = @IdEntidad, IdTipoIdentificacionPersonaNatural = @IdTipoIdentificacionPersonaNatural, NumeroIdentificacion = @NumeroIdentificacion, PorcentajeParticipacion = @PorcentajeParticipacion, ConfirmaCertificado = @ConfirmaCertificado, ConfirmaPersona = @ConfirmaPersona, UsuarioModifica = @UsuarioModifica, FechaModifica = GETDATE() WHERE IdIntegrante = @IdIntegrante
END


