﻿-- =============================================
-- Author:		Jeisson Lemus
-- Create date:  2015-12-23
-- Description:	Procedimiento almacenado que consulta usuarios
-- =============================================

CREATE PROCEDURE [dbo].[usp_RubOnline_Seg_ConsultarUsuarios]
	@IdPerfil int,
	@NumeroDocumento NVARCHAR(250),
	@PrimerNombre NVARCHAR(250),
	@PrimerApellido NVARCHAR(250),
	@Estado BIT = NULL
AS
BEGIN
  SELECT 
	   SEG.Usuario.IdUsuario
      ,SEG.Usuario.IdTipoDocumento
      ,Global.TiposDocumentos.NomTipoDocumento
      ,SEG.Usuario.NumeroDocumento
      ,SEG.Usuario.PrimerNombre
      ,SEG.Usuario.SegundoNombre
      ,SEG.Usuario.PrimerApellido
      ,SEG.Usuario.SegundoApellido
      ,SEG.Usuario.TelefonoContacto
      ,SEG.Usuario.CorreoElectronico
      ,SEG.Usuario.Estado
      ,SEG.Usuario.providerKey
      ,SEG.Usuario.UsuarioCreacion
      ,SEG.Usuario.FechaCreacion
      ,SEG.Usuario.UsuarioModificacion
      ,SEG.Usuario.FechaModificacion
	  ,SEG.Usuario.IdTipoPersona
	  ,SEG.Usuario.RazonSocial
	  ,SEG.Usuario.DV
	  ,SEG.Usuario.idRegional
	  ,seg.Usuario.AceptaEnvioCorreos
	  ,SEG.Usuario.TodosRegional
  FROM SEG.Usuario INNER JOIN [Global].[TiposDocumentos] ON SEG.Usuario.IdTipoDocumento=[Global].[TiposDocumentos].[IdTipoDocumento]
WHERE
	SEG.Usuario.NumeroDocumento = CASE WHEN @NumeroDocumento = '' THEN SEG.Usuario.NumeroDocumento ELSE @NumeroDocumento END
AND
	SEG.Usuario.PrimerNombre LIKE 	 CASE WHEN @PrimerNombre = '' THEN SEG.Usuario.PrimerNombre ELSE '%'+@PrimerNombre+'%' END
AND
	SEG.Usuario.PrimerApellido LIKE 	CASE WHEN @PrimerApellido = '' THEN SEG.Usuario.PrimerApellido ELSE '%'+@PrimerApellido+'%' END
AND 
	SEG.Usuario.Estado = CASE WHEN @Estado	IS NULL THEN SEG.Usuario.Estado ELSE @Estado END

END






