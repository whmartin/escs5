﻿using Icbf.ActualizacionAutomatica.DataAccess;
using Icbf.ActualizacionAutomatica.Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.ActualizacionAutomatica.DataAccess
{
    public class ActualizacionAutomaticaDAL : GeneralDAL
    {
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos()
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudes_Consultar"))
                {


                    using (IDataReader vDataReaderResults = vDataBase.ExecuteReader(vDbCommand))
                    {
                        List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCaso = new List<RegistroSolicitudEstudioSectoryCaso>();
                        while (vDataReaderResults.Read())
                        {
                            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
                            vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"] != DBNull.Value ? Convert.ToInt32(vDataReaderResults["IdRegistroSolicitudEstudioSectoryCaso"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso;

                            vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO = vDataReaderResults["VigenciaPACCO"] != DBNull.Value ? Convert.ToString(vDataReaderResults["VigenciaPACCO"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;

                            //vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial = vDataReaderResults["FechaSolicitudInicial"] != DBNull.Value ? Convert.ToDateTime(vDataReaderResults["FechaSolicitudInicial"].ToString()) : vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial;

                            vListaRegistroSolicitudEstudioSectoryCaso.Add(vRegistroSolicitudEstudioSectoryCaso);
                        }
                        return vListaRegistroSolicitudEstudioSectoryCaso;
                    }
                }
            }           
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ActualizarRegistroSolicitudEstudioSectoryCasos(int vigenciapacco, decimal consecutivo, string area, string codigoarea, string estado, string FechaInicioProceso, string Id_modalidad, decimal? Id_tipo_contrato, string modalidad, string objeto_contractual, string tipo_contrato, string FechaInicioEjecucion, decimal valor_contrato)
        {
            try
            {
                Database vDataBase = ObtenerInstancia();
                using (DbCommand vDbCommand = vDataBase.GetStoredProcCommand("usp_ICBF_ESC_RegistroSolicitudes_Actualizar"))
                {
                    int vResultado;
                    vDataBase.AddInParameter(vDbCommand, "@consecutivo", DbType.Decimal, consecutivo);
                    vDataBase.AddInParameter(vDbCommand, "@area", DbType.String, area);
                    vDataBase.AddInParameter(vDbCommand, "@vigenciapacco", DbType.Int32, vigenciapacco);
                    vDataBase.AddInParameter(vDbCommand, "@codigoarea", DbType.String, codigoarea);
                    vDataBase.AddInParameter(vDbCommand, "@estado", DbType.String, estado);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioProceso", DbType.String, FechaInicioProceso);
                    vDataBase.AddInParameter(vDbCommand, "@Id_modalidad", DbType.String, Id_modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@Id_tipo_contrato", DbType.String, Id_tipo_contrato);
                    vDataBase.AddInParameter(vDbCommand, "@modalidad", DbType.String, modalidad);
                    vDataBase.AddInParameter(vDbCommand, "@objeto_contractual", DbType.String, objeto_contractual);
                    vDataBase.AddInParameter(vDbCommand, "@tipo_contrato", DbType.String, tipo_contrato);
                    vDataBase.AddInParameter(vDbCommand, "@FechaInicioEjecucion", DbType.String, FechaInicioEjecucion);
                    vDataBase.AddInParameter(vDbCommand, "@valor_contrato", DbType.Decimal, valor_contrato);

                    vResultado = vDataBase.ExecuteNonQuery(vDbCommand);

                    //CreaLogActualizacion(Userp);
                    return vResultado;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
