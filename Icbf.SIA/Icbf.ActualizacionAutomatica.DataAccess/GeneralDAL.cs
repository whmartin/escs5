﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.ActualizacionAutomatica.DataAccess
{
    public class GeneralDAL
    {
        public GeneralDAL()
        {

        }
        public Microsoft.Practices.EnterpriseLibrary.Data.Database ObtenerInstancia()
        {
            //return DatabaseFactory.CreateDatabase("DataBaseConnectionString");
            return Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
        }
    }
}
