﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.ActualizacionAutomatica.Entity
{
    public class TiemposPACCO
    {
        public decimal IdConsecutivoEstudio
        {
            get;
            set;
        }
        public decimal IdTiempoEntreActividades
        {
            get; set;
        }
        public string FEstimadaFCTPreliminarREGINICIAL
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTPreliminarREGINICIAL
        {
            get;
            set;
        }
        public string FEstimadaFCTPreliminar
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTPreliminar
        {
            get;
            set;
        }
        public string FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado
        {
            get;
            set;
        }
        public string FEestimadaES_EC
        {
            get;
            set;
        }
        public bool? NoAplicaFEestimadaES_EC
        {
            get;
            set;
        }
        public string FEstimadaDocsRadicadosEnContratos
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaDocsRadicadosEnContratos
        {
            get;
            set;
        }
        public string FEstimadaComiteContratacion
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaComiteContratacion
        {
            get;
            set;
        }
        public string FEstimadaInicioDelPS
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaInicioDelPS
        {
            get;
            set;
        }
        public string FEstimadaPresentacionPropuestas
        {
            get;
            set;
        }
        public bool? NoAplicaFEstimadaPresentacionPropuestas
        {
            get;
            set;
        }
        public string FEstimadaDeInicioDeEjecucion { get; set; }

        public string UsuarioCrea
        {
            get;
            set;
        }
        public string UsuarioModifica
        {
            get;
            set;
        }
        public DateTime? FechaCrea
        {
            get;
            set;
        }
        public DateTime? FechaModifica
        {
            get;
            set;
        }
    }
}
