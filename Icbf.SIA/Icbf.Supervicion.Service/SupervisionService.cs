﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Supervision.Business;
using Icbf.Supervision.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Supervision.Service
{
    public class SupervisionService
    {

        private SupervisionPreguntasBLL vSupervisionPreguntasBLL;
        private SupervisionRespuestasBLL vSupervisionRespuestasBLL;
        private SupervisionCriteriosBLL vSupervisionCriteriosBLL;
        private SupervisionReglasCriteriosBLL vSupervisionReglasCriteriosBLL;
        private SupervisionResponsablesBLL vSupervisionResponsablesBLL;
        private SupervisionCuestionarioBLL vSupervisionCuestionarioDLL;
        private SupervisionSubcomponentesBLL vSupervisionSubcomponentesBLL;
        private SupervisionGruposApoyoBLL vSupervisionGruposApoyoBLL;
        private SupervisionGruposApoyoContratosBLL vSupervisionGruposApoyoContratosBLL;
        private SupervisionGruposApoyoIntegrantesBLL vSupervisionGruposApoyoIntegrantesBLL;
        private SupervisionDireccionBLL vSupervisionDireccionBLL;

        public SupervisionService()
        {
            vSupervisionPreguntasBLL = new SupervisionPreguntasBLL();
            vSupervisionRespuestasBLL = new SupervisionRespuestasBLL();
            vSupervisionCriteriosBLL = new SupervisionCriteriosBLL();
            vSupervisionReglasCriteriosBLL = new SupervisionReglasCriteriosBLL();
            vSupervisionResponsablesBLL = new SupervisionResponsablesBLL();
            vSupervisionCuestionarioDLL = new SupervisionCuestionarioBLL();
            vSupervisionSubcomponentesBLL = new SupervisionSubcomponentesBLL();
            vSupervisionGruposApoyoBLL = new SupervisionGruposApoyoBLL();
            vSupervisionGruposApoyoContratosBLL = new SupervisionGruposApoyoContratosBLL();
            vSupervisionGruposApoyoIntegrantesBLL = new SupervisionGruposApoyoIntegrantesBLL();
            vSupervisionDireccionBLL = new SupervisionDireccionBLL();

        }

        #region "Preguntas"
        public int InsertarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                return vSupervisionPreguntasBLL.InsertarSupervisionPreguntas(pSupervisionPreguntas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                return vSupervisionPreguntasBLL.ModificarSupervisionPreguntas(pSupervisionPreguntas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionPreguntas(SupervisionPreguntas pSupervisionPreguntas)
        {
            try
            {
                return vSupervisionPreguntasBLL.EliminarSupervisionPreguntas(pSupervisionPreguntas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionPreguntas ConsultarSupervisionPreguntas(int pIdPregunta)
        {
            try
            {
                return vSupervisionPreguntasBLL.ConsultarSupervisionPreguntas(pIdPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int ConsultarSupervisionPreguntas(string NombrePregunta, int? Aplicable)
        {
            try
            {
                return vSupervisionPreguntasBLL.ConsultarSupervisionPreguntas(NombrePregunta, Aplicable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<SupervisionPreguntas> ConsultarSupervisionPreguntass(int? pIdPregunta, int? pIdDireccion, String pNombre, String pDescripcion, int? pIdTipoRespuesta, int? pAplicable, int? pResponsable, int? pEstado, String pUsuarioCrea)
        {
            try
            {
                return vSupervisionPreguntasBLL.ConsultarSupervisionPreguntass(pIdPregunta, pIdDireccion, pNombre, pDescripcion, pIdTipoRespuesta, pAplicable, pResponsable, pEstado, pUsuarioCrea);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionPreguntas> ConsultarTipoRespuesta()
        {
            try
            {
                return vSupervisionPreguntasBLL.ConsultarTipoRespuesta();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }
        #endregion
        #region "Respuestas"
        public int InsertarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                return vSupervisionRespuestasBLL.InsertarSupervisionRespuestas(pSupervisionRespuestas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                return vSupervisionRespuestasBLL.ModificarSupervisionRespuestas(pSupervisionRespuestas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionRespuestas(SupervisionRespuestas pSupervisionRespuestas)
        {
            try
            {
                return vSupervisionRespuestasBLL.EliminarSupervisionRespuestas(pSupervisionRespuestas);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionRespuestas ConsultarSupervisionRespuestas(int pIdRespuesta)
        {
            try
            {
                return vSupervisionRespuestasBLL.ConsultarSupervisionRespuestas(pIdRespuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionRespuestas(string Valor)
        {
            try
            {
                return vSupervisionRespuestasBLL.ConsultarSupervisionRespuestas(Valor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionRespuestas> ConsultarSupervisionRespuestass(int? pIdPregunta, String pValor, Boolean? pVulneraDerecho, int? pEstado)
        {
            try
            {
                return vSupervisionRespuestasBLL.ConsultarSupervisionRespuestass(pIdPregunta, pValor, pVulneraDerecho, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region "Criterios"

        public int InsertarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                return vSupervisionCriteriosBLL.InsertarSupervisionCriterios(pSupervisionCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                return vSupervisionCriteriosBLL.ModificarSupervisionCriterios(pSupervisionCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionCriterios(SupervisionCriterios pSupervisionCriterios)
        {
            try
            {
                return vSupervisionCriteriosBLL.EliminarSupervisionCriterios(pSupervisionCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionCriterios(string Valor)
        {
            try
            {
                return vSupervisionCriteriosBLL.ConsultarSupervisionCriterios(Valor);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionCriterios ConsultarSupervisionCriterios(int pIdCriterio)
        {
            try
            {
                return vSupervisionCriteriosBLL.ConsultarSupervisionCriterios(pIdCriterio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCriterios> ConsultarSupervisionCriterioss(int? pIdPregunta, String pValor, int? pEstado)
        {
            try
            {
                return vSupervisionCriteriosBLL.ConsultarSupervisionCriterioss(pIdPregunta, pValor, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        #endregion
        #region "ReglasCriterios"
        //public List<SupervisionReglasCriterios> ConsultarSupervisionReglasCriterios(int? pIdPregunta, String pValor, int? pEstado)


        /************************REGLAS CRITERIOS************************/

        public List<SupervisionReglasCriterios> ConsultarSupervisionReglasCriterios(int? pIdCriterioRespuesta, int? pIdRespuesta, int? pTipoconf, int? pEstado, int? pTipoConsulta)
        {
            try
            {
                return vSupervisionReglasCriteriosBLL.ConsultarSupervisionReglasCriterios(pIdCriterioRespuesta, pIdRespuesta, pTipoconf, pEstado, pTipoConsulta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public SupervisionReglasCriterios InsertarSupervisionReglasCriterios(SupervisionReglasCriterios pObjSupReglasCriterios, System.Data.DataTable dtbCriterios)
        {
            try
            {
                return vSupervisionReglasCriteriosBLL.InsertarSupervisionReglasCriterios(pObjSupReglasCriterios, dtbCriterios);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region "Responsables"
        public SupervisionPreguntas ConsultarDatosFiltrosSupervisionCriterios(int? pIdRespuesta)
        {
            try
            {
                return vSupervisionReglasCriteriosBLL.ConsultarDatosFiltrosSupervisionCriterios(pIdRespuesta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /************************REGLAS CRITERIOS************************/

        public int InsertarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                return vSupervisionResponsablesBLL.InsertarSupervisionResponsables(pSupervisionResponsables);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                return vSupervisionResponsablesBLL.ModificarSupervisionResponsables(pSupervisionResponsables);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionResponsables(SupervisionResponsables pSupervisionResponsables)
        {
            try
            {
                return vSupervisionResponsablesBLL.EliminarSupervisionResponsables(pSupervisionResponsables);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionResponsables ConsultarSupervisionResponsables(int pIdResponsable)
        {
            try
            {
                return vSupervisionResponsablesBLL.ConsultarSupervisionResponsables(pIdResponsable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionResponsables(string Nombre)
        {
            try
            {
                return vSupervisionResponsablesBLL.ConsultarSupervisionResponsables(Nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionResponsables> ConsultarSupervisionResponsabless(int? pIdDireccion, String pNombre, int? pEstado)
        {
            try
            {
                return vSupervisionResponsablesBLL.ConsultarSupervisionResponsabless(pIdDireccion, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region "Cuestionario"

        public int InsertarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDLL.InsertarSupervisionCuestionario(pSupervisionCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDLL.ModificarSupervisionCuestionario(pSupervisionCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionCuestionario(SupervisionCuestionario pSupervisionCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDLL.EliminarSupervisionCuestionario(pSupervisionCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionCuestionario ConsultarSupervisionCuestionario(int pIdCuestionario)
        {
            try
            {
                return vSupervisionCuestionarioDLL.ConsultarSupervisionCuestionario(pIdCuestionario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCuestionario> ConsultarSupervisionCuestionarios(String pNombre, int? pIdDireccion, int? pIdVigenciaServicio, int? pIdModalidadServicio, int? pIdSubComponente, int? pIdComponente, int? pEstado)
        {
            try
            {
                return vSupervisionCuestionarioDLL.ConsultarSupervisionCuestionarios(pNombre, pIdDireccion, pIdVigenciaServicio, pIdModalidadServicio, pIdSubComponente, pIdComponente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionCuestionario> ConsultarSupervisionCuestionarioPreguntas(int? IdPregunta)
        {
            try
            {
                return vSupervisionCuestionarioDLL.ConsultarSupervisionCuestionarioPreguntas(IdPregunta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region "Subcomponentes"

        public int InsertarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.InsertarSupervisionSubcomponentes(pSupervisionSubcomponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.ModificarSupervisionSubcomponentes(pSupervisionSubcomponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionSubcomponentes(SupervisionSubcomponentes pSupervisionSubcomponentes)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.EliminarSupervisionSubcomponentes(pSupervisionSubcomponentes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionSubcomponentes ConsultarSupervisionSubcomponentes(int pIdSubcomponente)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.ConsultarSupervisionSubcomponentes(pIdSubcomponente);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionSubcomponentes(string Nombre)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.ConsultarSupervisionSubcomponentes(Nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionSubcomponentes> ConsultarSupervisionSubcomponentess(int? pIdDireccion, String pNombre, int? pEstado)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.ConsultarSupervisionSubcomponentess(pIdDireccion, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<SupervisionComponentes> ConsultarSupervisionComponentes(int? pIdComponente, int? pCodComponente, String pNombreComponente, int? pEstado)
        {
            try
            {
                return vSupervisionSubcomponentesBLL.ConsultarSupervisionComponentes(pIdComponente, pCodComponente, pNombreComponente, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {

                throw new GenericException(ex);
            }
        }

        #endregion
        #region "GruposApoyo"

        public int InsertarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                return vSupervisionGruposApoyoBLL.InsertarSupervisionGruposApoyo(pSupervisionGruposApoyo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                return vSupervisionGruposApoyoBLL.ModificarSupervisionGruposApoyo(pSupervisionGruposApoyo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyo(SupervisionGruposApoyo pSupervisionGruposApoyo)
        {
            try
            {
                return vSupervisionGruposApoyoBLL.EliminarSupervisionGruposApoyo(pSupervisionGruposApoyo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyo ConsultarSupervisionGruposApoyo(int pIdGrupo)
        {
            try
            {
                return vSupervisionGruposApoyoBLL.ConsultarSupervisionGruposApoyo(pIdGrupo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarSupervisionGruposApoyo(String Nombre)
        {
            try
            {
                return vSupervisionGruposApoyoBLL.ConsultarSupervisionGruposApoyo(Nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyo> ConsultarSupervisionGruposApoyos(int? pIdDireccion, int? pIdRegional, String pNombre, int? pEstado)
        {
            try
            {
                return vSupervisionGruposApoyoBLL.ConsultarSupervisionGruposApoyos(pIdDireccion, pIdRegional, pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region "Contratos"

        public int InsertarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos, System.Data.DataTable pDtbContratos)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.InsertarSupervisionGruposApoyoContratos(pSupervisionGruposApoyoContratos, pDtbContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.ModificarSupervisionGruposApoyoContratos(pSupervisionGruposApoyoContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyoContratos(SupervisionGruposApoyoContratos pSupervisionGruposApoyoContratos)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.EliminarSupervisionGruposApoyoContratos(pSupervisionGruposApoyoContratos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyoContratos ConsultarSupervisionGruposApoyoContratos(int pIdContratosGrupo, int pIdGrupo, int pIdContrato)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.ConsultarSupervisionGruposApoyoContratos(pIdContratosGrupo, pIdGrupo, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyoContratos> ConsultarSupervisionGruposApoyoContratoss(int? pIdContratosGrupo, int? pIdGrupo, int? pIdDireccion, int? pIdRegional, int? pVigencia, int? pEstado)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.ConsultarSupervisionGruposApoyoContratoss(pIdContratosGrupo, pIdGrupo, pIdDireccion, pIdRegional, pVigencia, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyoContratos> ConsultarContratosCuentame(Int32? pVigencia, string pIdRegional, Int16 IdDireccion)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.ConsultarContratosCuentame(pVigencia, pIdRegional, IdDireccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        public List<SupervisionGruposApoyoContratos> ConsultarContratosSim(int? pVigencia, string pIdRegional)
        {
            try
            {
                return vSupervisionGruposApoyoContratosBLL.ConsultarContratosSim(pVigencia, pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        #endregion
        #region "Integrantes"
        public int InsertarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesBLL.InsertarSupervisionGruposApoyoIntegrantes(pSupervisionGruposApoyoIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesBLL.ModificarSupervisionGruposApoyoIntegrantes(pSupervisionGruposApoyoIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarSupervisionGruposApoyoIntegrantes(SupervisionGruposApoyoIntegrantes pSupervisionGruposApoyoIntegrantes)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesBLL.EliminarSupervisionGruposApoyoIntegrantes(pSupervisionGruposApoyoIntegrantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public SupervisionGruposApoyoIntegrantes ConsultarSupervisionGruposApoyoIntegrantes(int pIdIntegranteGrupo, int pIdGrupo)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesBLL.ConsultarSupervisionGruposApoyoIntegrantes(pIdIntegranteGrupo, pIdGrupo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<SupervisionGruposApoyoIntegrantes> ConsultarSupervisionGruposApoyoIntegrantess(int? pIdIntegranteGrupo, int? pIdGrupo, int? pIdentificacion, String pNombres, String pApellidos, int? pEstado)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesBLL.ConsultarSupervisionGruposApoyoIntegrantess(pIdIntegranteGrupo, pIdGrupo, pIdentificacion, pNombres, pApellidos, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public List<SupervisionGruposLupaPersona> ConsultarPersonaLupa(String pIdTipoDocumento, Int64? pNumeroIdentificacion, String pNombre1, String pNombre2, String pApellido1, String pApellido2)
        {
            try
            {
                return vSupervisionGruposApoyoIntegrantesBLL.ConsultarPersonaLupa(pIdTipoDocumento, pNumeroIdentificacion, pNombre1, pNombre2, pApellido1, pApellido2);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region "Direccion"
        public List<SupervisionDireccion> ConsultarSupervisionDireccions(int? pIdDireccionesICBF, String pCodigoDireccion, String pNombreDireccion, int? pEstado)
        {
            try
            {
                return vSupervisionDireccionBLL.ConsultarSupervisionDireccions(pIdDireccionesICBF, pCodigoDireccion, pNombreDireccion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }


}
