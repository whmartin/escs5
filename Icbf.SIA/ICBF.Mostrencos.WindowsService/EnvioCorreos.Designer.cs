﻿namespace ICBF.Mostrencos.WindowsService
{
    partial class EnvioCorreos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spLapso = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.spLapso)).BeginInit();
            // 
            // spLapso
            // 
            this.spLapso.Enabled = true;
            this.spLapso.Interval = 1200D;
            this.spLapso.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
            // 
            // EnvioCorreos
            // 
            this.ServiceName = "EnvioCorreos";
            ((System.ComponentModel.ISupportInitialize)(this.spLapso)).EndInit();

        }

        #endregion

        private System.Timers.Timer spLapso;
    }
}
