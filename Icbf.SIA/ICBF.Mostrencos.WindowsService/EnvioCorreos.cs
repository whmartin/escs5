﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ICBF.Mostrencos.WindowsService
{
    partial class EnvioCorreos : ServiceBase
    {
        public EnvioCorreos()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            spLapso.Start();
        }

        protected override void OnStop()
        {
            spLapso.Stop();
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

        }
    }
}
